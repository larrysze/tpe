using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Threading;
using System.Drawing;
using dotnetCHARTING;
using System.Collections.Specialized;
using TPE.Utility;

namespace TPECharts
{
    /// <summary>
    /// Summary description for WebCustomControl1.
    /// </summary>
    [DefaultProperty("Text"),
    ToolboxData("<{0}:TPEChartsControl runat=server></{0}:TPEChartsControl>")]
    public class TPEChartsControl : System.Web.UI.WebControls.WebControl, IPostBackDataHandler
    {
        //onload=preload_images()
        private string contractID = "1";
        private string period = "1Y";
        private string size = "1";
        private string market = "D";
        private string connectionStringDB = "";
        private string chartsFolder = "";

        public string ConnectionStringDB
        {
            set
            {
                connectionStringDB = value;
            }
        }

        public string ContractID
        {
            get
            {
                return contractID;
            }

            set
            {
                contractID = value;
                ViewState["contractID"] = value;
            }
        }

        public string Period
        {
            get
            {
                return period;
            }

            set
            {
                period = value;
                ViewState["period"] = value;
            }
        }

        [DefaultValue("1"),
        Description("Size of the charts. Use 1 to small and 2 to large")]
        public string Size
        {
            get
            {
                return size;
            }

            set
            {
                if ((value.ToString() == "1") || (value.ToString() == "2"))
                {
                    size = value;
                    ViewState["size"] = value;
                }
            }
        }

        [DefaultValue("D"),
        Description("Type of market. Use D to Domestic and I to International")]
        public string Market
        {
            get
            {
                return market;
            }

            set
            {
                if ((value.ToString() == "D") || (value.ToString() == "I"))
                {
                    market = value;
                    ViewState["market"] = value;
                }
            }
        }

        private string PrefixFile
        {
            get
            {
                string prefix = "";
                if (size == "2") prefix = prefix + "B_";
                if (market == "I") prefix = prefix + "I_";
                return prefix;
            }
        }

        private int tdWidth
        {
            get
            {
                if (Size == "1")//small
                    return 352;
                else //large
                    return 509;
            }
        }

        private int tabWidth1m
        {
            get
            {
                if (Size == "1")//small
                    return 62;
                else //large
                    return 82;
            }
        }

        //		private int tabWidth6m
        //		{
        //			get
        //			{
        //				if (Size=="1")//small
        //					return 62;
        //				else //large
        //					return 82;
        //			}
        //		}

        private int tabWidth1y
        {
            get
            {
                if (Size == "1")//small
                    return 62;
                else //large
                    return 71;
            }
        }

        private int tabWidth5y
        {
            get
            {
                if (Size == "1")//small
                    return 53;
                else //large
                    return 69;
            }
        }

        private int tabWidth10y
        {
            get
            {
                if (Size == "1")//small
                    return 72;
                else //large
                    return 72;
            }
        }

        private int backgroundWidth
        {
            get
            {
                if (Size == "1")//small
                    return 100;
                else //large
                    return 206;
            }
        }

        /// <summary>
        /// Render this control to the output parameter specified.
        /// </summary>
        /// <param name="output"> The HTML writer to write out to </param>
        protected override void Render(HtmlTextWriter output)
        {
            string outPut =
                @"<table border=0 cellpadding=0 cellspacing=0 bgcolor=""#000000"" width=" + tdWidth + " >" +
                "<tr>" +
                @"<td align=""center""  valign=""bottom"" width=" + tdWidth + ">" +
                "<table cellpadding=0 cellspacing=0 border=0 width=" + tdWidth + " align=center>" +
                "<tr width=50%>";
            //			if (Size=="1")
            //			{
            /*
                @outPut = outPut +
                    "<td><img name=1m id=1m onmouseover=\"changeImage('1m','1y','5y','10y','1M'); __" + ClientID + "_State.value='1M';\" src=\"./Charts/tabs/1mOFF.gif\" width=" + tabWidth1m + " height=13></td>" +
                    //"<td><img name=6m id=6m onmouseover=\"changeImage('6m','1y','1m','5y','6M'); __" + ClientID + "_State.value='6M';\" src=\"./Charts/tabs/6mOFF.gif\" width=" + tabWidth6m + " height=13></td>" +
                    "<td><img name=1y id=1y onmouseover=\"changeImage('1y','5y','10y','1m','1Y'); __" + ClientID + "_State.value='1Y';\" src=\"./Charts/tabs/1yOFF.gif\"  width=" + tabWidth1y + " height=13></td>" +
                    "<td><img name=5y id=5y onmouseover=\"changeImage('5y','10y','1m','1y','5Y'); __" + ClientID + "_State.value='5Y';\" src=\"./Charts/tabs/5yOFF.gif\" width=" + tabWidth5y + " height=13></td>" +
                    "<td><img name=10y id=10y onmouseover=\"changeImage('10y','1m','1y','5y','10Y'); __" + ClientID + "_State.value='10Y';\" src=\"./Charts/tabs/10yOFF.gif\" width=" + tabWidth10y + " height=13></td>";
            "<img name=1m id=1m src=\"./Charts/tabs/1mOFF.gif\" width=" + tabWidth1m + " height=13>" +
            "<img name=1y id=1y src=\"./Charts/tabs/1yOFF.gif\"  width=" + tabWidth1y + " height=13>" +
            "<img name=5y id=5y src=\"./Charts/tabs/5yOFF.gif\" width=" + tabWidth5y + " height=13>" +
            "<img name=10y id=10y  src=\"./Charts/tabs/10yOFF.gif\" width=" + tabWidth10y + " height=13>" + 
*/

            @outPut = outPut +

            "<td align=\"center\" bgcolor='#FEA503' onmouseover=\"changeImage('1m','1y','5y','10y','1M');\"><b>1 month</b></td>" +
            "<TD bgcolor='black' width='1px'></TD>" +
            "<td align=\"center\" bgcolor='#FEA503' onmouseover=\"changeImage('1y','5y','10y','1m','1Y');\"><b>1 year</b></td>" +
            "<TD bgcolor='black' width='1px'></TD>" +
            "<td align=\"center\" bgcolor='#FEA503' onmouseover=\"changeImage('5y','10y','1m','1y','5Y');\"><b>5 years</b></td>" +
            "<TD bgcolor='black' width='1px'></TD>" +
            "<td align=\"center\" bgcolor='#FEA503' onmouseover=\"changeImage('10y','1m','1y','5y','10Y');\"><b>10 years</b></td>";


            //			}
            //			else //large
            //			{
            //				@outPut = outPut + //1---'1m','1y','5y','10y','1M' , 3---'1y','5y','1m','10y','1Y'
            //					"<td><img name=1m id=1m onmouseover=\"changeImage('1m','6m','1y','5y','1M'); __" + ClientID + "_State.value='1M';\" src=\"./Charts/tabs/1mOFF.gif\" width=" + tabWidth1m + " height=13></td>" +
            //					//"<td><img name=6m id=6m onmouseover=\"changeImage('6m','1y','1m','5y','6M'); __" + ClientID + "_State.value='6M';\" src=\"./Charts/tabs/6mOFF.gif\" width=" + tabWidth6m + " height=13></td>" +
            //					"<td><img name=1y id=1y onmouseover=\"changeImage('1y','5y','6m','10y','1Y'); __" + ClientID + "_State.value='1Y';\" src=\"./Charts/tabs/1yOFF.gif\"  width=" + tabWidth1y + " height=13></td>" +
            //					"<td><img name=5y id=5y onmouseover=\"changeImage('5y','10y','1y','1m','5Y'); __" + ClientID + "_State.value='5Y';\" src=\"./Charts/tabs/5yOFF.gif\" width=" + tabWidth5y + " height=13></td>" +
            //					"<td><img name=10y id=10y onmouseover=\"changeImage('10y','1m','5y','1y','10Y'); __" + ClientID + "_State.value='10Y';\" src=\"./Charts/tabs/10yOFF.gif\" width=" + tabWidth10y + " height=13></td>";
            //			}
            @outPut = outPut +
                //				@"<td><img name=white src=""./Charts/tabs/white.gif"" width=" + backgroundWidth + " height=13></td>" +
                "</tr>" +
                //if we want to split header of chart and graph
                "<tr><td>&nbsp;</td></tr>" +

                "<tr>";

            if (Size == "1")
            {
                @outPut = outPut +
                    @"<td colspan=""7"" STYLE=""border-right: 1px outset #61698C;border-left: 1px outset #61698C;border-bottom: 1px outset #61698C"">" +
                    @"<img id=""ChartsImage"" name=""ChartsImage"" width=""350"" height=""250"" src=""./Charts/tabs/loading.gif"">" +
                    "</td>";
            }
            else
            {
                @outPut = outPut +
                    @"<td colspan=""7"" STYLE=""border-right: 1px outset #61698C;border-left: 1px outset #61698C;border-bottom: 1px outset #61698C"">" +
                    @"<img id=""ChartsImage"" name=""ChartsImage"" width=""500"" height=""330"" src=""./Charts/tabs/loading.gif"">" +
                    "</td>";
            }
            @outPut = outPut +
                "</tr>" +
                "</table>" +
                "</td>" +
                "</tr>" +
                "</table>";
            output.Write(outPut);
        }


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (Page != null)
            {
                Page.RegisterRequiresPostBack(this);
            }
        }


        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            bool changed = false;
            if ((ViewState["contractID"] != null) && (market != (string)ViewState["market"]))
            {
                market = (string)ViewState["market"];
                changed = true;
            }

            if ((ViewState["contractID"] != null) && (contractID != (string)ViewState["contractID"]))
            {
                contractID = (string)ViewState["contractID"];
                changed = true;
            }

            if ((postCollection["__" + ClientID + "_State"] != null) && (period != (string)postCollection["__" + ClientID + "_State"]))
            {
                period = (string)postCollection["__" + ClientID + "_State"];
                changed = true;
            }

            if ((ViewState["size"] != null) && (size != (string)ViewState["size"]))
            {
                size = (string)ViewState["size"];
                changed = true;
            }
            return changed;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (Page != null)
            {
                Page.RegisterHiddenField("__" + ClientID + "_State", Period);
                registerFunction(this.Page);
            }
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            //do nothing
        }

        public void setFolder(string path)
        {
            this.chartsFolder = path;
        }


        /// <summary>
        /// This method must be used before Page_Prerender event.
        /// </summary>
        /// <param name="page">Page which the charts will be shown</param>
        public void registerFunction(System.Web.UI.Page page)
        {
            //Set the folder
            this.chartsFolder = @page.Server.MapPath("charts").ToString();

            //Trigger the Thread which will chack the charts
            if ((connectionStringDB.ToString() != "") && (connectionStringDB != null))
            {
                Thread threadCharts = new Thread(new ThreadStart(check_images));
                threadCharts.Start();
            }

            string jsB = "";
            //Script for handle charts
            jsB = jsB + @"	<script LANGUAGE=""JAVASCRIPT"">" + "\n";
            jsB = jsB + @"	<!--" + "\n";
            jsB = jsB + @"	function preload_images()" + "\n";
            jsB = jsB + @"	{" + "\n";
            jsB = jsB + @"		document.ChartsImage.src='./Charts/" + PrefixFile + "Chart_" + ContractID + "_" + Period + ".png';" + "\n";
            // set the rollover images sources
            jsB = jsB + @"		image3 = new Image();" + "\n";
            jsB = jsB + @"		image3.src = './Charts/" + PrefixFile + "Chart_" + ContractID + "_1Y.png';" + "\n";
            jsB = jsB + @"		image2 = new Image();" + "\n";
            jsB = jsB + @"		image2.src = './Charts/" + PrefixFile + "Chart_" + ContractID + "_1M.png';" + "\n";
            jsB = jsB + @"		image1 = new Image();" + "\n";
            jsB = jsB + @"		image1.src = './Charts/" + PrefixFile + "Chart_" + ContractID + "_5Y.png';" + "\n";
            jsB = jsB + @"		image4 = new Image();" + "\n";
            jsB = jsB + @"		image4.src = './Charts/" + PrefixFile + "Chart_" + ContractID + "_10Y.png';" + "\n";
            //			if (Size=="1") //small
            //			{
            //				jsB = jsB + @"		image5 = new Image();"+ "\n";
            //				jsB = jsB + @"		image5.src = './Charts/" + PrefixFile + "Chart_" + ContractID + "_6M.jpg';"+ "\n";
            //			}

            //check which period is chosen to select the right ON, OFF and NEAR images in the menu";

            //			if (Size=="1") //small
            //			{
            if (period == "1M")
                jsB = jsB + @"	changeImage('1m','1y','5y','10y','1M');" + "\n";
            //else if (period=="6M")
            //	jsB = jsB + @"	changeImage('6m','1y','1m','10y','6M');"+ "\n";
            else if (period == "1Y")
                jsB = jsB + @"	changeImage('1y','5y','10y','1m','1Y');" + "\n";
            else if (period == "5Y")
                jsB = jsB + @"	changeImage('5y','10y','1m','1y','5Y');" + "\n";
            else if (period == "10Y")
                jsB = jsB + @"	changeImage('10y','1m','1y','5y','10Y');" + "\n";
            else
                jsB = jsB + @"	changeImage('1y','5y','10y','1m','1Y');" + "\n";
            jsB = jsB + @"	}" + "\n";
            //			}
            //			else //(Size=="2") //large
            //			{
            //				if(period=="1M")
            //					jsB = jsB + @"	changeImage('1m','1y','5y','10y','1M');"+ "\n";
            //				//else if (period=="6M")
            //				//	jsB = jsB + @"	changeImage('6m','1y','1m','10y','6M');"+ "\n";
            //				else if(period=="1Y")
            //					jsB = jsB + @"	changeImage('1y','5y','10y','1m','1Y');"+ "\n";
            //				else if(period=="5Y")
            //					jsB = jsB + @"	changeImage('5y','10y','1y','1m','5Y');"+ "\n";
            //				else if(period=="10Y")
            //					jsB = jsB + @"	changeImage('10y','1m','5y','1m','10Y');"+ "\n";
            //				jsB = jsB + @"	}" + "\n";
            //			}

            jsB = jsB + @"	function reload_image(Cont_Id,period)" + "\n";
            jsB = jsB + @"	{" + "\n";
            jsB = jsB + @"		document.ChartsImage.src='./Charts/" + PrefixFile + "Chart_'+Cont_Id+'_'+period+'.png';" + "\n";
            jsB = jsB + @"		//document.Form.period.value=period;" + "\n";
            jsB = jsB + @"		//changeImage();" + "\n";
            jsB = jsB + @"		//we need to retrieve from AJAX-function map name and map data" + "\n";
            jsB = jsB + @"		//localhost.include.AjaxFunctions.getChartMapInfo('/Research/Charts/" + PrefixFile + "Chart_'+Cont_Id + '_' + period + '.map', getChartMapInfo_callback);";
            jsB = jsB + @"		//changeImage();" + "\n";
            jsB = jsB + @"	}" + "\n";
            /*
                        jsB = jsB + @"	function getChartMapInfo_callback(res)"+ "\n";
                        jsB = jsB + @"	{//change the images for the menu"+ "\n";
                        jsB = jsB + @"		document.ChartsImage.src='./Charts/' + res[0] + '.jpg';";
                        jsB = jsB + @"		document.ChartImage.useMap =  + res[1];";
                        jsB = jsB + @"		document.chartmap.innerHTML =   + res[2];";
                        jsB = jsB + @"	";
                        jsB = jsB + @"	}";
            */

            jsB = jsB + @"	function changeImage(nameImage,nameNearImage,off1,off2,period)" + "\n";
            jsB = jsB + @"	{//change the images for the menu" + "\n";
            /*
                        jsB = jsB + @"		document.images[nameImage].src=""./Charts/tabs/""+nameImage+""ON.gif"";"+ "\n";
                        jsB = jsB + @"		document.images[nameNearImage].src=""./Charts/tabs/""+nameNearImage+""NEAR.gif"";"+ "\n";
                        jsB = jsB + @"		document.images[off1].src=""./Charts/tabs/""+off1+""OFF.gif"";"+ "\n";
                        jsB = jsB + @"		document.images[off2].src=""./Charts/tabs/""+off2+""OFF.gif"";"+ "\n";
                        */
            jsB = jsB + @"		reload_image(" + ContractID + ",period);" + "\n";
            jsB = jsB + @"	}" + "\n";

            jsB = jsB + @"	-->" + "\n";
            jsB = jsB + @"	</script>" + "\n";

            page.RegisterClientScriptBlock("Script Charts", jsB);




            string jsB2 = "";
            jsB2 = jsB2 + @"	<script LANGUAGE=""JAVASCRIPT"">" + "\n";
            jsB2 = jsB2 + @"		preload_images();" + "\n";
            jsB2 = jsB2 + @"	</script>" + "\n";

            page.RegisterStartupScript("Script Charts2", jsB2);
        }

        #region Methods to Create new Charts
        
        public void check_images()
        {


            string Location = @chartsFolder;

            createHomePageCharts();

            int old = 0;
            int haveFiles = 0;
            DirectoryInfo dir = new DirectoryInfo(Location);
            DateTime now = DateTime.Now;
            foreach (FileInfo f in dir.GetFiles("*.png"))
            {// get the creation date of each image in the directory
                haveFiles = 1;
                string fullName = dir.FullName + "\\" + f.Name;
                //TimeSpan span = DateTime.Now.Subtract(f.LastWriteTime);

                if ((f.LastWriteTime.Year != now.Year) || (f.LastWriteTime.Month != now.Month) || (f.LastWriteTime.Day != now.Day))
                {
                    old = 1;
                    break;
                }

            }
//            if((old>0 || haveFiles == 0)  )
            if (true)
            {	//if one image is older than 1 day, all the images are recreated
                CreateChart(325, 215, "S_");
                CreateChart(390, 225, "");
                CreateChart(500, 330, "B_");
                //createHomePageCharts();
                /*
                    //Removing old files
                    foreach (FileInfo f in dir.GetFiles("*.jpg;*.txt"))
                    {// get the creation date of each image in the directory
                        string fullName = dir.FullName + "\\" + f.Name;
                        TimeSpan span = DateTime.Now.Subtract(f.CreationTime);
                        if(span.Days>0)
                        {
                            try
                            {
                            //	f.Delete();
                            }
                            catch(Exception ex)
                            {
                            }
                        }	
                    }
                    */
            }
            
        }

        protected dotnetCHARTING.Chart ChartObj = null;

        private void CreateChart(int width, int height, string nameFile)
        {
            ChartObj = new dotnetCHARTING.Chart();
            ChartObj.ChartArea.ClearColors();

            ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            ChartObj.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);
            
            ChartObj.XAxis.TimeInterval = TimeInterval.Week;



            string fileName2 = "";
            //System.Console.WriteLine("creation bitmap");
            string period1 = "";


            SqlConnection conn2 = new SqlConnection(this.connectionStringDB);
            string request1 = "SELECT Cont_ID,CONT_LABL FROM CONTRACT";
            conn2.Open();
            SqlCommand oCommand = new SqlCommand(request1, conn2);
            SqlDataReader myReader2 = oCommand.ExecuteReader();




            ChartObj.TempDirectory = chartsFolder;

            //}

            //ChartObj.ShowDateInTitle = true;


            // Set he chart size.
            ChartObj.Width = width;
            ChartObj.Height = height;
            //							ChartObj.BackColor = Color.FromArgb(255, 255,215,0);
            //							ChartObj.ForeColor = Color.FromArgb(255, 238,0,0);
            //            ChartObj.LegendBox.LabelStyle.Color = Color.Red;
            //ChartObj.SeriesCollection.

            //            ChartObj.YAxis.Label.Text = "Price ($/lbs)";

            if (myReader2.HasRows)
                while (myReader2.Read())
                {
                    try
                    {
                        for (int i = 0; i < 4; ++i)
                        {
                            if (i == 0)
                            {
                                period1 = "1M";
                                //   ChartObj.XAxis.TimePadding = TimeSpan.
                            }
                            else if (i == 1)
                            {
                                period1 = "1Y";
                                //                                ChartObj.XAxis.TimeInterval = TimeInterval.Months;
                            }
                            else if (i == 2)
                            {
                                period1 = "5Y";
                                //                            ChartObj.XAxis.TimeInterval = TimeInterval.Year;
                            }
                            else if (i == 3)
                            {
                                period1 = "10Y";
                                //                                ChartObj.XAxis.TimeInterval = TimeInterval.Year;

                            }
                            //							else if(i==4)
                            //							{
                            //								period1="6M";
                            //							}

                            //if (nameFile =="S_")
                            //{
                            //                            ChartObj.XAxis.Label.Text = myReader2["Cont_labl"].ToString();
                            //							ChartObj.Title = myReader2["Cont_labl"].ToString();
                            // Add the data.
                            ChartObj.SeriesCollection.Add(getData(Convert.ToInt32(myReader2.GetValue(0)), period1, nameFile));

                            // Set the directory where the images will be stored.

                            // Set the name of the file
                            ChartObj.FileName = nameFile + "Chart_" + myReader2.GetValue(0).ToString() + "_" + period1;
                            

                            ChartObj.FileManager.ImageFormat = ImageFormat.Png;
                            //                            ChartObj.OverlapFooter = true;
                            Bitmap bmp1 = ChartObj.GetChartBitmap();

                            fileName2 = ChartObj.FileManager.SaveImage(bmp1);
                        
                            ChartObj.SeriesCollection.Clear();

                        }
                    }
                    catch (Exception ex)
                    {
                        string filename = chartsFolder;
                        if (filename.Substring(filename.Length - 1, 1) != @"\") filename += @"\";
                        filename += "ErrorLog.txt";

                        LogFile(filename, ex.Message + " - " + ex.StackTrace);
                    }
                }
            conn2.Close();
        }

        private SeriesCollection getData(int iContract, string period, string nameFile)
        {
            SqlConnection conn;
            SqlDataReader dtrData;
            SqlCommand cmdData;
            string strSQL;
            conn = new SqlConnection(this.connectionStringDB);
            conn.Open();
            int iPreRead = 0; // the number of preloaded items
            int iInterval = 0; // the space between each items

            int range = 1;
            int count = 0;
            string tmpSQL = "";

            switch (period)
            {

                default:
                    strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate()) ";
                    iPreRead = 2;
                    iInterval = 0;
                    range = 4;
                    tmpSQL = "select COUNT(*) from HISTORICAL_PRICES WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate())";
                    cmdData = new SqlCommand(tmpSQL, conn);
                    cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                    count = Convert.ToInt32(cmdData.ExecuteScalar());

                    break;
                //case "1Y":
                //	strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-1,getdate()) ";
                //	iInterval = 0;
                //	iPreRead = 0;
                //	break;
                case "5Y":
                    strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate()) ";
                    iInterval = 0;
                    iPreRead = 2;
                    range = 4;
                    tmpSQL = "select COUNT(*) from HISTORICAL_PRICES WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate())";
                    cmdData = new SqlCommand(tmpSQL, conn);
                    cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                    count = Convert.ToInt32(cmdData.ExecuteScalar());

                    break;
                case "10Y":
                    strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-10,getdate()) ";
                    iInterval = 2;
                    iPreRead = 5;
                    range = 10;
                    tmpSQL = "select COUNT(*) from HISTORICAL_PRICES WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-10,getdate())";
                    cmdData = new SqlCommand(tmpSQL, conn);
                    cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                    count = Convert.ToInt32(cmdData.ExecuteScalar());

                    break;
                case "Forward":
                    strSQL = "Select FWD_ID, DATE= LEFT(FWD_MNTH,3)+' '+RIGHT(FWD_YEAR,2), PRICE = (SELECT MAX(OFFR_PRCE) FROM OFFER WHERE OFFR_CONT='" + iContract.ToString() + "' AND OFFR_MNTH=FWD_ID AND OFFR_SIZE='1') From FWDMONTH WHERE FWD_ACTV ='1' ORDER BY FWD_ID ASC";
                    //strSQL = "Select FWD_ID, DATE= LEFT(FWD_MNTH,3)+' '+RIGHT(FWD_YEAR,2), PRICE = (SELECT RIGHT(CONVERT(varchar,MAX(OFFR_PRCE)),4) FROM OFFER WHERE OFFR_CONT='" + iContract.ToString() + "' AND OFFR_MNTH=FWD_ID AND OFFR_SIZE='1') From FWDMONTH WHERE FWD_ACTV ='1' ORDER BY FWD_ID ASC";
                    break;
            }

            strSQL = "SELECT PRICE,DATE FROM (SELECT TOP 1 ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT ORDER BY DATE DESC)x UNION select PRICE,PRICE_DATE AS DATE from HISTORICAL_PRICES " + strSQL + " ORDER BY DATE";
            if (period.Equals("1M"))
            {
                // needs to overwrite string completely if it is one month
                strSQL = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(dd,-30,getdate())  ORDER BY DATE";
                iPreRead = 0;
                iInterval = 0;
                range = 4;
                tmpSQL = "select count(*) from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(dd,-30,getdate())";
                cmdData = new SqlCommand(tmpSQL, conn);
                cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                count = Convert.ToInt32(cmdData.ExecuteScalar());
            }
            //			if (period.Equals("6M"))
            //			{
            //				// needs to overwrite string completely if it is one month
            //				strSQL = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(mm,-6,getdate())  ORDER BY DATE";
            //				iPreRead = 12;
            //				iInterval = 6;
            //			}
            if (period.Equals("1Y"))
            {
                // needs to overwrite string completely if it is one month
                strSQL = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(mm,-12,getdate()) AND DATEPART(Dw,DATE)=DATEPART(Dw, (select max(date) from export_price WHERE CONT_ID=@CONTRACT)) ORDER BY DATE";
                iPreRead = 0; //24;
                iInterval = 0; //6;
                range = 4;

                tmpSQL = "select COUNT(*) from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(mm,-12,getdate()) AND DATEPART(Dw,DATE)=DATEPART(Dw, (select max(date) from export_price WHERE CONT_ID=@CONTRACT))";
                cmdData = new SqlCommand(tmpSQL, conn);
                cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                count = Convert.ToInt32(cmdData.ExecuteScalar());

            }


            cmdData = new SqlCommand(strSQL, conn);
            cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
            dtrData = cmdData.ExecuteReader();
            SeriesCollection SC = new SeriesCollection();

            for (int k = 0; k < iPreRead; k++)
            {
                if (dtrData.Read())
                { }
            }
            int i = 1;
            int add = 0;

            if (count != 0)
            {
                add = count % range;
            }


            Series s = new Series();

            while (dtrData.Read())
            {
                if (dtrData["DATE"] != DBNull.Value && dtrData["PRICE"] != DBNull.Value)
                {
                    Element e = new Element();
                    //					e.ToolTip = dtrData["PRICE"].ToString();
                    //					e.SmartLabel.DynamicDisplay = false;
                    //					e.SmartLabel.DynamicPosition = true;
                    //					e.ShowValue = false;
                    //e.Name = dtrData["DATE"].ToString();

                    if (((i - add) % range) == 0)
                    {
                        e.Name = Convert.ToDateTime(dtrData["DATE"]).ToString("MMM dd\r\nyyy");
                        //						e.SmartLabel.Text = dtrData["PRICE"].ToString();
                        e.SmartLabel.Color = Color.White;
                        e.XDateTime = (DateTime)dtrData["DATE"];
                    }
                    else
                    {
                        //e.Name = "";
                        e.SmartLabel.Text = "";
                    }

                    i++;


                    //e.Color = Color.Blue;//Color.FromArgb(255, 203, 1);
                    //					e.AxisMarker.Label.Color = Color.Red;

                    e.YValue = Convert.ToDouble(dtrData["PRICE"]);
                    //                    e.Hotspot.ToolTip = Convert.ToDouble(dtrData["PRICE"]).ToString();
                    /* wanted to have 'hand-cursor' while over values
                    e.Hotspot.Attributes.Custom.Add("onmouseover", "this.style.cursor='hand'");
                    e.Hotspot.Attributes.Custom.Add("onmouseout", "this.style.cursor='pointer'");
                    */
                    //                    s.Element.ShowValue = true;
                    s.Elements.Add(e);
                }

                if (period.Equals("6M") || period.Equals("1Y"))// || period.Equals("10Y")
                {
                    for (int k = 0; k < iInterval; k++)
                    {
                        if (dtrData.Read())
                        { }
                    }
                }
            }
            SC.Add(s);

            //ChartObj.Depth = 15;
            // Set 3D
            ChartObj.Use3D = false;
 

            ChartObj.ChartArea.DefaultElement.Color = Color.Orange;
            ChartObj.ChartArea.DefaultSeries.Line.Color = Color.Orange;
            ChartObj.ChartArea.DefaultSeries.Line.Width = 2;


            ChartObj.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
            ChartObj.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartObj.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
            ChartObj.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);


            ChartObj.ChartArea.XAxis.DefaultTick.Line.Color = Color.FromArgb(254, 0, 0); //red
            ChartObj.ChartArea.YAxis.DefaultTick.Line.Color = Color.FromArgb(254, 0, 0); //red

            ChartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;

            ChartObj.ChartArea.XAxis.Line.Width = 4;
            ChartObj.ChartArea.YAxis.Line.Width = 2;

            ChartObj.YAxis.Interval = 0.01;

            ChartObj.ChartArea.XAxis.LabelRotate = true;

            ChartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartObj.ChartArea.XAxis.TickLabelAngle = 90;

            ChartObj.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartObj.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);

            // Set a default transparency
            ChartObj.DefaultSeries.DefaultElement.Transparency = 20;

            // Set color of axis lines
            Axis AxisObj = new Axis();
            AxisObj.Line.Color = Color.FromArgb(255, 255, 0, 0);

            //            ChartObj.ChartArea.XAxis.DefaultTick.Line.Color = Color.FromArgb(254, 165, 3);
            //            ChartObj.ChartArea.YAxis.DefaultTick.Label.Color = Color.FromArgb(254, 165, 3);

            //							AxisObj.Line.Color = Color.FromArgb(2, System.Drawing.Color.Red);
            ChartObj.DefaultAxis = AxisObj;

            ChartObj.MarginLeft = 0;
            ChartObj.MarginRight = 0;
            ChartObj.MarginTop = 0;
            ChartObj.MarginBottom = 0;

            // Set the Default Series Type
            ChartObj.DefaultSeries.Type = SeriesType.Line;
            ChartObj.LegendBox.Position = LegendBoxPosition.None;

            // Set the y Axis Scale
            ChartObj.ChartArea.YAxis.Scale = Scale.Range;

            ChartObj.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            ChartObj.DefaultSeries.DefaultElement.Marker.Size = 6;
            ChartObj.DefaultSeries.DefaultElement.Marker.Color = Color.White;//Color.FromArgb( 255, 255, 153, 9);
            ChartObj.XAxis.Label.Color = Color.White;

            //ChartObj.XAxis.DefaultTick.Label.Text = "<%Value,mmm>";
            //            ChartObj.XAxis.TimeScaleLabels.DayFormatString = "p";
            //			ChartObj.YAxis.Minimum = 0.35;
            //			ChartObj.YAxis.Maximum = 0.85;
            //            ChartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM\r\ndd";
            //            ChartObj.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            //            ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);
            ChartObj.XAxis.SmartMinorTicks = true;
            ChartObj.XAxis.TimeInterval = TimeInterval.Month;
 
            //box around the chartarea
            ChartObj.ChartArea.Line.Color = Color.FromArgb(255, 255, 0, 0);//(254, 0, 0)
            ChartObj.ChartArea.Line.Width = 2;

            //ChartObj.Background = new Background("");
            if (nameFile == "S_") ChartObj.Background = new Background(chartsFolder + "\\Slogo.jpg");
            if (nameFile == "") ChartObj.Background = new Background(chartsFolder + "\\Mlogo.jpg");
            if (nameFile == "B_") ChartObj.Background = new Background(chartsFolder + "\\Blogo.jpg"); 

            ChartObj.ChartArea.Background.Color = Color.FromArgb(0, 0, 0, 0);
            ChartObj.ChartArea.Background.Mode = BackgroundMode.Color;
            ChartObj.Background.Color = Color.FromArgb(0, 0, 0, 0);
           

            return (SC);
        }

        private void LogFile(string filename, string message)
        {
            StreamWriter w = File.AppendText(filename);
            w.WriteLine("Date/Time: " + DateTime.Now.ToString());
            w.WriteLine(message);
            w.WriteLine("-----------------------");
            w.Flush();
            w.Close();
        }

        #endregion



        //public void createHomePageCharts()
        //{
        //    using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
        //    {
        //        conn.Open();

        //        string sSQL="SELECT GRADE= (SELECT GRADE_NAME from GRADE WHERE GRADE_ID = OFFR_GRADE), offr_grade,ContractID= (SELECT ContractID from HomePageSpotContract  WHERE SpotID=OFFR_GRADE) FROM bboffer where offr_grade is not null and offr_port is null  and OFFR_SIZE <> '38500' and OFFR_SIZE <> '44500'  group by offr_grade  ";
        //        using (SqlDataReader dtrResin = DBLibrary.GetDataReaderFromSelect(conn,sSQL))
        //        {
        //            while (dtrResin.Read())
        //            {
        //                createHistoricalGradeChart(int.Parse(dtrResin[1].ToString()), int.Parse(dtrResin[2].ToString()), dtrResin[0].ToString());

        //                createHistoricalContractChart(int.Parse(dtrResin[2].ToString()), dtrResin[0].ToString());

        //            }

        //        }
        //    }
        //}

        ////***** CEF 02/07/2008: Home Page Spot Market Chart with Weighted Avg
        ////CreateChart(390, 225, "");

        ////Create Historical Chart Main
        //private void createHistoricalGradeChart(int SpotID, int ContractID, string Grade)
        //{            
        //    dotnetCHARTING.Chart ChartObj = new dotnetCHARTING.Chart();

        //    //Size just for home page charts to fill right side of table
        //    ChartObj.Width = 390;
        //    ChartObj.Height = 260;

        //    ChartObj.ChartArea.ClearColors();
        //    ChartObj.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
        //    ChartObj.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);

        //    ChartObj.OverlapFooter = true;
        //    ChartObj.Mentor = false;

        //    System.Web.UI.Page page = this.Page;

        //    ChartObj.TempDirectory = "/Research/Charts/home"; 
        //    ChartObj.FileName = "Spot_" + SpotID;

        //    ChartObj.FileManager.ImageFormat = ImageFormat.Png;
        //    // Set the chart type                        
        //    ChartObj.Type = ChartType.Combo;

        //    ChartObj.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
        //    ChartObj.DefaultSeries.DefaultElement.Marker.Size = 4;
        //    //ChartObj.DefaultElement.Marker.Visible = true;
        //    ChartObj.DefaultElement.Marker.Visible = false;
        //    ChartObj.DefaultSeries.DefaultElement.Marker.Color = Color.White;//Color.FromArgb( 255, 255, 153, 9);
        //    ChartObj.XAxis.Label.Color = Color.White;

        //    // Debug mode. ( Will show generated errors if any )
        //    //ChartObj.Debug = true;
        //    ChartObj.Title = Grade;

        //    ChartObj.TitleBox.Position = TitleBoxPosition.FullWithLegend;
        //    ChartObj.TitleBox.Background.Color = Color.Orange;
        //    ChartObj.TitleBox.Label.Font = new Font("Arial", 7, FontStyle.Bold);


        //    ChartObj.LegendBox.Template = "%Icon%Name";
        //    ChartObj.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
        //    ChartObj.LegendBox.Label.Font = new Font("Arial", 6, FontStyle.Regular);

        //    ChartObj.ChartAreaSpacing = 1;

        //    Color[] MyColorObjectArray = new Color[3] { Color.Red, Color.FromArgb(255, 202, 0), Color.Yellow };
        //    ChartObj.Palette = MyColorObjectArray;

        //    //            // Modify the x axis labels.
        //    ChartObj.XAxis.TimeInterval = TimeInterval.Week;
        //    ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
        //    ChartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
        //    ChartObj.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
        //    //ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);
        //    ChartObj.XAxis.TimeScaleLabels.DayFormatString = "p";
        //    ChartObj.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
        //    ChartObj.XAxis.StaticColumnWidth = 2;
        //    ChartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
        //    ChartObj.ChartArea.XAxis.TickLabelAngle = 90;

        //    ChartObj.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
        //    ChartObj.ChartArea.XAxis.LabelRotate = true;
        //    ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
        //    //ChartObj.XAxis.TimeInterval = TimeInterval.Month;

        //    //XAxis
        //    ChartObj.XAxis.Label.Color = Color.White;
        //    ChartObj.ChartArea.XAxis.Label.Color = Color.White;
        //    ChartObj.XAxis.Line.Color = Color.Orange;
        //    ChartObj.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;

        //    ChartObj.YAxis.Interval = 0.01;
        //    // Setup the axes.
        //    ChartObj.YAxis.Label.Text = "Price";
        //    ChartObj.YAxis.FormatString = "Currency";
        //    ChartObj.YAxis.Scale = Scale.Range;

        //    ChartObj.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
        //    ChartObj.ChartArea.YAxis.Label.Color = Color.White;
        //    ChartObj.YAxis.Line.Color = Color.Orange;
        //    ChartObj.YAxis.Label.Color = Color.White;

        //    //Chart

        //    ChartObj.Background.Color = Color.Black;
        //    ChartObj.ChartArea.Background.Color = Color.Black;
        //    ChartObj.ChartArea.DefaultSeries.Line.Color = Color.Red;
        //    //CurrChart.ChartArea.DefaultSeries.Line.StartCap = System.Drawing.Drawing2D.LineCap.Triangle;
        //    //CurrChart.ChartArea.DefaultSeries.Line.EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
        //    ChartObj.ChartArea.DefaultSeries.Line.Width = 2;

        //    ChartObj.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
        //    ChartObj.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);

        //    //CurrChart.ChartArea.Line.Color = Color.FromArgb(255, 255, 0, 0);//(254, 0, 0)
        //    ChartObj.ChartArea.Line.Color = Color.Orange;
        //    //CurrChart.ChartArea.Line.Width = 2;

        //    ChartObj.MarginLeft = 0;
        //    ChartObj.MarginRight = 0;
        //    ChartObj.MarginTop = 0;
        //    ChartObj.MarginBottom = 0;

        //    //Load data as chart series

        //    SeriesCollection mySC = new SeriesCollection();
        //    ChartObj.SeriesCollection.Add(getPriceDataHistoricalChart(SpotID, ContractID, Grade));


        //    ChartObj.FileManager.ImageFormat = ImageFormat.Png;
        //    Bitmap bmp1 = ChartObj.GetChartBitmap();
        //    string fileName2 = "";
        //    fileName2 = ChartObj.FileManager.SaveImage(bmp1);
        //}

        ////Data - Range
        //private SeriesCollection getPriceDataHistoricalChart(int SpotID, int ContractID, string Grade)
        //{
        //    string sSqlContract = "select convert(char,date,110) as finaldate, bid, ask from export_price where cont_id={0} and date > DATEADD(mm,-12,getdate()) and datepart(dw,date)=6 order by date ";
        //    string sSpotMarket = "select ask, weightdate from SpotWeightAvg where contid={0} and weightdate > DATEADD(mm,-12,getdate()) and datepart(dw,weightdate)=6 order by weightdate";

        //    SeriesCollection SC = new SeriesCollection();

        //    Series sPriceRange = new Series();
        //    Series sBidPrice = new Series();

        //    sPriceRange.Name = "TPE Contract Range";
        //    sBidPrice.Name = "Weighted Spot Ask";

        //    //***** Load Contract values, high, low. 
        //    using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
        //    {
        //        conn.Open();

        //        SqlDataReader dtr = null;
        //        SqlCommand oCommand = new SqlCommand(string.Format(sSqlContract, ContractID), conn);
        //        dtr = oCommand.ExecuteReader();

        //        while (dtr.Read())
        //        {
        //            DateTime date = Convert.ToDateTime(dtr["finaldate"].ToString());
        //            Element ePriceRange = new Element();
        //            Element eBidPrice = new Element();

        //            //Price Range Element
        //            ePriceRange.XDateTime = date;

        //            ePriceRange.Close = Convert.ToDouble(dtr["ask"].ToString());
        //            ePriceRange.Open = Convert.ToDouble(dtr["bid"].ToString());
        //            ePriceRange.Name = Convert.ToDateTime(dtr["finaldate"]).ToString("MMM dd\r\nyyy");
        //            sPriceRange.Elements.Add(ePriceRange);
        //        }
        //    }

        //    SC.Add(sPriceRange);

        //    //***** Load Spot AVG        
        //    using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
        //    {
        //        SqlDataReader dtr = null;
        //        conn.Open();
        //        SqlCommand oCommand = new SqlCommand(string.Format(sSpotMarket, SpotID), conn);
        //        dtr = oCommand.ExecuteReader();

        //        while (dtr.Read())
        //        {
        //            DateTime date = Convert.ToDateTime(dtr["weightdate"].ToString());
        //            Element ePriceRange = new Element();
        //            Element eBidPrice = new Element();

        //            //Bid Element
        //            eBidPrice.ToolTip = dtr["ask"].ToString();
        //            eBidPrice.SmartLabel.DynamicDisplay = false;
        //            eBidPrice.SmartLabel.DynamicPosition = true;
        //            eBidPrice.ShowValue = false;
        //            eBidPrice.SmartLabel.Color = Color.White;
        //            eBidPrice.XDateTime = date;
        //            eBidPrice.SmartLabel.Text = "";
        //            eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
        //            eBidPrice.Name = Convert.ToDateTime(dtr["weightdate"]).ToString("MMM dd\r\nyyy");
        //            sBidPrice.AddElements(eBidPrice);
        //        }

        //    }

        //    SC.Add(sBidPrice);

        //    SC[0].Type = SeriesTypeFinancial.CandleStick;            
        //    SC[1].Type = SeriesType.Line;

        //    return (SC);
        //}


        //private void createHistoricalContractChart(int ContractID, string Grade)
        //{
        //    dotnetCHARTING.Chart ChartObj = new dotnetCHARTING.Chart();

        //    //Size just for home page charts to fill right side of table
        //    ChartObj.Width = 390;
        //    ChartObj.Height = 260;

        //    ChartObj.ChartArea.ClearColors();
        //    ChartObj.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
        //    ChartObj.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);

        //    ChartObj.OverlapFooter = true;
        //    ChartObj.Mentor = false;

        //    System.Web.UI.Page page = this.Page;

        //    ChartObj.TempDirectory = "/Research/Charts/home";
        //    ChartObj.FileName = "Contract_" + ContractID;

        //    ChartObj.FileManager.ImageFormat = ImageFormat.Png;
        //    // Set the chart type                        
        //    ChartObj.Type = ChartType.Combo;

        //    ChartObj.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
        //    ChartObj.DefaultSeries.DefaultElement.Marker.Size = 4;
        //    //ChartObj.DefaultElement.Marker.Visible = true;
        //    ChartObj.DefaultElement.Marker.Visible = false;
        //    //ChartObj.DefaultSeries.DefaultElement.Marker.Color = Color.White;//Color.FromArgb( 255, 255, 153, 9);
        //    ChartObj.DefaultSeries.DefaultElement.Marker.Color = Color.White;
        //    ChartObj.XAxis.Label.Color = Color.White;

        //    // Debug mode. ( Will show generated errors if any )
        //    //ChartObj.Debug = true;
        //    ChartObj.Title = Grade;

        //    ChartObj.TitleBox.Position = TitleBoxPosition.FullWithLegend;
        //    ChartObj.TitleBox.Background.Color = Color.Red;
        //    ChartObj.TitleBox.Label.Font = new Font("Arial", 8, FontStyle.Bold);
        //    ChartObj.TitleBox.Label.Color = Color.White;

        //    ChartObj.LegendBox.Template = "%Icon%Name";
        //    ChartObj.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
        //    ChartObj.LegendBox.Label.Font = new Font("Arial", 7, FontStyle.Bold);
        //    ChartObj.LegendBox.Label.Color = Color.White;
        //    //ChartObj.LegendBox.Label.

        //    ChartObj.ChartAreaSpacing = 1;

        //    Color[] MyColorObjectArray = new Color[3] { Color.Orange, Color.Red, Color.Red };
        //    //Color[] MyColorObjectArray = new Color[3] { Color.Orange, Color.FromArgb(255, 202, 0), Color.Yellow };
        //    ChartObj.Palette = MyColorObjectArray;

        //    //            // Modify the x axis labels.
        //    ChartObj.XAxis.TimeInterval = TimeInterval.Week;
        //    ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
        //    ChartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
        //    ChartObj.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
        //    //ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);
        //    ChartObj.XAxis.TimeScaleLabels.DayFormatString = "p";
        //    ChartObj.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
        //    ChartObj.XAxis.StaticColumnWidth = 2;
        //    ChartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
        //    ChartObj.ChartArea.XAxis.TickLabelAngle = 90;

        //    ChartObj.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
        //    ChartObj.ChartArea.XAxis.LabelRotate = true;
        //    ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
        //    //ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Red;
        //    //ChartObj.XAxis.TimeInterval = TimeInterval.Month;

        //    //XAxis
        //    ChartObj.XAxis.Label.Color = Color.White;
        //    ChartObj.ChartArea.XAxis.Label.Color = Color.White;
        //    ChartObj.XAxis.Line.Color = Color.Red;
        //    //ChartObj.XAxis.Line.Color = Color.Orange;
        //    ChartObj.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;

        //    ChartObj.YAxis.Interval = 0.01;
        //    // Setup the axes.
        //    ChartObj.YAxis.Label.Text = "Price";
        //    ChartObj.YAxis.FormatString = "Currency";
        //    ChartObj.YAxis.Scale = Scale.Range;

        //    ChartObj.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
        //    ChartObj.ChartArea.YAxis.Label.Color = Color.White;
        //    //ChartObj.YAxis.Line.Color = Color.Orange;
        //    ChartObj.YAxis.Line.Color = Color.Red;
        //    ChartObj.YAxis.Label.Color = Color.White;

        //    //Chart

        //    ChartObj.Background.Color = Color.Black;
        //    ChartObj.ChartArea.Background.Color = Color.Black;
        //    //ChartObj.ChartArea.DefaultSeries.Line.Color = Color.Orange;
        //    ChartObj.ChartArea.DefaultSeries.Line.Color = Color.Red; 
        //    //CurrChart.ChartArea.DefaultSeries.Line.StartCap = System.Drawing.Drawing2D.LineCap.Triangle;
        //    //CurrChart.ChartArea.DefaultSeries.Line.EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
        //    ChartObj.ChartArea.DefaultSeries.Line.Width = 2;

        //    ChartObj.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
        //    ChartObj.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);

        //    //CurrChart.ChartArea.Line.Color = Color.FromArgb(255, 255, 0, 0);//(254, 0, 0)
        //    //ChartObj.ChartArea.Line.Color = Color.Orange;
        //    ChartObj.ChartArea.Line.Color = Color.Red;
        //    //CurrChart.ChartArea.Line.Width = 2;

        //    ChartObj.MarginLeft = 0;
        //    ChartObj.MarginRight = 0;
        //    ChartObj.MarginTop = 0;
        //    ChartObj.MarginBottom = 0;

        //    //Load data as chart series

        //    SeriesCollection mySC = new SeriesCollection();
        //    ChartObj.SeriesCollection.Add(getDataContractChart(ContractID));


        //    ChartObj.FileManager.ImageFormat = ImageFormat.Png;
        //    Bitmap bmp1 = ChartObj.GetChartBitmap();
        //    string fileName2 = "";
        //    fileName2 = ChartObj.FileManager.SaveImage(bmp1);
        //}

        //private SeriesCollection getDataContractChart(int ContractID)
        //{
        //    string sSqlContract = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID={0} AND DATE > DATEADD(mm,-12,getdate()) AND DATEPART(Dw,DATE)=DATEPART(Dw, (select max(date) from export_price WHERE CONT_ID={1})) ORDER BY DATE";

        //    SeriesCollection SC = new SeriesCollection();

        //    Series sBidPrice = new Series();

        //    sBidPrice.Name = "TPE Contract Ask";

        //    //***** Load Spot AVG        
        //    using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
        //    {
        //        SqlDataReader dtr = null;
        //        conn.Open();
        //        SqlCommand oCommand = new SqlCommand(string.Format(sSqlContract, ContractID,ContractID), conn);
        //        dtr = oCommand.ExecuteReader();

        //        while (dtr.Read())
        //        {
        //            DateTime date = Convert.ToDateTime(dtr["date"].ToString());
        //            Element ePriceRange = new Element();
        //            Element eBidPrice = new Element();

        //            //Bid Element
        //            eBidPrice.ToolTip = dtr["price"].ToString();
        //            eBidPrice.SmartLabel.DynamicDisplay = false;
        //            eBidPrice.SmartLabel.DynamicPosition = true;
        //            eBidPrice.ShowValue = false;
        //            eBidPrice.SmartLabel.Color = Color.White;
        //            eBidPrice.XDateTime = date;
        //            eBidPrice.SmartLabel.Text = "";
        //            eBidPrice.YValue = Convert.ToDouble(dtr["price"]);
        //            eBidPrice.Name = Convert.ToDateTime(dtr["date"]).ToString("MMM dd\r\nyyy");
        //            sBidPrice.AddElements(eBidPrice);
        //        }

        //    }

        //    SC.Add(sBidPrice);

        //    SC[0].Type = SeriesType.Line;
        //    return (SC);
        //}




        public void createHomePageCharts()
        {
            using (SqlConnection conn = new SqlConnection(this.connectionStringDB))
            {
                conn.Open();

                string sSQL = "SELECT GRADE= (SELECT GRADE_NAME from GRADE WHERE GRADE_ID = OFFR_GRADE), offr_grade,ContractID= (SELECT ContractID from HomePageSpotContract  WHERE SpotID=OFFR_GRADE) FROM bboffer where offr_grade is not null and offr_grade <> 13 and offr_port is null  and OFFR_SIZE <> '38500' and OFFR_SIZE <> '44500'  group by offr_grade  ";

                using (SqlDataReader dtrResin = DBLibrary.GetDataReaderFromSelect(conn, sSQL))
                {
                    while (dtrResin.Read())
                    {
                        createHistoricalGradeChart(int.Parse(dtrResin[1].ToString()), int.Parse(dtrResin[2].ToString()), dtrResin[0].ToString());

                        createHistoricalContractChart(int.Parse(dtrResin[2].ToString()), dtrResin[0].ToString());
                    }

                }
            }
        }

        //***** CEF 02/07/2008: Home Page Spot Market Chart with Weighted Avg
        //CreateChart(390, 225, "");

        //Create Historical Chart Main
        private void createHistoricalGradeChart(int SpotID, int ContractID, string Grade)
        {
            dotnetCHARTING.Chart ChartObj = new dotnetCHARTING.Chart();

            //Size just for home page charts to fill right side of table
            ChartObj.Width = 390;
            ChartObj.Height = 260;

            ChartObj.ChartArea.ClearColors();
            ChartObj.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartObj.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);

            ChartObj.OverlapFooter = true;
            ChartObj.Mentor = false;

            System.Web.UI.Page page = this.Page;

            ChartObj.TempDirectory = chartsFolder + "\\home\\";
            ChartObj.FileName = "Spot_" + SpotID;

            ChartObj.FileManager.ImageFormat = ImageFormat.Png;
            // Set the chart type                        
            ChartObj.Type = ChartType.Combo;

            ChartObj.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            ChartObj.DefaultSeries.DefaultElement.Marker.Size = 4;
            ChartObj.DefaultElement.Marker.Visible = true;
            //ChartObj.DefaultElement.Marker.Visible = false;
            ChartObj.DefaultSeries.DefaultElement.Marker.Color = Color.White;//Color.FromArgb( 255, 255, 153, 9);
            ChartObj.XAxis.Label.Color = Color.White;

            // Debug mode. ( Will show generated errors if any )
            //ChartObj.Debug = true;
            ChartObj.Title = Grade;

            ChartObj.TitleBox.Position = TitleBoxPosition.FullWithLegend;
            ChartObj.TitleBox.Background.Color = Color.Orange;
            ChartObj.TitleBox.Label.Font = new Font("Arial", 7, FontStyle.Bold);


            ChartObj.LegendBox.Template = "%Icon%Name";
            ChartObj.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
            ChartObj.LegendBox.Label.Font = new Font("Arial", 6, FontStyle.Regular);

            ChartObj.ChartAreaSpacing = 1;

            Color[] MyColorObjectArray = new Color[3] { Color.Red, Color.FromArgb(255, 202, 0), Color.Yellow };
            ChartObj.Palette = MyColorObjectArray;

            //            // Modify the x axis labels.
            ChartObj.XAxis.TimeInterval = TimeInterval.Week;
            ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "dd";
            ChartObj.XAxis.TimeScaleLabels.MonthFormatString = "dd";
            ChartObj.XAxis.TimeScaleLabels.YearFormatString = "yy";
            //ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);
            ChartObj.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartObj.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartObj.XAxis.StaticColumnWidth = 2;
            ChartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartObj.ChartArea.XAxis.TickLabelAngle = 90;

            ChartObj.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartObj.ChartArea.XAxis.LabelRotate = true;
            ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            //ChartObj.XAxis.TimeInterval = TimeInterval.Month;

            //XAxis
            ChartObj.XAxis.Label.Color = Color.White;
            ChartObj.ChartArea.XAxis.Label.Color = Color.White;
            ChartObj.XAxis.Line.Color = Color.Orange;
            ChartObj.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;

            ChartObj.YAxis.Interval = 0.01;
            // Setup the axes.
            ChartObj.YAxis.Label.Text = "Price";
            ChartObj.YAxis.FormatString = "Currency";
            ChartObj.YAxis.Scale = Scale.Range;

            ChartObj.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartObj.ChartArea.YAxis.Label.Color = Color.White;
            ChartObj.YAxis.Line.Color = Color.Orange;
            ChartObj.YAxis.Label.Color = Color.White;

            //Chart

            ChartObj.Background.Color = Color.Black;
            ChartObj.ChartArea.Background.Color = Color.Black;
            ChartObj.ChartArea.DefaultSeries.Line.Color = Color.Red;
            //CurrChart.ChartArea.DefaultSeries.Line.StartCap = System.Drawing.Drawing2D.LineCap.Triangle;
            //CurrChart.ChartArea.DefaultSeries.Line.EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
            ChartObj.ChartArea.DefaultSeries.Line.Width = 2;

            ChartObj.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
            ChartObj.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);

            //CurrChart.ChartArea.Line.Color = Color.FromArgb(255, 255, 0, 0);//(254, 0, 0)
            ChartObj.ChartArea.Line.Color = Color.Orange;
            //CurrChart.ChartArea.Line.Width = 2;

            ChartObj.MarginLeft = 0;
            ChartObj.MarginRight = 0;
            ChartObj.MarginTop = 0;
            ChartObj.MarginBottom = 0;

            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();
            ChartObj.SeriesCollection.Add(getPriceDataHistoricalChart(SpotID, ContractID, Grade));


            ChartObj.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap bmp1 = ChartObj.GetChartBitmap();
            string fileName2 = "";
            fileName2 = ChartObj.FileManager.SaveImage(bmp1);
        }

        private void createHistoricalContractChart(int ContractID, string Grade)
        {
            dotnetCHARTING.Chart ChartObj = new dotnetCHARTING.Chart();

            //Size just for home page charts to fill right side of table
            ChartObj.Width = 390;
            ChartObj.Height = 260;

            ChartObj.ChartArea.ClearColors();
            ChartObj.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartObj.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);

            ChartObj.OverlapFooter = true;
            ChartObj.Mentor = false;

            System.Web.UI.Page page = this.Page;

            ChartObj.TempDirectory = chartsFolder + "\\home\\";
            ChartObj.FileName = "Contract_" + ContractID;

            ChartObj.FileManager.ImageFormat = ImageFormat.Png;
            // Set the chart type                        
            ChartObj.Type = ChartType.Combo;

            ChartObj.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            ChartObj.DefaultSeries.DefaultElement.Marker.Size = 4;
            ChartObj.DefaultElement.Marker.Visible = true;
            //ChartObj.DefaultElement.Marker.Visible = false;
            //ChartObj.DefaultSeries.DefaultElement.Marker.Color = Color.White;//Color.FromArgb( 255, 255, 153, 9);
            ChartObj.DefaultSeries.DefaultElement.Marker.Color = Color.White;
            ChartObj.XAxis.Label.Color = Color.White;

            // Debug mode. ( Will show generated errors if any )
            //ChartObj.Debug = true;
            ChartObj.Title = Grade;

            ChartObj.TitleBox.Position = TitleBoxPosition.FullWithLegend;
            ChartObj.TitleBox.Background.Color = Color.Red;
            ChartObj.TitleBox.Label.Font = new Font("Arial", 8, FontStyle.Bold);
            ChartObj.TitleBox.Label.Color = Color.White;

            ChartObj.LegendBox.Template = "%Icon%Name";
            ChartObj.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
            ChartObj.LegendBox.Label.Font = new Font("Arial", 7, FontStyle.Bold);
            ChartObj.LegendBox.Label.Color = Color.White;
            //ChartObj.LegendBox.Label.

            ChartObj.ChartAreaSpacing = 1;

            Color[] MyColorObjectArray = new Color[3] { Color.Orange, Color.Red, Color.Red };
            //Color[] MyColorObjectArray = new Color[3] { Color.Orange, Color.FromArgb(255, 202, 0), Color.Yellow };
            ChartObj.Palette = MyColorObjectArray;

            //            // Modify the x axis labels.
            ChartObj.XAxis.TimeInterval = TimeInterval.Week;
            ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM ndd";
            ChartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM ndd";
            ChartObj.XAxis.TimeScaleLabels.YearFormatString = "yy";
            //ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);
            ChartObj.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartObj.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartObj.XAxis.StaticColumnWidth = 2;
            ChartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartObj.ChartArea.XAxis.TickLabelAngle = 90;

            ChartObj.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartObj.ChartArea.XAxis.LabelRotate = true;
            ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            //ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Red;
            //ChartObj.XAxis.TimeInterval = TimeInterval.Month;

            //XAxis
            ChartObj.XAxis.Label.Color = Color.White;
            ChartObj.ChartArea.XAxis.Label.Color = Color.White;
            ChartObj.XAxis.Line.Color = Color.Red;
            //ChartObj.XAxis.Line.Color = Color.Orange;
            ChartObj.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;

            ChartObj.YAxis.Interval = 0.01;
            // Setup the axes.
            ChartObj.YAxis.Label.Text = "Price";
            ChartObj.YAxis.FormatString = "Currency";
            ChartObj.YAxis.Scale = Scale.Range;

            ChartObj.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartObj.ChartArea.YAxis.Label.Color = Color.White;
            //ChartObj.YAxis.Line.Color = Color.Orange;
            ChartObj.YAxis.Line.Color = Color.Red;
            ChartObj.YAxis.Label.Color = Color.White;

            //Chart

            ChartObj.Background.Color = Color.Black;
            ChartObj.ChartArea.Background.Color = Color.Black;
            //ChartObj.ChartArea.DefaultSeries.Line.Color = Color.Orange;
            ChartObj.ChartArea.DefaultSeries.Line.Color = Color.Red;
            //CurrChart.ChartArea.DefaultSeries.Line.StartCap = System.Drawing.Drawing2D.LineCap.Triangle;
            //CurrChart.ChartArea.DefaultSeries.Line.EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
            ChartObj.ChartArea.DefaultSeries.Line.Width = 2;

            ChartObj.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
            ChartObj.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);

            //CurrChart.ChartArea.Line.Color = Color.FromArgb(255, 255, 0, 0);//(254, 0, 0)
            //ChartObj.ChartArea.Line.Color = Color.Orange;
            ChartObj.ChartArea.Line.Color = Color.Red;
            //CurrChart.ChartArea.Line.Width = 2;

            ChartObj.MarginLeft = 0;
            ChartObj.MarginRight = 0;
            ChartObj.MarginTop = 0;
            ChartObj.MarginBottom = 0;

            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();
            ChartObj.SeriesCollection.Add(getDataContractChart(ContractID));


            ChartObj.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap bmp1 = ChartObj.GetChartBitmap();
            string fileName2 = "";
            fileName2 = ChartObj.FileManager.SaveImage(bmp1);
        }


        //Data - Range
        private SeriesCollection getPriceDataHistoricalChart(int SpotID, int ContractID, string Grade)
        {
            //string sSqlContract = "select convert(char,date,110) as finaldate, bid, ask from export_price where cont_id={0} and date > DATEADD(wk,-51,getdate()) and datepart(dw,date)=6 order by date ";
            //string sSpotMarket = "select ask, weightdate from SpotWeightAvg where contid={0} and weightdate > DATEADD(wk,-51,getdate()) and datepart(dw,weightdate)=6 order by weightdate";

            string sSqlContract = @"select convert(char,date,110) as finaldate, bid, ask, date from export_price 
                                    where cont_id={0} and date > DATEADD(wk,-51,getdate()) 
                                    and datepart(dw,date)=6
                                    UNION
                                    select convert(char,date,110) as finaldate, bid, ask, date from export_price 
                                    where cont_id={1} and date > DATEADD(dd,-1,getdate())
                                    order by date";


            string sSpotMarket = @"select ask, weightdate from SpotWeightAvg where contid={0} and 
                                weightdate > DATEADD(wk,-51,getdate()) and datepart(dw,weightdate)=6 
                                UNION
                                select ask, weightdate from SpotWeightAvg where contid={1} and 
                                weightdate > DATEADD(dd,-1,getdate())
                                order by weightdate";


            SeriesCollection SC = new SeriesCollection();

            Series sPriceRange = new Series();
            Series sBidPrice = new Series();

            sPriceRange.Name = "TPE Contract Range";
            sBidPrice.Name = "Weighted Spot Ask";

            //***** Load Contract values, high, low. 
            using (SqlConnection conn = new SqlConnection(this.connectionStringDB))
            {
                conn.Open();

                SqlDataReader dtr = null;
                SqlCommand oCommand = new SqlCommand(string.Format(sSqlContract, ContractID, ContractID), conn);
                dtr = oCommand.ExecuteReader();

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["finaldate"].ToString());
                    Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Price Range Element
                    ePriceRange.XDateTime = date;

                    ePriceRange.Close = Convert.ToDouble(dtr["ask"].ToString());
                    ePriceRange.Open = Convert.ToDouble(dtr["bid"].ToString());
                    ePriceRange.Name = Convert.ToDateTime(dtr["finaldate"]).ToString("MMM dd yy");
                    //eBidPrice.Name = Convert.ToDateTime(dtr["finaldate"]).ToString("MMM dd yy");
                    sPriceRange.Elements.Add(ePriceRange);
                }
            }

            SC.Add(sPriceRange);

            //***** Load Spot AVG        
            using (SqlConnection conn = new SqlConnection(this.connectionStringDB))
            {
                SqlDataReader dtr = null;
                conn.Open();
                SqlCommand oCommand = new SqlCommand(string.Format(sSpotMarket, SpotID, SpotID), conn);
                dtr = oCommand.ExecuteReader();

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["weightdate"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Bid Element
                    //eBidPrice.ToolTip = dtr["ask"].ToString();
                    //eBidPrice.SmartLabel.DynamicDisplay = false;
                    //eBidPrice.SmartLabel.DynamicPosition = true;
                    //eBidPrice.ShowValue = false;
                    //eBidPrice.SmartLabel.Color = Color.White;
                    //eBidPrice.XDateTime = Convert.ToDateTime(date.ToShortDateString());
                    //eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["weightdate"]).ToString("MMM dd yy");
                    sBidPrice.AddElements(eBidPrice);
                }

            }

            SC.Add(sBidPrice);

            SC[0].Type = SeriesTypeFinancial.CandleStick;
            SC[1].Type = SeriesType.Line;

            return (SC);
        }

        private SeriesCollection getDataContractChart(int ContractID)
        {
            //            string sSqlContract = @"select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID={0} AND 
            //                                    DATE > DATEADD(wk,-51,getdate()) AND DATEPART(Dw,DATE)=DATEPART(Dw, (select max(date)             
            //                                    from export_price WHERE CONT_ID={1})) ORDER BY DATE";


            string sSqlContract = @"select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID={0} AND 
                                    DATE > DATEADD(wk,-51,getdate()) AND datepart(dw,date)=6 
                                    UNION
                                    select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID={1} AND 
                                    DATE >  DATEADD(dd,-1,getdate()) 
                                    ORDER BY DATE";


            SeriesCollection SC = new SeriesCollection();

            Series sBidPrice = new Series();

            sBidPrice.Name = "TPE Contract Ask";

            //***** Load Spot AVG        
            using (SqlConnection conn = new SqlConnection(this.connectionStringDB))
            {
                SqlDataReader dtr = null;
                conn.Open();
                SqlCommand oCommand = new SqlCommand(string.Format(sSqlContract, ContractID, ContractID), conn);
                dtr = oCommand.ExecuteReader();

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Bid Element
                    eBidPrice.ToolTip = dtr["price"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["price"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["date"]).ToString("MMM dd yy");
                    sBidPrice.AddElements(eBidPrice);
                }

            }

            SC.Add(sBidPrice);

            SC[0].Type = SeriesType.Line;
            return (SC);
        }


    }
}