using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;


namespace tpeDataShare
{
    public class clsSpreads
    {
        public double getSpreadCalcValue(string bid, string ask)
        {
            clsCommon co = new clsCommon();

            double dResult = 0;

            if (co.IsNumeric(bid) && co.IsNumeric(ask))
            {
                dResult = (Convert.ToDouble(bid) + Convert.ToDouble(ask)) / 2;
            }
            else if (co.IsNumeric(bid) && co.IsNumeric(ask) == false)
            {
                dResult = Convert.ToDouble(bid);
            }
            else if (co.IsNumeric(ask) && co.IsNumeric(bid) == false)
            {
                dResult = Convert.ToDouble(ask);
            }

            return dResult;

        }

    }
}
