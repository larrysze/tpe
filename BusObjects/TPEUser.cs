using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using TPE.Utility;

namespace TPE.BusObjects
{
    public class TPEUser
    {
        string _FirstName;
        string _LastName;
        string _Title;
        string _LoginName;
        string _Email;
        string _Company;
        string _Phone;
        string _Fax;
        string _Zip;
        string _Password;
        string _Comments;
        string _Port;
        string _Pref;
        //string[] _Preferences = new string[21];
        string[] _Preferences = new string[25];
        bool _IsDomestic;
        TFreightType _FreightType;
        TFreightQuality _FreightQuality;

       
        public bool IsDomestic
        {
            get 
            {
                return _IsDomestic; 
            }
            set 
            { 
                _IsDomestic = value; 
            }
        }
        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
            }
        }
        public string Title
        {
            get
            {
                return _Title;
            }
            set
            {
                _Title = value;
            }
        }
        public string LoginName
        {
            get
            {
                return _LoginName;
            }
            set
            {
                _LoginName = value;
            }
        }
        public string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                _Email = value;
            }
        }
        public string Company
        {
            get
            {
                return _Company;
            }
            set
            {
                _Company = value;
            }
        }

        public string Phone
        {
            get
            {
                return _Phone;
            }
            set
            {
                _Phone = value;
            }
        }
        public string Fax
        {
            get
            {
                return _Fax;
            }
            set
            {
                _Fax = value;
            }
        }
        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                _Password = value;
            }
        }
        public string Comments
        {
            get
            {
                return _Comments;
            }
            set
            {
                _Comments = value;
            }
        }
        public string Zip
        {
            get
            {
                return _Zip;
            }
            set
            {
                _Zip = value;
            }
        }
        public string Port
        {
            get
            {
                return _Port;
            }
            set
            {
                _Port = value;
            }
        }
        public string FreightType
        {
            get
            {
                return _FreightType.ToString();
            }
        }
        public string FreightQuality
        {
            get
            {
                return _FreightQuality.ToString();
            }

        }
        public string Pref
        {
            get
            {
                return _Pref;
            }
            set
            {
                _Pref = value;
            }
        }
        public string[] Preferences
        {
            get
            {
                return _Preferences;
            }
            set
            {
                _Preferences = value;
            }
        }
        /// <summary>
        /// c'tor: populate object from database
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        public TPEUser(string ConnectionString, string UserName, string Password)
        {
            LoadFromDB(ConnectionString, UserName, Password);    
        }

  
        private void LoadFromDB(string ConnectionString, string UserName, string Password)
        {
            //...
            Hashtable htParams = new Hashtable();
			string Sql = "SELECT PERS_ID, PERS_FRST_NAME, PERS_LAST_NAME, PERS_TITL, PERS_PHON, PERS_MAIL, PERS_COMP, " +
                    "PERS_PRMLY_INTRST, PERS_INTRST_SIZE, PERS_INTRST_QUALT, PERS_PORT, PERS_ZIP, PORT_CITY, PERS_CMNT, " + 
                    "PERS_PREF, PERS_STAT, CNAME=ISNULL(COMP_NAME, " + 
                    "(SELECT C.COMP_NAME FROM COMPANY C WHERE COMP_ID=PERS_COMP)), ISNULL((SELECT TOP 1 CMNT_TEXT " +
                    "FROM COMMENT WHERE CMNT_PERS=PERS_ID AND CMNT_TEXT NOT LIKE 'User Login%' AND " +
                    "CMNT_EMAIL_SYSTEM_ID IS NULL ORDER BY CMNT_DATE DESC), '') AS NOTE " +
					"FROM PERSON, INTERNATIONAL_PORT " +
					"WHERE PERSON.PERS_PORT *= INTERNATIONAL_PORT.PORT_ID AND ((PERS_LOGN=@userName) OR " +
                    "(PERS_MAIL=@userName)) AND PERS_PSWD=@password";
			htParams.Add("@userName",UserName);
			htParams.Add("@password",Password);

            //using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
            using (SqlConnection conn = new SqlConnection(ConnectionString))
			{				
				conn.Open();			
				using (SqlDataReader dtrUser = DBLibrary.GetDataReaderFromSelect(conn, Sql,htParams))
				{
					if (dtrUser.Read())
					{
						IsDomestic = (dtrUser["PERS_PRMLY_INTRST"].ToString()=="1");
                        Zip = (dtrUser["PERS_ZIP"] != null) ? dtrUser["PERS_ZIP"].ToString() : "";
						if (!IsDomestic)
                        {						    
							Port = (dtrUser["PERS_PORT"] != null) ? dtrUser["PORT_CITY"].ToString() : "";
						}

						//ID = dtrUser["PERS_ID"].ToString();
                        FirstName = dtrUser["PERS_FRST_NAME"].ToString();
                        LastName = dtrUser["PERS_LAST_NAME"].ToString(); 
                        Title = dtrUser["PERS_TITL"].ToString();
                        Company = dtrUser["CNAME"].ToString();
                        Phone = dtrUser["PERS_PHON"].ToString();
                        Email = dtrUser["PERS_MAIL"].ToString();
                        Comments = dtrUser["NOTE"].ToString();
                        string tmpFreightType = dtrUser["PERS_INTRST_SIZE"].ToString() ;
                        if (tmpFreightType == "")
                        {
                            _FreightType = new TFreightType("000");
                        }
                        else
                        {
                            _FreightType = new TFreightType(tmpFreightType);
                        }
                        string tmpFreightQuality = dtrUser["PERS_INTRST_QUALT"].ToString();
                        if (tmpFreightQuality == "")
                        {
                            _FreightQuality = new TFreightQuality("000");
                        }
                        else
                        {
                            _FreightQuality = new TFreightQuality(tmpFreightQuality);
                        }
                        Pref = dtrUser["PERS_PREF"].ToString();
                 
					}
				}
                using (SqlDataReader dtrResin = DBLibrary.GetDataReaderFromSelect(conn, "SELECT CONT_LABL,CONT_ID FROM CONTRACT WHERE POWER(2,CONT_ID-1)&(" + Pref + ")<>0 ORDER BY CONT_ORDR"))
                {
                    int i = 0;
                    while (dtrResin.Read())
                    {
                       Preferences[i] += dtrResin["CONT_LABL"].ToString();
                       i++;
                    }
                }
           
         }

     }




    public class TFreightType
    {
        //string EncodedFreightType;
        bool IsRailCar = false;
        bool IsBulkTruck = false;
        bool IsTruckloadBox = false;

        public TFreightType(string EncodedFreightType)
        {
            {
                if (EncodedFreightType[0].Equals('1')) IsRailCar = true;
                if (EncodedFreightType[1].Equals('1')) IsBulkTruck = true;
                if (EncodedFreightType[2].Equals('1')) IsTruckloadBox = true;
            }
        }

        public override string ToString()
        {
            string result = string.Empty;
            if (IsRailCar) result += AddString("Rail Cars");            
            if (IsBulkTruck) result += AddString("Bulk Trucks");
            if (IsTruckloadBox) result += AddString("Truckload Boxes/Bags");
            if (result.Length > 0)
                result = result.Substring(0, result.Length - 2);
            return result;   
            
        }

        string AddString(string s)
        {
            if (s.Length > 0)
                return s + ", ";
            else
                return s;
        }
        
     }
    
   public class TFreightQuality
   {
        string EncodedFreightQuality;
        bool IsPrime = false;
        bool IsGoodOffgrade = false;
        bool IsRegrindRepro = false;

        public TFreightQuality(string EncodedFreightQuality)
        {
            if (EncodedFreightQuality[0].Equals('1')) IsPrime = true;
			if (EncodedFreightQuality[1].Equals('1')) IsGoodOffgrade = true;
            if (EncodedFreightQuality[2].Equals('1')) IsRegrindRepro = true;
        }

        public override string ToString()
        {
            string result = string.Empty;
            if (IsPrime) result += AddString("Prime");
		    if (IsGoodOffgrade) result += AddString("Good Offgrade");
            if (IsRegrindRepro) result = string.Empty;
            if (result.Length > 0)
                result = result.Substring(0, result.Length - 2);
            return result;   
        }

        string AddString(string s)
        {
            if (s.Length > 0)
                return s + ", ";
            else
                return s;
        }

   }
  }
}
