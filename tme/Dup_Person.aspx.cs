using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace localhost
{
	/// <summary>
	/// Summary description for Dup_Person.
	/// </summary>
	public class Dup_Person : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.TextBox txt1;
		protected System.Web.UI.WebControls.TextBox txt2;
		protected System.Web.UI.WebControls.Label lbl;
		protected System.Web.UI.WebControls.CheckBox cbAccess;
		protected System.Web.UI.WebControls.Label Label1;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Button1.Click += new System.EventHandler(this.Button1_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Button1_Click(object sender, System.EventArgs e)
		{
			// add spot offers
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			SqlConnection connCMD;
			connCMD = new SqlConnection(Application["DBconn"].ToString());
			connCMD.Open();
			conn.Open();

			string strNews ="";
			SqlCommand cmdNews;
			SqlCommand cmd;

			SqlDataReader dtrNews;
    
			cmdNews= new SqlCommand("SELECT PERS_FRST_NAME + ' ' + PERS_LAST_NAME AS NAME,PERS_NUMBER_LOGINS, PERS_ID,PERS_LOGN,PERS_PSWD,PERS_ACES FROM PERSON WHERE PERS_ID = '"+ txt1.Text +"'", conn);
			dtrNews = cmdNews.ExecuteReader();
		
			if  (dtrNews.Read())
			{
				
				cmd= new SqlCommand("UPDATE PERSON SET PERS_PSWD='"+dtrNews["PERS_PSWD"].ToString()+"',PERS_LOGN='"+dtrNews["PERS_LOGN"].ToString()+"' where PERS_ID = '"+txt2.Text+"' ", connCMD);
				cmd.ExecuteNonQuery();
				// TODO
				//UPDATE EMAIL_SENT_TO_LEAD SET EMAIL_PERSON ='4324' WHERE EMAIL_PERSON='2386' 
				cmd= new SqlCommand("UPDATE COMMENT SET CMNT_PERS ='"+txt2.Text+"' WHERE CMNT_PERS='"+txt1.Text+"' ", connCMD);
				cmd.ExecuteNonQuery();
				cmd= new SqlCommand("UPDATE GROUP_MEMBER SET GROUP_MEMBER_CONTACT_ID ='"+txt2.Text+"' WHERE GROUP_MEMBER_CONTACT_ID='"+txt1.Text+"' ", connCMD);
				cmd.ExecuteNonQuery();
				cmd= new SqlCommand("UPDATE EMAIL_SENT_TO_LEAD SET EMAIL_PERSON ='"+txt2.Text+"' WHERE EMAIL_PERSON='"+txt1.Text+"' ", connCMD);
				cmd.ExecuteNonQuery();
				
				cmd= new SqlCommand("UPDATE SHIPMENT SET SHIPMENT_BUYR ='"+txt2.Text+"' WHERE SHIPMENT_BUYR='"+txt1.Text+"' ", connCMD);
				cmd.ExecuteNonQuery();
				cmd= new SqlCommand("UPDATE SHIPMENT SET SHIPMENT_SELR ='"+txt2.Text+"' WHERE SHIPMENT_SELR='"+txt1.Text+"' ", connCMD);
				cmd.ExecuteNonQuery();

				cmd= new SqlCommand("UPDATE BBID SET BID_PERS_ID ='"+txt2.Text+"' WHERE BID_PERS_ID='"+txt1.Text+"' ", connCMD);
				cmd.ExecuteNonQuery();
				cmd= new SqlCommand("UPDATE BBIDARCH SET BID_PERS_ID ='"+txt2.Text+"' WHERE BID_PERS_ID='"+txt1.Text+"' ", connCMD);
				cmd.ExecuteNonQuery();

				cmd= new SqlCommand("UPDATE BBOFFER SET OFFR_PERS_ID ='"+txt2.Text+"' WHERE OFFR_PERS_ID='"+txt1.Text+"' ", connCMD);
				cmd.ExecuteNonQuery();
				cmd= new SqlCommand("UPDATE BBOFFERARCH SET OFFR_PERS_ID ='"+txt2.Text+"' WHERE OFFR_PERS_ID='"+txt1.Text+"' ", connCMD);
				cmd.ExecuteNonQuery();

				cmd= new SqlCommand("UPDATE PERSON SET PERS_PREF ='"+txt2.Text+"' WHERE PERS_PREF='"+txt1.Text+"' ", connCMD);
				cmd.ExecuteNonQuery();
				
				cmd= new SqlCommand("UPDATE PAYMENT SET PAY_PERS ='"+txt2.Text+"' WHERE PAY_PERS='"+txt1.Text+"' ", connCMD);
				cmd.ExecuteNonQuery();

				if (cbAccess.Checked)
				{
					cmd= new SqlCommand("UPDATE PERSON SET PERS_ACES='"+dtrNews["PERS_ACES"].ToString()+"' where PERS_ID = '"+txt2.Text+"' ", connCMD);
					cmd.ExecuteNonQuery();
				}
				if (Convert.ToInt16(dtrNews["PERS_NUMBER_LOGINS"]) > 0)
				{
					cmd= new SqlCommand("UPDATE PERSON SET PERS_NUMBER_LOGINS='"+dtrNews["PERS_NUMBER_LOGINS"].ToString()+"' where PERS_ID = '"+txt2.Text+"' ", connCMD);
					cmd.ExecuteNonQuery();

				}

				cmd= new SqlCommand("DELETE FROM PERSON where PERS_ID = '"+txt1.Text+"' ", connCMD);
				cmd.ExecuteNonQuery();
				lbl.Text =dtrNews["Name"].ToString()+" successfully merged";
			}
		
			dtrNews.Close();
			connCMD.Close();
			conn.Close();
			txt1.Text="";
			txt2.Text="";
			cbAccess.Checked = true;

		}
	}
}
