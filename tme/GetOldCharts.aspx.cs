using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using RestoreCharts;
using System.Threading;
using dotnetCHARTING;
using TPE.Utility;
namespace localhost.Administrator
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.Page.IsPostBack)
            {
                string[] Date = new string[15];
                Date[0] = "2005-10-12 17:02:36.037";
                Date[1] = "2005-11-16 17:02:36.037";
                Date[2] = "2005-12-28 17:02:36.037";
                Date[3] = "2006-02-02 17:02:36.037";
                Date[4] = "2006-03-06 17:02:36.037";
                Date[5] = "2006-04-12 17:02:36.037";
                Date[6] = "2006-05-10 17:02:36.037";
                Date[7] = "2006-06-13 17:02:36.037";
                Date[8] = "2006-07-17 17:02:36.037";
                Date[9] = "2006-08-16 17:02:36.037";
                Date[10] = "2006-09-15 17:02:36.037";
                Date[11] = "2006-10-25 17:02:36.037";
                Date[12] = "2006-10-26 17:02:36.037";
                Date[13] = "2006-12-07 17:02:36.037";
                Date[14] = "2007-01-11 17:02:36.037";


                int i = 0;
                while (i < 15)
                {
                    dotnetCHARTING.Chart ChartObj = new dotnetCHARTING.Chart();


                    // Add the random data.
                    ChartObj.SeriesCollection.Add(getRandomData());

                    ChartObj.Width = 10;
                    ChartObj.Height = 10;


                    ChartObj.TempDirectory = "Temp";

                    // Set the name of the file
                    ChartObj.FileName = "Temp";

                    // Set the format of the file
                    ChartObj.FileManager.ImageFormat = ImageFormat.Png;
                    Bitmap bmp1 = ChartObj.GetChartBitmap();
                    //ChartObj.FileManager.SaveImage(bmp1);


                    //Set the DB Connection to the Chart Control. It makes it to be able to re-create all the charts, if necessary.
                    TPEChartsControl chartsControl = new TPEChartsControl();
                    chartsControl.ConnectionStringDB = Application["DBConn"].ToString();
                    chartsControl.setFolder(Server.MapPath("/Research/RestoreCharts"));
                    chartsControl.CurrentDate = Date[i];
                    Thread threadCharts = new Thread(new ThreadStart(chartsControl.check_images));
                    threadCharts.Start();
                    while (threadCharts.ThreadState == ThreadState.Running)
                    {
                    }
                    if ((i == 11) | (i == 12) | (i == 13) | (i == 14))
                    {
                        copyChartsPNG(Date[i]);
                    }
                    else
                    {
                        copyChartsJPG(Date[i]);
                    }
                    i++;
                }
                                
            }
        }
        private SeriesCollection getRandomData()
        {
            SeriesCollection SC = new SeriesCollection();
            Random myR = new Random();
            for (int a = 0; a < 4; a++)
            {
                Series s = new Series();
                s.Name = "Series " + a;
                for (int b = 0; b < 5; b++)
                {
                    Element e = new Element();
                    e.Name = "E " + b;
                    e.YValue = myR.Next(50);
                    s.Elements.Add(e);
                }
                SC.Add(s);
            }

            // Set Different Colors for our Series
            SC[0].DefaultElement.Color = Color.FromArgb(49, 255, 49);
            SC[1].DefaultElement.Color = Color.FromArgb(255, 255, 0);
            SC[2].DefaultElement.Color = Color.FromArgb(255, 99, 49);
            SC[3].DefaultElement.Color = Color.FromArgb(0, 156, 255);
            return SC;
        }
        protected void copyChartsPNG(string sDate)
        {
            string[] charts = new string[6];
            DateTime moment = Convert.ToDateTime(sDate);

            charts[0] = Server.MapPath("/Research/RestoreCharts/S_Chart_3_1M.png");
            charts[1] = Server.MapPath("/Research/RestoreCharts/S_Chart_3_1Y.png");
            charts[2] = Server.MapPath("/Research/RestoreCharts/S_Chart_26_1M.png");
            charts[3] = Server.MapPath("/Research/RestoreCharts/S_Chart_26_1Y.png");
            charts[4] = Server.MapPath("/Research/RestoreCharts/S_Chart_20_1M.png");
            charts[5] = Server.MapPath("/Research/RestoreCharts/S_Chart_20_1Y.png");

            string path1 = "/Pics/mu_pics/MUPE" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + "_1M.png";
            string path2 = "/Pics/mu_pics/MUPE" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + ".png";
            string path3 = "/Pics/mu_pics/MUPP" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + "_1M.png";
            string path4 = "/Pics/mu_pics/MUPP" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + ".png";
            string path5 = "/Pics/mu_pics/MUPS" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + "_1M.png";
            string path6 = "/Pics/mu_pics/MUPS" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + ".png";
            
            System.IO.File.Copy(charts[0], Server.MapPath(path1), true);
            System.IO.File.Copy(charts[1], Server.MapPath(path2), true);

            System.IO.File.Copy(charts[2], Server.MapPath(path3), true);
            System.IO.File.Copy(charts[3], Server.MapPath(path4), true);
            System.IO.File.Copy(charts[4], Server.MapPath(path5), true);
            System.IO.File.Copy(charts[5], Server.MapPath(path6), true);
        }

        protected void copyChartsJPG(string sDate)
        {
            string[] charts = new string[6];
            DateTime moment = Convert.ToDateTime(sDate);

            charts[0] = Server.MapPath("/Research/RestoreCharts/S_Chart_3_1M.png");
            charts[1] = Server.MapPath("/Research/RestoreCharts/S_Chart_3_1Y.png");
            charts[2] = Server.MapPath("/Research/RestoreCharts/S_Chart_26_1M.png");
            charts[3] = Server.MapPath("/Research/RestoreCharts/S_Chart_26_1Y.png");
            charts[4] = Server.MapPath("/Research/RestoreCharts/S_Chart_20_1M.png");
            charts[5] = Server.MapPath("/Research/RestoreCharts/S_Chart_20_1Y.png");

            string path1 = "/Pics/mu_pics/MUPE" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + "_1M.jpg";
            string path2 = "/Pics/mu_pics/MUPE" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + ".jpg";
            string path3 = "/Pics/mu_pics/MUPP" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + "_1M.jpg";
            string path4 = "/Pics/mu_pics/MUPP" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + ".jpg";
            string path5 = "/Pics/mu_pics/MUPS" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + "_1M.jpg";
            string path6 = "/Pics/mu_pics/MUPS" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + ".jpg";

            System.IO.File.Copy(charts[0], Server.MapPath(path1), true);
            System.IO.File.Copy(charts[1], Server.MapPath(path2), true);

            System.IO.File.Copy(charts[2], Server.MapPath(path3), true);
            System.IO.File.Copy(charts[3], Server.MapPath(path4), true);
            System.IO.File.Copy(charts[4], Server.MapPath(path5), true);
            System.IO.File.Copy(charts[5], Server.MapPath(path6), true);
        }

    }
    
}
