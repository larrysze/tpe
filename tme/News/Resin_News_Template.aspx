<%@ Page Language="c#" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Data.SqlClient" %>
<script runat="server">

    /******************************************************************************
    '*   1. File Name       : Public\News_Template.aspx                          *
    '*   2. Description     : Display news                                       *
    '*			      Accessed from Public_News(View all news)                    *
    '*   3. Modification Log:                                                    *
    '*     Ver No.       Date          Author             Modification           *
    '*   -----------------------------------------------------------------       *
    '*                                                                           *
    '*      1.00       2-27-2004       Xiaoda               Comment              *
    '*      2.00       4-24-2004       Zach                 removed XML based lookup
    '*****************************************************************************/
    private void Page_Load(object sender, System.EventArgs e) {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
    
        //btnBack.Attributes.Add("OnClick", "document.history.back();");
            //Request a current news
        if (Request.QueryString["Id"] != null){
                  SqlConnection conn;
            conn = new SqlConnection(Application["DBconn"].ToString());
            conn.Open();
            SqlCommand cmdStory;
            SqlDataReader dtrStory;
            // selecting story
            cmdStory = new SqlCommand("SELECT * FROM NEWS WHERE NEWS_ID="+Request.QueryString["Id"].ToString(),conn);
            dtrStory = cmdStory.ExecuteReader();
    
            if (dtrStory.Read()){
                WebBox1.Heading = dtrStory["NEWS_LABL"].ToString();
                template.PageTitle = dtrStory["NEWS_LABL"].ToString();
                sb.Append( "<BR>"+ dtrStory["NEWS_CONTENT"].ToString() );
            }else{
                // unknown story
                sb.Append("<BR>Sorry, but the system can not access this story.  We apologize for any inconvience<BR><BR><BR><BR><BR><BR>");
            }
    
    
            conn.Close();
    
    
        }else{
            // no id in the querystring.  can't access news
            sb.Append("<BR>Sorry, but the system can not access this story.  We apologize for any inconvience<BR><BR><BR><BR><BR><BR>");
        }
        lblContent.Text = sb.ToString();
    }

</script>
<form runat="server" id="Form1">
    <TPE:Template id="template" Runat="Server" />
      <TPE:Web_Box id="WebBox1" Heading="News" width="100%" Runat="Server" />
        <asp:Label id="lblContent" runat="server" />
      <TPE:Web_Box Footer="true" Runat="Server" />
    <TPE:Template Footer="true" Runat="Server" />

</form>