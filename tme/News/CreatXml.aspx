<%@ Page language="c#" Codebehind="CreatXml.aspx.cs" AutoEventWireup="True" Inherits="localhost.news.CreatXml" validateRequest="false" Title="Create XML" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">	
			<table width="641" height="30" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="641" align="center">
						<asp:Label id="Message" CssClass="Content Color2" runat="server"></asp:Label>&nbsp;</td>
				</tr> 
			</table>
			<table width="641" height="300" border="0" cellpadding="0" cellspacing="0" class="Content Color2">
				<tr>
					<td width="49" rowspan="3" align="left" valign="top">&nbsp;</td>
					<td width="79" height="20" align="left" valign="middle">Headline</td>
					<td width="513" height="20" align="left" valign="top">
						<asp:TextBox id="txtHeadline" runat="server" CssClass="InputForm" Width="400px"></asp:TextBox></td>
				</tr>
				<tr>
					<td align="left" valign="top">Date</td>
					<td height="20" align="left" valign="top">
						<asp:TextBox id="txtData" runat="server" CssClass="InputForm" Width="80px"></asp:TextBox><FONT class="Content Color2">(mm-dd-yyyy)</FONT></td>
				</tr>
				<tr>
					<td align="left" valign="top">Story</td>
					<td height="260" align="left" valign="top">
						<asp:TextBox id="txtStory" runat="server" CssClass="InputForm" Width="400px" TextMode="MultiLine" Height="300px"></asp:TextBox></td>
				</tr>
			</table>
			<table width="641" border="0" cellspacing="0" cellpadding="0" class="Content Color2">
				<tr>
					<td align="center"><br />
						<asp:Button id="SaveNews" runat="server" CssClass="Content Color2" Width="80px" Text="Submit" onclick="SaveCustomNews_Click"></asp:Button>&nbsp;<br /><br /></td>
				    </tr>
			</table>
</asp:Content>