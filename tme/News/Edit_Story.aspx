<%@ Page Language="C#" validateRequest="false" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Web.Mail" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<script runat="server" language="C#">

    /************************************************************************
    *   1. File Name       :News\AddNews.aspx                               *
    *   2. Description     :Allows user to submit/edit news stories         *
    *   3. Modification Log:                                                *
    *     Ver No.       Date          Author             Modification       *
    *   -----------------------------------------------------------------   *
    *      1.00      2-25-2004      Zach                Comment             *
    *                                                                       *
    ************************************************************************/
    string EditNum;
    private void Page_Load(object sender, EventArgs e){

       EditNum = Request.QueryString["Id"].ToString();
       //if (Session["Typ"].ToString() !="A" && Session["Typ"].ToString() !="N") Response.Redirect("/default.aspx");
       if (!IsPostBack){
         SqlConnection conn;
         conn = new SqlConnection(Application["DBconn"].ToString());
         conn.Open();
         SqlCommand cmdContent;
         SqlDataReader dtrContent;
         cmdContent= new SqlCommand("Select * from NEWS WHERE NEWS_ID='" +EditNum +"'", conn);
         dtrContent = cmdContent.ExecuteReader();
         dtrContent.Read();

         txtHeadline.Text = dtrContent["NEWS_LABL"].ToString();
         txtStory.Text = dtrContent["NEWS_CONTENT"].ToString();

         ddlCategory.Items.Add(new ListItem (dtrContent["News_Type"].ToString()));
         ddlCategory.Items.Add(new ListItem ("General"));
         ddlCategory.Items.Add(new ListItem ("PE"));
         ddlCategory.Items.Add(new ListItem ("PP"));
         ddlCategory.Items.Add(new ListItem ("PS"));

         ddlLocation.Items.Add(new ListItem (dtrContent["News_AREA"].ToString()));
         ddlLocation.Items.Add(new ListItem ("North America"));
         ddlLocation.Items.Add(new ListItem ("Africa"));
         ddlLocation.Items.Add(new ListItem ("Asia"));
         ddlLocation.Items.Add(new ListItem ("Central/South America"));
         ddlLocation.Items.Add(new ListItem ("Europe"));
         ddlLocation.Items.Add(new ListItem ("Middle East"));
         ddlLocation.Items.Add(new ListItem ("N/A"));


         conn.Close();
       }

    }




    private void ClickCancel(object sender, EventArgs e){
        Response.Redirect("AddNews.aspx");
    }
    private void ClickSubmitStory(object sender, EventArgs e){
         SqlConnection conn;
         conn = new SqlConnection(Application["DBconn"].ToString());
         conn.Open();
         SqlCommand cmdInsert;
         cmdInsert= new SqlCommand("Update NEWS Set NEWS_LABL='"+txtHeadline.Text+"', NEWS_CONTENT='"+ParseParagraph(Parse(txtStory.Text))+"',NEWS_AREA='"+ddlLocation.SelectedItem.ToString()+"',NEWS_TYPE='"+ddlCategory.SelectedItem.ToString()+"' WHERE NEWS_ID='"+EditNum+"'", conn);
         cmdInsert.ExecuteNonQuery();
         conn.Close();
         // reset fields now that we are done
         txtStory.Text = "";
         txtHeadline.Text = "";
         Response.Redirect("AddNews.aspx");
    }



    // <summary>
    // The purpose of this method is for the program to render the carriage returns properly as new paragraphs
    // </summary>
    private string ParseParagraph(string strTextToParse){
        string[] arrParagraph;
        string strText;
        Regex r = new Regex(((char)13).ToString()); // Split on CR's.
        // compiler forces the array to be used outside of the switch statement
        // the following two lines will be overwritten but it is included to satisfy the compiler requirement
        arrParagraph = r.Split(strTextToParse);
        StringBuilder sb = new StringBuilder();


        // loops through the paragraphs now in the array and adds html to them
        for (int k=0;k < arrParagraph.Length;k++)
        {
            sb.Append("<P align=justify>" + arrParagraph[k]+"</p>");
        }
        // returns the properly formatted paragraph
        return sb.ToString();
    }


      // <summary>
      // Removes bad charachters that would crap out out the db insert
      // </summary>
      public string Parse(string TextIN){
       //  remove ' character
        string Parse;
        Parse = TextIN;
        while (Parse.IndexOf("'") > 0){
            Parse = Parse.Replace("'", "");
        }
        // remove " character
        while (Parse.IndexOf("\"") > 0){
            Parse = Parse.Replace("\"", "");
        }
        // remove whitespace
        while (Parse.IndexOf("  ") > 0){
            Parse = Parse.Replace("  ", " ");
        }

        return Parse;

      }

</script>
<html>
	<head>
	</head>
	<body>
		<form runat="server">
			<TPE:Template Margins="true" PageTitle="Edit Story" Runat="Server" />
			<table border="0" cellpadding="0" width="450" cellspacing="0" align="center">
				<tr>
					<td colspan="2" align="center"><span class="PageHeader">Edit Story</span></td>
				</tr>
				<tr>
					<td colspan="2">Location:
						<asp:DropDownList id="ddlLocation" runat="server"></asp:DropDownList>
						Category:
						<asp:DropDownList id="ddlCategory" runat="server"></asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						Headline:<BR>
						<asp:TextBox runat="server" id="txtHeadline" TextMode="MultiLine" Rows="2" Columns="50" maxlength="200" />
					</td>
				</tr>
				<tr>
					<td>
						Story:<BR>
						<asp:TextBox runat="server" id="txtStory" TextMode="MultiLine" Rows="30" Columns="50" maxlength="4000" />
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<asp:Button runat="server" onclick="ClickSubmitStory" Text="Submit" />
						&nbsp&nbsp&nbsp&nbsp&nbsp
						<asp:Button runat="server" onclick="ClickCancel" Text="Cancel" />
					</td>
				</tr>
			</table>
			<TPE:Template Footer="true" Runat="Server" />
		</form>
	</body>
</html>
