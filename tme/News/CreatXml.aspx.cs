using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.news
{
	/// <summary>
	/// CreatXml 
	/// </summary>
	public partial class CreatXml : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// 
            Master.Width = "760px";
			if(!Page.IsPostBack)
			{
                if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "L"))
				{
					Response.Redirect("../default.aspx");
				}
				
			}
		}

		#region 
		override protected void OnInit(EventArgs e)
		{
			//
			//
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// 
		/// 
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
        //read the templet of xml file,use the new data  instead of the placeholder(##XXX##).creat a new xml file write the data to the xml file,and save headline,date,filename to news.
		protected void SaveCustomNews_Click(object sender, System.EventArgs e)
		{
			if (!System.IO.File.Exists(Server.MapPath("../news/Current/")+"0000.xml"))
			{
				this.Message.Text="<font color=red>Error: Missing Xml Template File!!</font>";
			}
			else
			{
				try
				{
					if(this.txtHeadline.Text.Trim().Length==0||this.txtStory.Text.Trim().Length==0)//headline,story could not empty.
					{
						throw new Exception("you must input the headline or story");
					}
					if(!this.CheckData(this.txtData.Text))//the date inputed by user must be correct 
					{
						throw new Exception("date time incorrect");
					}

					string headline=this.FormatHeadline(this.txtHeadline.Text);//format headline
					string body=this.FormatStory(this.txtStory.Text);//format story
					DateTime date = Convert.ToDateTime(txtData.Text);
					
					
					CreatXMLFile(headline, date, body);
					if (this.Message.Text == "")
						this.Message.Text="input successfully!";
					
					this.txtData.Text="";
					this.txtHeadline.Text="";
					this.txtStory.Text="";
					    
				}
				catch(Exception message)
				{
					this.Message.Text=message.ToString();
				}
			}
		}
		private void CreatXMLFile(string headline, DateTime date, string body)
		{
			int filename = InsertToDB(headline, date);		

			System.Globalization.CultureInfo myculture=new System.Globalization.CultureInfo("en-US");
			string strDate = date.ToString("d MMMM ",myculture) + "," + date.Year.ToString();//creat a datetime just like :5 July ,2005

			string path=Server.MapPath("../news/Current/");
			string temppath=path+"0000.xml";
			path=path+filename+".xml";            
			System.Text.StringBuilder sb=new System.Text.StringBuilder("");
			try 
			{
				// Create an instance of StreamReader to read from the templet of xml file.
				// The using statement also closes the StreamReader.
				string line=null;
				try 
				{
					using (System.IO.StreamReader sr = new System.IO.StreamReader(temppath)) 
					{					
						//load the xml file to  string 
						while ((line=sr.ReadLine()) != null) 
						{
							sb.Append(line);
						}
						sr.Close();	
					}
				}
				catch 
				{
					throw new Exception("Missing xml template file");
				}

				//replace the placeholder(##XXX##) to the new data.
				sb.Replace("##filename##",filename+".xml");
				sb.Replace("##HeadLine##",headline);
				sb.Replace("##TransmissionId##",filename.ToString());
				sb.Replace("##DateLine##", strDate);
				sb.Replace("##content##",body);
				sb.Replace("##DateAndTime##",this.CreatLongDataTime());
				sb.Replace("##DateId##",this.CreatDataTime());
				sb.Replace("##FirstCreated##",this.CreatLongDataTime());
				System.IO.StreamWriter wr = System.IO.File.CreateText(path);
				wr.Write(sb.ToString());		
				wr.Close();
			}
			catch (Exception e) 
			{
				// Let the user know what went wrong.
				this.Message.Text=e.Message.ToString();
			}			                   
		}
		private string CreatDataTime()//format datatime just like 20050712
		{
			string datatime=System.DateTime.Now.Year.ToString()+System.DateTime.Now.Month.ToString()+System.DateTime.Now.Day.ToString();
			return datatime;
		}
		private string CreatLongDataTime()//format datatime just like 20050712T135431
		{
			string datatime=this.CreatDataTime()+"T"+System.DateTime.Now.Hour.ToString()+System.DateTime.Now.Minute.ToString()+System.DateTime.Now.Second.ToString();
			return datatime;
		}

		private int InsertToDB(string title, DateTime date)//insert the filename,headline to news
		{
			return DBLibrary.InsertAndReturnIdentity(Application["DBconn"].ToString(), "insert into NEWS (headline, date)values('"+title+"', getDate())",null);			
		}
		
		private bool CheckData(string data)
		{
			bool flag=true;
			System.DateTime d;
			try
			{
				d=System.DateTime.Parse(data);
			}
			catch
			{
				flag=false;
			}
			return flag;
		}		
		private string FormatCharacter(string ch)
		{
			ch=ch.Trim();
			ch=Server.HtmlEncode(ch);
			//ch=ch.Replace("'","''");
			ch=ch.Replace(@"\",@"\\");
			ch=ch.Replace("<","&lt;");
			ch=ch.Replace(">","&gt;");          
			return ch;
		}
		private string FormatStory(string ch)
		{
			ch=this.FormatCharacter(ch);
			ch="<p>"+ch+"</p>";
			ch=ch.Replace("\r","</p><p>");
			return ch;
		}
		private string FormatHeadline(string ch)
		{
			ch=FormatCharacter(ch);
			return ch;
		}
	}
}
