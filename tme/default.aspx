<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="localhost._default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>
        PCE - Logon
   </title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <center>
        <table>
            <tr>
                <td colspan="2" align="center">
                    <img src="images/lozier_header.gif" />                
                </td>            
            </tr>
        
            <tr>
                <td align="right">
                    <b>E-Mail:</b>
                </td>
                <td align="left">
                    <asp:TextBox runat="server" ID="txtEmail" />
                </td>            
            </tr>
            
            <tr>
                <td align="right"><b>Password:</b></td>
                <td align="left">
                    <asp:TextBox runat="server" id="txtPassword" TextMode="Password" />
                </td>            
            </tr>        
            <tr>
                <td colspan="2" align="center">
                    <asp:Button runat="server" ID="cmdLogon" Text="Sign On" OnClick="Button_Login" />
                </td>                
            </tr>        
        
        </table>
    
    </center>
        <asp:Literal ID="LivePerson" Visible="true" runat="server"></asp:Literal>
    
    </div>
    </form>
</body>
</html>
