<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TruckloadShippingMatrix.aspx.cs" Inherits="localhost.Shipping.TruckloadShippingMatrix" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" Title="Truckload Shipping Matrix"%>
<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master"%>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphCssLink">
        <style type="text/css">
        </style>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphHeading">
<div class="Header Bold Color1">Truckload Shipping Matrix</div>
</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
    <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="Black" Font-Size="10px"
        BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataSourceID="sdsTruckloadMatrix"
        ForeColor="Black" GridLines="Both" AutoGenerateColumns="False" EnableViewState="false" >
        <FooterStyle BackColor="#CCCCCC" />
        <SelectedRowStyle BackColor="#EEEEEE" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#FD9D00" Font-Bold="True" Font-Names="Verdana" ForeColor="Black" />
        <AlternatingRowStyle BackColor="#D9D7D3" />
        <Columns>
            <asp:BoundField DataField="From" HeaderText="To From" SortExpression="From" ItemStyle-BackColor="#FD9D00" ItemStyle-Font-Bold="true" ControlStyle-Width="5" >                    
            </asp:BoundField>
            <asp:BoundField DataField="AB" HeaderText="AB" ReadOnly="True" SortExpression="AB" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="AL" HeaderText="AL" ReadOnly="True" SortExpression="AL" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="AZ" HeaderText="AZ" ReadOnly="True" SortExpression="AZ" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="BC" HeaderText="BC" ReadOnly="True" SortExpression="BC" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="CA" HeaderText="CA" ReadOnly="True" SortExpression="CA" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="CO" HeaderText="CO" ReadOnly="True" SortExpression="CO" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="CT" HeaderText="CT" ReadOnly="True" SortExpression="CT" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="DE" HeaderText="DE" ReadOnly="True" SortExpression="DE" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="FL" HeaderText="FL" ReadOnly="True" SortExpression="FL" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="GA" HeaderText="GA" ReadOnly="True" SortExpression="GA" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="IA" HeaderText="IA" ReadOnly="True" SortExpression="IA" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="IL" HeaderText="IL" ReadOnly="True" SortExpression="IL" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="IN" HeaderText="IN" ReadOnly="True" SortExpression="IN" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="KS" HeaderText="KS" ReadOnly="True" SortExpression="KS" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="KY" HeaderText="KY" ReadOnly="True" SortExpression="KY" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="LA" HeaderText="LA" ReadOnly="True" SortExpression="LA" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="MA" HeaderText="MA" ReadOnly="True" SortExpression="MA" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="MB" HeaderText="MB" ReadOnly="True" SortExpression="MB" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="MD" HeaderText="MD" ReadOnly="True" SortExpression="MD" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="ME" HeaderText="ME" ReadOnly="True" SortExpression="ME" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="MI" HeaderText="MI" ReadOnly="True" SortExpression="MI" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="MN" HeaderText="MN" ReadOnly="True" SortExpression="MN" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="MO" HeaderText="MO" ReadOnly="True" SortExpression="MO" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="MS" HeaderText="MS" ReadOnly="True" SortExpression="MS" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="MT" HeaderText="MT" ReadOnly="True" SortExpression="MT" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="NB" HeaderText="NB" ReadOnly="True" SortExpression="NB" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="NC" HeaderText="NC" ReadOnly="True" SortExpression="NC" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="ND" HeaderText="ND" ReadOnly="True" SortExpression="ND" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="NE" HeaderText="NE" ReadOnly="True" SortExpression="NE" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="NH" HeaderText="NH" ReadOnly="True" SortExpression="NH" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="NJ" HeaderText="NJ" ReadOnly="True" SortExpression="NJ" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="NM" HeaderText="NM" ReadOnly="True" SortExpression="NM" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="NS" HeaderText="NS" ReadOnly="True" SortExpression="NS" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="NV" HeaderText="NV" ReadOnly="True" SortExpression="NV" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="NY" HeaderText="NY" ReadOnly="True" SortExpression="NY" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="OH" HeaderText="OH" ReadOnly="True" SortExpression="OH" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="OK" HeaderText="OK" ReadOnly="True" SortExpression="OK" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="ON" HeaderText="ON" ReadOnly="True" SortExpression="ON" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="OR" HeaderText="OR" ReadOnly="True" SortExpression="OR" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="PA" HeaderText="PA" ReadOnly="True" SortExpression="PA" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="PQ" HeaderText="PQ" ReadOnly="True" SortExpression="PQ" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="RI" HeaderText="RI" ReadOnly="True" SortExpression="RI" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="SC" HeaderText="SC" ReadOnly="True" SortExpression="SC" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="SD" HeaderText="SD" ReadOnly="True" SortExpression="SD" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="SK" HeaderText="SK" ReadOnly="True" SortExpression="SK" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="TN" HeaderText="TN" ReadOnly="True" SortExpression="TN" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="TX" HeaderText="TX" ReadOnly="True" SortExpression="TX" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="UT" HeaderText="UT" ReadOnly="True" SortExpression="UT" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="VA" HeaderText="VA" ReadOnly="True" SortExpression="VA" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="VT" HeaderText="VT" ReadOnly="True" SortExpression="VT" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="WA" HeaderText="WA" ReadOnly="True" SortExpression="WA" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="WI" HeaderText="WI" ReadOnly="True" SortExpression="WI" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="WV" HeaderText="WV" ReadOnly="True" SortExpression="WV" DataFormatString="{0:F2}" HtmlEncode="False"/>
            <asp:BoundField DataField="WY" HeaderText="WY" ReadOnly="True" SortExpression="WY" DataFormatString="{0:F2}" HtmlEncode="False"/>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="sdsTruckloadMatrix" runat="server" SelectCommand="spFreightMatrixTruckload" SelectCommandType="StoredProcedure"></asp:SqlDataSource>    
</asp:Content>

