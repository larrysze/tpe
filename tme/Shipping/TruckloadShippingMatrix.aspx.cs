using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace localhost.Shipping
{
    public partial class TruckloadShippingMatrix : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.Width = "1800px";
            if ((Session["Typ"].ToString() != "A") && (!bool.Parse(Application["IsDebug"].ToString())))
            {
                Response.Redirect("/default.aspx");
            }
			
            if (!this.IsPostBack)
            {
                this.sdsTruckloadMatrix.ConnectionString = Application["DBconn"].ToString();
            }
        }
    }
}
