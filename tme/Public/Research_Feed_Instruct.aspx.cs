using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace localhost.Public
{
	/// <summary>
	///
	/// </summary>
	public class HowToQuote : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox codeText;
		private System.Text.StringBuilder sb;
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(!Page.IsPostBack)
			{
				 MakeCode();
			}
			
		}
		private void MakeCode()
		{
			sb=new System.Text.StringBuilder("");
			sb.Append("<table width='100%' height='100%' border='0' align='center' cellpadding='0' cellspacing='0'>");
			sb.Append("<tr><td align='center' valign='top'>");
			sb.Append("<iframe align='top' width='766'  scrolling='no' src='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() +  "/Public/Research_Feed.aspx' frameborder='0' height='5000'></iframe>");
			sb.Append("</td></tr></table>");
			this.codeText.Text=sb.ToString();
		}
		#region Web 窗体设计器生成的代码
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: 该调用是 ASP.NET Web 窗体设计器所必需的。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// 设计器支持所需的方法 - 不要使用代码编辑器修改
		/// 此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
