<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page language="c#" Codebehind="MyExchange.aspx.cs" AutoEventWireup="false" Inherits="localhost.Public.MyExchange" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>My Exchange</title>
<meta content="Microsoft Visual Studio .NET 7.1" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema>
  </HEAD>
<body MS_POSITIONING="GridLayout">
<form id=Form1 method=post runat="server"><TPE:TEMPLATE id=Template1 Runat="server" PageTitle="News"></TPE:TEMPLATE>
<table cellpadding=0 cellspacing=0 border=0> 
  <TBODY>
 
    <tr>
    <td class=ListHeadlineBold>Current Spot Floor </td>
    <td class=ListHeadlineBold noWrap align=right><IMG 
      alt="Move this view up" 
      src="http://pics.ebaystatic.com/aw/pics/icon/iconMoveUp_20x20.gif" 
      align=absMiddle border=0> <IMG 
      alt="Move this view down" 
      src="http://pics.ebaystatic.com/aw/pics/icon/iconMoveDwn_20x20.gif" 
      align=absMiddle border=0>&nbsp; </td></tr>
  <tr>
    
  <TR>
    <TD vAlign=top align=right width="66%"><br>
      <TABLE id=dgSummaryGrid 
      style="BORDER-RIGHT: 0px; BORDER-TOP: 0px; FONT-SIZE: small; BORDER-LEFT: 0px; WIDTH: 100%; BORDER-BOTTOM: 0px; BORDER-COLLAPSE: collapse; BACKGROUND-COLOR: transparent" 
      cellSpacing=0 cellPadding=3 rules=rows align=center border=0 
      >
        <TBODY>
        <TR style="FONT-WEIGHT: bold; BACKGROUND-COLOR: #f1f1f1" 
        >
          <TD>Resin</TD>
          <TD>Total lbs</TD>
          <TD>Low</TD>
          <TD>High</TD></TR>
        <TR 
        onmouseover="ChartImg.src = '/Research/Charts/S_Chart_26_1Y.jpg';this.style.backgroundColor='Silver';this.style.cursor='hand';" 
        style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5" 
        onmouseout="this.style.backgroundColor='F5F5F5';this.style.cursor='pointer';" 
        >
          <TD noWrap><A href="/spot/Spot_Floor.aspx?Filter=8" >PP Homopolymer - 
            Inj</A></TD>
          <TD noWrap>6,807,920</TD>
          <TD noWrap>$0.48</TD>
          <TD noWrap>$0.61</TD></TR>
        <TR 
        onmouseover="ChartImg.src = '/Research/Charts/S_Chart_22_1Y.jpg';this.style.backgroundColor='Silver';this.style.cursor='hand';" 
        style="CURSOR: pointer; BORDER-TOP-STYLE: solid; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BACKGROUND-COLOR: #f5f5f5; BORDER-BOTTOM-STYLE: solid" 
        onclick="window.location ='/spot/Spot_Floor.aspx?Filter=9';" 
        onmouseout="this.style.backgroundColor='F5F5F5';this.style.cursor='pointer';" 
        >
          <TD noWrap><A href="/spot/Spot_Floor.aspx?Filter=9" >PP Copolymer - 
          Inj</A></TD>
          <TD noWrap>4,878,472</TD>
          <TD noWrap>$0.50</TD>
          <TD noWrap>$0.65</TD></TR>
        <TR 
        onmouseover="ChartImg.src = '/Research/Charts/S_Chart_6_1Y.jpg';this.style.backgroundColor='Silver';this.style.cursor='hand';" 
        style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5" 
        onclick="window.location ='/spot/Spot_Floor.aspx?Filter=4';" 
        onmouseout="this.style.backgroundColor='F5F5F5';this.style.cursor='pointer';" 
        >
          <TD noWrap><A href="/spot/Spot_Floor.aspx?Filter=4" >LDPE - Film</A></TD>
          <TD noWrap>4,722,024</TD>
          <TD noWrap>$0.52</TD>
          <TD noWrap>$0.68</TD></TR>
        <TR 
        onmouseover="ChartImg.src = '/Research/Charts/S_Chart_14_1Y.jpg';this.style.backgroundColor='Silver';this.style.cursor='hand';" 
        style="CURSOR: pointer; BORDER-TOP-STYLE: solid; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BACKGROUND-COLOR: #f5f5f5; BORDER-BOTTOM-STYLE: solid" 
        onclick="window.location ='/spot/Spot_Floor.aspx?Filter=6';" 
        onmouseout="this.style.backgroundColor='F5F5F5';this.style.cursor='pointer';" 
        >
          <TD noWrap><A href="/spot/Spot_Floor.aspx?Filter=6" >LLPDE - Film</A></TD>
          <TD noWrap>3,727,828</TD>
          <TD noWrap>$0.53</TD>
          <TD noWrap>$0.64</TD></TR>
        <TR 
        onmouseover="ChartImg.src = '/Research/Charts/S_Chart_2_1Y.jpg';this.style.backgroundColor='Silver';this.style.cursor='hand';" 
        style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5" 
        onclick="window.location ='/spot/Spot_Floor.aspx?Filter=2';" 
        onmouseout="this.style.backgroundColor='F5F5F5';this.style.cursor='pointer';" 
        >
          <TD noWrap><A href="/spot/Spot_Floor.aspx?Filter=2" >HDPE - Inj</A></TD>
          <TD noWrap>2,404,092</TD>
          <TD noWrap>$0.55</TD>
          <TD noWrap>$0.59</TD></TR>
        <TR 
        onmouseover="ChartImg.src = '/Research/Charts/S_Chart_3_1Y.jpg';this.style.backgroundColor='Silver';this.style.cursor='hand';" 
        style="CURSOR: pointer; BORDER-TOP-STYLE: solid; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BACKGROUND-COLOR: #f5f5f5; BORDER-BOTTOM-STYLE: solid" 
        onclick="window.location ='/spot/Spot_Floor.aspx?Filter=3';" 
        onmouseout="this.style.backgroundColor='F5F5F5';this.style.cursor='pointer';" 
        >
          <TD noWrap><A href="/spot/Spot_Floor.aspx?Filter=3" >HDPE - Blow 
          Mold</A></TD>
          <TD noWrap>1,951,000</TD>
          <TD noWrap>$0.53</TD>
          <TD noWrap>$0.57</TD></TR>
        <TR 
        onmouseover="ChartImg.src = '/Research/Charts/S_Chart_20_1Y.jpg';this.style.backgroundColor='Silver';this.style.cursor='hand';" 
        style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5" 
        onclick="window.location ='/spot/Spot_Floor.aspx?Filter=11';" 
        onmouseout="this.style.backgroundColor='F5F5F5';this.style.cursor='pointer';" 
        >
          <TD noWrap><A href="/spot/Spot_Floor.aspx?Filter=11" >HIPS</A></TD>
          <TD noWrap>1,359,868</TD>
          <TD noWrap>$0.72</TD>
          <TD noWrap>$0.79</TD></TR>
        <TR 
        onmouseover="ChartImg.src = '/Research/Charts/S_Chart_19_1Y.jpg';this.style.backgroundColor='Silver';this.style.cursor='hand';" 
        style="CURSOR: pointer; BORDER-TOP-STYLE: solid; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BACKGROUND-COLOR: #f5f5f5; BORDER-BOTTOM-STYLE: solid" 
        onclick="window.location ='/spot/Spot_Floor.aspx?Filter=10';" 
        onmouseout="this.style.backgroundColor='F5F5F5';this.style.cursor='pointer';" 
        >
          <TD noWrap><A href="/spot/Spot_Floor.aspx?Filter=10" >GPPS</A></TD>
          <TD noWrap>1,099,736</TD>
          <TD noWrap>$0.73</TD>
          <TD noWrap>$0.77</TD></TR>
        <TR 
        onmouseover="ChartImg.src = '/Research/Charts/S_Chart_17_1Y.jpg';this.style.backgroundColor='Silver';this.style.cursor='hand';" 
        style="CURSOR: pointer; BACKGROUND-COLOR: #f5f5f5" 
        onclick="window.location ='/spot/Spot_Floor.aspx?Filter=7';" 
        onmouseout="this.style.backgroundColor='F5F5F5';this.style.cursor='pointer';" 
        >
          <TD noWrap><A href="/spot/Spot_Floor.aspx?Filter=7" >LLDPE - Inj</A></TD>
          <TD noWrap>866,000</TD>
          <TD noWrap>$0.59</TD>
          <TD noWrap>$0.65</TD></TR>
        <TR 
        onmouseover="ChartImg.src = '/Research/Charts/S_Chart_1_1Y.jpg';this.style.backgroundColor='Silver';this.style.cursor='hand';" 
        style="BORDER-TOP-STYLE: solid; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BACKGROUND-COLOR: #f1f1f1; BORDER-BOTTOM-STYLE: solid" 
        onclick="window.location ='/spot/Spot_Floor.aspx?Filter=1';" 
        onmouseout="this.style.backgroundColor='F5F5F5';this.style.cursor='pointer';" 
        >
          <TD noWrap><A href="/spot/Spot_Floor.aspx?Filter=1" >HMWPE - Film 
          Grade</A></TD>
          <TD noWrap>636,184</TD>
          <TD noWrap>$0.53</TD>
          <TD noWrap>$0.59</TD></TR>
        <TR 
        onmouseover="ChartImg.src = '/Research/Charts/S_Chart_10_1Y.jpg';this.style.backgroundColor='Silver';this.style.cursor='hand';" 
        onclick="window.location ='/spot/Spot_Floor.aspx?Filter=5';" 
        onmouseout="this.style.backgroundColor='F5F5F5';this.style.cursor='pointer';" 
        >
          <TD noWrap><A href="/spot/Spot_Floor.aspx?Filter=5" >LDPE - Inj</A></TD>
          <TD noWrap>42,000</TD>
          <TD noWrap>$0.60</TD>
          <TD noWrap>$0.60</TD></TR></TBODY></TABLE></TD>
    <TD vAlign=top align=left width="34%"><br><IMG src="/Research/Charts/S_Chart_26_1Y.jpg" border=0 name=ChartImg ><A href="/research/charts.aspx" ></A> </TD></TR></TBODY></table>
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  <TBODY>
  <tr>
    <td class=ListHeadlineBold>Resin I'm Selling </td>
    <td class=ListHeadlineBold noWrap align=right><IMG 
      alt="Move this view up" 
      src="http://pics.ebaystatic.com/aw/pics/icon/iconMoveUp_20x20.gif" 
      align=absMiddle border=0> <IMG 
      alt="Move this view down" 
      src="http://pics.ebaystatic.com/aw/pics/icon/iconMoveDwn_20x20.gif" 
      align=absMiddle border=0>&nbsp; </td></tr>
  <tr>
    <td vAlign=top align=right colSpan=2><BR 
      >Period: <select> <OPTION 
        value=Last31Days selected>Last 31 days</OPTION> 
        <OPTION value=Last7Days>Last 7 days</OPTION> 
        <OPTION value=Last24Hrs>Last 24 hours</OPTION> 
        <OPTION value=CurrentWeek>Current week (Mar 
        20)</OPTION> <OPTION value=LastWeek>Last week (Mar 
        13)</OPTION> <OPTION value=CurrentMonth>Current 
        month (Mar)</OPTION> <OPTION value=LastMonth>Last 
        month (Feb)</OPTION> <OPTION value=Last62Days>Last 
        60 days</OPTION> <OPTION value=Custom>All 
        time</OPTION></select>&nbsp; </td></tr>
  <tr>
    <td align=center colSpan=2>
      <TABLE class=DataGrid id=SpotGrid1_dgAdmin 
      style="WIDTH: 100%; BORDER-COLLAPSE: collapse" cellSpacing=0 cellPadding=2 
      rules=all align=center border=1>
        <TBODY>
        <TR class=DataGridHeader>
          <TD></TD>
          <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl0','')" >#</A></TD>
          <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl1','')" >Product</A></TD>
          <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl2','')" >Size</A></TD>
          <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl3','')" >Melt</A></TD>
          <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl4','')" >Density</A></TD>
          <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl5','')" >Adds</A></TD>
          <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl6','')" >Delivery 
            Terms</A></TD>
          <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl9','')" >Price</A></TD><td></td></TR>
        <TR class=DataGridRow onmouseover="this.style.backgroundColor='Silver'" 
        style="BACKGROUND-COLOR: #eeeeee" 
        onmouseout="this.style.backgroundColor='#eeeeee'" 
          ><TD><A href="/Spot/Edit.aspx?ID=5362" >Edit</A><BR 
            ></TD>
          <TD align=right><A 
            href="javascript:confirmDelete('Enter&nbsp;Spot&nbsp;Floor','NREControl.jsp?cmd=delrule&amp;index=1&amp;contextid=5&amp;contextparam=null')" 
            lid="/hcp/ma/lp/delete.gif"></A>&nbsp;5362</TD>
          <TD noWrap>PP Copolymer - Inj<BR 
            >&nbsp;<SPAN class=S9 
            >Layered no break car</SPAN></TD>
          <TD noWrap align=right>1 Rail Car </TD>
          <TD align=right>6.45 </TD>
          <TD align=right>&nbsp;</TD>
          <TD align=right>19% Ethylene</TD>
          <TD noWrap align=right>Delivered</TD>
          <TD style="FONT-WEIGHT: bold" align=right 
          >0.5350</TD><td><img src="/pics/icons/icon_trash.gif"></td></TR>
        <TR class=DataGridRow_Alternate 
        onmouseover="this.style.backgroundColor='Silver'" 
        style="BACKGROUND-COLOR: #d9d7d3" 
        onmouseout="this.style.backgroundColor='#D9D7D3'" 
          ><TD><A href="/Spot/Edit.aspx?ID=5361" >Edit</A><BR 
            ></TD>
          <TD align=right>5361</TD>
          <TD noWrap>PP Homopolymer - Inj </TD>
          <TD noWrap align=right>1 Rail Car </TD>
          <TD align=right>13.4 </TD>
          <TD align=right>&nbsp;</TD>
          <TD align=right>&nbsp;</TD>
          <TD noWrap align=right>Delivered</TD>
          <TD style="FONT-WEIGHT: bold" align=right 
          >0.5300</TD><td><img src="/pics/icons/icon_trash.gif"></td></TR>
        <TR class=DataGridRow onmouseover="this.style.backgroundColor='Silver'" 
        style="BACKGROUND-COLOR: #eeeeee" 
        onmouseout="this.style.backgroundColor='#eeeeee'" 
          ><TD><A href="/Spot/Edit.aspx?ID=5360" >Edit</A><BR 
            ></TD>
          <TD align=right>5360</TD>
          <TD noWrap>PP Copolymer - Inj<BR 
            >&nbsp;<SPAN class=S9 
            >Blended Car</SPAN></TD>
          <TD noWrap align=right>1 Rail Car </TD>
          <TD align=right>13.4 </TD>
          <TD align=right>&nbsp;</TD>
          <TD align=right>5.5% Ethylene</TD>
          <TD noWrap align=right>Delivered</TD>
          <TD style="FONT-WEIGHT: bold" align=right 
          >0.5000</TD><td><img src="/pics/icons/icon_trash.gif"></td></TR>
        <TR class=DataGridRow_Alternate 
        onmouseover="this.style.backgroundColor='Silver'" 
        style="BACKGROUND-COLOR: #d9d7d3" 
        onmouseout="this.style.backgroundColor='#D9D7D3'" 
          ><TD><A href="/Spot/Edit.aspx?ID=5359" >Edit</A><BR 
            ></TD>
          <TD align=right>5359</TD>
          <TD noWrap>PP Homopolymer - Inj<BR 
            >&nbsp;<SPAN class=S9 
            >Blended Homopolymer &amp; Copolymer</SPAN></TD>
          <TD noWrap align=right>1 Rail Car </TD>
          <TD align=right>36.2 </TD>
          <TD align=right>&nbsp;</TD>
          <TD align=right>3.1% Ethylene</TD>
          <TD noWrap align=right>Delivered</TD>
          <TD style="FONT-WEIGHT: bold" align=right 
          >0.5000</TD><td><img src="/pics/icons/icon_trash.gif"></td></TR>
        <TR class=DataGridRow onmouseover="this.style.backgroundColor='Silver'" 
        style="BACKGROUND-COLOR: #eeeeee" 
        onmouseout="this.style.backgroundColor='#eeeeee'" 
          ><TD><A href="/Spot/Edit.aspx?ID=5358" >Edit</A><BR 
            ></TD>
          <TD align=right>5358</TD>
          <TD noWrap>PP Copolymer - Inj </TD>
          <TD noWrap align=right>1 Rail Car </TD>
          <TD align=right>3.2 </TD>
          <TD align=right>&nbsp;</TD>
          <TD align=right>18.6% Ethylene</TD>
          <TD noWrap align=right>Delivered</TD>
          <TD style="FONT-WEIGHT: bold" align=right 
          >0.5400</TD><td><img src="/pics/icons/icon_trash.gif"></td></TR>
        <TR class=DataGridRow_Alternate 
        onmouseover="this.style.backgroundColor='Silver'" 
        style="BACKGROUND-COLOR: #d9d7d3" 
        onmouseout="this.style.backgroundColor='#D9D7D3'" 
          ><TD><A href="/Spot/Edit.aspx?ID=5357" >Edit</A><BR 
            ></TD>
          <TD align=right>5357</TD>
          <TD noWrap>LDPE - Film<BR 
            >&nbsp;<SPAN class=S9 
            >Clarity</SPAN></TD>
          <TD noWrap align=right>8 Bulk Trucks</TD>
          <TD align=right>2 </TD>
          <TD align=right>.922 </TD>
          <TD align=right>1000/2000</TD>
          <TD noWrap align=right>FOB CHICAGO, IL</TD>
          <TD style="FONT-WEIGHT: bold" align=right 
          >0.6600</TD><td><img src="/pics/icons/icon_trash.gif"></td></TR>
        <TR class=DataGridRow onmouseover="this.style.backgroundColor='Silver'" 
        style="BACKGROUND-COLOR: #eeeeee" 
        onmouseout="this.style.backgroundColor='#eeeeee'" 
          ><TD><A href="/Spot/Edit.aspx?ID=5356" >Edit</A><BR 
            ></TD>
          <TD align=right>5356</TD>
          <TD noWrap>LDPE - Film </TD>
          <TD noWrap align=right>8 Bulk Trucks</TD>
          <TD align=right>.25 </TD>
          <TD align=right>.921 </TD>
          <TD align=right>0/4500</TD>
          <TD noWrap align=right>FOB ATLANTA, GA</TD>
          <TD style="FONT-WEIGHT: bold" align=right 
          >0.6650</TD><td><img src="/pics/icons/icon_trash.gif"></td></TR>
        <TR class=DataGridRow_Alternate 
        onmouseover="this.style.backgroundColor='Silver'" 
        style="BACKGROUND-COLOR: #d9d7d3" 
        onmouseout="this.style.backgroundColor='#D9D7D3'" 
          ><TD><A href="/Spot/Edit.aspx?ID=5355" >Edit</A><BR 
            ></TD>
          <TD align=right>5355</TD>
          <TD noWrap>LDPE - Film </TD>
          <TD noWrap align=right>4 Bulk Trucks</TD>
          <TD align=right>.25 </TD>
          <TD align=right>.921 </TD>
          <TD align=right>0/4500</TD>
          <TD noWrap align=right>FOB CHICAGO, IL</TD>
          <TD style="FONT-WEIGHT: bold" align=right 
          >0.6600</TD><td><img src="/pics/icons/icon_trash.gif"></td></TR>
        <TR class=DataGridRow onmouseover="this.style.backgroundColor='Silver'" 
        style="BACKGROUND-COLOR: #eeeeee" 
        onmouseout="this.style.backgroundColor='#eeeeee'" 
          ><TD><A href="/Spot/Edit.aspx?ID=5354" >Edit</A><BR 
            ></TD>
          <TD align=right>5354</TD>
          <TD noWrap>LLPDE - Film<BR 
            >&nbsp;<SPAN class=S9 
            >Octene with process aid</SPAN></TD>
          <TD noWrap align=right>4 Bulk Trucks</TD>
          <TD align=right>1 </TD>
          <TD align=right>.920 </TD>
          <TD align=right>1500/3500</TD>
          <TD noWrap align=right>FOB CHICAGO, IL</TD>
          <TD style="FONT-WEIGHT: bold" align=right 
          >0.6300</TD><td><img src="/pics/icons/icon_trash.gif"></td></TR>
        <TR class=DataGridRow_Alternate 
        onmouseover="this.style.backgroundColor='Silver'" 
        style="BACKGROUND-COLOR: #d9d7d3" 
        onmouseout="this.style.backgroundColor='#D9D7D3'" 
          ><TD><A href="/Spot/Edit.aspx?ID=5353" >Edit</A><BR 
            ></TD>
          <TD align=right>5353</TD>
          <TD noWrap>LLPDE - Film<BR 
            >&nbsp;<SPAN class=S9 
            >Octene with process aid</SPAN></TD>
          <TD noWrap align=right>4 Bulk Trucks</TD>
          <TD align=right>.9 </TD>
          <TD align=right>.921 </TD>
          <TD align=right>8000/3000</TD>
          <TD noWrap align=right>FOB CHICAGO, IL</TD>
          <TD style="FONT-WEIGHT: bold" align=right 
          >0.6000</TD><td><img src="/pics/icons/icon_trash.gif"></td></TR>
        <TR class=DataGridRow onmouseover="this.style.backgroundColor='Silver'" 
        style="BACKGROUND-COLOR: #eeeeee" 
        onmouseout="this.style.backgroundColor='#eeeeee'" 
          ><TD><A href="/Spot/Edit.aspx?ID=5352" >Edit</A><BR 
            ></TD>
          <TD align=right>5352</TD>
          <TD noWrap>LLPDE - Film<BR 
            >&nbsp;<SPAN class=S9 
            >Octene</SPAN></TD>
          <TD noWrap align=right>4 Bulk Trucks</TD>
          <TD align=right>.8 </TD>
          <TD align=right>.921 </TD>
          <TD align=right>1000/2500</TD>
          <TD noWrap align=right>FOB BUENA PARK, CA</TD>
          <TD style="FONT-WEIGHT: bold" align=right 
          >0.6400</TD><td><img src="/pics/icons/icon_trash.gif"></td></TR>
        <TR class=DataGridRow_Alternate 
        onmouseover="this.style.backgroundColor='Silver'" 
        style="BACKGROUND-COLOR: #d9d7d3" 
        onmouseout="this.style.backgroundColor='#D9D7D3'" 
          ><TD><A href="/Spot/Edit.aspx?ID=5351" >Edit</A><BR 
            ></TD>
          <TD align=right>5351</TD>
          <TD noWrap>LLDPE - Inj<BR 
            >&nbsp;<SPAN class=S9 
            >Hexene, Rotomolding</SPAN></TD>
          <TD noWrap align=right>4 Bulk Trucks</TD>
          <TD align=right>3.5 </TD>
          <TD align=right>.938 </TD>
          <TD align=right>2300 UVI</TD>
          <TD noWrap align=right>FOB BUENA PARK, CA</TD>
          <TD style="FONT-WEIGHT: bold" align=right 
          >0.6400</TD><td><img src="/pics/icons/icon_trash.gif"></td></TR>
        <TR class=DataGridRow onmouseover="this.style.backgroundColor='Silver'" 
        style="BACKGROUND-COLOR: silver" 
        onmouseout="this.style.backgroundColor='#eeeeee'" 
          ><TD><A href="/Spot/Edit.aspx?ID=5350" >Edit</A><BR 
            ></TD>
          <TD align=right>5350</TD>
          <TD noWrap>LLPDE - Film<BR 
            >&nbsp;<SPAN class=S9 
            >Hexene</SPAN></TD>
          <TD noWrap align=right>2 Bulk Trucks</TD>
          <TD align=right>1 </TD>
          <TD align=right>.917 </TD>
          <TD align=right>barefoot</TD>
          <TD noWrap align=right>FOB CHICAGO, IL</TD>
          <TD style="FONT-WEIGHT: bold" align=right 
          >0.6100</TD><td><img src="/pics/icons/icon_trash.gif"></td></TR>
        <TR class=DataGridRow_Alternate 
        onmouseover="this.style.backgroundColor='Silver'" 
        style="BACKGROUND-COLOR: #d9d7d3" 
        onmouseout="this.style.backgroundColor='#D9D7D3'" 
          ><TD><A href="/Spot/Edit.aspx?ID=5349" >Edit</A><BR 
            ></TD>
          <TD align=right>5349</TD>
          <TD noWrap>LLDPE - Inj<BR 
            >&nbsp;<SPAN class=S9 
            >Hexene</SPAN></TD>
          <TD noWrap align=right>2 Bulk Trucks</TD>
          <TD align=right>1 </TD>
          <TD align=right>.917 </TD>
          <TD align=right>barefoot</TD>
          <TD noWrap align=right>FOB CHICAGO, IL</TD>
          <TD style="FONT-WEIGHT: bold" align=right 
          >0.6100</TD><td><img src="/pics/icons/icon_trash.gif"></td></TR></TBODY></TABLE></td></tr>
  <tr>
    <td align=center colSpan=2>
    
    <HR>
		<table border=0 width="100%" cellspacing=0 cellpadding=0  >
			<tr>
				<td class="ListHeadlineBold" >
				Resin I have Sold 
				</td>
				<td  noWrap class="ListHeadlineBold" align=right>
					<IMG  alt="Move this view up" src="http://pics.ebaystatic.com/aw/pics/icon/iconMoveUp_20x20.gif" align=absMiddle border=0 >
					<IMG  alt="Move this view down" src="http://pics.ebaystatic.com/aw/pics/icon/iconMoveDwn_20x20.gif" align=absMiddle border=0>&nbsp; 
				</td>
			</tr>
			<tr>
				<td valign=top>
				 <table width="100%">
		   <tr>
          <td vAlign=top align=right colSpan=2><BR 
            >Period: <select> <OPTION 
              value=Last31Days selected>Last 31 
              days</OPTION> <OPTION value=Last7Days>Last 7 
              days</OPTION> <OPTION value=Last24Hrs>Last 24 
              hours</OPTION> <OPTION value=CurrentWeek 
              >Current week (Mar 20)</OPTION> <OPTION 
              value=LastWeek>Last week (Mar 13)</OPTION> 
              <OPTION value=CurrentMonth>Current month 
              (Mar)</OPTION> <OPTION value=LastMonth>Last 
              month (Feb)</OPTION> <OPTION value=Last62Days 
              >Last 60 days</OPTION> <OPTION value=Custom 
              >All Time</OPTION></select>&nbsp; </td></tr>
        <tr>
          <td align=center colSpan=2>
            <TABLE class=DataGrid id=SpotGrid1_dgAdmin 
            style="WIDTH: 100%; BORDER-COLLAPSE: collapse" cellSpacing=0 
            cellPadding=2 rules=all align=center border=1 
              ><TBODY>
              <TR class=DataGridHeader>
                <TD style="WIDTH: 1px"></TD>
                <TD style="WIDTH: 5px"><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl0','')" >#</A></TD>
                <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl1','')" >Product</A></TD>
                <TD style="WIDTH: 67px"><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl2','')" >Size</A></TD>
                <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl3','')" >Melt</A></TD>
                <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl4','')" >Density</A></TD>
                <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl5','')" >Adds</A></TD>
                <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl6','')" >Delivery 
                  Terms</A></TD>
                  <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl9','')" >Price/lbs</A></TD>
                <TD style="WIDTH: 63px"><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl9','')" >Total</A></TD>
                <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl9','')" >Balance Owed</A></TD></TR>
              <TR class=DataGridRow 
              onmouseover="this.style.backgroundColor='Silver'" 
              style="BACKGROUND-COLOR: #eeeeee" 
              onmouseout="this.style.backgroundColor='#eeeeee'" 
              >
                <TD style="WIDTH: 1px"><A 
                  href="Invoice ">Purchase Order</A> <BR></TD>
                <TD align=right style="WIDTH: 5px">37694-457</TD>
                <TD noWrap><SPAN class=S9 
                  >HDPE - Blow Mold Prime 5502</SPAN></TD>
                <TD noWrap align=right style="WIDTH: 67px">1 Rail Car </TD>
                <TD align=right>6.45 </TD>
                <TD align=right>&nbsp;</TD>
                <TD align=right>19% Ethylene</TD>
                <TD noWrap align=right>Waiting for 
                Pickup</TD>
                <TD  align=right>$0.47</TD>
               <TD  align=right style="WIDTH: 63px">N/A</TD>
                  <TD style="FONT-WEIGHT: bold" align=right 
                  ></TD>
                  </TR>
              <TR class=DataGridRow_Alternate 
              onmouseover="this.style.backgroundColor='Silver'" 
              style="BACKGROUND-COLOR: #d9d7d3" 
              onmouseout="this.style.backgroundColor='#D9D7D3'" 
              >
                <TD style="WIDTH: 1px"><A 
                  href="Invoice ">Purchase Order</A> <BR></TD>
                <TD align=right style="WIDTH: 5px">3693-961</TD>
                <TD noWrap>HDPE - Blow Mold Prime 5502 </TD>
                <TD noWrap align=right style="WIDTH: 67px">1 Rail Car </TD>
                <TD align=right>13.4 </TD>
                <TD align=right>&nbsp;</TD>
                <TD align=right>&nbsp;</TD>
                <TD noWrap align=right>Waiting for 
                Pickup</TD>
                <TD  align=right>$0.47</TD>
                <TD  align=right style="WIDTH: 63px">N/A</TD>
                  <TD style="FONT-WEIGHT: bold" align=right 
                  ></TD></TR>
              <TR class=DataGridRow 
              onmouseover="this.style.backgroundColor='Silver'" 
              style="BACKGROUND-COLOR: #eeeeee" 
              onmouseout="this.style.backgroundColor='#eeeeee'" 
              >
                <TD style="WIDTH: 1px"><A 
                  href="Invoice ">Purchase Order</A> <BR></TD>
                <TD align=right style="WIDTH: 5px">3692-987</TD>
                <TD noWrap>PP Copolymer - Inj<BR 
                  >&nbsp;<SPAN class=S9 
                  >Blended Car</SPAN></TD>
                <TD noWrap align=right style="WIDTH: 67px">190,789&nbsp; </TD>
                <TD align=right>13.4 </TD>
                <TD align=right>&nbsp;</TD>
					<TD align=right>5.5% Ethylene</TD>
					<TD noWrap align=right>Picked Up</TD>
					<TD  align=right>$0.47</TD>
					<TD  align=right style="WIDTH: 63px">($88,800)</TD>
					<TD style="FONT-WEIGHT: bold" align=right 
                  >($18,800)</TD>
					</TR>
					<TR class=DataGridRow_Alternate 
					onmouseover="this.style.backgroundColor='Silver'" 
					style="BACKGROUND-COLOR: #d9d7d3" 

					onmouseout="this.style.backgroundColor='#D9D7D3'">
					<TD style="WIDTH: 1px"><A 
                  href="Invoice ">Purchase Order</A><BR></TD>
					<TD align=right style="WIDTH: 5px">3691-531</TD>
					<TD noWrap>PP Homopolymer - Inj<BR>&nbsp;<SPAN class=S9>Blended Homopolymer &amp; 
					Copolymer</SPAN></TD>
					<TD noWrap align=right style="WIDTH: 67px"> 189,450 </TD>
					<TD align=right>36.2 </TD>
					<TD align=right>&nbsp;</TD>
					<TD align=right>3.1% Ethylene</TD>
					<TD noWrap align=right>Picked Up</TD>
					<TD  align=right>$0.47</TD>
					<TD  align=right style="WIDTH: 63px">($88,800)</TD>
					<TD style="FONT-WEIGHT: bold" align=right 
                  >($18,800)</TD>
					</TR>
					<TR class=DataGridHeader>
                <TD style="WIDTH: 1px"></TD>
                <TD style="WIDTH: 5px"></TD>
                <TD></TD>
                <TD style="WIDTH: 67px"></TD><TD></TD>
                <TD></TD>
                <TD></TD>
                <TD></TD>
                <TD></TD>
                <TD style="FONT-WEIGHT: bold; WIDTH: 63px" >Total:</TD>
                <TD  style="FONT-WEIGHT: bold">($37,600)</TD></TR>
					
					</TBODY>
					</TABLE></td></tr>
				 </table>

				</td>
			</tr>
		</table>
		<HR>
		<table border=0 width="100%" cellspacing=0 cellpadding=0  ></table>
				<table border=0 width="100%" cellspacing=0 cellpadding=0  >
			<tr>
				<td class="ListHeadlineBold" >
				Resin I'm Looking For
				</td>
				<td  noWrap class="ListHeadlineBold" align=right>
					<IMG  alt="Move this view up" src="http://pics.ebaystatic.com/aw/pics/icon/iconMoveUp_20x20.gif" align=absMiddle border=0 >
					<IMG  alt="Move this view down" src="http://pics.ebaystatic.com/aw/pics/icon/iconMoveDwn_20x20.gif" align=absMiddle border=0>&nbsp; 
				</td>
			</tr>
			<tr>
				<td valign=top>
				<br>
				<B>You do not have any pending requests. 
</B>	<br>
				</td>
			</tr>
		</table>
		<HR>
		<table border=0 width="100%" cellspacing=0 cellpadding=0  >
			<tr>
				<td class="ListHeadlineBold" >
				Resin I have Bought 
				</td>
				<td  noWrap class="ListHeadlineBold" align=right>
					<IMG  alt="Move this view up" src="http://pics.ebaystatic.com/aw/pics/icon/iconMoveUp_20x20.gif" align=absMiddle border=0 >
					<IMG  alt="Move this view down" src="http://pics.ebaystatic.com/aw/pics/icon/iconMoveDwn_20x20.gif" align=absMiddle border=0>&nbsp; 
				</td>
			</tr>
			<tr>
				<td valign=top>
				 <table width="100%">
		   <tr>
          <td vAlign=top align=right colSpan=2><BR 
            >Period: <select> <OPTION 
              value=Last31Days selected>Last 31 
              days</OPTION> <OPTION value=Last7Days>Last 7 
              days</OPTION> <OPTION value=Last24Hrs>Last 24 
              hours</OPTION> <OPTION value=CurrentWeek 
              >Current week (Mar 20)</OPTION> <OPTION 
              value=LastWeek>Last week (Mar 13)</OPTION> 
              <OPTION value=CurrentMonth>Current month 
              (Mar)</OPTION> <OPTION value=LastMonth>Last 
              month (Feb)</OPTION> <OPTION value=Last62Days 
              >Last 60 days</OPTION> <OPTION value=Custom 
              >All Time</OPTION></select>&nbsp; </td></tr>
        <tr>
          <td align=center colSpan=2>
            <TABLE class=DataGrid id=SpotGrid1_dgAdmin 
            style="WIDTH: 100%; BORDER-COLLAPSE: collapse" cellSpacing=0 
            cellPadding=2 rules=all align=center border=1 
              ><TBODY>
              <TR class=DataGridHeader>
                <TD style="WIDTH: 1px"></TD>
                <TD style="WIDTH: 5px"><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl0','')" >#</A></TD>
                <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl1','')" >Product</A></TD>
                <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl2','')" >Size</A></TD>
                <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl3','')" >Melt</A></TD>
                <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl4','')" >Density</A></TD>
                <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl5','')" >Adds</A></TD>
                <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl6','')" >Delivery 
                  Terms</A></TD>
                <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl9','')" >Price/lbs</A></TD>
                <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl9','')" >Price</A></TD>
                <TD><A href="javascript:__doPostBack('SpotGrid1$dgAdmin$_ctl1$_ctl9','')" >Balance </A></TD></TR>
              <TR class=DataGridRow 
              onmouseover="this.style.backgroundColor='Silver'" 
              style="BACKGROUND-COLOR: #eeeeee" 
              onmouseout="this.style.backgroundColor='#eeeeee'" 
              >
                <TD style="WIDTH: 1px"><A 
                  href="Invoice "> Invoice</A> <BR></TD>
                <TD align=right style="WIDTH: 5px">37694-457</TD>
                <TD noWrap><SPAN class=S9 
                  >HDPE - Blow Mold Prime 5502</SPAN></TD>
                <TD noWrap align=right> 191,964 </TD>
                <TD align=right>6.45 </TD>
                <TD align=right>&nbsp;</TD>
                <TD align=right>19% Ethylene</TD>
                <TD noWrap align=right> 
                Delivered</TD>
                <td>$0.52</td>
               <TD  align=right>$98,800</TD>
                  <TD style="FONT-WEIGHT: bold" align=right 
                  >$28,800</TD>
                  </TR>
              <TR class=DataGridRow_Alternate 
              onmouseover="this.style.backgroundColor='Silver'" 
              style="BACKGROUND-COLOR: #d9d7d3" 
              onmouseout="this.style.backgroundColor='#D9D7D3'" 
              >
                <TD style="WIDTH: 1px"><A 
                        href="Invoice ">Invoice</A> <BR></TD>
                <TD align=right style="WIDTH: 5px">3693-961</TD>
                <TD noWrap>HDPE - Blow Mold Prime 5502 </TD>
                <TD noWrap align=right> 192,111 </TD>
                <TD align=right>13.4 </TD>
                <TD align=right>&nbsp;</TD>
                <TD align=right>&nbsp;</TD>
                <TD noWrap align=right> 
                Enroute</TD>
                <td>$0.52</td>
                <TD  align=right>$98,800</TD>
                  <TD style="FONT-WEIGHT: bold" align=right 
                  >$28,800</TD></TR>
              <TR class=DataGridRow 
              onmouseover="this.style.backgroundColor='Silver'" 
              style="BACKGROUND-COLOR: #eeeeee" 
              onmouseout="this.style.backgroundColor='#eeeeee'" 
              >
                <TD style="WIDTH: 1px"><A 
                        href="Invoice ">Invoice</A> <BR></TD>
                <TD align=right style="WIDTH: 5px">3692-987</TD>
                <TD noWrap>PP Copolymer - Inj<BR 
                  >&nbsp;<SPAN class=S9 
                  >Blended Car</SPAN></TD>
                <TD noWrap align=right>1 Rail Car </TD>
                <TD align=right>13.4 </TD>
                <TD align=right>&nbsp;</TD>
					<TD align=right>5.5% Ethylene</TD>
					<TD noWrap align=right>Not shipped</TD>
					<td>$0.52</td>
					<TD  align=right>-</TD>
					<TD style="FONT-WEIGHT: bold" align=right 
                  >-</TD>
					</TR>
					<TR class=DataGridRow_Alternate 
					onmouseover="this.style.backgroundColor='Silver'" 
					style="BACKGROUND-COLOR: #d9d7d3" 

					onmouseout="this.style.backgroundColor='#D9D7D3'">
					<TD style="WIDTH: 1px"><BR><A href="Invoice ">Invoice</A> 
                      <BR></TD>
					<TD align=right style="WIDTH: 5px">3691-531</TD>
					<TD noWrap>PP Homopolymer - Inj<BR>&nbsp;<SPAN class=S9>Blended Homopolymer &amp; 
					Copolymer</SPAN></TD>
					<TD noWrap align=right> 188,125 </TD>
					<TD align=right>36.2 </TD>
					<TD align=right>&nbsp;</TD>
					<TD align=right>3.1% Ethylene</TD>
					<TD noWrap align=right> Delivered</TD>
					<td>$0.52</td>
					<TD  align=right>$98,800</TD>
					<TD style="FONT-WEIGHT: bold" align=right 
                  >$28,800</TD>
					</TR>
					<TR class=DataGridHeader>
                <TD style="WIDTH: 1px"></TD>
                <TD style="WIDTH: 5px"></TD>
                <TD></TD>
                <TD></TD><TD></TD>
                <TD></TD>
                <TD></TD>
                <TD></TD>
                <TD></TD>
                <TD style="FONT-WEIGHT: bold" >Total:</TD>
                <TD  style="FONT-WEIGHT: bold">$86,400</TD></TR>
					
					</TBODY>
					</TABLE></td></tr>
				 </table>

				</td>
			</tr>
		</table>
		<HR>
		
		
		
		
		
		
		
<TPE:Template Footer="true" Runat="server" id="Template2" /></form></TD></TR></TBODY></TABLE>
	
  </body>
</HTML>
