using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.Mail;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for Contact_Us.
	/// </summary>
	public partial class Contact_Us : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			btnSubmit.Attributes.Add("onMouseOver", "document.getElementById('" + btnSubmit.ClientID + "').src='/images2/submit_pressed.jpg';");
            btnSubmit.Attributes.Add("onMouseOut", "document.getElementById('" + btnSubmit.ClientID + "').src='/images2/submit_unpressed.jpg';");

			pnThankYou.Visible= false;

		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.Web.UI.ImageClickEventHandler(this.Button1_Click);

		}
		#endregion

		public void SaveUserRequest()
		{			
			Hashtable param = new Hashtable();
			param.Add("@param1", txtComments.Text);
			param.Add("@param2", txtName.Text);
			param.Add("@param3", txtEmail.Text);


			string strSQL = "INSERT INTO USER_REQUEST (REQ_MESSAGE, REQ_NAME, REQ_EMAIL, REQ_DATE_TIME) VALUES (@param1, @param2, @param3, getDate())";
			TPE.Utility.DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), strSQL, param);
				
			
		}

		private void Button1_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if (txtComments.Text != String.Empty)
			{
			
				MailMessage mail = new MailMessage();
				if (txtEmail.Text.Equals(""))
					mail.From = Application["strEmailOwner"].ToString();
				else
					mail.From = txtEmail.Text;

	
				mail.To = Application["strEmailInfo"].ToString();
				mail.Cc = Application["strEmailAdmin"]+";"+Application["strEmailOwner"].ToString();
				mail.Subject = "Webmail";
				mail.Body = "<H3>Webmail</H3>From: <b>"+ txtName.Text +"</b> <a href='mailto:"+ txtEmail.Text +"'>("+ txtEmail.Text +")</a><br>Comment: <b><pre>"+ txtComments.Text +"</pre></b><br>";
				mail.BodyFormat = MailFormat.Html;
				TPE.Utility.EmailLibrary.Send(Context,mail);
				//Response.Redirect("/default.aspx");

				//swap panels
				pnMain.Visible = false;
				pnThankYou.Visible = true;

				SaveUserRequest();
			}
		}
	}
}
