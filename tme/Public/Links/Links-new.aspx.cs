using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Web.Mail;
using TPE.Utility;

namespace localhost.Public.Links
{
	/// <summary>
	/// This screen will be used to add a new link into the Links Library.
	/// </summary>
	public partial class Links_new : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblTitle;
		private System.Data.SqlClient.SqlCommand cmd;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{		
			btnSave.Attributes.Add("onMouseOver","document.getElementById('" + btnSave.ClientID + "').src='/images/buttons/save_red.jpg';");
			btnSave.Attributes.Add("onMouseOut","document.getElementById('" + btnSave.ClientID + "').src='/images/buttons/save_orange.jpg';");
			btnCancel.Attributes.Add("onMouseOver","document.getElementById('" + btnCancel.ClientID + "').src='/images/buttons/cancel_red.jpg';");
			btnCancel.Attributes.Add("onMouseOut","document.getElementById('" + btnCancel.ClientID + "').src='/images/buttons/cancel_orange.jpg';");

			if (!IsPostBack)
			{
				//ViewState["Referrer"] = this.Request.UrlReferrer.AbsoluteUri.ToString();
				//ViewState["LINKS ID"] = Request.QueryString["ID"];
				//ViewState[" "] = Request.QueryString[""];

				Bind();
				if(this.Session["Typ"]!=null&&(this.Session["Typ"].ToString()=="A"||this.Session["Typ"].ToString()=="C")&&(Request.QueryString["ID"]!=null&&Request.QueryString["ID"].Trim().Length!=0))//the user must be admin.
				{
					ViewState["Id"]=Request.QueryString["ID"].ToString();
					this.LoadCompanyInfo();
				}
				else
				{
					ViewState["Id"] = System.DBNull.Value;
					lstCategories.Items[0].Selected = true;
				}
			}	
			
			
		}
		private void LoadCompanyInfo()//load the website's infomation
		{
			System.Text.StringBuilder sbStr=new System.Text.StringBuilder("");
			sbStr.Append("select * from links where links_id=@id");
			System.Data.SqlClient.SqlParameter[] parameter=new System.Data.SqlClient.SqlParameter[1];
			parameter[0]=new System.Data.SqlClient.SqlParameter("@id",ViewState["Id"].ToString());
			//dtr=Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteReader(Application["DBconn"].ToString(),System.Data.CommandType.Text,sbStr.ToString(),parameter);
			using (SqlConnection conn=new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
				cmd=conn.CreateCommand();
				cmd.CommandText=sbStr.ToString();
				cmd.CommandType=System.Data.CommandType.Text;
				for(int j=0;j<parameter.Length;j++)
				{
					cmd.Parameters.Add(parameter[j]);
				}
				using (SqlDataReader dtr=cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
				{
					if (dtr.Read())
					{
						this.txtCompany.Text=dtr["LINKS_COMP"].ToString();
						this.txtZip.Text=dtr["LINKS_ZIP"].ToString();
						this.txtCountry.Text=dtr["LINKS_COUNTRY"].ToString();
						this.txtEmail.Text=dtr["LINKS_EMAIL"].ToString();
						this.txtWebsite.Text = dtr["LINKS_WEBSITE"].ToString();
						this.txtDescription.Text=dtr["LINKS_DESCRIP"].ToString();
						this.txtContact.Text=dtr["LINKS_CONTACT"].ToString();
						this.txtPhone.Text=dtr["LINKS_PHONE"].ToString();
						this.txtFax.Text=dtr["LINKS_FAX"].ToString();
						this.txtAddress.Text=dtr["LINKS_STREET"].ToString();
						this.txtCity.Text=dtr["LINKS_CITY"].ToString();
						this.ddlState.Text=dtr["LINKS_STATE"].ToString();
					}			
					sbStr=new System.Text.StringBuilder("");
					sbStr.Append("select member_category from link_category_member where member_link_id=@id");
					parameter=new System.Data.SqlClient.SqlParameter[1];
					parameter[0]=new System.Data.SqlClient.SqlParameter("@id",dtr["links_id"].ToString());
					//dtr=Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteReader(Application["DBconn"].ToString(),System.Data.CommandType.Text,sbStr.ToString(),parameter);
				}
			}
			using (SqlConnection conn2 = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn2.Open();
				cmd=conn2.CreateCommand();
				cmd.CommandText=sbStr.ToString();
				cmd.CommandType=System.Data.CommandType.Text;
				for(int j=0;j<parameter.Length;j++)
				{
					cmd.Parameters.Add(parameter[j]);
				}
				using (SqlDataReader dtr2=cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
				{
					//get the category of the website
					while(dtr2.Read())
					{		
						lstCategories.Items.FindByValue(dtr2["member_category"].ToString()).Selected = true;
					}
				}
			}
		}

		public void Bind()
		{
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{			
				conn.Open();
				using (SqlDataReader dtrGroup = DBLibrary.GetDataReaderFromSelect(conn,"select LINK_CATEGORY_ID, (LINK_CATEGORY + ' - ' + LINK_SUBCATEGORY) as CATEGORY  from LINK_CATEGORY"))
				{
					while(dtrGroup.Read())
					{					
						lstCategories.Items.Add(new ListItem((string) dtrGroup["CATEGORY"],dtrGroup["LINK_CATEGORY_ID"].ToString()));
					}
				}
			}
		}

		private void btnCancel_Click(object sender, ImageClickEventArgs e)
		{
			//Response.Redirect(ViewState["Referrer"].ToString());
			//Response.Redirect("/Public/Links/Links-manager.aspx");
			Response.Redirect("/Public/Links/LinksResult.aspx");
		}

		
		private void btnSave_Click(object sender, ImageClickEventArgs e)
		{		
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
				
				if (ViewState["Id"] == System.DBNull.Value)
				{
					lblDuplicate.Text="";
					string txtWebString1="";
					string txtWebString2="";
					txtWebsite.Text=txtWebsite.Text.Trim();//remove " ";
					//the url "***.com"is equal to that "***.com/".
				
					StringBuilder sbSQL = new StringBuilder();

					if(txtWebsite.Text.Length!=0)//if user did not input the url,do nothing.
					{
						if(txtWebsite.Text.Substring(txtWebsite.Text.Length-1,1)==@"/")
						{
							txtWebString1=txtWebsite.Text.Substring(0,txtWebsite.Text.Length-1);
							txtWebString2=txtWebsite.Text;
						}
						else
						{
							txtWebString1=txtWebsite.Text;
							txtWebString2=txtWebsite.Text+@"/";
						}	
						txtWebString1=HelperFunction.RemoveSqlEscapeChars(txtWebString1);
						txtWebString2=HelperFunction.RemoveSqlEscapeChars(txtWebString2);

						Hashtable parameters = new Hashtable();
						parameters.Add("@LINKS_WEBSITE",txtWebString1);
						parameters.Add("@LINKS_WEBSITE2",txtWebString2);

						using (System.Data.SqlClient.SqlDataReader rd = DBLibrary.GetDataReaderFromSelect(conn,"Select links_id from LINKS WHERE LINKS_WEBSITE=@LINKS_WEBSITE or LINKS_WEBSITE=@LINKS_WEBSITE2", parameters))
						{
							if (!rd.Read())
							{
								//remove sql escape chars from all the fields.
								txtDescription.Text=HelperFunction.RemoveSqlEscapeChars(txtDescription.Text);//replace "'" to "''".
								txtCompany.Text=HelperFunction.RemoveSqlEscapeChars(txtCompany.Text);
								txtEmail.Text=HelperFunction.RemoveSqlEscapeChars(txtEmail.Text);
								txtPhone.Text=HelperFunction.RemoveSqlEscapeChars(txtPhone.Text);
								txtFax.Text=HelperFunction.RemoveSqlEscapeChars(txtFax.Text);
								txtContact.Text=HelperFunction.RemoveSqlEscapeChars(txtContact.Text);
								txtAddress.Text=HelperFunction.RemoveSqlEscapeChars(txtAddress.Text);
								txtCity.Text=HelperFunction.RemoveSqlEscapeChars(txtCity.Text);
								ddlState.Text=HelperFunction.RemoveSqlEscapeChars(ddlState.Text);
								txtZip.Text=HelperFunction.RemoveSqlEscapeChars(txtZip.Text);
								txtCountry.Text=HelperFunction.RemoveSqlEscapeChars(txtCountry.Text);
								txtWebsite.Text=HelperFunction.RemoveSqlEscapeChars(txtWebsite.Text);

								sbSQL.Append("INSERT INTO LINKS (LINKS_COMP,LINKS_EMAIL,LINKS_DESCRIP,LINKS_CONTACT,LINKS_PHONE,LINKS_FAX,LINKS_STREET,LINKS_CITY,LINKS_STATE,LINKS_ZIP,LINKS_COUNTRY,LINKS_WEBSITE)");
								sbSQL.Append("	VALUES ( ");
								sbSQL.Append(	"@LINKS_COMP,@LINKS_EMAIL,@LINKS_DESCRIP,@LINKS_CONTACT,@LINKS_PHONE,@LINKS_FAX,@LINKS_STREET,@LINKS_CITY,");
								sbSQL.Append(	"@LINKS_STATE,@LINKS_ZIP,@LINKS_COUNTRY,@LINKS_WEBSITE);");
								sbSQL.Append(" select @id=@@IDENTITY ");
								System.Data.SqlClient.SqlParameter[] parameter=new SqlParameter[1];
								parameter[0]=new SqlParameter("@id",System.Data.SqlDbType.Int);
								parameter[0].Direction=System.Data.ParameterDirection.Output;
								//Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(Application["DBconn"].ToString(),System.Data.CommandType.Text,sbSQL.ToString(),parameter);
								using (SqlConnection conn2 = new SqlConnection(Application["DBconn"].ToString()))
								{
									conn2.Open();
									cmd=conn2.CreateCommand();
									cmd.CommandText=sbSQL.ToString();
									cmd.CommandType=System.Data.CommandType.Text;
									for(int j=0;j<parameter.Length;j++)
									{
										cmd.Parameters.Add(parameter[j]);
									}
						
									cmd.Parameters.Add("@LINKS_COMP",txtCompany.Text);
									cmd.Parameters.Add("@LINKS_EMAIL",txtEmail.Text);
									cmd.Parameters.Add("@LINKS_DESCRIP",txtDescription.Text);
									cmd.Parameters.Add("@LINKS_CONTACT",txtContact.Text);
									cmd.Parameters.Add("@LINKS_PHONE",txtPhone.Text);
									cmd.Parameters.Add("@LINKS_FAX",txtFax.Text);
									cmd.Parameters.Add("@LINKS_STREET",txtAddress.Text);
									cmd.Parameters.Add("@LINKS_CITY",txtCity.Text);
									cmd.Parameters.Add("@LINKS_STATE",ddlState.Text);
									cmd.Parameters.Add("@LINKS_ZIP",txtZip.Text);
									cmd.Parameters.Add("@LINKS_COUNTRY",txtCountry.Text);
									cmd.Parameters.Add("@LINKS_WEBSITE",txtWebsite.Text.Trim());

									cmd.ExecuteNonQuery();
								}
								string url="LinksDetail.aspx?Id="+parameter[0].Value.ToString();
					
								sbSQL=new StringBuilder("");
								sbSQL.Append("insert into LINK_CATEGORY_MEMBER (MEMBER_CATEGORY,MEMBER_LINK_ID) values (@category,@link)");
								System.Data.SqlClient.SqlParameter[] par=new SqlParameter[2];
								using (SqlConnection conn3 =new SqlConnection(Application["DBconn"].ToString()))
								{
									conn3.Open();
									foreach(ListItem item in lstCategories.Items)
									{
										if (item.Selected == true)
										{
											par[0]=new SqlParameter("@category",item.Value.ToString());
											par[1]=new SqlParameter("@link",parameter[0].Value.ToString());
											cmd=conn3.CreateCommand();
											cmd.CommandText=sbSQL.ToString();
											cmd.CommandType=System.Data.CommandType.Text;
											for(int j=0;j<par.Length;j++)
											{
												cmd.Parameters.Add(par[j]);
											}
											cmd.ExecuteNonQuery();
										}
									}
								}								
					
								lblDuplicate.Visible=true;
								lblDuplicate.Text="Thank you for saving your web site to our library <br> please click <a href='"+url+"' >here</a> to visit the infomation";	
								System.Text.StringBuilder sbUserMail=new StringBuilder("");					
								CreatMail(txtWebsite.Text,ref sbUserMail);
								SendMail(sbUserMail.ToString(),Application["strEmailAdmin"].ToString(),txtWebsite.Text);

								string strblank ="";
								txtCompany.Text=strblank;
								txtWebsite.Text=strblank;
								txtDescription.Text=strblank;
								txtContact.Text=strblank;
								txtPhone.Text=strblank;
								txtFax.Text=strblank;
								txtAddress.Text=strblank;
								txtCity.Text=strblank;
								ddlState.Text=strblank;
								txtZip.Text=strblank;
								txtCountry.Text=strblank;
								txtEmail.Text=strblank;
								foreach(ListItem item in lstCategories.Items) 
									if (item.Selected == true) 
										item.Selected = false;
							}
							else
							{
								lblDuplicate.Text="URL already exists. Please change it.";
								this.lblDuplicate.Visible=true;
							}
						}
					}
					else
					{
						lblDuplicate.Text="Must be input the URL";
					}
				}
				else
				{
					this.txtDescription.Text=HelperFunction.RemoveSqlEscapeChars(this.txtDescription.Text);//replace "'" to "''".
					txtCompany.Text=HelperFunction.RemoveSqlEscapeChars(txtCompany.Text);
					txtEmail.Text=HelperFunction.RemoveSqlEscapeChars(txtEmail.Text);
					txtPhone.Text=HelperFunction.RemoveSqlEscapeChars(txtPhone.Text);
					txtFax.Text=HelperFunction.RemoveSqlEscapeChars(txtFax.Text);
					txtCity.Text=HelperFunction.RemoveSqlEscapeChars(txtCity.Text);
					this.ddlState.Text=HelperFunction.RemoveSqlEscapeChars(this.ddlState.Text);
					txtZip.Text=HelperFunction.RemoveSqlEscapeChars(txtZip.Text);
					txtCountry.Text=HelperFunction.RemoveSqlEscapeChars(txtCountry.Text);
					txtWebsite.Text=HelperFunction.RemoveSqlEscapeChars(txtWebsite.Text);
					System.Text.StringBuilder sbSQL=new System.Text.StringBuilder("");
					sbSQL.Append("UPDATE LINKS set LINKS_COMP=@comp,LINKS_EMAIL=@email,LINKS_DESCRIP=@descrip,LINKS_CONTACT=@contact,LINKS_PHONE=@phone,LINKS_FAX=@fax,LINKS_STREET=@street,LINKS_CITY=@city,LINKS_STATE=@state,LINKS_ZIP=@zip,LINKS_COUNTRY=@country,LINKS_WEBSITE=@website WHERE LINKS_ID=@id");	
					System.Data.SqlClient.SqlParameter[] parameter=new System.Data.SqlClient.SqlParameter[13];
					parameter[0]=new System.Data.SqlClient.SqlParameter("@comp",this.txtCompany.Text.Trim());
					parameter[1]=new System.Data.SqlClient.SqlParameter("@email",this.txtEmail.Text.Trim());
					parameter[2]=new System.Data.SqlClient.SqlParameter("@descrip",this.txtDescription.Text.Trim());
					parameter[3]=new System.Data.SqlClient.SqlParameter("@contact",this.txtContact.Text.Trim());
					parameter[4]=new System.Data.SqlClient.SqlParameter("@phone",this.txtPhone.Text.Trim());
					parameter[5]=new System.Data.SqlClient.SqlParameter("@fax",this.txtFax.Text.Trim());
					//parameter[6]=new System.Data.SqlClient.SqlParameter("@group",this.rblSubGroup.SelectedValue.ToString());
					parameter[6]=new System.Data.SqlClient.SqlParameter("@street",this.txtAddress.Text.Trim());
					parameter[7]=new System.Data.SqlClient.SqlParameter("@city",this.txtCity.Text.Trim());
					parameter[8]=new System.Data.SqlClient.SqlParameter("@state",this.ddlState.Text.Trim());
					parameter[9]=new System.Data.SqlClient.SqlParameter("@zip",this.txtZip.Text.Trim());
					parameter[10]=new System.Data.SqlClient.SqlParameter("@country",this.txtContact.Text.Trim());
					parameter[11]=new System.Data.SqlClient.SqlParameter("@website",this.txtWebsite.Text.Trim());
					parameter[12]=new System.Data.SqlClient.SqlParameter("@id",ViewState["Id"].ToString());
					try
					{
						if(this.txtCompany.Text.Trim().Length==0)
						{
							throw new Exception("company can not be empty");
						}
						if(this.txtWebsite.Text.Trim().Length==0)
						{
							throw new Exception("WebSite can not be empty");
						}
						//Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(Application["DBconn"].ToString(),System.Data.CommandType.Text,sbSQL.ToString(),parameter);//update the infomation 
						using (SqlConnection conn4 =new SqlConnection(Application["DBconn"].ToString()))
						{
							conn4.Open();
							cmd=conn4.CreateCommand();
							cmd.CommandText=sbSQL.ToString();
							cmd.CommandType=System.Data.CommandType.Text;
							for(int j=0;j<parameter.Length;j++)
							{
								cmd.Parameters.Add(parameter[j]);
							}
							cmd.ExecuteNonQuery();
						}
						sbSQL=new StringBuilder("");
						sbSQL.Append("INSERT INTO LINK_CATEGORY_MEMBER (MEMBER_CATEGORY,MEMBER_LINK_ID) VALUES (@category, @id)");
						System.Data.SqlClient.SqlParameter[] par=new SqlParameter[2];
				
						//Removing all associations
						Hashtable htParam = new Hashtable();
						htParam.Add("@LINK_ID",ViewState["Id"].ToString());
						DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"DELETE FROM LINK_CATEGORY_MEMBER WHERE MEMBER_LINK_ID = @LINK_ID",htParam);

						foreach(ListItem item in lstCategories.Items)
						{
							if (item.Selected == true)
							{
								par[0]=new SqlParameter("@category",item.Value.ToString());
								par[1]=new SqlParameter("@id",ViewState["Id"].ToString());
								using (SqlConnection conn6 = new SqlConnection(Application["DBconn"].ToString()))
								{
									conn6.Open();
									cmd=conn6.CreateCommand();
									cmd.CommandText=sbSQL.ToString();
									cmd.CommandType=System.Data.CommandType.Text;
									for(int j=0;j<par.Length;j++)
									{
										cmd.Parameters.Add(par[j]);
									}
									cmd.ExecuteNonQuery();
								}
							}
						}
						
						this.lblDuplicate.Visible=true;
						this.lblDuplicate.Text="Update successfully!";
					}
					catch(Exception em)
					{
						this.lblDuplicate.Visible=true;
						this.lblDuplicate.Text=em.Message.ToString();
					}
				}
			}
		}				
		private void CreatMail(string strUrl,ref System.Text.StringBuilder mailbody)//when add the links successly sent a mail. to admin 
		{
			string urlString=strUrl;
			System.Text.StringBuilder mailForUser=new StringBuilder("");//creat a mail body
			mailForUser.Append("Message from " + ConfigurationSettings.AppSettings["DomainName"].ToString() + ".<br><br>");
			mailForUser.Append("A new Link was included in our Link Library.<br>");
			mailForUser.AppendFormat("Web site address : <a href='{0}'>{0}</a>",urlString);
			mailbody=mailForUser;
		}
		private void SendMail(string MailBody,string mailTo, string strUrl)//send mail to user
		{
			System.Web.Mail.MailMessage mail=new System.Web.Mail.MailMessage();
			mail.To=mailTo;
			//mail.Cc=Application["strEmailOwner"].ToString();
			mail.From=mailTo;
			mail.Body=MailBody;
			mail.BodyFormat=System.Web.Mail.MailFormat.Html;
			mail.Subject = "Webmail New Link : " + strUrl;

			try
			{
				TPE.Utility.EmailLibrary.Send(Context,mail);
				this.errorMessage.Text="";
			}
			catch(Exception ie)
			{
				this.errorMessage.Text=ie.Message.ToString();
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{   
			this.btnSave.Click += new System.Web.UI.ImageClickEventHandler(this.btnSave_Click);
			this.btnCancel.Click += new System.Web.UI.ImageClickEventHandler(this.btnCancel_Click);

		}
		#endregion

	}
}