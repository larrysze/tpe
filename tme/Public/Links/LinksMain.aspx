<%@ Page language="c#" Codebehind="LinksMain.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.Links.LinkMain" MasterPageFile="~/MasterPages/Template.Master" Title = "Link Library"%>
<%@ MasterType virtualPath="~/MasterPages/Template.Master"%>
		
		<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">
		<script language="javascript">
		<!--
		if(document.all && !document.getElementById) 
		{
			document.getElementById = function(id) { 
				return document.all[id]; 
			}
		}
		//-->
		</script>
		</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphCssLink">
    <style type="text/css">
    .blackbold  {
	            font-family: Verdana, Arial, Helvetica, sans-serif;
	            font-size: 11px;
	            font-style: normal;
	            line-height: normal;
	            font-weight: bold;
	            font-variant: normal;
	            color: #000000;
	            padding-left: 2px;
	            padding-right: 2px;
                text-align: left;
                }
    .linksarrow {
	            background-image: url(/images2/links/arrow.gif);
	            background-color: #666666;	
	            height: 12px;
	            width: 16px;
	            padding-left: 2px;
	            }
    </style>
</asp:Content>


		
        <asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphInstructions">
			<table class='InstructionBullets'>
			<tr>
				<td width=35><img src='/pics/bullet.gif'></td><td>Click on a category below to display a brief description of each company listed in that group</td>
			</tr>
			<tr>
				<td width=35><img src='/pics/bullet.gif'></td><td>Click on Create New Link (on the bottom of the page) to join our directory</td>
			</tr>
			</table>
		</asp:Content>
		
		<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphHeading"><div class="Header Bold Color1">Industry links</div></asp:Content>		
		
		<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
		<table width="780" height="20" border="0" align="center" cellspacing="0">
		<tr>
			<td background="/images2/orangebar_background.jpg"><p class="blackbold">Plastic Processors </p></td>
		</tr>
		</table>
		<table width="780" border="0" cellspacing="1" cellpadding="2">
			<asp:Repeater id="rptPlasticProcessors" runat="server">			
				<ItemTemplate>
				<tr>
					<td class="linksarrow">&nbsp;</td>
					<td width="751" align="left"><span class="LinkNormal"><a href="LinksResult.aspx?Cat=<%# DataBinder.Eval(Container.DataItem, "LINK_CATEGORY_ID") %>"><%# DataBinder.Eval(Container.DataItem, "LINK_SUBCATEGORY") %> (<%# DataBinder.Eval(Container.DataItem, "NUM") %>)</a></span></td>
				</tr>				
				</ItemTemplate>
			</asp:Repeater>
			</table>
			
	<table width="780" height="20" border="0" align="center" cellspacing="0">
      <tr>
        <td background="/images2/orangebar_background.jpg"><p class="blackbold">Material Suppliers</p></td>
      </tr>
    </table>
			<table width="780" border="0" cellspacing="1" cellpadding="2">
			<asp:Repeater id="rptMaterialsSuppliers" runat="server">
				<ItemTemplate>

				<tr>
				<td width="18" class="linksarrow">&nbsp;</td>
				<td width="751" align="left"><span class="LinkNormal"><a href="LinksResult.aspx?Cat=<%# DataBinder.Eval(Container.DataItem, "LINK_CATEGORY_ID") %>" class="menu"><%# DataBinder.Eval(Container.DataItem, "LINK_SUBCATEGORY") %> (<%# DataBinder.Eval(Container.DataItem, "NUM") %>)</a></span></td>
				</tr>				
				</ItemTemplate>
			</asp:Repeater>
			</table>
			<BR>
			<CENTER><asp:ImageButton ID="btnLink" ImageUrl="/images/buttons/create_new_link_orange.jpg" OnClick="onClick_btnLink" runat=server></asp:ImageButton></CENTER>
			<br>
    </asp:Content>


