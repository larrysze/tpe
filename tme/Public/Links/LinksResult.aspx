<%@ Page language="c#" Codebehind="LinksResult.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.Links.LinksResult" MasterPageFile="~/MasterPages/Template.Master" Title = "Link Result"%>
<%@ MasterType virtualPath="~/MasterPages/Template.Master"%>


<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphCssLink">
    <style type="text/css">

	</style>
</asp:Content>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphInstructions">
	<table class='InstructionBullets'>
	<tr>
		<td width=35><img src='/pics/bullet.gif'></td><td>Click on a category below to display a brief description of each company listed in that group</td>
	</tr>
	<tr>
		<td width=35><img src='/pics/bullet.gif'></td><td>Click on Create New Link (on the bottom of the page) to join our directory</td>
	</tr>
	</table>
</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphHeading">

<table width='100%' border='0' cellpadding='0' cellspacing='0'>
<tr>
    <td align='left'><span class="Header Bold Color1"><asp:label id=lblTitle runat="server">Links</asp:label></span></td>
    <td align='right'><a class="Header Bold Color1" href='LinksMain.aspx'>Back to links</a></td>
</tr>
</table>
</asp:Content>


<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
	
<asp:datagrid BackColor="#000000" CellSpacing="1" BorderWidth="0" id=dg runat="server" Width="780px" HorizontalAlign="Left" CellPadding="2" AutoGenerateColumns="False" >
<AlternatingItemStyle CssClass="LinkNormal LightGray">
</AlternatingItemStyle>

<ItemStyle HorizontalAlign="Left" CssClass="LinkNormal DarkGray">
</ItemStyle>

<HeaderStyle CssClass="LinkNormal Bold OrangeColor">
</HeaderStyle>

<Columns>
<asp:HyperLinkColumn Target="_blank" DataNavigateUrlField="LINKS_WEBSITE" DataTextField="LINKS_COMP" HeaderText="Company"></asp:HyperLinkColumn>
<asp:BoundColumn DataField="LINKS_DESCRIP" HeaderText="Description"></asp:BoundColumn>
<asp:HyperLinkColumn Text="More" DataNavigateUrlField="LINKS_ID" DataNavigateUrlFormatString="LinksDetail.aspx?Id={0}">
<ItemStyle Wrap="False">
</ItemStyle>
</asp:HyperLinkColumn>
</Columns>
</asp:datagrid>
</asp:Content>

