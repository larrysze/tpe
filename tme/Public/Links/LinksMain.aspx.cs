using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Data.SqlClient;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;

namespace localhost.Public.Links
{
	/// <summary>
	/// Summary description for LinkMain.
	/// </summary>
	public partial class LinkMain : System.Web.UI.Page
	{
		//protected System.Web.UI.WebControls.Repeater rptOther;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			btnLink.Attributes.Add("onMouseOver","document.getElementById('" + btnLink.ClientID + "').src='/images/buttons/create_new_link_red.jpg';");
            btnLink.Attributes.Add("onMouseOut", "document.getElementById('" + btnLink.ClientID + "').src='/images/buttons/create_new_link_orange.jpg';");
			
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
				// Put user code to initialize the page here
				using (SqlDataReader dtrReader = DBLibrary.GetDataReaderFromSelect(conn,(getSQL("Plastics Processors"))))
					   {
					rptPlasticProcessors.DataSource = dtrReader;
					rptPlasticProcessors.DataBind();
				}

				using (SqlDataReader dtrReader2 = DBLibrary.GetDataReaderFromSelect(conn,(getSQL("Materials Suppliers"))))
					   {
					rptMaterialsSuppliers.DataSource = dtrReader2;
					rptMaterialsSuppliers.DataBind();
				}

				// others category
				/*
							cmd = new SqlCommand(getSQL("Others"),conn);
							dtrReader = cmd.ExecuteReader();
							rptOther.DataSource = dtrReader;
							rptOther.DataBind();
							dtrReader.Close();
				*/
				// close it down
			}
		}
		
		public void onClick_btnLink(object sender, ImageClickEventArgs e)
		{
			Response.Redirect("Links-new.aspx");
		}

		protected string getSQL(string strCategory)
		{
			string strSQL ="";
			strSQL = "Select ";
			strSQL += " LINK_CATEGORY_ID, ";
			strSQL += " LINK_CATEGORY, ";
			strSQL += " LINK_SUBCATEGORY, ";
			strSQL += " NUM=(SELECT count(*) from LINK_CATEGORY_MEMBER WHERE MEMBER_CATEGORY = LINK_CATEGORY_ID) ";
			strSQL += " FROM LINK_CATEGORY ";
			strSQL += " WHERE  ";
			if (strCategory == "Others")
			{
				strSQL += " LINK_CATEGORY <> 'Materials Suppliers' AND LINK_CATEGORY <> 'Plastics Processors' AND ";
			}
			else
			{
				strSQL += " LINK_CATEGORY = '"+strCategory+"' AND ";
			}
				strSQL += " (SELECT count(*) from LINK_CATEGORY_MEMBER WHERE MEMBER_CATEGORY = LINK_CATEGORY_ID) >0 ";
			strSQL += " ORDER BY LINK_SUBCATEGORY ";
			return strSQL;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
//			this. += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
