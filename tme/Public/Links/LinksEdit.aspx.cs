using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace localhost.Public.links
{
	/// <summary>
	/// 
	/// </summary>
	public class LinksEdit : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox txtDescription;
		protected System.Web.UI.WebControls.TextBox txtContact;
		protected System.Web.UI.WebControls.TextBox txtPhone;
		protected System.Web.UI.WebControls.TextBox txtFax;
		protected System.Web.UI.WebControls.TextBox txtAddress;
		protected System.Web.UI.WebControls.TextBox txtCountry;
		protected System.Web.UI.WebControls.TextBox txtState;
		protected System.Web.UI.WebControls.TextBox txtZip;
		protected System.Web.UI.WebControls.TextBox txtEmail;
		protected System.Web.UI.WebControls.DropDownList ddlSubCategory;
		protected System.Web.UI.WebControls.TextBox txtCompany;
		protected System.Web.UI.WebControls.TextBox txtWebsite;
		protected System.Web.UI.WebControls.TextBox txtCity;
		protected System.Web.UI.WebControls.Button update;
		protected System.Web.UI.WebControls.Label message;
		protected System.Web.UI.WebControls.DropDownList ddlCategory;
	
		private void Page_Load(object sender, System.EventArgs e)
		{	
			if(!Page.IsPostBack)
			{
				if(this.Session["Typ"]!=null&&(this.Session["Typ"].ToString()=="A"||this.Session["Typ"].ToString()=="C")&&(Request.QueryString["ID"]!=null&&Request.QueryString["ID"].Trim().Length!=0))//the user must be admin.
				{
					ViewState["Id"]=Request.QueryString["ID"].ToString();
					this.CreatCategory();
					this.LoadCompanyInfo();
				}
			}
			
		}
		private void CreatCategory()//creat category
		{
			System.Text.StringBuilder sbStr=new System.Text.StringBuilder("");
			sbStr.Append("SELECT DISTINCT LINK_CATEGORY FROM LINK_CATEGORY ORDER BY LINK_CATEGORY");
			System.Data.SqlClient.SqlDataReader dtrGroup=Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteReader(Application["DBconn"].ToString(),System.Data.CommandType.Text,sbStr.ToString());
			while(dtrGroup.Read())
			{
				this.ddlCategory.Items.Add(new ListItem(dtrGroup["LINK_CATEGORY"].ToString(),dtrGroup["LINK_CATEGORY"].ToString()));
			}
		}
		private void CreatSubCategory()//through the select item of the category,get the item of SubCategory.
		{
			System.Text.StringBuilder sbStr=new System.Text.StringBuilder("");
			sbStr.Append("SELECT DISTINCT LINK_SUBCATEGORY, LINK_CATEGORY_ID FROM LINK_CATEGORY WHERE LINK_CATEGORY=@category ORDER BY LINK_SUBCATEGORY");
			System.Data.SqlClient.SqlParameter[] parameter=new System.Data.SqlClient.SqlParameter[1];
            parameter[0]=new System.Data.SqlClient.SqlParameter("@category",this.ddlCategory.SelectedValue.ToString());
			System.Data.SqlClient.SqlDataReader dtrSubGroup=Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteReader(Application["DBconn"].ToString(),System.Data.CommandType.Text,sbStr.ToString(),parameter);
			this.ddlSubCategory.Items.Clear();
			while(dtrSubGroup.Read())
			{
				this.ddlSubCategory.Items.Add(new ListItem(dtrSubGroup["LINK_SUBCATEGORY"].ToString(),dtrSubGroup["LINK_CATEGORY_ID"].ToString()));
			}
		}
		private void LoadCompanyInfo()//load the website's infomation
		{
			System.Data.SqlClient.SqlDataReader dtr;
			System.Text.StringBuilder sbStr=new System.Text.StringBuilder("");
			sbStr.Append("select * from links where links_id=@id");
			System.Data.SqlClient.SqlParameter[] parameter=new System.Data.SqlClient.SqlParameter[1];
			parameter[0]=new System.Data.SqlClient.SqlParameter("@id",ViewState["Id"].ToString());
			dtr=Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteReader(Application["DBconn"].ToString(),System.Data.CommandType.Text,sbStr.ToString(),parameter);
			if (dtr.Read())
			{
				this.txtCompany.Text=dtr["LINKS_COMP"].ToString();
				this.txtZip.Text=dtr["LINKS_ZIP"].ToString();
				this.txtCountry.Text=dtr["LINKS_COUNTRY"].ToString();
				this.txtEmail.Text=dtr["LINKS_EMAIL"].ToString();
				this.txtWebsite.Text = dtr["LINKS_WEBSITE"].ToString();
				this.txtDescription.Text=dtr["LINKS_DESCRIP"].ToString();
				this.txtContact.Text=dtr["LINKS_CONTACT"].ToString();
				this.txtPhone.Text=dtr["LINKS_PHONE"].ToString();
				this.txtFax.Text=dtr["LINKS_FAX"].ToString();
				this.txtAddress.Text=dtr["LINKS_STREET"].ToString();
				this.txtCity.Text=dtr["LINKS_CITY"].ToString();
				this.txtState.Text=dtr["LINKS_STATE"].ToString();
			}
			sbStr=new System.Text.StringBuilder("");
			sbStr.Append("select * from link_category where link_category_id=@id");
			parameter=new System.Data.SqlClient.SqlParameter[1];
			parameter[0]=new System.Data.SqlClient.SqlParameter("@id",dtr["links_group"].ToString());
			dtr=Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteReader(Application["DBconn"].ToString(),System.Data.CommandType.Text,sbStr.ToString(),parameter);
			if(dtr.Read())//get the category of the website
			{		
				this.ddlCategory.SelectedItem.Selected=false;			
				this.ddlCategory.Items.FindByText(dtr["link_category"].ToString().Trim()).Selected=true;			
				CreatSubCategory();//get the subcategory
				this.ddlSubCategory.SelectedItem.Selected=false;
				this.ddlSubCategory.Items.FindByText(dtr["link_subcategory"].ToString().Trim()).Selected=true;			
			}
		}

		#region Web
		override protected void OnInit(EventArgs e)
		{
			//
			//
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///
		/// 
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddlCategory.SelectedIndexChanged += new System.EventHandler(this.ddlCategory_SelectedIndexChanged);
			this.update.Click += new System.EventHandler(this.update_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void ddlCategory_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.CreatSubCategory();
		}

		private void update_Click(object sender, System.EventArgs e)
		{			
			this.txtDescription.Text=HelperFunction.RemoveSqlEscapeChars(this.txtDescription.Text);//replace "'" to "''".
			txtCompany.Text=HelperFunction.RemoveSqlEscapeChars(txtCompany.Text);
			txtEmail.Text=HelperFunction.RemoveSqlEscapeChars(txtEmail.Text);
			txtPhone.Text=HelperFunction.RemoveSqlEscapeChars(txtPhone.Text);
			txtFax.Text=HelperFunction.RemoveSqlEscapeChars(txtFax.Text);
			txtCity.Text=HelperFunction.RemoveSqlEscapeChars(txtCity.Text);
			this.txtState.Text=HelperFunction.RemoveSqlEscapeChars(this.txtState.Text);
			txtZip.Text=HelperFunction.RemoveSqlEscapeChars(txtZip.Text);
			txtCountry.Text=HelperFunction.RemoveSqlEscapeChars(txtCountry.Text);
			txtWebsite.Text=HelperFunction.RemoveSqlEscapeChars(txtWebsite.Text);
			System.Text.StringBuilder sbSQL=new System.Text.StringBuilder("");
			sbSQL.Append("UPDATE LINKS set LINKS_COMP=@comp,LINKS_EMAIL=@email,LINKS_DESCRIP=@descrip,LINKS_CONTACT=@contact,LINKS_PHONE=@phone,LINKS_FAX=@fax,LINKS_GROUP=@group,LINKS_STREET=@street,LINKS_CITY=@city,LINKS_STATE=@state,LINKS_ZIP=@zip,LINKS_COUNTRY=@country,LINKS_WEBSITE=@website WHERE LINKS_ID=@id");	
			System.Data.SqlClient.SqlParameter[] parameter=new System.Data.SqlClient.SqlParameter[14];	
			parameter[0]=new System.Data.SqlClient.SqlParameter("@comp",this.txtCompany.Text.Trim());
			parameter[1]=new System.Data.SqlClient.SqlParameter("@email",this.txtEmail.Text.Trim());
			parameter[2]=new System.Data.SqlClient.SqlParameter("@descrip",this.txtDescription.Text.Trim());
			parameter[3]=new System.Data.SqlClient.SqlParameter("@contact",this.txtContact.Text.Trim());
			parameter[4]=new System.Data.SqlClient.SqlParameter("@phone",this.txtPhone.Text.Trim());
			parameter[5]=new System.Data.SqlClient.SqlParameter("@fax",this.txtFax.Text.Trim());
			parameter[6]=new System.Data.SqlClient.SqlParameter("@group",this.ddlSubCategory.SelectedValue.ToString());
			parameter[7]=new System.Data.SqlClient.SqlParameter("@street",this.txtAddress.Text.Trim());
			parameter[8]=new System.Data.SqlClient.SqlParameter("@city",this.txtCity.Text.Trim());
			parameter[9]=new System.Data.SqlClient.SqlParameter("@state",this.txtState.Text.Trim());
			parameter[10]=new System.Data.SqlClient.SqlParameter("@zip",this.txtZip.Text.Trim());
			parameter[11]=new System.Data.SqlClient.SqlParameter("@country",this.txtContact.Text.Trim());
			parameter[12]=new System.Data.SqlClient.SqlParameter("@website",this.txtWebsite.Text.Trim());
			parameter[13]=new System.Data.SqlClient.SqlParameter("@id",ViewState["Id"].ToString());
			try
			{
				if(this.txtCompany.Text.Trim().Length==0)
				{
					throw new Exception("company can not be empty");
				}
				if(this.txtWebsite.Text.Trim().Length==0)
				{
					throw new Exception("WebSite can not be empty");
				}
				Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(Application["DBconn"].ToString(),System.Data.CommandType.Text,sbSQL.ToString(),parameter);//update the infomation 
				this.message.Text="Update successfully!";
			}
			catch(Exception em)
			{
				this.message.Text=em.Message.ToString();
			}

		}
	}
}
