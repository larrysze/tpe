using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Public.Links
{
	/// <summary>
	/// Summary description for LinksDetail.
	/// </summary>
	public partial class LinksDetail : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
            Master.Width = "780px";
			ImageButton1.Attributes.Add("OnClick", "history.go(-1);");

			ImageButton1.Attributes.Add("onMouseOver", "document.getElementById('" + ImageButton1.ClientID + "').src ='/images/buttons/back_pressed.jpg';");
            ImageButton1.Attributes.Add("onMouseOut", "document.getElementById('" + ImageButton1.ClientID + "').src ='/images/buttons/back.jpg';");

			// Put user code to initialize the page here
			if(Request.QueryString["ID"] == null)
			{
				Response.Redirect("LinksResult.aspx");
			}


			if (((string)Session["Typ"]== "A") ||((string)Session["Typ"] == "C"))
			{				
				Response.Redirect("Links-new.aspx?ID="+Request.QueryString["ID"].ToString());
			}
			
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
				String strSql = "select * from links where links_id ='"+Request.QueryString["ID"].ToString()+"'";
				Hashtable htParams = new Hashtable();
				htParams.Add("@ID", Request.QueryString["ID"].ToString());
				using (SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn,strSql,htParams))
				{
					if (dtr.Read())
					{
						lblCompany.Text=dtr["LINKS_COMP"].ToString();
						lblZip.Text=dtr["LINKS_ZIP"].ToString();
						lblCountry.Text=dtr["LINKS_COUNTRY"].ToString();
						lblEmail.Text=dtr["LINKS_EMAIL"].ToString();
						HLWebsite.Text = dtr["LINKS_WEBSITE"].ToString();
						HLWebsite.NavigateUrl = dtr["LINKS_WEBSITE"].ToString();
						lblDescription.Text=dtr["LINKS_DESCRIP"].ToString();
						lblContact.Text=dtr["LINKS_CONTACT"].ToString();
						lblPhone.Text=dtr["LINKS_PHONE"].ToString();
						lblFax.Text=dtr["LINKS_FAX"].ToString();
						lblAddress.Text=dtr["LINKS_STREET"].ToString();
						lblCity.Text=dtr["LINKS_STATE"].ToString();
						lblState.Text=dtr["LINKS_STATE"].ToString();				
					}
				}
				// create list of biz types
				/*
				string strSQL ="";
				strSQL = "Select ";
				strSQL += " LINK_CATEGORY_ID, ";
				strSQL += " LINK_CATEGORY, ";
				strSQL += " LINK_SUBCATEGORY, ";
				strSQL += " NUM=(SELECT count(*) from LINK_CATEGORY_MEMBER WHERE MEMBER_CATEGORY = LINK_CATEGORY_ID) ";
				strSQL += " FROM LINK_CATEGORY ";
				strSQL += " WHERE MEMBER_LINK_ID = '"+Request.QueryString["ID"].ToString()+"'  ";
				strSQL += " ORDER BY LINK_SUBCATEGORY ";
			
				cmd = new SqlCommand(strSQL,conn);
				SqlDataReader dtrReader;
				dtrReader = cmd.ExecuteReader();
				rptBizType.DataSource = dtrReader;
				rptBizType.DataBind();
				dtrReader.Close();
				*/

			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ImageButton1.Click += new System.Web.UI.ImageClickEventHandler(this.ImageButton1_Click);

		}
		#endregion

		private void ImageButton1_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("/Public/Links/LinksMain.aspx");
		}
	}
}
