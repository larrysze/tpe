<%@ Page language="c#" Codebehind="Glossary.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.Glossary" MasterPageFile="~/MasterPages/Template.Master" %>
<%@ MasterType virtualPath="~/MasterPages/Template.Master"%>

<asp:Content ContentPlaceHolderID="cphJavaScript" runat="server">

    <script type="text/javascript" language="JavaScript1.2">

        //Pop-it menu- By Dynamic Drive
        //For full source code and more DHTML scripts, visit http://www.dynamicdrive.com
        //This credit MUST stay intact for use

        var linkset=new Array()
        //SPECIFY MENU SETS AND THEIR LINKS. FOLLOW SYNTAX LAID OUT

        <asp:PlaceHolder id="phActionButton"  runat="server" />
        ////No need to edit beyond here

        var ie4=document.all&&navigator.userAgent.indexOf("Opera")==-1
        var ns6=document.getElementById&&!document.all
        var ns4=document.layers

        function showdefinition(e,which){

        if (!document.all&&!document.getElementById&&!document.layers)
        return

        clearhidemenu()

        menuobj=ie4? document.all.popmenu : ns6? document.getElementById("popmenu") : ns4? document.popmenu : ""
//        menuobj.thestyle=(ie4||ns6)? menuobj.style : menuobj
        menuobj.thestyle= menuobj.style;

        if (ie4||ns6)
        menuobj.innerHTML=which
        else{
        menuobj.document.write('<layer name=gui bgColor=#E6E6E6 width=225 onmouseover="clearhidemenu()" onmouseout="hidemenu()">'+which+'</layer>')
        menuobj.document.close()
        }

        menuobj.contentwidth=(ie4||ns6)? menuobj.offsetWidth : menuobj.document.gui.document.width
        menuobj.contentheight=(ie4||ns6)? menuobj.offsetHeight : menuobj.document.gui.document.height
        eventX=ie4? event.clientX : ns6? e.clientX : e.x
        eventY=ie4? event.clientY : ns6? e.clientY : e.y

        //Find out how close the mouse is to the corner of the window
        var rightedge=ie4? document.body.clientWidth-eventX : window.innerWidth-eventX
        var bottomedge=ie4? document.body.clientHeight-eventY : window.innerHeight-eventY
        
      

        //if the horizontal distance isn't enough to accomodate the width of the context menu
        if (rightedge<menuobj.contentwidth){
            //move the horizontal position of the menu to the left by it's width
            menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX-menuobj.contentwidth : ns6? window.pageXOffset+eventX-menuobj.contentwidth + "px" : eventX-menuobj.contentwidth
        }
        else{
            //position the horizontal position of the menu where the mouse was clicked
            menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX : ns6? window.pageXOffset+eventX + "px" : eventX
        }

        //same concept with the vertical position
        var theTop = (ie4||ns6)? document.documentElement.scrollTop : document.body.scrollTop;
        if (bottomedge<menuobj.contentheight){
             menuobj.thestyle.top=ie4? theTop+eventY-menuobj.contentheight : ns6? window.pageYOffset+eventY-menuobj.contentheight + "px" : eventY-menuobj.contentheight
        }else{
            menuobj.thestyle.top=ie4? theTop+event.clientY : ns6? window.pageYOffset+eventY + "px" : eventY
        }
        menuobj.thestyle.visibility="visible"
        return false
  }

        function contains_ns6(a, b) {
        //Determines if 1 element in contained in another- by Brainjar.com
        while (b.parentNode)
        if ((b = b.parentNode) == a)
        return true;
        return false;
        }

        function hidemenu(){
        if (window.menuobj)
        menuobj.thestyle.visibility=(ie4||ns6)? "hidden" : "hide"
        }

        function dynamichide(e){
        if (ie4&&!menuobj.contains(e.toElement))
        hidemenu()
        else if (ns6&&e.currentTarget!= e.relatedTarget&& !contains_ns6(e.currentTarget, e.relatedTarget))
        hidemenu()
        }

        function delayhidemenu(){
        if (ie4||ns6||ns4)
        delayhide=setTimeout("hidemenu()",500)
        }

        function clearhidemenu(){
        if (window.delayhide)
        clearTimeout(delayhide)
        }

        function highlightmenu(e,state){
        if (document.all)
        source_el=event.srcElement
        else if (document.getElementById)
        source_el=e.target
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        else{
        while(source_el.id!="popmenu"){
        source_el=document.getElementById? source_el.parentNode : source_el.parentElement
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        }
        }
        }

        if (ie4||ns6)
        document.onclick=hidemenu
        
</script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
    <style type="text/css">
        .glossary_arrow 
        {
	        background-image: url(/Pics/arrow_litle.jpg);
	        height: 14px;
	        width: 16px;
	        padding-left: 2px;
        }

        .menu2 {
	        color: #000000;
	        text-decoration: none;
	        font-size: 11px;
	        font-family: Verdana, Arial, Helvetica, sans-serif;
	        font-weight: bold;
	        padding-left: 2px;
        }

        .menuskin 
	        { 
		        BORDER-RIGHT: black 2px solid; 
		        BORDER-TOP: black 2px solid; 
		        Z-INDEX: 100; 
		        VISIBILITY: hidden;
		        BORDER-LEFT: black 2px solid; 
		        WIDTH: 225px; 
		        BORDER-BOTTOM: black 2px solid; 
		        POSITION: absolute; 
		        BACKGROUND-COLOR: menu 
        }

         .menuskin A 
            { 
                PADDING-RIGHT: 5px; 
                PADDING-LEFT: 5px; 
                COLOR: black; 
                TEXT-DECORATION: none 
            }

        .blackcontent {
	        font-family: Verdana, Arial, Helvetica, sans-serif;
	        font-size: 11px;
	        font-style: normal;
	        line-height: normal;
	        font-weight: normal;
	        font-variant: normal;
	        color: #000000;
	        padding-left: 2px;
	        padding-right: 2px;
        }
    </style>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphInstructions">
    <table class='InstructionBullets'>
		<tr>
			<td width=35><img src='/pics/bullet.gif'></td><td>Welcome to The Plastics Exchange Glossary.</td>
		</tr>
		<tr>
			<td width=35><img src='/pics/bullet.gif'></td><td>Mouse over the Term to display the definition</td>
		</tr>
	</table>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphHeading">
    <div class="Header Bold Color1">Glossary</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">

    <div class="menuskin" id="popmenu" onmouseover="clearhidemenu();highlightmenu(event,'on')"
		onmouseout="highlightmenu(event,'off');dynamichide(event)">
	</div>		
	<table width="780" runat="server" id="tblWords" cellpadding="1">
	</table>
	<table width="780" runat="server" id="tblDefinitions">
	</table>
</asp:Content>