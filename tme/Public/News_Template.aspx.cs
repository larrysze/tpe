
using System;
using System.Data.SqlClient;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for News_Template.
	/// </summary>
	public partial class News_Template : System.Web.UI.Page
	{
	
		/******************************************************************************
		 '*   1. File Name       : Public\News_Template.aspx                          *
		 '*   2. Description     : Display news                                       *
		 '*			      Accessed from Public_News(View all news)           *
		 '*   3. Modification Log:                                                    *
		 '*     Ver No.       Date          Author             Modification           *
		 '*   -----------------------------------------------------------------       *
		 '*                                                                           *
		 '*      2.00       2-27-2003       Xiaoda               Comment              *
		 '*****************************************************************************/
		protected void Page_Load(object sender, System.EventArgs e)
		{
            try 
			{
				//Request a current news
				if (!Request.QueryString["Id"].ToString().Equals(""))
				{
					if (Request.QueryString["DIR"]!= null)
					{
						//request file path
						lbltest.Text= ReadXML(Server.MapPath("/News/"+Request.QueryString["DIR"].ToString()+"/"+ Request.QueryString["Id"].ToString()+".xml")) ;
					}
					else 
					{
						lbltest.Text= ReadXML(Server.MapPath("/News/Current/"+ Request.QueryString["Id"].ToString()+".xml")) ;
					}
					if ((Request.QueryString["SearchTerm"] != null) && (Request.QueryString["SearchTerm"].ToString().Length > 2))
					{
						lbltest.Text = HelperFunction.Highlight(Request.QueryString["SearchTerm"].ToString(),lbltest.Text);
					}
				}
			}
			catch
			{
                lbltest.Text = "<br />Sorry but the story you are looking for isn't in our records. Please click <a href=Public_News.aspx>here</a> to view our recent stories.<br /><br />";
			}
		}
		
		public string ReadXML(string UrlToXmlFile)
		{
			// this string will concatenate all the xml values
			System.Text.StringBuilder sbXML = new System.Text.StringBuilder();
			XmlTextReader reader = null;

			// load the file from the URL
			reader = new XmlTextReader(UrlToXmlFile);

			//Read news based on those XML nodes
			object oName = reader.NameTable.Add("HeadLine");
			object dateline = reader.NameTable.Add("DateLine");
			object content = reader.NameTable.Add("p");
			object copyright = reader.NameTable.Add("CopyrightLine");
			while (reader.Read())
			{
				if (reader.NodeType == XmlNodeType.Element)
				{
					//Fill the page with news content
					if (reader.Name.Equals(oName))
					{
						sbXML.Append("<u><font size=4>");
						sbXML.Append(reader.ReadString()).Append("</u></font></b><br>");
					}

					if (reader.Name.Equals(dateline))
					{
						sbXML.Append(reader.ReadString()).Append("<b>");
					}
					if (reader.Name.Equals(copyright))
					{
						sbXML.Append(reader.ReadString()).Append("</b><br><br>");
					}

					if (reader.Name.Equals(content))
					{
						if (Request.QueryString["Highlight"] != null)
						{
							sbXML.Append(Highlight(reader.ReadString())).Append("<div style='margin-bottom:5px'><br></div>");
						}
						else
						{
							sbXML.Append(reader.ReadString()).Append("<div style='margin-bottom:5px'><br></div>");
						}
					}

				}
			}

			sbXML.Append(reader.ReadString()).Append("<br><table><tr><td valign=middle>News Provided by  <span class='LinkNormal Color2'><a href='http://www.COMTEX.com'>COMTEX</a></span></td><td valign=middle></td></tr></table>"); 
			reader.Close();
			return sbXML.ToString();
		}
		protected string Highlight(string text)
		{
			HighlightRed(ref text,"plastic",true);
			HighlightRed(ref text,"resin",true);
			HighlightRed(ref text,"polyethylene",true);
			HighlightRed(ref text,"polypropylene",true);
			HighlightRed(ref text,"polystyrene",true);
			//HighlightRed(ref text," Dow ",false);
			HighlightRed(ref text,"DuPont",false);
			HighlightRed(ref text,"Dupont",false);
			HighlightRed(ref text,"Eastman",false);
			HighlightRed(ref text,"Huntsman",false);
			HighlightRed(ref text,"BASF",false);
			HighlightRed(ref text,"Bayer",false);
			HighlightRed(ref text,"monomer",true);
			HighlightRed(ref text,"Sunoco",false);
			HighlightRed(ref text,"HDPE",false);
			HighlightRed(ref text,"LDPE",false);
			HighlightRed(ref text,"LLDPE",false);
			HighlightRed(ref text,"HIPS",false);
			HighlightRed(ref text,"GPPS",false);
			HighlightRed(ref text,"Raffia",false);
			HighlightRed(ref text,"Westlake",false);
			HighlightRed(ref text,"Phillips",false);
			HighlightRed(ref text,"Sumika",false);
			HighlightRed(ref text,"Formosa",false);
			HighlightRed(ref text,"ITW",false);
			HighlightRed(ref text,"Solvay",false);
			HighlightRed(ref text,"Styrochem",false);
			HighlightRed(ref text,"Chemconnect",false);
			HighlightRed(ref text,"Omnexus",false);
			HighlightRed(ref text,"Equistar",false);
			HighlightRed(ref text,"Exxon",false);
			HighlightRed(ref text," Mobil ",false);
			HighlightRed(ref text,"Phillips",false);
			HighlightRed(ref text,"BP Amoco",false);
			HighlightRed(ref text,"Polyone",false);
			HighlightRed(ref text,"Muehlstein",false);
			HighlightRed(ref text,"Nova",false);
			HighlightRed(ref text,"benzene",true);
			HighlightRed(ref text,"ethylene",true);
			HighlightRed(ref text,"styrene",true);
			HighlightRed(ref text,"Chevron",false);
			HighlightRed(ref text,"ExxonMobil",false);
			HighlightRed(ref text,"ChevronTexaco",false);
			HighlightRed(ref text,"Wellman",false);
			HighlightRed(ref text,"Innovene",false);
			
			return text;
		}

		private void HighlightRed(ref string text, string keyword, bool bCapitalize)
		{
			text = text.Replace(keyword, "<font color='red'><b>"+keyword+"</b></font>");
			if (bCapitalize)
			{
				string strCapitalizedKeyword = keyword[0].ToString().ToUpper() + keyword.Substring(1,keyword.Length-1);
				text = text.Replace(strCapitalizedKeyword,"<font color='red'><b>" + strCapitalizedKeyword+"</b></font>");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
