<%@ Page Language="c#" CodeBehind="Careers.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.Careers" MasterPageFile="~/MasterPages/Template.Master" Title="The Plastics Exchange: Commodity Plastic Resin, Buy Resin, Sell Resin, Plastic Materials"%>
<%@ MasterType virtualPath="~/MasterPages/Template.Master"%>


<asp:Content ContentPlaceHolderID="cphCssLink" runat="server">
<style type="text/css">
.MailToLink a:link {color:#FFFFFF;}
.MailToLink a:visited {color:#FFFFFF;}
.MailToLink a:hover {color:#FFFFFF;}
.Left { text-align:left;}              
</style>
            

</asp:Content>

<asp:Content ContentPlaceHolderID="cphInstructions" runat="server">
<table class='InstructionBullets'>
<tr>
    <td width="35px"><img src='../pics/bullet.gif'/></td>
    <td>We are always looking for skilled, experienced and dedicated professionals to join our growing team and help drive corporate growth.</td>
</tr>
</table>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphHeading" runat="server">
<div class="Header Bold Color1">Careers</div>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphMain" runat="server">	
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#333333">
  <tr>
    <td>		
      <table width="778" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="506"><table width="506" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="20" background="/images/newdesign/orangebar_background.jpg" class="Header Bold Color2 Left">Current Openings</td>
              </tr>
              <tr>
                <td height="1"></td>
              </tr>
              <tr>
                <td height="120" valign="top" bgcolor="#dbdcd7" class="Content" style="text-align:justify";>
                <table width="505" height="118" border="0" cellpadding="0" cellspacing="1">
                    <tr>
                      <td>
                      <div style="padding:3px"><p>Commodity Resin Brokers to join our trading desk in Chicago to further develop our growing customer base within North America and beyond.</p><p>We offer a competitive compensation package based on experience and performance. An ideal candidate has sold HDPE, LDPE, LLDPE, HIPS, GPPS, PP to end-users from either the producer or reseller level. </p><br />
                          
                        <p>Please send cover letter, resume and salary history (where applicable) by e-mail to <span class="LinkNormal"><a href="mailto:hr@theplasticsexchange.com">HR@ThePlasticsExchange.com</a></span>, or by regular mail or fax to the side-listed information.</p></div></td>
                    </tr>
                </table></td>
              </tr>
          </table></td>
          <td width="1" bgcolor="#333333"></td>
          <td width="271" bgcolor="#999999">
          <table width="270" height="140" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td width="30" valign="middle"><img src="/images/newdesign/adress_icon.jpg" width="30" height="30"></td>
                <td width="240" valign="middle" class="Content Bold Color2" style="text-align:left">Address: 710 North Dearborn<br />
                  Chicago, Illinois 60610</td>
              </tr>
              <tr> 
                <td valign="middle"><img src="/images/newdesign/phone_icon.jpg" width="30" height="30"></td>
                <td valign="middle" class="Content Bold Color2" style="text-align:left">Phone: <%=ConfigurationSettings.AppSettings["PhoneNumber"].ToString() %> <br />
                  Fax: <%=ConfigurationSettings.AppSettings["FaxNumber"].ToString() %></td>
              </tr>
              <tr>
                <td valign="middle"><img src="/images/newdesign/email_icon.jpg" width="30" height="30"></td>
                <td valign="middle" style="text-align:left"><span class="Content Bold Color2">Email:</span><span class="LinkNormal MailToLink"><a href="mailto:hr@theplasticsexchange.com">HR@ThePlasticsExchange.com</a></span></td>
              </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
    <p></p></td>
  </tr>
</table>
</asp:Content>