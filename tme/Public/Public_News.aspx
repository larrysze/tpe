<%@ Page Language="c#" CodeBehind="Public_News.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.Public_News" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" Title="Plastic Resin News"%>
<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master"%>
<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
	<style type="text/css">
	#header-left { PADDING-RIGHT: 10px; DISPLAY: inline; PADDING-LEFT: 10px; FLOAT: left; PADDING-BOTTOM: 10px; PADDING-TOP: 10px }
	#header-right { PADDING-RIGHT: 10px; DISPLAY: inline; PADDING-LEFT: 10px; FLOAT: right; PADDING-BOTTOM: 10px; PADDING-TOP: 10px }
	#Answer {    Float: center;
	             z-index: 8;
                 height: 36px;
                 width: 650px;
                 margin: 0px 50px 0px 50px;
                 text-align: center;
                 position: relative;
                 }
	</style>
</asp:Content> 
<asp:Content runat="server" ContentPlaceHolderID="cphJavaScript">
<script type="text/javascript">
    var clientIDs = new Array(<asp:literal id='litClientIDs' runat='server'/>)

    function checkSearch()
    {
	    //return confirm("clientIDs[0].value = " + document.getElementById(clientIDs[0]).value);
	    if ((document.getElementById(clientIDs[0]).value=='') || (document.getElementById(clientIDs[0]).value == 'Start new search'))
	    {
		    return false;
	    }
	    else
	    { 
		    return true;
	    }
    }

    function checkKey()
    {
	    if (window.event.keyCode == 13) // checks whether the enter key is pressed
	    {
		    window.event.cancelBubble = true;
		    window.event.returnValue = false;
		    //__doPostBack('dg:_ctl1:btnSearch');	// doesn't work!
	    }
    }
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphHeading"><span class="Header Bold Color1">Resin News</span></asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
	<table height="1" cellspacing="0" cellpadding="0" width="780" align="center" bgcolor="#fdb400"
		border="0">
		<tr>
			<td></td>
		</tr>
	</table>
	<asp:datagrid BorderWidth="0" BackColor="#000000" CellSpacing="1" id="dg" runat="server" Width="780px" CssClass="DataGrid" AutoGenerateColumns="False" HorizontalAlign="Left" OnPageIndexChanged="dgPageChange" PageSize="30"
		PagerStyle-Mode="NumericPages" ShowFooter="True" >
		<AlternatingItemStyle CssClass="LinkNormal DarkGray" HorizontalAlign="left"></AlternatingItemStyle>
		<ItemStyle CssClass="LinkNormal LightGray" HorizontalAlign="left"></ItemStyle>
		<HeaderStyle HorizontalAlign="Center" CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
		<Columns>
			<asp:ButtonColumn Text="Edit" CommandName="Edit"></asp:ButtonColumn>
			<asp:ButtonColumn Text="Delete" CommandName="Delete"></asp:ButtonColumn>
			<asp:BoundColumn Visible="False" DataField="ID"></asp:BoundColumn>
			<asp:BoundColumn DataField="DATE" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}">
				<ItemStyle Wrap="False"></ItemStyle>
			</asp:BoundColumn>
			<asp:TemplateColumn>
				<HeaderTemplate>
					<div id="header-left">
						Select Month:
						<asp:DropDownList runat="server" id="ddlNewsDate" CssClass="InputForm" DataTextField="MonthAndYear" DataValueField="Sort" DataSource="<%#GetMonths()%>" SelectedValue="<%#GetDateSortId()%>" AutoPostBack="True" OnSelectedIndexChanged="ddlNewsDate_IndexChanged">
						</asp:DropDownList>
					</div>
					<div id="header-right">
						<asp:TextBox runat="server" name="txtSearch" id="txtSearch" CssClass="InputForm" text="<%#searchString%>" OnTextChanged="txtSearch_OnTextChanged">
						</asp:TextBox>
						<asp:Button runat="server" name="btnSearch" CssClass="Content Color2" id="btnSearch" text="Search" CommandName="Search"
							AutoPostBack="True" OnClick="btnSearch_Click"></asp:Button>
					</div>
				</HeaderTemplate>
				<ItemTemplate>
					<asp:LinkButton runat="server" ID="Linkbutton1"></asp:LinkButton>
				</ItemTemplate>
			</asp:TemplateColumn>
		</Columns>
		<PagerStyle Font-Size="Large" Font-Bold="True" ForeColor="#FF8000" CssClass="big" Mode="NumericPages"></PagerStyle>
	</asp:datagrid>
	<br />
	<br />
	<br />
	<br />
	<div id="Answer">
	<asp:Label id="lblNoRecords" runat="server" Visible="False"><span class="Content">No News Stories Found</span><br /><span class="LinkNormal"><a href="Public_News.aspx">Return</a></span></asp:Label>	
	</div>
</asp:Content>
