using System;
using System.Data.SqlClient;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using OWC11;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for News_Template.
	/// </summary>
	public class HistoricalPrices : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DropDownList ddlContracts;
		protected System.Web.UI.WebControls.RadioButtonList rbFrequency;
		protected System.Web.UI.WebControls.TextBox txtStartDay;
		protected System.Web.UI.WebControls.TextBox txtStartYear;
		protected System.Web.UI.WebControls.TextBox txtEndYear;
		protected System.Web.UI.WebControls.TextBox txtEndDay;
		protected System.Web.UI.WebControls.DropDownList ddlStartMonth;
		protected System.Web.UI.WebControls.DropDownList ddlEndMonth;
		protected System.Web.UI.WebControls.Button btnDownload;
	
		private void Page_Load(object sender, System.EventArgs e)
		{	
			if(!Page.IsPostBack)
			{	

				txtStartDay.Text = "1";
				ddlStartMonth.SelectedIndex = 0;
				txtStartYear.Text = "2004";

				txtEndDay.Text = DateTime.Today.Day.ToString();
				ddlEndMonth.SelectedIndex = DateTime.Today.Month - 1;
				txtEndYear.Text = DateTime.Today.Year.ToString();

				StringBuilder sbSQL = new StringBuilder();
				SqlDataAdapter dadContent;
				System.Data.DataTable dtContracts;
				SqlConnection conn;
				conn = new SqlConnection(Application["DBConn"].ToString());
				conn.Open();

				sbSQL.Append("Exec spContract");
				
				dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);			
				dtContracts = new DataTable();
				dadContent.Fill(dtContracts);

				ddlContracts.DataSource = dtContracts;
				ddlContracts.DataTextField= "Name";
				ddlContracts.DataValueField= "CONT_ID";
				ddlContracts.DataBind();
				
//				dg.DataSource = dtContracts;
//				dg.DataBind();
			}	
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddlStartMonth.SelectedIndexChanged += new System.EventHandler(this.ddlStartMonth_SelectedIndexChanged);
			this.rbFrequency.SelectedIndexChanged += new System.EventHandler(this.rbFrequency_SelectedIndexChanged);
			this.ddlEndMonth.SelectedIndexChanged += new System.EventHandler(this.DropDownList2_SelectedIndexChanged);
			this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnDownload_Click(object sender, System.EventArgs e)
		{	
			StringBuilder sbSQL;
			SqlDataAdapter dadContent;
			DataSet dsPrices;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();

			//Get the Id of the first row
//			int idList = (int)dg.DataKeys[0];
			int idList = Convert.ToInt16(ddlContracts.SelectedValue);

			sbSQL = new StringBuilder();
			sbSQL.Append("Exec spHistoricalPrices");
			sbSQL.Append(" @Contact = " + idList.ToString());			
			sbSQL.Append(" ,@StartDate = '" + ddlStartMonth.SelectedValue + "/" + txtStartDay.Text + "/" + txtStartYear.Text + "'");
			sbSQL.Append(" ,@EndDate = '" + ddlEndMonth.SelectedValue + "/" + txtEndDay.Text + "/" + txtEndYear.Text + "'");	
			dsPrices = new DataSet();
			dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);						
			dadContent.Fill(dsPrices);
			
			// make sure nothing is in response stream
			Response.Clear();
			Response.Charset = "";
			// set MIME type to be Excel file.
			Response.ContentType = "application/vnd.ms-excel";
			// add a header to response to force download (specifying filename)
			Response.AddHeader("Content-Disposition", "attachment; filename=\"" + ddlContracts.SelectedItem.Text + ".xls\"");

			// Send the data. Tab delimited, with newlines.
			Response.Write("Date\tPrice\n");

			for(int i=0; i<dsPrices.Tables[0].Rows.Count;i++)
			{
				if(i % Convert.ToInt16(rbFrequency.SelectedItem.Value) == 0)
					Response.Write(dsPrices.Tables[0].Rows[i]["Date"] + "\t" + dsPrices.Tables[0].Rows[i]["Price"] + "\n");
			}
			
//			Response.Write("Data 1\tData 2\tData 3\tData 4\n");
//			Response.Write("Data 1\tData 2\tData 3\tData 4\n");
//			Response.Write("Data 1\tData 2\tData 3\tData 4\n");

			// Close response stream.
			Response.End(); 


//			SpreadsheetClass XlsSheet = new SpreadsheetClass();
//			
//			StringBuilder sbSQL;
//			SqlDataAdapter dadContent;
//			DataSet dsPrices;
//			SqlConnection conn;
//			conn = new SqlConnection(Application["DBConn"].ToString());
//			conn.Open();
//
//			CheckBox cb;
//			int idList;
//			int countSheets = 1;
//			foreach(DataGridItem item in dg.Items)
//			{
//				
//				cb = (CheckBox)dg.Items[item.ItemIndex].FindControl("cbSelect");
//				if(cb.Checked)
//				{
//					idList = (int)dg.DataKeys[item.ItemIndex];
//					sbSQL = new StringBuilder();
//					sbSQL.Append("Exec spHistoricalPrices");
//					sbSQL.Append(" @Contact = " + idList.ToString());			
//					dsPrices = new DataSet();
//					dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);						
//					dadContent.Fill(dsPrices);
//					
//					for(int i=0; i<dsPrices.Tables[0].Rows.Count;i++)
//					{
//						XlsSheet.ActiveSheet.Cells[i+1,1] = dsPrices.Tables[0].Rows[i]["Date"];
//						XlsSheet.ActiveSheet.Cells[i+1,2] = dsPrices.Tables[0].Rows[i]["Price"];
//					}
//					
//					XlsSheet.Sheets.Add(Type.Missing, Type.Missing, countSheets, XlSheetType.xlWorksheet); 
//					
//					countSheets++;
//
//				}
//
//			}
//
//			conn.Close();
//			
//			idList = idList.Remove(idList.Length-1, 1);


//			XlsSheet.Export(HttpContext.Current.Server.MapPath("/test.xls"),OWC11.SheetExportActionEnum.ssExportActionNone,OWC11.SheetExportFormat.ssExportHTML);
//			HttpContext.Current.Response.ContentType = "application/x-msexcel";
//			HttpContext.Current.Response.WriteFile(HttpContext.Current.Server.MapPath("/test.xls"));
			
//		
//			Export objExport = new Export("Web");
//			objExport.ExportDetails(dtPrices, Export.ExportFormat.Excel, "EmployeesInfo2.xls");
			
//			Application MyExcel = new Excel.ApplicationClass();
//			Workbooks oWorkbooks = MyExcel.Workbooks;
//			Workbook theWorkbook = oWorkbooks[0];
//			Worksheet oSheet;
//			//file path for excel files
//			string xlsPath = HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath + "\\excelFiles\\");
//			//HttpContext.Current.Response.Write(xlsPath)
//
//			string xlsTemplateFile = xlsPath + "template.xls";
//			string xlsSaveFile = xlsPath + HttpContext.Current.Session.SessionID + ".xls";
//
//			//If file already exists, remove it
//			if(File.Exists(xlsSaveFile))
//				File.Delete(xlsSaveFile);
//			try
//			{
//				theWorkbook = oWorkbooks.Open(xlsTemplateFile,
//					0, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "",
//					true, false, 0, true, false, false);
//
//				oSheet = (Worksheet)MyExcel.ActiveSheet;
//				oSheet.Cells[3,1] = "sample data";
//				oSheet.Cells[4,3] = "more sample data";
//				//theWorkbook.SaveAs(xlsSaveFile);
//    
////				theWorkbook.Close(true, );    
//				MyExcel.Quit();
//    
////				Marshal.ReleaseComObject(oSheet);
////				Marshal.ReleaseComObject(oWorkbooks);
////				Marshal.ReleaseComObject(theWorkbook);
////				Marshal.ReleaseComObject(MyExcel);
////    
//				GC.Collect();
//				GC.WaitForPendingFinalizers();
//			}
//			catch(Exception ex)
//			{
//				throw new Exception("Error creating Excel spreadsheet:" + ex.Message.ToString());
//			}
//			finally
//			{
//				oSheet = null;
//				theWorkbook = null;
//				oWorkbooks = null;
//				MyExcel = null;
//			}

			//Delete file from server
//			if(File.Exists(xlsSaveFile))
//				File.Delete(xlsSaveFile);



//			string xml = ExcelUtil.CreateExcelFile(dsPrices);
//			Response.ContentType = "application/vnd.ms-excel";
//			Response.Charset = "";
//			Response.Write(xlsSaveFile);
//			Response.Flush();
//			Response.End();

		}

		private void rbFrequency_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void ddlStartMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void DropDownList2_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
	}
}
