<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page language="c#" Codebehind="ML_Unsubscribe.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.ML_Unsubscribe_2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
		<title>ML_Unsubscribe_2</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
  </HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TPE:Template Runat="Server" ID="Template1" NAME="Template1" />
			<TPE:Web_Box Heading="Unsubscribe" Runat="Server" ID="Web_box1" NAME="Web_box1" />
			<asp:Panel ID=pnlUnsubscribe Runat=server>
			<BR>
			<BR>
			Please type your email address in below if you would like to unsubscribe from 
			our mailing list.
			<BR>
			<BR>
<asp:TextBox id=txtMail Runat="server" maxlength="100"></asp:TextBox>
<asp:Button id=btnUnsubscribe Runat="server" Text="Unsubscribe" onclick="btnUnsubscribe_Click"></asp:Button>
			</asp:Panel>
			<br><br>
			<asp:Label ID=lblConfirmation Runat=server></asp:Label>
			<BR>
			<BR>
			<BR>
			<BR>
			<TPE:Web_Box Footer="true" Runat="Server" ID="Web_box2" NAME="Web_box2" />
			<TPE:Template Footer="true" Runat="Server" ID="Template2" NAME="Template2" />
		</form>
	</body>
</HTML>
