using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for ForwardToEditPref.
	/// </summary>
	public class ForwardToEditPref : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(!IsPostBack)
			{
				string pers_id = "";
				string pers_email = "";
				string pers_username = "";
				string pers_passw = "";
				string hrefLinkEditPreferences = "";
				if(Session["Id"] != null)
				{
					pers_id = Session["Id"].ToString();
					pers_id = Crypto.Encrypt(pers_id);
					pers_email =  Session["Email"].ToString();
					pers_username = Session["UserName"].ToString();
					pers_passw = Session["Password"].ToString();
//					pers_passw = Crypto.Encrypt(pers_passw);
					hrefLinkEditPreferences = "/Administrator/Update_Resins.aspx?UserID=" + pers_id;

				}

				if(hrefLinkEditPreferences != "")
				{
					Server.Transfer(hrefLinkEditPreferences);
				}

			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
