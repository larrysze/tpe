using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for UserResinEntryList.
	/// </summary>
	public class UserResinEntryList : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblExplanationRequest;
		protected System.Web.UI.WebControls.DataGrid dgEntries;
		protected System.Web.UI.WebControls.Label lblExplanationOffer;
		protected System.Web.UI.WebControls.Button btnNewTransaction;
		protected System.Web.UI.WebControls.Panel pnInternational;
		protected System.Web.UI.WebControls.Panel pnDomestic;
		protected System.Web.UI.WebControls.Panel PapnDomesticnel1;
		protected System.Web.UI.WebControls.LinkButton lnkInternational;
		protected System.Web.UI.WebControls.LinkButton lnkDomestic;
		protected System.Web.UI.WebControls.Label lblTitle;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if ((Session["Typ"]==null) || (Session["Typ"].ToString()=="")) 
			{
				Session["strLoginURL"] =Request.RawUrl.ToString(); 
				Response.Redirect("/common/FUNCLogin.aspx");
			}
			
			if (!IsPostBack)
			{
				if (Request.QueryString["Export"]!=null)
				{
					if (Request.QueryString["Export"].ToString()=="true")
						pnInternational.Visible = true;
					else
						pnDomestic.Visible = true;
				}
				else
				{
					if (Session["Market"].ToString()=="D")
						pnDomestic.Visible = true;
					else
						pnInternational.Visible = true;
				}

				//check if it's export transction
				if (pnInternational.Visible == true)
				{
					ViewState["Export"] =true;
				}
				else
				{
					ViewState["Export"] =false;
				}
				
				ViewState["Sort"] = "VARID DESC";
				ViewState["TransactionType"] = Request.QueryString["TransactionType"].ToString();

				BindDataGrid();
			}
		}

		public void BindDataGrid(string sortExpression)
		{
			DataView dv = (DataView)Session["SpotGrid_DataView"];
			dv.Sort = sortExpression;
			dgEntries.DataSource = dv;
			dgEntries.DataBind();
		}

		public void BindDataGrid()
		{
			SqlConnection conn;
			string strSQL;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			SqlDataAdapter dadContent;
			DataSet dstContent;

			strSQL = "Exec spSpot_Floor ";

				if (ViewState["TransactionType"].ToString()=="Offers")
			{
				strSQL +="@UserType='S'";
				lblExplanationRequest.Visible = false;
				lblTitle.Text = "Current Plastic Offers";
				btnNewTransaction.Text = "Make a new Offer";
			}
			else
			{
				strSQL +="@UserType='P'";
				lblExplanationOffer.Visible = false;
				lblTitle.Text = "Current Plastic Requests";
				btnNewTransaction.Text = "Make a new Request";
			}

			// flag to be set if this is one the import/export
			if ((bool)ViewState["Export"])
			{
				strSQL +=" ,@Export='true'";
			}
			
			strSQL +=",@User='"+Session["Id"].ToString()+"'";

			// add the sort command
			strSQL +=",@Sort='"+ViewState["Sort"].ToString()+"'";
			//Response.Write(strSQL);
			dadContent = new SqlDataAdapter(strSQL,conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			
			DataTable dtContent = dstContent.Tables[0];
			DataView dtView = new DataView(dtContent);
			Session["SpotGrid_DataView"] = dtView.Table.DefaultView;
			dgEntries.DataSource = Session["SpotGrid_DataView"];
			dgEntries.DataBind();
			conn.Close();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnNewTransaction.Click += new System.EventHandler(this.btnNewTransaction_Click);
			this.dgEntries.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgEntries_ItemCommand);
			this.dgEntries.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgEntries_SortCommand);
			this.dgEntries.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgEntries_ItemDataBound);
			this.dgEntries.SelectedIndexChanged += new System.EventHandler(this.dgEntries_SelectedIndexChanged);
			this.lnkInternational.Click += new System.EventHandler(this.lnkInternational_Click);
			this.lnkDomestic.Click += new System.EventHandler(this.lnkDomestic_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void dgEntries_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName == "Cancel")
			{
				SqlConnection conn;
				conn = new SqlConnection(Application["DBConn"].ToString());
				conn.Open();
				SqlCommand cmdDelete;
				if (ViewState["TransactionType"].ToString()=="Offers")
				{
					cmdDelete= new SqlCommand("DELETE FROM BBOFFER WHERE OFFR_ID='"+ e.Item.Cells[2].Text.ToString()+"'", conn);
					cmdDelete.ExecuteNonQuery();
				}
				else
				{
					cmdDelete= new SqlCommand("DELETE FROM BBID WHERE BID_ID='"+ e.Item.Cells[2].Text.ToString()+"'", conn);
					cmdDelete.ExecuteNonQuery();
				}
				conn.Close();
				BindDataGrid();
			}
			else if (e.CommandName == "Edit")
			{
				string market = "Domestic";
				if ((bool)ViewState["Export"]) market = "International";

				if (ViewState["TransactionType"].ToString()=="Offers")
				{
					Response.Redirect("/spot/Resin_Entry.aspx?type=offer&Change=" + e.Item.Cells[2].Text.ToString() + "&Market=" + market.ToString());
				}
				else
				{
					Response.Redirect("/spot/Resin_Entry.aspx?type=bid&Change=" + e.Item.Cells[2].Text.ToString() + "&Market=" + market.ToString());
				}
			}
		}
		
		double dbTotalWeight = 0.0;
		private void dgEntries_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				// setting the mouseover color
				e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver';"); //this.style.cursor='hand';

				string density = e.Item.Cells[6].Text;
				if ((density.Length>2) && (density.Substring(0,2) == "0.")) e.Item.Cells[6].Text = density.Substring(1);

				if (e.Item.ItemType == ListItemType.Item)
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee';"); //this.style.cursor='pointer';
				}
				else
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3';"); //;this.style.cursor='pointer'
				}

				if ((bool)ViewState["Export"])
				{
					dbTotalWeight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSIZE"));
				}
				else
				{
					dbTotalWeight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARWGHT"));
				}

				if ((bool)ViewState["Export"])
				{
					e.Item.Cells[4].Text = String.Format("{0:#,###}", (DataBinder.Eval(e.Item.DataItem, "VARSIZE")));
					e.Item.Cells[9].Text = "$" + String.Format("{0:#,###}", (DataBinder.Eval(e.Item.DataItem, "VARPRICE")));
				}
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[1].Text ="<b>Total</b>";
				if ((bool)ViewState["Export"])
				{
					e.Item.Cells[4].Text = "<b> " + String.Format("{0:#,###}", dbTotalWeight)+" Metric Tons</b>";
				}
				else
				{
					e.Item.Cells[4].Text = "<b> " + String.Format("{0:#,###}", dbTotalWeight)+" lbs</b>";
				}
			}
			else if (e.Item.ItemType == ListItemType.Header)
			{
				if ((bool)ViewState["Export"]) dgEntries.Columns[4].HeaderText ="Metric Tons";
			}
		}

		private void dgEntries_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;
			//  Parse the sort expression - delimiter space
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
			ColumnToSort = SortExprs[0];
			// If a sort order is specified get it, else default is descending
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DESC";
			}
			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;
			// Figure out the column index
			int iIndex;
			iIndex = 1;
			switch(ColumnToSort.ToUpper())
			{
				case "VARID":
					iIndex = 2;
					break;
				case "VARCONTRACT":
					iIndex = 3;
					break;
				case "VARSIZE":
					iIndex = 4;
					break;
				case "VARMELT":
					iIndex = 5;
					break;
				case "VARDENS":
					iIndex = 6;
					break;
				case "VARADDS":
					iIndex = 7;
					break;
				case "VARTERM":
					iIndex = 8;
					break;
				case "VARPRICE":
					iIndex = 9;
					break;
				case "VAREXPR":
					iIndex = 10;
					break;
			}
			// alter the column's sort expression
			dgEntries.Columns[iIndex].SortExpression = NewSortExpr;
//			if (NewSearchMode.Equals("DESC"))
//			{
//				dgEntries.Columns[iIndex].HeaderText = "<img border=\"0\" src=\"/pics/icons/icon_down_arrow.gif\">" + dgEntries.Columns[iIndex].HeaderText;
//			}
//			else
//			{
//				dgEntries.Columns[iIndex].HeaderText =  "<img border=\"0\" src=\"/pics/icons/icon_up_arrow.gif\">" + dgEntries.Columns[iIndex].HeaderText;
//			}
			dgEntries.CurrentPageIndex = 0;

			// Sort the data in new order
			ViewState["Sort"] = NewSortExpr;
			// reset dbase page to the first one whether the sorting changes
			BindDataGrid(NewSortExpr);
		}

		private void dgEntries_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void btnNewTransaction_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("/spot/Resin_Entry.aspx?Market=" + (Convert.ToBoolean(ViewState["Export"].ToString()) ? "International" : "Domestic"));
		}

		private void lnkInternational_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("/Public/UserResinEntryList.aspx?TransactionType=" + ViewState["TransactionType"].ToString() + "&Export=true");
		}

		private void lnkDomestic_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("/Public/UserResinEntryList.aspx?TransactionType=" + ViewState["TransactionType"].ToString() + "&Export=false");
		}
	}
}
