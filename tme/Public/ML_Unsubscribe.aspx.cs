using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for ML_Unsubscribe_2.
	/// </summary>
	public partial class ML_Unsubscribe_2 : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnUnsubscribe_Click(object sender, System.EventArgs e)
		{
			string status = "Please type your email to unsubscribe";
			if(txtMail.Text != "")
			{
				
				Hashtable param = new Hashtable();
				param.Add("@VALUE", HelperFunction.RemoveQuotationMarks(txtMail.Text));
				string sqlStr = "SELECT PERS_MAIL FROM PERSON WHERE PERS_MAIL=@VALUE";
				

				DataTable dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), sqlStr, param);
				if(dt.Rows.Count != 0)
				{
					sqlStr = "UPDATE PERSON SET Email_ENBL=0 WHERE PERS_MAIL=@PARAM1";
					param.Clear();
					param.Add("@PARAM1", HelperFunction.RemoveQuotationMarks(txtMail.Text));
					DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sqlStr, param);
					status = "<center>Your request has been proceesed.  You will no longer receive emails from the The Plastics Exchange<br>Thank you.</center>";
					txtMail.Text = "";
//					pnlUnsubscribe.Visible = false;
				}
				else
				{
					status = "Email wasn't found in our Database";
				}
			}

			lblConfirmation.Text = status;
		}
	}
}
