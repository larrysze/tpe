using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Data.SqlClient;
using System.Text;
using TPE.Utility;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for Registration.
	/// </summary>
	public partial class Registration : System.Web.UI.Page
	{
		/*****************************************************************************
	'*   1. File Name       : Public\Registration.aspx                           *
	'*   2. Description     : Demo user registration                             *
	'*			                                                                *
	'*   3. Modification Log:                                                    *
	'*     Ver No.       Date          Author             Modification           *
	'*   -----------------------------------------------------------------       *
	'*                                                                           *
	'*      2.00       2-27-2003       Xiaoda               Comment              *
	'*****************************************************************************/

		protected void Page_Load(object sender, EventArgs e)
		{
            if ( (Application["IsDebug"].ToString() != "True" ) && !Request.IsSecureConnection)
            {
                Response.Redirect("https://" + ConfigurationSettings.AppSettings["SecureDomainName"].ToString() + Request.Path);
            }
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			//btnCancel.Attributes.Add("OnClick", "window.history.back();");

            Master.Heading = "Register";


			if (!IsPostBack)
			{


				ImageButton2.Attributes.Add("onMouseOver", "document.getElementById('" + ImageButton2.ClientID + "').src ='/images2/register/register_pressed.jpg';");
                ImageButton2.Attributes.Add("onMouseOut", "document.getElementById('" + ImageButton2.ClientID + "').src ='/images2/register/register.jpg';");
                chkQuality1.Attributes.Add("OnClick", "changeQuality(1)");
                chkQuality2.Attributes.Add("OnClick", "changeQuality(2)");
                chkQuality3.Attributes.Add("OnClick", "changeQuality(3)");

				loadFields();

				if (Request.UrlReferrer != null) Session["Referrer"] = Request.UrlReferrer.ToString();

				
				if (Request.QueryString["Referrer"] != null)
				{
					// store the link from page in the viewstate
					// it will be used by the cancel button
					ViewState["Referrer"] = Request.QueryString["Referrer"];
				}
				
				lblMarketUpdateMessage.Text = "";
				if (Request.QueryString["Email"] != null)
				{
					if(!HelperFunction.IsEmail(Request.QueryString["Email"]))
					{
						lblMarketUpdateMessage.Text = "invalid email address";
						return;
					}

					//Check if the email is in the DB
					Hashtable param = new Hashtable();
					
					string strSql = "select MAI_MAIL FROM mailing WHERE MAI_MAIL = @Email";
					param.Add("@Email",Request.QueryString["Email"]);
					DataTable dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(),strSql,param);

					if (dt.Rows.Count == 0)
					{
						string strSqlInsert = "INSERT INTO mailing (MAI_MAIL, MAI_CODE) VALUES (@Email, '0')";
						DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlInsert, param);						
					} 
					else 
					{
						// already in db
						lblMarketUpdateMessage.Text = Request.QueryString["Email"] + " is already in our database";
						return;
					}
				
					// save in email into ViewState
					ViewState["Email"] = Request.QueryString["Email"];
					lblMarketUpdateMessage.Text  ="<BR>Thank you! Your email address (<a href=\""+ViewState["Email"].ToString()+"\">"+ViewState["Email"].ToString()+"</a>) has been added to our market update distribution list. <BR><BR> ";
					lblMarketUpdateMessage.Text +="If you would also like immediate access to our spot offers and forward pricing, please select the market below and fill out the form.";
					// setup command for the adding the address into the email buffer table
					// demo registrations are added at a different spot
					
//
//					//insert into EMAIL_BUFFER table, a VB script will be executed every night on GRANT
//					//to move email to MIALING talbe in out Intranet database
//					cmdAddtoBuffer= new SqlCommand("INSERT INTO EMAIL_BUFFER (Email,Is_Add) VALUES ('"+ViewState["Email"].ToString()+"','1')", conn);
//					cmdAddtoBuffer.ExecuteNonQuery();

					
					
//					I don't know why but it does't work					
//					Hashtable ht = new Hashtable(); 
//					ht.Add("@Email",ViewState["Email"].ToString());
//					ht.Add("@Code","0");
//					DatabaseLibrary.DBLib.ExecuteSQLStatement(this.Context, "INSERT INTO mailing (MAI_MAIL, MAI_CODE) VALUES (@Email,@Code)", ht);

					// email mike the email address
					MailMessage EmailSignUp;
					EmailSignUp = new MailMessage();
					EmailSignUp.From = Request.QueryString["Email"].ToString();
					EmailSignUp.To = Application["strEmailOwner"].ToString();
					// CC the techie
					EmailSignUp.Cc = Application["strEmailAdmin"].ToString();
					EmailSignUp.Subject = "EmailSignUp";
					if (Session["Referrer"] != null)
					{
						EmailSignUp.Body = "<br><b><font color=red>Referrer: "+Session["Referrer"].ToString() +"</font></b>";
					}
					else
					{
						EmailSignUp.Body = "";
					}


					EmailSignUp.BodyFormat = MailFormat.Html;
					if ((EmailSignUp.From.ToString().IndexOf(" ",0)<0) && (EmailSignUp.From.ToString().IndexOf("@",0)>=0))
						TPE.Utility.EmailLibrary.Send(Context,EmailSignUp);
					// autopopulate the field
					txtEmail.Text =Request.QueryString["Email"].ToString();

					StringBuilder sbEmailText = new StringBuilder();

					// create text for mail to be sent to user

					// sbEmailText.Append("<HTML><HEAD><TITLE>TPE</TITLE>");
					// sbEmailText.Append("<BODY>");
					sbEmailText.Append("<table border=0 cellspacing=0 cellpadding=0>");
					sbEmailText.Append("<tr><td><img src=http://" + ConfigurationSettings.AppSettings["DomainName"] + "/images/email/tpelogo.gif></td>");
					sbEmailText.Append("<td>");
					sbEmailText.Append("<font face=arial black size=4 color=Black>The</font><font face=arial black size=4 color='#4A4AFE'>Plastics</font><font face=arial black size=4 color=Black>Exchange</font><font face=arial black size=4 color='#4A4AFE'>.</font><font face=arial black size=4 color=Black>com</font>");
					sbEmailText.Append("</td>");
					sbEmailText.Append("</tr></table>");
					sbEmailText.Append("<br>");

					sbEmailText.Append("Thank you. Your email has been added to our market research list. We are all about resin and we know...price matters!<BR><BR> ");
					sbEmailText.Append("We are interested in working with you and hope you will find <a href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "\">The Plastics Exchange</a> to be your one stop for research, trading tools and your resin needs.<BR><BR>");
                    sbEmailText.Append("If you have any questions, please feel free to contact us at " + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + " or reply to this email.<BR><BR>");
					sbEmailText.Append("Our services include:");
					sbEmailText.Append("<UL><LI>Custom forward pricing analysis.</LI>");
					sbEmailText.Append("<LI>Spot market research and news.</LI>");
					sbEmailText.Append("<LI>Access to prime and wide spec resin.</LI>");
					sbEmailText.Append("<LI>Domestic and international resin.</LI></UL>");
					sbEmailText.Append("You will be receiving our market research on a regular basis.  If you would also like to receive information regarding our other tools, including spot offers and forward pricing, please click <a href='http://" + ConfigurationSettings.AppSettings["DomainName"] + "/Public/Registration.aspx'>here</a> to register for access to the exchange.<BR><BR>");
					sbEmailText.Append("Sincerely,<BR><BR>");
					sbEmailText.Append("Michael A. Greenberg, CEO<BR>");
					sbEmailText.Append("The Plastics Exchange<BR>");
					sbEmailText.Append("Tel: " + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + "<BR>");
					sbEmailText.Append("Fax: " + ConfigurationSettings.AppSettings["FaxNumber"].ToString() + "<BR>");

					// mail it out
					EmailSignUp = new MailMessage();
					EmailSignUp.From = Application["strEmailOwner"].ToString();
					EmailSignUp.To = ViewState["Email"].ToString() ;
					EmailSignUp.Subject = "Thank you for signing up" ;
					EmailSignUp.Body = sbEmailText.ToString();
					EmailSignUp.BodyFormat = MailFormat.Html;

					try
					{	
						TPE.Utility.EmailLibrary.Send(this.Context,EmailSignUp);
					}
					catch(Exception ex)
					{
						lblMarketUpdateMessage.Text = "sorry wrong email address";
						
					}

				}
				//				else
				//				{
//				lblHeading.Text = "<BR>Please setup my account which will give me full access to spot resin offers and market intelligence.";
				//				}
								
				DataTable dtPort;
				string strSql2 = "select PORT_ID, PORT= PORT_CITY +', ' + (SELECT CTRY_NAME FROM COUNTRY WHERE CTRY_CODE=PORT_COUNTRY)from INTERNATIONAL_PORT ORDER BY PORT";
				
				dtPort = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), strSql2);

				// build the port drop down
				ddlPort.Items.Add(new ListItem ("Select Port","*"));		
				foreach (DataRow row in dtPort.Rows)
				{					
					ddlPort.Items.Add(new ListItem (row["PORT"].ToString(),row["PORT_ID"].ToString()));
				}
				
				//Load Zipcodes
				HelperFunctionDistances hfd = new HelperFunctionDistances();
				hfd.loadMostPopularCities(ddlZipCode, "");
			}
			CheckBox CB;
			SqlDataReader dtrHDPE;
			SqlCommand cmdHDPE;
			string clientScript = "";

			if(conn.State == ConnectionState.Closed)
				conn.Open();

			clientScript = "function HDPE_Click(){";
			cmdHDPE= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_CATG='HDPE'  OR CONT_CATG='HMWPE' ORDER BY CONT_LABL", conn);
			dtrHDPE= cmdHDPE.ExecuteReader();
			while (dtrHDPE.Read())
			{
				CB= new CheckBox();
				// Set the label's Text and ID properties.
				CB.ID = "CB_" + dtrHDPE["CONT_ID"].ToString() ;
				CB.Text= dtrHDPE["CONT_LABL"].ToString();
				CB.Checked =false;
                pnHDPE.Controls.Add(new LiteralControl("<br>"));
                pnHDPE.Controls.Add(CB);
				// Add a spacer in the form of an HTML <BR> element.
                clientScript += " document.getElementById('" + CB.ClientID + "').checked = document.getElementById('chkHDPE').checked;";
			}
            pnHDPE.Controls.Add(new LiteralControl("<br>"));
            dtrHDPE.Close();
			clientScript+=" } ";
			RegisterClientScriptBlock("HDPE","<script language='JavaScript'>" + clientScript + "</script>");

			SqlDataReader dtrLDPE;
			SqlCommand cmdLDPE;
			clientScript = "function LDPE_Click(){";
			cmdLDPE= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_CATG='LDPE' ORDER BY CONT_LABL", conn);
			dtrLDPE= cmdLDPE.ExecuteReader();
			while (dtrLDPE.Read())
			{
				CB= new CheckBox();
				// Set the label's Text and ID properties.
				CB.ID = "CB_" + dtrLDPE["CONT_ID"].ToString() ;
				CB.Text= dtrLDPE["CONT_LABL"].ToString();
				CB.Checked =false;
                pnLDPE.Controls.Add(new LiteralControl("<br>"));
                pnLDPE.Controls.Add(CB);
				// Add a spacer in the form of an HTML <BR> element.
                clientScript += " document.getElementById('" + CB.ClientID + "').checked = document.getElementById('chkLDPE').checked;";
			}
            pnLDPE.Controls.Add(new LiteralControl("<br>"));
            dtrLDPE.Close();
			clientScript+=" } ";
			RegisterClientScriptBlock("LDPE","<script language='JavaScript'>" + clientScript + "</script>");


			SqlDataReader dtrLLDPE;
			SqlCommand cmdLLDPE;
			clientScript = "function LLDPE_Click(){";
			cmdLLDPE= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_CATG='LLDPE' ORDER BY CONT_LABL", conn);
			dtrLLDPE= cmdLLDPE.ExecuteReader();
			while (dtrLLDPE.Read())
			{
				CB= new CheckBox();
				// Set the label's Text and ID properties.
				CB.ID = "CB_" + dtrLLDPE["CONT_ID"].ToString() ;
				CB.Text= dtrLLDPE["CONT_LABL"].ToString();
				CB.Checked =false;
                pnLLDPE.Controls.Add(new LiteralControl("<br>"));
                pnLLDPE.Controls.Add(CB);
				// Add a spacer in the form of an HTML <BR> element.
                clientScript += " document.getElementById('" + CB.ClientID + "').checked = document.getElementById('chkLLDPE').checked;";
			}
            pnLLDPE.Controls.Add(new LiteralControl("<br>"));
            dtrLLDPE.Close();
			clientScript+=" } ";
			RegisterClientScriptBlock("LLDPE","<script language='JavaScript'>" + clientScript + "</script>");


			SqlDataReader dtrPS;
			SqlCommand cmdPS;
			clientScript = "function PS_Click(){";
			cmdPS= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_CATG='GPPS' OR CONT_CATG='HIPS'  ORDER BY CONT_LABL", conn);
			dtrPS= cmdPS.ExecuteReader();
			while (dtrPS.Read())
			{
				CB= new CheckBox();
				// Set the label's Text and ID properties.
				CB.ID = "CB_" + dtrPS["CONT_ID"].ToString() ;
				CB.Text= dtrPS["CONT_LABL"].ToString();
				CB.Checked =false;
                pnPS.Controls.Add(new LiteralControl("<br>"));
                pnPS.Controls.Add(CB);
				// Add a spacer in the form of an HTML <BR> element.
                clientScript += " document.getElementById('" + CB.ClientID + "').checked = document.getElementById('chkPS').checked;";
			}
            pnPS.Controls.Add(new LiteralControl("<br>"));

			dtrPS.Close();
			clientScript+=" } ";
			RegisterClientScriptBlock("PS","<script language='JavaScript'>" + clientScript + "</script>");


			SqlDataReader dtrPP;
			SqlCommand cmdPP;
			clientScript = "function PP_Click(){";
			cmdPP= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_CATG='HoPP' OR CONT_CATG='CoPP' ORDER BY CONT_LABL", conn);
			dtrPP= cmdPP.ExecuteReader();
			while (dtrPP.Read())
			{
				CB= new CheckBox();
				// Set the label's Text and ID properties.
				CB.ID = "CB_" + dtrPP["CONT_ID"].ToString() ;
				CB.Text= dtrPP["CONT_LABL"].ToString();
				CB.Checked =false;
                // Add a spacer in the form of an HTML <BR> element.
                pnPP.Controls.Add(new LiteralControl("<br>"));
                pnPP.Controls.Add(CB);
                clientScript += " document.getElementById('" + CB.ClientID + "').checked = document.getElementById('chkPP').checked;";
			}
            pnPP.Controls.Add(new LiteralControl("<br>"));
            dtrPP.Close();
			conn.Close();
			clientScript+=" } ";
			RegisterClientScriptBlock("PP","<script language='JavaScript'>" + clientScript + "</script>");

		}


		void loadFields()
		{
			if( (Session["saved_info"] != null) && (Session["saved_info"].ToString() == "true") )
			{
				txtFname.Text = Session["svd_name"].ToString();
				txtCname.Text = Session["svd_company"].ToString();
				txtPhone.Text = Session["svd_phone"].ToString();
				txtEmail.Text = Session["svd_email"].ToString();

				Session["saved_info"] = null;
				Session["svd_company"] = null;
				Session["svd_name"] = null;
				Session["svd_phone"] = null;
				Session["svd_email"] = null;
			}



		}

        protected string getMarket()
        {
            return this.Market.Value;
        }

		// <summary>
		//  Checks to see if the Username is available.  
		// </summary>
		protected void IsDuplicate(object source, ServerValidateEventArgs args)
		{
		
			
			// now check genius
			SqlConnection conn;
			SqlCommand cmd;
			SqlDataReader dtr;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			cmd= new SqlCommand("SELECT PERS_LOGN FROM PERSON WHERE PERS_MAIL='"+txtEmail.Text+"'", conn);
			dtr = cmd.ExecuteReader();
			if (dtr.HasRows)
			{
				// found one
				args.IsValid = false;
			}
			conn.Close();
		}
		// <summary>
		//  If it is a valid US number then it Converts the phone number to (xxx) xxx-xxxx format
		//  This is very important as the algo for matching states to demo is determined by the phone number
		// </summary>
		protected string ParsePhone(string TextIN)
		{
			string strTemp;
			strTemp = TextIN;
			foreach(char c in TextIN) 
			{
				if(!Char.IsNumber(c)) 
				{
					strTemp = strTemp.Replace(c.ToString(),"");
				}
			}
			strTemp.Trim();
			if (strTemp.Length == 10)
			{
				try
				{
					strTemp = "("+strTemp.Substring(0,3)+") " +strTemp.Substring(3,3)+"-"+strTemp.Substring(6,4)  ;
					return strTemp;
				}
				catch
				{
					return TextIN;
				}
			}
			else
			{
				return TextIN;
			}
			
    
		}


		// <summary>
		// Removes bad charachters that would crap out out the db insert
		// </summary>
		protected string Parse(string TextIN)
		{
			//  remove ' character
			string Parse;
			Parse = TextIN;
			while (Parse.IndexOf("'") > 0)
			{
				Parse = Parse.Replace("'", "");
			}
			// remove " character
			while (Parse.IndexOf("\"") > 0)
			{
				Parse = Parse.Replace("\"", "");
			}
			return Parse;

		}

		// <summary>
		// simple function to set all of the panels to nothing
		// </summary>
		protected void MakePanelsInvisible()
		{
			pnHDPE.Visible = false;
			pnLDPE.Visible = false;
			pnLLDPE.Visible = false;
			pnPS.Visible = false;
			pnPP.Visible = false;
			//			pnlJobFunction.Visible = tr;

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.cvQuality.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(this.cvQuality_ServerValidate);
//			this.ImageButton1.Click += new System.Web.UI.ImageClickEventHandler(this.btnBack_Click);
			this.ImageButton2.Click += new System.Web.UI.ImageClickEventHandler(this.btnSave_Click);

		}
		#endregion

		private void RadioButton1_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private string Bit(bool condition)
		{
			if (condition)
				return "1";
			else
				return "0";
		}

	


		private void btnSave_Click(object sender, System.EventArgs e)
		{
			if (Page.IsValid)
			{
				string strP="";
				SqlConnection conn;
				SqlConnection conndemo;
				
				//Check if the email is in the DB
				Hashtable param = new Hashtable();
					
				string strSql = "select MAI_MAIL FROM mailing WHERE MAI_MAIL = @Email";
				param.Add("@Email",txtEmail.Text);
				DataTable dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(),strSql,param);

				if (dt.Rows.Count == 0)
				{
					string strSqlInsert = "INSERT INTO mailing (MAI_MAIL, MAI_CODE) VALUES (@Email, '0')";
					DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlInsert, param);						
				}
								
				// Add new item in db
				conn = new SqlConnection(Application["DBConn"].ToString());
				conndemo = new SqlConnection(Application["DBConn"].ToString());	
				string cryptedPwd = "";
				try 
				{
					conn.Open();

					conndemo.Open();

					int iPref = 0;
					SqlDataReader dtrHDPE;
					SqlCommand cmdHDPE;
					cmdHDPE= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_CATG='HDPE'  OR CONT_CATG='HMWPE'", conn);
					dtrHDPE= cmdHDPE.ExecuteReader();
					while (dtrHDPE.Read())
					{
						CheckBox tempCB = (CheckBox)FindControl("CB_" + dtrHDPE["CONT_ID"].ToString()); // find the appropriate checkbox
						if(tempCB.Checked)
						{
							iPref += (int)Math.Pow(2,(Convert.ToInt32(dtrHDPE["CONT_ID"])-1)); // checked box found! add to total
						}
					}
					dtrHDPE.Close();
					
					SqlDataReader dtrLDPE;
					SqlCommand cmdLDPE;
					cmdLDPE= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_CATG='LDPE'", conn);
					dtrLDPE= cmdLDPE.ExecuteReader();
					while (dtrLDPE.Read())
					{
						CheckBox tempCB = (CheckBox)FindControl("CB_" + dtrLDPE["CONT_ID"].ToString()); // find the appropriate checkbox
						if(tempCB.Checked)
						{
							iPref += (int)Math.Pow(2,(Convert.ToInt32(dtrLDPE["CONT_ID"])-1)); // checked box found! add to total
						}
					}
					dtrLDPE.Close();

					SqlDataReader dtrLLDPE;
					SqlCommand cmdLLDPE;
					cmdLLDPE= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_CATG='LLDPE'", conn);
					dtrLLDPE= cmdLLDPE.ExecuteReader();
					while (dtrLLDPE.Read())
					{
						CheckBox tempCB = (CheckBox)FindControl("CB_" + dtrLLDPE["CONT_ID"].ToString()); // find the appropriate checkbox
						if(tempCB.Checked)
						{
							iPref += (int)Math.Pow(2,(Convert.ToInt32(dtrLLDPE["CONT_ID"])-1)); // checked box found! add to total
						}
					}
					dtrLLDPE.Close();
					
					SqlDataReader dtrPS;
					SqlCommand cmdPS;
					cmdPS= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_CATG='GPPS' OR CONT_CATG='HIPS'", conn);
					dtrPS= cmdPS.ExecuteReader();
					while (dtrPS.Read())
					{
						CheckBox tempCB = (CheckBox)FindControl("CB_" + dtrPS["CONT_ID"].ToString()); // find the appropriate checkbox
						if(tempCB.Checked)
						{
							iPref += (int)Math.Pow(2,(Convert.ToInt32(dtrPS["CONT_ID"])-1)); // checked box found! add to total
						}
					}
					dtrPS.Close();
					
					SqlDataReader dtrPP;
					SqlCommand cmdPP;
					cmdPP= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_CATG='HoPP' OR CONT_CATG='CoPP'", conn);
					dtrPP= cmdPP.ExecuteReader();
					while (dtrPP.Read())
					{
						CheckBox tempCB = (CheckBox)FindControl("CB_" + dtrPP["CONT_ID"].ToString()); // find the appropriate checkbox
						if(tempCB.Checked)
						{
							iPref += (int)Math.Pow(2.0,(Convert.ToInt32(dtrPP["CONT_ID"])-1)); // checked box found! add to total
						}
					}
					dtrPP.Close();

					cryptedPwd = Crypto.Encrypt(txtPassword.Text);
					
					string SQL1;
					string SQL2;
					SQL1 ="INSERT INTO PERSON(PERS_ACCOUNT_DISABLED,PERS_ACES,PERS_Stat,PERS_FRST_NAME,PERS_LAST_NAME,COMP_NAME,PERS_ENBL,PERS_TYPE,PERS_PSWD,PERS_MAIL,PERS_PHON,PERS_PREF,Email_ENBL";
					SQL2=" Values('False','-1','0',@Fname, @Lname, @Cname,'1','D', @Password, @Email,@Phone";

					Hashtable ht = new Hashtable();
					ht.Add("@Fname",Parse(txtFname.Text));
					ht.Add("@Lname",Parse(txtLname.Text));
					ht.Add("@Cname",Parse(txtCname.Text));
					ht.Add("@Password",cryptedPwd);
					ht.Add("@Email",Parse(txtEmail.Text));
					ht.Add("@Phone",ParsePhone(txtPhone.Text));


					//SQL1 ="INSERT INTO PERSON(PERS_FRST_NAME,PERS_LAST_NAME,PERS_COMP_NAME,PERS_ENBL,PERS_TYPE,PERS_PSWD,PERS_MAIL,PERS_PHON,PERS_PREF,Email_ENBL";
					//SQL2=" Values('"+Parse(txtFname.Text)+"','"+Parse(txtLname.Text)+"','"+Parse(txtCname.Text)+"','1','P','"+Parse(txtPassword.Text)+"','"+Parse(txtEmail.Text)+"','"+Parse(txtPhone.Text);
					if (iPref > 0)
					{
						// preference have been selected
						//SQL2 += "','"+iPref.ToString()+"','1'";
						SQL2 += ",@Pref,'1'";
						ht.Add("@Pref",iPref.ToString());
					}
					else
					{
						//SQL2 += "','"+iPref.ToString()+"','0'";
						SQL2 += ",@Pref,'0'";
						ht.Add("@Pref",iPref.ToString());
					}

					Session["Preferences"] = iPref.ToString();
					// add appropriate ports
					if (ddlPort.SelectedValue.ToString() != "*") 
					{
						SQL1+=",PERS_PORT";
						//SQL2+=",'"+Parse(ddlPort.SelectedValue.ToString())+"'";
						SQL2+=",@Port";
						ht.Add("@Port",Parse(ddlPort.SelectedValue.ToString()));
					}

					// add Job function
					if (ddlJobFunction.SelectedValue.ToString() != "Please Select") 
					{
						SQL1+=",PERS_CATEGORY";
						SQL2+=",@Category";
						ht.Add("@Category",Parse(ddlJobFunction.SelectedItem.ToString()));
					}

						string ZipCode = "";
						if (txtZipCode.Text.Trim()!="") ZipCode = txtZipCode.Text.Trim();
						if (ddlZipCode.SelectedIndex!=0) ZipCode = ddlZipCode.SelectedItem.Value;
		
						if (ZipCode!="")
						{
							SQL1+=",PERS_ZIP";
							//						SQL2+=",'"+Parse(ZipCode)+"'";
							SQL2+=",@ZipCode";
							ht.Add("@ZipCode", Parse(ZipCode));
						}

					// email somebody to let them know that an additional port has been added
					if (txtOtherPort.Text != "") 
					{
						EmailAlertNewPort();

					}
					if (txtTitle.Text != "")
					{
						SQL1+=",PERS_TITL";
						//SQL2+=",'"+Parse(txtTitle.Text)+"'";
						SQL2+=",@PersTitl";
						ht.Add("@PersTitl", Parse(txtTitle.Text));
					}			
					
					SQL1+=",PERS_PRMLY_INTRST";
					if (getMarket() == "D")
					{
						SQL2+=",'1'";
					}
					else
					{
						SQL2+=",'2'";
					}

					SQL1+=",PERS_INTRST_SIZE, PERS_INTRST_QUALT";
					if ((Bit(chkSize1.Checked)+ Bit(chkSize2.Checked)+ Bit(chkSize3.Checked)) == "000")
						SQL2+=",'111'";
					else
						SQL2+=",'" + Bit(chkSize1.Checked)+ Bit(chkSize2.Checked)+ Bit(chkSize3.Checked) + "'";
					
					if ((Bit((chkQuality1.Checked) || (getMarket()=="I"))+ Bit(chkQuality2.Checked)) == "00")
//					if ((Bit((chkQuality1.Checked) || (txtMarket.Text=="I"))+ Bit(chkQuality2.Checked)+ Bit(chkQuality3.Checked)) == "000")
						SQL2+=",'111'";
					else
						SQL2+=",'" + Bit((chkQuality1.Checked) || (getMarket()=="I"))+ Bit(chkQuality2.Checked) + "'";
//					SQL2+=",'" + Bit((chkQuality1.Checked) || (txtMarket.Text=="I"))+ Bit(chkQuality2.Checked)+ Bit(chkQuality3.Checked) + "'";

					SQL1 += ") " + SQL2 + ") SELECT @@IDENTITY AS NewID" ;

					Session["Id"]= Convert.ToInt32(DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), SQL1, ht));

					//				cmdInsert= new SqlCommand(SQL1, conndemo);
					//				cmdInsert.ExecuteNonQuery();

					// setup session info
					SqlCommand cmdIdentity;
					//				cmdIdentity = new SqlCommand("SELECT @@IDENTITY AS NewID",conndemo);
					//				Session["Id"]= Convert.ToInt32(cmdIdentity.ExecuteScalar());
					Session["Typ"]="Demo";
					Session["Name"]=txtFname.Text+" "+txtLname.Text;
					Session["Nick"]=txtFname.Text;
					Session["Comp"]=txtCname.Text;
					Session["Email"]=txtEmail.Text;
					Session["UserName"]=txtEmail.Text;
                    Session["Password"] = cryptedPwd;
					//Session["Password"]=txtPassword.Text;
					Session["Market"]= getMarket();

					Response.Cookies["Id"].Value = Session["Id"].ToString();
					Response.Cookies["Id"].Expires = DateTime.MaxValue;
					
					if (txtComment.Text != "" || (txtOtherPort.Text != ""))
					{
						string SQL = "INSERT INTO COMMENT (CMNT_PERS,CMNT_TEXT,CMNT_DATE) " ; 
						// email somebody to let them know that an additional port has been added
						if (txtOtherPort.Text != "") 
						{
							SQL+="VALUES('" + Session["Id"] + "','Customer delivered to "+txtOtherPort.Text+" - ";
						}
						else
						{
							SQL+="VALUES('" + Session["Id"] + "','Customer - ";
						}
						
						if (Parse(txtComment.Text).Length >= 750)
						{
							SQL+=Parse(txtComment.Text).Substring(0,750)+"',getdate())";
						}
						else
						{
							SQL+=Parse(txtComment.Text)+"',getdate())";
						}
						cmdIdentity = new SqlCommand(SQL,conndemo);
						cmdIdentity.ExecuteNonQuery();
					}
				}
				finally
				{
					conndemo.Close();
					conn.Close();
				}
				
				MailMessage mail = new MailMessage();
				mail.From = txtEmail.Text;

				mail.To = Application["strEmailOwner"].ToString();
				mail.Cc= Application["strEmailAdmin"].ToString();

				//strP="<HTML><HEAD><TITLE>TPE</TITLE></HEAD><BODY>";

				mail.Subject = "Demo Registration - " + txtFname.Text + " " + txtLname.Text;
				strP=strP+HelperFunction.getEmailHeaderHTML();
				strP=strP+"<br><b><a href=mailto:'"+(string)ViewState["Email"]+"'>"+(string)ViewState["Email"]+"</a></b> has signed up to receive market updates from The Plastics Exchange.";
				
				strP=strP+HelperFunction.getUserInformationHTML(txtEmail.Text, cryptedPwd, mail.To, this.Context);

				strP=strP+HelperFunction.getEmailFooterHTML();
				//strP=strP + "</table></BODY></HTML>";
				mail.Body = strP;
				mail.BodyFormat = MailFormat.Html;
				//SmtpMail.SmtpServer = ("localhost");
				TPE.Utility.EmailLibrary.Send(Context,mail);

				Response.Redirect ("http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/spot/Spot_Floor.aspx");
			}
		}

		private void cvQuality_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
		{
			args.IsValid = (this.chkQuality1.Checked || this.chkQuality2.Checked || this.chkQuality3.Checked);
		}

		private void EmailAlertNewPort()
		{
			MailMessage mail = new MailMessage();
			mail.From = "Website";

			//mail.To = "christian@theplasticsexchange.com";
			mail.To= "Christian@theplasticsexchange.com";
			mail.Cc= Application["strEmailAdmin"].ToString();
			mail.Subject = "New Port: " + txtOtherPort.Text;
			mail.Body = "New Port: " + txtOtherPort.Text;
			mail.BodyFormat = MailFormat.Html;
			//SmtpMail.SmtpServer = ("caesar");
			TPE.Utility.EmailLibrary.Send(Context,mail);
		}

		private void btnSave_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if (Page.IsValid)
			{
				string strP="";
				SqlConnection conn;
				SqlConnection conndemo;
				
				//Check if the email is in the DB
				Hashtable param = new Hashtable();
					
				string strSql = "select MAI_MAIL FROM mailing WHERE MAI_MAIL = @Email";
				param.Add("@Email",txtEmail.Text);
				DataTable dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(),strSql,param);

				if (dt.Rows.Count == 0)
				{
					string strSqlInsert = "INSERT INTO mailing (MAI_MAIL, MAI_CODE) VALUES (@Email, '0')";
					DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlInsert, param);						
				}
								
				// Add new item in db
				conn = new SqlConnection(Application["DBConn"].ToString());
				conndemo = new SqlConnection(Application["DBConn"].ToString());	
				string cryptedPwd = "";
				try 
				{
					conn.Open();

					conndemo.Open();

					int iPref = 0;
					SqlDataReader dtrHDPE;
					SqlCommand cmdHDPE;
					cmdHDPE= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_CATG='HDPE'  OR CONT_CATG='HMWPE'", conn);
					dtrHDPE= cmdHDPE.ExecuteReader();
					while (dtrHDPE.Read())
					{
                        
						CheckBox tempCB = (CheckBox)Master.FindControl("cphMain").FindControl("CB_" + dtrHDPE["CONT_ID"].ToString()); // find the appropriate checkbox
						if(tempCB.Checked)
						{
							iPref += (int)Math.Pow(2,(Convert.ToInt32(dtrHDPE["CONT_ID"])-1)); // checked box found! add to total
						}
					}
					dtrHDPE.Close();
					
					SqlDataReader dtrLDPE;
					SqlCommand cmdLDPE;
					cmdLDPE= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_CATG='LDPE'", conn);
					dtrLDPE= cmdLDPE.ExecuteReader();
					while (dtrLDPE.Read())
					{
                        CheckBox tempCB = (CheckBox)Master.FindControl("cphMain").FindControl("CB_" + dtrLDPE["CONT_ID"].ToString()); // find the appropriate checkbox
						if(tempCB.Checked)
						{
							iPref += (int)Math.Pow(2,(Convert.ToInt32(dtrLDPE["CONT_ID"])-1)); // checked box found! add to total
						}
					}
					dtrLDPE.Close();

					SqlDataReader dtrLLDPE;
					SqlCommand cmdLLDPE;
					cmdLLDPE= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_CATG='LLDPE'", conn);
					dtrLLDPE= cmdLLDPE.ExecuteReader();
					while (dtrLLDPE.Read())
					{
                        CheckBox tempCB = (CheckBox)Master.FindControl("cphMain").FindControl("CB_" + dtrLLDPE["CONT_ID"].ToString()); // find the appropriate checkbox
						if(tempCB.Checked)
						{
							iPref += (int)Math.Pow(2,(Convert.ToInt32(dtrLLDPE["CONT_ID"])-1)); // checked box found! add to total
						}
					}
					dtrLLDPE.Close();
					
					SqlDataReader dtrPS;
					SqlCommand cmdPS;
					cmdPS= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_CATG='GPPS' OR CONT_CATG='HIPS'", conn);
					dtrPS= cmdPS.ExecuteReader();
					while (dtrPS.Read())
					{
                        CheckBox tempCB = (CheckBox)Master.FindControl("cphMain").FindControl("CB_" + dtrPS["CONT_ID"].ToString()); // find the appropriate checkbox
						if(tempCB.Checked)
						{
							iPref += (int)Math.Pow(2,(Convert.ToInt32(dtrPS["CONT_ID"])-1)); // checked box found! add to total
						}
					}
					dtrPS.Close();
					
					SqlDataReader dtrPP;
					SqlCommand cmdPP;
					cmdPP= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_CATG='HoPP' OR CONT_CATG='CoPP'", conn);
					dtrPP= cmdPP.ExecuteReader();
					while (dtrPP.Read())
					{
                        CheckBox tempCB = (CheckBox)Master.FindControl("cphMain").FindControl("CB_" + dtrPP["CONT_ID"].ToString()); // find the appropriate checkbox
						if(tempCB.Checked)
						{
							iPref += (int)Math.Pow(2.0,(Convert.ToInt32(dtrPP["CONT_ID"])-1)); // checked box found! add to total
						}
					}
					dtrPP.Close();

					cryptedPwd = Crypto.Encrypt(txtPassword.Text);
					
					string SQL1;
					string SQL2;
					SQL1 ="INSERT INTO PERSON(PERS_ACCOUNT_DISABLED,PERS_ACES,PERS_Stat,PERS_FRST_NAME,PERS_LAST_NAME,COMP_NAME,PERS_ENBL,PERS_TYPE,PERS_PSWD,PERS_MAIL,PERS_PHON,PERS_PREF,Email_ENBL";
					SQL2=" Values('False','-1','0',@Fname, @Lname, @Cname,'1','D', @Password, @Email,@Phone";

					Hashtable ht = new Hashtable();
					ht.Add("@Fname",Parse(txtFname.Text));
					ht.Add("@Lname",Parse(txtLname.Text));
					ht.Add("@Cname",Parse(txtCname.Text));
					ht.Add("@Password",cryptedPwd);
					ht.Add("@Email",Parse(txtEmail.Text));
					ht.Add("@Phone",ParsePhone(txtPhone.Text));


					//SQL1 ="INSERT INTO PERSON(PERS_FRST_NAME,PERS_LAST_NAME,PERS_COMP_NAME,PERS_ENBL,PERS_TYPE,PERS_PSWD,PERS_MAIL,PERS_PHON,PERS_PREF,Email_ENBL";
					//SQL2=" Values('"+Parse(txtFname.Text)+"','"+Parse(txtLname.Text)+"','"+Parse(txtCname.Text)+"','1','P','"+Parse(txtPassword.Text)+"','"+Parse(txtEmail.Text)+"','"+Parse(txtPhone.Text);
					if (iPref > 0)
					{
						// preference have been selected
						//SQL2 += "','"+iPref.ToString()+"','1'";
						SQL2 += ",@Pref";
						ht.Add("@Pref",iPref.ToString());
					}
					else
					{
						//SQL2 += "','"+iPref.ToString()+"','0'";
						SQL2 += ",@Pref";
						ht.Add("@Pref",iPref.ToString());
					}

					//SQL2 += ", " + cbEmailSubscriber.SelectedValue;
					SQL2 += ", " + rblEmailSubscriber.SelectedValue;

					// add appropriate ports
					if (ddlPort.SelectedValue.ToString() != "*") 
					{
						SQL1+=",PERS_PORT";
						//SQL2+=",'"+Parse(ddlPort.SelectedValue.ToString())+"'";
						SQL2+=",@Port";
						ht.Add("@Port",Parse(ddlPort.SelectedValue.ToString()));
					}

					// add Job function
					if (ddlJobFunction.SelectedValue.ToString() != "Please Select") 
					{
						SQL1+=",PERS_CATEGORY";
						SQL2+=",@Category";
						ht.Add("@Category",Parse(ddlJobFunction.SelectedItem.ToString()));
					}

						string ZipCode = "";
						if (txtZipCode.Text.Trim()!="") ZipCode = txtZipCode.Text.Trim();
						if (ddlZipCode.SelectedIndex!=0) ZipCode = ddlZipCode.SelectedItem.Value;
		
						if (ZipCode!="")
						{
							SQL1+=",PERS_ZIP";
							//						SQL2+=",'"+Parse(ZipCode)+"'";
							SQL2+=",@ZipCode";
							ht.Add("@ZipCode", Parse(ZipCode));
						}

					// email somebody to let them know that an additional port has been added
					if (txtOtherPort.Text != "") 
					{
						EmailAlertNewPort();

					}
					if (txtTitle.Text != "")
					{
						SQL1+=",PERS_TITL";
						//SQL2+=",'"+Parse(txtTitle.Text)+"'";
						SQL2+=",@PersTitl";
						ht.Add("@PersTitl", Parse(txtTitle.Text));
					}			
					
					SQL1+=",PERS_PRMLY_INTRST";
					//if (txtMarket.Text == "D") //Domestic
                    if (getMarket() == "D")
					{
						SQL2+=",'1'";
					}
					else
					{
						SQL2+=",'2'";
					}

					SQL1+=",PERS_INTRST_SIZE, PERS_INTRST_QUALT";
					if ((Bit(chkSize1.Checked)+ Bit(chkSize2.Checked)+ Bit(chkSize3.Checked)) == "000")
						SQL2+=",'111'";
					else
						SQL2+=",'" + Bit(chkSize1.Checked)+ Bit(chkSize2.Checked)+ Bit(chkSize3.Checked) + "'";
					
//					if ((Bit((chkQuality1.Checked) || (txtMarket.Text=="I"))+ Bit(chkQuality2.Checked)+ Bit(chkQuality3.Checked)) == "000")
                    if ((Bit((chkQuality1.Checked) || (getMarket() == "I")) + Bit(chkQuality2.Checked)) == "00")
						SQL2+=",'111'";
					else
                        SQL2 += ",'" + Bit((chkQuality1.Checked) || (getMarket() == "I")) + Bit(chkQuality2.Checked) + "'";

					SQL1 += ") " + SQL2 + ") SELECT @@IDENTITY AS NewID" ;

					Session["Id"]= Convert.ToInt32(DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), SQL1, ht));

					//				cmdInsert= new SqlCommand(SQL1, conndemo);
					//				cmdInsert.ExecuteNonQuery();

					// setup session info
					SqlCommand cmdIdentity;
					//				cmdIdentity = new SqlCommand("SELECT @@IDENTITY AS NewID",conndemo);
					//				Session["Id"]= Convert.ToInt32(cmdIdentity.ExecuteScalar());
					Session["Typ"]="Demo";
					Session["Name"]=txtFname.Text+" "+txtLname.Text;
					Session["Nick"]=txtFname.Text;
					Session["Comp"]=txtCname.Text;
					Session["Email"]=txtEmail.Text;
					Session["UserName"]=txtEmail.Text;
                    Session["Password"] = cryptedPwd;
                    //Session["Password"]=txtPassword.Text;
                    Session["Market"] = this.getMarket();
					Session["Preferences"] = iPref.ToString();


					Response.Cookies["Id"].Value = Session["Id"].ToString();
					Response.Cookies["Id"].Expires = DateTime.MaxValue;
					
					if (txtComment.Text != "" || (txtOtherPort.Text != ""))
					{
						string SQL = "INSERT INTO COMMENT (CMNT_PERS,CMNT_TEXT,CMNT_DATE) " ; 
						// email somebody to let them know that an additional port has been added
						if (txtOtherPort.Text != "") 
						{
							SQL+="VALUES('" + Session["Id"] + "','Customer delivered to "+txtOtherPort.Text+" - ";
						}
						else
						{
							SQL+="VALUES('" + Session["Id"] + "','Customer - ";
						}
						
						if (Parse(txtComment.Text).Length >= 750)
						{
							SQL+=Parse(txtComment.Text).Substring(0,750)+"',getdate())";
						}
						else
						{
							SQL+=Parse(txtComment.Text)+"',getdate())";
						}
						cmdIdentity = new SqlCommand(SQL,conndemo);
						cmdIdentity.ExecuteNonQuery();
					}
				}
				finally
				{
					conndemo.Close();
					conn.Close();
				}
				
				MailMessage mail = new MailMessage();
				mail.From = txtEmail.Text;

				mail.To = Application["strEmailOwner"].ToString();
				mail.Cc= Application["strEmailAdmin"].ToString();

				//strP="<HTML><HEAD><TITLE>TPE</TITLE></HEAD><BODY>";

				mail.Subject = "Demo Registration - " + txtFname.Text + " " + txtLname.Text;
				strP=strP+HelperFunction.getEmailHeaderHTML();
				strP=strP+"<br><b><a href=mailto:'"+(string)ViewState["Email"]+"'>"+(string)ViewState["Email"]+"</a></b> has signed up to receive market updates from The Plastics Exchange.";
				
				strP=strP+HelperFunction.getUserInformationHTML(txtEmail.Text, cryptedPwd, mail.To, this.Context);

				strP=strP+HelperFunction.getEmailFooterHTML();
				//strP=strP + "</table></BODY></HTML>";
				mail.Body = strP;
				mail.BodyFormat = MailFormat.Html;
				//SmtpMail.SmtpServer = ("localhost");
				TPE.Utility.EmailLibrary.Send(Context,mail);

                Response.Redirect("http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/spot/Spot_Floor.aspx");
            }
		}
	}
}
