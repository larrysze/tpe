using System;
using System.Text;
using System.Web.Mail;
using System.Data.SqlClient;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using TPE.Utility;
using System.Xml;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for Public_News.
	/// </summary>
	public partial class Public_News : System.Web.UI.Page
	{
		const string SEARCHDEFAULT = "Search all news";

		protected System.Web.UI.WebControls.PlaceHolder offers;
		private int NewsMonth = 0;
		private int NewsYear = System.DateTime.Now.Year;
		private string DateSortId = "0";
		
		protected string searchString = SEARCHDEFAULT;

		protected Literal litClientIDs;

		/******************************************************************************
		'*   1. File Name       : Public\Public_News.aspx                            *
		'*   2. Description     : Public_News(View all news)                         *
		'*																			 *
		'*   3. Modification Log:                                                    *
		'*     Ver No.       Date          Author             Modification           *
		'*   -----------------------------------------------------------------       *
		'*                                                                           *
		'*      2.00       2-27-2003       Xiaoda               Comment              *
		'*****************************************************************************/
		   
		public void Page_Load(object sender, EventArgs e)
		{			
			if (!IsPostBack)
			{
				Bind();
				if (!(Session["Typ"].ToString()=="A" || Session["Typ"].ToString()=="B" || Session["Typ"].ToString()=="L"))
				{
					dg.Columns[0].Visible = false;
					dg.Columns[1].Visible = false;
				}			
			}
		}

		/// <summary>
		/// DataSet GetMonths()
		///   return dataset of previous 12 months
		/// </summary>
		/// <returns></returns>
		protected DataView GetMonths()
		{	
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				return DBLibrary.GetDataTableFromStoredProcedure(conn,"spNewsMonths").DefaultView;
			}
		}

		protected string GetDateSortId()
		{
			return this.DateSortId;
		}

		protected void dgPageChange(object sender, DataGridPageChangedEventArgs e)
		{

			dg.CurrentPageIndex = e.NewPageIndex;
			Bind();

		}
		private void Bind()
		{
			if ((searchString.Length > 0) && (searchString != "Search all news"))
			{
				BindSearch(searchString);
			} 
			else
			{
				Hashtable htParams = new Hashtable();
				htParams.Add("@Month",this.NewsMonth);
				htParams.Add("@Year",this.NewsYear);
				DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(), dg, "spNews",htParams);
			}
			
			lblNoRecords.Visible = (dg.Items.Count == 0);
		}

		protected void BindSearch(string searchString)
		{
			//search the week in reviews, then search comtex xml
			if ((searchString.Length > 0) && (searchString != "Search all news"))
			{
				DataTable dtAllResults = new DataTable();
				using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
				{
					Hashtable htParams = new Hashtable();
					htParams.Add("@SearchString",searchString);
					dtAllResults = DBLibrary.GetDataTableFromStoredProcedure(conn,"spSearchWeekInReviews",htParams);
				}
				// now search comtex
				//string fileNames = 

				//read the XML files in "Current" folder
				string strDir = Server.MapPath("/News/Current/");
				System.Text.StringBuilder sbSearchResults = new System.Text.StringBuilder();
			
				DirectoryInfo dir = new DirectoryInfo(strDir);
				//get all files in directory
				FileInfo[] filesInDir = dir.GetFiles("*.xml");
						
				if (searchString.Length > 0)
				{					
					foreach (FileInfo file in filesInDir)
					{
						if (file.Name != "0000.xml")	// special file, ignore
						{
							if (SearchXML(strDir+file.Name,searchString))
							{
								sbSearchResults.Append(file.Name + ",");
							}
						}
					}
				}
				string strIds = string.Empty;
				string s = sbSearchResults.ToString();
				if (s.Length > 0)
				{
					s = s.Remove(s.Length - 1, 1);
					string[] arrFileNames = s.Split(',');
				
					for(int i = 0;i<arrFileNames.Length;i++)
					{
						strIds += arrFileNames[i].Remove(arrFileNames[i].IndexOf(".xml"),4) + ",";
					}
				}
				if (strIds.Length >0)
				{
					strIds = strIds.Remove(strIds.Length -1,1);	// remove last comma

					string strSql = "select id,date,headline from News where id in (" +strIds + ")";

					DataTable dtComtex = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(),strSql);
					foreach(DataRow row in dtComtex.Rows)
					{
						DataRow newRow = dtAllResults.NewRow();
						newRow[0] = row[0];
						newRow[1] = row[1];
						newRow[2] = row[2];
						dtAllResults.Rows.Add(newRow);
					}
				}
				DataView dv = dtAllResults.DefaultView;
				dv.Sort = "date desc";
				dg.DataSource = dv;
				dg.DataBind();
				//this.NewsMonth = "0";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_ItemCommand);
			this.dg.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_ItemDataBound);

		}
		#endregion

		private void deleteWeekInReview(string id, DateTime review_date)
		{
			Hashtable htParam = new Hashtable();
			htParam.Add("@ID", id);
			TPE.Utility.DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"DELETE FROM NEWS WHERE ID=@ID",htParam);

			TPE.Utility.DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"DELETE FROM WEEKLY_REVIEWS WHERE published=1 AND Day(review_date)=" + review_date.Day + " AND MONTH(review_date)=" + review_date.Month + " AND YEAR(review_date)=" + review_date.Year);

		}

		private void dg_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName=="Delete")
			{
				if(e.Item.Cells[4].Text.IndexOf("The Plastics Exchange Week in Review") != -1)
				{
					deleteWeekInReview(e.Item.Cells[2].Text.ToString(), Convert.ToDateTime(e.Item.Cells[3].Text));
					Bind();
					return;
				}

				string id = e.Item.Cells[1].Text.ToString();
				
				string FilePath = Server.MapPath("/News/Current/" + id + ".xml");

				File.Delete(FilePath);
				Hashtable htParam = new Hashtable();
				htParam.Add("@ID",e.Item.Cells[2].Text.ToString());
				TPE.Utility.DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"DELETE FROM NEWS WHERE ID=@ID",htParam);
				
				Bind();
			}
			else if (e.CommandName=="Edit")
			{
				if(e.Item.Cells[4].Text.IndexOf("The Plastics Exchange Week in Review") != -1)
				{
					string date = Convert.ToDateTime(e.Item.Cells[3].Text).Ticks.ToString();
					Response.Redirect("/Administrator/WeeklyMUNotepad.aspx?Edit=true&Date=" + date);
				}
				else
				{
					Response.Redirect("/Administrator/Edit_News.aspx?Id=" + e.Item.Cells[2].Text.ToString());
				}
			}
		}

		public object FormatHeadline(object s)
		{
			return ((object) s.ToString().Replace("Alert!!","<font color='red'>Alert!!</font>").Replace("The Plastics Exchange Week in Review","<font color='red'>The Plastics Exchange Week in Review</font>"));
		}

		public void ddlNewsDate_IndexChanged(object sender, System.EventArgs e)
		{
			DropDownList ddl = (DropDownList)sender;
			this.DateSortId = ddl.SelectedValue.ToString();
			string s = ddl.SelectedItem.Text.ToString();
			if (s == "Select a Month")
			{
				this.NewsMonth = 0;
			} 
			else
			{
				this.NewsMonth = ConvertMonthToInt(s.Split(' ')[0].ToString());
				this.NewsYear = Int32.Parse(s.Split(' ')[1].ToString());
			}
			Bind();
		}

		private int ConvertMonthToInt(string monthName)
		{
			int result = 0;
			string[] MonthNames = {"January","February","March","April","May","June","July","August","September","October","November","December"};
			for (int i = 0; i < MonthNames.Length; i++)
			{
				if (MonthNames[i] == monthName)
				{
					result = i + 1;
					break;
				}
			}
			return result;
		}	
		public void txtSearch_OnTextChanged(object sender, EventArgs e)
		{
			searchString = ((TextBox)sender).Text;
		}

		private void dg_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Header)
			{
				TextBox tb = (TextBox)e.Item.FindControl("txtSearch");
				tb.Attributes.Add("OnFocus","javascript:if (this.value == 'Search all news') {this.value = '';};");
				tb.Attributes.Add("OnKeyPress","javascript:checkKey();");
				Button bt = (Button)e.Item.FindControl("btnSearch");
				bt.Attributes.Add("onClick","javascript:return checkSearch();");				
				
				litClientIDs.Text="'" + tb.ClientID.ToString() + "','" + bt.ClientID.ToString() + "'";

				/*
				if(tb != null)
				{
					String scriptString = "<script language=JavaScript> function checkSearch() ";
					scriptString += "{ if ((document.getElementById('dg__ctl1_txtSearch').value='' || document.getElementById('dg__ctl1_txtSearch').value='Search all news'))"; // was tb.ClientID
					scriptString += "{ return false; } else { return true; } }";
					scriptString += "</script>";
					
					if(!this.IsClientScriptBlockRegistered("clientScript"))
						this.RegisterClientScriptBlock("clientScript", scriptString);
				}
				*/
			}

			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				// retrieving the file extension
				string strDate = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DATE"));
				string strTitle = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "HEADLINE"));
				string id = string.Empty;
				id = e.Item.Cells[2].Text;

				string searchTerm = string.Empty;
				if ((this.searchString != string.Empty) && (searchString != SEARCHDEFAULT))
				{
					searchTerm = "&SearchTerm=" + searchString;	
				}
				if(strTitle.StartsWith("The Plastics Exchange Week in Review"))
				{
					strDate = Convert.ToDateTime(strDate).Ticks.ToString();
					e.Item.Cells[4].Text = "<a href=\"/Research/WeeklyReview.aspx?Date=" + strDate + searchTerm + "\"><font color='red'>" + strTitle + "</font></a>";
				}
                else if (strTitle.StartsWith("The Plastics Exchange Monthly Update"))
                {
                    strDate = Convert.ToDateTime(strDate).Ticks.ToString();
                    e.Item.Cells[4].Text = "<a href=\"/Public/Public_Research.aspx?Date=" + strDate + searchTerm + "\"><font color='red'>" + strTitle + "</font></a>";
                }
                else if (strTitle.StartsWith("Alert!!"))
				{
					e.Item.Cells[4].Text = "<a href=\"/Public/News_Template.aspx?Buffer=&Id=" + id + searchTerm + "\">" + strTitle.Replace("Alert!!","<font color=red>Alert!!</font>") + "</a>";
				}
				else if(strTitle.StartsWith(" The Plastics Exchange Week in Review"))
				{				
					e.Item.Cells[4].Text = "<a href=\"/Public/News_Template.aspx?Buffer=&Id=" + id + searchTerm + "\"><font='red'>" + strTitle + "</font></a>";
				}
				else
				{				
					e.Item.Cells[4].Text = "<a href=\"/Public/News_Template.aspx?Buffer=&Id=" + id + searchTerm + "\">" + strTitle + "</a>";

				}
				if (e.Item.Cells[1].Visible)	// if delete is visible then add confirmation dialog
				{
					if (e.Item.Cells[1] != null)
						((LinkButton) e.Item.Cells[1].Controls[0]).Attributes.Add("onClick", "return confirm('Are you sure you want to delete this news item?');");
				}
			}
		
		}
		
		protected void btnSearch_Click(Object sender, EventArgs e)
		{
			// do search
			Bind();
		}

		private bool SearchXML(string UrlToXmlFile,string strSearch)
		{
			XmlTextReader reader = null;
			// load the file
			reader = new XmlTextReader(UrlToXmlFile);
			try
			{
				//read XML files based on node
				while (reader.Read())
				{
					if (reader.ReadString().ToUpper().IndexOf(strSearch.ToUpper())>=0)
					{
						reader.Close();
						return true;
					}
				}
				// can't find search string. return false 
				return false;
			}
			finally
			{
				reader.Close();
			}
		}

	}
}
