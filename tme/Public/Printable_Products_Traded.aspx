<%@ Page %>
<%
   '*****************************************************************************
   '*   1. File Name       : Public\Printable_Products_Traded.aspx              *
   '*   2. Description     : A printable chart                                  *
   '*			                                                        *
   '*						                                *
   '*   3. Modification Log:                                                    *
   '*     Ver No.       Date          Author             Modification           *
   '*   -----------------------------------------------------------------       *
   '*      1.00       2-27-2004       Xiaoda             Comment                *
   '*                                                                           *
   '*****************************************************************************
%>

<title>Products Traded</title>
<center>
<img src=/images/misc/rprintable2.gif align=top onclick=window.print()>
</center>
</body></html>
<script>window.print()</script>
