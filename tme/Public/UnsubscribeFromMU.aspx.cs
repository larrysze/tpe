using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Xml;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Public
{
    public partial class UnsubscribeFromMU : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // NOTE: ID is encrypted email address!!
                if (Request["ID"] != null)
                {

                    string crypt_email = Request["ID"];
                    string decrypt_email = Crypto.Decrypt(crypt_email);
                    lblEmail.Text = decrypt_email;

                    if (!isInTheList(decrypt_email))
                    {
                        pnlDelete.Visible = false;
                        pnlInput.Visible = false;
                        pnlThankYou.Visible = true;

                        lblThankYouMessage.Text = "Sorry, we could not find this email in our List.";

                    }
                    else
                    {
                        pnlDelete.Visible = true;
                        pnlInput.Visible = false;
                        pnlThankYou.Visible = false;

                        lblEncryptEmail.Text = crypt_email;

                    }

                }
                else
                {
                    btnDelete.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to unsubscribe?');");
                    pnlDelete.Visible = false;
                    pnlInput.Visible = true;
                    pnlThankYou.Visible = false;
                }
            }
        }

        private bool isInTheList(string email)
        {
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                Hashtable param = new Hashtable();
                param.Add("@EMAIL", email);
                SqlDataReader dtrHave = DBLibrary.GetDataReaderFromSelect(conn, "SELECT * FROM MAILING WHERE MAI_MAIL=@EMAIL", param);
                return dtrHave.Read();

            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            pnlDelete.Visible = false;
            pnlInput.Visible = false;
            pnlThankYou.Visible = true;
            //delete email and show confirmation message
            string email = "";
            if (lblEncryptEmail.Text.Length > 0)
            {
                email = Crypto.Decrypt(lblEncryptEmail.Text);
                Hashtable param = new Hashtable();
                param.Add("@EMAIL", email);
                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), "DELETE FROM MAILING WHERE MAI_MAIL=@EMAIL", param);
            }

            lblThankYouMessage.Text = "Successfully unsubscribed " + email;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlDelete.Visible = false;
            pnlInput.Visible = false;
            pnlThankYou.Visible = true;

            lblThankYouMessage.Text = "Unsubscribe cancelled.";
            //show confirmation message


        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            pnlDelete.Visible = false;
            pnlInput.Visible = false;
            pnlThankYou.Visible = true;
            
            // todo: send email to user with link to unsubscribe confirmation.
            // message = "We have received an unsusbcribe request from " + email + ".  Click <a href="/public/UnsubscribeFromMU.aspx?ID=" + encrypted email address + ">here</a> to unsubscribe. 
            // for now, just send email to admin with email to unsubscribe. 
            EmailLibrary.Send(this.Context, ConfigurationSettings.AppSettings["strEmailInfo"].ToString(), ConfigurationSettings.AppSettings["strEmailAdmin"].ToString(), "MarketUpdate Unsubscribe Request", this.txtEmail.Text + " is requesting to be unsubscribed.  IP = " + Request.ServerVariables["REMOTE_ADDR"].ToString());
            lblThankYouMessage.Text = "Unsubscribe Request Submitted for " + txtEmail.Text;
/*
            string xslFile = Server.MapPath("/test.xsl");
            string xmlFile = Server.MapPath("/test.xml");
            string htmlFile = Server.MapPath("/test.htm");

            XslTransform transf = new XslTransform();


            transf.Load(xslFile);
//            transf.Transform(xmlFile, );

            XPathDocument xpdoc = new XPathDocument(new XmlTextReader(xmlFile));

            XPathNavigator xpnav = xpdoc.CreateNavigator();

            XmlReader reader = transf.Transform(xpnav, null);

            reader.MoveToContent();
            Response.Write(reader.ReadOuterXml());
            reader.Close();
 */
        }
    }
}
