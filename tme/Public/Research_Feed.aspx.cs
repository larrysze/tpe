using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Xml;


namespace localhost.Public
{
	/// <summary>
	/// Summary description for Public_Research.
	/// </summary>
	public class Public_ResearchNew : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DropDownList ddlIssue;
		protected System.Web.UI.WebControls.Label lblContent;
		protected System.Web.UI.WebControls.ImageButton ibutton;
		protected System.Web.UI.WebControls.Label lblPrintLink;
	
		
		/*****************************************************************************
		'*   1. File Name       : Public\Public_Research.aspx                         *
		'*   2. Description     : View the content of current and atchieved Market Update *
		'*			                                                                *
		'*						                                                    *
		'*   3. Modification Log:                                                    *
		'*     Ver No.       Date          Author             Modification           *
		'*   -----------------------------------------------------------------       *
		'*      1.00       2-27-2004       Xiaoda             Comment                *
		'*                                                                           *
		'*****************************************************************************/


		public void Page_Load(object sender, EventArgs e)
		{

			string strXmlFileName = Server.MapPath("../Administrator/marketUpdate.xml");				
			if(!Page.IsPostBack)
			{
				if(System.IO.File.Exists(strXmlFileName.ToString()))
				{
					XmlTextReader xmlreader = new XmlTextReader(strXmlFileName.ToString());
					XmlDocument xmldoc = new XmlDocument();
					xmldoc.Load(xmlreader);
					xmlreader.Close();

					XmlNodeList all_records = xmldoc.GetElementsByTagName("MU_record");
					int last_published = all_records.Count - 2;
					loadHTML(last_published, strXmlFileName);

				}
			}
			/*
					 * string strFileName;
			StringBuilder sbContent = new StringBuilder();


			strFileName = Server.MapPath("/Research/Market_Update/"+ddlIssue.SelectedItem.Value.ToString()+".txt");
			FileStream fsIn =  File.OpenRead(strFileName);
			StreamReader Reader;
			Reader = new StreamReader(fsIn);
			while (Reader.Peek() > -1 )
			{
				sbContent.Append(Reader.ReadLine());
			}
			lblContent.Text = sbContent.ToString();
			//lblPrintLink.Text ="<a href='/Research/Market_Update/pdf/"+ddlIssue.SelectedItem.Value.ToString()+".pdf'><img border='0'  src='/pics/printer.jpg'>Print friendly version</a>";
*/
		
		}
		
		private void loadHTML(int index, string strXmlFileName)
		{
			XmlTextReader xmlreader = new XmlTextReader(strXmlFileName.ToString());
			XmlDocument xmldoc = new XmlDocument();
			xmldoc.Load(xmlreader);
			xmlreader.Close();

			XmlNodeList all_records = xmldoc.GetElementsByTagName("MU_record");
			
			string strPP = all_records.Item(index).SelectSingleNode("polypropylene_article").InnerText;
			string strPS = all_records.Item(index).SelectSingleNode("polystyrene_article").InnerText;
			string strPE = all_records.Item(index).SelectSingleNode("polyethylene_article").InnerText;
			string strComments = all_records.Item(index).SelectSingleNode("common/comments").InnerText;
			string total = all_records.Item(index).SelectSingleNode("total_pounds").InnerText;
			string snapshot = all_records.Item(index).SelectSingleNode("snapshot").InnerText;
			string issue_date = all_records.Item(index).SelectSingleNode("date").InnerText;
				
			string[] charts = new string[6];
							
			charts[0] = all_records.Item(index).SelectSingleNode("path1").InnerText;
			charts[1] = all_records.Item(index).SelectSingleNode("path2").InnerText;
			charts[2] = all_records.Item(index).SelectSingleNode("path3").InnerText;
			charts[3] = all_records.Item(index).SelectSingleNode("path4").InnerText;
			charts[4] = all_records.Item(index).SelectSingleNode("path5").InnerText;
			charts[5] = all_records.Item(index).SelectSingleNode("path6").InnerText;
			

			lblContent.Text = Administrator.MarketUpdateLibrary.getPreview(strPE, strPP, strPS, charts, strComments, total, snapshot, issue_date, this.Context);
/*
			string strPdfFileName = Server.MapPath("/Research/Market_Update/pdf/" + (- sel_value) + ".pdf");
			if(System.IO.File.Exists(strPdfFileName))
			{
				lblPrintLink.Text = "<a href='https://" + ConfigurationSettings.AppSettings["SecureDomainName"].ToString() + "/Research/Market_Update/pdf/" + (- sel_value) + ".pdf'><img border='0'  src='https://" + ConfigurationSettings.AppSettings["SecureDomainName"].ToString() + "/pics/printer.jpg'>Print friendly version</a>";
			}
			else
			{
				//					lblPrintLink.Text = "No PDF version available yet, sorry";
				lblPrintLink.Text = "";
			}
*/
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	
	}
}
