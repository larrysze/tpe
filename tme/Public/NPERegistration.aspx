<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page Language="c#" CodeBehind="NPERegistration.aspx.cs" AutoEventWireup="false" Inherits="localhost.Public.NPERegistration" %>
<LINK href="tpe.css" type="text/css" rel="stylesheet">
	<style type="text/css">BODY { MARGIN: 0px; BACKGROUND-COLOR: #ffffff }
	#menu { Z-INDEX: 1; LEFT: 121px; VISIBILITY: visible; WIDTH: 128px; POSITION: absolute; TOP: 575px; HEIGHT: 97px }
	#Layer1 { Z-INDEX: 1; LEFT: 0px; VISIBILITY: hidden; WIDTH: 140px; POSITION: absolute; TOP: 575px; HEIGHT: 24px }
	#Layer2 { Z-INDEX: 2; LEFT: 140px; VISIBILITY: hidden; WIDTH: 140px; POSITION: absolute; TOP: 575px; HEIGHT: 72px }
	#Layer3 { Z-INDEX: 3; LEFT: 281px; VISIBILITY: hidden; WIDTH: 150px; POSITION: absolute; TOP: 575px; HEIGHT: 49px }
	#Layer4 { Z-INDEX: 4; LEFT: 432px; VISIBILITY: hidden; WIDTH: 130px; POSITION: absolute; TOP: 575px; HEIGHT: 25px }
	#Layer5 { Z-INDEX: 5; LEFT: 562px; VISIBILITY: hidden; WIDTH: 205px; POSITION: absolute; TOP: 575px; HEIGHT: 91px }
	</style>
	<form id="Form" name="Form" runat="server">
		<TPE:TEMPLATE id="Template1" title="Registration" ShowLeftMargin="false" Runat="server"></TPE:TEMPLATE><asp:validationsummary id="ValidationSummary1" runat="server" HeaderText="There are problems with the following fields. Please correct the errors below."></asp:validationsummary><TPE:WEB_BOX id="Web_Box1" Runat="server" Heading="Register" width="780"></TPE:WEB_BOX>
		<table cellSpacing="0" cellPadding="0" width="600" align="center" border="0">
			<TR>
				<TD colSpan="2"></TD>
			</TR>
			<TR>
				<TD align="center" colSpan="2"><BR>
					<TABLE cellSpacing="0" cellPadding="0" width="780" align="center" bgColor="#333333" border="0">
						<TBODY>
							<TR>
								<TD vAlign="top"><SPAN class="normalcontent">Please setup my account which will give me full 
            access to spot resin offers and market intelligence.</SPAN><BR>
									<BR>
									<TABLE height="20" cellSpacing="0" cellPadding="0" width="778" align="center" border="0">
										<TR>
											<TD background="/images2/orangebar_background.jpg">
												<P class="blackbold">Contact Information
												</P>
											</TD>
										</TR>
									</TABLE>
									<TABLE cellSpacing="1" cellPadding="0" width="780" border="0">
										<TR>
											<TD class="blackbold" align="right" width="134" bgColor="#dbdcd7" height="29">First 
												Name:
											</TD>
											<TD width="255" bgColor="#999999">&nbsp;
												<asp:textbox id="txtFname" runat="server" maxlength="20" Width="150"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" DESIGNTIMEDRAGDROP="459" ControlToValidate="txtFname"
													Display="none" ErrorMessage="First name can not be blank."></asp:requiredfieldvalidator></TD>
											<TD class="blackbold" align="right" width="130" bgColor="#dbdcd7">Last Name:
											</TD>
											<TD width="256" bgColor="#999999">&nbsp;
												<asp:textbox id="txtLname" runat="server" maxlength="20" Width="150"></asp:textbox>&nbsp;
												<asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" DESIGNTIMEDRAGDROP="462" ControlToValidate="txtLname"
													Display="none" ErrorMessage="Last name can not be blank."></asp:requiredfieldvalidator></TD>
										</TR>
										<TR>
											<TD class="blackbold" align="right" bgColor="#dbdcd7" height="29">Title:</TD>
											<TD bgColor="#999999">&nbsp;
												<asp:textbox id="txtTitle" runat="server" maxlength="29" Width="150"></asp:textbox>&nbsp;
												<asp:requiredfieldvalidator id="Requiredfieldvalidator8" runat="server" DESIGNTIMEDRAGDROP="463" ControlToValidate="txtTitle"
													Display="none" ErrorMessage="Title can not be blank."></asp:requiredfieldvalidator></TD>
											<TD class="blackbold" align="right" bgColor="#dbdcd7">Company Name:
											</TD>
											<TD bgColor="#999999">&nbsp;
												<asp:textbox id="txtCname" runat="server" maxlength="40" Width="150"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator3" runat="server" ControlToValidate="txtCname" Display="none"
													ErrorMessage="Company name can not be blank."></asp:requiredfieldvalidator></TD>
										</TR>
										<TR>
											<TD class="blackbold" align="right" bgColor="#dbdcd7" height="29">Phone:</TD>
											<TD bgColor="#999999">&nbsp;
												<asp:textbox id="txtPhone" runat="server" maxlength="15" Width="150" DESIGNTIMEDRAGDROP="478"></asp:textbox><asp:requiredfieldvalidator id="Requiredfieldvalidator7" runat="server" ControlToValidate="txtPhone" Display="none"
													ErrorMessage="Phone number can not be blank."></asp:requiredfieldvalidator></TD>
											<TD class="blackbold" align="right" bgColor="#dbdcd7">E-mail:</TD>
											<TD bgColor="#999999">&nbsp;
												<asp:textbox id="txtEmail" runat="server" maxlength="50" Width="150"></asp:textbox><asp:customvalidator id="Customvalidator1" runat="server" ErrorMessage="Email address already used."
													controltovalidate="txtEmail" display="none" OnServerValidate="IsDuplicate" NAME="Customvalidator1"></asp:customvalidator><asp:requiredfieldvalidator id="RequiredFieldValidator4" runat="server" ControlToValidate="txtEmail" Display="none"
													ErrorMessage="Email can not be blank."></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="RegularExpressionValidator1" Runat="server" ControlToValidate="txtEmail" Display="none"
													ErrorMessage="Please enter a valid email address. This message may be caused by extra spaces after the address." ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator></TD>
										</TR>
										<TR>
											<TD class="blackbold" align="right" bgColor="#dbdcd7" height="29">Password:</TD>
											<TD bgColor="#999999">&nbsp;
												<asp:textbox id="txtPassword" runat="server" maxlength="15" Width="150" DESIGNTIMEDRAGDROP="481"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator6" runat="server" ControlToValidate="txtPassword" Display="none"
													ErrorMessage="Password can not be blank."></asp:requiredfieldvalidator></TD>
											<TD bgColor="#dbdcd7" align="right">Staff&nbsp;member:&nbsp;</TD>
											<TD bgColor="#999999">&nbsp;
												<asp:DropDownList id="ddlNPEStaff" runat="server" Width="150px"></asp:DropDownList></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TBODY></TABLE>
					<table height="334" width="100%">
						<asp:panel id="pnlMarket" runat="server" visible="true">
							<TBODY>
								<TR>
									<TD align="center" colSpan="2" height="20">
										<P align="left">
											<asp:label id="lblMarketUpdateMessage" runat="server"></asp:label></P>
									</TD>
								</TR>
								<TR>
									<TD align="center" colSpan="2" height="183">
										<P><BR>
											<BR>
											<BR>
											&nbsp;</P>
										<P>
											<asp:label id="lblHeadingMarket" runat="server" Font-Size="Medium">Please select which market you are primarily interested:</asp:label>
											<asp:Label id="lblMarket" runat="server" Visible="False"></asp:Label><BR>
											<BR>
											<BR>
										</P>
									</TD>
								</TR>
								<TR>
									<TD vAlign="bottom" align="center" height="70">&nbsp;
										<asp:ImageButton id="ImageButtonDomestic" runat="server" ImageAlign="Baseline" Height="23px" ImageUrl="/Pics/icons/icon_US_Flag.jpg"></asp:ImageButton><BR>
										<asp:LinkButton id="lnkDomestic" runat="server" Font-Size="Small">U.S. Domestic Market</asp:LinkButton><BR>
										I accept delivery within the USA or Canada.</TD>
									<TD vAlign="bottom" align="center" height="70">
										<asp:ImageButton id="ImageButtonInternational" runat="server" Height="30px" ImageUrl="/Pics/icons/Icon_Globe.gif"></asp:ImageButton>&nbsp;
										<BR>
										<asp:LinkButton id="lnkInternational" runat="server" Font-Size="Small">International Market</asp:LinkButton><BR>
										I accept delivery at an international port.
									</TD>
								</TR>
						</asp:panel></table>
					<asp:panel id="pnlForm" runat="server" visible="false" Height="760px">
						<TABLE cellSpacing="0" cellPadding="0" width="780" align="center" bgColor="#333333" border="0">
							<TR>
								<TD vAlign="top">
									<TABLE height="20" cellSpacing="0" cellPadding="0" width="778" align="center" border="0">
										<TR>
											<TD background="/images2/orangebar_background.jpg">
												<P class="blackbold">Delivery Information
												</P>
											</TD>
										</TR>
									</TABLE>
									<asp:panel id="pnlPorts" runat="server" visible="false">
										<TABLE height="40" cellSpacing="1" cellPadding="0" width="780" border="0">
											<TR>
												<TD class="blackbold" align="right" width="134" bgColor="#dbdcd7" height="29">Primary 
													delivery port:
												</TD>
												<TD width="643" bgColor="#999999">
													<TABLE cellSpacing="0" cellPadding="0" width="624" align="center" border="0">
														<TR>
															<TD width="190">
																<asp:DropDownList id="ddlPort" runat="server"></asp:DropDownList></TD>
															<TD class="blackcontent" align="right" width="137">or enter the Zip Code:</TD>
															<TD width="297">
																<asp:TextBox id="txtOtherPort" runat="server" Width="80px" MaxLength="8"></asp:TextBox></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel><asp:panel id="pnlZipCode" runat="server" visible="false">
										<TABLE height="40" cellSpacing="1" cellPadding="0" width="780" border="0">
											<TR>
												<TD class="blackbold" align="right" width="134" bgColor="#dbdcd7" height="29">Primary 
													delivery location :
												</TD>
												<TD width="643" bgColor="#999999">
													<TABLE cellSpacing="0" cellPadding="0" width="624" align="center" border="0">
														<TR>
															<TD width="190">
																<asp:DropDownList id="ddlZipCode" runat="server"></asp:DropDownList></TD>
															<TD class="blackcontent" align="right" width="137">or enter the Zip Code:</TD>
															<TD width="297">
																<asp:TextBox id="txtZipCode" runat="server" Width="80px" MaxLength="8"></asp:TextBox></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel><asp:panel id="pnlJobFunction" runat="server" visible="false">
										<TABLE height="20" cellSpacing="0" cellPadding="0" width="778" align="center" border="0">
											<TR>
												<TD background="/images2/orangebar_background.jpg">
													<P class="blackbold">Corporate Identity
													</P>
												</TD>
											</TR>
										</TABLE>
										<TABLE height="40" cellSpacing="1" cellPadding="0" width="780" border="0">
											<TR>
												<TD class="blackbold" align="right" width="134" bgColor="#dbdcd7" height="29">Corporate 
													Identity:
												</TD>
												<TD width="643" bgColor="#999999">
													<TABLE cellSpacing="0" cellPadding="0" width="624" align="center" border="0">
														<TR>
															<TD width="190">
																<asp:DropDownList id="ddlJobFunction" runat="server">
																	<asp:ListItem Text="Please Select" Value="1" />
																	<asp:ListItem Text="Resin Processor" Value="2" />
																	<asp:ListItem Text="Broker/Distributor/Trader" Value="3" />
																	<asp:ListItem Text="Resin Producer" Value="4" />
																	<asp:ListItem Text="Analyst/Market Observer" Value="5" />
																	<asp:ListItem Text="Other" Value="6" />
																</asp:DropDownList>
																<asp:RangeValidator id="RangeValidator1" runat="server" ErrorMessage="Corporate Identity needs to be selected."
																	Display="None" ControlToValidate="ddlJobFunction" MaximumValue="7" MinimumValue="2"></asp:RangeValidator></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel><asp:panel id="pnlInterested" runat="server" visible="false">
										<TABLE height="20" cellSpacing="0" cellPadding="0" width="778" align="center" border="0">
											<TR>
												<TD background="/images2/orangebar_background.jpg">
													<P class="blackbold">I�m primarily interested in:
													</P>
												</TD>
											</TR>
										</TABLE>
										<TABLE height="61" cellSpacing="1" cellPadding="0" width="780" border="0">
											<TR>
												<TD class="blackbold" align="right" width="134" bgColor="#dbdcd7" height="60">Sizes:
												</TD>
												<TD vAlign="middle" width="643" bgColor="#999999" height="62">
													<TABLE cellSpacing="0" cellPadding="0" width="624" align="center" border="0">
														<TR>
															<TD width="20"></TD>
															<TD class="blackcontent" width="173">
																<P align="left">
																	<asp:checkbox id="chkSize1" runat="server" Text="Rail Cars"></asp:checkbox></P>
															</TD>
															<TD width="20"></TD>
															<TD class="blackcontent" width="188">
																<asp:checkbox id="chkSize2" runat="server" Text="Bulk Trucks"></asp:checkbox></TD>
															<TD width="20"></TD>
															<TD class="blackcontent" width="188">
																<P align="left">
																	<asp:checkbox id="chkSize3" runat="server" Text="Truckload Boxes/ Bags"></asp:checkbox></P>
															</TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD class="blackbold" align="right" bgColor="#dbdcd7" height="29">Quality:</TD>
												<TD bgColor="#999999">
													<TABLE cellSpacing="0" cellPadding="0" width="624" align="center" border="0">
														<TR>
															<TD width="20"><LABEL>&nbsp;</LABEL></TD>
															<TD class="blackcontent" width="171">
																<P align="left">
																	<asp:checkbox id="chkQuality1" runat="server" Text="Prime"></asp:checkbox></P>
															</TD>
															<TD width="20"></TD>
															<TD class="blackcontent" width="188">
																<P align="left">
																	<asp:checkbox id="chkQuality2" runat="server" Text="Offgrade"></asp:checkbox></P>
															</TD>
															<TD width="20"></TD>
															<TD class="blackcontent" width="188">
																<P align="left">
																	<asp:checkbox id="chkQuality3" runat="server" Text="Regrind/Repro"></asp:checkbox></P>
															</TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
									<TABLE height="20" cellSpacing="0" cellPadding="0" width="778" align="center" border="0">
										<TR>
											<TD background="/images2/orangebar_background.jpg">
												<P class="blackbold">Please select the resins that you use. (Click to visualize the 
													grades)
												</P>
											</TD>
										</TR>
									</TABLE>
									<TABLE cellSpacing="1" cellPadding="0" width="780" border="0">
										<TR>
											<TD class="menu" onclick="MM_showHideLayers('Layer1','','show')" align="center" width="140"
												bgColor="#dbdcd7" height="30"><INPUT id="chkHDPE" onclick="HDPE_Click()" type="checkbox"><A onmouseover="MM_swapImage('Image17','','/images2/register/HDPE_pressed.jpg',1)"
													onmouseout="MM_swapImgRestore()" href="#"><IMG id="Image17" onclick="MM_showHideLayers('Layer1','','show')" height="18" src="/images2/register/HDPE.jpg"
														width="70" border="0" name="Image17"></A></TD>
											<TD class="blackbold" align="center" width="140" bgColor="#dbdcd7"><INPUT id="chkLDPE" onclick="LDPE_Click()" type="checkbox"><A onmouseover="MM_swapImage('Image18','','/images2/register/LDPE_pressed.jpg',1)"
													onmouseout="MM_swapImgRestore()" href="#"><IMG id="Image18" onclick="MM_showHideLayers('Layer2','','show')" height="18" src="/images2/register/LDPE.jpg"
														width="70" border="0" name="Image18"></A></TD>
											<TD class="blackbold" align="center" width="150" bgColor="#dbdcd7"><INPUT id="chkLLDPE" onclick="LLDPE_Click()" type="checkbox"><A onmouseover="MM_swapImage('Image19','','/images2/register/LLDPE_pressed.jpg',1)"
													onmouseout="MM_swapImgRestore()" href="#"><IMG id="Image19" onclick="MM_showHideLayers('Layer3','','show')" height="18" src="/images2/register/LLDPE.jpg"
														width="70" border="0" name="Image19"></A></TD>
											<TD class="blackbold" align="center" width="130" bgColor="#dbdcd7"><INPUT id="chkPS" onclick="PS_Click()" type="checkbox"><A onmouseover="MM_swapImage('Image20','','/images2/register/PS_pressed.jpg',1)" onmouseout="MM_swapImgRestore()"
													href="#"><IMG id="Image20" onclick="MM_showHideLayers('Layer4','','show')" height="18" src="/images2/register/PS.jpg"
														width="70" border="0" name="Image20"></A></TD>
											<TD class="blackbold" align="center" width="220" bgColor="#dbdcd7"><INPUT id="chkPP" onclick="PP_Click()" type="checkbox"><A onmouseover="MM_swapImage('Image21','','/images2/register/PP_pressed.jpg',1)" onmouseout="MM_swapImgRestore()"
													href="#"><IMG id="Image21" onclick="MM_showHideLayers('Layer5','','show')" height="18" src="/images2/register/PP.jpg"
														width="70" border="0" name="Image21"></A></TD>
										</TR>
										<TR>
											<TD class="blackbold" align="right" bgColor="#999999" height="190"><LABEL class="blackcontent" for="CB_4"></LABEL><BR>
												<asp:panel id="pnHDPE" runat="server" DESIGNTIMEDRAGDROP="5814" visible="true"></asp:panel></TD>
											<TD bgColor="#999999"><LABEL class="blackcontent" for="CB_10"><asp:panel id="pnLDPE" runat="server" visible="true"></asp:panel>
												</LABEL></TD>
											<TD bgColor="#999999"><LABEL class="blackcontent" for="CB_14"><asp:panel id="pnLLDPE" runat="server" visible="true"></asp:panel>
												</LABEL></TD>
											<TD bgColor="#999999"><LABEL class="blackcontent" for="CB_20"></LABEL><BR>
												<asp:panel id="pnPS" runat="server" visible="true"></asp:panel></TD>
											<TD bgColor="#999999"><LABEL class="blackcontent" for="CB_24"><asp:panel id="pnPP" runat="server" visible="true"></asp:panel>
												</LABEL></TD>
										</TR>
									</TABLE>
									<TABLE height="20" cellSpacing="0" cellPadding="0" width="778" align="center" border="0">
										<TR>
											<TD background="/images2/orangebar_background.jpg">
												<P class="blackbold">I�m primarily interested in:
												</P>
											</TD>
										</TR>
									</TABLE>
									<TABLE cellSpacing="1" cellPadding="0" width="780" border="0">
										<TR>
											<TD class="blackcontent" align="center" bgColor="#999999">Please provide any 
												additional information so that we can better serve you:<BR>
												<asp:textbox id="txtComment" runat="server" maxlength="499" Width="630px" DESIGNTIMEDRAGDROP="6194"
													TextMode="MultiLine" Rows="3" Columns="50"></asp:textbox>
												<TABLE height="30" cellSpacing="0" cellPadding="0" width="300" border="0">
													<TR>
														<TD align="center" width="150"><A onmouseover="MM_swapImage('Image15','','/images2/register/back_pressed.jpg',1)"
																onmouseout="MM_swapImgRestore()" href="#"></A><asp:imagebutton id="ImageButton1" runat="server" ImageUrl="/images2/register/back.jpg"></asp:imagebutton></TD>
														<TD align="center" width="150"><A onmouseover="MM_swapImage('Image16','','/images2/register/register_pressed.jpg',0)"
																onmouseout="MM_swapImgRestore()" href="#"></A><asp:imagebutton id="ImageButton2" runat="server" ImageUrl="/images2/register/register.jpg"></asp:imagebutton></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE></TD>
			</TR>
			</TD></TR></TBODY></table>
		</asp:panel></TBODY></TABLE><TPE:TEMPLATE id="Template2" Runat="server" Footer="true"></TPE:TEMPLATE></form>
