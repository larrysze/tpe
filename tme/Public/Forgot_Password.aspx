
<%@ Page language="c#" Codebehind="Forgot_Password.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.Forgot_Password" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master"%>
<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master"%>


<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphHeading"><div class="Header Bold Color1">Forgot your password?</div></asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
<center>
	<asp:Panel id="pnDirections" runat="server">
	<br> 
<P class ="Content">Enter your email address here and press the button. <BR><BR>

<asp:TextBox id=txtEmail CssClass="InputForm" runat="server"></asp:TextBox>&nbsp;
<asp:ImageButton id=btnSend runat="server" ImageUrl="/images/buttons/my_password_orange.jpg" ImageAlign="Middle"></asp:ImageButton></P>
	</asp:Panel>
	<asp:Panel id="pnThanks" visible="false" runat="server">
<P class = "Content">
<asp:Label id=lblResults runat="server"></asp:Label><BR><BR></P>
	</asp:Panel>
</center>
     <br><br>
	<P></P>
</asp:Content>
