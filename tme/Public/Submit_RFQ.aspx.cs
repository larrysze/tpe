using System;
using System.Web.Mail;
using System.Collections;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for Submit_RFQ.
	/// </summary>
	public partial class Submit_RFQ : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, EventArgs e)
		{
			// logged on users should be directed to the Resin_Entry screen
			Response.Redirect("/spot/Resin_Entry.aspx");
		
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
