<%@ Page Language="c#" CodeBehind="Contact_Us.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.Contact_Us" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" Title="The Plastics Exchange: Commodity Plastic Resin, Buy Resin, Sell Resin, Plastic Materials"%>
<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
	<style type="text/css">
	#content { MARGIN: 6px; WIDTH: 768px }
    .Content { text-align: right } 	
    </style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphJavaScript">	<script language="javascript">
			<!--
			if(document.all && !document.getElementById) 
			{
				document.getElementById = function(id) { 
					return document.all[id]; 
				}
			}
function TABLE2_onclick() {

}

			//-->
	</script>
	</asp:Content>
	
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphHeading">
    <div class="Header Bold Color1">Contact us</div>
</asp:Content>
	
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
<div style="width:780px">
		<asp:Panel id="pnMain" runat="server">
		<br />
			<table id="Table1" cellspacing="1" cellpadding="0" width="400" align="center" border="0">
				<tr>
					<td>
						<table width="400" cellspacing="3" cellpadding="3" border="0" id="TABLE2" onclick="return TABLE2_onclick()">
							<tr>
								<td width="83" valign="top"><p class="Content Bold Color2">Address:</p>
								</td>
								<td width="313">
									<p class="Content Color2" style="text-align:left">The Plastics Exchange<br />
										710 North Dearborn<br />
										Chicago, Illinois 60610</p>
								</td>
							</tr>
							<tr>
								<td><p class="Content Bold Color2">Phone:</p></td>
								<td>
									<p class="Content Color2" style="text-align:left"><%=ConfigurationSettings.AppSettings["PhoneNumber"].ToString() %></p>
								</td>
							</tr>
							<tr>
								<td>
										<p class="Content Bold Color2">Fax:</p>
									</td>
								<td>
									<p class="Content Color2" style="text-align:left"><%=ConfigurationSettings.AppSettings["FaxNumber"].ToString() %></p>
								</td>
							</tr>
							<tr>
								<td valign="top">
										<p class="Content Bold Color2">Email:</p>
									</td>
								<td>
									<p class="LinkNormal Color2" style="text-align:left"><a href="mailto:info@ThePlasticsExchange.com">info@ThePlasticsExchange.com</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</p>
								</td>
							</tr>
						</table>
						<table width="400" cellspacing="3" cellpadding="3" border="0">
							<tr>
								<td width="78" valign="top">
										<p class="Content Bold Color2">Name:</p>
									</td>
								<td align="left" width="318">
									<asp:TextBox CssClass="InputForm" id="txtName" runat="server" Width="200"></asp:TextBox></td>
							</tr>
							<tr>
								<td valign="top">
										<p class="Content Bold Color2">Email:</p>
									</td>
								<td align="left">
									<asp:TextBox CssClass="InputForm" id="txtEmail" runat="server" Width="200"></asp:TextBox>
									<asp:RegularExpressionValidator CssClass="Content Color3" id="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email " ControlToValidate="txtEmail"
										ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
							</tr>
							<tr>
								<td valign="top">
										<p class="Content Bold Color2">Comments:</p>
									</td>
								<td align="left">
									<asp:TextBox CssClass="InputForm" id="txtComments" runat="server" Rows="5" cols="45" TextMode="Multiline" Width="200"></asp:TextBox></td>
							</tr>
							<tr>
								<td></td>
								<td align="left">
									<asp:ImageButton id="btnSubmit" runat="server" BorderStyle="None" ImageUrl="/images2/submit_unpressed.jpg"></asp:ImageButton></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br /> <!--
	<tr bgcolor=#E8E8E8>
    	<td background="../Images/Bars/BG.gif" bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3>Career
        Opportunities with The Plastics Exchange
		</td>
    </tr>
	<tr bgcolor=#E8E8E8>
		<td><br>
<font size=2><b>Sales Professionals</b></font> -
Direct plastics sales and customer coordination for accounts within North America.<br><br>
<u>Responsibilities:</u>  Coordinate and manage all aspects of Sales that would include:<br>
<li>Master our unique technology and business model
<li>Identify and contact prospective accounts within the plastics industry
<li>Develop sales and marketing strategies for current business and potential accounts
<li>Negotiate pricing and long-term supply contracts with customers
<li>Coordinate all order activity between customer service and customers
<li>Direct customer involvement for planning and scheduling orders
<li>Maintain computer database on current and prospective accounts
<li>Actively manage customer receivables and coordinate collections<br><br>
<u>Minimum Qualifications:</u>
<li>Bachelors Degree in Business, Marketing or Distribution
<li>Sales or work related experience in a plastics environment
<li>General knowledge or experience with Windows 98 Word and Excel
<li>Excellent business communication and people skills at all levels
<li>Desire to work in an entrepreneurial, results driven business
<li>Ability to manage multi-task environment
<br><br><br>
<font size=2><b>Internship Opportunities</b></font> -
Seeking Chicago-based undergraduate or graduate Business and Computer Science majors.<br><br><br>
Please send cover letter, resume and salary history (where applicable) by e-mail to <a href=mailto:info@theplasticsexchange.com>info@ThePlasticsExchange.com</a>, or by regular mail or fax to the above-listed information.
		</td>
	</tr>
	--></asp:Panel>
		<asp:Panel id="pnThankYou" runat="server" visible="False">
		    <div style="text-align:center">
			    <br />
			    <br />
			
		    	<span class="Header Color2">Thank you for taking the time to contact us.<br /> If 
						requested, we will contact you shortly.</span>
			    <br />
			    <br />
			    <span class="LinkNormal Color2"><a href="/default.aspx">Return to the home page</a></span><br />
			    <br />
		    </div>
		</asp:Panel>
		</div>
</asp:Content>