<%@ Page language="c#" Codebehind="Report_Form.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.Report_Form" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">

<div class="DivTitleBarMenu"><asp:label id="lblTitle" Runat="server" CssClass="Header Color1 Bold">Financial Reports</asp:label></div>
			<asp:panel id="pnlEnter" Runat="server">
				<TABLE id="Table1" width="100%" align="center">
					<TR>
						<TD colSpan="2">
							<asp:validationsummary id="ValidationSummary1" runat="server" HeaderText="There are problems with the following fields. Please correct the errors below."
								Width="487px" Height="92px"></asp:validationsummary></TD>
					</TR>
					<TR>
						<TD align="left" width="30%">
							<asp:Label CssClass="Content Color2 Bold" id="lblReportTitle" Runat="server">Reports:&nbsp;&nbsp;</asp:Label>
							<asp:DropDownList CssClass="InputForm" id="ddlReportType" Runat="server" AutoPostBack="True" onselectedindexchanged="ddlReportType_SelectedIndexChanged">
								<asp:ListItem Value="1" Selected="True">Income Statement</asp:ListItem>
								<asp:ListItem Value="2">Balance Sheet</asp:ListItem>
							</asp:DropDownList></TD>
						<TD align="left">
							<asp:Label CssClass="Content Color2 Bold" id="lblMonthTitle" Runat="server">Month:&nbsp;&nbsp;</asp:Label>
							<asp:DropDownList CssClass="InputForm" id="ddlMonth" Runat="server" AutoPostBack="True" onselectedindexchanged="ddlMonth_SelectedIndexChanged"></asp:DropDownList></TD>
					</TR>
				</TABLE>
			</asp:panel><asp:panel id="pnlMain" Runat="server" Visible="false">
				<TABLE id="Table2" width="100%">
					<TR>
						<TD align="left">
							<asp:Label CssClass="Content Color2" id="lblNameOfParameter" Runat="server"></asp:Label>
							<asp:TextBox CssClass="InputForm" id="txtParameter" Runat="server" Visible="false" MaxLength="10"></asp:TextBox>
							<asp:RangeValidator CssClass="Content Color3 Bold" id="RangeValidator0" runat="server" ControlToValidate="txtParameter" ErrorMessage="invalid value of Overhead or Average Inventory Valuation"
								MinimumValue="0" MaximumValue="999999999" Type="Currency" Display="None"></asp:RangeValidator>
							<asp:RequiredFieldValidator CssClass="Content Bold Color3" id="RequiredFieldValidator0" Runat="server" ControlToValidate="txtParameter" ErrorMessage="Overhead or Average Inventory Valuation required"
								Display="None"></asp:RequiredFieldValidator></TD>
					</TR>
					<TR>
						<TD align="center" valign="Top"><br />
							<asp:DataGrid BackColor="#000000" CellSpacing="1" BorderWidth="0" id="dg" runat="server" Width="740px" Height="123px" Visible="False" 
								AutoGenerateColumns="False" HorizontalAlign="Center" GridLines="None">
								<AlternatingItemStyle CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
								<ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
								<HeaderStyle HorizontalAlign="Center" CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
								<Columns>
									<asp:BoundColumn DataField="ACCOUNT_DATE_TITLE" HeaderText="Date"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="ACCOUNT_TRADING" HeaderText="Trading ACB ($)"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="ACCOUNT_OPERATION" HeaderText="Operatings ACB ($)"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="ACCOUNT_COLLATERAL" HeaderText="Collateral ACB ($)"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="ACCOUNT_TPE_CREDIT" HeaderText="TPE Credit ACB ($)"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="ACCOUNT_MAG_CREDIT" HeaderText="MAG Credit ACB ($)"></asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Trading ACB ($)">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:TextBox CssClass="InputForm" id="TextBox1" runat="server" Width="100px" Visible="True"></asp:TextBox>
											<asp:RangeValidator id="Rangevalidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="Trading ACB - invalid value"
												MinimumValue="-999999999" MaximumValue="999999999" Type="Currency" Display="None"></asp:RangeValidator>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Operatings ACB ($)">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:TextBox CssClass="InputForm" id="TextBox2" runat="server" Width="100px" Visible="True"></asp:TextBox>
											<asp:RangeValidator id="Rangevalidator2" runat="server" ControlToValidate="TextBox2" ErrorMessage="Operating ACB - invalid value"
												MinimumValue="-999999999" MaximumValue="999999999" Type="Currency" Display="None"></asp:RangeValidator>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Collateral ACB ($)">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:TextBox CssClass="InputForm" id="TextBox3" runat="server" Width="100px" Visible="True"></asp:TextBox>
											<asp:RangeValidator id="RangeValidator3" runat="server" ControlToValidate="TextBox3" ErrorMessage="Collateral ACB - invalid value"
												MinimumValue="-999999999" MaximumValue="999999999" Type="Currency" Display="None"></asp:RangeValidator>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="TPE Credit ACB ($)">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:TextBox CssClass="InputForm" id="TextBox4" runat="server" Width="100px" Visible="True"></asp:TextBox>
											<asp:RangeValidator id="Rangevalidator4" runat="server" ControlToValidate="TextBox4" ErrorMessage="TPE Credit - invalid value"
												MinimumValue="-999999999" MaximumValue="999999999" Type="Currency" Display="None"></asp:RangeValidator>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="MAG Credit ACB ($)">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:TextBox CssClass="InputForm" id="TextBox5" runat="server" Width="100px" Visible="True" MaxLength="10"></asp:TextBox>
											<asp:RangeValidator id="Rangevalidator5" runat="server" ControlToValidate="TextBox5" ErrorMessage="MAG Credit - invalid value"
												MinimumValue="-999999999" MaximumValue="999999999" Type="Currency" Display="None"></asp:RangeValidator>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn Visible="False" DataField="ACCOUNT_ENTERED"></asp:BoundColumn>
								</Columns>
							</asp:DataGrid>
							<br />
							<asp:Button CssClass="Content Color2" id="btnUpdate" Runat="server" Text="Update and View Report" onclick="btnUpdate_Click"></asp:Button>
							</TD>
						<TD vAlign="top" width="400" class="LinkNormal" align="left"><B><U>Steps to Update Daily Account Balances</U></B>
							<br />
							Data entered is �End of Previous Business Day�
							<br />
							<br />
							<FONT color="red">Attention: Ignore decimals when copying number!</FONT>
							<br />
							<br />
							<U>1. Go to ACB website:</U>
							<br />
							<A href="https://americanchartered.portalvault.com/">https://americanchartered.portalvault.com/</A><br />
							<br />
							<U>2. Select:
								<br />
							</U>&nbsp;&nbsp;&nbsp;&nbsp;eCorp � Corporate Cash Management
							<br />
							&nbsp;&nbsp;&nbsp;&nbsp;Access ID: ------
							<br />
							&nbsp;&nbsp;&nbsp;&nbsp;Pass: -------
							<br />
							&nbsp;&nbsp;&nbsp;&nbsp;Log-In
							<br />
							<br />
							<U>3. Select: Accounts : Under �List� </U>
							<br />
							<br />
							a)<U>Select �Trading Account�</U>
							<br />
							Under Transactions Select �Previous Business Day�
							<br />
							Copy &amp; Paste Ending Balance into daily spreadsheet
							<br />
							<br />
							If message is WARNING �No information available�
							<br />
							Copy &amp; Paste Previous balance sheet entry into spreadsheet<br />
							<br />
							b)<U>Select �Operating Account�</U>
							<br />
							Under Transactions Select �Previous Business Day�
							<br />
							Copy &amp; Paste Ending Balance into daily spreadsheet
							<br />
							<br />
							If message is WARNING �No information available�
							<br />
							Copy &amp; Paste Previous balance sheet entry into spreadsheet
							<br />
							<br />
							c)<U>Select �Cash Collatoral Account�</U>
							<br />
							Under Transactions Select �Previous Business Day�
							<br />
							Copy &amp; Paste Ending Balance into daily spreadsheet
							<br />
							<br />
							If message is WARNING �No information available�
							<br />
							Copy &amp; Paste Previous balance sheet entry into spreadsheet
							<br />
							<br />
							d)<U>Select: Loan TPE # 228768401 line = $1.5M</U>
							<br />
							Select Principal Balance
							<br />
							Copy &amp; Paste it into daily spreadsheet
							<br />
							<br />
							e)<U>Select: Loan MAG # 283712901 line = $ 4.0M</U>
							<br />
							Select Principal Balance
							<br />
							Deduct MG Personal loan amount ($ 1,209,814)
							<br />
							Add Result it into daily spreadsheet
							<br />
							<br />
							<U>8. Log off when you are done.</U>
						</TD>
					</TR>
				</TABLE>
				<br />
				<br />

			</asp:panel><br />
</asp:Content>