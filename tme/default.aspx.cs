using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace localhost
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            if (Request.UserHostAddress.ToString() == ConfigurationSettings.AppSettings["OfficeIP"].ToString())
            {
                LivePerson.Visible = false;
            }

            LivePerson.Text = "<script language=\"JavaScript1.2\"> ";
            LivePerson.Text += "if (typeof(tagVars) == \"undefined\") tagVars = \"\"; ";
            string strUserName = string.Empty;
            if (Session["UserName"] != null) strUserName = Session["UserName"].ToString();
            LivePerson.Text += "tagVars += \"&VISITORVAR!User%20Name=" + strUserName + "\";";
            LivePerson.Text += "<" + "/SCRIPT>";
            LivePerson.Text += "<script language='javascript' src='https://server.iad.liveperson.net/hc/92199096/x.js?cmd=file&file=chatScript3&site=92199096&&category=en;woman;5'>";
            LivePerson.Text += "<" + "/SCRIPT>";



        }


        protected void Button_Login(object sender, EventArgs e)
        {
            // Saving information into session object to be passed on to the login function
            Session["UserName"] = txtEmail.Text;
            Session["Password"] = Crypto.Encrypt(txtPassword.Text);
            Response.Redirect("/market/Common/FUNCLogin.aspx");
        }

    }
}
