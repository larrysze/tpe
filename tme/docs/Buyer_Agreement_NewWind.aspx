<%@ Page Language="C#"%>
<%
   /*****************************************************************************
   '*   1. File Name       : docs\Buyer_Agreement_NewWind.aspx                  *
   '*   2. Description     : Buyer agree agreement, accessed from trading floor *
   '*			     when logged on as a buyer                          *
   '*						                                *
   '*   3. Modification Log:                                                    *
   '*     Ver No.       Date          Author             Modification           *
   '*   -----------------------------------------------------------------       *
   '*      1.00       2-26-2004       Xiaoda             Comment                *
   '*                                                                           *
   '*****************************************************************************/
%>

<table border=0 cellspacing=0 cellpadding=0 bgcolor=#E8E8E8>
<tr><td nowrap bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td><td nowrap bgcolor=white width=100%><img src=/images/1x1.gif height=8 border=0></td><td nowrap bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td nowrap bgcolor=#000000><img src=/images/1x1.gif width=1></td><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3>Participation Application and Agreement for Buyer<img src=/images/1x1.gif width=300 height=1 border=0>(<a CLASS=Menu href=/docs/buyer.pdf target=_blank> PDF version </a>)&nbsp;&nbsp;</td><td nowrap bgcolor=#000000><img src=/images/1x1.gif width=1></td></tr>
<tr>
<td nowrap bgcolor=#000000><img src=/images/1x1.gif width=1></td>
<td>
&nbsp;<a href=#A CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;Incorporation of The Plastics Exchange Policies ("TPE Policies")</a><br>
&nbsp;<a href=#B CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;System Operations</a><br>
&nbsp;<a href=#C CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;Representations, Warranties and Covenants of the Participant</a><br>
&nbsp;<a href=#D CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;Operational Procedures</a><br>
&nbsp;<a href=#E CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;Indemnification</a><br>
&nbsp;<a href=#F CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;Suspension</a><br>
&nbsp;<a href=#G CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;Term and Termination</a><br>
&nbsp;<a href=#H CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;Assignment</a><br>
&nbsp;<a href=#I CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;Partial Invalidity</a><br>
&nbsp;<a href=#J CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;Notices</a><br>
&nbsp;<a href=#K CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;Applicable Law</a><br>
&nbsp;<a href=#L CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;Jurisdictional Consent</a><br>
&nbsp;<a href=#M CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;B2BExchangeCredit, Demand Credit Agreement</a><br>
&nbsp;<a href=#N CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;Secretary's certificate as to the adoption of corporate resolutions</a><br>
&nbsp;<a href=#O CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;Appendix I</a><br>
&nbsp;<a href=#P CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;Appendix II</a><br>
&nbsp;<a href=#Q CLASS=S2><img src='/images/icons/arRight.gif' border=0>&nbsp;Appendix III</a>
</td>
<td nowrap bgcolor=black><img src=/images/1x1.gif width=1></td>
</tr>
<tr><td colspan=3 bgcolor=black><img src=/images/1x1.gif height=1></td></tr>
</table>

<table border=0 cellspacing=0 cellpadding=0 bgcolor=#E8E8E8>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td>
________________________________ ("Participant"), by completing this Application and Agreement ("Agreement"), applies to ThePlasticsExchange.com, LLC ("TPE") for the right of access to the electronic trading system operated by TPE ("The Exchange"). Participant understands and agrees that this Agreement is made pursuant to and subject to the TPE Policies set forth in Appendix I as amended from time to time.
</td></tr>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5 valign=bottom><img src=/images/1x1.gif width=3><a name=A href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;Incorporation of The Plastics Exchange Policies ("TPE Policies")</td></tr>
<tr><td>
This Agreement incorporates the TPE Policies as amended from time to time and makes the TPE Policies part of this Agreement in their entirety. In the event of a conflict between the terms and conditions of this Agreement and any other terms and conditions set forth in the TPE Policies, the terms and conditions of the TPE Policies shall apply.
</td></tr>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5 valign=middle><img src=/images/1x1.gif width=3><a name=B href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;System Operations</td></tr>
<tr><td>
<br><div class=S1>Services Provided by TPE</div>TTPE will provide Participant with access to The Exchange via the Internet or otherwise to engage in the buying of certain generic and branded plastic resin product ("Products") in the cash market in the form of standardized contracts ("Contracts"). TPE may add new contracts or delete existing contracts at any time. The Exchange will allow Participant to enter, modify or cancel orders to purchase Contracts, to obtain information from The Exchange and to communicate with The Exchange via the Internet. The Exchange will provide a trade matching function to match Participant's orders with any eligible sell orders existing in The Exchange. The Exchange will communicate all successfully matched orders to Participant. TPE will also provide account management services, including freight, logistics and a credit function for facilitation of payment for transactions.
<br><br><div class=S1>Designated Representatives</div>Each Participant may designate certain of its employees as representatives to be authorized to access The Exchange ("Designated Representatives"). These Designated Representatives will also be able to enter, modify or cancel orders and transmit instructions to TPE.
<br><br><div class=S1>Password and User ID</div>Participant will select an individual User ID and Password, subject to TPE's approval, with which to access the system. Participant will also select an individual Password, subject to TPE's approval, for each Designated Representative of a Participant.
<br><br><div class=S1>Electronic Equipment</div>Participant acknowledges that it is responsible for providing all computer equipment, technology and communication lines to access The Exchange and is responsible for paying all related costs, charges and fees.
</td></tr>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3><a name=C href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;Representations, Warranties and Covenants of the Participant</td></tr>
<tr><td><br><div class=S1>Basic Representations and Warranties</div>Participant represents and warrants that as of the date of this Agreement:
<li>it is duly organized, validly existing and in good standing under the jurisdiction of its organization, with full power and authority to enter into and perform its obligations under this Agreement;</li>
<li>it, through an authorized officer, has validly executed this Agreement;</li>
<li>this Agreement constitutes its legal, valid and binding obligation, enforceable against it in accordance with its terms, except as the enforcement may be limited by applicable bankruptcy, reorganization, insolvency or similar laws affecting the enforcement of creditors' rights generally; and</li>
<li>4.	its entry into this Agreement and the performance of its obligations hereunder will not violate, conflict with, or cause a default under any of its organizational documents, any material contractual covenant or restriction by which it is bound, or any applicable law, regulation, rule, ordinance, order, judgment, or decree.</li>
<br><br><div class=S1>Participant's Transactions</div>Participant warrants that:
<li>every purchase by it of a Product will be for its own account or, if it is authorized and/or licensed to do so legally, for another entity;</li>
<li>it is capable of accepting and will accept delivery of all purchases made by it pursuant to this Agreement and the TPE Policies;</li>
<li>it is an end user or licensed distributor, reseller, compounder or broker of plastic resin products;</li>
<li>it is not an individual speculator, commodity pool, passive collective investment vehicle or other non-commercial entity;</li>
<li>it will not use its access to engage in trading futures contracts that are subject to regulation by the Commodity Futures Trading Commission under the Commodity Exchange Act, as amended, or for speculative purposes;</li>
<li>it is in good financial standing such that it is now and will remain fully able to satisfy its financial obligations in every transaction in which it engages; and</li>
<li>except as may be excused due to an uncontrollable force or for a bona fide commercial reason, each purchase it makes through TPE shall be for actual delivery.</li>
<br><br><div class=S1>Performance by Participant</div>Participant covenants to abide by, and perform all of its obligations required under the TPE Policies and this Agreement including, without limitation, all matters relating to the trading of Contracts, and all ongoing obligations with respect to bidding, settlement, delivery, security requirements, billing and payments, confidentiality and dispute resolution.
<br><br><div class=S1>Right to Reject Delivery</div>Participant may only reject delivery of a Contract where the Product delivered does not meet the standardized contract specifications set forth in Appendix II as amended from time to time, in accordance with TPE Policy 4.
<br><br><div class=S1>Financial Standing</div>Participant represents that it is in good financial condition. Participant further represents that it will provide TPE with sufficient information to allow TPE to verify such condition. If TPE is not satisfied with Participant's financial condition, Participant will be given the opportunity to provide additional security to TPE in such form as TPE may, in its sole discretion, desire, including security in the form of a performance bond or an irrevocable letter of credit for the benefit of TPE. TPE reserves the right to refuse access rights to any person, in its sole discretion, based on its financial condition or otherwise.
<br><br><div class=S1>Financial Credit Guarantee</div>Participant agrees to secure appropriate financing from B2BExchangeCredit, LLC, the financial partner of TPE, to ensure its credit worthiness to enter any order on The Exchange. Participant represents that it will not enter orders in an amount that exceeds Participant's line of credit from B2BExchangeCredit, LLC. Participant acknowledges that it will not be provided access to enter orders on The Exchange prior to obtaining such financing.
<br><br><div class=S1>Payment</div>Buyer represents that it will pay for all Contracts delivered to and accepted by Participant in accordance with the B2BExchangeCredit, LLC Financing Agreement.
<br><br><div class=S1>Rail Car Fee</div>For all rail deliveries, Participant agrees to release the rail car within 60 days of constructive placement of such rail car with Participant. Participant acknowledges and agrees to pay a penalty fee of $35.00 per day for each day Participant retains such rail car past 60 days.
</td></tr>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3><a name=D href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;Operational Procedures</td></tr>
<tr><td>
<br><div class=S1>Reliance Upon Instructions</div>Participant acknowledges and consents to TPE's acceptance, execution and/or reliance upon any order or instruction from Participant or any of its Designated Representatives that is entered into The Exchange under the Participant's User IDs and Password. TPE shall have no responsibility or liability for any errors which may occur in the course of any electronic transmission to The Exchange. Participant shall indemnify, defend and hold TPE harmless for information entered into The Exchange using the User ID and Password selected by the Participant and/or its Designated Representative or actions taken upon the instruction of Participant or its Designated Representatives.
<br><br><div class=S1>Recordings</div>Participant expressly grants to TPE the right to tape, record or otherwise monitor, store, review and use all communications, electronic or otherwise, between the Participant and TPE, its agents and successors, without limitation.
<br><br><div class=S1>Market Integrity</div>Participant irrevocably and without reservation consents to the use by TPE of all information (including Participant's identity) regarding bids, offers, matches, its credit information, and all information relating to any of the foregoing in connection with the monitoring and maintaining the integrity of The Exchange. Participant also irrevocably and without reservation consents to the use by TPE of all such information received from Participant, except Participant's identity, for all other uses.
<br><br><div class=S1>Power of Attorney</div>Participant hereby irrevocably constitutes and appoints TPE and any of its officers or agents, with full power of substitution, as its attorney-in-fact with full power and authority in the name of Participant for the purpose of (a) correcting errors and mitigating potential losses arising from errors and claimed errors and (b) upon the occurrence and during the continuance of a default by Participant in meeting its obligations under this Agreement (including, without limitation, the obligation to provide and maintain security in accordance with the terms of this Agreement), canceling orders, liquidating positions and making trades for the purpose of reducing the credit exposure or fulfilling the obligations of Participant.
<br><br><div class=S1>Confidentiality</div>Participant acknowledges that all information provided by TPE on The Exchange is confidential. Participant warrants that it will not make any such information obtained via The Exchange available to third parties. Participant covenants to keep confidential and not publish, broadcast, retransmit, reproduce, commercially exploit, or otherwise redisseminate the data, information, or services provided under the Agreement or received via the Internet. Participant covenants to keep confidential its User IDs, Password(s), and other security data, methods and devices.
<br><br><div class=S1>Consents and Waivers Not Comprehensive</div>The consents, waivers and releases contained in this Section are not intended to be an exclusive and comprehensive enumeration of all consents, waivers and releases granted by the Participant pursuant to this Agreement or TPE Policies.
<br><br><div class=S1>Right of Inspection</div>Participant expressly grants to TPE the right to inspect Participant's facilities upon reasonable notice. Participant acknowledges that TPE may inspect such facilities on a repeated basis.
</td></tr>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3><a name=E href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;Indemnification</td></tr>
<tr><td>
Participant agrees to defend, indemnify and hold TPE, its officers, directors, employees and agents harmless from and against any claims, losses, liabilities, costs and expenses (including but not limited to attorney's fees) arising out of: any breach by Participant or its Designated Representative of any representation, warranty or covenant made in connection with this Agreement; any violation by Participant or its Designated Representative of the TPE Policies; any violation by Participant or its Designated Representative of any third party rights including but not limited to copyright, proprietary and privacy right; and, if Participant allows third parties to access TPE's services, any claim or suit by such third parties based upon or related to such access and use. The terms of this indemnification shall survive the termination of this Agreement.
</td></tr>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3><a name=F href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;Suspension</td></tr>
<tr><td>
TPE reserves the right to suspend Participant's access to the Exchange immediately if TPE, in its sole discretion, determines that such suspension is in the best interests of The Exchange in order to preserve the integrity of the market.
</td></tr>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3><a name=G href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;Term and Termination</td></tr>
<tr><td>
<br><div class=S1>Term</div>This Agreement shall commence on the later of the date this Agreement is executed or the date Participant is approved by TPE, and shall continue until terminated in accordance with the Termination provisions below.
<br><br><div class=S1>Termination</div>This Agreement may be terminated by either party upon one business day's prior written notice to the other party, subject to the survival of all payment, delivery and other obligations to which the Participant is subject at the time of termination. TPE may terminate this Agreement immediately if Participant violates any Policy of TPE. Participant agrees that its status as a Participant is at all times subject to this Agreement and, in particular, that its access to The Exchange may be terminated in accordance with this Agreement.
<br><br><div class=S1>Termination is Not Waiver</div>The termination of this Agreement, either by TPE or by Participant, shall not cancel, void, satisfy or waive any payment or other obligation owing by Participant to TPE or to any other entity.
</td></tr>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3><a name=H href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;Assignment</td></tr>
<tr><td>
None of the rights or obligations under this Agreement may be assigned or delegated by Participant, whether by operation of law or otherwise, without the prior written consent of TPE, which consent shall not be unreasonably withheld, delayed or conditioned; provided that TPE may, in its sole discretion, consent to an assignment by Participant of any of Participant's obligations under this Agreement. TPE may assign or delegate its rights or obligations under this Agreement.
</td></tr>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3><a name=I href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;Partial Invalidity</td></tr>
<tr><td>
If any provision of this Agreement, or the application of such provision to any person, circumstance or transaction, shall be held invalid, the remainder of this Agreement, or the application of such provision to other persons, circumstances or transactions, shall not be affected. The partial invalidity of this Agreement shall not cancel, void, satisfy or waive any payment owing by and due from Participant to TPE or to any other entity.
</td></tr>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3><a name=J href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;Notices</td></tr>
<tr><td>
Any notice, demand or request made to or by either party regarding this Agreement shall be made in accordance with this Agreement and the TPE Policies and unless otherwise stated or agreed shall be made to the representative of the other party indicated below:
<table border=0>
<tr><td>ThePlasticsExchange.com, LLC</td><td>Scott E. Early</td></tr>
<tr><td>Member Services</td><td>Foley & Lardner</td></tr>
<tr><td>710 North Dearborn</td><td>330 North Wabash Avenue Suite 3330</td></tr>
<tr><td>Chicago, IL 60610<br><br>Participant: To the address set forth above</td><td valign=top>Chicago, IL  60610</td></tr>
</table>
Any general notice to all Participants regarding the operation of The Exchange, including the addition or deletion of Contracts, that is posted on TPE's electronic website shall be considered adequate notice.
</td></tr>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3><a name=K href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;Applicable Law</td></tr>
<tr><td>This Agreement shall be governed by and construed in accordance with the laws of the State of Illinois in all disputes arising out of and in connection with this Agreement and shall be subject to the jurisdiction of the U.S. District Court for the Northern District of Illinois and the Circuit Court of Cook County, Illinois.
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3><a name=L href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;Jurisdictional Consent</td></tr>
<tr><td>
I agree to be bound by the Policies of The Exchange and pledge myself to abide by the same and all subsequent amendments to the Policies, and by all regulations now existing, or which may hereafter be adopted by The Exchange.  I also consent to the jurisdiction of The Exchange for all matters relating to my access and use of The Exchange as governed by the TPE Policies.
<center><hr width="100"></center>
Participant, in executing this Application and Agreement, confirms that all information provided by Participant is true and complete and agrees to each and all terms contained in this Agreement.  In addition, Participant's signature represents agreement to abide by the TPE Policies as they now exist or may from time to time be amended by electronic notification to all Participants.  Each Participant will be given the opportunity to terminate its participation in The Exchange because of such amendment.  Any Participant that does not elect to terminate its participation will be deemed to have accepted the amended terms.  Further, if Participant has any Designated Representatives, Participant represents that they will observe and be bound by the TPE Policies.
<table border=0>
<tr><td>&nbsp;</td><td>&nbsp;</td><td colspan=2>_________________________________________________________</td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td><td>Participant</td><td>&nbsp;</td></tr>
<tr><td>Date :</td><td>_______________</td><td>By :</td><td>________________________________________________</td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td><td>Name :</td><td>________________________________________________</td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td><td>Title :</td><td>________________________________________________</td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
</table>
</td></tr>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3><a name=M href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;B2BExchangeCredit, Demand Credit Agreement</td></tr>
<tr><td>
THIS _________ day of _________, 20____, B2BExchangeCredit, LLC, a Delaware limited liability company (the "Lender") is pleased to confirm that the Lender may, in its sole and absolute discretion, make and extend credit to ____________________________ [BORROWER] (the "Borrower") from time to time, on the following terms and conditions :
<br><br><div class=S1>Line of credit amount and terms</div>Purpose: The Borrower has applied to and may be accepted as a member of the Internet exchange commonly known as "ThePlasticsExchange.com" ("The Exchange"), on which parties buy and sell plastics (the "Products"). The purpose of the line of credit established pursuant to this Agreement is to finance and clear the Borrower's purchases from time to time on the Exchange.
<br><i>Line of Credit Amount:</i> The Borrower may borrow, repay and reborrow from the Lender to and including the first anniversary hereof (the "Termination Date"), unless sooner notified by the Lender of the termination of this line of credit, such amounts (the "Advances") as the Borrower may from time to time request, but not exceeding the credit limit established by the Lender from time to time, in the Lender's sole and absolute discretion.  Such Advances shall be for the account of the borrower and/or paid at the direction of The Exchange.
<br><i>Advances:</i> The Advances will be payable on the earlier of the Termination Date or demand by the Lender.  The Lender will record all Advances made pursuant to this Agreement and all payments by the Lender in its records, which records will be rebuttable presumptive evidence of the subject matter thereof.
<br><i>Election to Make Advances:</i> The Borrower agrees that its compliance with and its performance of the provisions of this Agreement do not obligate the Lender to make any Advances and that the Lender will make any Advance in its sole and absolute discretion.
<br><i>Use of Advances; Prepayment Terms:</i> When the Borrower buys Products on the Exchange, advances will be used by the Borrower to pay the corresponding percentage of the full-face Amount, which shall include transportation ? (collectively, the "Trade Amount") that is due on the day of the payment as described in Appendix III. An Advance in the amount of the Trade Amount shall be deemed advanced on the date on which the applicable Product(s) depart the warehouse of the seller thereof (the "Advance Date"); provided, however, that if the Borrower timely and properly rejects such Products in accordance with the rules of The Exchange, then the Advance will not be deemed made with respect to such Products. Each Advance shall be repayable by the Borrower to the Lender in accordance with the schedule noted as Appendix III.
<br><br><div class=S1>Disbursements, payments and costs</div><i>Requests for Advances:</i> The Borrower's execution of a trade as a buyer on the Exchange shall constitute a request for an Advance in the Trade Amount of the Borrower's trade.
<br>Disbursements and Payments: Each Advance made by the Lender in its sole and absolute discretion and each payment by the Borrower will be made in immediately available funds and the Lender shall give no credit to the Borrower until the Lender has received collected funds.
<br><i>Taxes:</i> The Borrower will not deduct any taxes from any payments it makes to the Lender. If any government authority imposes any taxes on any payments made by the Borrower, the Borrower will pay the taxes and will also pay to the Lender, at the time interest is paid, any additional amount which the Lender specifies as necessary to preserve the after?tax yield the Lender would have received if such taxes had not been imposed. Upon request by the Lender, the Borrower will confirm that it has paid the taxes by giving the Lender official tax receipts (or notarized copies) within 30 days after the due date. The Borrower will not pay the Lender's net income taxes.
<br><br><div class=S1>Conditions</div>Prior to requesting the initial Advance, the Borrower will furnish the Lender with each of the following documents, each duly executed and dated as of the date of the Borrower's acceptance of this Agreement:
<li>Authorizations: Evidence that the Borrower's execution, delivery and performance of this agreement have been duly authorized; in the form of a Corporate Resolution and Incumbency Certificate (Exhibit B).</li>
<li>Good Standing: Certificates of good standing for the Borrower from its state of incorporation and from any other state in which the Borrower is required to qualify to conduct its business.</li>
<li>Other Items: Any other items that the Lender reasonably requires.</li>
<br><br><div class=S1>Enforcing this agreement; miscellaneous</div>
<i>Illinois Law:</i> This Agreement is governed by the internal laws of the State of Illinois.<br>
<i>Successors and Assigns:</i> This Agreement is binding on the Borrower's and the Lender's successors and assignees. The Borrower agrees that it may not assign this Agreement, without the prior written consent of the lender.<br>
<i>Severability; Waivers:</i> If any part of this Agreement is not enforceable, the rest of the Agreement may be enforced.<br>
<i>Expenses:</i> The Borrower agrees to reimburse the Lender upon demand, whether or not any Advance is made under this Agreement, for all expenses and reasonable attorneys' fees, including any allocated costs of in?house counsel, incurred by the Lender; (a) enforcing the Borrower's obligations under this Agreement, or any other document delivered in connection with this Agreement; and (b) participating in any proceeding (whether instituted by the Lender, the Borrower or any other person and whether in bankruptcy or otherwise) or responding to any claim in any way relating to this Agreement, or any document delivered in connection with this Agreement. The Borrower further agrees to pay, and save the Lender harmless from all liability for, any stamp or other taxes which may be payable with respect to the execution or delivery of this Agreement, which obligations will survive any termination of this Agreement.<br>
<i>Multiple Borrowers:</i> If two or more borrowers sign this Agreement, each will be individually obligated to repay the Lender in full, and all will be obligated together.<br>
One Agreement: This Agreement and any related other agreements required by this Agreement, collectively:
<li>represent the sum of the understandings and agreements between the Lender and the Borrower concerning this credit; and</li>
<li>replace any prior oral or written agreements between the Lender and the Borrower concerning this credit; and</li>
<li>are intended by the Lender and the Borrower as the final, complete and exclusive statement of the terms agreed to by them.</li>
In the event of any conflict between this Agreement and any other agreements required by this Agreement, this Agreement will prevail.
<br><i>Notices:</i> All notices required under this Agreement shall be personally delivered or sent by e?mail or by first class mail, postage prepaid, to the addresses set forth below or to such other addresses as the Lender and the Borrower may specify from time to time in writing.
<br><i>Headings:</i> Article and paragraph headings are for reference only and shall not affect the interpretation or meaning of any provisions of this Agreement.
<br><i>Counterparts:</i> This Agreement may be executed in as many counterparts as necessary or convenient, and by the different parties on separate counterparts each of which, when so executed, shall be deemed an original but all such counterparts shall constitute but one and the same agreement.
<br>Arbitration; Consent to Jurisdiction: To induce the Lender to accept this Agreement, the Borrower irrevocably agrees that ALL ACTIONS OR PROCEEDINGS IN ANY WAY ARISING OUT OF, FROM OR RELATED TO THIS AGREEMENT WILL BE SUBMITTED TO BINDING ARBITRATION THAT WILL BE CONDUCTED BY FEDNET, INC. AND THAT DECISIONS THEREFROM MAY BE ENFORCED BY STATE AND FEDERAL COURTS LOCATED IN THE CITY OF CHICAGO, ILLINOIS.  SUCH ARBITRATION MAY, AT LENDER'S ELECTION, BE CONSOLIDATED WITH RELATED MATTERS BEING ARBITRATED.  THE BORROWER HEREBY CONSENTS AND SUBMITS TO THE JURISDICTION OF ANY COURT LOCATED WITHIN CHICAGO, ILLINOIS, WAIVES PERSONAL SERVICE OF PROCESS AND AGREES THAT ALL SUCH SERVICE OF PROCESS MAY BE MADE BY REGISTERED MAIL DIRECTED TO THE BORROWER AT THE ADDRESS STATED ABOVE AND SERVICE SO MADE WILL BE DEEMED TO BE COMPLETED UPON ACTUAL RECEIPT.
<br><br>
WAIVER OF RIGHTS: THE BORROWER AGREES THAT IT WILL NOT ASSERT ANY CLAIM AGAINST THE LENDER ON ANY THEORY OF LIABILITY FOR SPECIAL, INDIRECT, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES.
<br><br>
If the foregoing is acceptable, please indicate the Borrower's agreement by signing a copy of this Agreement where indicated below.
<br>Very truly yours,<br>B2BEXCHANGECREDIT,<br>A Delaware limited Liability Company<br><br>
By:_________________________________________<br>
Name:______________________________________<br>
Title:________________________________________<br>
<br>
Address for Notice:
<table border=0>
<tr><td><b>B2BExchangeCredit, LLC,</b></td><td>ThePlasticsExchange.com</td></tr>
<tr><td>55 West Monroe Street, Suite 970</td><td>710 North Dearborn</td></tr>
<tr><td>Chicago, Illinois 60603</td><td>Chicago, IL 60610</td></tr>
<tr><td>e-mail: <a href=mailto:Plastics@B2BExchangeCredit.com>Plastics@B2BExchangeCredit.com</a></td><td>e-mail: <a href=mailto:Credit@ThePlasticsExchange.com>Credit@ThePlasticsExchange.com</a></td></tr>
</table>
The foregoing is agreed to this.<br>_____ day of _____________, 200_.<br>
[BORROWER]<br>
&nbsp;&nbsp;By:________________________<br>
&nbsp;&nbsp;Name:________________________<br>
&nbsp;&nbsp;Title:________________________<br>
Address for Notice:<br>
__________________________________________________<br>
__________________________________________________<br>
Attn:_____________________________________________<br>
e-mail:___________________________________________<br><br>
STATE OF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)<br>
COUNTY OF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)<br><br>
Subscribed, sworn to and acknowledged before me this ____ day of _________, 200__ by _______________________, as <br>
____________________________ of ____________________________, who personally appeared before me.<br>
Witness my hand and official seal.<br>
My commission expires:	___________ Notary Public__________________________________<br>
</td></tr>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3><a name=N href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;Secretary's certificate as to the adoption of corporate resolutions</td></tr>
<tr><td>
The undersigned hereby certifies as follows:<br>
1.	The undersigned is the duly elected, qualified and acting Secretary of ________________________________________, a________________________ corporation, and as such Secretary has custody and care of the corporate books and records of said corporation and has personal knowledge of the matters set forth herein.<br>
2.	At a special joint meeting of the Board of Directors and Shareholders of said corporation, called and held in accordance with applicable law and the provisions of the By?Laws of said corporation on ____________ , 20___, at which meeting all Directors and Shareholders were present and voting throughout, the following resolutions were unanimously adopted by the Board of Directors and the Shareholders of said corporation in accordance with the Articles of Incorporation and By?Laws of said corporation, as amended to date, and the laws of its state of incorporation:
<li>WHEREAS, this corporation or an affiliate hereof has applied to B2BExchangeCredit, LLC, a Delaware limited liability company  ("B2B"), for an extension of credit  on the terms and conditions set forth in that certain Demand Credit Agreement (the "Credit Agreement") by and between this corporation and B2B; copies of the Credit Agreement have been reviewed by the directors and shareholders of this corporation;
<li>NOW, THEREFORE, BE IT RESOLVED, that the negotiation of the Credit Agreement with B2B be and hereby is ratified, approved and confirmed in all respects, and that this corporation make, execute and deliver the Credit Agreement.
<li>FURTHER RESOLVED, that the President or any Vice President of this corporation, acting alone or together with the Secretary or any other officer of this corporation, be and hereby is, or hereby are, authorized and empowered, in the name and on behalf of said corporation, to execute and deliver the Credit Agreement and such instruments and documents as may be necessary or desirable in order to effect said extension of credit and the security therefore, containing such terms, conditions, covenants and warranties as the President or any Vice President in his or her sole discretion may approve, his or her execution and delivery thereof to constitute conclusive evidence of such approval.
<li>FURTHER RESOLVED, that the officers of this corporation be and each of them hereby is authorized and empowered to do and perform all acts and things that such officers deem advisable, necessary, expedient, convenient or proper in order to consummate fully all the transactions contemplated by these resolutions.<br>
3. The foregoing resolutions are in full force and effect on the date hereof and have not been rescinded or modified in any manner whatsoever.<br>
4. The following are the names and genuine specimen signatures of the duly elected, qualified and acting President and one of the Vice Presidents, respectively, of said corporation.<br>
	           _   __________	                            __________________________________________________
(Name of President)					(Specimen Signature of President)
	           _   __________	                            __________________________________________________
(Name of Vice President)				(Specimen Signature of Vice President)
	Executed and sealed on the ___________ day of _______________________, 20_______.

__________________________________________________

</td></tr>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3><a name=O href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;Appendix I</td></tr>
<tr><td>
Set forth below are the policies that govern the trading and operations of The Exchange. Participant, in completing and executing the Participation Application and Agreement, agrees to be subject to and comply with the following Policies.<br>
These Policies may be amended from time to time by electronic notification to all Participants. Each Participant will be given the opportunity to terminate its participation in The Exchange because of such amendment. Any Participant that does not elect to terminate its participation will be deemed to have accepted the amended terms.
<br><br><div class=S1>Electronic Orders and Information</div>
Any orders given by a Participant and any information furnished to a Participant electronically via the Internet shall be subject to the following terms and conditions:
<li>If an order has been placed through the Internet and a Participant has not received an accurate electronic confirmation of the order or of its execution in a timely manner, the Participant must immediately notify TPE.</li>
<li>If Participant has received confirmation of an order that Participant did not place or any similar conflicting report, Participant must immediately notify TPE.</li>
<li>Each Participant must immediately notify TPE if there is unauthorized use of its Password, ID or other security data.</li>
<li>If a Participant fails to notify TPE in writing when any of the above conditions occur, neither TPE nor any of its employees, agents, affiliates, subsidiaries, or its parent, nor any third parties, can or will have any responsibility or liability to the Participant or to any other person whose claim may arise through the Participant for any claims with respect to the handling, mishandling, or loss of any order. TPE shall not be deemed to have received any order or similar instruction electronically transmitted by Participant until TPE has acknowledged to the Participant that the order or similar instruction has been actually received. Each Participant is fully responsibility for the monitoring of its Account.</li>
<li>All orders entered on The Exchange shall be good-til-canceled which means that they will be open until they are either canceled or matched.</li>
<br><br><div class=S1>Trade Matching</div>Participants' orders are electronically matched by The Exchange based on price and time of entry, considering freight charges. Upon the matching of a buy and sell order, the transaction becomes a legally binding contract and The Exchange generates an electronic confirmation which is automatically sent to both the buy and sell Participants. TPE, in the absence of such additional delivery instructions, will initiate delivery following Participant's default instructions. If necessary, Participants are required to forward any additional delivery instructions to The Exchange immediately following the transaction either via phone at <%=ConfigurationSettings.AppSettings["PhoneNumber"].ToString()%>, e-mail to <a href=mailto:Logistics@ThePlasticsExchange.com>Logistics@ThePlasticsExchange.com</a> or system message.<br>
Once a transaction becomes a legally binding contract, both parties to the contract must perform (Seller must deliver, Buyer must take delivery). Netting of transactions is not permitted. TPE reserves the right to mitigate disputed trades in a fair and equitable manner.
<br><br><div class=S1>Transportation</div>Each Contract offered FOB shipping point includes the provision and cost of delivery.  Delivery shall be provided by TPE through its Transportation Partner(s). Because transportation is an integral part of each contract, Participant must provide shipping instructions to TPE upon application as a Participant. Neither the Transportation Partner nor TPE shall be liable for any consequential damages that occur in transit.
<br><br><div class=S1>Right of Rejection</div>A Participant may only reject delivery of a Contract where the Product delivered does not meet the standardized contract specifications set forth in Appendix II of the Agreement as amended from time to time. The Participant must notify TPE immediately of such rejection. TPE, upon such notification, may dispatch an independent party to analyze the Product for compliance with the standardized contract specifications.<br>
If the Product meets such specifications, Buyer must bear the cost of such independent analysis and must accept delivery.<br>
If the Product does not meet such specifications, Seller must bear the cost of such independent analysis and the cost of return shipping. Further, the Seller must accept the returned Product and ship to Buyer a Product that meets the standardized contract specifications.<br>
If Seller is unable to provide such Product, TPE will purchase a substitute Product at the current market price and Seller shall be responsible for all related costs, including any difference in price.
<br><br><div class=S1>Failure to Deliver</div>If a Seller fails to deliver any order pursuant to the terms of the Contract, TPE may purchase the Product at the prevailing market price for the benefit of the Buyer and the Seller shall be responsible for all costs related to such failure, including any price difference. TPE may offset these fees and other payables against payment due to Seller.
<br><br><div class=S1>Dispute Resolution</div>Each Participant must agree that it will use its best efforts to resolve any dispute that arises between the Participant and TPE and/or B2BExchangeCredit, LLC by participating in a meeting with the management of TPE. If the parties cannot come to an agreement through such meeting, the Participant must agree to submit to binding arbitration that will be conducted by FedNet, Inc<a href=#Footnote name=Foot>[1]</a>.  Each Participant must further agree to submit to binding arbitration when a dispute is brought to arbitration that involves the contraparty to any corresponding trade of the Participant and/or B2BExchangeCredit, LLC.
<br><br><div class=S1>Business Conduct</div>Participants must adhere to the following restrictions and requirements. For violating any of the following, TPE may, in its sole discretion, impose such sanctions against Participant as TPE may deem reasonable and appropriate, including but not limited to fine, suspension, termination or any such other sanction.
<li>Neither a Participant nor any employee of a Participant shall engage in any fraudulent act or to deceive, trick or engage in any scheme to defraud, in connection with or related to any trade on or other activity related to The Exchange. Orders of Contracts entered in The Exchange for the purpose of upsetting the equilibrium of the market and bringing about a condition of demoralization in which prices do not or will not reflect fair market values, are forbidden and any member of TPE and any employee of a member of TPE who makes or assists in entering such orders with knowledge of the purpose thereof, or who, with such knowledge shall be a party to assist in carrying out any plan or scheme for the entering of such orders shall be deemed guilty of an act inconsistent with just and equitable principles of trade.</li>
<li>Manipulation of the market is prohibited.</li>
<li>It shall be an offense to violate any TPE Policy regulating the conduct or business of Participant, or any agreement made with TPE, or to engage in fraud, dishonorable or dishonest conduct, or in conduct or proceedings inconsistent with just and equitable principles of trade, or intentionally default on the delivery of any Contract.</li>
<li>It shall be an offense against TPE to make a misstatement of material fact to TPE.</li>
<li>Neither a Participant nor any employee of a Participant may use its right of access to The Exchange in any way which would tend to bring disrepute upon the Participant or The Exchange.</li>
<br><br><div class=S1>Disclaimer/Limitation of Liability</div>
TPE shall not be responsible for the accuracy, completeness, or use of any information received by TPE from a Participant. TPE shall not be responsible for any damages caused by communications line failure, unauthorized access, theft, systems failure, and other occurrences beyond TPE's control. TPE shall not be responsible for any damages in the event that any order is not received by TPE. TPE shall have no liability if any or all of TPE's systems or the systems of any third party working with TPE "goes down" or otherwise fails to operate for any period of time. TPE is not liable for a Participant's inability to provide such access or for any delay in TPE's providing such access. To the extent that the System utilizes Internet or similar open telecommunication line services to transport data or communications, although TPE will take reasonable security precautions, TPE expressly disclaims any liability for interception of any such data or communications, and TPE shall not be responsible for and TPE makes no warranties regarding, the access, speed or availability of Internet or network services. The use and storage of any information, including without limitation, transaction activity, and any other information or orders available through a Participant's right of access to The Exchange ("Electronic Access") and all orders placed through the Electronic Access, is at each Participant's sole risk and responsibility. THE ELECTRONIC ACCESS IS PROVIDED AT EACH PARTICIPANT'S SOLE RISK ON AN "AS IS," "AS AVAILABLE" BASIS.  NEITHER TPE NOR ANY OF ITS DIRECTORS, OFFICERS, EMPLOYEES, AGENTS, CONTRACTORS, AFFILIATES, INFORMATION PROVIDERS, LICENSORS, OR OTHER SUPPLIERS PROVIDING DATA, INFORMATION, ACCESS OR SOFTWARE (COLLECTIVELY, "THE TPE PARTIES") MAKES ANY REPRESENTATIONS OR WARRANTIES EXPRESS OR IMPLIED INCLUDING WITHOUT LIMITATION ANY WARRANTY THAT THE ELECTRONIC ACCESS WILL BE UNINTERRUPTED OR ERROR FREE OR MAKES ANY WARRANTIES INCLUDING IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE IN RESPECT OF THE ELECTRONIC ACCESS OR PRODUCTS OBTAINED FROM, THROUGH, OR IN CONNECTION WITH THE ELECTRONIC ACCESS; NOR DO ANY OF THE TPE PARTIES MAKE ANY WARRANTY AS TO THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF SUCH ELECTRONIC ACCESS, OR AS TO THE TIMELINESS, SEQUENCE, ACCURACY, COMPLETENESS, RELIABILITY OR CONTENT OF ANY DATA, INFORMATION, ACCESS OR TRANSACTIONS PROVIDED THROUGH THE ELECTRONIC ACCESS; IN NO EVENT WILL ANY OF THE TPE PARTIES BE LIABLE FOR DIRECT, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES, RESULTING FROM ANY DEFECT IN, OR USE OF, OR INABILITY TO USE THE ELECTRONIC ACCESS (INCLUDING BUT NOT LIMITED TO LOST PROFITS, TRADING LOSSES OR DAMAGES THAT RESULT FROM LOSS OF THE USE OF THE ELECTRONIC ACCESS, INCONVENIENCE OR DELAY), EVEN IF ANY OF THE TPE PARTIES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES OR LOSSES.
<br><br><div class=S1>Insolvency and Bankruptcy</div>
If a Participant fails to perform its contracts or is near insolvency or becomes insolvent, such Participant shall promptly notify TPE of such fact. In such event, and even if such Participant fails to give notice to TPE, TPE may terminate or cancel the Participant's right of access if it deems it necessary.<br>
Whenever an order for relief under the United States Bankruptcy Code (hereinafter, the "Bankruptcy Code") is entered for a Participant, Participants having Exchange Contracts with the bankrupt Participant may proceed to close the same on The Exchange in accordance with the provisions of this policy.
<br><br><div class=S1>Emergencies</div>In the event of an Emergency, TPE, may place into immediate effect a temporary emergency policy which may remain in effect for up to thirty (30) Business Days and which may provide for, or may authorize TPE to undertake actions necessary or appropriate to meet the Emergency, including, but not limited to, such actions as:
<li>limiting trading to liquidation only, in whole or in part;</li>
<li>extending the time of delivery;</li>
<li>changing delivery points and/or the means of delivery;</li>
<li>ordering the liquidation of contracts, the fixing of a settlement price or the reduction in positions;</li>
<li>extending, limiting or changing hours of trading;</li>
<li>suspending trading; or</li>
<li>modifying or suspending any provision of the Policies of The Exchange.</li>
<br>
If, in the judgment of TPE, the physical functions of The Exchange are, or are threatened to be, severely and adversely affected by a physical emergency, TPE shall have authority to take such action as it deems necessary or appropriate to deal with such physical emergency. Such authorized action shall include, but shall not be limited to, closing The Exchange, delaying the opening of trading in any one or more Contracts and/or suspending trading in or extending trading hours for any one or more Contracts; provided, however, that suspension of trading ordered pursuant to this paragraph shall not continue in effect for more than five (5) Business Days unless TPE approves extending such action.<br>
In the event such action is taken, thereafter TPE may order restoration of trading on The Exchange or removal of any other restriction heretofore imposed pursuant to this paragraph, upon a determination by TPE that the physical emergency has sufficiently abated to permit the physical functions of The Exchange to continue in an orderly manner.
<br><br><div class=S1>Force Majeure</div>If strike, fire, accident, ice, equipment malfunction or other act of God which might otherwise be described as "Force Majeure" results in the failure of seller to deliver on a Contract, The Exchange's obligation to make delivery will be extended daily until such condition of Force Majeure no longer exists.
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3><a name=P href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;Appendix II</td></tr>
<tr><td align=center>
<img src=../images/Corporate/rprintable.gif border=0>
<tr><td bgcolor=white><img src=/images/1x1.gif width=1 height=8 border=0></td></tr>
<tr><td bgcolor=black CLASS=S5><img src=/images/1x1.gif width=3><a name=Q href=#top><img border=0 src=/images/buttons/top.gif></a>&nbsp;Appendix III</td></tr>
<tr><td>
<br><div class=S1>Payment Terms</div>The foregoing Schedule may be changed by the Lender from time to time, such change being effective for all Advances made from and after 30 days following notice of such changes from the Lender to the Borrower.
<div align=center>
<table width=500 border=1 cellspacing=0 cellpading=1>
<tr><td width=100%>
<table border=0 width=100%>
<tr><td colspan=2 nowrap>Shipping Date+Receive</td><td colspan=2 nowrap>Shipping Date+Receive</td><td colspan=2 nowrap>Shipping Date+Receive</td></tr>
<tr><td><b>0</td><td><b>99.25%</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><td>1</td><td>99.28%</td><td>31</td><td>100.07%</td><td>61</td><td>103.07%</td></tr>
<tr><td>2</td><td>99.30%</td><td>32</td><td>100.13%</td><td>62</td><td>103.20%</td></tr>
<tr><td>3</td><td>99.33%</td><td>33</td><td>100.20%</td><td>63</td><td>103.30%</td></tr>
<tr><td>4</td><td>99.35%</td><td>34</td><td>100.27%</td><td>64</td><td>103.40%</td></tr>
<tr><td>5</td><td>99.38%</td><td>35</td><td>100.33%</td><td>65</td><td>103.50%</td></tr>
<tr><td>6</td><td>99.40%</td><td>36</td><td>100.40%</td><td>66</td><td>103.60%</td></tr>
<tr><td>7</td><td>99.43%</td><td>37</td><td>100.47%</td><td>67</td><td>103.70%</td></tr>
<tr><td>8</td><td>99.45%</td><td>38</td><td>100.53%</td><td>68</td><td>103.80%</td></tr>
<tr><td>9</td><td>99.48%</td><td>39</td><td>100.60%</td><td>69</td><td>103.90%</td></tr>
<tr><td>10</td><td>99.50%</td><td>40</td><td>100.67%</td><td>70</td><td>104.00%</td></tr>
<tr><td>11</td><td>99.53%</td><td>41</td><td>100.73%</td><td>71</td><td>104.10%</td></tr>
<tr><td>12</td><td>99.55%</td><td>42</td><td>100.80%</td><td>72</td><td>104.20%</td></tr>
<tr><td>13</td><td>99.58%</td><td>43</td><td>100.87%</td><td>73</td><td>104.30%</td></tr>
<tr><td>14</td><td>99.60%</td><td>44</td><td>100.93%</td><td>74</td><td>104.40%</td></tr>
<tr><td><b>15</td><td><b>99.63%</td><td><b>45</td><td><b>101.00%</td><td><b>75</td><td><b>104.50%</td></tr>
<tr><td>16</td><td>99.65%</td><td>46</td><td>101.13%</td><td>76</td><td>104.60%</td></tr>
<tr><td>17</td><td>99.68%</td><td>47</td><td>101.27%</td><td>77</td><td>104.70%</td></tr>
<tr><td>18</td><td>99.70%</td><td>48</td><td>101.40%</td><td>78</td><td>104.80%</td></tr>
<tr><td>19</td><td>99.73%</td><td>49</td><td>101.53%</td><td>79</td><td>104.90%</td></tr>
<tr><td>20</td><td>99.75%</td><td>50</td><td>101.67%</td><td>80</td><td>105.00%</td></tr>
<tr><td>21</td><td>99.77%</td><td>51</td><td>101.80%</td><td>81</td><td>105.10%</td></tr>
<tr><td>22</td><td>99.80%</td><td>52</td><td>101.93%</td><td>82</td><td>105.20%</td></tr>
<tr><td>23</td><td>99.82%</td><td>53</td><td>102.07%</td><td>83</td><td>105.30%</td></tr>
<tr><td>24</td><td>99.85%</td><td>54</td><td>102.20%</td><td>84</td><td>104.50%</td></tr>
<tr><td>25</td><td>99.87%</td><td>55</td><td>102.33%</td><td>85</td><td>105.50%</td></tr>
<tr><td>26</td><td>99.90%</td><td>56</td><td>102.47%</td><td>86</td><td>105.60%</td></tr>
<tr><td>27</td><td>99.92%</td><td>57</td><td>102.60%</td><td>87</td><td>105.70%</td></tr>
<tr><td>28</td><td>99.95%</td><td>58</td><td>102.73%</td><td>88</td><td>105.80%</td></tr>
<tr><td>29</td><td>99.97%</td><td>59</td><td>102.87%</td><td>89</td><td>105.90%</td></tr>
<tr><td><b>30</td><td><b>100.00%</td><td><b>60</td><td><b>103.00%</td><td><b>90</td><td><b>106.00%</td></tr>
</td></tr></table>
</td></tr></table>
</div>
</td></tr>
</table>
<br>
<hr align=left size=1 width="33%">
<a href=#Foot name=Footnote>[1]</a>
<SPAN class=S1 id=M2>
FedNet, Inc., founded in 1998, is a private alternative dispute resolution organization comprised exclusively of former federal judges. Its offices are located at The Commons, P.O. Box 470338, Building Two, 8223 Brecksville Road, Cleveland, Ohio 44141-0338.
</SPAN></body></html>
