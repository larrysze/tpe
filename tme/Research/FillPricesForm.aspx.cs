using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace localhost.Research
{
	/// <summary>
	/// Summary description for FillPricesForm.
	/// </summary>
	public partial class FillPricesForm : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
            if ((Session["Typ"].ToString() != "A") && (Session["Typ"].ToString() != "B") && (Session["Typ"].ToString() != "L"))
			{
				Response.Redirect("../default.aspx");
			}

			if(!IsPostBack)
			{
				fillDates();
				Bind("PRICE_DATE ASC");
			}
		}

		private void fillDates()
		{
			DateTime today = DateTime.Now;
			//			DateTime today = Convert.ToDateTime("04/02/2006");
			int year = today.Year;
			int month = today.Month;

			string[] all_months = new string[13];

			all_months[1] = "January";
			all_months[2] = "February";
			all_months[3] = "March";
			all_months[4] = "April";
			all_months[5] = "May";
			all_months[6] = "June";
			all_months[7] = "July";
			all_months[8] = "August";
			all_months[9] = "September";
			all_months[10] = "October";
			all_months[11] = "November";
			all_months[12] = "December";
			
			DateTime start_date = Convert.ToDateTime("07/01/2006");
			DateTime somedate = start_date;
			while(somedate < today)
			{
				ListItem item = new ListItem(all_months[somedate.Month] + ", " + somedate.Year, somedate.ToShortDateString());
				ddlMonth.Items.Add(item);
				if(somedate.Month == 12)
				{
					somedate = somedate.AddYears(1);
					somedate = somedate.AddMonths(-11);
				}
				else
				{
					somedate = somedate.AddMonths(1);
				}
			}

				
			ddlMonth.Items[ddlMonth.Items.Count - 1].Selected = true;


		}

		private bool is_holiday(int day, int month, int year)
		{
			bool ret = false;
			
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();

			string sql = "SELECT * FROM HOLIDAYS WHERE MONTH(HOLIDAY_DATE)=" + month + " AND DAY(HOLIDAY_DATE)=" + day + " AND YEAR(HOLIDAY_DATE)=" + year;
			
			SqlCommand command = new SqlCommand(sql, conn);
			SqlDataReader dtr = command.ExecuteReader();

			if(dtr.Read())
			{
				ret = true;
			}

			dtr.Close();
			conn.Close();


			return ret;
		}

		public void Bind(string SQLOrder)
		{

			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			string strSQL;
			strSQL ="";

			DateTime date = Convert.ToDateTime(ddlMonth.SelectedValue.ToString());
			
			/*
			 *	we have to make sure that all dates are present in the TABLE
			 *	if no then INSERT with default values
			 */

			strSQL = "SELECT DISTINCT PRICE_DATE, DAY(PRICE_DATE) as PRICE_DAY, PRICE_DATE FROM MISC_PRICES " + 
				" WHERE MONTH(PRICE_DATE)=" + date.Month + " AND YEAR(PRICE_DATE)=" + date.Year + 
				" AND (DATEPART(weekday, PRICE_DATE) IN (2,3,4,5,6)) " + 
				" AND PRICE_DATE not in (select holiday_date from holidays) ORDER BY PRICE_DATE ASC";

			SqlCommand command = new SqlCommand(strSQL, conn);
			SqlDataReader dtr = command.ExecuteReader();
			
			int max = DateTime.DaysInMonth(date.Year, date.Month);
			int k = 1;
			
			SqlConnection temp_conn;
			temp_conn = new SqlConnection(Application["DBconn"].ToString());
			temp_conn.Open();

			int i = 1;
			do
			{
				if(i >= k)
				{
					if(dtr.Read())
					{
						k = Convert.ToInt32(dtr["PRICE_DAY"].ToString());
					}
					else
					{
						k = max + 1;
					}
				}

			while(k > i)
			{

				DateTime this_day = Convert.ToDateTime(date.Month + "/" + i + "/" + date.Year);
				if((this_day.DayOfWeek != DayOfWeek.Saturday) && (this_day.DayOfWeek != DayOfWeek.Sunday))
				{
					bool was_holiday = false;
			

					string sql = "SELECT * FROM HOLIDAYS WHERE MONTH(HOLIDAY_DATE)=" + date.Month + " AND DAY(HOLIDAY_DATE)=" + i + " AND YEAR(HOLIDAY_DATE)=" + date.Year;
			
					SqlCommand sql_command = new SqlCommand(sql, temp_conn);
					SqlDataReader temp_dtr = sql_command.ExecuteReader();

					if(temp_dtr.Read())
					{
						was_holiday = true;
					}

					temp_dtr.Close();
					if(!was_holiday)
					{
						create_record(i, date.Month, date.Year); //insert line with default values
					}
				}
				i++;
			}

				i++;

			}while(i <= max);

			
			temp_conn.Close();

			dtr.Close();

			strSQL="SELECT * FROM MISC_PRICES WHERE MONTH(PRICE_DATE)=" + date.Month + " AND YEAR(PRICE_DATE)=" + date.Year + " ORDER BY "+ SQLOrder;
			SqlDataAdapter dadContent;
			DataSet dstContent;
			dadContent = new SqlDataAdapter(strSQL ,conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dg.DataSource = dstContent;
			dg.DataBind();

			conn.Close();


		}

		private void create_record(int day, int month, int year)
		{
			
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			
			DateTime date =  Convert.ToDateTime(month.ToString() + "/" + day.ToString() + "/" + year.ToString()); 
			string sql = "INSERT INTO MISC_PRICES(PRICE_DATE, IS_ENTERED) VALUES('" +date.ToShortDateString() + "', 0)";
			
			SqlCommand command = new SqlCommand(sql, conn);
			command.ExecuteNonQuery();

			conn.Close();

		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_ItemDataBound);

		}
		#endregion

		private void dg_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				if(e.Item.Cells[17].Text == "False")
				{
					((TextBox)e.Item.Cells[9].Controls[1]).Text =  dontDisplayZeros(nullValuesHTML(e.Item.Cells[1].Text));
					((TextBox)e.Item.Cells[10].Controls[1]).Text =  dontDisplayZeros(nullValuesHTML(e.Item.Cells[2].Text));
					((TextBox)e.Item.Cells[11].Controls[1]).Text =  dontDisplayZeros(nullValuesHTML(e.Item.Cells[3].Text));
					((TextBox)e.Item.Cells[12].Controls[1]).Text =  dontDisplayZeros(nullValuesHTML(e.Item.Cells[4].Text));
					((TextBox)e.Item.Cells[13].Controls[1]).Text =  dontDisplayZeros(nullValuesHTML(e.Item.Cells[5].Text));
					((TextBox)e.Item.Cells[14].Controls[1]).Text =  dontDisplayZeros(nullValuesHTML(e.Item.Cells[6].Text));
					((TextBox)e.Item.Cells[15].Controls[1]).Text =  dontDisplayZeros(nullValuesHTML(e.Item.Cells[7].Text));
					((TextBox)e.Item.Cells[16].Controls[1]).Text =  dontDisplayZeros(nullValuesHTML(e.Item.Cells[8].Text));
				}
				else
				{
					((TextBox)e.Item.Cells[9].Controls[1]).Text =  nullValuesHTML(e.Item.Cells[1].Text);
					((TextBox)e.Item.Cells[10].Controls[1]).Text =  nullValuesHTML(e.Item.Cells[2].Text);
					((TextBox)e.Item.Cells[11].Controls[1]).Text =  nullValuesHTML(e.Item.Cells[3].Text);
					((TextBox)e.Item.Cells[12].Controls[1]).Text =  nullValuesHTML(e.Item.Cells[4].Text);
					((TextBox)e.Item.Cells[13].Controls[1]).Text =  nullValuesHTML(e.Item.Cells[5].Text);
					((TextBox)e.Item.Cells[14].Controls[1]).Text =  nullValuesHTML(e.Item.Cells[6].Text);
					((TextBox)e.Item.Cells[15].Controls[1]).Text =  nullValuesHTML(e.Item.Cells[7].Text);
					((TextBox)e.Item.Cells[16].Controls[1]).Text =  nullValuesHTML(e.Item.Cells[8].Text);
				}
			}
		
		}

		private string nullValuesHTML(string input)
		{
			string str_return = "0";

			if (input != null)
			{
				if ((input != "&nbsp;") && (input.Trim() != "") && (input.Trim() != "0"))
				{
					//					string strTmp = Con
					str_return = String.Format("{0:0.000}", Convert.ToDecimal(input));
				}
			}
			return str_return;
		}

		string dontDisplayZeros(string input_value)
		{
			if(input_value == "0")
			{
				return "";
			}
			return input_value;
		}

		private string outValuesHTML(string input)
		{
			string str_return = "0";

			if (input != null)
			{
				if ((input != "&nbsp;") && (input.Trim() != ""))
				{
					//					string strTmp = Con
					str_return = Convert.ToDecimal(input).ToString();
				}
			}
			return str_return;
		}

		protected void btnSave_Click(object sender, System.EventArgs e)
		{
			String s = dg.Items[0].Cells[0].Text;
			string on = "0";
			for(int i = dg.Items.Count - 1; i >= 0 ; i--)
			{
				if (dg.Items[i].ItemType == ListItemType.Item || dg.Items[i].ItemType == ListItemType.AlternatingItem)
				{
					string date = dg.Items[i].Cells[0].Text;
					string s1 = outValuesHTML(((TextBox)(dg.Items[i].Cells[9].Controls[1])).Text);
					string s2 = outValuesHTML(((TextBox)(dg.Items[i].Cells[10].Controls[1])).Text);
					string s3 = outValuesHTML(((TextBox)(dg.Items[i].Cells[11].Controls[1])).Text);
					string s4 = outValuesHTML(((TextBox)(dg.Items[i].Cells[12].Controls[1])).Text);
					string s5 = outValuesHTML(((TextBox)(dg.Items[i].Cells[13].Controls[1])).Text);
					string s6 = outValuesHTML(((TextBox)(dg.Items[i].Cells[14].Controls[1])).Text);
					string s7 = outValuesHTML(((TextBox)(dg.Items[i].Cells[15].Controls[1])).Text);
					string s8 = outValuesHTML(((TextBox)(dg.Items[i].Cells[16].Controls[1])).Text);
					
					if( (on != "1") && ( (Convert.ToDecimal(s1) != 0) || (Convert.ToDecimal(s2) != 0) || (Convert.ToDecimal(s3) != 0) || (Convert.ToDecimal(s4) != 0) || (Convert.ToDecimal(s5) != 0) || (Convert.ToDecimal(s6) != 0) || (Convert.ToDecimal(s7) != 0) || (Convert.ToDecimal(s8) != 0)) )
					{
						on = "1";
					}

					SqlConnection conn;
					conn = new SqlConnection(Application["DBconn"].ToString());
					conn.Open();
			
					DateTime item_date =  Convert.ToDateTime(date); 
					
					string sql = "UPDATE MISC_PRICES SET " + 
						"  ETHYLENE=" + s1 + 
						", PROPYLENE=" + s2 + 
						", STYRENE=" + s3 + 
						", BENZENE=" + s4 + 
						", COIL=" + s5 + 
						", NGAS=" + s6 + 
						", ETHANE=" + s7 + 
						", PROPANE=" + s8 + 
						", IS_ENTERED=" + on + 
						" WHERE DAY(PRICE_DATE)=" + item_date.Day + " AND MONTH(PRICE_DATE)=" + item_date.Month + " AND YEAR(PRICE_DATE)=" + item_date.Year;
			
					SqlCommand command = new SqlCommand(sql, conn);
					command.ExecuteNonQuery();

					conn.Close();


				}
			}
		
		}

		protected void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Bind("PRICE_DATE ASC");
		}


	}
}
