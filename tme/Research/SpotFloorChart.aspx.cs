using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using dotnetCHARTING;
using TPE.Utility;
using System.Data.SqlClient;

namespace localhost
{
	/// <summary>
	/// Summary description for SpotFloorChart.
	/// </summary>
	public partial class SpotFloorChart : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if ((string)Session["Typ"] != "A")
			{
				Response.Redirect("/default.aspx");
			}

			if(!IsPostBack)
			{
				loadDDL();
				createGradeChart();
			
			}
		}

		private void loadDDL()
		{
			using(SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, "SELECT * FROM GRADE");
				while(dtr.Read())
				{
					ddlResinType.Items.Add(new ListItem(dtr["GRADE_NAME"].ToString(), dtr["GRADE_ID"].ToString()));
				}

				ddlResinType.Items.Add(new ListItem("Total Volume Chart", "-1"));
				
				ddlResinType.Items[5].Selected = true;

			}

		}



		private void createGradeChart()
		{
			Chart.ChartArea.ClearColors();			
			Chart.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
			Chart.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
			int grade_id = Convert.ToInt32(ddlResinType.SelectedValue);
           
			// Set the chart type
			Chart.Type = ChartType.Combo;

			Chart.OverlapFooter = true;
			Chart.Mentor = false;
			// Set the size
			Chart.Width = 780;
			Chart.Height = 600;
			// Set the temp directory
			Chart.TempDirectory = "temp";
			// Debug mode. ( Will show generated errors if any )
			Chart.Debug = true;
			Chart.Title = "Historical Price and Volume: " + ddlResinType.SelectedItem;
			
      
			Chart.DefaultSeries.Type = SeriesType.AreaLine;
			Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;// = false;
			Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
			Chart.DefaultElement.Marker.Visible = false;

			Color[] MyColorObjectArray = new Color[3]{Color.Red,Color.FromArgb(255, 202,0 ),Color.Yellow}; 
			Chart.Palette = MyColorObjectArray;

			Chart.ChartAreaSpacing = 8;
			Chart.LegendBox.Template = "%Icon%Name";
      
			// Modify the x axis labels.
			//Chart.XAxis.TimeScaleLabels.Mode = TimeScaleLabelMode.Smart;
			Chart.XAxis.TimeScaleLabels.DayFormatString = "p";
			Chart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Year);
			Chart.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
			Chart.ChartArea.XAxis.TickLabelAngle = 90;
			
			//Chart.XAxis.DefaultTick.Label.Text = "<%Value,mmm>";
			Chart.ChartArea.XAxis.LabelRotate = true;
//			Chart.YAxis.Minimum = 0.35;
//			Chart.YAxis.Maximum = 0.85;
			Chart.XAxis.TimeScaleLabels.MonthFormatString = "MMM";
			Chart.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
			Chart.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.White;
			Chart.XAxis.TimeInterval = TimeInterval.Month;
      
      
			// Setup the axes.
			Chart.YAxis.Label.Text = "Price";
			Chart.YAxis.FormatString = "Currency";
			Chart.YAxis.Scale = Scale.Range;

      
			// *DYNAMIC DATA NOTE*
			// This sample uses random data to populate the chart. To populate
			// a chart with database data see the following resources:
			// - Classic samples folder
			// - Help File > Data Tutorials
			// - Sample: features/DataEngine.aspx
			SeriesCollection mySC = getPriceData(grade_id);
			mySC[0].Type = SeriesTypeFinancial.Bar;
			SeriesCollection mySCV = getVolumeData(grade_id);
      
			// Add volume chart area.
			ChartArea volumeArea = new ChartArea();
			volumeArea.YAxis.Label.Text = "Volume";
			volumeArea.SeriesCollection.Add(mySCV);
			volumeArea.HeightPercentage = 30;
			volumeArea.ClearColors();
//			volumeArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
			volumeArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
			
			// can create a box for a chart.
			volumeArea.Line.Color = Color.Orange;
			Chart.ChartArea.Line.Color = Color.Orange;
			
//			volumeArea.DefaultSeries.Palette = ;

			Chart.ExtraChartAreas.Add(volumeArea);
      
			
			// Add the price data.
			Chart.SeriesCollection.Add(mySC);
      
             
			//Chart
			Chart.TitleBox.Background.Color = Color.Orange;
			Chart.Background.Color = Color.Black;
			Chart.ChartArea.Background.Color = Color.Black;
			Chart.ChartArea.DefaultSeries.Line.Color = Color.Red;
			Chart.ChartArea.DefaultSeries.Line.Width = 2;
			volumeArea.Background.Color = Color.Black;


			//XAxis
			Chart.XAxis.Label.Color = Color.White;
			Chart.ChartArea.XAxis.Label.Color = Color.White;
			Chart.XAxis.Line.Color = Color.Orange;
			Chart.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;


			//YAxis
			Chart.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
			Chart.ChartArea.YAxis.Label.Color = Color.White;
			Chart.YAxis.Line.Color = Color.Orange;
			Chart.YAxis.Label.Color = Color.White;
			volumeArea.YAxis.Label.Color = Color.White;
			volumeArea.YAxis.DefaultTick.Label.Color = Color.White;
			volumeArea.YAxis.Line.Color = Color.Orange;    
		
			            
		}

		private void createVolumeChart()
		{
			Chart.ChartArea.ClearColors();			
			Chart.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
			Chart.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
           
			// Set the chart type
//			Chart.Type = ChartType.;

			Chart.OverlapFooter = true;
			Chart.Mentor = false;
			// Set the size
			Chart.Width = 780;
			Chart.Height = 600;
			// Set the temp directory
			Chart.TempDirectory = "temp";
			// Debug mode. ( Will show generated errors if any )
//			Chart.Debug = true;

			Chart.Title = "Historical Volume";
			
      		Chart.DefaultSeries.Type = SeriesType.AreaLine;
			Chart.DefaultElement.Marker.Visible = false;
			Chart.DefaultSeries.Line.Color = Color.Red;
			Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;
			Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
//			Chart.DefaultElement.Marker.Visible = false;

//			Color[] MyColorObjectArray = new Color[3]{Color.Red,Color.FromArgb(255, 202,0 ),Color.Yellow}; 
//			Chart.Palette = MyColorObjectArray;

//			Chart.ChartAreaSpacing = 8;
//			Chart.LegendBox.Template = "%Icon%Name";
      
			// Modify the x axis labels.
			//Chart.XAxis.TimeScaleLabels.Mode = TimeScaleLabelMode.Smart;
			Chart.XAxis.TimeScaleLabels.DayFormatString = "p";
			Chart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Year);
			Chart.YAxis.Minimum = 0;
			Chart.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
			Chart.ChartArea.XAxis.TickLabelAngle = 90;
			
			//Chart.XAxis.DefaultTick.Label.Text = "<%Value,mmm>";
			Chart.ChartArea.XAxis.LabelRotate = true;
			//			Chart.YAxis.Minimum = 0.35;
			//			Chart.YAxis.Maximum = 0.85;
			Chart.XAxis.TimeScaleLabels.MonthFormatString = "MMM";
			Chart.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
			Chart.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0 );
			Chart.XAxis.TimeInterval = TimeInterval.Month;
			Color[] MyColorObjectArray = new Color[3]{Color.FromArgb(255, 202,0 ),Color.FromArgb(255, 202,0 ),Color.FromArgb(255, 202,0 )};
			Chart.Palette = MyColorObjectArray;
			//Chart.ChartArea.DefaultSeries.Line.Color = Color.FromArgb(255, 202, 0, 0);
      
      
			// Setup the axes.
			Chart.YAxis.Label.Text = "Pounds";
//			Chart.YAxis.FormatString = "###,###";
//			Chart.DefaultSeries.DefaultElement.Marker.Color =  Color.FromArgb(255, 202, 0 );
			Chart.YAxis.Scale = Scale.Range;

      
			// *DYNAMIC DATA NOTE*
			// This sample uses random data to populate the chart. To populate
			// a chart with database data see the following resources:
			// - Classic samples folder
			// - Help File > Data Tutorials
			// - Sample: features/DataEngine.aspx
			SeriesCollection mySC = getAllVolumeData();
			Chart.ChartArea.Line.Color = Color.Orange;
			
			//			volumeArea.DefaultSeries.Palette = ;

			
			// Add the price data.
			Chart.SeriesCollection.Add(mySC);
      
             
			//Chart
			Chart.TitleBox.Background.Color = Color.Orange;
			Chart.Background.Color = Color.Black;
			Chart.ChartArea.Background.Color = Color.Black;
			Chart.DefaultSeries.Line.Color = Color.Red;
			Chart.ChartArea.DefaultSeries.Line.Color = Color.Red;
			Chart.ChartArea.DefaultSeries.Line.Width = 2;


			//XAxis
			Chart.XAxis.Label.Color = Color.White;
			Chart.ChartArea.XAxis.Label.Color = Color.White;
			Chart.XAxis.Line.Color = Color.Orange;
			Chart.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;


			//YAxis
			Chart.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
			Chart.ChartArea.YAxis.Label.Color = Color.White;
			Chart.YAxis.Line.Color = Color.Orange;
			Chart.YAxis.Label.Color = Color.White;

			
			            
		}

		private SeriesCollection getVolumeData(int grade_id)
		{
			SeriesCollection SC = new SeriesCollection();

                  
			Series s = new Series();
			s.Name = "Volume";                  
			//double startPrice = 50;

			using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{

				SqlDataReader dtr = null;
				Hashtable param = new Hashtable();
				param.Add("@resin_grade_id", grade_id);
				param.Add("@date", "01-01-2004");
				dtr = DBLibrary.GetDataReaderStoredProcedure(conn, "spSpot_Offers_SummaryByResinGrade", param);

				while (dtr.Read())
				{
					DateTime date = Convert.ToDateTime(dtr["friday_date"].ToString());
					double weight = Convert.ToDouble(dtr["weight"].ToString());

					Element e = new Element();
					e.XDateTime = date;
					e.YValue = weight;
					s.Elements.Add(e);

					

				}
			}
			SC.Add(s);
			return(SC);
		}

		private SeriesCollection getPriceData(int grade_id)
		{
      
      
			SeriesCollection SC = new SeriesCollection();

			Series s = new Series();
			s.Name = "Price Range";       

		    
			using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				SqlDataReader dtr = null;
				Hashtable param = new Hashtable();
				param.Add("@resin_grade_id", grade_id);
				param.Add("@date", "01-01-2004");

				dtr = DBLibrary.GetDataReaderStoredProcedure(conn, "spSpot_Offers_SummaryByResinGrade", param);
				while (dtr.Read())
				{
					DateTime date = Convert.ToDateTime(dtr["friday_date"].ToString());
					Element e = new Element();
					e.XDateTime = date;
					

					//e.SmartLabel.Color = Color.White;

					
                     
					double price_low = Convert.ToDouble(dtr["price_low"].ToString());
					double price_high = Convert.ToDouble(dtr["price_high"].ToString());
					
					e.Close = price_high;
					e.Open = price_low;
                              
					e.High = price_high;
					e.Low = price_low;

					s.Elements.Add(e);

				}
			}

			
			SC.Add(s);
			return(SC);
		}
            
		private SeriesCollection getAllVolumeData()
		{
			SeriesCollection SC = new SeriesCollection();

                  
			Series s = new Series();
			s.Name = "Volume";                  
			//double startPrice = 50;

			using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{

				SqlDataReader dtr = null;
				dtr = DBLibrary.GetDataReaderStoredProcedure(conn, "spSpot_Offers_Summary_TotalVolume_EachFriday", null);

				while (dtr.Read())
				{
					DateTime date = Convert.ToDateTime(dtr["Friday_Date"].ToString());
					double weight = Convert.ToDouble(dtr["TotalWeight"].ToString());

					Element e = new Element();
					e.XDateTime = date;
					e.YValue = weight;
					s.Elements.Add(e);

					

				}
			}
			SC.Add(s);
			return(SC);
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void ddlResinType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(ddlResinType.SelectedValue == "-1")
			{
				createVolumeChart();
			}else
			{
				createGradeChart();
			}
		}


	}
}
