<%@ Page Language="c#" Codebehind="FillPricesForm.aspx.cs" AutoEventWireup="True" Inherits="localhost.Research.FillPricesForm" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
    <br>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="487px" Height="92px" HeaderText="There are problems with the following fields. Please correct the errors below."></asp:ValidationSummary>
    <br>
    <asp:Label CssClass="Content Color2" ID="lblMonthTitle" runat="server">Month:&nbsp;&nbsp;</asp:Label><asp:DropDownList ID="ddlMonth" runat="server" CssClass="InputForm" AutoPostBack="True" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged">
    </asp:DropDownList><br>
    <br>
    <asp:DataGrid ID="dg" runat="server" BorderWidth="0" BackColor="#000000" CellSpacing="1" ItemStyle-CssClass="DataGridRow" AlternatingItemStyle-CssClass="DataGridRow_Alternate" HeaderStyle-CssClass="DataGridHeader" AutoGenerateColumns="False" HorizontalAlign="Center" GridLines="None">
        <AlternatingItemStyle CssClass="LinkNormal Color2 DarkGray"></AlternatingItemStyle>
        <ItemStyle CssClass="LinkNormal Color2 LightGray"></ItemStyle>
        <HeaderStyle CssClass="LinkNormal Color2 OrangeColor"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="PRICE_DATE" HeaderText="Date" DataFormatString="{0:d}">
                <ItemStyle Wrap="False" CssClass="Content Color2"></ItemStyle>
            </asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="ETHYLENE" HeaderText="Ethylene"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="PROPYLENE" HeaderText="Propylene"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="STYRENE" HeaderText="Styrene"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="BENZENE" HeaderText="Benzene"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="COIL" HeaderText="Crude Oil"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="NGAS" HeaderText="Natrual Gas"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="ETHANE" HeaderText="Ethane"></asp:BoundColumn>
            <asp:BoundColumn Visible="False" DataField="PROPANE" HeaderText="Propane"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Ethylene">
                <ItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" CssClass="InputForm" Width="100px" Visible="True" TabIndex=10></asp:TextBox>
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="price - invalid value" MinimumValue="-999999999" MaximumValue="999999999" Type="Double" Display="None"></asp:RangeValidator>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Propylene">
                <ItemTemplate>
                    <asp:TextBox ID="Textbox2" runat="server" CssClass="InputForm" Width="100px" Visible="True" TabIndex=11></asp:TextBox>
                    <asp:RangeValidator ID="Rangevalidator2" runat="server" ControlToValidate="TextBox2" ErrorMessage="Propylene price - invalid value" MinimumValue="-999999999" MaximumValue="999999999" Type="Double" Display="None"></asp:RangeValidator>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Styrene">
                <ItemTemplate>
                    <asp:TextBox ID="Textbox3" runat="server" CssClass="InputForm" Width="100px" Visible="True" TabIndex=12></asp:TextBox>
                    <asp:RangeValidator ID="Rangevalidator3" runat="server" ControlToValidate="TextBox3" ErrorMessage="Styrene price - invalid value" MinimumValue="-999999999" MaximumValue="999999999" Type="Double" Display="None"></asp:RangeValidator>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Benzene">
                <ItemTemplate>
                    <asp:TextBox ID="Textbox4" runat="server" CssClass="InputForm" Width="100px" Visible="True" TabIndex=13></asp:TextBox>
                    <asp:RangeValidator ID="Rangevalidator4" runat="server" ControlToValidate="TextBox4" ErrorMessage="Benzene price - invalid value" MinimumValue="-999999999" MaximumValue="999999999" Type="Double" Display="None"></asp:RangeValidator>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Crude Oil">
                <ItemTemplate>
                    <asp:TextBox ID="Textbox5" runat="server" CssClass="InputForm" Width="100px" Visible="True" TabIndex=14></asp:TextBox>
                    <asp:RangeValidator ID="Rangevalidator5" runat="server" ControlToValidate="TextBox5" ErrorMessage="Crude Oil price - invalid value" MinimumValue="-999999999" MaximumValue="999999999" Type="Double" Display="None"></asp:RangeValidator>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Natural Gas">
                <ItemTemplate>
                    <asp:TextBox ID="Textbox6" runat="server" CssClass="InputForm" Width="100px" Visible="True" TabIndex=15></asp:TextBox>
                    <asp:RangeValidator ID="Rangevalidator6" runat="server" ControlToValidate="TextBox6" ErrorMessage="Natural Gas price - invalid value" MinimumValue="-999999999" MaximumValue="999999999" Type="Double" Display="None"></asp:RangeValidator>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Ethane">
                <ItemTemplate>
                    <asp:TextBox ID="Textbox7" runat="server" CssClass="InputForm" Width="100px" Visible="True" TabIndex=16></asp:TextBox>
                    <asp:RangeValidator ID="Rangevalidator7" runat="server" ControlToValidate="TextBox6" ErrorMessage="Ethane price - invalid value" MinimumValue="-999999999" MaximumValue="999999999" Type="Double" Display="None"></asp:RangeValidator>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Propane">
                <ItemTemplate>
                    <asp:TextBox ID="Textbox8" runat="server" CssClass="InputForm" Width="100px" Visible="True" TabIndex=17></asp:TextBox>
                    <asp:RangeValidator ID="Rangevalidator8" runat="server" ControlToValidate="TextBox6" ErrorMessage="Propane price - invalid value" MinimumValue="-999999999" MaximumValue="999999999" Type="Double" Display="None"></asp:RangeValidator>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn Visible="False" DataField="IS_ENTERED" HeaderText="PRICE_ENTERED"></asp:BoundColumn>
        </Columns>
    </asp:DataGrid><br />
    <div style="text-align:center"><asp:Button ID="btnSave" runat="server" CssClass="Content Color2" Text="Save" OnClick="btnSave_Click"></asp:Button></div>
    <br />
    <br />
</asp:Content>
