using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;
using dotnetCHARTING;


namespace localhost.Research
{
    public partial class DayPriceEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, System.EventArgs e)
        {
            Master.Width = "780px";
            // Put user code to initialize the page here

            if ((string)Session["Typ"] != "A")
            {
                Response.Redirect("../default.aspx");
            }

            if (!IsPostBack)
            {
                loadDDL();
                BindDG();
                CreateChart();
            }
        }
        private void CreateChart()
        {
            dotnetCHARTING.Chart ChartObj = new dotnetCHARTING.Chart();

            ChartObj.ChartArea.ClearColors();

            ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            ChartObj.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);

            ChartObj.TempDirectory = Server.MapPath("/Research/charts/dp");

            ChartObj.Width = 390;
            ChartObj.Height = 225;

            string[] period1 = new string[3];
            period1[0] = "1M";
            period1[1] = "1Y";
            period1[2] = "5Y";
            string nameFile = "Chart";
            //ChartObj.SeriesCollection.Add(getData(Convert.ToInt32(myReader2.GetValue(0)), period1, nameFile));


            for (int k = 0; k < 3; k++)
            {
                ChartObj.FileName = nameFile + "_" + ddlChart.SelectedValue + "_" + period1[k];

                ChartObj.SeriesCollection.Add(getData(Convert.ToInt32(ddlChart.SelectedValue), period1[k], nameFile));

                ChartObj.Use3D = false;

                ChartObj.Width = 390;
                ChartObj.Height = 225;
                ChartObj.ChartArea.ClearColors();

                ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
                ChartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
                ChartObj.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
                ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);

                ChartObj.XAxis.TimeInterval = TimeInterval.Week;


                ChartObj.ChartArea.DefaultElement.Color = Color.Orange;
                ChartObj.ChartArea.DefaultSeries.Line.Color = Color.Orange;
                ChartObj.ChartArea.DefaultSeries.Line.Width = 2;


                ChartObj.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
                ChartObj.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
                ChartObj.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
                ChartObj.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);


                ChartObj.ChartArea.XAxis.DefaultTick.Line.Color = Color.FromArgb(254, 0, 0); //red
                ChartObj.ChartArea.YAxis.DefaultTick.Line.Color = Color.FromArgb(254, 0, 0); //red

                ChartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;

                ChartObj.ChartArea.XAxis.Line.Width = 4;
                ChartObj.ChartArea.YAxis.Line.Width = 2;

                ChartObj.YAxis.Interval = 0.01;

                ChartObj.ChartArea.XAxis.LabelRotate = true;

                ChartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
                ChartObj.ChartArea.XAxis.TickLabelAngle = 90;

                ChartObj.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
                ChartObj.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);

                // Set a default transparency
                ChartObj.DefaultSeries.DefaultElement.Transparency = 20;

                // Set color of axis lines
                Axis AxisObj = new Axis();
                AxisObj.Line.Color = Color.FromArgb(255, 255, 0, 0);

                //            ChartObj.ChartArea.XAxis.DefaultTick.Line.Color = Color.FromArgb(254, 165, 3);
                //            ChartObj.ChartArea.YAxis.DefaultTick.Label.Color = Color.FromArgb(254, 165, 3);

                //							AxisObj.Line.Color = Color.FromArgb(2, System.Drawing.Color.Red);
                ChartObj.DefaultAxis = AxisObj;

                ChartObj.MarginLeft = 0;
                ChartObj.MarginRight = 0;
                ChartObj.MarginTop = 0;
                ChartObj.MarginBottom = 0;

                // Set the Default Series Type
                ChartObj.DefaultSeries.Type = SeriesType.Line;
                ChartObj.LegendBox.Position = LegendBoxPosition.None;

                // Set the y Axis Scale
                ChartObj.ChartArea.YAxis.Scale = Scale.Range;

                ChartObj.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
                ChartObj.DefaultSeries.DefaultElement.Marker.Size = 6;
                ChartObj.DefaultSeries.DefaultElement.Marker.Color = Color.White;//Color.FromArgb( 255, 255, 153, 9);
                ChartObj.XAxis.Label.Color = Color.White;

                ChartObj.ChartArea.Line.Color = Color.FromArgb(255, 255, 0, 0);//(254, 0, 0)
                ChartObj.ChartArea.Line.Width = 2;

                ChartObj.Background.Color = Color.Black;
                ChartObj.ChartArea.Background.Color = Color.Black;

                ChartObj.FileManager.ImageFormat = ImageFormat.Png;
                //                            ChartObj.OverlapFooter = true;

                Bitmap bmp1 = ChartObj.GetChartBitmap();

                string fileName2 = "";
                fileName2 = ChartObj.FileManager.SaveImage(bmp1);

                ChartObj.SeriesCollection.Clear();

            }
            imgChart.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/charts/dp/" + nameFile + "_" + ddlChart.SelectedValue + "_1M.png";
            imgChart1.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/charts/dp/" + nameFile + "_" + ddlChart.SelectedValue + "_1Y.png";
            imgChart2.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/charts/dp/" + nameFile + "_" + ddlChart.SelectedValue + "_5Y.png";



            //       ChartObj.SeriesCollection.Clear();0

        }
        private SeriesCollection getData(int iContract, string period, string nameFile)
        {
            SqlConnection conn;
            SqlDataReader dtrData;
            SqlCommand cmdData;
            string strSQL;
            conn = new SqlConnection(Application["DBConn"].ToString());
            conn.Open();
            int iPreRead = 0; // the number of preloaded items
            int iInterval = 0; // the space between each items

            int range = 1;
            int count = 0;
            string tmpSQL = "";


            switch (period)
            {

                default:
                    strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate()) ";
                    iPreRead = 2;
                    iInterval = 0;
                    range = 4;
                    tmpSQL = "select COUNT(*) from HISTORICAL_PRICES WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate())";
                    cmdData = new SqlCommand(tmpSQL, conn);
                    cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                    count = Convert.ToInt32(cmdData.ExecuteScalar());

                    break;
                //case "1Y":
                //	strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-1,getdate()) ";
                //	iInterval = 0;
                //	iPreRead = 0;
                //	break;
                case "5Y":
                    strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate()) ";
                    iInterval = 0;
                    iPreRead = 2;
                    range = 4;
                    tmpSQL = "select COUNT(*) from HISTORICAL_PRICES WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate())";
                    cmdData = new SqlCommand(tmpSQL, conn);
                    cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                    count = Convert.ToInt32(cmdData.ExecuteScalar());

                    break;
                case "10Y":
                    strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-10,getdate()) ";
                    iInterval = 2;
                    iPreRead = 5;
                    range = 10;
                    tmpSQL = "select COUNT(*) from HISTORICAL_PRICES WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-10,getdate())";
                    cmdData = new SqlCommand(tmpSQL, conn);
                    cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                    count = Convert.ToInt32(cmdData.ExecuteScalar());

                    break;
                case "Forward":
                    strSQL = "Select FWD_ID, DATE= LEFT(FWD_MNTH,3)+' '+RIGHT(FWD_YEAR,2), PRICE = (SELECT RIGHT(CONVERT(varchar,MAX(OFFR_PRCE)),4) FROM OFFER WHERE OFFR_CONT='" + iContract.ToString() + "' AND OFFR_MNTH=FWD_ID AND OFFR_SIZE='1') From FWDMONTH WHERE FWD_ACTV ='1' ORDER BY FWD_ID ASC";
                    break;
            }

            strSQL = "select PRICE,PRICE_DATE AS DATE from HISTORICAL_PRICES " + strSQL + " ORDER BY PRICE_DATE";
            if (period.Equals("1M"))
            {
                // needs to overwrite string completely if it is one month
                strSQL = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(dd,-30,getdate())  ORDER BY DATE";
                iPreRead = 0;
                iInterval = 0;
                range = 4;
                tmpSQL = "select count(*) from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(dd,-30,getdate())";
                cmdData = new SqlCommand(tmpSQL, conn);
                cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                count = Convert.ToInt32(cmdData.ExecuteScalar());
            }
            //			if (period.Equals("6M"))
            //			{
            //				// needs to overwrite string completely if it is one month
            //				strSQL = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(mm,-6,getdate())  ORDER BY DATE";
            //				iPreRead = 12;
            //				iInterval = 6;
            //			}
            if (period.Equals("1Y"))
            {
                // needs to overwrite string completely if it is one month
                strSQL = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(mm,-12,getdate()) AND DATEPART(Dw,DATE)=DATEPART(Dw, (select max(date) from export_price WHERE CONT_ID=@CONTRACT)) ORDER BY DATE";
                iPreRead = 0; //24;
                iInterval = 0; //6;
                range = 4;

                tmpSQL = "select COUNT(*) from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(mm,-12,getdate()) AND DATEPART(Dw,DATE)=DATEPART(Dw, (select max(date) from export_price WHERE CONT_ID=@CONTRACT))";
                cmdData = new SqlCommand(tmpSQL, conn);
                cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                count = Convert.ToInt32(cmdData.ExecuteScalar());

            }


            cmdData = new SqlCommand(strSQL, conn);
            cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
            dtrData = cmdData.ExecuteReader();

            SeriesCollection SC = new SeriesCollection();

            for (int k = 0; k < iPreRead; k++)
            {
                if (dtrData.Read())
                { }
            }
            int i = 1;
            int add = 0;

            if (count != 0)
            {
                add = count % range;
            }


            Series s = new Series();

            while (dtrData.Read())
            {
                if (dtrData["DATE"] != DBNull.Value && dtrData["PRICE"] != DBNull.Value)
                {
                    Element e = new Element();
                    //					e.ToolTip = dtrData["PRICE"].ToString();
                    //					e.SmartLabel.DynamicDisplay = false;
                    //					e.SmartLabel.DynamicPosition = true;
                    //					e.ShowValue = false;
                    //e.Name = dtrData["DATE"].ToString();

                    if (((i - add) % range) == 0)
                    {
                        e.Name = Convert.ToDateTime(dtrData["DATE"]).ToString("MMM dd\r\nyyy");
                        //						e.SmartLabel.Text = dtrData["PRICE"].ToString();
                        e.SmartLabel.Color = Color.White;
                        e.XDateTime = (DateTime)dtrData["DATE"];
                    }
                    else
                    {
                        //e.Name = "";
                        e.SmartLabel.Text = "";
                    }

                    i++;


                    //e.Color = Color.Blue;//Color.FromArgb(255, 203, 1);
                    //					e.AxisMarker.Label.Color = Color.Red;

                    e.YValue = Convert.ToDouble(dtrData["PRICE"]);
                    //                    e.Hotspot.ToolTip = Convert.ToDouble(dtrData["PRICE"]).ToString();
                    /* wanted to have 'hand-cursor' while over values
                    e.Hotspot.Attributes.Custom.Add("onmouseover", "this.style.cursor='hand'");
                    e.Hotspot.Attributes.Custom.Add("onmouseout", "this.style.cursor='pointer'");
                    */
                    //                    s.Element.ShowValue = true;
                    s.Elements.Add(e);
                }

                if (period.Equals("6M") || period.Equals("1Y"))// || period.Equals("10Y")
                {
                    for (int k = 0; k < iInterval; k++)
                    {
                        if (dtrData.Read())
                        { }
                    }
                }
            }
            SC.Add(s);

            //ChartObj.Depth = 15;
            // Set 3D



            return (SC);
        }

        private void loadDDL()
        {

            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, "SELECT * FROM CONTRACT order by cont_ordr ");
                while (dtr.Read())
                {
                    ddlChart.Items.Add(new ListItem(dtr["CONT_LABL"].ToString(), dtr["CONT_ID"].ToString()));
                }

                ddlChart.Items[0].Selected = true;
            }

        }

        private void BindDG()
        {

            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                //  Hashtable param = new Hashtable();
                //  param.Add("@cont_id", ddlChart.SelectedValue);
                string sSql;

                sSql = string.Format("SELECT * FROM EXPORT_PRICE WHERE CONT_ID={0} AND DATE >= '02-04-2004' ORDER BY DATE DESC", ddlChart.SelectedValue);

                if (cbMonth.Checked)
                {

                    sSql = string.Format("select PRICE_DATE as Date, Price as ASK from HISTORICAL_PRICES WHERE CONT_ID={0} ORDER BY DATE DESC", ddlChart.SelectedValue);
                }

                if (cbWeek.Checked)
                {

                    sSql = string.Format("SELECT * FROM EXPORT_PRICE WHERE CONT_ID={0} AND DATE >= '02-04-2004' and  datepart(dw,date) = 6 ORDER BY DATE DESC", ddlChart.SelectedValue);
                }

                DataSet DS;
                SqlDataAdapter MyCommand;
                MyCommand = new SqlDataAdapter(sSql, conn);
                DS = new DataSet();
                MyCommand.Fill(DS, "tblDynamicText");

                if (!cbMonth.Checked)
                {
                    if (!dgDaily.Visible)
                    {
                        dgDaily.CurrentPageIndex = 0;
                    }
                    //dgDaily.CurrentPageIndex = 0;
                    dgDaily.DataSource = DS;
                    dgDaily.DataBind();
                    dgDaily.Visible = true;
                    dgMonthly.Visible = false;
                }
                else
                {
                    if (!dgMonthly.Visible)
                    {
                        dgMonthly.CurrentPageIndex = 0;
                    }
                    //dgMonthly.CurrentPageIndex = 0;
                    dgMonthly.DataSource = DS;
                    dgMonthly.DataBind();
                    dgMonthly.Visible = true;
                    dgDaily.Visible = false;
                }

                //dg.DataSource = DS;
                //dg.DataBind();
                conn.Close();
                //Hashtable param = new Hashtable();
                //param.Add("@cont_id", ddlChart.SelectedValue);
                //param.Add("@date", "02-04-2004");

                //DBLibrary.BindDataGrid(Application["DBConn"].ToString(), dg, "SELECT * FROM EXPORT_PRICE WHERE CONT_ID=@CONT_ID AND DATE >= @DATE ORDER BY DATE DESC", param);

            }
        }

        private void dgMonthly_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            // We use CommandEventArgs e to get the row which is being clicked 
            // This also changes the DataGrid labels into Textboxes so user can edit them 
            dgMonthly.EditItemIndex = e.Item.ItemIndex;
            // Always bind the data so the datagrid can be displayed. 
            BindDG();
        }

        private void dgDaily_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            // We use CommandEventArgs e to get the row which is being clicked 
            // This also changes the DataGrid labels into Textboxes so user can edit them 
            dgDaily.EditItemIndex = e.Item.ItemIndex;
            // Always bind the data so the datagrid can be displayed. 
            BindDG();
        }

        public void Check_Clicked(Object sender, EventArgs e)
        {

            cbWeek.Checked = false;
            BindDG();
        }
        public void Check_Clicked2(Object sender, EventArgs e)
        {

            cbMonth.Checked = false;
            dgDaily.CurrentPageIndex = 0;
            BindDG();
        }
        protected void dgDaily_ItemDataBound(object sender, DataGridItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DateTime RowDate = Convert.ToDateTime(e.Item.Cells[1].Text);

                if (RowDate.DayOfWeek.ToString() == "Friday")
                {
                    if (!cbWeek.Checked)
                        e.Item.BackColor = Color.Orange;

                }

            }

        }

        private void dgMonthly_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            // All we do in the cancel method is to assign '-1' to the datagrid editItemIndex 
            // Once the edititemindex is set to '-1' the datagrid returns back to its original condition 
            dgMonthly.EditItemIndex = -1;
            BindDG();
        }

        private void dgDaily_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            // All we do in the cancel method is to assign '-1' to the datagrid editItemIndex 
            // Once the edititemindex is set to '-1' the datagrid returns back to its original condition 
            dgDaily.EditItemIndex = -1;
            BindDG();
        }


        private void dgMonthly_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {

            string strDate = e.Item.Cells[1].Text;
            //e.Item.Controls[2]


            string strAsk = ((TextBox)e.Item.Cells[2].Controls[0]).Text;

            string cont_id = ddlChart.SelectedValue;
            Hashtable htParams = new Hashtable();
            htParams.Add("@ask", strAsk);
            htParams.Add("@cont_id", cont_id);
            htParams.Add("@DATE", strDate);


            // updating existing item
            string strSqlUpdate = "Update HISTORICAL_PRICES Set price=@ask WHERE CONT_ID=@cont_id AND YEAR(PRICE_DATE)=YEAR(@DATE) AND MONTH(PRICE_DATE)=MONTH(@DATE) AND DAY(PRICE_DATE)=DAY(@DATE)";
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlUpdate, htParams);

            dgMonthly.EditItemIndex = -1;
            BindDG();

        }
        private void dgDaily_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {

            string strDate = e.Item.Cells[1].Text;
            //e.Item.Controls[2]

            string strBid = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
            string strAsk = ((TextBox)e.Item.Cells[3].Controls[0]).Text;

            string cont_id = ddlChart.SelectedValue;
            Hashtable htParams = new Hashtable();
            htParams.Add("@ask", strAsk);
            htParams.Add("@bid", strBid);
            htParams.Add("@cont_id", cont_id);
            htParams.Add("@DATE", strDate);


            // updating existing item
            string strSqlUpdate = "Update Export_Price Set BID=@bid, ASK=@ask WHERE CONT_ID=@cont_id AND YEAR(DATE)=YEAR(@DATE) AND MONTH(DATE)=MONTH(@DATE) AND DAY(DATE)=DAY(@DATE)";
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlUpdate, htParams);

            dgDaily.EditItemIndex = -1;
            BindDG();

        }
        public void myDataGrid_PageChanger(object Source, DataGridPageChangedEventArgs E)
        {
            dgDaily.CurrentPageIndex = E.NewPageIndex;
            dgMonthly.CurrentPageIndex = E.NewPageIndex;
            BindDG();
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgMonthly.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgMonthly_EditCommand);
            this.dgMonthly.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgMonthly_UpdateCommand);

            this.dgDaily.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgDaily_EditCommand);
            this.dgDaily.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgDaily_UpdateCommand);

        }
        #endregion

        protected void ddlChart_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BindDG();
            CreateChart();
        }


    }
}
