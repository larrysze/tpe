using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;
using dotnetCHARTING;

namespace localhost.Research
{
	/// <summary>
	/// Summary description for ChangeDataSFChart.
	/// </summary>
	public partial class ChangeDataSFChart : System.Web.UI.Page
	{

        protected void Page_Load(object sender, System.EventArgs e)
        {

            Master.Width = "760px";
            // Put user code to initialize the page here

            if ((string)Session["Typ"] != "A")
            {
                Response.Redirect("../default.aspx");
            }

            if (!IsPostBack)
            {
                loadDDL();
                BindDG();
                createGradeChart();
            }
        }

        private void loadDDL()
        {
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, "SELECT * FROM GRADE");
                while (dtr.Read())
                {
                    ddlChart.Items.Add(new ListItem(dtr["GRADE_NAME"].ToString(), dtr["GRADE_ID"].ToString()));
                }

                ddlChart.Items[0].Selected = true;
            }
        }

        private void BindDG()
        {
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                Hashtable param = new Hashtable();
                param.Add("@resin_grade_id", ddlChart.SelectedValue);
                param.Add("@date", "01-01-2004");

                DBLibrary.BindDataGrid(Application["DBConn"].ToString(), dg, "SELECT * FROM Spot_Offers_Summary WHERE GRADE_ID=@RESIN_GRADE_ID AND DATE >= @DATE ORDER BY DATE DESC", param);

            }
        }


        private void dg_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            // We use CommandEventArgs e to get the row which is being clicked 
            // This also changes the DataGrid labels into Textboxes so user can edit them 
            dg.EditItemIndex = e.Item.ItemIndex;
            // Always bind the data so the datagrid can be displayed. 
            createGradeChart();
            BindDG();
        }


        private void dg_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            // All we do in the cancel method is to assign '-1' to the datagrid editItemIndex 
            // Once the edititemindex is set to '-1' the datagrid returns back to its original condition 
            dg.EditItemIndex = -1;
            BindDG();
        }

        private void dg_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {

            string strDate = e.Item.Cells[1].Text;
            //e.Item.Controls[2]

            string strPrice_Low = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
            string strPrice_High = ((TextBox)e.Item.Cells[3].Controls[0]).Text;
            string strVolume = ((TextBox)e.Item.Cells[4].Controls[0]).Text;

            string grade_id = ddlChart.SelectedValue;
            Hashtable htParams = new Hashtable();
            htParams.Add("@price_high", strPrice_High);
            htParams.Add("@price_low", strPrice_Low);
            htParams.Add("@grade_id", grade_id);
            htParams.Add("@DATE", strDate);
            htParams.Add("@volume", RemoveComma(strVolume));

            // updating existing item
            string strSqlUpdate = "Update Spot_Offers_Summary Set price_low=@price_low, price_high=@price_high, weight=@volume WHERE GRADE_ID=@GRADE_ID AND YEAR(DATE)=YEAR(@DATE) AND MONTH(DATE)=MONTH(@DATE) AND DAY(DATE)=DAY(@DATE)";
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlUpdate, htParams);

            dg.EditItemIndex = -1;
            BindDG();
            createGradeChart();

        }

        private string RemoveComma(string weight)
        {
            int commaPlace = weight.IndexOf(",");
            return weight.Remove(commaPlace, 1);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dg.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_EditCommand);
            this.dg.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_UpdateCommand);

        }
        #endregion

        protected void ddlChart_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BindDG();
            createGradeChart();
        }

        private SeriesCollection getPriceData(int grade_id, string period)
        {
            DateTime mydate = new DateTime();
            mydate = DateTime.Now;

            string Date;

            int day = mydate.Day;
            int month = mydate.Month;
            int year = mydate.Year;

            SeriesCollection SC = new SeriesCollection();

            Series s = new Series();
            s.Name = "Price Range";

            if (period == "1M")
                if (month == 1)
                {
                    month = 12;
                }
                else
                {
                    month = month - 1;
                }
            if (period == "1Y")
                year = year - 1;
            if (period == "5Y")
                year = year - 5;

            Date = month.ToString() + '-' + day.ToString() + '-' + year.ToString();

            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = null;
                Hashtable param = new Hashtable();
                param.Add("@resin_grade_id", grade_id);
                param.Add("@date", Date);

                dtr = DBLibrary.GetDataReaderStoredProcedure(conn, "spSpot_Offers_SummaryByResinGrade", param);
                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["friday_date"].ToString());
                    Element e = new Element();
                    e.XDateTime = date;


                    //e.SmartLabel.Color = Color.White;



                    double price_low = Convert.ToDouble(dtr["price_low"].ToString());
                    double price_high = Convert.ToDouble(dtr["price_high"].ToString());

                    e.Close = price_high;
                    e.Open = price_low;

                    e.High = price_high;
                    e.Low = price_low;

                    s.Elements.Add(e);

                }
            }


            SC.Add(s);
            return (SC);
        }

        private void createGradeChart()
        {

            string[] period1 = new string[3];
            period1[0] = "1M";
            period1[1] = "1Y";
            period1[2] = "5Y";
            string nameFile = "Chart";
            int grade_id = Convert.ToInt32(ddlChart.SelectedValue);

            for (int k = 0; k < 3; k++)
            {

                Chart.FileName = nameFile + "_" + ddlChart.SelectedValue + "_" + period1[k];

                Chart.ChartArea.ClearColors();
                Chart.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
                Chart.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);


                // Set the chart type
                Chart.Type = ChartType.Combo;

                Chart.OverlapFooter = true;
                Chart.Mentor = false;
                // Set the size
                Chart.Width = 390;
                Chart.Height = 225;
                // Set the temp directory
                Chart.TempDirectory = "temp";
                // Debug mode. ( Will show generated errors if any )
                Chart.Debug = true;
                //Chart.Title = "Historical Price and Volume: " + ddlResinType.SelectedItem;
                //Chart.Title = "Historical Spot Resin Offers";

                Chart.DefaultSeries.Type = SeriesType.AreaLine;

                Chart.DefaultElement.Marker.Visible = false;

                Color[] MyColorObjectArray = new Color[3] { Color.Red, Color.FromArgb(255, 202, 0), Color.Yellow };
                Chart.Palette = MyColorObjectArray;

                Chart.LegendBox.Visible = false;
                Chart.ChartAreaSpacing = 8;

                Chart.DefaultSeries.DefaultElement.Transparency = 20;

                Chart.ChartArea.XAxis.DefaultTick.Line.Color = Color.FromArgb(255, 202, 0);
                Chart.ChartArea.YAxis.DefaultTick.Line.Color = Color.FromArgb(255, 202, 0);
                Chart.XAxis.TimeScaleLabels.MonthFormatString = "MMM dd \n yyyy";
                Chart.XAxis.DefaultTick.Label.Text = "<%Value,MMM dd> \n <%Value,yyyy>";
                Chart.ChartArea.YAxis.DefaultTick.Label.Font = new Font(Chart.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
                Chart.ChartArea.XAxis.DefaultTick.Label.Font = new Font(Chart.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);
                Chart.XAxis.LabelRotate = true;

                Chart.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
                Chart.ChartArea.XAxis.TickLabelAngle = 90;

                // Setup the axes.
                //Chart.YAxis.Label.Text = "Price";
                //          Chart.YAxis.FormatString = "Currency";
                Chart.YAxis.Scale = Scale.Range;
                Chart.XAxis.Scale = Scale.Range;

                SeriesCollection mySC = getPriceData(grade_id, period1[k]);
                mySC[0].Type = SeriesTypeFinancial.Bar;
                // SeriesCollection mySCV = getVolumeData(grade_id);

                Chart.ChartArea.Line.Color = Color.Orange;

                // Add the price data.
                Chart.SeriesCollection.Add(mySC);


                //Chart
                //Chart.TitleBox.Background.Color = Color.Orange;
                Chart.Background.Color = Color.Black;
                Chart.ChartArea.Background.Color = Color.Black;
                Chart.ChartArea.DefaultSeries.Line.Color = Color.Red;
                Chart.ChartArea.DefaultSeries.Line.Width = 2;

                Chart.ChartArea.XAxis.Line.Width = 4;
                Chart.ChartArea.YAxis.Line.Width = 2;

                //XAxis
                Chart.XAxis.Label.Color = Color.White;
                Chart.ChartArea.XAxis.Label.Color = Color.White;
                Chart.XAxis.Line.Color = Color.Orange;
                Chart.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;

                //YAxis
                Chart.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
                Chart.ChartArea.YAxis.Label.Color = Color.White;
                Chart.YAxis.Line.Color = Color.Orange;
                Chart.YAxis.Label.Color = Color.White;

                Chart.ChartArea.Line.Width = 2;

                Chart.TempDirectory = Server.MapPath("/Research/charts/pv");

                Chart.FileManager.ImageFormat = ImageFormat.Png;
                //                            ChartObj.OverlapFooter = true;

                Bitmap bmp1 = Chart.GetChartBitmap();

                string fileName2 = "";
                fileName2 = Chart.FileManager.SaveImage(bmp1);

                Chart.SeriesCollection.Clear();

            }
            imgChart.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/charts/pv/" + nameFile + "_" + ddlChart.SelectedValue + "_1M.png";
            imgChart1.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/charts/pv/" + nameFile + "_" + ddlChart.SelectedValue + "_1Y.png";
            imgChart2.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/charts/pv/" + nameFile + "_" + ddlChart.SelectedValue + "_5Y.png";
        }

        protected void dg_ItemDataBound(object sender, DataGridItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DateTime RowDate = Convert.ToDateTime(e.Item.Cells[1].Text);

                if (RowDate.DayOfWeek.ToString() == "Friday")
                {

                    e.Item.BackColor = Color.Orange;

                }

            }

        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {


            BindDG();
            createGradeChart();

        }


        

	}
}
