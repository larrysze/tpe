using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Research
{
	/// <summary>
	/// Summary description for WeeklyReview.
	/// </summary>
	public partial class WeeklyReview : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblPrintLink;

	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(!Page.IsPostBack)
			{
				initDDL();                
				if(Request["date"] != null)
				{
					try
					{
						DateTime review_date = new DateTime(Convert.ToInt64(Request["date"]));
                        ddlIssue.SelectedValue = review_date.ToShortDateString();
					}
					catch(Exception ee)
					{
						loadHTML("");
					}
				}
            }

            BindGrid(ddlIssue.SelectedValue);
            loadHTML(ddlIssue.SelectedValue);			
		}

        private void BindGrid(string strDate)
        {         
            Hashtable htParam = new Hashtable();
            htParam.Add("@date", strDate);            
            TPE.Utility.DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(),dgSpotFloorSummary,"spWeekInReviewSpotOffersSnapshot",htParam);

            string seleStr = "SELECT SUM(weight) AS TotalVolume FROM WeeklyReviewSpotOffersSummary WHERE datediff(day,date,@date)=0";
            string total = DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), seleStr, htParam).ToString();
            if (total.Trim().Equals(""))
            {
                lblTotalVolume.Text = "(Not available)";
            }
            else
            {
                lblTotalVolume.Text = "(" + String.Format("{0:#,###}", decimal.Parse(total)) + " lbs)";
            }
        }

		private void initDDL()
		{
			using(SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
				SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, "SELECT REVIEW_DATE FROM WEEKLY_REVIEWS WHERE published = 1 ORDER BY REVIEW_DATE DESC");
				while(dtr.Read())
				{
					DateTime temp_date = Convert.ToDateTime(dtr["REVIEW_DATE"].ToString());
					ddlIssue.Items.Add(temp_date.ToShortDateString());

				}

			}
		}

		private void loadHTML(string review_date)
		{

			if(review_date != "")
			{
				DateTime review = Convert.ToDateTime(review_date);
				review_date = " AND Year(review_date)=" + review.Year + " AND Month(review_date)=" + review.Month + " AND Day(review_date)=" + review.Day;
			}
			else
			{
				review_date = " AND review_date = (select max(review_date) from weekly_reviews where published=1)";
			}

            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                conn.Open();
                SqlDataReader dtr = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn, "SELECT * FROM WEEKLY_REVIEWS WHERE published=1 " + review_date);
				if(dtr.Read())
				{
                    string total = dtr["TOTAL"].ToString();
                    string issue_date = dtr["REVIEW_DATE"].ToString();

                    string snapshot = dtr["SPOT_FLOOR"].ToString();
                    string strVolumePP = dtr["volumePP"].ToString();
                    string strVolumePS = dtr["volumePS"].ToString();
                    string strVolumePE = dtr["volumePE"].ToString();
                    string strPricePP = dtr["pricePP"].ToString();
                    string strPricePS = dtr["pricePS"].ToString();
                    string strPricePE = dtr["pricePE"].ToString();

                    string strSummary = HelperFunction.ConvertCarriageReturnToBR(dtr["COMMENTS"].ToString());
                    
                    string strPreTextPP = HelperFunction.ConvertCarriageReturnToBR(dtr["pretext_pp"].ToString());
                    string strPreTextPS = HelperFunction.ConvertCarriageReturnToBR(dtr["pretext_ps"].ToString());
                    string strPreTextPE = HelperFunction.ConvertCarriageReturnToBR(dtr["pretext_pe"].ToString());
                                        		
                    string strPP = HelperFunction.ConvertCarriageReturnToBR(dtr["POLYPROPYLENE"].ToString());
                    string strPS = HelperFunction.ConvertCarriageReturnToBR(dtr["POLYSTYRENE"].ToString());
                    string strPE = HelperFunction.ConvertCarriageReturnToBR(dtr["POLYETHYLENE"].ToString());                                                       
                    
					DateTime date_obj = Convert.ToDateTime(issue_date);
					issue_date = date_obj.ToString("MMMM") + " " + date_obj.Day + ", " + date_obj.Year;

                    this.lblDate.Text = issue_date;

                    //charts:
                    //lblSpotFloorSummaryChart.Text = snapshot;
                    //lblTotalVolume.Text = "(" + total + ")";
					imgPEMonthChart.ImageUrl = dtr["PATH1"].ToString();
					imgPEYearChart.ImageUrl = dtr["PATH2"].ToString();
					imgPPMonthChart.ImageUrl = dtr["PATH3"].ToString();
					imgPPYearChart.ImageUrl = dtr["PATH4"].ToString();
					imgPSMonthChart.ImageUrl = dtr["PATH5"].ToString();
					imgPSYearChart.ImageUrl = dtr["PATH6"].ToString();

                    // Summary text
                    lblSummary.Text = Highlight(strSummary);

                    // Polyethylene:
                    lblPEVolume.Text = strVolumePE;
                    lblPEPrice.Text = strPricePE;
                    lblPEIntro.Text = Highlight(strPreTextPE);
                    lblPEBody.Text = Highlight(strPE);

                    // Polypropylene:
                    lblPPVolume.Text = strVolumePP;
                    lblPPPrice.Text = strPricePP;
                    lblPPIntro.Text = Highlight(strPreTextPP);
                    lblPPBody.Text = Highlight(strPP);

                    // Polystyrene:
                    lblPSVolume.Text = strVolumePS;
                    lblPSPrice.Text = strPricePS;
                    lblPSIntro.Text = Highlight(strPreTextPS);
                    lblPSBody.Text = Highlight(strPS);
				}
			}
		}

        private string Highlight(string text)
        {
            if ((Request.QueryString["SearchTerm"] != null) && (Request.QueryString["SearchTerm"].ToString().Length > 2))	// don't highlight short words
            {
                text = HelperFunction.Highlight(Request.QueryString["SearchTerm"].ToString(), text);
            }
            return text;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
