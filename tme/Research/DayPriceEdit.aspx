b<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DayPriceEdit.aspx.cs" Inherits="localhost.Research.DayPriceEdit"  MasterPageFile="~/MasterPages/menu.master" Title="DayPriceEdit" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ MasterType VirtualPath="~/MasterPages/Menu.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphMain">
   <table>
   <tr>
   <td>
    <br />
    <asp:DropDownList ID="ddlChart" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlChart_SelectedIndexChanged" CssClass="InputForm">
    </asp:DropDownList>
        
    <br>
    <asp:CheckBox ID="cbMonth" runat="server" CssClass="Content" Text="Monthly" AutoPostBack="True" OnCheckedChanged="Check_Clicked" />
    <asp:CheckBox ID="cbWeek" runat="server" CssClass="Content" Text="Weekly" AutoPostBack="True" OnCheckedChanged="Check_Clicked2" /><br>   
    
    <asp:DataGrid ID="dgDaily" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="Content LinkNormal" ItemStyle-CssClass="Content LinkNormal"
                    AllowPaging="True" PageSize="100" PagerStyle-CssClass = "Content LinkNormal" PagerStyle-Mode="NumericPages"  OnPageIndexChanged="myDataGrid_PageChanger" 
                    PagerStyle-NextPageText="Next ->" PagerStyle-PrevPageText="<- Previous" PagerStyle-Position="TopAndBottom"
                     CellPadding="5" CellSpacing="5" OnItemDataBound="dgDaily_ItemDataBound">
        <Columns>
            <asp:EditCommandColumn ItemStyle-CssClass="Content LinkNormal" UpdateText="Update" CancelText="Cancel" EditText="Edit">
              
            </asp:EditCommandColumn>
            <asp:BoundColumn DataField="DATE" ReadOnly="True" HeaderText="Date" DataFormatString="{0:d}"  />
            <asp:BoundColumn DataField="BID" HeaderText="BID" />
            <asp:BoundColumn DataField="ASK" HeaderText="ASK" />
        </Columns>     
        <ItemStyle CssClass="LinkNormal" />
        <ItemStyle CssClass="Content" />
        <HeaderStyle CssClass="Header" />        
    </asp:DataGrid>
   
    
    <asp:DataGrid ID="dgMonthly" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="Content LinkNormal" ItemStyle-CssClass="Content LinkNormal"
                    AllowPaging="True" PageSize="100" PagerStyle-CssClass = "Content LinkNormal" PagerStyle-Mode="NumericPages"  OnPageIndexChanged="myDataGrid_PageChanger" 
                    PagerStyle-NextPageText="Next ->" PagerStyle-PrevPageText="<- Previous" PagerStyle-Position="TopAndBottom" 
                    CellPadding="5" CellSpacing="5">
        <Columns>
            <asp:EditCommandColumn ItemStyle-CssClass="Content LinkNormal" UpdateText="Update" CancelText="Cancel" EditText="Edit">
              
            </asp:EditCommandColumn>
            <asp:BoundColumn DataField="DATE" ReadOnly="True" HeaderText="Date" DataFormatString="{0:d}" />
            <asp:BoundColumn DataField="ASK" HeaderText="ASK" />
        </Columns>     
        <ItemStyle CssClass="LinkNormal" />
        <ItemStyle CssClass="Content" />
        <HeaderStyle CssClass="Header" />        
    </asp:DataGrid>
    
  </td>
  <td valign="top">    
<br />

<br /><br />    
<asp:Image runat="server" ID="imgChart" Visible="true" /><br /><br />
<asp:Image runat="server" ID="imgChart1" Visible="true" /><br /><br />
<asp:Image runat="server" ID="imgChart2" Visible="true" /><br /><br />
    
    
<dotnet:Chart id="ChartObj" runat="server" Visible="false">
<TitleBox Position="Left">
</TitleBox>

<DefaultLegendBox Padding="4" Visible="True" CornerBottomRight="Cut">

<HeaderEntry SortOrder="-1" Visible="False" Value="Value" Name="Name">

<DividerLine Color="Gray">
</DividerLine>

<LabelStyle Font="Arial, 8pt, style=Bold">
</LabelStyle>

</HeaderEntry>

</DefaultLegendBox>

<DefaultTitleBox Visible="True">

</DefaultTitleBox>

<DefaultElement>

<DefaultSubValue>

<Line Color="93, 28, 59" Length="4">
</Line>

</DefaultSubValue>

</DefaultElement>
</dotnet:Chart>

</td>
</tr>
</table>
</asp:Content>
