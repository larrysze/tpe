using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using dotnetCHARTING;
using localhost.include;
using System.IO;



namespace localhost.Research
{
    /// <summary>
    /// Summary description for FillPricesForm.
    /// </summary>
    public partial class Charts : System.Web.UI.Page
    {


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

        private void Page_Load(Object sender, EventArgs e)
        {
            if (Session["Typ"] == "")
            {
                Session["strLoginURL"] = Request.RawUrl.ToString();
                Response.Redirect("/common/FUNCLogin.aspx");
            }
            if (!IsPostBack)
            {
                SqlConnection conn;
                conn = new SqlConnection(Application["DBconn"].ToString());
                conn.Open();
                // defaults charts depth to five year
                ViewState["ChartType"] = "5 year";

                // opening contract list
                SqlCommand cmdContract;
                SqlDataReader dtrContract;

                cmdContract = new SqlCommand("SELECT CONT_ID,CONT_LABL FROM Contract ORDER BY CONT_ORDR", conn);
                dtrContract = cmdContract.ExecuteReader();

                ddlProduct.Items.Clear();
                bool anySelected = false;
                while (dtrContract.Read())
                {
                    ListItem liContract = new ListItem(dtrContract["CONT_LABL"].ToString(), dtrContract["CONT_ID"].ToString());
                    if (Request.QueryString["Cont_Id"] != null)
                    {
                        if (dtrContract["CONT_ID"].ToString().Equals(Request.QueryString["Cont_Id"].ToString()))
                        {
                            if (!anySelected)
                            {
                                liContract.Selected = true;
                                anySelected = true;
                            }
                        }
                    }
                    ddlProduct.Items.Add(liContract);
                }
                dtrContract.Close();
                conn.Close();

            }
        }
    }
}