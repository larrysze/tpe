<%@ Page Language="C#" AutoEventWireup="true" Codebehind="Charts.aspx.cs" Inherits="localhost.Research.Charts" MasterPageFile="~/MasterPages/Template.Master" Title="Charts" %>

<%@ MasterType VirtualPath="~/MasterPages/Template.Master" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">

    <script language="javascript">
	<!--
	function changeChart(filename) {
		document.getElementById("loading").style.display = 'block';

		var chartInnerHTML = "<IMG border='0' name='ChartImg' src='/Research/Charts/" + filename + ".png'>";
		
		document.getElementById('<%=chartImg.ClientID %>').innerHTML = chartInnerHTML;
		
		document.getElementById("loading").style.display = 'none';
    }

    
    function changeContract() 
    {
		changeChart('B_Chart_' + document.getElementById('<%= ddlProduct.ClientID %>').value + '_1Y');
    }

    function changePeriod(period)
    {
		changeChart('B_Chart_' + document.getElementById('<%= ddlProduct.ClientID %>').value + '_' + period);
    }

	function onLoad()
	{
		changeChart('B_Chart_2_5Y');
	}
	//-->
	
    </script>

</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphCssLink">
    <style type="text/css">
.divpost {
	BORDER-RIGHT: #999999 1px solid; PADDING-RIGHT: 20px; BORDER-TOP: #999999 1px solid; PADDING-LEFT: 20px; BACKGROUND: #000000; PADDING-BOTTOM: 15px; MARGIN: 1px 0px; BORDER-LEFT: #999999 1px solid; WIDTH: 188px; PADDING-TOP: 15px; BORDER-BOTTOM: #999999 1px solid
}
.menu3 {
	FONT-WEIGHT: bold; FONT-SIZE: 11px; BACKGROUND-IMAGE: url(imagens/charts/orange_background.jpg); VERTICAL-ALIGN: baseline; WIDTH: 70px; COLOR: #000000; FONT-STYLE: normal; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; HEIGHT: 20px; TEXT-ALIGN: center; TEXT-DECORATION: none
}
</style>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphInstructions">
    <table class='InstructionBullets'>
        <tr>
            <td align='right' height="20" width="80">
            </td>
            <td>
                Chart values are based on The Plastics Exchange Generic Prime railcar asking price, delivered North America. These prices are updated daily.</td>
        </tr>
        <tr>
            <td align='right' height="20" width="80">
                <img src='../pics/bullet.gif' /></td>
            <td>
                1 month Charts, each data point is captured at close of business each day.
            </td>
        </tr>
        <tr>
            <td align='right' height="20" width="80">
                <img src='../pics/bullet.gif' /></td>
            <td>
                1 year Charts, each data point is captured at close of business each Friday.
            </td>
        </tr>
        <tr>
            <td align='right' height="20" width="80">
                <img src='../pics/bullet.gif' /></td>
            <td>
                5 year Charts, each data point is captured at close of business each month.
            </td>
        </tr>
        <tr>
            <td align='right' height="20" width="80">
                <img src='../pics/bullet.gif' /></td>
            <td>
                10 year Charts, each data point is captured at close of business every other month.
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphHeading">
    <div class="Header Bold Color1">
        <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td valign="middle">
                    &nbsp;&nbsp;&nbsp;<font color="#fea403">Resin Charts</font></td>
                <td align="right">
                    <img src="/Pics/select_resin.jpg" onload="onLoad()">
                        <asp:DropDownList ID="ddlProduct" CssClass="InputForm" Height="22px" runat="server" name="ddlProduct" onchange="changeContract()">
                        </asp:DropDownList>
                </td>
            </tr>
            
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
    <div id="loading" style="padding-right: 2px; display: none; padding-left: 2px; right: 1px; padding-bottom: 2px; margin: 2px; color: white; padding-top: 2px; position: absolute; top: 1px; background-color: red">
        Loading...</div>
    <table id='Table2"' height="100%" cellspacing="0" cellpadding="0" width="780" bgcolor="#000000" border="0">
        <tbody>
            <tr valign="top">
                <td>
                    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td valign="middle">
                            </td>
                        </tr>
                        <tr valign="middle">
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td valign="bottom" align="center">
                                <table cellspacing="0" cellpadding="0" align="center" border="0">
                                    <tr width="50%">
                                        <td onmouseover="changePeriod('1M');" align="center" bgcolor="#fea503">
                                            <b>1 month</b></td>
                                        <td bgcolor='black' width='1'>
                                        </td>
                                        <td align="center" bgcolor='#fea503' onmouseover="changePeriod('1y');">
                                            <b>1 year</b></td>
                                        <td bgcolor='black' width='1'>
                                        </td>
                                        <td align="center" bgcolor='#fea503' onmouseover="changePeriod('5y');">
                                            <b>5 years</b></td>
                                        <td bgcolor='black' width='1'>
                                        </td>
                                        <td align="center" bgcolor='#fea503' onmouseover="changePeriod('10y');">
                                            <b>10 years</b></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="7" width="500" height="330">
                                            <div id="chartImg" align="center" runat="server">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
            </tr>
        </tbody>
    </table>
</asp:Content>
