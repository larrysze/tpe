// **********************************
// * Nom du script : querystring_fonc v1.0
// * Auteur: Fr�d�ric REMISE (Derf)
// * Date de cr�ation: 23/01/2003
// * Email : 
// **********************************

// R�cup�ration de la requ�te contenue dans l'URL (sans le ?)
var req = window.location.search.substr(1,window.location.search.length);

// R�cup�ration des paires param�tre=valeur
var dbl = req.split('&');

var aPrm = new Array();    // Pour stock. le nom des param�tres
var aVal = new Array();    // Pour stock. la valeur des param�tres
var objQS = new Object();  // Objet pour stock. le nom des param�tres

for (i=0;i < dbl.length;i++) {
  // Recup. le nom des parametres (Attention : elem.1 du tab. = param.1)
  aPrm[i+1] = dbl[i].substring(0,dbl[i].indexOf('='));
  
  // Recup. la valeur des parametres (Attention : elem.1 du tab. = val.param.1)
  aVal[i+1] = unescape(dbl[i].substring(dbl[i].indexOf('=')+1,dbl[i].length));
  
  // Stock la valeur des param�tres sous forme de propri�t�
  objQS[aPrm[i+1]] = aVal[i+1];
}

// Renvoie la valeur d'un parametre par son nom
function getQueryStringByName(pname) 
{
  return eval("objQS." + pname);
}

// Renvoie la valeur d'un param�tre par sa position dans la requete
// Attention : le premier param�tre de la requete est le 1 etc..
function getQueryStringByPos(ppos)
{
  return aVal[ppos];
}

// Renvoie le nom d'un param�tre par sa position dans la requete
// Attention : le premier param�tre de la requete est le 1 etc..
function getParamNameByPos(ppos)
{
  return aPrm[ppos];
}
