<%@ import Namespace="System.Web.Mail" %>
<%@ Page Language="C#" %>
<script runat="server">
   /************************************************************************
    *   1. File Name       :Research\Research_Default.aspx                 *
    *   2. Description     :defaul page with links to all Research related pages*
    *                       click "Research" tab on top navigation bar to get here*
    *   3. Modification Log:                                                *
    *     Ver No.       Date          Author             Modification       *
    *   -----------------------------------------------------------------   *
    *      1.00      2-22-2004      Zach                First Baseline      *
    *                                                                       *
    ************************************************************************/
    public void Page_Load(object sender, EventArgs e){
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B")){
                Response.Redirect("../default.aspx");
            }
        }

</script>
<head>
<!--#include FILE="../include/headC#.inc"-->
 </head>
 <body>
    <table border=0 width=260 align=center><tr><td>
   <form id="Form1" runat="server">
        <fieldset class="fieldset">
            <legend class="Researchfieldset-legend">Research</legend>
            <table>
                <tbody>
                    <tr>
                        <td>
                        <a href="../Public/Charts.aspx?nav=Research">Historic Charts</a>
			                        <br />
			                        <a href="../forward/Forward_Chart.aspx?nav=Research">Forward Curve</a>
			                        <br />
			                        <a href="../Public/Public_News.aspx?nav=Research">News</a>
			                        <br />
			                        <a href="../Public/Public_Research.asp?a=PE&amp;nav=Research">Research</a>
			                        <br />
			                        <a href="../Public/Public_Traded.aspx?nav=Research">Resin Traded</a>
			                        <br />
			                        <a href="../Public/Public_Traded2.aspx?nav=Research">Contract
			                        Specs</a>
			                        <br />
			                        <a href="../Public/Public_Glossary.asp?nav=Research">Glossary</a>
			                        <br />
			                        <a href="../Public/Public_FAQs.asp?nav=Research">FAQ</a>
                        <br />
                        </td>

                    </tr>
                </tbody>
            </table>
        </fieldset>
        <br />
        </form>
 </td></tr></table>
<!--#include FILE="../include/bottomC#.inc"-->
