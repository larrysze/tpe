<%@ Page Language="c#" Codebehind="WeeklyReview.aspx.cs" AutoEventWireup="True" Inherits="localhost.Research.WeeklyReview" Title="Public Research" MasterPageFile="~/MasterPages/Menu.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
    <asp:Table ID="Table1" runat="server" Width="100%">
        <asp:TableRow ID="trPrint">
            <asp:TableCell Width="35px">&nbsp;</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Width="35px">&nbsp;</asp:TableCell>
            <asp:TableCell Width="35px" Text="
				    &lt;IMG src=&quot;../pics/bullet.gif&quot;&gt;"></asp:TableCell>
            <asp:TableCell CssClass="Header" HorizontalAlign="Left" Text="Past Reports"></asp:TableCell>
            <asp:TableCell>
                <asp:DropDownList runat="server" AutoPostBack="True" CssClass="InputForm" ID="ddlIssue">
                </asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell Width="350px" ColumnSpan="2">
                <asp:Label runat="server" ID="lblEmail"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <div class="DivTitleBar">
        <span class="Header Color1 Bold">Week in Review</span>
    </div>
    <div style="margin-left: 20px; margin-right: 20px;">
        <asp:Table ID="Table2" runat="server" Height="152px" Width="100%">
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2">
                    <span class="LinkNormal"><a href="http://<%#ConfigurationSettings.AppSettings["DomainName"]%>" target="blank" style="float: left">
                        <asp:Image ID="imgLogo" runat="server" ImageAlign="Left" ImageUrl="~/Pics/Market_Update/tpelogo.gif" />
                        <span style="text-align: left; font-family: Arial Black; font-size: 220%; padding-left: 5;"><span style="color: Black">The</span><span style="color: Red">Monomer</span><span style="color: Black">Exchange</span><span style="color: Red">.</span><span style="color: Black">com</span></span> </a></span>
                    <br />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" CssClass="Header" HorizontalAlign="center">
                    <span style="font-size: 200%">Week in Review</span>
                    <br />
                    <asp:Label runat="server" ID="lblDate" CssClass="Header" Style="font-size: 125%"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:Table runat="server" ID="Table3" Width="100%" CellPadding="10" CellSpacing="10">
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="50%"  VerticalAlign="Top">
                    <span style="float: left; text-decoration: underline; font-weight: bold">Spot Floor Summary</span>&nbsp;<asp:Label runat="server" ID="lblTotalVolume" CssClass="Content Bold Color3 Underline"></asp:Label>
                    <asp:DataGrid ID="dgSpotFloorSummary" runat="server" Width="100%" CssClass="LinkNormal Content" BorderWidth="0" BackColor="Black" CellSpacing="1" CellPadding="3" AutoGenerateColumns="False" HorizontalAlign="Center" EnableViewState="False">
                        <HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
                        <ItemStyle CssClass="LinkNormal LightGray" />
                        <AlternatingItemStyle CssClass="LinkNormal DarkGray" />
                        <Columns>
                            <asp:HyperLinkColumn DataNavigateUrlField="grade_id" ItemStyle-HorizontalAlign="Left" DataNavigateUrlFormatString="/spot/Spot_Floor.aspx?Filter={0}" DataTextField="GRADE" HeaderText="Resin">
                                <HeaderStyle CssClass="Content Bold Color2" HorizontalAlign="Left"></HeaderStyle>
                            </asp:HyperLinkColumn>
                            <asp:BoundColumn DataField="varsize" HeaderText="Total lbs" DataFormatString="{0:#,##}">
                                <HeaderStyle CssClass="Content Bold Color2"></HeaderStyle>
                                <ItemStyle Wrap="False" CssClass="Content Color2"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PriceLow" HeaderText="Low" DataFormatString="{0:0.##0}">
                                <HeaderStyle CssClass="Content Bold Color2"></HeaderStyle>
                                <ItemStyle Wrap="False" CssClass="Content Color2"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PriceHigh" HeaderText="High" DataFormatString="{0:0.##0}">
                                <HeaderStyle CssClass="Content Bold Color2"></HeaderStyle>
                                <ItemStyle Wrap="False" CssClass="Content Color2"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="Price Range">
                                <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
                                <ItemStyle Wrap="False" CssClass="Content Color4"></ItemStyle>
                            </asp:TemplateColumn>
                        </Columns>
                        <ItemStyle BorderColor="Black" BorderWidth="0px" />
                    </asp:DataGrid>
                    
                    <br />
                    
                   <font size="1">
                        All transactions are for actual delivery; they are cleared through The Monomer Exchange and are <span class="Color3">totally anonymous.</span>
                            <br />
                            <br />
                            All offers are subject to prior sale and credit approval.
                    </font>                                                    
                    
                </asp:TableCell>
                <asp:TableCell Width="50%" HorizontalAlign="left">
                    <asp:Label runat="server" ID="lblSummary" CssClass="Content TextJustify"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left">
                    <span class="Content Color2 Bold Underline">Polyethylene</span>
                    <br />
                    <br />
                    <span class="Content Bold">Volume: </span>
                    <asp:Label runat="server" ID="lblPEVolume" CssClass="Content"></asp:Label><br>
                    <br />
                    <span class="Content Bold">Price: </span>
                    <asp:Label runat="server" ID="lblPEPrice" CssClass="Content"></asp:Label><br>
                </asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                    <asp:Label runat="server" ID="lblPEIntro" CssClass="Content"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left">
                <center><asp:Label ID="Label4" CssClass="Content Bold" runat="server" Text="HDPE Blow - HIC"></asp:Label><br><br></center>
                    <asp:Image runat="server" ID="imgPEMonthChart" />
                    <center><asp:Label ID="Label12" CssClass="Content Bold" runat="server" Text="1 Month"></asp:Label><br></center>
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Right">
                <center><asp:Label ID="Label6" CssClass="Content Bold" runat="server" Text="HDPE Blow - HIC"></asp:Label><br><br></center>
                    <asp:Image runat="server" ID="imgPEYearChart" />
                    <center><asp:Label ID="Label7" CssClass="Content Bold" runat="server" Text="1 Year"></asp:Label><br></center>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                    <asp:Label runat="server" ID="lblPEBody" CssClass="Content TextJustify"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left">
                    <span class="Content Color2 Bold Underline">Polypropylene</span>
                    <br />
                    <br />
                    <span class="Content Bold">Volume: </span>
                    <asp:Label runat="server" ID="lblPPVolume" CssClass="Content"></asp:Label>
                    <br />
                    <span class="Content Bold">Price: </span>
                    <asp:Label runat="server" ID="lblPPPrice" CssClass="Content"></asp:Label>
                </asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                    <asp:Label runat="server" ID="lblPPIntro" CssClass="Content TextJustify"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left">
                <center><asp:Label ID="Label3" CssClass="Content Bold" runat="server" Text="HoPP Inj - 35 melt"></asp:Label><br><br></center>                   
                    <asp:Image runat="server" ID="imgPPMonthChart" />
                    <center><asp:Label ID="Label8" CssClass="Content Bold" runat="server" Text="1 Month"></asp:Label><br></center>
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Right">
                <center><asp:Label ID="Label1" CssClass="Content Bold"  runat="server" Text="HoPP Inj - 35 melt"></asp:Label><br><br></center>
                <asp:Image runat="server" ID="imgPPYearChart" />
                <center><asp:Label ID="Label9" CssClass="Content Bold" runat="server" Text="1 Year"></asp:Label><br></center>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                    <asp:Label runat="server" ID="lblPPBody" CssClass="Content TextJustify"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left">
                    <span class="Content Color2 Bold Underline">Polystyrene</span>
                    <br />
                    <br />
                    <span class="Content Bold">Volume: </span>
                    <asp:Label runat="server" ID="lblPSVolume" CssClass="Content"></asp:Label>
                    <br />
                    <span class="Content Bold">Price: </span>
                    <asp:Label runat="server" ID="lblPSPrice" CssClass="Content"></asp:Label>
                </asp:TableCell>
                <asp:TableCell></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                    <asp:Label runat="server" ID="lblPSIntro" CssClass="Content"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left">
                <center> <asp:Label ID="Label2" CssClass="Content Bold" runat="server" Text="HIPS Inj - 8 melt"></asp:Label><br><br></center>
                    <asp:Image runat="server" ID="imgPSMonthChart" />
                    <center><asp:Label ID="Label10" CssClass="Content Bold" runat="server" Text="1 Month"></asp:Label><br></center>
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Right">
               <center> <asp:Label ID="Label5" CssClass="Content Bold" runat="server" Text="HIPS Inj - 8 melt"></asp:Label><br><br></center>
                    <asp:Image runat="server" ID="imgPSYearChart" />
                    <center><asp:Label ID="Label11" CssClass="Content Bold" runat="server" Text="1 Year"></asp:Label><br></center>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                    <asp:Label runat="server" ID="lblPSBody" CssClass="Content"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow CssClass="Content">
                <asp:TableCell> 
            <br />
            Michael Greenberg, CEO
            <br />
            The Monomer Exchange
            <br />
            (312) 202-0002
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2">
       <hr width=100% />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow HorizontalAlign="Left">
                <asp:TableCell CssClass="Content" Font-Size="XX-Small" ColumnSpan="2">
Check out The Monomer Exchange before your buy or sell! We have live markets and prices on prime and widespec commodity grade resin in truckloads and railcars, and quality and delivery are guaranteed by our fully integrated credit and logistics - click here to register. We also have access to a wide range of wide-spec resin as well as foreign prime resin for international trade.<br />
Call us at (800) 850-2380 or send an email and we'll source the resin for you. <br />
If you are already a member, many thanks for your continued support. If not, join today! To make purchases, fax us your credit info at (312) 202-0174 or apply online at <a href="http://themonomerexchange.com">www.ThemonomerExchange.com</a>.<br />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2">
       <hr width=100% />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow HorizontalAlign="Left">
                <asp:TableCell CssClass="Content" ColumnSpan="2" Font-Size="XX-Small">
Disclaimer: The information and data in this report is gathered from exchange observations as well as interactions with producers, distributors, brokers, and processors. These sources are considered reliable. The accuracy and completeness of this information is not guaranteed. Any decision to purchase or sell as a result of the opinions expressed in this report will be the full responsibility of the person authorizing such transaction. Our market updates are compiled with integrity and we hope that you find them of value.<br />
Chart values reflect our asking prices of generic prime railcars delivered USA.<br />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</asp:Content>
