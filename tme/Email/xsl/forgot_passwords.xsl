<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:variable name="domain"><xsl:value-of  select="tpe/forgot_passwords/domain"/></xsl:variable>
<xsl:include href="template.xsl"/>

<xsl:template match="/">
<xsl:call-template name="header" />

<table width="779" border="0" align="center" cellpadding="0" cellspacing="1" bordercolor="#000000" bgcolor="#000000">
  <tr>
    <td height="227" valign="top" bgcolor="#DBDCD7"><p></p>
      <blockquote  style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; padding-left: 4px">
         <br />
         <p>Dear <xsl:value-of  select="tpe/forgot_passwords/firstname"/></p><br />
         <p>This email was sent automatically in response to your request to recover your password. Only you, the recipient of this email will receive the password. This is done for your protection.</p><br />
         <p>If you did not forget your password, please ignore this email.</p><br />
         <p>Email: <xsl:value-of  select="tpe/forgot_passwords/email"/></p>
         <p>Password: <xsl:value-of select="tpe/forgot_passwords/password"/></p><br />
         <p>This request made from IP address: <xsl:value-of  select="tpe/forgot_passwords/ip"/></p><br />
         <p>Thank you for using The Plastics Exchange</p>   
    </blockquote></td>
  </tr>
</table>

<xsl:call-template name="footer" />

</xsl:template>

</xsl:stylesheet>