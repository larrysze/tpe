<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="header">
<table width="779" border="0" align="center" cellpadding="0" cellspacing="1" bordercolor="#000000" bgcolor="#000000">
  <tr>
    <td width="779" bgcolor="#333333"><img src="http://{$domain}/images/email/EmailHeadImage.png" width="779" height="94" /></td>
  </tr>
  <tr>
  <td width="779" height="20" background="http://{$domain}/images/email/EmailOrangeBar.png" bgcolor="#FFD900">
    <table width="670" height="18" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="90" valign="bottom" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; font-weight: bold"><div align="center"><a href="http://{$domain}" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; padding-left: 4px">Home</a></div></td>
      <td width="103" valign="bottom" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; font-weight: bold"><div align="center"><a href="http://{$domain}/Spot/Spot_Floor.aspx" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; padding-left: 4px">Spot Market </a></div></td>
      <td width="125" valign="bottom" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; font-weight: bold"><div align="center"><a href="http://{$domain}/spot/Resin_Entry.aspx" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; padding-left: 4px">Request Resin </a></div></td>
      <td width="125" valign="bottom" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; font-weight: bold"><div align="center"><a href="http://{$domain}/Spot/Floor_Summary.aspx" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; padding-left: 4px">Spot Summary </a></div></td>
      <td width="100" valign="bottom" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; font-weight: bold"><div align="center"><a href="http://{$domain}/Public/Public_Research.aspx" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; padding-left: 4px">Research</a></div></td>
      <td width="120" valign="bottom" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; font-weight: bold"><div align="center"><a href="http://{$domain}/Public/Contact_Us.aspx" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; padding-left: 4px">Contact US </a></div></td>
    </tr>
  </table>
 </td>
  </tr>
</table>
</xsl:template>

<xsl:template name="footer">
<table width="779" border="0" align="center" cellpadding="0" cellspacing="1" bordercolor="#000000" bgcolor="#000000">
  <tr>
    <td height="18" bgcolor="#333333"><div align="center">
      <p align="center" style="font-size: 10px; color: #FFFFFF; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold">710 North Dearborn  Chicago, IL   60610  tel 312.202.0002  fax   312.202.0174</p>
    </div></td>
  </tr>
</table>
</xsl:template>

</xsl:stylesheet>