using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;

namespace localhost
{
	/// <summary>
	/// Summary description for Dup_Links.
	/// </summary>
	public class Dup_Links : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button Button1;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Button1.Click += new System.EventHandler(this.Button1_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Button1_Click(object sender, System.EventArgs e)
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			SqlConnection conndash;
			conndash = new SqlConnection(Application["DBconn"].ToString());
			conndash.Open();

			string strWWW;
			string strTemp;
    		StringBuilder sbDuplicates = new StringBuilder();
			SqlCommand cmdDuplicates;
			SqlDataReader dtrDuplicates;
			
			// first eliminate trailing /
			cmdDuplicates= new SqlCommand("select * from links", conn);
			dtrDuplicates= cmdDuplicates.ExecuteReader();
			while (dtrDuplicates.Read())
			{
				if (dtrDuplicates["LINKS_WEBSITE"].ToString().Length > 2)
				{
					if (dtrDuplicates["LINKS_WEBSITE"].ToString().Substring(dtrDuplicates["LINKS_WEBSITE"].ToString().Length-1,1) =="/")
					{
						//
						strTemp ="update links set LINKS_WEBSITE='"+dtrDuplicates["LINKS_WEBSITE"].ToString().Substring(0,dtrDuplicates["LINKS_WEBSITE"].ToString().Length-1)+"' WHERE LINKS_ID='"+dtrDuplicates["LINKS_ID"].ToString()+"'";
						cmdDuplicates= new SqlCommand(strTemp, conndash);
						cmdDuplicates.ExecuteNonQuery();
					}
				}
			}
			dtrDuplicates.Close();
			// end trailing fix


			sbDuplicates.Append("SELECT     y.LINKS_WEBSITE,LINKS.LINKS_ID,LINKS_GROUP,y.cpt-1 AS Count ");
			sbDuplicates.Append("			FROM         (SELECT     LINKS_WEBSITE, COUNT(*) AS cpt ");
			sbDuplicates.Append("								FROM          LINKS ");
			sbDuplicates.Append("								GROUP BY LINKS_WEBSITE) y INNER JOIN ");
			sbDuplicates.Append("								LINKS ON y.LINKS_WEBSITE = LINKS.LINKS_WEBSITE ");
			sbDuplicates.Append("			WHERE     (y.cpt > 0) AND y.LINKS_WEBSITE<>'' ORDER BY y.LINKS_WEBSITE,LINKS.LINKS_ID ");
			


			//cmdDuplicates= new SqlCommand("(SELECT Count(*),LINKS_WEBSITE FROM LINKS GROUP BY LINKS_WEBSITE) ", conn);
			cmdDuplicates= new SqlCommand(sbDuplicates.ToString(), conn);
			
			
			dtrDuplicates= cmdDuplicates.ExecuteReader();
			// read the first one 
			int x;
			string tempID;
			while (dtrDuplicates.Read())
			{
				//strWWW = dtrDuplicates["LINKS_WEBSITE"].ToString();
				AddMember(dtrDuplicates["LINKS_WEBSITE"].ToString(),dtrDuplicates["LINKS_ID"].ToString(),dtrDuplicates["LINKS_GROUP"].ToString());
				tempID = dtrDuplicates["LINKS_ID"].ToString();
				for (x=0;x<Convert.ToInt16(dtrDuplicates["COUNT"]);x++)
				{
					dtrDuplicates.Read();
					AddMember(dtrDuplicates["LINKS_WEBSITE"].ToString(),tempID,dtrDuplicates["LINKS_GROUP"].ToString());
					Delete(dtrDuplicates["LINKS_ID"].ToString());
					
				}
			}
			
			conn.Close();
			

		}
		void AddMember(string strWebsite, string strID, string strGroup)
		{
			SqlConnection conn;
			SqlConnection conn2;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			conn2 = new SqlConnection(Application["DBconn"].ToString());
			conn2.Open();
			SqlCommand cmd;
			SqlCommand cmd2;
			SqlDataReader dtr;
			// check to see if link has already be added to group
			cmd = new SqlCommand("SELECT *,(SELECT LINKS_WEBSITE FROM LINKS WHERE LINKS_ID=MEMBER_LINK_ID)  FROM LINK_CATEGORY_MEMBER WHERE MEMBER_CATEGORY = '"+strGroup+"' AND MEMBER_LINK_ID = '"+strID+"' AND (SELECT LINKS_WEBSITE FROM LINKS WHERE LINKS_ID=MEMBER_LINK_ID)<>'"+strWebsite+"'",conn);
			dtr = cmd.ExecuteReader();
			if (!dtr.Read())
			{
				cmd2 = new SqlCommand("INSERT INTO LINK_CATEGORY_MEMBER ( MEMBER_CATEGORY,MEMBER_LINK_ID) VALUES ('"+strGroup+"','"+strID+"')",conn2);
				cmd2.ExecuteNonQuery();
			}
			dtr.Close();
			conn.Close();
			conn2.Close();


		}
		void Delete(string strID)
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			SqlCommand cmd2;
			cmd2 = new SqlCommand("DELETE FROM LINKS WHERE LINKS_ID='"+strID+"'",conn);
			cmd2.ExecuteNonQuery();
			conn.Close();
		}
	}
}
