using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using MetaBuilders.WebControls;

namespace localhost.Creditor
{
	/// <summary>
	/// Summary description for Inventory.
	/// </summary>
	public partial class Inventory : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "T") && ((string)Session["Typ"] != "L"))
			{
				Response.Redirect("../default.aspx");
			}
            Master.Width = "1250px";
			if (!IsPostBack)
			{
				Bind();
				
				btnSendSpotFloor.Visible = ((string)Session["Typ"] != "B");
				dg.Columns[0].Visible = ((string)Session["Typ"] != "B");
				dg.Columns[1].Visible = ((string)Session["Typ"] != "B");
			}
		}
		// <summary>
		// Binds the content data.  This is always called after strOrderBy has been determined
		// </summary>
		private void Bind()
		{
            TPE.Utility.DBLibrary.BindDataGridWithStoredProcedure(Application["dbConn"].ToString(), this.dg, "spInventory");
        }
		
		double dbTotalWeight = 0;
		double dbTotalBuyPrice = 0.0;
		double dbTotalTargetPrice = 0.0;


		protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
			//Response.Write("1:<BR>");
			if (  (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
			{
				//Response.Write("1:<BR>");
				dbTotalWeight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARWEIGHT"));
				dbTotalBuyPrice += Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARSELLPRICE"))*Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARWEIGHT"));
				dbTotalTargetPrice += Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARBUYPRICE"))*Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARWEIGHT"));
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[1].Text = "<b>Totals:</b> " ;
				e.Item.Cells[10].Text = "<b>"+String.Format("{0:#,###}", dbTotalWeight)+"</b>";
				e.Item.Cells[11].Text = "<b>"+String.Format("{0:c}", dbTotalBuyPrice)+"</b>";
				e.Item.Cells[12].Text = "<b>"+String.Format("{0:c}", dbTotalTargetPrice)+"</b>";
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

        protected void btnExportToExcel_Click(object sender, System.EventArgs e)
        {
            //export to excel

            //using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            //{
              //  DataSet Ds = TPE.Utility.DBLibrary.GetDataSetFromStoredProcedure(conn, "spInventory");

            Bind();

            dg.Columns[0].Visible = false;
            dg.Columns[1].Visible = false;
            
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition",
                "attachment; filename=Inventory.xls");

            Response.BufferOutput = true;
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Charset = "UTF-8";
            EnableViewState = false;

            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw =
                new System.Web.UI.HtmlTextWriter(tw);

            dg.RenderControl(hw);

            Response.Write(tw.ToString());
            Response.End();

              //  ConvertToExcel(Ds, Response);
//            }



            //TPE.Utility.DBLibrary.BindDataGridWithStoredProcedure(Application["dbConn"].ToString(), this.dg, "spInventory");

            //Response.Clear();
            //Response.Buffer = true;
            //Response.ContentType = "application/vnd.ms-excel";
            //Response.Charset = "";
            //this.EnableViewState = false;
            //System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
            //System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
            //this.ClearControls(dg);
            ////dg.RenderControl(oHtmlTextWriter);
            //Response.Write(oStringWriter.ToString());
            //Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */

        }

        private void ClearControls(Control control)
        {
            for (int i = control.Controls.Count - 1; i >= 0; i--)
            {
                ClearControls(control.Controls[i]);
            }
            if (!(control is TableCell))
            {
                if (control.GetType().GetProperty("SelectedItem") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    try
                    {
                        literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
                    }
                    catch
                    {
                    }
                    control.Parent.Controls.Remove(control);
                }
                else
                    if (control.GetType().GetProperty("Text") != null)
                    {
                        LiteralControl literal = new LiteralControl();
                        control.Parent.Controls.Add(literal);
                        literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);
                        control.Parent.Controls.Remove(control);
                    }
            }
            return;
        }


        public static void ConvertToExcel(DataSet ds, HttpResponse response)
        {
            //response.Clear();
            //response.Charset = "";
            //response.ContentType = "application/vnd.ms-excel";
            //System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            //System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite);
            //DataGrid dg = new DataGrid();
            //dg.DataSource = ds.Tables(0);
            //dg.DataBind();
            //dg.RenderControl(htmlWrite);
            //stringWrite.ToString;
            //response.Write(s);
            //response.End();

        }



		protected void btnSendSpotFloor_Click(object sender, System.EventArgs e)
		{
			string strID;
			string strOrdrID;
			string strShipmentSku;
			
			// delete all the offers from TPE.  This cleans up offers that have been pushed in the past 
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			SqlCommand cmd;
			cmd = new SqlCommand("Delete from BBOFFER WHERE OFFR_COMP_ID='148'",conn);
			cmd.ExecuteNonQuery();
			conn.Close();
			
			
			for(int i=0;i<dg.Items.Count;i++)
			{
				// if the control box is checked
				if (((System.Web.UI.HtmlControls.HtmlInputCheckBox)dg.Items[i].Cells[0].Controls[0]).Checked==true)
				{
					strID = dg.Items[i].Cells[2].Text;
					strOrdrID = "";
					strShipmentSku = "";
					// if there is a chacacter "-" inside the code
					int indexChar = strID.IndexOf("-",0);
					if (indexChar!=-1)
					{
						strOrdrID = strID.Substring(0,strID.IndexOf("-",0));
						strShipmentSku = strID.Substring(indexChar+1,strID.Length-(indexChar+1));
					}
					else
					{
						strOrdrID = strID.ToString();
					}
					
					// Call a Stored Procedure which will make all the job
					// Parameters: strShipmentOrdrID, strShipmentSku
					SendOrderToOffer(strOrdrID, strShipmentSku);

					//set the checkbox to false
					((System.Web.UI.HtmlControls.HtmlInputCheckBox)dg.Items[i].Cells[0].Controls[0]).Checked=false;
				}
			}
			//Go to Spot Floor
			Response.Redirect("../spot/spot_floor.aspx");
		}

		private void SendOrderToOffer(string OrderID, string ShipmentSku)
		{
			String strSql = "";
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			
			strSql = "Exec spSendOrderToOffer @OrderID=" + OrderID.ToString();
			strSql = strSql+ ", @ShipmentSku='" + ShipmentSku.ToString() + "'";

			SqlCommand cmd = new SqlCommand(strSql, conn);
			cmd.ExecuteNonQuery();
			conn.Close();
		}
	}
}