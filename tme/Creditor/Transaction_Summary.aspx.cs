using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using TPE.Utility;

namespace localhost.Creditor
{
	/// <summary>
	/// Summary description for Transaction_Summary.
	/// </summary>
	public partial class Transaction_Summary : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button Button3;
		protected System.Web.UI.WebControls.Button Button4;
		protected System.Web.UI.WebControls.PlaceHolder phActionButton;
	
		//string for "ODDER BY"
		string strOrderBy;
		//string for search function
		
		string bymonth;
		double TotalSum = 0.0;
		double interest = 0.0;
		double limit = 0.0;

		protected void Page_Load(object sender, EventArgs e)
		{
            
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C"))
			{
				Response.Redirect("../default.aspx");
			}

           Master.Width = "1350px"; 
			
			if (!IsPostBack)
			{
				// db connection for a list of companies
				SqlConnection conn2;
				conn2 = new SqlConnection(Application["DBConn"].ToString());
				try
				{
					conn2.Open();				
					SqlCommand cmdCompanies;
					SqlDataReader dtrCompanies;
					cmdCompanies= new SqlCommand("SELECT * FROM (SELECT COMP_ID,COMP_NAME,COMP_TYPE FROM COMPANY WHERE COMP_REG=1 AND COMP_TYPE IN ('P','S','D'))Q ORDER BY COMP_NAME", conn2);
					dtrCompanies= cmdCompanies.ExecuteReader();

					// add the default value(display all companies)
					ddlCompanies.Items.Add(new ListItem ("Select a Company","*"));
					try
					{
						while (dtrCompanies.Read())
						{
							ddlCompanies.Items.Add(new ListItem (dtrCompanies["COMP_NAME"].ToString(),dtrCompanies["COMP_ID"].ToString()));
						}
					}
					finally
					{
						dtrCompanies.Close();
					}
				}
				finally
				{
					conn2.Close();
				}
					
				// setting up date ddl
				CreateDateList();

				//setting up broker list
				CreateBrokerList();

				lblSort.Text = "ORDR_ID DESC";
				
				Bind(lblSort.Text);
			}
		}

		
		
	/*	protected void ShowAll(object sender, EventArgs e)
		{
			search="";
			bymonth="";
			ddlMonth.SelectedItem.Value="*";
			
			// changing the db page back to 0
			Bind(lblSort.Text);
		}
		
*/
		// <summary>
		//  searches past months to see which months have transactions pending
		// </summary>
		protected void CreateDateList()
		{
			ddlMonth.Items.Clear(); // starts the list out fresh
			DateTime moment = DateTime.Now;
			//start out with today's dates
			int iYY = moment.Year;
			int iMM = moment.Month;
			string strCompSQL =""; // holds the syntax of the company should it be needed

			if (!ddlCompanies.SelectedItem.Value.Equals("*") || (ddlBroker.Items.Count>0 && !ddlBroker.SelectedItem.Value.Equals("*")))
			{
				if (!ddlCompanies.SelectedItem.Value.Equals("*"))
				{
					strCompSQL +=" AND ((SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)="+ddlCompanies.SelectedItem.Value.ToString()+" OR (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)="+ddlCompanies.SelectedItem.Value.ToString()+")";
				}
				if ( !ddlBroker.SelectedItem.Value.Equals("*"))
				{
			
					strCompSQL +=" AND SHIPMENT_BROKER_ID='"+ddlBroker.SelectedItem.Value.ToString()+"' ";
				}
			}
		
			SqlConnection conn2;
			conn2 = new SqlConnection(Application["DBConn"].ToString());
			try
			{
				conn2.Open();
				SqlCommand cmdMonth;
				SqlDataReader dtrMonth;

				while (iYY >= 2000)
				{

					cmdMonth= new SqlCommand("SELECT TOP 1 LEFT(CONVERT(varchar,ORDR_DATE,107),3) + ' ' + Right(CONVERT(varchar,ORDR_DATE,107),4) AS Date FROM ORDERS,SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID AND Month(ORDR_DATE) =@Month AND Year(ORDR_DATE) = @Year AND SHIPMENT_BUYR IS NOT NULL" + strCompSQL, conn2);
					cmdMonth.Parameters.Add("@Month",iMM);
					cmdMonth.Parameters.Add("@Year",iYY);
					dtrMonth= cmdMonth.ExecuteReader();
					try
					{
						if (dtrMonth.HasRows)
						{
							dtrMonth.Read();
							ListItem LIdate = new ListItem (dtrMonth["Date"].ToString(),iMM.ToString() +"-"+iYY.ToString());
							if ((string)Request.QueryString["ShowDate"] == iMM.ToString() +"-"+iYY.ToString() )
							{
								LIdate.Selected = true;
							}
							ddlMonth.Items.Add(LIdate);
						}
					}
					finally
					{
						dtrMonth.Close();
					}

					iMM--;
					if (iMM == 0)
					{
						iMM = 12;
						iYY--;
					}


				}
				ListItem lstItem = new ListItem ("Show All Dates","*");
				//lstItem.Selected = true;
				ddlMonth.Items.Add(lstItem);
			}
			finally
			{
				conn2.Close();
			}

		}

		// <summar
		// Updated the dbase page of the grid
		// </summary>
		protected void dg_PageIndexChanged(object sender,DataGridPageChangedEventArgs e)
		{
			dg.CurrentPageIndex = e.NewPageIndex;
			// rebind grid
			Bind(lblSort.Text);
		}
    

		// <summary>
		// Binds the content data.  This is always called after strOrderBy has been determined
		// </summary>
		protected void Bind(string strSort)
		{    
			interest = HelperFunction.getInterestBankParameter(this.Context);
			limit = HelperFunction.getInterestLimitParameter(this.Context);			

			StringBuilder sbSql = new StringBuilder();
			sbSql.Append(" SELECT *,");
			sbSql.Append("CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR) AS ID, ");
            sbSql.Append("VARWEIGHT = ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY),");
            sbSql.Append("VARRCNUMBER = ISNULL(SHIPMENT_COMMENT, '-'),");
            sbSql.Append("ORDR_DATE AS ORDR_DTE FROM (SELECT *,");
			sbSql.Append("VARTPE=VARFEE-VARCREDIT FROM (SELECT *,");
			sbSql.Append("DATEADD(d,CAST(VARTERM AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)) As VARNETDUEBUYR,");
			sbSql.Append("VARSPAID=ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_SELR),0),");
			sbSql.Append("VARPPAID=ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),0),");
			sbSql.Append("VARPPAIDDATE=ISNULL((SELECT CONVERT(VARCHAR,MAX(PAY_DATE),101) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),'-'),");
			sbSql.Append("DATEADD(d,CAST(SHIPMENT_SELLER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)) AS VARNETDUESELR,");
			sbSql.Append("VAROWESBUYR=VARTVBUYR,");
			sbSql.Append("VAROWESSELR=VARTVSELR,");
			sbSql.Append("VARGROSS=(CASE WHEN VARTVBUYR-VARTVSELR<0 THEN 0 ELSE VARTVBUYR-VARTVSELR END),");
			sbSql.Append("VARCOMMISION=SHIPMENT_COMM * ((( SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) * ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-SHIPMENT_SHIP_PRCE) / 100,");
			sbSql.Append("VARBROKER=(SELECT PERS_FRST_NAME FROM PERSON WHERE PERS_ID=SHIPMENT_BROKER_ID ),");
			sbSql.Append("VARFEE=((SHIPMENT_BUYR_PRCE-SHIPMENT_PRCE)*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-SHIPMENT_SHIP_PRCE - (SHIPMENT_COMM * ((( SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) * ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-SHIPMENT_SHIP_PRCE)/100),");
			sbSql.Append("VARFREIGHT = SHIPMENT_SHIP_PRCE,");
			sbSql.Append("VARCREDIT=VARTVBUYR*0.001,");
			sbSql.Append("VARHANOVER=VARTVBUYR*0.005 FROM (SELECT *,");
			sbSql.Append("VARBUYPRICE = LEFT(SHIPMENT_BUYR_PRCE,6),");
			sbSql.Append("VARSELLPRICE = LEFT(SHIPMENT_PRCE,6),");
			sbSql.Append("VARTVBUYR=(SHIPMENT_BUYR_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (100 + ISNULL(SHIPMENT_TAX, 0)) / 100),");
			sbSql.Append("VARTVSELR=SHIPMENT_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (100 + ISNULL(SHIPMENT_TAX, 0)) / 100,");
			sbSql.Append("VARBUYR=(SELECT LEFT(COMP_NAME,10)+'...' FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)),");
			sbSql.Append("VARSELR=(SELECT LEFT(COMP_NAME,10)+'...' FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)),");
			sbSql.Append("VARBUYR_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR),");
			sbSql.Append("VARSELR_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR),");
			sbSql.Append("BPAYMENTS=(SELECT TOP 1 PAY_ORDR FROM PAYMENT WHERE PAY_ORDR=SHIPMENT_ORDR_ID AND PAY_PERS=SHIPMENT_BUYR),");
			sbSql.Append("SPAYMENTS=(SELECT TOP 1 PAY_ORDR FROM PAYMENT WHERE PAY_ORDR=SHIPMENT_ORDR_ID AND PAY_PERS=SHIPMENT_SELR) FROM (SELECT *,");
			sbSql.Append("VARTERM=ISNULL(SHIPMENT_BUYER_TERMS,30),");
			sbSql.Append("VARDATE=CONVERT(VARCHAR,ORDR_DATE,101),");
			sbSql.Append("VARCONTRACT=LEFT(ISNULL(ORDR_PROD_SPOT,(SELECT CONT_LABL FROM CONTRACT WHERE CONT_ID=ORDR_CONT)),10)+'...' ");	
			sbSql.Append("FROM ORDERS, SHIPMENT WHERE SHIPMENT_BUYR is not null and SHIPMENT_ORDR_ID=ORDR_ID  )Q0)Q1)Q2 WHERE VARBUYR<>'The Plasti...' OR (VARBUYR='The Plasti...' AND SHIPMENT_SKU = 'RC1' ) )Q3   ");
			if (!ddlMonth.SelectedItem.Value.Equals("null")&& (txtSearch.Text!="") || !ddlBroker.SelectedItem.Value.Equals("null") || !ddlCompanies.SelectedItem.Value.Equals("*") )
			{
				sbSql.Append(" WHERE ORDR_DATE > ='12/1/97' ");  // random date prior to the company founding.  this will always be true
			}
			if (!ddlMonth.SelectedItem.Value.Equals("*"))
			{
				// value passed is in MM-YYYY format. needs to broken up into separate parameters
				// In case of Today only value passed in MM-YYYY-DD format and have new element in strDate array.
				string[] strDate;
				Regex r = new Regex("-"); // Split on spaces.
				strDate = r.Split(ddlMonth.SelectedItem.Value.ToString()) ;
				bymonth +=" AND Month(ORDR_DATE)='"+strDate[0]+"'";
				bymonth +=" AND Year(ORDR_DATE)='"+strDate[1]+"'";
					   
				//If Today only selected
				if(strDate.GetUpperBound(0)>1)
				{
					bymonth +=" AND Day(ORDR_DATE)='"+strDate[2]+"'";	
				}
					
				sbSql.Append(bymonth);
			}

			if ( !ddlBroker.SelectedItem.Value.Equals("*"))
			{
				sbSql.Append("AND SHIPMENT_BROKER_ID='"+ddlBroker.SelectedItem.Value.ToString()+"' ");

			}
			if ( !ddlCompanies.SelectedItem.Value.Equals("*"))
			{
				sbSql.Append("AND (VARBUYR_ID='"+ddlCompanies.SelectedValue.ToString()+"' OR  VARSELR_ID='"+ddlCompanies.SelectedValue.ToString()+"') ");

			}
			
			if(txtSearch.Text!="")
			{
			
				string searchString = DBLibrary.ScrubSQLStringInput(txtSearch.Text);
				string search="";
				char [] sep=new Char[1];
				sep[0]='-';
				String [] aux=searchString.Split(sep);
				search=search+"  SHIPMENT_ORDR_ID LIKE '%"+aux[0]+"%'";
				search=search+" OR VARNETDUEBUYR LIKE '%"+searchString+"%'";
				search=search+" OR VARNETDUESELR LIKE '%"+searchString+"%'";
				search=search+" OR VAROWESBUYR LIKE '%"+searchString+"%'";
				search=search+" OR VAROWESSELR LIKE '%"+searchString+"%'";
				search=search+" OR VARTVBUYR LIKE '%"+searchString+"%'";
				search=search+" OR VARTVSELR LIKE '%"+searchString+"%'";
				search=search+" OR VARGROSS LIKE '%"+searchString+"%'";
				search=search+" OR VARSPAID LIKE '%"+searchString+"%'";
				search=search+" OR VARPPAID LIKE '%"+searchString+"%'";
				search=search+" OR VARBUYR LIKE '%"+searchString+"%'";
				search=search+" OR VARSELR LIKE '%"+searchString+"%'";
				search=search+" OR SHIPMENT_FOB LIKE '%" + searchString + "%'";
				
				//
				sbSql.Append("AND " +  search);
			}
			
			
			sbSql.Append(" ORDER BY " +strSort);
			SqlDataAdapter dadContent;
			DataSet dstContent;
			
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				dadContent = new SqlDataAdapter(sbSql.ToString() ,conn);
				dstContent = new DataSet();
				dadContent.Fill(dstContent);
    
				dg.DataSource = dstContent;
				dg.DataBind();

				if ((dg.Items.Count==0) )
				{
					dg.Visible=false;	
					lblMessage.Visible = true;
				}
				else
				{
					dg.Visible=true;
					lblMessage.Visible = false;
				}
			}
		}
    
		// <summary>
		// Sorting function
		// </summary>
		protected void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{
			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;
    
			//  Parse the sort expression - delimiter space
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
			ColumnToSort = SortExprs[0];
			// If a sort order is specified get it, else default is descending
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DeSC";
			}
    
			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;
    
			// Figure out the column index
			int iIndex;
			iIndex = 0;
			switch(ColumnToSort.ToUpper())
			{
				case "ID":
					iIndex = 1;
					break;
				case "ORDR_DTE":
					iIndex = 2;
					break;
				case "VARCONTRACT":
					iIndex = 3;
					break;
				case "VARWEIGHT":
					iIndex = 4;
					break;
                case "VARRCNUMBER":
                    iIndex = 5;
                    break;
                case "VARTVBUYR":
                    iIndex = 6;
                    break;
                case "VARPPAID":
					iIndex = 7;
					break;
				case "VARBUYPRICE":
					iIndex = 8;
					break;
				case "VARSELLPRICE":
					iIndex = 9;
					break;
				case "VARTVSELR":
					iIndex = 10;
					break;
				case "VARSPAID":
					iIndex = 11;
					break;
				case "VARFREIGHT":
					iIndex = 12;
					break;
				case "VARCOMMISION":
					iIndex = 13;
					break;
				case "VARBROKER":
					iIndex = 14;
					break;
				case "SHIPMENT_BUYER_TERMS":
					iIndex = 15;
					break;
				case "VARFEE":
					iIndex = 16;
					break;
				case "VARBUYR":
					iIndex = 17;
					break;
				case "VARSELR":
					iIndex = 18;
					break;
			}
			// alter the column's sort expression
			dg.Columns[iIndex].SortExpression = NewSortExpr;
    
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
    
			// store the new SortExpr in a labl for use by page function
			lblSort.Text =NewSortExpr;
			// Sort the data in new order

			Bind(NewSortExpr);
		}
    
		double dbMargin = 0.0;
		double dbTotalInterest = 0.0;
		double dbTotalCommision = 0.0;
		double dbTotalTVBuyr = 0.0;
		double dbTotalBuyrPaid = 0.0;
		double dbTotalTVSelr = 0.0;
		double dbTotalSelrPaid = 0.0;
		double dbTotalFreight = 0.0;
		double iTotalWeight = 0;
		int iLinkSetCount = 1;
		protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
			// determines if the order is a railcar end of railcar to bulk truck
			int iSKU = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ID")).LastIndexOf("RC1");
			if (  (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
			{
				double dbInterest = 0;
				int DaysPastDue = 0;
				double percentPaid = 0.0;
				DateTime dueDate;
				TimeSpan ts;

				if (e.Item.Cells[21].Text != "&nbsp;")
				{
					dueDate = Convert.ToDateTime(e.Item.Cells[21].Text).AddDays(Convert.ToInt32(e.Item.Cells[15].Text));
					
					percentPaid = Math.Round((Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))/Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR")))*100,2);
					if (percentPaid >= limit) //WY
						ts = Convert.ToDateTime(e.Item.Cells[22].Text) - dueDate;
					else
						ts = DateTime.Today - dueDate;
					
					DaysPastDue = ts.Days <= 0 ? 0: ts.Days;

					if (DaysPastDue > 0)
						dbInterest = Math.Round((Convert.ToDouble(DaysPastDue)/ 360.00) * (interest/100) * Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR")),2);//WY
					else
						dbInterest = 0;
				}
				else
				{
					DaysPastDue = 0;
					dbInterest = 0;
				}

				e.Item.Cells[16].Text = DaysPastDue.ToString()=="0" ? "-" : DaysPastDue.ToString();
				e.Item.Cells[17].Text = string.Format("{0:c}",0);

				if (iSKU ==-1) // railcar in railcar to bulktruck 
				{
					e.Item.Cells[17].Text = string.Format("{0:c}",dbInterest);
					e.Item.Cells[18].Text = string.Format("{0:c}",Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARFEE")) - dbInterest);
					dbTotalInterest += dbInterest;
					dbMargin += Convert.ToSingle(Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARFEE")) - dbInterest);
					dbTotalCommision += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARCOMMISION"));
					dbTotalFreight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARFREIGHT"));
					iTotalWeight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARWEIGHT"));
					dbTotalTVBuyr += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR"));
					dbTotalTVSelr += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVSELR"));
				
					dbTotalBuyrPaid += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"));
					dbTotalSelrPaid += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID"));
				}
				else
				{
					e.Item.Cells[16].Text = "-";
				}
			
				
				//e.Item.ForeColor = Color.Firebrick;
				if (Math.Abs(Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID"))-Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVSELR"))) > 0.05 && Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID")) >0 )
				{
					e.Item.Cells[11].Text    = "<a href=\"Edit_Payments.aspx?ID="+DataBinder.Eval(e.Item.DataItem, "ID").ToString()+"\"><font color=red>" + String.Format("{0:c}",Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID"))) + "</a></font>";
				}
				if (Math.Abs(Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))-Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR"))) > 0.05 && Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))>0 )
				{
					e.Item.Cells[7].Text    = " <a href=\"Edit_Payments.aspx?Receivable=true&ID="+DataBinder.Eval(e.Item.DataItem, "ID").ToString()+"\"><font color=red>" + String.Format("{0:c}",Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))) + "</a></font>";
				}
				// Any time the Plastics Exchange is involved in a transaction, blank out the buyer/seller paid columns 
				if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VARBUYR"))=="The Plastics...")
				{
					e.Item.Cells[8].Text =" &nbsp;&nbsp;N/A";
					e.Item.Cells[6].Text =" &nbsp;&nbsp;N/A";
					e.Item.Cells[7].Text =" &nbsp;&nbsp;N/A";
					// This displays the amount in the 
					if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID")) !=0)
					{
						e.Item.Cells[7].Text +="<a href=\"Edit_Payments.aspx?ID="+DataBinder.Eval(e.Item.DataItem, "ID").ToString()+"\"> <font color=red> ($"+Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))+")</font></a> ";
					}
				}
				if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VARSELR"))=="The Plastics...")
				{
					e.Item.Cells[9].Text =" &nbsp;&nbsp;N/A";
					e.Item.Cells[10].Text =" &nbsp;&nbsp;N/A";
					e.Item.Cells[11].Text =" &nbsp;&nbsp;N/A";
					if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID")) !=0)
					{
						e.Item.Cells[11].Text +="<a href=\"Edit_Payments.aspx?ID="+DataBinder.Eval(e.Item.DataItem, "ID").ToString()+"\"> <font color=red> ($"+Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VARSPAID"))+")</font></a> ";
					}
				}

				string strActionBoxHTML = "";
				// get Order id
				//Response.Write(e.Item.DataItem.);
				string strID = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ID"));
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]='<div class=\"menuitems\"><a href=\"/administrator/Transaction_Details.aspx?Market=Spot&Referer=/Common/Filled_Orders.aspx&ID="+strID+"\">Edit # "+strID+"</a></div>'" + ((char)13).ToString();

				// remove extra info
				//strID = strID.Substring(0,strID.LastIndexOf("-"));
				
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<hr>' //Optional Separator" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=1\">Invoice</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=2\">Purchase Order</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<hr>' //Optional Separator" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=3\">Certificate</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=4\">Sale Confirmation</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/hauler/Freight_Company.aspx?ID="+strID+"\">Freight R.F.Q</a></div>'" + ((char)13).ToString();
				//strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=6\">Title Transfer</a></div>'" + ((char)13).ToString();
				
				//if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARWGHT")).ToString() == "190000")
				//{
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/common/Convert_Order.aspx?ID="+strID+"\">Convert to 4 BT</a></div>'" + ((char)13).ToString();
				//}
				phActionButton.Controls.Add (new LiteralControl(strActionBoxHTML));


				// setting the mouseover color
				e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");
				if (e.Item.ItemType == ListItemType.Item)
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee'");
				}
				else
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3'");
				}
				// add scripts for 'action boxes'
				e.Item.Cells[0].Attributes.Add("onmouseover","showmenu(event,linkset["+iLinkSetCount.ToString()+"])");
				e.Item.Cells[0].Attributes.Add("onMouseout", "delayhidemenu()");
				//e.Item.Cells[7].Attributes.Add("onmouseover","showmenu(event,linkset["+iLinkSetCount.ToString()+"])");
				//e.Item.Cells[7].Attributes.Add("onMouseout", "delayhidemenu()");

				iLinkSetCount ++;

				
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[1].Text = "<b>Totals:</b> " ;
				e.Item.Cells[4].Text = "<b>"+String.Format("{0:#,###}", iTotalWeight)+"</b>";
				e.Item.Cells[6].Text = "<b>"+String.Format("{0:c}", dbTotalTVBuyr)+"</b>";
				
				e.Item.Cells[7].Text = "<b>"+String.Format("{0:c}", dbTotalBuyrPaid)+"</b>";

				e.Item.Cells[10].Text = "<b>"+String.Format("{0:c}", dbTotalTVSelr)+"</b>";
				
				e.Item.Cells[11].Text = "<b>"+String.Format("{0:c}", dbTotalSelrPaid)+"</b>";

				e.Item.Cells[12].Text = "<b>"+String.Format("{0:c}", dbTotalFreight)+"</b>";
				e.Item.Cells[13].Text = "<b>"+String.Format("{0:c}", dbTotalCommision)+"</b>";
				e.Item.Cells[17].Text = "<b>"+String.Format("{0:c}", dbTotalInterest)+"</b>";
				e.Item.Cells[18].Text = "<b>"+String.Format("{0:c}", dbMargin)+"</b>";
			}
		}
		
		/*
		protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
			// determines if the order is a railcar end of railcar to bulk truck
			int iSKU = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ID")).LastIndexOf("RC1");
			if (  (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
			{
				double dbInterest = 0;
				int DaysPastDue = 0;
				double percentPaid = 0.0;
				DateTime dueDate;
				TimeSpan ts;

				if (e.Item.Cells[19].Text != "&nbsp;")
				{
					dueDate = Convert.ToDateTime(e.Item.Cells[19].Text).AddDays(Convert.ToInt32(e.Item.Cells[13].Text));
					
					percentPaid = Math.Round((Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))/Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR")))*100,2);
					if (percentPaid >= limit) //WY
						ts = Convert.ToDateTime(e.Item.Cells[20].Text) - dueDate;
					else
						ts = DateTime.Today - dueDate;
					
					DaysPastDue = ts.Days <= 0 ? 0: ts.Days;

					if (DaysPastDue > 0)
						dbInterest = Math.Round((Convert.ToDouble(DaysPastDue)/ 360.00) * (interest/100) * Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR")),2);//WY
					else
						dbInterest = 0;
				}
				else
				{
					DaysPastDue = 0;
					dbInterest = 0;
				}

				e.Item.Cells[14].Text = DaysPastDue.ToString()=="0" ? "-" : DaysPastDue.ToString();
				e.Item.Cells[15].Text = string.Format("{0:c}",0);

				if (iSKU ==-1) // railcar in railcar to bulktruck 
				{
					e.Item.Cells[15].Text = string.Format("{0:c}",dbInterest);
					e.Item.Cells[16].Text = string.Format("{0:c}",Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARFEE")) - dbInterest);
					dbTotalInterest += dbInterest;
					dbMargin += Convert.ToSingle(Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARFEE")) - dbInterest);
					dbTotalCommision += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARCOMMISION"));
					dbTotalFreight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARFREIGHT"));
					iTotalWeight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARWEIGHT"));
					dbTotalTVBuyr += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR"));
					dbTotalTVSelr += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVSELR"));
				
					dbTotalBuyrPaid += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"));
					dbTotalSelrPaid += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID"));
				}
				else
				{
					e.Item.Cells[14].Text = "-";
				}
			
				
				//e.Item.ForeColor = Color.Firebrick;
				if (Math.Abs(Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID"))-Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVSELR"))) > 0.05 && Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID")) >0 )
				{
					e.Item.Cells[10].Text    = "<a href=\"Edit_Payments.aspx?ID="+DataBinder.Eval(e.Item.DataItem, "ID").ToString()+"\"><font color=red>" + String.Format("{0:c}",Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID"))) + "</a></font>";
				}
				if (Math.Abs(Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))-Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR"))) > 0.05 && Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))>0 )
				{
					e.Item.Cells[6].Text    = " <a href=\"Edit_Payments.aspx?Receivable=true&ID="+DataBinder.Eval(e.Item.DataItem, "ID").ToString()+"\"><font color=red>" + String.Format("{0:c}",Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))) + "</a></font>";
				}
				// Any time the Plastics Exchange is involved in a transaction, blank out the buyer/seller paid columns 
				if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VARBUYR"))=="The Plastics...")
				{
					e.Item.Cells[7].Text =" &nbsp;&nbsp;N/A";
					e.Item.Cells[5].Text =" &nbsp;&nbsp;N/A";
					e.Item.Cells[6].Text =" &nbsp;&nbsp;N/A";
					// This displays the amount in the 
					if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID")) !=0)
					{
						e.Item.Cells[6].Text +="<a href=\"Edit_Payments.aspx?ID="+DataBinder.Eval(e.Item.DataItem, "ID").ToString()+"\"> <font color=red> ($"+Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))+")</font></a> ";
					}
				}
				if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VARSELR"))=="The Plastics...")
				{
					e.Item.Cells[8].Text =" &nbsp;&nbsp;N/A";
					e.Item.Cells[9].Text =" &nbsp;&nbsp;N/A";
					e.Item.Cells[10].Text =" &nbsp;&nbsp;N/A";
					if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID")) !=0)
					{
						e.Item.Cells[10].Text +="<a href=\"Edit_Payments.aspx?ID="+DataBinder.Eval(e.Item.DataItem, "ID").ToString()+"\"> <font color=red> ($"+Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VARSPAID"))+")</font></a> ";
					}
				}

				string strActionBoxHTML = "";
				// get Order id
				//Response.Write(e.Item.DataItem.);
				string strID = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ID"));
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]='<div class=\"menuitems\"><a href=\"/administrator/Transaction_Details.aspx?Market=Spot&Referer=/Common/Filled_Orders.aspx&ID="+strID+"\">Edit # "+strID+"</a></div>'" + ((char)13).ToString();

				// remove extra info
				//strID = strID.Substring(0,strID.LastIndexOf("-"));
				
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<hr>' //Optional Separator" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=1\">Invoice</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=2\">Purchase Order</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<hr>' //Optional Separator" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=3\">Certificate</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=4\">Sale Confirmation</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/hauler/Freight_Company.aspx?ID="+strID+"\">Freight R.F.Q</a></div>'" + ((char)13).ToString();
				//strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=6\">Title Transfer</a></div>'" + ((char)13).ToString();
				
				//if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARWGHT")).ToString() == "190000")
				//{
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/common/Convert_Order.aspx?ID="+strID+"\">Convert to 4 BT</a></div>'" + ((char)13).ToString();
				//}
				phActionButton.Controls.Add (new LiteralControl(strActionBoxHTML));


				// setting the mouseover color
				e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");
				if (e.Item.ItemType == ListItemType.Item)
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee'");
				}
				else
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3'");
				}
				// add scripts for 'action boxes'
				e.Item.Cells[0].Attributes.Add("onmouseover","showmenu(event,linkset["+iLinkSetCount.ToString()+"])");
				e.Item.Cells[0].Attributes.Add("onMouseout", "delayhidemenu()");
				//e.Item.Cells[7].Attributes.Add("onmouseover","showmenu(event,linkset["+iLinkSetCount.ToString()+"])");
				//e.Item.Cells[7].Attributes.Add("onMouseout", "delayhidemenu()");

				iLinkSetCount ++;

				
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[1].Text = "<b>Totals:</b> " ;
				e.Item.Cells[4].Text = "<b>"+String.Format("{0:#,###}", iTotalWeight)+"</b>";
				e.Item.Cells[5].Text = "<b>"+String.Format("{0:c}", dbTotalTVBuyr)+"</b>";
				
				e.Item.Cells[6].Text = "<b>"+String.Format("{0:c}", dbTotalBuyrPaid)+"</b>";

				e.Item.Cells[9].Text = "<b>"+String.Format("{0:c}", dbTotalTVSelr)+"</b>";
				
				e.Item.Cells[10].Text = "<b>"+String.Format("{0:c}", dbTotalSelrPaid)+"</b>";

				e.Item.Cells[11].Text = "<b>"+String.Format("{0:c}", dbTotalFreight)+"</b>";
				e.Item.Cells[12].Text = "<b>"+String.Format("{0:c}", dbTotalCommision)+"</b>";
				e.Item.Cells[15].Text = "<b>"+String.Format("{0:c}", dbTotalInterest)+"</b>";
				e.Item.Cells[16].Text = "<b>"+String.Format("{0:c}", dbMargin)+"</b>";
			}
		}
		*/

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// if the show all months option is select, user needs to be redirected to a new page
    
			if (ddlMonth.SelectedItem.Value.Equals("AllMonth"))
			{
				Response.Redirect("/Creditor/Month_Summary.aspx");
			}
			
			//CreateBrokerList();	// Q: why is this here?? A: was needed to refresh record count which showed in parens after broker name in dropdown ie, John Doe(4) // john doe has 4 transactions 

			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
			//bymonth=bymonth+" WHERE ORDR_ID LIKE '%"+txtSearch.Text+"%'";
			Bind("ORDR_ID DESC");
		}
		/// <summary>
		/// Handler for when the company ddl changes
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void ddlCompanies_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// date list needs to be rebuild according to the new company
			CreateDateList();
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
			txtSearch.Text="";
			Bind(lblSort.Text);
		}

		protected void ddlBroker_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//CreateDateList();
			dg.CurrentPageIndex = 0;
			txtSearch.Text="";
			Bind(lblSort.Text);
		
		}
		/// <summary>
		/// These functions get call by the Sorting button at the top of the column.
		///  There is probably a more elegant way to do it w/ a switch statement but I didn't
		///  set it up like that.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			dg.CurrentPageIndex =0;
			// set all drop down lists to the default. Search against everything.
			ddlMonth.SelectedItem.Selected = false;
			ddlCompanies.SelectedItem.Selected = false;
			ddlBroker.SelectedItem.Selected = false;
			ddlMonth.Items.FindByValue("*").Selected = true;
			Bind("ORDR_ID DESC");			
		}

		private void CreateBrokerList()
		{
			string period = "";
			if (!ddlMonth.SelectedItem.Value.Equals("*"))
			{
				string[] strDate;
				Regex r = new Regex("-"); // Split on spaces.
				strDate = r.Split(ddlMonth.SelectedItem.Value.ToString());
				period +=" AND MONTH(ORDR_DATE)='"+strDate[0]+"'";
				period +=" AND YEAR(ORDR_DATE)='"+strDate[1]+"'";
			}

			using(SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				conn.Open();

				// selecting admins
				string stringSQL = "SELECT P2.PERS_FRST_NAME, P2.PERS_LAST_NAME , P2.PERS_ID, T.QTD "+
					"FROM PERSON P2, (SELECT COUNT(1) QTD, S.SHIPMENT_BROKER_ID "+
					"		FROM ORDERS O, SHIPMENT S, COMPANY C, PERSON P "+
					"		WHERE S.SHIPMENT_BUYR is not null  "+
					"		AND S.SHIPMENT_ORDR_ID=O.ORDR_ID "+
					"		AND P.PERS_ID=S.SHIPMENT_BUYR "+
					"		AND C.COMP_ID=P.PERS_COMP "+
					"		AND (LEFT(C.COMP_NAME,10)<>'The Plasti' OR (LEFT(C.COMP_NAME,10)='The Plasti' AND SHIPMENT_SKU = 'RC1')) "+
					period + 
					"		GROUP BY S.SHIPMENT_BROKER_ID "+
					"		) T "+
					"WHERE P2.PERS_ID *= T.SHIPMENT_BROKER_ID "+
					"AND (P2.PERS_TYPE='T' OR P2.PERS_TYPE='B' OR P2.PERS_TYPE='A') "+
					"ORDER BY T.QTD DESC, P2.PERS_FRST_NAME ";
				using (SqlDataReader dtrAllocate = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn,stringSQL))
				{
					//binding the allocate dd list
					ddlBroker.Items.Clear();
					ddlBroker.Items.Add(new ListItem ("Select Broker","*"));
					while (dtrAllocate.Read())
					{
						//ddlBroker.Items.Add ( new ListItem ((string) dtrAllocate[0] + " " + dtrAllocate[1] + "(" + (HelperFunction.getSafeStringFromDB(dtrAllocate[3])=="" ? "0":HelperFunction.getSafeStringFromDB(dtrAllocate[3])) + ")", dtrAllocate[2].ToString() ) );
						ddlBroker.Items.Add( new ListItem(dtrAllocate[0].ToString() + " " + dtrAllocate[1].ToString(),dtrAllocate[2].ToString()));
					}
					ddlBroker.Items.Add(new ListItem ("All Brokers","*"));
				}				
			}		
		}
	}
}
