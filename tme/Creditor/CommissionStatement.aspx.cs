using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using MetaBuilders.WebControls;
using TPE.Utility;

namespace localhost.Creditor
{
	public partial class CommissionStatement : System.Web.UI.Page
	{
		private int currentMonth;
		private int currentYear;
		protected System.Web.UI.WebControls.DataGrid dbCurrent;
		private int brokerID;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((string)Session["Typ"] != "A")
			{
				Response.Redirect("../default.aspx");
			}
			
			if (!IsPostBack)
			{
				currentMonth = Convert.ToInt32(Request.QueryString["currentMonth"]);
				currentYear = Convert.ToInt32(Request.QueryString["currentYear"]);
				brokerID = Convert.ToInt32(Request.QueryString["BrokerID"]);
				lblBrokerName.Text = Request.QueryString["BrokerName"];
				lblMonth.Text = Request.QueryString["MonthDesc"];
				
				BindLastMonth();
				BindCurrent();
				BindOpenCommissions();
			}
		}
		
		private void BindCurrent()
		{   
			/*
			StringBuilder sbSQL = new StringBuilder();		
			sbSQL.Append("SELECT ");
			sbSQL.Append("BROKER_ID = S.SHIPMENT_BROKER_ID,");
			sbSQL.Append("BROKER_NAME = PERS_FRST_NAME + ' ' + PERS_LAST_NAME,");
			sbSQL.Append("BROKER_TYPE = PERS_TYPE,");
			sbSQL.Append("CUSTOMER = (SELECT COMPANY.COMP_NAME + ' (' + PERSON.PERS_FRST_NAME + ' ' + PERSON.PERS_LAST_NAME + ')' FROM COMPANY, PERSON WHERE COMP_ID = PERS_COMP AND PERS_ID = SHIPMENT_BUYR),");
			sbSQL.Append("COMMISSION_DATE = P.PAY_DATE, ");
			sbSQL.Append("TRANS_NUMBER = CONVERT(VARCHAR,S.SHIPMENT_ORDR_ID) + '-' + CONVERT(VARCHAR,S.SHIPMENT_SKU),");
			sbSQL.Append("WEIGHT = ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY),");
			sbSQL.Append("BUY = SHIPMENT_PRCE,");
			sbSQL.Append("SELL = SHIPMENT_BUYR_PRCE,");
			sbSQL.Append("FREIGHT = ROUND(SHIPMENT_SHIP_PRCE / ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY),2),");
			sbSQL.Append("LOGISTICS_COST = S.SHIPMENT_SHIP_PRCE,");
			sbSQL.Append("DAYS_PAST_DUE = DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),P.PAY_DATE)),");
			sbSQL.Append("CAPITAL_CHARGE =  CONVERT(MONEY,(DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),P.PAY_DATE))/360.0) * 0.18 * ((ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * SHIPMENT_BUYR_PRCE) - S.SHIPMENT_SHIP_PRCE)),");
			sbSQL.Append("GROSS_PROFIT = ((ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) - S.SHIPMENT_SHIP_PRCE) - CONVERT(MONEY,(DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),P.PAY_DATE))/360.0) * 0.18 * ((ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * SHIPMENT_BUYR_PRCE) - S.SHIPMENT_SHIP_PRCE))),");
			sbSQL.Append("COMMISSION = (SHIPMENT_COMM/100.0) * ((ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) - S.SHIPMENT_SHIP_PRCE) - CONVERT(MONEY,(DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),P.PAY_DATE))/360.0) * 0.18 * ((ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * SHIPMENT_BUYR_PRCE) - S.SHIPMENT_SHIP_PRCE))) ");
			sbSQL.Append("FROM ");
			sbSQL.Append("PAYMENT P, SHIPMENT S, PERSON ");
			sbSQL.Append("WHERE ");
			sbSQL.Append("P.PAY_COM_TRANS = 1 ");
			sbSQL.Append("AND S.SHIPMENT_BROKER_ID IS NOT NULL ");
			sbSQL.Append("AND P.PAY_SHIPMENT = S.SHIPMENT_ID ");
			sbSQL.Append("AND PERS_ID = SHIPMENT_BROKER_ID ");
			sbSQL.Append("AND PERS_TYPE IN ('B', 'A', 'T') ");
			sbSQL.Append("AND MONTH(P.PAY_DATE) = " + currentMonth.ToString());
			sbSQL.Append(" AND YEAR(P.PAY_DATE) = " + currentYear.ToString());
			sbSQL.Append(" AND S.SHIPMENT_BROKER_ID = " + brokerID);
			sbSQL.Append(" ORDER BY P.PAY_DATE ");
			*/
			
			Hashtable htParams = new Hashtable();
			htParams.Add("@Month",currentMonth.ToString());
			htParams.Add("@Year",currentYear.ToString());
			htParams.Add("@BrokerID",brokerID);

			DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(),this.dgCurrent,"spCommissionsCurrentMonth",htParams);
		}

		private void BindLastMonth()
		{   
			int lastMonth = currentMonth-1;
			int lastMonthYear =  currentYear;
			
			if (lastMonth == 0)
			{
				lastMonth++;
				lastMonthYear--;
			}

			/*
			StringBuilder sbSQL = new StringBuilder();	
			sbSQL.Append("SELECT ");
			sbSQL.Append("BROKER_ID = S.SHIPMENT_BROKER_ID, ");
			sbSQL.Append("BROKER_NAME = PERS_FRST_NAME + ' ' + PERS_LAST_NAME,");
			sbSQL.Append("BROKER_TYPE = PERS_TYPE,");
			sbSQL.Append("CUSTOMER = (SELECT COMPANY.COMP_NAME + ' (' + PERSON.PERS_FRST_NAME + ' ' + PERSON.PERS_LAST_NAME + ')' FROM COMPANY, PERSON WHERE COMP_ID = PERS_COMP AND PERS_ID = SHIPMENT_BUYR),");
			sbSQL.Append("COMMISSION_DATE = P.PAY_DATE, ");
			sbSQL.Append("TRANS_NUMBER = CONVERT(VARCHAR,S.SHIPMENT_ORDR_ID) + '-' + CONVERT(VARCHAR,S.SHIPMENT_SKU),");
			sbSQL.Append("WEIGHT = ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY),");
			sbSQL.Append("BUY = SHIPMENT_PRCE,");
			sbSQL.Append("SELL = SHIPMENT_BUYR_PRCE,");
			sbSQL.Append("FREIGHT = ROUND(SHIPMENT_SHIP_PRCE / ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY),2),");
			sbSQL.Append("LOGISTICS_COST = S.SHIPMENT_SHIP_PRCE,");
			sbSQL.Append("DAYS_PAST_DUE = DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),P.PAY_DATE)),");
			sbSQL.Append("CAPITAL_CHARGE =  CONVERT(MONEY,(DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),P.PAY_DATE))/360.0) * 0.18 * ((ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * SHIPMENT_BUYR_PRCE) - S.SHIPMENT_SHIP_PRCE)),");
			sbSQL.Append("GROSS_PROFIT = ((ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) - S.SHIPMENT_SHIP_PRCE) - CONVERT(MONEY,(DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),P.PAY_DATE))/360.0) * 0.18 * ((ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * SHIPMENT_BUYR_PRCE) - S.SHIPMENT_SHIP_PRCE))),");
			sbSQL.Append("COMMISSION = (SHIPMENT_COMM/100.0) * ((ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) - S.SHIPMENT_SHIP_PRCE) - CONVERT(MONEY,(DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),P.PAY_DATE))/360.0) * 0.18 * ((ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * SHIPMENT_BUYR_PRCE) - S.SHIPMENT_SHIP_PRCE))) ");
			sbSQL.Append("FROM ");
			sbSQL.Append("PAYMENT P, SHIPMENT S, PERSON ");
			sbSQL.Append("WHERE ");
			sbSQL.Append("P.PAY_COM_TRANS = 1 ");
			sbSQL.Append("AND S.SHIPMENT_BROKER_ID IS NOT NULL ");
			sbSQL.Append("AND P.PAY_SHIPMENT = S.SHIPMENT_ID ");
			sbSQL.Append("AND PERS_ID = SHIPMENT_BROKER_ID ");
			sbSQL.Append("AND PERS_TYPE IN ('B', 'A', 'T') ");
			sbSQL.Append("AND MONTH(P.PAY_DATE) = " + lastMonth.ToString() + " ");
			sbSQL.Append("AND YEAR(P.PAY_DATE) = " + lastMonthYear.ToString() + " ");
			sbSQL.Append("AND S.SHIPMENT_BROKER_ID = " + brokerID + " ");
			sbSQL.Append("ORDER BY P.PAY_DATE ");
			*/
			
			Hashtable htParams = new Hashtable();
			htParams.Add("@Month",lastMonth.ToString());
			htParams.Add("@Year",lastMonthYear.ToString());
			htParams.Add("@BrokerID",brokerID);

			DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(),dgLastMonth,"spCommissionsLastMonth",htParams);			
		}

		private void BindOpenCommissions()
		{   
			/*
			StringBuilder sbSQL = new StringBuilder();	
			sbSQL.Append("SELECT ");
			sbSQL.Append("BROKER_ID = S.SHIPMENT_BROKER_ID, ");
			sbSQL.Append("BROKER_NAME = PERS_FRST_NAME + ' ' + PERS_LAST_NAME, ");
			sbSQL.Append("BROKER_TYPE = PERS_TYPE, ");
			sbSQL.Append("CUSTOMER = (SELECT COMPANY.COMP_NAME + ' (' + PERSON.PERS_FRST_NAME + ' ' + PERSON.PERS_LAST_NAME + ')' FROM COMPANY, PERSON WHERE COMP_ID = PERS_COMP AND PERS_ID = SHIPMENT_BUYR), ");
			sbSQL.Append("TRANS_NUMBER = CONVERT(VARCHAR,S.SHIPMENT_ORDR_ID) + '-' + CONVERT(VARCHAR,S.SHIPMENT_SKU), ");
			sbSQL.Append("WEIGHT = ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY), ");
			sbSQL.Append("BUY = SHIPMENT_PRCE, ");
			sbSQL.Append("SELL = SHIPMENT_BUYR_PRCE, ");
			sbSQL.Append("FREIGHT = ROUND(SHIPMENT_SHIP_PRCE / ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY),2), ");
			sbSQL.Append("LOGISTICS_COST = S.SHIPMENT_SHIP_PRCE, ");
			sbSQL.Append("DAYS_PAST_DUE = DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),GETDATE())), ");
			sbSQL.Append("CAPITAL_CHARGE =  CONVERT(MONEY,(DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),GETDATE()))/360.0) * 0.18 * ((ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * SHIPMENT_BUYR_PRCE) - S.SHIPMENT_SHIP_PRCE)), ");
			sbSQL.Append("GROSS_PROFIT = ((ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) - S.SHIPMENT_SHIP_PRCE) - CONVERT(MONEY,(DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),GETDATE()))/360.0) * 0.18 * ((ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * SHIPMENT_BUYR_PRCE) - S.SHIPMENT_SHIP_PRCE))), ");
			sbSQL.Append("COMMISSION = (SHIPMENT_COMM/100.0) * ((ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) - S.SHIPMENT_SHIP_PRCE) - CONVERT(MONEY,(DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),GETDATE()))/360.0) * 0.18 * ((ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * SHIPMENT_BUYR_PRCE) - S.SHIPMENT_SHIP_PRCE))) ");
			sbSQL.Append("FROM  ");
			sbSQL.Append("SHIPMENT S, PERSON, ORDERS O ");
			sbSQL.Append("WHERE ");
			sbSQL.Append("S.SHIPMENT_BROKER_ID IS NOT NULL ");
			sbSQL.Append("and S.SHIPMENT_ORDR_ID = O.ORDR_ID ");
			sbSQL.Append("AND PERS_ID = SHIPMENT_BROKER_ID ");
			sbSQL.Append("AND CONVERT(VARCHAR, O.ORDR_DATE, 101) >= '12/01/2005' ");
			sbSQL.Append("AND S.SHIPMENT_ID NOT IN (SELECT PAY_SHIPMENT FROM PAYMENT WHERE PAY_COM_TRANS = 1  AND PAY_SHIPMENT = S.SHIPMENT_ID) ");
			sbSQL.Append("AND S.SHIPMENT_BROKER_ID = " + brokerID + " ");
			sbSQL.Append("ORDER BY DAYS_PAST_DUE ");
			*/

			Hashtable htParams = new Hashtable();
			htParams.Add("@BrokerID",brokerID);
			
			DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(),dgOpen,"spCommissionsOpen",htParams);
		}

		
		long dbTotalWeight = 0;
		double dbTotalGrossProfit = 0;
		double dbTotalCommission = 0.0;
		protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
			if (  (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
			{
				dbTotalWeight += Convert.ToInt64(Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "WEIGHT")));
				dbTotalGrossProfit += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "GROSS_PROFIT"));
				dbTotalCommission += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "COMMISSION"));
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[1].Text = "<b>Total:</b>" ;
				e.Item.Cells[2].Text = "<b>"+String.Format("{0:#,###}", dbTotalWeight)+"</b>";
				e.Item.Cells[9].Text = "<b>"+String.Format("{0:#,###.00}", dbTotalGrossProfit)+"</b>";
				e.Item.Cells[10].Text = "<b>"+String.Format("{0:#,###.00}", dbTotalCommission)+"</b>";
			}
		}

		long dbTotalWeightLM = 0;
		double dbTotalGrossProfitLM = 0;
		double dbTotalCommissionLM = 0.0;
		protected void KeepRunningSumLM(object sender, DataGridItemEventArgs e)
		{
			if (  (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
			{
				dbTotalWeightLM += Convert.ToInt64(Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "WEIGHT")));
				dbTotalGrossProfitLM += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "GROSS_PROFIT"));
				dbTotalCommissionLM += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "COMMISSION"));
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[1].Text = "<b>Total:</b>" ;
				e.Item.Cells[2].Text = "<b>"+String.Format("{0:#,###}", dbTotalWeightLM)+"</b>";
				e.Item.Cells[9].Text = "<b>"+String.Format("{0:#,###.00}", dbTotalGrossProfitLM)+"</b>";
				e.Item.Cells[10].Text = "<b>"+String.Format("{0:#,###.00}", dbTotalCommissionLM)+"</b>";
			}
		}

		long dbTotalWeightOpen = 0;
		double dbTotalGrossProfitOpen = 0;
		double dbTotalCommissionOpen = 0.0;
		protected void KeepRunningSumOpen(object sender, DataGridItemEventArgs e)
		{
			if (  (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
			{
				dbTotalWeightOpen+= Convert.ToInt64(Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "WEIGHT")));
				dbTotalGrossProfitOpen += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "GROSS_PROFIT"));
				dbTotalCommissionOpen += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "COMMISSION"));
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[1].Text = "<b>Total:</b>" ;
				e.Item.Cells[2].Text = "<b>"+String.Format("{0:#,###}", dbTotalWeightOpen)+"</b>";
				e.Item.Cells[9].Text = "<b>"+String.Format("{0:#,###.00}", dbTotalGrossProfitOpen)+"</b>";
				e.Item.Cells[10].Text = "<b>"+String.Format("{0:#,###.00}", dbTotalCommissionOpen)+"</b>";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

	}
}