<%@ Register TagPrefix="mbdb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.DefaultButtons" %>
<%@ Page Language="c#" CodeBehind="Transaction_Summary.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.Transaction_Summary" MasterPageFile="~/MasterPages/Menu.Master"%>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphCssLink">
	<style>
	.menuskin { BORDER-RIGHT: black 2px solid; BORDER-TOP: black 2px solid; Z-INDEX: 100; VISIBILITY: hidden; FONT: 12px/18px Verdana; BORDER-LEFT: black 2px solid; WIDTH: 165px; BORDER-BOTTOM: black 2px solid; POSITION: absolute; BACKGROUND-COLOR: menu }
	.menuskin A { PADDING-RIGHT: 10px; PADDING-LEFT: 10px; COLOR: black; TEXT-DECORATION: none }
	#mouseoverstyle { BACKGROUND-COLOR: highlight }
	#mouseoverstyle A { COLOR: white }
	</style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">
	<script language="JavaScript1.2">

        //Pop-it menu- By Dynamic Drive
        //For full source code and more DHTML scripts, visit http://www.dynamicdrive.com
        //This credit MUST stay intact for use

        var linkset=new Array()
        //SPECIFY MENU SETS AND THEIR LINKS. FOLLOW SYNTAX LAID OUT

        <asp:PlaceHolder id="phActionButton"  runat="server" />
        ////No need to edit beyond here

        var ie4=document.all&&navigator.userAgent.indexOf("Opera")==-1
        var ns6=document.getElementById&&!document.all
        var ns4=document.layers

        function showmenu(e,which){

        if (!document.all&&!document.getElementById&&!document.layers)
        return

        clearhidemenu()

        menuobj=ie4? document.all.popmenu : ns6? document.getElementById("popmenu") : ns4? document.popmenu : ""
        menuobj.thestyle=(ie4||ns6)? menuobj.style : menuobj

        if (ie4||ns6)
        menuobj.innerHTML=which
        else{
        menuobj.document.write('<layer name=gui bgColor=#E6E6E6 width=165 onmouseover="clearhidemenu()" onmouseout="hidemenu()">'+which+'</layer>')
        menuobj.document.close()
        }

        menuobj.contentwidth=(ie4||ns6)? menuobj.offsetWidth : menuobj.document.gui.document.width
        menuobj.contentheight=(ie4||ns6)? menuobj.offsetHeight : menuobj.document.gui.document.height
        eventX=ie4? event.clientX : ns6? e.clientX : e.x
        eventY=ie4? event.clientY : ns6? e.clientY : e.y

        //Find out how close the mouse is to the corner of the window
        var rightedge=ie4? document.body.clientWidth-eventX : window.innerWidth-eventX
        var bottomedge=ie4? document.body.clientHeight-eventY : window.innerHeight-eventY

        //if the horizontal distance isn't enough to accomodate the width of the context menu
        if (rightedge<menuobj.contentwidth)
        //move the horizontal position of the menu to the left by it's width
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX-menuobj.contentwidth : ns6? window.pageXOffset+eventX-menuobj.contentwidth : eventX-menuobj.contentwidth
        else
        //position the horizontal position of the menu where the mouse was clicked
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX : ns6? window.pageXOffset+eventX : eventX

        //same concept with the vertical position
        if (bottomedge<menuobj.contentheight)
        menuobj.thestyle.top=ie4? document.body.scrollTop+eventY-menuobj.contentheight : ns6? window.pageYOffset+eventY-menuobj.contentheight : eventY-menuobj.contentheight
        else
        menuobj.thestyle.top=ie4? document.body.scrollTop+event.clientY : ns6? window.pageYOffset+eventY : eventY
        menuobj.thestyle.visibility="visible"
        return false
        }

        function contains_ns6(a, b) {
        //Determines if 1 element in contained in another- by Brainjar.com
        while (b.parentNode)
        if ((b = b.parentNode) == a)
        return true;
        return false;
        }

        function hidemenu(){
        if (window.menuobj)
        menuobj.thestyle.visibility=(ie4||ns6)? "hidden" : "hide"
        }

        function dynamichide(e){
        if (ie4&&!menuobj.contains(e.toElement))
        hidemenu()
        else if (ns6&&e.currentTarget!= e.relatedTarget&& !contains_ns6(e.currentTarget, e.relatedTarget))
        hidemenu()
        }

        function delayhidemenu(){
        if (ie4||ns6||ns4)
        delayhide=setTimeout("hidemenu()",500)
        }

        function clearhidemenu(){
        if (window.delayhide)
        clearTimeout(delayhide)
        }

        function highlightmenu(e,state){
        if (document.all)
        source_el=event.srcElement
        else if (document.getElementById)
        source_el=e.target
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        else{
        while(source_el.id!="popmenu"){
        source_el=document.getElementById? source_el.parentNode : source_el.parentElement
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        }
        }
        }

        if (ie4||ns6)
        document.onclick=hidemenu

	</script>
</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
    <div class="menuskin" id="popmenu" onmouseover="clearhidemenu();highlightmenu(event,'on')"
		onmouseout="highlightmenu(event,'off');dynamichide(event)"></div>
	<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0" class="Content LinkNormal">
		<tr>
			<td align="center"><span class="Content">Transaction Summary</span>&nbsp;&nbsp;&nbsp;&nbsp;
				<A class="LinkNormal" href="/Creditor/Month_Summary.aspx">Monthly Summary</A></td>
		</tr>
		<tr>
			<td align="center">
				<table width="100%" align="left">
					<tr vAlign="bottom">
						<td vAlign="top" align="left"><B>Filters:</B><br>
						</td>
						<td><B>Date</B><br>
							<asp:dropdownlist CssClass="InputForm" id="ddlMonth" runat="server" AutoPostBack="True" EnableViewState="True" onselectedindexchanged="ddlMonth_SelectedIndexChanged"></asp:dropdownlist></td>
						<td vAlign="top"><B>Company</B><br>
							<asp:dropdownlist CssClass="InputForm" id="ddlCompanies" runat="server" AutoPostBack="True" EnableViewState="True" onselectedindexchanged="ddlCompanies_SelectedIndexChanged"></asp:dropdownlist></td>
						<td vAlign="top"><B>Broker</B><br>
							<asp:dropdownlist CssClass="InputForm" id="ddlBroker" runat="server" AutoPostBack="True" EnableViewState="True" onselectedindexchanged="ddlBroker_SelectedIndexChanged"></asp:dropdownlist></td>
						<td vAlign="top" align="left"><B>Search</B><br>
							<mbdb:defaultbuttons id="DefaultSearchButtons" runat="server">
								<mbdb:DefaultButtonSetting button="btnSearch" parent="txtSearch"></mbdb:DefaultButtonSetting>
							</mbdb:defaultbuttons><asp:label id="lblSort" runat="server" visible="false"></asp:label><asp:textbox CssClass="InputForm" id="txtSearch" runat="server" size="8"></asp:textbox><asp:button CssClass="Content Color2" id="btnSearch" runat="server" Text="Go" onclick="btnSearch_Click"></asp:button></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center"><asp:datagrid BorderWidth="0" BackColor="#000000" CellSpacing="1" id="dg" runat="server" Width="85%" CssClass="DataGrid" HorizontalAlign="Center"
					CellPadding="2" DataKeyField="ORDR_ID" ShowFooter="True" OnItemDataBound="KeepRunningSum" AllowSorting="True"
					onSortCommand="SortDG" AutoGenerateColumns="False">
					<AlternatingItemStyle CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
					<ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
					<HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
					<FooterStyle CssClass="Content Color4 Bold FooterColor" />
					<Columns>
						<asp:TemplateColumn>
							<ItemTemplate>
								<CENTER><IMG src="/pics/icons/icon_magnifying_glass.gif"></CENTER>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="ID" DataNavigateUrlFormatString="../administrator/Transaction_Details.aspx?Referer=/Creditor/Transaction_Summary.aspx&amp;ID={0}"
							DataTextField="ID">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:BoundColumn DataField="ORDR_DTE" SortExpression="ORDR_DTE ASC" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
						<asp:BoundColumn DataField="VARCONTRACT" SortExpression="VARCONTRACT ASC" HeaderText="Product">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARWEIGHT" SortExpression="VARWEIGHT ASC" HeaderText="Weight(lbs)" DataFormatString="{0:#,###}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARRCNUMBER" SortExpression="VARRCNUMBER ASC" HeaderText="RC#">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARTVBUYR" SortExpression="VARTVBUYR ASC" HeaderText="From Buyer" DataFormatString="{0:c}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="ID" DataNavigateUrlFormatString="Edit_Payments.aspx?Receivable=true&amp;ID={0}"
							DataTextField="VARPPAID" SortExpression="VARPPAID ASC" HeaderText="Buyer Paid" DataTextFormatString="{0:c}">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:BoundColumn DataField="VARBUYPRICE" SortExpression="VARBUYPRICE ASC" HeaderText="Buy Price">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARSELLPRICE" SortExpression="VARSELLPRICE ASC" HeaderText="Sell Price">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARTVSELR" SortExpression="VARTVSELR ASC" HeaderText="To Seller" DataFormatString="{0:c}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="ID" DataNavigateUrlFormatString="Edit_Payments.aspx?ID={0}"
							DataTextField="VARSPAID" SortExpression="VARSPAID ASC" HeaderText="Seller Paid" DataTextFormatString="{0:c}">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:BoundColumn DataField="VARFREIGHT" SortExpression="VARFREIGHT ASC" HeaderText="Frieght Cost"
							DataFormatString="{0:c}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARCOMMISION" SortExpression="VARCOMMISION ASC" HeaderText="Commission"
							DataFormatString="{0:c}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARBROKER" SortExpression="VARBROKER ASC" HeaderText="Broker"></asp:BoundColumn>
						<asp:BoundColumn DataField="SHIPMENT_BUYER_TERMS" SortExpression="SHIPMENT_BUYER_TERMS ASC" HeaderText="Terms">
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn HeaderText="Days Past Due">
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn HeaderText="Interest" DataFormatString="{0:c}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARFEE" SortExpression="VARFEE ASC" HeaderText="Margin" DataFormatString="{0:c}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="VARBUYR_ID" DataNavigateUrlFormatString="/Creditor/AccountSummary.aspx?Id={0}"
							DataTextField="VARBUYR" SortExpression="VARBUYR ASC" HeaderText="Buyer">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="VARSELR_ID" DataNavigateUrlFormatString="/Creditor/AccountSummary.aspx?Id={0}"
							DataTextField="VARSELR" SortExpression="VARSELR ASC" HeaderText="Seller">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:BoundColumn Visible="False" DataField="SHIPMENT_DATE_TAKE" HeaderText="Shipment Date"></asp:BoundColumn>
						<asp:BoundColumn Visible="False" DataField="VARPPAIDDATE" HeaderText="Payment Date"></asp:BoundColumn>
					</Columns>
				</asp:datagrid><asp:label id="lblMessage" runat="server">There are no transactions listed that match this request!</asp:label></td>
		</tr>
	</table>
</asp:Content>
