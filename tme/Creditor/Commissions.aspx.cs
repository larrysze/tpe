using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using MetaBuilders.WebControls;
using TPE.Utility;

namespace localhost.Creditor
{
	public partial class Commission : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((string)Session["Typ"] != "A")
			{
				Response.Redirect("../default.aspx");
			}
			
			if (!IsPostBack)
			{
				HelperFunction.fillDates(12,2005,ddlMonth);

				ddlMonth.SelectedItem.Selected = false;
				ddlMonth.Items[0].Selected = true;

				Bind();
			}
		}
		
		private void Bind()
		{   
			/*
			StringBuilder sbSQL = new StringBuilder();
			sbSQL.Append("SELECT ");
			sbSQL.Append("BROKER_ID, ");
			sbSQL.Append("BROKER_NAME, ");
			sbSQL.Append("BROKER_COMMISSION = ISNULL(SUM(COMMISSION),0) ");
			sbSQL.Append("FROM ");
			sbSQL.Append("( ");
			sbSQL.Append("SELECT ");
			sbSQL.Append("BROKER_ID = S.SHIPMENT_BROKER_ID, ");
			sbSQL.Append("BROKER_NAME = PERS_FRST_NAME + ' ' + PERS_LAST_NAME, ");
			sbSQL.Append("BROKER_TYPE = PERS_TYPE, ");
			sbSQL.Append("CUSTOMER = (SELECT COMPANY.COMP_NAME + ' (' + PERSON.PERS_FRST_NAME + ' ' + PERSON.PERS_LAST_NAME + ')' FROM COMPANY, PERSON WHERE COMP_ID = PERS_COMP AND PERS_ID = 381), ");
			sbSQL.Append("COMMISSION_DATE = P.PAY_DATE, ");
			sbSQL.Append("TRANS_NUMBER = CONVERT(VARCHAR,S.SHIPMENT_ORDR_ID) + '-' + CONVERT(VARCHAR,S.SHIPMENT_SKU), ");
			sbSQL.Append("WEIGHT = ISNULL(SHIPMENT_WEIGHT, SHIPMENT_SIZE*SHIPMENT_QTY), ");
			sbSQL.Append("BUY = SHIPMENT_PRCE, ");
			sbSQL.Append("SELL = SHIPMENT_BUYR_PRCE, ");
			sbSQL.Append("FREIGHT = ROUND(SHIPMENT_SHIP_PRCE / ISNULL(SHIPMENT_WEIGHT, SHIPMENT_SIZE*SHIPMENT_QTY),2), ");
			sbSQL.Append("LOGISTICS_COST = S.SHIPMENT_SHIP_PRCE, ");
			sbSQL.Append("DAYS_PAST_DUE = DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),P.PAY_DATE)), ");
			sbSQL.Append("CAPITAL_CHARGE =  CONVERT(MONEY,(DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),P.PAY_DATE))/360.0) * 0.18 * ((ISNULL(SHIPMENT_WEIGHT, SHIPMENT_SIZE*SHIPMENT_QTY) * SHIPMENT_BUYR_PRCE) - S.SHIPMENT_SHIP_PRCE)), ");
			sbSQL.Append("GROSS_PROFIT = ((ISNULL(SHIPMENT_WEIGHT, SHIPMENT_SIZE*SHIPMENT_QTY) * (SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) - S.SHIPMENT_SHIP_PRCE) - CONVERT(MONEY,(DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),P.PAY_DATE))/360.0) * 0.18 * ((ISNULL(SHIPMENT_WEIGHT, SHIPMENT_SIZE*SHIPMENT_QTY) * SHIPMENT_BUYR_PRCE) - S.SHIPMENT_SHIP_PRCE))), ");
			sbSQL.Append("COMMISSION = (SHIPMENT_COMM/100.0) * ((ISNULL(SHIPMENT_WEIGHT, SHIPMENT_SIZE*SHIPMENT_QTY) * (SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) - S.SHIPMENT_SHIP_PRCE) - CONVERT(MONEY,(DBO.POSITIVEORZERO(DATEDIFF(DAY,DATEADD(DAY,S.SHIPMENT_BUYER_TERMS, ISNULL(S.SHIPMENT_DATE_TAKE,GETDATE())),P.PAY_DATE))/360.0) * 0.18 * ((ISNULL(SHIPMENT_WEIGHT, SHIPMENT_SIZE*SHIPMENT_QTY) * SHIPMENT_BUYR_PRCE) - S.SHIPMENT_SHIP_PRCE))) ");
			sbSQL.Append("FROM ");
			sbSQL.Append("PAYMENT P, SHIPMENT S, PERSON ");
			sbSQL.Append("WHERE ");
			sbSQL.Append("P.PAY_COM_TRANS = 1 ");
			sbSQL.Append("AND S.SHIPMENT_BROKER_ID IS NOT NULL ");
			sbSQL.Append("AND P.PAY_SHIPMENT = S.SHIPMENT_ID ");
			sbSQL.Append("AND PERS_ID = SHIPMENT_BROKER_ID ");
			sbSQL.Append("AND PERS_TYPE IN ('B', 'A', 'T') ");
			sbSQL.Append(") C ");
			sbSQL.Append("WHERE MONTH(C.COMMISSION_DATE) = " + ddlMonth.SelectedValue.Substring(4,2).ToString() + " ");
			sbSQL.Append("AND YEAR(C.COMMISSION_DATE) = " + ddlMonth.SelectedValue.Substring(0,4).ToString() + " ");
			sbSQL.Append("GROUP BY BROKER_ID, BROKER_NAME ");
			sbSQL.Append("ORDER BY BROKER_NAME");
			*/
			
			Hashtable htParams = new Hashtable();
			htParams.Add("@Month",ddlMonth.SelectedValue.Substring(4,2).ToString());
			htParams.Add("@Year",ddlMonth.SelectedValue.Substring(0,4).ToString());
			DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(),dg,"spCommissionStatements",htParams);
		}

		
		double dbTotalCommission = 0.0;
		protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
			if (  (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
			{
				dbTotalCommission += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "BROKER_COMMISSION"));

				string transfer = "currentMonth="+ddlMonth.SelectedValue.Substring(4,2);
				transfer+= "&currentYear="+ddlMonth.SelectedValue.Substring(0,4);
				transfer+= "&BrokerID="+e.Item.Cells[0].Text;
				transfer+= "&BrokerName="+e.Item.Cells[1].Text;
				transfer+= "&MonthDesc="+ddlMonth.SelectedItem.Text;

				((Button)e.Item.Cells[3].Controls[1]).Attributes.Add("onClick",
					"javascript: window.open('" + "../Creditor/CommissionStatement.aspx?" + transfer + "','','toolbar=false,resizable=1,scrollbars=1,menubar=false,width=1000,height=950,top=20,left=100')");
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[1].Text = "<b>Total:</b> " ;
				e.Item.Cells[2].Text = "<b>"+String.Format("{0:#,###}", dbTotalCommission)+"</b>";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Bind();
		}

		private void dg_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			
		}

	}
}