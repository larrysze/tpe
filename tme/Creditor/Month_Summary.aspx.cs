using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Text;

namespace localhost.Creditor
{
	/// <summary>
	/// Summary description for Month_Summary.
	/// </summary>
	public partial class Month_Summary : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, EventArgs e)
		{
			//Administrator and Creditor have access
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C"))
			{
				Response.Redirect("/default.aspx");
			}
			//SqlConnection conn;
			//conn = new SqlConnection(ConfigurationSettings.AppSettings["DBConn"]);
			//conn.Open();
			if (!IsPostBack)
			{
				ViewState["Sort"] ="VARYEAR DESC,VARMONTH DESC";
				// populating drop-down box for days late
				// ViewState["Sort"] = "COMP_NAME ASC";
				// ddl.Items.Add(new ListItem ("All","*"));
				// ddl.Items.Add(new ListItem ("Receivables","R"));
				// ddl.Items.Add(new ListItem ("Payables","P"));
    
			}
			Bind();
    
		}
		private void Bind()
		{
			double interest = HelperFunction.getInterestBankParameter(this.Context);
			double limit = HelperFunction.getInterestLimitParameter(this.Context);

			StringBuilder sbSQL = new StringBuilder();
			SqlDataAdapter dadContent;
			DataSet dstContent;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();

			sbSQL.Append(" Select ");
			sbSQL.Append("		VARMONTH = MONTH(ORDR_DATE), ");
			sbSQL.Append("		VARYEAR = YEAR(ORDR_DATE), ");
			sbSQL.Append("		VARWEIGHT = SUM(ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY)), ");
			sbSQL.Append("		VARRECEIVABLES = SUM(SHIPMENT_BUYR_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (100 + ISNULL(SHIPMENT_TAX,0)) / 100), ");
			sbSQL.Append("		VARPAYABLES = SUM(SHIPMENT_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (100 + ISNULL(SHIPMENT_TAX,0)) / 100), ");
			sbSQL.Append("		VARFREIGHT = SUM(SHIPMENT_SHIP_PRCE), ");
//			sbSQL.Append("		VARCOMM = SUM(SHIPMENT_COMM * (( SHIPMENT_PRCE - SHIPMENT_BUYR_PRCE) * ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) - SHIPMENT_SHIP_PRCE) / 100), ");
			sbSQL.Append("		VARCOMM = SUM(SHIPMENT_COMM * ((( SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) * ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-SHIPMENT_SHIP_PRCE) / 100), ");
			sbSQL.Append("		VARINTEREST = ISNULL(MAX(INTERESTS.INTEREST),0), ");
			//sbSQL.Append("      VARMARGIN=SUM(((SHIPMENT_BUYR_PRCE-SHIPMENT_PRCE-SHIPMENT_COMM)*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-SHIPMENT_SHIP_PRCE )- ISNULL(MAX(INTERESTS.INTEREST),0) ");
			sbSQL.Append("      VARMARGIN=SUM(((SHIPMENT_BUYR_PRCE-SHIPMENT_PRCE)*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-SHIPMENT_SHIP_PRCE )- ISNULL(MAX(INTERESTS.INTEREST),0) -SUM(SHIPMENT_COMM * ((( SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) * ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-SHIPMENT_SHIP_PRCE) / 100) ");
			sbSQL.Append("	FROM ORDERS,SHIPMENT, ");
			sbSQL.Append("			(");
			sbSQL.Append("			SELECT M, Y, SUM(ROUND((DAYS_PAST_DUE/360.000000) * (" + interest.ToString() +  "/100) * RECEIVABLES,2)) AS INTEREST");
			sbSQL.Append("			FROM");
			sbSQL.Append("			(");
			sbSQL.Append("			SELECT  MONTH(ORDR_DATE) AS M, YEAR(ORDR_DATE) AS Y,");
			sbSQL.Append("				SHIPMENT_ID,");
			sbSQL.Append("				SHIPMENT_BUYR_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) AS RECEIVABLES,");
			sbSQL.Append("				DATEDIFF(d, CONVERT(VARCHAR,DATEADD(d, CAST(ISNULL(SHIPMENT_BUYER_TERMS,30) AS INTEGER), ISNULL(SHIPMENT_DATE_TAKE, getdate())),101), (SELECT CASE WHEN ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),0) / NULLIF((SHIPMENT_BUYR_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY)),0)*100 < " + limit.ToString() + " THEN CONVERT(VARCHAR,getdate(),101) ELSE ISNULL((SELECT CONVERT(VARCHAR,MAX(PAY_DATE),101) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),CONVERT(VARCHAR,getDate(),101)) END)) AS DAYS_PAST_DUE");
			sbSQL.Append("			FROM SHIPMENT, ORDERS");
			sbSQL.Append("			WHERE SHIPMENT_ORDR_ID=ORDR_ID");
			sbSQL.Append("			AND ORDR_DATE > '1/1/2004'");
			sbSQL.Append("			AND SHIPMENT_SKU <>'RC1'");
			sbSQL.Append("			AND (SELECT COMP_NAME FROM COMPANY where COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)) <> 'The Plastics Exchange'");
			sbSQL.Append("			AND ((DATEDIFF(d, CONVERT(VARCHAR,DATEADD(d, CAST(ISNULL(SHIPMENT_BUYER_TERMS,30) AS INTEGER), ISNULL(SHIPMENT_DATE_TAKE, getdate())),101), ISNULL((SELECT CONVERT(VARCHAR,MAX(PAY_DATE),101) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),CONVERT(VARCHAR,getDate(),101))) > 0) ");
			sbSQL.Append("				OR");
			sbSQL.Append("				(ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),0) / NULLIF((SHIPMENT_BUYR_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY)),0)*100 between 0.01 and " + limit.ToString() + "))");
			sbSQL.Append("			) AS INTEREST");
			sbSQL.Append("			GROUP BY M, Y");
			sbSQL.Append("			) INTERESTS");
			sbSQL.Append("	WHERE  SHIPMENT_ORDR_ID=ORDR_ID  ");
			//sbSQL.Append("		ORDR_DATE is not null and SHIPMENT_BUYR is not null and SHIPMENT_SELR is not null ");
			sbSQL.Append("		AND ORDR_DATE > '1/1/1999' AND SHIPMENT_SKU <>'RC1' ");
			sbSQL.Append("		AND (SELECT COMP_NAME FROM COMPANY where COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)) <> 'The Plastics Exchange' ");
			sbSQL.Append("		AND INTERESTS.M =* MONTH(ORDR_DATE) ");
			sbSQL.Append("		AND INTERESTS.Y =* YEAR(ORDR_DATE) ");
			sbSQL.Append("		GROUP BY YEAR(ORDR_DATE),MONTH(ORDR_DATE) ");
			sbSQL.Append("		ORDER BY " + ViewState["Sort"].ToString());
			//Response.Write(sbSQL.ToString());
			dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dg.DataSource = dstContent;
			dg.DataBind();
			conn.Close();
    
		}
		// <summary>
		// Sorting function
		// </summary>
		public void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{
    
			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;
    
			//  Parse the sort expression - delimiter space
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
			ColumnToSort = SortExprs[0];
			// If a sort order is specified get it, else default is descending
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DeSC";
			}
    
			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;
    
			// Figure out the column index
			int iIndex;
			iIndex = 1;
			switch(ColumnToSort.ToUpper())
			{
				case "VARWEIGHT":
					iIndex = 1;
					break;
				case "VARRECEIVABLES":
					iIndex = 2;
					break;
				case "VARPAYABLES":
					iIndex = 3;
					break;
				case "VARFREIGHT":
					iIndex = 4;
					break;
				case "VARCOMM":
					iIndex = 5;
					break;
				case "VARINTEREST":
					iIndex = 6;
					break;
				case "VARMARGIN":
					iIndex = 7;
					break;
    
    
			}
			// alter the column's sort expression
			dg.Columns[iIndex].SortExpression = NewSortExpr;
    
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
    
			// store the new SortExpr in a labl for use by page function
			ViewState["Sort"] =NewSortExpr;
			// Sort the data in new order
    
			Bind();
		}
    
		double dbMargin = 0.0;
		double dbInterest = 0.0;
		double dbTotalCommision = 0.0;
		double dbTotalTVBuyr = 0.0;
		double dbTotalTVSelr = 0.0;
		double dbTotalFreight = 0.0;
		double iTotalWeight = 0;

		void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				dbInterest += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARINTEREST"));
				dbMargin += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARMARGIN"));
				dbTotalCommision += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARCOMMISION"));
				dbTotalFreight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARFREIGHT"));
				iTotalWeight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARWEIGHT"));
				dbTotalTVBuyr += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR"));
				dbTotalTVSelr += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVSELR"));
				if (Math.Abs(Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID"))-Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVSELR"))) > 10 && Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID")) >0 )
				{
					e.Item.Cells[9].Text    = "<a href=\"Edit_Payments.aspx?ID="+DataBinder.Eval(e.Item.DataItem, "ORDR_ID").ToString()+"\"><font color=red>" + String.Format("{0:c}",Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID"))) + "</a></font>";
				}
				if (Math.Abs(Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))-Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR"))) > 10 && Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))>0 )
				{
					e.Item.Cells[5].Text    = "<a href=\"Edit_Payments.aspx?Receivable=true&ID="+DataBinder.Eval(e.Item.DataItem, "ORDR_ID").ToString()+"\"><font color=red>" + String.Format("{0:c}",Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))) + "</a></font>";
				}
    
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[1].Text = "<b>Totals:</b> " ;
				e.Item.Cells[3].Text = "<b>"+String.Format("{0:#,###}", iTotalWeight)+"</b>";
				e.Item.Cells[4].Text = "<b>"+String.Format("{0:c}", dbTotalTVBuyr)+"</b>";
				e.Item.Cells[8].Text = "<b>"+String.Format("{0:c}", dbTotalTVSelr)+"</b>";
				e.Item.Cells[10].Text = "<b>"+String.Format("{0:c}", dbTotalFreight)+"</b>";
				e.Item.Cells[11].Text = "<b>"+String.Format("{0:c}", dbTotalCommision)+"</b>";
				e.Item.Cells[12].Text = "<b>"+String.Format("{0:c}", dbInterest)+"</b>";
				e.Item.Cells[13].Text = "<b>"+String.Format("{0:c}", dbMargin)+"</b>";
    
			}
    
		}
    
    
    
		protected void OnDataGridBind(object sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");
				e.Item.Attributes.Add("onclick", "window.location ='/Creditor/Transaction_Summary.aspx?ShowDate="+ (DataBinder.Eval(e.Item.DataItem, "VARMONTH")).ToString()+"-"+(DataBinder.Eval(e.Item.DataItem, "VARYEAR")).ToString() +"' ");
				if (e.Item.ItemType == ListItemType.Item)
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee'");
				}
				else
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3'");
				}
    
    
				// sums the items for calculation at bottom
				dbInterest += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARINTEREST"));
				dbMargin += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARMARGIN"));
				dbTotalCommision += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARCOMM"));
				dbTotalFreight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARFREIGHT"));
				iTotalWeight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARWEIGHT"));
				dbTotalTVBuyr += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARRECEIVABLES"));
				dbTotalTVSelr += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPAYABLES"));
    
    
    
				e.Item.Cells[0].Text =(DataBinder.Eval(e.Item.DataItem, "VARMONTH")).ToString()+"/"+(DataBinder.Eval(e.Item.DataItem, "VARYEAR")).ToString();
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[0].Text = "<b>Totals:</b> " ;
				e.Item.Cells[1].Text = "<b>"+String.Format("{0:#,###}", iTotalWeight)+"</b>";
				e.Item.Cells[2].Text = "<b>"+String.Format("{0:c}", dbTotalTVBuyr)+"</b>";
				e.Item.Cells[3].Text = "<b>"+String.Format("{0:c}", dbTotalTVSelr)+"</b>";
				e.Item.Cells[4].Text = "<b>"+String.Format("{0:c}", dbTotalFreight)+"</b>";
				e.Item.Cells[5].Text = "<b>"+String.Format("{0:c}", dbTotalCommision)+"</b>";
				e.Item.Cells[6].Text = "<b>"+String.Format("{0:c}", dbInterest)+"</b>";
				e.Item.Cells[7].Text = "<b>"+String.Format("{0:c}", dbMargin)+"</b>";
    
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
