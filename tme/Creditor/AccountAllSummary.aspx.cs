using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Text;

namespace localhost.Creditor
{
	/// <summary>
	/// Summary description for AccountSummary.
	/// </summary>
	public partial class AccountAllSummary : System.Web.UI.Page
	{
	
		
		/************************************************************************
			  *   1. File Name       :AccountSummary.aspx							 *
			  *   2. Description     :												 *
			  *   3. Modification Log:                                                *
			  *     Ver No.       Date          Author             Modification       *
			  *   -----------------------------------------------------------------   *
			  *                                                                       *
			  *                                                                       *
			  ************************************************************************/
		String  CompType="";
		protected System.Web.UI.WebControls.Label Label1;
		//public string fct = ""; 
		public string fct//save the Transactions
		{
			get
			{
				if(ViewState["fct"]!=null&&ViewState["fct"].ToString().Trim().Length!=0)
				{
					return ViewState["fct"].ToString();
				}
				else
				{
					return "all_hist";
				}
			}
			set
			{
				ViewState["fct"]=value;
			}
		}
		double ValueSum = 0.0;
		double WeightSum = 0.0; 
		double TotalPaidSum = 0.0;
		double ValueSumP = 0.0;
		double WeightSumP = 0.0;
		long iDG1Weight = 0;
		long iDG2Weight =0;
		double TotalPaidSumP = 0.0;

		protected void Page_Load(object sender, EventArgs e)
		{
			//fct = Request.QueryString["fct"];
			if (!IsPostBack )
			{	
				ViewState["Sort2"] ="VARDATEDIFF DESC";
				ViewState["Sort"] ="VARDATEDIFF DESC";
				//Binddg();//bind the dg
				//Binddg2();//bind the dg2
				CheckDisplay();	// does the binds
			}

			//Administrator and Creditor have access

            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C") && ((string)Session["Typ"] != "L"))
			{
				Response.Redirect("/default.aspx");
			}		
			    
		}
	    		
		private void Binddg2()//bind the datagrid (db2)
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			SqlDataAdapter dadContent2;
			DataSet dstContent,dstContent2;		
			System.Data.SqlClient.SqlCommand com2;
			com2=conn.CreateCommand();
			com2.CommandType=CommandType.StoredProcedure;
			com2.CommandText="spAccountAllSummary";	
			if ((ViewState["Sort2"].ToString()!="") && (ViewState["Sort2"]!=null))
			{
				com2.Parameters.Add("@ORDER",ViewState["Sort2"].ToString());
			}
			com2.Parameters.Add("@Type","P");//call the sp,the "p" mains Payables 
			if (fct == "030")
				com2.Parameters.Add("@Funct","030");//0-30
			else if (fct == "03160")
				com2.Parameters.Add("@Funct","03160");//31-60
			else if (fct == "06190")
			{
				com2.Parameters.Add("@Funct","06190");//61-90
			
			}
			else if (fct == "90")
				com2.Parameters.Add("@Funct","90");//91-
			else if (fct == "all_hist")
				com2.Parameters.Add("@Funct","all_hist");//show all(paid /not paid)	
			dadContent2 = new SqlDataAdapter();
			dadContent2.SelectCommand=com2;
			dstContent2 = new DataSet();    
			dadContent2.Fill(dstContent2);
			dg2.DataSource = dstContent2;
			dg2.DataBind();
			conn.Close();
			if (dg2.Items.Count==0) //if dg2 storage data,the title would show
			{
				dg2.Visible=false;
				lblPayables.Visible=false;
                pnlPayables.Visible = false;
			}
			else 
			{
                pnlPayables.Visible = true;
				lblPayables.Visible=true;
				dg2.Visible=true;
			}
			
			//}
			//catch(Exception e)
			//{
			//	Response.Write("exception: "+e+"<br>");
			//}
		}
		
        private void Binddg()//bind datagrid(dg)
		{
			
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			SqlDataAdapter dadContent;
			SqlDataAdapter dadContent2;
			DataSet dstContent,dstContent2;		
			System.Data.SqlClient.SqlCommand com=conn.CreateCommand();
			System.Data.SqlClient.SqlCommand com2;
			com.CommandType=CommandType.StoredProcedure;
			com.CommandText="spAccountAllSummary";//execute	the 		
			if ((ViewState["Sort"]!="") && (ViewState["Sort"]!=null))
			{
				com.Parameters.Add("@ORDER",ViewState["Sort"].ToString());
			}
			com.Parameters.Add("@Type","R");			
			if (fct == "030")
				com.Parameters.Add("@Funct","030");//0-30
			else if (fct == "03160")
				com.Parameters.Add("@Funct","03160");//31-60
			else if (fct == "06190")
				com.Parameters.Add("@Funct","06190");//61-90
			else if (fct == "90")
				com.Parameters.Add("@Funct","90");//91-
			else if (fct == "all_hist")
			{
				com.Parameters.Add("@Funct","all_hist");//show all
			}		
			dadContent=new SqlDataAdapter();
			dadContent.SelectCommand=com;
			dstContent = new DataSet();			
			dadContent.Fill(dstContent);
			dg.DataSource = dstContent;
		    dg.DataBind();
			conn.Close();			
			if (dg.Items.Count==0)
			{
                pnlReceivables.Visible=false;
                lblReceivables.Visible=false;
				dg.Visible=false;
			}
			else
			{
                pnlReceivables.Visible=true;
                lblReceivables.Visible=true;
				dg.Visible=true;
			}	

			if ((dg.Items.Count==0) && (dg2.Items.Count==0))
			{
				lblMessage.Visible = true;
			}
			else
			{
				lblMessage.Visible = false;
			}
			
			//}
			//catch(Exception e)
			//{
			//	Response.Write("exception: "+e+"<br>");
			//}
			
		}
		// <summary>
		//  calculates the amount outstanding.  shows statement
		// </summary>

		private string ReturnPaymentStr(DataGridItemEventArgs e)//creat link direct to make payment screen
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();    
			string Id =Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VARCOMPANYID"));
			String Str;
			//Query the company based the Id
			Str="Select COMP_NAME, COMP_TYPE from company where COMP_ID='"+Id+"'";
			SqlDataReader dtr;
			SqlCommand cmd;
			cmd= new SqlCommand(Str, conn);
			dtr= cmd.ExecuteReader();
			string comName="";
			string comType="";			
			if(dtr.Read())
			{
				comName=dtr["comp_name"].ToString();
				comType=dtr["comp_type"].ToString();			    
			}
			dtr.Close();
			conn.Close();			
			return "<a href=/Creditor/Creditor_Payment.aspx?id="+Id+"&Type="+comType+"&fct="+fct+">"+comName+"</a>";	
		}
		
        protected void OnDataGridBind2(object sender, DataGridItemEventArgs e)
		{	
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{	
				e.Item.Cells[1].Text=ReturnPaymentStr(e);
				if (e.Item.Cells[5].Text.Equals("N/A"))
				
					e.Item.Cells[5].Text="<font align='center'>-</font>";

				//if (e.Item.Cells[5].Text.Equals("0"))
				//	e.Item.Cells[5].Text="<font  align='center'>-</font>";

				if (e.Item.Cells[8].Text.Length>6)
					e.Item.Cells[8].Text = "$"+(e.Item.Cells[8].Text).ToString().Substring(0,6);
				
				double val1 = Convert.ToDouble(e.Item.Cells[10].Text.Substring(1));
				double val2 = Convert.ToDouble(e.Item.Cells[9].Text.Substring(1));
				if(val1>=val2) e.Item.Cells[7].Text = "-";

				e.Item.Cells[0].Text ="<a href=/administrator/Transaction_Details.aspx?Referer="+ Request.Url.ToString() +"&ID="+DataBinder.Eval(e.Item.DataItem, "TRANS_ID")+">Edit</a>";
				e.Item.Cells[11].Text ="<a href=/common/MB_Specs_Invoice.aspx?PO=True&ID="+DataBinder.Eval(e.Item.DataItem, "TRANS_ID")+">PO</a>";
				if (e.Item.Cells[6].Text.Trim()=="&nbsp;") e.Item.Cells[6].Text = " - ";
				e.Item.Cells[10].Text ="<a href=/Creditor/Edit_Payments.aspx?ID="+DataBinder.Eval(e.Item.DataItem, "TRANS_ID")+">"+e.Item.Cells[10].Text+"</a>";

				try
				{
					e.Item.Cells[3].Text = String.Format("{0:#,##}", System.Convert.ToInt32(e.Item.Cells[3].Text));
				}
				catch{}
				iDG2Weight += Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "SHIPMENT_WEIGHT_NUMBER"));
				ValueSumP += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "OUTSTANDING"));
				TotalPaidSumP += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "TOTAL"));
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[0].Text = "<b>Total:</b> ";
				e.Item.Cells[3].Text = "<b>"+String.Format("{0:#,##}", iDG2Weight)+"</b> ";
				e.Item.Cells[9].Text = "<b>"+String.Format("{0:c}", ValueSumP)+"</b>";
				e.Item.Cells[10].Text = "<b>&nbsp;&nbsp;"+String.Format("{0:c}", TotalPaidSumP)+"</b>";
			}
		}

		protected void OnDataGridBind(object sender, DataGridItemEventArgs e)
		{	
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				e.Item.Cells[1].Text=ReturnPaymentStr(e);
				e.Item.Cells[0].Text ="<a href=/administrator/Transaction_Details.aspx?Referer="+ Request.Url.ToString() +"&ID="+DataBinder.Eval(e.Item.DataItem, "TRANS_ID")+">Edit</a>";
				if (e.Item.Cells[5].Text.Equals("N/A"))
					e.Item.Cells[5].Text="<font  align='center'>-</font>";
				
				//if (e.Item.Cells[5].Text.Equals("0"))
				//	e.Item.Cells[5].Text="<font  align='center'>-</font>";

				if (e.Item.Cells[8].Text.Length>6)
					e.Item.Cells[8].Text = "$"+(e.Item.Cells[8].Text).ToString().Substring(0,6) ;

				double val1 = Convert.ToDouble(e.Item.Cells[10].Text.Substring(1));
				double val2 = Convert.ToDouble(e.Item.Cells[9].Text.Substring(1));
				if(val1>=val2) e.Item.Cells[7].Text = "-";

				e.Item.Cells[11].Text ="<a href=/common/MB_Specs_Invoice.aspx?ID="+DataBinder.Eval(e.Item.DataItem, "TRANS_ID")+">Invoice</a>";
				if (e.Item.Cells[6].Text.Trim()=="&nbsp;") e.Item.Cells[6].Text = " - ";
				e.Item.Cells[10].Text ="<a href=/Creditor/Edit_Payments.aspx?Receivable=true&ID="+DataBinder.Eval(e.Item.DataItem, "TRANS_ID")+">"+e.Item.Cells[10].Text+"</a>";
				
				try
				{
					e.Item.Cells[3].Text = String.Format("{0:#,##}", System.Convert.ToInt32(e.Item.Cells[3].Text));
				}
				catch{}

				iDG1Weight += Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "SHIPMENT_WEIGHT_NUMBER"));
				ValueSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "OUTSTANDING"));
				TotalPaidSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "TOTAL"));
			}

			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[0].Text = "<b>Total:</b> ";
				e.Item.Cells[3].Text = "<b>"+String.Format("{0:#,##}", iDG1Weight)+"</b> ";
				e.Item.Cells[9].Text = "<b>"+ String.Format("{0:c}", ValueSum)+"</b>";
				e.Item.Cells[10].Text = "<b>&nbsp;&nbsp;"+String.Format("{0:c}", TotalPaidSum)+"</b>";
			}
		}
		
		// <summary>
		// Sorting function
		// </summary>
		public void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{
			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
    
			ColumnToSort = SortExprs[0];
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DESC";
			}
			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;
			// Figure out the column index
			int iIndex;
			iIndex = 0;
    
			switch(ColumnToSort.ToUpper())
			{
				case "VARCOMPANYNAME"://sort by company id
					iIndex =1;
					break;					
				case "TRANS_ID":
					iIndex =2;
					break;
    
				case "SHIPMENT_WEIGHT":
					iIndex = 3;
					break;
    
				case "SHIPMENT_BUYER_TERMS":
					iIndex = 4;
					break;
    
				case "VARDDATE":
					iIndex = 5;
					break;
    
				case "SHIPMENT_BUYER_CLOSED_DATE":
					iIndex = 6;
					break;

				case "VARDATEDIFF":
					iIndex = 7;
					break;
    
				case "SELL_PRICE":
					iIndex =8;
					break;
    
				case "OUTSTANDING":
					iIndex = 9;
					break;
    
				case "TOTAL":
					iIndex = 10;
					break;
			}
			// alter the column's sort expression
			dg.Columns[iIndex].SortExpression = NewSortExpr;
			dg.CurrentPageIndex = 0;
    
			// Sort the data in new order
			ViewState["Sort"] = NewSortExpr;
			// reset dbase page to the first one whether the sorting changes
			
			Binddg();
			Binddg2();
			CheckDisplay();
		}

		// <summary>
		// Sorting function
		// </summary>
		public void SortDG2(Object sender, DataGridSortCommandEventArgs e )
		{
			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
    
			ColumnToSort = SortExprs[0];
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DESC";
			}
			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;
			// Figure out the column index
			int iIndex;
			iIndex = 0;
    
			switch(ColumnToSort.ToUpper())
			{
				case "VARCOMPANYNAME":
					iIndex =1;
					break;		
				case "TRANS_ID":
					iIndex = 1;
					break;
    
				case "SHIPMENT_WEIGHT":
					iIndex = 2;
					break;
    
				case "SHIPMENT_BUYER_TERMS":
					iIndex = 3;
					break;
    
				case "VARDDATE":
					iIndex = 4;
					break;
    
				case "SHIPMENT_SELLER_CLOSED_DATE1":
					iIndex = 5;
					break;

				case "VARDATEDIFF":
					iIndex = 6;
					break;
    
				case "BUY_PRICE":
					iIndex = 7;
					break;
    
				case "OUTSTANDING":
					iIndex = 8;
					break;
    
				case "TOTAL":
					iIndex = 9;
					break;
			}
			// alter the column's sort expression
			dg2.Columns[iIndex].SortExpression = NewSortExpr;
			dg2.CurrentPageIndex = 0;
    
			// Sort the data in new order
			ViewState["Sort2"] = NewSortExpr;
			// reset dbase page to the first one whether the sorting changes
			
			Binddg();
			Binddg2();
			CheckDisplay();
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void ddlTransaction_SelectedIndexChanged(object sender, System.EventArgs e)
		{		
			this.fct=this.ddlTransaction.SelectedValue;//save listbox value to viewstate.
			
			CheckDisplay();

		}

		protected void rdbReceivables_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckDisplay();
		}

		private void CheckDisplay()
		{
			Binddg();
			Binddg2();

            pnlReceivables.Visible = rdbReceivables.Checked;
			lblReceivables.Visible = rdbReceivables.Checked;
			dg.Visible = rdbReceivables.Checked;

            pnlPayables.Visible = rdbPayables.Checked;
			lblPayables.Visible = rdbPayables.Checked;
			dg2.Visible = rdbPayables.Checked;
		}

		protected void rdbPayables_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckDisplay();
		}

		
	}
}
