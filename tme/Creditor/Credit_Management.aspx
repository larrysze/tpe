
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page Language="c#" CodeBehind="Credit_Management.aspx.cs" AutoEventWireup="false" Inherits="localhost.Creditor.Credit_Management" %>

<form runat="Server">
	<TPE:Template PageTitle="Credit Management" Runat="server" id="Template1" />
	<table align="center">
		<tr>
			<td>
				<asp:DropDownList id="ddl" runat="server" AutoPostBack="true" />
			</td>
		</tr>
		<tr>
			<td>
				<asp:DataGrid id="dg" runat="server" CssClass="DataGrid" ItemStyle-CssClass="DataGridRow" AlternatingItemStyle-CssClass="DataGridRow_Alternate"
					HeaderStyle-CssClass="DataGridHeader" AutoGenerateColumns="false" AllowSorting="true" OnItemDataBound="OnDataGridBind"
					onSortCommand="SortDG" CellPadding="2" ShowFooter="True" HorizontalAlign="Center" Width="400px">
					<Columns>
						<asp:HyperLinkColumn HeaderText="Company Name" ItemStyle-Wrap="false" DataNavigateUrlField="COMP_ID"
							DataNavigateUrlFormatString="/Administrator/Update_Company_Info.aspx?Id={0}" DataTextField="COMP_NAME"
							SortExpression="COMP_NAME ASC" />
						<asp:HyperLinkColumn HeaderText="History" ItemStyle-Wrap="false" DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/Creditor/List_Payments.aspx?Id={0}"
							Text="Past Payments" />
						<asp:BoundColumn DataField="COMP_TYPE" HeaderText="Type" ItemStyle-Wrap="false" SortExpression="COMP_TYPE ASC" />
						<asp:BoundColumn ItemStyle-HorizontalAlign="right" DataField="FO" HeaderText="Outstanding" DataFormatString="{0:c}"
							ItemStyle-Wrap="false" SortExpression="FO ASC" />
						<asp:templatecolumn>
							<headertemplate>Statement</headertemplate>
							<itemtemplate></itemtemplate>
						</asp:templatecolumn>
					</Columns>
				</asp:DataGrid>
			</td>
		</tr>
	</table>
	<TPE:Template Footer="true" Runat="server" id="Template2" />
</form>
