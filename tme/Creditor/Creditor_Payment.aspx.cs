using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
namespace localhost.Creditor
{
	/// <summary>
	/// Summary description for Creditor_Payment.
	/// </summary>
	public partial class Creditor_Payment : System.Web.UI.Page
	{

		//public int cpt1=0;
		//public int cpt2=0;
	
		
		/************************************************************************
		*   1. File Name       :Creditor\Creditors_Payments.aspx                *
		*   2. Description     :Allows users to make payments against the total *
		*   3. Modification Log:                                                *
		*     Ver No.       Date          Author             Modification       *
		*   -----------------------------------------------------------------   *
		*      1.00      3-20-2004      Zach             Rewritten from scratch *
		*                                                                       *
		************************************************************************/
    
    
		// Insert page code here
    
		public void Page_Load(object sender, EventArgs e)
		{
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C") && ((string)Session["Typ"] != "L"))
			{
				Response.Redirect("../default.aspx");
			}
			if (!IsPostBack)
			{
				// save important variables into viewstate
				ViewState["Id"] = Request.QueryString["id"].ToString();
				ViewState["Type"] = Request.QueryString["Type"].ToString();
				ViewState["Fct"] = Request.QueryString["fct"].ToString();
				lblPayables.Visible=false;
                pnlPayables.Visible = false;
				lblReceivables.Visible=false;
                pnlReceivables.Visible=false;
				Bind();
			}       
		}
		private void Bind()
		{
			
			SqlDataAdapter dadContent;
			DataSet dstContent;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
    
			StringBuilder sbSQL = new StringBuilder();
			StringBuilder sbSQL2 = new StringBuilder();
			
			string sbSQLfct="";
			string sbSQL2fct="";

			if (ViewState["Fct"].ToString() == "cur")
			{
				sbSQLfct = " and (DATEDIFF(DAY, SHIPMENT_DATE_TAKE, getdate()) < SHIPMENT_BUYER_TERMS)";	 
				sbSQL2fct = " and (DATEDIFF(DAY, SHIPMENT_DATE_TAKE, getdate()) < SHIPMENT_SELLER_TERMS)";
			}
			
			else if (ViewState["Fct"].ToString() == "O30")
			{
				sbSQLfct = " and (ISNULL(DATEDIFF(DAY,DATEADD(d,CAST(SHIPMENT_BUYER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)),getdate()),0) BETWEEN 1 AND 30) ";	
				sbSQL2fct = " and (ISNULL(DATEDIFF(DAY,DATEADD(d,CAST(SHIPMENT_SELLER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)),getdate()),0) BETWEEN 1 AND 30) ";			
			}
			else if (ViewState["Fct"].ToString() == "O3160")
			{
				sbSQLfct = " and (DATEDIFF(DAY,DATEADD(d,CAST(SHIPMENT_BUYER_TERMS AS INTEGER) , CAST(SHIPMENT_DATE_TAKE AS DATETIME)),getdate()) BETWEEN 31 AND 60) ";
				sbSQL2fct = " and (DATEDIFF(DAY,DATEADD(d,CAST(SHIPMENT_SELLER_TERMS AS INTEGER) , CAST(SHIPMENT_DATE_TAKE AS DATETIME)),getdate()) BETWEEN 31 AND 60) ";	
			}
			else if (ViewState["Fct"].ToString() == "O6190")
			{
				sbSQLfct = " and (DATEDIFF(DAY, DATEADD(d,CAST(SHIPMENT_BUYER_TERMS AS INTEGER), CAST(SHIPMENT_DATE_TAKE AS DATETIME)), getdate()) BETWEEN 61 AND 90) ";
				sbSQL2fct = " and (DATEDIFF(DAY, DATEADD(d,CAST(SHIPMENT_SELLER_TERMS AS INTEGER), CAST(SHIPMENT_DATE_TAKE AS DATETIME)), getdate()) BETWEEN 61 AND 90) ";				
			}
			else if (ViewState["Fct"].ToString() == "90")
			{
				sbSQLfct = " and (DATEDIFF(DAY,DATEADD(d,CAST(SHIPMENT_BUYER_TERMS AS INTEGER), CAST(SHIPMENT_DATE_TAKE AS DATETIME)), getdate()) > 90) ";
				sbSQL2fct = " and (DATEDIFF(DAY,DATEADD(d,CAST(SHIPMENT_SELLER_TERMS AS INTEGER), CAST(SHIPMENT_DATE_TAKE AS DATETIME)), getdate()) > 90) ";				
			}
			else
			{
				sbSQLfct = "";
				sbSQL2fct = "";			
			}

			sbSQL.Append(" SELECT * ");
			sbSQL.Append(" FROM (SELECT *, ");
			sbSQL.Append("		VARID = CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR), ");
			sbSQL.Append(" 		VAROWES=VARTV30-ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),0), ");
			sbSQL.Append(" 		VARTV=VARTV30, ");
			sbSQL.Append("		VARNETDUE=ISNULL (CONVERT(VARCHAR,DATEADD(d,CAST(SHIPMENT_BUYER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)),101),0) FROM (SELECT BOOL=ISNULL(SHIPMENT_WEIGHT,0), ");
			sbSQL.Append("      VARDIFF =ISNULL(DATEDIFF(d, DATEADD(d,CAST(SHIPMENT_BUYER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)), getdate()),0), ");
			sbSQL.Append(" 		SHIPMENT_BUYER_TERMS=ISNULL(SHIPMENT_BUYER_TERMS,30), ");
			sbSQL.Append(" 		SHIPMENT_SIZE, ");
			sbSQL.Append(" 		SHIPMENT_DATE_TAKE=ISNULL(SHIPMENT_DATE_TAKE,GETDATE()), ");
			sbSQL.Append(" 		SHIPMENT_ORDR_ID, ");
			sbSQL.Append(" 		SHIPMENT_BUYR, ");
			sbSQL.Append(" 		VARCOMPANY =(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)), ");
			//sbSQL.Append(" 		VARFEE=(CASE SHIPMENT_MARK WHEN 0 THEN (SHIPMENT_PRCE*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)*(CASE SHIPMENT_SIZE WHEN 42000 THEN 0.04 ELSE 0.03 END)) ELSE (SHIPMENT_MARK*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)) END), ");
			sbSQL.Append(" 		VARTV30=(SHIPMENT_BUYR_PRCE)*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)* (( 100 + ISNULL(SHIPMENT_TAX,0) ) / 100), ");
			sbSQL.Append(" 		SHIPMENT_ID,");	
			//sbSQL.Append(" 		SHIPMENT_BUYR,");
			sbSQL.Append(" 		SHIPMENT_SKU ");
			sbSQL.Append(" 		FROM SHIPMENT ");
			sbSQL.Append(" 		WHERE SHIPMENT_BUYER_CLOSED_DATE IS NULL AND (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)="+ ViewState["Id"].ToString() + sbSQLfct);
			sbSQL.Append(" )Q0)Q1 ");
			//Response.Write(sbSQL.ToString());
			dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			
			dg.DataSource = dstContent;
			dg.DataBind();
			// resets sum variables
			dbTotalValueOwed = 0.0;
			dbTotalTransactionValue = 0.0;


			lblReceivables.Visible=true;
            pnlReceivables.Visible=true;
			if (dstContent.Tables[0].Rows.Count==0)
			{	lblReceivables.Visible=false;
                pnlReceivables.Visible=false;
				dg.Visible = false;
			}
			//else
			//	Response.Write("No data from the 1st request!");
                 
			/*sbSQL2.Append("SELECT * ");
			sbSQL2.Append("FROM (SELECT *, ");
			sbSQL2.Append("			VARID = CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR), ");
			sbSQL2.Append("			VAROWES=VARTV, ");
			sbSQL2.Append("          VARDIFF ='', ");
			sbSQL2.Append("			VARNETDUE=CONVERT(VARCHAR,DATEADD(d,30,CAST(SHIPMENT_DATE_TAKE AS DATETIME)),101) FROM (SELECT BOOL=ISNULL(SHIPMENT_WEIGHT,0), ");
			sbSQL2.Append("			SHIPMENT_DATE_TAKE=ISNULL(SHIPMENT_DATE_TAKE,GETDATE()), ");
			sbSQL2.Append("			SHIPMENT_SIZE, ");
			sbSQL2.Append("			SHIPMENT_ORDR_ID, ");
			sbSQL2.Append("			SHIPMENT_SELR, ");
			sbSQL2.Append("			VARCOMPANY=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)), ");
			//sbSQL2.Append("			VARFEE=(CASE SHIPMENT_MARK WHEN 0 THEN (SHIPMENT_PRCE*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)*(CASE SHIPMENT_SIZE WHEN 42000 THEN 0.04 ELSE 0.03 END)) ELSE (SHIPMENT_MARK*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)) END), ");
			sbSQL2.Append("			VARTV=SHIPMENT_PRCE*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY), ");
			sbSQL2.Append("			SHIPMENT_SKU ");
			sbSQL2.Append("FROM SHIPMENT ");
			sbSQL2.Append("WHERE SHIPMENT_SELLER_CLOSED_DATE1 IS NULL AND (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)=" + ViewState["Id"].ToString());
			sbSQL2.Append(")Q0)Q1 "); */

			sbSQL2.Append(" SELECT * ");
			sbSQL2.Append(" FROM (SELECT *, ");
			sbSQL2.Append("		    VARID = CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR), ");
			sbSQL2.Append(" 		VAROWES=VARTV30-ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_SELR),0), ");
			sbSQL2.Append(" 		VARTV=VARTV30, ");
			sbSQL2.Append("         VARNETDUE=ISNULL (CONVERT(VARCHAR,DATEADD(d,CAST(SHIPMENT_SELLER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)),101),0) FROM (SELECT BOOL=ISNULL(SHIPMENT_WEIGHT,0), ");
			sbSQL2.Append("         VARDIFF =ISNULL(DATEDIFF(d, DATEADD(d,CAST(SHIPMENT_SELLER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)), getdate()),0), ");
			sbSQL2.Append(" 		SHIPMENT_SELLER_TERMS=ISNULL(SHIPMENT_SELLER_TERMS,30), ");
			sbSQL2.Append(" 		SHIPMENT_SIZE, ");
			sbSQL2.Append(" 		SHIPMENT_DATE_TAKE=ISNULL(SHIPMENT_DATE_TAKE,GETDATE()), ");
			sbSQL2.Append(" 		SHIPMENT_ORDR_ID, ");
			sbSQL2.Append(" 		SHIPMENT_SELR, ");
			sbSQL2.Append(" 		VARCOMPANY =(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)), ");
			//sbSQL.Append(" 		VARFEE=(CASE SHIPMENT_MARK WHEN 0 THEN (SHIPMENT_PRCE*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)*(CASE SHIPMENT_SIZE WHEN 42000 THEN 0.04 ELSE 0.03 END)) ELSE (SHIPMENT_MARK*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)) END), ");
			sbSQL2.Append(" 		VARTV30=(SHIPMENT_PRCE)*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY) * (( 100 + ISNULL(SHIPMENT_TAX,0) ) / 100), ");
			sbSQL2.Append(" 		SHIPMENT_ID,");	
			//sbSQL.Append(" 		SHIPMENT_BUYR,");
			sbSQL2.Append(" 		SHIPMENT_SKU ");
			sbSQL2.Append(" 		FROM SHIPMENT ");
			sbSQL2.Append(" 		WHERE SHIPMENT_SELLER_CLOSED_DATE1 IS NULL AND (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)="+ ViewState["Id"].ToString() + sbSQL2fct);
			sbSQL2.Append(" )Q0)Q1 ");
			
			dadContent = new SqlDataAdapter(sbSQL2.ToString(),conn);
			dstContent = new DataSet();
			//Response.Write(sbSQL2.ToString());
			dadContent.Fill(dstContent);
			
			//cpt2 = dstContent.Tables[0].Rows.Count +1 ;

			//Response.Write("get data from database... | ");
			dg2.DataSource = dstContent;
			dg2.DataBind();
			dbTotalValueOwed = 0.0;
			dbTotalTransactionValue = 0.0;

			lblPayables.Visible=true;
            pnlPayables.Visible=true;
			if (dstContent.Tables[0].Rows.Count==0)
			{   lblPayables.Visible=false;
                pnlPayables.Visible=false;
				dg2.Visible = false;
			}
			//else
			//	Response.Write("No data from the 2nd request!");
			
			conn.Close();		
    
		}
		// <summary>
		// Removes bad charachters that would crap out out the db insert
		// </summary>
		public string Parse(string TextIN)
		{
			//  remove ' character
			string Parse;
			Parse = TextIN;
			// remove $ sign at beginning
			Parse = Parse.Substring(1,Parse.Length-1);
    
			while (Parse.IndexOf(",") > 0)
			{
				Parse = Parse.Replace(",", "");
			}
			// remove " character
			//while (Parse.IndexOf("$") > 0){
			//    Parse = Parse.Replace("$", "");
			//}
			Parse.Trim();
			return Parse;
    
		}

		
		private bool checkFlagFullPayment(string pay_shipment, string new_ammount)
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			string sqlSelect = "select " +
				"		  ISNULL((select (pay_com_trans) " + 
				" from payment " + 
				" where pay_com_trans=1 and pay_shipment=" + pay_shipment + "),0) as paid," + 

				"	(" +
				"	  (select PARAM_VALUE from parameters where param_name='PERCENT_FULL_PAYMENT') / 100.0 * " +
				"		(select SHIPMENT_BUYR_PRCE * " +
				"			ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) " +
				"			from shipment " +
				"			where shipment_id=" + pay_shipment + 
				"	         )" +
				"	  - ISNULL(sum(pay_amnt),0) - " + new_ammount +
				"	  ) as diff " +
				" from payment, shipment  " +
				" where pay_shipment=shipment_id " +
				" and (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR) = (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=pay_pers) " +
				"	and " + 
				"shipment_id =" + pay_shipment;

			SqlCommand cmdSelect = new SqlCommand(sqlSelect, conn);

			SqlDataReader dtrDiff = cmdSelect.ExecuteReader();


			bool checkFlag = false;
			if(dtrDiff.Read())
			{
				//> 0 means NOT full ammount with current payment
				if( (HelperFunction.getSafeDoubleFromDB(dtrDiff["diff"]) > 0) &&  !HelperFunction.getSafeBooleanFromDB(dtrDiff["paid"]))
				{
						//only if flag is 0
						checkFlag = true;
				}

			}
			
			dtrDiff.Close();

			conn.Close();
			return checkFlag;

		}
		
		
		// <summary>
		// Adds the payment and updates the correct tables
		// </summary>
    


		protected void Click_Submit(object sender, EventArgs e)
		{
			//Bind(); //pb!
			SqlConnection conn;
			SqlCommand cmdSQL;
			SqlDataReader dtrSQL;

			SqlCommand cmdAddPayment;
			string strShipment_ID;
			string strPersID;
			string strOrdr_ID;
			// payments default to current time
			string strDate = DateTime.Now.ToString();
    
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
    
			int iCounter =0;
			string strSQL;
			// enumerate though trasactions
			//Response.Write("<br>cpt1 = "+cpt1.ToString());
			bool alreadyHaveFlag = false;
			if (Convert.ToInt16(dg.Items.Count) !=0)
			{
				foreach ( DataGridItem myDataGridItem in dg.Items)
				{
					// only proceed if a amount is entered for the order
					if (((TextBox)myDataGridItem.FindControl("txtAmount")).Text !="")
					{
						// save Order ID into a string for later use
						// Get SHIPMENT ID
						string[] arrID;
						Regex r = new Regex("-"); // Split on .
						arrID = r.Split(dg.DataKeys[iCounter].ToString()) ;
						SqlCommand cmdShipmentID = new SqlCommand("Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID = '"+arrID[0].ToString()+"' and SHIPMENT_SKU ='"+arrID[1].ToString()+"'",conn);
						strShipment_ID = cmdShipmentID.ExecuteScalar().ToString(); 
						strOrdr_ID =arrID[0].ToString();
						
						//strOrdr_ID = dg.DataKeys[iCounter].ToString();

						// get info for later inserting //manoj
						cmdSQL = new SqlCommand("Select SHIPMENT_BUYR,SHIPMENT_SELR from SHIPMENT where SHIPMENT_ID =" + strShipment_ID,conn);
						dtrSQL = cmdSQL.ExecuteReader();
						dtrSQL.Read();
						// determine what type of user in making the payment.  saves the correct id
					
					
						strPersID = dtrSQL["SHIPMENT_BUYR"].ToString();
    
						dtrSQL.Close();
    
						strSQL = ""; // reset string
						
						string new_ammount = ((TextBox)myDataGridItem.FindControl("txtAmount")).Text;
						strSQL += "INSERT INTO PAYMENT (PAY_ORDR,PAY_SHIPMENT,PAY_PERS,PAY_AMNT,PAY_DATE,PAY_CMNT, PAY_COM_TRANS) VALUES('" +strOrdr_ID+"','"+strShipment_ID+"','"+strPersID+"','" + new_ammount +"','"+strDate+"','"+ HelperFunction.RemoveQuotationMarks(((TextBox)myDataGridItem.FindControl("txtComments")).Text) +"'";
						string flag = "0";
						if(!alreadyHaveFlag)
						{
							if(checkFlagFullPayment(strShipment_ID, new_ammount))
							{
								flag = "1";
								alreadyHaveFlag = true;
							}

						}

						strSQL += ", " + flag + ")";
						//Response.Write(strSQL);
						cmdAddPayment = new SqlCommand(strSQL,conn);
						cmdAddPayment.ExecuteNonQuery();

						
						// determine if the payment is large enough to cover the transaction
						if (Convert.ToDouble(Parse((myDataGridItem.Cells[2]).Text)) <= Convert.ToDouble(((TextBox)myDataGridItem.FindControl("txtAmount")).Text))
						{
							SqlCommand cmdUpdate;
							cmdUpdate = new SqlCommand("Update SHIPMENT SET SHIPMENT_BUYER_CLOSED_DATE =getDate() WHERE SHIPMENT_ID = '" +strShipment_ID+"'",conn);
							//Response.Write("update");
							cmdUpdate.ExecuteNonQuery();
						}
					}
					iCounter++;
				}
			}
		
			//Response.Write("<br>cpt2 = "+cpt2.ToString());
			if (Convert.ToInt16(dg2.Items.Count)!=0)	
			{
				//Response.Write("update dg2");
				iCounter=0;
				foreach ( DataGridItem myDataGridItem in dg2.Items)
				{
					// only proceed if a amount is entered for the order
					if (((TextBox)myDataGridItem.FindControl("txtAmount")).Text !="")
					{
						// save Order ID into a string for later use
						// Get SHIPMENT ID
						string[] arrID;
						Regex r = new Regex("-"); // Split on .
						arrID = r.Split(dg2.DataKeys[iCounter].ToString()) ;
						SqlCommand cmdShipmentID = new SqlCommand("Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID = '"+arrID[0].ToString()+"' and SHIPMENT_SKU ='"+arrID[1].ToString()+"'",conn);
						strShipment_ID = cmdShipmentID.ExecuteScalar().ToString(); 
						strOrdr_ID =arrID[0].ToString();
						
						//strOrdr_ID = dg.DataKeys[iCounter].ToString();

						// get info for later inserting //manoj
						cmdSQL = new SqlCommand("Select SHIPMENT_BUYR,SHIPMENT_SELR from SHIPMENT where SHIPMENT_ID =" + strShipment_ID,conn);
						dtrSQL = cmdSQL.ExecuteReader();
						dtrSQL.Read();
						// determine what type of user in making the payment.  saves the correct id
					
					
						strPersID = dtrSQL["SHIPMENT_SELR"].ToString();
    
						dtrSQL.Close();
    
						strSQL = ""; // reset string
						
						strSQL += "INSERT INTO PAYMENT (PAY_ORDR,PAY_SHIPMENT,PAY_PERS,PAY_AMNT,PAY_DATE,PAY_CMNT) VALUES('" +strOrdr_ID+"','"+strShipment_ID+"','"+strPersID+"','"+((TextBox)myDataGridItem.FindControl("txtAmount")).Text+"','"+strDate+"','"+ ((TextBox)myDataGridItem.FindControl("txtComments")).Text +"')";
						//Response.Write("<br>"+strSQL);
						cmdAddPayment = new SqlCommand(strSQL,conn);
						cmdAddPayment.ExecuteNonQuery();
						
						// determine if the payment is large enough to cover the transaction
						//Response.Write((Parse((myDataGridItem.Cells[2]).Text)).ToString() +" <= "+(((TextBox)myDataGridItem.FindControl("txtAmount")).Text).ToString());
						if (Convert.ToDouble(Parse((myDataGridItem.Cells[2]).Text)) <= Convert.ToDouble(((TextBox)myDataGridItem.FindControl("txtAmount")).Text))
						{
							SqlCommand cmdUpdate;
							cmdUpdate = new SqlCommand("Update SHIPMENT SET SHIPMENT_SELLER_CLOSED_DATE1 =getDate() WHERE SHIPMENT_ID = '" +strShipment_ID+"'",conn);
							//Response.Write("update");
							cmdUpdate.ExecuteNonQuery();
						}
					}
					iCounter++;
				}
			}
			conn.Close();
			//Response.Write("about to call bind");
			Bind();
		}
    
		// <summary>
		// gets totals and drops them into the footer
		// </summary>
    
		double dbTotalValueOwed = 0.0;
		double dbTotalTransactionValue = 0.0;
		// calculates total for the footer
		protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
					if (e.Item.Cells[4].Text.Equals("0") )
					e.Item.Cells[4].Text="<font  align='center'>-</font>";
				if (e.Item.Cells[5].Text.Equals("0") || e.Item.Cells[5].Text.StartsWith("-"))
					e.Item.Cells[5].Text="<font  align='center'>-</font>";
				dbTotalValueOwed += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VAROWES"));
				dbTotalTransactionValue += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTV"));
				//Response.Write("<br>dbTotalTransactionValue = "+(DataBinder.Eval(e.Item.DataItem, "VARTV")).ToString() );
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[0].Text = "<b> Totals: </b>";
				
				//e.Item.Cells[].Text="-";
				e.Item.Cells[2].Text = "<b> " + String.Format("{0:$#,###}", dbTotalValueOwed) +"</b>";
				e.Item.Cells[3].Text = "<b> " + String.Format("{0:$#,###}", dbTotalTransactionValue) +"</b>";
    
			}
		}
    
		//<summary>
		// Handler for when the user clicks done
		//</summary>
		protected void Navigate_Back(object sender, EventArgs e)
		{
			Response.Redirect("/Creditor/CurrARAPAging.aspx");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
