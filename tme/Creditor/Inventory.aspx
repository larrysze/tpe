<%@ Page language="c#" Codebehind="Inventory.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.Inventory" MasterPageFile="~/MasterPages/Menu.Master"%>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
<div class="DivTitleBarMenu"><span class="Header Color1 Bold">Inventory Summary</span></div>
	<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
<tr>
	<td align="left"><br />
	<asp:button id="btnSendSpotFloor" runat="server" Text="Send to Spot Floor" CssClass="Content Color2" onclick="btnSendSpotFloor_Click" />
	&nbsp;&nbsp;&nbsp;
	<asp:button id="btnExportToExcel" runat="server" Text="Export to Excel" CssClass="Content Color2" onclick="btnExportToExcel_Click" />
	<br /><br />
	</td>

</tr>
		<tr>
			<td align="center">
			
			<asp:datagrid BorderWidth="0" BackColor="#000000" CellSpacing="1" id="dg" runat="server" AutoGenerateColumns="False" OnItemDataBound="KeepRunningSum"
					ShowFooter="True" DataKeyField="ID" CellPadding="2" HorizontalAlign="Center" Width="85%" HeaderStyle-CssClass="LinkNormal Bold OrangeColor"
					AlternatingItemStyle-CssClass="LinkNormal LightGray" ItemStyle-CssClass="LinkNormal DarkGray" CssClass="Content LinkNormal">
                    <FooterStyle CssClass="Content Color4 Bold FooterColor" />
<Columns>
<mbrsc:RowSelectorColumn AllowSelectAll="True"></mbrsc:RowSelectorColumn>
<asp:HyperLinkColumn Text="Edit" DataNavigateUrlField="ID" DataNavigateUrlFormatString="../administrator/Transaction_Details.aspx?Referer=/Creditor/Inventory.aspx&amp;ID={0}" HeaderText="Edit"></asp:HyperLinkColumn>
<asp:BoundColumn DataField="ID" SortExpression="ID ASC" HeaderText="#">
<ItemStyle Wrap="False">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="ORDR_DATE" SortExpression="ORDR_DATE ASC" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}">
<ItemStyle Wrap="False">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARCONTRACT" SortExpression="VARCONTRACT ASC" HeaderText="Product">
<ItemStyle Wrap="False" HorizontalAlign="Left">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="ORDR_MELT" SortExpression="ORDR_MELT ASC" HeaderText="Melt">
<ItemStyle Wrap="False" HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="ORDR_DENS" SortExpression="ORDR_DENS ASC" HeaderText="Density">
<ItemStyle Wrap="False" HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="ORDR_ADDS" SortExpression="ORDR_ADDS ASC" HeaderText="Melt">
<ItemStyle Wrap="False" HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARLABEL" SortExpression="VARLABEL ASC" HeaderText="Warehouse"></asp:BoundColumn>
<asp:BoundColumn DataField="VARPLACE" SortExpression="VARPLACE ASC" HeaderText="Location">
<ItemStyle Wrap="False" HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARWEIGHT" SortExpression="VARWEIGHT ASC" HeaderText="Weight" DataFormatString="{0:#,###}">
<ItemStyle Wrap="False" HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARSELLPRICE" SortExpression="VARSELLPRICE ASC" HeaderText="Our Price">
<ItemStyle HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARBUYPRICE" SortExpression="VARBUYPRICE ASC" HeaderText="Target Price">
<ItemStyle HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
</Columns>
				</asp:datagrid></td>
		</tr>
	</table>
</asp:Content>
