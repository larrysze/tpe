<%@ Page Language="c#" CodeBehind="Month_Summary.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.Month_Summary" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">

	<asp:DataGrid BackColor="#000000" CellSpacing="1" BorderWidth="0" id="dg" runat="server" CssClass="Content LinkNormal" AutoGenerateColumns="False" 
	OnItemDataBound="OnDataGridBind" CellPadding="2" onSortCommand="SortDG" AllowSorting="True" ShowFooter="True" 
	HorizontalAlign="Center" Width="780px">
		<AlternatingItemStyle HorizontalAlign="LEFT" CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
		<ItemStyle HorizontalAlign="Left" CssClass="LinkNormal LightGray"></ItemStyle>
		<HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
		<FooterStyle CssClass="Content Color4 Bold FooterColor" />
		<Columns>
			<asp:TemplateColumn>
				<ItemStyle HorizontalAlign="Right"></ItemStyle>
				<HeaderTemplate>Date
                </HeaderTemplate>
				<ItemTemplate></ItemTemplate>
			</asp:TemplateColumn>
			<asp:BoundColumn DataField="VARWEIGHT" SortExpression="VARWEIGHT ASC" HeaderText="Weight" DataFormatString="{0:###,###}">
				<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
			</asp:BoundColumn>
			<asp:BoundColumn DataField="VARRECEIVABLES" SortExpression="VARRECEIVABLES ASC" HeaderText="Buyer Total"
				DataFormatString="{0:c}">
				<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
			</asp:BoundColumn>
			<asp:BoundColumn DataField="VARPAYABLES" SortExpression="VARPAYABLES ASC" HeaderText="Seller Total"
				DataFormatString="{0:c}">
				<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
			</asp:BoundColumn>
			<asp:BoundColumn DataField="VARFREIGHT" SortExpression="VARFREIGHT ASC" HeaderText="Freight" DataFormatString="{0:c}">
				<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
			</asp:BoundColumn>
			<asp:BoundColumn DataField="VARCOMM" SortExpression="VARCOMM ASC" HeaderText="Commission" DataFormatString="{0:c}">
				<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
			</asp:BoundColumn>
			<asp:BoundColumn DataField="VARINTEREST" SortExpression="VARINTEREST ASC" HeaderText="Interest" DataFormatString="{0:c}">
				<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
			</asp:BoundColumn>
			<asp:BoundColumn DataField="VARMARGIN" SortExpression="VARMARGIN ASC" HeaderText="Margin" DataFormatString="{0:c}">
				<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
			</asp:BoundColumn>
		</Columns>
	</asp:DataGrid>
</asp:Content>
