using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Web.Mail;


namespace localhost.Creditor
{
	/// <summary>
	/// Summary description for Statement.
	/// </summary>
	public partial class Statement : System.Web.UI.Page
	{

        public string EmailBody;
		protected String mailBody;
		protected Single TotalBalance;	
		protected System.TimeSpan days;


		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C"))
			{
				Response.Redirect("../default.aspx");
			}
			if (!IsPostBack)
			{
				// save important variables into viewstate
				//TotalBalance			
				ViewState["Id"] = Request.QueryString["Id"].ToString();				
			
				
				
			}   
			TotalBalance=0;
			Bind();
			lblDate.Text="STATEMENT DATE: "+ DateTime.Today.ToShortDateString();
		}


		protected void bSend_Click(object sender, EventArgs e)
		{
			int style=0;	

			StringBuilder sbHTML = new StringBuilder();	
			
			// BEGIN HEADER 
			sbHTML.Append("<html>");
			sbHTML.Append("<head>");
			sbHTML.Append("	<title> :: The Plastics Exchange - Invoice :: </title>");
			sbHTML.Append("	<body>	");
			sbHTML.Append("<table id=\"Table2\" cellSpacing=\"0\" cellPadding=\"0\" width=\"730\"  border=\"0\" align=\"center\">");
			sbHTML.Append("<tr>");
			sbHTML.Append("<td bgcolor=#000000><img src=/images/1x1.gif width=1></td>");
			sbHTML.Append("<td>");
			sbHTML.Append("	<table id=\"table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\"  border=\"0\" >");
			sbHTML.Append("		<tr>		");
			sbHTML.Append("			<td bgcolor=\"black\"  height=\"1\"></td>");
			sbHTML.Append("		</tr>");
			sbHTML.Append("		<tr>");
			sbHTML.Append("		  	<td valign=\"top\">");
			sbHTML.Append("				<!-- BEGIN OF `TPE LOGO` and `GRADIENT BACKGROUND PIC` -->");
			sbHTML.Append("					<table id=\"AutoNumber1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" border=\"0\">");
			sbHTML.Append("						<tr>");
			sbHTML.Append("							<td width=\"450\" height=\"50\" bgcolor=\"black\">");
			sbHTML.Append("								<a href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/default.aspx\" target=\"_blank\"><IMG SRC=\"http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/Logo_Black.gif\"  alt=\"The Plastics Exchange\" border=\"0\"></a>");
			sbHTML.Append("							</td>");
			sbHTML.Append("							<td width=\"170\" bgcolor=\"#9D9D9D\" height=\"50\" background=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/pics/header_gradient.gif\"></td>");
			sbHTML.Append("							<td width=\"100\" bgcolor=\"#9D9D9D\" height=\"50\"></td>");
			sbHTML.Append("						</tr>");
			sbHTML.Append("					</table>");
			sbHTML.Append("				<!-- END OF `TPE LOGO` and `GRADIENT BACKGROUND PIC` -->");
			sbHTML.Append("			</td>");
			sbHTML.Append("		</tr>");
			sbHTML.Append("		<tr>");
			sbHTML.Append("			<td vAlign=\"top\" width=\"730\">");
			sbHTML.Append("				<!-- BEGIN OF `MENU BAR` -->");
			sbHTML.Append("				<table height=\"25\" cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" border=\"0\">");
			sbHTML.Append("     					<tr>");
			sbHTML.Append("						<td align=\"left\" width=\"730\" background=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/pics/gray_gradient_header.gif\" height=\"25\">");
			sbHTML.Append("							<!-- BEGIN `NAVIGATION BAR` -->");
			sbHTML.Append("							<!-- END NAVIGATION BAR -->	");
			sbHTML.Append("						</td>");
			sbHTML.Append("						<td align=\"right\" valign=\"middle\" width=\"30\" height=\"25\" background=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/pics/gray_gradient_header.gif\">");
			sbHTML.Append("                            		<img src=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/Pics/icons/icon_Phone.gif\" >");
			sbHTML.Append("                        </td>");				
			sbHTML.Append("						<td align=\"right\" valign=\"middle\" width=\"115\" height=\"25\" background=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/pics/gray_gradient_header.gif\">");
			sbHTML.Append("							&nbsp;<font face=\"Verdana, Arial\" size=\"2\" color=\"white\"><b>" + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + " </b></font>&nbsp;");
			sbHTML.Append("						</td>");
			sbHTML.Append("					</tr>");
			sbHTML.Append("				</table>");
			sbHTML.Append("			</td>");
			sbHTML.Append("		</tr>");
			sbHTML.Append("		<tr>");
			sbHTML.Append("			<td>");
			sbHTML.Append("				<!-- BEGIN OF `CONTENT OF THE INVOICE` HERE -->");
			// END HEADER 
            
			// BEGIN CONTENT 

			
			sbHTML.Append("<table border=0 align=center cellspacing=2 cellpadding=2 width=100%>");
			// account info table
			sbHTML.Append("<tr><td align=left colspan='1' width=100%>");
			sbHTML.Append("<TABLE id='tMail' border=0 cellspacing=0 cellpadding=0 align=left width=100%>");		   
			
			sbHTML.Append("<tr><td align='left' colspan='2'><P align='center'><Font size='4'><b>");
			sbHTML.Append(lblNameCompany.Text+"</b></Font></P></td></tr><tr><td align='left' colspan='2'>");
			sbHTML.Append(lblAddress.Text+"</td></tr><tr><td align='left'colspan='2'>");
			sbHTML.Append(lblPlace.Text+"</td></tr><tr><td align='left'colspan='2'>United States</td></tr></TABLE>");
			sbHTML.Append("</td><td align=right colspan='1' width=50% valign=top>");
			sbHTML.Append(lblDate.Text+"</td></tr>");
            // end account info
            sbHTML.Append("<tr><td><br>");
            EmailBody = txtBody.Text;
            sbHTML.Append(EmailBodyHtml(EmailBody));
            sbHTML.Append("</td></tr>");
			sbHTML.Append("<tr><td align='center' colspan='2'><P align='center'><Font size='4'></br><b> Statement of Account from The Plastics Exchange </b></br></Font></P></td></tr>");
			
			// begin main table
			sbHTML.Append("<tr><td align='center' colSpan='2'>");
			sbHTML.Append("<Table  bgcolor='#eeeeee' align='center' width='95%'>");
			sbHTML.Append("<tr bgcolor=#BEB8A2><td STYLE='FONT: 8pt ARIAL'>Order #</td><td STYLE='FONT: 8pt ARIAL'>PO#</td><td STYLE='FONT: 8pt ARIAL'>Product</td><td STYLE='FONT: 8pt ARIAL'>Ship Date</td><td STYLE='FONT: 8pt ARIAL'>Due Date</td><td STYLE='FONT: 8pt ARIAL'>Terms</td><td STYLE='FONT: 8pt ARIAL'>Days Late</td><td STYLE='FONT: 8pt ARIAL'>Open Balance</td></tr>");

			for (int i=0;i<dg.Items.Count;i++)
			{
				if (dg.Items[i].Visible==true)
				{
					
					if (style==0)
					{
						style=1;
					}
					else
					{
						style=0;
					}
					sbHTML.Append("<tr>");

					for (int j=0;j<8;j++)
					{
						
						if (style==0)	
						{
							sbHTML.Append("<td STYLE='FONT: 8pt ARIAL' bgcolor='#D9D7D3'>");	
							sbHTML.Append(dg.Items[i].Cells[j].Text);
							sbHTML.Append("</td>");	

						}
						else 
						{
							sbHTML.Append("<td STYLE='FONT: 8pt ARIAL' >");	
							sbHTML.Append(dg.Items[i].Cells[j].Text);
							sbHTML.Append("</td>");	
						}
						
					}
					sbHTML.Append("</tr>");
					
					
				}	
			}
			
			sbHTML.Append("<tr><td colspan='7'align='right'><b>Totals:</b></td>");
			sbHTML.Append( "<td><b>"+String.Format("{0:c}", TotalBalance)+"</b></td></tr>");
			sbHTML.Append("</Table>");
			sbHTML.Append("</td></tr>");
			sbHTML.Append("<tr><td><BR><BR></td></tr>");
			// end main table 

			
			
			sbHTML.Append("<tr><td colSpan='2' vAlign='top'>");
			sbHTML.Append("<TABLE width='500' align='center' border='0' ><TR><TD valign=top>");
			sbHTML.Append("<TABLE id='tCheck' align='left' border='0' >");
			sbHTML.Append("<tr>");
			sbHTML.Append("<td valign=top><font size='2'><b><i>CHECK REMITTANCE INSTRUCTIONS: </i></b></font></td></tr>");
			sbHTML.Append("<tr><td><font size='1'>The Plastics Exchange</font></td></tr>");
			sbHTML.Append("<tr><td><font size='1'>Department 20-3041</font></td></tr>");
			sbHTML.Append("<tr><td><font size='1'>PO Box 5977</font></td></tr>");
			sbHTML.Append("<tr><td><font size='1'>Carol Stream, IL 60197-5977</td></tr>	");
			sbHTML.Append("</TABLE>");
			sbHTML.Append("</td><td vAlign='top' colSpan='1'>");
			sbHTML.Append("<table id='tTransfer' align='right'>");
			sbHTML.Append("<tr vAlign='top'><td align='right'><font size='2'><b><i>WIRE TRANSFER INSTRUCTIONS: </b></I></font></td></tr>");
			
			sbHTML.Append("<tr><td><font size='1'>American Chartered Bank</font></td></tr>");
			sbHTML.Append("<tr><td><font size='1'>Elk Grove Village, IL 60007</font></td></tr>");
			sbHTML.Append("<tr><td><font size='1'>A.B.A. #071 925 046</font><BR></td></tr>");
			sbHTML.Append("<tr><td><font size='1'>For Futher Credit:</font></td></tr>");
			
			sbHTML.Append("<tr><td><font size='1'>The Plastics Exchange</font></td></tr>");
			sbHTML.Append("<tr><td><font size='1'>Account# 1130245</font></td></tr>");
			sbHTML.Append("</table>");
			sbHTML.Append("</td></tr></table>");
			sbHTML.Append("</TD></TR>");
			
			// close container table
			sbHTML.Append("</TABLE>");
			// END CONTENT
			
			// BEGIN FOOTER
			sbHTML.Append("				<!-- END OF `CONTENT OF THE INVOICE` HERE -->");
			sbHTML.Append("			</td>");
			sbHTML.Append("		</tr>");
			sbHTML.Append("		<tr>");
			sbHTML.Append("				<!-- BEGIN `INVOICE FOOTER` -->");
			sbHTML.Append("			<td bgcolor=\"#464646\" height=\"15\"  align=\"center\" width=\"100%\">");
			sbHTML.Append("				<font face=\"Verdana,Arial\" size=\"1\" color=\"white\">");
			sbHTML.Append("					<b>Address:</b>	 710 North Dearborn - Chicago, IL - 60610 - USA  |  ");
			sbHTML.Append("					<b>Toll Free:</b> " + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + " |  ");
			sbHTML.Append("					<b>Fax:</b> " + ConfigurationSettings.AppSettings["FaxNumber"].ToString() + "</font>");
			sbHTML.Append("					</center>");
			sbHTML.Append(" 			</td>");
			sbHTML.Append("	        </tr>");
			sbHTML.Append("		<!-- BEGIN CLOSE CONTAINER TABLE -->");
			sbHTML.Append("		<tr>");
			sbHTML.Append("			<td bgcolor=\"black\" width=\"100%\" height=\"2\"></td>");
			sbHTML.Append("      	</tr>");
			sbHTML.Append("	</table>");
			sbHTML.Append("</td>");
			sbHTML.Append("<td bgcolor=#000000><img src=/images/1x1.gif width=1></td>");
			sbHTML.Append("</table>");
			sbHTML.Append("</body>");
			sbHTML.Append("</html>");
			// END FOOTER
			
			System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
			mail.From=ddlFrom.SelectedValue.ToString();
			mail.To=ddlTo.SelectedValue.ToString();
			//mail.To="wanderley@theplasticsexchange.com";
			mail.Cc=Application["strEmailOwner"].ToString();
			mail.Body=sbHTML.ToString();
			mail.Subject="Statement of Account from The Plastics Exchange";
			mail.BodyFormat = MailFormat.Html;
			//SmtpMail.SmtpServer="caesar";
			
			TPE.Utility.EmailLibrary.Send(Context,mail);
			Response.Write(sbHTML.ToString());
					
			Response.Redirect("AccountSummary.aspx?Id="+Request.QueryString["Id"].ToString());
            
		}

        private string EmailBodyHtml(string EmailBody)
        {
            EmailBody = EmailBody.Replace("\r\n", "<br>");
            return EmailBody;
        }

		private void Bind()
		{
			SqlDataAdapter dadContent;
			SqlDataAdapter dadContent2;
			DataSet dstContent2;
			DataSet dstContent;
			SqlConnection conn;
			SqlConnection conn2;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn2 = new SqlConnection(Application["DBConn"].ToString());
			try
			{
				conn.Open();
				conn2.Open();

           
				StringBuilder sbSQL = new StringBuilder();
				StringBuilder sbSQL2 = new StringBuilder();


				sbSQL.Append(" Select Company.Comp_Name,PERS_LAST_NAME,PERS_FRST_NAME,PERS_ID,PERS_MAIL,PLAC_ZIP,LOCL_STAT,LOCL_CITY,PLAC_ADDR_ONE from Person,Place,Company,Locality,Shipment WHERE ");
				sbSQL.Append(" COMP_ID='"+ ViewState["Id"].ToString() +"' and ");
				sbSQL.Append(" Person.PERS_COMP=Company.COMP_ID  and ");
				sbSQL.Append(" Place.PLAC_COMP=Company.COMP_ID and PLAC_TYPE='h' and ");
				sbSQL.Append(" PLAC_LOCL= LOCL_ID and SHIPMENT_BUYER_CLOSED_DATE is null ");
				sbSQL.Append(" and  PERS_ID=SHIPMENT_BUYR group by Company.Comp_Name,PERS_ID,PERS_MAIL,PLAC_ZIP,LOCL_STAT,LOCL_CITY,LOCL_CTRY,PLAC_ADDR_ONE,PERS_LAST_NAME, PERS_FRST_NAME ");

				
				dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
				dstContent = new DataSet();
				dadContent.Fill(dstContent);

			
				Object [] array;

			
				if (!dstContent.Tables[0].Rows.Count.ToString().Equals("0"))
				{ 
			
				
					array=dstContent.Tables[0].Rows[0].ItemArray;
			
					lblNameCompany.Text= array[0].ToString();  
					ddlTo.Items.Add(new ListItem(array[4].ToString(),array[4].ToString()));
					//ddlTo.Items.Add(new ListItem(array[2].ToString()+" "+array[1].ToString() ,array[4].ToString()));
					ddlTo.Items.Add(new ListItem("Helio","helio@ThePlasticsExchange.com"));
					ddlTo.Items.Add(new ListItem("Mike","Mike@ThePlasticsExchange.com"));//"Mike@ThePlasticExchange.com"));
					ddlTo.Items.Add(new ListItem("Stuart","Stuart@ThePlasticsExchange.com"));
						
			
					lblAddress.Text=array[8].ToString();
					lblPlace.Text=array[7].ToString()+", "+array[6].ToString()+" - "+array[5].ToString();
				
					sbSQL2.Append("SELECT ");
					sbSQL2.Append("CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR) AS ID, ");
					sbSQL2.Append("O.ORDR_PROD_SPOT , ");
					sbSQL2.Append("O.ORDR_MELT, ");
					sbSQL2.Append("O.ORDR_DENS, ");
					sbSQL2.Append("O.ORDR_ADDS, ");
					sbSQL2.Append("VARDATETAKEN=CONVERT(VARCHAR,S.SHIPMENT_DATE_TAKE,101), ");
					sbSQL2.Append("S.SHIPMENT_BUYER_TERMS, ");
					sbSQL2.Append("S.SHIPMENT_PO_NUM, ");
					sbSQL2.Append("DAYSSOFAR=DATEDIFF(DAY, DATEADD(d,CAST(SHIPMENT_BUYER_TERMS AS INTEGER), CAST(SHIPMENT_DATE_TAKE AS DATETIME)), getdate()) ,");
					sbSQL2.Append(" (SHIPMENT_BUYR_PRCE )*(ISNULL(S.SHIPMENT_WEIGHT,S.SHIPMENT_QTY*(S.SHIPMENT_SIZE)))-ISNULL(P.PAY_AMNT,0) AS OPEN_BALANCE ");
					sbSQL2.Append("FROM SHIPMENT S, ORDERS O, (select P.PAY_SHIPMENT, P.PAY_PERS, P.PAY_ORDR, SUM(P.PAY_AMNT) PAY_AMNT from PAYMENT P group by P.PAY_SHIPMENT, P.PAY_PERS, P.PAY_ORDR) P ");
					sbSQL2.Append("WHERE S.SHIPMENT_ORDR_ID = O.ORDR_ID and S.SHIPMENT_BUYER_CLOSED_DATE is  null ");
					//sbSQL2.Append("AND DATEDIFF(DAY, DATEADD(d,CAST(SHIPMENT_BUYER_TERMS AS INTEGER), CAST(SHIPMENT_DATE_TAKE AS DATETIME)), getdate()) >0");
					sbSQL2.Append("AND P.PAY_ORDR =* S.SHIPMENT_ORDR_ID AND P.PAY_SHIPMENT =* S.SHIPMENT_ID AND P.PAY_PERS =* S.SHIPMENT_BUYR ");
					sbSQL2.Append("AND S.SHIPMENT_DATE_TAKE is not null AND (SELECT PERS_COMP from PERSON WHERE PERS_ID=SHIPMENT_BUYR) = '"+ViewState["Id"].ToString()+"' ORDER BY ID" );
				
					dadContent2 = new SqlDataAdapter(sbSQL2.ToString(),conn2);
					dstContent2 = new DataSet();
					dadContent2.Fill(dstContent2);
					dg.DataSource=dstContent2;
					dg.DataBind();


		
				}
			}
			finally
			{
				conn2.Close();
				conn.Close();
			}

			if (dstContent.Tables[0].Rows.Count.ToString().Equals("0"))
				Response.Redirect("No_Statement.aspx?Id="+ViewState["Id"].ToString());

		
    
		}


		protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
			
		

			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				
				
				
				if (!Convert.IsDBNull(DataBinder.Eval(e.Item.DataItem,"VARDATETAKEN")))
				{								
					System.DateTime dueDate=Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "VARDATETAKEN")).AddDays(Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "SHIPMENT_BUYER_TERMS")));
					e.Item.Cells[4].Text=dueDate.ToShortDateString();
					System.TimeSpan daysLate=System.DateTime.Today.Subtract(dueDate);
					e.Item.Cells[6].Text= daysLate.Days.ToString();
					if (!Convert.IsDBNull(DataBinder.Eval(e.Item.DataItem,"OPEN_BALANCE")))
						TotalBalance =TotalBalance+ Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "OPEN_BALANCE"));

				}
				if ((DataBinder.Eval(e.Item.DataItem,"ORDR_MELT")).ToString().Trim().Length >1 || (DataBinder.Eval(e.Item.DataItem,"ORDR_DENS")).ToString().Trim().Length >1 || (DataBinder.Eval(e.Item.DataItem,"ORDR_ADDS")).ToString().Trim().Length >1)
				{
					e.Item.Cells[2].Text +="<BR>";
					if ((DataBinder.Eval(e.Item.DataItem,"ORDR_MELT")).ToString().Trim().Length >1)
					{
						e.Item.Cells[2].Text +=(DataBinder.Eval(e.Item.DataItem,"ORDR_MELT")).ToString()+" melt";
					}
					if ((DataBinder.Eval(e.Item.DataItem,"ORDR_DENS")).ToString().Trim().Length >1)
					{
						e.Item.Cells[2].Text +=" - " + (DataBinder.Eval(e.Item.DataItem,"ORDR_DENS")).ToString()+" density";
					}
					if ((DataBinder.Eval(e.Item.DataItem,"ORDR_ADDS")).ToString().Trim().Length >1)
					{
						e.Item.Cells[2].Text +=" - " + (DataBinder.Eval(e.Item.DataItem,"ORDR_ADDS")).ToString();
					}

				}
				
			}

			else if (e.Item.ItemType == ListItemType.Footer)
			{		
				e.Item.Cells[6].Text = "<b>Totals:</b>" ;
				e.Item.Cells[7].Text = "<b>"+String.Format("{0:c}", TotalBalance)+"</b>";					
						

			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
