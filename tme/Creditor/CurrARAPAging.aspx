<%@ Page Language="c#" Codebehind="CurrARAPAging.aspx.cs" AutoEventWireup="True"
    Inherits="localhost.Creditor.CurrARAPAging" MasterPageFile="~/MasterPages/Menu.Master"
    Title="Credit Management" %>

<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
    <table align="center" cellspacing="0" cellpadding="0" border="0" width="100%">
        <tbody>
            <tr>
                <td>
                    <br />
                    <center>
                        <span class="Header Color2">Current </span>
                        <%string etat;
         etat = Request["etat"];
         if (etat == "P")
         {%>
                        <span class="Header Color2">Payables </span><span class="LinkNormal">
                            <asp:HyperLink ID="Receivables" runat="server" Text="Show Receivables" NavigateUrl="CurrARAPAging.aspx?etat=R"></asp:HyperLink></span>
                        <%}
         else
         {%>
                        <span class="Header Color2">Receivables </span><span class="LinkNormal">
                            <asp:HyperLink ID="Payables1" runat="server" Text="Show Payables" NavigateUrl="CurrARAPAging.aspx?etat=P"
                                color="#BEB8A2" Font-Size="11px"></asp:HyperLink></span>
                        <%}%>
                    </center>
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:DataGrid BackColor="#000000" BorderWidth="0" CellSpacing="1" ID="dg" runat="server"
                        CssClass="DataGrid" ItemStyle-CssClass="DataGridRow" AlternatingItemStyle-CssClass="DataGridRow_Alternate"
                        HeaderStyle-CssClass="DataGridHeader" AutoGenerateColumns="False" AllowSorting="True"
                        OnItemDataBound="OnDataGridBind" OnSortCommand="SortDG" CellPadding="2" ShowFooter="True"
                        HorizontalAlign="Center">
                        <AlternatingItemStyle CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
                        <ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
                        <HeaderStyle CssClass="LinkNormal OrangeColor"></HeaderStyle>
                        <FooterStyle CssClass="Content Bold Color4 FooterColor"></FooterStyle>
                        
                        <Columns>
                            <asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/administrator/Company_Info.aspx?Id={0}"
                                DataTextField="COMP_NAME" SortExpression="COMP_NAME ASC" HeaderText="Company Name">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:HyperLinkColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Image1" runat="server" ImageUrl="/Pics/icons/comment.gif" CommandName="Comments"
                                        ToolTip="Company Comments"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:HyperLinkColumn Text="Transactions" DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="AccountSummary.aspx?Id={0}&amp;fct=all_hist">
                            </asp:HyperLinkColumn>
                            <asp:BoundColumn DataField="COMP_AVRG_PAY_DAYS" SortExpression="COMP_AVRG_PAY_DAYS ASC"
                                HeaderText="Terms">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="COMP_CREDIT_LIMIT" SortExpression="COMP_CREDIT_LIMIT ASC"
                                HeaderText="Credit Limit" DataFormatString="{0:c}"></asp:BoundColumn>
                            <asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="AccountSummary.aspx?Id={0}&amp;fct=out"
                                DataTextField="FO" SortExpression="FO ASC" HeaderText="Outstanding" DataTextFormatString="{0:c}"
                                ItemStyle-CssClass="blackcontent">
                                <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            </asp:HyperLinkColumn>
                            <asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="AccountSummary.aspx?Id={0}&amp;fct=com"
                                DataTextField="COMMINVTRY" SortExpression="COMMINVTRY ASC" HeaderText="Committed Inventory"
                                DataTextFormatString="{0:c}">
                                <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            </asp:HyperLinkColumn>
                            <asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="AccountSummary.aspx?Id={0}&amp;fct=cur"
                                DataTextField="OCURRENT" SortExpression="OCURRENT ASC" HeaderText="Current" DataTextFormatString="{0:c}">
                                <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            </asp:HyperLinkColumn>
                            <asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="AccountSummary.aspx?Id={0}&amp;fct=030"
                                DataTextField="O30" SortExpression="O30 ASC" HeaderText="1-30" DataTextFormatString="{0:c}">
                                <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            </asp:HyperLinkColumn>
                            <asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="AccountSummary.aspx?Id={0}&amp;fct=03160"
                                DataTextField="O3160" SortExpression="O3160 ASC" HeaderText="30-60" DataTextFormatString="{0:c}">
                                <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            </asp:HyperLinkColumn>
                            <asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="AccountSummary.aspx?Id={0}&amp;fct=06190"
                                DataTextField="O6190" SortExpression="O6190 ASC" HeaderText="60-90" DataTextFormatString="{0:c}">
                                <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            </asp:HyperLinkColumn>
                            <asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="AccountSummary.aspx?Id={0}&amp;fct=90"
                                DataTextField="O90ORMORE" SortExpression="O90ORMORE ASC" HeaderText="&gt;90"
                                DataTextFormatString="{0:c}">
                                <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            </asp:HyperLinkColumn>
                            <asp:TemplateColumn>
                                <ItemStyle Wrap="False"></ItemStyle>
                                <HeaderTemplate>
                                    Add Payments
                                </HeaderTemplate>
                                <ItemTemplate>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="COMP_ID"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
