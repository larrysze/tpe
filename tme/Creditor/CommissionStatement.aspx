<%@ Page language="c#" Codebehind="CommissionStatement.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.CommissionStatement" %>
<html>
<body>
<form runat="server">
	<table width='100%' border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="50">
				<img src="/images/email/tpelogo.gif">
			</td>
			<td align='left'><FONT face="Arial Black" size="5"> The<font color="red">Plastics</font>Exchange<font color="red">.</font>com</FONT>
			</td>
		</tr>
	</table>
	<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
		<tr>
			<td align="left" vAlign="top" colSpan="1" rowSpan="1">
				<P align="center">
					<span>
						<FONT size="5"><U>Commission Statement</U></FONT>
					</span></P>
			</td>
		</tr>
		<tr>
			<STRONG></STRONG>
		<tr>
			<td align="left" height="37"><span class="PageHeader1">
					<P align="left"><BR>
						<asp:label id="Label2" runat="server" Font-Size="Medium">Broker:</asp:label><STRONG>&nbsp;</STRONG>
						<asp:label id="lblBrokerName" runat="server" Font-Bold="True" Font-Size="Medium"></asp:label><STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
							&nbsp; &nbsp;</STRONG>
						<asp:label id="lbl" runat="server" Font-Size="Medium">Month:</asp:label><STRONG>&nbsp;
						</STRONG>
						<asp:label id="lblMonth" runat="server" Font-Bold="True" Font-Size="Medium"></asp:label></P>
				</span></td>
		</tr>
		<tr>
			<td align="left">
				<DIV align="left">
					<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD>
								<P align="left"><BR>
									<FONT size="4"><U>Transaction&nbsp;Paid&nbsp;on last&nbsp;Statement</U></FONT></P>
							</TD>
						</TR>
						<TR>
							<TD>
								<DIV align="left">
									<asp:datagrid id="dgLastMonth" runat="server" CellPadding="2" ShowFooter="True" OnItemDataBound="KeepRunningSumLM"
										AutoGenerateColumns="False" BorderColor="Black" BorderWidth="1px">
										<AlternatingItemStyle CssClass="DataGridRow_Alternate"></AlternatingItemStyle>
										<ItemStyle CssClass="DataGridRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="DataGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="CUSTOMER" HeaderText="Customer">
												<HeaderStyle Wrap="False"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="TRANS_NUMBER" HeaderText="Transaction">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
												<FooterStyle HorizontalAlign="Right"></FooterStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="WEIGHT" HeaderText="Weight" DataFormatString="{0:#,###}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="BUY" HeaderText="Buy" DataFormatString="{0:#,###.0000}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="SELL" HeaderText="Sell" DataFormatString="{0:#,###.0000}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FREIGHT" HeaderText="Freight" DataFormatString="{0:#,###.00}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="LOGISTICS_COST" HeaderText="Logistics" DataFormatString="{0:#,###.00}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DAYS_PAST_DUE" HeaderText="Days Past Due">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="CAPITAL_CHARGE" HeaderText="Capital Charge" DataFormatString="{0:#,###.00}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="GROSS_PROFIT" HeaderText="Gross Profit" DataFormatString="{0:#,###.00}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
												<FooterStyle HorizontalAlign="Right"></FooterStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="COMMISSION" HeaderText="Commission" DataFormatString="{0:#,###.00}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
												<FooterStyle HorizontalAlign="Right"></FooterStyle>
											</asp:BoundColumn>
										</Columns>
									</asp:datagrid></DIV>
							</TD>
						</TR>
						<TR>
							<TD>&nbsp;</TD>
						</TR>
						<TR>
							<TD>
								<P align="left"><FONT size="4"><U>Transaction&nbsp;Being Paid</U></FONT></P>
							</TD>
						</TR>
						<TR>
							<TD>
								<DIV align="left"><asp:datagrid id="dgCurrent" runat="server" CellPadding="2" ShowFooter="True" OnItemDataBound="KeepRunningSum"
										AutoGenerateColumns="False" BorderColor="Black" BorderWidth="1px">
										<AlternatingItemStyle CssClass="DataGridRow_Alternate"></AlternatingItemStyle>
										<ItemStyle CssClass="DataGridRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="DataGridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="CUSTOMER" HeaderText="Customer">
												<HeaderStyle Wrap="False"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="TRANS_NUMBER" HeaderText="Transaction">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
												<FooterStyle HorizontalAlign="Right"></FooterStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="WEIGHT" HeaderText="Weight" DataFormatString="{0:#,###}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="BUY" HeaderText="Buy" DataFormatString="{0:#,###.0000}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="SELL" HeaderText="Sell" DataFormatString="{0:#,###.0000}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FREIGHT" HeaderText="Freight" DataFormatString="{0:#,###.00}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="LOGISTICS_COST" HeaderText="Logistics" DataFormatString="{0:#,###.00}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DAYS_PAST_DUE" HeaderText="Days Past Due">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="CAPITAL_CHARGE" HeaderText="Capital Charge" DataFormatString="{0:#,###.00}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="GROSS_PROFIT" HeaderText="Gross Profit" DataFormatString="{0:#,###.00}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
												<FooterStyle HorizontalAlign="Right"></FooterStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="COMMISSION" HeaderText="Commission" DataFormatString="{0:#,###.00}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
												<FooterStyle HorizontalAlign="Right"></FooterStyle>
											</asp:BoundColumn>
										</Columns>
									</asp:datagrid></DIV>
							</TD>
						</TR>
						<TR>
							<TD>&nbsp;</TD>
						</TR>
						<TR>
							<TD>
								<P align="left"><FONT size="4"><U>Transaction&nbsp;To Be Paid</U></FONT></P>
							</TD>
						</TR>
						<TR>
							<TD>
								<asp:datagrid id="dgOpen" runat="server" CellPadding="2" ShowFooter="True" OnItemDataBound="KeepRunningSumOpen"
									AutoGenerateColumns="False" BorderColor="Black" BorderWidth="1px">
									<AlternatingItemStyle CssClass="DataGridRow_Alternate"></AlternatingItemStyle>
									<ItemStyle CssClass="DataGridRow"></ItemStyle>
									<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="DataGridHeader"></HeaderStyle>
									<Columns>
										<asp:BoundColumn DataField="CUSTOMER" HeaderText="Customer">
											<HeaderStyle Wrap="False"></HeaderStyle>
											<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="TRANS_NUMBER" HeaderText="Transaction">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<FooterStyle HorizontalAlign="Right"></FooterStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="WEIGHT" HeaderText="Weight" DataFormatString="{0:#,###}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="BUY" HeaderText="Buy" DataFormatString="{0:#,###.0000}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="SELL" HeaderText="Sell" DataFormatString="{0:#,###.0000}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="FREIGHT" HeaderText="Freight" DataFormatString="{0:#,###.00}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="LOGISTICS_COST" HeaderText="Logistics" DataFormatString="{0:#,###.00}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="DAYS_PAST_DUE" HeaderText="Days Past Due">
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="CAPITAL_CHARGE" HeaderText="Capital Charge" DataFormatString="{0:#,###.00}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="GROSS_PROFIT" HeaderText="Gross Profit" DataFormatString="{0:#,###.00}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<FooterStyle HorizontalAlign="Right"></FooterStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="COMMISSION" HeaderText="Commission" DataFormatString="{0:#,###.00}">
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<FooterStyle HorizontalAlign="Right"></FooterStyle>
										</asp:BoundColumn>
									</Columns>
								</asp:datagrid></TD>
						</TR>
					</TABLE>
				</DIV>
				<DIV align="left">&nbsp;</DIV>
			</td>
		</tr>
	</table>
</form>
</body>
</html>