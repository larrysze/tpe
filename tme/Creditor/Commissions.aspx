<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Page language="c#" Codebehind="Commissions.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.Commission" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<%@ Register TagPrefix="mbdb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.DefaultButtons" %>


<asp:Content runat="server" ContentPlaceHolderID="cphMain">
<div class="DivTitleBarMenu"><span class="Header Color1 Bold">Commission Payment</span></div>
	<table cellSpacing="0" cellPadding="0" width="780" border="0">
		<tr>
			<td>
		</tr>
		<tr>
		<tr>
			<td align="left"><span class="PageHeader1">
					<P align="center"><BR>
						<asp:Label id="lblMonth" runat="server"><span class="Content Color2">Month:</span></asp:Label>&nbsp;
						<asp:DropDownList CssClass="InputForm" id="ddlMonth" runat="server" AutoPostBack="True" onselectedindexchanged="ddlMonth_SelectedIndexChanged"></asp:DropDownList><BR>
						<BR>
					</P>
				</span></td>
		</tr>
		<tr>
			<td align="center"><asp:datagrid CellSpacing="1" BorderWidth="0" BackColor="#000000" Width="500"  id="dg" runat="server" AutoGenerateColumns="False" OnItemDataBound="KeepRunningSum"
					ShowFooter="True" CellPadding="2" HorizontalAlign="Center" HeaderStyle-CssClass="DataGridHeader" AlternatingItemStyle-CssClass="DataGridRow_Alternate"
					ItemStyle-CssClass="DataGridRow" CssClass="DataGrid">
					<AlternatingItemStyle CssClass="Content Bold Color2 DarkGray"></AlternatingItemStyle>
					<ItemStyle CssClass="Content Bold Color2 LightGray"></ItemStyle>
					<HeaderStyle CssClass="Content Bold Color2 OrangeColor"></HeaderStyle>
					<FooterStyle CssClass="Content Bold Color4 FooterColor" />
					<Columns>
						<asp:BoundColumn Visible="False" DataField="BROKER_ID" SortExpression="BROKER_ID ASC" HeaderText="Broker ID">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="BROKER_NAME" SortExpression="BROKER_NAME ASC" HeaderText="Broker Name">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="BROKER_COMMISSION" SortExpression="BROKER_COMMISSION ASC" HeaderText="Commission"
							DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
							<FooterStyle HorizontalAlign="Right"></FooterStyle>
						</asp:BoundColumn>
						<asp:TemplateColumn>
							<ItemTemplate>
								<asp:Button id="Button1" CssClass="Content Color2" runat="server" Text="Preview Statement"></asp:Button>
							</ItemTemplate>
						</asp:TemplateColumn>
					</Columns>
				</asp:datagrid><br /></td>
		</tr>
	</table>
</asp:Content>
