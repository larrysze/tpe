<%@ Page Language="c#" Codebehind="AccountAllSummary.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.AccountAllSummary" MasterPageFile="~/MasterPages/Menu.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphCssLink">
    <style type="text/css">
    .style1 { FONT-WEIGHT: bold; FONT-SIZE: 14px }
	.style5 { FONT-SIZE: medium }
    </style>
</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
    <table width="780px" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td height="18">
                <div class="DivTitleBarMenu">
                    <span class="Header Bold Color1">Open Transactions</span></div>
                <%
                    if (fct == "all_hist")
                    {%>
                <%}
                %>
            </td>
        </tr>
        <tr>
            <td align="left">
                <span class="Content Color2">This is a list of all open transactions</span></td>
        </tr>
        <tr>
            <td height="26">
                <div>
                    <asp:RadioButton ID="rdbReceivables" runat="server" AutoPostBack="True" Text="Show Receivables" Checked="True" GroupName="Show" CssClass="Content Bold Color2" OnCheckedChanged="rdbReceivables_CheckedChanged"></asp:RadioButton>&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                    <asp:RadioButton ID="rdbPayables" runat="server" AutoPostBack="True" Text="Show Payables" GroupName="Show" CssClass="Content Bold Color2" OnCheckedChanged="rdbPayables_CheckedChanged"></asp:RadioButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;
                    <asp:Label CssClass="Content Color2" ID="lblTransaction" runat="server">Transactions:</asp:Label>
                    <asp:DropDownList CssClass="InputForm" ID="ddlTransaction" runat="server" AutoPostBack="True" Width="178px" OnSelectedIndexChanged="ddlTransaction_SelectedIndexChanged">
                        <asp:ListItem Value="030">0-30 Days Late</asp:ListItem>
                        <asp:ListItem Value="03160">30-60 Days Late</asp:ListItem>
                        <asp:ListItem Value="06190">60-90 Days Late</asp:ListItem>
                        <asp:ListItem Value="90">More than 90 Days Late</asp:ListItem>
                        <asp:ListItem Value="all_hist" Selected="True">All Transactions</asp:ListItem>
                    </asp:DropDownList></div>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnlPayables">
                    <div class="DivTitleBarMenu" style="width: 780px">
                        <asp:Label CssClass="Header Bold Color1" ID="lblPayables" runat="server" Text="Payables"></asp:Label></div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlReceivables">
                    <div class="DivTitleBarMenu" style="width: 780px">
                        <asp:Label CssClass="Header Bold Color1" ID="lblReceivables" runat="server" Text="Receivables"></asp:Label></div>
                    <br />
                </asp:Panel>
                <asp:DataGrid CellSpacing="1" BackColor="#000000" BorderWidth="0" ID="dg" runat="server" Width="100%" HorizontalAlign="Center" CellPadding="2" ShowFooter="True" OnSortCommand="SortDG" OnItemDataBound="OnDataGridBind" AllowSorting="True" AutoGenerateColumns="False" CssClass="Content LinkNormal">
                    <AlternatingItemStyle CssClass="LinkNormal LightGray"></AlternatingItemStyle>
                    <ItemStyle CssClass="LinkNormal DarkGray"></ItemStyle>
                    <HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
                    <FooterStyle CssClass="Content Color4 Bold FooterColor" />
                    <Columns>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                -
                            </HeaderTemplate>
                            <ItemTemplate>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="VARCOMPANYNAME ASC" HeaderText="Company">
                            <ItemTemplate>
                                <asp:Label runat="server"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox runat="server"></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TRANS_ID" SortExpression="TRANS_ID ASC" HeaderText="Transaction #">
                            <ItemStyle Wrap="False" HorizontalAlign="Left" CssClass="Content" Font-Bold="false"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SHIPMENT_WEIGHT" SortExpression="SHIPMENT_WEIGHT ASC" HeaderText="Weight" DataFormatString="{0:#,##}">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="SHIPMENT_BUYER_TERMS" SortExpression="SHIPMENT_BUYER_TERMS ASC" HeaderText="Terms (Day)">
                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="VARDDATE" SortExpression="VARDDATE ASC" HeaderText="Due Date" DataFormatString="{0:MM/dd/yyyy}">
                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SHIPMENT_BUYER_CLOSED_DATE" SortExpression="SHIPMENT_BUYER_CLOSED_DATE ASC" HeaderText="Date Paid" DataFormatString="{0:MM/dd/yyyy}">
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="VARDATEDIFF" SortExpression="VARDATEDIFF ASC" HeaderText="Days Past Due">
                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SELL_PRICE" SortExpression="SELL_PRICE ASC" HeaderText="Sell Price">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Outstanding" SortExpression="Outstanding ASC" HeaderText="Value" DataFormatString="{0:c}">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TOTAL" SortExpression="TOTAL ASC" HeaderText="Total Received" DataFormatString="{0:c}">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn Visible="False">
                            <HeaderTemplate>
                                -
                            </HeaderTemplate>
                            <ItemTemplate>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid><br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:DataGrid BackColor="#000000" CellSpacing="1" BorderWidth="0" ID="dg2" runat="server" Width="100%" HorizontalAlign="Center" CellPadding="2" ShowFooter="True" OnSortCommand="SortDG2" OnItemDataBound="OnDataGridBind2" AllowSorting="True" AutoGenerateColumns="False" CssClass="Content LinkNormal">
                    <AlternatingItemStyle CssClass="LinkNormal LightGray"></AlternatingItemStyle>
                    <ItemStyle CssClass="LinkNormal DarkGray"></ItemStyle>
                    <HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
                    <FooterStyle CssClass="Content Bold Color4 FooterColor" />
                    <Columns>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                -
                            </HeaderTemplate>
                            <ItemTemplate>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="VARCOMPANYNAME ASC" HeaderText="Company">
                            <ItemTemplate>
                                <asp:Label runat="server"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox runat="server"></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TRANS_ID" SortExpression="TRANS_ID ASC" HeaderText="Transaction #">
                            <ItemStyle Wrap="False" HorizontalAlign="Left" CssClass="Content" Font-Bold="false"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SHIPMENT_WEIGHT" SortExpression="SHIPMENT_WEIGHT ASC" HeaderText="Weight" DataFormatString="{0:#,##}">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="SHIPMENT_BUYER_TERMS" SortExpression="SHIPMENT_BUYER_TERMS ASC" HeaderText="Terms (Day)">
                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="VARDDATE" SortExpression="VARDDATE ASC" HeaderText="Due Date" DataFormatString="{0:MM/dd/yyyy}">
                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SHIPMENT_SELLER_CLOSED_DATE1" SortExpression="SHIPMENT_SELLER_CLOSED_DATE1 ASC" HeaderText="Date Paid" DataFormatString="{0:MM/dd/yyyy}">
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="VARDATEDIFF" SortExpression="VARDATEDIFF ASC" HeaderText="Days Past Due">
                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="BUY_PRICE" SortExpression="BUY_PRICE ASC" HeaderText="Buy Price">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Outstanding" SortExpression="Outstanding ASC" HeaderText="Value" DataFormatString="{0:c}">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TOTAL" SortExpression="TOTAL ASC" HeaderText="Total Paid Out" DataFormatString="{0:c}">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn Visible="False">
                            <HeaderTemplate>
                                -
                            </HeaderTemplate>
                            <ItemTemplate>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblMessage" runat="server">There are no transactions listed that match this request!</asp:Label></td>
        </tr>
    </table>
</asp:Content>
