<%@ Page Language="c#" CodeBehind="Edit_Payments.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.Edit_Payments" MasterPageFile="~/MasterPages/Menu.Master"%>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
	<div style="width:780px"><center>
	    <br>
		<asp:Button CssClass="Content Color2" id="btnCloseTransaction" runat="server" Text="Close transaction" Width="123px" onclick="btnCloseTransaction_Click"></asp:Button>&nbsp;<asp:button CssClass="Content Color2" id="Button1" onclick="Add_New" runat="server" Text="Add Payment"></asp:button><BR>
		<BR>
	</center>
	<asp:datagrid BackColor="#000000" CellSpacing="1" BorderWidth="0" id="dg" runat="server" AutoGenerateColumns="false" ShowFooter="True" EditItemStyle-BackColor="Silver"
		Font-Size="9pt" horizontalalign="Center" CellPadding="2" OnDeleteCommand="DG_Delete" OnUpdateCommand="DG_Update"
		OnCancelCommand="DG_Cancel" OnEditCommand="DG_Edit" Width="770px" HeaderStyle-CssClass="LinkNormal Bold OrangeColor"
		AlternatingItemStyle-CssClass="LinkNormal DarkGray" ItemStyle-CssClass="LinkNormal LightGray" FooterStyle-CssClass="Content Color4 Bold FooterColor">
		<Columns>
			<asp:editcommandcolumn edittext="Edit" canceltext="Cancel" updatetext="Update" />
			<asp:buttoncolumn text="Delete" commandname="Delete" />
			<asp:BoundColumn DataField="PAY_ID" HeaderText="Payment Number" ReadOnly="True" ItemStyle-Wrap="false"
				ItemStyle-HorizontalAlign="center" />
			<asp:BoundColumn DataField="PAY_ORDR" HeaderText="Order Number" ReadOnly="True" ItemStyle-Wrap="false"
				ItemStyle-HorizontalAlign="left" />
			<asp:BoundColumn DataField="COMPANY" HeaderText="Company" ReadOnly="True" ItemStyle-Wrap="false"
				ItemStyle-HorizontalAlign="center" />
			<asp:BoundColumn DataField="PAY_DATE" DataFormatString="{0:MM/dd/yyyy}" ReadOnly="false" HeaderText="Date"
				ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
			<asp:BoundColumn DataField="PAY_AMNT" HeaderText="Amount" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
			<asp:BoundColumn DataField="PAY_CMNT" HeaderText="Comment" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
		</Columns>
	</asp:datagrid>
	<center><br>
		<br>
		<br>
		<asp:button CssClass="Content Color2" id="Button2" onclick="Navigate_Back" runat="server" Text="Return To Transaction Summary"></asp:button></center>
	<br>
	<br>
	<br>
	<br>
	<br>
	</div>
</asp:Content>

