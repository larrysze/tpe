//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.42
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace localhost.Creditor {
    
    public partial class AccountAllSummary {
        protected System.Web.UI.WebControls.RadioButton rdbReceivables;
        protected System.Web.UI.WebControls.RadioButton rdbPayables;
        protected System.Web.UI.WebControls.Label lblTransaction;
        protected System.Web.UI.WebControls.DropDownList ddlTransaction;
        protected System.Web.UI.WebControls.Panel pnlPayables;
        protected System.Web.UI.WebControls.Label lblPayables;
        protected System.Web.UI.WebControls.Panel pnlReceivables;
        protected System.Web.UI.WebControls.Label lblReceivables;
        protected System.Web.UI.WebControls.DataGrid dg;
        protected System.Web.UI.WebControls.DataGrid dg2;
        protected System.Web.UI.WebControls.Label lblMessage;
        public new localhost.MasterPages.Menu Master {
            get {
                return ((localhost.MasterPages.Menu)(base.Master));
            }
        }
    }
}
