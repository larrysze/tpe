using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using TPE.Utility;


namespace localhost.Creditor
{
	// Summary description for Edit_Payments.
	
	public partial class Edit_Payments : System.Web.UI.Page
	{
	
	/************************************************************************
	*   1. File Name       :Creditor\Edit_Payments.aspx                     *
	*   2. Description     :Transaction summary                             *
	*   3. Modification Log:                                                *
	*     Ver No.       Date          Author             Modification       *
	*   -----------------------------------------------------------------   *
	*      1.00      3-2-2004      Zach                    Comment          *
	*      1.01		 8-12-2004     Alexis                                   *                        *
	************************************************************************/

		public void Page_Load(object sender, EventArgs e)
		{
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C"))
			{
				Response.Redirect("/default.aspx");
			}

			if (!IsPostBack)
			{
				//SHIPMENT ID
				ViewState["OrderNum"] = Request.QueryString["Id"].ToString();
				
				string Id0;
				string[] tempId = new string[2];
				char[] splitter  = {'-'};  				
				Id0 = ViewState["OrderNum"].ToString();
				tempId = Id0.Split(splitter);
				ViewState["Id1"] = tempId[0];
				ViewState["Id2"] = tempId[1];


				using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
				{
					conn.Open();
					SqlCommand cmdPerson, cmdClosingDate;
					if (Request.QueryString["Receivable"] != null)
					{
						//Response.Write("BUYER case<br>");
						ViewState["Receivable"] = true;
						// ok  cmdPerson = new SqlCommand("SELECT SHIPMENT_BUYR FROM SHIPMENT WHERE SHIPMENT_ORDR_ID="+ViewState["OrderNum"].ToString(),conn);
						cmdPerson = new SqlCommand("SELECT SHIPMENT_BUYR FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='" + ViewState["Id1"] + "' AND SHIPMENT_SKU='" + ViewState["Id2"]+"'",conn);
						cmdClosingDate = new SqlCommand("SELECT SHIPMENT_BUYER_CLOSED_DATE FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='" + ViewState["Id1"] + "' AND SHIPMENT_SKU='" + ViewState["Id2"]+"'",conn);
					}
					else
					{
						//Response.Write("SELLER case<br>");
						ViewState["Receivable"] = false;
						cmdPerson = new SqlCommand("SELECT SHIPMENT_SELR FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='" + ViewState["Id1"] + "' AND SHIPMENT_SKU='" + ViewState["Id2"]+"'",conn);
						cmdClosingDate = new SqlCommand("SELECT SHIPMENT_SELLER_CLOSED_DATE1 FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='" + ViewState["Id1"] + "' AND SHIPMENT_SKU='" + ViewState["Id2"]+"'",conn);
					}

					ViewState["Person"] = cmdPerson.ExecuteScalar();
					if(cmdClosingDate.ExecuteScalar() != System.DBNull.Value)
						btnCloseTransaction.Enabled = false;
					else
						btnCloseTransaction.Enabled = true;

					//Response.Write("ViewState[Person]: "+ViewState["Person"].ToString()+"<br><br><br>");
				}

				//Need SHIPMENT_ID
				using (SqlConnection connex = new SqlConnection(Application["DBConn"].ToString()))
				{
					connex.Open();
	
					SqlCommand cmdShipId;
					cmdShipId= new SqlCommand("SELECT SHIPMENT_ID FROM SHIPMENT WHERE SHIPMENT_ORDR_ID = '" + ViewState["Id1"].ToString() + "' AND SHIPMENT_SKU = '" + ViewState["Id2"].ToString() + "'", connex);
					ViewState["Shipment_Id"] = cmdShipId.ExecuteScalar();
				}
				//Response.Write("ViewState[Shipment_Id]: "+ViewState["Shipment_Id"].ToString()+"<br><br>");
				Bind();
			} 
		}
		
		
	
		private void Bind()
		{
			
			SqlDataAdapter dadContent;
			DataSet dstContent;
			using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				conn.Open();
				StringBuilder sbSQL = new StringBuilder();
			
				/* initial
				sbSQL.Append(" SELECT PAY_ORDR, ");
				sbSQL.Append("    COMPANY=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID =(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=PAY_PERS)), ");
				sbSQL.Append("    PAY_AMNT, ");
				sbSQL.Append("    PAY_DATE, ");
				sbSQL.Append("    PAY_ID, ");
				sbSQL.Append("    PAY_CMNT ");
				sbSQL.Append(" FROM PAYMENT,SHIPMENT ");
				// ok  sbSQL.Append(" WHERE PAY_ORDR="+ViewState["OrderNum"]+" ");
				//"+ViewState["Id1"]+"
				//sbSQL.Append(" WHERE PAY_ORDR='3367' ");
				sbSQL.Append(" WHERE PAY_ORDR='"+ViewState["Id1"]+"' ");			
				*/	
			
				sbSQL.Append(" SELECT PAYMENT.PAY_ORDR, COMPANY.COMP_NAME AS COMPANY, PAYMENT.PAY_AMNT, PAYMENT.PAY_DATE, PAYMENT.PAY_ID, PAYMENT.PAY_CMNT ");
				sbSQL.Append(" FROM COMPANY INNER JOIN ");
				sbSQL.Append(" PERSON ON COMPANY.COMP_ID = PERSON.PERS_COMP CROSS JOIN ");
				sbSQL.Append(" PAYMENT ");
				sbSQL.Append(" WHERE (PAYMENT.PAY_SHIPMENT = '" + ViewState["Shipment_Id"] + "') AND (PAYMENT.PAY_PERS = '"+ViewState["Person"]+"') AND (PERSON.PERS_ID = '"+ViewState["Person"]+"') ");

				dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
				dstContent = new DataSet();
				dadContent.Fill(dstContent);
				dg.DataSource = dstContent;
				dg.DataBind();
			}
			
			if(dstContent.Tables[0].Rows.Count == 0)
				btnCloseTransaction.Enabled = false;
		}
		
		//     Handler -- Selects the row to be edited	
		public void DG_Edit(object sender, DataGridCommandEventArgs e)
		{
			// store the orignal amount for later use
			// if text is blank then the value needs to be set to zero
			if (e.Item.Cells[6].Text.Length >0 )
			{
				ViewState["Orignal_Amount"] = 0;
			}
			else
			{
				ViewState["Orignal_Amount"] = e.Item.Cells[6].Text;
			}
			dg.EditItemIndex = e.Item.ItemIndex; 
			Bind();   
		}
		
		//     Handler --  cancels the row editing
		public void DG_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dg.EditItemIndex =-1;
			Bind(); 
		}

		private void checkPayId(string pay_id)
		{
			string sqlSelect = "select " +
				"		  ISNULL((select (pay_com_trans) " + 
				" from payment " + 
				" where pay_com_trans=1 and pay_shipment=(select pay_shipment from payment where pay_id=" + pay_id + ")),0) as paid," + 

				"	(" +
				"	  (select PARAM_VALUE from parameters where param_name='PERCENT_FULL_PAYMENT') / 100.0 * " +
				"		(select (SHIPMENT_BUYR_PRCE * " +
				"			ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY)) * (100 + ISNULL(SHIPMENT_TAX, 0)) / 100 " +
				"			from shipment " +
				"			where shipment_id=(select pay_shipment from payment where pay_id=" + pay_id + ")" + 
				"	         )" +
				"	  - ISNULL(sum(pay_amnt),0) " + 
				"	  ) as diff " +
				" from payment, shipment  " +
				" where pay_shipment=shipment_id " +
				"	and  " +
				" pay_id <= " + pay_id +
				" and (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR) = (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=pay_pers) " +
				"	and " + 
				"shipment_id = (select pay_shipment from payment where pay_id=" + pay_id + ")";

			bool checkFlag = false;
					
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				using (SqlDataReader dtrDiff = DBLibrary.GetDataReaderFromSelect(conn,sqlSelect))
				{
					if(dtrDiff.Read())
					{
						//> 0 means NOT full ammount with current payment
						if( (HelperFunction.getSafeDoubleFromDB(dtrDiff["diff"]) <= 0) && !HelperFunction.getSafeBooleanFromDB(dtrDiff["paid"]))
						{
							//only if flag is 0
							checkFlag = true;
						}
					}
				}
			}

			string sqlUpdate = "";
			if(checkFlag)
			{
				sqlUpdate = "UPDATE payment set pay_com_trans=1 WHERE pay_id=" + pay_id;
			}
			else
			{
				sqlUpdate = "UPDATE payment set pay_com_trans=1 WHERE pay_id=(select max(pay_id) from payment group by pay_shipment having pay_shipment=(select pay_shipment from payment where pay_id=" + pay_id + "))";
			}
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),sqlUpdate);
					
		}
		
		private void checkFlagFullPayment(string pay_id, string new_ammount)
		{
			string sqlSelect = "select " +
				"		  ISNULL((select (pay_com_trans) " + 
				" from payment " + 
				" where pay_com_trans=1 and pay_shipment=(select pay_shipment from payment where pay_id=" + pay_id + ")),0) as paid," + 
				"	(" +
				"	  (select PARAM_VALUE from parameters where param_name='PERCENT_FULL_PAYMENT') / 100.0 * " +
				"		(select SHIPMENT_BUYR_PRCE * " +
				"			ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) " +
				"			from shipment " +
				"			where shipment_id=(select pay_shipment from payment where pay_id=" + pay_id + ")" + 
				"	         )" +
				"	  - ISNULL(sum(pay_amnt),0) - " + new_ammount +
				"	  ) as diff " +
				" from payment, shipment  " +
				" where pay_shipment=shipment_id " +
				"	and  " +
				" pay_id <> " + pay_id +
				" and (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR) = (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=pay_pers) " +
				"	and " + 
				"shipment_id = (select pay_shipment from payment where pay_id=" + pay_id + ")";

			bool checkFlag = false;
			bool cancelFlag = false;

			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				using (SqlDataReader dtrDiff = DBLibrary.GetDataReaderFromSelect(conn,sqlSelect))
				{
					if(dtrDiff.Read())
					{
						//> 0 means NOT full ammount with current payment
						if( (HelperFunction.getSafeDoubleFromDB(dtrDiff["diff"]) > 0))
						{
							if(HelperFunction.getSafeBooleanFromDB(dtrDiff["paid"]))
							{
								//have to cancel flag
								cancelFlag = true;
							}
						}
						else
						{
							if(!HelperFunction.getSafeBooleanFromDB(dtrDiff["paid"]))
							{
								//only if flag is 0
								checkFlag = true;
							}
						}
					}
				}
			}
			if(cancelFlag)
			{
				string sqlCancel= "Update payment set pay_com_trans=0 where pay_com_trans=1 and pay_shipment=(select pay_shipment from payment where pay_id=" + pay_id + ")";
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),sqlCancel);
			}

			if(checkFlag)
			{
				checkPayId(pay_id);
			}
		}
	
		//     Handler --  deletes the record the row editing
		public void DG_Delete(object sender, DataGridCommandEventArgs e)
		{	
			checkFlagFullPayment(e.Item.Cells[2].Text, "0.0");

			string SQL="Delete From PAYMENT WHERE PAY_ID=@PayID";
			Hashtable htParam = new Hashtable();
			htParam.Add("@PayID", e.Item.Cells[2].Text);
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),SQL,htParam);
			
			Bind();  
		}

		//     Handler --  updates the tables based on new information		
		public void DG_Update(object sender, DataGridCommandEventArgs e)
		{
			double dbAmount_Diff;

			string new_ammount = ParseMoney(((TextBox) e.Item.Cells[6].Controls[0]).Text).ToString();
			string pay_id = e.Item.Cells[2].Text;

			// calc difference between the new and old amount
			dbAmount_Diff = Convert.ToDouble( ViewState["Orignal_Amount"]) - Convert.ToDouble(ParseMoney(((TextBox) e.Item.Cells[6].Controls[0]).Text));
			string SQL;
			//SQL = "Update PAYMENT SET PAY_DATE='"+((TextBox) e.Item.Cells[5].Controls[0]).Text+"', PAY_AMNT='"+ new_ammount +"', PAY_CMNT='"+HelperFunction.RemoveSqlEscapeChars(((TextBox) e.Item.Cells[7].Controls[0]).Text) + "' ";
			//SQL += " WHERE PAY_ID=" + e.Item.Cells[2].Text;
			SQL = "Update PAYMENT SET PAY_DATE=@PayDate, PAY_AMNT=@PayAmount, PAY_CMNT=@payComment WHERE PAY_ID=@PayID";
			Hashtable htParams = new Hashtable();
			htParams.Add("@PayDate",HelperFunction.RemoveSqlEscapeChars(((TextBox) e.Item.Cells[5].Controls[0]).Text));
			htParams.Add("@PayAmount",new_ammount);
			htParams.Add("@PayComment",HelperFunction.RemoveSqlEscapeChars(((TextBox) e.Item.Cells[7].Controls[0]).Text));
			htParams.Add("@PayID",pay_id);
			
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),SQL,htParams);

			checkFlagFullPayment(pay_id, new_ammount);

			/* Updates for new shipment table
			********* NO NEED OF THIS CODE AS ORDR_SELR_PAID IS NOT USED ANYMORE.
						if ((bool)ViewState["Recievable"]){
							SQL = "UPDATE ORDERS SET ORDR_BUYR_PAID=ORDR_BUYR_PAID-"+dbAmount_Diff.ToString() +" WHERE ORDR_ID=" +ViewState["OrderNum"];
						}else{
							SQL = "UPDATE ORDERS SET ORDR_SELR_PAID=ORDR_SELR_PAID-"+dbAmount_Diff.ToString() +" WHERE ORDR_ID=" +ViewState["OrderNum"];
						}

						if ((bool)ViewState["Recievable"]){
							SQL = "UPDATE OR SET ORDR_BUYR_PAID=ORDR_BUYR_PAID-"+dbAmount_Diff.ToString() +" WHERE ORDR_ID=" +ViewState["OrderNum"];
						}else{
							SQL = "UPDATE SHIPMENT SET ORDR_SELR_PAID=ORDR_SELR_PAID-"+dbAmount_Diff.ToString() +" WHERE ORDR_ID=" +ViewState["OrderNum"];
						}
						cmdUpdate = new SqlCommand(SQL,conn);
						cmdUpdate.ExecuteNonQuery();*/
		
			UpdateOrderStatus();
			dg.EditItemIndex =-1;
			Bind();  
		}

		// removes 
		protected string ParseMoney(string TextIN)
		{
			string strTemp = TextIN;
			foreach(char c in TextIN) 
			{
				if((!Char.IsNumber(c)) && (c.ToString() != ".") ) 
				{
					strTemp = strTemp.Replace(c.ToString(),"");
				}
			}
			return strTemp;

		}
	
	
		// Updates the status of both sides of the transactions
		private void UpdateOrderStatus()
		{
			string SQL="";
			string SQL2="";
			bool bClosed;
			StringBuilder sb = new StringBuilder();
			/*
			sb.Append(" SELECT ");
			sb.Append(" ((ORDR_PRCE+ORDR_MARK+ORDR_SHIP_PRCE+ORDR_COMM)*ISNULL(ORDR_WGHT,ORDR_SIZE*ORDR_QTY) - ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_ORDR=ORDR_ID AND PAY_PERS=ORDR_BUYR),0)) AS BUYER, ");
			sb.Append(" (ORDR_PRCE*ISNULL(ORDR_WGHT,ORDR_SIZE*ORDR_QTY)- ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_ORDR=ORDR_ID AND PAY_PERS=ORDR_SELR),0)) ");
			sb.Append(" AS Seller from ORDERS WHERE ORDR_ID = '"+ViewState["OrderNum"].ToString()+"'  ");
			*/
			/* init
			sb.Append(" SELECT ");
			sb.Append(" ((SHIPMENT_BUYR_PRCE)*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) - ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ORDR_ID AND PAY_PERS=SHIPMENT_BUYR),0)) AS BUYER, ");
			sb.Append(" (SHIPMENT_PRCE       *ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) - ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ORDR_ID AND PAY_PERS=SHIPMENT_SELR),0)) ");
			sb.Append(" AS SELLER from SHIPMENT WHERE SHIPMENT_ORDR_ID = '"+ViewState["Id1"]+"' AND SHIPMENT_SKU='"+ViewState["Id2"]+"'  ");
			*/
			sb.Append(" SELECT ");
			sb.Append(" ((SHIPMENT_BUYR_PRCE)*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) - ");
			sb.Append(" ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),0)) AS BUYER, ");
			sb.Append(" (SHIPMENT_PRCE *ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) - ");
			sb.Append(" ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_SELR),0)) ");
			sb.Append(" AS SELLER from SHIPMENT WHERE SHIPMENT_ORDR_ID = '"+ViewState["Id1"]+"' AND SHIPMENT_SKU='"+ViewState["Id2"]+"'  ");

			/*					
			sb.Append(" SELECT SHIPMENT_MARK + (SHIPMENT_PRCE + SHIPMENT_SHIP_PRCE + SHIPMENT_COMM) * ISNULL(SHIPMENT_WEIGHT,  ");
			sb.Append(" SHIPMENT_SIZE * SHIPMENT_QTY) - ISNULL ");
			sb.Append(" ((SELECT SUM(PAY_AMNT) ");
			sb.Append(" FROM PAYMENT ");
			sb.Append(" WHERE PAY_SHIPMENT = SHIPMENT_ORDR_ID AND PAY_PERS = SHIPMENT_BUYR), 0) AS BUYER,  ");
			sb.Append(" SHIPMENT_PRCE * ISNULL(SHIPMENT_WEIGHT, SHIPMENT_SIZE * SHIPMENT_QTY) - ISNULL ");
			sb.Append(" ((SELECT SUM(PAY_AMNT) ");
			sb.Append(" FROM PAYMENT ");
			sb.Append(" WHERE PAY_SHIPMENT = SHIPMENT_ORDR_ID AND PAY_PERS = SHIPMENT_SELR), 0) AS Seller ");
			sb.Append(" FROM SHIPMENT ");
			sb.Append(" WHERE (SHIPMENT_ORDR_ID = '"+ViewState["Id1"]+"') AND (SHIPMENT_SKU = '"+ViewState["Id2"]+"') ");
			*/

			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				using (SqlDataReader dtrUpdate = DBLibrary.GetDataReaderFromSelect(conn,sb.ToString()))
				{
					dtrUpdate.Read();
		
					SQL = "SELECT GETDATE()";
					SQL2 = "SELECT GETDATE()";
				
					//Response.Write("<br>Resul buyer: "+Math.Abs(Convert.ToDouble(dtrUpdate["BUYER"]))+"<br>");
					//Response.Write("Resul seller: "+Math.Abs(Convert.ToDouble(dtrUpdate["SELLER"]))+"<br>");
					//Response.Write("In update Id1-Id2: "+ViewState["Id1"]+"-"+ViewState["Id2"]+"<br><br>");
						
					if (Math.Abs(Convert.ToDouble(dtrUpdate["BUYER"])) < 1)
					{
						//Response.Write("Ds update BUYER et < 1 Id1-Id2: "+ViewState["Id1"]+"-"+ViewState["Id2"]+"<br><br>");
						SQL = "UPDATE SHIPMENT SET SHIPMENT_BUYER_CLOSED_DATE=GETDATE() WHERE (SHIPMENT_ORDR_ID = '"+ViewState["Id1"]+"') AND (SHIPMENT_SKU = '"+ViewState["Id2"]+"')";		
					}	
					DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),SQL);
		
					if (Math.Abs(Convert.ToDouble(dtrUpdate["SELLER"])) < 1)
					{
						//Response.Write("Ds update BUYER et < 1 Id1-Id2: "+ViewState["Id1"]+"-"+ViewState["Id2"]+"<br><br>");
						SQL2 = "UPDATE SHIPMENT SET SHIPMENT_SELLER_CLOSED_DATE1=GETDATE() WHERE (SHIPMENT_ORDR_ID = '"+ViewState["Id1"]+"') AND (SHIPMENT_SKU = '"+ViewState["Id2"]+"')";
					}
					DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),SQL2);
				}
			}
		}
		
		// Adds a blank payment to the screen
		public void Add_New(object sender, EventArgs e)
		{
			/*
			SqlConnection connex;
			connex = new SqlConnection(Application["DBConn"].ToString());
			connex.Open();
	
			SqlCommand cmdShipId;
			SqlDataReader dtrShipId;
			cmdShipId= new SqlCommand("SELECT SHIPMENT_ID FROM SHIPMENT WHERE SHIPMENT_ORDR_ID = '" + ViewState["Id1"].ToString() + "' AND SHIPMENT_SKU = '" + ViewState["Id2"].ToString() + "'", connex);
			dtrShipId= cmdShipId.ExecuteReader(); 
			dtrShipId.Read();
			
			string spay_ship ="";
			int pay_ship;
			spay_ship = dtrShipId["SHIPMENT_ID"].ToString();
			pay_ship = Convert.ToInt32(spay_ship);
			dtrShipId.Close();
			connex.Close();
					
			Response.Write("ship_id: "+pay_ship+"<br><br>");
			*/

				// ok   cmdNew = new SqlCommand("INSERT INTO PAYMENT (PAY_PERS,PAY_ORDR,) VALUES ('"+ViewState["Person"].ToString()+"','"+ViewState["OrderNum"].ToString()+"')",conn);
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"INSERT INTO PAYMENT (PAY_PERS,PAY_ORDR,PAY_SHIPMENT) VALUES ('"+ViewState["Person"].ToString()+"','"+ViewState["Id1"].ToString()+"','"+ ViewState["Shipment_Id"].ToString() +"')");
			Bind();

			//Response.Write("ship id: "+pay_ship+"<br><br>");
			//Response.Write("ship id full: "+ViewState["Id1"].ToString()+"-"+ViewState["Id2"].ToString()+"<br><br>");
		}

		// Handler for when the user clicks done
	
		
		public void Navigate_Back(object sender, EventArgs e)
		{
			Response.Redirect("/Creditor/Transaction_Summary.aspx");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnCloseTransaction_Click(object sender, System.EventArgs e)
		{					
			string SQL="";
			StringBuilder sb = new StringBuilder();
			
			using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				conn.Open();
			
				if ((bool)ViewState["Receivable"] == true)
				{
					sb.Append(" SELECT ");			
					sb.Append(" (SELECT MAX(PAY_DATE) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS = SHIPMENT_BUYR) ");
					sb.Append(" AS CLOSINGDATE from SHIPMENT WHERE SHIPMENT_ORDR_ID = '" + ViewState["Id1"] + "' AND SHIPMENT_SKU='" + ViewState["Id2"] + "'  ");
					using (SqlDataReader dtrUpdate = DBLibrary.GetDataReaderFromSelect(conn,sb.ToString()))
					{
						dtrUpdate.Read();

						//Response.Write("Ds update BUYER et < 1 Id1-Id2: " + ViewState["Id1"] + "-" + ViewState["Id2"] + "<br><br>");
						SQL = "UPDATE SHIPMENT SET SHIPMENT_BUYER_CLOSED_DATE = '" + Convert.ToDateTime(dtrUpdate["CLOSINGDATE"]).ToShortDateString() + "' WHERE (SHIPMENT_ORDR_ID = '"+ViewState["Id1"]+"') AND (SHIPMENT_SKU = '"+ViewState["Id2"]+"')";
					}
				}					
				else
				{
					sb.Append(" SELECT ");			
					sb.Append(" (SELECT MAX(PAY_DATE) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS = SHIPMENT_SELR) ");
					sb.Append(" AS CLOSINGDATE from SHIPMENT WHERE SHIPMENT_ORDR_ID = '" + ViewState["Id1"] + "' AND SHIPMENT_SKU='" + ViewState["Id2"] + "'  ");
					using (SqlDataReader dtrUpdate2 = DBLibrary.GetDataReaderFromSelect(conn,sb.ToString()))
					{
						dtrUpdate2.Read();

						//Response.Write("Ds update BUYER et < 1 Id1-Id2: "+ViewState["Id1"]+"-"+ViewState["Id2"]+"<br><br>");
						SQL = "UPDATE SHIPMENT SET SHIPMENT_SELLER_CLOSED_DATE1= '" + Convert.ToDateTime(dtrUpdate2["CLOSINGDATE"]).ToShortDateString() + "' WHERE (SHIPMENT_ORDR_ID = '"+ViewState["Id1"]+"') AND (SHIPMENT_SKU = '"+ViewState["Id2"]+"')";
					}
				}

				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),SQL);				
			}
		}
	}
}
