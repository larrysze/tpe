<%@ Page Language="c#" CodeBehind="AccountSummary.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.AccountSummary" MasterPageFile="~/MasterPages/Menu.Master" Title="Account Summary" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
<style type="text/css">
    .style1 { FONT-WEIGHT: bold; FONT-SIZE: 14px }
    .style5 { FONT-SIZE: medium }
</style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
	<table align="center" width="780px" cellspacing="0" cellpadding="0">
		<tr>
			<td><span class="Header Color2 Bold"><br /><asp:label id="lblCompName" runat="server"></asp:label>: 
         
         <%
         if(fct == "all_hist")
         {%>                                                                                                                                                           All Transaction<%}
         else if(fct == "out")
         {%>                                   Total Outstanding <asp:label id="lblTotalOutstand" runat="server"></asp:label><%}
         else if (fct == "com")
         {%>                              Committed Inventory <asp:label id="lblComInv" runat="server"></asp:label><%}
         else if (fct == "cur")
         {%>                              Current <asp:label id="lblCurr" runat="server"></asp:label><%}
         else if (fct == "030")
         {%>                              0-30 Days Late <asp:label id="lblO30" runat="server"></asp:label><%}
         else if (fct == "03160")
         {%>                              30-60 Days Late <asp:label id="lblO3160" runat="server"></asp:label><%}
         else if (fct == "06190")
         {%>                              60-90 Days Late <asp:label id="lblO6190" runat="server"></asp:label><%}
         else if (fct == "90")
         {%>                              More than 90 Days Late <asp:label id="lbl90" runat="server"></asp:label><%}
         %>
       
          </span></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td height="26">
				<div align="center"><asp:button Height="22" id="btnMakePayment" onclick="MakePayment" runat="server" Text="Make Payment"></asp:button>&nbsp;
					<asp:button Height="22" id="btnEMailStatement" onclick="EmailStatement" runat="server" Text="E-mail statement"></asp:button>&nbsp;&nbsp;
					<asp:label id="lblTransaction" runat="server"><span class="Content Color2">Transactions:</span></asp:label><asp:dropdownlist CssClass="InputForm" id="ddlTransaction" runat="server" Width="178px" AutoPostBack="True" onselectedindexchanged="ddlTransaction_SelectedIndexChanged">
						<asp:ListItem Value="out" Selected="True">Total Outstanding</asp:ListItem>
						<asp:ListItem Value="com">Committed Inventory</asp:ListItem>
						<asp:ListItem Value="cur">Current</asp:ListItem>
						<asp:ListItem Value="030">0-30 Days Late</asp:ListItem>
						<asp:ListItem Value="03160">30-60 Days Late</asp:ListItem>
						<asp:ListItem Value="06190">60-90 Days Late</asp:ListItem>
						<asp:ListItem Value="90">More than 90 Days Late</asp:ListItem>
						<asp:ListItem Value="all_hist">All Transactions</asp:ListItem>
					</asp:dropdownlist></div>
			</td>
		</tr>
		<tr>
			<td><br />
				<asp:Panel runat="server" ID="pnlReceivables"><div class="DivTitleBarMenu"><span class="Header Color1 Bold"><asp:label id="lblReceivables" runat="server" text="Receivables"></asp:label>
					</span></div></asp:Panel>
			</td>
		</tr>
		<tr>
			<td>
				<div align="center"><strong></strong></div>
				<asp:datagrid BackColor="000000" CellSpacing="1" BorderWidth="0" id="dg" runat="server" Width="100%" CssClass="DataGrid" ItemStyle-CssClass="DataGridRow"
					AlternatingItemStyle-CssClass="DataGridRow_Alternate" HeaderStyle-CssClass="LinkNormal"
					AutoGenerateColumns="False" AllowSorting="True" OnItemDataBound="OnDataGridBind" onSortCommand="SortDG"
					ShowFooter="True" CellPadding="2" HorizontalAlign="Center">
					<AlternatingItemStyle CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
					<ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
					<HeaderStyle CssClass="LinkNormal OrangeColor"></HeaderStyle>
					<FooterStyle CssClass="Content Color4 Bold FooterColor"></FooterStyle>
					<Columns>
						<asp:TemplateColumn>
							<HeaderTemplate>
-
</HeaderTemplate>
							<ItemTemplate></ItemTemplate>
						</asp:TemplateColumn>
						<asp:HyperLinkColumn DataNavigateUrlFormatString="#" DataTextField="TRANS_ID" SortExpression="TRANS_ID ASC"
							HeaderText="Transaction #">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:BoundColumn DataField="SHIPMENT_WEIGHT" SortExpression="SHIPMENT_WEIGHT ASC" HeaderText="Weight"
							DataFormatString="{0:#,##}">
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn Visible="False" DataField="SHIPMENT_BUYER_TERMS" SortExpression="SHIPMENT_BUYER_TERMS ASC"
							HeaderText="Terms (Day)">
							<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARDDATE" SortExpression="VARDDATE ASC" HeaderText="Due Date" DataFormatString="{0:MM/dd/yyyy}">
							<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="SHIPMENT_BUYER_CLOSED_DATE" SortExpression="SHIPMENT_BUYER_CLOSED_DATE ASC"
							HeaderText="Date Paid" DataFormatString="{0:MM/dd/yyyy}">
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARDATEDIFF" SortExpression="VARDATEDIFF ASC" HeaderText="Days Past Due">
							<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="SELL_PRICE" SortExpression="SELL_PRICE ASC" HeaderText="Sell Price">
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Outstanding" SortExpression="Outstanding ASC" HeaderText="Value" DataFormatString="{0:c}">
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="TOTAL" SortExpression="TOTAL ASC" HeaderText="Total Received" DataFormatString="{0:c}">
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:TemplateColumn Visible="False">
							<HeaderTemplate>
-
</HeaderTemplate>
							<ItemTemplate></ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn DataField="DAYSTOPAY" SortExpression="DAYSTOPAY ASC" HeaderText="Days to Pay">
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
					</Columns>
				</asp:datagrid></td>
		</tr>
		<tr>
			<td>
				<asp:Panel id="pnlTitleBar" runat="server" Visible="false"><div class="DivTitleBarMenu"><span class="Header Color1 Bold"><asp:label id="lblPayables" runat="server" text="Payables"></asp:label>
					</span></div></asp:Panel>
				</td></tr>
		<tr>
			<td>
				<div align="center"></div>
				<asp:datagrid BackColor="000000" CellSpacing="1" BorderWidth="0" id="dg2" runat="server" Width="100%" CssClass="DataGrid" ItemStyle-CssClass="DataGridRow"
					AlternatingItemStyle-CssClass="DataGridRow_Alternate" HeaderStyle-CssClass="DataGridHeader"
					ShowFooter="True" AutoGenerateColumns="False" AllowSorting="True" OnItemDataBound="OnDataGridBind2"
					onSortCommand="SortDG2" CellPadding="2" HorizontalAlign="Center">
					<AlternatingItemStyle CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
					<ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
					<HeaderStyle CssClass="LinkNormal OrangeColor"></HeaderStyle>
					<FooterStyle CssClass="Content Color4 FooterColor"></FooterStyle>
					<Columns>
						<asp:TemplateColumn>
							<HeaderTemplate>
-
</HeaderTemplate>
							<ItemTemplate></ItemTemplate>
						</asp:TemplateColumn>
						<asp:HyperLinkColumn DataNavigateUrlFormatString="#" DataTextField="TRANS_ID" SortExpression="TRANS_ID ASC"
							HeaderText="Transaction #">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:BoundColumn DataField="SHIPMENT_WEIGHT" SortExpression="SHIPMENT_WEIGHT ASC" HeaderText="Weight"
							DataFormatString="{0:#,##}">
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn Visible="False" DataField="SHIPMENT_BUYER_TERMS" SortExpression="SHIPMENT_BUYER_TERMS ASC"
							HeaderText="Terms (Day)">
							<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARDDATE" SortExpression="VARDDATE ASC" HeaderText="Due Date" DataFormatString="{0:MM/dd/yyyy}">
							<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="SHIPMENT_SELLER_CLOSED_DATE1" SortExpression="SHIPMENT_SELLER_CLOSED_DATE1 ASC"
							HeaderText="Date Paid" DataFormatString="{0:MM/dd/yyyy}">
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARDATEDIFF" SortExpression="VARDATEDIFF ASC" HeaderText="Days Past Due">
							<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="BUY_PRICE" SortExpression="BUY_PRICE ASC" HeaderText="Buy Price">
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Outstanding" SortExpression="Outstanding ASC" HeaderText="Value" DataFormatString="{0:c}">
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="TOTAL" SortExpression="TOTAL ASC" HeaderText="Total Paid Out" DataFormatString="{0:c}">
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:TemplateColumn Visible="False">
							<HeaderTemplate>
-
</HeaderTemplate>
							<ItemTemplate></ItemTemplate>
						</asp:TemplateColumn>
					</Columns>
				</asp:datagrid><br /></td>
		</tr>
		<TR>
			<TD align="center"><asp:label id="lblMessage" runat="server">There are no transactions listed that match this request!</asp:label></TD>
		</TR>
	</table>

</asp:Content>