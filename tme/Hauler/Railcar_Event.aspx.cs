using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;



namespace localhost.Hauler
{
	/// <summary>
	/// Summary description for Railcar_Event.
	/// </summary>
	public partial class Railcar_Event : System.Web.UI.Page
	{
	
		public void Page_Load(object sender, EventArgs e)
		{
//			if ((string)Session["Typ"] != "A")
//			{
//				Response.Redirect("../default.aspx");
//			}

			Data_Bind();
		}
		private void Data_Bind()
		{

			SqlDataAdapter dadContent;
			DataSet dstContent;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			StringBuilder sbSQL = new StringBuilder();

			sbSQL.Append(" select * from RAILCAR_EVENT WHERE EVENT_RAILNUMBER='"+Request.QueryString["Num"].ToString()+"' ORDER BY EVENT_TIME DESC ");
		
			dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dg.DataSource = dstContent;
			dg.DataBind();
			conn.Close();

		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
