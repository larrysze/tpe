using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using TPE.Utility;

namespace localhost.Hauler
{
	/// <summary>
	/// Summary description for Track_Rail_Car.
	/// </summary>
	public partial class Track_Rail_Car : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.HyperLink All;
        private bool ShowRailCarHistory = false;
	
		
		/************************************************************************
		 *   1. File Name       :Administrator\Users_Management.aspx             *
		 *   2. Description     :Exchange user management                        *
		 *   3. Modification Log:                                                *
		 *     Ver No.       Date          Author             Modification       *
		 *   -----------------------------------------------------------------   *
		 *      1.00      2-25-2004      Zach                Comment             *
		 *                                                                       *
		 ************************************************************************/
		public void Page_Load(object sender, EventArgs e)
		{            
            if ((string)Session["Typ"] != "A")
			{
				 Response.Redirect("../default.aspx");
			}
            
			if (!IsPostBack)
			{
                Master.Width = "1020px";
				Bind_EnRoute();
				Bind_Inventory();                                
			}
		}
		private void Bind_EnRoute()
		{
            DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(), this.dg, "spRailCars_EnRoute");
		}

		private void Bind_Inventory()
		{
            DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(), this.dg_Inventory, "spRailCars_Inventory");                                
		}

		private void dg_Inventory_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (Page.IsValid)
			{
				if (e.CommandName == "Submit")
				{
					using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
					{
                        conn.Open();                    

					    string SQL; // string to store query
    				
					    SQL ="UPDATE SHIPMENT SET ";
					    if (((TextBox) e.Item.Cells[4].Controls[1]).Text !="")
						    SQL +="SHIPMENT_WEIGHT ='"+ ((TextBox) e.Item.Cells[4].Controls[1]).Text+"' ,";
					    if (((TextBox) e.Item.Cells[5].Controls[1]).Text !="")
						    SQL += " SHIPMENT_SHIP_STATUS='2', SHIPMENT_DATE_TAKE ='"+((TextBox) e.Item.Cells[5].Controls[1]).Text+"' ,";
					    if (((TextBox) e.Item.Cells[6].Controls[1]).Text !="")
						    SQL += "SHIPMENT_COMMENT ='"+((TextBox) e.Item.Cells[6].Controls[1]).Text+"' ,";
    				
					    //if there is any parameter to update
					    if (SQL != "UPDATE SHIPMENT SET ")
					    {
						    // remove final comma where it lies
						    SQL = SQL.Substring(0,SQL.Length-1);
						    // close the query up
						    SQL +=  "WHERE SHIPMENT_ID='"+e.Item.Cells[7].Text+"'";
						    SqlCommand cmd = new SqlCommand(SQL,conn);
						    //Response.Write(SQL);
						    cmd.ExecuteNonQuery();
						    cmd.Dispose();		

						    string strSQLgetShipment = "SELECT SHIPMENT_ID, SHIPMENT_COMMENT, " +
							    " (SELECT LOCL_CITY FROM LOCALITY, PLACE WHERE PLAC_ID=SHIPMENT_TO AND PLAC_LOCL=LOCL_ID) AS DEST_CITY, " + 
							    " (SELECT LOCL_STAT FROM LOCALITY, PLACE WHERE PLAC_ID=SHIPMENT_TO AND PLAC_LOCL=LOCL_ID) AS DEST_STAT, " + 
							    " (SELECT LOCL_CITY FROM LOCALITY, PLACE WHERE PLAC_ID=SHIPMENT_FROM AND PLAC_LOCL=LOCL_ID) AS LOC_CITY, " + 
							    " (SELECT LOCL_STAT FROM LOCALITY, PLACE WHERE PLAC_ID=SHIPMENT_FROM AND PLAC_LOCL=LOCL_ID) AS LOC_STAT " + 
							    " FROM SHIPMENT WHERE SHIPMENT_ID='" + e.Item.Cells[7].Text+"'";
						    cmd = new SqlCommand(strSQLgetShipment, conn);
						    SqlDataReader dtrShipment = cmd.ExecuteReader();

						    string strSQL = "";
						    if(dtrShipment.Read())
						    {
							    strSQL ="  INSERT INTO RAILCAR_EVENT (EVENT_RAILNUMBER, EVENT_LOCATION_CITY, ";
							    strSQL +=" EVENT_LOCATION_STATE,EVENT_DESTINATION_CITY,EVENT_DESTINATION_STATE,";
							    strSQL +=" EVENT_CODE,EVENT_STATUS,EVENT_TIME) ";
							    strSQL +=" VALUES ('" + dtrShipment["SHIPMENT_COMMENT"].ToString() 
								    + "', '" + dtrShipment["LOC_CITY"] + "', '" + dtrShipment["LOC_STAT"] 
								    + "', '" + dtrShipment["DEST_CITY"] + "', '" + dtrShipment["DEST_STAT"]
								    + "'," + "'Taken'," + "'T','"
								    + ((TextBox) e.Item.Cells[5].Controls[1]).Text + "')";

						    }
    						
						    dtrShipment.Close();
						    if(strSQL != "")
						    {
							    cmd = new SqlCommand(strSQL, conn);
							    cmd.ExecuteNonQuery();
						    }
    						
						    cmd.Dispose();
						    // rebind
						    Bind_EnRoute();
						    Bind_Inventory();
					    }
				    }
                }
			}
		}

		private void makeDelivered(string railcar_number)
		{
            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();

                string strSQL = "UPDATE SHIPMENT SET SHIPMENT_DATE_EMPTIED='" + DateTime.Now.ToShortDateString() + "' WHERE SHIPMENT_COMMENT='" + railcar_number + "'";
                SqlCommand cmd = new SqlCommand(strSQL, conn);
                cmd.ExecuteNonQuery();

                strSQL = "UPDATE RAILCAR_EVENT SET EVENT_STATUS='L', EVENT_CODE='Arrive Dest' WHERE EVENT_RAILNUMBER='" + railcar_number + "'";
                cmd = new SqlCommand(strSQL, conn);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
		}

        /// <summary>
        /// insert javascript to scroll down to rail car history datagrid
        /// </summary>
        private void insertScrollToScript()
        {
            string strJS = "<script type='text/javascript'>divHistory.scrollIntoView();</script>";
            RegisterStartupScript("ScrollToHistoryScriptBlock", strJS);
        }

		private void dg_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{			
			if (e.CommandName == "Track")
			{
				txtSearch.Text = e.Item.Cells[4].Text;
                Bind_Track(txtSearch.Text);
                insertScrollToScript();
			}
			else if(e.CommandName == "Delivered")
			{
				makeDelivered(e.Item.Cells[4].Text);
				Bind_EnRoute();
			}
		}

		// <summary>
		//     Adds all car to a list all screen
		// </summary>

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg_Inventory.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_Inventory_ItemCommand);
			this.dg_Inventory.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_Inventory_ItemDataBound);
			this.dg.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_ItemCommand);

		}
		#endregion

		protected void btnTrack_Click(object sender, System.EventArgs e)
		{
			txtSearch.Text = txtSearch.Text.Trim();
			if (txtSearch.Text.IndexOf("-") >0) // probably passing an order number 
			{
				string strOrderID = txtSearch.Text.Substring(0, txtSearch.Text.IndexOf("-"));
                string strSKU = txtSearch.Text.Substring(txtSearch.Text.IndexOf("-") + 1, txtSearch.Text.Length - txtSearch.Text.IndexOf("-") - 1);
                Hashtable htParams = new Hashtable();
                htParams.Add("@OrderID", txtSearch.Text.Substring(0, txtSearch.Text.IndexOf("-")));
                htParams.Add("@SKU", txtSearch.Text.Substring(txtSearch.Text.IndexOf("-") + 1, txtSearch.Text.Length - txtSearch.Text.IndexOf("-") - 1));
                string sql = "SELECT SHIPMENT_COMMENT FROM SHIPMENT WHERE SHIPMENT_ORDR_ID=@OrderID AND SHIPMENT_SKU=@SKU";

                string strRailCarNumber = DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), sql, htParams).ToString();
                if (strRailCarNumber != null)
				{
                    Bind_Track(strRailCarNumber);
				}
				else
				{
					Bind_Track(txtSearch.Text);
				}
			}
			else
			{	
				Bind_Track(txtSearch.Text);
			}
            insertScrollToScript();
		}

		private void Bind_Track(string strSearch)
		{			
            Hashtable htParams = new Hashtable();
            htParams.Add("@Search", strSearch);
            string strSQL = "select *, (CAST(B.shipment_ordr_id as VARCHAR(50))+ '-' + CAST(B.shipment_sku as VARCHAR(50))) as OrderNumber From railcar_event inner join Shipment B on railcar_event.event_railnumber = B.shipment_comment where event_railnumber = @Search";
            DBLibrary.BindDataGrid(Application["DBconn"].ToString(), dg_Track, strSQL, htParams);
			
            // display message if it can't find anything
			if (dg_Track.Items.Count==0)
			{	
				dg_Track.Visible=false;
				lblNoTrackInfo.Visible=true;
			}
			else
			{
				dg_Track.Visible=true;
				lblNoTrackInfo.Visible=false;
			}
		}

		private void dg_Inventory_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				((Button)e.Item.Cells[0].Controls[0]).CausesValidation = true;
			}

			if ((DataBinder.Eval(e.Item.DataItem, "RAIL_NUMBER")) !=null)
			{
				if ((DataBinder.Eval(e.Item.DataItem, "RAIL_NUMBER")).ToString().Length >1)
				{
					e.Item.Cells[8].Text ="<a target='blank' href=https://www.steelroads.com/servlet/SAServlet?CONTROLLER=ETScriptedAccessCtlr&ScriptedAccessType=Dynamic&ACTION=&LANGUAGE=en&ImportList&EquipmentList="+(DataBinder.Eval(e.Item.DataItem, "RAIL_NUMBER")).ToString()+">Track</a>";
						
				}
			}
		
		}
    }
}
