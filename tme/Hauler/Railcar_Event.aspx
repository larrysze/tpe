<%@ Page language="c#" Codebehind="Railcar_Event.aspx.cs" AutoEventWireup="True" Inherits="localhost.Hauler.Railcar_Event" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
	<LINK href="/include/master.css" type="text/css" rel="stylesheet">
		<title>Track Railcar </title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		
		<form id="Form1" method="post" runat="server">
		<B>Track Railcar Shipment Status</B>
			<asp:DataGrid id="dg" runat="server" CssClass="DataGrid" ItemStyle-CssClass="DataGridRow" AlternatingItemStyle-CssClass="DataGridRow_Alternate"
				HeaderStyle-CssClass="DataGridHeader" AutoGenerateColumns="false" 
				CellPadding="2" HorizontalAlign="left" Width="400px" ShowFooter="false">
				<Columns>
					<asp:BoundColumn DataField="EVENT_LOCATION_CITY" HeaderText="City" ItemStyle-Wrap="false" />
					<asp:BoundColumn DataField="EVENT_LOCATION_STATE" HeaderText="St" ItemStyle-Wrap="false" />
					<asp:BoundColumn DataField="EVENT_TIME" HeaderText="Time" ItemStyle-Wrap="false" />
					<asp:BoundColumn DataField="EVENT_CODE" HeaderText="Event" ItemStyle-Wrap="false" />
					<asp:BoundColumn DataField="EVENT_DESTINATION_CITY" HeaderText="Dest City" ItemStyle-Wrap="false" />
					<asp:BoundColumn DataField="EVENT_DESTINATION_STATE" HeaderText="St" ItemStyle-Wrap="false" />
					
				</Columns>
			</asp:DataGrid>
		</form>
	</body>
</HTML>
