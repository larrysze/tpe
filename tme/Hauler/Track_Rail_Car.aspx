<%@ Page Language="c#" CodeBehind="Track_Rail_Car.aspx.cs" AutoEventWireup="True" Inherits="localhost.Hauler.Track_Rail_Car" MasterPageFile="~/MasterPages/Menu.Master" MaintainScrollPositionOnPostback="false" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphMain">
<div class="DivTitleBarMenu"><span class="Header Color1 Bold">Inventory</span></div>
	<table cellspacing="0" cellpadding="0">
		<tr>
			<td><asp:datagrid Width="1073" BorderWidth="0" BackColor="#000000" CellSpacing="1" id="dg_Inventory" runat="server" ShowFooter="false" HorizontalAlign="Left" CellPadding="2"
					AutoGenerateColumns="false" HeaderStyle-CssClass="DataGridHeader" AlternatingItemStyle-CssClass="DataGridRow_Alternate"
					ItemStyle-CssClass="DataGridRow" CssClass="DataGrid">
					<AlternatingItemStyle CssClass="LinkNormal LightGray"></AlternatingItemStyle>
					<ItemStyle CssClass="LinkNormal DarkGray"></ItemStyle>
					<HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
					<Columns>
						<asp:ButtonColumn Text="Update" ButtonType="PushButton" CommandName="Submit"></asp:ButtonColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="ORDER_NUMBER" DataNavigateUrlFormatString="/administrator/Transaction_Details.aspx?ID={0}" 
 DataTextField="ORDER_NUMBER" HeaderText="Trade">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="SELLER_ID" DataNavigateUrlFormatString="/administrator/Company_Info.aspx?Id={0}" 
 DataTextField="TEXTSELLER" HeaderText="Seller">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="BUYER_ID" DataNavigateUrlFormatString="/administrator/Company_Info.aspx?Id={0}" 
 DataTextField="TEXTBUYER" HeaderText="Buyer">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:TemplateColumn HeaderText="Weight">
							<ItemTemplate>
								<asp:TextBox CssClass="InputForm" id="txtWeight" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SHIPMENT_WEIGHT") %>' size="15">
								</asp:TextBox>
								<asp:RangeValidator id="RangeValidator1" runat="server" ErrorMessage="Invalid!" Type="Integer" MinimumValue="0" 
 MaximumValue="190000000" ControlToValidate="txtWeight" Display="Dynamic"></asp:RangeValidator>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="Date Taken">
							<ItemTemplate>
								<asp:TextBox CssClass="InputForm" id="txtDate" size="15" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SHIPMENT_DATE_TAKE") %>'>
								</asp:TextBox>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="Railcar #">
							<ItemTemplate>
								<asp:TextBox CssClass="InputForm" id="txtRCNum" size="15" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RAIL_NUMBER") %>' >
								</asp:TextBox>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn Visible="False" DataField="SHIPMENT_ID" HeaderText="Railcar #"></asp:BoundColumn>
						<asp:TemplateColumn>
							<ItemTemplate></ItemTemplate>
						</asp:TemplateColumn>
					</Columns>
				</asp:datagrid>
				<!----></td>
		</tr>
		<tr>
			<td><div class="DivTitleBarMenu"><span class="Header Color1 Bold">Shipped</span>
				<span class="Content Color1">(Status is updated from SteelRoads.com every hour on the hour)</span></div></td>
		</tr>
		<tr>
			<td><asp:datagrid BackColor="#000000" CellSpacing="1" BorderWidth="0" id="dg" runat="server" HorizontalAlign="Left" CellPadding="2" AutoGenerateColumns="False"
					HeaderStyle-CssClass="DataGridHeader" AlternatingItemStyle-CssClass="DataGridRow_Alternate"
					ItemStyle-CssClass="DataGridRow" CssClass="DataGrid">
					<AlternatingItemStyle CssClass="LinkNormal LightGray"></AlternatingItemStyle>
					<ItemStyle CssClass="LinkNormal DarkGray"></ItemStyle>
					<HeaderStyle CssClass="LinkNormal Bold Color1 OrangeColor"></HeaderStyle>
					<Columns>
						<asp:ButtonColumn Text="History" ButtonType="PushButton" CommandName="Track"></asp:ButtonColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="ORDER_NUMBER" DataNavigateUrlFormatString="/administrator/Transaction_Details.aspx?ID={0}" 
 DataTextField="ORDER_NUMBER" HeaderText="Trade">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="SELLER_ID" DataNavigateUrlFormatString="/administrator/Company_Info.aspx?Id={0}" 
 DataTextField="TEXTSELLER" HeaderText="Seller">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="BUYER_ID" DataNavigateUrlFormatString="/administrator/Company_Info.aspx?Id={0}" 
 DataTextField="TEXTBUYER" HeaderText="Buyer">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:BoundColumn DataField="RAIL_NUMBER" HeaderText="Railcar #">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="CURRENT_LOCATION" HeaderText="Current">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="EVENT_TIME" HeaderText="Updated" DataFormatString="{0:MM/dd/yyyy}">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="DAYS_SITTING" HeaderText="Days">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="EVENT_CODE" HeaderText="Last Event">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="DESTINATION_LOCATION" HeaderText="Destination">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:ButtonColumn Text="Delivered" ButtonType="PushButton" CommandName="Delivered"></asp:ButtonColumn>
					</Columns>
				</asp:datagrid></td>
		</tr>
		<tr>
			<td>
			    <div id="divHistory" class="DivTitleBarMenu">
			        <span class="Content Color1 Bold">Rail Car History </span>
			        <span class="Content Color1">Enter RC # or Order #&nbsp;</span>
				    <asp:textbox CssClass="InputForm" id="txtSearch" runat="server"></asp:textbox>
				    <asp:button Height="20" id="btnTrack" runat="server" Text="Get History" onclick="btnTrack_Click"></asp:button>
				</div>
	        </td>
		</tr>
		<tr>
			<td><asp:datagrid Width="1073" BackColor="#000000" BorderWidth="0" CellSpacing="1" id="dg_Track" runat="server" ShowFooter="false" HorizontalAlign="Left" CellPadding="2"
					AutoGenerateColumns="false" HeaderStyle-CssClass="LinkNormal Bold Color1 OrangeColor" AlternatingItemStyle-CssClass="LinkNormal DarkGray"
					ItemStyle-CssClass="LinkNormal LightGray" CssClass="DataGrid">
					<Columns>
					    <asp:BoundColumn DataField="OrderNumber" HeaderText="Order Number" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="EVENT_LOCATION_CITY" HeaderText="City" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="EVENT_LOCATION_STATE" HeaderText="St" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="EVENT_TIME" HeaderText="Time" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="EVENT_STATUS" HeaderText="" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="EVENT_CODE" HeaderText="Event" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="EVENT_DESTINATION_CITY" HeaderText="Dest City" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="EVENT_DESTINATION_STATE" HeaderText="St" ItemStyle-Wrap="false" />
					</Columns>
				</asp:datagrid><asp:label id="lblNoTrackInfo" runat="server" Visible="False" Font-Bold="True" ForeColor="Red" CssClass="Content Bold"><br />There is no information for this request.<br />&nbsp;</asp:label></td>
		</tr>
	</table>

</asp:Content>