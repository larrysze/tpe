<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page language="c#" Codebehind="Manage_RC.aspx.cs" AutoEventWireup="false" Inherits="localhost.Hauler.Manage_RC" %>
<form id="Form" runat="server">
	<TPE:TEMPLATE id="Template1" Runat="server" PageTitle=""></TPE:TEMPLATE><BR>
	<table>
		<tr>
			<td><span class="PageHeader">Inventory</span></td>
		</tr>
		<tr>
			<td>
				<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="90%" border="0">
					<TR>
						<TD>Trade</TD>
						<TD>Seller</TD>
						<TD>Buyer</TD>
						<TD>Weight</TD>
						<TD>Date</TD>
						<TD>RC#</TD>
						<TD></TD>
					</TR>
					<TR>
						<TD><A href="/administrator/Transaction_Details.aspx?Referer=/Hauler/In_Transit.aspx&amp;ID=3753-517">3796-517</A></TD>
						<TD>Cal Thermo<BR>
							Buena Park, CA</TD>
						<TD>Grand Haven<br>
							Grand Haven, MI</TD>
						<TD><INPUT type="text"></TD>
						<TD><INPUT type="text"></TD>
						<TD><INPUT type="text"></TD>
						<TD><INPUT type="button" value="Track"></TD>
					</TR>
					<TR>
						<TD><A href="/administrator/Transaction_Details.aspx?Referer=/Hauler/In_Transit.aspx&amp;ID=3753-517">3713-517</A></TD>
						<TD>Laudadio<BR>
							Chicago, IL</TD>
						<TD>Nordon<BR>
							Rochester, NY</TD>
						<TD><INPUT type="text"></TD>
						<TD><INPUT type="text"></TD>
						<TD><INPUT type="text"></TD>
						<TD><INPUT type="button" value="Track"></TD>
					</TR>
					<TR>
						<TD><A href="/administrator/Transaction_Details.aspx?Referer=/Hauler/In_Transit.aspx&amp;ID=3753-517">3753-517</A></TD>
						<TD>United Poly<BR>
							Houston, TX</TD>
						<TD>Nersery Supplies<BR>
							Chambersburg, PA</TD>
						<TD><INPUT type="text"></TD>
						<TD><INPUT type="text"></TD>
						<TD><INPUT type="text"></TD>
						<TD><INPUT type="button" value="Track"></TD>
					</TR>
				</TABLE>
			</td>
		</tr>
		<tr>
			<td><span class="PageHeader">En-Route</span>
			</td>
		</tr>
		<tr>
			<td>
				<TABLE id="dg" cellSpacing="0" cellPadding="2" align="left" width="90%" border="0">
					<TR>
						<TD>Trade</TD>
						<TD>Seller</TD>
						<TD>Buyer</TD>
						<td>RC Number</td>
						<TD>Current</TD>
						<TD>Updated</TD>
						<TD>Days</TD>
						<TD>Event</TD>
						<TD>Destination</TD>
					</TR>
					<TR>
						<TD><A href="../administrator/Transaction_Details.aspx?Referer=/Hauler/In_Transit.aspx&amp;ID=3753-517">3796-517</A></TD>
						<TD>Cal Thermo<BR>
							Buena Park, CA</TD>
						<TD>Grand Haven<br>
							Grand Haven, MI</TD>
						<td><A href="Railcar_Event.aspx?Num=PSPX003113" target="_blank">PSPX003113</A></td>
						<TD noWrap>DAYTON, TX</TD>
						<TD noWrap>3/30/2005</TD>
						<TD noWrap>L (25)</TD>
						<TD noWrap>Arrive Dest</TD>
						<TD noWrap>DAYTON, TX</TD>
					</TR>
					<TR>
						<TD><A href="../administrator/Transaction_Details.aspx?Referer=/Hauler/In_Transit.aspx&amp;ID=3753-517">3596-517</A></TD>
						<TD>Cal Thermo<BR>
							Buena Park, CA</TD>
						<TD>Grand Haven<br>
							Grand Haven, MI</TD>
						<td><A href="Railcar_Event.aspx?Num=PSPX003113" target="_blank">PSPX003113</A></td>
						<TD noWrap>DAYTON, TX</TD>
						<TD noWrap>3/30/2005</TD>
						<TD noWrap>L</TD>
						<TD noWrap>Arrive Dest</TD>
						<TD noWrap>DAYTON, TX</TD>
					</TR>
					<TR>
						<TD><A href="../administrator/Transaction_Details.aspx?Referer=/Hauler/In_Transit.aspx&amp;ID=3753-517">3765-517</A></TD>
						<TD>Cal Thermo<BR>
							Buena Park, CA</TD>
						<TD>Grand Haven<br>
							Grand Haven, MI</TD>
						<td><A href="Railcar_Event.aspx?Num=PSPX003113" target="_blank">PSPX003113</A></td>
						<TD noWrap>DAYTON, TX</TD>
						<TD noWrap>3/30/2005</TD>
						<TD noWrap>L</TD>
						<TD noWrap>Arrive Dest</TD>
						<TD noWrap>DAYTON, TX</TD>
					</TR>
				</TABLE>
			</td>
		</tr>
		<tr>
			<td>
				<span class="PageHeader">Track Rail Car</span> (Enter RC # or Order #)
			</td>
		</tr>
		<tr>
			<td>
				Track RC <INPUT type="text" value=""> <INPUT type="button" value="Search">
			</td>
		</tr>
	</table>
	<TPE:Template Footer="true" Runat="server" id="Template2" /></P>
</form>
