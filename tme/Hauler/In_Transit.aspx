<%@ Page Language="c#" codebehind="In_Transit.aspx.cs" autoeventwireup="True" Inherits="localhost.Hauler.In_Transit" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<%@ import Namespace="System.Data" %>
<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
<style type="text/css">


</style>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">

	<table id="tMain" cellspacing="0" cellpadding="0" width="100%" border="0" >		
	<tr>
		<td align="center">
			<table id="tFilters" width="100%" cellspacing="0" cellpadding="0" style="text-align:center">
				<tr valign="bottom">
					<td>&nbsp;<span class="Content Color2">Status</span>
						<asp:dropdownlist id="ddlStatus" CssClass="InputForm" runat="server" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="True">
							<asp:ListItem value="All">All</asp:ListItem>
							<asp:ListItem selected="true" value="Inventory">Inventory</asp:ListItem>
							<asp:ListItem value="Enroute">Enroute</asp:ListItem>
							<asp:ListItem value="Delivered">Delivered</asp:ListItem>
						</asp:dropdownlist><span class="Content Color2">&nbsp;&nbsp; Sizes</span>
						<asp:dropdownlist CssClass="InputForm" id="ddlSizes" runat="server" OnSelectedIndexChanged="ddlSizes_SelectedIndexChanged" AutoPostBack="True">
							<asp:ListItem value="All">All</asp:ListItem>
							<asp:ListItem value="Truckloads">Truckloads</asp:ListItem>
							<asp:ListItem value="Other">Other Sizes</asp:ListItem>
						</asp:dropdownlist>&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:Label id="label1" runat="server"><span class="Content Color2">Search (at least 2 characters):</span></asp:Label>
						<asp:TextBox CssClass="InputForm" id="txtSearch" runat="server" AutoPostBack="True" Width="144px"></asp:TextBox>
						<asp:Button Height="20px" id="btnSearch" runat="server" Text="Go" onclick="btnSearch_Click"></asp:Button>&nbsp;&nbsp;
						<asp:Button Height="20px" id="btnShowAll" runat="server" Width="61px" Text="Show All" onclick="btnShowAll_Click"></asp:Button>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
		    <input id="SortBuyr" type="hidden" value=" ASC " runat="server" />
	        <input id="SortSellr" type="hidden" value=" ASC " runat="server" /> 
	        <input id="SortTrade" type="hidden" value=" ASC " runat="server" />
			<input id="SortDelivery" type="hidden" value=" ASC " runat="server" /> 
			<input id="SortOrigin" type="hidden" value=" ASC " runat="server" />
			<input id="SortStatus" type="hidden" value=" ASC " runat="server" /> 
			<input id="SortWeight" type="hidden" value=" ASC " runat="server" />
			<input id="SortDateTaken" type="hidden" value=" ASC " runat="server" /> 
			<input id="SortDateDelivered" type="hidden" value=" ASC " runat="server" />
			<input id="SortComment" type="hidden" value=" ASC " runat="server" />
		</td>
	</tr>
	<tr>
		<td align="center">
			<asp:Label id="lblTitle" runat="server" ForeColor="#C00000" Visible="False" Font-Size="Larger"><span class="Header Bold Color3">Only first 50 records are shown<br />You may use 'Search' if you want to find any specific record using keywords</span><br /></asp:Label>
		</td>
	</tr>
	<tr>
		<td>
			<asp:datalist BorderWidth="0" BackColor="#000000" CellSpacing="0" id="dl" runat="server" OnItemDataBound="dl_ItemDataBound" Width="100%"
				CellPadding="0" ShowFooter="True" DataKeyField="ID">
				<HeaderTemplate>
					<table id="t" width="100%" border="0" cellspacing="1" cellpadding="2">
						<tr>
							<td class="OrangeColor" style="width:40px">
								<asp:LinkButton CssClass="LinkNormal" width="40px" ID="Edit" Runat="server">Edit</asp:LinkButton>
							</td>
							<td class="OrangeColor" style="width:120px">
								<asp:LinkButton  width="120px" CssClass="LinkNormal OrangeColor" OnClick="BuyerSort_Click" ID="LinkbuttonBuyer" Runat="server">Buyer</asp:LinkButton>
							</td>
							<td class="OrangeColor" style="width:120px">
								<asp:LinkButton  width="120px" CssClass="LinkNormal OrangeColor" OnClick="SellerSort_Click" ID="LinkbuttonSeller" Runat="server">Seller</asp:LinkButton>
							</td>
							<td class="OrangeColor" style="width:80px">
								<asp:LinkButton  width="80px" CssClass="LinkNormal OrangeColor" OnClick="TradeSort_Click" ID="LinkbuttonTrade" Runat="server">#Trade</asp:LinkButton>
							</td>
							<td class="OrangeColor" style="width:150px">
								<asp:LinkButton  width="150px" CssClass="LinkNormal OrangeColor" OnClick="DeliveryPointSort_Click" ID="LinkbuttonDeliveryPoint" Runat="server">Delivery Point</asp:LinkButton>								
							</td>
							<td class="OrangeColor" style="width:150px">
								<asp:LinkButton width="150px" CssClass="LinkNormal OrangeColor" OnClick="OriginPointSort_Click" ID="LinkbuttonOrigin" Runat="server">Origin Point</asp:LinkButton>								
							</td>
							<td class="OrangeColor" style="width:50px">
								<asp:LinkButton width="50px" CssClass="LinkNormal OrangeColor" OnClick="StatusSort_Click" ID="LinkbuttonStatus" Runat="server">Status</asp:LinkButton>
							</td>
							<td class="OrangeColor" style="width:60px">
								<asp:LinkButton width="60px" CssClass="LinkNormal OrangeColor" OnClick="WeightSort_Click" ID="LinkbuttonWeight" Runat="server">Weight</asp:LinkButton>
							</td>
							<td class="OrangeColor" style="width:80px">
								<asp:LinkButton width="80px" CssClass="LinkNormal OrangeColor" OnClick="DateTakenSort_Click" ID="LinkbuttonDateTaken" Runat="server" style="white-space:nowrap">Date Taken</asp:LinkButton>
							</td>
							<td class="OrangeColor" style="width:80px">
								<asp:LinkButton width="80px" CssClass="LinkNormal OrangeColor" OnClick="DateDeliveredSort_Click" ID="LinkbuttonDateDelivered" Runat="server" style="white-space:nowrap">Date Delivered</asp:LinkButton>								
							</td>
							<td class="OrangeColor" style="width:110px">
								<asp:LinkButton width="110px" CssClass="LinkNormal OrangeColor" OnClick="CommentSort_Click" ID="LinkbuttonComment" Runat="server">Comment</asp:LinkButton>
							</td>
						</tr>
					</table>
				</HeaderTemplate>
				<AlternatingItemStyle CssClass="LinkNormal LightGray">
				</AlternatingItemStyle>
				<ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
				<ItemTemplate>
					<table border="0" width="100%" cellpadding="1" cellspacing="2">
						<tr>
							<td align="left" style="width:40px">
								<a style="width:40px" href='../administrator/Transaction_Details.aspx?Referer=/Hauler/In_Transit.aspx&ID=<%# DataBinder.Eval(Container.DataItem,"ID")%>'>
									Edit</a></td>
							<td align="left" style="width:120px">
								<a style="width:120px" href='../common/MB_Specs_User.aspx?ID=<%# DataBinder.Eval(Container.DataItem,"SHIPMENT_BUYR") %>'>
									<%# DataBinder.Eval(Container.DataItem,"VARBUYR")%>
								</a>
							</td>
							<td align="left" style="width:120px">
							    <a style="width:120px" href='../common/MB_Specs_User.aspx?ID=<%# DataBinder.Eval(Container.DataItem,"SHIPMENT_SELR" ) %>'>
									<%# DataBinder.Eval(Container.DataItem,"VARSELR")%>
								</a>
							</td>
							<td align="left" style="width:80px">
							    <a style="width:80px" href='/administrator/Transaction_Details.aspx?ID=<%# DataBinder.Eval(Container.DataItem,"ID") %>'>
									<%# DataBinder.Eval(Container.DataItem,"ID") %>
								</a>
							</td>
							<td align="left" style="width:150px">
							    <a style="width:150px" href='../common/MB_Specs_Location.aspx?ID=<%# DataBinder.Eval(Container.DataItem,"SHIPMENT_FROM")%>'>
									<%# DataBinder.Eval(Container.DataItem,"VARDELIVERY")%>
								</a>
							</td>
							<td align="left" style="width:150px">
							    <a style="width:150px" href='../common/MB_Specs_Location.aspx?ID=<%# DataBinder.Eval(Container.DataItem,"SHIPMENT_TO")%>'>
									<%# DataBinder.Eval(Container.DataItem,"VARORIGIN")%>
								</a>
							</td>
							<td align="center" style="width:50px">
							    <asp:DropDownList Width="50px" CssClass="InputForm" AutoPostBack="false" id="ddlStatusDL2" runat="server"></asp:DropDownList>
							</td>
							<td align="center" style="width:60px">
							    <asp:TextBox CssClass="InputForm" width="60px" runat="server" ID="TxtWeight" Text='<%# DataBinder.Eval(Container.DataItem,"SHIPMENT_WEIGHT" ) %>' />
							</td>
							<td align="center" style="width:80px">
							    <asp:TextBox CssClass="InputForm" textalign="center" width="80px" size="12" runat="server" ID="TxtDateTaken" Text='<%# DataBinder.Eval(Container.DataItem, "VARDATETAKEN") %>' />
								<asp:regularexpressionvalidator runat="server" controltovalidate="TxtDateTaken" display="dynamic" ValidationExpression="^(([0]?[0-9]|1[0-2])/([0]?[0-9]|[1-2][0-9]|[3][0-1])/[0-9][0-9][0-9][0-9])?$"
									errormessage="MM/DD/YYYY" />
							</td>
							<td align="center" style="width:80px">
							    <asp:TextBox CssClass="InputForm" textalign="center" width="80px" size="12" runat="server" ID="TxtDateDelivered" Text='<%# DataBinder.Eval(Container.DataItem,"VARDATEDELIVERED") %>' />
								<asp:regularexpressionvalidator runat="server" controltovalidate="TxtDateDelivered" display="dynamic" ValidationExpression="^(([0]?[0-9]|1[0-2])/([0]?[0-9]|[1-2][0-9]|[3][0-1])/[0-9][0-9][0-9][0-9])?$"
									errormessage="MM/DD/YYYY" ID="Regularexpressionvalidator3" NAME="Regularexpressionvalidator3" />
							</td>
							<td align="center" style="width:110px">
							    <asp:TextBox CssClass="InputForm" textalign="center" width="110px" size="17" runat="server" ID="TxtComment" Text='<%# DataBinder.Eval(Container.DataItem, "VARCOMMENT") %>' />
								<!--		<a  href='https://www.steelroads.com/servlet/SAServlet?CONTROLLER=ETScriptedAccessCtlr&ScriptedAccessType=Dynamic&ACTION=&LANGUAGE=en&ImportList&EquipmentList=<%# DataBinder.Eval(Container.DataItem,"VARCOMMENT") %>'> 
						<img border="0" runat="server" src="..\images\icons\train.gif"></a>
				-->
							</td>
						</tr>
				</ItemTemplate>
				<HeaderStyle Wrap="False" Width="100%" CssClass="LinkNormal"></HeaderStyle>
				<AlternatingItemTemplate>
					<tr bgcolor="#D9D7D3">
						<td align="left" style="width:40px">
							<a style="width:40px" href='../administrator/Transaction_Details.aspx?Referer=/Hauler/In_Transit.aspx&ID=<%# DataBinder.Eval(Container.DataItem,"ID")%>'>
								Edit</a></td>
						<td align="left" style="width:120px">
						    <a style="width:120px" href='../common/MB_Specs_User.aspx?ID=<%# DataBinder.Eval(Container.DataItem,"SHIPMENT_BUYR" ) %>'>
								<%# DataBinder.Eval(Container.DataItem,"VARBUYR")%>
							</a>
						</td>
						<td align="left" style="width:120px">
						    <a style="width:120px" href='../common/MB_Specs_User.aspx?ID=<%# DataBinder.Eval(Container.DataItem,"SHIPMENT_SELR" ) %>'>
								<%# DataBinder.Eval(Container.DataItem,"VARSELR")%>
							</a>
						</td>
						<td align="left" style="width:80px">
						    <a style="width:80px" href='/administrator/Transaction_Details.aspx?ID=<%# DataBinder.Eval(Container.DataItem,"ID") %>'>
								<%# DataBinder.Eval(Container.DataItem,"ID") %>
							</a>
						</td>
						<td align="left" style="width:150px">
						    <a style="width:150px" href='../common/MB_Specs_Location.aspx?ID=<%# DataBinder.Eval(Container.DataItem,"SHIPMENT_TO")%>'>
								<%# DataBinder.Eval(Container.DataItem,"VARDELIVERY")%>
							</a>
						</td>
						<td align="left" style="width:150px">
						    <a style="width:150px" href='../common/MB_Specs_Location.aspx?ID=<%# DataBinder.Eval(Container.DataItem,"SHIPMENT_FROM")%>'>
								<%# DataBinder.Eval(Container.DataItem,"VARORIGIN")%>
							</a>
						</td>
						<td align="center" style="width:50px">
						    <asp:DropDownList CssClass="InputForm" Width="50px" id="ddlStatusDL2" AutoPostBack="false" runat="server" />
						</td>
						<td align="center" style="width:60px">
						    <asp:TextBox CssClass="InputForm" textalign="center" runat="server" ID="TxtWeight" Width="60px" Text='<%# DataBinder.Eval(Container.DataItem,"SHIPMENT_WEIGHT" ) %>' />
						</td>
						<td align="center" style="width:80px">
						    <asp:TextBox CssClass="InputForm" textalign="center" width="80px" size="12" runat="server" ID="TxtDateTaken" Text='<%# DataBinder.Eval(Container.DataItem, "VARDATETAKEN") %>' />
							<asp:regularexpressionvalidator runat="server" controltovalidate="TxtDateTaken" display="dynamic" ValidationExpression="^(([0]?[0-9]|1[0-2])/([0]?[0-9]|[1-2][0-9]|[3][0-1])/[0-9][0-9][0-9][0-9])?$"
								wrap="false" valign="top" errormessage="MM/DD/YYYY" ID="Regularexpressionvalidator1" NAME="Regularexpressionvalidator1" />
						</td>
						<td align="center" style="width:80px">
						    <asp:TextBox CssClass="InputForm" textalign="center" Width="80px" size="12" runat="server" ID="TxtDateDelivered" Text='<%# DataBinder.Eval(Container.DataItem,"VARDATEDELIVERED") %>' />
							<asp:regularexpressionvalidator runat="server" controltovalidate="TxtDateDelivered" display="dynamic" ValidationExpression="^(([0]?[0-9]|1[0-2])/([0]?[0-9]|[1-2][0-9]|[3][0-1])/[0-9][0-9][0-9][0-9])?$"
								wrap="false" valign="top" errormessage="MM/DD/YYYY" ID="Regularexpressionvalidator2" NAME="Regularexpressionvalidator2" />
						</td>
						<td align="center" style="width:110px">
						    <asp:TextBox CssClass="InputForm" textalign="center" Width="110px" size="17" runat="server" ID="TxtComment" Text='<%# DataBinder.Eval(Container.DataItem, "VARCOMMENT") %>' />
							<!--		<a  href='https://www.steelroads.com/servlet/SAServlet?CONTROLLER=ETScriptedAccessCtlr&ScriptedAccessType=Dynamic&ACTION=&LANGUAGE=en&ImportList&EquipmentList=<%# DataBinder.Eval(Container.DataItem,"VARCOMMENT") %>'>
						<img border="0" runat="server" src="..\images\icons\train.gif"></a>
						
				-->
						</td>
					</tr>
	</table>
	</AlternatingItemTemplate> </asp:datalist></td></tr>
	<tr>
		<td align="center"><asp:button class="tpebutton" id="bUpdate" onclick="bUpdate_Click" runat="server" align="center"
				text="Update"></asp:button></td>
	</tr></table>
	
</asp:Content>
