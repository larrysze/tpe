<%@ Page Language="C#" %>
<%@ Register TagPrefix="TPE" TagName="Instructions" Src="/include/ScreenInstructions.ascx" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<script runat="server">

    /************************************************************************
     *   1. File Name       :Forward\Forward_Floor2.aspx                     *
     *   2. Description     :Show forward prices for one contract            *
     *   3. Modification Log:                                                *
     *     Ver No.       Date          Author             Modification       *
     *   -----------------------------------------------------------------   *
     *      1.00      2-25-2004      Zach                Comment             *
     *                                                                       *
     ************************************************************************/
     public void Page_Load(object sender, EventArgs e){
        if ((string)Session["Typ"] == null ){
          // Response.Redirect("../default.aspx");
        }
        if (!IsPostBack){
            SqlConnection conn;
            conn = new SqlConnection(Application["DBconn"].ToString());
            conn.Open();
    
             // opening contract list
            SqlCommand cmdContract;
            SqlDataReader dtrContract;
    
            //binding company dd list
            if (Request.QueryString["ID"] != null){
              // add the preselected one
              cmdContract = new SqlCommand("SELECT CONT_ID,CONT_LABL FROM Contract WHERE CONT_ID =" +Request.QueryString["ID"].ToString() ,conn);
              dtrContract = cmdContract.ExecuteReader();
              dtrContract.Read();
              ddlProduct.Items.Add ( new ListItem ((string) dtrContract["CONT_LABL"],dtrContract["CONT_ID"].ToString() ) );
              dtrContract.Close();
            }
            cmdContract = new SqlCommand("SELECT CONT_ID,CONT_LABL FROM Contract ORDER BY CONT_ORDR ASC",conn);
            dtrContract = cmdContract.ExecuteReader();
            while (dtrContract.Read()){
                    ddlProduct.Items.Add ( new ListItem ((string) dtrContract["CONT_LABL"],dtrContract["CONT_ID"].ToString() ) );
            }
            dtrContract.Close();
            conn.Close();
            Data_Bind();
        }
      }
       // <summary>
      //   Handler for resets the data grid
      // </summary>
      public void Reset_Grid(object sender, EventArgs e){
        Data_Bind();
      }
      private void Data_Bind(){
    
          SqlDataAdapter dadContent;
          DataSet dstContent;
          SqlConnection conn;
          conn = new SqlConnection(Application["DBconn"].ToString());
          conn.Open();
          StringBuilder sbSQL = new StringBuilder();
    
          sbSQL.Append("  Select FWD_ID, MONTH= FWD_MNTH+' '+RIGHT(FWD_YEAR,2),  ");
          sbSQL.Append("      QTYBID =(SELECT SUM(BID_QTY) FROM BID WHERE BID_CONT='"+ddlProduct.SelectedItem.Value.ToString()+"' AND BID_MNTH=FWD_ID AND BID_SIZE='1'), ");
          sbSQL.Append("      BID = (SELECT RIGHT(CONVERT(varchar,MAX(BID_PRCE)),4) FROM BID WHERE BID_CONT='"+ddlProduct.SelectedItem.Value.ToString()+"' AND BID_MNTH=FWD_ID AND BID_SIZE='1'),  ");
          sbSQL.Append("      OFFR = (SELECT RIGHT(CONVERT(varchar,MAX(OFFR_PRCE)),4) FROM OFFER WHERE OFFR_CONT='"+ddlProduct.SelectedItem.Value.ToString()+"' AND OFFR_MNTH=FWD_ID AND OFFR_SIZE='1'), ");
          sbSQL.Append("      QTYOFFR = (SELECT SUM(OFFR_QTY) FROM OFFER WHERE OFFR_CONT='"+ddlProduct.SelectedItem.Value.ToString()+"' AND OFFR_MNTH=FWD_ID AND OFFR_SIZE='1') ");
          sbSQL.Append("  From FWDMONTH WHERE FWD_ACTV ='1' ");
    
          dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
          dstContent = new DataSet();
          dadContent.Fill(dstContent);
          dg.DataSource = dstContent;
          dg.DataBind();
          conn.Close();
    
     }

</script>
<form id="Form" runat="server">
        <TPE:Template PageTitle="Forward Floor" Runat="Server" />
            <asp:ValidationSummary runat="server" HeaderText="There are problems with the following fields. Please correct the errors below." />
<TPE:Instructions Text="Here on our Forward Market page, you can get forward resin pricing to help anticipate future needs or to protect yourself against uncertain price fluctuations that may occur. You can also use this pricing data to help predict future spot prices. Please contact us for more detailed information regarding our Forward Markets." Runat="Server" />
                    <BR><BR>

            <table border="0" align="center"">
                <tr>
                    <td align="left">


                    <span class="PageHeader">Foward Prices </span><font color="#CC6600"><B> By Grade</B></font><BR>
                        Product:
                        <asp:DropDownList id="ddlProduct" runat="server"
                            AutoPostBack="true" OnSelectedIndexChanged="Reset_Grid" />

                    </td>
                    <td align="right">
                        <img src="/images/misc/trainload.gif" border=0>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td valign="top" colspan="2">
                        <asp:DataGrid id="dg" runat="server"
                            AutoGenerateColumns="false"
                            CellPadding="2"
                            DataKeyField="FWD_ID"
                            EditItemStyle-BackColor="Silver"
                            Width="550px"
                            ShowFooter="false"
                            CssClass = "DataGrid"
                            ItemStyle-CssClass="DataGridRow"
                            AlternatingItemStyle-CssClass = "DataGridRow_Alternate"
                            HeaderStyle-CssClass="DataGridHeader" >
                           <Columns>

                                <asp:BoundColumn
                                    DataField="Month" HeaderText="Contract" ReadOnly="True"
                                    ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left" />

                                 <asp:TemplateColumn HeaderText="Qty">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYBID") %>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:CompareValidator ControlToValidate="txtQTYBID" ErrorMessage="Silly rabit, quantity must be an Integer."
                                             Display="none" Operator="DataTypeCheck" Type="Integer" Runat="server" />
                                        <asp:TextBox id="txtQTYBID" Size="1" maxlength="2" Text='<%# DataBinder.Eval(Container, "DataItem.QTYBID") %>' runat="server" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtQTYBID" Display="none" ErrorMessage="Bid quantity can not be blank" />
                                        <asp:RangeValidator ControlToValidate="txtQTYBID" runat="server" ErrorMessage="Bid quantity must be greater than one."
                                            Display="none" type="Integer" MinimumValue ="1" MaximumValue = "100" />
                                    </EditItemTemplate>
                                 </asp:TemplateColumn>

                                 <asp:TemplateColumn HeaderText="Bid">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BID") %>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:CompareValidator ControlToValidate="txtBID" ErrorMessage="Silly rabit, bid must be an Integer."
                                             Display="none" Operator="DataTypeCheck" Type="Double" Runat="server" />
                                        <asp:TextBox Size="1" id="txtBID" maxlength="4" Text='<%# DataBinder.Eval(Container, "DataItem.BID") %>' runat="server" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtBID" Display="none" ErrorMessage="Bid price can not be blank" />
                                        <asp:RangeValidator ControlToValidate="txtBID" runat="server" ErrorMessage="Invalid bid amount."
                                            Display="none" type="Double" MinimumValue ="0" MaximumValue = ".999" />
                                    </EditItemTemplate>
                                 </asp:TemplateColumn>

                                 <asp:TemplateColumn HeaderText="Ask">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OFFR") %>' />
                                    </ItemTemplate>

                                    <EditItemTemplate>
                                        <asp:CompareValidator ControlToValidate="txtOFFER" ErrorMessage="Silly rabit, offer must be an Integer."
                                             Display="none" Operator="DataTypeCheck" Type="Double" Runat="server" />
                                        <asp:TextBox Size="1" id="txtOFFER" maxlength="4" Text='<%# DataBinder.Eval(Container, "DataItem.OFFR") %>' runat="server" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtOFFER" Display="none" ErrorMessage="Offer price can not be blank" />
                                        <asp:RangeValidator ControlToValidate="txtOFFER" runat="server" ErrorMessage="Invalid offer amount."
                                            Display="none" type="Double" MinimumValue ="0" MaximumValue = ".999" />
                                    </EditItemTemplate>
                                 </asp:TemplateColumn>

                                 <asp:TemplateColumn HeaderText="Qty">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYOFFR") %>' />
                                    </ItemTemplate>

                                    <EditItemTemplate>
                                        <asp:CompareValidator ControlToValidate="txtQTYOFFER" ErrorMessage="Silly rabit, quantity must be an Integer."
                                             Display="none" Operator="DataTypeCheck" Type="Integer" Runat="server" />
                                        <asp:TextBox Size="1" id="txtQTYOFFER" maxlength="2" Text='<%# DataBinder.Eval(Container, "DataItem.QTYOFFR") %>' runat="server" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtQTYOFFER" Display="none" ErrorMessage="Offer quantity can not be blank" />
                                        <asp:RangeValidator ControlToValidate="txtQTYOFFER" runat="server" ErrorMessage="Offer quantity must be greater than one."
                                            Display="none" type="Integer" MinimumValue ="1" MaximumValue = "100" />

                                    </EditItemTemplate>
                                 </asp:TemplateColumn>



                            </Columns>
                        </asp:DataGrid>

                     </td>
                     <!--
                     <td valign="top">


                        <table border=0 cellpadding=0 cellspacing=0 bgcolor=#E8E8E8 width=231>



	                       <tr>
		                      <td bgcolor=black CLASS=S5 colspan=3><img src=/images/1x1.gif width=3>Questions,&nbsp;<a href="mailto:info@ThePlasticsExchange.com">email</a> or call 1-800-850-2380&nbsp;
		                      </td>
	                       </tr>
	                       <tr>
		                      <td width=10 rowspan=3><img width=10 src=../images/1x1.gif>
		                      </td>
		                      <td><img height=10 src=../images/1x1.gif>
		                      </td>
		                      <td width=10 rowspan=3><img width=10 src=../images/1x1.gif>
		                      </td>
	                       </tr>

	                       <tr bgcolor=#E8E8E8>
		                      <td CLASS=S1 align=left>
                                <li><a href=/docs/userguide.pdf target="_blank" Class=S1>User Guide</a></li>
                                <%if (Session["Typ"].ToString()=="S"){%>
                                <li><a href=/docs/seller.pdf target="_blank" Class=S1>Seller's Agreement (pdf)</a></li>
                                <li><a href=/docs/Seller_Agreement_NewWind.aspx target="_blank" Class=S1>Seller's Agreement (html)</a></li>
                                <%}else if (Session["Typ"].ToString()=="P"){%>
                                <li><a href=/docs/buyer.pdf target="_blank" Class=S1>Buyer's Agreement (pdf)</a></li>
                                <li><a href=/docs/Buyer_Agreement_NewWind.aspx target="_blank" Class=S1>Buyer's Agreement (html)</a></li>

                                <%}%>
                                <li><a href=javascript:sealWin=window.open("/docs/Rules_of_the_Exchange4PopUp.aspx","win",'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=700,height=500');void(0) Class=S1>Rules Of The Exchange</a></li>
		                      </td>
	                       </tr>
                        </table><br>
                        <table border=0 cellpadding=0 cellspacing=0 bgcolor=#E8E8E8 width=231>
	                       <tr>
		                      <td bgcolor=black CLASS=S5 colspan=3><img src=/images/1x1.gif width=3>Instructions&nbsp;
		                      </td>
	                       </tr>
	                       <tr>
		                      <td width=10 rowspan=3><img width=10 src=../images/1x1.gif>
		                      </td>
		                      <td><img height=10 src=../images/1x1.gif>
		                      </td>
		                      <td width=10 rowspan=3><img width=10 src=../images/1x1.gif>
		                      </td>
	                       </tr>
	                       <tr bgcolor=#E8E8E8>
		                      <td CLASS=S9 align=left>
                                <li>Click on a contract to get the ACTION BOX.</li>
                                <li>Check your working and filled orders<br>
                                &nbsp;&nbsp;from the action box only for that contract
                                &nbsp;&nbsp;or from the top bar for all contracts.
                                </li>
                                <li>View your <a href="/Common/System_Messages.asp" class=S2><u>System Messages</u></a> for permanent records.</li>
                                <%if (Session["Typ"].ToString()=="S"){%>
                                <li>Selecting different cities or shipping causes the prices to change.</li>
                                <li>Click <a href="/Supplier/Supplier_Exchange_Filters.aspx" class=S2><u>Preferences</u></a> to customize your quotes or add new locations.</li>
                                <%}else if (Session["Typ"].ToString()=="P"){%>
                                <li>Selecting different destination cities causes the delivered prices to change.</li>
                                <li>Click <a href="/Purchaser/Purchaser_Exchange_Filters.aspx" class=S2><u>Preferences</u></a> to customize your quotes or add new locations.</li>
                                <%}%>
                              </td>
                            </tr>
                          </table>



                     </td> -->
                 </tr>
                 <tr>
                    <td colspan="2">
                        <img name=imgChart src='../common/ChartGif2.asp?resin=<% Response.Write(ddlProduct.SelectedItem.Value.ToString());%>'>

                    </td>
                 </tr>

               </table>


         <TPE:Template Footer="True" Runat="Server" />

        </form>
