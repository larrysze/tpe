<%@ Page Language="VB" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Web.Mail" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>

<form runat="server" id="Form">
    <TPE:Template PageTitle="Place Offer" Runat="Server" />
<!--#include FILE="../include/VBLegacy_Code.aspx"-->

<!--#INCLUDE FILE="../include/Loading.inc"-->
<!--#INCLUDE FILE="../include/PopUp.inc"-->

<%

IF Session("Typ")<>"A" AND Session("Typ")<>"B" THEN Response.Redirect ("../default.aspx")
'Database connection'
Dim conn as SqlConnection
Dim cmdContent as SQLCommand
Dim Rec0 as SqlDataReader

conn = new SqlConnection(Application("DBconn"))
conn.Open()

Dim conn1 as SqlConnection
conn1 = new SqlConnection(Application("DBconn"))
conn1.Open()
Dim Rec1 as SqlDataReader
Dim conn2 as SqlConnection
conn2 = new SqlConnection(Application("DBconn"))
conn2.Open()
Dim Rec2 as SqlDataReader
Dim connP as SqlConnection
connP = new SqlConnection(Application("DBconn"))
connP.Open()
Dim RecP as SqlDataReader
Dim connL as SqlConnection
connL = new SqlConnection(Application("DBconn"))
connL.Open()
Dim RecL as SqlDataReader
Dim connM as SqlConnection
connM = new SqlConnection(Application("DBconn"))
connM.Open()
Dim RecM as SqlDataReader

DIM Quantity,Str,Str0,Str1,Str2,Str3,Str4,Locl,i,j,k,l,m,n,o,p,Height,StrA,StrB,z,StrPA,StrPB,bpe,comp,pers
DIM arBA(5), ind	' array for Bids & Asks

IF Request.Form("Valid")<>"" THEN
	'Database Lock
	Application.Lock()
	Application("Flag")=1

	'Locl(0):Locality
	'Locl(1):Place
	Locl=Split(Request.Form("Place"),",")

	'Offer Creation:
	Str = "Exec spTransactionEngine_S @CompId="&Request.Form("Seller")&", @PersId="&Request.Form("PSeller")&", @ContId="&Request.Form("Contract")&", @Qty="&Request.Form("Quantity")&", @Price=0."&Request.Form("Price")&", @Size=1, @Locl0="&Locl(0)&", @Locl1="&Locl(1)&",  @FOB="&Request.Form("FOB")&", @Brand="&Request.Form("Brand")&", @Mess='"&Request.Form("Mess")&"', @Month="&Request.Form("month")
	IF Request.Form("IdOrder")<>"" THEN
		Str = Str + ", @IdOrder="&Request.Form("IdOrder")
	ELSEIF Request.Querystring("IdOrder")<>"" THEN
		Str = Str + ", @IdOrder="&Request.Querystring("IdOrder")
	END IF
		Str = Str & ", @Gene=1"

	cmdContent= new SqlCommand(Str, conn)
	cmdContent.ExecuteNonQuery()
	'Database Unlock
	Application("Flag")=0
	Application.UnLock()
	Response.Redirect ("/Common/Common_Working_Orders.aspx")
END IF
Height=260


'from adminoffer.inc
cmdContent= new SqlCommand("SELECT COMP_ID,COMP_NAME FROM COMPANY WHERE COMP_TYPE IN ('S','D') AND COMP_ENBL=1 AND COMP_ID IN (SELECT DISTINCT PERS_COMP FROM PERSON WHERE PERS_TYPE IN ('S','D') AND PERS_ENBL=1) ORDER BY COMP_NAME", connP)
RecP= cmdContent.ExecuteReader()
RecP.Read

StrA=""
StrB=""
IF Request.Form("Seller")<>"" THEN
	z=Request.Form("Seller")
ELSEIF Request.Querystring("IdOrder")<>"" THEN
        cmdContent= new SqlCommand("SELECT OFFR_SELR,(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=OFFR_SELR),(SELECT PERS_FRST_NAME+' '+PERS_LAST_NAME FROM PERSON WHERE PERS_ID=OFFR_SELR),(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=OFFR_SELR)) FROM OFFER WHERE OFFR_ID="&Request.Querystring("IdOrder"), connL)
	RecL= cmdContent.ExecuteReader()
        RecL.Read
	z=RecL(1)
	pers=RecL(2)
	comp=RecL(3)
	RecL.Close

ELSEIF Request.Form("IdOrder")<>"" THEN
	cmdContent= new SqlCommand("SELECT OFFR_SELR,(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=OFFR_SELR),(SELECT PERS_FRST_NAME+' '+PERS_LAST_NAME FROM PERSON WHERE PERS_ID=OFFR_SELR),(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=OFFR_SELR)) FROM OFFER WHERE OFFR_ID="&Request.Form("IdOrder"), connL)
	RecL= cmdContent.ExecuteReader()
        RecL.Read
	z=RecL(1)
	pers=RecL(2)
	comp=RecL(3)
	RecL.Close

ELSE
	z=RecP("COMP_ID")
END IF
RecP.Close
cmdContent= new SqlCommand("SELECT COMP_ID,COMP_NAME FROM COMPANY WHERE COMP_TYPE IN ('S','D') AND COMP_ENBL=1 AND COMP_ID IN (SELECT DISTINCT PERS_COMP FROM PERSON WHERE PERS_TYPE IN ('S','D') AND PERS_ENBL=1) ORDER BY COMP_NAME", connP)
RecP= cmdContent.ExecuteReader()
While RecP.Read()
	StrA=StrA+"'"&RecP("COMP_ID")&"',"
	StrB=StrB+"'"&RecP("COMP_NAME")&"',"

End While
StrA=Left(StrA,Len(StrA)-1)&"),new Array("&Left(StrB,Len(StrB)-1)
RecP.Close



cmdContent= new SqlCommand("SELECT PERS_ID,(SELECT PERS_FRST_NAME+' '+PERS_LAST_NAME) FROM PERSON WHERE PERS_TYPE='S' AND PERS_ENBL=1 AND PERS_COMP="&z, connP)
RecP= cmdContent.ExecuteReader()
RecP.Read
StrPA=""
StrPB=""
IF Request.Form("PSeller")<>"" AND Request.Form("PSeller")<>0 THEN
	bpe=Request.Form("PSeller")
	cmdContent= new SqlCommand("SELECT PERS_FRST_NAME+' '+PERS_LAST_NAME,(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID="+bpe+")) FROM PERSON WHERE PERS_ID="&bpe, connL)
	RecL= cmdContent.ExecuteReader()
        RecL.Read
	pers=RecL(0)
	comp=RecL(1)
	RecL.Close

ELSEIF Request.Querystring("IdOrder")<>"" THEN
	cmdContent= new SqlCommand("SELECT OFFR_SELR FROM OFFER WHERE OFFR_ID="&Request.Querystring("IdOrder"), connL)
	RecL= cmdContent.ExecuteReader()
	RecL.Read
	bpe=RecL(0)
	RecL.Close

ELSEIF Request.Form("IdOrder")<>"" THEN
	cmdContent= new SqlCommand("SELECT OFFR_SELR FROM OFFER WHERE OFFR_ID="&Request.Form("IdOrder"), connL)
	RecL= cmdContent.ExecuteReader()
	RecL.Read
	bpe=RecL(0)
	RecL.Close

ELSEIF RecP.Hasrows=0 THEN
	bpe="0"
	StrPA="'0',"
	StrPB="'No Purchasers',"
ELSE
	bpe=RecP("PERS_ID")
END IF
RecP.Close
cmdContent= new SqlCommand("SELECT PERS_ID,(SELECT PERS_FRST_NAME+' '+PERS_LAST_NAME) FROM PERSON WHERE PERS_TYPE='S' AND PERS_ENBL=1 AND PERS_COMP="&z, connP)
RecP= cmdContent.ExecuteReader()
While RecP.Read()
	StrPA=StrPA+"'"&RecP(0)&"',"
	StrPB=StrPB+"'"&RecP(1)&"',"

End While

StrPA=Left(StrPA,Len(StrPA)-1)&"),new Array("&Left(StrPB,Len(StrPB)-1)
RecP.Close


cmdContent= new SqlCommand("SELECT PLAC_ID, PLAC_NAME=(CASE WHEN PLAC_TYPE='W' THEN ((SELECT '(W)'+LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL)) ELSE (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) END),PLAC_LOCL,PLAC_ADDR_ONE=(CASE WHEN PLAC_ADDR_ONE IS NULL THEN 1 ELSE 0 END) FROM PLACE WHERE (PLAC_COMP="+z.ToString()+" AND PLAC_TYPE='S' AND PLAC_ENBL=1) OR PLAC_TYPE='W' ORDER BY PLAC_TYPE", conn1)

Rec1= cmdContent.ExecuteReader()
Rec1.Read

IF Rec1.HasRows=0 THEN
Session("Message")="Your officer needs to select shipping locations<br><font color=red>Need help? Call us 1-800-850-2380"
Response.redirect ("../Common/MB_Message.aspx")
End IF
cmdContent= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE POWER(2,CONT_ID-1)&(SELECT COMP_PREF FROM COMPANY WHERE COMP_ID="+z.ToString()+")<>0 AND POWER(2,CONT_ID-1)&(SELECT PERS_PREF FROM PERSON WHERE PERS_ID="+bpe.ToString()+")<>0 ORDER BY CONT_ORDR", conn)
Rec0= cmdContent.ExecuteReader()
Rec0.Read

IF Request.Form("IdOrder")<>"" THEN
cmdContent= new SqlCommand("SELECT *,CONT_LABL=(SELECT CONT_LABL FROM CONTRACT WHERE CONT_ID=OFFR_CONT) FROM OFFER WHERE OFFR_ID="&Request.Form("IdOrder"), conn2)
Rec2= cmdContent.ExecuteReader()
Rec2.Read

ELSEIF Request.Querystring("IdOrder")<>"" THEN
	cmdContent= new SqlCommand("SELECT *,CONT_LABL=(SELECT CONT_LABL FROM CONTRACT WHERE CONT_ID=OFFR_CONT) FROM OFFER WHERE OFFR_ID="&Request.Querystring("IdOrder"), conn2)
        Rec2= cmdContent.ExecuteReader()
	Rec2.Read

ELSE
	IF Rec0.Hasrows=0 THEN
	Session("Message")="You haven't defined your contracts."
	Response.Redirect ("../Common/MB_Message.aspx")
	End IF
END IF

IF Request.Form("Contract")<>"" THEN
	i=Request.Form("Contract")
	o=CDbl(Request.Form("Brand"))
ELSEIF Request.Form("IdOrder")<>"" OR Request.Querystring("IdOrder")<>"" THEN
	i=Rec2("OFFR_CONT")
	o=CDbl(Rec2("OFFR_BRAN"))
ELSE
	i=Rec0("CONT_ID")
	o=0
END IF

IF Request.Form("Place")<>"" THEN
	k=Split(Request.Form("Place"),",")
	j=k(0)
	Locl=j.ToString()+","&k(1)
ELSEIF Request.Form("Place")<>"" AND Request.Form("Price")="" THEN
	j=Request.Form("Place")
	Locl=j.ToString()+","
ELSEIF Request.Form("Price")<>"" AND Request.Form("Place")<>"" THEN
	k=Split(Request.Form("Place"),",")
	j=k(0)
	Locl=j.ToString()+","&k(1)
ELSEIF Request.Form("IdOrder")<>"" OR Request.Querystring("IdOrder")<>"" THEN
	j=Rec2("OFFR_FROM_LOCL")
	Locl=j.ToString()+","&Rec2("OFFR_FROM")
ELSE
	j=Rec1("PLAC_LOCL").ToString()
	Locl=j.ToString()+","&Rec1("PLAC_ID")
END IF

Str0=""
Str1=""
Rec0.Close
cmdContent= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE POWER(2,CONT_ID-1)&(SELECT COMP_PREF FROM COMPANY WHERE COMP_ID="+z.ToString()+")<>0 AND POWER(2,CONT_ID-1)&(SELECT PERS_PREF FROM PERSON WHERE PERS_ID="+bpe.ToString()+")<>0 ORDER BY CONT_ORDR", conn)
Rec0= cmdContent.ExecuteReader()
While Rec0.Read()
	Str0=Str0+"'"&Rec0("CONT_ID")&"',"
	Str1=Str1+"'"&Rec0("CONT_LABL")&"',"

End While
Str0=Left(Str0,Len(Str0)-1)&"),new Array("&Left(Str1,Len(Str1)-1)
Rec0.Close


k=Rec1("PLAC_LOCL")
l=0
Str1=""
Str2=""
Rec1.Close
'cmdContent= new SqlCommand("SELECT PLAC_ID, PLAC_LABL,PLAC_NAME= (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) ,PLAC_LOCL,PLAC_ADDR_ONE=(CASE WHEN PLAC_ADDR_ONE IS NULL THEN 1 ELSE 0 END),(select LOCL_CITY FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) AS CITY, (select LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) AS STATE FROM PLACE WHERE (PLAC_COMP="+z.ToString()+" AND PLAC_TYPE='S' AND PLAC_ENBL=1) OR PLAC_TYPE='W' ORDER BY PLAC_TYPE", conn1)
cmdContent= new SqlCommand("SELECT PLAC_ID, PLAC_NAME=(CASE WHEN PLAC_TYPE='W' THEN ((SELECT '(W)'+LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL)) ELSE (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) END),PLAC_LOCL,PLAC_ADDR_ONE=(CASE WHEN PLAC_ADDR_ONE IS NULL THEN 1 ELSE 0 END) FROM PLACE WHERE (PLAC_COMP="+z.ToString()+" AND PLAC_TYPE='S' AND PLAC_ENBL=1) OR PLAC_TYPE='W' ORDER BY PLAC_TYPE", conn1)

Rec1= cmdContent.ExecuteReader()

While Rec1.Read()
	Str1=Str1+"'"&Rec1("PLAC_LOCL")&","&Rec1("PLAC_ID")&"',"
	l=l*CByte(CInt(Rec1("PLAC_LOCL"))=Cint(k))/255+1
	k=Rec1("PLAC_LOCL")

	IF l>1 THEN Str2=Str2+"'"&Rec1("PLAC_NAME")&" ("+l.ToString()+")'," ELSE IF Rec1("PLAC_ADDR_ONE")="1" THEN Str2=Str2+"'"&Rec1("PLAC_NAME")&" (U)'," ELSE Str2=Str2+"'"&Rec1("PLAC_NAME")&"',"

End While
Str1=Left(Str1,Len(Str1)-1)&"),new Array("&Left(Str2,Len(Str2)-1)
Rec1.Close

        cmdContent= new SqlCommand("SELECT BRAN_ID,BRAN_NAME FROM BRAND WHERE BRAN_CONT="+i.ToString()+" AND BRAN_ENBL=1 ORDER BY BRAN_NAME", conn)
        Rec0= cmdContent.ExecuteReader()

Str2=""
Str3=""
' 1-1-04 zach -  Commented out to default to TPE Prime
'Str2="'0',"
'Str3="'Select a brand',"
While Rec0.Read()
	Str2=Str2+"'"&Rec0("BRAN_ID")&"',"
	Str3=Str3+"'"&Rec0("BRAN_NAME")&"',"

End While
Str2=Left(Str2,Len(Str2)-1)&"),new Array("&Left(Str3,Len(Str3)-1)
Rec0.Close



'end of adminoffer.inc
%>



<%
'get the value of front month
Dim cmonth,StM0,StM1
cmdContent= new SqlCommand("SELECT FWD_ID,FWDLABL=(FWD_MNTH+' '+RIGHT(CONVERT(varchar,FWD_YEAR),2)) FROM FWDMONTH WHERE FWD_ACTV=1 ORDER BY FWD_ID", conn)
RecM= cmdContent.ExecuteReader()
RecM.Read
IF Request.Form("month")<>"" THEN
	cmonth=Request.Form("month")
ELSEIF Request.Form("IdOrder")<>"" OR Request.Querystring("IdOrder")<>"" THEN
	cmonth=Rec2("offr_mnth")
ELSE
	cmonth=RecM(0)
END IF
StM0=""
StM1=""
While RecM.Read()
	StM0=StM0+"'"&RecM("FWD_ID")&"',"
	StM1=StM1+"'"&RecM("FWDLABL")&"',"

End While
StM0=Left(StM0,Len(StM0)-1)&"),new Array("&Left(StM1,Len(StM1)-1)
RecM.Close

%>



<SPAN ID=Main name=Main >
<script>
<%IF NOT MSIE THEN%>
function noclick(scx)
{
	if (scx.which != 1) return false
	else if(scx.target.name=='Size')
		Upd(1)
}
<%END IF%>
function Trim(Para,Flag)
{
	Arr=new Array('0','0','0')
	for(i=0;i<Para.value.length;i++)
		if(Para.value.charAt(i)<'0'||Para.value.charAt(i)>'9')
			break
		else
			Arr[i]=Para.value.charAt(i)
	Val=(i==Para.value.length)?((Arr[0]=='0')?((Arr[1]=='0')?parseInt(Arr[2]):parseInt(Arr[1]+Arr[2])):parseInt(Arr[0]+Arr[1]+Arr[2])):0
	if(Flag==1)
		{if(Val<999) Val+=1}
	else if(Val>0)
		Val-=1
	Para.value=("000"+Val).substr(("000"+Val).length-3,3)
}
function Check()
{
		with(<%Response.Write (N1)%>Form)
	{
		Qty=Quantity.options[Quantity.options.selectedIndex].text

		if(Price.value==0)	return Path.Mess(window,'Please enter a selling price.','0')
		if (Qty=="0")		return Path.Mess(window,'Please enter the number of contracts you wish to sell.','0')
		if (Place.options[Place.options.selectedIndex].text.substring(Place.options[Place.options.selectedIndex].text.length-3,Place.options[Place.options.selectedIndex].text.length)=="(U)")
		return Path.Mess(window,'You have selected an undefined location, please define this location on the <a href=../Officer/Officer_Add_Location.asp>Add Location</a> page before placing an order.','0')
		if (Price.value=="")	return Path.Mess(window,'Please enter a valid price.','0')
		if (Brand.value=="0")	return Path.Mess(window,'Please select a brand.','0')
		for(j=0;j<Price.value.length;j++)
			if (Price.value.charAt(j)<"0"||Price.value.charAt(j)>"9")
				return Path.Mess(window,'Please, enter a valid price.','0')

		Val=eval("parseFloat(Price.value)/(Math.pow(10,Price.value.length-3))*Qty")
		Prce=new String(Math.round(eval("190*100*Val"))/100)
		Valid.value=Prce
		t=0
		for(t=0;t<Prce.length;t++)
			if(Prce.charAt(t)=='.')
				break;
		t=Prce.length-t
		for(i=3+t;Prce.length-i>0;i+=4)
			Prce=Prce.substring(0,Prce.length-i)+","+Prce.substring(Prce.length-i,Prce.length)
		Str=new Array('Your are going to place an order to sell',' '+Qty+(' Rail Car')+(eval("Qty>1")?'s':''),' of ',Contract.options[Contract.options.selectedIndex].text,' for ','$ .'+Price.value,' per pound.\n\n','Picked up at ',Place.options[Place.options.selectedIndex].text,' in ',month.options[month.options.selectedIndex].text ,'.\n\n Estimate value $',Prce)
		Mess.value=Str.join('').substring(32,Str.join('').length)
		Str1='<font color=red>'
		Path.Mess(window,Str[0]+':<br>'+Str1+Str[1]+'</font>'+Str[2]+Str1+Str[3]+'</font><br>'+Str[4]+Str1+Str[5]+'</font>'+Str[6]+'<br>'+Str[7]+Str1+Str[8]+'</font>'+Str[9]+'<br>'+Str1+Str[10]+'</font>'+Str[11]+Str1+Str[12]+'</font>','1')
	}
}
function Answer(Flag)
{
	with(<%Response.Write (N1)%>Form)
		if(Flag)
		{
			Submit.value='Submit'
			submit()
		}
		else
			Valid.value=''
}
function Swtch()
  {
	with(document.Form)

			FOBImg.src='../images/misc/Delivery.gif'
  }
function Upd(Flag)
	{
	with(<%Response.Write (N1)%>Form) Price.value=((Flag==1)?Bid:Ask)
	}
</script>

<input type=hidden name=FOB value='<%
IF Request.Form("FOB")<>"" THEN
	Response.write (Request.Form("FOB"))
ELSEIF Request.Form("IdOrder")<>"" AND Request.Form("Price")="" THEN

	Response.write (Math.Abs(Cbyte(Rec2("OFFR_FOB")))/255)
ELSE
	Response.write ("1")
END IF
%>'>
<input type=hidden name=Valid value=''>
<input type=hidden name=Mess value=''>
<input type=hidden name=Submit value=''>
<input type=hidden name=IdOrder value='<%=Request.Form("IdOrder")%>'>
<table cellspacing=0 cellpadding=0 border=0>
	<tr>
		<td height=6>
		</td>
	</tr>
	<tr>
		<td align=left>
			<table border=1 bordercolor=black height=68 cellspacing=0 cellpadding=0>
				<tr valign=middle bgcolor=#cccccc height=20>
					<td CLASS=S1>&nbsp;&nbsp;<%IF Request.Form("IdOrder")<>"" THEN%>Change Offer #<%=Request.Form("IdOrder")%><%ELSEIF Request.Querystring("IdOrder")<>"" THEN%>Change Offer #<%=Request.Querystring("IdOrder")%><%ELSE%>Seller<%END IF%>
					</td>
				</tr>
				<tr>
					<td>
Company:
<%IF Request.Form("IdOrder")<>"" OR Request.Querystring("IdOrder")<>"" THEN%>
<input type=hidden name=Seller value='<%=z%>'>
<input type=hidden name=PSeller value='<%=bpe%>'>
<%response.write (comp)%><br>Seller:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%response.write (pers)%>
<%ELSE%>
<script>Path.SOS(new Array(<%=StrA%>),'<%=z%>',document,'Seller','')</script><br>
Seller:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<select name=PSeller>
<script>Path.SO(new Array(<%=StrPA%>),'<%=bpe%>',document)</script>
<%END IF%>
					</td>
				</tr>
			</table>
		</td>
		<td align=left>
			<table cellspacing=0 cellpadding=0 border=0 align=left>
				<tr>
					<td align=right height=71 align=left>
					</td>




<!--End of display for truckload-->



<!--Display railcar-->
<td background=/Images/Panel/bgRC.gif valign=bottom align=left><img src=/Images/1x1.gif width=<%IF NOT MSIE THEN%>165<%ELSE%>173<%END IF%> height=1>
<%

'Query quantity, max bid price and minimum offer price from BID and OFFER talbe
'OFFR_SIZE =1 OR BID_SIZE=1: railcar
'OFFR_SIZE =0 OR BID_SIZE=0: trackload
'System doesn't allow trackload anymore!


'railcar!
Str="Select CONT_LABL, CONT_ID, QTYBID =(SELECT SUM(BID_QTY) FROM BID WHERE BID_CONT="+i.ToString()+" AND BID_MNTH='"+cmonth.ToString()+"' AND BID_SIZE='1'), "
Str=Str+" BID = (SELECT RIGHT(CONVERT(varchar,MAX(BID_PRCE)),4) FROM BID WHERE BID_CONT="+i.ToString()+" AND BID_MNTH='"+cmonth.ToString()+"' AND BID_SIZE='1'), "
Str=Str+"     OFFR = (SELECT RIGHT(CONVERT(varchar,MAX(OFFR_PRCE)),4) FROM OFFER WHERE OFFR_CONT="+i.ToString()+" AND OFFR_MNTH='"+cmonth.ToString()+"' AND OFFR_SIZE='1'), "
Str=Str+"      QTYOFFR = (SELECT SUM(OFFR_QTY) FROM OFFER WHERE OFFR_CONT="+i.ToString()+" AND OFFR_MNTH='"+cmonth.ToString()+"' AND OFFR_SIZE='1') "
Str=Str+"     From CONTRACT"
cmdContent= new SqlCommand(Str, conn)
Rec0= cmdContent.ExecuteReader()
Rec0.Read
'Display%>

<table border=0 cellspacing=0 cellpadding=0>
	<tr>
<%
	Response.Write ("<td><div align=center class=S1>")
	Response.Write (""+Rec0("QTYBID").ToString()+"</font>")
	Response.Write ("</div></td>")

	Response.Write ("<td><div align=center class=S1>")
	Response.Write (""+Rec0("BID")+"</font>")
	Response.Write ("</div></td>")

	Response.Write ("<td><div align=center class=S1>")
	Response.Write (""+Rec0("OFFR")+"</font>")
	Response.Write ("</div></td>")

	Response.Write ("<td><div align=center class=S1>")
	Response.Write (""+Rec0("QTYOFFR").ToString()+"</font>")
	Response.Write ("</div></td>")

%>
	</tr>
	<tr>
<%FOR ind=0 TO 3
Response.Write ("<td width=42 height=1></td>")
NEXT%>
	</tr>
</table>
<%Rec0.Close()%>
<%IF MSIE THEN%>
					</td>
				</tr>
			</table>
<%ELSE%>
					</td>
					<td bgcolor=#212121 width=2 height=2>
					</td>
				</tr>
				<tr>
					<td colspan=4 bgcolor=#212121 width=2 height=2>
					</td>
				</tr>
			</table>
<%END IF%>

<!--End of display for railcar-->








		</td>
	</tr>
	<tr>
		<td colspan=2>
<%'border begin
IF NOT NN6 THEN%>
<table border=1 cellspacing=0 cellpadding=0 bordercolor=#212121>
	<tr>
		<td>
<%ELSE%>
<table border=0 cellspacing=0 cellpadding=0>
	<tr>
		<td colspan=3 bgcolor=#212121 width=2 height=2><img src=/images/1x1.gif width=2 height=2>
		</td>
	</tr>
	<tr>
		<td bgcolor=#212121 width=2 height=2><img src=/images/1x1.gif width=2 height=2>
		</td>
		<td>
<%END IF%>
			<table border=0 cellspacing=0 cellpadding=0>
				<tr valign=middle bgcolor=#cccccc height=20>
					<td CLASS=S1 width=325>&nbsp;&nbsp;Quantity
					</td>
					<td bgcolor=#212121 rowspan=4 width=2 height=2>
					</td>
					<td width=337 CLASS=S1>&nbsp;&nbsp;Contract
					</td>
				</tr>
				<tr>
					<td align=left>
						<table>
							<tr>
								<td>
<script>Path.SO(0,20,'<%IF (Request.Form("IdOrder")<>"" OR Request.Querystring("IdOrder")<>"") AND Request.Form("Quantity")="" THEN%><%=Rec2("OFFR_QTY")%><%ELSE%><%=Request.Form("Quantity")%><%END IF%>',document,'Quantity')</script>
								</td>


								<td>
<%IF MSIE THEN Response.Write (" <STYLE=BORDER:NONE;BACKGROUND-COLOR:TRANSPARENT;")%>
<font size=1>Rail Cars (190,000 lbs) FOB Delivery Point
								</td>
							</tr>
						</table>
					</td>
					<td align=left>
						<table>
							<tr>
								<td>
<script>
Bid='<%=Right("000"+CStr(l*1000),3)%>'
Ask='<%=Right("000"+CStr(m*1000),3)%>'
Path.SOS(new Array(<%=Str0%>),'<%=i%>',document,'Contract','')
</script><br>
<script>
Path.SOS(new Array(<%=Str2%>),'<%IF o<>"0" THEN%><%=o%><%ELSE%>0<%END IF%>',document, 'Brand','Path.Wait(window);')
</script><br>Picked Up In:&nbsp;&nbsp;<script>Path.SOS(new Array(<%=StM0%>),('<%=cmonth%>'),document,'month','')</script>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr bgcolor=#cccccc height=20 valign=middle>
					<td CLASS=S1>&nbsp;&nbsp;Shipping Point
					</td>
					<td class=S1>&nbsp;&nbsp;Selling&nbsp;Price&nbsp;FOB&nbsp;<img name=FOBImg src=/images/misc/<%
IF (Request.Form("IdOrder")<>"" OR Request.Querystring("IdOrder")<>"") AND Request.Form("Price")="" THEN
	IF Rec2("OFFR_FOB")=True THEN%>Shipping<%ELSE%>Delivery<%END IF
ELSEIF Request.Form("FOB")="1" THEN%>Delivery<%ELSE%>Shipping<%
END IF%>.gif align=absmiddle>&nbsp;Point
					</td>
				</tr>
				<tr>
					<td><div align=left>&nbsp;
<script>Path.SOS(new Array(<%=Str1%>),'<%=Locl%>',document, 'Place','Path.Wait(window);')</script>
&nbsp;&nbsp;&nbsp;<input type=button class=tpebutton value="Specifications" onClick=Javascript:Path.Show(window,"../Common/MB_Specs_Location.asp",<%Response.write (N1)%>Form.Place.options[<%Response.write (N1)%>Form.Place.selectedIndex].value.split(',')[1])></div>
					</td>
					<td>$<b>.</b><input	type=text name=Price maxlength=3 size=3 value='<%
IF (Request.Form("IdOrder")<>"" OR Request.Querystring("IdOrder")<>"") AND Request.Form("Price")="" THEN%>
<%=Right(FormatNumber(Rec2("OFFR_PRCE"),3,0,-2,-2),3)%><%
ELSE%><%=Right(FormatNumber(l,3,0,-2,-2),3)%><%
END IF%>'>
<%IF MSIE THEN%><img src=../Images/buttons/white_updn.gif usemap=#Map border=0 align="absmiddle"><map name=Map><area shape=rect coords=0,0,20,9 href=Javascript:Trim(Form.Price,1)><area shape=rect coords=0,10,20,20 href=Javascript:Trim(Form.Price,0)></map><%END IF%> per lb.<img src=/Images/1x1.gif width=140 height=1>
<input type=button class=tpebutton value="OK" onclick=Javascript:Check()><img src=/Images/1x1.gif width=<%IF NOT MSIE THEN%>25<%ELSE%>10<%END IF%> height=30 align=absmiddle border=0>

					</td>
				</tr>
			</table>
<%'border end
IF MSIE THEN%>
		</td>
	</tr>
</table>
<%ELSE%>
		</td>
		<td bgcolor=#212121 width=2 height=2>
		</td>
	</tr>
	<tr>
		<td colspan=3 bgcolor=#212121 width=2 height=2>
		</td>
	</tr>
</table>
<%END IF%>


</form>
<script><%IF  Request.Form("Price")<>"" THEN%>Upd();<%END IF%>Path.Start(window)</script>
