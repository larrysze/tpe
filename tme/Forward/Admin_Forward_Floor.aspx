<%@ Page Language="c#" Codebehind="Admin_Forward_Floor.aspx.cs" AutoEventWireup="True" Inherits="localhost.Forward.Admin_Forward_Floor" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>

<asp:Content runat="server" ContentPlaceHolderID="cphJavaScript">

    <script>

    function menuP(n,txt,flag,down)
    // script for adding the raise or lower number in the checkbox by one
    {
	   if(flag==1)
	   {
		  Obj=n
		  wherex=315
		  wherey=60

	  eval(zz+Obj+cc+"<DIV id=Test style=\"position:absolute; left:"+wherex+"px; top:"+wherey+"px; visibility: inherit; overflow: visible\"><img name=P border=0 width=191 src=\"/images/specifications/"+(Obj=='Truc'?"TL":"RC")+"Specs.gif\"></DIV>'");
	   }
	}

    function Trim(Para,Flag)
    {
	   Arr=new Array('0','0','0')
	   for(i=0;i<Para.value.length;i++)
		  if(Para.value.charAt(i)<'0'||Para.value.charAt(i)>'9')
			 break
		  else
			 Arr[i]=Para.value.charAt(i)
	   Val=(i==Para.value.length)?((Arr[0]=='0')?((Arr[1]=='0')?parseInt(Arr[2]):parseInt(Arr[1]+Arr[2])):parseInt(Arr[0]+Arr[1]+Arr[2])):0
	   if(Flag==1)
		  {if(Val<999) Val+=1}
	   else if(Val>0)
		  Val-=1
	   Para.value=("000"+Val).substr(("000"+Val).length-3,3)
    }
    

    </script>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
    <asp:ValidationSummary runat="server" HeaderText="There are problems with the following fields. Please correct the errors below." ID="ValidationSummary1" />
    <table align="center" border="0">
        <tr>            
                <td align="left" class="Header Bold" width="100%">
                
                Ship Month:
                <asp:DropDownList ID="ddlFWDMonth" runat="server" OnSelectedIndexChanged="Reset_Grid" AutoPostBack="true" CssClass="InputForm">
                </asp:DropDownList></td>
            <td align="right">
                <img src="/images/misc/trainload.gif" border="0">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                <asp:DataGrid BackColor="#000000" CellSpacing="1" BorderWidth="0" ID="dg" runat="server" ItemStyle-CssClass="LinkNormal LightGray" AlternatingItemStyle-CssClass="LinkNormal DarkGray" HeaderStyle-CssClass="LinkNormal Bold OrangeColor" AutoGenerateColumns="false" OnItemDataBound="CollectAllRCNum" CellPadding="0" DataKeyField="CONT_ID" OnEditCommand="DG_Edit" OnUpdateCommand="DG_Update" OnCancelCommand="DG_Cancel" EditItemStyle-CssClass="LinkNormal LightGray" Width="550px" ShowFooter="false">
                    <Columns>
                        <mbrsc:RowSelectorColumn SelectionMode="Multiple" AllowSelectAll="true" ItemStyle-HorizontalAlign="center" />
                        <asp:EditCommandColumn EditText="Edit" CancelText="Cancel" UpdateText="Update" ItemStyle-CssClass="LinkNormal"/>
                        <asp:BoundColumn DataField="Cont_labl" HeaderText="Contract" ReadOnly="True" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left" />
                        <asp:TemplateColumn HeaderText="Railcar">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYBID") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CompareValidator ControlToValidate="txtQTYBID" ErrorMessage="Silly rabit, quantity must be an Integer." Display="none" Operator="DataTypeCheck" Type="Integer" runat="server" />
                                <asp:TextBox ID="txtQTYBID" Size="1" MaxLength="3" Text='<%# DataBinder.Eval(Container, "DataItem.QTYBID") %>' runat="server" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtQTYBID" Display="none" ErrorMessage="Bid quantity can not be blank" />
                                <asp:RangeValidator ControlToValidate="txtQTYBID" runat="server" ErrorMessage="Bid quantity must be greater than one." Display="none" Type="Integer" MinimumValue="1" MaximumValue="100" />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Bid">
                            <ItemTemplate>
                                <asp:Label runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.BID") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CompareValidator ControlToValidate="txtBID" ErrorMessage="Silly rabit, bid must be an Integer." Display="none" Operator="DataTypeCheck" Type="Double" runat="server" />
                                <asp:TextBox Size="1" ID="txtBID" MaxLength="5" Text='<%# DataBinder.Eval(Container, "DataItem.BID") %>' runat="server" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtBID" Display="none" ErrorMessage="Bid price can not be blank" />
                                <asp:RangeValidator ControlToValidate="txtBID" runat="server" ErrorMessage="Invalid bid amount." Display="none" Type="Double" MinimumValue="0" MaximumValue=".9990" />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Ask">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OFFR") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CompareValidator ControlToValidate="txtOFFER" ErrorMessage="Silly rabit, offer must be an Integer." Display="none" Operator="DataTypeCheck" Type="Double" runat="server" />
                                <asp:TextBox Size="1" ID="txtOFFER" MaxLength="5" Text='<%# DataBinder.Eval(Container, "DataItem.OFFR") %>' runat="server" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtOFFER" Display="none" ErrorMessage="Offer price can not be blank" />
                                <asp:RangeValidator ControlToValidate="txtOFFER" runat="server" ErrorMessage="Invalid offer amount." Display="none" Type="Double" MinimumValue="0" MaximumValue=".9990" />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Railcar">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYOFFR") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CompareValidator ControlToValidate="txtQTYOFFER" ErrorMessage="Silly rabit, quantity must be an Integer." Display="none" Operator="DataTypeCheck" Type="Integer" runat="server" />
                                <asp:TextBox ID="txtQTYOFFER" MaxLength="3" Text='<%# DataBinder.Eval(Container, "DataItem.QTYOFFR") %>' runat="server" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtQTYOFFER" Display="none" ErrorMessage="Offer quantity can not be blank" />
                                <asp:RangeValidator ControlToValidate="txtQTYOFFER" runat="server" ErrorMessage="Offer quantity must be greater than one." Display="none" Type="Integer" MinimumValue="1" MaximumValue="100" />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
            <td valign="top">
                <table style="border-width:1px; border-style:solid; border-color:Black;" cellpadding="0" cellspacing="0" width="220">
                    <tr>
                        <td colspan="3" valign="middle" class="Content Color2" align="center">
                            <br>
                            <br>
                            <asp:DropDownList ID="ddlRaiseLower" runat="server" CssClass="InputForm">
                                <asp:ListItem Value="Raise" Text="Raise" />
                                <asp:ListItem Value="Lower" Text="Lower" />
                            </asp:DropDownList>
                            selected
                            <asp:DropDownList ID="ddlBidOffer" runat="server" CssClass="InputForm">
                                <asp:ListItem Value="both" Text="both" />
                                <asp:ListItem Value="0" Text="bids" />
                                <asp:ListItem Value="1" Text="offers" />
                            </asp:DropDownList>
                            by
                            <br>
                            $.<asp:TextBox CssClass="InputForm" ID="UpDownPrc" size="3" runat="server" />
                            <img src="../Images/buttons/white_updn.gif" style="border-bottom-style:solid; border-width:1px; border-color:Black;" usemap="#Map" border="0" align="absMiddle">
                            <map name="Map">
                                <area shape="RECT" coords="0,0,20,9" href="javascript:Trim(document.getElementById('<%= UpDownPrc.ClientID %>'),1)">
                                <area shape="RECT" coords="0,10,20,20" href="javascript:Trim(document.getElementById('<%= UpDownPrc.ClientID %>'),0)">
                            </map>
                            per pound. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button CssClass="Content Color2" runat="server" Text="Update" OnClick="Update_Price" ID="Button1" /><br>
                            <br>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <li><a href="/docs/userguide.pdf" target="_blank" class="LinkNormal">User Guide</a>
                                <%if (Session["Typ"].ToString() == "S")
                                  {%>
                                <li><a href="/docs/seller.pdf" target="_blank" class="LinkNormal">Seller's Agreement (pdf)</a>
                            <li><a href="/docs/Seller_Agreement_NewWind.aspx" target="_blank" class="LinkNormal">Seller's Agreement (html)</a>
                                <%}
                                  else if (Session["Typ"].ToString() == "P")
                                  {%>
                                <li><a href="/docs/buyer.pdf" target="_blank" class="LinkNormal">Buyer's Agreement (pdf)</a>
                            <li><a href="/docs/Buyer_Agreement_NewWind.aspx" target="_blank" class="LinkNormal">Buyer's Agreement (html)</a>
                                <%}%>
                                <li><a href="javascript:sealWin=window.open(&quot;/docs/Rules_of_the_Exchange4PopUp.aspx&quot;,&quot;win&quot;,'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=805,height=500');void(0)" class="LinkNormal">Rules Of The Exchange</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
