<%@ Page Language="VB" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Web.Mail" %>
<form runat="server" id="Form">
    <TPE:Template PageTitle="Place Bid" Runat="Server" />
    <!--#include FILE="../include/VBLegacy_Code.aspx"-->

<%IF Session("Typ")<>"A" AND Session("Typ")<>"B" THEN Response.Redirect ("../default.aspx")


'Database connection'
Dim conn as SqlConnection
Dim cmdContent as SQLCommand
Dim Rec0 as SqlDataReader
conn = new SqlConnection(Application("DBconn"))
conn.Open()
Dim conn1 as SqlConnection
conn1 = new SqlConnection(Application("DBconn"))
conn1.Open()
Dim Rec1 as SqlDataReader
Dim conn2 as SqlConnection
conn2 = new SqlConnection(Application("DBconn"))
conn2.Open()
Dim Rec2 as SqlDataReader
Dim connP as SqlConnection
connP = new SqlConnection(Application("DBconn"))
connP.Open()
Dim RecP as SqlDataReader
Dim connL as SqlConnection
connL = new SqlConnection(Application("DBconn"))
connL.Open()
Dim RecL as SqlDataReader
Dim connM as SqlConnection
connM = new SqlConnection(Application("DBconn"))
connM.Open()
Dim RecM as SqlDataReader
Dim conn3 as SqlConnection
conn3 = new SqlConnection(Application("DBconn"))
conn3.Open()
Dim Rec3 as SqlDataReader



DIM Quantity,Str,Str0,Str1,Str2,Str3,Str4,Locl,i,j,k,l,m,n,Height,StrA,StrB,z,StrPA,StrPB,bpe,pers,comp
DIM IdOrder, IdComp, Valid, Pers_Id, Cont_Id, Mess, errMsg, res, Price, Size, Locl0, Locl1, strSql,Mont
DIM arBA(12), ind	' array for Bids & Asks

IF Request.Form("Valid")<>"" THEN
	'k=Credit available
	IdOrder = Request.Form("IdOrder")
	If IdOrder<>"" Then
		IdOrder = "@Order_Id="+IdOrder+", "
	ELSE
		IdOrder = Request.Querystring("IdOrder")
		If IdOrder<>"" Then IdOrder = "@Order_Id="+IdOrder+", "
	END IF
	IdComp = Request.Form("Buyer")
	If IdComp<>"" Then IdComp = "@Comp_Id="+IdComp+", "
	Valid = Request.Form("Valid")
	If Valid<>"" Then Valid = "@Valid="+Valid+", "
	Pers_Id = Request.Form("PBuyer")
	If Pers_Id<>"" Then Pers_Id = "@Pers_Id="+Pers_Id+", "
	Cont_Id = Request.Form("Contract")
	If Cont_Id<>"" Then Cont_Id = "@Cont_Id="+Cont_Id+", "
	Quantity=Request.Form("Quantity")
	If Quantity<>"" Then Quantity = "@Qty="+Quantity+", "
	Price = Request.Form("Price")
	If Price<>"" Then Price = "@Price=0."+Price+", "

	Size = "@Size=1, "
	Mont = Request.Form("month")
	If Mont<>"" Then Mont = "@Month="+Mont+", "
	'Locl(0):Locality
	'Locl(1):Place
	Locl=Split(Request.Form("Place"),",")
	If Locl(0)<>"" Then Locl0 = "@Locl0="&Locl(0)&", "
	If Locl(1)<>"" Then Locl1 = "@Locl1="&Locl(1)&", "
	Mess = Request.Form("Mess")

	If Mess<>"" Then Mess = "@Mess='"+Mess+"', "
	errMsg = "@ErrMsg=NULL"
	'Database Lock
	Application.Lock()
	Application("Flag")=1

	strSql = "Exec spTransactionEngine_P "+IdOrder+" "+IdComp+" "+Valid+" "+Pers_Id+" "+Cont_Id+" "+Quantity+" "+Price+" "+Size+" "+Mont+" "+Locl0+" "+Locl1+" "+Mess+" "&errMsg

	cmdContent= new SqlCommand(strSql, conn)
	cmdContent.ExecuteNonQuery()

	Application("Flag")=0
	Application.UnLock()
	Response.Redirect("/Common/Common_Working_Orders.aspx?type=*Bid")
END IF
Height=260
%>

<!--#INCLUDE FILE="../include/Loading.inc"-->
<!--#INCLUDE FILE="../include/PopUp.inc"-->

<%
cmdContent= new SqlCommand("SELECT COMP_ID,COMP_NAME FROM COMPANY WHERE COMP_TYPE='P' AND COMP_ENBL=1 AND COMP_ID IN (SELECT DISTINCT PERS_COMP FROM PERSON WHERE PERS_TYPE = 'P' AND PERS_ENBL=1) ORDER BY COMP_NAME", connP)
RecP= cmdContent.ExecuteReader()
RecP.Read
StrA=""
StrB=""
IF Request.Form("Buyer")<>"" THEN
	z=Request.Form("Buyer")
ELSEIF Request.Querystring("IdOrder")<>"" THEN
	cmdContent= new SqlCommand("SELECT BID_BUYR,(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=BID_BUYR),(SELECT PERS_FRST_NAME+' '+PERS_LAST_NAME FROM PERSON WHERE PERS_ID=BID_BUYR),(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=BID_BUYR)) FROM BID WHERE BID_ID="&Request.Querystring("IdOrder"), connL)
        RecL= cmdContent.ExecuteReader()
	RecL.Read
	z=RecL(1)
	pers=RecL(2)
	comp=RecL(3)
	RecL.Close

ELSEIF Request.Form("IdOrder")<>"" THEN
	cmdContent= new SqlCommand("SELECT BID_BUYR,(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=BID_BUYR),(SELECT PERS_FRST_NAME+' '+PERS_LAST_NAME FROM PERSON WHERE PERS_ID=BID_BUYR),(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=BID_BUYR)) FROM BID WHERE BID_ID="&Request.Form("IdOrder"), connL)
	RecL= cmdContent.ExecuteReader()
	RecL.Read

	z=RecL(1)
	pers=RecL(2)
	comp=RecL(3)
	RecL.Close

ELSE
	z=RecP("COMP_ID")
END IF
RecP.Close
cmdContent= new SqlCommand("SELECT COMP_ID,COMP_NAME FROM COMPANY WHERE COMP_TYPE='P' AND COMP_ENBL=1 AND COMP_ID IN (SELECT DISTINCT PERS_COMP FROM PERSON WHERE PERS_TYPE = 'P' AND PERS_ENBL=1) ORDER BY COMP_NAME", connP)
RecP= cmdContent.ExecuteReader()
While RecP.Read()
	StrA=StrA+"'"&RecP("COMP_ID")&"',"
	StrB=StrB+"'"&RecP("COMP_NAME")&"',"

End While
StrA=Left(StrA,Len(StrA)-1)&"),new Array("&Left(StrB,Len(StrB)-1)
RecP.Close

cmdContent= new SqlCommand("SELECT PERS_ID,(SELECT PERS_FRST_NAME+' '+PERS_LAST_NAME) FROM PERSON WHERE PERS_TYPE='P' AND PERS_ENBL=1 AND PERS_COMP="&z, connP)
RecP= cmdContent.ExecuteReader()
RecP.Read
StrPA=""
StrPB=""
IF Request.Form("Mess")="1" THEN
	bpe=RecP(0)
ELSEIF Request.Form("PBuyer")<>"" AND Request.Form("PBuyer")<>0 THEN
	bpe=Request.Form("PBuyer")
	cmdContent= new SqlCommand("SELECT PERS_FRST_NAME+' '+PERS_LAST_NAME,(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID="+bpe+")) FROM PERSON WHERE PERS_ID="&bpe, connL)
        RecL= cmdContent.ExecuteReader()
	RecL.Read

	pers=RecL(0)
	comp=RecL(1)
	RecL.Close

ELSEIF Request.Querystring("IdOrder")<>"" THEN
	cmdContent= new SqlCommand("SELECT BID_BUYR FROM BID WHERE BID_ID="&Request.Querystring("IdOrder"), connL)
	RecL= cmdContent.ExecuteReader()
        RecL.Read


	bpe=RecL(0)
	RecL.Close

ELSEIF Request.Form("IdOrder")<>"" THEN
        cmdContent= new SqlCommand("SELECT BID_BUYR FROM BID WHERE BID_ID="&Request.Form("IdOrder"), connL)
	RecL= cmdContent.ExecuteReader()
	RecL.Read
	bpe=RecL(0)
	RecL.Close

ELSEIF RecP.Hasrows=0 THEN
	bpe="0"
	StrPA="'0',"
	StrPB="'No Purchasers',"
ELSE
	bpe=RecP("PERS_ID")
END IF
RecP.Close
cmdContent= new SqlCommand("SELECT PERS_ID,(SELECT PERS_FRST_NAME+' '+PERS_LAST_NAME) FROM PERSON WHERE PERS_TYPE='P' AND PERS_ENBL=1 AND PERS_COMP="&z, connP)
RecP= cmdContent.ExecuteReader()
While RecP.Read()
	StrPA=StrPA+"'"&RecP(0)&"',"
	StrPB=StrPB+"'"&RecP(1)&"',"

End While
StrPA=Left(StrPA,Len(StrPA)-1)&"),new Array("&Left(StrPB,Len(StrPB)-1)
RecP.Close

cmdContent= new SqlCommand("SELECT PLAC_ID, PLAC_NAME=(CASE WHEN PLAC_TYPE='W' THEN ((SELECT '(W)'+LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL)) ELSE (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) END),PLAC_LOCL,PLAC_ADDR_ONE=(CASE WHEN PLAC_ADDR_ONE IS NULL THEN 1 ELSE 0 END) FROM PLACE WHERE (PLAC_COMP="+z.ToString()+" AND PLAC_TYPE='D' AND PLAC_ENBL=1) OR PLAC_TYPE='W' ORDER BY PLAC_TYPE", conn1)

Rec1= cmdContent.ExecuteReader()
Rec1.Read
IF Rec1.Hasrows=0 THEN
Session("Message")="Your officer needs to select delivery locations<br><font color=red>Need help? Call us 1-800-850-2380"
Response.Redirect ("../Common/MB_Message.aspx")
End IF
cmdContent= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE POWER(2,CONT_ID-1)&(SELECT COMP_PREF FROM COMPANY WHERE COMP_ID="+z.ToString()+")<>0 AND POWER(2,CONT_ID-1)&(SELECT PERS_PREF FROM PERSON WHERE PERS_ID="+bpe.ToString()+")<>0 ORDER BY CONT_ORDR", conn)
Rec0= cmdContent.ExecuteReader()
Rec0.Read
IF Request.Form("IdOrder")<>"" THEN
        cmdContent= new SqlCommand("SELECT *,CONT_LABL=(SELECT CONT_LABL FROM CONTRACT WHERE CONT_ID=BID_CONT) FROM BID WHERE BID_ID="&Request.Form("IdOrder"), conn2)
        Rec2= cmdContent.ExecuteReader()
	Rec2.Read
ELSEIF Request.Querystring("IdOrder")<>"" THEN
        cmdContent= new SqlCommand("SELECT *,CONT_LABL=(SELECT CONT_LABL FROM CONTRACT WHERE CONT_ID=BID_CONT) FROM BID WHERE BID_ID="&Request.Querystring("IdOrder"), conn2)
        Rec2= cmdContent.ExecuteReader()
	Rec2.Read

ELSE
	IF Rec0.HasRows=0 THEN
	Session("Message")="You haven't defined your contracts."
	Response.Redirect ("../Common/MB_Message.aspx")
	End IF
END IF

IF Request.Form("Contract")<>"" THEN
	i=Request.Form("Contract")
ELSEIF Request.Form("IdOrder")<>"" OR Request.Querystring("IdOrder")<>"" THEN
	i=Rec2("BID_CONT")
ELSE
	i=Rec0("CONT_ID")
END IF

IF Request.Form("IdPlace")<>"" THEN
	j=Request.Form("Place")
	Locl=j.ToString()+","
ELSEIF Request.Form("IdPlace")="" AND Request.Form("Place")<>"" THEN
	k=Split(Request.Form("Place"),",")
	j=k(0)
	Locl=j.ToString()+","&k(1)
ELSEIF Request.Form("IdOrder")<>"" OR Request.Querystring("IdOrder")<>"" THEN
	j=Rec2("BID_TO_LOCL")
	Locl=j.ToString()+","&Rec2("BID_TO")
ELSE
	j=Rec1("PLAC_LOCL")
	Locl=j.ToString()+","&Rec1("PLAC_ID")
END IF

Str0=""
Str1=""
Rec0.Close
cmdContent= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE POWER(2,CONT_ID-1)&(SELECT COMP_PREF FROM COMPANY WHERE COMP_ID="+z.ToString()+")<>0 AND POWER(2,CONT_ID-1)&(SELECT PERS_PREF FROM PERSON WHERE PERS_ID="+bpe.ToString()+")<>0 ORDER BY CONT_ORDR", conn)
Rec0= cmdContent.ExecuteReader()
While Rec0.Read()
	Str0=Str0+"'"&Rec0("CONT_ID")&"',"
	Str1=Str1+"'"&Rec0("CONT_LABL")&"',"

End While
Str0=Left(Str0,Len(Str0)-1)&"),new Array("&Left(Str1,Len(Str1)-1)
Rec0.Close


k=Rec1("PLAC_LOCL")
l=0
Str1=""
Str2=""
Rec1.Close
'cmdContent= new SqlCommand("SELECT PLAC_ID, PLAC_NAME= (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) ,PLAC_LOCL,PLAC_ADDR_ONE=(CASE WHEN PLAC_ADDR_ONE IS NULL THEN 1 ELSE 0 END) FROM PLACE WHERE (PLAC_COMP="+z.ToString()+" AND PLAC_TYPE='D' AND PLAC_ENBL=1) OR PLAC_TYPE='W' ORDER BY PLAC_TYPE", conn1)
cmdContent= new SqlCommand("SELECT PLAC_ID, PLAC_NAME=(CASE WHEN PLAC_TYPE='W' THEN ((SELECT '(W)'+LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL)) ELSE (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) END),PLAC_LOCL,PLAC_ADDR_ONE=(CASE WHEN PLAC_ADDR_ONE IS NULL THEN 1 ELSE 0 END) FROM PLACE WHERE (PLAC_COMP="+z.ToString()+" AND PLAC_TYPE='D' AND PLAC_ENBL=1) OR PLAC_TYPE='W' ORDER BY PLAC_TYPE", conn1)

Rec1= cmdContent.ExecuteReader()
While Rec1.Read()
	Str1=Str1+"'"&Rec1("PLAC_LOCL")&","&Rec1("PLAC_ID")&"',"
	l=l*CByte(CInt(Rec1("PLAC_LOCL"))=Cint(k))/255+1
	k=Rec1("PLAC_LOCL")
	IF l>1 THEN Str2=Str2+"'"&Rec1("PLAC_NAME")&" ("+l.ToString()+")'," ELSE IF Rec1("PLAC_ADDR_ONE")="1" THEN Str2=Str2+"'"&Rec1("PLAC_NAME")&" (U)'," ELSE Str2=Str2+"'"&Rec1("PLAC_NAME")&"',"

End While
Str1=Left(Str1,Len(Str1)-1)&"),new Array("&Left(Str2,Len(Str2)-1)
Rec1.Close
%>
<%
Dim cmonth,StM0,StM1
cmdContent= new SqlCommand("SELECT FWD_ID,FWDLABL=(FWD_MNTH+' '+RIGHT(CONVERT(varchar,FWD_YEAR),2)) FROM FWDMONTH WHERE FWD_ACTV=1 ORDER BY FWD_ID", connM)
RecM= cmdContent.ExecuteReader()
RecM.Read
IF Request.Form("month")<>"" THEN
	cmonth=Request.Form("month")
ELSEIF Request.Form("IdOrder")<>"" OR Request.Querystring("IdOrder")<>"" THEN
	cmonth=Rec2("bid_mnth")
ELSE
	cmonth=RecM(0)
END IF
StM0=""
StM1=""
RecM.Close
cmdContent= new SqlCommand("SELECT FWD_ID,FWDLABL=(FWD_MNTH+' '+RIGHT(CONVERT(varchar,FWD_YEAR),2)) FROM FWDMONTH WHERE FWD_ACTV=1 ORDER BY FWD_ID", connM)
RecM= cmdContent.ExecuteReader()
While RecM.Read()
	StM0=StM0+"'"&RecM("FWD_ID")&"',"
	StM1=StM1+"'"&RecM("FWDLABL")&"',"

End While
StM0=Left(StM0,Len(StM0)-1)&"),new Array("&Left(StM1,Len(StM1)-1)
RecM.Close

%>


<SPAN ID=Main name=Main style=' x:0px; y:0px; visibility: hidden'>
<script>
function Trim(Para,Flag)
{
	Arr=new Array('0','0','0')
	for(i=0;i<Para.value.length;i++)
		if(Para.value.charAt(i)<'0'||Para.value.charAt(i)>'9')
			break
		else
			Arr[i]=Para.value.charAt(i)
	Val=(i==Para.value.length)?((Arr[0]=='0')?((Arr[1]=='0')?parseInt(Arr[2]):parseInt(Arr[1]+Arr[2])):parseInt(Arr[0]+Arr[1]+Arr[2])):0
	if(Flag==1)
		{if(Val<999) Val+=1}
	else if(Val>0)
		Val-=1
	Para.value=("000"+Val).substr(("000"+Val).length-3,3)
}

function Check()
{
	with(<%Response.Write (N1)%>Form)
	{
		Qty=Quantity.options[Quantity.options.selectedIndex].text

		if(Price.value==0)	return Path.Mess(window,'Please enter a buying price.','0')
		if (Qty=="0")		return Path.Mess(window,'Please enter the number of contracts you wish to buy.','0')
		if (Place.options[Place.options.selectedIndex].text.substring(Place.options[Place.options.selectedIndex].text.length-3,Place.options[Place.options.selectedIndex].text.length)=="(U)")
		return Path.Mess(window,'You have selected an undefined location, please define this location on the <a href=../Officer/Officer_Add_Location.asp>Add Location</a> page before placing an order.','0')
		if (Price.value=="")	return Path.Mess(window,'Please enter a valid price.','0')
		for(j=0;j<Price.value.length;j++)
			if (Price.value.charAt(j)<"0"||Price.value.charAt(j)>"9")
				return Path.Mess(window,'Please, enter a valid price.','0')

		Val=eval("parseFloat(Price.value)/(Math.pow(10,Price.value.length-3))*Qty")
		Prce=new String(Math.round(eval("190*100*Val"))/100)
		Valid.value=Prce
		t=0
		for(t=0;t<Prce.length;t++)
			if(Prce.charAt(t)=='.')
				break;
		t=Prce.length-t
		for(i=3+t;Prce.length-i>0;i+=4)
			Prce=Prce.substring(0,Prce.length-i)+","+Prce.substring(Prce.length-i,Prce.length)


		Str=new Array('Your are going to place an order to buy',' '+Qty+(' Rail Car')+(eval("Qty>1")?'s':''),' of ',Contract.options[Contract.options.selectedIndex].text,' for ','$ .'+Price.value,' per pound.\n\n','Picked up at ',Place.options[Place.options.selectedIndex].text,' in ',month.options[month.options.selectedIndex].text ,'.\n\n Estimate value $',Prce)

                Mess.value=Str.join('').substring(32,Str.join('').length)





		Str1='<font color=red>'
		Path.Mess(window,Str[0]+':<br>'+Str1+Str[1]+'</font>'+Str[2]+Str1+Str[3]+'</font><br>'+Str[4]+Str1+Str[5]+'</font>'+Str[6]+'<br>'+Str[7]+Str1+Str[8]+'</font>'+Str[9]+Str1+Str[10]+'</font>'+Str[11]+Str1+Str[12]+'</font>','1')
	}
}
function Answer(Flag)
{
	with(<%Response.Write (N1)%>Form)
		if(Flag)
		{
			Submit.value='Submit'
			submit()
		}
		else
			Valid.value=''
}
</script>

<input type=hidden name=Valid value=''>
<input type=hidden name=Mess value=''>
<input type=hidden name=Submit value=''>
<input type=hidden name=IdOrder value='<%IF Request.Querystring("IdOrder")<>"" THEN%><%=Request.Querystring("IdOrder")%><%ELSE%><%=Request.Form("IdOrder")%><%END IF%>'>

<table cellspacing=0 cellpadding=0 border=0>
	<tr>
		<td><img src=/images/1x1.gif height=6>
		</td>
	</tr>
	<tr>
		<td>
			<table cellspacing=0 cellpadding=0 border=0>
				<tr>
					<td>
						<table border=1 bordercolor=black height=68 cellspacing=0 cellpadding=0>
							<tr valign=middle bgcolor=#cccccc height=20>
								<td width=175 CLASS=S1>&nbsp;&nbsp;<%IF Request.Form("IdOrder")<>"" THEN%>Change Bid #<%=Request.Form("IdOrder")%><%ELSEIF Request.Querystring("IdOrder")<>"" THEN%>Change Bid #<%=Request.Querystring("IdOrder")%><%ELSE%>Buyer<%END IF%>
								</td>
							</tr>
							<tr>
								<td>
<%IF Request.Form("IdOrder")<>"" OR Request.Querystring("IdOrder")<>"" THEN%>
<input type=hidden name=Buyer value='<%=z%>'>
<input type=hidden name=PBuyer value='<%=bpe%>'>
<%response.write (comp)%><br>Buyer:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%response.write (pers)%>
<%ELSE%>
<script>Path.SOS(new Array(<%=StrA%>),'<%=z%>',document,'Buyer','window.document.Form.Mess.value=1;')</script>
<br>Buyer:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<select name=PBuyer>
<script>Path.SO(new Array(<%=StrPA%>),'<%=bpe%>',document)</script>
<%END IF%>
					</td>
				</tr>
			</table>
		</td>
		<td align=left>
			<table cellspacing=0 cellpadding=0 border=0 align=left>
				<tr>
					<td align=left height=71>
					</td>




<!--display the quantity, max bid price and minimum offer price of trading floor-->





<!--Display railcar-->
<td background=/Images/Panel/bgRC.gif valign=bottom align=left><img src=/Images/1x1.gif width=<%IF NOT MSIE THEN%>165<%ELSE%>173<%END IF%> height=1>
<%

'Query quantity, max bid price and minimum offer price from BID and OFFER talbe
'OFFR_SIZE =1 OR BID_SIZE=1: railcar
'OFFR_SIZE =0 OR BID_SIZE=0: trackload
'System doesn't allow trackload anymore!


'railcar!

Str="Select CONT_LABL, CONT_ID, QTYBID =(SELECT SUM(BID_QTY) FROM BID WHERE BID_CONT="+i.ToString()+" AND BID_MNTH='"+cmonth.ToString()+"' AND BID_SIZE='1'), "
Str=Str+" BID = (SELECT RIGHT(CONVERT(varchar,MAX(BID_PRCE)),4) FROM BID WHERE BID_CONT="+i.ToString()+" AND BID_MNTH='"+cmonth.ToString()+"' AND BID_SIZE='1'), "
Str=Str+"     OFFR = (SELECT RIGHT(CONVERT(varchar,MAX(OFFR_PRCE)),4) FROM OFFER WHERE OFFR_CONT="+i.ToString()+" AND OFFR_MNTH='"+cmonth.ToString()+"' AND OFFR_SIZE='1'), "
Str=Str+"      QTYOFFR = (SELECT SUM(OFFR_QTY) FROM OFFER WHERE OFFR_CONT="+i.ToString()+" AND OFFR_MNTH='"+cmonth.ToString()+"' AND OFFR_SIZE='1') "
Str=Str+"     From CONTRACT"
cmdContent= new SqlCommand(Str, conn)
Rec0= cmdContent.ExecuteReader()
Rec0.Read
'Display%>

<table border=0 cellspacing=0 cellpadding=0>
	<tr>
<%
	Response.Write ("<td><div align=center class=S1>")
	Response.Write (""+Rec0("QTYBID").ToString()+"</font>")
	Response.Write ("</div></td>")

	Response.Write ("<td><div align=center class=S1>")
	Response.Write (""+Rec0("BID")+"</font>")
	Response.Write ("</div></td>")

	Response.Write ("<td><div align=center class=S1>")
	Response.Write (""+Rec0("OFFR")+"</font>")
	Response.Write ("</div></td>")

	Response.Write ("<td><div align=center class=S1>")
	Response.Write (""+Rec0("QTYOFFR").ToString()+"</font>")
	Response.Write ("</div></td>")

%>
	</tr>
	<tr>
<%FOR ind=0 TO 3
Response.Write ("<td width=42 height=1></td>")
NEXT%>
	</tr>
</table>
<%Rec0.Close()%>
<!--End of display for railcar-->












<%IF MSIE THEN%>
					</td>
				</tr>
			</table>
<%ELSE%>
					</td>
					<td bgcolor=#212121 width=2 height=2>
					</td>
				</tr>
				<tr>
					<td colspan=4 bgcolor=#212121 width=2 height=2>
					</td>
				</tr>
			</table>
<%END IF%>
					</td>
				</tr>
				<tr>
					<td colspan=2>
						<table border=1 cellspacing=0 cellpadding=0 bordercolor=#212121>
							<tr>
								<td>
									<table border=0 cellspacing=0 cellpadding=0>
										<tr valign=middle bgcolor=#cccccc height=20>
											<td CLASS=S1 width=250>&nbsp;&nbsp;Quantity
											</td>
											<td bgcolor=#212121 rowspan=4><img src=/images/1x1.gif width=2 height=2>
											</td>
											<td width=337 CLASS=S1>&nbsp;&nbsp;Contract
											</td>
										</tr>
										<tr>
											<td align=left>
												<table>
													<tr>
														<td>
<script>Path.SO(0,20,'<%IF (Request.Form("IdOrder")<>"" OR Request.Querystring("IdOrder")<>"") AND Request.Form("Quantity")="" THEN%><%=Rec2("BID_QTY")%><%ELSE%><%=Request.Form("Quantity")%><%END IF%>',document,'Quantity')</script>
														</td>
														<td>
														<%IF MSIE THEN Response.Write (" <STYLE=BORDER:NONE;BACKGROUND-COLOR:TRANSPARENT;")%>
														<font size=1>Rail Cars (190,000 lbs) FOB Delivery Point
								</td>
													</tr>
												</table>
											</td>
											<td align=left>&nbsp;
<script>
Bid='<%=Right("000"+CStr(l*1000),3)%>'
Ask='<%=Right("000"+CStr(m*1000),3)%>'
Path.SOS(new Array(<%=Str0%>),('<%=i%>'),document,'Contract','')
</script><br>&nbsp;&nbsp;Shipped In:&nbsp;&nbsp;<script>Path.SOS(new Array(<%=StM0%>),('<%=cmonth%>'),document,'month','')</script>
											</td>
										</tr>
										<tr bgcolor=#cccccc height=20 valign=middle>
											<td CLASS=S1>&nbsp;&nbsp;Delivery Point
											</td>
											<td CLASS=S1>&nbsp;&nbsp;Buying Price FOB Delivery Point
											</td>
										</tr>
										<tr>
											<td>&nbsp;
<script>Path.SOS(new Array(<%=Str1%>),'<%=Locl%>',document,'Place','Path.Wait(window);')</script>
&nbsp;&nbsp;&nbsp;<input type=button value="Specifications" class=tpebutton onClick=Javascript:Path.Show(window,"../Common/MB_Specs_Location.aspx",<%Response.write (N1)%>Form.Place.options[<%Response.write (N1)%>Form.Place.selectedIndex].value.split(',')[1])>
											</td>
											<td valign=top align=left>&nbsp;$<b>.</b><input type=text name=Price maxlength=3 size=3 value='<%IF (Request.Form("IdOrder")<>"" OR Request.Querystring("IdOrder")<>"") AND Request.Form("Price")="" THEN%><%=Right(FormatNumber(Rec2("BID_PRCE"),3,0,-2,-2),3)%><%ELSE%><%=Right(FormatNumber(l,3,0,-2,-2),3)%><%END IF%>'>
<%IF MSIE THEN%><img src=../Images/buttons/white_updn.gif usemap=#Map border=0 align=absmiddle><map name=Map><area shape=rect coords=0,0,20,9 href=Javascript:Trim(Form.Price,1)><area shape=rect coords=0,10,20,20 href=Javascript:Trim(Form.Price,0)></map><%END IF%> per lb.<img src=/Images/1x1.gif width=160 height=1>
<input type=button class=tpebutton value="OK" onClick="Javascript:Check()"><img src=/Images/1x1.gif width=<%IF NOT MSIE AND NOT NN6 THEN%>25<%ELSE%>10<%END IF%> height=30 align=absmiddle border=0>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height=15>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>







</SPAN>

<TPE:Template Footer="true" Runat="Server" />

</form>
<script>Path.Start(window)</script>
