<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Monomer_Forward_Floor.aspx.cs" Inherits="localhost.Forward.Monomer_Forward_Floor"  MasterPageFile="~/MasterPages/Template.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Template.Master" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphInstructions">
    <table class='InstructionBullets'>
        <tr>
            <td width="35">
                <img src='/pics/bullet.gif'></td>
            <td>
                The Contract Market reflects the prices that our Market Making division will buy or sell (at least) 1 Generic Prime Railcar delivered to most North American locations.</td>
        </tr>
        <tr>
            <td width="35">
                <img src='/pics/bullet.gif'></td>
            <td>
                We evaluate and change the market daily.</td>
        </tr>
        <tr>
            <td width="35">
                <img src='/pics/bullet.gif'></td>
            <td>
                Use this as a good indication of the Spot Generic Prime market.</td>
        </tr>
        <tr>
            <td width="35">
                <img src='/pics/bullet.gif'></td>
            <td>
                Call 312.202.0002 to further discuss.</td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphHeading">
    <div class="Header Bold Color1">
        Contract Prices</div>
</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
    <div align="left">
    <table border="0" cellpadding="0" cellspacing="0">
        <tr><td valign="top" style="width: 615px">
        <asp:DataGrid ID="dg" CssClass="Content" runat="server" BorderWidth="0" CellSpacing="1" BackColor="#000000" Width="550px" HorizontalAlign="Left" EditItemStyle-BackColor="Silver" DataKeyField="CONT_ID" CellPadding="2" AutoGenerateColumns="False">
            <EditItemStyle BackColor="Silver"></EditItemStyle>
            <AlternatingItemStyle CssClass="LinkNormal LightGray"></AlternatingItemStyle>
            <ItemStyle CssClass="LinkNormal DarkGray" HorizontalAlign="Left"></ItemStyle>
            <HeaderStyle CssClass="LinkNormal Bold OrangeColor" HorizontalAlign="Left"></HeaderStyle>
            <Columns>
                <asp:BoundColumn DataField="Cont_labl" ReadOnly="True" HeaderText="Contract">
                    <ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Unit">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYBID") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CompareValidator ID="CompareValidator1" ControlToValidate="txtQTYBID" ErrorMessage="Silly rabit, quantity must be an Integer." Display="none" Operator="DataTypeCheck" Type="Integer" runat="server" />
                        <asp:TextBox ID="txtQTYBID" Size="1" MaxLength="2" Text='<%# DataBinder.Eval(Container, "DataItem.QTYBID") %>' runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtQTYBID" Display="none" ErrorMessage="Bid quantity can not be blank" />
                        <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtQTYBID" runat="server" ErrorMessage="Bid quantity must be greater than one." Display="none" Type="Integer" MinimumValue="1" MaximumValue="100" />
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Bid Price">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BID") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CompareValidator ID="CompareValidator2" ControlToValidate="txtBID" ErrorMessage="Silly rabit, bid must be an Integer." Display="none" Operator="DataTypeCheck" Type="Double" runat="server" />
                        <asp:TextBox Size="1" ID="txtBID" MaxLength="4" Text='<%# DataBinder.Eval(Container, "DataItem.BID") %>' runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBID" Display="none" ErrorMessage="Bid price can not be blank" />
                        <asp:RangeValidator ID="RangeValidator2" ControlToValidate="txtBID" runat="server" ErrorMessage="Invalid bid amount." Display="none" Type="Double" MinimumValue="0" MaximumValue=".999" />
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Ask Price">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OFFR") %>' ID="Label33" NAME="Label2" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CompareValidator ControlToValidate="txtOFFER" ErrorMessage="Silly rabit, offer must be an Integer." Display="none" Operator="DataTypeCheck" Type="Double" runat="server" ID="Comparevalidator1" NAME="Comparevalidator1" />
                        <asp:TextBox Size="1" ID="txtOFFER" MaxLength="4" Text='<%# DataBinder.Eval(Container, "DataItem.OFFR") %>' runat="server" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtOFFER" Display="none" ErrorMessage="Offer price can not be blank" ID="Requiredfieldvalidator1" NAME="Requiredfieldvalidator1" />
                        <asp:RangeValidator ControlToValidate="txtOFFER" runat="server" ErrorMessage="Invalid offer amount." Display="none" Type="Double" MinimumValue="0" MaximumValue=".999" ID="Rangevalidator1" NAME="Rangevalidator1" />
                    </EditItemTemplate>
                 </asp:TemplateColumn>
                 <asp:TemplateColumn HeaderText="Unit">
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYOFFR") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CompareValidator ID="CompareValidator3" ControlToValidate="txtQTYOFFER" ErrorMessage="Silly rabit, quantity must be an Integer." Display="none" Operator="DataTypeCheck" Type="Integer" runat="server" />
                        <asp:TextBox Size="1" ID="txtQTYOFFER" MaxLength="2" Text='<%# DataBinder.Eval(Container, "DataItem.QTYOFFR") %>' runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtQTYOFFER" Display="none" ErrorMessage="Offer quantity can not be blank" />
                        <asp:RangeValidator ID="RangeValidator3" ControlToValidate="txtQTYOFFER" runat="server" ErrorMessage="Offer quantity must be greater than one." Display="none" Type="Integer" MinimumValue="1" MaximumValue="100" />
                    </EditItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid> 
        </td><td  valign="top">
        <div  align="right">
        <img src="/images/misc/trainload.gif" border="0">       
        
            <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <li><a href="/docs/userguide.pdf" target="_blank" class="LinkNormal">User Guide</a>
                                <%if (Session["Typ"].ToString() == "S")
                                  {%>
                                <li><a href="/docs/seller.pdf" target="_blank" class="LinkNormal">Seller's Agreement (pdf)</a>
                            <li><a href="/docs/Seller_Agreement_NewWind.aspx" target="_blank" class="LinkNormal">Seller's Agreement (html)</a>
                                <%}
                                  else if (Session["Typ"].ToString() == "P")
                                  {%>
                                <li><a href="/docs/buyer.pdf" target="_blank" class="LinkNormal">Buyer's Agreement (pdf)</a>
                            <li><a href="/docs/Buyer_Agreement_NewWind.aspx" target="_blank" class="LinkNormal">Buyer's Agreement (html)</a>
                                <%}%>
                                <li><a href="javascript:sealWin=window.open(&quot;/docs/Rules_of_the_Exchange4PopUp.aspx&quot;,&quot;win&quot;,'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=805,height=500');void(0)" class="LinkNormal">Rules Of The Exchange</a>
                        </td>
                    </tr>
                </table>
               </div>
        </td></tr>      
        </table> 
   </div>

                <!--Products:
                        <asp:DropDownList id="ddlResinFilter" runat="server"
                            AutoPostBack="true" OnSelectedIndexChanged="Reset_Grid" />-->
                <!--Ship Month:
                        <asp:DropDownList id="ddlFWDMonth" runat="server"
                            AutoPostBack="true" OnSelectedIndexChanged="Reset_Grid" />-->

</asp:Content>