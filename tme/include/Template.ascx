<%@ Register TagPrefix="mbdb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.DefaultButtons" %>
<%@ Control Language="c#" AutoEventWireup="True" Codebehind="Template.ascx.cs" Inherits="localhost.include.Template" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>
			<%=PageTitle%>
		</title>
		<asp:panel id="pnHeader" runat="server">
			<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
			<script type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
			</script>
			<link href="/tpe.css" type="text/css" rel="stylesheet"/>
			<style type="text/css">BODY { MARGIN: 0px; BACKGROUND-COLOR: #ffffff }
	#menu { Z-INDEX: 1; LEFT: 351px; VISIBILITY: hidden; WIDTH: 128px; POSITION: absolute; TOP: 10px; HEIGHT: 97px }
	#Layer1 { Z-INDEX: 1; LEFT: 124px; WIDTH: 779px; POSITION: absolute; TOP: 124px; HEIGHT: 155px }
	.border {
	BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; BORDER-LEFT: #000000 1px solid; WIDTH: 778px; TEXT-ALIGN: center; background: #dbdcd7;
}
			</style>
			<asp:Panel id="pnAdminMenu" Visible="false" runat="server"> <!-- DHTML Menu Builder Loader Code START -->
				<div id="DMBRI" style="POSITION: absolute"><IMG height="1" alt="" src="/Pics/menu/dmb_i.gif" width="1" border="0" name="DMBImgFiles">
					<IMG height="1" alt="" src="/menu/dmb_m.gif" width="1" border="0" name="DMBJSCode">
				</div>
				<script language="JavaScript" type="text/javascript">
        var rimPath=null;var rjsPath=null;var rPath2Root=null;function InitRelCode(){var iImg;var jImg;var tObj;if(!document.layers){iImg=document.images['DMBImgFiles'];jImg=document.images['DMBJSCode'];tObj=jImg;}else{tObj=document.layers['DMBRI'];if(tObj){iImg=tObj.document.images['DMBImgFiles'];jImg=tObj.document.images['DMBJSCode'];}}if(!tObj){window.setTimeout("InitRelCode()",700);return false;}rimPath=_gp(iImg.src);rjsPath=_gp(jImg.src);rPath2Root=rjsPath+"../";return true;}function _purl(u){return xrep(xrep(u,"%%REP%%",rPath2Root),"\\","/");}function _fip(img){if(img.src.indexOf("%%REL%%")!=-1) img.src=rimPath+img.src.split("%%REL%%")[1];return img.src;}function _gp(p){return p.substr(0,p.lastIndexOf("/")+1);}function xrep(s,f,n){if(s) s=s.split(f).join(n);return s;}InitRelCode();
				</script>
				<script language="JavaScript" type="text/javascript">
        function LoadMenus() {if(!rjsPath){window.setTimeout("LoadMenus()", 10);return false;}var navVer = navigator.appVersion;
        if(navVer.substr(0,3) >= 4)
        if((navigator.appName=="Netscape") && (parseInt(navigator.appVersion)==4)) {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/nsAdminDDmenu.js"><\/script\>');
        } else {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/ieAdminDDmenu.js"><\/script\>');
        }return true;}LoadMenus();</script>
				<!-- DHTML Menu Builder Loader Code END -->
			</asp:Panel>
			<asp:Panel id="pnBrokerNav" Visible="false" runat="server"> <!-- DHTML Menu Builder Loader Code START -->
				<div id="DMBRI" style="POSITION: absolute"><IMG height="1" alt="" src="/Pics/menu/dmb_i.gif" width="1" border="0" name="DMBImgFiles">
					<IMG height="1" alt="" src="/menu/dmb_m.gif" width="1" border="0" name="DMBJSCode">
				</div>
				<script language="JavaScript" type="text/javascript">
        var rimPath=null;var rjsPath=null;var rPath2Root=null;function InitRelCode(){var iImg;var jImg;var tObj;if(!document.layers){iImg=document.images['DMBImgFiles'];jImg=document.images['DMBJSCode'];tObj=jImg;}else{tObj=document.layers['DMBRI'];if(tObj){iImg=tObj.document.images['DMBImgFiles'];jImg=tObj.document.images['DMBJSCode'];}}if(!tObj){window.setTimeout("InitRelCode()",700);return false;}rimPath=_gp(iImg.src);rjsPath=_gp(jImg.src);rPath2Root=rjsPath+"../";return true;}function _purl(u){return xrep(xrep(u,"%%REP%%",rPath2Root),"\\","/");}function _fip(img){if(img.src.indexOf("%%REL%%")!=-1) img.src=rimPath+img.src.split("%%REL%%")[1];return img.src;}function _gp(p){return p.substr(0,p.lastIndexOf("/")+1);}function xrep(s,f,n){if(s) s=s.split(f).join(n);return s;}InitRelCode();
				</script>
				<script language="JavaScript" type="text/javascript">
        function LoadMenus() {if(!rjsPath){window.setTimeout("LoadMenus()", 10);return false;}var navVer = navigator.appVersion;
        if(navVer.substr(0,3) >= 4)
        if((navigator.appName=="Netscape") && (parseInt(navigator.appVersion)==4)) {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/nsBrokerDDmenu.js"><\/script\>');
        } else {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/ieBrokerDDmenu.js"><\/script\>');
        }return true;}LoadMenus();</script>
				<!-- DHTML Menu Builder Loader Code END -->
			</asp:Panel>
			<asp:Panel id="pnDistributorNav" Visible="false" runat="server"> <!-- DHTML Menu Builder Loader Code START -->
				<div id="DMBRI" style="POSITION: absolute"><IMG height="1" alt="" src="/Pics/menu/dmb_i.gif" width="1" border="0" name="DMBImgFiles">
					<IMG height="1" alt="" src="/menu/dmb_m.gif" width="1" border="0" name="DMBJSCode">
				</div>
				<script language="JavaScript" type="text/javascript">
        var rimPath=null;var rjsPath=null;var rPath2Root=null;function InitRelCode(){var iImg;var jImg;var tObj;if(!document.layers){iImg=document.images['DMBImgFiles'];jImg=document.images['DMBJSCode'];tObj=jImg;}else{tObj=document.layers['DMBRI'];if(tObj){iImg=tObj.document.images['DMBImgFiles'];jImg=tObj.document.images['DMBJSCode'];}}if(!tObj){window.setTimeout("InitRelCode()",700);return false;}rimPath=_gp(iImg.src);rjsPath=_gp(jImg.src);rPath2Root=rjsPath+"../";return true;}function _purl(u){return xrep(xrep(u,"%%REP%%",rPath2Root),"\\","/");}function _fip(img){if(img.src.indexOf("%%REL%%")!=-1) img.src=rimPath+img.src.split("%%REL%%")[1];return img.src;}function _gp(p){return p.substr(0,p.lastIndexOf("/")+1);}function xrep(s,f,n){if(s) s=s.split(f).join(n);return s;}InitRelCode();
				</script>
				<script language="JavaScript" type="text/javascript">
        function LoadMenus() {if(!rjsPath){window.setTimeout("LoadMenus()", 10);return false;}var navVer = navigator.appVersion;
        if(navVer.substr(0,3) >= 4)
        if((navigator.appName=="Netscape") && (parseInt(navigator.appVersion)==4)) {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/nsDistributorDDmenu.js"><\/script\>');
        } else {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/ieDistributorDDmenu.js"><\/script\>');
        }return true;}LoadMenus();</script>
				<!-- DHTML Menu Builder Loader Code END -->
			</asp:Panel>
			<asp:Panel id="pnPurchaserNav" Visible="false" runat="server"> <!-- DHTML Menu Builder Loader Code START -->
				<div id="DMBRI" style="POSITION: absolute"><IMG height="1" alt="" src="/Pics/menu/dmb_i.gif" width="1" border="0" name="DMBImgFiles">
					<IMG height="1" alt="" src="/menu/dmb_m.gif" width="1" border="0" name="DMBJSCode">
				</div>
				<script language="JavaScript" type="text/javascript">
        var rimPath=null;var rjsPath=null;var rPath2Root=null;function InitRelCode(){var iImg;var jImg;var tObj;if(!document.layers){iImg=document.images['DMBImgFiles'];jImg=document.images['DMBJSCode'];tObj=jImg;}else{tObj=document.layers['DMBRI'];if(tObj){iImg=tObj.document.images['DMBImgFiles'];jImg=tObj.document.images['DMBJSCode'];}}if(!tObj){window.setTimeout("InitRelCode()",700);return false;}rimPath=_gp(iImg.src);rjsPath=_gp(jImg.src);rPath2Root=rjsPath+"../";return true;}function _purl(u){return xrep(xrep(u,"%%REP%%",rPath2Root),"\\","/");}function _fip(img){if(img.src.indexOf("%%REL%%")!=-1) img.src=rimPath+img.src.split("%%REL%%")[1];return img.src;}function _gp(p){return p.substr(0,p.lastIndexOf("/")+1);}function xrep(s,f,n){if(s) s=s.split(f).join(n);return s;}InitRelCode();
				</script>
				<script language="JavaScript" type="text/javascript">
        function LoadMenus() {if(!rjsPath){window.setTimeout("LoadMenus()", 10);return false;}var navVer = navigator.appVersion;
        if(navVer.substr(0,3) >= 4)
        if((navigator.appName=="Netscape") && (parseInt(navigator.appVersion)==4)) {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/nsPurchaserDDmenu.js"><\/script\>');
        } else {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/iePurchaserDDmenu.js"><\/script\>');
        }return true;}LoadMenus();</script>
				<!-- DHTML Menu Builder Loader Code END -->
			</asp:Panel>
			<asp:Panel id="pnSellerNav" Visible="false" runat="server"> <!-- DHTML Menu Builder Loader Code START -->
				<div id="DMBRI" style="POSITION: absolute"><IMG height="1" alt="" src="/Pics/menu/dmb_i.gif" width="1" border="0" name="DMBImgFiles">
					<IMG height="1" alt="" src="/menu/dmb_m.gif" width="1" border="0" name="DMBJSCode">
				</div>
				<script language="JavaScript" type="text/javascript">
        var rimPath=null;var rjsPath=null;var rPath2Root=null;function InitRelCode(){var iImg;var jImg;var tObj;if(!document.layers){iImg=document.images['DMBImgFiles'];jImg=document.images['DMBJSCode'];tObj=jImg;}else{tObj=document.layers['DMBRI'];if(tObj){iImg=tObj.document.images['DMBImgFiles'];jImg=tObj.document.images['DMBJSCode'];}}if(!tObj){window.setTimeout("InitRelCode()",700);return false;}rimPath=_gp(iImg.src);rjsPath=_gp(jImg.src);rPath2Root=rjsPath+"../";return true;}function _purl(u){return xrep(xrep(u,"%%REP%%",rPath2Root),"\\","/");}function _fip(img){if(img.src.indexOf("%%REL%%")!=-1) img.src=rimPath+img.src.split("%%REL%%")[1];return img.src;}function _gp(p){return p.substr(0,p.lastIndexOf("/")+1);}function xrep(s,f,n){if(s) s=s.split(f).join(n);return s;}InitRelCode();
				</script>
				<script language="JavaScript" type="text/javascript">
        function LoadMenus() {if(!rjsPath){window.setTimeout("LoadMenus()", 10);return false;}var navVer = navigator.appVersion;
        if(navVer.substr(0,3) >= 4)
        if((navigator.appName=="Netscape") && (parseInt(navigator.appVersion)==4)) {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/nsSellerDDmenu.js"><\/script\>');
        } else {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/ieSellerDDmenu.js"><\/script\>');
        }return true;}LoadMenus();</script>
				<!-- DHTML Menu Builder Loader Code END -->
			</asp:Panel>
			<asp:Panel ID="pnDefault" Runat="server" Visible="False" TabIndex=1>
				<div id="DMBRI" style="POSITION: absolute"><IMG height="1" alt="" src="/Pics/menu/dmb_i.gif" width="1" border="0" name="dmbif">
					<IMG height="1" alt="" src="/menu/dmb_m.gif" width="1" border="0" name="dmbjs">
				</div>
				<script language="JavaScript" type="text/javascript">
var rimPath=null;var rjsPath=null;var rPath2Root=null;function InitRelCode(){var iImg;var jImg;var tObj;if(!document.layers){iImg=document.images['dmbif'];jImg=document.images['dmbjs'];tObj=jImg;}else{tObj=document.layers['DMBRI'];if(tObj){iImg=tObj.document.images['dmbif'];jImg=tObj.document.images['dmbjs'];}}if(!tObj){window.setTimeout("InitRelCode()",700);return false;}rimPath=_gp(iImg.src);rjsPath=_gp(jImg.src);rPath2Root=rjsPath+"../";return true;}function _purl(u){return xrep(xrep(u,"%%REP%%",rPath2Root),"\\","/");}function _fip(img){if(img.src.indexOf("%%REL%%")!=-1) img.src=rimPath+img.src.split("%%REL%%")[1];return img.src;}function _gp(p){return p.substr(0,p.lastIndexOf("/")+1);}function xrep(s,f,n){if(s) s=s.split(f).join(n);return s;}InitRelCode();
				</script>
				<script language="JavaScript" type="text/javascript">
function LoadMenus() {if(!rjsPath){window.setTimeout("LoadMenus()", 10);return false;}var navVer = navigator.appVersion;
if(navVer.substr(0,3) >= 4)
if((navigator.appName=="Netscape") && (parseInt(navigator.appVersion)==4)) {
document.write('<' + 'script language="JavaScript" type="text/javascript" src="' + rjsPath + 'nsDefaultDDmenu.js"><\/script\>');
} else {
document.write('<' + 'script language="JavaScript" type="text/javascript" src="' + rjsPath + 'ieDefaultDDmenu.js"><\/script\>');
}return true;}LoadMenus();</script>
				<!-- DHTML Menu Builder Loader Code END -->
			</asp:Panel>
	</head>
	<body onload="MM_preloadImages('/pics/menu/menu_red_r1_c1.jpg','/menu/pics/menu_red_r1_c3.jpg','/pics/menu/menu_red_r1_c5.jpg','/pics/menu/menu_red_r1_c7.jpg')">
	<asp:Panel id="pnContextMenu" Visible="false" runat="server">
				<style type="text/css">
					<!--

        /* Context menu Script-  Dynamic Drive (www.dynamicdrive.com) Last updated: 01/08/22
        For full source code and Terms Of Use, visit http://www.dynamicdrive.com */

        .skin0{
        position:absolute;
        width:165px;
        border:2px solid black;
        background-color:menu;
        font-family:Verdana;
        line-height:20px;
        cursor:default;
        font-size:12px;
        z-index:100;
        visibility:hidden;
        }

        .menuitems{
        padding-left:10px;
        padding-right:10px;
        }
        --></style>
				<div id="ie5menu" class="skin0" onMouseover="highlightie5(event)" onMouseout="lowlightie5(event)"
					onClick="jumptoie5(event)" display:none>					
					<div class="menuitems" url="/Administrator/CRM.aspx">Demo Leads</div>
					<!-- only mike will see this link -->
					<%
        if (Session["ID"].ToString() == "2") Response.Write("<div class='menuitems' url='/Administrator/CRM.aspx?EmailOffers=true'>Trading Partners</div>");
        
        %>
					<hr>
					<div class="menuitems" url="/Spot/Floor_Summary.aspx">Floor Summary</div>
					<div class="menuitems" url="/Spot/Spot_Floor.aspx">Spot Floor</div>
					<div class="menuitems" url="/Spot/Resin_Entry.aspx">Order Entry</div>
					<div class="menuitems" url="/Spot/Matched_Order.aspx">Matched Order</div>
					<div class="menuitems" url="/Creditor/Inventory.aspx">Inventory</div>
					<hr>
					<div class="menuitems" url="/Research/Charts.aspx">Charts</div>
					<div class="menuitems" url="/Forward/Admin_Forward_Floor.aspx">Contract Prices</div>
					<hr>
					<div class="menuitems" url="/Creditor/CurrARAPAging.aspx">Credit Management</div>
					<div class="menuitems" url="/Creditor/Transaction_Summary.aspx">Transaction Summary</div>
				</div>
				<script type="text/javascript" language="JavaScript1.2">

        //set this variable to 1 if you wish the URLs of the highlighted menu to be displayed in the status bar
        var display_url=0

        var ie5=document.all&&document.getElementById
        var ns6=document.getElementById&&!document.all
        if (ie5||ns6)
        var menuobj=document.getElementById("ie5menu")

        function showmenuie5(e){
        //Find out how close the mouse is to the corner of the window
        var rightedge=ie5? document.body.clientWidth-event.clientX : window.innerWidth-e.clientX
        var bottomedge=ie5? document.body.clientHeight-event.clientY : window.innerHeight-e.clientY

        //if the horizontal distance isn't enough to accomodate the width of the context menu
        if (rightedge<menuobj.offsetWidth)
        //move the horizontal position of the menu to the left by it's width
        menuobj.style.left=ie5? document.body.scrollLeft+event.clientX-menuobj.offsetWidth : window.pageXOffset+e.clientX-menuobj.offsetWidth
        else
        //position the horizontal position of the menu where the mouse was clicked
        menuobj.style.left=ie5? document.body.scrollLeft+event.clientX : window.pageXOffset+e.clientX

        //same concept with the vertical position
        if (bottomedge<menuobj.offsetHeight)
        menuobj.style.top=ie5? document.body.scrollTop+event.clientY-menuobj.offsetHeight : window.pageYOffset+e.clientY-menuobj.offsetHeight
        else
        menuobj.style.top=ie5? document.body.scrollTop+event.clientY : window.pageYOffset+e.clientY

        menuobj.style.visibility="visible"
        return false
        }

        function hidemenuie5(e){
        menuobj.style.visibility="hidden"
        }

        function highlightie5(e){
        var firingobj=ie5? event.srcElement : e.target
        if (firingobj.className=="menuitems"||ns6&&firingobj.parentNode.className=="menuitems"){
        if (ns6&&firingobj.parentNode.className=="menuitems") firingobj=firingobj.parentNode //up one node
        firingobj.style.backgroundColor="highlight"
        firingobj.style.color="white"
        if (display_url==1)
        window.status=event.srcElement.url
        }
        }

        function lowlightie5(e){
        var firingobj=ie5? event.srcElement : e.target
        if (firingobj.className=="menuitems"||ns6&&firingobj.parentNode.className=="menuitems"){
        if (ns6&&firingobj.parentNode.className=="menuitems") firingobj=firingobj.parentNode //up one node
        firingobj.style.backgroundColor=""
        firingobj.style.color="black"
        window.status=''
        }
        }

        function jumptoie5(e){
        var firingobj=ie5? event.srcElement : e.target
        if (firingobj.className=="menuitems"||ns6&&firingobj.parentNode.className=="menuitems"){
        if (ns6&&firingobj.parentNode.className=="menuitems") firingobj=firingobj.parentNode
        if (firingobj.getAttribute("target"))
        window.open(firingobj.getAttribute("url"),firingobj.getAttribute("target"))
        else
        window.location=firingobj.getAttribute("url")
        }
        }

        if (ie5||ns6){
        menuobj.style.display=''
        document.oncontextmenu=showmenuie5
        document.onclick=hidemenuie5
        }

				</script>
			</asp:Panel>
		<table cellspacing="0" cellpadding="0" align="center" border="0">
			<tr>
				<td>
					<div class="border" align="center">

		<table id="table1" cellSpacing="0" cellPadding="0" align="center" border="0" <% if (Width != null) { Response.Write("width=\""+Width+"\"");}%> >
			<tr>
				<td vAlign="top" width="100%">
					<table cellSpacing="0" cellPadding="0" align="center" border="0">
						<asp:panel id="pnLogin" runat="server">
							<TBODY>
								<tr>
									<td width="648" height="21"><a href="/default.aspx"><IMG height="51" src="/pics/logo_up_r1_c1.jpg" width="648" border="0"></a></td>
									<td>
										<table id="Table2" cellSpacing="0" cellPadding="0" border="0">
											<tr>
												<td align="center" width="132" background="/pics/register_r1_c1.jpg" height="21">
												<A href="/Public/Registration.aspx?Referrer=<%=Request.Url.ToString()%>" style="COLOR: black">Register</A></td>
											</tr>
											<tr>
												<td height="6"><IMG height="13" src="/pics/register_r2_c1.jpg" width="132"></td>
											</tr>
											<tr>
												<td align="right" background="/pics/register_r3_c1.jpg" height="17"><A style="COLOR: #f3c300;font-size:10px" href="/Public/Forgot_Password.aspx">Forgot your password?</A></td>
											</tr>
										</table>
									</td>
									<td background="/images2/loggedmenu/logo_top_r1_c23.gif"></td>
								</tr>
								<tr>
									<td width="648">
										<table cellSpacing="0" cellPadding="0" border="0">
											<tr>
												<td><a href="/default.aspx"><IMG height="53" src="/pics/logo_up_r4_c2.jpg" border="0"></a></td>
												<td>
													<asp:imagebutton id="btnLogin" runat="server" CausesValidation="False" BorderStyle="None" ImageUrl="/pics/login_image.jpg"></asp:imagebutton></td>
												<td>
													<asp:image id="imgEmailPassword" Runat="server" height="53" width="68" src="/pics/email_password_lables.jpg"></asp:image></td>
											</tr>
										</table>
									</td>
									<td background="/pics/fields.jpg">
										<table id="Table3" cellSpacing="0" cellPadding="1" width="100%" border="0">
											<tr>
												<td>
													<mbdb:defaultbuttons id="DefaultLoginButton" runat="server">
														<mbdb:DefaultButtonSetting parent="Password" button="btnLogin" />
														<mbdb:DefaultButtonSetting parent="UserName" button="btnLogin" />
													</mbdb:defaultbuttons>
													<asp:textbox id="UserName" CssClass="formzindex" runat="server" Width="110px" maxlength="100"></asp:textbox></td>
											</tr>
											<tr>
												<td>
													<asp:textbox id="Password" CssClass="formzindex" runat="server" Width="110px" maxlength="100" TextMode="Password"></asp:textbox></td>
											</tr>
										</table>
									</td>
									<td background="/images2/loggedmenu/logo_top_r2_c23.gif"></td>
								</tr>
						</asp:panel><asp:panel id="pnLogout" runat="server" Visible="False">
							<tr>
								<td height="21"><a href="/default.aspx"><IMG height="51" src="/images2/loggedmenu/logo_top_r1_c1.jpg" border="0"></a></td> <!--upper strenght for Logged menu -->
								<td background="/images2/loggedmenu/logo_top_r1_c23.gif"></td>
								<td class="loggedname" vAlign="bottom" background="/images2/loggedmenu/logo_top_r1_c23.gif">Welcome,
									<asp:Label id="lblUser" runat="server">Guest</asp:Label></td>
							</tr>
							<tr>
								<td><a href="/default.aspx"><IMG height="53" src="/images2/loggedmenu/logo_top_r2_c1.jpg" width="588" border="0"></a></td>
								<td background="/images2/loggedmenu/logo_top_r2_c1.jpg"></td>
								<td vAlign="top" width="100%" background="/images2/loggedmenu/logo_top_r2_c23.gif"><A class="menuwhite" href="/common/FUNCLogout.aspx">Logout</A></td>
							</tr>
						</asp:panel></table>
					<table cellSpacing="0" cellPadding="0" align="center" border="0">
						<tr>
							<td width="100%" background="/pics/menu/background.jpg"></td>
							<td><img src="/Pics/menu/menu_orange_phone-1.jpg"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="100%" bgColor="#dbdcd7" height="100%" valign="top"></asp:panel><asp:panel id="pnFooter" runat="server" Visible="False" BorderColor="#999999"><!-- begin footer --></td>
			</tr>
			<tr>
				<td align="center" bgColor="#464646" colSpan="5" height="15"><B><A href="/Public/Privacy_Policy.aspx"><FONT face="Verdana,Arial" color="white" size="1">Privacy 
							Statement</A> | 2005 The Plastics Exchange. LLC. | All Rights Reserved. 
						Patent Pending</B></FONT>
				</td>
			</tr> <!-- begin close container table --> </td></tr></TBODY></table> <!-- end close container table --> </asp:panel></div></td></tr></table>
			<asp:Literal ID="LivePerson" Visible="true" Runat="server"></asp:Literal>
			</BODY>
</html>
