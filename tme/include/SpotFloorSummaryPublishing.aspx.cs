using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace localhost.include
{
	/// <summary>
	/// Summary description for SpotFloorSummaryPublishing.
	/// </summary>
	public partial class SpotFloorSummaryPublishing : System.Web.UI.Page
	{
		string company;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			company = HelperFunction.getSafeStringFromDB(this.Request.QueryString["Referrer"]);
			BindSpotSummary();
		}

		private void BindSpotSummary()//bind the data with the grid.
		{
			//select the data from the bboffer table,and group the record by the data stored in the OFFR_PROD field of the BBOFFER table.to calculate the count of records,the maximum price,the minimum price and the count of the weight for each group.
			string seleStr ="SELECT ";
			seleStr+="count=count(*), ";
			seleStr+="prodlink=('/Spot/Spot_Floor.aspx?Filter='), ";
			seleStr+="GRADE= (SELECT GRADE_NAME from GRADE WHERE GRADE_ID = OFFR_GRADE), ";
			seleStr+="offr_grade, ";
			seleStr+="Min=Cast(min(offr_prce) AS money) , ";//Cast(min(offr_prce) AS money)
			seleStr+="Max=Cast(max(offr_prce) AS money), ";
			seleStr+="VARSIZE=sum(OFFR_QTY*OFFR_SIZE) ";
			//seleStr+="FROM bboffer where offr_grade is not null group by offr_grade  ";
			seleStr+="FROM bboffer where offr_grade is not null and offr_port is null and OFFR_SIZE <> '38500' and OFFR_SIZE <> '44500' group by offr_grade  ";
			seleStr+="ORDER BY VARSIZE DESC";

			System.Data.SqlClient.SqlConnection conn=new System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString());//connect to the database.
			System.Data.DataSet ds=new DataSet();
			System.Data.SqlClient.SqlDataAdapter ad=new System.Data.SqlClient.SqlDataAdapter(seleStr,conn);
			ad.Fill(ds);//set the query data to dataset.
			
			dgSummaryGrid.DataSource=ds;//get data from the dataset.
			dgSummaryGrid.DataBind();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgSummaryGrid.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgSummaryGrid_ItemDataBound);

		}
		#endregion

		private void dgSummaryGrid_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			//string strGrade;
		
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem  )
			{
				e.Item.Cells[4].Text =e.Item.Cells[2].Text +"-" +e.Item.Cells[3].Text;
			
//				switch ((DataBinder.Eval(e.Item.DataItem, "GRADE")).ToString())// store the grade 
//				{
//					case "HMWPE - Film Grade":
//						strGrade ="1";
//						break;
//					case "HDPE - Inj":
//						strGrade ="2";
//						break;
//					case "HDPE - Blow Mold":
//						strGrade ="3";
//						break;
//					case "LDPE - Film":
//						strGrade ="6";
//						break;
//					case "LDPE - Inj":
//						strGrade ="10";
//						break;
//					case "LLDPE - Film":
//						strGrade ="14";
//						break;
//					case "LLDPE - Inj":
//						strGrade ="17";
//						break;
//					case "PP Homopolymer - Inj":
//						strGrade ="26";
//						break;
//					case "PP Copolymer - Inj":
//						strGrade ="22";
//						break;
//					case "GPPS":
//						strGrade ="19";
//						break;
//					case "HIPS":
//						strGrade ="20";
//						break;
//					default:
//						strGrade ="14";
//						break;
//				}
				e.Item.Attributes.Add("onclick", "window.open('http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "?Referrer=" + company + "');");
				//window.open('/administrator/user_Details.aspx?ID=4661','win','scrollbars=yes,resizable=yes,width=550,height=565')
				e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver';this.style.cursor='hand';");
				e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='F1F1F1';this.style.cursor='pointer';");
			}
		}

		protected void hplTotalLbs_PreRender(object sender, System.EventArgs e)
		{
			//Total lbs.
			decimal totalLbs = totalLbs = HelperFunction.totalPoundsOffers(this.Context);
			hplTotalLbs.Text = String.Format("{0:#,###}",totalLbs) + " lbs";
			hplTotalLbs.NavigateUrl = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "?Referrer=" + company;
			hplTotalLbs.Target = "_blank";
		}
	}
}
