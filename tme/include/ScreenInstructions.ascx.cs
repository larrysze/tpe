namespace localhost.include
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for WebUserControl1.
	/// </summary>
	public partial class ScreenInstructions : System.Web.UI.UserControl
	{

		private string _Content = "";
		private string _Height = "100";
		private string _Width = "100%";

		protected System.Web.UI.WebControls.Panel pnHeader;

		public string Content 
		{
			get { return _Content; }
			set { _Content = value; }
		}
		public string Height
		{
			get { return _Height; }
			set { _Height = value; }
		}

		public string Width
		{
			get { return _Width; }
			set { _Width = value; }
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
