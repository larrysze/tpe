using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;

//
// http://www.dotnetbips.com/displayarticle.aspx?id=43f
// http://www.microbion.co.uk/developers/csharp/dirlist.htm

// Stripping HTML
// http://www.4guysfromrolla.com/webtech/042501-1.shtml

// Opening a file from ASP.NET
// http://aspnet.4guysfromrolla.com/articles/051802-1.aspx

// Practical parsing in Regular Expressions
// http://weblogs.asp.net/rosherove/articles/6946.aspx
namespace TPE.NewsCrawler {

    /// <summary>Catalog of words and pages<summary>
	public class Catalog {
		/// <summary>Internal datastore of Words referencing Files</summary>
		private System.Collections.Hashtable index;	//TODO: implement collection with faster searching

		public int Length {
		  get {return index.Count;}
		}
		/// <summary>Constructor</summary>
		public Catalog () {
			index = new System.Collections.Hashtable ();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="searchWord"></param>
		/// <param name="fullCatalogPath"></param>
		/// <returns>hashtable of xml file names and number of occurences of searchWord found</returns>
		public static Hashtable Search(string searchWord, string path, string catalogFileName)
		{		
			string fullCatalogPath = path + @"\" +  catalogFileName;
			if (!System.IO.File.Exists(fullCatalogPath))
			{
				// catalog doesn't exist so create
				TPE.NewsCrawler.NewsCrawler Crawler = new NewsCrawler(path,"*.xml",catalogFileName);
			}
			using (FileStream fs = new FileStream(fullCatalogPath,FileMode.Open))
			{
				BinaryFormatter bf = new BinaryFormatter();
				Hashtable index = new Hashtable();
				index = (Hashtable)bf.Deserialize(fs);
				searchWord = searchWord.Trim('?','\"', ',', '\'', ';', ':', '.', '(', ')').ToLower();
				//Hashtable retval = null;
				Hashtable retval = new Hashtable();
				foreach(string s in searchWord.Split(' '))
				{
					if (index.ContainsKey (s) ) 
					{ // does all the work !!!
						Word thematch = (Word)index[s];
						Hashtable ht = thematch.InFiles();
						foreach(DictionaryEntry d in ht)
						{
							if (retval.ContainsKey(d.Key))
								retval[d.Key] = (int)retval[d.Key] + (int)d.Value;
							else
								retval.Add(d.Key,d.Value);//thematch.InFiles(); // return the collection of File objects
						}
					}
				}
				return retval;			
			}
		}		
		
		public void save(string path)
		{
			if (System.IO.File.Exists(path))
			{
				System.IO.File.Delete(path);
			}
			using (FileStream fs = new FileStream(path,FileMode.CreateNew,FileAccess.Write))
			{
				BinaryFormatter bf=new BinaryFormatter();
				bf.Serialize(fs,index);
			}
		}

		/// <summary>Add a new Word/File pair to the Catalog</summary>
		public bool Add (string word, File infile, int position)
		{
			// ### Make sure the Word object is in the index ONCE only
			if (index.ContainsKey (word) ) 
			{
				Word theword = (Word)index[word];	// add this file reference to the Word
				theword.Add(infile, position);
			} else {
				Word theword = new Word(word, infile, position);	// create a new Word object
				index.Add(word, theword);
			}
			return true;
		}
		/// <summary>Debug string</summary>
		public override string ToString() {
			string wordlist="";
			//foreach (object w in index.Keys) temp += ((Word)w).ToString();	// output ALL words, will take a long time
			return "\nCATALOG :: " + index.Values.Count.ToString() + " words.\n" + wordlist;
		}
	}

    /// <summary>Instance of a word<summary>
    /// 
    [Serializable]
	public class Word {
        /// <summary>The cataloged word</summary>
        public string Text;
		/// <summary>Collection of files the word appears in</summary>
		private System.Collections.Hashtable fileCollection = new System.Collections.Hashtable ();
		/// <summary>Constructor with first file reference</summary>
		public Word (string text, File infile, int position) 
		{
			Text = text;
			//WordInFile thefile = new WordInFile(filename, position);
			fileCollection.Add(infile, 1);
		}
		/// <summary>Add a file referencing this word</summary>
		public void Add (File infile, int position) 
		{
			if (fileCollection.ContainsKey (infile)) 
			{
				int wordcount = (int)fileCollection[infile];
				fileCollection[infile] = wordcount + 1 ; //thefile.Add (position);
			} 
			else 
			{
				//WordInFile thefile = new WordInFile(filename, position);
				fileCollection.Add (infile, 1);
			}
		}
		/// <summary>Collection of files containing this Word (Value=WordCount)</summary>
		public Hashtable InFiles () 
		{
			return fileCollection;
		}
		/// <summary>Debug string</summary>
		public override string ToString() {
			string temp="";
			foreach (object tempFile in fileCollection.Values) temp += ((File)tempFile).ToString();
			return "\tWORD :: " + Text + "\n\t\t" + temp + "\n";
		}
	}

	/// <summary>TPE.NewsCrawler.File attributes</summary>
	/// <remarks>Beware ambiguity with System.IO.File - always fully qualify (TPE.NewsCrawler.File) object references</remarks>
	[Serializable]
    public class File {
        public string Url;
		public string Name;
        public string Title;
		public string Description;
		public DateTime Date;

        /// <summary>Constructor requires all File attributes</summary>
        public File (string url, string name, string title, string description, DateTime date) {
			Title       = title;
			Description = description;
			Url         = url;
			Date		= date;
			Name		= name;
        }
        /// <summary>Debug string</summary>
		public override string ToString() {
			return "tFILE :: " + Url + " -- " + Title + "\n";
		}
    } // File

	/// <summary>
	/// Comparer used to sort hashtable by values in descending numerical order (ie, show most matches first)
	/// eg. foreach (string s in new IterSortHashValue(aHashtable,new DescendingNumberComparer()))
	/// </summary>
	public class DescendingNumberComparer:IComparer
	{	
		public int Compare(object obj1, object obj2)
		{
			int result = 0;
			if 	((int)obj1 < (int)obj2)
				result = 1;
			else if ((int)obj1 > (int)obj2)
				result = -1;
			else
				result = 0;
			
			return result;
		}			
	}
} // namespace Searcharoo.Net
