using System;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using System.Net;
using System.Text.RegularExpressions;
using System.Collections;
using TPE.Utility;
using System.Configuration;

namespace localhost
{
	/// <summary>
	/// Summary description for HelperFunction.
	/// </summary>
	public class HelperFunction
	{
		public HelperFunction()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		/// <summary>
		/// Returns the type of the company: D-Distributor, P-Purchase, S-Supplier and any other type
		/// </summary>
		/// <param name="companyID">ID of the company</param>
		/// <returns>Letter that represents the type of the company</returns>
		static public string CompanyType(HttpContext webContext, string companyID)
		{
			string compType = "";
			string strSQL="SELECT COMP_TYPE FROM COMPANY WHERE COMP_ID = " + companyID.ToString();

			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{
				conn.Open();
				using (SqlDataReader rdrCompany = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
				{
					rdrCompany.Read();
					compType = rdrCompany["COMP_TYPE"].ToString();
				}				
			}
			return compType;
		}
		
		static public string EmailBroker(HttpContext webContext, string PersID)
		{
			string emailBroker = "";
            string strSQL = "select broker.pers_mail from person broker, person lead where broker.pers_id = lead.pers_aces and lead.pers_id = " + PersID;
						
			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{
				conn.Open();
				using (SqlDataReader rdrBroker = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
				{
					if (rdrBroker.Read())
					{
						emailBroker = rdrBroker[0].ToString();
					}
				}
			}
			return emailBroker;
		}
        static public string EmailPerson(string ConnectionString, string PersonID)
        {
            string emailPerson = string.Empty;
            string sql = "select pers_mail from person where pers_id = @PersonID";
            Hashtable htParam = new Hashtable();
            htParam.Add("@PersonID", PersonID);
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                using (SqlDataReader drEmail = DBLibrary.GetDataReaderFromSelect(conn,sql,htParam))
                {
                    if (drEmail.Read())
                    {
                        emailPerson = drEmail[0].ToString();
                    }
                }
            }
            return emailPerson;
        }

		static public void LoadGrades(HttpContext webContext, DropDownList ddlControl, string OrderByField)
		{
			string strSQL="SELECT GRADE_NAME, GRADE_ID FROM GRADE ORDER BY " + OrderByField;

			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{
				conn.Open();
				using (SqlDataReader rdrGrade = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
				{
					while(rdrGrade.Read())
					{
						ddlControl.Items.Add(new ListItem(rdrGrade["GRADE_NAME"].ToString(), rdrGrade["GRADE_ID"].ToString()));
					}
				}				
			}
		}

		/// <summary>
		/// ExistFax function check out if the user has any fax in he/she FaxBox
		/// </summary>
		/// <param name="userID">ID of User, usually the same kept in the ID Session variable</param>
		/// <returns>returns true if exists any fax, returns false if not</returns>
		static public bool ExistFax(string connectionString, int userID)
		{
			string strSQL="SELECT 1 FROM FAX " +
				"WHERE (PERS_ID = @PersID) AND (FAX_ATTACHED = 0 OR FAX_ATTACHED IS NULL) and (FAX_ENBL = 1)";

			Hashtable param = new Hashtable();
			param.Add("@PersID", userID);
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				connection.Open();
				return DBLibrary.GetDataReaderFromSelect(connection, strSQL, param).HasRows;
			}
		}

		static public bool ExistEmail(HttpContext webContext, string Email)
		{
			return ExistEmail(webContext, Email, "");
		}

		static public bool ExistEmail(HttpContext webContext, string Email, string userID)
		{
			string strSQL="SELECT 1 FROM PERSON " +
				"WHERE PERS_MAIL = '" + Email + "'";

			if (userID!="") strSQL+=" AND PERS_ID <> " + userID;
			
			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{				
				conn.Open();
				return DBLibrary.GetDataReaderFromSelect(conn, strSQL).HasRows;
			}
		}

		static public string getSafeStringFromDB(object objString)
		{
			string ret = "";
			try
			{	
				if (objString!=null)
				{
					ret = objString.ToString();
				}
			}
			finally
			{
				//do nothing
			}
			
			return ret;
		}

		static public double getSafeDoubleFromDB(object objDouble)
		{
			double ret = 0;
			try
			{	
				if ((objDouble!=null) && (objDouble!=DBNull.Value))
				{
					ret = Convert.ToDouble(objDouble);
				}
			}
			finally
			{
				//do nothing
			}
			
			return ret;
		}

		static public long getSafeLongFromDB(object objLong)
		{
			long ret = 0;
			try
			{	
				if (objLong!=null)
				{
					ret = Convert.ToInt64(objLong);
				}
			}

			catch
			{
			}
			finally
			{
				//do nothing
			}
			
			return ret;
		}

		static public decimal getSafeDecimalFromDB(object objDecimal)
		{
			decimal ret = 0;
			try
			{	
				if (objDecimal!=null)
				{
					ret = Convert.ToDecimal(objDecimal);
				}
			}
			catch
			{
				//do nothing
			}
			finally
			{
				//do nothing
			}
			
			return ret;
		}

		static public bool getSafeBooleanFromDB(object objBoolean)
		{
			bool ret = false;
			try
			{	
				if (objBoolean!=DBNull.Value)
				{
					ret = Convert.ToBoolean(objBoolean);
				}
			}
			finally
			{
				//do nothing
			}
			
			return ret;
		}


		/// <summary>
		/// single quotes cause problems when they are used in SQL queries.  This removes them
		/// </summary>
		static public string RemoveSqlEscapeChars(string TextIN)
		{
			//  remove ' character
			string Parse;
			Parse = TextIN;
			Parse = Parse.Replace("'", "''");
			return Parse;

		}

		static public void RegisterBlockDisableEnterKey(System.Web.UI.Page page)
		{
			string jsB = @"
			<script>
				function keyDown(){
					if (window.event.keyCode == 13) {return false;}
					//return !(window.event && window.event.keyCode == 13);
				}
				document.onkeydown = keyDown;
			</script>";
			
			page.RegisterClientScriptBlock("Disable Enter Key",jsB);
		}

		static public void RegisterBlockNoEnterKeyPress(System.Web.UI.Page page)
		{
			string jsB = @"
			<script type='text/javascript'>
				function noenter(){
					return !(window.event && window.event.keyCode == 13); }
			</script>";
			
			page.RegisterClientScriptBlock("Disable Enter KeyPress",jsB);
		}

		static public string getShipmentID(string shipmentOrderID, string shipmentSKU, HttpContext httpContext)
		{
			using (SqlConnection conn = new SqlConnection(httpContext.Application["DBconn"].ToString()))
			{
				conn.Open();
				Hashtable ht = new Hashtable(2);
				ht.Add("@ShipmentOrderID",Int32.Parse(shipmentOrderID));
				ht.Add("@ShipmentSKU",shipmentSKU);
				using (SqlDataReader drShipmentID = DBLibrary.GetDataReaderFromSelect(conn, "Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID=@ShipmentOrderID and SHIPMENT_SKU =@ShipmentSKU", ht))
				{
					drShipmentID.Read();
					return drShipmentID["SHIPMENT_ID"].ToString();
				}				
			}
		}

		public static void InsertConfirmationCode(System.Web.UI.Page page_)
		{
			// Check if the page has validators
			if(page_.Validators.Count != 0)
			{
				// if it does, include an "else" clause, forcing passing through the validator code.
				page_.RegisterOnSubmitStatement("onsubmit", @"if(msg != '') { CheckOperation(true); } else ");
			}
			else
			{
				// if it doesn't, the onsubmit event just check with the message
				page_.RegisterOnSubmitStatement("onsubmit", @"if(msg != '') { CheckOperation(false); }");
			}
			
			// register the javascript code for confirmation
			page_.RegisterClientScriptBlock("CheckOperation", GenerateConfirmationScript());
		}

		/// <summary>
		/// Create an event OnMouseDown to the control for that suffer confirmation.
		/// </summary>
		public static void InsertControlConfirmationCode(System.Web.UI.WebControls.WebControl control_, string ConfirmationMessage_)
		{
			control_.Attributes.Add("onmousedown", "msg='" + ConfirmationMessage_ + "'");
		}

		/// <summary>
		/// Remove the event OnMouseDown to the control.
		/// </summary>
		public static void RemoveControlConfirmationCode(System.Web.UI.WebControls.WebControl control_)
		{
			control_.Attributes.Remove("onmousedown");
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		private static string GenerateConfirmationScript()
		{
			StringBuilder JavaScriptCode = new StringBuilder(
				@"<script language=""javascript"">
	var msg = """";

	function CheckOperation(ComValidator)
	{
		var ret = true;
		var validator;
		if(ComValidator)
		{
			ValidatorOnSubmit();
			validator = event.returnValue;
		}
		else
		{
			validator = true;
		}

		if(validator)
		{
			if(msg != '')
			{
				ret = window.confirm(msg);
			}
		}
		else
		{
			ret = false;
		}
		event.returnValue = ret;
		msg = '';
	}
</script>");

			return JavaScriptCode.ToString();
		}
		/// <summary>
		/// Gives a summary of the number of resins selected out of the total
		/// </summary>
		/// <param name="userID">iPref = The value of the preferences</param>
		/// <returns>A string that shows the number of selected out of the total</returns>
		static public string CountSelectedResins(HttpContext webContext,int iPref)
		{
			string strText = String.Empty;
			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{
				conn.Open();
				using (SqlDataReader dtrCount = DBLibrary.GetDataReaderFromSelect(conn, "SELECT (SELECT COUNT(*) FROM CONTRACT WHERE POWER(2,CONT_ID-1)&("+iPref.ToString()+")<>0) AS COUNT, (SELECT COUNT(*) FROM CONTRACT) AS TOTAL"))
				{
					dtrCount.Read();
					strText = "("+dtrCount["COUNT"].ToString() +"/" + dtrCount["TOTAL"].ToString()+")" ;
				}				
			}
			return strText;
		}

		static public int getInternationalPort(string DBconn,string personID)
		{
			int portID = 0;
			using (SqlConnection conn = new SqlConnection(DBconn))
			{
				conn.Open();
			
				using (SqlDataReader dtrCount = DBLibrary.GetDataReaderFromSelect(conn, "select pers_prmly_intrst, pers_port from person where pers_id = " + personID))
				{
					dtrCount.Read();
			
					string market = getSafeStringFromDB(dtrCount["pers_prmly_intrst"]);
					string port = getSafeStringFromDB(dtrCount["pers_port"]);
					if (market == "2") //Internationl
					{
						if (port != "") portID = System.Convert.ToInt32(port);
					}
					else
					{
						portID = getHoustonPortID(DBconn);
					}
				}
			}
			return portID;
		}

		static public int getHoustonPortID(string DBconn)
		{
			int portID = 0;
			using (SqlConnection conn = new SqlConnection(DBconn))
			{
				conn.Open();
				using (SqlDataReader dtrPorts = DBLibrary.GetDataReaderFromSelect(conn, "SELECT PORT_ID FROM INTERNATIONAL_PORT WHERE UPPER(PORT_CITY) LIKE '%HOUSTON%'"))
				{
					if (dtrPorts.Read()) portID = System.Convert.ToInt32(dtrPorts["PORT_ID"]);
				}	
			}
			return portID;
		}

		static public int SaveEmailSystem(string EmailBody, string emailType, string connString)
		{
			string strEmailSystemNumber = String.Empty;
			using (SqlConnection conn = new SqlConnection(connString))
			{
				conn.Open();
			
				SqlCommand cmdEmail = new SqlCommand("spAddEmailSystem", conn);
				cmdEmail.CommandType = CommandType.StoredProcedure;
				cmdEmail.Parameters.Add("@EmailBody",EmailBody);
				cmdEmail.Parameters.Add("@EmailType",emailType);			
				cmdEmail.ExecuteNonQuery();

				//Getting the ID from that item just added
				string sqlID = "SELECT @@IDENTITY AS New";
				cmdEmail = new SqlCommand(sqlID, conn);
				using (SqlDataReader dtOrder = cmdEmail.ExecuteReader())
				{
					dtOrder.Read();
					strEmailSystemNumber = dtOrder[0].ToString();
				}
			}
			return System.Convert.ToInt32(strEmailSystemNumber);
		}

		static public int SaveEmailSystem(string EmailBody, string emailType, int reportId, string connString)
		{
			using (SqlConnection conn = new SqlConnection(connString))
			{
				conn.Open();
			
				SqlCommand cmdEmail = new SqlCommand("spAddEmailSystem", conn);
				cmdEmail.CommandType = CommandType.StoredProcedure;
				cmdEmail.Parameters.Add("@EmailBody",EmailBody);
				cmdEmail.Parameters.Add("@EmailType",emailType);
				cmdEmail.Parameters.Add("@ReportId",reportId);
				cmdEmail.ExecuteNonQuery();

				//Getting the ID from that item just added
				string sqlID = "SELECT @@IDENTITY AS New";
				cmdEmail = new SqlCommand(sqlID, conn);
				SqlDataReader dtOrder = cmdEmail.ExecuteReader();
				dtOrder.Read();
				string strEmailSystemNumber = dtOrder[0].ToString();
			
				return System.Convert.ToInt32(strEmailSystemNumber);
			}			
		}

		static public long totalPoundsOffers(HttpContext webContext)
		{
			long total = 0;
			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{
				conn.Open();
				using (SqlDataReader dtrTotal = DBLibrary.GetDataReaderFromSelect(conn, "SELECT sum(OFFR_QTY*OFFR_SIZE) FROM bboffer where offr_port is null and OFFR_SIZE <> '38500' and OFFR_SIZE <> '44500'"))
				{
					if (dtrTotal.Read()) total = System.Convert.ToInt32(dtrTotal[0]);
				}				
			}
			return total;
		}

		static public long totalPoundsBids(HttpContext webContext)
		{
			long total = 0;
			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{
				conn.Open();
				using (SqlDataReader dtrTotal = DBLibrary.GetDataReaderFromSelect(conn, "SELECT sum(BID_QTY*BID_SIZE) FROM bbid where bid_port is null and bid_SIZE <> '38500' and bid_SIZE <> '44500'"))
				{
					if (dtrTotal.Read()) total = System.Convert.ToInt32(dtrTotal[0]);
				}			
				return total;
			}
		}

		static public string getUserInformationHTML(string userName, string password, string emailTo, HttpContext webContext)
		{
			string htmInfo = "";
			string htmlComment = "";
			string Preferences = "";
			string persID = "";
			Hashtable htParams = new Hashtable();
			string strSql = "SELECT PERS_ID, PERS_FRST_NAME, PERS_LAST_NAME, PERS_TITL, PERS_PHON, PERS_MAIL, PERS_COMP, PERS_PRMLY_INTRST, PERS_INTRST_SIZE, PERS_INTRST_QUALT, PERS_PORT, PERS_ZIP, PORT_CITY, PERS_CMNT, PERS_PREF, PERS_STAT, " +
					" CNAME=ISNULL(COMP_NAME,(SELECT C.COMP_NAME FROM COMPANY C WHERE COMP_ID=PERS_COMP)), ISNULL((SELECT TOP 1 CMNT_TEXT FROM COMMENT WHERE CMNT_PERS=PERS_ID AND CMNT_TEXT NOT LIKE 'User Login%' AND CMNT_EMAIL_SYSTEM_ID IS NULL ORDER BY CMNT_DATE DESC), '') AS NOTE " +
					"FROM PERSON, INTERNATIONAL_PORT " +
					"WHERE PERSON.PERS_PORT *= INTERNATIONAL_PORT.PORT_ID AND ((PERS_LOGN=@userName) OR (PERS_MAIL=@userName)) AND PERS_PSWD=@password";
			htParams.Add("@userName",userName);
			htParams.Add("@password",password);
			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{				
				conn.Open();			
				using (SqlDataReader dtrUser = DBLibrary.GetDataReaderFromSelect(conn, strSql,htParams))
				{
					if (dtrUser.Read())
					{
						if (HelperFunction.getSafeStringFromDB(dtrUser["PERS_PRMLY_INTRST"])=="1")
						{
							htmInfo += "<br><b>Market:</b> Domestic";
							if (HelperFunction.getSafeStringFromDB(dtrUser["PERS_ZIP"])!="")
								htmInfo += "&nbsp; <b>Zip Code:</b> " + HelperFunction.getSafeStringFromDB(dtrUser["PERS_ZIP"]);
						}
						else
						{
							htmInfo += "<br><b>Market:</b> International";
							if (HelperFunction.getSafeStringFromDB(dtrUser["PERS_PORT"])!="")
								htmInfo += "&nbsp; <b>Port:</b> " + HelperFunction.getSafeStringFromDB(dtrUser["PORT_CITY"]);
						}
						persID = getSafeStringFromDB(dtrUser["PERS_ID"]);
						htmInfo += "<br><b>User:</b> " + getSafeStringFromDB(dtrUser["PERS_FRST_NAME"]) + "&nbsp; " + getSafeStringFromDB(dtrUser["PERS_LAST_NAME"]) + "&nbsp;(ID:" + userName + " Password:" +  password + ")";
						htmInfo += "<br><b>Title</b>: " + getSafeStringFromDB(dtrUser["PERS_TITL"]);
						htmInfo += "<br><b>Company</b>: " + getSafeStringFromDB(dtrUser["CNAME"]);
						htmInfo += "<br><b>Phone</b> :" + getSafeStringFromDB(dtrUser["PERS_PHON"]);
						htmInfo += "<br><b>Email</b> :" + getSafeStringFromDB(dtrUser["PERS_MAIL"]);
						//				string rate = HelperFunction.getSafeStringFromDB(dtrUser["PERS_STAT"]);
						//				switch(rate)
						//				{
						//					case "1":
						//						rate = "Great";
						//						break;
						//					case "2":
						//						rate = "Fair";
						//						break;
						//					case "3":
						//						rate = "Unlikely";
						//						break;
						//					case "4":
						//						rate = "Message";
						//						break;
						//					default:
						//						rate = "-";
						//						break;
						//				}
						//				htmInfo += "<br><b>Lead Rate</b>: " + rate;
						htmInfo += "<br><b>IP Address:</b>: " + webContext.Request.UserHostAddress.ToString();
						htmInfo += "<br><br><b><u>Primarily Interested in</u></b>";
				
						string size = HelperFunction.getSafeStringFromDB(dtrUser["PERS_INTRST_SIZE"]);
						string sizeHTML="";
						if (size!="")
						{
							if (size[0].Equals('1')) sizeHTML+=", Rail Cars";
							if (size[1].Equals('1')) sizeHTML+=", Bulk Trucks";
							if (size[2].Equals('1')) sizeHTML+=@", Truckload Boxes\Bags";
							if (sizeHTML.Length>0) 
								sizeHTML = sizeHTML.Substring(2);
							else
								sizeHTML = " - ";
						}
						htmInfo += "<br><b>&nbsp;&nbsp;Size: " + sizeHTML + "</b>";

						string quality = HelperFunction.getSafeStringFromDB(dtrUser["PERS_INTRST_QUALT"]);
						string qualityHTML="";
						if (quality=="") quality="000";
						if ((quality[0].Equals('1')) || (HelperFunction.getSafeStringFromDB(dtrUser["PERS_PRMLY_INTRST"])=="2")) qualityHTML+=", Prime";
						if (quality[1].Equals('1')) qualityHTML+=", Good Offgrade";
						if (quality[2].Equals('1')) qualityHTML+=@", Regrind/Repro";
						if (qualityHTML.Length>0) 
							qualityHTML = qualityHTML.Substring(2);
						else
							qualityHTML = " - ";
						htmInfo += "<br><b>&nbsp;&nbsp;Quality: " + qualityHTML + "</b>";

						if (webContext.Session["Referrer"] != null)
						{
							htmInfo += "<br><b><font color=red>Referrer: "+ webContext.Session["Referrer"].ToString() +"</font></b>";
						}

						htmInfo += "<br><br><b>Resin Preference:</b>";
						htmlComment = HelperFunction.getSafeStringFromDB(dtrUser["NOTE"]);
						Preferences = HelperFunction.getSafeStringFromDB(dtrUser["PERS_PREF"]);
					}
				}

				if(Preferences == "")
				{
					Preferences = "0";
				}

				using (SqlDataReader dtrResin = DBLibrary.GetDataReaderFromSelect(conn, "SELECT CONT_LABL,CONT_ID FROM CONTRACT WHERE POWER(2,CONT_ID-1)&("+Preferences+")<>0 ORDER BY CONT_ORDR"))
				{
					while(dtrResin.Read())
					{
						htmInfo += "<br>" + dtrResin["CONT_LABL"].ToString();
					}
				}
			}
			
			htmInfo += "<br><br><b>Comments</b> :" + htmlComment;
			htmInfo += "<br>";
			//htmInfo += "<br>" + getEmailLink(emailTo, "Click on here to see full user details", "/Administrator/Contact_Details.aspx?ID=" + persID,30,webContext);
			//htmInfo += "<br>";

			return htmInfo;
		}

		static public string getEmailHeaderHTML()
		{
			string strDomainName = ConfigurationSettings.AppSettings["DomainName"].ToString();
			string htmInfo = "<HTML><HEAD></HEAD><BODY bgColor=#ffffff>" +
								"<table width='100%' border=0 cellspacing=0 cellpadding=0>" +
									"<tr>" +
										"<td width=50>" +
											"<img src=http://" + strDomainName + "/images/email/tpelogo.gif>" +
										"</td>" +
										"<td align='left' STYLE='FONT: 14pt ARIAL BLACK;COLOR=BLACK'>" +
											"The<font color=red>Plastic</font>Exchange<font color=red>.</font>com" +
										"</td>" +
									"</tr>" +
									"<tr>" +
										"<Td colspan=2>" +
											"<img src=http://" + strDomainName + "/images/email/bar.gif width=100% height=3>" +
										"</TD>" +
									"</tr>" +
								"</table>";
			return htmInfo;
		}

		static public string getEmailFooterHTML()
		{
			string strDomainName = ConfigurationSettings.AppSettings["DomainName"].ToString();
			string htmInfo = "<table width='100%' border=0>" +
								"<tr>" +
									"<Td>" +
										"<img src=http://" + strDomainName + "/images/email/bar.gif width=100% height=3>" +
									"</TD>" +
								"</tr>" +
							 "</table>" +
							 "<br>" +
							 "<center><font face=arial size=1 color=Black>710 North Dearborn, 3rd Floor</font>&nbsp;&nbsp;<font face=arial size=1 color=red>Chicago, IL 60610</font>&nbsp;&nbsp;<font face=arial size=1 color=Black><b>tel " + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + "</b></font>&nbsp;&nbsp;<font face=arial size=1 color=red>fax "+ ConfigurationSettings.AppSettings["FaxNumber"].ToString()+ "</font>" +
							 "</center></BODY></HTML>";
			return htmInfo;
		}
		
		static public string getEmailLink(string emailUser, string LinkLabel, string URLTarget, int ExpireDays, HttpContext webContext)
		{
			string link = "";
			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{
				conn.Open();
				string persPassword = "";
				Hashtable htParams = new Hashtable();
				htParams.Add("@emailUser",emailUser);
				using (SqlDataReader dtrPerson = DBLibrary.GetDataReaderFromSelect(conn, "SELECT PERS_PSWD FROM PERSON WHERE PERS_MAIL =@emailUser",htParams))
				{
					if (dtrPerson.Read())
					{
						persPassword = Crypto.Decrypt(getSafeStringFromDB(dtrPerson["PERS_PSWD"]));
						link = getEmailLink(emailUser, persPassword, LinkLabel, URLTarget, ExpireDays);
					}
				}				
			}
			return link;
		}

		static public string getEmailLink(string emailUser, string passwordUser, string LinkLabel, string URLTarget, int ExpireDays)
		{
			string strDomainName = ConfigurationSettings.AppSettings["DomainName"].ToString();
			string link = "";
			string persPassword = passwordUser;
			
			string encryptedParams = "";
			encryptedParams += "User=" + Crypto.Encrypt(emailUser);
			encryptedParams += "&Token=" + Crypto.Encrypt(persPassword);
			encryptedParams += "&Exp=" + Crypto.Encrypt(DateTime.Today.AddDays((double)ExpireDays).ToShortDateString());
			encryptedParams += "&LoginURL=" + Crypto.Encrypt(URLTarget);
            
			link = "<a href='http://" + strDomainName + "/common/FUNCLogin.aspx?" + encryptedParams + "'>" + LinkLabel + "</a>";
			return link;
		}

		static public string getLivePersonButton()
		{
			string HTML="";
			HTML += "<!-- BEGIN Timpani Button Code  -->";
			HTML += "	<a href='https://server.iad.liveperson.net/hc/92199096/?cmd=file&file=visitorWantsToChat&site=92199096&byhref=1'";
			HTML += "	target='chat92199096' onClick=javascript:window.open('https://server.iad.liveperson.net/hc/92199096/?cmd=file&file=visitorWantsToChat&site=92199096&referrer='+escape(document.location),'chat92199096','width=472,height=320');return false;>";
			HTML += "	<img src='/images2/questions_unpress.jpg'";
			HTML += "	name='hcIcon' width=165 height=60 border=0> </a>";
			HTML += "<!-- END Button Code -->";
			return HTML;
		}
		
		static public void SaveZipCode(string ZipCode, string userID, HttpContext webContext)
		{
			string currentZipCode = "";
			string strSQL="select pers_zip from person where pers_id = @userID";
			Hashtable htParams = new Hashtable();
			htParams.Add("@userID",userID);
			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{
				conn.Open();
				using (SqlDataReader rdrCompany = DBLibrary.GetDataReaderFromSelect(conn, strSQL, htParams))
				{
					rdrCompany.Read();
					currentZipCode = getSafeStringFromDB(rdrCompany["pers_zip"].ToString());
				}				
			}
			
			if (currentZipCode=="")
			{
				htParams.Add("@zipCode",ZipCode);
				DBLibrary.ExecuteSQLStatement(webContext.Application["DBconn"].ToString(), "UPDATE PERSON SET PERS_ZIP = @zipCode WHERE PERS_ID = @userID",htParams);
			}
		}

		static public void SavePort(string PortID, string userID, HttpContext webContext)
		{
			string currentPort = "";
			string strSQL="select pers_port from person where pers_id = @userID";
			Hashtable htParams = new Hashtable();
			htParams.Add("@userID",userID);
			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{
				conn.Open();
				using (SqlDataReader rdrCompany = DBLibrary.GetDataReaderFromSelect(conn, strSQL,htParams))
				{
					rdrCompany.Read();
					currentPort = getSafeStringFromDB(rdrCompany["pers_port"].ToString());
				}	
			}
			if (currentPort=="")
			{
				htParams.Add("@PortID",PortID);
				DBLibrary.ExecuteSQLStatement(webContext.Application["DBconn"].ToString(), "UPDATE PERSON SET PERS_PORT = @PortID WHERE PERS_ID = @userID",htParams);
			}
		}
		
		static public void IncludeNoteUser(string UserID, string Message, HttpContext webContext)
		{
			Hashtable htParams = new Hashtable();
			htParams.Add("@UserID",UserID);
			htParams.Add("@Message",Message);
			string SQL = "INSERT INTO COMMENT (CMNT_PERS,CMNT_TEXT,CMNT_DATE) VALUES (@UserID,@Message,getdate())";
			DBLibrary.ExecuteSQLStatement(webContext.Application["DBconn"].ToString(), SQL, htParams);
		}

		static public string RemoveQuotationMarks(string text)
		{
			return text.Replace("'","");
		}

		static public string getUserPreferencesHTML(string ID, HttpContext webContext)
		{
			string htmInfo = "";
			string htmlComment = "";
			string Preferences = "";
			Hashtable htParams = new Hashtable();
			htParams.Add("@ID",ID);
						
			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{
				conn.Open();
			
				using (SqlDataReader dtrUser = DBLibrary.GetDataReaderFromSelect(conn, "SELECT PERS_ID, PERS_FRST_NAME, PERS_LAST_NAME, PERS_TITL, PERS_PHON, PERS_MAIL, PERS_COMP, PERS_PRMLY_INTRST, PERS_INTRST_SIZE, PERS_INTRST_QUALT, PERS_PORT, PERS_ZIP, PORT_CITY, PERS_CMNT, PERS_PREF, PERS_STAT, " +
					" CNAME=ISNULL(COMP_NAME,(SELECT C.COMP_NAME FROM COMPANY C WHERE COMP_ID=PERS_COMP)), (SELECT TOP 1 CMNT_TEXT FROM COMMENT WHERE CMNT_PERS=PERS_ID) AS NOTE " +
					"FROM PERSON, INTERNATIONAL_PORT " +
					"WHERE PERSON.PERS_ID = @ID",htParams))
				{
					if (dtrUser.Read())
					{

						string size = HelperFunction.getSafeStringFromDB(dtrUser["PERS_INTRST_SIZE"]);
						string sizeHTML="";
						if (size!="")
						{
							if (size[0].Equals('1')) sizeHTML+=", Rail Cars";
							if (size[1].Equals('1')) sizeHTML+=", Bulk Trucks";
							if (size[2].Equals('1')) sizeHTML+=@", Truckload Boxes\Bags";
							if (sizeHTML.Length>0) 
								sizeHTML = sizeHTML.Substring(2);
							else
								sizeHTML = " - ";
						}
						htmInfo += "<br><b>&nbsp;&nbsp;Size: " + sizeHTML + "</b>";

						string quality = HelperFunction.getSafeStringFromDB(dtrUser["PERS_INTRST_QUALT"]);
						string qualityHTML="";
						if (quality=="") quality="000";
						if ((quality[0].Equals('1')) || (HelperFunction.getSafeStringFromDB(dtrUser["PERS_PRMLY_INTRST"])=="2")) qualityHTML+=", Prime";
						if (quality[1].Equals('1')) qualityHTML+=", Good Offgrade";
						if (quality[2].Equals('1')) qualityHTML+=@", Regrind/Repro";
						if (qualityHTML.Length>0) 
							qualityHTML = qualityHTML.Substring(2);
						else
							qualityHTML = " - ";
						htmInfo += "<br><b>&nbsp;&nbsp;Quality: " + qualityHTML + "</b>";

						if (webContext.Session["Referrer"] != null)
						{
							htmInfo += "<br><b><font color=red>Referrer: "+ webContext.Session["Referrer"].ToString() +"</font></b>";
						}

						htmInfo += "<br><br><b>Resin Preference:</b>";
						htmlComment = HelperFunction.getSafeStringFromDB(dtrUser["NOTE"]);
						Preferences = HelperFunction.getSafeStringFromDB(dtrUser["PERS_PREF"]);
					}
				}
				
				using (SqlDataReader dtrResin = DBLibrary.GetDataReaderFromSelect(conn, "SELECT CONT_LABL,CONT_ID FROM CONTRACT WHERE POWER(2,CONT_ID-1)&("+Preferences+")<>0 ORDER BY CONT_ORDR"))
				{
					while(dtrResin.Read())
					{
						htmInfo += "<br>" + dtrResin["CONT_LABL"].ToString();
					}
				}	
			}
			htmInfo += "<br>";
			//htmInfo += "<br>" + getEmailLink(emailTo, "Click on here to see full user details", "/Administrator/Contact_Details.aspx?ID=" + persID,30,webContext);
			//htmInfo += "<br>";

			return htmInfo;
		}

		static public string getGeneralUserInformationHTML(string ID, HttpContext webContext)
		{
			string htmInfo = "";
			string persID = "";
			Hashtable htParams = new Hashtable();
			htParams.Add("@ID",ID);
			
			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{
				conn.Open();
				
				using (SqlDataReader dtrUser = DBLibrary.GetDataReaderFromSelect(conn, "SELECT PERS_ID, PERS_PSWD, PERS_FRST_NAME, PERS_LAST_NAME, PERS_TITL, PERS_PHON, PERS_MAIL, PERS_COMP, PERS_PRMLY_INTRST, PERS_INTRST_SIZE, PERS_INTRST_QUALT, PERS_PORT, PERS_ZIP, PORT_CITY, PERS_CMNT, PERS_PREF, PERS_STAT, " +
					" CNAME=ISNULL(COMP_NAME,(SELECT C.COMP_NAME FROM COMPANY C WHERE COMP_ID=PERS_COMP)), (SELECT TOP 1 CMNT_TEXT FROM COMMENT WHERE CMNT_PERS=PERS_ID) AS NOTE " +
					"FROM PERSON, INTERNATIONAL_PORT " +
					"WHERE PERSON.PERS_ID = @ID", htParams))
				{
					if (dtrUser.Read())
					{
						if (HelperFunction.getSafeStringFromDB(dtrUser["PERS_PRMLY_INTRST"])=="1")
						{
							htmInfo += "<br><b>Market:</b> Domestic";
							if (HelperFunction.getSafeStringFromDB(dtrUser["PERS_ZIP"])!="")
								htmInfo += "&nbsp; <b>Zip Code:</b> " + HelperFunction.getSafeStringFromDB(dtrUser["PERS_ZIP"]);
						}
						else
						{
							htmInfo += "<br><b>Market:</b> International";
							if (HelperFunction.getSafeStringFromDB(dtrUser["PERS_PORT"])!="")
								htmInfo += "&nbsp; <b>Port:</b> " + HelperFunction.getSafeStringFromDB(dtrUser["PORT_CITY"]);
						}
						persID = getSafeStringFromDB(dtrUser["PERS_ID"]);
						htmInfo += "<br><b>User:</b> " + getSafeStringFromDB(dtrUser["PERS_FRST_NAME"]) + "&nbsp; " + getSafeStringFromDB(dtrUser["PERS_LAST_NAME"]) + "&nbsp;(ID:" + dtrUser["PERS_FRST_NAME"] + " Password:" +  Crypto.Decrypt(dtrUser["PERS_PSWD"].ToString()) + ")";
						htmInfo += "<br><b>Title</b>: " + getSafeStringFromDB(dtrUser["PERS_TITL"]);
						htmInfo += "<br><b>Company</b>: " + getSafeStringFromDB(dtrUser["CNAME"]);
						htmInfo += "<br><b>Phone</b> :" + getSafeStringFromDB(dtrUser["PERS_PHON"]);
						htmInfo += "<br><b>Email</b> :" + getSafeStringFromDB(dtrUser["PERS_MAIL"]);
						htmInfo += "<br><b>IP Address:</b>: " + webContext.Request.UserHostAddress.ToString();
					}
				}				
			}
			return htmInfo;
		}

		static public double getInterestBankParameter(HttpContext webContext)
		{
			double interest = 0.0;			
			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))			
			{
				conn.Open();

				using (SqlDataReader dtrUser = DBLibrary.GetDataReaderFromSelect(conn, "SELECT PARAM_VALUE FROM PARAMETERS WHERE PARAM_NAME = 'PERCENT_INTEREST_BANK'"))
				{
					if (dtrUser.Read())
					{
						interest = HelperFunction.getSafeDoubleFromDB(dtrUser["PARAM_VALUE"]);
					}
				}
			}
			return interest;
		}

		static public double getInterestLimitParameter(HttpContext webContext)
		{
			double interest = 0.0;
			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{
				conn.Open();
				using (SqlDataReader dtrUser = DBLibrary.GetDataReaderFromSelect(conn, "SELECT PARAM_VALUE FROM PARAMETERS WHERE PARAM_NAME = 'PERCENT_LIMIT_INTEREST'"))
				{
					if (dtrUser.Read())
					{
						interest = HelperFunction.getSafeDoubleFromDB(dtrUser["PARAM_VALUE"]);
					}
				}
			}
			return interest;
		}


		static public void fillDates(int StartMonth, int StartYear, DropDownList ddlList)
		{
			DateTime today = DateTime.Now;
			DateTime startDate = Convert.ToDateTime(StartMonth.ToString() + "/01/" + StartYear.ToString());
			string[] all_months = new string[12]{"January","February","March","April","May","June","July","August","September","October","November","December"};
			
			while(startDate <= today)
			{
				ListItem item = new ListItem(all_months[startDate.Month-1] + ", " + startDate.Year, startDate.Year.ToString() + startDate.Month.ToString().PadLeft(2,'0'));
				ddlList.Items.Add(item);
				startDate = startDate.AddMonths(1);
			}

			ddlList.SelectedItem.Selected = false;
			ddlList.Items[ddlList.Items.Count - 1].Selected = true;
		}

		public static decimal returnTaxByPlace(string plac_id, HttpContext webContext)
		{
			decimal tax = 0;

			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{
				
				Hashtable param = new Hashtable();
				conn.Open();
				param.Add("@plac_id",Int32.Parse(plac_id));
				string strProvince = "";
				using (SqlDataReader dtrLocality = DBLibrary.GetDataReaderFromSelect(conn, "Select * FROM Place WHERE PLAC_ID=@plac_id",param))
				{			
					if(dtrLocality.Read())
					{
						long local_id = HelperFunction.getSafeLongFromDB(dtrLocality["PLAC_LOCL"]);
			
						if(local_id == 0)
						{
							if((dtrLocality["PLAC_CTRY"] != null) && (dtrLocality["PLAC_CTRY"].ToString() == "Canada"))
							{
								if(dtrLocality["PLAC_STAT"] != null)
								{
									strProvince = HelperFunction.getSafeStringFromDB(dtrLocality["PLAC_STAT"]);

								}
							}
						}
						else
						{
							dtrLocality.Close();
							param.Clear();
							param.Add("@local_id",local_id);
							using (SqlDataReader dtrProvince = DBLibrary.GetDataReaderFromSelect(conn,"SELECT LOCL_STAT FROM LOCALITY WHERE LOCL_CTRY='CA' AND LOCL_ID=@local_id",param))
							{
								if(dtrProvince.Read())
								{
									strProvince = HelperFunction.getSafeStringFromDB(dtrProvince["LOCL_STAT"]);
								}
//								dtrProvince.Close();
							}		

						}
						if(strProvince != "")
						{
							decimal hst_tax = 0;
							decimal gst_tax = 0;
							decimal qst_tax = 0;

							using (SqlDataReader dtrTaxes = DBLibrary.GetDataReaderFromSelect(conn, "select " + 
								"(select param_value from parameters where param_name='GST_TAX') AS GST," + 
								"(select param_value from parameters where param_name='QST_TAX') AS QST," + 
								"(select param_value from parameters where param_name='HST_TAX') AS  HST"))
							{
								if(dtrTaxes.Read())
								{
									gst_tax = HelperFunction.getSafeDecimalFromDB(dtrTaxes["GST"]);
									hst_tax = HelperFunction.getSafeDecimalFromDB(dtrTaxes["HST"]);
									qst_tax = HelperFunction.getSafeDecimalFromDB(dtrTaxes["QST"]);
								}
							}

							if((strProvince == "NB") || (strProvince == "NS") || (strProvince == "NF") )
							{
								tax = hst_tax;
							}
							else if(strProvince == "QU")
							{
								tax = qst_tax;						
							}
							else
							{
								tax = gst_tax;
							}
						}
					}
				}
			}
			return tax;
		}


		public static decimal returnTaxByName(string taxname, HttpContext webContext)
		{
			decimal tax = 0;

			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))
			{
				
				conn.Open();
				
				using (SqlDataReader dtrTaxes = DBLibrary.GetDataReaderFromSelect(conn, "select PARAM_VALUE FROM PARAMETERS WHERE PARAM_NAME='" + taxname + "_TAX'"))
				{
					if(dtrTaxes.Read())
					{
						tax = HelperFunction.getSafeDecimalFromDB(dtrTaxes["PARAM_VALUE"]);
					}
				}

			}
			return tax;
		}

		public static string returnTaxName(string plac_id, HttpContext webContext)
		{
			string tax = "";
			string strProvince = "";
				
			Hashtable htParams = new Hashtable();
			htParams.Add("@plac_id",plac_id);
			
			using (SqlConnection conn = new SqlConnection(webContext.Application["DBconn"].ToString()))			
			{
				conn.Open();
				string SQL = "Select * FROM Place WHERE PLAC_ID=@plac_id";
				using (SqlDataReader dtrLocality = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn,SQL,htParams))
				{
					if(dtrLocality.Read())
					{
						long local_id = HelperFunction.getSafeLongFromDB(dtrLocality["PLAC_LOCL"]);
				
						if(local_id == 0)
						{
							if((dtrLocality["PLAC_CTRY"] != null) && (dtrLocality["PLAC_CTRY"].ToString() == "Canada"))
							{
								if(dtrLocality["PLAC_STAT"] != null)
								{
									strProvince = HelperFunction.getSafeStringFromDB(dtrLocality["PLAC_STAT"]);

								}
							}
							dtrLocality.Close();
						}
						else
						{
							if (!dtrLocality.IsClosed)
								dtrLocality.Close();

							string SQL2 = "SELECT LOCL_STAT FROM LOCALITY WHERE LOCL_CTRY='CA' AND LOCL_ID=" + local_id;
							using (SqlDataReader dtrProvince = DBLibrary.GetDataReaderFromSelect(conn,SQL2))
							{
								if(dtrProvince.Read())
								{
									strProvince = HelperFunction.getSafeStringFromDB(dtrProvince["LOCL_STAT"]);
								}
							}							
						}
						if(strProvince != "")
						{
							if((strProvince == "NB") || (strProvince == "NS") || (strProvince == "NF") 
								|| (strProvince == "New Brunswick") || (strProvince == "Nova Scotia") || (strProvince == "Newfoundland"))
							{
								tax = "HST";
							}
							else if((strProvince == "QU") || (strProvince == "Quebec"))
							{
								tax = "QST";						
							}
							else
							{
								tax = "GST";
							}
						}
					}				
					return tax;	
				}
			}
		}

		public static bool IsEmail(string inputEmail)
		{
			//inputEmail  = NulltoString(inputEmail);
			string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
				@"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + 
				@".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
			Regex re = new Regex(strRegex);
			if (re.IsMatch(inputEmail))
			{
//				try
//				{
//					//DNS checking
//					string[] host = (inputEmail.Split('@'));
//					string hostname = host[1];
//					IPHostEntry IPhst = Dns.Resolve("www." + hostname);
//				}
//				catch(Exception ex)
//				{
//					return (false);
//				}

				return (true);
			}
			else
				return (false);

			
		}
		
		public static string Highlight(string Search_Str, string InputTxt) 
		{ 
			// Setup the regular expression and add the Or operator.
			Regex RegExp = new Regex(Search_Str.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
			//Regex RegExp = new Regex(Search_Str.Trim(), RegexOptions.IgnoreCase);
			try 
			{
				// Highlight keywords by calling the delegate each time a keyword is found.
				return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords)); 
			}
			finally
			{
				RegExp = null;
			}
		}

		public static string ReplaceKeyWords(Match m) 
		{
			return "<span class='SearchHighlight'>" + m.Value + "</span>"; 
		}

        public static string ConvertCarriageReturnToBR(string s)
        {
            return s.Replace("\r\n", "<br />");
        }
	}

}
