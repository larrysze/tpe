<%@ Page language="c#" Codebehind="SpotFloorSummaryPublishing.aspx.cs" AutoEventWireup="True" Inherits="localhost.include.SpotFloorSummaryPublishing" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>SpotFloorSummaryPublishing</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body bottomMargin="0" bgColor="black" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 8px; WIDTH: 176px; POSITION: absolute; TOP: 8px; HEIGHT: 164px"
				cellSpacing="0" cellPadding="0" width="176" align="center" border="0">
				<TR>
					<TD vAlign="middle" align="center">
						<asp:Image id="Image1" runat="server" ImageUrl="/Pics/Logo_Black.gif"></asp:Image></TD>
					<TD style="HEIGHT: 19px" vAlign="middle" noWrap align="center"></TD>
				</TR>
				<TR>
					<TD bgColor="#f1f1f1" height="4" style="WIDTH: 384px" align="center"><FONT color="#cc6600" size="3"><B>Current 
								Spot Offers&nbsp; &nbsp;
								<asp:HyperLink id="hplTotalLbs" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red" onprerender="hplTotalLbs_PreRender">0,00 Lbs</asp:HyperLink></B></FONT></TD>
					<TD vAlign="middle" noWrap align="center" height="159" rowSpan="3">
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" align="center" bgColor="#f1f1f1" rowSpan="2" style="WIDTH: 384px">
						<asp:datagrid id="dgSummaryGrid" runat="server" BorderWidth="0px" BackColor="Transparent" BorderStyle="None"
							GridLines="Horizontal" CellPadding="3" AutoGenerateColumns="False" HorizontalAlign="Center"
							Width="288px">
							<AlternatingItemStyle Font-Size="X-Small" BorderStyle="Solid" BackColor="#F1F1F1"></AlternatingItemStyle>
							<ItemStyle Font-Size="X-Small" BackColor="#F1F1F1"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Font-Bold="True" BackColor="#F1F1F1"></HeaderStyle>
							<Columns>
								<asp:HyperLinkColumn DataNavigateUrlField="offr_grade" DataNavigateUrlFormatString="SpotFloorSummaryPublishing.aspx"
									DataTextField="GRADE" HeaderText="Resin">
									<ItemStyle Wrap="False"></ItemStyle>
								</asp:HyperLinkColumn>
								<asp:BoundColumn DataField="varsize" HeaderText="Total lbs" DataFormatString="{0:#,##}">
									<ItemStyle Wrap="False"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Min" HeaderText="Low" DataFormatString="{0:c}">
									<ItemStyle Wrap="False"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Max" HeaderText="High" DataFormatString="{0:c}">
									<ItemStyle Wrap="False"></ItemStyle>
								</asp:BoundColumn>
								<asp:TemplateColumn Visible="False" HeaderText="Price Range">
									<ItemStyle Wrap="False"></ItemStyle>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></TD>
				</TR>
				<TR>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
