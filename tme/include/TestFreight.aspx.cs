using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace localhost.include
{
	/// <summary>
	/// Summary description for TestFreight.
	/// </summary>
	public class TestFreight : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			HelperFunctionDistances helper = new HelperFunctionDistances();

			// Put user code to initialize the page here
			Response.Write("Chicago IL - Sta Monica CA : TruckLoad");
			decimal freight = 0;
			Response.Write(DateTime.Now.ToString() + "<BR>");
			if (helper.calculateFreightByZipCode("60610","90411", ref freight, TruckType.TruckLoad, Application["DBconn"].ToString()))
				Response.Write("Freight: " + freight.ToString());
			else
				Response.Write("Freight not available");
			Response.Write("<BR>");
			Response.Write(DateTime.Now.ToString() + "<BR>");

			freight = 0;
			string placeFROM = "";
			string placeTO = "";
			Response.Write(DateTime.Now.ToString() + "<BR>");
			if (helper.calculateFreightByZipCode("60610","90411", ref placeFROM, ref placeTO, ref freight, TruckType.TruckLoad, Application["DBconn"].ToString()))
				Response.Write("Freight: " + freight.ToString() + " Place From:" + placeFROM + " Place To:" + placeTO);
			else
				Response.Write("Freight not available");
			Response.Write("<BR>");
			Response.Write(DateTime.Now.ToString() + "<BR>");
			
			freight = 0;
			Response.Write(DateTime.Now.ToString() + "<BR>");
			if (helper.calculateFreightByCity("Chicago","IL","Santa Monica","CA", ref freight, TruckType.TruckLoad, Application["DBconn"].ToString()))
				Response.Write("Freight: " + freight.ToString());
			else
				Response.Write("Freight not available");
			Response.Write("<BR>");
			Response.Write(DateTime.Now.ToString() + "<BR>");
			
			
			
			Response.Write("<BR><BR>");
			Response.Write("Chicago IL - Cincinatti OH : BulkTruck");
			freight = 0;
			Response.Write(DateTime.Now.ToString() + "<BR>");
			if (helper.calculateFreightByZipCode("60610","45271", ref freight, TruckType.BulkTruck, Application["DBconn"].ToString()))
				Response.Write("Freight: " + freight.ToString());
			else
				Response.Write("Freight not available");
			Response.Write("<BR>");
			Response.Write(DateTime.Now.ToString() + "<BR>");

			freight = 0;
			placeFROM = "";
			placeTO = "";
			Response.Write(DateTime.Now.ToString() + "<BR>");
			if (helper.calculateFreightByZipCode("60610","45271", ref placeFROM, ref placeTO, ref freight, TruckType.BulkTruck, Application["DBconn"].ToString()))
				Response.Write("Freight: " + freight.ToString() + " Place From:" + placeFROM + " Place To:" + placeTO);
			else
				Response.Write("Freight not available");
			Response.Write("<BR>");
			Response.Write(DateTime.Now.ToString() + "<BR>");
			
			freight = 0;
			Response.Write(DateTime.Now.ToString() + "<BR>");
			if (helper.calculateFreightByCity("Chicago","IL","Cincinnati","OH", ref freight, TruckType.BulkTruck, Application["DBconn"].ToString()))
				Response.Write("Freight: " + freight.ToString());
			else
				Response.Write("Freight not available");
			Response.Write("<BR>");
			Response.Write(DateTime.Now.ToString() + "<BR>");
			Response.Write("<BR><BR>");


			
			Response.Write("<BR><BR>");
			Response.Write("International Freight - Houston, US to Los Angeles, US");
			freight = 0;
			Response.Write(DateTime.Now.ToString() + "<BR>");
			if (helper.calculateInternationalFreight(1,1, ref freight, Application["DBconn"].ToString()))
				Response.Write("Freight: " + freight.ToString());
			else
				Response.Write("Freight not available");
			Response.Write("<BR>");
			Response.Write(DateTime.Now.ToString() + "<BR>");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
