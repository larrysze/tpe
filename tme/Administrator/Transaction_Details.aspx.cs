using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using TPE.Utility;
using System.Reflection;
using System.Text;
using System.IO;
using System.Configuration;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Edit_Transaction.
	/// </summary>
	public partial class Transaction_Details : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.ListBox lbComment;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.Label Label24;
		protected System.Web.UI.WebControls.Label Label25;
		protected System.Web.UI.WebControls.Label Label26;
		protected System.Web.UI.WebControls.Label Label27;
		protected System.Web.UI.WebControls.Label Label28;
		protected System.Web.UI.WebControls.Label Label29;
		protected System.Web.UI.WebControls.Label Label30;
		protected System.Web.UI.WebControls.Label Label31;
		protected System.Web.UI.WebControls.Label Label33;
		protected System.Web.UI.WebControls.LinkButton LinkButton1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.PlaceHolder phActionButton;

		public void Page_Load(object sender, EventArgs e)
		{
            Master.Width = "1020px";
			// Kick user out if they aren't allowed
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "L"))
			{
				Response.Redirect("/default.aspx");
			}
			
			HelperFunction.InsertConfirmationCode(this);
			HelperFunction.InsertControlConfirmationCode(btnDeleteOrder,"Are you sure you want to delete this Order and all Shipments attached to it?");
			HelperFunction.InsertControlConfirmationCode(btnDeleteShipment,"Are you sure you want to delete this Shipment?");

			if (!IsPostBack)
			{
				//Getting the ShipmentID
				if (Request.QueryString["Id"].ToString() != null)
				{
					string[] Shipment = Request.QueryString["Id"].ToString().Split('-');
					lblShipmentID.Text = HelperFunction.getShipmentID((string)Shipment[0],(string)Shipment[1],this.Context);
					lblOrderID.Text = Shipment[0].ToString();
					lblOrderNumber.Text = "Order # " + Request.QueryString["Id"].ToString();

					SetUpOrder(lblOrderID.Text);
					SetUpShipment(lblShipmentID.Text);
					SetUpComments();
					SetUpDocuments();

					OrderMode(false);
					ShipmentMode(false);
				}
				saveFields();
			}
		}

		private void saveFields()
		{
			string[] fields = new string[8];
			fields[0] = txtBuyPrice.Text;
			fields[1] = txtSellPrice.Text;
			fields[2] = txtComission.Text;
			fields[3] = txtTermsFrom.Text;
			fields[4] = txtTermsSoldTo.Text;
			fields[5] = txtWeight.Text;
			//fields[6] = txtFreight.Text;
			fields[7] = ddlStatus.SelectedValue;
			ViewState["fields"] = fields;
		}

		
		private void SetUpOrder(string orderID)
		{			
			//Details like Melt, Dens and Adds
            DataTable dtOrderDetails = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), "Select ISNULL(PLAC_LABL, '-') AS WAREHOUSE, ORDR_ISSPOT, ORDR_MELT, ORDR_DENS, ORDR_ADDS, ORDR_PROD_SPOT, ORDR_DATE,ORDR_PRIME FROM ORDERS, PLACE, SHIPMENT WHERE ORDR_ID='" + orderID + "' AND SHIPMENT_ORDR_ID=ORDR_ID AND PLAC_ID=SHIPMENT_FROM");
			DataRow drOrderDetails = dtOrderDetails.Rows[0];

			if (drOrderDetails["ORDR_ISSPOT"].ToString()=="True" )
			{
				if (Convert.ToBoolean(drOrderDetails["ORDR_PRIME"]))
					cbPrime.Checked= true;
				else
					cbPrime.Checked=false;
				txtMelt.Text = drOrderDetails["ORDR_MELT"].ToString();
				txtDensity.Text = drOrderDetails["ORDR_DENS"].ToString();
				txtAdds.Text = drOrderDetails["ORDR_ADDS"].ToString();
				txtProductDesc.Text = drOrderDetails["ORDR_PROD_SPOT"].ToString();
				txtTransactionDate.Text = ((DateTime)Convert.ToDateTime(drOrderDetails["ORDR_DATE"].ToString())).ToShortDateString();
                txtWarehouseName.Text = drOrderDetails["WAREHOUSE"].ToString();

				DefineOrderTracks();
			}			
		}

		private void SetUpShipment(string shipmentID)
		{
			//Populate screen using valus from DB
						
			string userIDSoldTo = "";
			string userIDFrom = "";
			string placeIDSoldTo = "";
			string placeIDFrom = "";

			double gst_hst_tax = 0.0;
			string broker = "";

			using (SqlConnection connection = new SqlConnection(Context.Application["DBconn"].ToString()))
			{
				connection.Open();

				// query returns all companies except TPE.  We should not be listed on the screen
				ddlCompanySoldTo.Items.Clear();
				using (SqlDataReader dtrCompanies = DBLibrary.GetDataReaderFromSelect(connection, "Select COMP_NAME,COMP_ID FROM COMPANY ORDER BY COMP_NAME ASC"))
				{
					while (dtrCompanies.Read())
					{
						if (dtrCompanies["COMP_NAME"].ToString().Length > 25)
							ddlCompanySoldTo.Items.Add (new ListItem(dtrCompanies["COMP_NAME"].ToString().Substring(0,25)+"...",dtrCompanies["COMP_ID"].ToString()));
						else 
							ddlCompanySoldTo.Items.Add (new ListItem(dtrCompanies["COMP_NAME"].ToString(),dtrCompanies["COMP_ID"].ToString()));
					}
				}
				//Sold To
				using (SqlDataReader drSoldTo = DBLibrary.GetDataReaderFromSelect(connection, "Select SHIPMENT_BUYR,(Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME FROM PERSON WHERE PERS_ID =SHIPMENT_BUYR) AS PERS_NAME,(Select PERS_ID FROM PERSON WHERE PERS_ID =SHIPMENT_BUYR) AS PERS_ID,(Select COMP_NAME FROM COMPANY WHERE COMP_ID =(Select PERS_COMP FROM PERSON WHERE PERS_ID =SHIPMENT_BUYR)) AS COMP_NAME,(Select COMP_ID FROM COMPANY WHERE COMP_ID =(Select PERS_COMP FROM PERSON WHERE PERS_ID =SHIPMENT_BUYR)) AS COMP_ID,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=SHIPMENT_TO)) AS PLAC_NAME, SHIPMENT_TO AS PLAC_ID, SHIPMENT_TAX AS TAX FROM SHIPMENT WHERE SHIPMENT_ID=" + shipmentID))
				{
					drSoldTo.Read();
					ddlCompanySoldTo.SelectedItem.Selected = false;
					if (drSoldTo["COMP_ID"].ToString().Trim()!="")
					{
						ddlCompanySoldTo.SelectedIndex = ddlCompanySoldTo.Items.IndexOf(ddlCompanySoldTo.Items.FindByValue(drSoldTo["COMP_ID"].ToString()));
					}
					userIDSoldTo = drSoldTo["PERS_ID"].ToString();
					if (drSoldTo["PLAC_NAME"]!= DBNull.Value)
					{
						placeIDSoldTo = drSoldTo["PLAC_ID"].ToString();
					}

					gst_hst_tax = HelperFunction.getSafeDoubleFromDB(drSoldTo["TAX"]);
				}
				//Selection the right itens
				RebindCompanySoldTo(true);
                ddlUserSoldTo.SelectedIndex = ddlUserSoldTo.Items.IndexOf(ddlUserSoldTo.Items.FindByValue(userIDSoldTo));

                lblBuyerEditUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=" + ddlUserSoldTo.SelectedValue + "&CompanyID=" + ddlCompanySoldTo.SelectedValue + "\">Edit</a>";
                lblBuyerAddUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=0&CompanyID=0" + ddlCompanySoldTo.SelectedValue + "\">Add new</a>";

				if (placeIDSoldTo!="") ddlDeliveryTo.SelectedIndex = ddlDeliveryTo.Items.IndexOf(ddlDeliveryTo.Items.FindByValue(placeIDSoldTo));
			
				// this way the currently selected ones are listed automatically on the top
				ddlCompanyFrom.Items.Clear();
				using (SqlDataReader dtrBuyerCompanies = DBLibrary.GetDataReaderFromSelect(connection, "Select COMP_NAME,COMP_ID FROM COMPANY ORDER BY COMP_NAME ASC"))
				{
					while (dtrBuyerCompanies.Read())
					{
						if (dtrBuyerCompanies["COMP_NAME"].ToString().Length > 25)
							ddlCompanyFrom.Items.Add(new ListItem(dtrBuyerCompanies["COMP_NAME"].ToString().Substring(0,25)+"...",dtrBuyerCompanies["COMP_ID"].ToString()));
						else 
							ddlCompanyFrom.Items.Add(new ListItem(dtrBuyerCompanies["COMP_NAME"].ToString(),dtrBuyerCompanies["COMP_ID"].ToString()));
					}
				}
				//Purchase From
				using (SqlDataReader dtrBuyer = DBLibrary.GetDataReaderFromSelect(connection, "Select SHIPMENT_SELR,(Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME FROM PERSON WHERE PERS_ID =SHIPMENT_SELR) AS PERS_NAME,(Select PERS_ID FROM PERSON WHERE PERS_ID =SHIPMENT_SELR) AS PERS_ID,(Select COMP_NAME FROM COMPANY WHERE COMP_ID =(Select PERS_COMP FROM PERSON WHERE PERS_ID =SHIPMENT_SELR)) AS COMP_NAME,(Select COMP_ID FROM COMPANY WHERE COMP_ID =(Select PERS_COMP FROM PERSON WHERE PERS_ID =SHIPMENT_SELR)) AS COMP_ID,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=SHIPMENT_FROM)) AS PLAC_NAME, SHIPMENT_FROM AS PLAC_ID FROM SHIPMENT WHERE SHIPMENT_ID=" + shipmentID))
				{
					dtrBuyer.Read();
					if (dtrBuyer["COMP_ID"].ToString().Trim()!="")
					{
						ddlCompanyFrom.SelectedIndex = ddlCompanyFrom.Items.IndexOf(ddlCompanyFrom.Items.FindByValue(dtrBuyer["COMP_ID"].ToString()));
					}
					userIDFrom = dtrBuyer["PERS_ID"].ToString();
					placeIDFrom = dtrBuyer["PLAC_ID"].ToString();
				}
				//Selecting the right item
				RebindCompanyFrom(true);
				ddlUserFrom.SelectedIndex = ddlUserFrom.Items.IndexOf(ddlUserFrom.Items.FindByValue(userIDFrom));
                
                lblSellerEditUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=" + ddlUserFrom.SelectedValue + "&CompanyID=" + ddlCompanyFrom.SelectedValue + "\">Edit</a>";
                lblSellerAddUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=0&CompanyID=0" + ddlCompanyFrom.SelectedValue + "\">Add new</a>";

				if (placeIDFrom!="") ddlPickupFrom.SelectedIndex = ddlPickupFrom.Items.IndexOf(ddlPickupFrom.Items.FindByValue(placeIDFrom));

				//Terms
				using (SqlDataReader drShipTerms = DBLibrary.GetDataReaderFromSelect(connection, "Select SHIPMENT_FOB FROM SHIPMENT WHERE SHIPMENT_ID=" + shipmentID))
				{
					drShipTerms.Read();
					ddlTerm.Items.Clear();
					ddlTerm.Items.Add(new ListItem ("FOB/Delivered","FOB/Delivered"));
					ddlTerm.Items.Add(new ListItem ("FOB/Shipping","FOB/Shipping"));
					ddlTerm.Items.Add(new ListItem ("Delivered by TPE","Delivered Tpe"));
                    ddlTerm.SelectedIndex = ddlTerm.Items.IndexOf(ddlTerm.Items.FindByValue((string)drShipTerms["SHIPMENT_FOB"]));
				}
			
				StringBuilder sbSql = new StringBuilder();
				sbSql.Append("select CAST(ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR) AS ID,");
				sbSql.Append("(SELECT PERS_FRST_NAME FROM PERSON WHERE PERS_ID=SHIPMENT_BROKER_ID) AS BROKER,");
				sbSql.Append("SHIPMENT_BROKER_ID, SHIPMENT_COMMENT,SHIPMENT_DATE_TAKE,SHIPMENT_DATE_DELIVERED,SHIPMENT_SHIP_STATUS,");
				sbSql.Append("SHIPMENT_BUYER_TERMS,SHIPMENT_SELLER_TERMS, SHIPMENT_WEIGHT,(SHIPMENT_SIZE*SHIPMENT_QTY) AS SHIPMENT_WEIGHT_ESTIMATE,");
				sbSql.Append("SHIPMENT_PRCE,SHIPMENT_MARK=(SHIPMENT_BUYR_PRCE-SHIPMENT_PRCE-SHIPMENT_COMM)*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY)-SHIPMENT_SHIP_PRCE,");
                sbSql.Append("CONVERT(varchar(14),ORDR_DATE,101) AS ORDR_Date, SPRICE=SHIPMENT_BUYR_PRCE,SHIPMENT_SHIP_PRCE,SHIPMENT_SHIP_PRICE_STATUS,SHIPMENT_PO_NUM, SHIPMENT_SIZE,");
				sbSql.Append("SHIPMENT_QTY, SHIPMENT_COMM from ORDERS , SHIPMENT where  SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ID = "+ shipmentID);
				//"select CAST(ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR) AS ID,(SELECT PERS_FRST_NAME FROM PERSON WHERE PERS_ID=SHIPMENT_BROKER_ID) AS BROKER,SHIPMENT_BROKER_ID,SHIPMENT_COMMENT,SHIPMENT_DATE_TAKE,SHIPMENT_DATE_DELIVERED,SHIPMENT_SHIP_STATUS,SHIPMENT_BUYER_TERMS,SHIPMENT_SELLER_TERMS, SHIPMENT_WEIGHT,(SHIPMENT_SIZE*SHIPMENT_QTY) AS SHIPMENT_WEIGHT_ESTIMATE,SHIPMENT_PRCE,SHIPMENT_MARK=(SHIPMENT_BUYR_PRCE-SHIPMENT_PRCE-SHIPMENT_COMM)*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY)-SHIPMENT_SHIP_PRCE, CONVERT(varchar(14),ORDR_DATE,101) AS ORDR_Date, SPRICE=SHIPMENT_BUYR_PRCE,SHIPMENT_SHIP_PRCE, SHIPMENT_PO_NUM, SHIPMENT_SIZE, SHIPMENT_QTY, SHIPMENT_COMM from ORDERS , SHIPMENT where  SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ID =" + shipmentID
					
				// populating other information
				using (SqlDataReader dtrTransaction = DBLibrary.GetDataReaderFromSelect(connection, sbSql.ToString()))
				{
					dtrTransaction.Read();
					txtPONumber.Text = dtrTransaction["SHIPMENT_PO_NUM"].ToString();
					txtTermsFrom.Text = dtrTransaction["SHIPMENT_SELLER_TERMS"].ToString();
					txtTermsSoldTo.Text=dtrTransaction["SHIPMENT_BUYER_TERMS"].ToString();
					txtBuyPrice.Text = String.Format("{0:c}", dtrTransaction["SHIPMENT_PRCE"].ToString());
					txtSellPrice.Text = String.Format("{0:c}", dtrTransaction["SPRICE"].ToString());
					txtQuatity.Text = dtrTransaction["SHIPMENT_QTY"].ToString();
					txtComission.Text = dtrTransaction["SHIPMENT_COMM"].ToString();
					txtWeight.Text = dtrTransaction["SHIPMENT_WEIGHT"].ToString();
					this.ddlFreightStatus.SelectedValue = dtrTransaction["SHIPMENT_SHIP_PRICE_STATUS"].ToString();
					if (ddlFreightStatus.SelectedValue == "E")
					{
						txtFreight.ForeColor = Color.OrangeRed;
					}
					else if (ddlFreightStatus.SelectedValue == "I")
					{
						txtFreight.ForeColor = Color.Green;
					}
					
					txtFreight.Text = String.Format("{0:c}", dtrTransaction["SHIPMENT_SHIP_PRCE"].ToString());
					double weight= 0;
					if ((txtWeight.Text=="") || (txtWeight.Text=="0")) 
						weight = Convert.ToDouble(dtrTransaction["SHIPMENT_WEIGHT_ESTIMATE"].ToString());
					else
						weight = Convert.ToDouble(txtWeight.Text);
					lblProfit.Text = String.Format("{0:c}", ((Convert.ToDouble(txtSellPrice.Text)-Convert.ToDouble(txtBuyPrice.Text)) * weight) - Convert.ToDouble(txtFreight.Text));

					double OutstandingFrom =  Math.Round(Convert.ToDouble(weight * Convert.ToDouble(txtBuyPrice.Text)) - Convert.ToDouble(TotalPaid(ddlUserFrom.SelectedItem.Value,lblShipmentID.Text)), 3);
			
					double OutstandingSoldTo = Convert.ToDouble(weight * Convert.ToDouble(txtSellPrice.Text)) - Convert.ToDouble(TotalPaid(ddlUserSoldTo.SelectedItem.Value,lblShipmentID.Text));

					ViewState["sellprice_without_tax"] = OutstandingSoldTo;
					ViewState["buyprice_without_tax"] = OutstandingFrom;


					//Check if Canadian taxes (GST, HST) apply to the shipment
					if(gst_hst_tax != 0)
					{
						OutstandingSoldTo = ( (100 + gst_hst_tax) / 100 )* OutstandingSoldTo;
						OutstandingFrom = ( (100 + gst_hst_tax) / 100 )* OutstandingFrom;
					}
					lblTotalOustandingFrom.Text = String.Format("{0:c}",Convert.ToString(OutstandingFrom)).ToString();

					ViewState["Canadian_tax"] = gst_hst_tax;

					lblTotalOutstandingsSoldTo.Text = String.Format("{0:c}",Convert.ToString(OutstandingSoldTo)).ToString();
			
					//	SHIPMENT_BUYR
			
					if (dtrTransaction["SHIPMENT_DATE_DELIVERED"].ToString()!="")
						txtDeliveryDate.Text = ((DateTime)Convert.ToDateTime(dtrTransaction["SHIPMENT_DATE_DELIVERED"].ToString())).ToShortDateString();
					if (dtrTransaction["SHIPMENT_DATE_TAKE"].ToString()!="")
						txtShippingDate.Text = ((DateTime)Convert.ToDateTime(dtrTransaction["SHIPMENT_DATE_TAKE"].ToString())).ToShortDateString();
			
					if (dtrTransaction["SHIPMENT_DATE_TAKE"].ToString() != "")
					{
						lblDueFrom.Text = ((DateTime)System.Convert.ToDateTime(dtrTransaction["SHIPMENT_DATE_TAKE"].ToString())).AddDays(System.Convert.ToDouble(dtrTransaction["SHIPMENT_SELLER_TERMS"])).ToShortDateString();
						lblDueSoldTo.Text = ((DateTime)System.Convert.ToDateTime(dtrTransaction["SHIPMENT_DATE_TAKE"].ToString())).AddDays(System.Convert.ToDouble(dtrTransaction["SHIPMENT_BUYER_TERMS"])).ToShortDateString();
					}

					if ((dtrTransaction["SHIPMENT_SIZE"].ToString().Equals("190000")) && (dtrTransaction["SHIPMENT_COMMENT"] != null))
						lblRailCarNumber.Text = "Rail Car #: <a href=\"https://www.steelroads.com/servlet/SAServlet?CONTROLLER=ETScriptedAccessCtlr&ScriptedAccessType=Dynamic&ACTION=&LANGUAGE=en&ImportList&EquipmentList="+ dtrTransaction["SHIPMENT_COMMENT"].ToString()+"\" target=_blank >" + dtrTransaction["SHIPMENT_COMMENT"].ToString()+"</a>";

					ddlStatus.SelectedItem.Selected = false;
                    ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(dtrTransaction["SHIPMENT_SHIP_STATUS"].ToString()));

					//order size drop-down
					ddlSize.Items.Add(new ListItem ("Rail Car","190000"));
					ddlSize.Items.Add(new ListItem ("Metric Ton","2205"));
					ddlSize.Items.Add(new ListItem ("Truckload Boxes","42000"));
					ddlSize.Items.Add(new ListItem ("Truckload Bags","44092"));
					ddlSize.Items.Add(new ListItem ("Bulk Truck","45000"));
					ddlSize.Items.Add(new ListItem ("Pounds (lbs)","1"));
					ddlSize.Items.Add(new ListItem ("20' Container","38500"));
					ddlSize.Items.Add(new ListItem ("30' Container","44500"));
					ddlSize.SelectedItem.Selected = false;
                    ddlSize.SelectedIndex = ddlSize.Items.IndexOf(ddlSize.Items.FindByValue(dtrTransaction["SHIPMENT_SIZE"].ToString()));

					if (dtrTransaction["BROKER"] !=  DBNull.Value)
					{
						broker = dtrTransaction["SHIPMENT_BROKER_ID"].ToString();
					}
					ddlBroker.Items.Add(new ListItem("None","null"));
				}

				using (SqlDataReader dtrBroker = DBLibrary.GetDataReaderFromSelect(connection, "select PERS_FRST_NAME,PERS_ID FROM PERSON WHERE PERS_TYPE='A' or PERS_TYPE='B' or PERS_TYPE='T'"))
				{
					while (dtrBroker.Read())
					{	
						ddlBroker.Items.Add(new ListItem(dtrBroker["PERS_FRST_NAME"].ToString(),dtrBroker["PERS_ID"].ToString()));
					}
					//selection the right item
                    if (broker != "") ddlBroker.SelectedIndex = ddlBroker.Items.IndexOf(ddlBroker.Items.FindByValue(broker)); 
				}			
				DefineShipmentTracks();
			}						
		}

		private void DefineShipmentTracks()
		{
			ArrayList arShipment = new ArrayList();

			//Tracking to see if any information changed
			AddOriginalValue(ref arShipment,ddlCompanySoldTo.SelectedItem.Text,lblCompanySoldTo.Text + "(Sold To)","ddlCompanySoldTo");
			AddOriginalValue(ref arShipment,ddlDeliveryTo.SelectedItem.Text,"Delivery To","ddlDeliveryTo");
			AddOriginalValue(ref arShipment,ddlUserSoldTo.SelectedItem.Text,lblCompanySoldTo.Text + "(Sold To)","ddlUserSoldTo");
			AddOriginalValue(ref arShipment,ddlCompanyFrom.SelectedItem.Text,lblCompanyFrom.Text + "(Purchase From)","ddlCompanyFrom");
			AddOriginalValue(ref arShipment,ddlUserFrom.SelectedItem.Text,lblUserFrom.Text + "(Purchase From)","ddlUserFrom");
			AddOriginalValue(ref arShipment,ddlPickupFrom.SelectedItem.Text,"Pickup From","ddlPickupFrom");
			AddOriginalValue(ref arShipment,ddlTerm.SelectedItem.Text,lblTerm.Text,"ddlTerm");
			AddOriginalValue(ref arShipment,txtPONumber.Text,lblPONumber.Text,"txtPONumber");
			AddOriginalValue(ref arShipment,txtTermsFrom.Text,lblTermsFrom.Text + "(Purchase From)","txtTermsFrom");
			AddOriginalValue(ref arShipment,txtTermsSoldTo.Text,lblTermsSoldTo.Text + "(Sold To)","txtTermsSoldTo");
			AddOriginalValue(ref arShipment,txtBuyPrice.Text,lblBuyPrice.Text,"txtBuyPrice");
			AddOriginalValue(ref arShipment,txtSellPrice.Text,lblSellPrice.Text,"txtSellPrice");
			AddOriginalValue(ref arShipment,txtQuatity.Text,lblQuantity.Text,"txtQuatity");
			AddOriginalValue(ref arShipment,txtComission.Text,lblComission.Text,"txtComission");
			AddOriginalValue(ref arShipment,txtWeight.Text,lblWeight.Text,"txtWeight");
			AddOriginalValue(ref arShipment,txtFreight.Text,lblFreight.Text,"txtFreight");
			AddOriginalValue(ref arShipment,txtDeliveryDate.Text,lblDeliveryDate.Text,"txtDeliveryDate");
			AddOriginalValue(ref arShipment,txtShippingDate.Text,lblShippingDate.Text,"txtShippingDate");
			AddOriginalValue(ref arShipment,ddlStatus.SelectedItem.Text,lblStatus.Text,"ddlStatus");
			AddOriginalValue(ref arShipment,ddlSize.SelectedItem.Text,"Size","ddlSize");
			AddOriginalValue(ref arShipment,ddlBroker.SelectedItem.Text,lblBroker.Text,"ddlBroker");
			
			ViewState["ShipmentOriginalValues"] = arShipment;
		}

		private void DefineOrderTracks()
		{
			ArrayList arOrder = new ArrayList();

			//Tracking to check if any information changed
			AddOriginalValue(ref arOrder,txtAdds.Text,lblAdds.Text,"txtAdds");
			AddOriginalValue(ref arOrder,txtMelt.Text,lblMelt.Text,"txtMelt");
			AddOriginalValue(ref arOrder,txtDensity.Text,lblDensity.Text,"txtDensity");
			AddOriginalValue(ref arOrder,txtAdds.Text,lblAdds.Text,"txtAdds");
			AddOriginalValue(ref arOrder,txtTransactionDate.Text,lblTransactionDate.Text,"txtTransactionDate");

			ViewState["OrderOriginalValues"] = arOrder;
		}

		private void OrderMode(bool editMode)
		{
			editMode = true;

			txtProductDesc.Enabled = editMode;
			txtTransactionDate.Enabled = editMode;
			txtMelt.Enabled = editMode;
			txtAdds.Enabled = editMode;
			txtDensity.Enabled = editMode;
			
			btnEditShipment.Enabled = editMode;
			btnDeleteShipment.Enabled = editMode;

			if (editMode)
			{
				//ShipmentMode(false);
				btnEditOrder.Text = "Update Order";
				//btnDeleteOrder.Text = "Cancel";
				//HelperFunction.RemoveControlConfirmationCode(btnDeleteOrder);
			}
			else
			{
				btnEditOrder.Text = "Edit Order";
				btnDeleteOrder.Text = "Delete Order";
				HelperFunction.InsertControlConfirmationCode(btnDeleteOrder,"Are you sure you want to delete this Order and all Shipments attached to it?");
			}
		}

		private void ShipmentMode(bool editMode)
		{
			editMode = true;

			ddlCompanySoldTo.Enabled = editMode;
			ddlUserSoldTo.Enabled = editMode;
			ddlDeliveryTo.Enabled = editMode;
			txtTermsSoldTo.Enabled = editMode;
			txtSellPrice.Enabled = editMode;
			ddlCompanyFrom.Enabled = editMode;
			ddlUserFrom.Enabled = editMode;
			ddlPickupFrom.Enabled = editMode;
			txtTermsFrom.Enabled = editMode;
			txtBuyPrice.Enabled = editMode;
			txtQuatity.Enabled = editMode;
			txtComission.Enabled = editMode;
			ddlTerm.Enabled = editMode;
			txtPONumber.Enabled = editMode;
			txtWeight.Enabled = editMode;
			txtFreight.Enabled = editMode;
			txtDeliveryDate.Enabled = editMode;
			txtShippingDate.Enabled = editMode;
			ddlStatus.Enabled = editMode;
			ddlSize.Enabled = editMode;
			ddlBroker.Enabled = editMode;

			btnEditOrder.Enabled = editMode;
			btnDeleteOrder.Enabled = editMode;

			if (editMode)
			{
				StatusEnable(); //Adjust components enabled acoording with Status selected
				ddlSize_SelectedIndexChanged(this,null);
				//OrderMode(false);
				btnEditShipment.Text = "Update Shipment";
				//btnDeleteShipment.Text = "Cancel";
				//HelperFunction.RemoveControlConfirmationCode(btnDeleteShipment);
			}
			else
			{
				btnEditShipment.Text = "Edit Shipment";
				btnDeleteShipment.Text = "Delete Shipment";
				HelperFunction.InsertControlConfirmationCode(btnDeleteShipment,"Are you sure you want to delete this Shipment?");
			}
		}

		private void SetUpComments()
		{			
			string strComment = "";
			string strSQL ="SELECT CONVERT(varchar,COMMENT_DATE,101) AS COMMENT_DATE, COMMENT_TEXT,(SELECT PERS_FRST_NAME FROM PERSON WHERE PERS_ID=COMMENT_PERSID) AS PERSON FROM SHIPMENT_COMMENT WHERE COMMENT_SHIPMENT=@ShipID ORDER BY COMMENT_DATE";
			Hashtable param = new Hashtable();
			param.Add("@ShipID", lblShipmentID.Text);
			DataTable dtComments = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), strSQL, param);
			foreach(DataRow dr in dtComments.Rows)
			{
				//creat string to show when mouseover
				if (strComment.ToString()!="") strComment += "<BR>"; //((char)13).ToString();
				strComment += dr["PERSON"] + " - " + dr["COMMENT_DATE"] + ": " + dr["COMMENT_TEXT"];
			}
			
			//Displaying comments
			if (strComment.ToString()!="")
			{
				imgComment.ImageUrl = "/pics/Icons/icon_quotation_bubble.gif";
				//imgComment.ToolTip = strComment;
			}
			else
			{
				imgComment.ImageUrl = "/Pics/icons/comment.gif";
				//imgComment.ToolTip = "";
			}
			string strActionBoxHTML = "linkset[1]='" + strComment + "'" + ((char)13).ToString();

			phActionButton.Controls.Add (new LiteralControl(strActionBoxHTML));

			imgComment.Attributes.Add("onmouseover","showmenu(event,linkset[1])");
			imgComment.Attributes.Add("onMouseout", "delayhidemenu()");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg.EditCommand += new DataGridCommandEventHandler(dg_EditCommand);
            this.dg.ItemCommand += new DataGridCommandEventHandler(dg_ItemCommand);
            this.dg.ItemDataBound += new DataGridItemEventHandler(dg_ItemDataBound);
		}
		#endregion

		protected void ddlCompanySoldTo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			RebindCompanySoldTo(true);
		}

		private void RebindCompanySoldTo(bool cleanCurrentItens)
		{
			if (cleanCurrentItens)
			{
				ddlUserSoldTo.Items.Clear();
				ddlDeliveryTo.Items.Clear();
			}

			// adding all the possible users within the company
			DataTable dtUsers = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), "Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME as PERS_NAME,PERS_ID FROM PERSON WHERE PERS_COMP="+ddlCompanySoldTo.SelectedItem.Value+" ORDER By PERS_TYPE DESC");
			foreach(DataRow dr in dtUsers.Rows)
			{
				ddlUserSoldTo.Items.Add(new ListItem((string)dr["PERS_NAME"],dr["PERS_ID"].ToString()));
			}

            lblBuyerEditUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=" + ddlUserSoldTo.SelectedValue + "&CompanyID=" + ddlCompanySoldTo.SelectedValue + "\">Edit</a>";
            lblBuyerAddUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=0&CompanyID=0" + ddlCompanySoldTo.SelectedValue + "\">Add new</a>";

			// adding all the possible locations within the company
			DataTable dtLocations = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), "select  PLAC_ID, LABEL = (select LOCL_CITY +', '+ LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) from place where PLAC_COMP="+ddlCompanySoldTo.SelectedItem.Value+" ORDER BY LABEL");
			foreach(DataRow dr in dtLocations.Rows)
			{
				ddlDeliveryTo.Items.Add(new ListItem(dr["LABEL"].ToString(),dr["PLAC_ID"].ToString()));
			}
			    		
			//add the warehouses
			dtLocations = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), "select  PLAC_ID, LABEL = (select LOCL_CITY +', '+ LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) + ' ('+PLAC_LABL+')'  from place where plac_type='W'  ORDER BY LABEL");
			foreach(DataRow dr in dtLocations.Rows)
			{
				ddlDeliveryTo.Items.Add(new ListItem(dr["LABEL"].ToString(),dr["PLAC_ID"].ToString()));
			}			
		}

		protected void ddlCompanyFrom_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			RebindCompanyFrom(true);
		}

		private void RebindCompanyFrom(bool cleanCurrentItens)
		{
			if (cleanCurrentItens)
			{
				ddlUserFrom.Items.Clear();
				ddlPickupFrom.Items.Clear();
			}
			
			// adding all the possible users within the company
			DataTable dtUsers = DBLibrary.GetDataTableFromSelect(Application["DBConn"].ToString(), "Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME as PERS_NAME,PERS_ID FROM PERSON WHERE PERS_COMP="+ddlCompanyFrom.SelectedItem.Value+" ORDER By PERS_TYPE DESC");
			foreach(DataRow dr in dtUsers.Rows)
			{
				ddlUserFrom.Items.Add(new ListItem((string)dr["PERS_NAME"],dr["PERS_ID"].ToString()));
			}

            lblSellerEditUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=" + ddlUserFrom.SelectedValue + "&CompanyID=" + ddlCompanyFrom.SelectedValue + "\">Edit</a>";
            lblSellerAddUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=0&CompanyID=0" + ddlCompanyFrom.SelectedValue + "\">Add new</a>";
		    
			// adding all the possible locations within the company
			DataTable dtLocations = DBLibrary.GetDataTableFromSelect(Application["DBConn"].ToString(), "select  PLAC_ID, LABEL = (select LOCL_CITY +', '+ LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) from place where PLAC_COMP="+ddlCompanyFrom.SelectedItem.Value+" ORDER BY LABEL");
			foreach(DataRow dr in dtLocations.Rows)
			{
				ddlPickupFrom.Items.Add ( new ListItem (dr["LABEL"].ToString(),dr["PLAC_ID"].ToString() ) );
			}
			    
			//add the warehouses
			dtLocations = DBLibrary.GetDataTableFromSelect(Application["DBConn"].ToString(), "select  PLAC_ID, LABEL = (select LOCL_CITY +', '+ LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) + ' ('+PLAC_LABL+')'  from place where plac_type='W'  ORDER BY LABEL");
			foreach(DataRow dr in dtLocations.Rows)
			{
				ddlPickupFrom.Items.Add ( new ListItem (dr["LABEL"].ToString(),dr["PLAC_ID"].ToString() ) );
			}			
		}

		protected void btnEditOrder_Click(object sender, System.EventArgs e)
		{
			if (btnEditOrder.Text=="Edit Order")
			{
				OrderMode(true);
			}
			else
			{
				//Check changes and Add Notes
				ArrayList arShipmentList = ShipmentList(lblOrderID.Text); //Look for all shipments from that Order is being changed
				CheckChanges((ArrayList)ViewState["OrderOriginalValues"],arShipmentList);
				//Update Order
				string Prime;
				if (cbPrime.Checked)
					Prime ="1";
				else
					Prime ="0";
				DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), "UPDATE ORDERS set ORDR_PRIME='"+Prime+"', ORDR_DATE='"+txtTransactionDate.Text+"', ORDR_MELT='"+txtMelt.Text+"', ORDR_DENS='"+txtDensity.Text+"', ORDR_ADDS='"+txtAdds.Text+"', ORDR_PROD_SPOT='"+txtProductDesc.Text+"' where ORDR_ID='"+lblOrderID.Text+"'");
				OrderMode(false);
				SetUpOrder(lblOrderID.Text);
				SetUpComments();
			}
		}

		protected void btnEditShipment_Click(object sender, System.EventArgs e)
		{
			ViewState["command"] = "Edit Shipment";
			confirmEditing();
		}


		private void editShipment()
		{
			if (btnEditShipment.Text=="Edit Shipment")
			{
				ShipmentMode(true);
			}
			else
			{
				//Check changes and Add Notes
				ArrayList arShipmentList = new ArrayList();
				arShipmentList.Add(lblShipmentID.Text); //Add the shipment ID is being changed
				CheckChanges((ArrayList)ViewState["ShipmentOriginalValues"],arShipmentList);
				
				//Status Validation: Shipment_Date_Take, Shipment_Date_Delivered
				ddlStatus_SelectedIndexChanged(this,null);
				
				//Update Shipments
				string strSQLShipment = "UPDATE SHIPMENT set SHIPMENT_FROM='"+ddlPickupFrom.SelectedItem.Value+"',SHIPMENT_FOB='"+ddlTerm.SelectedItem.Value.ToString()+"',SHIPMENT_TO='"+ddlDeliveryTo.SelectedItem.Value+"'";
				strSQLShipment +=",SHIPMENT_BUYR='"+ddlUserSoldTo.SelectedItem.Value+"',SHIPMENT_SELR='"+ddlUserFrom.SelectedItem.Value+"',SHIPMENT_PRCE='"+DBLibrary.ScrubSQLStringInput(txtBuyPrice.Text)+"',SHIPMENT_BUYR_PRCE='"+DBLibrary.ScrubSQLStringInput(txtSellPrice.Text)+"',SHIPMENT_COMM="+DBLibrary.ScrubSQLStringInput(txtComission.Text)+"";
				if(txtWeight.Enabled == true)
				{
					strSQLShipment +=",SHIPMENT_WEIGHT='"+DBLibrary.ScrubSQLStringInput(txtWeight.Text)+"'";
				}
				else
				{
					strSQLShipment +=",SHIPMENT_WEIGHT = null";
				}
				if (ddlBroker.SelectedItem.Value != "null")
				{
					strSQLShipment +=",SHIPMENT_BROKER_ID='"+ddlBroker.SelectedItem.Value+"'";
				}
				else
				{
					strSQLShipment +=",SHIPMENT_BROKER_ID = null";
				}
				strSQLShipment +=",SHIPMENT_BUYER_TERMS='"+DBLibrary.ScrubSQLStringInput(txtTermsSoldTo.Text)+"',SHIPMENT_PO_NUM='"+DBLibrary.ScrubSQLStringInput(txtPONumber.Text)+"',SHIPMENT_SELLER_TERMS='"+DBLibrary.ScrubSQLStringInput(txtTermsFrom.Text)+"'";
				strSQLShipment +=",SHIPMENT_SHIP_PRCE='"+DBLibrary.ScrubSQLStringInput(txtFreight.Text)+"',SHIPMENT_SHIP_PRICE_STATUS='"+ddlFreightStatus.SelectedValue.ToString()+"',SHIPMENT_QTY='"+DBLibrary.ScrubSQLStringInput(txtQuatity.Text)+"', SHIPMENT_SIZE="+ddlSize.SelectedItem.Value;
				if (txtDeliveryDate.Text.Trim()=="")
				{
					strSQLShipment +=",SHIPMENT_DATE_DELIVERED=null";
				}
				else
				{
					strSQLShipment +=",SHIPMENT_DATE_DELIVERED='"+DBLibrary.ScrubSQLStringInput(txtDeliveryDate.Text)+"'";
				}
				if (txtShippingDate.Text.Trim()=="")
				{
					strSQLShipment +=",SHIPMENT_DATE_TAKE=null";
				}
				else
				{
					strSQLShipment +=",SHIPMENT_DATE_TAKE='"+DBLibrary.ScrubSQLStringInput(txtShippingDate.Text)+"'";
				}

				double tax = 0;
				if(ViewState["Canadian_tax"] != null)
				{
					tax = Convert.ToDouble(ViewState["Canadian_tax"].ToString());
				}
				
				strSQLShipment += ", SHIPMENT_TAX=" + tax;


				strSQLShipment +=", SHIPMENT_SHIP_STATUS="+ddlStatus.SelectedItem.Value+", SHIPMENT_UPDATED ='1'";
				strSQLShipment +=" WHERE SHIPMENT_ORDR_ID='"+lblOrderID.Text+"' AND SHIPMENT_ID='"+lblShipmentID.Text+"';";

				DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), strSQLShipment);
				ShipmentMode(false);
				SetUpShipment(lblShipmentID.Text);
				SetUpComments();
				SetUpDocuments();//In case the user changes the company from seller or from buyer
			}

		}

		protected void btnDeleteOrder_Click(object sender, System.EventArgs e)
		{
			ViewState["command"] = "Delete Order";
			confirmEditing();
		}

		private void deleteOrder()
		{
			if (btnDeleteOrder.Text=="Cancel")
			{
				SetUpOrder(lblOrderID.Text);
				OrderMode(false);
			}
			else
			{

				//Stuart and Mike Fedele requires that
				//under no circumstance can an order be deleted 
				//if the material has been paid for ie it�s in the system as a paid payable. 
				if(alreadyPaid(lblShipmentID.Text))
				{
					//say something bad
					pnlFiles.Visible = false;
					pnlCanNotEdit.Visible = true;
					return;
				}

				//Delete Order
				Hashtable ht = new Hashtable();
				ht.Add("@Order_ID", lblOrderID.Text);
				
				DBLibrary.ExecuteStoredProcedure(Application["DBConn"].ToString(),"spCancelOrder",ht);
				Response.Redirect(Request.QueryString["Referer"]);
			}

		}

		private bool alreadyPaid(string order_id)
		{
			bool paid = false;
			
			string strSQL = "select pay_amnt from payment, shipment " + 
							"   where pay_pers=shipment_buyr " + 
							"   and pay_ordr=" + order_id;
			paid = DBLibrary.HasRows(Application["DBConn"].ToString(), strSQL);
			//need to check if we have payments associated with this transaction
			//payment>0 from payment where pay_ordr=order_id

			return paid;
		}


		private void deleteShipment()
		{
			if (btnDeleteShipment.Text=="Cancel")
			{
				SetUpShipment(lblShipmentID.Text);
				ShipmentMode(false);
			}
			else
			{
				//Stuart and Mike Fedele requires that
				//under no circumstance can an order be deleted 
				//if the material has been paid for ie it�s in the system as a paid payable. 
				if(alreadyPaid(lblShipmentID.Text))
				{
					//say something bad
					pnlConfirmationPanel.Visible = false;
					pnlFiles.Visible = false;
					pnlCanNotEdit.Visible = true;
					return;
				}

				//Delete Shipment
				//Checking if the oder has just that shipment
				
				DataTable dtShipment = DBLibrary.GetDataTableFromSelect(Application["DBConn"].ToString(), "select COUNT(SHIPMENT_ID) Quantity from shipment where shipment_ordr_id = '" + lblOrderID.Text + "'");
				
				int quantity = System.Convert.ToInt32(dtShipment.Rows[0]["Quantity"].ToString());
				if (quantity <=1)
				{
					Hashtable ht = new Hashtable();
					ht.Add("@Order_ID",lblOrderID.Text);

					//Delete Order
					DBLibrary.ExecuteStoredProcedure(Application["DBConn"].ToString(),"spCancelOrder",ht);
				}
				else
				{
					Hashtable ht = new Hashtable();
					ht.Add("@ShipmentID", lblShipmentID.Text);
					 
					//Delete Shipment
					DBLibrary.ExecuteStoredProcedure(Application["DBConn"].ToString(),"spCancelShipment",ht);
				}
				Response.Redirect(Request.QueryString["Referer"]);
			}

		}

		protected void btnDeleteShipment_Click(object sender, System.EventArgs e)
		{
			ViewState["command"] = "Delete Shipment";
			//deleteShipment();
			confirmEditing();
		}

		protected void btnAddNotes_Click(object sender, System.EventArgs e)
		{
			if (txtCommentNotes.Text != "")
			{
				DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), "INSERT into Shipment_Comment (Comment_Shipment, Comment_PersID, Comment_Date, Comment_Text) values ('"+lblShipmentID.Text+"','"+ Session["Id"].ToString()+"','"+System.DateTime.Today.Date+"','"+DBLibrary.ScrubSQLStringInput(txtCommentNotes.Text)+"')");
				txtCommentNotes.Text = "";
				SetUpComments();
			}
		}

		private void AddOriginalValue(ref ArrayList arList, string fieldContent, string displayLabel, string controlName)
		{
			string[] arValues = new string[4];
			arValues[0] = fieldContent;
			arValues[1] = displayLabel;
			arValues[2] = controlName;
			arList.Add(arValues);
		}

		private void CheckChanges(ArrayList arOriginalValues, ArrayList arShipments)
		{
			string currentValue;
			for(int i=0;i<arOriginalValues.Count;i++)
			{
				currentValue="";
				if (ChangedData((string[])arOriginalValues[i], ref currentValue))
				{
					//Save in the Shipment Notes Table of all Shipments send
					for(int j=0;j<arShipments.Count;j++)
					{
						string[] strItem = (string[])arOriginalValues[i];
						string strComment = HelperFunction.RemoveSqlEscapeChars (strItem[1] + " changed from " + strItem[0] +" to " + currentValue);
						DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(),"INSERT into Shipment_Comment (Comment_Shipment, Comment_PersID, Comment_Date, Comment_Text) values ('"+arShipments[j].ToString()+"','"+ Session["Id"].ToString()+"','"+System.DateTime.Today.Date+"','"+strComment+"')");
					}
				}
			}
		}

		private bool ChangedData(string[] data, ref string currentValue)
		{
			string[] arValues = data;
			bool changed = false;
			object _object = this.GetType().GetField(arValues[2],BindingFlags.Instance|BindingFlags.Public|BindingFlags.NonPublic).GetValue(this);
			if (_object.GetType().ToString()==typeof(TextBox).ToString())
			{
				currentValue = (string)((System.Type)_object.GetType()).InvokeMember("Text",BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty,null,_object,null);
			}
			else if(_object.GetType().ToString()==typeof(DropDownList).ToString())
			{
				ListItem lst = (ListItem)((System.Type)_object.GetType()).InvokeMember("SelectedItem",BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty,null,_object,null);
				currentValue = lst.Text;
			}
			else
			{
				arValues[0] = ""; //Nothing would be changed
			}
	
			//if the field was changed
			if (currentValue.Trim()!= arValues[0].Trim())
			{
				changed = true;
			}
			return changed;
		}

		private ArrayList ShipmentList(string OrderID)
		{			
			ArrayList arShipment = new ArrayList();

			DataTable dtShipment = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), "select SHIPMENT_ID from Shipment where shipment_ordr_id='" + OrderID + "'");
			foreach(DataRow dr in dtShipment.Rows)
			{
				arShipment.Add(dr["SHIPMENT_ID"].ToString());
			}
		
			return arShipment;
		}

		protected void ddlSize_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (ddlSize.SelectedValue == "1")
			{
				txtQuatity.Enabled = true;
			}
			else
			{
				txtQuatity.Enabled = false;
			}
		}

		private void StatusEnable()
		{
			if (ddlStatus.SelectedItem.Value.ToString() == "1") //Inventory
			{
				txtWeight.Enabled  = false;
				txtDeliveryDate.Enabled  = false;
				txtShippingDate.Enabled  = false;
			}
			else if (ddlStatus.SelectedItem.Value.ToString() == "2") //En Route
			{
				txtWeight.Enabled  = true;
				txtDeliveryDate.Enabled  = false;
				txtShippingDate.Enabled  = true;
			}
			else //Delivered
			{
				txtWeight.Enabled  = true;
				txtDeliveryDate.Enabled  = true;
				txtShippingDate.Enabled  = true;
			}
		}

		protected void ddlStatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//Status Validation
			if (ddlStatus.SelectedItem.Value.ToString() == "1") //Inventory
			{
				txtWeight.Text = "";
				txtDeliveryDate.Text = "";
				txtShippingDate.Text = "";
			}
			else if (ddlStatus.SelectedItem.Value.ToString() == "2") //En Route
			{
				txtDeliveryDate.Text = "";
				if (txtShippingDate.Text.Trim() == "") txtShippingDate.Text = DateTime.Today.ToShortDateString();
			}
			else //Delivered
			{
				if (txtDeliveryDate.Text.Trim() == "") txtDeliveryDate.Text = DateTime.Today.ToShortDateString();;
				if (txtShippingDate.Text.Trim() == "") txtShippingDate.Text = DateTime.Today.ToShortDateString();
			}

			StatusEnable();
		}

		protected void btnUpload_Click(object sender, System.EventArgs e)
		{
			int intImageSize;
			string strImageType;
			Stream ImageStream;
			string Path_FName;

			intImageSize = File_to_add.PostedFile.ContentLength;
			strImageType = File_to_add.PostedFile.ContentType;
			Path_FName = File_to_add.PostedFile.FileName;

			string [] strFName;
			char [] r = {'\\'};
			strFName=Path_FName.Split(r);
			int strFName_lengh = strFName.Length;
			string FName = strFName[strFName_lengh-1];	                 
       
			// Reads the Image
			ImageStream = File_to_add.PostedFile.InputStream;
 
			byte [] ImageContent = new byte [intImageSize];
			int intStatus;
			intStatus = ImageStream.Read(ImageContent, 0, intImageSize);
 
			//Create Instance of Connection and Command 
			SqlConnection myConnection = new  SqlConnection(Application["DBConn"].ToString());
			//SqlCommand myCommand = new SqlCommand("Insert into  File_Person (PersonEmail, PersonName, PersonSex, PersonDOB, PersonImage, PersonImageType) Values (@PersonEmail, @PersonName, @PersonSex, @PersonDOB, @PersonImage, @PersonImageType)", myConnection);


			SqlCommand myCommand = new SqlCommand("INSERT INTO UPLOADED_FILES (FILE_SHIPMENT,FILE_NAME,FILE_TYPE,FILE_DATA,FILE_LENGTH,FILE_OWNER,FILE_DESCRIPT,FILE_DATE,FILE_BUYER,FILE_SELLER) VALUES (@FILE_SHIPMENT,@FILE_NAME,@FILE_TYPE,@FILE_DATA,@FILE_LENGTH,@FILE_OWNER,@FILE_DESCRIPT,@FILE_DATE,@FILE_BUYER,@FILE_SELLER)", myConnection );

			
			SqlParameter prmName = new SqlParameter("@FILE_NAME", SqlDbType.VarChar, 255);
			prmName.Value = FName;
			myCommand.Parameters.Add(prmName);
				
			SqlParameter prmDescript = new SqlParameter("@FILE_DESCRIPT", SqlDbType.VarChar, 255);
			prmDescript.Value = Description.Text;
			myCommand.Parameters.Add(prmDescript);
				
			SqlParameter prmOwner = new SqlParameter("@FILE_OWNER", SqlDbType.Int, 4);
			prmOwner.Value = Convert.ToInt32(Session["ID"]);
			myCommand.Parameters.Add(prmOwner);

			SqlParameter prmShipment = new SqlParameter("@FILE_SHIPMENT", SqlDbType.Int, 4);
			prmShipment.Value = Convert.ToInt32(lblShipmentID.Text);
			myCommand.Parameters.Add(prmShipment);

			SqlParameter prmLength = new SqlParameter("@FILE_LENGTH", SqlDbType.Int, 9);
			prmLength.Value = intImageSize;
			myCommand.Parameters.Add(prmLength);

			SqlParameter prmBuyer = new SqlParameter("@FILE_BUYER", SqlDbType.VarChar, 50);
			if(CheckBuyer.Checked == true)
				prmBuyer.Value = "1";
			else
				prmBuyer.Value = "0";

			SqlParameter prmSeller = new SqlParameter("@FILE_SELLER", SqlDbType.VarChar, 50);
			if(CheckSeller.Checked == true)
				prmSeller.Value = "1";
			else
				prmSeller.Value = "0";
				
			myCommand.Parameters.Add(prmBuyer);
			myCommand.Parameters.Add(prmSeller);

			SqlParameter prmDate = new SqlParameter("@FILE_DATE", SqlDbType.DateTime);
			prmDate.Value = DateTime.Now;
			myCommand.Parameters.Add(prmDate);
 
			SqlParameter prmImage = new SqlParameter("@FILE_DATA", SqlDbType.Image);
			prmImage.Value = ImageContent;
			myCommand.Parameters.Add(prmImage);
 
			SqlParameter prmImageType = new SqlParameter("@FILE_TYPE", SqlDbType.VarChar, 255);
			prmImageType.Value = strImageType;
			myCommand.Parameters.Add(prmImageType);
 
			myConnection.Open();
			myCommand.ExecuteNonQuery();
			myConnection.Close();
			Description.Text="";
			SetUpDocuments();
		}

		private void SetUpDocuments()
		{
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			string strSQL="";
			strSQL="SELECT CAST(FILE_ID AS VARCHAR)AS ID,FILE_NAME,FILE_DESCRIPT,(SELECT PERS_FRST_NAME FROM PERSON WHERE PERS_ID=FILE_OWNER) AS FILE_OWNER,FILE_DATE,(CASE WHEN  FILE_BUYER='1' THEN '<B>X </B>' END) AS FILE_BUYER,(CASE WHEN FILE_SELLER='1' THEN '<B>X </B>' END) AS FILE_SELLER FROM UPLOADED_FILES WHERE FILE_SHIPMENT="+lblShipmentID.Text;

			SqlDataAdapter dadContent;
			DataSet dstContent;

			dadContent = new SqlDataAdapter(strSQL ,conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
    
			dg.DataSource = dstContent;
			
			dg.DataBind();
			if (dg.Items.Count==0)
				dg.Visible=false;
			else
				dg.Visible=true;
			conn.Close();

			lblBuyer.Text = ddlCompanySoldTo.SelectedItem.Text +" (Buyer)";
			lblSeller.Text = ddlCompanyFrom.SelectedItem.Text +" (Seller)";
		}

		private void dg_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName == "Delete")
			{
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), "Delete From UPLOADED_FILES WHERE FILE_ID='"+e.Item.Cells[8].Text+"'");
				SetUpDocuments();
			}
		}

		private void dg_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				// retrieving the file extension
				string strExtension = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "FILE_NAME"));
				try
				{
					strExtension = strExtension.Substring(strExtension.LastIndexOf("."),strExtension.Length-strExtension.LastIndexOf("."));
					// adding picture to match the file extension
					switch (strExtension)
					{
						case ".doc":
							e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_doc.gif\">";
							break;
						case ".xls":
							e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_xls.gif\">";
							break;
						case ".pdf":
							e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_pdf.gif\">";
							break;
						case ".mdb":
							e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_mdb.gif\">";
							break;
						case ".htm":
							e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_htm.gif\">";
							break;
						case ".html":
							e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_htm.gif\">";
							break;
						default:
							e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_other.gif\">";
							break;
					}
				}
				catch
				{
					e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_other.gif\">";
				}	
			}
		}

		private void dg_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			SqlConnection myConnection = new SqlConnection(Application["DBConn"].ToString());
			myConnection.Open();
			SqlCommand cmdUpload = new SqlCommand("select * from UPLOADED_FILES WHERE FILE_ID='"+e.Item.Cells[8].Text+"'", myConnection);
			SqlDataReader myDataReader = cmdUpload.ExecuteReader(CommandBehavior.CloseConnection);
               
			myDataReader.Read();
			Response.ContentType = myDataReader["FILE_TYPE"].ToString();
			Response.AppendHeader("Content-Disposition", "attachment;filename=" + myDataReader["FILE_NAME"].ToString());	
				
			Response.BinaryWrite((byte [])myDataReader["FILE_DATA"]);
			Response.End();
			myDataReader.Close();
			myConnection.Close();
		}

		protected void lnkRecordPaymentSoldTo_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("/Creditor/Edit_Payments.aspx?Receivable=true&ID=" + Request.QueryString["Id"].ToString());
		}

		protected void lnkRecordPaymentFrom_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("/Creditor/Edit_Payments.aspx?ID=" + Request.QueryString["Id"].ToString());
		}

		private decimal TotalPaid(string userID, string shipmentID)
		{
			decimal total = 0;

			string SQL = "SELECT SUM(PAYMENT.PAY_AMNT) PAYMENT ";
			SQL += "FROM COMPANY INNER JOIN  PERSON ON COMPANY.COMP_ID = PERSON.PERS_COMP CROSS JOIN  PAYMENT ";
			SQL += "WHERE (PAYMENT.PAY_SHIPMENT = '"+shipmentID+"') AND (PAYMENT.PAY_PERS = '"+userID+"') AND (PERSON.PERS_ID = '"+userID+"')";
			
			DataTable dtPayment = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), SQL);			
			total = HelperFunction.getSafeDecimalFromDB(dtPayment.Rows[0]["PAYMENT"].ToString());
			return total;
		}

		protected void ddlBroker_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());
			SqlCommand cmdBroker;
			SqlDataReader dtrBroker;
			conn.Open();
    
			cmdBroker= new SqlCommand("select pers_id, BROKER_STANDARD_COMMISION from person where pers_id=" + ddlBroker.SelectedValue , conn);
			dtrBroker = cmdBroker.ExecuteReader();
			if(dtrBroker.Read())
			{
				txtComission.Text = HelperFunction.getSafeDoubleFromDB(dtrBroker["BROKER_STANDARD_COMMISION"]).ToString();
			}
			dtrBroker.Close();
			conn.Close();
		
		}

		private string needsApprovement()
		{
			string[] fields = (string[])ViewState["fields"];
			string ret_value = "";
			if(fields[0] != txtBuyPrice.Text)
			{
				ret_value = "Buy Price, ";
			}
			if(fields[1] != txtSellPrice.Text)
			{
				ret_value += "Sell Price, ";
			}
			if(fields[2] != txtComission.Text)
			{
				ret_value += "Comission, ";
			}
			if(fields[3] != txtTermsFrom.Text)
			{
				ret_value += "Terms, ";
			}
			if(fields[4] != txtTermsSoldTo.Text)
			{
				ret_value += "Terms, ";
			}
			if(fields[5] != txtWeight.Text)
			{
				if(fields[7] != "1")
				{
					ret_value += "Weight, ";
				}
			}
//			if(fields[6] != txtFreight.Text)
//			{
//				ret_value += "Frieght, ";
//			}

			if(ret_value != "") ret_value = ret_value.Substring(0, ret_value.Length - 2);
			

			return ret_value;
		}

		private bool isFinancialManager()
		{
			bool ret_value = false;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();

			SqlCommand command = new SqlCommand("SELECT FINANCIAL_MANAGER_STATUS FROM PERSON WHERE PERS_ID=" + Session["Id"], conn);
			SqlDataReader reader = command.ExecuteReader();
			if(reader.Read())
			{
				if(HelperFunction.getSafeBooleanFromDB(reader["FINANCIAL_MANAGER_STATUS"]))
				{
					ret_value = true;
				}
			}
			reader.Close();
			conn.Close();
			return ret_value;

		}

		private void confirmEditing()
		{
//			lblConfirmationMessage.Visible = false;
			if(!isFinancialManager())
			{
				string fields = "";
				string action = (string)ViewState["command"];
				if((action == "Delete Order") || (action == "Delete Shipment"))
				{
					pnlConfirmationPanel.Visible = true;
					pnlFiles.Visible = false;
					lblConfirmationMessage.Text = "Action needs to be approved by supervisor!<br>" + action;
				}
				else if((fields = needsApprovement()) != "")
				{
					pnlConfirmationPanel.Visible = true;
					pnlFiles.Visible = false;
					lblConfirmationMessage.Text = "Action needs to be approved by supervisor!<br>Folowing fields were changed:<br><b>" + fields + "</b>";
				}
				else
				{
					makeAction();
				}
			}
			else
			{
				makeAction();
			}
		}

		private void makeAction()
		{
			pnlConfirmationPanel.Visible = false;
			pnlFiles.Visible = true;
			string action = (string)ViewState["command"];
			if(action == "Delete Order")
			{
				deleteOrder();
			}
			else if (action == "Edit Shipment")
			{
				editShipment();
			}
			else if (action == "Delete Shipment")
			{
				deleteShipment();
			}
			saveFields();
		}

		private bool isCorrectPassword(string passwd)
		{
			bool ret_value = false;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();

			SqlCommand command = new SqlCommand("SELECT PERS_FRST_NAME FROM PERSON WHERE FINANCIAL_MANAGER_STATUS=1 AND PERS_PSWD='" + HelperFunction.RemoveSqlEscapeChars(Crypto.Encrypt(passwd)) + "'", conn);
			SqlDataReader reader = command.ExecuteReader();
			if(reader.Read())
			{
					ret_value = true;
			}

			reader.Close();
			conn.Close();

			return ret_value;

		}

		protected void btnSubmitPassword_Click(object sender, System.EventArgs e)
		{
				
			if(isCorrectPassword(txtPassword.Text))
			{
				pnlConfirmationPanel.Visible = false;
				lblConfirmMessage.Visible = false;
				makeAction();
				ViewState["fields"] = "";
				saveFields();
			}
			else
			{
				lblConfirmMessage.Visible = true;
				pnlSubPanel.Visible = true;
			}
		}

		private void returnOldValues()
		{
			string[] fields = (string[])ViewState["fields"];
			txtBuyPrice.Text = fields[0];
			txtSellPrice.Text = fields[1];
			txtComission.Text = fields[2];
			txtTermsFrom.Text = fields[3];
			txtTermsSoldTo.Text = fields[4];
			txtWeight.Text = fields[5];
			//txtFreight.Text = fields[6];

		}

		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			lblConfirmMessage.Visible = false;
			pnlFiles.Visible = true;
			pnlConfirmationPanel.Visible = false;
//			returnOldValues();
		
		}

		void applyTaxes()
		{
			decimal tax = 0;
			tax = HelperFunction.returnTaxByPlace(ddlDeliveryTo.SelectedValue, this.Context);
			ViewState["Canadian_tax"] = tax;
		}

		protected void ddlDeliveryTo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			applyTaxes();

			decimal sellprice = 0;
			decimal buyprice = 0;
			
			if(ViewState["sellprice_without_tax"] != null)
			{
				sellprice = Convert.ToDecimal(ViewState["sellprice_without_tax"].ToString());
			}

			if(ViewState["buyprice_without_tax"] != null)
			{
				buyprice = Convert.ToDecimal(ViewState["buyprice_without_tax"].ToString());
			}

			decimal gst_hst_tax = 0;
			
			if(ViewState["Canadian_tax"] != null)
			{
				gst_hst_tax = Convert.ToDecimal(ViewState["Canadian_tax"].ToString());
			}

			//Check if Canadian taxes (GST, HST) apply to the shipment
			if(gst_hst_tax != 0)
			{
				lblTotalOutstandingsSoldTo.Text = Convert.ToString(( (100 + gst_hst_tax) / 100 )* sellprice);
				lblTotalOustandingFrom.Text = Convert.ToString(( (100 + gst_hst_tax) / 100 )* buyprice);
			}
			else
			{
				lblTotalOutstandingsSoldTo.Text = Convert.ToString(sellprice);
				lblTotalOustandingFrom.Text = Convert.ToString(buyprice);
			}
		
		}

		protected void btnOK_Click(object sender, System.EventArgs e)
		{
			pnlCanNotEdit.Visible = false;
			pnlFiles.Visible = true;
		}
	}
}