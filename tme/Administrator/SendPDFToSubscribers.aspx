<%@ Page language="c#" Codebehind="SendPDFToSubscribers.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.SendPDFToSubscribers" MasterPageFile="~/MasterPages/Menu.Master" Title="Send PDF to Subscribers" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
<div style="width:780px">
<asp:panel CssClass="Content Color2" id=pnlSubmitPDF Runat="server"><br /><b>Please upload recent PDF file with Monthly Update you want to send to Subscribers:</b>
<br /><br /><INPUT class="InputForm" id="File1" type="file" name="File1" runat="server"> 
<br /><br /><INPUT class="Content Color2" id="Submit1" type="submit" value="Upload" name="Submit1" runat="server" onserverclick="Submit1_ServerClick"><br /><br />
</asp:Panel>
<div style="width:780px; padding-left:40px">
<asp:panel HorizontalAlign="Left" id="pnlSendEmail" Runat="server" Visible="False"><br /><br /><span class="Content Color2">Send To:</span> 
<asp:DropDownList CssClass="InputForm" id="ddlSendTo" Runat="server">
	<asp:ListItem Selected="True">admin</asp:ListItem>
	<asp:ListItem>All subscribers</asp:ListItem>
</asp:DropDownList><br />
<br /><span class="Content Color2 Left">Subject:</span><br />
<asp:TextBox CssClass="InputForm" id=txtSubject Width="70%" Runat="server"></asp:TextBox><span class="Contet Color2"><br /><br />Attachment:</span> 
<span class="LinkNormal"><asp:Literal id="lnkPDF" Runat="server"></asp:Literal></span><br /><br />Message<br />
<asp:TextBox CssClass="InputForm" id=txtMessage Width="70%" Runat="server" Height="150px" TextMode="MultiLine">Please find attached The Plastic Exchange Market Update in a new easy to print .pdf format. Feel free to forward it to your colleagues.

The Market Update is also online &lt;a href=&quot;http://www.theplasticexchange.com&quot;&gt;The Plastic Exchange&lt;/a&gt;

Your comments are always welcome.

Sincerely,

Michael Greenberg, CEO
The Plastic Exchange
312.202.0002</asp:TextBox><br /><br /><br />
<asp:Button CssClass="Content Color2" id="btnSend" Runat="server" Text="Send Message" onclick="btnSend_Click"></asp:Button><br /></asp:Panel><asp:panel id=pnlConfirmation Runat="server" Visible="false">
<br /><br />
<asp:Label id="lblConfirm" Runat="server"></asp:Label><br /><br />
<table align="center" width="60%">
  <tr>
    <td>
      <DIV 
    align="left">
<asp:Label CssClass="Content Color2" id="lblMessage" Runat="server"></asp:Label></DIV>
</td>
</tr>
</table><br /><br />
<asp:Button CssClass="Content Color2" id="btnSure" Runat="server" Text="Yes, I want to send" onclick="btnSure_Click"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<asp:Button CssClass="Content Color2" id="btnCancel" Runat="server" Text="No" onclick="btnCancel_Click"></asp:Button><br /><br /></asp:Panel>
<asp:panel id="pnlOK" Runat="server" Visible="false"><br /><br />
<asp:Label CssClass="Content Color2" id="lblOK" Runat="server">Message(s) was(were) successfully sent. You will receive confirmation message.</asp:Label><br /><br />
<asp:Button CssClass="Content Color2" id="btnGo" Runat="server" Text="Go to Default Page" onclick="btnGo_Click"></asp:Button><br /><br /></asp:Panel>
<br /><br /></div></div>
</asp:Content>