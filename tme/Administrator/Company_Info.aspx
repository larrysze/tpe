<%@ Page Language="c#" Codebehind="Company_Info.aspx.cs" AutoEventWireup="True" Inherits="localhost.Common.Company_Info"
    MasterPageFile="~/MasterPages/Menu.Master" Title="Company Info" %>

<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
    <div id="pnlStart">
        <br />
        <center>
            <span class="Header Color2 Bold" id="lblcompanyname2">
                <asp:Label ID="lblCompanyName" runat="server" Width="100%" CssClass="PageHeader"></asp:Label>
            </span>
            <h3>
                <span>
                    <div class="DivTitleBarMenu">
                        <table id="Table2copy" width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td height="25" align="left">
                                    <asp:Label ID="Label4" runat="server" CssClass="Header Bold Color1">Company Info</asp:Label></td>
                                <td class="Content Bold Color1" height="25">
                                    &nbsp;
                                    <asp:Label ID="lblCompanyID" runat="server" Visible="False"></asp:Label></td>
                                <td class="Content Bold Color2" colspan="3" height="25">
                                </td>
                            </tr>                            
                        </table>
                    </div>
                    <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td height="63" valign="bottom" align="left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label1" runat="server" Font-Bold="True">Company Name:</asp:Label><br />
                                <asp:TextBox CssClass="InputForm" ID="txtCompanyName" runat="server" Width="282px"
                                    Enabled="False" MaxLength="40"></asp:TextBox></td>
                            <td height="63" valign="bottom" align="left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label2" runat="server" Font-Bold="True">WebSite:</asp:Label><br />
                                <asp:TextBox CssClass="InputForm" ID="txtWebSite" runat="server" Width="257px" Enabled="False"
                                    MaxLength="50"></asp:TextBox></td>
                            <td height="63" valign="bottom" align="left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label3" runat="server" Font-Bold="True">Credit Limit:</asp:Label>
                                <asp:Label CssClass="Content Bold Color2" ID="lblOriginalValue" runat="server" Visible="False"></asp:Label><br />
                                <asp:TextBox CssClass="InputForm" ID="txtCreditLimit" runat="server" Width="88px"
                                    Enabled="False" MaxLength="15"></asp:TextBox></td>
                            <td height="63" valign="bottom" align="left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label20" runat="server" Font-Bold="True"
                                    Width="56px">Terms (days):</asp:Label>
                                <asp:Label CssClass="Content Bold Color2" ID="lblOriginalTermsValue" runat="server"
                                    Visible="False"></asp:Label><br />
                                <asp:TextBox CssClass="InputForm" ID="txtTerms" Width="40px" runat="server" MaxLength="15"
                                    Enabled="False"></asp:TextBox></td>
                        </tr>
                        <tr style="height:30px">
                            <td colspan="4">                                
                                <asp:Button ID="btnUpdateCompanyInfo" runat="server" CssClass="Content" Width="70px"
                                    Text="Update" OnClick="btnUpdateCompanyInfo_Click"></asp:Button>&nbsp;
                                <asp:Button ID="btnCancelUpdateCompany" Width="70px" runat="server" CssClass="Content"
                                    Text="Cancel" Visible="False" OnClick="btnCancelUpdateCompany_Click"></asp:Button>
                            </td>
                        </tr>
                    </table>
                    <div class="DivTitleBarMenu">
                        <table width="100%" id="table4copy" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td width="580" class="Header Bold Color1" align="left">
                                    <asp:Label ID="Label8" runat="server">Bank Info</asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table id="Table4" cellspacing="0" cellpadding="1" width="100%" border="0">
                        <tr>
                            <td colspan="4">
                                &nbsp;<br>
                            </td>
                        </tr>
                        <tr>
                            <td width="238" align="Left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label7" runat="server">Bank Name:</asp:Label><br />
                                <asp:TextBox CssClass="InputForm" ID="txtBankName" runat="server" Width="203px" Enabled="False"
                                    MaxLength="50"></asp:TextBox></td>
                            <td width="258" align="Left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label6" runat="server" Font-Bold="True">Contact:</asp:Label><br>
                                <asp:TextBox CssClass="InputForm" ID="txtContact" runat="server" Enabled="False"
                                    MaxLength="20"></asp:TextBox></td>
                            <td width="181" align="Left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label5" runat="server" Font-Bold="True">Phone:</asp:Label><br>
                                <asp:TextBox CssClass="InputForm" ID="txtPhone" runat="server" Enabled="False" MaxLength="20"></asp:TextBox></td>
                            <td width="157" align="Left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label12" runat="server" Font-Bold="True">Fax:</asp:Label><br>
                                <asp:TextBox CssClass="InputForm" ID="txtFax" runat="server" Enabled="False" MaxLength="20"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td width="238" align="Left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label9" runat="server" Font-Bold="True">Address:</asp:Label><br>
                                <asp:TextBox CssClass="InputForm" ID="txtAddress" runat="server" Width="203px" Enabled="False"
                                    MaxLength="50"></asp:TextBox></td>
                            <td width="258" align="Left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label10" runat="server" Font-Bold="True">City:</asp:Label><br>
                                <asp:TextBox CssClass="InputForm" ID="txtCity" runat="server" Enabled="False" MaxLength="20"></asp:TextBox></td>
                            <td width="181" align="Left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label13" runat="server" Font-Bold="True">State:</asp:Label><br>
                                <asp:TextBox CssClass="InputForm" ID="txtState" runat="server" Enabled="False" MaxLength="2"></asp:TextBox></td>
                            <td width="157" align="Left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label11" runat="server" Font-Bold="True">Zip:</asp:Label><br>
                                <asp:TextBox CssClass="InputForm" ID="txtZip" runat="server" Enabled="False" MaxLength="10"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td valign="top" width="238" align="Left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label17" runat="server" Font-Bold="True">Address (Cont):</asp:Label><br>
                                <asp:TextBox CssClass="InputForm" ID="txtAddress2" runat="server" Width="203px" Enabled="False"
                                    MaxLength="50"></asp:TextBox></td>
                            <td width="258" align="Left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label14" runat="server" Font-Bold="True">Account Number:</asp:Label><br>
                                <asp:TextBox CssClass="InputForm" ID="txtAccountNumber" runat="server" Enabled="False"
                                    MaxLength="15" OnTextChanged="txtAccountNumber_TextChanged"></asp:TextBox></td>
                            <td width="181" align="Left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label15" runat="server" Font-Bold="True">Repetitive Number:</asp:Label><br>
                                <asp:TextBox CssClass="InputForm" ID="txtRepetitiveNumber" runat="server" Enabled="False"
                                    MaxLength="5"></asp:TextBox></td>
                            <td width="157" align="Left">
                                <asp:Label CssClass="Content Bold Color2" ID="Label16" runat="server" Font-Bold="True">ABA Number:</asp:Label><br>
                                <asp:TextBox CssClass="InputForm" ID="txtABANumber" runat="server" Enabled="False"
                                    MaxLength="20"></asp:TextBox></td>
                        </tr>
                        <tr style="height:30px">                            
                            <td colspan="4" class="Content Bold Color1" width="100%">
                                <asp:Button ID="btnUpdateBankInfo" runat="server" Width="70px" Text="Update" CssClass="Content"
                                    OnClick="btnUpdateBankInfo_Click"></asp:Button>&nbsp;
                                <asp:Button ID="btnCancelUpdateBankInfo" Width="70px" runat="server" Text="Cancel"
                                    CssClass="Content" Visible="False" OnClick="btnCancelUpdateBankInfo_Click"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </span>
            </h3>
        </center>
    </div>
    <br />
    <table id="Table5" cellspacing="0" cellpadding="1" width="100%" border="0">
        <tr>
            <td width="50%" class="Header Bold Color2" align="Left">
                <asp:Label ID="Label18" Font-Size="Medium" runat="server" Font-Bold="True" Width="61px">Users</asp:Label></td>
            <td width="178px" class="Header Color2">
                <p align="right">
                    <asp:Button ID="btnAddNewUser" runat="server" CssClass="Content" Width="82px" Text="Add new"
                        OnClick="btnAddNewUser_Click"></asp:Button>&nbsp;</p>
            </td>
            <td height="26">
                &nbsp;</td>
            <td width="50%" class="Header Bold Color2" align="left">
                <asp:Label ID="Label19" Font-Size="Medium" runat="server" Font-Bold="True" Width="141px">&nbsp;Locations</asp:Label></td>
            <td width="50%" class="ListHeadlineBold">
                <p align="right">
                    <asp:Button ID="btnAddNewLocation" runat="server" CssClass="Content" Width="82px"
                        Text="Add new" OnClick="btnAddNewLocation_Click"></asp:Button>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td valign="top" width="380" colspan="2">
                <br>
                <asp:DataGrid BorderWidth="0" BackColor="#000000" CellSpacing="1" ID="dgUsers" runat="server"
                    Width="370px" HorizontalAlign="Center" CellPadding="2" AllowSorting="True" AutoGenerateColumns="False"
                    CssClass="DataGrid" OnPreRender="dgUsers_PreRender">
                    <AlternatingItemStyle CssClass="LinkNormal LightGray"></AlternatingItemStyle>
                    <ItemStyle CssClass="LinkNormal DarkGray"></ItemStyle>
                    <HeaderStyle CssClass="Header Color2 Bold OrangeColor"></HeaderStyle>
                    <Columns>
                        <asp:ButtonColumn HeaderText="User Name" CommandName="Edit"></asp:ButtonColumn>
                        <asp:BoundColumn Visible="False" DataField="PERS_NAME" HeaderText="User Name"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="PERS_ID" HeaderText="Person ID"></asp:BoundColumn>
                        <asp:BoundColumn DataField="PERS_TYPE_DESC" HeaderText="Type"></asp:BoundColumn>
                        <asp:ButtonColumn Visible="False" Text="Delete" CommandName="Delete"></asp:ButtonColumn>
                    </Columns>
                </asp:DataGrid></td>
            <td valign="top">
            </td>
            <td valign="top" colspan="2">
                <br />
                <asp:DataGrid CellSpacing="1" BackColor="#000000" BorderWidth="0" ID="dgLocations"
                    runat="server" Width="365px" HorizontalAlign="Center" CellPadding="2" AllowSorting="True"
                    AutoGenerateColumns="False" CssClass="DataGrid" OnPreRender="dgLocations_PreRender">
                    <AlternatingItemStyle CssClass="LinkNormal LightGray"></AlternatingItemStyle>
                    <ItemStyle CssClass="LinkNormal DarkGray"></ItemStyle>
                    <HeaderStyle CssClass="Header Color2 Bold OrangeColor"></HeaderStyle>
                    <Columns>
                        <asp:ButtonColumn HeaderText="Location Name" CommandName="Edit"></asp:ButtonColumn>
                        <asp:BoundColumn Visible="False" DataField="PLAC_LOCATION" HeaderText="Location Name">
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="PLAC_ID" HeaderText="Place ID"></asp:BoundColumn>
                        <asp:BoundColumn DataField="PLAC_TYPE" HeaderText="Type"></asp:BoundColumn>
                        <asp:BoundColumn DataField="PLAC_ZIP" HeaderText="Zip Code"></asp:BoundColumn>
                        <asp:ButtonColumn Text="Delete" CommandName="Delete"></asp:ButtonColumn>
                    </Columns>
                </asp:DataGrid></td>
        </tr>
    </table>
</asp:Content>
