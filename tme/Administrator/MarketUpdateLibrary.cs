using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;
using System.Data.SqlClient;
using System.Web.Mail;
using System.Text.RegularExpressions;
using System.Threading;


namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for MarketUpdateLibrary.
	/// </summary>
	public class MarketUpdateLibrary
	{
		public MarketUpdateLibrary()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		
		static public string getPreview(string strPolyethylene, string strPolypropylene, string strPolystyrene, string strComments, string strTitle, HttpContext webContext)
		{
			string [] charts = new string[6];
			DateTime moment = DateTime.Now;
			charts[0] = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Charts/S_Chart_3_1M.png";
			charts[1] = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Charts/S_Chart_3_1Y.png";
			charts[2] = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Charts/S_Chart_26_1M.png";
			charts[3] = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Charts/S_Chart_26_1Y.png";
			charts[4] = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Charts/S_Chart_20_1M.png";
			charts[5] = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Charts/S_Chart_26_1Y.png";
			string total = TotalPoundsSpotFloor(webContext);
			string snapshot = HTMLSnapshotSpotFloor(webContext);
			return getPreview(strPolyethylene, strPolypropylene, strPolystyrene, charts, strComments, total, snapshot, moment.ToShortDateString(), strTitle, "", "", "", "", "", "", "", "", "", webContext);
		}

		//would be better if we can declare this function as static and place in Library
		static public string getPreview(string strPolyethylene, string strPolypropylene, string strPolystyrene, string [] charts, string strComments, string totalPounds, string snapshot, string issue_date, string strTitle, string txtPreTextPE, string txtPreTextPP, string txtPreTextPS, string txtVolumePE, string txtVolumePP, string txtVolumePS, string txtPricePE, string txtPricePP, string txtPricePS,  HttpContext webContext)
		{
			string returnHTML = "";

			returnHTML = "<table border=0 cellspacing=0 cellpadding=0 width=100% >" ; 
		
			returnHTML += "<tr><td align=left width=50>";
			returnHTML += "<a href=\"" + ConfigurationSettings.AppSettings["DomainName"] + "\" target=\"blank\">";
			
			returnHTML += "<img border=0 src=http://" + ConfigurationSettings.AppSettings["DomainName"] + "/pics/Market_Update/tpelogo.gif></a></td>" + 
			
				"<td align=left><a href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "\" target=\"blank\">" + 
				"<font face='arial black' size=5 color=Black>The</font>" + 
				"<font face='arial black' size=5 color=red>Plastics</font>" + 
				"<font face='arial black' size=5 color=Black>Exchange</font>" + 
				"<font face='arial black' size=5 color=red>.</font>" + 
				"<font face='arial black' size=5 color=Black>com</font></a></td></tr></table>" 
				+ Generate_Content(strPolyethylene, strPolypropylene, strPolystyrene, charts, strComments, totalPounds, snapshot, issue_date, strTitle, txtPreTextPE, txtPreTextPP, txtPreTextPS, txtVolumePE, txtVolumePP, txtVolumePS, txtPricePE, txtPricePP, txtPricePS, webContext);

			return returnHTML;
		}

		//would be better if we can declare this function as static and place in Library
		static public int getUnpublishedID(string strXmlFileName)
		{
			//if XML-file doesn't exist or no "unpublished" record in it 
			//then we should create the file or/and create empty record
			
			//must return index of "unpublished" record in XML-file
			int return_index = 0;
			bool create_new = true;
			if(System.IO.File.Exists(strXmlFileName.ToString()))
			{
				XmlTextReader xmlreader = new XmlTextReader(strXmlFileName.ToString());
				XmlDocument xmldoc = new XmlDocument();
				xmldoc.Load(xmlreader);
				XmlNodeList unpublished = xmldoc.GetElementsByTagName("unpublished");
				
				//we could just return (count - 2) value, since we are sure that we add new node to the end of XML
				//but this is not very general approach
				int i = 0;
				while((i < unpublished.Count) && create_new)
				{
					if(unpublished.Item(i).InnerText == "true")
					{
						create_new = false;
						return_index = i;
					}
					i++;
				}

				xmlreader.Close();

			}
			else
			{

				XmlTextWriter xmlwriter = new XmlTextWriter(strXmlFileName.ToString(), System.Text.Encoding.UTF8);
				xmlwriter.WriteStartDocument();
				xmlwriter.WriteStartElement("marketUpdate");
				xmlwriter.WriteEndDocument();
				xmlwriter.Close();
			}

			if(create_new)
			{
				XmlTextReader xmlreader = new XmlTextReader(strXmlFileName.ToString());
				XmlDocument xmldoc = new XmlDocument();
				xmldoc.Load(xmlreader);
				xmlreader.Close();


				//Create a new node.
				XmlElement record_elem = xmldoc.CreateElement("MU_record");

				DateTime now = DateTime.Now;

				XmlElement elem = xmldoc.CreateElement("date");
				elem.InnerText = now.ToShortDateString();
				//Add the node to the document.
				record_elem.AppendChild(elem);

				elem = xmldoc.CreateElement("time");
				elem.InnerText = now.ToShortTimeString();
				record_elem.AppendChild(elem);
				/*
								elem = xmldoc.CreateElement("total_pounds");
								elem.InnerText = MarketUpdateLibrary.TotalPoundsSpotFloor();
								record_elem.AppendChild(elem);

								elem = xmldoc.CreateElement("snapshot");
								elem.InnerText = MarketUpdateLibrary.HTMLSnapshotSpotFloor();
								record_elem.AppendChild(elem);
				*/

				//Add the node to the document.
				record_elem.AppendChild(elem);

				elem = xmldoc.CreateElement("unpublished");
				elem.InnerText = "true";
				record_elem.AppendChild(elem);


				//tag for feedstock cost group <common>
				XmlElement common_element = xmldoc.CreateElement("common");
			
				//writing up|down|N/A to the XML
				elem = xmldoc.CreateElement("natural_gas");
				elem.InnerText = "NotApplicable";
				common_element.AppendChild(elem);

				elem = xmldoc.CreateElement("oil");
				elem.InnerText = "NotApplicable";
				common_element.AppendChild(elem);

				elem = xmldoc.CreateElement("ethylene");
				elem.InnerText = "NotApplicable";
				common_element.AppendChild(elem);

				elem = xmldoc.CreateElement("propylene");
				elem.InnerText = "NotApplicable";
				common_element.AppendChild(elem);

				//writing comments
				elem = xmldoc.CreateElement("comments");
				elem.InnerText = "";
				common_element.AppendChild(elem);
			
				record_elem.AppendChild(common_element);

				//writing polyethylene, polypropylene, polystyrene articles
				elem = xmldoc.CreateElement("polyethylene_article");
				elem.InnerText = "";
				record_elem.AppendChild(elem);

				elem = xmldoc.CreateElement("polypropylene_article");
				elem.InnerText = "";
				record_elem.AppendChild(elem);

				elem = xmldoc.CreateElement("polystyrene_article");
				elem.InnerText = "";
				record_elem.AppendChild(elem);
				xmldoc.LastChild.AppendChild(record_elem);
				
				return_index = xmldoc.GetElementsByTagName("MU_record").Count - 1;
				XmlTextWriter writer = new XmlTextWriter(strXmlFileName.ToString(), System.Text.Encoding.UTF8);
				xmldoc.Save(writer);
				writer.Close();
				
			}
			return return_index;
		}

		static string strImage;
		//would be better if we can declare this function as static and place in Library
		static public string Generate_Content(string strPolyethylene, string strPolypropylene, string strPolystyrene, string[] chart_paths, string strComments, string totalPounds, string snapshot, string issue_date, string strTitle, string txtPreTextPE, string txtPreTextPP, string txtPreTextPS, string txtVolumePE, string txtVolumePP, string txtVolumePS, string txtPricePE, string txtPricePP, string txtPricePS,  HttpContext webContext)
		{
			
			string replaceUrl = "<a href=\"http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "\"><img src=\"http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/Market_Update/MUGNV_CRUDE" 
				+ DateTime.Now.Month.ToString() +"_"+ DateTime.Now.Year.ToString() 
				+ ".gif\" align=\"right\" border=\"0\" hspace=\"5\" vspace=\"5\"></a>";

			strPolyethylene = strPolyethylene.Replace("<NG/OIL>", replaceUrl);
			strPolypropylene = strPolypropylene.Replace("<NG/OIL>", replaceUrl);
			strPolystyrene = strPolystyrene.Replace("<NG/OIL>", replaceUrl);
			strComments = strComments.Replace("<NG/OIL>", replaceUrl);

			// BEGIN CONTENT 
			StringBuilder sbHTML = new StringBuilder();
		
			// define current time object 
			DateTime moment = DateTime.Now;
			// output date
			//sbHTML.Append("<table border=0 cellspacing=0 cellpadding=0 width=100% ><tr><td align=left width=50><img src=https://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/Market_Update/tpelogo.gif></td><td align=left><font face='arial black' size=5 color=Black>The</font><font face='arial black' size=5 color=red>Plastics</font><font face='arial black' size=5 color=Black>Exchange</font><font face='arial black' size=5 color=red>.</font><font face='arial black' size=5 color=Black>com</font></td></tr></table>");																		
			sbHTML.Append("<table border='0'><tr><td align=center>");
			
			sbHTML.Append("<table border='0'><tr><td align=center width=100%>");

			sbHTML.Append("<font size=5>" + strTitle + "</font><BR>");
			//			sbHTML.Append("<font size=4>" + moment.ToShortDateString()+"</font>");	
			sbHTML.Append("<font size=4>" + issue_date + "</font>");	
			//sbHTML.Append("<br><br>");
			
			
			string logoFilename = "/pics/Market_Update/logo_" + Convert.ToDateTime(issue_date).Month + "_" + Convert.ToDateTime(issue_date).Day + "_" + Convert.ToDateTime(issue_date).Year + ".gif";
			if(System.IO.File.Exists(webContext.Server.MapPath(logoFilename)))
			{
				sbHTML.Append("</td><td aligh=left><img align=right border=\"0\" src=\"http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/Market_Update/logo_" + Convert.ToDateTime(issue_date).Month + "_" + Convert.ToDateTime(issue_date).Day + "_" + Convert.ToDateTime(issue_date).Year + ".gif" + "\"></img></TD>");
			}

			sbHTML.Append("</td></tr></table></td></tr>");

			//			sbHTML.Append("<a href='https://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Market_Update/pdf/44.pdf'><img border='0'  src='https://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/printer.jpg'>Print friendly version</a>");
			// create string that hold the dynamic file name for the images
			strImage = moment.Month.ToString() +"_"+ moment.Year.ToString();
			//strImage ="2_2005";
			
			//			sbHTML.Append("<tr><td><BLOCKQUOTE><P align='justify'><P align='justify'><b><u>Marketplace at a glance</u></b></P></BLOCKQUOTE></td></tr>");
			//			sbHTML.Append("<tr><td><BLOCKQUOTE><TABLE cellpadding='0' cellSpacing='0'>");
			//				sbHTML.Append("<TR>");
			//					sbHTML.Append("<TD valign='top' width='50%'>");
			//					sbHTML.Append(@"
			//						<TABLE cellpadding='0' cellSpacing='0'>
			//							 <TR><TD><font color='#0000FF'><b>US Monomer Prices</b></font></TD><TD align='center'><font color='#0000FF'><b>Spot</b></font></TD><TD align='center'><font color='#0000FF'><b>&nbsp;July Contract&nbsp;</b></font></TD></TR>
			//							 <TR><TD>Ethylene</TD><TD align='center'>$.35-36<IMG SRC='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/icons/icon_up_arrow.gif' border='0'></TD><TD align='center'>$.38 + .02</TD></TR>
			//							 <TR><TD>Propylene</TD><TD align='center'>$.35-36<IMG SRC='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/icons/icon_up_arrow.gif' border='0'></TD><TD align='center'>$.38 + .03</TD></TR>
			//							 <TR><TD>Styrene</TD><TD align='center'>$.56-60<IMG SRC='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/icons/icon_up_arrow.gif' border='0'></TD><TD align='center'>$.55 + .03</TD></TR>
			//						</TABLE>");
			//					sbHTML.Append("</TD>");
			//					sbHTML.Append("<TD>&nbsp;&nbsp;&nbsp;&nbsp;</TD>");
			//					sbHTML.Append("<TD valign='top' width='50%'>");
			//					sbHTML.Append(@"
			//						<TABLE cellpadding='0' cellSpacing='0'>
			//							 <TR><TD><font color='#0000FF'><b>Future Prices</b></font></TD><TD align='center'><font color='#0000FF'><b>&nbsp;&nbsp;&nbsp;Sept '05&nbsp;&nbsp;&nbsp;</b></font></TD><TD align='center'><font color='#0000FF'><b>&nbsp;Feb '06</b></font></TD></TR>
			//							 <TR><TD>Crude Oil</TD><TD align='center'>$60.53</TD><TD align='center'>$63.44</TD></TR>
			//							 <TR><TD>Natural Gas</TD><TD align='center'>$7.88</TD><TD align='center'>$9.30</TD></TR>
			//							 <TR><TD>LLDPE-Butene</TD><TD align='center'>$1070</TD><TD align='center'>$1095</TD></TR>
			//							 <TR><TD>HoPP barefoot</TD><TD align='center'>$1100</TD><TD align='center'>$1100</TD></TR>
			//						</TABLE>");
			//					sbHTML.Append("</TD>");
			//				sbHTML.Append("</TR>");
			//			sbHTML.Append("</TABLE></BLOCKQUOTE></td></tr>");
				
			sbHTML.Append("<tr><td><BLOCKQUOTE><TABLE cellpadding='0' cellSpacing='1'>");
			//			sbHTML.Append("<TR>");
			//			sbHTML.Append("<TD colspan='2'></TD>");
			//			sbHTML.Append("</TR>");
			sbHTML.Append("<BR><BR>");


			sbHTML.Append("<TR>");
			sbHTML.Append("<TD width='45%' valign='top' >");
			sbHTML.Append("<b><u>Spot Floor Summary</u> <u><font size=\"2\" color='#FF0000'>(" + totalPounds + ")</font></b></u>" + "<BR>" +snapshot + "<BR>");
			sbHTML.Append("<p>All transactions are for actual delivery; they are cleared through The Plastics Exchange and are <font color='red'>totally anonymous.</p></font><BR><BR>");
			sbHTML.Append("<p>All offers are subject to prior sale and credit approval.</p>");
					

			sbHTML.Append("</TD>");
			sbHTML.Append("<TD width='3%'>&nbsp;&nbsp;&nbsp;</TD>");
			sbHTML.Append("<TD valign='top' width='52%'>");
			
			sbHTML.Append("<P>" + makeHTML(strComments) + "</P><BR>");

			//sbHTML.Append("This accompanying table summarizes current spot offerings of good offgrade and generic prime railcars by major producers, distributors and traders. <BR><BR>");
			//sbHTML.Append("Most lots are offered for 1 week, some just a day. Upon simple registration, you can click on the resin title to drill down to the specific offers for that grade.<BR><BR>");
			//sbHTML.Append("Secure it online or if you do not see what you are looking for give our trading desk a call. No worries and no pressure, we are very passive and enjoy talking about the resin market to savvy buyers and sellers. <BR><BR>");
			//sbHTML.Append("We really do not work with branded prime or single truckloads � so if those are your needs, enjoy our market commentary but keep buying from your distributors.<BR><BR>");
			//sbHTML.Append("This table summarizes our current spot offerings of good offgrade and generic prime railcars by  major producers, distributors and traders.");
			//sbHTML.Append("<BR><BR>Most lots are offered for 1 week, some just a day. <font color='#FF0000'>All transactions are cleared through The Plastics Exchange and are totally anonymous.</font> All purchases are for actual delivery and are <font color='#FF0000'>subject to prior sale and credit approval.</font>");
			//sbHTML.Append("<BR><BR>You can click on the resin to drill down to the specific offers for that grade.");
			sbHTML.Append("</TD>");
			sbHTML.Append("</TR>");
			sbHTML.Append("</TABLE></BLOCKQUOTE></td></tr>");

			//sbHTML.Append("<tr><td><BLOCKQUOTE><P align='justify'><b><u>Polyethylene</u></b><br><a href=https://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "><img src=https://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/Market_Update/MUPE"+strImage+".jpg align='right' vspace='5' hspace='5' border=0></a>");
			sbHTML.Append(ParseParagraph("Polyethylene", strPolyethylene, chart_paths[0], chart_paths[1], txtPreTextPE, txtVolumePE, txtPricePE, webContext));
			sbHTML.Append("</P></BLOCKQUOTE></td></tr>");

			//sbHTML.Append("<tr><td><BLOCKQUOTE><P align='justify'><b><u>Polypropylene</u></b><br><a href=https://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "><img src=https://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/Market_Update/MUPP"+strImage+".jpg  align='right' vspace='5' hspace='5' border=0></a>");
			//sbHTML.Append("  ");
			sbHTML.Append(ParseParagraph("Polypropylene", strPolypropylene, chart_paths[2], chart_paths[3], txtPreTextPP, txtVolumePP, txtPricePP, webContext));
			sbHTML.Append("</P></BLOCKQUOTE></td></tr>");

			//sbHTML.Append("<tr><td><BLOCKQUOTE><P align='justify'><b><u>Polystyrene</u></b><br><a href=https://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "><img src=https://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/Market_Update/MUPS"+strImage+".jpg align='right' vspace='5' hspace='5' border=0></a>");
			//sbHTML.Append("  ");
			sbHTML.Append(ParseParagraph("Polystyrene", strPolystyrene, chart_paths[4], chart_paths[5], txtPreTextPS, txtVolumePS, txtPricePS, webContext));
				
			sbHTML.Append("<br><br>Michael Greenberg, CEO<br>The Plastics Exchange<br> " + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + "<br><hr></P></BLOCKQUOTE></td></tr>");
			sbHTML.Append("<tr><td><BLOCKQUOTE><P align='justify'><font face='arial' size=1>Check out The Plastics Exchange before your buy or sell!  We have live markets and prices on prime and widespec commodity grade resin in truckloads and railcars, and quality and delivery are guaranteed by our fully integrated credit and logistics - click </font><a href=http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/public/registration.aspx><font face='arial' color=red size=1>here</font></a><font face='arial' size=1> to register.  We also have access to a wide range of wide-spec resin as well as foreign prime resin for international trade.<br>Call us at " + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + " or send an </font><a href=mailto:info@theplasticsexchange.com><font face='arial' color=red size=1>email</font></a><font face='arial' size=1> and we'll source the resin for you.  <br>If you are already a member, many thanks for your continued support.  If not, join today!  To make purchases, fax us your credit info at " + ConfigurationSettings.AppSettings["FaxNumber"].ToString()  + " or apply online at </font>");
			sbHTML.Append("<a href=http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "><font face='arial' color=red size=1>" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "</font></a>.</P><hr></font></P><P align='justify'><font face='arial' size=1>Disclaimer: The information and data in this report is gathered from exchange observations as well as interactions with producers, distributors, brokers, and processors. These sources are considered reliable. The accuracy and completeness of this information is not guaranteed. Any decision to purchase or sell as a result of the opinions expressed in this report will be the full responsibility of the person authorizing such transaction. Our market updates are compiled with integrity and we hope that you find them of value.<br><br>Chart values reflect our asking prices of generic prime railcars delivered USA.</font><br></P></BLOCKQUOTE></td></tr></table>");
			// END CONTENT
			return sbHTML.ToString();
		}

		//would be better if we can declare this function as static and place in Library
		static public string TotalPoundsSpotFloor(HttpContext webContext)
		{
			SqlConnection conn;
			//			conn = new SqlConnection("Server=localhost;UID=sa;PWD=austerlitz;database=Production");
			conn = new SqlConnection(webContext.Application["DBconn"].ToString());
			conn.Open();
			
			long total = 0;
			SqlCommand cmdTotal = new SqlCommand("SELECT sum(OFFR_QTY*OFFR_SIZE) FROM bboffer where offr_port is null and OFFR_SIZE <> '38500' and OFFR_SIZE <> '44500'",conn);
			SqlDataReader dtrTotal = cmdTotal.ExecuteReader();
			if (dtrTotal.Read()) total = System.Convert.ToInt32(dtrTotal[0]);
			dtrTotal.Close();
			conn.Close();
			
			return String.Format("{0:n0}",total) + " lbs";
		}

		//would be better if we can declare this function as static and place in Library
		static public string HTMLSnapshotSpotFloor(HttpContext webContext)
		{
			string htmlSnapshot="<font size='1'><TABLE id='Table0' cellSpacing='0' width='100%' cellPadding='3' bgColor='#f5f5f5' border='1'>";
			htmlSnapshot+="<TR>";
			htmlSnapshot+="<TD>";
			htmlSnapshot+="<font size='2'><b>Resin</b></font>";
			htmlSnapshot+="</TD>";
			htmlSnapshot+="<TD>";
			htmlSnapshot+="<font size='2'><b>Total lbs</b></font>";
			htmlSnapshot+="</TD>";
			htmlSnapshot+="<TD>";
			htmlSnapshot+="<font size='2'><b>Low</b></font>";
			htmlSnapshot+="</TD>";
			htmlSnapshot+="<TD>";
			htmlSnapshot+="<font size='2'><b>High</b></font>";
			htmlSnapshot+="</TD>";
			htmlSnapshot+="</TR>";

			string sql ="SELECT GRADE=(SELECT GRADE_NAME from GRADE WHERE GRADE_ID = OFFR_GRADE), offr_grade, Min=Cast(min(offr_prce) AS money), Max=Cast(max(offr_prce) AS money), VARSIZE=sum(OFFR_QTY*OFFR_SIZE) ";
			sql+="FROM bboffer where offr_grade is not null and offr_port is null and OFFR_SIZE <> '38500' and OFFR_SIZE <> '44500' group by offr_grade ";
			sql+="ORDER BY VARSIZE DESC";

			SqlConnection conn;
			//			conn = new SqlConnection("Server=localhost;UID=sa;PWD=austerlitz;database=Production");
			conn = new SqlConnection(webContext.Application["DBconn"].ToString());
			conn.Open();
			
			SqlCommand cmdSpotFloor = new SqlCommand(sql,conn);
			SqlDataReader dtrSpotFloor = cmdSpotFloor.ExecuteReader();
			while(dtrSpotFloor.Read())
			{
				htmlSnapshot+="<TR>";
				htmlSnapshot+="<TD><font size='2'><b>";
				htmlSnapshot+="<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Spot/Spot_Floor.aspx?Filter=" + dtrSpotFloor["offr_grade"].ToString() + "'>" + dtrSpotFloor["GRADE"].ToString() + "</a>";
				htmlSnapshot+="</b></font></TD>";
				htmlSnapshot+="<TD><font size='2'>";
				htmlSnapshot+=String.Format("{0:n0}",dtrSpotFloor["VARSIZE"]);
				htmlSnapshot+="</font></TD>";
				htmlSnapshot+="<TD><font size='2'><b>";
				htmlSnapshot+= String.Format("{0:c3}", dtrSpotFloor["Min"]);
				htmlSnapshot+="</b></font></TD>";
				htmlSnapshot+="<TD><font size='2'><b>";
				htmlSnapshot+=String.Format("{0:c3}", dtrSpotFloor["Max"]);
				htmlSnapshot+="</b></font></TD>";
				htmlSnapshot+="</TR>";
			}
			dtrSpotFloor.Close();
			conn.Close();

			htmlSnapshot+="</TABLE></font>";

			return htmlSnapshot;
		}

		private static bool isEmpty(string strValue)
		{
			return ( (strValue == null) || strValue.Trim().Equals("") );

		}
		//would be better if we can declare this function as static and place in Library
		static public string ParseParagraph(string strParagraph, string strMainText, string path1, string path2, string strPreText, string strVolume, string strPrice, HttpContext webContext)
		{
			string strText = "";
			string[] arrParagraph;  
			Regex r = new Regex(((char)13).ToString()); // Split on CR's.
			// compiler forces the array to be used outside of the switch statement
			// the following two lines will be overwritten but it is included to satisfy the compiler requirement
			arrParagraph = r.Split(strMainText); 
			strText ="";

			string strBeforeCharts = "";
			string strAfterChart = "";
			
			arrParagraph = r.Split(strMainText);

			// loops through the paragraphs now in the array and adds html to them
			strText +="<tr><td><BLOCKQUOTE><P align='justify'><P align='justify'><b><u><a name='#" + strParagraph +  "'>" +strParagraph+"</a></u></b><br><BR>";

			if( !isEmpty(strVolume))
			{
				strBeforeCharts += "<B>Volume: </b>" + strVolume + "<BR>";
			}
			if( !isEmpty(strVolume))
			{
				strBeforeCharts += "<B>Price: </b>" + strPrice + "<BR><BR>";
			}
			if( !isEmpty(strPreText) )
			{
				strBeforeCharts +=  strPreText + "<BR>";
				for (int k = 0; k < arrParagraph.Length; k++)
				{
					strAfterChart += arrParagraph[k] + "<BR>";
				}
			}
			else
			{
				strBeforeCharts += arrParagraph[0] + "<BR>";
				for (int k = 1; k < arrParagraph.Length; k++)
				{
					strAfterChart += arrParagraph[k] + "<BR>";
				}
			}
			
			strText += strBeforeCharts;





			strText += "<BR><a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "'><img src='" + path1 + "' border=0></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "'><img src='" + path2 + "' border=0></a><BR>";

			strText += "<BR>" + strAfterChart;

			//strText +="</P></BLOCKQUOTE></td></tr>";
			// returns the properly formatted paragraph
			strText = strText.Replace("<IMAGE ONE>","<a href=http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "><img src='" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/Market_Update/MUGNV_CRUDE9_2005.gif' align='right' vspace='5' hspace='5' border=0></a>");
			return strText;
		}

		private static string makeHTML(string strText)
		{
			return strText.Replace("\r\n", "<BR>");
		}

	}
}
