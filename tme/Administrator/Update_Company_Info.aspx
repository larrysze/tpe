
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page Language="c#" CodeBehind="Update_Company_Info.aspx.cs" AutoEventWireup="false" Inherits="localhost.Administrator.Update_Company_Info" %>

<form runat="server" id="Form">
	<TPE:Template PageTitle="" Runat="Server" ID="Template1" NAME="Template1" />
	<script language="JavaScript">
    <!--
    function Address_PopUp(ID)
    // This function opens up a pop up window and passes the appropiate ID number for the window
    {


    if (document.all)
        var xMax = screen.width, yMax = screen.height;
    else
        if (document.layers)
            var xMax = window.outerWidth, yMax = window.outerHeight;
        else
            var xMax = 640, yMax=480;

    var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
    var str = "../administrator/Location_Details.aspx?ID=" + ID ;
    window.open(str,'Address','location=true,toolbar=true,width=275,height=370,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');

    }
    function Person_PopUp(ID)

    {


    if (document.all)
        var xMax = screen.width, yMax = screen.height;
    else
        if (document.layers)
            var xMax = window.outerWidth, yMax = window.outerHeight;
        else
            var xMax = 740, yMax=580;

    var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
    var str = "../administrator/Person_Details.aspx?ID=" + ID;
    window.open(str,'Address','location=true,toolbar=true,width=275,height=400,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');

    }

    //-->
	</script>
	<!-- Insert content here -->
	<asp:Panel id="pnlStart" Visible="True" Runat="Server">
		<CENTER><STRONG>
				<H3>
					<asp:Label id="lblcompanyname2" runat="server"></asp:Label></H3>
			</STRONG>
			<BR>
			<asp:Label id="lbltest" runat="server"></asp:Label></CENTER>
		<TABLE align="center" border="0">
			<TR>
				<TD vAlign="top">
					<FIELDSET>
						<TABLE width="250" border="0">
							<TR>
								<TD align="left"><STRONG>Company Info</STRONG></TD>
								<TD align="right">
									<asp:Button class="tpebutton" id="Button1" onclick="Update_Company" runat="server" Text="Update"></asp:Button></TD>
							</TR>
							<TR>
								<TD colSpan="2">
									<P>Name:
										<asp:Label id="lblcompanyname" runat="server"></asp:Label>
										<BR>
										<asp:Label id="lblcompanyURL" runat="server"></asp:Label><BR>
										Credit Limit:
										<asp:Label id="lblCreditLimit" runat="server"></asp:Label><BR>
										Credit Available:
										<asp:Label id="lblCreditAvail" runat="server"></asp:Label><BR>
										<asp:Label id="lblOrigCreditLimit" runat="server" visible="false"></asp:Label></P>
								</TD>
							</TR>
						</TABLE>
					</FIELDSET>
				</TD>
				<TD vAlign="top">
					<FIELDSET>
						<TABLE width="250" border="0">
							<TR>
								<TD align="left" colSpan="2"><STRONG>User</STRONG></TD>
								<TD align="right">
									<asp:Button class="tpebutton" id="Button14" onclick="Add_User" runat="server" Text="Add New"></asp:Button></TD>
							</TR>
							<TR>
								<TD colSpan="2">
									<P>
										<TABLE width="100%" border="0">
											<asp:PlaceHolder id="phOfficer" runat="server"></asp:PlaceHolder>
											<asp:PlaceHolder id="phPurchaser" runat="server"></asp:PlaceHolder>
											<asp:PlaceHolder id="phSupplier" runat="server"></asp:PlaceHolder></TABLE>
									</P>
								</TD>
							</TR>
						</TABLE>
					</FIELDSET>
				</TD>
			</TR>
			<TR>
				<TD vAlign="top">
					<FIELDSET>
						<TABLE width="250" border="0">
							<TR>
								<TD align="left"><STRONG>Bank</STRONG></TD>
								<TD align="right">
									<asp:Button class="tpebutton" id="Button3" onclick="Update_Bank" runat="server" Text="Update"></asp:Button></TD>
							</TR>
							<TR>
								<TD colSpan="2">
									<asp:Label id="lblbankname" runat="server"></asp:Label><BR>
									Contacts:
									<asp:Label id="lblbankpers" runat="server"></asp:Label><BR>
									Phone:
									<asp:Label id="lblbankphon" runat="server"></asp:Label><BR>
									Fax:
									<asp:Label id="lblbankfax" runat="server"></asp:Label><BR>
									Account Number:
									<asp:Label id="lblbankacnt" runat="server"></asp:Label><BR>
									Address:
									<asp:Label id="lblbankaddr1" runat="server"></asp:Label>
									<asp:Label id="lblbankaddr2" runat="server"></asp:Label><BR>
									<asp:Label id="lblbankcity" runat="server"></asp:Label>,&nbsp;
									<asp:Label id="lblbankzip" runat="server"></asp:Label>&nbsp;
									<asp:Label id="lblbankstat" runat="server"></asp:Label>
								</TD>
							</TR>
						</TABLE>
					</FIELDSET>
				</TD>
				<TD vAlign="top">
					<FIELDSET>
						<TABLE width="250" border="0">
							<TR>
								<TD align="left"><STRONG>Locations</STRONG></TD>
								<TD align="right">
									<asp:Button class="tpebutton" id="Button4" onclick="Add_Location" runat="server" Text="Add New"></asp:Button></TD>
							</TR>
							<TR>
								<TD colSpan="2">
									<P>
										<TABLE width="100%" border="0">
											<asp:PlaceHolder id="phHeadquarters" runat="server"></asp:PlaceHolder>
											<asp:PlaceHolder id="phShippingPoints" runat="server"></asp:PlaceHolder></TABLE>
									</P>
								</TD>
							</TR>
						</TABLE>
					</FIELDSET>
				</TD>
				<TD></TD>
			</TR>
		</TABLE>
		<TABLE>
		</TABLE>
	</asp:Panel>
	<asp:Panel id="pnlCompany" Visible="False" Runat="Server">
		<TABLE width="50%" align="center">
			<TR>
				<TD>Company Name:</TD>
				<TD>
					<asp:TextBox id="txtCompanyName" runat="server" maxlength="40"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD>Website:
				</TD>
				<TD>
					<asp:TextBox id="txtCompanyURL" runat="server" maxlength="50"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD>Credit Limit:
				</TD>
				<TD>
					<asp:TextBox id="txtCreditLimit" runat="server" maxlength="10"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD align="center" colSpan="2">
					<asp:Button class="tpebutton" id="Button5" onclick="Save_Company_Info" runat="server" Text="Save"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:Button class="tpebutton" id="Button2" onclick="CompanyInfoBack" runat="server" NAME="Button2"
						Text="Cancel"></asp:Button>
				</TD>
			</TR>
		</TABLE>
	</asp:Panel>
	<asp:Panel id="pnlBank" Visible="False" Runat="Server"> <!-- GameKey: 284 -->
		<TABLE cellSpacing="0" cellPadding="0" width="305" align="center" border="0">
			<TR>
				<TD>Bank Name:</TD>
				<TD>
					<asp:TextBox id="txtbankname" runat="server"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD>Contact:</TD>
				<TD>
					<asp:TextBox id="txtbankpers" runat="server"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD>Phone:</TD>
				<TD>
					<asp:TextBox id="txtbankphon" runat="server"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD>Fax:</TD>
				<TD>
					<asp:TextBox id="txtbankfax" runat="server"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD vAlign="top">Address:</TD>
				<TD>
					<asp:TextBox id="txtbankaddr1" runat="server"></asp:TextBox><BR>
					<asp:TextBox id="txtbankaddr2" runat="server"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD>City:</TD>
				<TD>
					<asp:TextBox id="txtbankcity" runat="server"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD>State:</TD>
				<TD>
					<asp:TextBox id="txtbankstat" runat="server"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD>Zip:</TD>
				<TD>
					<asp:TextBox id="txtbankzip" runat="server"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD>Account Number:</TD>
				<TD>
					<asp:TextBox id="txtbankacnt" runat="server"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD>Repetitive Number:</TD>
				<TD>
					<asp:TextBox id="txtbankrep" runat="server"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD>ABA Number:</TD>
				<TD>
					<asp:TextBox id="txtbankaba" runat="server"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD align="center" colSpan="2">
					<asp:Button class="tpebutton" id="Button7" onclick="Save_Bank" runat="server" Text="Save"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:Button class="tpebutton" id="Button8" onclick="BankBack" runat="server" Text="Cancel"></asp:Button>
				</TD>
			</TR>
		</TABLE>
	</asp:Panel>
	<asp:Panel id="pnlLocations" Visible="False" Runat="Server"></asp:Panel>
	<TPE:Template Footer="true" Runat="Server" ID="Template2" NAME="Template2" />
</form>
