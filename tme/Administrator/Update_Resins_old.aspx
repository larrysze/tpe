<%@ Page Language="VB" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Web.Mail" %>
<form runat="server" id="Form">
    <TPE:Template PageTitle="" Runat="Server" />

<%

'Database connection'
 Dim conn as SqlConnection
 Dim cmdContent as SQLCommand
 Dim Rec0 as SqlDataReader
 conn = new SqlConnection(Application("DBconn"))
 conn.Open()
 Dim connPref as SqlConnection
 Dim cmdContentPref as SQLCommand
 Dim RecPref as SqlDataReader
 connPref = new SqlConnection(Application("DBconn"))
 connPref.Open()


'Get person ID from form or email's querysring
'From querystring: it's administrator
'From request.form: it's demo user himself

dim strID
If Request.Form("Change")<>"" Then
strID = Request.Form("Change")
Else
strID = request.Querystring("ID")
End If


Dim Id,Str
IF Request.Form("Submit")<>"" THEN



        'use POWER method to calculate a number to represent preference.
        dim total
        total=0
        dim temp
	if Request.Form("pref0") = "0" THEN
	total = total + 2^0
	temp=0
	end if
	if Request.Form("pref1") = "1" THEN
	total = total + 2^1
	temp=1
	end if
	if Request.Form("pref2") = "2" THEN
	total = total + 2^2
	temp=2
	end if
	if Request.Form("pref3") = "3" THEN
	total = total + 2^3
	temp=3
	end if
	if Request.Form("pref4") = "4" THEN
	total = total + 2^4
	temp=4
	end if
	if Request.Form("pref5") = "5" THEN
	total = total + 2^5
	temp=5
	end if
	if Request.Form("pref6") = "6" THEN
	total = total + 2^6
	temp=6
	end if
	if Request.Form("pref7") = "7" THEN
	total = total + 2^7
	temp=7
	end if
	if Request.Form("pref8") = "8" THEN
	total = total + 2^8
	temp=8
	end if
	if Request.Form("pref9") = "9" THEN
	total = total + 2^9
	temp=9
	end if
	if Request.Form("pref10") = "10" THEN
	total = total + 2^10
	temp=10
	end if
	if Request.Form("pref11") = "11" THEN
	total = total + 2^11
	temp=11
	end if
	if Request.Form("pref12") = "12" THEN
	total = total + 2^12
	temp=12
        end if
	if Request.Form("pref13") = "13" THEN
	total = total + 2^13
	temp=13
	end if
	if Request.Form("pref14") = "14" THEN
	total = total + 2^14
	temp=14
	end if
	if Request.Form("pref15") = "15" THEN
	total = total + 2^15
	temp=15
	end if
	if Request.Form("pref16") = "16" THEN
	total = total + 2^16
	temp=16
	end if
	if Request.Form("pref17") = "17" THEN
	total = total + 2^17
	temp=17
	end if
	if Request.Form("pref18") = "18" THEN
	total = total + 2^18
	temp=18
	end if
	if Request.Form("pref19") = "19" THEN
	total = total + 2^19
	temp=19
	end if
	if Request.Form("pref20") = "20" THEN
	total = total + 2^20
	temp=20
	end if
	if Request.Form("pref21") = "21" THEN
	total = total + 2^21
	temp=21
	end if
	if Request.Form("pref22") = "22" THEN
	total = total + 2^22
	temp=22
	end if
	if Request.Form("pref23") = "23" THEN
	total = total + 2^23
	temp=23
	end if
	if Request.Form("pref24") = "24" THEN
	total = total + 2^24
	temp=24
	end if
	if Request.Form("pref25") = "25" THEN
	total = total + 2^25
	temp=25
	end if
	if Request.Form("pref26") = "26" THEN
	total = total + 2^26
	temp=26
	end if
	if Request.Form("pref27") = "27" THEN
	total = total + 2^27
        temp=27
	end if
	

    cmdContent= new SqlCommand("UPDATE PERSON Set PERS_PREF='"+total.ToString()+"' WHERE PERS_ID='"+strID+"'", conn)
	cmdContent.ExecuteNonQuery()



	'administrator will be redirected to "Demo-singup" page after update
	IF Session("Typ")="A" Or Session("Typ")="B" Then
		response.redirect ("/Administrator/Contact_Details.aspx?ID="+Request.QueryString("ID"))

	Else
		'Demo user will be redirected to "thank you" page after update
		response.redirect ("/Administrator/Update_Pref_Thank_You.aspx")
	End IF

END IF

%>


<script>
function valid() {
document.Form.Submit.value='submit';document.Form.submit()
}
</script>

<%
'Query all person info from PERSON table under DEMO database
Str="SELECT PERS_PREF FROM PERSON WHERE PERS_ID='"+strID+"'"
cmdContent= new SqlCommand(Str, conn)
Rec0= cmdContent.ExecuteReader()
Rec0.Read
DIM Pref ' The users preference
Pref = Rec0("PERS_PREF")

'RETURN THE NUMBER IN BINARY BUT BEGIN WITH THE LAST
Dim bin(28)
Dim i
FOR i = 0 TO 27
	'PERS_PREF&POWER(2,' +@resin_filter+')<>0
  	IF (Pref AND 2^i ) THEN
	 bin(i) = 1
	ElSE
	 bin(i) = 0
	END IF
	' bin(i) = (Pref AND 2^i = 2^i)
NEXT

%>


<SCRIPT language=JavaScript>
    <!-- Hide script

    function AllInjection()
    {
    document.Form.pref1.checked = true;
    document.Form.pref3.checked = true;
    document.Form.pref4.checked = true;
    document.Form.pref9.checked = true;
    document.Form.pref11.checked = true;
    document.Form.pref20.checked = true;
    document.Form.pref21.checked = true;
    document.Form.pref23.checked = true;
    document.Form.pref26.checked = true;
    document.Form.pref8.checked = true;
    document.Form.pref16.checked = true;
    document.Form.pref18.checked = true;
    document.Form.pref19.checked = true;
    document.Form.pref25.checked = true;


    }
    function AllFilm()
    {
    document.Form.pref0.checked = true;
    document.Form.pref5.checked = true;
    //document.Form.pref6.checked = true;
    document.Form.pref7.checked = true;
    document.Form.pref10.checked = true;
    document.Form.pref13.checked = true;
    }

    // End script hiding -->
</SCRIPT>

<input type=hidden name=Submit value=''>
<input type=hidden name=Change value='<%=Request.Form("Change")%>'>
<center>

<%'display all info for administrator'%>


<%'display resin preferences'%>
<TPE:Web_Box Heading="Please update your resin preferences" Runat="Server" />
	       <table border=0 cellspacing=0 cellpadding=0 width=600 align=center >

		      <tr >
			     <td colspan=2><div align=left>
		              <table border=0 cellspacing=0 cellpadding=0 width=500 align=center>
			             <tr>

				            <td colspan="2">
				                <p align=center>
				                    <table align=center width=100% border=0>
				                        <tr width=100%>
					                       <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					                           <input type="button" class="tpebutton" value="Select All Injection" name=btnAllInjection onclick="AllInjection()">
					                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td colspan=4></td>
					                       <td><Input type="button" class="tpebutton" value="Select All Film" name=btnAllInjection onclick="AllFilm()">
					                       </td>
				                        </tr>
				                     </table>
				             </td>
				        </tr>
				        <tr>

				            <td valign="top">
        	                   <!--Go through every singel contract category, display the contract-->
			                     

			                     <table><td colspan=2>
			                     HDPE</td></tr><tr><td width=10></td><td>
			                     <%
			                     Dim count=0
			                     cmdContentPref= new SqlCommand("SELECT * FROM CONTRACT WHERE CONT_CATG='HDPE'", connPref)
			                     RecPref= cmdContentPref.ExecuteReader()
			                     While RecPref.Read()
			                     count=RecPref("CONT_ID")-1
			                     %><tr><td width=10></td><td>
			                     <input type="checkbox" name=pref<%=RecPref("CONT_ID")-1%> value="<%=RecPref("CONT_ID")-1%>" <% if bin(count) THEN Response.Write("Checked")%>>
			                     </input>
			                     <%=RecPref("CONT_LABL")%></td></tr>
			                     <%
			                     End While
			                     RecPref.Close()
			                     %></table>

			                     <table><td colspan=2>
			                     LDPE</td></tr><tr><td width=10></td><td>
			                     <%
			                     cmdContentPref= new SqlCommand("SELECT * FROM CONTRACT WHERE CONT_CATG ='LDPE' ", connPref)
			                     RecPref= cmdContentPref.ExecuteReader()
			                     While RecPref.Read()
			                     count=RecPref("CONT_ID")-1
			                     %><tr><td width=10></td><td>
			                     <input type="checkbox" name=pref<%=RecPref("CONT_ID")-1%> value="<%=RecPref("CONT_ID")-1%>" <% if bin(count) THEN Response.Write("Checked")%>>
			                     </input>
			                     <%=RecPref("CONT_LABL")%></td></tr>
			                     <%
			                     End While
			                     RecPref.Close()
			                     %></table>

			                     <table><td colspan=2>
			                     LLDPE</td></tr><tr><td width=10></td><td>
			                     <%
			                     cmdContentPref= new SqlCommand("SELECT * FROM CONTRACT WHERE CONT_CATG='LLDPE'", connPref)
			                     RecPref= cmdContentPref.ExecuteReader()
			                     While RecPref.Read()
			                     count=RecPref("CONT_ID")-1
			                     %><tr><td width=10></td><td>
			                     <input type="checkbox" name=pref<%=RecPref("CONT_ID")-1%> value="<%=RecPref("CONT_ID")-1%>" <% if bin(count) THEN Response.Write("Checked")%>>
			                     </input>
			                     <%=RecPref("CONT_LABL")%></td></tr>
			                     <%
			                     End While
			                     RecPref.Close()
			                     %></table>


		                  	</td>
			                 <td valign="top">

			                         <table><td colspan=2>
			                         Homopolymer</td></tr><tr><td width=10></td><td>
			                         <%
			                         cmdContentPref= new SqlCommand("SELECT * FROM CONTRACT WHERE CONT_CATG='HoPP'", connPref)
			                         RecPref= cmdContentPref.ExecuteReader()
			                         While RecPref.Read()
			                         count=RecPref("CONT_ID")-1
			                         %><tr><td width=10></td><td>
			                         <input type="checkbox" name=pref<%=RecPref("CONT_ID")-1%> value="<%=RecPref("CONT_ID")-1%>" <% if bin(count) THEN Response.Write("Checked")%>>
			                         </input>
			                         <%=RecPref("CONT_LABL")%></td></tr>
			                         <%
			                         End While
			                         RecPref.Close()
			                         %></table>

			                         <table><td colspan=2>
			                         Copolymer</td></tr><tr><td width=10></td><td>
			                         <%
			                         cmdContentPref= new SqlCommand("SELECT * FROM CONTRACT WHERE CONT_CATG='CoPP'", connPref)
			                         RecPref= cmdContentPref.ExecuteReader()
			                         While RecPref.Read()
			                         count=RecPref("CONT_ID")-1
			                         %><tr><td width=10></td><td>
			                         <input type="checkbox" name=pref<%=RecPref("CONT_ID")-1%> value="<%=RecPref("CONT_ID")-1%>" <% if bin(count) THEN Response.Write("Checked")%>>
			                         </input>
			                         <%=RecPref("CONT_LABL")%></td></tr>
			                         <%
			                         End While
			                         RecPref.Close()
			                         %></table>

			                         <table><td colspan=2>
			                         GPPS</td></tr><tr><td width=10></td><td>
			                         <%
			                         cmdContentPref= new SqlCommand("SELECT * FROM CONTRACT WHERE CONT_CATG='GPPS'", connPref)
			                         RecPref= cmdContentPref.ExecuteReader()
			                         While RecPref.Read()
			                         count=RecPref("CONT_ID")-1
			                         %><tr><td width=10></td><td>
			                         <input type="checkbox" name=pref<%=RecPref("CONT_ID")-1%> value="<%=RecPref("CONT_ID")-1%>" <% if bin(count) THEN Response.Write("Checked")%>>
			                         </input>
			                         <%=RecPref("CONT_LABL")%></td></tr>
			                         <%
			                         End While
			                         RecPref.Close()
			                         %></table>

			                         <table><td colspan=2>
			                         HIPS</td></tr><tr><td width=10></td><td>
			                         <%
			                         cmdContentPref= new SqlCommand("SELECT * FROM CONTRACT WHERE CONT_CATG='HIPS'", connPref)
			                         RecPref= cmdContentPref.ExecuteReader()
			                         While RecPref.Read()
			                         count=RecPref("CONT_ID")-1
			                         %><tr><td width=10></td><td>
			                         <input type="checkbox" name=pref<%=RecPref("CONT_ID")-1%> value="<%=RecPref("CONT_ID")-1%>" <% if bin(count) THEN Response.Write("Checked")%>>
			                         </input>
			                         <%=RecPref("CONT_LABL")%></td></tr>
			                         <%
			                         End While
			                         RecPref.Close()
			                         %></table>

			                 </td>


		              	</tr>

		          </table>
	           </td>
	       </tr>
        </table>
  <TPE:Web_Box Footer="true" Runat="Server" />

<br>
<center>

    <input type=button class=tpebutton value="Update" onClick="Javascript:valid()">
    <%IF Session("Typ")="A" Or Session("Typ")="B" Then%>
    <input type=button class=tpebutton value="Cancel" onclick="Javascript:history.back()">
    <%End IF%>

</center>




<%
Rec0.Close()
conn.Close()

connPref.Close()
%>
<TPE:Template Footer="true" Runat="Server" />

</form>
