
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tarnellreport.aspx.cs" Inherits="localhost.Administrator.tarnellreport"  MasterPageFile="~/MasterPages/menu.master" Title="Tarnell Report" %>

<%@ MasterType VirtualPath="~/MasterPages/Menu.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphMain">
   <br />
   <asp:Label runat="server" ID="lblDate" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="BtnExport" runat="server" OnClick="BtnExport_Click"   Text="Export to Excel" />
    <br />
   
   <asp:GridView ID="gvReport" Font-Size="Small" runat="server" HeaderStyle-CssClass="Content LinkNormal" RowStyle-CssClass="Content LinkNormal">  
    </asp:GridView>
              
    <br />
</asp:Content>