using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Text;
using System.Web.Mail;
using TPE.Utility;

namespace localhost.Common
{
	/// <summary>
	/// Summary description for Order_Documents.
	/// </summary>
	public partial class Fax_Center : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.HyperLink Hyperlink1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl FileDetails;
		protected System.Web.UI.HtmlControls.HtmlGenericControl FileName;
		protected System.Web.UI.HtmlControls.HtmlGenericControl MyContentType;
		protected System.Web.UI.HtmlControls.HtmlGenericControl ContentLength;
		protected System.Web.UI.WebControls.ListBox ListBox1;
		protected System.Web.UI.WebControls.ListBox ListBox2;
		protected System.Web.UI.WebControls.ListBox Inbox;
		protected System.Web.UI.WebControls.ListBox Employeebox;
		protected System.Web.UI.WebControls.ListBox ddlInbox;
		protected System.Web.UI.WebControls.ListBox ddlEmployeebox;
		protected System.Web.UI.WebControls.HyperLink del;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
//				Session["ID"] = "263";
//				Session["Name"] = "Zach Simmons";
//				Session["Typ"] = "B";

				//must logged in to access this page
                if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "T") && ((string)Session["Typ"] != "L"))
				{
					Response.Redirect("/default.aspx");
				}
				lblPageHeader.Text ="Fax Center";
				Bind();
			}
            Master.Width = "780px";
		}

		public void Bind()
		{   	
			//Binding the Inbox Box List
			BindInboxList();
			
			//Binding the Employee List
			string strSQL1="SELECT PERS_ID, PERS_FRST_NAME FROM PERSON WHERE (PERS_TYPE IN ('B', 'A','T')) AND PERS_ENBL='1' ORDER BY PERS_FRST_NAME";
			DBLibrary.BindDropDownList(Application["DBConn"].ToString(), ddlEmployee, strSQL1, "PERS_FRST_NAME" ,"PERS_ID");
			//Positioning on the current user
			ListItem liFound = ddlEmployee.Items.FindByValue(Session["ID"].ToString());
			liFound.Selected = true;

			//Checking if the user is Admin and show/hide controls
			ShowHideControl((string)Session["Typ"] == "A");

			//Binding the Employee Fax Box List
			BindEmployeeFaxes();

			//Binding Orders to Move Faxes
			BindOrders();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void ShowHideControl(bool adminUser)
		{
			ddlEmployee.Visible = adminUser;
			lblSelectEmployee.Visible = adminUser;
			lblMyFaxes.Visible = !adminUser;
			btnDelete.Visible = adminUser;
		}

		private void BindEmployeeFaxes()
		{
			//Binding the Imbox List
			string strSQL="SELECT CONVERT(VARCHAR, FAX_DATE, 101) + ' ' + LEFT(CONVERT(VARCHAR, FAX_DATE, 114),5) + ' ------ ' + CONVERT(VARCHAR, FAX_NUMBER_PAGES) + ' ------ ' + FAX_NUMBER AS LISTBOX_LINE, FAX_ID " +
						  "FROM FAX " +
						  "WHERE (PERS_ID = " + ddlEmployee.SelectedItem.Value + ") AND (FAX_ATTACHED = 0 OR FAX_ATTACHED IS NULL) and (FAX_ENBL = 1) " +
						  "ORDER BY FAX_DATE";
			DBLibrary.BindListBox(Application["DBConn"].ToString(), lstEmployeebox, strSQL, "LISTBOX_LINE" ,"FAX_ID");
		}

		protected void ddlEmployee_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BindEmployeeFaxes();
		}

		private void sendEmail(string from, string to, string FAX_info)
		{
			MailMessage mail;
			mail = new MailMessage();
			
			string strSQL = "SELECT PERS_FRST_NAME, PERS_MAIL, PERS_LAST_NAME FROM PERSON WHERE PERS_ID=@from";
			Hashtable htParam = new Hashtable();
			htParam.Add("@from",from);
			string fromName = string.Empty;
			string fromEmail = string.Empty;
			string toName = string.Empty;
			string toEmail = string.Empty;
					
					
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
				using (SqlDataReader dtrSQL = DBLibrary.GetDataReaderFromSelect(conn,strSQL,htParam))
				{

					if(dtrSQL.Read())
					{
						fromName = dtrSQL["PERS_FRST_NAME"].ToString();
						fromEmail = dtrSQL["PERS_MAIL"].ToString();
					}

				}

				strSQL = "SELECT PERS_FRST_NAME, PERS_MAIL, PERS_LAST_NAME FROM PERSON WHERE PERS_ID=@to";
				htParam.Clear();
				htParam.Add("@to",to);
				using (SqlDataReader dtrSQL2 = DBLibrary.GetDataReaderFromSelect(conn,strSQL,htParam))
				{
					if(dtrSQL2.Read())
					{
						toName = dtrSQL2["PERS_FRST_NAME"].ToString();
						toEmail = dtrSQL2["PERS_MAIL"].ToString();
					}

					mail.To = toEmail;
			
					mail.From = fromEmail;

					string subject = toName + ", you have a new FAX: " + FAX_info;
			
					mail.Subject = subject;
			
					string body = HelperFunction.getEmailHeaderHTML() +  "<b>" + toName + "</b> you have a new FAX:<BR><BR><b>" + FAX_info + "</b><BR><BR><b>" + fromName + "</b> added this FAX to your INBOX.<BR><BR>";
					body += "<BR>GO TO " + HelperFunction.getEmailLink(dtrSQL2["PERS_MAIL"].ToString(), "INBOX", "/Administrator/Fax_Center.aspx", 100, this.Context) +  HelperFunction.getEmailFooterHTML();

					mail.Body = body;
				}
			}
			
			mail.BodyFormat = MailFormat.Html;
//			System.Web.Mail.SmtpMail.SmtpServer= System.Configuration.ConfigurationSettings.AppSettings["SmtpServer"]; 
			TPE.Utility.EmailLibrary.Send(Context,mail);
		}

		protected void btnAttachFaxUser_Click(object sender, System.EventArgs e)
		{
			//Attaching selected faxes to the selected user
			foreach(ListItem Item in lstInbox.Items)
			{
				if (Item.Selected)
				{
					//Set the Fax to selected User
					AttachFax(Convert.ToInt16(Item.Value),Convert.ToInt16(ddlEmployee.SelectedValue));
					if(Session["ID"].ToString() != ddlEmployee.SelectedValue)
					{
						sendEmail(Session["ID"].ToString(), ddlEmployee.SelectedValue, Item.Text);
					}
				}
			}
			//Binding the Inbox Box List and Employee Fax Box List
			BindInboxList();
			BindEmployeeFaxes();
		}

		protected void btnDetachFaxUser_Click(object sender, System.EventArgs e)
		{
			//Detaching selected fax from the selected user
			if (lstEmployeebox.SelectedValue.ToString()!="")
			{
				//Set the Fax to none
				AttachFax(Convert.ToInt16(lstEmployeebox.SelectedValue),0);

				//Binding the Inbox Box List and Employee Fax Box List
				BindInboxList();
				BindEmployeeFaxes();
			}
		}

		/// <summary>
		/// Attach/Detach a fax from/to the users
		/// Use userID=0 for Detach a FaxID passed
		/// </summary>
		/// <param name="faxID">PK of the table FAX</param>
		/// <param name="userID">PK of the table PERSON</param>
		private void AttachFax(int faxID, int userID)
		{
			string UserID = userID.ToString();
			if (UserID=="0") UserID = "Null";

			string strSQL = "UPDATE FAX SET PERS_ID = @UserID WHERE (FAX_ID = @faxID)";
			Hashtable param = new Hashtable();
			param.Add("@UserID",userID);
			param.Add("@faxID",faxID);
			DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(),strSQL, param);
		}

		private void BindInboxList()
		{
			//Binding the Imbox List
			string strSQL="SELECT CONVERT(VARCHAR, FAX_DATE, 101) + ' ' + LEFT(CONVERT(VARCHAR, FAX_DATE, 114),5) + ' ------ ' + CONVERT(VARCHAR, FAX_NUMBER_PAGES) + ' ------ ' + FAX_NUMBER AS LISTBOX_LINE, FAX_ID " + 
				"FROM FAX WHERE (PERS_ID IS NULL) and (FAX_ENBL = 1) ORDER BY FAX_DATE";
			DBLibrary.BindListBox(Application["DBConn"].ToString(), lstInbox, strSQL, "LISTBOX_LINE" ,"FAX_ID");
		}

		protected void btnDelete_Click(object sender, System.EventArgs e)
		{
			//Attaching selected faxes to the selected user
			foreach(ListItem Item in lstInbox.Items)
			{
				if (Item.Selected)
				{
					//Set the Fax to selected User
					DeleteFax(Convert.ToInt16(Item.Value));
				}
			}
			
			//Binding the Inbox Box List and Employee Fax Box List
			BindInboxList();
			BindEmployeeFaxes();
		}

		private void DeleteFax(int faxID)
		{
			Hashtable param = new Hashtable();
			string strSQL = "UPDATE FAX SET FAX_ENBL = 0 WHERE FAX_ID=@faxID";
			param.Add("@faxID", faxID.ToString());
			DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(),strSQL, param);
		}

		private void DeleteFaxByMoving(int faxID, int ShipmentID)
		{
			Hashtable param = new Hashtable();
			//string strSQL = "UPDATE FAX SET FAX_ENBL = 0, SHIPMENT_ID = " + ShipmentID.ToString() + " WHERE FAX_ID=" + faxID.ToString();
			string strSQL = "UPDATE FAX SET FAX_ENBL = 0, SHIPMENT_ID = @ShipmentID WHERE FAX_ID=@faxID";
			param.Add("@ShipmentID",ShipmentID.ToString());
			param.Add("@faxID",faxID.ToString());
			DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(),strSQL, param);
		}

		protected void cmdMove_Click(object sender, System.EventArgs e)
		{
			MoveProcess();
		}
		
		private void PreviewFax(int faxID, string typeFax)
		{
			Hashtable param = new Hashtable();
			string sql = "SELECT FAX_DATA_FILE FROM FAX WHERE FAX_ID=@faxID";
			param.Add("@faxID",faxID.ToString());
				
			using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				conn.Open();				
				using (SqlDataReader myDataReader = DBLibrary.GetDataReaderFromSelect(conn,sql,param))
				{
	            
					myDataReader.Read();
				
					Response.Clear();
					Response.ContentType = "application/octet-stream";
					//Response.ContentType = "document/doc";
					Response.AddHeader("Content-Length", ((byte [])myDataReader["FAX_DATA_FILE"]).Length.ToString());
					Response.Buffer = false;
					//Response.AppendHeader("Content-Disposition", "attachment;filename=PreviewFax" + typeFax + ".tif");
					//Response.AddHeader("Content-Type", "image/tif");
					Response.AddHeader("Content-Disposition", "attachment;filename=PreviewFax" + typeFax + ".tif");
					//Response.AddHeader("Content-Disposition", "inline;PreviewFax" + typeFax + ".tiff");
					Response.BinaryWrite((byte [])myDataReader["FAX_DATA_FILE"]);
					Response.Flush();
					Response.End();				
				}
			}			
		}

		protected void cmdPreviewFaxInbox_Click(object sender, System.EventArgs e)
		{
			int count = 0;
			foreach(ListItem Item in lstInbox.Items)
			{
				if (Item.Selected) count++;
			}

			if (count==1)
			{
				foreach(ListItem Item in lstInbox.Items)
				{
					if (Item.Selected)
					{
						PreviewFax(Convert.ToInt16(Item.Value),"Inbox");
					}
				}
				lblMessagePreview.Visible = false;
			}
			else
			{
				lblMessagePreview.Visible = true;
			}
		}

		protected void cmdPreviewFaxEmployeeBox_Click(object sender, System.EventArgs e)
		{
			if (lstEmployeebox.SelectedValue!="")
			{
				PreviewFax(Convert.ToInt16(lstEmployeebox.SelectedValue),"Employee");
			}
		}

		private void BindOrders()
		{
			//Binding the Orders from the last 2 months
			string strSQL="SELECT SHIPMENT.SHIPMENT_ID AS SHIP_ID, (CONVERT(VARCHAR,SHIPMENT.SHIPMENT_ORDR_ID) + '-' + CONVERT(VARCHAR,SHIPMENT.SHIPMENT_SKU)) AS ORDER_NBR " +
						  "FROM SHIPMENT INNER JOIN ORDERS ON SHIPMENT.SHIPMENT_ORDR_ID = ORDERS.ORDR_ID " +
						  "WHERE     (ORDERS.ORDR_DATE <= DATEADD(month, 2, GETDATE())) " +
						  "ORDER BY ORDER_NBR DESC";
			DBLibrary.BindDropDownList(Application["DBConn"].ToString(), ddlOrders, strSQL, "ORDER_NBR" ,"SHIP_ID");
		}

		private void MoveFax(int FaxID, int ShipmentID, string faxDescription)
		{
			Hashtable param = new Hashtable();
			string strSQL = "INSERT INTO UPLOADED_FILES (FILE_SHIPMENT, FILE_NAME, FILE_TYPE, FILE_DATA, FILE_LENGTH, FILE_OWNER, FILE_DESCRIPT, FILE_DATE, FILE_BUYER, FILE_SELLER) (SELECT @ShipmentID, FAX_FILENAME, 'application/octet-stream', FAX_DATA_FILE, 0, @ID, @faxDescription, GETDATE(), 0, 0 FROM FAX WHERE (FAX_ID = @FaxID))";
			param.Add("@ShipmentID",ShipmentID.ToString());
			param.Add("@ID",Session["ID"]);
			param.Add("@faxDescription",faxDescription);
			param.Add("@FaxID",FaxID.ToString());

			DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), strSQL, param);
		}

		protected void cmdMoveShowFolder_Click(object sender, System.EventArgs e)
		{
			MoveProcess();
			Response.Redirect("/common/Order_Documents.aspx?ID=" + ddlOrders.SelectedItem.Text);
		}

		private void MoveProcess()
		{
			//Copying the fax(data) to Uploaded files
			if ((lstEmployeebox.SelectedValue!="") && (ddlOrders.SelectedValue!=""))
			{
				MoveFax(Convert.ToInt16(lstEmployeebox.SelectedValue), Convert.ToInt16(ddlOrders.SelectedValue), txtFaxDescription.Text);
				DeleteFaxByMoving(Convert.ToInt16(lstEmployeebox.SelectedValue),Convert.ToInt16(ddlOrders.SelectedValue));
				txtFaxDescription.Text = "";
				BindEmployeeFaxes();
			}
		}

		protected void btnDeleteFaxMyBox_Click(object sender, System.EventArgs e)
		{
			//Attaching selected faxes to the selected user
			foreach(ListItem Item in lstEmployeebox.Items)
			{
				if (Item.Selected)
				{
					//Set the Fax to selected User
					DeleteFax(Convert.ToInt16(Item.Value));
				}
			}
			
			//Binding the Inbox Box List and Employee Fax Box List
			BindInboxList();
			BindEmployeeFaxes();
		}
	}
}