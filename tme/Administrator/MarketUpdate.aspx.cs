using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Administrator
{
    /// <summary>
    /// Summary description for Market Update.
    /// </summary>
    public partial class MarketUpdate : System.Web.UI.Page
    {
        private string strTitle = "Market Update";

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (Session["Typ"].ToString() != "A")
            {
                Response.Redirect("/default.aspx");

            }

            // Put user code to initialize the page here
            if (!Page.IsPostBack)
            {
                loadData("");
            }

            BindSpotSummary();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        protected void btnPreview_Click(object sender, System.EventArgs e)
        {
            string tmp = generateCurrentBody();
            Session["preview"] = tmp;

            Page.RegisterStartupScript("MU_Preview", @"
				<SCRIPT LANGUAGE='Javascript'> 
						window.open('previewForm.aspx', toolbar=false);
				</SCRIPT>		
			");


        }

        private void saveData()
        {
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                conn.Open();
                string sqlString = "UPDATE MARKET_UPDATES SET " +
                    " comments=@comments, polypropylene=@polypropylene, polystyrene=@polystyrene, polyethylene=@polyethylene, " +
                    "	pretext_pp=@pretext_pp, pretext_pe=@pretext_pe, pretext_ps=@pretext_ps " +
                    " WHERE published <> 1";
                Hashtable param = new Hashtable();
                param.Add("@comments", txtComments.Text);
                param.Add("@polypropylene", txtPolypropylene.Text);
                param.Add("@polystyrene", txtPolystyrene.Text);
                param.Add("@polyethylene", txtPolyethylene.Text);

                param.Add("@pretext_ps", txtPreTextPS.Text);
                param.Add("@pretext_pp", txtPreTextPP.Text);
                param.Add("@pretext_pe", txtPreTextPE.Text);


                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlString, param);


            }

        }

        private string generateCurrentBody()
        {
            string[] charts = new string[6];
            DateTime moment = DateTime.Now;

            charts[0] = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Charts/S_Chart_3_1M.png";
            charts[1] = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Charts/S_Chart_3_1Y.png";
            charts[2] = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Charts/S_Chart_26_1M.png";
            charts[3] = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Charts/S_Chart_26_1Y.png";
            charts[4] = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Charts/S_Chart_20_1M.png";
            charts[5] = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Charts/S_Chart_20_1Y.png";
            return GenerateBody(charts, MarketUpdateLibrary.TotalPoundsSpotFloor(this.Context), MarketUpdateLibrary.HTMLSnapshotSpotFloor(this.Context), moment.ToShortDateString(), this.Context);

        }

        private string GenerateBody(string[] chart_paths, string total, string snapshot, string issue_date, HttpContext webContext)
        {
            StringBuilder sbHTML = new StringBuilder();

            sbHTML.Append("<html>");
            sbHTML.Append("<LINK href=\"/styles/TPENewStyles.css\" type=text/css rel=stylesheet>");
            sbHTML.Append("<STYLE type=text/css>");
            sbHTML.Append(".border {	BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; BACKGROUND: #dbdcd7; BORDER-LEFT: #000000 1px solid; WIDTH: 778px; TEXT-ALIGN: center }");
            sbHTML.Append("</STYLE>");

            sbHTML.Append("	<title> :: The Plastics Exchange - Market Update :: </title>");
            sbHTML.Append("	<body>	");
            sbHTML.Append("<table id=\"Table2\" cellSpacing=\"0\" cellPadding=\"0\" width=\"780\"  border=\"0\" align=\"center\">");
            sbHTML.Append("<tr>");
            sbHTML.Append("<td bgcolor=#000000></td>");
            sbHTML.Append("<td>");
            sbHTML.Append("<div class=\"border\" align=\"center\">");

            sbHTML.Append("	<table id=\"table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\"  border=\"0\" >");
            sbHTML.Append("		<tr>		");
            sbHTML.Append("			<td bgcolor=\"black\"  height=\"1\"></td>");
            sbHTML.Append("		</tr>");
            sbHTML.Append("		<tr>");
            sbHTML.Append("		  	<td valign=\"top\">");
            sbHTML.Append("				<!-- BEGIN OF `TPE LOGO` and `GRADIENT BACKGROUND PIC` -->");
            sbHTML.Append("					<table id=\"AutoNumber1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" border=\"0\">");
            sbHTML.Append("						<tr>");
            sbHTML.Append("							<td><img src=\"http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/Header.bmp\"></td>");
            sbHTML.Append("						</tr>");
            sbHTML.Append("					</table>");
            sbHTML.Append("				<!-- END OF `TPE LOGO` and `GRADIENT BACKGROUND PIC` -->");
            sbHTML.Append("			</td>");
            sbHTML.Append("		</tr>");
            sbHTML.Append("		<tr>");
            sbHTML.Append("			<td height='25' vAlign=\"top\" width=\"730\">");
            sbHTML.Append("				<!-- BEGIN OF `MENU BAR` -->");
            sbHTML.Append("				<table height=\"25\" cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" border=\"0\">");
            sbHTML.Append("     					<tr>");
            sbHTML.Append("						<td align=\"left\" width=\"730\" valign='top' height=\"25\">");
            sbHTML.Append("							<!-- BEGIN `NAVIGATION BAR` -->");


            sbHTML.Append("				<!-- Load the Navigation Main Menu Bar  Here --> ");
            sbHTML.Append("				<table id='table3' cellSpacing='0' cellPadding='0'  border='0' background='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/background.jpg' width=\"780\">  ");
            sbHTML.Append("				 <tr> ");
            sbHTML.Append("					<TD background=\"pics/background.jpg\">");
            sbHTML.Append("							&nbsp;<A href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/default.aspx' ><font color='black'>Home</A>");
            sbHTML.Append("					        &nbsp;<A href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Public/About_Us.aspx' ><font color='black'>About Us</A></font>&nbsp; ");
            sbHTML.Append("					        &nbsp;<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Public/Registration.aspx?Referrer=MU' ><font color='black'>Register</A>&nbsp; ");
            sbHTML.Append("					        &nbsp;<A href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Public/Submit_RFQ.aspx' ><font color='black'>Resin Quote</A>&nbsp; ");
            sbHTML.Append("					        &nbsp;<A href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Public/Contact_Us.aspx' ><font color='black'>Contact Us</A>&nbsp; ");
            sbHTML.Append("					</TD>");
            sbHTML.Append("						<td align=\"right\" valign=\"top\" background=\"http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/background.jpg\">");
            sbHTML.Append("                            		<img src=\"http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Pics/menu/menu_orange_phone-1.jpg\" >");
            sbHTML.Append("                        </td>");

            sbHTML.Append("			</tr> ");
            sbHTML.Append("				</table> ");


            sbHTML.Append("					</tr>");
            sbHTML.Append("				</table>");
            sbHTML.Append("			</td>");
            sbHTML.Append("		</tr>");
            sbHTML.Append("		<tr>");
            sbHTML.Append("			<td>");
            sbHTML.Append("				<!-- BEGIN OF `CONTENT OF THE INVOICE` HERE -->");
            sbHTML.Append("<table width='100%'><TR><TD align=right valign='center'><a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Market_Update/pdf/" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + ".pdf'><img src='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/pics/printer.jpg' border='0'>Print friendly version</a></td></tr></table>");
            // END HEADER 
            // BEGIN CONTENT 

            sbHTML.Append(MarketUpdateLibrary.Generate_Content(txtPolyethylene.Text, txtPolypropylene.Text, txtPolystyrene.Text, chart_paths, txtComments.Text, total, snapshot, issue_date, strTitle, txtPreTextPE.Text, txtPreTextPP.Text, txtPreTextPS.Text, "", "", "", "", "", "", webContext));

            // END CONTENT
            // BEGIN FOOTER
            sbHTML.Append("				<!-- END OF `CONTENT OF THE INVOICE` HERE -->");
            sbHTML.Append("			</td>");
            sbHTML.Append("		</tr>");
            sbHTML.Append("		<tr>");
            sbHTML.Append("				<!-- BEGIN `INVOICE FOOTER` -->");
            sbHTML.Append("			<td bgcolor=\"#464646\" height=\"15\"  align=\"center\" width=\"100%\">");
            sbHTML.Append("				<font face=\"Verdana,Arial\" size=\"1\" color=\"white\">");
            sbHTML.Append("					710 North Dearborn Chicago, IL 60610 |  ");
            sbHTML.Append("					<b>Tel:</b> " + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + "  |  ");
            sbHTML.Append("					<b>Fax:</b> " + ConfigurationSettings.AppSettings["FaxNumber"].ToString() + "</font>");
            sbHTML.Append("					</center>");
            sbHTML.Append(" 			</td>");
            sbHTML.Append("	        </tr>");
            sbHTML.Append("		<!-- BEGIN CLOSE CONTAINER TABLE --></div>");
            sbHTML.Append("		<tr>");
            sbHTML.Append("			<td bgcolor=\"black\" width=\"100%\" height=\"2\"></td>");
            sbHTML.Append("      	</tr>");
            sbHTML.Append("	</table>");
            sbHTML.Append("</td>");
            sbHTML.Append("<td bgcolor=#000000><img src=\"http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/images/1x1.gif\" width=1></td>");
            sbHTML.Append("</table>");
            sbHTML.Append("</body>");
            sbHTML.Append("</html>");
            // END FOOTER

            return sbHTML.ToString();
        }

        protected void btnSave_Click(object sender, System.EventArgs e)
        {
            saveData();
        }

        protected void btnPublish_Click(object sender, System.EventArgs e)
        {
            Label allHTML = new Label();
            allHTML.Text = "<BR><BR>You are attempting to PUBLISH Market Update!<BR>";

            allHTML.Text += "If you click \"Yes, I am sure\" button, so there is no way to cancel it.<BR><BR><center><h3>Are you SURE?</h3></center><BR>";

            htmlPlace.Controls.Add(allHTML);
            mainPanel.Visible = false;
            btnSure.Visible = true;
            btnCancel.Visible = true;

        }

        protected void btnClose_Click(object sender, System.EventArgs e)
        {
            htmlPlace.Controls.Clear();
            btnClose.Visible = false;
            btnPublish.Text = "Publish";
            btnSave.Visible = true;
            mainPanel.Visible = true;
            loadData("");

        }

        private void loadData(string strDate)
        {
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                conn.Open();
                string sqlString = "SELECT * FROM MARKET_UPDATES WHERE ";
                if (strDate.Trim() != "")
                {
                    DateTime review_date = new DateTime(Convert.ToInt64(strDate));
                    sqlString += " published=1 AND Day(date)=" + review_date.Day + " AND MONTH(date)=" + review_date.Month + " AND YEAR(date)=" + review_date.Year;

                }
                else
                {
                    sqlString += "published <> 1";
                }

                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, sqlString);
                if (dtr.Read())
                {
                    txtPolystyrene.Text = dtr["POLYSTYRENE"].ToString();
                    txtPolyethylene.Text = dtr["POLYETHYLENE"].ToString();
                    txtPolypropylene.Text = dtr["POLYPROPYLENE"].ToString();
                    txtComments.Text = dtr["COMMENTS"].ToString();
                    txtPreTextPE.Text = dtr["pretext_pe"].ToString();
                    txtPreTextPS.Text = dtr["pretext_ps"].ToString();
                    txtPreTextPP.Text = dtr["pretext_pp"].ToString();
                }
                else
                {
                    DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), "INSERT INTO MARKET_UPDATES(date) VALUES(getDate())");
                    txtPolystyrene.Text = string.Empty;
                    txtPolyethylene.Text = string.Empty;
                    txtPolypropylene.Text = string.Empty;
                    txtComments.Text = string.Empty;

                    txtPreTextPE.Text = string.Empty;
                    txtPreTextPS.Text = string.Empty;
                    txtPreTextPP.Text = string.Empty;
                }



            }

            /*			txtPolystyrene.Text = xmldoc.GetElementsByTagName("polystyrene_article").Item(index).InnerText;
                        txtPolyethylene.Text = xmldoc.GetElementsByTagName("polyethylene_article").Item(index).InnerText;
                        txtPolypropylene.Text = xmldoc.GetElementsByTagName("polypropylene_article").Item(index).InnerText;

                        txtComments.Text = xmldoc.GetElementsByTagName("comments").Item(index).InnerText;
                        */
        }

        //private void updateReview()
        //{
        //    using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
        //    {
        //        conn.Open();
        //        DateTime review_date = new DateTime(Convert.ToInt64(Session["EditDate"].ToString()));
        //        string sqlString = "UPDATE Market_Updates " +
        //            "SET comments=@comments, polypropylene=@polypropylene, " +
        //            "polystyrene=@polystyrene, polyethylene=@polyethylene, " +
        //            "pretext_pp=@pretext_pp, pretext_pe=@pretext_pe, pretext_ps=@pretext_ps " +
        //            "WHERE YEAR(date)=" + review_date.Year + " AND MONTH(date)=" + review_date.Month + " AND DAY(date)=" + review_date.Day;

        //        Hashtable param = new Hashtable();
        //        param.Add("@comments", txtComments.Text);
        //        param.Add("@polypropylene", txtPolypropylene.Text);
        //        param.Add("@polystyrene", txtPolystyrene.Text);
        //        param.Add("@polyethylene", txtPolyethylene.Text);

        //        param.Add("@pretext_ps", txtPreTextPS.Text);
        //        param.Add("@pretext_pp", txtPreTextPP.Text);
        //        param.Add("@pretext_pe", txtPreTextPE.Text);

        //        DBLibrary.ExecuteSqlWithoutScrub(Application["DBConn"].ToString(), sqlString, param);
        //    }


        //}

        protected void btnSure_Click(object sender, System.EventArgs e)
        {

            string[] charts = new string[6];
            DateTime moment = DateTime.Now;

            charts[0] = Server.MapPath("/Research/Charts/S_Chart_3_1M.png");
            charts[1] = Server.MapPath("/Research/Charts/S_Chart_3_1Y.png");
            charts[2] = Server.MapPath("/Research/Charts/S_Chart_26_1M.png");
            charts[3] = Server.MapPath("/Research/Charts/S_Chart_26_1Y.png");
            charts[4] = Server.MapPath("/Research/Charts/S_Chart_20_1M.png");
            charts[5] = Server.MapPath("/Research/Charts/S_Chart_20_1Y.png");

            string path1 = "/Pics/mu_pics/MUPE" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + "_1M.png";
            string path2 = "/Pics/mu_pics/MUPE" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + ".png";
            string path3 = "/Pics/mu_pics/MUPP" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + "_1M.png";
            string path4 = "/Pics/mu_pics/MUPP" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + ".png";
            string path5 = "/Pics/mu_pics/MUPS" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + "_1M.png";
            string path6 = "/Pics/mu_pics/MUPS" + moment.Day + "_" + moment.Month.ToString() + "_" + moment.Year.ToString() + ".png";
            System.IO.File.Copy(charts[0], Server.MapPath(path1), true);
            System.IO.File.Copy(charts[1], Server.MapPath(path2), true);

            System.IO.File.Copy(charts[2], Server.MapPath(path3), true);
            System.IO.File.Copy(charts[3], Server.MapPath(path4), true);
            System.IO.File.Copy(charts[4], Server.MapPath(path5), true);
            System.IO.File.Copy(charts[5], Server.MapPath(path6), true);

            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                conn.Open();
                string sqlString = "UPDATE Market_Updates " +
                    " SET published=1, date=getDate(), comments=@comments, polypropylene=@polypropylene, " +
                    "	polystyrene=@polystyrene, polyethylene=@polyethylene, total=@total, " +
                    "	pretext_pp=@pretext_pp, pretext_pe=@pretext_pe, pretext_ps=@pretext_ps, " +
                    "   PEMonthChartPath=@path1, PEYearChartPath=@path2, PPMonthChartPath=@path3, PPYearChartPath=@path4, PSMonthChartPath=@path5, PSYearChartPath=@path6" +
                    "	WHERE published <> 1";

                Hashtable param = new Hashtable();
                param.Add("@comments", txtComments.Text);
                param.Add("@polypropylene", txtPolypropylene.Text);
                param.Add("@polystyrene", txtPolystyrene.Text);
                param.Add("@polyethylene", txtPolyethylene.Text);
                param.Add("@total", MarketUpdateLibrary.TotalPoundsSpotFloor(this.Context));

                param.Add("@pretext_ps", txtPreTextPS.Text);
                param.Add("@pretext_pp", txtPreTextPP.Text);
                param.Add("@pretext_pe", txtPreTextPE.Text);

                param.Add("@path1", path1);
                param.Add("@path2", path2);
                param.Add("@path3", path3);
                param.Add("@path4", path4);
                param.Add("@path5", path5);
                param.Add("@path6", path6);

                DBLibrary.ExecuteSqlWithoutScrub(Application["DBConn"].ToString(), sqlString, param);

                SaveSpotSummary();

                sqlString = "INSERT INTO NEWS(HEADLINE) VALUES('The Plastics Exchange Monthly Update')";
                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlString);


            }

            btnClose.Visible = true;
            btnCancel.Visible = false;
            btnSure.Visible = false;
            htmlPlace.Controls.Clear();



            Label allHTML = new Label();
            allHTML.Text = "<BR><BR><h2>CONGRATULATIONS!!!</h2><BR>Market Update succesfully published!<BR><BR>";

            htmlPlace.Controls.Add(allHTML);


        }


        private void BindSpotSummary()//bind the data with the grid.
        {
            //select the data from the bboffer table,and group the record by the data stored in the OFFR_PROD field of the BBOFFER table.to calculate the count of records,the maximum price,the minimum price and the count of the weight for each group.
            string seleStr = "SELECT ";
            seleStr += "count=count(*), ";
            seleStr += "prodlink=('/Spot/Spot_Floor.aspx?Filter='), ";
            seleStr += "GRADE= (SELECT GRADE_NAME from GRADE WHERE GRADE_ID = OFFR_GRADE), ";
            seleStr += "offr_grade, ";
            seleStr += "Cast(min(offr_prce) AS money) AS MIN, ";//Cast(min(offr_prce) AS money)
            seleStr += "Cast(max(offr_prce) AS money) AS MAX, ";
            seleStr += "VARSIZE=sum(OFFR_QTY*OFFR_SIZE) ";
            //seleStr+="FROM bboffer where offr_grade is not null group by offr_grade  ";
            seleStr += "FROM bboffer where offr_grade is not null and offr_port is null and OFFR_SIZE <> '38500' and OFFR_SIZE <> '44500' group by offr_grade  ";
            seleStr += "ORDER BY VARSIZE DESC";

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString()))
            {
                //connect to the database.
                System.Data.DataSet ds = new DataSet();
                System.Data.SqlClient.SqlDataAdapter ad = new System.Data.SqlClient.SqlDataAdapter(seleStr, conn);
                ad.Fill(ds);//set the query data to dataset.

                dgSummaryGrid.DataSource = ds;//get data from the dataset.
                dgSummaryGrid.DataBind();
            }
        }

        private void SaveSpotSummary()
        {
            string seleStr = "INSERT INTO MarketUpdateSpotOffersSummary (date, grade_id, weight, price_low, price_high) ";
            seleStr += "SELECT ";
            seleStr += "getDate(), ";
            seleStr += "offr_grade, ";
            seleStr += "VARSIZE=sum(OFFR_QTY*OFFR_SIZE), ";
            seleStr += "Cast(min(offr_prce) AS money) , ";//Cast(min(offr_prce) AS money)
            seleStr += "Cast(max(offr_prce) AS money) ";
            //seleStr+="FROM bboffer where offr_grade is not null group by offr_grade  ";
            seleStr += "FROM bboffer where offr_grade is not null and offr_port is null and OFFR_SIZE <> '38500' and OFFR_SIZE <> '44500' group by offr_grade  ";
            seleStr += "ORDER BY VARSIZE DESC";

            DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), seleStr);

        }

        protected void btnCancel_Click(object sender, System.EventArgs e)
        {
            btnCancel.Visible = false;
            btnSure.Visible = false;
            mainPanel.Visible = true;
            htmlPlace.Controls.Clear();

        }

    }
}
