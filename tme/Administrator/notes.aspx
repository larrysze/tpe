<%@ Page language="c#" Codebehind="notes.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.notes" %>
<%@ Register TagPrefix="ninerays" Namespace="NineRays.Web.UI.WebControls.FlyTreeView" Assembly="NineRays.Web.UI.WebControls.FlyTreeView" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>notes</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<style>
.red A{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #0000FF;
	text-decoration: none;
	font-weight: bold;
}
</style>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<NineRays:FlyNodeSet id="myNotes" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server"></NineRays:FlyNodeSet>
		</form>
	</body>
</HTML>
