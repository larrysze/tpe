<%@ Page Language="c#" Codebehind="CRM.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.CRM2" EnableEventValidation="false" MasterPageFile="~/MasterPages/Menu.Master" %>

<%@ Register TagPrefix="radg" Namespace="Telerik.WebControls" Assembly="RadGrid" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Register TagPrefix="ninerays" Namespace="NineRays.Web.UI.WebControls.FlyTreeView" Assembly="NineRays.Web.UI.WebControls.FlyTreeView" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.master" %>
<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
    <style>
    .menuskin { BORDER-RIGHT: black 2px solid; BORDER-TOP: black 2px solid; Z-INDEX: 100; VISIBILITY: hidden; FONT: 10px/14px Verdana; BORDER-LEFT: black 2px solid; WIDTH: 225px; BORDER-BOTTOM: black 2px solid; POSITION: absolute; BACKGROUND-COLOR: menu }
	.menuskin A { PADDING-RIGHT: 5px; PADDING-LEFT: 5px; COLOR: black; TEXT-DECORATION: none }
	#mouseoverstyle { BACKGROUND-COLOR: highlight }
	#mouseoverstyle A { COLOR: white }
	.red { FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #0000ff; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	.red A { FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #0000ff; FONT-FAMILY: Arial, Helvetica, sans-serif; TEXT-DECORATION: none }
	</style>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphJavaScript">

    <script type="text/javascript">
        <!--
            function RowCreated(gridRow)
            {
                if ((gridRow.ItemType == "Item")||(gridRow.ItemType == "AlternatingItem"))
                {
                    
                    var gridRowTable = gridRow.Owner;
                    var cell = gridRowTable.GetCellByColumnUniqueName(gridRow, "TemplateColumn");                    
                    if (!cell)
                        return;

                    var checkBox = GetCheckBox(cell);

                    if (!checkBox)
                        return;

                    gridRow.Selected ? checkBox.checked = true : checkBox.checked = false;

                    var gridTable = this;
                    checkBox.onclick = function()
                    {
                        gridTable.SelectRow(gridRow.Control, false);
                    };
                }
            }

            function RowSelected(gridRow)
            {
                var gridRowTable = gridRow.Owner;
                var cell = gridRowTable.GetCellByColumnUniqueName(gridRow, "TemplateColumn");

                if (!cell)
                    return;

                var checkBox = GetCheckBox(cell);

                if (!checkBox)
                    return;

                gridRow.Selected ? checkBox.checked = true : checkBox.checked = false;
            }

            function RowDeselected(gridRow)
            {
                var gridRowTable = gridRow.Owner;
                var cell = gridRowTable.GetCellByColumnUniqueName(gridRow, "TemplateColumn");

                if (!cell)
                    return;

                var checkBox = GetCheckBox(cell);

                if (!checkBox)
                    return;

                gridRow.Selected ? checkBox.checked = true : checkBox.checked = false;
            }

            function GetCheckBox(control)
            {
                if (!control)
                    return;

                for (var i = 0; i < control.childNodes.length; i++)
                {
                    if (!control.childNodes[i].tagName)
                        continue;

                    if ((control.childNodes[i].tagName.toLowerCase() == "input") &&
                        (control.childNodes[i].type.toLowerCase() == "checkbox"))
                    {
                        return control.childNodes[i];
                    }
                }
            }
        -->
    </script>

    <script language="javascript">
        var str="";
        function cancelEvent() {
            window.event.returnValue = false;
        }
       
        function drop() {
            window.event.srcElement.style.backgroundColor = "gray";
            //alert(window.event.dataTransfer.getData("text"));
            arg = str;
            document.getElementById('<%=funcParam.ClientID %>').value =arg;   
            __doPostBack('<%=CreateFile.ClientID %>','');
            window.onerror=window.close;
        }
                        
        function handleMouseMove() {
            if (window.event.button == 1) {
               event.srcElement.dragDrop();
            }
        }
        
        function handleDragStart() {
            window.event.dataTransfer.setData("text", "From the DIV")
        }
        
        function handleDragEnter() {
            window.event.srcElement.style.backgroundColor = "red";
            str = event.srcElement.id; 
            cancelEvent();
        }
        
        function handleDragLeave() {
            window.event.srcElement.style.backgroundColor = "";
            cancelEvent();
        }
        
    </script>

    <script language="JavaScript1.2">

        //Pop-it menu- By Dynamic Drive
        //For full source code and more DHTML scripts, visit http://www.dynamicdrive.com
        //This credit MUST stay intact for use

        var linkset=new Array()
        //SPECIFY MENU SETS AND THEIR LINKS. FOLLOW SYNTAX LAID OUT

        <asp:PlaceHolder id="phActionButton"  runat="server" />
        ////No need to edit beyond here

        var ie4=document.all&&navigator.userAgent.indexOf("Opera")==-1
        var ns6=document.getElementById&&!document.all
        var ns4=document.layers

        function showmenu(e,which){

        if (!document.all&&!document.getElementById&&!document.layers)
        return

        clearhidemenu()

        menuobj=ie4? document.all.popmenu : ns6? document.getElementById("popmenu") : ns4? document.popmenu : ""
        menuobj.thestyle=(ie4||ns6)? menuobj.style : menuobj

        if (ie4||ns6)
        menuobj.innerHTML=which
        else{
        menuobj.document.write('<layer name=gui bgColor=#E6E6E6 width=225 onmouseover="clearhidemenu()" onmouseout="hidemenu()">'+which+'</layer>')
        menuobj.document.close()
        }

        menuobj.contentwidth=(ie4||ns6)? menuobj.offsetWidth : menuobj.document.gui.document.width
        menuobj.contentheight=(ie4||ns6)? menuobj.offsetHeight : menuobj.document.gui.document.height
        eventX=ie4? event.clientX : ns6? e.clientX : e.x
        eventY=ie4? event.clientY : ns6? e.clientY : e.y

        //Find out how close the mouse is to the corner of the window
        var rightedge=ie4? document.body.clientWidth-eventX : window.innerWidth-eventX
        var bottomedge=ie4? document.body.clientHeight-eventY : window.innerHeight-eventY

        //if the horizontal distance isn't enough to accomodate the width of the context menu
        if (rightedge<menuobj.contentwidth)
        //move the horizontal position of the menu to the left by it's width
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX-menuobj.contentwidth : ns6? window.pageXOffset+eventX-menuobj.contentwidth : eventX-menuobj.contentwidth
        else
        //position the horizontal position of the menu where the mouse was clicked
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX : ns6? window.pageXOffset+eventX : eventX

        //same concept with the vertical position
        if (bottomedge<menuobj.contentheight)
        menuobj.thestyle.top=ie4? document.body.scrollTop+eventY-menuobj.contentheight : ns6? window.pageYOffset+eventY-menuobj.contentheight : eventY-menuobj.contentheight
        else
        menuobj.thestyle.top=ie4? document.body.scrollTop+event.clientY : ns6? window.pageYOffset+eventY : eventY
        menuobj.thestyle.visibility="visible"
        return false
        }

        function contains_ns6(a, b) {
        //Determines if 1 element in contained in another- by Brainjar.com
        while (b.parentNode)
        if ((b = b.parentNode) == a)
        return true;
        return false;
        }

        function hidemenu(){
        if (window.menuobj)
        menuobj.thestyle.visibility=(ie4||ns6)? "hidden" : "hide"
        }

        function dynamichide(e){
        if (ie4&&!menuobj.contains(e.toElement))
        hidemenu()
        else if (ns6&&e.currentTarget!= e.relatedTarget&& !contains_ns6(e.currentTarget, e.relatedTarget))
        hidemenu()
        }

        function delayhidemenu(){
        if (ie4||ns6||ns4)
        delayhide=setTimeout("hidemenu()",500)
        }

        function clearhidemenu(){
        if (window.delayhide)
        clearTimeout(delayhide)
        }

        function highlightmenu(e,state){
        if (document.all)
        source_el=event.srcElement
        else if (document.getElementById)
        source_el=e.target
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        else{
        while(source_el.id!="popmenu"){
        source_el=document.getElementById? source_el.parentNode : source_el.parentElement
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        }
        }
        }

        if (ie4||ns6)
        document.onclick=hidemenu
        
    </script>

    <script language="javascript">
var oNewOption;		    
var str;
// Code for dynamically adding options to a select.
function fnHandleDragStart()
{                                      
  var oData = window.event.dataTransfer;

  // Set the effectAllowed on the source object.
  oData.effectAllowed = "move";
}

// This function is called by the target 
//  object in the ondrop event.
function fnHandleDrop()
{
  var oTarg = window.event.srcElement;
  var oData = window.event.dataTransfer;

  // Cancel default action.
  fnCancelDefault();

  // Set the content of the oTarget to the information stored
  //  in the data transfer object in the desired format.
  //oTarg.innerText += oData.getData("text");
}

// This function sets the dropEffect when the user moves the 
//  mouse over the target object.
function fnHandleDragEnter()
{
  var oData = window.event.dataTransfer;

  // Cancel default action.
  fnCancelDefault();

  // Set the dropEffect for the target object.
  oData.dropEffect = "move";
}

function fnCancelDefault()
{
  // Cancel default action.
  var oEvent = window.event;
  oEvent.returnValue = false;
}
function ShowResults()
{
    // Information about the events 
    // and what object fired them.
  str = event.srcElement.id; 
   var oData = window.event.dataTransfer;
  fnCancelDefault();
  // Set the dropEffect for the target object.
  oData.dropEffect = "move";  
}
function end(node, menuItemKey)
{	
	// Information about the events 
				 // and what object fired them.
	var i
	i=0;	 
}
function Lee()
{
  arg = str + "," + event.srcElement.id; 
  document.getElementById('<%=funcParam.ClientID %>').value =arg; 
  //oNewOption = new Option(); 
  //oNewOption.text = arg;
  //oResults.add(oNewOption,0);
  __doPostBack('<%=CreateFile.ClientID %>',''); 
  
  
}
			<!--
			function EditNodeText(node,menuItemKey) {				
				var key=node.Key;				
				var str = prompt("New text:", "");
				document.getElementById('<%=editnote.ClientID %>').value ="";
				if (str != null)
				 {
					if (str == "")
						alert("Node Text cannot be empty.");
					else 
					{					  
					    document.getElementById('<%=editnote.ClientID %>').value=key+"_"+str;	 			    
                       __doPostBack('<%=EditFile.ClientID %>',''); 
                    }
				}
			}
			function AddNodeText(node,menuItemKey) {				
				var key=node.Key;				
				var str = prompt("New text:", "");
				document.getElementById('<%=addnote.ClientID %>').value ="";
				if (str != null)
				 {
					if (str == "")
						alert("Node Text cannot be empty.");
					else 
					{					    					  
					    document.getElementById('<%=addnote.ClientID %>').value=key+"_"+str;	 			    
                        __doPostBack('<%=AddFile.ClientID %>',''); 
                    }
				}
			}
			function handleNodeEvent(){
				var node = window.event.node;
				if (window.event.eventName == 'ONDESELECTED' && node.IsInEditMode){
					node.Text = window.event.node.HtmlElement.children(0).innerHTML;
					node.IsInEditMode = false;				
				}
			}
			function EditNodeTextInline(node, menuItemKey) {
				if (!node.IsInEditMode){
					node.Select();
					node.HtmlElement.innerHTML = "<SPAN CONTENTEDITABLE='TRUE'>" + node.HtmlElement.innerHTML + "</SPAN>";
					node.HtmlElement.children(0).focus();
					node.IsInEditMode = true;
				}
			}
			// -->
    </script>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
    <input id="funcParam" type="hidden" name="funcParam" runat="server">
    <input id="editnote" type="hidden" name="editnote" runat="server">
    <input id="addnote" type="hidden" name="addnote" runat="server">
    <asp:LinkButton ID="CreateFile" OnClick="CreateFile_Click" runat="server" Visible="False" CssClass=LinkNormal></asp:LinkButton><asp:LinkButton ID="EditFile" OnClick="EditFile_Click" runat="server" Visible="False" CssClass=LinkNormal>EditFile</asp:LinkButton><asp:LinkButton ID="AddFile" OnClick="AddFile_Click" runat="server" Visible="False" CssClass=LinkNormal>LinkButton</asp:LinkButton>
    <div class="menuskin" id="popmenu" onmouseover="clearhidemenu();highlightmenu(event,'on')" onmouseout="highlightmenu(event,'off');dynamichide(event)">
    </div>
    <table cellspacing="0" cellpadding="0" width="740" border="0">
        <tr>
            <td valign="top" align="left" width="170">
                <ninerays:FlyTreeView ID="FTV" runat="server" CssClass="LinkNormal" NodeSrc="notes.aspx">
					<DefaultNodeType NavigateTarget="_self" IsCheckBox="False" SelectedHoverStyle="padding: 1px 3px 1px 3px; border: #aaaaaa 1px solid; FONT-FAMILY: Tahoma; FONT-SIZE: 11px; background-color: whitesmoke; cursor: hand; TEXT-DECORATION: underline;"
						PostBackOnCollapse="False" PostBackOnExpand="False" DragDropJavascript="return true" HoverStyle="padding: 2px 4px 2px 4px; FONT-FAMILY: Tahoma; FONT-SIZE: 11px; cursor: hand; TEXT-DECORATION: underline;"
						PostBackOnDeselect="False" SelectedExpandedImageUrl="" NavigateUrl="" ImageUrl="" CanBeSelected="True"
						DragDropName="" ContextMenuID="FlyContextMenu2" DragDropAcceptNames="" SelectCollapses="False"
						PostBackOnSelect="True" SelectedImageUrl="" ExpandedImageUrl="" PostBackOnUncheck="False" PostBackOnCheck="False"
						DefaultStyle="padding: 2px 4px 2px 4px; FONT-FAMILY: Tahoma; FONT-SIZE: 11px;" SelectExpands="True"
						SelectedStyle="padding: 1px 3px 1px 3px; border: #aaaaaa 1px solid; FONT-FAMILY: Tahoma; FONT-SIZE: 11px; background-color: whitesmoke; cursor: hand;"></DefaultNodeType>
                </ninerays:FlyTreeView><ninerays:FlyContextMenu ID="FlyContextMenu1" runat="server" >
					<ninerays:MenuItem Disabled="True" Key="menuItem3" AutoPostBack="True" Text="add"></ninerays:MenuItem>
					<ninerays:MenuItem Key="delete" AutoPostBack="True" Text="delete"></ninerays:MenuItem>
					<ninerays:MenuItem Javascript="EditNodeText(node, menuItemKey);" Key="edit" AutoPostBack="True" Text="Edit"></ninerays:MenuItem>
                </ninerays:FlyContextMenu><ninerays:FlyContextMenu ID="FlyContextMenu2" runat="server">
					<ninerays:MenuItem Javascript="AddNodeText(node,menuItemKey);" Key="add" AutoPostBack="True" Text="add"></ninerays:MenuItem>
					<ninerays:MenuItem Disabled="True" Key="menuItem7"  Text="ddit"></ninerays:MenuItem>
					<ninerays:MenuItem Disabled="True" Key="menuItem8" Text="delete"></ninerays:MenuItem>
					<ninerays:MenuItem Disabled="True" Key="menuItem9" Text="save edit"></ninerays:MenuItem>
                </ninerays:FlyContextMenu></td>
            <td valign="top" align="left" width="700" >
                <asp:Label ID="count" runat="server" Font-Bold="True" ForeColor="Red" CssClass="Header Color1"></asp:Label><br>
                <asp:Label ID="Label1" runat="server" CssClass="LinkNormal Content" ForeColor="Red"></asp:Label><asp:Label ID="Label3" CssClass="Content" runat="server" ForeColor="Red"></asp:Label><br>
                <asp:Label ID="message" runat="server" ForeColor="Red"></asp:Label><asp:Panel ID="pnEmail_Offer" runat="server" Visible="false">
                    <table id="Table5"  class=Content>
                        <tr>
                            <td>
                                <table id="Table6" align="center" border="0">
                                    <tr>
                                        <td>
                                            <img src="/pics/icons/alert.gif">
                                        </td>
                                        <td>
                                            <strong>Items have been selected to email out. Please select which leads should recieve these offers/requests</strong>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCancelSendingOffers" runat="server" Text="Cancel Sending Items" Width="190px" OnClick="btnCancelSendingOffers_Click"></asp:Button><br>
                                            <asp:Button ID="btnEmailAllMyLeads" runat="server" Text="Email all my Leads" Width="190px" OnClick="btnEmailAllMyLeads_Click"></asp:Button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table height="20" cellspacing="0" cellpadding="0" width="700" border="0" class=Content>
                    <tr>
                        <td align="right">
                            &nbsp;Search within leads:&nbsp;
                            <asp:TextBox CssClass="InputForm"  ID="txtSearch" runat="server"></asp:TextBox><asp:Button CssClass="Content Color2" ID="btnGo" runat="server" Text="Go" OnClick="btnGo_Click"></asp:Button></td>
                    </tr>
                </table>
                <table height="20" cellspacing="0" cellpadding="0" width="700" border="0" class=Content>
                    <tr align="right">
                        <td align="right">
                            User Title:
                            <asp:DropDownList CssClass="InputForm" ID="ddlUserTitle" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlUserTitle_SelectedIndexChanged">
                                <asp:ListItem Value="All" Selected="true">ALL</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="700" border="0" class=Content>
                    <tr>
                        <td valign="top" align="left" height="20">
                        <td>
                            <asp:CheckBox ID="cbAllResin" runat="server" Width="176px" Text="Include select All" Checked="True" AutoPostBack=true></asp:CheckBox><asp:CheckBox ID="cbDetails" runat="server" Width="176px" Text="Load details" AutoPostBack="True" OnCheckedChanged="cbDetails_CheckedChanged"></asp:CheckBox></td>
                        <td width="66">
                            Displayed:<br>
                            <asp:DropDownList CssClass="InputForm"  ID="ddlPageSize" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                <asp:ListItem Value="50">50</asp:ListItem>
                                <asp:ListItem Value="100">100</asp:ListItem>
                                <asp:ListItem Value="200">200</asp:ListItem>
                                <asp:ListItem Value="1000">1000</asp:ListItem>
                                <asp:ListItem Value="5000">5000</asp:ListItem>
                            </asp:DropDownList></td>
                        <td>
                            &nbsp; Resin:<br>
                            &nbsp;
                            <asp:DropDownList CssClass="InputForm" ID="ddlResinFilter" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlResinFilter_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td>
                            &nbsp; Market:<br>
                            &nbsp;
                            <asp:DropDownList CssClass="InputForm" ID="ddlMarketFilter" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlMarketFilter_SelectedIndexChanged">
                                <asp:ListItem Value="*">Show All</asp:ListItem>
                                <asp:ListItem Value="1">Domestic</asp:ListItem>
                                <asp:ListItem Value="2">International</asp:ListItem>
                            </asp:DropDownList></td>
                        <td>
                            &nbsp; State:<br>
                            &nbsp;
                            <asp:DropDownList CssClass="InputForm" ID="ddlState" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="700" border="0">
                    <tr>
                        <td>
                            <asp:Button CssClass="Content Color2" ID="Button1" runat="server" Text="Select All" OnClick="Button1_Click"></asp:Button>
                            <asp:Button CssClass="Content Color2" ID="btnEmailOffer" runat="server" Width="80px" Text="Email" OnClick="btnEmailOffer_Click"></asp:Button>
                            <asp:Button CssClass="Content Color2" ID="btnDelete" runat="server" Width="80px" Text="Delete" OnClick="btnDelete_Click"></asp:Button>
                            <asp:Button CssClass="Content Color2" ID="btnRestore" runat="server" Visible="False" Width="80px" Text="Restore" OnClick="btnRestore_Click"></asp:Button>
                            <asp:Button CssClass="Content Color2" ID="btnAllocate" runat="server" Width="80px" Text="Allocate" OnClick="btnAllocate_Click"></asp:Button>
                            <asp:Button CssClass="Content Color2" ID="btnRemoveGroupMember" runat="server" Visible="False" Text="Remove from Folder" OnClick="btnRemoveGroupMember_Click"></asp:Button>
                            <asp:Button CssClass="Content Color2" ID="btnEmailLetter" runat="server" Text="Email Letter" OnClick="btnEmailLetter_Click"></asp:Button>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="780" border="0">
                    <tr>
                        <td valign="top" align="left" width="780">
                            <radg:RadGrid ID="dg" runat="server" Width="700px" DataKeyField="pers_id" PageSize="200" AllowPaging="True" AutoGenerateColumns="False" AllowMultiRowSelection="True" AllowSorting="True" ItemStyle-CssClass=Content AlternatingItemStyle-CssClass=Content EditItemStyle-CssClass=LinkNormal>
                                <MasterTableView AllowCustomPaging="False" AllowSorting="True" PageSize="200" DataKeyField="pers_id">
                                    <RowIndicatorColumn UniqueName="RowIndicator" Visible="False">
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </RowIndicatorColumn>
                                    <EditFormSettings>
                                        <EditColumn UniqueName="EditCommandColumn">
                                        </EditColumn>
                                    </EditFormSettings>
                                    <Columns>
                                        <radg:GridTemplateColumn Groupable="False" UniqueName="TemplateColumn">
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" AutoPostBack="False" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                        </radg:GridTemplateColumn>
                                        <radg:GridHyperLinkColumn DataNavigateUrlField="PERS_MAIL" UniqueName="PERS_NAME" DataNavigateUrlFormatString="mailto:{0}" SortExpression="PERS_NAME ASC" HeaderText="Name" DataTextField="PERS_NAME" ItemStyle-CssClass=LinkNormal>
                                        </radg:GridHyperLinkColumn>
                                        <radg:GridBoundColumn UniqueName="column" SortExpression="PERS_TITL ASC" HeaderText="Title" DataField="PERS_TITL">
                                        </radg:GridBoundColumn>
                                        <radg:GridBoundColumn UniqueName="column" SortExpression="PERS_COMPANY ASC" HeaderText="Company" DataField="PERS_COMPANY" ItemStyle-CssClass=LinkNormal>
                                        </radg:GridBoundColumn>
                                        <radg:GridBoundColumn UniqueName="column" SortExpression="PERS_CATEGORY ASC" HeaderText="Corporate Identity" DataField="PERS_CATEGORY">
                                        </radg:GridBoundColumn>
                                        <radg:GridBoundColumn UniqueName="column" DataFormatString="{0:d}" SortExpression="PERS_LAST_CHKD ASC" HeaderText="Last Login" DataField="PERS_LAST_CHKD">
                                            <HeaderStyle Width="80px"></HeaderStyle>
                                        </radg:GridBoundColumn>
                                        <radg:GridBoundColumn UniqueName="column" SortExpression="PERS_NUMBER_LOGINS ASC" HeaderText="Logins" DataField="PERS_NUMBER_LOGINS">
                                        </radg:GridBoundColumn>
                                        <radg:GridBoundColumn UniqueName="column" SortExpression="STATE ASC" HeaderText="State" DataField="STATE">
                                        </radg:GridBoundColumn>
                                        <radg:GridBoundColumn UniqueName="column" SortExpression="PERS_PHON ASC" HeaderText="Phone" DataField="PERS_PHON">
                                        </radg:GridBoundColumn>
                                        <radg:GridBoundColumn UniqueName="column" DataFormatString="{0:MM/dd/yyyy}" SortExpression="PERS_DATE ASC" HeaderText="Date Registered" DataField="PERS_DATE">
                                        </radg:GridBoundColumn>
                                        <radg:GridBoundColumn UniqueName="column" SortExpression="PERS_PRMLY_INTRST ASC" HeaderText="M" DataField="PERS_PRMLY_INTRST">
                                        </radg:GridBoundColumn>
                                        <radg:GridBoundColumn UniqueName="column" SortExpression="PERS_ALLOCATED ASC" HeaderText="Broker" DataField="PERS_ALLOCATED">
                                            <HeaderStyle Width="30px"></HeaderStyle>
                                        </radg:GridBoundColumn>
                                        <radg:GridBoundColumn UniqueName="column" HeaderText="PERS_PRMLY_INTRST" Visible="False" DataField="PERS_PRMLY_INTRST">
                                            <HeaderStyle Width="30px"></HeaderStyle>
                                        </radg:GridBoundColumn>
                                        <radg:GridBoundColumn UniqueName="column" HeaderText="PERS_INTRST_SIZE" Visible="False" DataField="PERS_INTRST_SIZE">
                                        </radg:GridBoundColumn>
                                        <radg:GridBoundColumn UniqueName="column" HeaderText="PERS_INTRST_QUALT" Visible="False" DataField="PERS_INTRST_QUALT">
                                        </radg:GridBoundColumn>
                                        <radg:GridBoundColumn HeaderText="Email Freq" DataField="EMAIL_ENBL" UniqueName="column" SortExpression="EMAIL_ENBL ASC">
                                        </radg:GridBoundColumn>
                                    </Columns>
                                    <ExpandCollapseColumn ButtonType="ImageButton" UniqueName="ExpandColumn" Visible="False">
                                        <HeaderStyle Width="19px"></HeaderStyle>
                                    </ExpandCollapseColumn>
                                </MasterTableView>
                                <AlternatingItemStyle CssClass="Content"></AlternatingItemStyle>
                                <SelectedItemStyle BackColor="#C0C0FF"></SelectedItemStyle>
                                <GroupHeaderItemStyle BorderColor="Black" BackColor="Silver"></GroupHeaderItemStyle>
                                <ClientSettings ApplyStylesOnClient="True">
                                    <Selecting AllowRowSelect="True" EnableDragToSelectRows="False"></Selecting>
                                    <ClientEvents OnRowDeselected="RowDeselected" OnRowSelected="RowSelected" OnRowCreated="RowCreated"></ClientEvents>
                                </ClientSettings>
                                <HeaderStyle CssClass="LinkNormal Header"></HeaderStyle>
                                <GroupPanel Visible="False">
                                </GroupPanel>
                                <PagerStyle Mode="NumericPages" CssClass = "Content LinkNormal"></PagerStyle>
                            </radg:RadGrid></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
