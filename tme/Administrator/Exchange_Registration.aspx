<%@ Page Language="VB" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Web.Mail" %>
<%@ import Namespace="System.Web.Mail" %>


<%
'only system adminstrator can register an exchange user.
IF Session("Typ")<>"A" THEN Response.Redirect ("/default.aspx")

'Database connection'
Dim conn as SqlConnection
Dim conn1 as SqlConnection
Dim cmdContent as SQLCommand
Dim cmdContent1 as SQLCommand
Dim Rec0 as SqlDataReader
Dim Rec1 as SqlDataReader
conn = new SqlConnection(Application("DBconn").ToString())
conn.Open()
conn1 = new SqlConnection(Application("DBconn").ToString())
conn1.Open()
DIM i,Str,Height

        Dim Ex
        dim Id, j, n, m

        'person Id(we insert a new record into PERSON table at registration step 1)
        Id=Request.QueryString("iddd")

	'Creation of the mother company
	Str="INSERT INTO COMPANY (COMP_TYPE,COMP_REG,COMP_DATE,COMP_FELN,COMP_BKRP,COMP_ENBL, COMP_NAME) VALUES('"&Request.QueryString("type")&"',1,GETDATE(),0,0,1,'"+Request.QueryString("comp_name")+"')"
	cmdContent= new SqlCommand(Str, conn1)
	cmdContent.ExecuteNonQuery()


        'This query returns the lastes comp_id from COMPANY table
	cmdContent= new SqlCommand("SELECT COMP_ID FROM COMPANY ORDER BY COMP_ID DESC", conn)
	Rec0= cmdContent.ExecuteReader()
	Rec0.Read
	i=Rec0(0)

	'Get the second comp_id if it's distributor(creates two companies)
        Rec0.Read
       'Update to PERSON table
       'when we insert info into PERSON table at registration step 1, we left PERS_COMP blank
       'so we need to assign the new company to this person.
       	Str="UPDATE PERSON SET PERS_COMP="+i.ToString()+", PERS_STAT='0',EMAIL_ENBL='1' WHERE PERS_ID="+Id+""
	cmdContent= new SqlCommand(Str, conn1)
	cmdContent.ExecuteNonQuery()

	'insert to BRAND table
	Str="INSERT INTO BRAND (BRAN_COMP, BRAN_ENBL) Values("+i.ToString()+",0)"
	cmdContent= new SqlCommand(Str, conn1)
	cmdContent.ExecuteNonQuery()

	'insert to Associate
        Str="INSERT INTO ASSOCIATE (ASSO_COMP) Values("+i.ToString()+")"
	cmdContent= new SqlCommand(Str, conn1)
	cmdContent.ExecuteNonQuery()

	'insert to Bank
	Str="INSERT INTO BANK (BANK_COMP) Values("+i.ToString()+")"
	cmdContent= new SqlCommand(Str, conn1)
	cmdContent.ExecuteNonQuery()

	'insert to CREDIT
	Str="INSERT INTO CREDIT (CRED_COMP,CRED_AMNT,CRED_AVLB,CRED_RATE) Values("+i.ToString()+",0,0,0)"
	cmdContent= new SqlCommand(Str, conn1)
	cmdContent.ExecuteNonQuery()
	'insert to PLACE
	Str="INSERT INTO PLACE (PLAC_COMP) Values("+i.ToString()+")"
	cmdContent= new SqlCommand(Str, conn1)
	cmdContent.ExecuteNonQuery()
	Rec0.Close


	'if someone registered as a Distributor then two companies(distributor, purchaser)
    'will be generated, so when we perform an update, it needs to update two compaies
	'at Update_Company_Info.aspx.

	'Update_Company_Info.aspx will know this is not a distributor
	Response.Redirect("Update_Company_Info.aspx?Id="+i.ToString())
%>
