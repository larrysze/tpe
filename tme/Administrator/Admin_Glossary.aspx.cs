using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;

namespace localhost.Administrator
{
    public partial class Admin_Glossary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.Width = "780px";
            if (Session["Typ"].ToString() != "A")
            {
                Response.Redirect("/default.aspx");
            }
            if (!IsPostBack)
            {
                Binddg();
            }
        }
        private void Binddg()
        {

            DBLibrary.BindDataGrid(Application["DBconn"].ToString(), dg, "SELECT * from Glossary");

        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.dg.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_CancelCommand);
            this.dg.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_EditCommand);
            this.dg.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_UpdateCommand);
            this.dg.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_DeleteCommand);
            this.dg.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_ItemDataBound);
        }
        #endregion

        private void dg_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            dg.EditItemIndex = -1;
            Binddg();
        }

        private void dg_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            string id = dg.DataKeys[e.Item.ItemIndex].ToString();
           // string word = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
            string sql = "delete from glossary where word = @ID";

            Hashtable param = new Hashtable();
            param.Add("@ID", id);
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sql, param);
            Binddg();
        }

        private void dg_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            dg.EditItemIndex = e.Item.ItemIndex;
            Binddg();

        }

        private void dg_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
             string key=dg.DataKeys[e.Item.ItemIndex].ToString();

        /*       string word;
               string definition;
               TextBox tb;
               tb = (TextBox) e.Item.Cells[1].Controls[0];
               word=tb.Text;
               tb = (TextBox) e.Item.Cells[2].Controls[0];
               definition=tb.Text; */

            string word = ((TextBox)e.Item.Cells[2].Controls[1]).Text;
            string definition = ((TextBox)e.Item.Cells[3].Controls[1]).Text;

            string id = dg.DataKeys[e.Item.ItemIndex].ToString();

            string sql = "UPDATE Glossary set word = @word, definition = @definition where word = @ID;";
            Hashtable param = new Hashtable();
            param.Add("@word", word);
            param.Add("@definition", definition);
            param.Add("@ID", id);
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sql, param);

            dg.EditItemIndex = -1;
            Binddg();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if ((word.Text != "") & (definition.Text != ""))
            {
                Hashtable param = new Hashtable();

                string strSql = "select word FROM glossary WHERE word = @word";
                param.Add("@word", word.Text);
                param.Add("@definition", definition.Text);
                DataTable dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), strSql, param);

                if (dt.Rows.Count == 0)
                {
                    string strSqlInsert = "INSERT INTO glossary VALUES (@word, @definition)";
                    DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlInsert, param);
                    Binddg();
                    lblError.Text = "";
                    word.Text = "";
                    definition.Text = "";
                }
                else lblError.Text = "This word already used"; 
            }
            else lblError.Text = "Please complete all fields"; 
        }

        protected void dg_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[1] != null)
                    ((LinkButton)e.Item.Cells[1].Controls[0]).Attributes.Add("onClick", "return confirm('Are you sure you want to delete this definition?');");
            }       
       }

    }
}
