//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.42
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace localhost.Administrator {
    
    public partial class Edit_News {
        protected System.Web.UI.WebControls.Label Message;
        protected System.Web.UI.WebControls.TextBox txtHeadline;
        protected System.Web.UI.WebControls.TextBox txtDate;
        protected System.Web.UI.WebControls.TextBox txtStory;
        protected System.Web.UI.WebControls.Button UpdateNews;
        protected System.Web.UI.WebControls.Button CancelNews;
        public new localhost.MasterPages.Menu Master {
            get {
                return ((localhost.MasterPages.Menu)(base.Master));
            }
        }
    }
}
