namespace localhost.Administrator
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Data.SqlClient;
	using DatabaseLibrary;
	using localhost;
	using System.Web.Mail;
	using System.Text;

	/// <summary>
	///		Summary description for Shipment_Documents.
	/// </summary>
	public class Shipment_Documents : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.Label lblType;
		protected System.Web.UI.WebControls.DropDownList ddlType;
		protected System.Web.UI.WebControls.Label lblSendTo;
		protected System.Web.UI.WebControls.DropDownList ddlSendTo;
		protected System.Web.UI.WebControls.Label lblCC;
		protected System.Web.UI.WebControls.DropDownList ddlCC;
		protected System.Web.UI.WebControls.Label lblCustom;
		protected System.Web.UI.WebControls.Label lblOrderID;
		protected System.Web.UI.WebControls.Button btnPreview;
		protected System.Web.UI.WebControls.Button btnEmail;
		protected System.Web.UI.WebControls.TextBox txtCustom;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				if ((Session["Shipment_Documents_OrderID"]!= null)&&(Session["Shipment_Documents_OrderID"].ToString()!=""))
				{
					lblOrderID.Text = Session["Shipment_Documents_OrderID"].ToString();
				}

				if (lblOrderID.Text != "")
				{
					//Remove Title Transfer letter if the size is not a r/c or if the r/c # is empty
					 string[] ID = lblOrderID.Text.Split('-');
					if (!ShowTitleTransferLetter(ID[0],ID[1]))
					{
						ddlType.Items.Remove(ddlType.Items.FindByValue("6"));
					}
					PopulateScreen(ddlType.SelectedItem.Value);
				}
			}
		}

		public void PopulateScreen(string typeDocument)
		{
			string[] ID = lblOrderID.Text.Split('-');

			ddlSendTo.Items.Clear();
			ddlCC.Items.Clear();
			
			//Types: 1-Invoice; 2-Perchase Order; 3-Certificate; 4-Sales Confirmation; 
			//	5-Customer Release; 6-HC Title Transfer; 7-Trucking Company; 8-Wahehouse Release
			string emailAdd = "";
			if ((typeDocument=="1")||(typeDocument=="3")||(typeDocument=="4")||(typeDocument=="5"))
				emailAdd = getEmailBuyer(ID[0],ID[1]);
			else if (typeDocument=="2")
				emailAdd = getEmailSeller(ID[0],ID[1]);
			else if ((typeDocument=="6") || (typeDocument=="8"))
				emailAdd = getEmailWarehouse(ID[0],ID[1]);

			//Custom field
			txtCustom.Text = "";
			lblCustom.Visible = true;
			txtCustom.Visible = true;
			if ((typeDocument=="1")||(typeDocument=="2")||(typeDocument=="4"))
			{
				lblCustom.Text = "Comment:";
			}
			else if (typeDocument=="7")
			{
				lblCustom.Text = "Ship Date:";
			}
			else if (typeDocument=="8")
			{
				lblCustom.Text = "Release #:";
			}
			else
			{
				lblCustom.Visible = false;
				txtCustom.Visible = false;
			}

			//if the email of the seller/buyer/warehouse is equal Mike's email, don't add
			if ((emailAdd!="michael@theplasticsexchange.com")&&(emailAdd!="")) ddlSendTo.Items.Add(emailAdd);

			//Adding other Emails
			ddlCC.Items.Add(""); //There is a option to do not cc anyone.
			ddlSendTo.Items.Add("michael@theplasticsexchange.com");
			ddlCC.Items.Add("michael@theplasticsexchange.com");
			ddlSendTo.Items.Add("helio@theplasticsexchange.com");
			ddlCC.Items.Add("helio@theplasticsexchange.com");
			ddlSendTo.Items.Add("chris@theplasticsexchange.com");
			ddlCC.Items.Add("christian@theplasticsexchange.com");
			ddlSendTo.Items.Add("john@theplasticsexchange.com");
			ddlCC.Items.Add("john@theplasticsexchange.com");

			DBLib db = new DBLib(this.Context);
			db.OpenConnection();
			SqlDataReader sqlDTPers = db.ExecuteReader("select PERS_MAIL from person WHERE PERS_TYPE = 'B' and PERS_ENBL = 1");
			while (sqlDTPers.Read())
			{
				ddlSendTo.Items.Add(sqlDTPers["PERS_MAIL"].ToString());
				ddlCC.Items.Add(sqlDTPers["PERS_MAIL"].ToString());
			}
			sqlDTPers.Close();
			db.CloseConnection();

			//Selecting email user logged on in CC field
			string emailAdminBroker = getEmailUser(Session["ID"].ToString());
			ListItem itemCC = ddlCC.Items.FindByText(emailAdminBroker);
			if (itemCC!=null)
			{
				itemCC.Selected = true;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ddlType.SelectedIndexChanged += new System.EventHandler(this.ddlType_SelectedIndexChanged);
			this.btnPreview.Click += new System.EventHandler(this.btnEmail_Click);
			this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region Certificate HTML
		public string CertificateHTML(string shipmentOrderID, string shipmentSKU)
		{
			DBLib db = new DBLib(this.Context);
			string StrEmail = "";
			db.OpenConnection();

			//Gathering info about Person
			SqlDataReader dtPerson = db.ExecuteReader("Select * from PERSON WHERE PERS_ID=(SELECT SHIPMENT_BUYR FROM SHIPMENT WHERE SHIPMENT_ID = (Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID = '" + shipmentOrderID + "' and SHIPMENT_SKU ='" + shipmentSKU + "'))");
			dtPerson.Read();
			string personFirstName = HelperFunction.getSafeStringFromDB(dtPerson["PERS_FRST_NAME"]);
			string personLastName = HelperFunction.getSafeStringFromDB(dtPerson["PERS_LAST_NAME"]);
			string personTitle = HelperFunction.getSafeStringFromDB(dtPerson["PERS_TITL"]);
			dtPerson.Close();

			//Gathering info about Shipment
			SqlDataReader dtShipment = db.ExecuteReader("SELECT (SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=PLAC_COMP),PLAC_ADDR_ONE,PLAC_ADDR_TWO,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL),PLAC_ZIP,(SELECT (SELECT CTRY_NAME FROM COUNTRY WHERE CTRY_CODE=LOCL_CTRY) FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) FROM PLACE WHERE PLAC_TYPE='H' AND PLAC_COMP=(SELECT (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR) FROM SHIPMENT WHERE SHIPMENT_ORDR_ID = '" + shipmentOrderID + "' and SHIPMENT_SKU ='" + shipmentSKU + "')");
			dtShipment.Read();
			string companyName = HelperFunction.getSafeStringFromDB(dtShipment[0]);
			string placeAddressOne = HelperFunction.getSafeStringFromDB(dtShipment["PLAC_ADDR_ONE"]);
			string placeZip = HelperFunction.getSafeStringFromDB(dtShipment["PLAC_ZIP"]);
			string placeCityState = HelperFunction.getSafeStringFromDB(dtShipment[3]);
			dtShipment.Close();
            
			//Gathering info about Orders
			SqlDataReader dtOrder = db.ExecuteReader("SELECT * FROM ORDERS,SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ORDR_ID = '" + shipmentOrderID + "' and SHIPMENT_SKU ='" + shipmentSKU + "'");
			dtOrder.Read();
			string orderID = HelperFunction.getSafeStringFromDB(dtOrder["ORDR_ID"]);
			bool orderIsSpot = System.Convert.ToBoolean(dtOrder["ORDR_ISSPOT"]);
			string orderProdutoSpot = HelperFunction.getSafeStringFromDB(dtOrder["ORDR_PROD_SPOT"]);
			string orderDensity = HelperFunction.getSafeStringFromDB(dtOrder["ORDR_DENS"]);
			string orderMelt = HelperFunction.getSafeStringFromDB(dtOrder["ORDR_MELT"]);
			string orderContract = HelperFunction.getSafeStringFromDB(dtOrder["ORDR_CONT"]);
			string shipmentSize = HelperFunction.getSafeStringFromDB(dtOrder["SHIPMENT_SIZE"]);
			dtOrder.Close();
			
			//Gathering Order Date
			SqlDataReader dtOrderDate = db.ExecuteReader("SELECT LEFT(CONVERT(varchar(14),ORDR_DATE,101), 10) FROM ORDERS WHERE ORDR_ID='" + shipmentOrderID + "'");
			dtOrderDate.Read();
			string orderDate = HelperFunction.getSafeStringFromDB(dtOrderDate[0]);
			dtOrderDate.Close();

			//Creating email Body
			StrEmail = HTMLHeader();

			StrEmail=StrEmail+"<br>" + personFirstName + "&nbsp;" + personLastName;
			StrEmail=StrEmail+"<br>" + personTitle;
			//StrEmail=StrEmail+"<br>" + companyName;
			//StrEmail=StrEmail+"<br>" + placeAddressOne;
			StrEmail=StrEmail+"<br>" + placeCityState;
			StrEmail=StrEmail+"&nbsp;" + placeZip;
			StrEmail = StrEmail + "<br><br><br>" + bl();

			StrEmail = StrEmail + "Dear " + personFirstName + ",<br><br>" + bl();
			StrEmail=StrEmail+"This letter certifies that the resin sold to&nbsp;" + companyName + " on<font color=navy>&nbsp;";
			StrEmail=StrEmail + orderDate;
			StrEmail=StrEmail+"</font>, referenced by transaction #" + orderID;

			//190000-pound is railcar, display railcar # if exist
			if (shipmentSize=="190000")
			{
				SqlDataReader dtContentSHIP = db.ExecuteReader("SELECT SHIPMENT_COMMENT FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='" + orderID + "'");
				dtContentSHIP.Read();
				if (dtContentSHIP.HasRows)
				{
					string shipmentComment = HelperFunction.getSafeStringFromDB(dtContentSHIP["SHIPMENT_COMMENT"]);
					if (shipmentComment!= "")
					{
		                StrEmail=StrEmail+", r/c # ";
						dtContentSHIP.Close();
						dtContentSHIP = db.ExecuteReader("SELECT SHIPMENT_COMMENT FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='" + orderID + "'");
						while(dtContentSHIP.Read())
						{
							StrEmail=StrEmail + HelperFunction.getSafeStringFromDB(dtContentSHIP["SHIPMENT_COMMENT"])+ "<br>";
						}
					}
				}
				dtContentSHIP.Close();
			}

			//if it's spot order, display Dens, Melt
			//only spot odder has Dens, Melt, Adds
			if (orderIsSpot)
			{
				StrEmail=StrEmail+",&nbsp;is " + orderProdutoSpot;
				StrEmail=StrEmail+"&nbsp;and conforms to the producer's prime specification.";
				StrEmail = StrEmail + "<br><br>" + bl();
				if (orderDensity != "") StrEmail=StrEmail+"Density (ASTM D1505)<b>&nbsp;&nbsp;&nbsp;" + orderDensity + "</b>g/cm3&nbsp;&nbsp;&nbsp;ASTM D1505<br>";
				if (orderMelt!= "") StrEmail=StrEmail+"Melt Flow Rate (190C/2.16 kg - E) <b>&nbsp;&nbsp;&nbsp;" +  orderMelt + "</b>g/cm3&nbsp;&nbsp;&nbsp;ASTM D1238<br>";
			}
			else
			{
				//if it's not spot order, have to find order details from CONTRACT table
				SqlDataReader dtContract = db.ExecuteReader("SELECT * FROM CONTRACT WHERE CONT_ID=" + orderContract);
				dtContract.Read();
				StrEmail=StrEmail+",&nbsp;is " + dtContract["CONT_LABL"].ToString();
				StrEmail=StrEmail+"&nbsp;and conforms to the producer's prime specification.";
				StrEmail = StrEmail + "<br><br>" + bl();
				string contractDesnsityTGT = HelperFunction.getSafeStringFromDB(dtContract["CONT_DENS_TGT"]);
				string contractMeltTGT = HelperFunction.getSafeStringFromDB(dtContract["CONT_MELT_TGT"]);
				if (contractDesnsityTGT!="") StrEmail=StrEmail+"Density (ASTM D1505)<b>&nbsp;&nbsp;&nbsp;" + contractDesnsityTGT + "</b>g/cm3&nbsp;&nbsp;&nbsp;ASTM D1505<br>";
				if (contractMeltTGT!="") StrEmail=StrEmail+"Melt Flow Rate (190C/2.16 kg - E) <b>&nbsp;&nbsp;&nbsp;" + contractMeltTGT + "</b>g/cm3&nbsp;&nbsp;&nbsp;ASTM D1238<br>";
				dtContract.Close();
			}
			
			StrEmail = StrEmail + HTMLFooter("Michael Greenberg, CEO");
			db.CloseConnection();

			return StrEmail;
		}
		#endregion

		#region HC Title Transfer
		private string TitleTransfer(string shipmentOrderID, string shipmentSKU)
		{
			DBLib db = new DBLib(this.Context);
			db.OpenConnection();

			//string OrderId = ShipmentID(shipmentOrderID,shipmentSKU);
			string com_name="";
			string LocationName="";
			string Shipment_Comment="";
			string loctype="";
			string locContact="";
			string locPhone="";
			string sqlBuyer="select com_name=(select companyName=(select comp_name from company where comp_id=pers_comp) from person where pers_id=shipment_buyr) from shipment where shipment_ordr_id='"+shipmentOrderID.ToString() + "' and shipment_sku = '" + shipmentSKU.ToString().Trim() + "'";
			SqlDataReader rd= db.ExecuteReader(sqlBuyer);
			if(rd.Read())
			{
				com_name=rd["com_name"].ToString().Trim();//buryer
			}
			rd.Close();
			string sqlOrderTo="select (L.LOCL_CITY+', '+L.LOCL_STAT) LocationName, P.PLAC_TYPE locType, PLAC_PERS, PLAC_PHON, PLAC_INST " + 
							 "from SHIPMENT S, LOCALITY L, PLACE P " +
							 "where S.SHIPMENT_TO = P.PLAC_ID " +
							 "	and P.PLAC_LOCL = L.LOCL_ID " +
							 "  and S.SHIPMENT_ORDR_ID='"+shipmentOrderID.ToString().Trim() + "'" + 
							 "  and S.SHIPMENT_SKU = '" + shipmentSKU.ToString().Trim() + "'";
			rd= db.ExecuteReader(sqlOrderTo);
			if(rd.Read())
			{
				//LocationName=rd["LocationName"].ToString().Trim();
				LocationName=HelperFunction.getSafeStringFromDB(rd["PLAC_INST"]);
				locContact = HelperFunction.getSafeStringFromDB(rd["plac_pers"]);
				locPhone = HelperFunction.getSafeStringFromDB(rd["plac_phon"]);

				if(rd["locType"]!=null)
				{
					if ((string)rd["locType"]=="H")
					{
						loctype = "Headquarters";
					}
					else if ((string)rd["locType"]=="S")
					{
						loctype = "Shipping Point";
					}
					else if ((string)rd["locType"]=="D")
					{
						loctype = "Delivery Point";
					}
					else
					{
						loctype= "Warehouse";
					}
				}
				else
				{
					loctype="warehouse";
				}
			}
			rd.Close();
			string sqlShip="SELECT SHIPMENT_COMMENT FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='"+shipmentOrderID.ToString().Trim() + "' AND SHIPMENT_SKU = '" + shipmentSKU.ToString().Trim() + "'";
			rd= db.ExecuteReader(sqlShip);
			if(rd.Read())
			{
				Shipment_Comment = HelperFunction.getSafeStringFromDB(rd["SHIPMENT_COMMENT"]);
			}
			rd.Close();
			//creat email body
			System.Text.StringBuilder sb=new System.Text.StringBuilder("");

			sb.Append(HTMLHeader());

			sb.Append("Date: " + DateTime.Today.ToShortDateString() + "<br>");
			sb.Append("To: "+LocationName+"(<font face=arial black color=red>"+loctype+"</font>)<br>");
			sb.Append("Tel: " + locPhone + "<br>");
			sb.Append("Fax: <br><br>");
			sb.Append("Dear " + locContact + ",<br><br>");
			sb.Append(@"Upon receipt of Railcar #");
			sb.Append(Shipment_Comment.ToString().Trim());
			sb.Append(@", please accept it, on behalf of The Plastics Exchange and immediately transfer title to the Account of&nbsp");
			sb.Append("<b>" + getCompanyNameBuyer(shipmentOrderID,shipmentSKU) + "</b>");
			sb.Append(@", please notify&nbsp;");
			sb.Append("<b>" + getCompanyNameBuyer(shipmentOrderID,shipmentSKU) + "</b>");
			sb.Append(@" to accept the Hopper car when it arrives.<br><br>");
			sb.Append(@"<br>Thank you very much.");
			
			sb.Append(HTMLFooter("Helio Pimentel<br>Logistics Coordinator"));
			
			db.CloseConnection();
			return sb.ToString();
		}
		#endregion HC Title Transfer

		#region Warehouse Release
		private string WarehouseRelease(string shipmentOrderID, string shipmentSKU)
		{
			DBLib db = new DBLib(this.Context);
			db.OpenConnection();

			string quantity="";
			string product="";
			string releaseNumberFromSeller = txtCustom.Text;
			string LocationName="";
			string locContact="";
			string locPhone="";
			string locFax="";
			
			string sqlOrderTo="select (L.LOCL_CITY+', '+L.LOCL_STAT) LocationName, P.PLAC_TYPE locType, PLAC_PERS, PLAC_PHON, PLAC_INST, PLAC_FAX " + 
				"from SHIPMENT S, LOCALITY L, PLACE P " +
				"where S.SHIPMENT_TO = P.PLAC_ID " +
				"	and P.PLAC_LOCL = L.LOCL_ID " +
				"  and S.SHIPMENT_ORDR_ID='"+shipmentOrderID.ToString().Trim() + "'" + 
				"  and S.SHIPMENT_SKU = '" + shipmentSKU.ToString().Trim() + "'";
			SqlDataReader rd= db.ExecuteReader(sqlOrderTo);
			if(rd.Read())
			{
				LocationName=HelperFunction.getSafeStringFromDB(rd["PLAC_INST"]);
				locContact = HelperFunction.getSafeStringFromDB(rd["plac_pers"]);
				locPhone = HelperFunction.getSafeStringFromDB(rd["plac_phon"]);
				locFax = HelperFunction.getSafeStringFromDB(rd["plac_fax"]);
			}
			rd.Close();

			//Gathering info about Orders
			SqlDataReader dtOrder = db.ExecuteReader("SELECT * FROM ORDERS,SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ORDR_ID = '" + shipmentOrderID + "' and SHIPMENT_SKU ='" + shipmentSKU + "'");
			dtOrder.Read();
			product = HelperFunction.getSafeStringFromDB(dtOrder["ORDR_PROD_SPOT"]);
			quantity = HelperFunction.getSafeStringFromDB(dtOrder["SHIPMENT_WEIGHT"]) + " lbs";
			if (quantity.Trim() == " lbs") quantity = "0 lbs";
			dtOrder.Close();
			
			//creat email body
			System.Text.StringBuilder sb=new System.Text.StringBuilder("");

			sb.Append(HTMLHeader());

			sb.Append("Date: " + DateTime.Today.ToShortDateString() + "<br>");
			sb.Append("To: "+LocationName+"<br>");
			sb.Append("Fax: " + locFax + "<br><br><br>");
			sb.Append("Dear " + locContact + ",<br><br>");
			
			sb.Append(getCompanyNameSeller(shipmentOrderID,shipmentSKU));
			sb.Append(" releases to The Plastics Exchange ");
			sb.Append(quantity + " - " + product);
			sb.Append(", under the release #" + releaseNumberFromSeller + ", please release this material to ");
			sb.Append(getCompanyNameBuyer(shipmentOrderID,shipmentSKU));
			sb.Append(@" under the release #" + shipmentOrderID + "-" + shipmentSKU + ".");
			sb.Append(@"<br><br>");
			sb.Append(@"We would appreciate if " + LocationName + " would help to maintain the anonymity between all parts.");
			sb.Append(@"<br><br>");
			sb.Append(@"Please fax us copy of straight bill of landing upon pick up of material");
			sb.Append(@"<br>Thank you very much.");
			
			sb.Append(HTMLFooter("Helio Pimentel<br>Logistics Coordinator"));
			
			db.CloseConnection();
			return sb.ToString();
		}
		#endregion Warehouse Release

		#region Release Customer
		private string ReleaseCustomer(string shipmentOrderID, string shipmentSKU)
		{
			DBLib db = new DBLib(this.Context);
			db.OpenConnection();

			string strOrderNumber = shipmentOrderID + "-" + shipmentSKU;

			//Gathering info about Person
			SqlDataReader dtPerson = db.ExecuteReader("Select * from PERSON WHERE PERS_ID=(SELECT SHIPMENT_BUYR FROM SHIPMENT WHERE SHIPMENT_ID = (Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID = '" + shipmentOrderID + "' and SHIPMENT_SKU ='" + shipmentSKU + "'))");
			dtPerson.Read();
			string strBuyerName = HelperFunction.getSafeStringFromDB(dtPerson["PERS_FRST_NAME"]) + " " + HelperFunction.getSafeStringFromDB(dtPerson["PERS_LAST_NAME"]);
			string strBuyerFax = HelperFunction.getSafeStringFromDB(dtPerson["PERS_FAX"]);
			dtPerson.Close();

			//Gathering info about Shipment
			SqlDataReader dtShipment = db.ExecuteReader("SELECT (SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=PLAC_COMP),PLAC_ADDR_ONE,PLAC_ADDR_TWO,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL),PLAC_ZIP,(SELECT (SELECT CTRY_NAME FROM COUNTRY WHERE CTRY_CODE=LOCL_CTRY) FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) FROM PLACE WHERE PLAC_TYPE='H' AND PLAC_COMP=(SELECT (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR) FROM SHIPMENT WHERE SHIPMENT_ORDR_ID = '" + shipmentOrderID + "' and SHIPMENT_SKU ='" + shipmentSKU + "')");
			dtShipment.Read();
			string strBuyerCompanyName = HelperFunction.getSafeStringFromDB(dtShipment[0]);
			dtShipment.Close();

			string sqlOrderTo="select (L.LOCL_CITY+', '+L.LOCL_STAT) LOCATION_NAME, PLAC_PERS, PLAC_PHON, PLAC_INST, PLAC_ADDR_ONE, PLAC_ADDR_TWO, PLAC_ZIP " + 
				"from SHIPMENT S, LOCALITY L, PLACE P " +
				"where S.SHIPMENT_TO = P.PLAC_ID " +
				"	and P.PLAC_LOCL = L.LOCL_ID " +
				"  and S.SHIPMENT_ORDR_ID='"+shipmentOrderID.ToString().Trim() + "'" + 
				"  and S.SHIPMENT_SKU = '" + shipmentSKU.ToString().Trim() + "'";
			SqlDataReader dtLocality = db.ExecuteReader(sqlOrderTo);
			dtLocality.Read();
			string strPickupLocationName = HelperFunction.getSafeStringFromDB(dtLocality["PLAC_INST"]);
			string strPhoneNumber = HelperFunction.getSafeStringFromDB(dtLocality["PLAC_PHON"]);
			string strContact = HelperFunction.getSafeStringFromDB(dtLocality["PLAC_PERS"]);
			string strPickupLocationAddress = HelperFunction.getSafeStringFromDB(dtLocality["PLAC_ADDR_ONE"]);
			string strPickupLocationAddress2 = HelperFunction.getSafeStringFromDB(dtLocality["PLAC_ADDR_TWO"]);
			string strPickupLocationZipCode = HelperFunction.getSafeStringFromDB(dtLocality["PLAC_ZIP"]);
			string strPickupLocationCityState = HelperFunction.getSafeStringFromDB(dtLocality["LOCATION_NAME"]);
			dtLocality.Close();
            
			//Gathering info about Orders
			SqlDataReader dtOrder = db.ExecuteReader("SELECT * FROM ORDERS,SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ORDR_ID = '" + shipmentOrderID + "' and SHIPMENT_SKU ='" + shipmentSKU + "'");
			dtOrder.Read();
			string strProductDescription = HelperFunction.getSafeStringFromDB(dtOrder["ORDR_PROD_SPOT"]);
			string strQuantity = HelperFunction.getSafeStringFromDB(dtOrder["SHIPMENT_WEIGHT"]) + " lbs";
			string strPONumber = HelperFunction.getSafeStringFromDB(dtOrder["SHIPMENT_PO_NUM"]);
			dtOrder.Close();

			string HTMLSpaces = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

			System.Text.StringBuilder sb=new System.Text.StringBuilder("");
			sb.Append(HTMLHeader());

			sb.Append("Date: " + DateTime.Today.ToShortDateString() + "<br>");
			sb.Append("To: " + strBuyerName + "<br>");
			sb.Append("Fax: " + strBuyerFax + "<br><br>");

			sb.Append("<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Release Information</b><br><br>");

			sb.Append("Customer's Name: " + strBuyerCompanyName + "<BR><BR>" + bl());
			sb.Append("Customer PO #: " + strPONumber + "<BR><BR>" + bl());
			sb.Append("TPE Reference #: " + strOrderNumber + "<BR><BR>" + bl());
			sb.Append("Product: " + strProductDescription + "<BR><BR>" + bl());
			sb.Append("Quantity: " + strQuantity + "<BR><BR><BR>" + bl());
			sb.Append("Pick up Location: " + strPickupLocationName + "<BR>" + bl());
			sb.Append(HTMLSpaces + HTMLSpaces + HTMLSpaces + strPickupLocationAddress + "<BR>" + bl());
			if (strPickupLocationAddress2.Trim().Length>0) sb.Append(HTMLSpaces + HTMLSpaces + HTMLSpaces + strPickupLocationAddress2 + "<BR>" + bl());
			sb.Append(HTMLSpaces + HTMLSpaces + HTMLSpaces + strPickupLocationCityState + " - " + strPickupLocationZipCode + "<BR>" + bl());
			sb.Append("<BR>Phone N�.: " + strPhoneNumber + "<BR><BR>" + bl());
			sb.Append("Contact: " + strContact + "<BR><BR>" + bl());
			sb.Append("TPE Release #: " + strOrderNumber + "<BR><BR><BR>" + bl());
			
			sb.Append("Feel free to contact me if you have any other questions.<BR>" + bl());

			sb.Append(HTMLFooter("Helio Pimentel<br>Logistics Coordinator"));
			
			db.CloseConnection();
			return sb.ToString();
		}
		#endregion Release Customer

		#region Invoice, Purchase Order, Sales Confirmation
		private string InvoicePOSalesConfirmation(string shipmentOrderID, string shipmentSKU, string DocumentType)
		{
			DBLib db = new DBLib(this.Context);
			db.OpenConnection();
			string ShipmentID = "";

			string strLocation="";
			System.DateTime time;

			//Looking for Shipment ID
			SqlDataReader drShipmentID = db.ExecuteReader("Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID="+shipmentOrderID+ "and SHIPMENT_SKU ='"+shipmentSKU+"'");
			drShipmentID.Read();
			ShipmentID = drShipmentID["SHIPMENT_ID"].ToString();
			drShipmentID.Close();
			
			StringBuilder StrEmail = new StringBuilder("");
			StrEmail.Append(HTMLHeader("center"));
			StrEmail.Append("<table border=0 cellspacing=0 cellpadding=0 width='650' align='center'>");
			StrEmail.Append("<tr><td colspan=3><center><SPAN STYLE='BACKGROUND-COLOR:#CCCCCC;FONT: BOLD 10pt ARIAL'>");
			
			//defining title
			if(DocumentType == "PO") StrEmail.Append("PURCHASE ORDER");
			else if(DocumentType == "SC") StrEmail.Append("SALES CONFIRMATION");
			else StrEmail.Append("INVOICE");

			StrEmail.Append("</SPAN></center><br><br></td></tr>");
			StrEmail.Append("<tr valign=top>");
			string strSQL ="";
			strSQL +="   SELECT ";
			strSQL +="   COMP_NAME=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=PLAC_COMP),";
			strSQL +="   PLAC_ADDR_ONE,";
			strSQL +="   PLAC_ADDR_TWO,";
			strSQL +="   (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL),";
			strSQL +="   PLAC_ZIP,";
			strSQL +="   (SELECT ";
			strSQL +="	 (SELECT CTRY_NAME FROM COUNTRY WHERE CTRY_CODE=LOCL_CTRY) ";
			strSQL +="	 FROM LOCALITY";
			strSQL +="	 WHERE LOCL_ID=PLAC_LOCL)";
			strSQL +="   FROM PLACE ";
			strSQL +="   WHERE PLAC_TYPE='H' ";
			strSQL +="   AND PLAC_COMP=(SELECT ";
			strSQL +="	(SELECT PERS_COMP FROM PERSON ";
			if(DocumentType=="PO")//select seller's company
			{
				strSQL +=" WHERE PERS_ID=SHIPMENT_SELR) ";
			}
			else//select buyer's company
			{
				strSQL +=" WHERE PERS_ID=SHIPMENT_BUYR) ";
			}
			strSQL += "FROM ORDERS,SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ID="+ShipmentID+") ";

			SqlDataReader drOrder = db.ExecuteReader(strSQL);
			if(drOrder.Read())
			{
				StrEmail.Append("<td><SPAN STYLE='FONT: 8pt ARIAL'><b><i>");
				if(DocumentType == "PO")
				{
					StrEmail.Append("Purchased From:");
					StrEmail.Append("</i></b><br>"+drOrder["COMP_NAME"].ToString()+"<br>"+drOrder["PLAC_ADDR_ONE"].ToString());
				}
				else
				{
					StrEmail.Append("Sold to:");
					StrEmail.Append("</i></b><br>"+drOrder["COMP_NAME"].ToString()+"<br>"+drOrder["PLAC_ADDR_ONE"].ToString());
				}
				if(drOrder["PLAC_ADDR_TWO"]!=null)
				{
					if(drOrder["PLAC_ADDR_TWO"].ToString().Length!=0)
					{
						StrEmail.Append("<br>"+drOrder["PLAC_ADDR_TWO"].ToString());
					}
				}
				StrEmail.Append("<br>"+drOrder[3]+"  "+drOrder["PLAC_ZIP"]+"</SPAN></td>");
			}
			drOrder.Close();
			
			//determines the ship term is FOB/Shipping or FOB/Delivered or Delivered Tpe
			string ShipMethod="";
			SqlDataReader drShipment = db.ExecuteReader("SELECT SHIPMENT_FOB FROM SHIPMENT WHERE SHIPMENT_ID="+ShipmentID);
			if(drShipment.Read())
			{
				ShipMethod = HelperFunction.getSafeStringFromDB(drShipment["SHIPMENT_FOB"]);
			}
			drShipment.Close();

			//Building SQL query into a string(easy to read) returns company, Headquarters address info will be displayed in the right side of the screen
			strSQL ="";
			strSQL +="SELECT ";
			strSQL +="   PLAC_ADDR_ONE, PLAC_ADDR_TWO,";
			strSQL +="   PLAC_INST, PLAC_PHON,";
			strSQL +="   (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) As Location,";
			strSQL +="   (SELECT PLAC_ZIP FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) As Zip";
			strSQL +=" FROM ";
			strSQL +=" PLACE ";
			
			//display address based on ship term for FOB/shipping
			if(ShipMethod=="FOB/Shipping")
			{
				strSQL += " WHERE PLAC_ID=(SELECT SHIPMENT_FROM FROM SHIPMENT WHERE SHIPMENT_ID="+ShipmentID+")";
			}
			else //ShipMethod=="FOB/Delivered"
			{
				strSQL += " WHERE PLAC_ID=(SELECT SHIPMENT_TO FROM SHIPMENT WHERE SHIPMENT_ID="+ShipmentID+")";
			}

			SqlDataReader drPlace = db.ExecuteReader(strSQL);
			drPlace.Read();
			string dress2 = HelperFunction.getSafeStringFromDB(drPlace["PLAC_INST"]);
			string PlaceAddOne = HelperFunction.getSafeStringFromDB(drPlace["PLAC_ADDR_ONE"]);
			string PlaceAddTwo = HelperFunction.getSafeStringFromDB(drPlace["PLAC_ADDR_TWO"]);
			string Location = HelperFunction.getSafeStringFromDB(drPlace["Location"]);
			string Phone = HelperFunction.getSafeStringFromDB(drPlace["PLAC_PHON"]);
			string Zip = HelperFunction.getSafeStringFromDB(drPlace["Zip"]);
			drPlace.Close();
			StrEmail.Append("<td><SPAN STYLE='FONT: 8pt ARIAL'><b><i>");
			if(DocumentType == "PO")
				if(ShipMethod=="FOB/Shipping")
					StrEmail.Append("Our Pickup:</i></b>");
				else
					if(ShipMethod=="FOB/Delivered")
						StrEmail.Append("Delivered To:</i></b>");
					else
						if(ShipMethod=="Delivered Tpe")
							StrEmail.Append("Our Pickup:</i></b>");
						else
							StrEmail.Append("Our Pickup:</i></b>");
			else
				if(ShipMethod=="FOB/Shipping")
					StrEmail.Append("Your Pickup:</i></b>");
				else
					if(ShipMethod=="FOB/Delivered")
						StrEmail.Append("Delivered To:</i></b>");
					else
						if(ShipMethod=="Delivered Tpe")
							StrEmail.Append("Delivered To:</i></b>");
						else
							StrEmail.Append("Ship to:</i></b>");

			if(ShipMethod=="Delivered Tpe")
			{
				strSQL ="";
				strSQL +="SELECT ";
				strSQL +="   PLAC_ADDR_ONE, ";
				strSQL +="   PLAC_ADDR_TWO,";
				strSQL +="   PLAC_INST,";
				strSQL +="   PLAC_PHON,";
				strSQL +="   (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) As Location,";
				strSQL +="   (SELECT PLAC_ZIP FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) As Zip";
				strSQL +=" FROM ";
				strSQL +=" PLACE ";
				strSQL += " WHERE PLAC_ID=(SELECT SHIPMENT_FROM FROM SHIPMENT WHERE SHIPMENT_ID="+ShipmentID+")";
				
				SqlDataReader drPlace2 = db.ExecuteReader(strSQL);
				drPlace2.Read();
				if(DocumentType == "PO")
				{
					if(HelperFunction.getSafeStringFromDB(drPlace2["PLAC_INST"]).Trim().Length>0)
					{
						StrEmail.Append("<br>"+drPlace2["PLAC_INST"].ToString()+"");
					}
					StrEmail.Append("<br>"+drPlace2["PLAC_ADDR_ONE"].ToString()+"<br>");
					
					if(HelperFunction.getSafeStringFromDB(drPlace2["PLAC_ADDR_TWO"]).Trim().Length>0)
					{
						StrEmail.Append(drPlace2["PLAC_ADDR_TWO"].ToString()+"<br>");
					}
					strLocation = drPlace2["Location"].ToString();
					StrEmail.Append(drPlace2["Location"].ToString() + "  " + drPlace2["Zip"].ToString());
					if(HelperFunction.getSafeStringFromDB(drPlace2["PLAC_PHON"]).Trim().Length>0)
					{
						StrEmail.Append("<br>"+drPlace2["PLAC_PHON"].ToString()+"<br>");
					}
					StrEmail.Append("</SPAN></td>");
					StrEmail.Append("<td align=right>");
				}
				drPlace2.Close();
			}

			if ((ShipMethod!="Delivered Tpe") || (DocumentType!="PO"))
			{
				if(dress2.Length>0)
				{
					StrEmail.Append("<br>"+dress2+"");
				}
				StrEmail.Append("<br>"+PlaceAddOne+"<br>");
				if(PlaceAddTwo.Length>0)
				{
					StrEmail.Append(PlaceAddTwo+"<br>");
				}
				strLocation=Location;
				StrEmail.Append(Location+"  "+Zip+"");
				if(Phone.Length>0)
				{
					StrEmail.Append("<br>"+Phone+"<br>");
				}
				StrEmail.Append("</SPAN></td>");
				StrEmail.Append("<td align=right>");
			}
			
			strSQL="";
			strSQL +=" SELECT ";
			strSQL +="    ORDR_DATE = CONVERT(varchar,ORDR_DATE,101), ";
			strSQL +="    ORDR_ID, ";
			strSQL +="    ORDR_BID, ";
			strSQL +="    SHIPMENT_BUYR_PRCE*(ISNULL(SHIPMENT_WEIGHT,SHIPMENT_QTY*(SHIPMENT_SIZE))), ";
			strSQL +="    ISNULL(SHIPMENT_BUYER_TERMS,'30'), ";
			strSQL +="    SHIPMENT_QTY, ";
			strSQL +="    SHIPMENT_SIZE, ";
			strSQL +="    SHIPMENT_WEIGHT, ";
			strSQL +="    SHIPMENT_PRCE, ";
			strSQL +="    SHIPMENT_DATE_TAKE, ";
			strSQL +="    (SELECT CONT_LABL FROM CONTRACT WHERE CONT_ID=ORDR_CONT), ";
			strSQL +="    (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=SHIPMENT_TO)), ";
			strSQL +="    BUYERDUEDATE = DATEADD(day,CAST(SHIPMENT_BUYER_TERMS AS Integer),SHIPMENT_DATE_TAKE), ";
			//strSQL +="    SELLERDUEDATE = DATEADD(day,SHIPMENT_SELLER_TERMS,CAST(SHIPMENT_DATE_TAKE AS INTEGER)), ";
			strSQL +="    VARDATE=CONVERT(varchar(14),SHIPMENT_DATE_TAKE,101), ";
			strSQL +="    (SELECT CASE WHEN SHIPMENT_WEIGHT IS NULL THEN '0' ELSE '1' END), ";
			strSQL +="    ISNULL(SHIPMENT_WEIGHT,SHIPMENT_QTY*(SHIPMENT_SIZE)) AS Lbs, ";
			strSQL +="    VARMONTH=(SELECT FWD_MNTH+' '+RIGHT(CONVERT(varchar,FWD_YEAR),2) FROM FWDMONTH WHERE FWD_ID=SHIPMENT_MNTH), ";
			strSQL +="    SHIPMENT_INVOICE_SENT, ";
			strSQL +="    SHIPMENT_INVOICE_EMAIL_TO, ";
			strSQL +="    SHIPMENT_SKU, ";
			strSQL +="    SHIPMENT_PRCE, ";
			strSQL +="    SHIPMENT_PO_SENT, ";
			strSQL +="    SHIPMENT_PO_EMAIL_TO, ";
			strSQL +="    SHIP=(SELECT TOP 1 SHIPMENT_DATE_TAKE FROM SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID), ";
			strSQL +="    ORDR_PROD_SPOT, ";
			strSQL +="    ORDR_ISSPOT, ";
			strSQL +="    SHIPMENT_SKU, ";
			strSQL +="    SHIPMENT_SELLER_TERMS, " ;
			strSQL +="    SHIPMENT_BUYER_TERMS, " ;
			strSQL +="    SHIPMENT_FOB, ";
			strSQL +="    ORDR_MELT, ";
			strSQL +="    ORDR_DENS, ";
			strSQL +="    SHIPMENT_PO_NUM, ";
			strSQL +="    ORDR_ADDS, ";
			strSQL +="    SHIPMENT_BUYR_PRCE ";
			strSQL +=" FROM ORDERS, SHIPMENT ";
			strSQL +=" WHERE SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ID="+ShipmentID;

			SqlDataReader drOrder2 = db.ExecuteReader(strSQL);
			drOrder2.Read();
			StrEmail.Append("<table border=1 cellspacing=0 >");
			//Display P.O number if it's available, only for Invoices and Sales Confirmation
			if(DocumentType != "PO")
			{
				if(HelperFunction.getSafeStringFromDB(drOrder2["SHIPMENT_PO_NUM"]).Trim().Length>0)
				{
					StrEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC width=100>P.O. #</td><td STYLE='FONT: 8pt ARIAL' align=left>&nbsp;"+drOrder2["SHIPMENT_PO_NUM"].ToString()+"");
					StrEmail.Append("</td></tr>");
				}
			}
			//Display transaction number
			StrEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC width=100>Transaction #</td><td STYLE='FONT: 8pt ARIAL' align=left>&nbsp;"+drOrder2["ORDR_ID"].ToString()+"");
			if(drOrder2["SHIPMENT_SKU"]==null)
			{
				StrEmail.Append("</td></tr>");
			}
			else
			{
				StrEmail.Append("-"+drOrder2["SHIPMENT_SKU"].ToString()+"</td></tr>");
			}
			StrEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Transaction Date</td><td STYLE='FONT: 8pt ARIAL' align=left>&nbsp;"+ String.Format("{0:MM/dd/yyyy}",drOrder2["ORDR_DATE"].ToString()) +"</td></tr>");
			bool checkIsShipping;
			if(DocumentType != "SC")
			{
				if(drOrder2["SHIP"].ToString().Length==0)
				{
					StrEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>");
					if(ShipMethod=="Delivered Tpe")
					{
						if(DocumentType == "PO")
						{
							StrEmail.Append("Pickup Date");
						}
						else
						{
							StrEmail.Append("Shipping Date");
							checkIsShipping=true;
						}
					}
					else
					{
						if(ShipMethod=="FOB/Delivered")
						{
							StrEmail.Append("Shipping Date");
							checkIsShipping=true;
						}
						else
						{
							if(ShipMethod=="FOB/Shipping")
							{
								if(DocumentType == "PO")
									StrEmail.Append("Pickup Date");
								else
									StrEmail.Append("Release Date");
							}
							else
							{
								StrEmail.Append("Shipping Date");
								checkIsShipping=true;
							}
						}
					}
					
					if(DocumentType == "PO")
					{
						StrEmail.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left><font color=red>&nbsp;ASAP</td></tr>");
					}
					else
					{
						if(drOrder2["SHIPMENT_DATE_TAKE"] is DBNull)
						{
							time=Convert.ToDateTime(drOrder2["ORDR_DATE"].ToString());
							time.AddDays(10);
							StrEmail.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left>&nbsp;"+ExchangeTimeFormat(time.ToString())+"</font></td></tr>");
							
						}
						else
						{
							StrEmail.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left>&nbsp;"+drOrder2["SHIPMENT_DATE_TAKE"].ToString()+"</font></td></tr>");
						}
					}
				}
				else
				{
					StrEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>");
					
					if(ShipMethod=="Delivered Tpe")
						if(DocumentType == "PO")
							StrEmail.Append("Pickup Date");
						else
							StrEmail.Append("Shipping Date");
					else
						if(ShipMethod=="FOB/Delivered")
							StrEmail.Append("Shipping Date");
						else
							if(ShipMethod=="FOB/Shipping")
								if(DocumentType == "PO")
									StrEmail.Append("Pickup Date");
								else
									StrEmail.Append("Release Date");
							else
								StrEmail.Append("Shipping Date");
					
					if (DocumentType == "PO")
						StrEmail.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left><font color=red>&nbsp;ASAP</td></tr>");
					else
						if(drOrder2["SHIPMENT_DATE_TAKE"] is DBNull)
							StrEmail.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left><font color=red>&nbsp;"+ExchangeTimeFormat(drOrder2["VARDATE"].ToString())+"</td></tr>");
						else
							StrEmail.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left><font color=red>&nbsp;"+ExchangeTimeFormat(drOrder2["SHIPMENT_DATE_TAKE"].ToString())+"</td></tr>");
				}
			}
			StrEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Transaction Value</td><td STYLE='");
			if(DocumentType == "PO")
			{
				double money=Convert.ToDouble(drOrder2["lbs"].ToString())*Convert.ToDouble(drOrder2["SHIPMENT_PRCE"].ToString());
				StrEmail.Append("FONT: 8pt ARIAL' align=left>&nbsp"+money.ToString("c")+"</td></tr>");
			}
			else
			{
				double m=Convert.ToDouble(drOrder2[3].ToString());
				StrEmail.Append("FONT: 8pt ARIAL' align=left>&nbsp"+m.ToString("c")+"</td></tr>");
			}
			if(DocumentType != "SC")
			{
				if(DocumentType != "PO")
				{
					if(drOrder2["SHIPMENT_DATE_TAKE"] is DBNull)
					{
						//if(rd["DAYS"].ToString().Trim().Length==0)
						//{
						time=Convert.ToDateTime(drOrder2["ORDR_DATE"].ToString());
						time.AddDays(30);
						StrEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Payment Due</td><td STYLE='FONT: 8pt ARIAL' align=left><b>&nbsp;"+ExchangeTimeFormat(time.ToString())+"</b></td></tr>");
					}
					else
					{
						//time=Convert.ToDateTime(rd["SHIPMENT_DATE_TAKE"].ToString());
						//time.AddDays(Convert.ToInt32(rd["SHIPMENT_BUYER_TERMS"].ToString()));
						StrEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Payment Due</td><td STYLE='FONT: 8pt ARIAL' align=left><b>&nbsp;"+ExchangeTimeFormat(drOrder2["BUYERDUEDATE"].ToString())+"</b></td></tr>");
					}
				}
				/*
				else
				{
					if(drOrder2["VARDATE"].ToString().Trim().Length==0)
					{
						time=Convert.ToDateTime(drOrder2["VARDATE"].ToString());
						time.AddDays(Convert.ToInt32(drOrder2["DAYS"].ToString()));
						StrEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Payment Due</td><td STYLE='FONT: 8pt ARIAL' align=left><b>&nbsp;"+ExchangeTimeFormat(time.ToString())+"</b></td></tr>");
					}
					else
					{
						time=Convert.ToDateTime(drOrder2["VARDATE"].ToString());
						time.AddDays(Convert.ToInt32(drOrder2["DAYS"].ToString()));
						StrEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Payment Due</td><td STYLE='FONT: 8pt ARIAL' align=left><b>&nbsp;"+ExchangeTimeFormat(time.ToString())+"</b></td></tr>");
					}
				}
				*/
			}
			StrEmail.Append("</table>");
			StrEmail.Append("</tr>");
			StrEmail.Append("<tr valign=top><td align=center colspan=3>");
			StrEmail.Append("<br>");
			StrEmail.Append("<table border=1 cellspacing=0 width=100% >");
			if(drOrder2["SHIPMENT_SIZE"].ToString()=="1")
			{
				StrEmail.Append("<tr bgcolor=#CCCCCC>");
				StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Size (lbs)</td>");
			}
			else
			{
				StrEmail.Append("<tr bgcolor=#CCCCCC>");
				StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Amount</td>");
				StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Size</td>");
				if(drOrder2["SHIPMENT_WEIGHT"] is DBNull)
				{
					StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Lbs<font color=red>&nbsp(Est)</td>");
				}
				else
				{
					StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Lbs</td>");
				}
			}
			StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>$/Lb</td>");
			StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Product</td>");
			
			if(DocumentType == "PO")
				if(ShipMethod=="FOB/Shipping")
					StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Our Pickup</td>");
				else
					if( ShipMethod=="FOB/Delivered")
						StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Delivered To</td>");
					else
						if(ShipMethod=="Delivered Tpe")
							StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Our Pickup</td>");
						else
							StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Our Pickup</td>");
			else
				if(ShipMethod=="FOB/Shipping")
					StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Your Pickup</td>");
				else
					if( ShipMethod=="FOB/Delivered")
						StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Delivered To</td>");
					else
						if(ShipMethod=="Delivered Tpe")
							StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Delivered To</td>");
						else
							StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Ship to</td>");
			
			StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Terms</td>");
			StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Total Amount</td>");
			StrEmail.Append("</tr>");
			StrEmail.Append("<tr>");
			if(drOrder2["SHIPMENT_SIZE"].ToString()=="1")
			{
				if(drOrder2["SHIPMENT_WEIGHT"].ToString().Trim().Length==0)
				{
					StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+String.Format("{0:#,###}",drOrder2["SHIPMENT_QTY"].ToString()).ToString()+"<font color=red>&nbsp(Est)</font></td>");
				}
				else
				{
					StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+String.Format("{0:#,###}",drOrder2["SHIPMENT_WEIGHT"].ToString()).ToString()+"</td>");
				}
			}
			else
			{
				StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+String.Format("{0:#,###}", 
					drOrder2["SHIPMENT_QTY"].ToString()).ToString()+"</td>");
				if(drOrder2["SHIPMENT_SIZE"]is DBNull )
				{
					StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>");
				}
				else
				{
					if(drOrder2["SHIPMENT_SIZE"].ToString()=="42000")
					{
						StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Truckload");
					}
					if(drOrder2["SHIPMENT_SIZE"].ToString()=="1")
					{
						StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Lb");
					}
					if(drOrder2["SHIPMENT_SIZE"].ToString()=="2.205")
					{
						StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Kilo");
					}
					if(drOrder2["SHIPMENT_SIZE"].ToString()=="190000")
					{
						StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Railcar");
					}
					if(drOrder2["SHIPMENT_SIZE"].ToString()=="45000")
					{
						StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Bulk Truck");
					}
					if(drOrder2["SHIPMENT_SIZE"].ToString()=="44092")
					{
						StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Truckload Bag");
					}
					if(drOrder2["SHIPMENT_SIZE"].ToString()=="2205")
					{
						StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Metric Ton");
					}
					if(drOrder2["SHIPMENT_SIZE"].ToString()=="38500")
					{
						StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>20 Container");
					}
					if(drOrder2["SHIPMENT_SIZE"].ToString()=="44500")
					{
						StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>30 Container");
					}
				}
				if(drOrder2["SHIPMENT_QTY"].ToString()!="1")
				{
					StrEmail.Append("s");
					StrEmail.Append("</td>");
				}
				if(drOrder2["SHIPMENT_WEIGHT"] is DBNull)
				{
					StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'><font color=red>"+String.Format("{0:#,###}",Convert.ToInt32(drOrder2["lbs"].ToString())).ToString()+"</td>");
				}
				else
				{
					StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+String.Format("{0:#,###}",Convert.ToInt32(drOrder2["lbs"].ToString())).ToString()+"</td>");
				}
			}
			if(DocumentType == "PO")
			{
				StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+String.Format("${0:#,###0.0000}",Convert.ToDouble(drOrder2["SHIPMENT_PRCE"].ToString()))+"</td>");
			}
			else
			{
				StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+String.Format("{0:#,###0.0000}",Convert.ToDouble(drOrder2["SHIPMENT_BUYR_PRCE"].ToString()))+"</td>");
			}
			if(Convert.ToBoolean(drOrder2["ORDR_ISSPOT"]))
			{
				StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+drOrder2["ORDR_PROD_SPOT"].ToString());
				if(HelperFunction.getSafeStringFromDB(drOrder2["ORDR_MELT"]).Trim().Length!=0)
				{
					StrEmail.Append(" - "+drOrder2["ORDR_MELT"].ToString()+" melt");
				}
				
				if(HelperFunction.getSafeStringFromDB(drOrder2["ORDR_DENS"]).Trim().Length!=0)
				{
					StrEmail.Append(" - "+drOrder2["ORDR_DENS"].ToString()+" density");
				}

				if(HelperFunction.getSafeStringFromDB(drOrder2["ORDR_ADDS"]).Trim().Length>0)
				{
					StrEmail.Append(" - "+drOrder2["ORDR_ADDS"].ToString());
				}
			}
			else
			{
				StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+drOrder2[9].ToString());
			}
			StrEmail.Append("</td>");
			StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+ strLocation + "</td>");
			if(DocumentType == "PO")
			{
				if(drOrder2["SHIPMENT_SELLER_TERMS"].ToString()=="0")
				{
					StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Cash</td>");
				}
				else
				{
					StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+drOrder2["SHIPMENT_SELLER_TERMS"].ToString()+" days</td>");
				}
			}
			else
			{
				if(drOrder2["SHIPMENT_BUYER_TERMS"].ToString()=="0")
				{
					StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Cash</td>");
				}
				else
				{
					StrEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+drOrder2["SHIPMENT_BUYER_TERMS"].ToString()+" days</td>");
				}
			}
			StrEmail.Append("<td STYLE='");
			if(drOrder2[13].ToString()=="0")
			{
				StrEmail.Append("");
			}
			if(DocumentType == "PO")
			{
				double money=Convert.ToDouble(drOrder2["lbs"].ToString())*Convert.ToDouble(drOrder2["SHIPMENT_PRCE"].ToString());
				StrEmail.Append("FONT: 8pt ARIAL'>"+money.ToString("C")+"</td>");
			}
			else
			{
				StrEmail.Append("FONT: 8pt ARIAL'>"+String.Format("{0:C}",Convert.ToDouble(drOrder2[3].ToString()))+"</td>");
			}
			StrEmail.Append("</tr>");
			StrEmail.Append("</table><br>");
			StrEmail.Append("</td></tr>");
			StrEmail.Append("<br><tr><td colspan=4 Style'FONT: 8pt ARIAL'>");
			string shipment_size=drOrder2["SHIPMENT_SIZE"].ToString();
			string shipment_fob=drOrder2["SHIPMENT_FOB"].ToString();
			drOrder2.Close();
			if(DocumentType == "PO")
			{
				if(shipment_fob!="FOB/Delivered")
				{
					StrEmail.Append("<span STYLE='FONT: 8pt ARIAL'>Please provide release # ASAP.</span><BR><BR>");
				}
			}
			else
			{
				if(DocumentType != "SC")
				{
					if(shipment_size=="190000")
					{
						SqlDataReader drShipComment = db.ExecuteReader("SELECT SHIPMENT_COMMENT FROM SHIPMENT WHERE SHIPMENT_ID="+ShipmentID);
						if (drShipComment.Read())
						{
							if(!(drShipComment["SHIPMENT_COMMENT"]is DBNull))
							{
								StrEmail.Append("<span STYLE='FONT: 8pt ARIAL'>Railcar # ");
								do
								{
									StrEmail.Append(drShipComment["SHIPMENT_COMMENT"].ToString()+"</span><br>");
									
								}while(drShipComment.Read());
								drShipComment.Close();
							}
							else
							{
								StrEmail.Append("</span><Br>");
							}
						}
						drShipComment.Close();
					}
				}
			}
			
			//Email Bottom
			if(DocumentType != "PO")
			{
				StrEmail.Append("<span STYLE='FONT: 8pt ARIAL'>Thank you for your order.");
				if ((txtCustom.Text.Trim().Length>0))
					StrEmail.Append("<BR><b>"+txtCustom.Text+"</b></span><br></td></tr>");
				else
					StrEmail.Append("</span><br>");
				if(DocumentType != "SC")
				{
					StrEmail.Append("<tr><td colspan=2 valign=top align=left><span STYLE='FONT: 8pt ARIAL'>Please reference transaction # in your payments.</span><br><br><br></span></td></tr>");
				}
			}
			StrEmail.Append("<tr valign=top>");
			if((DocumentType != "PO")&&(DocumentType != "SC"))
			{
				StrEmail.Append("<td colspan=1><SPAN STYLE='FONT: 8pt ARIAL'><b><i>CHECK REMITTANCE INSTRUCTIONS:</i></b><br>The Plastics Exchange<br>Department 20-3041<BR>PO Box 5977<BR>Carol Stream, IL 60197-5977</SPAN></td>");
				StrEmail.Append("<td colspan=1><SPAN STYLE='FONT: 8pt ARIAL'><b><i>WIRE TRANSFER INSTRUCTIONS:</i></b><br>American Chartered Bank<br>Elk Grove Village, IL 60007<BR>ABA # 071 925 046<BR><BR>For Further Credit:<BR>The Plastics Exchange<BR>Account #: 1147143</SPAN><BR><BR><BR></td>");
			}
			StrEmail.Append("</tr>");
			//StrEmail.Append("<tr><td colspan=3 valign=top align=center></td></tr>");
			//StrEmail.Append("<tr><td colspan=3 valign=top align=center><font face=arial size=1 color=Black>730 North LaSalle, 3rd Floor</font>&nbsp;&nbsp;<font face=arial size=1 color=red>Chicago, IL 60610</font>&nbsp;&nbsp;<font face=arial size=1 color=Black><b>tel 312.202.0002</b></font>&nbsp;&nbsp;<font face=arial size=1 color=red>fax 312.202.0174</font></td></tr>");
			StrEmail.Append("</table>");
			StrEmail.Append("<BR>");
			
			StrEmail.Append(HTMLFooter());
			return StrEmail.ToString();
		}
		#endregion

		private string getEmailBuyer(string shipmentOrderID, string shipmentSKU)
		{
			string email = "";
			DBLib db = new DBLib(this.Context);

			db.OpenConnection();
			SqlDataReader sqlDTPers = db.ExecuteReader("Select PERS_MAIL from PERSON WHERE PERS_ID=(Select SHIPMENT_BUYR FROM SHIPMENT where SHIPMENT_ORDR_ID = '"+shipmentOrderID+"' and SHIPMENT_SKU ='"+shipmentSKU+"')");
			sqlDTPers.Read();
			email = sqlDTPers["PERS_MAIL"].ToString();
			sqlDTPers.Close();
			db.CloseConnection();

			return email;
		}

		private string getEmailWarehouse(string shipmentOrderID, string shipmentSKU)
		{
			string email = "";
			DBLib db = new DBLib(this.Context);

			db.OpenConnection();
			string sqlOrderTo="SELECT PLAC_EMAIL FROM SHIPMENT S, LOCALITY L, PLACE P WHERE S.SHIPMENT_TO = P.PLAC_ID AND P.PLAC_LOCL = L.LOCL_ID AND S.SHIPMENT_ORDR_ID='"+shipmentOrderID.ToString().Trim() + "' AND S.SHIPMENT_SKU = '" + shipmentSKU.ToString().Trim() + "'";
			SqlDataReader sqlDTPers = db.ExecuteReader(sqlOrderTo);
			sqlDTPers.Read();
			email = sqlDTPers["PLAC_EMAIL"].ToString();
			sqlDTPers.Close();
			db.CloseConnection();

			return email;
		}

		private string getEmailSeller(string shipmentOrderID, string shipmentSKU)
		{
			string email = "";
			DBLib db = new DBLib(this.Context);
			db.OpenConnection();
			SqlDataReader sqlDTPers = db.ExecuteReader("Select PERS_MAIL from PERSON WHERE PERS_ID=(Select SHIPMENT_SELR FROM SHIPMENT where SHIPMENT_ORDR_ID = '"+shipmentOrderID+"' and SHIPMENT_SKU ='"+shipmentSKU+"')");
			sqlDTPers.Read();
			email = sqlDTPers["PERS_MAIL"].ToString();
			sqlDTPers.Close();
			db.CloseConnection();

			return email;
		}

		private string getCompanyNameSeller(string shipmentOrderID, string shipmentSKU)
		{
			string company = "";
			DBLib db = new DBLib(this.Context);
			db.OpenConnection();
			SqlDataReader sqlDTPers = db.ExecuteReader("Select COMP_NAME from COMPANY WHERE COMP_ID = (Select PERS_COMP from PERSON WHERE PERS_ID=(Select SHIPMENT_SELR FROM SHIPMENT where SHIPMENT_ORDR_ID = '"+shipmentOrderID+"' and SHIPMENT_SKU ='"+shipmentSKU+"'))");
			sqlDTPers.Read();
			company = sqlDTPers["COMP_NAME"].ToString();
			sqlDTPers.Close();
			db.CloseConnection();

			return company;
		}

		private string getCompanyNameBuyer(string shipmentOrderID, string shipmentSKU)
		{
			string company = "";
			DBLib db = new DBLib(this.Context);
			db.OpenConnection();
			SqlDataReader sqlDTPers = db.ExecuteReader("Select COMP_NAME from COMPANY WHERE COMP_ID = (Select PERS_COMP from PERSON WHERE PERS_ID=(Select SHIPMENT_BUYR FROM SHIPMENT where SHIPMENT_ORDR_ID = '"+shipmentOrderID+"' and SHIPMENT_SKU ='"+shipmentSKU+"'))");
			sqlDTPers.Read();
			company = sqlDTPers["COMP_NAME"].ToString();
			sqlDTPers.Close();
			db.CloseConnection();

			return company;
		}

		private string getEmailUser(string IDUser)
		{
			string email = "";
			DBLib db = new DBLib(this.Context);
			db.OpenConnection();
			SqlDataReader sqlDTPers = db.ExecuteReader("select PERS_MAIL from person WHERE PERS_ID = " + IDUser);
			sqlDTPers.Read();
			email = sqlDTPers["PERS_MAIL"].ToString();
			sqlDTPers.Close();
			db.CloseConnection();

			return email;
		}

		//break line
		private string bl()
		{
			return ((char)13).ToString();
		}

		private void SendEmail(string emailFrom, string emailTo, string emailCC, string emailSubject, string emailBody)
		{
			MailMessage email = new MailMessage();
			email.From = emailFrom;
			email.To = emailTo;
			email.Cc = emailCC;
			
			//if Mike is not one of the recievers, include him in the bcc.
			if ((emailTo != "michael@theplasticsexchange.com") && (emailCC != "michael@theplasticsexchange.com"))
			{
				if (email.Cc.Length>0) email.Cc += ";";
				email.Cc += "michael@theplasticsexchange.com";
			}

			if ((emailFrom == emailTo) && (emailTo == "helio@theplasticsexchange.com"))
			{
				//Mike's address would already be here
				email.Cc += ";zach@theplasticsexchange.com";
			}

			email.Subject = emailSubject;
			email.Body = emailBody;
			email.BodyFormat = MailFormat.Html;
			SmtpMail.Send(email);
		}

		private void PreviewEmail(string emailFrom, string emailTo, string emailCC, string emailSubject, string emailBody)
		{
			string CC=emailCC;

			//if Mike is not one of the recievers, include him in the bcc.
			if ((emailTo != "michael@theplasticsexchange.com") && (emailCC != "michael@theplasticsexchange.com"))
			{
				if (emailCC.Length>0) CC += ";";
				CC += "michael@theplasticsexchange.com";
			}

			if ((emailFrom == emailTo) && (emailTo == "helio@theplasticsexchange.com"))
			{
				//Mike's address would already be here
				CC += ";zach@theplasticsexchange.com";
			}

			Session["EmailBody_Shipment_Documents_emailFrom"] = emailFrom;
			Session["EmailBody_Shipment_Documents_emailTo"] = emailTo;
			Session["EmailBody_Shipment_Documents_emailCC"] = CC;
			Session["EmailBody_Shipment_Documents_emailSubject"] = emailSubject;
			Session["EmailBody_Shipment_Documents_emailBody"] = emailBody;
			//Response.Redirect("Email.aspx",false);
			
			string command = "<script language='javascript'> ";
			command += "window.open('../administrator/Email.aspx','','toolbar=false,resizable=1,scrollbars=1,menubar=false,width=800,height=750,top=20,left=100') ";
			command += "</script>";
			//,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+''
			//Page.RegisterClientScriptBlock("Pop up Email Preview",command);
			Page.RegisterStartupScript("Pop up Email Preview",command);
		}

		private void btnEmail_Click(object sender, System.EventArgs e)
		{
			string emailBody = "";
			string emailSubject = "";
			string[] ID = lblOrderID.Text.Split('-');

			switch(System.Convert.ToInt16(ddlType.SelectedValue))
			{
				case 1:
					emailBody = InvoicePOSalesConfirmation(ID[0], ID[1], "IN");
					emailSubject = "Invoice";
					break;
				case 2:
					emailBody = InvoicePOSalesConfirmation(ID[0], ID[1], "PO");
					emailSubject = "Purchase Order";
					break;
				case 3:
					emailBody = CertificateHTML(ID[0], ID[1]);
					emailSubject = "Certificate";
					break;
				case 4:
					emailBody = InvoicePOSalesConfirmation(ID[0], ID[1], "SC");
					emailSubject = "Sales";
					break;
				case 5:
					emailBody = ReleaseCustomer(ID[0], ID[1]);
					emailSubject = "Release Customer";
					break;
				case 6:
					emailBody = TitleTransfer(ID[0], ID[1]);
					emailSubject = "The Plastics Exchange";
					break;
				case 8:
					emailBody = WarehouseRelease(ID[0], ID[1]);
					emailSubject = "Release Material";
					break;
				default:
					break;
			}
			
			if (((Button)sender).Text  == "Preview Email")
			{
				PreviewEmail(getEmailUser(Session["ID"].ToString()),ddlSendTo.SelectedItem.Text,ddlCC.SelectedItem.Text,emailSubject,emailBody);
			}
			else
			{
				SendEmail(getEmailUser(Session["ID"].ToString()),ddlSendTo.SelectedItem.Text,ddlCC.SelectedItem.Text,emailSubject,emailBody);
			}
		}

		private string ExchangeTimeFormat(string str)
		{
			string mystr="";
			if (str.Length > 2)
			{
				mystr=Convert.ToDateTime(str).ToString("MM/dd/yyyy");
			}
			return mystr.Replace("-","/");
		}

		private string ShipmentID(string shipmentOrderID, string shipmentSKU)
		{
			//Looking for Shipment ID
			DBLib db = new DBLib(this.Context);
			db.OpenConnection();

			SqlDataReader drShipmentID = db.ExecuteReader("Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID='"+shipmentOrderID+ "' and SHIPMENT_SKU ='"+shipmentSKU+"'");
			drShipmentID.Read();
			string ShipmentID = drShipmentID["SHIPMENT_ID"].ToString();
			drShipmentID.Close();
			
			db.CloseConnection();
			return ShipmentID;
		}

		private bool ShowTitleTransferLetter(string shipmentOrderID, string shipmentSKU)
		{
			DBLib db = new DBLib(this.Context);
			db.OpenConnection();

			bool bShow = false;
			string sqlShip="SELECT SHIPMENT_SIZE, SHIPMENT_COMMENT FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='"+shipmentOrderID.ToString().Trim() + "' AND SHIPMENT_SKU = '" + shipmentSKU.ToString().Trim() + "'";
			SqlDataReader rd= db.ExecuteReader(sqlShip);
			if(rd.Read())
			{
				if ((rd["SHIPMENT_SIZE"].ToString().Equals("190000")) && (HelperFunction.getSafeStringFromDB(rd["SHIPMENT_COMMENT"]) != ""))
				{
					bShow = true;
				}
			}
			db.CloseConnection();
			return bShow;
		}

		
		private string HTMLHeader()
		{
			return HTMLHeader("left");
		}
		private string HTMLHeader(string alignmentType)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder("");
			sb.Append("<HTML>" + bl());
			sb.Append("<BODY bgColor=#ffffff STYLE='FONT: 10pt ARIAL'>" + bl());
			sb.Append("<p align=left><table align=" + alignmentType + " border=0 cellspacing=0 cellpadding=0>" + bl());
			sb.Append("<tr><td align=" + alignmentType + "><img src=https://www.plasticsexchange.com//images/email/tpelogo.gif></td>" + bl());
			sb.Append("<td align=" + alignmentType + ">" + bl());
			sb.Append("<font face=arial black size=4 color=Black>The</font><font face=arial black size=4 color=red>Plastics</font><font face=arial black size=4 color=Black>Exchange</font><font face=arial black size=4 color=red>.</font><font face=arial black size=4 color=Black>com</font>" + bl());
			sb.Append("</td>" + bl());
			sb.Append("</tr></table>" + bl());
			sb.Append("<br>" + bl());
			sb.Append("<br></p><br><br>");

			return sb.ToString();
		}

		private string HTMLFooter(string HtmlPersonName)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder("");
			sb.Append("<br>" + bl());
			sb.Append("<br>" + bl());
			sb.Append("Sincerely,<br>" + bl());
			sb.Append("<br>" + bl());
			sb.Append(HtmlPersonName + bl());
			sb.Append("<br>" + bl());
			sb.Append("The Plastics Exchange" + bl());
			sb.Append("<br>Tel: 312.202.0002" + bl());
			sb.Append("<br>Fax: 312.202.0174" + bl());
			sb.Append("<table align=center width='100%'><tr><td colspan=3 valign=top align=center><hr></td></tr>");
			sb.Append("<tr><td colspan=3 valign=top align=center><font face=arial size=1 color=Black>710 North Dearborn</font>&nbsp;&nbsp;<font face=arial size=1 color=red>Chicago, IL 60610</font>&nbsp;&nbsp;<font face=arial size=1 color=Black><b>tel 312.202.0002</b></font>&nbsp;&nbsp;<font face=arial size=1 color=red>fax 312.202.0174</font></td></tr></table>");
			sb.Append("</BODY></HTML>" + bl());

			return sb.ToString();
		}

		private void btnPreview_Click(object sender, System.EventArgs e)
		{
		
		}

		private void ddlType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			PopulateScreen(ddlType.SelectedValue);
		}

		private string HTMLFooter()
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder("");
			sb.Append("<br>" + bl());
			sb.Append("<br>" + bl());
			sb.Append("<table align=center width='100%'><tr><td colspan=3 valign=top align=center><hr></td></tr>");
			sb.Append("<tr><td colspan=3 valign=top align=center><font face=arial size=1 color=Black>710 North Dearborn</font>&nbsp;&nbsp;<font face=arial size=1 color=red>Chicago, IL 60610</font>&nbsp;&nbsp;<font face=arial size=1 color=Black><b>tel 312.202.0002</b></font>&nbsp;&nbsp;<font face=arial size=1 color=red>fax 312.202.0174</font></td></tr></table>");
			sb.Append("</BODY></HTML>" + bl());

			return sb.ToString();
		}
	}
}
