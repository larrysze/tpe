<%@ Page Language="c#" Codebehind="Transaction_Details.aspx.cs" AutoEventWireup="True"
    Inherits="localhost.Administrator.Transaction_Details" MasterPageFile="~/MasterPages/Menu.Master"
    Title="Transaction Details" %>

<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<%@ Register TagPrefix="uc1" TagName="Shipment_Documents" Src="/include/Shipment_Documents.ascx" %>
<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
    <style type="text/css"> 
        .menuskin { BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 100; VISIBILITY: hidden; FONT: 9px/10px Verdana; BORDER-LEFT: black 1px solid; WIDTH: 360px; BORDER-BOTTOM: black 1px solid; POSITION: absolute; BACKGROUND-COLOR: menu }
        .menuskin A { PADDING-RIGHT: 10px; PADDING-LEFT: 10px; COLOR: black; TEXT-DECORATION: none }
        #mouseoverstyle { BACKGROUND-COLOR: highlight }
        #mouseoverstyle A { COLOR: white }
    </style>
</asp:Content>
<asp:Content runat="Server" ContentPlaceHolderID="cphJavaScript">

    <script type="text/javascript" language="JavaScript">
    function Address_PopUp_Seller()
    // This function opens up a pop up window and passes the appropiate ID number for the window
    {
    if (document.all)
        var xMax = screen.width, yMax = screen.height;
    else
        if (document.layers)
            var xMax = window.outerWidth, yMax = window.outerHeight;
        else
            var xMax = 640, yMax=480;

    var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
    var str = "../administrator/Location_Details.aspx?ID=" + document.getElementById('<%=ddlPickupFrom.ClientID%>').options[document.getElementById('<%=ddlPickupFrom.ClientID%>').selectedIndex].value;    
    
    window.open(str,'Address','location=true,toolbar=true,width=275,height=390,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');

    }
        function Address_PopUp_Buyer()
    // This function opens up a pop up window and passes the appropiate ID number for the window
    {
    if (document.all)
        var xMax = screen.width, yMax = screen.height;
    else
        if (document.layers)
            var xMax = window.outerWidth, yMax = window.outerHeight;
        else
            var xMax = 640, yMax=480;

    var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
    var str = "../administrator/Location_Details.aspx?ID=" + document.getElementById('<%=ddlDeliveryTo.ClientID%>').options[document.getElementById('<%=ddlDeliveryTo.ClientID%>').selectedIndex].value;
    window.open(str,'Address','location=true,toolbar=true,width=275,height=390,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');
    }

    </script>

    <script type="text/javascript" language="JavaScript1.2">

        //Pop-it menu- By Dynamic Drive
        //For full source code and more DHTML scripts, visit http://www.dynamicdrive.com
        //This credit MUST stay intact for use

        var linkset=new Array()
        //SPECIFY MENU SETS AND THEIR LINKS. FOLLOW SYNTAX LAID OUT

        <asp:PlaceHolder id="phActionButton"  runat="server" />
        ////No need to edit beyond here

        var ie4=document.all&&navigator.userAgent.indexOf("Opera")==-1
        var ns6=document.getElementById&&!document.all
        var ns4=document.layers

        function showmenu(e,which){

        if (!document.all&&!document.getElementById&&!document.layers)
        return

        clearhidemenu()

        menuobj=ie4? document.all.popmenu : ns6? document.getElementById("popmenu") : ns4? document.popmenu : ""
        menuobj.thestyle=(ie4||ns6)? menuobj.style : menuobj

        if (ie4||ns6)
        menuobj.innerHTML=which
        else{
        menuobj.document.write('<layer name=gui bgColor=#E6E6E6 width=165 onmouseover="clearhidemenu()" onmouseout="hidemenu()">'+which+'</layer>')
        menuobj.document.close()
        }

        menuobj.contentwidth=(ie4||ns6)? menuobj.offsetWidth : menuobj.document.gui.document.width
        menuobj.contentheight=(ie4||ns6)? menuobj.offsetHeight : menuobj.document.gui.document.height
        eventX=ie4? event.clientX : ns6? e.clientX : e.x
        eventY=ie4? event.clientY : ns6? e.clientY : e.y

        //Find out how close the mouse is to the corner of the window
        var rightedge=ie4? document.body.clientWidth-eventX : window.innerWidth-eventX
        var bottomedge=ie4? document.body.clientHeight-eventY : window.innerHeight-eventY

        //if the horizontal distance isn't enough to accomodate the width of the context menu
        if (rightedge<menuobj.contentwidth)
        //move the horizontal position of the menu to the left by it's width
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX-menuobj.contentwidth : ns6? window.pageXOffset+eventX-menuobj.contentwidth : eventX-menuobj.contentwidth
        else
        //position the horizontal position of the menu where the mouse was clicked
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX : ns6? window.pageXOffset+eventX : eventX

        //same concept with the vertical position
        if (bottomedge<menuobj.contentheight)
        menuobj.thestyle.top=ie4? document.body.scrollTop+eventY-menuobj.contentheight : ns6? window.pageYOffset+eventY-menuobj.contentheight : eventY-menuobj.contentheight
        else
        menuobj.thestyle.top=ie4? document.body.scrollTop+event.clientY : ns6? window.pageYOffset+eventY : eventY
        menuobj.thestyle.visibility="visible"
        return false
        }

        function contains_ns6(a, b) {
        //Determines if 1 element in contained in another- by Brainjar.com
        while (b.parentNode)
        if ((b = b.parentNode) == a)
        return true;
        return false;
        }

        function hidemenu(){
        if (window.menuobj)
        menuobj.thestyle.visibility=(ie4||ns6)? "hidden" : "hide"
        }

        function dynamichide(e){
        if (ie4&&!menuobj.contains(e.toElement))
        hidemenu()
        else if (ns6&&e.currentTarget!= e.relatedTarget&& !contains_ns6(e.currentTarget, e.relatedTarget))
        hidemenu()
        }

        function delayhidemenu(){
        if (ie4||ns6||ns4)
        delayhide=setTimeout("hidemenu()",500)
        }

        function clearhidemenu(){
        if (window.delayhide)
        clearTimeout(delayhide)
        }

        function highlightmenu(e,state){
        if (document.all)
        source_el=event.srcElement
        else if (document.getElementById)
        source_el=e.target
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        else{
        while(source_el.id!="popmenu"){
        source_el=document.getElementById? source_el.parentNode : source_el.parentElement
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        }
        }
        }

        if (ie4||ns6)
        document.onclick=hidemenu

    </script>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
    <asp:Panel ID="pnlMainPanel" runat="server">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="There are problems with the following fields. Please correct the errors below.">
        </asp:ValidationSummary>
        <br />
        <div class="menuskin" id="popmenu" onmouseover="clearhidemenu();highlightmenu(event,'on')"
            onmouseout="highlightmenu(event,'off');dynamichide(event)">
        </div>
        <fieldset width="200">
            <legend><span class="Header Color2 Bold">Order&nbsp;Details</span></legend>
            <table id="Table2" cellspacing="1" cellpadding="1" width="100%" border="0">
                <tr>
                    <td colspan="4" align="left">
                        <asp:Label ID="lblOrderNumber" runat="server" CssClass="Content Bold Color2" Style="text-align: right">Order # 3565</asp:Label>
                        <asp:Label ID="lblShipmentID" runat="server" Visible="False" CssClass="Content Bold Color2"></asp:Label>
                        <asp:Label ID="lblOrderID" runat="server" Visible="False" CssClass="Content Bold Color2"></asp:Label></td>
                    <td>
                    </td>
                    <td colspan="4" align="right">
                        <asp:Button CssClass="Content Color2" ID="btnEditOrder" runat="server" Text="Edit Order"
                            Width="120px" OnClick="btnEditOrder_Click"></asp:Button>&nbsp;&nbsp;
                        <asp:Button CssClass="Content Color2" ID="btnDeleteOrder" runat="server" Text="Delete Order"
                            Width="100px" OnClick="btnDeleteOrder_Click"></asp:Button>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label CssClass="Content Color2" ID="lblProductDesc" runat="server">Product</asp:Label>&nbsp;
                        <asp:TextBox CssClass="InputForm" ID="txtProductDesc" runat="server" Width="180px"></asp:TextBox></td>
                    <td colspan="2">
                        <asp:Label CssClass="Content Color2" ID="lblWarehouse" runat="server">Warehouse</asp:Label>&nbsp;
                        <asp:TextBox CssClass="InputForm" ID="txtWarehouseName" runat="server" Width="70px"
                            ReadOnly="true"></asp:TextBox></td>
                    <td colspan="2" align="right">
                        <asp:Label CssClass="Content Color2" ID="lblTransactionDate" runat="server">Transaction Date:</asp:Label>&nbsp;
                    </td>
                    <td>
                        <asp:TextBox CssClass="InputForm" ID="txtTransactionDate" runat="server" Width="101px"></asp:TextBox></td>
                    <td>
                        <asp:Label CssClass="Content Color2" ID="lblMelt" runat="server">Melt</asp:Label>&nbsp;
                        <asp:TextBox CssClass="InputForm" ID="txtMelt" runat="server" Width="30px"></asp:TextBox></td>
                    <td colspan="2">
                        <p>
                            <asp:Label CssClass="Content Color2" ID="lblDensity" runat="server">Density</asp:Label>&nbsp;
                            <asp:TextBox CssClass="InputForm" ID="txtDensity" runat="server" Width="56px"></asp:TextBox></p>
                    </td>
                    <td>
                        <asp:Label CssClass="Content Color2" ID="lblAdds" runat="server">Adds</asp:Label>&nbsp;
                    </td>
                    <td>
                        <asp:TextBox CssClass="InputForm" ID="txtAdds" runat="server" Width="73px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:CheckBox CssClass="Content Color2" ID="cbPrime" runat="server" Text="Prime"></asp:CheckBox></td>
                    <td>
                    </td>
                </tr>
            </table>
        </fieldset>
        <p>
        </p>
        <!-- Insert content here -->
        <fieldset width="200">
            <legend>
                <asp:Label CssClass="Header Color2 Bold" ID="lblTabShipTitle" runat="server">Shipment Details</asp:Label></legend>
            <table id="Table1" height="0" cellspacing="1" cellpadding="1" width="100%" border="0">
                <tr>
                    <td height="20">
                        <asp:Label CssClass="Content Color2" ID="lblQuantity" runat="server">Quantity</asp:Label></td>
                    <td colspan="2" height="20">
                        <asp:TextBox CssClass="InputForm" ID="txtQuatity" runat="server" Width="48px"></asp:TextBox>&nbsp;
                        <asp:DropDownList CssClass="InputForm" ID="ddlSize" runat="server" Width="125px"
                            AutoPostBack="True" OnSelectedIndexChanged="ddlSize_SelectedIndexChanged">
                        </asp:DropDownList></td>
                    <td height="20">
                        <asp:Label CssClass="Content Color2" ID="lblStatus" runat="server">Status</asp:Label>&nbsp;
                        <asp:DropDownList CssClass="InputForm" ID="ddlStatus" runat="server" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                            <asp:ListItem Value="1">Inventory</asp:ListItem>
                            <asp:ListItem Value="2">Enroute</asp:ListItem>
                            <asp:ListItem Value="3">Delivered</asp:ListItem>
                        </asp:DropDownList></td>
                    <td height="20">
                        &nbsp;</td>
                    <td colspan="3" height="20">
                        <p align="right">
                            <asp:Label CssClass="Content Color2" ID="lblRailCarNumber" runat="server" BorderStyle="None"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp; &nbsp;
                            <asp:Button CssClass="Content Color2" ID="btnEditShipment" runat="server" Text="Edit Shipment"
                                Width="130px" OnClick="btnEditShipment_Click"></asp:Button>&nbsp;&nbsp;
                            <asp:Button CssClass="Content Color2" ID="btnDeleteShipment" runat="server" Text="Delete Shipment"
                                Width="120px" OnClick="btnDeleteShipment_Click"></asp:Button>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" height="137">
                        <fieldset width="200">
                            <legend><span class="Header Color2 Bold">Sold To</span></legend>
                            <table id="Table3" cellspacing="1" cellpadding="1" width="100%" border="0">
                                <tr>
                                    <td>
                                        <asp:Label CssClass="Content Color2" ID="lblCompanySoldTo" runat="server">Company</asp:Label></td>
                                    <td nowrap>
                                        <asp:DropDownList CssClass="InputForm" ID="ddlCompanySoldTo" runat="server" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlCompanySoldTo_SelectedIndexChanged">
                                        </asp:DropDownList>&nbsp;
                                        <asp:Label CssClass="Content Color2" ID="lblUserSoldTo" runat="server">User</asp:Label>&nbsp;
                                        <asp:DropDownList CssClass="InputForm" ID="ddlUserSoldTo" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td nowrap>
                                        <span class="LinkNormal"><a onmouseover="this.style.cursor='hand'" onclick="javascript:Address_PopUp_Buyer()">
                                            <u>Delivery to</u></a></span>
                                    </td>
                                    <td>
                                        <asp:DropDownList CssClass="InputForm" ID="ddlDeliveryTo" runat="server" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlDeliveryTo_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        &nbsp;&nbsp;&nbsp;<asp:Label CssClass="LinkNormal" ID="lblBuyerEditUser" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;<asp:Label
                                            CssClass="LinkNormal" ID="lblBuyerAddUser" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label CssClass="Content Color2" ID="lblSellPrice" runat="server">Sell Price</asp:Label></td>
                                    <td>
                                        <asp:TextBox CssClass="InputForm" ID="txtSellPrice" runat="server" Width="70px"></asp:TextBox>&nbsp;
                                        <asp:Label CssClass="Content Color2" ID="lblTermsSoldTo" runat="server">Terms</asp:Label>&nbsp;
                                        <asp:TextBox CssClass="InputForm" ID="txtTermsSoldTo" runat="server" Width="48px"></asp:TextBox>&nbsp;
                                        <asp:Label CssClass="Content Color2" ID="Label16" runat="server">Due: </asp:Label>&nbsp;
                                        <asp:Label CssClass="Content Color2" ID="lblDueSoldTo" runat="server">-</asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label CssClass="Content Color2" ID="Label17" runat="server">Total Outstanding ($):</asp:Label>&nbsp;
                                        <asp:Label CssClass="Content Color2" ID="lblTotalOutstandingsSoldTo" runat="server"></asp:Label>&nbsp;
                                        &nbsp;
                                        <asp:LinkButton CssClass="LinkNormal" ID="lnkRecordPaymentSoldTo" runat="server"
                                            OnClick="lnkRecordPaymentSoldTo_Click">Record Payment</asp:LinkButton></td>
                                </tr>
                            </table>
                        </fieldset>
                        <asp:RangeValidator CssClass="Content Color3 Bold" ID="TermValidator1" runat="server"
                            ControlToValidate="txtTermsSoldTo" ErrorMessage="Buyer Term must be greater than zero."
                            Display="none" MinimumValue="0" MaximumValue="999"></asp:RangeValidator>
                        <asp:CompareValidator CssClass="Content Color3 Bold" ID="TermValidator2" runat="server"
                            ControlToValidate="txtTermsSoldTo" ErrorMessage="Buyer Term must be a integer."
                            Display="none" Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                        <asp:RequiredFieldValidator CssClass="Content Color3 Bold" ID="TermValidator3" runat="server"
                            ControlToValidate="txtTermsSoldTo" ErrorMessage="Buyer Term cannot be blank"
                            Display="none"></asp:RequiredFieldValidator>
                        <asp:RangeValidator CssClass="Content Color3 Bold" ID="SellPriceValidator1" runat="server"
                            ControlToValidate="txtSellPrice" ErrorMessage="Buy price must be less than one dollar per pound."
                            Display="none" MinimumValue=".001" MaximumValue=".999" Type="Double"></asp:RangeValidator>
                        <asp:CompareValidator CssClass="Content Color3 Bold" ID="SellPriceValidator2" runat="server"
                            ControlToValidate="txtSellPrice" ErrorMessage="Buy Price must be a number." Display="none"
                            Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
                        <asp:RequiredFieldValidator CssClass="Content Color3 Bold" ID="SellPriceValidator3"
                            runat="server" ControlToValidate="txtSellPrice" ErrorMessage="Buy Price cannot be blank"
                            Display="none"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator CssClass="Content Color3 Bold" ID="POValidator1" runat="server"
                            ControlToValidate="txtPONumber" ErrorMessage="PO# cannot be blank" Display="none"></asp:RequiredFieldValidator></td>
                    <td colspan="4" height="137">
                        <fieldset width="200">
                            <legend><span class="Header Color2 Bold">Purchased From</span></legend>
                            <table id="Table4" cellspacing="1" cellpadding="1" width="100%" border="0">
                                <tr>
                                    <td width="93">
                                        <asp:Label CssClass="Content Color2" ID="lblCompanyFrom" runat="server">Company</asp:Label></td>
                                    <td nowrap colspan="5">
                                        <asp:DropDownList CssClass="InputForm" ID="ddlCompanyFrom" runat="server" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlCompanyFrom_SelectedIndexChanged">
                                        </asp:DropDownList>&nbsp;
                                        <asp:Label CssClass="Content Color2" ID="lblUserFrom" runat="server">User</asp:Label>&nbsp;
                                        <asp:DropDownList CssClass="InputForm" ID="ddlUserFrom" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td nowrap width="93">
                                        <span class="LinkNormal"><a onmouseover="this.style.cursor='hand'" onclick="javascript:Address_PopUp_Seller()">
                                            <u>Pick up From</u></a></span>
                                    </td>
                                    <td>
                                        <asp:DropDownList CssClass="InputForm" ID="ddlPickupFrom" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                        &nbsp;&nbsp;&nbsp;<asp:Label CssClass="LinkNormal" ID="lblSellerEditUser" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;<asp:Label
                                            CssClass="LinkNormal" ID="lblSellerAddUser" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="93">
                                        <asp:Label CssClass="Content Color2" ID="lblBuyPrice" runat="server">Buy Price</asp:Label></td>
                                    <td>
                                        <asp:TextBox CssClass="InputForm" ID="txtBuyPrice" runat="server" Width="70px"></asp:TextBox>&nbsp;
                                        <asp:Label CssClass="Content Color2" ID="lblTermsFrom" runat="server">Terms</asp:Label>&nbsp;
                                        <asp:TextBox CssClass="InputForm" ID="txtTermsFrom" runat="server" Width="48px"></asp:TextBox>&nbsp;
                                        <asp:Label CssClass="Content Color2" ID="Label19" runat="server">Due:</asp:Label>&nbsp;
                                        <asp:Label CssClass="Content Color2" ID="lblDueFrom" runat="server">-</asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label CssClass="Content Color2" ID="Label18" runat="server">Total Outstanding ($):</asp:Label>&nbsp;&nbsp;
                                        <asp:Label CssClass="Content Color2" ID="lblTotalOustandingFrom" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;
                                        <asp:LinkButton ID="lnkRecordPaymentFrom" runat="server" CssClass="LinkNormal" OnClick="lnkRecordPaymentFrom_Click">Record Payment</asp:LinkButton></td>
                                </tr>
                            </table>
                        </fieldset>
                        <asp:RangeValidator CssClass="Content Color3 Bold" ID="TermFromValidator1" runat="server"
                            ControlToValidate="txtTermsFrom" ErrorMessage="Sell Term must be greater than zero."
                            Display="none" MinimumValue="0" MaximumValue="999"></asp:RangeValidator>
                        <asp:CompareValidator CssClass="Content Color3 Bold" ID="TermFromValidator2" runat="server"
                            ControlToValidate="txtTermsFrom" ErrorMessage="Sell Term must be a integer."
                            Display="none" Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                        <asp:RequiredFieldValidator CssClass="Content Color3 Bold" ID="TermFromValidator3"
                            runat="server" ControlToValidate="txtTermsFrom" ErrorMessage="Sell Term cannot be blank"
                            Display="none" Enabled="False"></asp:RequiredFieldValidator>
                        <asp:RangeValidator CssClass="Content Color3 Bold" ID="BuyPriceValidator1" runat="server"
                            ControlToValidate="txtBuyPrice" ErrorMessage="Buy price must be less than one dollar per pound."
                            Display="none" MinimumValue=".001" MaximumValue=".999" Type="Double"></asp:RangeValidator>
                        <asp:CompareValidator CssClass="Content Color3 Bold" ID="BuyPriceValidator2" runat="server"
                            ControlToValidate="txtBuyPrice" ErrorMessage="Buy Price must be a number." Display="none"
                            Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
                        <asp:RequiredFieldValidator CssClass="Content Color3 Bold" ID="BuyPriceValidator3"
                            runat="server" ControlToValidate="txtBuyPrice" ErrorMessage="Buy Price cannot be blank"
                            Display="none"></asp:RequiredFieldValidator></td>
                </tr>
            </table>
            <table id="Table5" cellspacing="1" cellpadding="1" width="100%" border="0">
                <tr>
                    <td height="11">
                        <asp:Label CssClass="Content Color2" ID="lblBroker" runat="server">Broker</asp:Label></td>
                    <td height="11">
                        <asp:DropDownList CssClass="InputForm" ID="ddlBroker" runat="server" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlBroker_SelectedIndexChanged">
                        </asp:DropDownList></td>
                    <td height="11">
                        <asp:Label CssClass="Content Color2" ID="lblComission" runat="server">Commission (%)</asp:Label></td>
                    <td height="11">
                        <asp:TextBox CssClass="InputForm" ID="txtComission" runat="server" Width="80px"></asp:TextBox></td>
                    <td height="11">
                        <asp:Label CssClass="Content Color2" ID="lblWeight" runat="server">Weight</asp:Label></td>
                    <td height="11">
                        <asp:TextBox CssClass="InputForm" ID="txtWeight" runat="server" Width="80px"></asp:TextBox></td>
                    <td colspan="2" rowspan="3">
                        <fieldset width="200">
                            <legend><span class="Header Color2 Bold">Shipment Notes</span></legend>
                            <table id="Table6" cellspacing="1" cellpadding="1" width="100%" border="0">
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:TextBox CssClass="InputForm" ID="txtCommentNotes" runat="server" Width="140px"
                                            TextMode="Multiline" lines="2" size="500"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="imgComment" runat="server"></asp:Image>
                                    </td>
                                    <td>
                                        <span class="Content">Comment</span>&nbsp;&nbsp;
                                        <asp:Button CssClass="Content Color2" ID="btnAddNotes" runat="server" Text="Add"
                                            Width="40px" OnClick="btnAddNotes_Click"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td nowrap width="80">
                        <asp:Label CssClass="Content Color2" ID="lblFreight" runat="server">Freight ($) </asp:Label></td>
                    <td>
                        <asp:DropDownList CssClass="InputForm" ID="ddlFreightStatus" runat="server">
                            <asp:ListItem Value="E">Estimate</asp:ListItem>
                            <asp:ListItem Value="I">Invoice</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox CssClass="InputForm" ID="txtFreight" runat="server" Width="90px"></asp:TextBox></td>
                    <td>
                        <asp:Label CssClass="Content Color2" ID="Label32" runat="server">Profit ($)</asp:Label></td>
                    <td>
                        <asp:Label CssClass="Content Color2" ID="lblProfit" runat="server"></asp:Label></td>
                    <td>
                        <asp:Label CssClass="Content Color2" ID="lblTerm" runat="server">Term</asp:Label></td>
                    <td>
                        <asp:DropDownList CssClass="InputForm" ID="ddlTerm" runat="server" AutoPostBack="True">
                            <asp:ListItem Value="FOB/Shipping">FOB/Shipping</asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td height="21">
                        <asp:Label CssClass="Content Color2" ID="lblPONumber" runat="server">PO#</asp:Label></td>
                    <td height="21">
                        <asp:TextBox CssClass="InputForm" ID="txtPONumber" runat="server" Width="90px"></asp:TextBox></td>
                    <td height="21">
                        <asp:Label CssClass="Content Color2" ID="lblShippingDate" runat="server" Width="88px">Shipping Date</asp:Label></td>
                    <td height="21">
                        <asp:TextBox CssClass="InputForm" ID="txtShippingDate" runat="server" Width="80px"></asp:TextBox></td>
                    <td height="21">
                        <asp:Label CssClass="Content Color2" ID="lblDeliveryDate" runat="server" Width="88px">Delivery Date</asp:Label></td>
                    <td height="21">
                        <asp:TextBox CssClass="InputForm" ID="txtDeliveryDate" runat="server" Width="80px"></asp:TextBox></td>
                </tr>
            </table>
        </fieldset>
        <asp:CompareValidator CssClass="Content Color3 Bold" ID="FreightValidator1" runat="server"
            ControlToValidate="txtFreight" ErrorMessage="Freight price must be a number."
            Display="none" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
        <asp:RequiredFieldValidator CssClass="Content Color3 Bold" ID="FreightValidator2"
            runat="server" ControlToValidate="txtFreight" ErrorMessage="Freight price cannot be blank"
            Display="none"></asp:RequiredFieldValidator>
        <asp:RangeValidator CssClass="Content Color3 Bold" ID="CommissionValidator1" runat="server"
            ControlToValidate="txtComission" ErrorMessage="Commission must be between 0% and 100%."
            Display="none" MinimumValue="0" MaximumValue="100" Type="Double"></asp:RangeValidator>
        <asp:CompareValidator CssClass="Content Color3 Bold" ID="CommissionValidator2" runat="server"
            ControlToValidate="txtComission" ErrorMessage="Commission must be a number."
            Display="none" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator><br />
        <table id="Table7" cellspacing="1" cellpadding="1" width="100%" border="0">
            <tr>
                <td valign="top">
                    <uc1:shipment_documents id="Shipment_Documents1" runat="server">
                    </uc1:shipment_documents></td>
                <td valign="top">
                    <asp:Panel ID="pnlFiles" runat="server">
                        <fieldset width="200">
                            <legend><span class="Header Color2 Bold">Shipment Files</span> </legend>
                            <br />
                            <asp:DataGrid ID="dg" runat="server" Width="500" CssClass="DataGrid" HeaderStyle-CssClass="LinkNormal Bold OrangeColor"
                                AlternatingItemStyle-CssClass="LinkNormal DarkGray" ItemStyle-CssClass="LinkNormal LightGray"
                                AutoGenerateColumns="false">
                                <Columns>
                                    <asp:ButtonColumn Text="Delete" CommandName="Delete" />
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:ButtonColumn Text="FILE_NAME" DataTextField="FILE_NAME" HeaderText="File" CommandName="Edit" />
                                    <asp:BoundColumn DataField="FILE_DESCRIPT" HeaderText="Description" SortExpression="FILE_DESCRIPT ASC"
                                        ItemStyle-HorizontalAlign="left" />
                                    <asp:BoundColumn DataField="FILE_OWNER" HeaderText="Owner" SortExpression="FILE_OWNER ASC"
                                        ItemStyle-HorizontalAlign="left" />
                                    <asp:BoundColumn DataField="FILE_DATE" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}" />
                                    <asp:BoundColumn DataField="FILE_BUYER" HeaderText="Buyer" ItemStyle-HorizontalAlign="center" />
                                    <asp:BoundColumn DataField="FILE_SELLER" HeaderText="Seller" ItemStyle-HorizontalAlign="center" />
                                    <asp:BoundColumn DataField="ID" Visible="false" HeaderText="File_Id" SortExpression="ID ASC"
                                        ItemStyle-HorizontalAlign="right" />
                                </Columns>
                            </asp:DataGrid><br />
                            <table id="Table8" width="100%" border="0">
                                <tr>
                                    <td>
                                        <asp:Label CssClass="Content Color2" ID="Label22" runat="server">File to Add</asp:Label>&nbsp;</td>
                                    <td>
                                        <input class="InputForm" id="File_to_add" type="file" size="22" name="MyFile" runat="Server"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label CssClass="Content Color2" ID="Label13" runat="server">Description</asp:Label></td>
                                    <td>
                                        <asp:TextBox CssClass="InputForm" ID="Description" runat="server" Width="188px"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label CssClass="Content Color2" ID="Label36" runat="server">Access Rights</asp:Label></td>
                                    <td>
                                        <asp:CheckBox CssClass="Content Color2" ID="CheckBuyer" runat="server"></asp:CheckBox>
                                        <asp:Label CssClass="Content Color2" ID="lblBuyer" runat="server" Font-Bold="True">JLM Engineered Resins</asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:CheckBox CssClass="Content Color2" ID="CheckSeller" runat="server"></asp:CheckBox><b>
                                            <asp:Label CssClass="Content Color2" ID="lblSeller" runat="server">Cal Thermo</asp:Label></b></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" height="15">
                                        <asp:Button CssClass="Content Color2" ID="btnUpload" runat="server" Text="Upload"
                                            OnClick="btnUpload_Click"></asp:Button></td>
                                </tr>
                            </table>
                        </fieldset>
                    </asp:Panel>
                </td>
            </tr>
        </table>        
    </asp:Panel>    
   
   
   
   
    <asp:Panel ID="pnlConfirmationPanel" runat="server" Visible="false">
        <td width="100%" align="left" valign="top">
            <fieldset width="200">
                <legend><span class="Header Color3 Bold">Approvement</span></legend>
                <br />
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label CssClass="Content Color3 Bold" ID="lblConfirmMessage" runat="server" Visible="false">Sorry, incorrect password! Please, try again.</asp:Label></td>
                    </tr>
                </table>
                <asp:Panel ID="pnlSubPanel" runat="server" Visible="true">
                    <table width="100%">
                        <tr>
                            <td align="left">
                                <asp:Label CssClass="Content Color3 Bold" ID="lblConfirmationMessage" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:TextBox CssClass="InputForm" ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                                <asp:Button CssClass="Content Color2" ID="btnSubmitPassword" runat="server" Text="Confirm"
                                    OnClick="btnSubmitPassword_Click"></asp:Button>
                                <asp:Button CssClass="Content Color2" ID="btnCancel" runat="server" Text="Cancel"
                                    OnClick="btnCancel_Click"></asp:Button></td>
                        </tr>
                    </table>
                </asp:Panel>
            </fieldset>
    </asp:Panel>
   
   
   
   
   
    <asp:Panel ID="pnlCanNotEdit" runat="server" Visible="false">
        <td width="100%" align="left" valign="top">
            <fieldset width="200">
                <legend><span class="Header Color3 Bold">Already received Payment</span></legend>
                <br />
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label CssClass="Content Color3 Bold" ID="lblCanNotEdit" runat="server">This can not be edited/deleted since we have payments associated with this transaction!!!</asp:Label><br />
                            <br />
                            <asp:Button CssClass="Content Color2" ID="btnOK" runat="server" Text="Return To The Screen"
                                OnClick="btnOK_Click"></asp:Button>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </td>
    </asp:Panel>
</asp:Content>
