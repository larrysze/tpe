<%@ Page Language="C#" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<script runat="server">

    /************************************************************************
    *   1. File Name       :Administrator\Add_User.aspx                     *
    *   2. Description     :Add new user to company                         *
    *   3. Modification Log:                                                *
    *     Ver No.       Date          Author             Modification       *
    *   -----------------------------------------------------------------   *
    *      1.00      2-22-2004      Zach                First Baseline      *
    *                                                                       *
    ************************************************************************/
    SqlConnection conn;
    SqlConnection connDemo;
    
    public void Page_Load(object sender, EventArgs e)
    {
		BindDdlType();
         //only adminstrator and borker can access this page.
         if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B"))
         {
			Response.Redirect("../default.aspx");
         }
    
      conn = new SqlConnection(Application["DBconn"].ToString());
      conn.Open();
      connDemo = new SqlConnection(Application["DBconn"].ToString());
      connDemo.Open();
    }
    public void CompanyBack(object sender, EventArgs e){
         //back to previous page if "Cancel" button is clicked
         Server.Transfer("../administrator/Update_Company_Info.aspx?Id="+Request.QueryString["Id"]+"");
    }
    
    public void BindDdlType()
    {
		//Get type of company.
		string compType="";
		SqlConnection conn1 = new SqlConnection(Application["DBconn"].ToString());
		conn1.Open();
		string strSQL1="SELECT COMP_TYPE FROM COMPANY WHERE COMP_ID='"+Request.QueryString["Id"]+"'";
		SqlCommand cmdSelect1;
		cmdSelect1= new SqlCommand(strSQL1, conn1);
		SqlDataReader dtrSelect1= cmdSelect1.ExecuteReader();
		while(dtrSelect1.Read())
		{
			compType = dtrSelect1["COMP_TYPE"].ToString();
		}
		conn1.Close();
		
		//check if there is already an officer.
		int foundOfficer=0;
		SqlConnection conn0 = new SqlConnection(Application["DBconn"].ToString());
		conn0.Open();
		string strSQL0="SELECT PERS_ID FROM PERSON WHERE PERS_COMP='"+Request.QueryString["Id"]+"' AND PERS_TYPE='O'";
		SqlCommand cmdSelect0;
		cmdSelect0= new SqlCommand(strSQL0, conn0);
		SqlDataReader dtrSelect0= cmdSelect0.ExecuteReader();
		while(dtrSelect0.Read())
		{
			foundOfficer = 1;
		}
		conn0.Close();
		if(foundOfficer == 0)
			ddlType.Items.Add(new ListItem ("Officer","O"));
		if(Request.QueryString["Id"] == "148")
		{
			ddlType.Items.Add(new ListItem ("Purchaser","P"));
			ddlType.Items.Add(new ListItem ("Supplier","S"));
			ddlType.Items.Add(new ListItem ("Admin","A"));
			ddlType.Items.Add(new ListItem ("Broker","B"));
		}
		else if(compType=="D")
		{
			ddlType.Items.Add(new ListItem ("Purchaser","P"));
			ddlType.Items.Add(new ListItem ("Supplier","S"));
		}
		else if(compType=="P")
			ddlType.Items.Add(new ListItem ("Purchaser","P"));
		else if(compType=="S")
			ddlType.Items.Add(new ListItem ("Supplier","S"));
    }
    protected void IsDuplicate(object source, ServerValidateEventArgs args)
		{
			SqlConnection conn;
			SqlCommand cmd;
			SqlDataReader dtr;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			cmd= new SqlCommand("SELECT PERS_LOGN FROM PERSON WHERE PERS_MAIL='"+txtEmail.Text+"' ", conn);
			dtr = cmd.ExecuteReader();
			if (dtr.HasRows)
			{
				// found same Username exist already!
				args.IsValid = false;
			}
			conn.Close();
		}
    public void Save(object sender, EventArgs e)
    {
        if (Page.IsValid)
		{
			string check;
			string checkDemo;
			SqlCommand cmdchecklogin;
			SqlDataReader dtrchecklogin;
			//Check if this user name already in database.
			//System doesn't allow duplicate user name!

			cmdchecklogin= new SqlCommand("Select PERS_LOGN from Person where PERS_LOGN='"+txtLOGN.Text+"'", conn);
			dtrchecklogin= cmdchecklogin.ExecuteReader();
		    
			lbltest.Text="Select PERS_LOGN from Person where PERS_LOGN='"+txtLOGN.Text+"'";
			if (dtrchecklogin.Read())
			{
				check="exsit";
				lbltest.Text="This username is already exist in our database!";
				dtrchecklogin.Close();
			}
			else
			{
				check="";
				dtrchecklogin.Close();
			}
		
		
			if (check=="exsit") 
			{
			}
			else 
			{
					//this user name doesn't exsit, we can insert it to PERSON table under Genius database.
					string strSQL;
					strSQL="INSERT INTO PERSON (PERS_COMP,PERS_TYPE,PERS_LAST_NAME,PERS_FRST_NAME,PERS_TITL,PERS_PHON,PERS_FAX,PERS_MAIL, PERS_PSWD, PERS_DATE,PERS_ENBL,PERS_REG, PERS_LOGN, PERS_LAST_CHKD, PERS_ALIA, PERS_PREF) VALUES("+Request.QueryString["Id"]+",'"+ddlType.SelectedItem.Value+"','"+txtLname.Text+"','"+txtFname.Text+"','"+txtTitle.Text+"','"+txtPhone.Text+"','"+txtFax.Text+"','"+txtEmail.Text+"','" + Crypto.Encrypt(txtPSWD.Text) + "',getdate(),1,1,'"+txtLOGN.Text+"',getdate(), Null,268435455)";
					SqlCommand cmdUpdate;
					cmdUpdate= new SqlCommand(strSQL, conn);
					cmdUpdate.ExecuteNonQuery();
					conn.Close();
					connDemo.Close();

					Response.Redirect("../administrator/Update_Company_Info.aspx?Id="+Request.QueryString["Id"]+"");
			}
		}
    }

</script>
<form runat="server" id="Form">
	<TPE:Template PageTitle="" Runat="Server" />
	<TPE:Web_Box Heading="Add User" Runat="Server" />
	<asp:ValidationSummary runat="server" HeaderText="There are problems with the following fields. Please correct the errors below."
		id="ValidationSummary1" />
	<table align="center" border="0">
		<tbody>
			<tr>
				<td>
					Type:</td>
				<td>
					<asp:DropDownList id="ddlType" runat="server"></asp:DropDownList>
				</td>
			</tr>
			<tr>
				<td>
					First Name:</td>
				<td>
					<asp:RequiredFieldValidator runat="server" ControlToValidate="txtFname" Display="none" ErrorMessage="First name can not be blank."
					id="RequiredFieldValidator1" />
					<asp:TextBox id="txtFname" runat="server"></asp:TextBox>
				</td>
			</tr>
			<tr>
				<td>
					Last Name:</td>
				<td>
					<asp:RequiredFieldValidator runat="server" ControlToValidate="txtLname" Display="none" ErrorMessage="Last name can not be blank."
					id="Requiredfieldvalidator2" />
					<asp:TextBox id="txtLname" runat="server"></asp:TextBox>
				</td>
			</tr>
			<tr>
				<td>
					Title:</td>
				<td>
					<asp:TextBox id="txtTitle" runat="server"></asp:TextBox>
				</td>
			</tr>
			<tr>
				<td>
					Phone:</td>
				<td>
					<asp:TextBox id="txtPhone" runat="server"></asp:TextBox>
				</td>
			</tr>
			<tr>
				<td>
					Fax:</td>
				<td>
					<asp:TextBox id="txtFax" runat="server"></asp:TextBox>
				</td>
			</tr>
			<tr>
				<td>
					Email:</td>
				<td>
					<asp:CustomValidator runat="server" controltovalidate="txtEmail" display="none" ErrorMessage="Email already exists."
					OnServerValidate="IsDuplicate" id="CustomValidator1" />
					<asp:RequiredFieldValidator runat="server" ControlToValidate="txtEmail" Display="none" ErrorMessage="Email can not be blank."
					id="Requiredfieldvalidator3" />
					<asp:TextBox id="txtEmail" runat="server"></asp:TextBox>
				</td>
			</tr>
			<tr>
				<td>
					Login:</td>
				<td>
					<asp:TextBox id="txtLOGN" runat="server"></asp:TextBox>
					<font color="red"><b>
							<asp:Label id="lbltest" runat="server"></asp:Label>
						</b><font>
				</td>
			</tr>
			<tr>
				<td>
					Password:</td>
				<td>
					<asp:RequiredFieldValidator runat="server" ControlToValidate="txtPSWD" Display="none" ErrorMessage="Password can not be blank."
					id="Requiredfieldvalidator4" />
					<asp:TextBox id="txtPSWD" runat="server"></asp:TextBox>
				</td>
			</tr>
			<tr>
				<td align="middle" colspan="2">
					<asp:Button id="Button5" onclick="Save" class="tpebutton" runat="server" Text="Save"></asp:Button>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button class="tpebutton" onclick="CompanyBack" runat="server" Text="Cancel"></asp:Button>
				</td>
			</tr>
		</tbody>
	</table>
	<TPE:Web_Box Footer="true" Runat="Server" />
	<TPE:Template Footer="true" Runat="Server" />
</form>
