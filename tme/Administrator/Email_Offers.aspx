<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page language="c#" Codebehind="Email_Offers.aspx.cs" AutoEventWireup="false" Inherits="localhost.Administrator.Email_Offers" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<form id="Form" runat="server">
	<TPE:TEMPLATE id="Template1" NAME="Template1" Runat="Server" PageTitle=""></TPE:TEMPLATE>
	<table width="90%" align="center" border="0">
		<TR>
			<TD align="center" colSpan="2"><font color="red" size="4"><asp:label id="lblError" runat="server" visible="false"></asp:label></font></TD>
		</TR>
		<TR>
			<TD colSpan="2"><b>Body:</b><br>
				<asp:textbox id="txtBody" runat="server" TextMode="Multiline" cols="75" Rows="18"></asp:textbox></TD>
		</TR>
		<tr>
			<TD colSpan="2"><b>
					Select <asp:Label id="lblBidOffers" runat="server"></asp:Label> to Send:</b><br>
			</TD>
		</tr>
	</table>
	<asp:label id="lblContent" runat="server"></asp:label><BR>
	<HR>
	<BR>
	<center><asp:button id="btnSend" runat="server" Text="Send Emails"></asp:button></center>
	<TPE:TEMPLATE id="Template2" NAME="Template2" Runat="Server" Footer="true"></TPE:TEMPLATE></form>
