using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Location_Details.
	/// </summary>
	public partial class Location_Details : System.Web.UI.Page
	{
		//protected System.Web.UI.WebControls.Panel pnlLabl;
	
		/************************************************************************
	*   1. File Name       :Administrator\Location_Details.aspx             *
	*   2. Description     :View and change a location, accessed from "company list" *
	*   3. Modification Log:                                                *
	*     Ver No.       Date          Author             Modification       *
	*   -----------------------------------------------------------------   *
	*      1.00      2-22-2004      Zach                First Baseline      *
	*                                                                       *
	************************************************************************/
		public void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{					
				//Check the current location, display type of location based on PLAC_TYPE
				//S:shipping, H: Headquarters, D: Delivery, W: Warehouse
				int loc_id = 0;
				if((Request.QueryString["ID"] != null) && (Request.QueryString["ID"].Trim() != ""))
				{
					try
					{
						loc_id= Convert.ToInt32((string)Request.QueryString["ID"]);
					}
					catch(Exception ee)
					{
						Response.Redirect("/default.aspx");
					}
				}
				else
				{
					Response.Redirect("/default.aspx");
				}
				/*cmdLocation = new SqlCommand("select *,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) AS Location from place where plac_id='"+ loc_id +"'",conn);
				dtrLocation = cmdLocation.ExecuteReader(); */

			
				using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
				{
					conn.Open();

					string strSql = "select *,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) AS Location from place where plac_id=@plac_id";
					Hashtable htParams = new Hashtable();
					htParams.Add("@plac_id",loc_id.ToString());
					using (SqlDataReader dtrLocation = DBLibrary.GetDataReaderFromSelect(conn,strSql,htParams))
					{
						// update fields
						dtrLocation.Read();
						//if dtrLocation["Location"]
						lbllocation.Text = dtrLocation["Location"].ToString();
						if (dtrLocation["PLAC_TYPE"].ToString()=="H")
						{
							lblType.Text = "Headquarters";
							pnlInst.Visible = false; 
						}
						else if (dtrLocation["PLAC_TYPE"].ToString()=="S")
						{
							lblType.Text = "Shipping Point";
							pnlInst.Visible = true; 
						}
						else if (dtrLocation["PLAC_TYPE"].ToString()=="D")
						{
							lblType.Text = "Delivery Point";
							pnlInst.Visible = true; 
						}
						else
						{
							lblType.Text = "Warehouse";
							//if it's a warehouse, then get the name, rail # and label for warehouse
							//name and label can not be blank for a warehouse
	                
							pnlInst.Visible = true; //PLAC_INST is warehouse name
					
					
							if ( dtrLocation["PLAC_LABL"] != DBNull.Value)
							{
								lbllocation.Text = lbllocation.Text + " (" + (string)dtrLocation["PLAC_LABL"] + ")";
							}
						}
						if ( dtrLocation["PLAC_RAIL_NUM"] != DBNull.Value)
						{
							txtRail.Text = (string)dtrLocation["PLAC_RAIL_NUM"];
						}
						if ( dtrLocation["PLAC_PERS"] != DBNull.Value)
						{
							txtContact.Text = (string)dtrLocation["PLAC_PERS"];
						}
						if ( dtrLocation["PLAC_PHON"] != DBNull.Value)
						{
							txtPhone.Text = (string)dtrLocation["PLAC_PHON"];
						}
						if ( dtrLocation["PLAC_FAX"] != DBNull.Value)
						{
							txtFax.Text = (string)dtrLocation["PLAC_FAX"];
						}
						if ( dtrLocation["PLAC_EMAIL"] != DBNull.Value)
						{
							txtEmail.Text = (string)dtrLocation["PLAC_EMAIL"];
						}
						if (dtrLocation["PLAC_ADDR_ONE"] != DBNull.Value )
						{
							txtAddress1.Text = (string)dtrLocation["PLAC_ADDR_ONE"];
						}
						if (dtrLocation["PLAC_ADDR_TWO"] != DBNull.Value)
						{
							txtAddress2.Text = (string)dtrLocation["PLAC_ADDR_TWO"];
						}
						if (dtrLocation["PLAC_ZIP"] != DBNull.Value)
						{
							txtZip.Text = (string)dtrLocation["PLAC_ZIP"];
						}
						if (dtrLocation["PLAC_INST"] != DBNull.Value)
						{
							txtComments.Text = (string)dtrLocation["PLAC_INST"];
						}
	                
						lblText.Text  =dtrLocation["PLAC_INST"].ToString() +"<BR>";
						lblText.Text +=dtrLocation["PLAC_ADDR_ONE"].ToString() +"<BR>";
						lblText.Text +=dtrLocation["Location"].ToString() + " "+ dtrLocation["PLAC_ZIP"].ToString() +"<BR>";
						lblText.Text +=dtrLocation["PLAC_PHON"].ToString() +"<BR>";
						lblText.Text +=dtrLocation["PLAC_RAIL_NUM"].ToString() ;         
						// window should regain focus if users clicks off
						//phBody.Controls.Add (new LiteralControl("<body onblur=\"window.focus()\">"));
						phBody.Controls.Add (new LiteralControl("<body>"));
					}
				}
			}
		}
		public void Click_Save(object sender, EventArgs e)
		{
			// window should close after saving 

/*			string strSQL;
			string Id;
			Id = Request.QueryString["Id"];
			strSQL="UPDATE PlACE SET PLAC_PERS='"+txtContact.Text+"', PLAC_PHON='"+txtPhone.Text+"', PLAC_FAX='"+txtFax.Text+"',PLAC_EMAIL='"+txtEmail.Text+"',PLAC_ADDR_ONE='"+txtAddress1.Text+"',PLAC_ADDR_TWO='"+txtAddress2.Text+"',PLAC_ZIP='"+txtZip.Text+"',PLAC_RAIL_NUM='"+txtRail.Text+"',PLAC_INST='"+txtComments.Text+"' Where PLAC_ID=@Cid";
			SqlCommand cmdUpdate;
			cmdUpdate= new SqlCommand(strSQL, conn);
			cmdUpdate.Parameters.Add("@Cid", Id);
			cmdUpdate.ExecuteNonQuery();
			phBody.Controls.Add (new LiteralControl("<body onload=\"window.close()\">"));
*/	

			string strSQL;
			string Id;
			Id = Request.QueryString["Id"];
			strSQL="UPDATE PlACE SET PLAC_PERS=@plac_pers, PLAC_PHON=@plac_phon, PLAC_FAX=@plac_fax,PLAC_EMAIL=@plac_email,PLAC_ADDR_ONE=@plac_addr_one,PLAC_ADDR_TWO=@plac_addr_two,PLAC_ZIP=@plac_zip,PLAC_RAIL_NUM=@plac_rail_num,PLAC_INST=@plac_inst Where PLAC_ID=@Cid";
			Hashtable htParams2 = new Hashtable();
			htParams2.Add("@plac_pers",txtContact.Text);
			htParams2.Add("@plac_fax",txtFax.Text);
			htParams2.Add("@plac_phon",txtPhone.Text);
			htParams2.Add("@plac_email",txtEmail.Text);
			htParams2.Add("@plac_addr_one",txtAddress1.Text);
			htParams2.Add("@plac_addr_two",txtAddress2.Text);
			htParams2.Add("@plac_zip",txtZip.Text);
			htParams2.Add("@plac_rail_num",txtRail.Text);
			htParams2.Add("@plac_inst",txtComments.Text);
			htParams2.Add("@Cid", Id);
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),strSQL,htParams2);
			phBody.Controls.Add (new LiteralControl("<body onload=\"window.close()\">"));
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
