<%@ Page Language="VB" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Web.Mail" %>
<script runat="server">

    '<summary>
    'Function removes punctuation that may cause the insert to fail on the db.
    'Basically this script just loops against acsii value
    '</summary>

    Public Function Parse(ByVal TextIN As String) As String


        'Dim Parse as String
         Parse = Trim(TextIN)

         'If NonPrints Then
             Dim x As Long

             For x = 0 To 31
                 While InStr(Parse, Chr(x)) > 0
                     Parse = Replace(Parse, Chr(x), "")
                 End While
             Next x

             For x = 127 To 255
                 While InStr(Parse, Chr(x)) > 0
                     Parse = Replace(Parse, Chr(x), "")
                 End While
             Next x

            '  remove ' character
            While InStr(Parse, Chr(34)) > 0
                Parse = Replace(Parse, Chr(34), "")
            End While
            ' remove " character
            While InStr(Parse, Chr(39)) > 0
                Parse = Replace(Parse, Chr(39), "")
            End While


    End Function

</script>
<%
Dim Str, StrClose
'Database connection'
Dim conn as SqlConnection
Dim cmdContent as SQLCommand
Dim Recu as SqlDataReader
Dim Rec0 as SqlDataReader
Dim Rec as SqlDataReader
conn = new SqlConnection(Application("DBconn"))
conn.Open()

IF Request.Form("Close")<>"" THEN
        'close the task from to_do_list by set DO_STATUS = 1

	StrClose =("Update TODOLIST Set DO_STATUS = 1, CLOSE_DATE=getdate() where DO_ID="&Request.form("Close"))
	cmdContent= new SqlCommand(StrClose, conn)
	cmdContent.ExecuteNonQuery()
	StrClose=""

	'send email to alert Zach after close
	Dim EmailClose
	Dim mailClose AS MailMessage
	mailClose = new MailMessage()
	mailClose.From = "Suggestions@theplasticsexchange.com"
	mailClose.To = Application("strEmailAdmin")
	mailClose.Cc = Application("strEmailOwner")
	mailClose.Subject = "To Do List!"
	EmailClose=""


	'Query the task info which just get closed
	cmdContent= new SqlCommand("Select * from TODOLIST where DO_ID="&Request.form("Close"), conn)
	Recu= cmdContent.ExecuteReader()
	Recu.Read
	EmailClose=EmailClose & "<b>The following entry from to do list has been closed!</b><br>ID:&nbsp;&nbsp;" & Recu(0) & "<br>Screen Name:&nbsp;&nbsp;" & Recu(4) & "<br>Desc:&nbsp;&nbsp;" & Recu(3) & "<br>Update Time:&nbsp;&nbsp; " & Recu(1) & "<br>Priority:&nbsp;&nbsp;" & Recu(6) & "<br></table>"
	mailClose.Body = EmailClose
	mailClose.BodyFormat = MailFormat.Html
	TPE.Utility.EmailLibrary.Send(Context,mailClose)
	Recu.Close
	Response.Redirect("To_Do_List.aspx")
END IF



IF Request.Form("Open")<>"" THEN
        'Reopen the task from to_do_list by set DO_STATUS = 0
	StrClose =("Update TODOLIST Set DO_STATUS = 0 where DO_ID="&Request.form("Open"))
	cmdContent= new SqlCommand(StrClose, conn)
	cmdContent.ExecuteNonQuery()
	StrClose=""

	'send email to alert Zach after reopen the task
	Dim EmailOpen
	Dim mailOpen AS MailMessage
	mailOpen = new MailMessage()
	mailOpen.From = "Suggestions@theplasticsexchange.com"
	mailOpen.To = Application("strEmailAdmin")
	mailOpen.Cc = Application("strEmailOwner")
	mailOpen.Subject = "To Do List!"
	EmailOpen=""

	'Query the task info which just reopened

	cmdContent= new SqlCommand("Select * from TODOLIST where DO_ID="&Request.form("Open"), conn)
	Recu= cmdContent.ExecuteReader()
	Recu.Read
	EmailOpen=EmailOpen & "<b>The following entry from to do list has been reopened!</b><br>ID:&nbsp;&nbsp;" & Recu(0) & "<br>Screen Name:&nbsp;&nbsp;" & Recu(4) & "<br>Desc:&nbsp;&nbsp;" & Recu(3) & "<br>Update Time:&nbsp;&nbsp; " & Recu(1) & "<br>Priority:&nbsp;&nbsp;" & Recu(6) & "<br></table>"
	mailOpen.Body = EmailOpen
	mailOpen.BodyFormat = MailFormat.Html
	TPE.Utility.EmailLibrary.Send(Context,mailOpen)
	Recu.Close
	Response.Redirect("To_Do_List.aspx")
END IF



       'Update the record'
IF Request.form("Update")<>"" THEN


	Str =("Update TODOLIST Set DO_SCREEN='"&Parse(Request.Form("screenname"))&"', Do_DESC='"&Parse(Request.Form("desc"))&"',  Do_PRIORITY="&Request.Form("priority")&" where DO_ID="&Request.form("Update"))
	cmdContent= new SqlCommand(Str, conn)
	cmdContent.ExecuteNonQuery()



       'Send email alert after update
	Dim EmailStru
	Dim mail AS MailMessage
	mail = new MailMessage()
	mail.From = "Suggestions@theplasticsexchange.com"
	mail.To = Application("strEmailAdmin")
	mail.Cc = Application("strEmailOwner")
	mail.Subject = "To Do List!"
	EmailStru=""
	cmdContent= new SqlCommand("Select * from TODOLIST where DO_ID="&Request.form("Update"), conn)
	Recu= cmdContent.ExecuteReader()
	Recu.Read
	EmailStru=EmailStru & "<b>The following entry from to do list has been updated!</b><br>ID:&nbsp;&nbsp;" & Recu(0) & "<br>Screen Name:&nbsp;&nbsp;" & Recu(4) & "<br>Desc:&nbsp;&nbsp;" & Recu(3) & "<br>Update Time:&nbsp;&nbsp; " & Recu(1) & "<br>Priority:&nbsp;&nbsp;" & Recu(6) & "<br></table>"
	mail.Body = EmailStru
	mail.BodyFormat = MailFormat.Html
	TPE.Utility.EmailLibrary.Send(Context,mail)
	Recu.Close

	Response.Redirect("To_Do_List.aspx")
END IF

'Insert new task
IF Request.form("New")<>"" THEN

	Application("Flag")=1
	Application.Lock()
	Str =("INSERT INTO TODOLIST (OPEN_DATE, DO_DESC, DO_SCREEN, DO_STATUS, DO_PRIORITY) VALUES(getdate(), '"&Parse(Request.Form("desc"))&"', '"&Parse(Request.Form("screenname"))&"', 0,"&Request.Form("priority")&")")
	cmdContent= new SqlCommand(Str, conn)
	cmdContent.ExecuteNonQuery()
	Application("Flag")=0
	Application.UnLock()



'send email alert after new entry
	dim EmailStr
	EmailStr=""
	cmdContent= new SqlCommand("Select * from TODOLIST order by OPEN_DATE DESC", conn)
	Rec= cmdContent.ExecuteReader()
	Rec.Read
	EmailStr=EmailStr & "<b>New Entry from to do list!</b><br>ID:&nbsp;&nbsp;" & Rec(0) & "<br>Screen Name:&nbsp;&nbsp;" & Rec(4) & "<br>Desc:&nbsp;&nbsp;" & Rec(3) & "<br>Time:&nbsp;&nbsp; " & Rec(1) & "<br>Priority:&nbsp;&nbsp;" & Rec(6) & "<br></table>"
        Dim mail1 AS MailMessage
	mail1 = new MailMessage()
	mail1.From = "Suggestions@theplasticsexchange.com"
	mail1.To = Application("strEmailAdmin")
	mail1.To = Application("strEmailOwner")
	mail1.Subject = "To Do List!"
	mail1.Body = EmailStr
	mail1.BodyFormat = MailFormat.Html
	TPE.Utility.EmailLibrary.Send(Context,mail1)
	Rec.Close
	EmailStr=""

         'Response.Redirect("To_Do_List.aspx?type=close")
END IF
%>
<form runat="server" id="Form">
    <TPE:Template PageTitle="" Runat="Server" />
<!--#include FILE="../include/VBLegacy_Code.aspx"-->

<Table align=center>
<tr><td>
<%'allow user choose to view all, close and open record'%>
<SELECT NAME="output">
<OPTION VALUE="All">All
<OPTION VALUE="Open">Open
<OPTION VALUE="Close">Close
</SELECT>
<input type=submit  class="tpebutton" value="Go">
</form>
</td>
<td>

<%'new empty row display on top for new entry'%>
<form method=post name=Form>
<input type=hidden name=New value="">
<input type=hidden name=AddNew value="tt">
<input type=submit class="tpebutton" value="Add New Item">
</form>
</td></tr></table>

<table border=0 width="90%" align=center cellspacing=0 cellpadding=0 bgcolor="#E8E8E8">
   	<tr>
		<td bgcolor=#000000 colspan=15 height=1>
		</td>


	</tr>
    <tr>
        <td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
        <td width=3  Class="TableHeader" ><img src=/images/1x1.gif width=1></td>

        <TD align=center  Class="TableHeader" ><font color=white><strong>&nbsp;&nbsp;&nbsp;Close</strong></td>
        <TD align=center  Class="TableHeader" ><font color=white><strong>&nbsp;&nbsp;&nbsp;Update</strong></td>
        <TD align=center  Class="TableHeader" ><font color=white><strong>&nbsp;&nbsp;&nbsp;Screen Name</strong></td>
        <TD align=center  Class="TableHeader" ><font color=white><strong>Suggestion / Bug </td>
        <TD align=center  Class="TableHeader" ><font color=white><strong>&nbsp;&nbsp;&nbsp;Priority </td>
        <TD align=center  Class="TableHeader" ><font color=white><strong>&nbsp;&nbsp;&nbsp;Open Date</strong></td>
        <TD align=center  Class="TableHeader" ><font color=white><strong>&nbsp;&nbsp;&nbsp;Close Date</font></td>
        <TD align=center  Class="TableHeader"  colspan=4><font color=white><strong>&nbsp;&nbsp;&nbsp;Status</font></td>
        <td width=3  Class="TableHeader" ><img src=/images/1x1.gif width=1></td>
        <td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
    </tr>
   


<%'submit form with new entry values'%>
<% IF Request.form("AddNew")<>"" THEN%>
<form method=post name=Form>
<input type=hidden name=New value=1>
<tr bgcolor=white>
<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
<td width=3 ><img src=/images/1x1.gif width=1></td>
<TD align=center>

</td>
<TD align=center>

<input type=submit class=tpebutton value='SUBMIT' >
</td>
<TD align=center>

<input type=text name=screenname value=""></input>
</td>

<td align=center>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<textarea name=desc cols=35 rows=3 value=""></textarea>
</td>

<td align=center>
<SELECT NAME="priority" >
<OPTION VALUE=1>1
<OPTION VALUE=2>2
<OPTION VALUE=3>3
<OPTION VALUE=4>4
<OPTION VALUE=5>5
</SELECT></form>
</td>
<td align=center></td>
<td align=center colspan=4>
</td>
<td align=center></td>
<td width=3><img src=/images/1x1.gif width=1></td>
<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
</tr>

<%Else
%>


<%
'Query the records based on user choice of "open,close, all"'



Dim i
If Request.form("output")="" Then
cmdContent= new SqlCommand("SELECT DO_ID, OPEN_DATE=Convert(Varchar,OPEN_DATE, 101), CLOSE_DATE=Convert(Varchar,CLOSE_DATE, 101), DO_DESC, DO_SCREEN, DO_STATUS, DO_PRIORITY FROM TODOLIST where DO_STATUS=0 Order by DO_PRIORITY ASC", conn)

End IF

If Request.form("output")="Close" Then

cmdContent= new SqlCommand("SELECT DO_ID, OPEN_DATE=Convert(Varchar,OPEN_DATE, 101), CLOSE_DATE=Convert(Varchar,CLOSE_DATE, 101), DO_DESC, DO_SCREEN, DO_STATUS, DO_PRIORITY FROM TODOLIST where DO_STATUS=1 Order by DO_PRIORITY ASC", conn)

End IF
If Request.form("output")="Open" Then

cmdContent= new SqlCommand("SELECT DO_ID, OPEN_DATE=Convert(Varchar,OPEN_DATE, 101), CLOSE_DATE=Convert(Varchar,CLOSE_DATE, 101), DO_DESC, DO_SCREEN, DO_STATUS, DO_PRIORITY FROM TODOLIST where DO_STATUS=0 Order by DO_PRIORITY ASC", conn)

End IF
If Request.form("output")="All" Then
cmdContent= new SqlCommand("SELECT DO_ID, OPEN_DATE=Convert(Varchar,OPEN_DATE, 101), CLOSE_DATE=Convert(Varchar,CLOSE_DATE, 101), DO_DESC, DO_SCREEN, DO_STATUS, DO_PRIORITY FROM TODOLIST Order by DO_PRIORITY ASC", conn)

End IF
If Request.Querystring("type")<>"" Then
cmdContent= new SqlCommand("SELECT DO_ID, OPEN_DATE=Convert(Varchar,OPEN_DATE, 101), CLOSE_DATE=Convert(Varchar,CLOSE_DATE, 101), DO_DESC, DO_SCREEN, DO_STATUS, DO_PRIORITY FROM TODOLIST where DO_STATUS=1 Order by DO_PRIORITY ASC", conn)
End IF

Rec0= cmdContent.ExecuteReader()

'Begin loop'
While Rec0.Read()
%>

<tr bgcolor=white>
<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
<td width=3 ><img src=/images/1x1.gif width=1></td>


<%If Rec0("DO_STATUS") = True Then%>
<form method=post name=Form>
<TD align="center" valign="top">
<input type=hidden name=Open value="<%=Rec0("DO_ID")%>"></input>
<input type=submit value="Reopen">
</input></form>
<%Else%>
<form method=post name=Form>
<TD align="center" valign="top">
<input type=hidden name=Close value="<%=Rec0("DO_ID")%>"></input>
<input type=submit value="Close">
</input></form>
<%End IF%>
</td>


<TD align=center valign="top">
<%If Request.form("enable")="" Then%>
<form method=post name=Form>
<input type=hidden name=enable value="yes">

<input type=submit value="Update">
<%else%>
<form method=post name=Form>
<input type=hidden name=Update value=<%=Rec0("DO_ID")%>>

<input type=submit value="Update">
<%End If%>
</td>




<TD align=center valign="top">
<input type=text name=screenname size="7" value=<%=Rec0("DO_SCREEN")%>></input>
</td>
<td align=left valign="top">

<%If Request.form("enable")="" Then%>
<%=Rec0("DO_DESC")%>
<%Else%>
<textarea name=desc cols=35 rows=5 maxlength=250><%=Rec0("DO_DESC")%></textarea>
<%End If%>


</td>
<!--<td align=center> Est Development Time</td>-->
<td align=center valign="top">

<SELECT NAME="priority">
<OPTION VALUE=1 <% If Rec0("DO_PRIORITY") =1 Then Response.Write("selected")%>>1
<OPTION VALUE=2 <% If Rec0("DO_PRIORITY") =2 Then Response.Write("selected")%>>2
<OPTION VALUE=3 <% If Rec0("DO_PRIORITY") =3 Then Response.Write("selected")%>>3
<OPTION VALUE=4 <% If Rec0("DO_PRIORITY") =4 Then Response.Write("selected")%>>4
<OPTION VALUE=5 <% If Rec0("DO_PRIORITY") =5 Then Response.Write("selected")%>>5
</SELECT>
</td>
<td align=center valign="top">
<%
dim opend
opend=""
opend=opend & "" & Rec0("OPEN_DATE") & ""
response.write (opend)
%>
</td>

<td align=center valign="top">

<%
'display only the date'
dim closed
closed=""
If Rec0("CLOSE_DATE") Is DBNull.Value Then
Else
closed=closed & "" & Rec0("CLOSE_DATE") & ""
End IF
response.write (closed)
%>
</td>

<td align=center colspan=4 valign="top">&nbsp;&nbsp;&nbsp;

<%If Rec0("DO_STATUS") = True Then%>

Close

<%Else%>
Open

<%End IF%>



</form>

</td>

<td width=3 valign="top"><img src=/images/1x1.gif width=1></td>
<td bgcolor=#000000 width=1 valign="top"><img src=/images/1x1.gif width=1></td>

</tr>
<tr bgcolor=white>
<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
<td width=3 ><img src=/images/1x1.gif width=1></td>

    <td colspan="11"><HR></td>
    <td width=3 valign="top"><img src=/images/1x1.gif width=1></td>
<td bgcolor=#000000 width=1 valign="top"><img src=/images/1x1.gif width=1></td>

</tr>
<%
End While
Rec0.Close
End IF
	%>
<tr>
<td colspan=10 Class="Header">

</td>
<td colspan=7 Class="Header">
<img src=/images/1x1.gif height=1>
</td>
</tr>
</table>
<TPE:Template Footer="true" Runat="Server" />

</form>
