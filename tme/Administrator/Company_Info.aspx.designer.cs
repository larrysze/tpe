//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.42
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace localhost.Common {
    
    public partial class Company_Info {
        protected System.Web.UI.WebControls.Label lblCompanyName;
        protected System.Web.UI.WebControls.Label Label4;
        protected System.Web.UI.WebControls.Label lblCompanyID;
        protected System.Web.UI.WebControls.Label Label1;
        protected System.Web.UI.WebControls.TextBox txtCompanyName;
        protected System.Web.UI.WebControls.Label Label2;
        protected System.Web.UI.WebControls.TextBox txtWebSite;
        protected System.Web.UI.WebControls.Label Label3;
        protected System.Web.UI.WebControls.Label lblOriginalValue;
        protected System.Web.UI.WebControls.TextBox txtCreditLimit;
        protected System.Web.UI.WebControls.Label Label20;
        protected System.Web.UI.WebControls.Label lblOriginalTermsValue;
        protected System.Web.UI.WebControls.TextBox txtTerms;
        protected System.Web.UI.WebControls.Button btnUpdateCompanyInfo;
        protected System.Web.UI.WebControls.Button btnCancelUpdateCompany;
        protected System.Web.UI.WebControls.Label Label8;
        protected System.Web.UI.WebControls.Label Label7;
        protected System.Web.UI.WebControls.TextBox txtBankName;
        protected System.Web.UI.WebControls.Label Label6;
        protected System.Web.UI.WebControls.TextBox txtContact;
        protected System.Web.UI.WebControls.Label Label5;
        protected System.Web.UI.WebControls.TextBox txtPhone;
        protected System.Web.UI.WebControls.Label Label12;
        protected System.Web.UI.WebControls.TextBox txtFax;
        protected System.Web.UI.WebControls.Label Label9;
        protected System.Web.UI.WebControls.TextBox txtAddress;
        protected System.Web.UI.WebControls.Label Label10;
        protected System.Web.UI.WebControls.TextBox txtCity;
        protected System.Web.UI.WebControls.Label Label13;
        protected System.Web.UI.WebControls.TextBox txtState;
        protected System.Web.UI.WebControls.Label Label11;
        protected System.Web.UI.WebControls.TextBox txtZip;
        protected System.Web.UI.WebControls.Label Label17;
        protected System.Web.UI.WebControls.TextBox txtAddress2;
        protected System.Web.UI.WebControls.Label Label14;
        protected System.Web.UI.WebControls.TextBox txtAccountNumber;
        protected System.Web.UI.WebControls.Label Label15;
        protected System.Web.UI.WebControls.TextBox txtRepetitiveNumber;
        protected System.Web.UI.WebControls.Label Label16;
        protected System.Web.UI.WebControls.TextBox txtABANumber;
        protected System.Web.UI.WebControls.Button btnUpdateBankInfo;
        protected System.Web.UI.WebControls.Button btnCancelUpdateBankInfo;
        protected System.Web.UI.WebControls.Label Label18;
        protected System.Web.UI.WebControls.Button btnAddNewUser;
        protected System.Web.UI.WebControls.Label Label19;
        protected System.Web.UI.WebControls.Button btnAddNewLocation;
        protected System.Web.UI.WebControls.DataGrid dgUsers;
        protected System.Web.UI.WebControls.DataGrid dgLocations;
        public new localhost.MasterPages.Menu Master {
            get {
                return ((localhost.MasterPages.Menu)(base.Master));
            }
        }
    }
}
