using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Report_Form.
	/// </summary>
	public class Report_Form : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Panel pnlEnter;
		protected System.Web.UI.WebControls.DropDownList ddlReportType;
		protected System.Web.UI.WebControls.DropDownList ddlMonth;
		protected System.Web.UI.WebControls.Label lblNameOfParameter;
		protected System.Web.UI.WebControls.TextBox txtParameter;
		protected System.Web.UI.WebControls.DataGrid dg;
		protected System.Web.UI.WebControls.Button btnUpdate;
		protected System.Web.UI.WebControls.RangeValidator RangeValidator2;
		protected System.Web.UI.WebControls.RangeValidator RangeValidator0;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator0;
		protected System.Web.UI.WebControls.Label lblReportTitle;
		protected System.Web.UI.WebControls.Label lblMonthTitle;
		protected System.Web.UI.WebControls.Panel pnlMain;

	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(!IsPostBack)
			{
				fillDates();
				setParameterType();
			}
		
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddlReportType.SelectedIndexChanged += new System.EventHandler(this.ddlReportType_SelectedIndexChanged);
			this.ddlMonth.SelectedIndexChanged += new System.EventHandler(this.ddlMonth_SelectedIndexChanged);
			this.dg.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_ItemDataBound);
			this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		private void fillDates()
		{
//			DateTime today = DateTime.Now;
			DateTime today = Convert.ToDateTime("04/02/2005");
			int year = today.Year;
			int month = today.Month;

			string[] all_months = new string[13];

			all_months[1] = "January";
			all_months[2] = "February";
			all_months[3] = "March";
			all_months[4] = "April";
			all_months[5] = "May";
			all_months[6] = "June";
			all_months[7] = "July";
			all_months[8] = "August";
			all_months[9] = "September";
			all_months[10] = "October";
			all_months[11] = "November";
			all_months[12] = "December";
			
			if(month < 11)
			{
				DateTime somedate = today;
				somedate = somedate.AddYears(-1);
				somedate = somedate.AddMonths(11 - today.Month);
				while(somedate <= today)
				{
					ListItem item = new ListItem(all_months[somedate.Month] + ", " + somedate.Year, somedate.ToShortDateString());
					ddlMonth.Items.Add(item);
					if(somedate.Month == 12)
					{
						somedate = somedate.AddYears(1);
						somedate = somedate.AddMonths(-11);
					}
					else
					{
						somedate = somedate.AddMonths(1);
					}
				}
				//				ddl

				
			}
			else 
			{

				ListItem item = new ListItem(all_months[11] + ", " + today.Year, "11/" + today.Day + "/" + today.Year);
				ddlMonth.Items.Add(item);
				if (month == 12)
				{
					item = new ListItem(all_months[month] + ", " + today.Year, today.ToShortDateString());
					ddlMonth.Items.Add(item);

				}
			}

			ddlMonth.Items[ddlMonth.Items.Count - 1].Selected = true;


		}

		private bool is_holiday(int day, int month, int year)
		{
			bool ret = false;
			
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();

			string sql = "SELECT * FROM HOLIDAYS WHERE MONTH(HOLIDAY_DATE)=" + month + " AND DAY(HOLIDAY_DATE)=" + day + " AND YEAR(HOLIDAY_DATE)=" + year;
			
			SqlCommand command = new SqlCommand(sql, conn);
			SqlDataReader dtr = command.ExecuteReader();

			if(dtr.Read())
			{
				ret = true;
			}

			dtr.Close();
			conn.Close();


			return ret;
		}
		
		private void create_record(int day, int month, int year)
		{
			
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			
			DateTime date =  Convert.ToDateTime(month.ToString() + "/" + day.ToString() + "/" + year.ToString()); 
			string sql = "INSERT INTO ACB_ACCOUNTS(ACCOUNT_DATE, ACCOUNT_ENTERED) VALUES('" +date.ToShortDateString() + "', 0)";
			
			SqlCommand command = new SqlCommand(sql, conn);
			command.ExecuteNonQuery();

			conn.Close();

		}

		public void Bind(string SQLOrder)
		{

			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			string strSQL;
			strSQL ="";

			DateTime date = Convert.ToDateTime(ddlMonth.SelectedValue.ToString());
			
			/*
			 *	we have to make sure that all dates are present in the TABLE
			 *	if no then INSERT with default values
			 */

			strSQL = "SELECT DAY(account_date) as ACCOUNT_DAY, ACCOUNT_DATE FROM ACB_ACCOUNTS " + 
				" WHERE MONTH(ACCOUNT_DATE)=" + date.Month + " AND YEAR(ACCOUNT_DATE)=" + date.Year + 
				" AND (DATEPART(weekday, account_date) IN (2,3,4,5,6)) " + 
				" AND account_date not in (select holiday_date from holidays) ORDER BY ACCOUNT_DATE ASC";

			SqlCommand command = new SqlCommand(strSQL, conn);
			SqlDataReader dtr = command.ExecuteReader();
			
			int max = DateTime.DaysInMonth(date.Year, date.Month);
			int k = 0;
			
			SqlConnection temp_conn;
			temp_conn = new SqlConnection(Application["DBconn"].ToString());
			temp_conn.Open();

			int i = 1;
			do
			{
					if(dtr.Read())
					{
						k = Convert.ToInt32(dtr["ACCOUNT_DAY"].ToString());
					}
					else
					{
						k = max;
					}

					while(k > i)
					{

						DateTime this_day = Convert.ToDateTime(date.Month + "/" + i + "/" + date.Year);
						if((this_day.DayOfWeek != DayOfWeek.Saturday) && (this_day.DayOfWeek != DayOfWeek.Sunday))
						{
							bool was_holiday = false;
			

							string sql = "SELECT * FROM HOLIDAYS WHERE MONTH(HOLIDAY_DATE)=" + date.Month + " AND DAY(HOLIDAY_DATE)=" + i + " AND YEAR(HOLIDAY_DATE)=" + date.Year;
			
							SqlCommand sql_command = new SqlCommand(sql, temp_conn);
							SqlDataReader temp_dtr = sql_command.ExecuteReader();

							if(temp_dtr.Read())
							{
								was_holiday = true;
							}

							temp_dtr.Close();
							if(!was_holiday)
							{
								create_record(i, date.Month, date.Year); //insert line with default values
							}
						}
						i++;
					}

				i++;

			}while(i <= max);

			
			temp_conn.Close();

			dtr.Close();

			strSQL="SELECT (CAST(MONTH(ACCOUNT_DATE) AS VARCHAR) + '/' + CAST(DAY(ACCOUNT_DATE) AS VARCHAR) + '/' + CAST(YEAR(ACCOUNT_DATE) AS VARCHAR)) AS ACCOUNT_DATE_TITLE, ACCOUNT_TRADING, ACCOUNT_OPERATION, ACCOUNT_COLLATERAL, ACCOUNT_TPE_CREDIT, ACCOUNT_MAG_CREDIT FROM ACB_ACCOUNTS WHERE MONTH(ACCOUNT_DATE)=" + date.Month + " AND YEAR(ACCOUNT_DATE)=" + date.Year + " ORDER BY "+ SQLOrder;
			SqlDataAdapter dadContent;
			DataSet dstContent;
			dadContent = new SqlDataAdapter(strSQL ,conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dg.DataSource = dstContent;
			dg.DataBind();

			conn.Close();


		}


		private void setParameterType()
		{
			if((ddlMonth.SelectedValue.ToString() != "-1") && (ddlReportType.SelectedValue.ToString() != "-1"))
			{	
				SqlConnection conn;
				conn = new SqlConnection(Application["DBconn"].ToString());
				conn.Open();
				string strSQL;
				strSQL ="";

				DateTime date = Convert.ToDateTime(ddlMonth.SelectedValue.ToString());
			
				/*
				 *	we have to make sure that all dates are present in the TABLE
				 *	if no then INSERT with default values
				 */

				strSQL = "SELECT * FROM REPORT_PARAMETERS WHERE MONTH(REPORT_MONTH)=" + date.Month + " AND YEAR(REPORT_MONTH)=" + date.Year;
				SqlCommand command = new SqlCommand(strSQL, conn);
				SqlDataReader dtr = command.ExecuteReader();
				string par_value;
				string par_value1;
				string par_value2;

				if(dtr.Read())
				{
					par_value1 = nullValuesHTML(dtr["REPORT_OVERHEAD"].ToString());
					par_value2 = nullValuesHTML(dtr["REPORT_INVENTORY_VALUATION"].ToString());
				}
				else
				{
					dtr.Close();

					par_value1 = "0";
					par_value2 = "0";

					strSQL = "INSERT INTO REPORT_PARAMETERS (REPORT_MONTH, REPORT_OVERHEAD, REPORT_INVENTORY_VALUATION) VALUES('" + date.ToShortDateString() + "', 0, 0)";
					command = new SqlCommand(strSQL, conn);
					command.ExecuteNonQuery();


				}

				if(ddlReportType.SelectedValue.ToString() == "1")
				{
					lblNameOfParameter.Text = "Over head ($)";
					txtParameter.MaxLength = 9;
					txtParameter.Width = 80;
					par_value = par_value1;
				}
				else
				{
					lblNameOfParameter.Text = "Average Inventory Valuation (%)";
					txtParameter.MaxLength = 3;
					txtParameter.Width = 40;
					par_value = par_value2;
				}
				
				txtParameter.Visible = true;
				txtParameter.Text = par_value;
				Bind("ACCOUNT_DATE ASC");
				pnlMain.Visible = true;
				dg.Visible = true;


			}
			else
			{
				txtParameter.Visible = false;
				lblNameOfParameter.Text = "";
				pnlMain.Visible = false;
			}

		}

		private void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			setParameterType();		
		}

		private void ddlReportType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			setParameterType();		
		}

		private void btnStart_Click(object sender, System.EventArgs e)
		{
		}

		private void dg_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				((TextBox)e.Item.Cells[6].Controls[1]).Text =  nullValuesHTML(e.Item.Cells[1].Text);
				((TextBox)e.Item.Cells[7].Controls[1]).Text =  nullValuesHTML(e.Item.Cells[2].Text);
				((TextBox)e.Item.Cells[8].Controls[1]).Text =  nullValuesHTML(e.Item.Cells[3].Text);
				((TextBox)e.Item.Cells[9].Controls[1]).Text =  nullValuesHTML(e.Item.Cells[4].Text);
				((TextBox)e.Item.Cells[10].Controls[1]).Text =  nullValuesHTML(e.Item.Cells[5].Text);
			}
		}

		private string nullValuesHTML(string input)
		{
			string str_return = "0";

			if (input != null)
			{
				if (input != "&nbsp;")
				{
					str_return = Convert.ToInt32(Convert.ToDecimal(input)).ToString();
				}
			}
			return str_return;
		}

		private void btnUpdate_Click(object sender, System.EventArgs e)
		{
			//			dg.Controls[1]
			//dg.Items.Count
			String s = dg.Items[0].Cells[0].Text;
			for(int i = 0; i < dg.Items.Count; i++)
			{
				if (dg.Items[i].ItemType == ListItemType.Item || dg.Items[i].ItemType == ListItemType.AlternatingItem)
				{
					string date = dg.Items[i].Cells[0].Text;
					string s1 = ((TextBox)(dg.Items[i].Cells[6].Controls[1])).Text;
					string s2 = ((TextBox)(dg.Items[i].Cells[7].Controls[1])).Text;
					string s3 = ((TextBox)(dg.Items[i].Cells[8].Controls[1])).Text;
					string s4 = ((TextBox)(dg.Items[i].Cells[9].Controls[1])).Text;
					string s5 = ((TextBox)(dg.Items[i].Cells[10].Controls[1])).Text;
					string on = "0";
					
					if((Convert.ToDecimal(s1) != 0) && (Convert.ToDecimal(s2) != 0) &&(Convert.ToDecimal(s3) != 0) &&(Convert.ToDecimal(s4) != 0) &&(Convert.ToDecimal(s5) != 0))
					{
						on = "1";
					}

					SqlConnection conn;
					conn = new SqlConnection(Application["DBconn"].ToString());
					conn.Open();
			
					DateTime item_date =  Convert.ToDateTime(date); 
					
					string sql = "UPDATE ACB_ACCOUNTS SET " + 
						"  ACCOUNT_TRADING=" + s1 + 
						", ACCOUNT_OPERATION=" + s2 + 
						", ACCOUNT_COLLATERAL=" + s3 + 
						", ACCOUNT_TPE_CREDIT=" + s4 + 
						", ACCOUNT_MAG_CREDIT=" + s5 + 
						", ACCOUNT_ENTERED=" + on + 
						" WHERE DAY(ACCOUNT_DATE)=" + item_date.Day + " AND MONTH(ACCOUNT_DATE)=" + item_date.Month + " AND YEAR(ACCOUNT_DATE)=" + item_date.Year;
			
					SqlCommand command = new SqlCommand(sql, conn);
					command.ExecuteNonQuery();

					sql = "UPDATE REPORT_PARAMETERS SET ";
					if(ddlReportType.SelectedValue.ToString() == "1")
					{
						sql += " REPORT_OVERHEAD=";
					}
					else
					{
						sql += " REPORT_INVENTORY_VALUATION=";
					}
					sql += txtParameter.Text;
					command = new SqlCommand(sql, conn);
					command.ExecuteNonQuery();


					conn.Close();


				}
			}

			Bind("ACCOUNT_DATE ASC");
		}
	}
}
