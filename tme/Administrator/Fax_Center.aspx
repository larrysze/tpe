<%@ Page language="c#" Codebehind="Fax_Center.aspx.cs" AutoEventWireup="True" Inherits="localhost.Common.Fax_Center" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>
 <asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
 <div style="width:780px">

	<table height="443" width="780px" align="right" border="0" cellpadding="0" cellspacing="0" >
		<tr> 
			<TD align=center  width="241" colSpan="3" height="21"><br><asp:label id="lblPageHeader" CssClass="Content" runat="server" Font-Size="Large" Font-Bold=true></asp:label></TD>
		</tr>
		<asp:panel id="pnlCentralFax" Runat="Server" Visible="true">
			<TR>
				<TD align="center" width="346" height="45">
					<asp:Label id="Label1" CssClass="Content Color2 " runat="server" Font-Bold="True">Inbox</asp:Label></TD>
				<TD align="center" width="47" height="45"></TD>
				<TD align="center" height="45">
					<asp:Label CssClass="Content" id="lblMyFaxes" Font-Size="Larger" runat="server" Visible="False" Font-Bold="True">My Faxes</asp:Label>
					<asp:Label CssClass="Content" id="lblSelectEmployee" runat="server">Select Employee: </asp:Label>
					<asp:DropDownList id="ddlEmployee" runat="server" CssClass="InputForm" AutoPostBack="True" Width="164px" onselectedindexchanged="ddlEmployee_SelectedIndexChanged"></asp:DropDownList></TD>
			</TR>
			<TR>
				<TD vAlign="top" align="center" width="346" height="314">
					<asp:Button id="cmdPreviewFaxInbox" runat="server" CssClass="Content Color2" Width="82px" Text="Preview Fax" onclick="cmdPreviewFaxInbox_Click"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:Button id="btnDelete" runat="server" CssClass="Content Color2" Width="82px" Text="Delete Fax" onclick="btnDelete_Click"></asp:Button><BR>
					<TABLE id="Table1" class="Content" height="23" cellSpacing="0" cellPadding="0" width="230" border="0">
						<TR>
							<TD width="95">Date/Time</TD>
							<TD width="70">Pages</TD>
							<TD>Phone</TD>
						</TR>
					</TABLE>
					<asp:ListBox CssClass="InputForm" id="lstInbox" runat="server" Width="280px" Rows="20" SelectionMode="Multiple" Height="298px">
						<asp:ListItem Value="10/13/2004 10:12 ------ 3 ------ 789 78945656">10/13/2004 10:12 ------ 3 ------ 789 78945656</asp:ListItem>
					</asp:ListBox></TD>
				<TD align="center" width="47" height="314">
					<P>
						<asp:Button id="btnAttachFaxUser" runat="server" CssClass="Content Color2" Font-Bold="True" Width="26px" Text=">" onclick="btnAttachFaxUser_Click"></asp:Button></P>
					<P>
						<asp:Button id="btnDetachFaxUser" runat="server" CssClass="Content Color2" Font-Bold="True" Width="26px" Text="<" onclick="btnDetachFaxUser_Click"></asp:Button></P>
					<P>&nbsp;</P>
				</TD>
				<TD vAlign="top" align="center" height="314">
					<asp:Button id="cmdPreviewFaxEmployeeBox" runat="server" CssClass="Content Color2" Width="82px" Text="Preview Fax" onclick="cmdPreviewFaxEmployeeBox_Click"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:Button id="btnDeleteFaxMyBox" runat="server" CssClass="Content Color2" Width="82px" Text="Delete Fax" onclick="btnDeleteFaxMyBox_Click"></asp:Button><BR>
					<TABLE id="Table3" class="Content" height="23" cellSpacing="0" cellPadding="0" width="230" border="0">
						<TR>
							<TD width="93">Date/Time</TD>
							<TD width="71">Pages</TD>
							<TD>Phone</TD>
						</TR>
					</TABLE>
					<asp:ListBox id="lstEmployeebox" CssClass="InputForm" runat="server" Width="280px" Rows="20" Height="298px">
						<asp:ListItem Value="10/13/2004 10:12 ------ 3 ------ 789 78945656">10/13/2004 10:12 ------ 3 ------ 789 78945656</asp:ListItem>
					</asp:ListBox></TD>
			</TR>
			<TR>
				<TD vAlign="top" align="center" width="296" height="123">
					<asp:Label id="Label3" CssClass="Content" runat="server" Width="249px">Press "SHIFT" key for multiple selection</asp:Label><BR>
						<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD width="6"></TD>
								<TD align="center">
									<asp:Label id="lblMessagePreview" runat="server" Visible="False" ForeColor="Red">Select only one Fax to Preview it!</asp:Label></TD>
								<TD></TD>
							</TR>
						</TABLE>
						<BR>
				</TD>
				<TD align="center" width="47" height="123"></TD>
				<TD vAlign="top" align="right" height="123">
					<TABLE id="Table2" align="right" cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TR>
							<TD width="60"></TD>
							<TD width="124" align="center">
								<asp:Label id="Label4" CssClass="Content"  runat="server" Font-Bold="True">Attach to Order</asp:Label></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD width="13"></TD>
							<TD width="100" align="center">
								<asp:Label id="Label5" CssClass="Content" runat="server">Order #: </asp:Label></TD>
							<TD>
								<P align="left">
									<asp:Label id="Label6" CssClass="Content" runat="server">Fax Description:</asp:Label></P>
							</TD>
						</TR>
						<TR>
							<TD width="13" height="35"></TD>
							<TD width="100" height="35">
								<asp:DropDownList id="ddlOrders" CssClass="InputForm" runat="server" Width="118px"></asp:DropDownList></TD>
							<TD height="35">
								<P align="left">
									<asp:TextBox id="txtFaxDescription" CssClass="InputForm" runat="server" Width="140px"></asp:TextBox></P>
							</TD>
						</TR>
						<TR>
							<TD width="13"></TD>
							<TD colSpan="2">
								<P align="left">&nbsp;
									<asp:Button id="cmdMove" runat="server" CssClass="Content Color2" Width="63px" Text="Move" onclick="cmdMove_Click"></asp:Button>&nbsp;
									<asp:Button id="cmdMoveShowFolder" runat="server" CssClass="Content Color2" Width="141px" Text="Move and Show Folder" onclick="cmdMoveShowFolder_Click"></asp:Button></P>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</asp:panel>
		</table>
	</div>
	</asp:Content>

