using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using MetaBuilders.WebControls;
using TPE.Utility;

namespace localhost
{
	/// <summary>
	/// Summary description for task_list.
	/// </summary>
	public partial class Ports : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["Typ"].ToString() != "A" )
			{
				Response.Redirect("/default.aspx");
			}
			
			if (!IsPostBack)
			{
				//ddlPortCoutry.Items.Add();

				BindGroups();
				LoadCountries();
				LoadGroupsPorts(ddlPortGroup);

				BindPorts();
			}
		}
		
		private void BindGroups()
		{
			SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();

			string sbSQL = "SELECT PORT_GROUP_ID, PORT_GROUP_NAME FROM PORT_GROUP ORDER BY PORT_GROUP_NAME";
			SqlDataAdapter dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
			DataSet dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dgGroups.DataSource = dstContent;
			dgGroups.DataBind();
			
			conn.Close();
			lblMessage.Text = "";
		}

		private void BindPorts()
		{
			if (lblSelectedGroup.Text != "")
			{
				SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());
				conn.Open();

				string sbSQL = "SELECT IP.PORT_ID, IP.PORT_CITY, C.CTRY_NAME AS PORT_COUNTRY, IP.PORT_ENBL, IP.PORT_GROUP " + 
					"FROM INTERNATIONAL_PORT IP, COUNTRY C WHERE PORT_GROUP = " + lblSelectedGroup.Text + " AND IP.PORT_COUNTRY = C.CTRY_CODE " +
					"ORDER BY PORT_CITY";
				SqlDataAdapter dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
				DataSet dstContent = new DataSet();
				dadContent.Fill(dstContent);
				dgPorts.DataSource = dstContent;
				dgPorts.DataBind();
				dgPorts.Visible = true;
				conn.Close();
			}
			else
			{
				dgPorts.Visible = false;
			}
			lblMessage.Text = "";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgGroups.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgGroups_ItemCommand);
			this.dgPorts.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgPorts_ItemCommand);
			this.dgPorts.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgPorts_ItemDataBound);

		}
		#endregion

		private void LoadCountries()
		{
			ddlPortCoutry.Items.Clear();
			
			DataTable dtCountries = DBLibrary.GetDataTableFromSelect(Application["DBConn"].ToString(),"Select CTRY_CODE, CTRY_NAME FROM COUNTRY ORDER BY CTRY_NAME ASC");
			
			foreach(DataRow dr in dtCountries.Rows)
			{
				ddlPortCoutry.Items.Add(new ListItem((string)dr["CTRY_NAME"],dr["CTRY_CODE"].ToString()));
			}
		}

		private void LoadGroupsPorts(DropDownList ddlControl)
		{
			ddlControl.Items.Clear();			
			DataTable dtGroups = DBLibrary.GetDataTableFromSelect(Application["DBConn"].ToString(),"SELECT PORT_GROUP_ID, PORT_GROUP_NAME FROM PORT_GROUP ORDER BY PORT_GROUP_NAME");
			foreach (DataRow dr in dtGroups.Rows)
			{
				ddlControl.Items.Add(new ListItem((string)dr["PORT_GROUP_NAME"],dr["PORT_GROUP_ID"].ToString()));
			}			
		}

		private void dgPorts_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if  (e.Item.ItemType!=ListItemType.Header && e.Item.ItemType!=ListItemType.Footer)                  
			{ 
				DropDownList ddlGroups = (DropDownList)e.Item.Cells[4].Controls[1];
				LoadGroupsPorts(ddlGroups);
				ddlGroups.SelectedItem.Selected = false;
				ddlGroups.Items.FindByValue(e.Item.Cells[3].Text).Selected = true;

				CheckBox chkOrigin = (CheckBox)e.Item.Cells[6].Controls[1];
				chkOrigin.Checked = e.Item.Cells[5].Text=="1" ? true : false;
			}
		}

		private void dgPorts_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			lblMessage.Text = "";
			string SQLStatement="";
			Hashtable param = new Hashtable();
			if (e.CommandName == "Delete") //delete
			{
				//Checking out if the group has any Port associated
				SQLStatement = "SELECT 1 FROM FREIGHT_MATRIX_INTERNATIONAL WHERE IFM_ORIGIN_PORT = @Port";// + e.Item.Cells[0].Text;
				param.Add("@Port",e.Item.Cells[0].Text);
				DataTable dtPort = DBLibrary.GetDataTableFromSelect(Application["DBConn"].ToString(),SQLStatement,param);
				//SqlDataReader rdPort = db.ExecuteReader(SQLStatement);
				if (dtPort.Rows.Count == 0)
				{
					SQLStatement = "DELETE FROM INTERNATIONAL_PORT WHERE PORT_ID = @Port";
					DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), SQLStatement,param);
					BindPorts();
				}
				else
				{
					lblMessage.Text = "You cannot delete the Port of " + e.Item.Cells[1].Text + ". There are Freights associated to this port";
				}
			}
			else if (e.CommandName == "Save") //save
			{
				SQLStatement = "UPDATE INTERNATIONAL_PORT SET PORT_GROUP = @PortGroup, PORT_ENBL = @PortEnbl WHERE PORT_ID = @PortID";
				param.Add("@PortGroup",((DropDownList)e.Item.Cells[4].Controls[1]).SelectedValue);
				param.Add("@PortEnbl", ((CheckBox)e.Item.Cells[6].Controls[1]).Checked ? "1":"0");
				param.Add("@PortID",e.Item.Cells[0].Text);
				DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), SQLStatement, param);
			}
		}

		private void dgGroups_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			lblMessage.Text = "";
			string SQLStatement="";
			if (e.CommandName == "Delete") //delete
			{
				//Checking out if the group has any Port associated
				//SQLStatement = "SELECT 1 FROM INTERNATIONAL_PORT WHERE PORT_GROUP = " + e.Item.Cells[0].Text + " UNION SELECT 2 FROM FREIGHT_MATRIX_INTERNATIONAL WHERE IFM_DESTINATION_GROUP = " + e.Item.Cells[0].Text;
				SQLStatement = "SELECT 1 FROM INTERNATIONAL_PORT WHERE PORT_GROUP = @PORTGROUP UNION SELECT 2 FROM FREIGHT_MATRIX_INTERNATIONAL WHERE IFM_DESTINATION_GROUP = @DESTINATIONGROUP";
				Hashtable param = new Hashtable();
				param.Add("@PORTGROUP",e.Item.Cells[0].Text);
				param.Add("@DESTINATIONGROUP",e.Item.Cells[0].Text);
				DataTable dtPort = DBLibrary.GetDataTableFromSelect(Application["DBConn"].ToString(),SQLStatement,param);
				if (dtPort.Rows.Count==0)
				{
					if (e.Item.Cells[0].Text == lblSelectedGroup.Text) lblSelectedGroup.Text = "";
					SQLStatement = "DELETE FROM PORT_GROUP WHERE PORT_GROUP_ID = @PORTGROUP";
					param.Clear();
					param.Add("@PORTGROUP",e.Item.Cells[0].Text);
					DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), SQLStatement, param);
					LoadGroupsPorts(ddlPortGroup);
					BindGroups();
				}
				else
				{
					if (dtPort.Rows[0][0].ToString()=="1")	//rdPort[0].ToString()=="1")
						lblMessage.Text = "You cannot delete the Group " + ((LinkButton)e.Item.Cells[1].Controls[0]).Text + ". There are Ports associated to this group.";
					else //2
						lblMessage.Text = "You cannot delete the Group " + ((LinkButton)e.Item.Cells[1].Controls[0]).Text + ". There are Freight associated to this group.";
				}
			}
			else if (e.CommandName == "List") //save
			{
				lblSelectedGroup.Text = e.Item.Cells[0].Text;
				BindPorts();
			}
		}

		protected void btnAddGroup_Click(object sender, System.EventArgs e)
		{
			if (txtNewGroup.Text!="")
			{
				string SQLStatement="INSERT INTO PORT_GROUP(PORT_GROUP_NAME) VALUES(@NewGroup)";//'" + txtNewGroup.Text + "')";
				Hashtable param = new Hashtable();
				param.Add("@NewGroup",txtNewGroup.Text);
				DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), SQLStatement, param);

				txtNewGroup.Text = "";
				BindGroups();
				LoadGroupsPorts(ddlPortGroup);
				LoadCountries();
			}
		}

		protected void btnAddPort_Click(object sender, System.EventArgs e)
		{
			string SQLStatement="INSERT INTO INTERNATIONAL_PORT(PORT_CITY, PORT_COUNTRY, PORT_ENBL, PORT_GROUP) VALUES(@PortCity,@PortCountry,@IsOriginPort,@PortGroup)";
			Hashtable param = new Hashtable();
			param.Add("@PortCity",txtPortCity.Text);
			param.Add("@PortCountry",ddlPortCoutry.SelectedItem.Value);
			param.Add("@IsOriginPort",(chkOriginPort.Checked ? "1":"0"));
			param.Add("@PortGroup",ddlPortGroup.SelectedItem.Value);
			DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), SQLStatement, param);

			txtPortCity.Text = "";
			chkOriginPort.Checked = false;
			LoadCountries();
			LoadGroupsPorts(ddlPortGroup);
			BindPorts();
		}
	}
}
