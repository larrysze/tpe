using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;


namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Demo_Promote.
	/// </summary>
	public class Demo_Promote : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox txtf;
		protected System.Web.UI.WebControls.TextBox txtl;
		protected System.Web.UI.WebControls.TextBox txttitle;
		protected System.Web.UI.WebControls.TextBox txtp;
		protected System.Web.UI.WebControls.TextBox txte;
		protected System.Web.UI.WebControls.TextBox txtfax;
		protected System.Web.UI.WebControls.TextBox txtcname;
		protected System.Web.UI.WebControls.TextBox txtsite;
		protected System.Web.UI.WebControls.TextBox txtcredit;
		protected System.Web.UI.WebControls.TextBox txta1;
		protected System.Web.UI.WebControls.TextBox txtci;
		protected System.Web.UI.WebControls.TextBox txtzip;
		protected System.Web.UI.WebControls.DropDownList ddlstate;
		protected System.Web.UI.WebControls.DropDownList ddlCountry;
		protected System.Web.UI.WebControls.DropDownList ddlCountry2; //list for the bank
		protected System.Web.UI.WebControls.ImageButton imagebutton1;
		protected System.Web.UI.WebControls.ImageButton imagebutton2;
		protected System.Web.UI.WebControls.ImageButton Imagebutton5;
		protected System.Web.UI.WebControls.Panel pnlstep1;
		protected System.Web.UI.WebControls.TextBox txtbanme;
		protected System.Web.UI.WebControls.TextBox txtbcontact;
		protected System.Web.UI.WebControls.TextBox txtbphone;
		protected System.Web.UI.WebControls.TextBox txtBfax;
		protected System.Web.UI.WebControls.TextBox txtBadd;
		protected System.Web.UI.WebControls.TextBox txtBcity;
		protected System.Web.UI.WebControls.DropDownList ddlstate2;
		protected System.Web.UI.WebControls.TextBox txtzip2;
		protected System.Web.UI.WebControls.TextBox txtBacc;
		protected System.Web.UI.WebControls.TextBox txtbre;
		protected System.Web.UI.WebControls.TextBox txtaba;
		protected System.Web.UI.WebControls.ImageButton imagebutton3;
		protected System.Web.UI.WebControls.ImageButton imagebutton4;
		protected System.Web.UI.WebControls.HyperLink OtherCompagny;
		protected System.Web.UI.WebControls.DropDownList ddlListComp;
		protected System.Web.UI.WebControls.Panel pnlstep2;
		protected System.Web.UI.WebControls.Panel panel1;
		protected System.Web.UI.WebControls.CheckBox txtselling;
		protected System.Web.UI.WebControls.CheckBox txtbuying;
		protected System.Web.UI.WebControls.Panel pnlMiss;
		protected System.Web.UI.WebControls.Panel pnlMissComp;
		protected System.Web.UI.WebControls.Panel panelBankErr;
		
		protected System.Web.UI.WebControls.Panel  panelFinish; // will be deleted!

		protected System.Web.UI.WebControls.Panel pnFinish;
		protected System.Web.UI.WebControls.Panel pnFinish_Bank;
		protected System.Web.UI.WebControls.Button Button1;

		//public int temp1;	
		public string temp2;

		//public HttpSessionState S1; //Session

		public void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				BindState();
				Bind_DropDownList_CompName();
				Bind_DropDownList_CompState();
				Bind_DropDownList_CompCountry();
				Bind_DropDownList_bankCountry();
			}
		}
		public void Bind_DropDownList_bankCountry()
		{// create the dropdown list for the company and the bank
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			SqlDataReader dtrComp;
			conn.Open();

			// Query buyer companies name
			SqlCommand cmd_Comp;
			cmd_Comp = new SqlCommand("SELECT CTRY_CODE,CTRY_NAME FROM COUNTRY ORDER BY CTRY_NAME ASC",conn);
			dtrComp = cmd_Comp.ExecuteReader();

			//binding buyer company drop-down list
			ddlCountry2.DataSource = dtrComp;
			ddlCountry2.DataTextField= "CTRY_NAME"; 
			ddlCountry2.DataValueField= "CTRY_CODE";
			ddlCountry2.DataBind();
			ddlCountry2.Items.Add("Other");
			dtrComp.Close();
			conn.Close(); 

			ListItem li1 = ddlCountry2.Items.FindByValue("US");
			ddlCountry2.SelectedIndex = -1;
			li1.Selected = true;

		}
		public void Bind_DropDownList_CompCountry()
		{// create the dropdown list for the company and the bank
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			SqlDataReader dtrComp;
			conn.Open();

			// Query buyer companies name
			SqlCommand cmd_Comp;
			cmd_Comp = new SqlCommand("SELECT CTRY_CODE,CTRY_NAME FROM COUNTRY ORDER BY CTRY_NAME ASC",conn);
			dtrComp = cmd_Comp.ExecuteReader();

			//binding buyer company drop-down list
			ddlCountry.DataSource = dtrComp;
			ddlCountry.DataTextField= "CTRY_NAME"; 
			ddlCountry.DataValueField= "CTRY_CODE";
			ddlCountry.DataBind();
			ddlCountry.Items.Add("Other");
			dtrComp.Close();
			conn.Close(); 

			ListItem li1 = ddlCountry.Items.FindByValue("US");
			ddlCountry.SelectedIndex = -1;
			li1.Selected = true;
		}
		public void Bind_DropDownList_CompState()
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			SqlDataReader dtrComp;
			conn.Open();

			// Query buyer companies name
			SqlCommand cmd_Comp;
			cmd_Comp = new SqlCommand("SELECT STAT_CODE,STAT_NAME FROM STATE ORDER BY STAT_NAME ASC",conn);
			dtrComp = cmd_Comp.ExecuteReader();

			//binding buyer company drop-down list
			ddlstate.DataSource = dtrComp;
			ddlstate.DataTextField= "STAT_NAME"; 
			ddlstate.DataValueField= "STAT_CODE";
			ddlstate.DataBind();
			ddlstate.Items.Add("Other");
			dtrComp.Close();
			conn.Close(); 
		}		
		
		public void Bind_DropDownList_CompName()
		{// Creation of the Compagny List
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			SqlDataReader dtrComp;
			conn.Open();

			// Query buyer companies name
			SqlCommand cmd_Comp;
			//cmd_Comp = new SqlCommand("SELECT COMP_ID,COMP_NAME FROM COMPANY WHERE COMP_TYPE IN ('D','P') AND COMP_ID IN (SELECT DISTINCT PERS_COMP FROM PERSON WHERE PERS_TYPE = 'O' OR PERS_TYPE='P')  ORDER BY COMP_NAME ASC",conn);
			cmd_Comp = new SqlCommand("SELECT COMP_ID,COMP_NAME FROM COMPANY WHERE COMP_TYPE IN ('D','P') ORDER BY COMP_NAME ASC",conn);
			dtrComp = cmd_Comp.ExecuteReader();

			//binding buyer company drop-down list
			ddlListComp.DataSource = dtrComp;
			ddlListComp.DataTextField= "COMP_NAME"; 
			ddlListComp.DataValueField= "COMP_ID";
			ddlListComp.DataBind();
			ddlListComp.Items.Add("Other");
			dtrComp.Close();
			conn.Close(); 
		}
		public void BindState()
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			SqlDataReader dtrComp;
			conn.Open();

			// Query buyer companies name
			SqlCommand cmd_Comp;
			cmd_Comp = new SqlCommand("SELECT STAT_CODE,STAT_NAME FROM STATE ORDER BY STAT_NAME ASC",conn);
			dtrComp = cmd_Comp.ExecuteReader();

			//binding buyer company drop-down list
			ddlstate2.DataSource = dtrComp;
			ddlstate2.DataTextField= "STAT_NAME"; 
			ddlstate2.DataValueField= "STAT_CODE";
			ddlstate2.DataBind();
			ddlstate2.Items.Add("Other");
			dtrComp.Close();
			conn.Close(); 
		}


		public void changeToOther(object sender,EventArgs e)
		{//step 1
			ListItem _li = ddlListComp.Items.FindByValue("Other");
			ddlListComp.SelectedIndex = -1;
			_li.Selected = true;
			panel1.Visible = true;
		}
		public void checkDropDown(object sender, EventArgs e)
		{//step 1	
			if(ddlListComp.SelectedItem.ToString() != "Other")
			{
				ListItem li3 = ddlListComp.Items.FindByValue(ddlListComp.SelectedItem.Value.ToString());
				ddlListComp.SelectedIndex = -1;
				li3.Selected = true;
				panel1.Visible = false;
			}
		}
		
		public string replaceSlashes(string toReplace)
		{
			return toReplace.Replace('"','\"').Replace("\'","\\'");
			
		}
		public void Continue(object sender, ImageClickEventArgs e)
		{	

			if(txtf.Text == "" || txtl.Text == "")
			{
				pnlMiss.Visible = true;			
			}
			else if(txtcname.Text == "" && ddlListComp.SelectedItem.Value.ToString() == "Other")
				pnlMissComp.Visible = true;
			else
			{
				//If the company does not figure in the list
				ListItem _li = ddlListComp.Items.FindByValue("Other");
				if ( _li.Selected == true)
				{	
					pnlstep1.Visible = false;
					pnlstep2.Visible = true;
					//Test if the bank is attributed for the chosen company
					int comp_Id=0;
					if(ddlListComp.SelectedItem.Value.ToString() != "Other")
						comp_Id = Convert.ToInt32(ddlListComp.SelectedItem.Value.ToString());
					//BindState();
	
					if(ddlListComp.SelectedItem.Value.ToString() != "Other")
					{
						SqlConnection conn;
						conn = new SqlConnection(Application["DBConn"].ToString());
						conn.Open();
	
						SqlCommand cmdCheckBank;
						SqlDataReader dtrCheckBank;
						cmdCheckBank= new SqlCommand("SELECT * FROM BANK WHERE (BANK_COMP = '"+comp_Id+"')", conn);
						dtrCheckBank= cmdCheckBank.ExecuteReader(); 
					
						if (dtrCheckBank.Read())
						{  
							//Response.Write("A Bank is already attributed to the compagny<br><br>");
							//FinishBis();
							txtbanme.Text = dtrCheckBank["BANK_NAME"].ToString();
							txtbcontact.Text = dtrCheckBank["BANK_PERS"].ToString();
							txtbphone.Text = dtrCheckBank["BANK_PHON"].ToString();
							txtBfax.Text = dtrCheckBank["BANK_FAX"].ToString();
							txtBadd.Text = dtrCheckBank["BANK_ADDR_ONE"].ToString();
							txtBcity.Text = dtrCheckBank["BANK_CITY"].ToString();
												
							if(dtrCheckBank["BANK_STAT"] != null) 
							{
								ListItem li2 = ddlstate2.Items.FindByValue(dtrCheckBank["BANK_STAT"].ToString());
								ddlListComp.SelectedIndex = -1;
								li2.Selected = true;
							}
							//Response.Write(dtrCheckBank["BANK_STAT"].ToString());
							//Response.Write(vartemp);
							txtzip2.Text = dtrCheckBank["BANK_ZIP"].ToString();
						}
						else
						{
							txtbanme.Text = "";
							txtbcontact.Text = "";
							txtbphone.Text = "";
							txtBfax.Text = "";
							txtBadd.Text = "";
							txtBcity.Text = "";
							txtzip2.Text ="";
						}
						dtrCheckBank.Close();
						conn.Close();					
					}
					else
					{
						txtbanme.Text = "";
						txtbcontact.Text = "";
						txtbphone.Text = "";
						txtBfax.Text = "";
						txtBadd.Text = "";
						txtBcity.Text = "";
						txtzip2.Text ="";
					}
					pnlstep1.Visible = false;
					pnlstep2.Visible = true;
				}
				else
				{ 
					//test if a bank is assigned to the chosen company
					SqlConnection conn;
					conn = new SqlConnection(Application["DBConn"].ToString());
					conn.Open();
	
					SqlCommand cmdCheckBank;
					SqlDataReader dtrCheckBank;
					cmdCheckBank= new SqlCommand("SELECT * FROM BANK WHERE (BANK_COMP = '"+ddlListComp.SelectedItem.Value.ToString()+"')", conn);
					dtrCheckBank= cmdCheckBank.ExecuteReader(); 
					
					if (dtrCheckBank.Read())
					{
						FinishBis();
					}
					else
					{
						pnlstep1.Visible = false;
						pnlstep2.Visible = true;
						pnFinish.Visible = false;
						pnFinish_Bank.Visible = true;
					}
				}
			}
		}


		
		public void SaveCompany()
		{//Insert into company
			SqlConnection conn2;
			conn2 = new SqlConnection(Application["DBConn"].ToString());
			SqlDataReader dtrComp2;
			string strSQL2;
			//s=seller p=puschaser d=both
			//tester affichage values des checkbox
			string personState="";
			if(txtselling.Checked == true && txtbuying.Checked == false)
				personState = "s";
			else if(txtselling.Checked == false && txtbuying.Checked == true)
				personState = "p";
			else if(txtselling.Checked == true && txtbuying.Checked == true)
				personState = "d";

			strSQL2 = "INSERT INTO COMPANY (COMP_TYPE, COMP_NAME, COMP_URL, COMP_REG, COMP_DATE, COMP_FED_TAX, COMP_STAT_TAX, COMP_DUNS, COMP_SIC,COMP_STRC, COMP_STAT_INC, COMP_PUB, COMP_OWN, COMP_FELN, COMP_BKRP, COMP_ENBL, COMP_PREF) VALUES('"+personState+"','"+replaceSlashes(txtcname.Text.ToString())+"','"+replaceSlashes(txtsite.Text.ToString())+"','1',GETDATE(),'"+replaceSlashes(txtfax.Text.ToString())+"','','','','','"+replaceSlashes(ddlstate.SelectedValue.ToString())+"','','','','','1','')";
			conn2.Open();
			SqlCommand cmd_Comp2;
			cmd_Comp2 = new SqlCommand(strSQL2,conn2);
			dtrComp2 = cmd_Comp2.ExecuteReader();
			conn2.Close();
		}
		public void Cancel(object sender, ImageClickEventArgs e)
		{//step 1
			Response.Redirect("../research/Dashboard.aspx?Cont_Id=3");
		}
		public void Continue2(object sender, ImageClickEventArgs e)
		{
			pnlstep1.Visible = true;
			pnlstep2.Visible = false;
		}

		public void Continuebis()
		{
			pnlstep1.Visible = false;
			pnlstep2.Visible = true;
		}
		public bool testBankAccount()
		{
			if ( txtbanme.Text == "" || txtaba.Text == "" || txtBacc.Text == "" )
			{
				return false;
			}
			else
			{	
				return true;
			}
		}

		//If a new company is created
		public void Finish(object sender, ImageClickEventArgs e)
		{
			string IDbis="";
			bool ette = testBankAccount();
			if(ette == true)
			{
				//Response.Write(<br>);
				SaveCompany();
				SavePerson();
				SaveBankInfo();
				SaveLocality();
				panel1.Visible = false;
				pnlMiss.Visible = false;
				pnlMissComp.Visible = false;
				pnlstep2.Visible = false;
				panelBankErr.Visible = false;
				
			
				SqlConnection conn;
				conn = new SqlConnection(Application["DBConn"].ToString());
				conn.Open();
				SqlCommand cmdMax;
				SqlDataReader dtrMax;
				cmdMax= new SqlCommand("SELECT MAX(COMP_ID) AS MAXI FROM COMPANY", conn);
				dtrMax= cmdMax.ExecuteReader(); 

				if(dtrMax.Read())
				{
					IDbis=dtrMax["MAXI"].ToString();
				}
				
				conn.Close();
				Server.Transfer("../administrator/Update_Company_Info.aspx?Id="+IDbis);	
			}
			else
			{
				panelBankErr.Visible = true;
			}
		}
		
		//If the company is chosen in the List && a bank is attributted to it
		public void FinishBis()
		{
			string IDbis="";
			SavePerson();
			//Response.Write("New Custumer registered<br>");
			IDbis = ddlListComp.SelectedValue.ToString();
			//Response.Write("ID Company :"+IDbis+"<br><br>");
			Server.Transfer("../administrator/Update_Company_Info.aspx?Id="+IDbis);
		}
		
		//If the company is chosen in the List && without a bank is attributted to it
		public void FinishBis_Bank(object sender, ImageClickEventArgs e)
		{
			string IDbis="";
			SavePerson();

			bool ette = testBankAccount();
			Response.Write("ette: "+ette.ToString());
			if(ette == true)
			{
				SaveBankInfo();
				panel1.Visible = false;
				pnlMiss.Visible = false;
				pnlMissComp.Visible = false;
				pnlstep2.Visible = false;
				panelBankErr.Visible = false;
				IDbis = ddlListComp.SelectedValue.ToString();
				Server.Transfer("../administrator/Update_Company_Info.aspx?Id="+IDbis);
			}
			else
			{		
				panelBankErr.Visible = true;
			}
		}


		/********************************************************************************/
		//						Person Registration
		/********************************************************************************/

		public void SavePerson()
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			

			string strSQL1="";
			if(ddlListComp.SelectedItem.Value == "Other")
			{
				//Response.Write("other ok...");
				conn.Open();
				SqlCommand cmdMax;
				SqlDataReader dtrMax;
				cmdMax= new SqlCommand("SELECT MAX(COMP_ID) AS MAXI FROM COMPANY", conn);
				dtrMax= cmdMax.ExecuteReader(); 

				if(dtrMax.Read())
				{
					strSQL1=dtrMax["MAXI"].ToString();
				}
				conn.Close();
			}
			else
				strSQL1=ddlListComp.SelectedItem.Value.ToString();
			
			string check;	
			SqlCommand cmdchecklogin;
			SqlDataReader dtrchecklogin;
			conn.Open();
			cmdchecklogin= new SqlCommand("Select PERS_FRST_NAME,PERS_LAST_NAME from Person where PERS_FRST_NAME='"+txtf.Text+"' AND PERS_LAST_NAME='"+txtl.Text+"'", conn);
			dtrchecklogin= cmdchecklogin.ExecuteReader(); 

			if (dtrchecklogin.Read())
			{  
				check="exsit";
				dtrchecklogin.Close();
			}
			else
			{
				check="";
				dtrchecklogin.Close();    
			}
			conn.Close();
			
			if (check != "exsit") 
			{
				//this user is not registered, we can insert it into PERSON table.
				SqlConnection conn2;
				conn2 = new SqlConnection(Application["DBConn"].ToString());
				conn2.Open();

				string strSQL;
				
				//Response.Write(strSQL1);

				strSQL="INSERT INTO PERSON (PERS_LAST_NAME,PERS_FRST_NAME,PERS_TYPE,PERS_TITL,PERS_PHON,PERS_FAX,PERS_MAIL,PERS_DATE,PERS_ENBL,PERS_REG,PERS_LAST_CHKD,PERS_ALIA,PERS_PREF,PERS_COMP) VALUES('"+txtl.Text+"','"+txtf.Text+"','O','"+txttitle.Text+"','"+txtp.Text+"','"+txtfax.Text+"','"+txte.Text+"',getdate(),1,1,getdate(),Null,268435455,'"+strSQL1+"')"; //"+Convert.ToInt32(strSQL1)+"
				
				SqlCommand cmd_Add_Pers;
				cmd_Add_Pers= new SqlCommand(strSQL, conn2);
				cmd_Add_Pers.ExecuteNonQuery();
				conn2.Close();
				//Response.Write("enregistrement effectue...<br><br>");
			}
		}


		/********************************************************************************/
		//							Bank Registration
		/********************************************************************************/

		public void SaveBankInfo()
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			string strSQL1="";
			if(ddlListComp.SelectedItem.Value == "Other")
			{
				SqlCommand cmdMax;
				SqlDataReader dtrMax;
				cmdMax= new SqlCommand("SELECT MAX(COMP_ID) AS MAXI FROM COMPANY", conn);
				dtrMax= cmdMax.ExecuteReader(); 

				if(dtrMax.Read())
				{
					strSQL1=dtrMax["MAXI"].ToString();;
				}
			}
			else
				strSQL1=ddlListComp.SelectedItem.Value.ToString();
			conn.Close();
		   //Response.Write("temp2 SaveBankInfo = " + ddlListComp.SelectedItem.Value.ToString());
			if ( txtbanme.Text != "" && txtaba.Text != "" && txtBacc.Text != "" )
			{
				string strSaveBank;				
				strSaveBank="INSERT INTO BANK VALUES('"+strSQL1+"','"+replaceSlashes(txtbanme.Text)+"','"+replaceSlashes(txtbcontact.Text)+"','"+replaceSlashes(txtBacc.Text)+"','"+replaceSlashes(txtBadd.Text)+"','','"+replaceSlashes(txtBcity.Text)+"','"+txtzip2.Text+"','"+ddlstate2.SelectedItem.Value+"','"+ddlCountry2.SelectedItem.Value+"','"+replaceSlashes(txtbphone.Text)+"','"+replaceSlashes(txtBfax.Text)+"','"+replaceSlashes(txtaba.Text)+"','"+replaceSlashes(txtbre.Text)+"')";
				
				SqlCommand cmd_Add_Bank;
				conn.Open();
				cmd_Add_Bank= new SqlCommand(strSaveBank, conn);
				cmd_Add_Bank.ExecuteNonQuery();
				conn.Close();
				//Response.Write("Bank recorded...<br><br>");
			}
			else
			{
				//Response.Write("Fields are missing!<br><br>");
				Continuebis();
				//Response.Redirect("Demo_Promote.aspx");
			}
		}		


		/********************************************************************************/
		//					Compagny Locality Registration
		/********************************************************************************/

		public void SaveLocality()
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();

			string strSaveLocality;				
			string strIdLocality = "";

			string strSQL1="";
			if(ddlListComp.SelectedItem.Value == "Other")
			{
				SqlCommand cmdMax;
				SqlDataReader dtrMax;
				cmdMax= new SqlCommand("SELECT MAX(COMP_ID) AS MAXI FROM COMPANY", conn);
				dtrMax= cmdMax.ExecuteReader(); 

				if(dtrMax.Read())
				{
					strSQL1=dtrMax["MAXI"].ToString();;
				}
			}
			else
				strSQL1=ddlListComp.SelectedItem.Value.ToString();
			conn.Close();
			
			conn.Open();
			strSaveLocality="INSERT INTO LOCALITY (LOCL_ID ,LOCL_CITY,LOCL_STAT,LOCL_CTRY) VALUES('"+strSQL1+"','"+txtci.Text+"','"+ddlstate.SelectedItem.Value+"','"+ddlCountry.SelectedItem.Value+"')";
			SqlCommand cmd_Locality;
			cmd_Locality = new SqlCommand(strSaveLocality, conn);
			cmd_Locality.ExecuteNonQuery();
			conn.Close();

			conn.Open();
			strIdLocality="SELECT max(LOCL_ID) AS Last_Loc FROM LOCALITY";			
			SqlCommand cmd_IdLocality;
			SqlDataReader dtr_IdLocality;
			cmd_IdLocality = new SqlCommand(strIdLocality, conn);
			dtr_IdLocality = cmd_IdLocality.ExecuteReader();
			dtr_IdLocality.Read();

			int Last_Loc_Id;
			Last_Loc_Id = Convert.ToInt32(dtr_IdLocality["Last_Loc"].ToString());

			dtr_IdLocality.Close();
			conn.Close();

			SavePlace(Last_Loc_Id);
		}

		private void InitializeComponent()
		{
		
		}


		/********************************************************************************/
		//					Compagny Place Registration
		/********************************************************************************/

		public void SavePlace(int ID_Locality)
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();

			string strSQL1="";
			if(ddlListComp.SelectedItem.Value == "Other")
			{
				SqlCommand cmdMax;
				SqlDataReader dtrMax;
				cmdMax= new SqlCommand("SELECT MAX(COMP_ID) AS MAXI FROM COMPANY", conn);
				dtrMax= cmdMax.ExecuteReader(); 

				if(dtrMax.Read())
				{
					strSQL1=dtrMax["MAXI"].ToString();;
				}
			}
			else
				strSQL1=ddlListComp.SelectedItem.Value.ToString();
			conn.Close();

            string strSavePlace;				
			strSavePlace="INSERT INTO PLACE (PLAC_COMP,PLAC_LOCL,PLAC_PERS,PLAC_PHON,PLAC_ADDR_ONE,PLAC_ZIP,PLAC_ENBL) VALUES('"+strSQL1+"','"+ID_Locality+"','"+txtf.Text+" "+txtl.Text+"','"+txtp.Text+"','"+txta1.Text+"','"+txtzip.Text+"','1')";
				
			SqlCommand cmd_Place;
			conn.Open();
			cmd_Place= new SqlCommand(strSavePlace, conn);
			cmd_Place.ExecuteNonQuery();
			//Response.Write("<br>locality");
			conn.Close();
		}
	}
}
