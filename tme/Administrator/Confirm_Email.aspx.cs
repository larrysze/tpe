using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Configuration;

namespace localhost.Administrator
{
	/// <summary>
	///
	/// </summary>
	public class offers_mail : System.Web.UI.Page
	{
		protected System.Data.SqlClient.SqlConnection myConn;
		protected System.Data.SqlClient.SqlDataAdapter ad;
		protected System.Data.SqlClient.SqlDataReader rd;
		protected System.Data.SqlClient.SqlCommand mycomm;
		protected System.Web.UI.WebControls.Label errorMessage;
		protected System.Web.UI.WebControls.Button sentMail;
		private System.Text.RegularExpressions.Regex rex;
		protected System.Web.UI.WebControls.TextBox txtBody;
		private string UserId="";
        //private string ComId="";//save the the company id ,if the item is created by the same person unselected checkbox
		private int userNumber=-1;
		protected System.Web.UI.WebControls.DataGrid mainGrid;
		protected System.Web.UI.WebControls.Label hidderContent;
		protected System.Web.UI.WebControls.Label hiddenTop;
		protected System.Web.UI.WebControls.TextBox lblBidOffers;
		protected System.Web.UI.WebControls.TextBox SecTextBox;
		protected System.Web.UI.WebControls.Button exchange;//
		private string userFirstName="";
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(!Page.IsPostBack)
			{
				JudgeUserType();//This page can not be visited by the customers who do not belong A or B class.
				LoginMail();//get user's email and save in the ViewState["LoginEmail"].
				//The following codes will generate the contents appearing in the textbox.
				CreatDbConnect();//connect DataBase
				DbGridBind();//mainGrid were being  populated
				StringBuilder sbText = new StringBuilder();
				StringBuilder scText=new StringBuilder();
				//if userNumber is 0 then means only one user has been selected
				if(this.userNumber==0)
				{
					sbText.Append("Dear "+this.userFirstName+","+((char)(13)).ToString()+((char)(13)).ToString());
				}
				else
				{
					sbText.Append("Dear <PERSON FIRST NAME>,"+((char)(13)).ToString()+((char)(13)).ToString());
				}
				if(Session["strCompId"]!=null)//Session["strCompId"]!=nullthis.lblBidOffers.SelectedValue.Equals("Resin Requests")
				{
					lblBidOffers.Text = "Resin Requests";
					sbText.Append("We are looking for the following resin today.  Please let us know if you have something close."+((char)(13)).ToString()+((char)(13)).ToString());
				}
				else
				{
					lblBidOffers.Text = "Resin Offers";
					sbText.Append("Based on the resin preferences that you provided upon registration, we are sending you the following resin offers."+((char)(13)).ToString()+((char)(13)).ToString());
					sbText.Append("Please call or reply to this email if we can serve you.");
				}
				// split the Body textbox into two Label one is hiddentop other is hidderContent
				if(Session["strCompId"]==null)
				{
					scText.Append( "Hurry, they are subject to prior sale!"+((char)(13)).ToString()+((char)(13)).ToString());
				}
				else
				{
					scText.Append( "Thanks!"+((char)(13)).ToString()+((char)(13)).ToString());
				}
				scText.Append("Sincerely,"+((char)(13)).ToString()+((char)(13)).ToString());
				scText.Append(Session["Name"].ToString()+((char)(13)).ToString());
				scText.Append("The Plastics Exchange"+((char)(13)).ToString());
				scText.Append("312.202.0002");
				txtBody.Text = sbText.ToString();//top of the body
				this.SecTextBox.Text=scText.ToString();//bottom of the body
				if(Session["strExport"]==null)
				{
					this.exchange.Text="Convert Prices for International Market";
				}
				else
				{
					this.exchange.Text="Convert Prices for U.S. Market";
				}
			}
		}
		private void LoginMail()
		{
			if(Session["Id"]!=null)
			{
				string sql="select pers_id,pers_mail from person where pers_id="+Session["Id"].ToString();//through user's Id get user's email from the table "person"
				myConn=new 
System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString());
				myConn.Open();
				this.mycomm=new System.Data.SqlClient.SqlCommand(sql,myConn);
				this.rd=mycomm.ExecuteReader(CommandBehavior.CloseConnection);
				if(rd.Read())
				{
					ViewState["LoginMail"]=rd["pers_mail"].ToString();//save the email to the ViewState.when sent the mail,the user's id will be used .
				}
				rd.Close();
			}
		}
		private void CreatDbConnect()
		{
			myConn=new System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString());
		}
		private void JudgeUserType()//judge the user type
		{
			if((Session["Typ"].ToString()!="A")&&(Session["Typ"].ToString()!="B"))
			{
				Response.Redirect("/default.aspx");
			}
		}
		public string EmailOffer_Address//return the user list you selected from session
		{
			//if the Session["strEmailOffer_Address"]is not null or Length is not 0 then return the user list else return empty
			get
			{
				if(Session["strEmailOffer_Address"]!=null)
				{
					if(Session["strEmailOffer_Address"].ToString().Length!=0)
					{
						return Session["strEmailOffer_Address"].ToString().Substring(0,Session["strEmailOffer_Address"].ToString().Length-1);
					}
					else
					{
						return String.Empty;
					}
				}
				else
				{
					return String.Empty;
				}
			}
		}
		private void DbGridBind()//bind the data to mainGride(the user list) if it is null return error
		{
			rex=new System.Text.RegularExpressions.Regex(",");
			//if user list is not empty then bind the mainGrid the show the error
			if(this.EmailOffer_Address!=null)
			{
				if(EmailOffer_Address.Length!=0)
				{
					this.errorMessage.Text="";
					string[] emailAddress=rex.Split(this.EmailOffer_Address);//change the user list to array
					string sqlstr="";
					// the following codes will get a data list from table "person".the mainGrid will show the list.
					sqlstr="select PERS_ID,PERS_FRST_NAME, PERS_LAST_NAME, PERS_MAIL, PERS_PREF,EMAIL_ENBL,PERS_COMP from PERSON where EMAIL_ENBL ='1' AND PERS_ID in (";
					for(int i=0;i<emailAddress.Length;i++)
					{
						sqlstr+="'"+emailAddress[emailAddress.Length-1-i].ToString()+"',";
						this.userNumber=i;//count the loop
					}
					sqlstr=sqlstr.Substring(0,sqlstr.Length-1);
					sqlstr+=") order by pers_id desc";
					this.myConn.Open();
					mycomm=new System.Data.SqlClient.SqlCommand(sqlstr,myConn);
					rd=mycomm.ExecuteReader(CommandBehavior.CloseConnection);
					this.mainGrid.DataSource=rd;
					this.mainGrid.DataBind();
					rd.Close();
					if(this.userNumber==0)
					{
						foreach(DataGridItem myitem in this.mainGrid.Items)
						{
							this.rex=new System.Text.RegularExpressions.Regex(",");
							string[] user=rex.Split(((System.Web.UI.WebControls.Label)myitem.FindControl("Label1")).Text);
							this.userFirstName=user[0];
						}
					}
				}
				else
				{
					this.errorMessage.Text="ERROR: Please select at least one user from the demo leads page.";
				}
			}
			else
			{
				this.errorMessage.Text="ERROR: Please select at least one user from the demo leads page.";
			}
		}
		private string Parse(string TextIN)//if TextIN is less than zero then delete zero.
		{
			double number;
			try
			{
				number=Convert.ToDouble(TextIN);
			}
			catch
			{
				return String.Empty;
			}
			if(number>=1)
			{
				return TextIN;
			}
			else
			{
				int state=TextIN.LastIndexOf(".");
				return TextIN.Substring(state,TextIN.Length-1);
			}
		}
		public string isEmpty
		{
			set
			{
				ViewState["isEmpty"]=value;
			}
			get
			 {
				return ViewState["isEmpty"].ToString();
			 }
		}
		//IfIdsIsNotNull and IfIdsIsNull have the same function but get different data list
		private void IfIdsIsNull(DataTable tab,string erf,System.Data.SqlClient.SqlConnection con,System.Data.SqlClient.SqlDataAdapter myad)//if Session["strEmailOffer_IDS"] is null use the function to fill the table that passed by the parameter "tab"
		{
			isEmpty="true";
			string strTemp;
			myad.SelectCommand=new System.Data.SqlClient.SqlCommand("spSpot_Floor",con);
			myad.SelectCommand.CommandType=CommandType.StoredProcedure;
			if(Session["strCompId"]==null)
			{
				myad.SelectCommand.Parameters.Add("@UserType", "S");
			}
			else
			{

				myad.SelectCommand.Parameters.Add("@UserType", "P");
			}
			if(Session["strExport"]!=null)
			{
				if(Session["strExport"].ToString().Length!=0)
				{
					myad.SelectCommand.Parameters.Add("@Export", "true");
				}
			}
			myad.SelectCommand.Parameters.Add("@Sort", "VARCONTRACT DESC");
	        myad.SelectCommand.Parameters.Add("@Limit_Contract",erf);
			System.Data.DataSet ds=new DataSet();
			myad.Fill(ds,"mytable");//through Stored Procedures get the data list
			foreach(DataRow rw in ds.Tables["mytable"].Rows)//bind data to the table "tab"
			{
				DataRow drNewRow = tab.NewRow();
				if(Session["strExport"]!=null)//if select International market the size should be the one rounded
				{
					drNewRow["size"]=String.Format("{0:0,0}",Convert.ToDouble(rw["VARSIZE"].ToString())).ToString()+"&nbsp;MT&nbsp;";
				}
				else
				{
					drNewRow["size"] = rw["VARSIZE"].ToString();
				}
				drNewRow["product"]=rw["VARCONTRACT"].ToString();
				drNewRow["melt"]=rw["VARMELT"].ToString();
				drNewRow["density"]=Parse(rw["VARDENS"].ToString());
				drNewRow["adds"]=rw["VARADDS"].ToString();
				if(drNewRow["adds"].ToString().Trim().Length>0)
				{
					isEmpty="false";
				}
				if(Session["strExport"]!=null)//if select International market the size should be the one rounded
				{
					 
					//if(Session["strCompId"]!=null)	// bids should use our buy price and offers should use the sell price
					//{
					//	drNewRow["price"]=round(rw["VARPRICE"].ToString());//5$ round and add the "$"
					//}
					//else
					//{
						drNewRow["price"]=round(rw["VARPRICE2"].ToString());//5$ round and add the "$"
					//}
					
				}
				else
				{
					//if(Session["strCompId"]!=null)	// bids should use our buy price and offers should use the sell price
					//{
					//	strTemp = String.Format("${0:0.000}",Convert.ToDouble(rw["VARPRICE"].ToString())).ToString();//do not round and add the "$"
					
					//}
					//else
					//{
						strTemp = String.Format("${0:0.000}",Convert.ToDouble(rw["VARPRICE2"].ToString())).ToString();//do not round and add the "$"
					
					//}
					
					if (strTemp.Substring(strTemp.Length-1,1) =="0") // changes the lenght oi the total from 2-3 digits based upon precision
					{
						drNewRow["price"] = strTemp.Substring(0,(strTemp.Length-1));
					}
					else
					{
						drNewRow["price"] = strTemp;
					}
				}
				drNewRow["fob"]=rw["VARTERM"].ToString();
				drNewRow["varid"]=rw["VARID"].ToString();
				tab.Rows.Add(drNewRow);
			}
		}
		private void IfIdsIsNotNull(DataTable tab,System.Data.SqlClient.SqlConnection con, System.Data.SqlClient.SqlDataAdapter myad)//if Session["strEmailOffer_IDS"] is not null use the function to fill the table that passed by the parameter "tab"
		{
			rex=new System.Text.RegularExpressions.Regex(",");
			string ids=Session["strEmailOffer_IDS"].ToString().Substring(0,Session["strEmailOffer_IDS"].ToString().Length-1);
			isEmpty="true";
			string strTemp;
			string[]strEmailOffer_IDS=rex.Split(ids);
			for(int i=0;i<strEmailOffer_IDS.Length;i++)
			{

				myad.SelectCommand=new System.Data.SqlClient.SqlCommand("spSpot_Floor",con);
				myad.SelectCommand.CommandType=CommandType.StoredProcedure;
				if(Session[("strCompId")]==null)
				{
					myad.SelectCommand.Parameters.Add("@UserType", "S");
				}
				else
				{
					myad.SelectCommand.Parameters.Add("@UserType", "P");
				}
				if(Session["strExport"]!=null)
				{
					myad.SelectCommand.Parameters.Add("@Export", "true");
				}
				myad.SelectCommand.Parameters.Add("@Sort", "VARCONTRACT DESC");
				myad.SelectCommand.Parameters.Add("@Select_Single_Offer",strEmailOffer_IDS[i]);
				System.Data.DataSet ds=new DataSet();
				myad.Fill(ds,"mytable");//through Stored Procedures get the data list
				foreach(DataRow rw in ds.Tables["mytable"].Rows)//bind data to the table "tab"
				{
					DataRow drNewRow = tab.NewRow();
					if(Session["strExport"]!=null)//if select International market the size should be the one rounded
					{
						drNewRow["size"]=String.Format("{0:0,0}",Convert.ToDouble(rw["VARSIZE"].ToString())).ToString()+"&nbsp;MT&nbsp;";
					}
					else
					{
						drNewRow["size"] = rw["VARSIZE"].ToString();
					}
					drNewRow["product"]=rw["VARCONTRACT"].ToString();
					drNewRow["melt"]=rw["VARMELT"].ToString();
					drNewRow["density"]=Parse(rw["VARDENS"].ToString());
					drNewRow["adds"]=rw["VARADDS"].ToString();
					if(drNewRow["adds"].ToString().Trim().Length>0)
					{
						isEmpty="false";
					}
					if(Session["strExport"]!=null)//if select International market the size should be the one rounded
					{
						//if(Session["strCompId"]!=null)	// bids should use our buy price and offers should use the sell price
						//{
						//	drNewRow["price"]=round(rw["VARPRICE"].ToString());//5$ round and add the "$"
						//}
						//else
						//{
							drNewRow["price"]=round(rw["VARPRICE2"].ToString());//5$ round and add the "$"
						//}
						
						
					}
					else
					{
						//if(Session["strCompId"]!=null)	// bids should use our buy price and offers should use the sell price
						//{
						//	strTemp =String.Format("${0:0.000}",Convert.ToDouble(rw["VARPRICE"].ToString())).ToString();//do not round and add the "$"
						//}
						//else
						//{
							strTemp =String.Format("${0:0.000}",Convert.ToDouble(rw["VARPRICE2"].ToString())).ToString();//do not round and add the "$"
						//}
						
						if (strTemp.Substring((strTemp.Length-1),1) =="0") // changes the lenght oi the total from 2-3 digits based upon precision
						{
							drNewRow["price"] = strTemp.Substring(0,(strTemp.Length-1));
						}
						else
						{
							drNewRow["price"] = strTemp;
						}
						}
					drNewRow["fob"]=rw["VARTERM"].ToString();
					drNewRow["varid"]=rw["VARID"].ToString();
					tab.Rows.Add(drNewRow);
				}
			}
		}
		private int pingfang(int a,int b)//calculate a^b
		{
			int s=1;
			for(int i=0;i<b;i++)
			{
				s=s*a;
			}
			return s;
		}
		private DataTable CreatSubDataGridDate(string pref)//creat the data that the datagrid "mygrid" used
		{
			System.Data.SqlClient.SqlConnection MyC=new System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString());
			MyC.Open();
			//creat a empty table that type matching mygrid
			DataTable table=new DataTable();
			table.Columns.Add(new DataColumn("size",typeof(string)));
			table.Columns.Add(new DataColumn("product",typeof(string)));
			table.Columns.Add(new DataColumn("melt",typeof(string)));
			table.Columns.Add(new DataColumn("density",typeof(string)));
			table.Columns.Add(new DataColumn("adds",typeof(string)));
			table.Columns.Add(new DataColumn("price",typeof(string)));
			table.Columns.Add(new DataColumn("fob",typeof(string)));
			table.Columns.Add(new DataColumn("varid",typeof(string)));
			ad=new System.Data.SqlClient.SqlDataAdapter();
			if(Session["strEmailOffer_IDS"]!=null)
			{
				if(Session["strEmailOffer_IDS"].ToString().Length!=0)
				{
					table.Clear();
					IfIdsIsNotNull(table,MyC,ad);
				}
				else//In the event that Session["strEmailOffer_IDS"].ToString().Length is 0.  The grids	should populate based upon the users preferences.
				{
					table.Clear();
					int rf=Convert.ToInt32(pref);// through "a" and "b" retrun data the function  "IfIdsIsNull" used
					if(Convert.ToBoolean(rf&1))
					{
						IfIdsIsNull(table,"HMWPE - Film Grade",MyC,ad);
					}
					if(Convert.ToBoolean(rf&pingfang(2,1))||Convert.ToBoolean(rf&pingfang(2,3))||Convert.ToBoolean(rf&pingfang(2,4)))
					{
						IfIdsIsNull(table,"HDPE - inj",MyC,ad);
					}
					if(Convert.ToBoolean(rf&pingfang(2,2)))
					{
						IfIdsIsNull(table,"HDPE - Blow Mold",MyC,ad);
					}
					if(Convert.ToBoolean(rf&pingfang(2,5))||Convert.ToBoolean(rf&pingfang(2,5))||Convert.ToBoolean(rf&pingfang(2,7)))
					{
						IfIdsIsNull(table,"LDPE - Film",MyC,ad);
					}
					if(Convert.ToBoolean(rf&pingfang(2,9)))
					{
						IfIdsIsNull(table,"LDPE - inj",MyC,ad);
					}
					if(Convert.ToBoolean(rf&pingfang(2,10))||Convert.ToBoolean(rf&pingfang(2,13)))
					{
						IfIdsIsNull(table,"LLPDE - film",MyC,ad);
					}
					if(Convert.ToBoolean(rf&pingfang(2,16)))
					{
						IfIdsIsNull(table,"LLDPE - inj",MyC,ad);
					}
					if(Convert.ToBoolean(rf&pingfang(2,18)))
					{
						IfIdsIsNull(table,"GPPS",MyC,ad);
					}
					if(Convert.ToBoolean(rf&pingfang(2,19)))
					{
						IfIdsIsNull(table,"HIPS",MyC,ad);
					}
					if(Convert.ToBoolean(rf&pingfang(2,26))||Convert.ToBoolean(rf&pingfang(2,8))||Convert.ToBoolean(rf&pingfang(2,6))||Convert.ToBoolean(rf&pingfang(2,25)))
					{
						IfIdsIsNull(table,"PP Homopolymer - inj",MyC,ad);
					}
					if(Convert.ToBoolean(rf&pingfang(2,20))||Convert.ToBoolean(rf&pingfang(2,21))||Convert.ToBoolean(rf&pingfang(2,11))||Convert.ToBoolean(rf&pingfang(2,23)))
					{
						IfIdsIsNull(table,"PP Copolymer - inj",MyC,ad);
					}
				}
			}
			else//In the event that Session["strEmailOffer_IDS"]is null.  The grids	should populate based upon the users preferences.
			{
				table.Clear();
				int rf=Convert.ToInt32(pref);// through "a" and "b" retrun data the function  "IfIdsIsNull" used
				if(Convert.ToBoolean(rf&1))
				{
					IfIdsIsNull(table,"HMWPE - Film Grade",MyC,ad);
				}
				if(Convert.ToBoolean(rf&pingfang(2,1))||Convert.ToBoolean(rf&pingfang(2,3))||Convert.ToBoolean(rf&pingfang(2,4)))
				{
					IfIdsIsNull(table,"HDPE - inj",MyC,ad);
				}
				if(Convert.ToBoolean(rf&pingfang(2,2)))
				{
					IfIdsIsNull(table,"HDPE - Blow Mold",MyC,ad);
				}
				if(Convert.ToBoolean(rf&pingfang(2,5))||Convert.ToBoolean(rf&pingfang(2,5))||Convert.ToBoolean(rf&pingfang(2,7)))
				{
					IfIdsIsNull(table,"LDPE - Film",MyC,ad);
				}
				if(Convert.ToBoolean(rf&pingfang(2,9)))
				{
					IfIdsIsNull(table,"LDPE - inj",MyC,ad);
				}
				if(Convert.ToBoolean(rf&pingfang(2,10))||Convert.ToBoolean(rf&pingfang(2,13)))
				{
					IfIdsIsNull(table,"LLPDE - film",MyC,ad);
				}
				if(Convert.ToBoolean(rf&pingfang(2,16)))
				{
					IfIdsIsNull(table,"LLDPE - inj",MyC,ad);
				}
				if(Convert.ToBoolean(rf&pingfang(2,18)))
				{
					IfIdsIsNull(table,"GPPS",MyC,ad);
				}
				if(Convert.ToBoolean(rf&pingfang(2,19)))
				{
					IfIdsIsNull(table,"HIPS",MyC,ad);
				}
				if(Convert.ToBoolean(rf&pingfang(2,26))||Convert.ToBoolean(rf&pingfang(2,8))||Convert.ToBoolean(rf&pingfang(2,6))||Convert.ToBoolean(rf&pingfang(2,25)))
				{
					IfIdsIsNull(table,"PP Homopolymer - inj",MyC,ad);
				}
				if(Convert.ToBoolean(rf&pingfang(2,20))||Convert.ToBoolean(rf&pingfang(2,21))||Convert.ToBoolean(rf&pingfang(2,11))||Convert.ToBoolean(rf&pingfang(2,23)))
				{
					IfIdsIsNull(table,"PP Copolymer - inj",MyC,ad);
				}
			}
			MyC.Close();
			return table;
		}
		public void KeepRunningSubGrid(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				System.Data.SqlClient.SqlConnection myconn=new System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString());
				myconn.Open();
				string str=String.Format("{0:f}",DataBinder.Eval(e.Item.DataItem,"price"));
				((System.Web.UI.WebControls.TextBox)e.Item.FindControl("price")).Text=str;//put the price into textbox
				string proId=String.Format("{0}",DataBinder.Eval(e.Item.DataItem, "varid"));//put the bid/offer id that have been selected ,into a hidden label. that id will be used whith one of the columnthe of price list "details" .
				((System.Web.UI.WebControls.Label)e.Item.FindControl("mylab")).Text=proId;
				string strsel="";
                //string strUser="";
				if(Session["strCompId"]==null)
				{
					strsel="select Count(*) from email_sent_to_lead where email_person="+UserId+" and email_offr="+proId;
					this.mycomm=new System.Data.SqlClient.SqlCommand(strsel,myconn);
					if(Convert.ToInt32(mycomm.ExecuteScalar())!=0)//if table "email_sent_to_lead " has saved this record mains that offer/bid had been selected.
					{
						((System.Web.UI.WebControls.CheckBox)e.Item.FindControl("mycheck")).Checked=false;
					}					
					e.Item.Cells[1].Text="<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Spot/Inquire.aspx?Id="+proId+"'>Inquire</a>";
				}
				else
				{
					strsel="select Count(*) from email_sent_to_lead where email_person="+UserId+" and email_bid="+proId;
					this.mycomm=new System.Data.SqlClient.SqlCommand(strsel,myconn);
					if(Convert.ToInt32(mycomm.ExecuteScalar())!=0)
					{
						((System.Web.UI.WebControls.CheckBox)e.Item.FindControl("mycheck")).Checked=false;
					}					
				}
				myconn.Close();
			}
		}
		public string round(string money)
		{
			string strMoney=String.Format("{0:00}",Convert.ToDouble(money));
			int lastNumber=Convert.ToInt32(strMoney.Substring(strMoney.Length-1,1));
			int intMoney=Convert.ToInt32(strMoney);
			double douMoney=Convert.ToDouble(money);
			if(douMoney>1)
			{
				if(lastNumber==5||lastNumber==0)
				{
					return "$"+strMoney;
				}
				else
				{
				
					if(lastNumber>5)
					{
						intMoney=intMoney+10-lastNumber;
					}
					else
					{
						intMoney=intMoney-lastNumber+5;
					}
					return "$"+intMoney.ToString();
				}
			}
			else
			{
				return "$"+money;
			}
			
		}
		void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
			//bind the data to mygrad.
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				 this.UserId=String.Format("{0}",DataBinder.Eval(e.Item.DataItem, "PERS_ID"));				 
                 //this.ComId=String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"PERS_COMP"));
				 string str=String.Format("{0}",DataBinder.Eval(e.Item.DataItem, "PERS_PREF"));
				 DataTable dtTestData=this.CreatSubDataGridDate(str);			    
                 ((System.Web.UI.WebControls.Label)e.Item.FindControl("Label1")).Text=String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"PERS_FRST_NAME"))+","+String.Format("{0}",DataBinder.Eval(e.Item.DataItem, "PERS_LAST_NAME"))+","+String.Format("{0}",DataBinder.Eval(e.Item.DataItem, "PERS_ID"))+","+String.Format("{0}",DataBinder.Eval(e.Item.DataItem, "PERS_MAIL"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("title")).Text=String.Format("{0}",DataBinder.Eval(e.Item.DataItem, "PERS_FRST_NAME"))+" "+String.Format("{0}",DataBinder.Eval(e.Item.DataItem, "PERS_LAST_NAME"));
				System.Web.UI.WebControls.DataGrid mygrad=(System.Web.UI.WebControls.DataGrid)e.Item.FindControl("mygrid");
				((System.Web.UI.WebControls.Label)e.Item.FindControl("Label3")).Text="<HR>";
				if(Session["strCompId"]!=null)//if the item is bids the column "details" could not show.
				{
					mygrad.Columns[1].Visible=false;
				}

				if(isEmpty=="true")
				{
					mygrad.Columns[6].Visible=false;
				}

				mygrad.DataSource=dtTestData;
				mygrad.DataBind();
			}
		}
		#region Web 窗体设计器生成的代码
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: 该调用是 ASP.NET Web 窗体设计器所必需的。
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// 设计器支持所需的方法 - 不要使用代码编辑器修改
		/// 此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			this.mainGrid.ItemDataBound += new 
System.Web.UI.WebControls.DataGridItemEventHandler(this.KeepRunningSum);
			this.sentMail.Click += new System.EventHandler(this.sentMail_Click);
			this.exchange.Click += new System.EventHandler(this.exchange_Click);
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
		private void CreatMail(string UserFirstName,string UserId,System.Web.UI.WebControls.DataGrid dg,string FromMail,string MailTo)//creat the mail body
		{
			isEmpty="true";
			foreach(DataGridItem item in dg.Items)//if the adds in selected item is null then could not show the column adds
			{
				if(((System.Web.UI.WebControls.CheckBox)item.FindControl("mycheck")).Checked)
				{
					string adds=item.Cells[6].Text.Replace("&nbsp;","");
					adds=adds.Trim();
					if(adds.Length>0)
					{
						isEmpty="false";
					}
				}
			}
			System.Web.Mail.MailMessage mail=new System.Web.Mail.MailMessage();
			string MailFood="";
			MailFood="<BR><BR><font size=2>Please click <a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/administrator/Update_Resins.aspx?id="+UserId+"'>update</a> to change your resin preferences and only recieve resins that you wish to monitor or <a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/public/ML_Unsubscribe.aspx'>unsubscribe</a> to this service.</font>";
		
			//the following code will creat the table title in the mail
			System.Text.StringBuilder MailBody=new System.Text.StringBuilder("<br>");
			MailBody.Append("<BR><BR><table border=0 align=center cellspacing=0 cellpadding=0 bgcolor='#E8E8E8'>");
			MailBody.Append("<tr><td bgcolor=#000000 colspan=11 height=1></td></tr>");
			MailBody.Append("<tr><td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>");
			MailBody.Append("<td width=3 bgcolor='#BEB8A2'><img src=/images/1x1.gif width=1></td>");
			if(Session["strCompId"]==null)//if the list is offers show the column "details"
			{
				MailBody.Append("<TD valign=middle align=left bgcolor='#BEB8A2'width=80>&nbsp;<font face='arial,verdana' size=2 color='white'><strong>Inquire</td>");
			}
			if(Session["strExport"]!=null)
			{
				MailBody.Append( "<TD valign=middle align=center bgcolor='#BEB8A2'><font face='arial,verdana' size=2 color='white'><strong>Size</td>");
			}
			else
			{
				MailBody.Append( "<TD valign=middle align=center bgcolor='#BEB8A2'  width='200'><font face='arial,verdana' size=2 color='white'><strong>Size</td>");
			}
			MailBody.Append( "<TD  valign=middle align=center bgcolor='#BEB8A2'width='150'><font face='arial,verdana' size=2 color='white'><strong>Product</td>");
			MailBody.Append("<TD valign=middle align=right bgcolor='#BEB8A2' width=80><font face='arial,verdana' size=2 color='white'><strong>Melt</td>");
			MailBody.Append("<TD valign=middle align=right bgcolor='#BEB8A2'width=150><font face='arial,verdana' size=2 color='white'><strong>&nbsp &nbsp Density</td>");
			if(isEmpty=="false")
			{
				MailBody.Append("<TD valign=middle align=center bgcolor='#BEB8A2' width=120><font face='arial,verdana' size=2 color='white'><strong>&nbsp &nbsp Adds</td>");
			}
			MailBody.Append("<TD valign=middle align=left bgcolor='#BEB8A2'width=80><font face='arial,verdana' size=2 color='white'><strong>&nbsp &nbsp Price</td>");
			MailBody.Append("<TD valign=middle align=center bgcolor='#BEB8A2'width=100><font face='arial,verdana' size=2 color='white'><strong>FOB</td>");
			MailBody.Append("<td valign=middle width=3 bgcolor='#BEB8A2'><img src=/images/1x1.gif width=1></td>");
			MailBody.Append("<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td></tr>");
			bool bAltColor=true;
			bool IsItem=false;
			string myid="";
			foreach(DataGridItem item in dg.Items)
			{
				//read every Item from mygrid.creat table body by the item.
				if(((System.Web.UI.WebControls.CheckBox)item.FindControl("mycheck")).Checked)
				{
					string price=((System.Web.UI.WebControls.TextBox)item.FindControl("price")).Text;
					myid=((System.Web.UI.WebControls.Label)item.FindControl("mylab")).Text;
					if(bAltColor)
					{
						bAltColor=false;
					}
					else
					{
						bAltColor=true;
					}
					MailBody.Append("<TR ");
					if(bAltColor)
					{
						MailBody.Append("bgcolor=#D9D7D3");
					}
					MailBody.Append("><td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>");
					MailBody.Append("<td width=3><img src=/images/1x1.gif width=1></td>");
					if(Session["strCompId"]==null)
					{
						MailBody.Append( "<td>&nbsp<font size=2><a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Spot/Inquire.aspx?Id="+myid.ToString()+"'>Inquire</a></font></td>");
						if(Session["strExport"]!=null)
						{
							MailBody.Append("<TD  valign=middle align=left  width=120><font face='arial,verdana' size=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+item.Cells[2].Text);
						}
						else
						{
							MailBody.Append("<TD  valign=middle align=left  width=120><font face='arial,verdana' size=2>&nbsp;&nbsp;"+item.Cells[2].Text);
						}
						MailBody.Append("</td><td   valign=middle align=left><font face='arial,verdana' size=2>&nbsp;&nbsp;"+item.Cells[3].Text);
						MailBody.Append("</td><td  valign=middle align=right><font face='arial,verdana' size=2>"+item.Cells[4].Text);
						MailBody.Append("</td><td valign=middle align=right><font face='arial,verdana' size=2>"+item.Cells[5].Text);
						if(isEmpty=="false")
						{
							MailBody.Append("</td><td align=left><font face='arial,verdana' size=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+item.Cells[6].Text);
						}
						MailBody.Append( "</td><td valign=middle align=left><font face='arial,verdana' size=2>&nbsp;&nbsp;"+price);
						MailBody.Append( "</td><td valign=middle align=left><font face='arial,verdana' size=2>&nbsp;&nbsp;"+item.Cells[8].Text);
						MailBody.Append( "</td><td valign=middle width=3><img src=/images/1x1.gif width=1></td>");
						MailBody.Append( "<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td></tr>");
						MailBody.Append( "<tr><td bgcolor=#000000 colspan=11 height=1></td></tr>");
					}
					else
					{
						if(Session["strExport"]!=null)//if it is international the column size should be center
						{
							MailBody.Append("<TD valign=middle align=left  width=120><font face='arial,verdana' size=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+item.Cells[2].Text);
						}
						else
						{
							MailBody.Append("<TD valign=middle align=left  width=120><font face='arial,verdana' size=2>&nbsp;&nbsp;"+item.Cells[2].Text);
						}
						MailBody.Append("</td><td valign=middle align=left width=100><font face='arial,verdana' size=2>&nbsp;&nbsp;"+item.Cells[3].Text);
						MailBody.Append("</td><td valign=middle align=right><font face='arial,verdana' size=2>"+item.Cells[4].Text);
						MailBody.Append("</td><td valign=middle align=right><font face='arial,verdana' size=2>"+item.Cells[5].Text);
						if(isEmpty=="false")
						{
							MailBody.Append("</td><td valign=middle align=left><font face='arial,verdana' size=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+item.Cells[6].Text);
						}
						MailBody.Append( "</td><td valign=middle align=left width=80><font face='arial,verdana' size=2>&nbsp;&nbsp;"+price);
						MailBody.Append( "</td><td valign=middle align=left><font face='arial,verdana' size=2>&nbsp;&nbsp;"+item.Cells[8].Text);
						MailBody.Append( "</td><td width=3><img src=/images/1x1.gif width=1></td>");
						MailBody.Append( "<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td></tr>");
						MailBody.Append( "<tr><td bgcolor=#000000 colspan=11 height=1></td></tr>");
					}
					IsItem=true;
				}
			}
			MailBody.Append("</table>");
			mail.From=FromMail;
			mail.Bcc=Application["strEmailOwner"].ToString()+";"+FromMail;
			mail.To=MailTo;
			mail.Subject=this.lblBidOffers.Text;
			this.SecTextBox.Text=this.SecTextBox.Text.Replace("The Plastics Exchange","<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "'>The Plastics Exchange</a>");
			this.SecTextBox.Text=this.SecTextBox.Text.Replace(((char)(13)).ToString(),"<br>");

			this.txtBody.Text=this.txtBody.Text.Replace("The Plastics Exchange","<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "'>The Plastics Exchange</a>");
			this.txtBody.Text=this.txtBody.Text.Replace(((char)(13)).ToString(),"<br>");
			this.txtBody.Text = this.txtBody.Text.Replace("<PERSON FIRST NAME>","&nbsp;"+UserFirstName);
			//string mail
			mail.Body=this.txtBody.Text+MailBody.ToString()+"<BR><BR>"+this.SecTextBox.Text+MailFood;
			// once your done with this one.  swap the <PERSON FIRST NAME> placeholder back.  Otherwise every email after the 1st will be wrong.
			this.txtBody.Text = this.txtBody.Text.Replace("&nbsp;"+UserFirstName,"<PERSON FIRST NAME>");
			mail.BodyFormat=System.Web.Mail.MailFormat.Html;
			try
			{
				int EmailSystemID = 0;
				if(IsItem)
				{
					//System.Web.Mail.SmtpMail.SmtpServer = "localhost";
					TPE.Utility.EmailLibrary.Send(Context,mail);
					string EmailType = "B"; //Bids
					if(Session["strCompId"]==null) EmailType = "O"; //offers
					string DBString = this.Context.Application["DBConn"].ToString();
					EmailSystemID = HelperFunction.SaveEmailSystem(mail.Body, EmailType, DBString);
				}
				string id="";
				this.myConn=new System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString());
				this.myConn.Open();
				string str="";
				foreach(DataGridItem myitem in dg.Items)
				{
					//save erery item into table "email_sent_to_lead".
					id=((System.Web.UI.WebControls.Label)myitem.FindControl("mylab")).Text;
					if(((System.Web.UI.WebControls.CheckBox)myitem.FindControl("mycheck")).Checked)
					{
						if(Session["strCompId"]==null)//offers
						{
							str="select Count(*) from email_sent_to_lead where email_person="+UserId+" and email_offr="+id;
							this.mycomm=new System.Data.SqlClient.SqlCommand(str,this.myConn);
							if(Convert.ToInt32(mycomm.ExecuteScalar())==0)
							{
								str="insert into email_sent_to_lead (email_person,email_offr) values ('"+UserId+"','"+id+"')";
								this.mycomm=new System.Data.SqlClient.SqlCommand(str,myConn);
								this.mycomm.ExecuteNonQuery();
							}
						}
						else//bids
						{
							str="select Count(*) from email_sent_to_lead where email_person="+UserId+" and email_bid="+id;
							this.mycomm=new System.Data.SqlClient.SqlCommand(str,this.myConn);
							if(Convert.ToInt32(mycomm.ExecuteScalar())==0)
							{
								str="insert into email_sent_to_lead (email_person,email_bid) values ('"+UserId+"','"+id+"')";
								this.mycomm=new System.Data.SqlClient.SqlCommand(str,myConn);
								this.mycomm.ExecuteNonQuery();
							}
						}
					}
				}
				string EmailID = "NULL";
				if (EmailSystemID!=0) EmailID = EmailSystemID.ToString();
				str="INSERT INTO COMMENT (CMNT_PERS,CMNT_TEXT,CMNT_DATE, CMNT_EMAIL_SYSTEM_ID) VALUES ('"+UserId+"','Email Subject: "+this.lblBidOffers.Text+"', getDate(), " + EmailID + ")";
				
				this.mycomm=new System.Data.SqlClient.SqlCommand(str,this.myConn);
				mycomm.ExecuteNonQuery();
				this.myConn.Close();
			}
			catch(Exception m)
			{
				this.errorMessage.Text=m.Message.ToString();
			}
		}
		private void sentMail_Click(object sender, System.EventArgs e)
		{
			foreach(DataGridItem myitem in this.mainGrid.Items)
			{
				System.Web.UI.WebControls.DataGrid mydg;
				mydg=(System.Web.UI.WebControls.DataGrid)myitem.FindControl("mygrid");
				this.rex=new System.Text.RegularExpressions.Regex(",");
				string[] 
user=rex.Split(((System.Web.UI.WebControls.Label)myitem.FindControl("Label1")).Text);
				this.CreatMail(user[0],user[2],mydg,ViewState["LoginMail"].ToString(),user[3]);//user[3]
			}
			Session["strCompId"] = null;
            Session["strEmailOffer_Address"] = null;
            Session["strEmailOffer_IDS"]= null;
            Response.Redirect("/administrator/CRM.aspx");
		}

		private void exchange_Click(object sender, System.EventArgs e)
		{
			if(Session["strExport"]==null)
			{
				Session["strExport"]="-1";
			}
			else
			{
				Session["strExport"]=null;
			}
			Response.Redirect("/administrator/Confirm_Email.aspx");
		}
	}
}

