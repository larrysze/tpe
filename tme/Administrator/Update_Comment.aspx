<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ Page language="c#" Codebehind="Update_Comment.aspx.cs" AutoEventWireup="false" Inherits="localhost.Administrator.Update_Comment" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Update Lead Info & Add Comments</title>
		<form id="Update_Comment" runat="server">
	</HEAD>
	<body>
		<table width="100%">
			<tr>
				<!-- primeira tabela -->
				<td>
					<table>
						<tr>
							<TD class="ListHeadlineBold" colSpan="4" align=center>
								<FONT size="3"><b>User Information</b></FONT>
							</TD>
						</tr>
						<tr>
							<td colspan="2">
								<font size="2"><b>User Type: </b></font>
								<asp:Label ID="lblUserType" Runat="server">test</asp:Label>
							</td>
							<td colspan="2">
								<font size="2"><b>Date: </b></font>
								<asp:Label ID="lblDate" Runat="server">03/10/1982 ((getdate))</asp:Label>
							</td>
						</tr>
						<tr>
							<td>
								<font size="2">First Name:</font>
							</td>
							<td>
								<asp:TextBox id="txtFirstName" runat="server"></asp:TextBox>
							</td>
							<td>
								<font size="2">Last Name:</font>
							</td>
							<td>
								<asp:TextBox id="txtLastName" runat="server"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td>
								<font size="2">Title:</font>
							</td>
							<td>
								<asp:TextBox id="txtTitle" runat="server"></asp:TextBox>
							</td>
							<td>
								<font size="2">Company Name:</font>
							</td>
							<td>
								<asp:TextBox id="txtCompanyName" runat="server"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td>
								<font size="2">Phone:</font>
							</td>
							<td>
								<asp:TextBox id="txtPhone" runat="server"></asp:TextBox>
							</td>
							<td>
								<font size="2">Email:</font>
							</td>
							<td>
								<asp:TextBox id="txtEmail" runat="server"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td>
								<font size="2">Username:</font>
							</td>
							<td>
								<asp:label id="lblUsername" runat="server">whatever82</asp:label>
							</td>
							<td>
								<font size="2">Password:</font>
							</td>
							<td>
								<asp:label id="lblPassword" runat="server">xyz1982</asp:label>
							</td>
						</tr>
						<tr>
							<td>
								<font size="2">Email Subscriber: </font>
							</td>
							<td>
								<asp:DropDownList id="ddlSubscriber" runat="server"></asp:DropDownList>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!-- segunda tabela -->
			<tr>
				<td><br>
					<table>
						<tr>
							<td >
								<font size="3"><b>List of Resins</b></font>
							</td>
							<td>
								<a href="/administrator/Update_Contact_Info.aspx">(edit)</a>
							</td>
						</tr>
						<tr>
							<td>
								<asp:Repeater id="rptResins" runat="server">
									<ItemTemplate>
										<%# DataBinder.Eval(Container.DataItem, "Resin") %>
									</ItemTemplate>
								</asp:Repeater>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!-- fechando tudo -->
		</table>
		</FORM>
	</body>
</HTML>
