using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Manage_Groups.
	/// </summary>
	public class Manage_Groups : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox txtNewGroup;
		protected System.Web.UI.WebControls.Button btnCreate;
		protected System.Web.UI.WebControls.DataGrid dg;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.DataGrid dgFolders;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") )
			{
				Response.Redirect("/default.aspx");
			}
				
			if (!IsPostBack)
			{
				Bind();
			}
			
			
		}
		
		private void Bind()
		{
			SqlDataAdapter dadContent;
			DataSet dstContent;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			dadContent = new SqlDataAdapter("SELECT GROUP_NAME,GROUP_ID FROM [GROUP] WHERE GROUP_BROKER='"+Session["ID"].ToString()+"' ORDER BY GROUP_NAME",conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dg.DataSource = dstContent;
			dg.DataBind();
			conn.Close();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion



		private void btnCreate_Click(object sender, System.EventArgs e)
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			SqlCommand cmdInsert = new SqlCommand("Insert into [GROUP] ([GROUP_NAME],[GROUP_BROKER]) VALUES ('"+HelperFunction.RemoveSqlEscapeChars(txtNewGroup.Text)+"','"+Session["ID"].ToString()+"'); ",conn);
			cmdInsert.ExecuteNonQuery();
			conn.Close();
			Bind();
		}

		//     Handler -- Selects the row to be edited	
		protected void DG_Edit(object sender, DataGridCommandEventArgs e)
		{
			// store the orignal amount for later use
			// if text is blank then the value needs to be set to zero
			
			dg.EditItemIndex = e.Item.ItemIndex; 
			Bind();   
		}
		protected void DG_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dg.EditItemIndex =-1;
			Bind(); 
		}
		//     Handler --  deletes the record the row editing
		public void DG_Delete(object sender, DataGridCommandEventArgs e)
		{	
			
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			SqlCommand cmdDelete;
			string SQL="";
			SQL = "Delete FROM GROUP_MEMBER WHERE GROUP_MEMBER_GROUP_ID='"+e.Item.Cells[3].Text+"';";
			SQL += "Delete From [GROUP] WHERE GROUP_ID=" + e.Item.Cells[3].Text;
			//Response.Write(SQL);
			cmdDelete = new SqlCommand(SQL,conn);
			cmdDelete.ExecuteNonQuery();
			conn.Close();
			
			
			Bind();
		}
		
		
	
		//     Handler --  updates the tables based on new information		
		public void DG_Update(object sender, DataGridCommandEventArgs e)
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			SqlCommand cmdUpdate;
			string SQL;
			SQL = "Update [GROUP] SET GROUP_NAME='"+((TextBox) e.Item.Cells[1].Controls[0]).Text+"' WHERE GROUP_ID='" + e.Item.Cells[3].Text+"'";
			//Response.Write(SQL);
			cmdUpdate = new SqlCommand(SQL,conn);
			cmdUpdate.ExecuteNonQuery(); 
			conn.Close();
			dg.EditItemIndex =-1;
			Bind();  
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("/administrator/CRM.aspx");
		}
	}
}
