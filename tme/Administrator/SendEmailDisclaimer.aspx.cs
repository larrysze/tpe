
using System;
using System.Data.SqlClient;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for News_Template.
	/// </summary>
	public partial class SendEmailDisclaimer : System.Web.UI.Page
	{
	
		/******************************************************************************
		 '*   1. File Name       : Public\News_Template.aspx                          *
		 '*   2. Description     : Display news                                       *
		 '*			      Accessed from Public_News(View all news)           *
		 '*   3. Modification Log:                                                    *
		 '*     Ver No.       Date          Author             Modification           *
		 '*   -----------------------------------------------------------------       *
		 '*                                                                           *
		 '*      2.00       2-27-2003       Xiaoda               Comment              *
		 '*****************************************************************************/
		protected void Page_Load(object sender, System.EventArgs e)
		{
			

		}



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
