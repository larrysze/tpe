using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Company_Management.
	/// </summary>
	public partial class Company_Management : System.Web.UI.Page
	{
	
	
		/************************************************************************
		*   1. File Name       :Company_Management.aspx                         *
		*   2. Description     :List of currently companies in the system       *
		*   3. Modification Log:                                                *
		*     Ver No.       Date          Author             Modification       *
		*   -----------------------------------------------------------------   *
		*      1.00      2-22-2004      Zach                First Baseline      *
		*                                                                       *
		************************************************************************/
		public void Page_Load(object sender, EventArgs e)
		{
            Master.Width = "1200px";
            if ((string)Session["Typ"] != "A" )
			{
				Response.Redirect("../default.aspx");
			}
			if (!IsPostBack)
			{
				// sets the default sort
				ViewState["Sort"] = "COMP_NAME ASC";
				Data_Bind();
			}
			if (Request.QueryString["Switch"] != null)
			{
				// enables or disables the company
				Hashtable htParams = new Hashtable();
				htParams.Add("@CompanyID",Request.QueryString["Switch"].ToString());
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"UPDATE COMPANY SET COMP_ENBL=1-COMP_ENBL WHERE COMP_ID=@CompanyID",htParams);
				// COMP_ENBL=1-COMP_ENBL is the key.  makes sure that it works for both directions of the switch
				Response.Redirect("Company_Management.aspx");
			}
			if (Request.QueryString["Decline"] != null)
			{
				// remove the company from the list
				Hashtable htParams2 = new Hashtable();
				htParams2.Add("@CompanyID",Request.QueryString["Decline"].ToString());
				string connectionString = Application["DBconn"].ToString();
				DBLibrary.ExecuteSQLStatement(connectionString,"UPDATE COMPANY SET COMP_REG=0, COMP_ENBL=0 WHERE COMP_NAME=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=@CompanyID)", htParams2);
				
				DBLibrary.ExecuteSQLStatement(connectionString,"UPDATE PERSON SET PERS_ENBL=0 WHERE PERS_COMP IN (SELECT COMP_ID FROM COMPANY WHERE COMP_NAME=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=@CompanyID))", htParams2);
    
				DBLibrary.ExecuteSQLStatement(connectionString,"UPDATE PLACE SET PLAC_ENBL=0 WHERE PLAC_COMP IN (SELECT COMP_ID FROM COMPANY WHERE COMP_NAME=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=@CompanyID))", htParams2);
				
				DBLibrary.ExecuteSQLStatement(connectionString,"DELETE FROM BID WHERE BID_BUYR IN (SELECT PERS_ID FROM PERSON WHERE PERS_COMP IN (SELECT COMP_ID FROM COMPANY WHERE COMP_NAME=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=@CompanyID)))", htParams2);
				
				DBLibrary.ExecuteSQLStatement(connectionString,"DELETE FROM OFFER WHERE OFFR_SELR IN (SELECT PERS_ID FROM PERSON WHERE PERS_COMP IN (SELECT COMP_ID FROM COMPANY WHERE COMP_NAME=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=@CompanyID)))", htParams2);
	
				Response.Redirect("Company_Management.aspx");
    
			}
    
    
		}
    
		private void Data_Bind()
		{
			Hashtable htParams = new Hashtable();
			htParams.Add("@Type",ddlCompanyType.SelectedValue.ToString());
			htParams.Add("@Sort",ViewState["Sort"].ToString());
			DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(),dg,"spCompanySummary",htParams);
		}
    
		// <summary>
		// Sorting function
		// </summary>
		
		 public void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{
    
			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;
    
			//  Parse the sort expression - delimiter space
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
			ColumnToSort = SortExprs[0];
			// If a sort order is specified get it, else default is descending
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DeSC";
			}
    
			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;
    
			// Figure out the column index
			int iIndex;
			iIndex = 0;
			switch(ColumnToSort.ToUpper())
			{
				case "COMP_NAME":
					iIndex = 0;
					break;
				case "COMP_TYPE":
					iIndex = 1;
					break;
				case "WO_TOTAL_LBS":
					iIndex = 3;
					break;
				case "WO_NUM":
					iIndex = 4;
					break;
				case "FO_TOTAL_LBS":
					iIndex = 6;
					break;
				case "FO_NUM":
					iIndex = 7;
					break;
				case "FO_TOTAL_DOLLARS":
					iIndex = 8;
					break;
				case "FO_TOTAL_PROFIT":
					iIndex = 9;
					break;
				case "Balance":
					iIndex = 10;
					break;
				
    
    
			}
			// alter the column's sort expression
			dg.Columns[iIndex].SortExpression = NewSortExpr;
    
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
    
			// store the new SortExpr in a labl for use by page function
			ViewState["Sort"] =NewSortExpr;
			// Sort the data in new order
    
    
			Data_Bind();
			
		}
		
		double dbWO_TOTAL_LBS = 0.0;
		double dbWO_NUM = 0.0;
		double dbFO_TOTAL_LBS = 0.0;
		double dbFO_NUM = 0.0;
		double dbFO_TOTAL_DOLLARS = 0.0;
		double dbBalance= 0.0;
		
		// <summary>
		//  Determines if the row is a 'thumbs up' or a 'thumbs down'
        protected void dg_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				if  ((bool)(DataBinder.Eval(e.Item.DataItem, "COMP_ENBL")))
				{
					e.Item.Cells[12].Text = "<a href=\"Company_Management.aspx?switch="+ (DataBinder.Eval(e.Item.DataItem, "COMP_ID")).ToString() +"\"><img src=/Images/Icons/Enabled.gif border=0></a>";
				}
				else
				{
					e.Item.Cells[12].Text = "<a href=\"Company_Management.aspx?switch="+ (DataBinder.Eval(e.Item.DataItem, "COMP_ID")).ToString() +"\"><img src=/Images/Icons/Disabled.gif border=0></a>";
				}

				if ( (string)(DataBinder.Eval(e.Item.DataItem, "COMMENTSTAT")) =="1")
				{
					//e.Item.Cells[3].Text = "<a href=\"/Administrator/Company_Comment.aspx?ID="+ (DataBinder.Eval(e.Item.DataItem, "COMP_ID")).ToString() +"\"><img src=/pics/Icons/icon_quotation_bubble.gif height='14' width='14' border=0></a>";
                    ImageButton btnComments = (ImageButton)e.Item.Cells[3].Controls[1];
                    btnComments.ImageUrl = "/pics/Icons/icon_quotation_bubble.gif";
                    btnComments.CommandName = "Comments";
                }
				if ((ddlCompanyType.SelectedValue =="Sellers") && (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "BALANCE"))> 0) ) 
				{
					e.Item.Cells[11].Text = "<font color=red>("+e.Item.Cells[11].Text+")</font>";

				}

				// sum up items in the datagrid columns
				
				//if (e.Item.Cells[3].Text!="")
				if ((DataBinder.Eval(e.Item.DataItem, "WO_TOTAL_LBS")) != DBNull.Value)
					dbWO_TOTAL_LBS += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "WO_TOTAL_LBS"));
				if ((DataBinder.Eval(e.Item.DataItem, "WO_NUM")) != DBNull.Value)
					dbWO_NUM += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "WO_NUM"));
				if ((DataBinder.Eval(e.Item.DataItem, "FO_TOTAL_LBS")) != DBNull.Value)
					dbFO_TOTAL_LBS += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "FO_TOTAL_LBS"));
				if ((DataBinder.Eval(e.Item.DataItem, "FO_NUM")) != DBNull.Value)
					dbFO_NUM += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "FO_NUM"));
				if ((DataBinder.Eval(e.Item.DataItem, "FO_TOTAL_DOLLARS")) != DBNull.Value)
					dbFO_TOTAL_DOLLARS += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "FO_TOTAL_DOLLARS"));
				if ((DataBinder.Eval(e.Item.DataItem, "Balance")) != DBNull.Value)
					dbBalance += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "Balance"));
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[1].Text = "<b>Totals:</b> " ;
				e.Item.Cells[4].Text = "<b>"+String.Format("{0:#,###}", dbWO_TOTAL_LBS)+"</b>";
				e.Item.Cells[5].Text = "<b>"+String.Format("{0:#,###}", dbWO_NUM)+"</b>";
				e.Item.Cells[7].Text = "<b>"+String.Format("{0:#,###}", dbFO_TOTAL_LBS)+"</b>";
				e.Item.Cells[8].Text = "<b>"+String.Format("{0:#,###}", dbFO_NUM)+"</b>";
				e.Item.Cells[9].Text = "<b>"+String.Format("{0:#,###}", dbFO_TOTAL_DOLLARS)+"</b>";
				e.Item.Cells[11].Text = "<b>"+String.Format("{0:#,###}", dbBalance)+"</b>";
            }
    
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			//this.dg.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_ItemCommand);
			this.dg.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_ItemDataBound);

		}
		#endregion

		public void dg_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
            string id = e.Item.Cells[0].Text.ToString();
				
			if (e.CommandName=="NoComments")
			{
                //number = number.Substring(number.IndexOf("=",0)+1);
				Response.Redirect("/Administrator/Company_Comment.aspx?ID=" + id + "&Referrer=" + Request.Url.AbsoluteUri.ToString());
			}
            else if (e.CommandName == "Comments")
            {
                //e.Item.Cells[3].Text = "<a href=\"/Administrator/Company_Comment.aspx?ID="+ (DataBinder.Eval(e.Item.DataItem, "COMP_ID")).ToString() +"\"><img src=/pics/Icons/icon_quotation_bubble.gif height='14' width='14' border=0></a>";
                //string id = ((HyperLink)e.Item.Cells[4].Controls[0]).NavigateUrl.ToString();
                //id = id.Substring(id.IndexOf("=", 0) + 1);
                Response.Redirect("/Administrator/Company_Comment.aspx?ID=" + id + "&Referrer=" + Request.Url.AbsoluteUri.ToString());                    
            }
		}

		protected void ddlCompanyType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (ddlCompanyType.SelectedValue !="*")
			{
				ViewState["Sort"] = "FO_TOTAL_DOLLARS DESC";
			}
			else
			{
				ViewState["Sort"] = "COMP_NAME ASC";
			}
			Data_Bind();
		}
	}
}
