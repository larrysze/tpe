<%@ Register TagPrefix="mbdb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.DefaultButtons" %>
<%@ Page Language="c#" CodeBehind="Setup_News.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.Setup_News" MasterPageFile="~/MasterPages/Menu.Master"%>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">
<script>
<!-- //begin
function SelectAll()
{
	for (var i = 0; i < document.Form1.elements.length; i++) 
	{
		var e = document.Form1.elements[i];
		if (e.type == 'checkbox')
		{
			e.checked = true;
		}
    }
}
// end -->
</script>
</asp:Content>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
<br />
<asp:button id=Button4 CssClass="Content Color2" onclick=add runat="server" Text="Add Selected Stories"></asp:button>
<BR>
<input type='button' id=Button1 class="Content Color2" onclick='SelectAll()' value='Select All'>
<BR>
<BR>
<SPAN class=Content>Filtered Stories</SPAN> 
<br>
<asp:label id=lbltest runat="server" CssClass="Content LinkNormal"></asp:label>
<BR><BR>
<SPAN class=Content>Search News Feed</SPAN> 
<asp:textbox CssClass="InputForm" id=txtSearch runat="server"></asp:textbox>
<asp:button id=btnFind CssClass="Content Color2" runat="server" Text="Find"></asp:button><br>
<br>
<asp:label id="lblSearchResults" runat="server"></asp:label>
</asp:Content>
