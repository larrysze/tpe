using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for US_Map.
	/// </summary>
	public partial class US_Map : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
            if ((Session["Typ"].ToString() != "A") && (Session["Typ"].ToString() != "B") && (Session["Typ"].ToString() != "T") && (Session["Typ"].ToString() != "L"))
			{
				Response.Redirect("/default.aspx");
			}
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			SqlConnection conn2;
			conn2 = new SqlConnection(Application["DBconn"].ToString());
			conn2.Open();

			SqlCommand cmdStates;
			SqlDataReader dtrStates;
			SqlCommand cmdData;
			SqlDataReader dtrData;
			cmdStates = new SqlCommand("SELECT STAT_CODE FROM STATE WHERE STAT_CTRY='US' ORDER BY STAT_CODE",conn);
			dtrStates = cmdStates.ExecuteReader();
			StringBuilder sbContent = new StringBuilder();
			sbContent.Append("<table width=360 border=0 cellpadding=2 cellspacing=2>");
			sbContent.Append("<tr><td align=left><b>State</b></td><td align=right><b>Leads</b></td><td align=right><b>Offers</b></td><td align=right><b>Bids</b></td></tr>");
			string strSQL="";
			while (dtrStates.Read())
			{
				strSQL=  "exec spUS_Map @State='"+ dtrStates["STAT_CODE"].ToString()+"' ";
				if (!Session["ID"].ToString().Equals("2"))
				{
					strSQL +=", @USERID='"+Session["ID"].ToString()+"'";
				}
				//Response.Write(strSQL+"; <BR><BR>Go<BR><BR>");
				cmdData = new SqlCommand(strSQL,conn2);
				dtrData = cmdData.ExecuteReader();
				if (dtrData.Read())
				{
					sbContent.Append("<tr>");
					sbContent.Append("<td align=left><a href=\"/Administrator/CRM.aspx?State="+dtrStates["STAT_CODE"].ToString()+"\">"+dtrData["Stat_NAME"].ToString()+"</a></td>");
                    sbContent.Append("<td align=right>" + dtrData["LEAD"].ToString() + "</td>");
                    sbContent.Append("<td align=right>" + dtrData["OFFER_COUNT"].ToString() + "</td>");
                    sbContent.Append("<td align=right>" + dtrData["BID_COUNT"].ToString() + "</td>");
					sbContent.Append("</tr>");
				}
				dtrData.Close();
			}
			sbContent.Append("</table>");
			dtrStates.Close();
			conn.Close();
			conn2.Close();
			lblContent.Text=sbContent.ToString();		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
