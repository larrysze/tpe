using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.Mail;
using TPE.Utility;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Update_Resins.
	/// </summary>
	public partial class Update_Resins : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.CheckBox CheckBox1;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
            pnThankYou.Visible = false;
			//I don't know what type of user can access!!!
			if (!IsPostBack)
			{
				string ID = "";
				if(Request.Params["UserID"] != null)
				{
					ID = Crypto.Decrypt(Request.Params["UserID"]);
				}
				else if(Request.Params["ID"] != null)
				{
                    if (!((Session["Typ"].ToString() == "A") || (Session["Typ"].ToString() == "B") || (Session["Typ"].ToString() == "T") || (Session["Typ"].ToString() == "L")))
					{
						Response.Redirect("/default.aspx");
					}
					ID = Request.Params["ID"];
				}
				else
				{
					Response.Redirect("/default.aspx");
				}

				ViewState["ID"] = ID;
				
				if(Bind())
				{
					Build_Resin_Table();
				}
				else
				{
					Response.Redirect("/default.aspx");
				}
			}					
			else
			{
				Build_Resin_Table();
			}
		}

		private bool Bind()
		{
			bool result = false;
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
			
				using (SqlDataReader dtrPerson = DBLibrary.GetDataReaderFromSelect(conn,"SELECT *,CITY=(select DISTANCE_CITY +', '+DISTANCE_STATE from DISTANCE_ZIPCODE WHERE DISTANCE_ZIPCODE =P.PERS_ZIP ),(SELECT PERS_MAIL FROM PERSON B WHERE B.PERS_ID = P.PERS_ACES ) AS BROKER_MAIL FROM PERSON P WHERE PERS_ID = '"+ViewState["ID"]+"'"))
				{
					if (dtrPerson.Read())
					{
						result = true;
						if (dtrPerson["PERS_ACES"].ToString() !="-1") // overwrite the BrokerID with a allocated broker
						{
							ViewState["BrokerID"] = dtrPerson["PERS_ACES"].ToString() ;
						}
				
				

						lblName.Text = HelperFunction.getSafeStringFromDB(dtrPerson["PERS_FRST_NAME"]) + " " + HelperFunction.getSafeStringFromDB(dtrPerson["PERS_LAST_NAME"]);
						ViewState["Pref"] = dtrPerson["PERS_PREF"].ToString();
						ViewState["USER_NAME"] = HelperFunction.getSafeStringFromDB(dtrPerson["PERS_FRST_NAME"]) + " " + HelperFunction.getSafeStringFromDB(dtrPerson["PERS_LAST_NAME"]);
						ViewState["BROKER_MAIL"] = HelperFunction.getSafeStringFromDB(dtrPerson["BROKER_MAIL"]);
						/*
						// add the broker lable 
						txtZip.Text = getSafeString(dtrPerson["PERS_ZIP"]);
						lblBroker.Text = getSafeString(dtrPerson["BROKER"]);
						txtFirstName.Text = getSafeString(dtrPerson["PERS_FRST_NAME"]);
						txtLastName.Text = getSafeString(dtrPerson["PERS_LAST_NAME"]);
						txtTitle.Text = getSafeString(dtrPerson["PERS_TITL"]);
						lblCity.Text = getSafeString(dtrPerson["CITY"]);
						*/
						string emailPref = HelperFunction.getSafeStringFromDB(dtrPerson["EMAIL_ENBL"]);
						cbEmailSubscriber.SelectedValue = emailPref;
				
				
			
						string strSize = HelperFunction.getSafeStringFromDB(dtrPerson["PERS_INTRST_SIZE"]).Trim();
						string strQuality = HelperFunction.getSafeStringFromDB(dtrPerson["PERS_INTRST_QUALT"]).Trim();
				
						if (strSize.Length==3)
						{
							chkSize1.Checked = strSize[0].Equals('1')? true:false;
							chkSize2.Checked = strSize[1].Equals('1')? true:false;
							chkSize3.Checked = strSize[2].Equals('1')? true:false;
						}
						else
						{
							chkSize1.Checked = false;
							chkSize2.Checked = false;
							chkSize3.Checked = false;
						}

						if (strQuality.Length==3)
						{
							chkQuality1.Checked = strQuality[0].Equals('1')? true:false;
							chkQuality2.Checked = strQuality[1].Equals('1')? true:false;
							chkQuality3.Checked = strQuality[2].Equals('1')? true:false;
						}
						else
						{
							chkQuality1.Checked = false;
							chkQuality2.Checked = false;
							chkQuality3.Checked = false;
						}

					}
				}
			}
			return result;
		}

		private void Build_Resin_Table()
		{

			tblDynamic.Rows.Clear();
			ArrayList ALResin = new ArrayList();
						
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();

				// defins the cmdResin before dtrPerson 
				string strSql ="SELECT CONT_LABL,CONT_ID, CONT_GRADE FROM CONTRACT WHERE POWER(2,CONT_ID-1)&("+ViewState["Pref"].ToString()+")<>0 ORDER BY CONT_ORDR";
			
				// binding resin list repeater control
				using (SqlDataReader dtrResin = DBLibrary.GetDataReaderFromSelect(conn,strSql))
				{			
					while(dtrResin.Read())
					{
						ALResin.Add(dtrResin["CONT_ID"].ToString());

					}

				}

				ALResin.TrimToSize();
				
				TableRow tr;

				TableCell td1;
				TableCell td2;

				// select all contract
				strSql = "SELECT CONT_LABL,CONT_ID, CONT_GRADE FROM CONTRACT  ORDER BY CONT_ORDR";
				using (SqlDataReader dtrResin2 = DBLibrary.GetDataReaderFromSelect(conn,strSql))
				{
					while(dtrResin2.Read())
					{
						tr = new TableRow();
						td1 = new TableCell();
						td1.Wrap = false;
						CheckBox cb = new CheckBox();
						cb.ID = "cbResin_" + dtrResin2["CONT_ID"].ToString().Trim();
						if (ALResin.IndexOf(dtrResin2["CONT_ID"].ToString().Trim()) >= 0)
						{
							cb.Checked =true;
						}
						//cb.
						td1.Controls.Add(cb);
						td2 = new TableCell();
						td2.Text= dtrResin2["CONT_LABL"].ToString().Trim();
						td2.Wrap = false;
					
						tr.Controls.Add(td1);
						tr.Controls.Add(td2);
						tblDynamic.Rows.Add(tr);
					}
				}				
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnSave_ServerClick(object sender, System.EventArgs e)
		{
			// insert updated code 
			string strSQL;
			string strAllowEmail;
			
			strAllowEmail = cbEmailSubscriber.SelectedValue;
			
			string strSQL2 = "";
			strSQL2 += ", PERS_INTRST_SIZE = " + "'" + Bit(chkSize1.Checked)+ Bit(chkSize2.Checked)+ Bit(chkSize3.Checked) + "'";
			strSQL2 += ", PERS_INTRST_QUALT = " + "'" + Bit(chkQuality1.Checked)+ Bit(chkQuality2.Checked)+ Bit(chkQuality3.Checked) + "'";


			int i = tblDynamic.Rows.Count;
			i++;
			i--;

			IterateControls(this); // iterate through cb controls to determine the resin preferences.  stored as iPref
			// add Person Status
			strSQL = "UPDATE PERSON set PERS_PREF='"+iPref.ToString()+ "',EMAIL_ENBL='" + strAllowEmail + "'" + strSQL2 + " WHERE PERS_ID ='" + ViewState["ID"] + "'";
			
			String old_prefs = HelperFunction.getUserPreferencesHTML(ViewState["ID"].ToString(), this.Context);

			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),strSQL);
			
			String new_prefs = HelperFunction.getUserPreferencesHTML(ViewState["ID"].ToString(), this.Context);

			MailMessage mail;
			mail = new MailMessage();
			if((ViewState["BROKER_MAIL"] != null) && (ViewState["BROKER_MAIL"].ToString() != ""))
			{
				mail.To = ViewState["BROKER_MAIL"].ToString();
			}
			else
			{
				mail.To = "michael@theplasticsexchange.com";
			}

			if( (ViewState["BrokerID"] != null) && (ViewState["BrokerID"].ToString() != "2") && (mail.To != "michael@theplasticsexchange.com"))
			{
				mail.Cc = "michael@theplasticsexchange.com";
			}

			mail.From = "system@theplasticsexchange.com";
			mail.Subject = "Resin Preferences Update: " + ViewState["USER_NAME"].ToString();
			String textEmail = HelperFunction.getEmailHeaderHTML();
			textEmail += "<BR>";
			textEmail += HelperFunction.getGeneralUserInformationHTML(ViewState["ID"].ToString(), this.Context) + "<BR>";

			textEmail += "<hr width=100%>";
			textEmail += "<H3>Previously interested in:</H3>";
			textEmail += old_prefs + "<BR><BR>";
			textEmail += "<hr width=100%>";
			textEmail += "<H3>Currently interested in:</H3>";
			textEmail += new_prefs + "<BR>";
			textEmail += HelperFunction.getEmailFooterHTML();
			mail.Body = textEmail;
			
			mail.BodyFormat = MailFormat.Html;
			string s = "";
			TPE.Utility.EmailLibrary.Send(Context,mail);
			
			//Response.Redirect("/Administrator/Update_Pref_Thank_You.aspx");
            pnMain.Visible = false;
            pnThankYou.Visible = true;
		}

		/// <summary>
		/// interate through all controls and create sql statement to pass the updates to the db.
		/// Function is called recursivly so the string is stored within another variable
		/// </summary>
		/// <param name="parent"></param>
		///		

		/*
		int iPref=0;
		void InterateControls(Control parent)
		{
		
			foreach (Control child in parent.Controls)
				
			{
				if (child.GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox") && child.ID.IndexOf("cbResin_") == 0)
				{
					CheckBox checkbox  = (CheckBox)child;
					if (checkbox.Checked)

					{
						string arrCheckBoxID;
						string name = child.ID.ToString();
						arrCheckBoxID = name.Substring(name.IndexOf("_") + 1);
						iPref += (int)Math.Pow(2.0,(Convert.ToInt32(arrCheckBoxID)-1)); // checked box found! add to total
						
						
						//				arrCheckBoxID[1].ToString()+"<BR>");
					

					}
				}
        
				if (child.Controls.Count > 0)
				{          
					InterateControls(child);          
				}
			}
		}
*/

		int iPref=0;
		void IterateControls(Control parent)
		{
			foreach (Control child in parent.Controls)
			{
				if (child.GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox") && child.ID.IndexOf("cbResin_") == 0)
				{
					CheckBox checkbox  = (CheckBox)child;
					if (checkbox.Checked)

					{
						string[] arrCheckBoxID;
						Regex r = new Regex("_"); // Split apart id to reveal the needed values.
						arrCheckBoxID = r.Split(child.ID.ToString()) ;
						iPref += (int)Math.Pow(2.0,(Convert.ToInt32(arrCheckBoxID[1])-1)); // checked box found! add to total
						
						
						//				arrCheckBoxID[1].ToString()+"<BR>");
					

					}
					
				}
        
				if (child.Controls.Count > 0)
				{          
					IterateControls(child);          
				}
			}
		}

		private string Bit(bool condition)
		{
			if (condition)
				return "1";
			else
				return "0";
		}


	}


}
