<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Page language="c#" Codebehind="SendEmail.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.SendEmail" Title="Send Email" MasterPageFile="~/MasterPages/Menu.Master"%>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">	
			<TABLE id="Table1" height="200" cellSpacing="0" cellPadding="0" width="100%" align="center"	border="0" class="Content Color2">
				<TR>
					<TD align="center" colSpan="3"><asp:label id="lblErrorMessage" runat="server"></asp:label></TD>
				</TR>
				<TR>
				    <TD>&nbsp;</td>
				</TR>
				<TR>
					<TD class="Content" align="center" colSpan="3"><asp:label id="lblTitlePage" runat="server" CssClass="Content">Send Email</asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 39px" vAlign="top" align="left"><asp:label  id="lblSubject"  runat="server" CssClass="Content" Font-Bold="True">Subject:</asp:label></TD>
					<TD align="left" colSpan="2"><asp:textbox id="txtSubject" runat="server" CssClass="InputForm" Width="224px"></asp:textbox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 39px; HEIGHT: 99px" vAlign="top" align="right"><asp:label id="lblBody" runat="server" Font-Bold="True">Body:</asp:label></TD>
					<TD style="HEIGHT: 99px" align="left" colSpan="2" height="99"><asp:textbox id="txtBody" runat="server" CssClass="InputForm" Width="700px" Height="112px" TextMode="MultiLine"></asp:textbox></TD>
				</TR>
				<TR>
					<TD vAlign="top" align="left" colSpan="3"><BR>
						<B>
							<asp:label id="lblItens" runat="server" Font-Bold="True">Select items to send:</asp:label></B></TD>
				</TR>
				<TR>
					<TD vAlign="top" align="left" colSpan="3">
						<P><asp:datagrid id="dgUsers" runat="server" Width="820px" BorderStyle="None" AutoGenerateColumns="False">
								<Columns>
									<asp:TemplateColumn>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:Label id="lblTitle" runat="server" Font-Bold="True"></asp:Label>
											<asp:Label id="lblPersID" runat="server" Visible="False"></asp:Label>&nbsp;
											<asp:Label id="lblMessage" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label><BR>
											<asp:datagrid id="dgEmail" runat="server" CssClass="DataGrid" Width="100%" AutoGenerateColumns="False" CellPadding="2" HorizontalAlign="Center" ItemStyle-CssClass="DataGridRow" AlternatingItemStyle-CssClass="DataGridRow_Alternate" HeaderStyle-CssClass="DataGridHeader" OnItemDataBound="dgEmail_ItemDataBound">
												<AlternatingItemStyle CssClass="DataGridRow_Alternate"></AlternatingItemStyle>
												<ItemStyle CssClass="DataGridRow"></ItemStyle>
												<HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
												<Columns>
													<asp:BoundColumn Visible="False" DataField="selected" HeaderText="selected">
														<ItemStyle HorizontalAlign="Right"></ItemStyle>
													</asp:BoundColumn>
													<mbrsc:RowSelectorColumn AllowSelectAll="True"></mbrsc:RowSelectorColumn>
													<asp:BoundColumn DataField="Product" HeaderText="Product">
														<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Size" HeaderText="Size">
														<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Melt" HeaderText="Melt">
														<ItemStyle HorizontalAlign="Right"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Density" HeaderText="Density">
														<ItemStyle HorizontalAlign="Right"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Adds" HeaderText="Adds">
														<ItemStyle HorizontalAlign="Right"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="Price" HeaderText="Price">
														<ItemStyle Font-Bold="True" HorizontalAlign="Left"></ItemStyle>
													</asp:BoundColumn>
													<asp:TemplateColumn HeaderText="Price">
														<ItemStyle HorizontalAlign="Left"></ItemStyle>
														<ItemTemplate>
															<asp:TextBox id="txtPrice" runat="server" CssClass="InputForm" Width="60px"></asp:TextBox>
															<asp:Label id="lblPrice" runat="server" Visible="False"></asp:Label>
															<asp:Label id="lblSelected" runat="server" Visible="False"></asp:Label>
															<asp:Label id="lblTransactionID" runat="server" Visible="False"></asp:Label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn DataField="FOB" HeaderText="FOB">
														<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
													</asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="pers_id" HeaderText="pers_id"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="TransactionID" HeaderText="TransactionID"></asp:BoundColumn>
												</Columns>
											</asp:datagrid><BR>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid></P>
						<BR>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 39px" vAlign="top" align="right">&nbsp;
						<asp:label id="lblSalutation" runat="server" Font-Bold="True">Salutation:</asp:label></TD>
					<TD vAlign="top" align="left" colSpan="3"><asp:textbox id="txtSalutation" runat="server" CssClass="InputForm" Width="700px" Height="125px" TextMode="MultiLine"></asp:textbox></TD></TR>
				<TR>
					<TD vAlign="top" align="center" colSpan="3"><BR>
						<asp:button id="btnSendMail" runat="server" CssClass="Content Bold2" CausesValidation="False" Text="Send Mail" onclick="btnSendMail_Click"></asp:button><br><br></TD>
				</TR>
			</TABLE>
</asp:Content>