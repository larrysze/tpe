using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Data.SqlClient;
using TPE.Utility;
using System.Threading;
using System.Configuration;
using System.IO;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for SendPDFToSubscribers.
	/// </summary>
	public partial class SendPDFToSubscribers : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["Typ"].ToString() != "A" )
			{
				Response.Redirect("/default.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void Submit1_ServerClick(object sender, System.EventArgs e)
		{
			if( ( File1.PostedFile != null ) && ( File1.PostedFile.ContentLength > 0 ) )
			{
				string dir = "/Research/Market_Update/pdf";
				string fn = DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + ".pdf";
				string SaveLocation = Server.MapPath(dir) + "\\" +  fn;
				try
				{
					File1.PostedFile.SaveAs(SaveLocation);
					pnlSendEmail.Visible = true;
					pnlSubmitPDF.Visible = false;
					lnkPDF.Text = "<a target=\"blank\" href=\"" + dir + "/" + fn + "\">attached PDF</a>";
				}
				catch ( Exception ex )
				{
//					Response.Write("Error: " + ex.Message);
					//Note: Exception.Message returns a detailed message that describes the current exception. 
					//For security reasons, we do not recommend that you return Exception.Message to end users in 
					//production environments. It would be better to put a generic error message. 
				}
			}
			else
			{
				Response.Write("Please select a file to upload.");
			}
		}

		protected void btnSure_Click(object sender, System.EventArgs e)
		{
			MailMessage mail;
			mail = new MailMessage();

			mail.From = "research@theplasticexchange.com";
			
			mail.Subject = txtSubject.Text;
			
			string txtBody = "";

			txtBody += HelperFunction.getEmailHeaderHTML() + "<br /><br />";
			txtBody += txtMessage.Text.Replace("\r\n", "<br />");

			
			mail.Body = txtBody;

			mail.BodyFormat = MailFormat.Html;
			
			string fn = DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + ".pdf";
			
			MailAttachment mAttch = new MailAttachment(Server.MapPath("/Research/Market_Update/pdf/" + fn))  ;
			mail.Attachments.Add(mAttch);

			if(Session["all"] != null)
			{
                Session["unsubscribe_url"] = "http://" + ConfigurationSettings.AppSettings["DomainName"] + "/Public/UnsubscribeFromMU.aspx";
                Session["msg"] = mail;
                Thread send_mails_thread = new Thread(SendThreadMails);

                send_mails_thread.Start();
			}
			else
			{
                mail.Body += "<br /><br />" + HelperFunction.getEmailFooterHTML();
                mail.To = Application["strEmailAdmin"].ToString();
				SendOneMail(mail);
			}

			pnlConfirmation.Visible = false;
			pnlOK.Visible = true;
			
		}

		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			Session["all"] = null;
			pnlConfirmation.Visible = false;
			pnlSendEmail.Visible = true;
		
		}

		protected void btnSend_Click(object sender, System.EventArgs e)
		{
			pnlSendEmail.Visible = false;
			pnlConfirmation.Visible = true;
			if (ddlSendTo.SelectedValue == "All subscribers")
			{
				Session["all"] = "yes";
				lblConfirm.Text = "You are attempting to Send this message for ALL SUBSCRIBERS to Monthly Market Update.";
			}
			else
			{
				lblConfirm.Text = "You are attempting to Send this message only to " + Application["strEmailAdmin"].ToString();
			}
			string strMsg = "";
			strMsg += "Subject: " + txtSubject.Text + "<br /><br />";
			strMsg += txtMessage.Text.Replace("\r\n", "<br />");

			lblMessage.Text = strMsg + "<br />";
			
		}

		private void SendOneMail(MailMessage msg)
		{
			TPE.Utility.EmailLibrary.SendWithinThread(msg);
		}

		private void SendThreadMails()
		{
            MailMessage msg = (MailMessage)Session["msg"];
            
            string originalBody = msg.Body;

            string unsubscribe_url = Session["unsubscribe_url"].ToString();
			int count = 0;
			using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				conn.Open();

				string strSql = string.Empty;
                string strMUStoredProcedure = "";
				if (bool.Parse(Application["IsDebug"].ToString()))
				{
                    strMUStoredProcedure = "spMarketUpdateMailingTest";
//                    strSql = "Select mai_mail from mailingTEST order by mai_mail ASC";
				}
				else
				{
                    strMUStoredProcedure = "spMarketUpdateMailing";
//					strSql = "Select mai_mail from mailing order by mai_mail ASC";
				}

                string file_path = Server.MapPath("/logfiles/marketupdate_log_file.txt");

                // Create a file to write to.
                StreamWriter sw = File.CreateText(file_path);
                

                int i = 0;
				// generate mailing list 
				using (SqlDataReader dtr = DBLibrary.GetDataReaderStoredProcedure(conn, strMUStoredProcedure, null))
				{
					while (dtr.Read())
					{
                        string currentBody = originalBody;
						try
						{

                            i++;
                            msg.To = dtr["mai_mail"].ToString();
                            currentBody += "<br /><br />";
                            string link = "<font size=\"-1\">If you want to unsubscribe from Market Update mailing list, please <a href=\"" + unsubscribe_url + "?ID=" + Crypto.Encrypt(msg.To) + "\">click here</a></font>";
                            currentBody += link;
                            currentBody += "<br /><br />" + HelperFunction.getEmailFooterHTML();

                            msg.Body = currentBody;
							SendOneMail(msg);

                            sw.WriteLine(i + " " + DateTime.Now.ToShortTimeString() + ":" + msg.To);


                            if (i % 50 == 0)
                            {
                                sw.Flush();
                            }
                        

                        }
						catch(Exception Error)
						{
                            sw.WriteLine(i + " " + DateTime.Now.ToShortTimeString() + ":" + Error.Message);
                            count--;
                            sw.Flush();

						}
						count++;
					}
				}
                sw.Flush();
			}
			/*
						
			MailMessage mail;
			mail = new MailMessage();
			mail.To = "wanderley@theplasticsexchange.com";
			mail.From = "research@theplasticexchange.com";
			mail.Subject = "Market Update Stats";
			mail.Body = "This is an automated message.  The Market Update Script has completed successfully.  " + count + " emails have been sent out." ;
			mail.BodyFormat = MailFormat.Html;
			System.Web.Mail.SmtpMail.SmtpServer="caesar";  
			TPE.Utility.EmailLibrary.Send(Context,mail);
*/
			count--; //correct number of emails
			MailMessage mail;
			mail = new MailMessage();
			mail.To = Application["strEmailAdmin"].ToString();
			//			mail.To = "amre@theplasticsexchange.com";
			mail.From = "research@theplasticexchange.com";
			mail.Subject = "Market Update Stats";
			mail.Body = "This is an automated message.  The Market Update Script has completed successfully.  " + count + " emails have been sent out.";
			mail.BodyFormat = MailFormat.Html;
			try
			{
				TPE.Utility.EmailLibrary.SendWithinThread(mail);
			}
			catch(Exception sendingError )
			{
			}
		}

		protected void btnGo_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("/default.aspx");
		}
	}
}
