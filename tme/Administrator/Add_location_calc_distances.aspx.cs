using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using TPE.Utility;

namespace localhost.Administrator
{
	public class Add_location_calc_distances : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblEstimationTime;
		protected System.Web.UI.WebControls.Label lblCalculating;
		public string idLocality = "";
		public string idQuery = "";
		public string pathServer = ""; // used for thread to store wwwroot path

		private void Page_Load(object sender, System.EventArgs e)
		{
			idLocality = insertLocality();
			idQuery = Request.QueryString["id"];
			Hashtable param = new Hashtable();
						
			// get the country of the new locality
			string countryTempPlace = "";
			string strSQL2="SELECT TMP_PLAC_CTRY FROM TMP_PLACE WHERE TMP_PLAC_ID=@Id";
			param.Add("@Id", Int32.Parse(idQuery));
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());

			try 
			{
				conn.Open();
				SqlDataReader dtrSelect= DBLibrary.GetDataReaderFromSelect(conn,strSQL2,param);
				while(dtrSelect.Read())
				{
					countryTempPlace = dtrSelect["TMP_PLAC_CTRY"].ToString();
				}
			}
			finally
			{
				conn.Close();
			}
			if(countryTempPlace == "US")
			{// calculate distances if the country is US because geoplace does not support other country
				calcEstimateTime();
				
				//set current path for the thread, because server.MapPath in a thread is windows root directory.
				pathServer = @Server.MapPath("./").ToString() + @"/sqlDistances.sql"; 

				Thread threadDistances = new Thread(new ThreadStart(calcAllDistances));
				threadDistances.Start();
			}
			else
			{// insert place for other country
				insertPlace();
				lblEstimationTime.Text = "The new location has been added to the database. Distance cannot be calculated. There will be no estimation freight price.";
				lblCalculating.Text = "You can continue to navigate through the website.";
			}
		}
		private void insertPlace()
		{// inserting the new place to the company

			Hashtable param = new Hashtable();
			// get information from tmp_place which has been set on add_location.aspx.cs page.
			string strSQL2="SELECT * FROM TMP_PLACE WHERE TMP_PLAC_ID=@Id";
			param.Add("@Id", Int32.Parse(this.idQuery));
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			try
			{
				conn.Open();

				SqlDataReader dtrSelect= DBLibrary.GetDataReaderFromSelect(conn,strSQL2,param);
				while(dtrSelect.Read())
				{   
					// inserting values in PLACE table
					//string strSQL3="INSERT INTO PLACE (PLAC_COMP, PLAC_LOCL, PLAC_PERS, PLAC_PHON, PLAC_ADDR_ONE, PLAC_ADDR_TWO, PLAC_ZIP, PLAC_TYPE, PLAC_ENBL,PLAC_INST, PLAC_LABL, PLAC_RAIL_NUM) VALUES ('"+dtrSelect["TMP_PLAC_COMP"]+"', '"+idLocality+"','"+dtrSelect["TMP_PLAC_PERS"]+"' ,'"+dtrSelect["TMP_PLAC_PHON"]+"', '"+dtrSelect["TMP_PLAC_ADDR_ONE"]+"', '"+dtrSelect["TMP_PLAC_ADDR_TWO"]+"', '"+dtrSelect["TMP_PLAC_ZIP"]+"', '"+dtrSelect["TMP_PLAC_TYPE"]+"', '1', '"+dtrSelect["TMP_PLAC_INST"]+"', '"+dtrSelect["TMP_PLAC_LABL"]+"','"+dtrSelect["TMP_PLAC_RAIL_NUM"]+"')";
					string strSQL3="INSERT INTO PLACE (PLAC_COMP, PLAC_LOCL, PLAC_PERS, PLAC_PHON, PLAC_ADDR_ONE, PLAC_ADDR_TWO, PLAC_ZIP, PLAC_TYPE, PLAC_ENBL,PLAC_INST, PLAC_LABL, PLAC_RAIL_NUM) VALUES (@Company, @Locality,@Person ,@Phone, @Address1,  @Address2, @Zip, @Type, '1', @Inst, @Label,@RailNum)";
					//@Company, @Locality,@Person ,@Phone, @Address1,  @Address2, @Zip, @Type, '1', @Inst, @Label,@RailNum				
					//'"+dtrSelect["TMP_PLAC_PERS"]+"' ,'"+'"+dtrSelect["TMP_PLAC_TYPE"]+"', '1', '"+dtrSelect["TMP_PLAC_INST"]+"', '"+dtrSelect["TMP_PLAC_LABL"]+"','"+dtrSelect["TMP_PLAC_RAIL_NUM"]+"')";
					param.Add("@Company",dtrSelect["TMP_PLAC_COMP"]);
					param.Add("@Locality",idLocality);
					param.Add("@Person",dtrSelect["TMP_PLAC_PERS"]);
					param.Add("@Phone",dtrSelect["TMP_PLAC_PHON"]);
					param.Add("@Address1",dtrSelect["TMP_PLAC_ADDR_ONE"]);
					param.Add("@Address2",dtrSelect["TMP_PLAC_ADDR_TWO"]);
					param.Add("@Zip",dtrSelect["TMP_PLAC_ZIP"]);
					param.Add("@Type",dtrSelect["TMP_PLAC_TYPE"]);
					param.Add("@Inst",dtrSelect["TMP_PLAC_INST"]);

					DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(),strSQL3,param);
				}
			}
			finally
			{
				conn.Close();
			}	
			// delete the entry from TMP_PLACE table
			string strSQL4="DELETE TMP_PLACE";
			DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(),strSQL4,null);
		}
		private string insertLocality()
		{// insert the new locality in LOCALITY table
			string id = "";
			
			// get values about the new place from TMP_PLACE table
			string cityFrom = "";
			string stateFrom = "";	
			string countryFrom = "";	
			Hashtable param = new Hashtable();

			//string strSQL2="SELECT TMP_PLAC_ID,TMP_PLAC_CITY,TMP_PLAC_STAT,TMP_PLAC_CTRY FROM TMP_PLACE WHERE TMP_PLAC_ID='"+Request.QueryString["id"]+"'";
			string strSQL2="SELECT TMP_PLAC_ID,TMP_PLAC_CITY,TMP_PLAC_STAT,TMP_PLAC_CTRY FROM TMP_PLACE WHERE TMP_PLAC_ID=@Id";
			param.Add("@Id",Int32.Parse(Request.QueryString["id"]));
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			try
			{
				conn.Open();

				SqlDataReader dtrSelect= DBLibrary.GetDataReaderFromSelect(conn,strSQL2,param);
				while(dtrSelect.Read())
				{
					cityFrom = dtrSelect["TMP_PLAC_CITY"].ToString();
					stateFrom = dtrSelect["TMP_PLAC_STAT"].ToString();	
					countryFrom = dtrSelect["TMP_PLAC_CTRY"].ToString();	
				}
			}
			finally
			{
				conn.Close();
			}

			// set the new place in LOCALITY
			string strSQL3="INSERT INTO LOCALITY (LOCL_CITY,LOCL_STAT,LOCL_CTRY) VALUES (@CityFrom,@StateFrom,@CountryFrom)";
			param.Clear();
			param.Add("@CityFrom",cityFrom); 
			param.Add("@StateFrom",stateFrom);
			param.Add("@CountryFrom",countryFrom);

			DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(),strSQL3,param);
			  
			// get the id of the new locality
			string strSQL4="SELECT MAX(LOCL_ID) AS MAXI FROM LOCALITY";
			try
			{
				conn.Open();

				SqlDataReader dtrSelectMax= DBLibrary.GetDataReaderFromSelect(conn,strSQL4,null);
				while(dtrSelectMax.Read())
				{
					id = dtrSelectMax["MAXI"].ToString();
				}
				return id;

			}
			finally
			{
				conn.Close();
			}
		}
		private void calcAllDistances()
		{// calculate the distances between the new locality and the the others that already exist.

			// get values about the new place from TMP_PLACE table
			string cityFrom = "";
			string stateFrom = "";	
			string countryFrom = "";	
			Hashtable param = new Hashtable();

			SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());
			try
			{
				conn.Open();
				string strSQL2="SELECT TMP_PLAC_ID,TMP_PLAC_CITY,TMP_PLAC_STAT,TMP_PLAC_CTRY FROM TMP_PLACE WHERE TMP_PLAC_ID=@Id";
				param.Add("@Id",Int32.Parse(idQuery));
				SqlDataReader dtrSelect= DBLibrary.GetDataReaderFromSelect(conn,strSQL2,param);
				while(dtrSelect.Read())
				{
					cityFrom = dtrSelect["TMP_PLAC_CITY"].ToString();
					stateFrom = dtrSelect["TMP_PLAC_STAT"].ToString();	
					countryFrom = dtrSelect["TMP_PLAC_CTRY"].ToString();	
				}
			}
			finally
			{
				conn.Close();
			}
			float sDistance = 0;

			// initialize geoplaces webservice
			GeoPlaces myGeoPlaces = new GeoPlaces();
			AuthenticationHeader AuthenticationHeaderValue = new AuthenticationHeader();
			AuthenticationHeaderValue.SessionID="Wn7BufIemFWK6F6J5s2aS+UJlA7WPHA44bregtf8HqQtA/RH4b8VNa4JOhZmrPAOFqGvDqbCMwvPUZVjkkriBf/vxKxNHLxi";
			myGeoPlaces.AuthenticationHeaderValue = AuthenticationHeaderValue;

			// get all cities that arre different from the new locality
			//string strSQL = "SELECT LOCL_ID,LOCL_CITY,LOCL_STAT,LOCL_CTRY FROM LOCALITY WHERE (NOT(LOCL_CITY = '"+cityFrom+"' AND LOCL_STAT = '"+stateFrom+"')) AND LOCL_CTRY ='US'"; 
			string strSQL = "SELECT LOCL_ID,LOCL_CITY,LOCL_STAT,LOCL_CTRY FROM LOCALITY WHERE (NOT(LOCL_CITY = @CityFrom AND LOCL_STAT = @StateFrom)) AND LOCL_CTRY ='US'"; 
			param.Clear();
			param.Add("@CityFrom",cityFrom);
			param.Add("@StateFrom",stateFrom);
			try
			{
				conn.Open();
				SqlDataReader dtrLocality= DBLibrary.GetDataReaderFromSelect(conn,strSQL,param);
				while(dtrLocality.Read())			
				{
					// get the distance between the new locality and all other cities
					sDistance = myGeoPlaces.GetDistanceBetweenPlaces(cityFrom,stateFrom,dtrLocality["LOCL_CITY"].ToString(),dtrLocality["LOCL_STAT"].ToString());
					
					// insert each distance in DISTANCES table
					SqlConnection conn3 = new SqlConnection(Application["DBconn"].ToString());
					conn3.Open();
					//string strSQL3="INSERT INTO DISTANCES (CITY_FROM_ID,CITY_FROM_NAME,CITY_TO_ID,CITY_TO_NAME,DISTANCE) VALUES ('"+idLocality+"','"+cityFrom+"','"+dtrLocality["LOCL_ID"].ToString()+"','"+dtrLocality["LOCL_CITY"].ToString()+"','"+sDistance.ToString()+"')";
					string strSQL3="INSERT INTO DISTANCES (CITY_FROM_ID,CITY_FROM_NAME,CITY_TO_ID,CITY_TO_NAME,DISTANCE) VALUES (@Id,@CityFrom,@Locality,@LocalCity,@Distance)";
					//VALUES ('"+idLocality+"','"+cityFrom+"','"+dtrLocality["LOCL_ID"].ToString()+"','"+dtrLocality["LOCL_CITY"].ToString()+"','"+sDistance.ToString()+"')";
					param.Clear();	
					param.Add("@Id",Int32.Parse(idLocality));
					param.Add("@CityFrom",cityFrom);
					param.Add("@Locality",dtrLocality["LOCL_ID"].ToString());
					param.Add("@LocalCity",dtrLocality["LOCL_CITY"].ToString());
					param.Add("@Distance",sDistance.ToString());
					DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(),strSQL3,param);
					
					// insert each distance in DISTANCES table (city_from and city_to are reversed)
					//string strSQL4="INSERT INTO DISTANCES (CITY_FROM_ID,CITY_FROM_NAME,CITY_TO_ID,CITY_TO_NAME,DISTANCE) VALUES ('"+dtrLocality["LOCL_ID"].ToString()+"','"+dtrLocality["LOCL_CITY"].ToString()+"','"+idLocality+"','"+cityFrom+"','"+sDistance.ToString()+"')";
					string strSQL4="INSERT INTO DISTANCES (CITY_FROM_ID,CITY_FROM_NAME,CITY_TO_ID,CITY_TO_NAME,DISTANCE) VALUES (@Id,@LocalCity,@Locality,@CityFrom,@Distance)";
					param.Clear();
					param.Add("@Id",dtrLocality["LOCL_ID"].ToString());
					param.Add("@LocalCity",dtrLocality["LOCL_CITY"].ToString());
					param.Add("@Locality",idLocality);
					param.Add("@CityFrom",cityFrom);
					param.Add("@Distance",sDistance.ToString());
					
					DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(),strSQL4,param);
				}
				insertPlace();
			}
			finally
			{
				conn.Close();
			}

		}
		private void calcEstimateTime()
		{// estimate the maximum time to calculate all distances
			float sDistance = 0;
			
			// initialize geoplaces webservice
			GeoPlaces myGeoPlaces = new GeoPlaces();
			AuthenticationHeader AuthenticationHeaderValue = new AuthenticationHeader();
			AuthenticationHeaderValue.SessionID="Wn7BufIemFWK6F6J5s2aS+UJlA7WPHA44bregtf8HqQtA/RH4b8VNa4JOhZmrPAOFqGvDqbCMwvPUZVjkkriBf/vxKxNHLxi";
			myGeoPlaces.AuthenticationHeaderValue = AuthenticationHeaderValue;
			
			// calculate time to get the 5 distances
			int sec1 = DateTime.Now.Second;
			int min1 = DateTime.Now.Minute;
			for(int i=0;i<5;++i)
			{
				sDistance = myGeoPlaces.GetDistanceBetweenPlaces("chicago","il","new york","ny");
			}
			int sec2 = DateTime.Now.Second;
			int min2 = DateTime.Now.Minute;
			
			// time to get 5 request from the geoplaces web service
			float total = (min2*60 + sec2) - (min1*60 + sec1); 


			string strSQL = "SELECT count(*) AS MAXI FROM LOCALITY"; 
			SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			SqlCommand cmdNb= new SqlCommand(strSQL, conn);
			SqlDataReader dtrNb= cmdNb.ExecuteReader();
			float total2 = 0;
			int nbCities = 0;
			while(dtrNb.Read())			
			{// build the maximum estimated time to get all the distances.
				total2 = (total / 5) * Convert.ToInt32(dtrNb["MAXI"].ToString());
				nbCities = Convert.ToInt32(dtrNb["MAXI"].ToString());
			}
			conn.Close();
			
			string delimStr = ".";
			char [] delimiter = delimStr.ToCharArray();
			string[] tabTotal2 = ((total2/60).ToString()).Split(delimiter);

			lblEstimationTime.Text = "The estimated time to calculate distance with the other cities is " + (tabTotal2[0]).ToString() + " min " +Convert.ToInt32(Convert.ToDouble(tabTotal2[1])* 0.6).ToString() + " sec.";
			lblCalculating.Text = "You can continue to navigate through the website and the new location will be added at the end of the time.";
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
