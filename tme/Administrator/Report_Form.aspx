<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page language="c#" Codebehind="Report_Form.aspx.cs" AutoEventWireup="false" Inherits="localhost.Administrator.Report_Form" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Report_Form</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TPE:TEMPLATE id="Template1" PageTitle="Market Update" Runat="Server"></TPE:TEMPLATE><br>
			<CENTER>
				<H3><asp:label id="lblTitle" Runat="server" CssClass="PageHeader">Financial Reports</asp:label></H3>
			</CENTER>
			<asp:panel id="pnlEnter" Runat="server">
				<TABLE id="Table1" width="100%" align="center">
					<TR>
						<TD colSpan="2">
							<asp:validationsummary id="ValidationSummary1" runat="server" HeaderText="There are problems with the following fields. Please correct the errors below."
								Width="487px" Height="92px"></asp:validationsummary></TD>
					</TR>
					<TR>
						<TD align="left" width="30%">
							<asp:Label id="lblReportTitle" Runat="server">Reports:&nbsp;&nbsp;</asp:Label>
							<asp:DropDownList id="ddlReportType" Runat="server" AutoPostBack="True">
								<asp:ListItem Value="1" Selected="True">Balance Sheet</asp:ListItem>
								<asp:ListItem Value="2">Income Statement</asp:ListItem>
							</asp:DropDownList></TD>
						<TD align="left">
							<asp:Label id="lblMonthTitle" Runat="server">Month:&nbsp;&nbsp;</asp:Label>
							<asp:DropDownList id="ddlMonth" Runat="server" AutoPostBack="True"></asp:DropDownList></TD>
					</TR>
				</TABLE>
			</asp:panel><asp:panel id="pnlMain" Runat="server" Visible="false">
				<TABLE id="Table2" width="100%">
					<TR>
						<TD align="left">
							<asp:Label id="lblNameOfParameter" Runat="server"></asp:Label>
							<asp:TextBox id="txtParameter" Runat="server" Visible="false" MaxLength="10"></asp:TextBox>
							<asp:RangeValidator id="RangeValidator0" runat="server" ControlToValidate="txtParameter" ErrorMessage="invalid value of Overhead or Average Inventory Valuation"
								MinimumValue="0" MaximumValue="999999999" Type="Currency" Display="None"></asp:RangeValidator>
							<asp:RequiredFieldValidator id="RequiredFieldValidator0" Runat="server" ControlToValidate="txtParameter" ErrorMessage="Overhead or Average Inventory Valuation required"
								Display="None"></asp:RequiredFieldValidator></TD>
					</TR>
					<TR>
						<TD align="center">
							<asp:DataGrid id="dg" runat="server" Visible="False" ItemStyle-CssClass="DataGridRow" AlternatingItemStyle-CssClass="DataGridRow_Alternate"
								HeaderStyle-CssClass="DataGridHeader" AutoGenerateColumns="False" HorizontalAlign="Center"
								Width="740px" Height="123px" GridLines="None">
								<AlternatingItemStyle CssClass="DataGridRow_Alternate"></AlternatingItemStyle>
								<ItemStyle CssClass="DataGridRow"></ItemStyle>
								<HeaderStyle HorizontalAlign="Center" CssClass="DataGridHeader"></HeaderStyle>
								<Columns>
									<asp:BoundColumn DataField="ACCOUNT_DATE_TITLE" HeaderText="Date"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="ACCOUNT_TRADING" HeaderText="Trading ACB ($)"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="ACCOUNT_OPERATION" HeaderText="Operatings ACB ($)"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="ACCOUNT_COLLATERAL" HeaderText="Collateral ACB ($)"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="ACCOUNT_TPE_CREDIT" HeaderText="TPE Credit ACB ($)"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="ACCOUNT_MAG_CREDIT" HeaderText="MAG Credit ACB ($)"></asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Trading ACB ($)">
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
										<ItemTemplate>
											<asp:TextBox id="TextBox1" runat="server" Visible="True" Width="130px"></asp:TextBox>
											<asp:RangeValidator id="Rangevalidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="Trading ACB - invalid value"
												MinimumValue="0" MaximumValue="999999999" Type="Currency" Display="None"></asp:RangeValidator>
											<asp:RequiredFieldValidator ID="Requiredfieldvalidator1" Runat="server" ControlToValidate="TextBox1" ErrorMessage="Trading ACB required"
												Display="None"></asp:RequiredFieldValidator>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Operatings ACB ($)">
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
										<ItemTemplate>
											<asp:TextBox id="TextBox2" runat="server" Visible="True" Width="130px"></asp:TextBox>
											<asp:RangeValidator id="Rangevalidator2" runat="server" ControlToValidate="TextBox2" ErrorMessage="Operating ACB - invalid value"
												Type="Currency" Display="None" MinimumValue="0" MaximumValue="999999999"></asp:RangeValidator>
											<asp:RequiredFieldValidator ID="Requiredfieldvalidator2" Runat="server" ControlToValidate="TextBox2" ErrorMessage="Operating ACB required"
												Display="None"></asp:RequiredFieldValidator>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Collateral ACB ($)">
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
										<ItemTemplate>
											<asp:TextBox id="TextBox3" runat="server" Visible="True" Width="130px"></asp:TextBox>
											<asp:RangeValidator id="RangeValidator3" runat="server" ControlToValidate="TextBox3" ErrorMessage="Collateral ACB - invalid value"
												Type="Currency" Display="None" MinimumValue="0" MaximumValue="999999999"></asp:RangeValidator>
											<asp:RequiredFieldValidator ID="Requiredfieldvalidator3" Runat="server" ControlToValidate="TextBox3" ErrorMessage="Collateral ACB required"
												Display="None"></asp:RequiredFieldValidator>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="TPE Credit ACB ($)">
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
										<ItemTemplate>
											<asp:TextBox id="TextBox4" runat="server" Visible="True" Width="130px"></asp:TextBox>
											<asp:RangeValidator id="Rangevalidator4" runat="server" ControlToValidate="TextBox4" ErrorMessage="TPE Credit - invalid value"
												Type="Currency" Display="None" MinimumValue="0" MaximumValue="999999999"></asp:RangeValidator>
											<asp:RequiredFieldValidator ID="Requiredfieldvalidator4" Runat="server" ControlToValidate="TextBox4" ErrorMessage="TPE Credit required"
												Display="None"></asp:RequiredFieldValidator>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="MAG Credit ACB ($)">
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
										<ItemTemplate>
											<asp:TextBox id="TextBox5" runat="server" Visible="True" MaxLength="10" Width="130px"></asp:TextBox>
											<asp:RangeValidator id="Rangevalidator5" runat="server" ControlToValidate="TextBox5" ErrorMessage="MAG Credit - invalid value"
												Type="Currency" Display="None" MinimumValue="0" MaximumValue="999999999"></asp:RangeValidator>
											<asp:RequiredFieldValidator ID="Requiredfieldvalidator5" Runat="server" ControlToValidate="TextBox5" ErrorMessage="MAG Credit required"
												Display="None"></asp:RequiredFieldValidator>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:DataGrid></TD>
					</TR>
				</TABLE>
				<BR>
				<BR>
				<CENTER>
					<asp:Button id="btnUpdate" Runat="server" Text="Update and View Report"></asp:Button></CENTER>
			</asp:panel><br>
			<TPE:TEMPLATE id="Template2" Runat="Server" Footer="true"></TPE:TEMPLATE></form>
	</body>
</HTML>
