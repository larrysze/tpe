<%@ Page language="c#" Codebehind="ApproveOrder.aspx.cs" AutoEventWireup="True" Inherits="pes1.Administrator.ApproveOrder" MasterPageFile="~/MasterPages/Menu.Master" Title="ApproveOrder"%>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
	<TABLE id="Table1" style="WIDTH: 773px; HEIGHT: 326px" borderColor="black" height="326"
		cellSpacing="0" cellPadding="0" width="773" align="center" border="0" class="Content">
		<TR>
			<TD vAlign="top" align="left">
				<TABLE id="Table11" style="WIDTH: 768px; HEIGHT: 26px" height="26" width="768" border="0">
					<TR>
						<!--<TD align="center"><FONT size="4"><B>Order Confirmation</B> </FONT></td>-->
						<TD style="WIDTH: 700px" align="center"><asp:label id="lblTitle" runat="server">Order Confirmation</asp:label></TD>
					</TR>
				</TABLE>
				<TABLE id="Table12" style="WIDTH: 769px; HEIGHT: 227px" height="227" width="769" border="0" class="Content">
					<TR>
						<TD style="WIDTH: 703px" align="left" colSpan="7"><FONT color="blue" size="3"><B>Order 
									Summary</B></FONT></TD>
					</TR> 
					<TR>
						<TD style="WIDTH: 62px; HEIGHT: 27px" align="left"><FONT size="2"><B>ID#</B></FONT>
						</TD>
						<TD style="WIDTH: 202px; HEIGHT: 27px" align="left"><FONT size="2"><B>Product</B></FONT></TD>
						<TD style="WIDTH: 79px; HEIGHT: 27px" align="left"><FONT size="2"><B>Size</B></FONT></TD>
						<TD style="WIDTH: 67px; HEIGHT: 27px" align="left"><FONT size="2"><B>Melt</B></FONT></TD>
						<TD style="WIDTH: 69px; HEIGHT: 27px" align="left"><FONT size="2"><B>Density</B></FONT></TD>
						<TD style="HEIGHT: 27px" align="left"><FONT size="2"><B>Adds</B></FONT></TD>
						<TD style="WIDTH: 57px; HEIGHT: 27px" align="left"><FONT size="2"><B>Terms</B></FONT></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 62px; HEIGHT: 18px" align="left"><FONT  size="2"><asp:label id="lblId" runat="server"></asp:label></FONT></TD>
						<TD style="WIDTH: 202px; HEIGHT: 18px" align="left"><FONT size="2"><asp:label id="lblProduct" runat="server" Width="279px"></asp:label></FONT></TD>
						<TD style="WIDTH: 79px; HEIGHT: 18px" align="left"><FONT  size="2"><asp:label id="lblSize" runat="server"></asp:label></FONT></TD>
						<TD style="WIDTH: 67px; HEIGHT: 18px" align="left"><FONT  size="2"><asp:label id="lblMelt" runat="server"></asp:label></FONT></TD>
						<TD style="WIDTH: 69px; HEIGHT: 18px" align="left"><FONT  size="2"><asp:label id="lblDens" runat="server"></asp:label></FONT></TD>
						<TD style="HEIGHT: 18px" align="left"><FONT face="Times New Roman" size="2"><asp:label id="lblAdds" runat="server"></asp:label></FONT></TD>
						<TD style="WIDTH: 57px; HEIGHT: 18px" align="left"><FONT face="Times New Roman" size="2"><asp:label id="lblTerms" runat="server"></asp:label></FONT></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 62px; HEIGHT: 97px" align="left"><FONT size="2"></FONT></TD>
						<TD style="WIDTH: 380px; HEIGHT: 97px" align="left" colSpan="2"><FONT size="2"></FONT></TD>
						<!--<TD style="WIDTH: 91px; HEIGHT: 111px" align="left"><FONT size="2"></FONT></TD>-->
						<TD style="WIDTH: 67px; HEIGHT: 97px" align="left"><FONT size="2"></FONT></TD>
						<TD style="WIDTH: 69px; HEIGHT: 97px" align="left"><FONT size="2"></FONT></TD>
						<TD style="HEIGHT: 97px" align="left"><FONT color="blue" size="2"><b><FONT size="2"><B><FONT color="blue" size="2"><B>TOTAL:</B></FONT></B></FONT></b></FONT></TD>
						<TD style="WIDTH: 57px; HEIGHT: 97px" align="left"><FONT size="2"><b><FONT color="blue" size="2"><B><asp:label id="lblTotal" runat="server"></asp:label></B></FONT></b></FONT></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 62px" align="left"><FONT size="2"></FONT></TD>
						<TD style="WIDTH: 380px; HEIGHT: 13px" align="left" colSpan="2"><FONT size="2"><STRONG>Choose 
									The Start Location Address</STRONG></FONT></TD>
						<!--<TD style="WIDTH: 91px; HEIGHT: 13px" align="left" ><FONT size="2"></FONT></TD>-->
						<TD style="WIDTH: 67px; HEIGHT: 13px" align="left"><FONT size="2"></FONT></TD>
						<TD style="WIDTH: 69px; HEIGHT: 13px" align="left"><FONT size="2"></FONT></TD>
						<TD style="HEIGHT: 13px" align="left"><FONT size="2"><b><FONT size="2"><B><FONT size="2"><B>Ship to:</B></FONT></B></FONT></b></FONT></TD>
						<TD style="WIDTH: 57px; HEIGHT: 13px" align="left"><FONT size="2"><b><FONT size="2"><B><FONT size="2"><B>Bill 
													to:</B></FONT></B></FONT></b></FONT></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 62px" align="left"><FONT size="2"></FONT></TD>
						<TD style="WIDTH: 449px; HEIGHT: 13px" vAlign="top" align="left" colSpan="3"><FONT size="2"><asp:dropdownlist id="ddlAdd" runat="server" CssClass="Content Color2" Width="427px"></asp:dropdownlist></FONT></TD>
						<!--<TD style="WIDTH: 91px; HEIGHT: 13px" align="left" ><FONT size="2"></FONT></TD>
						<TD style="HEIGHT: 13px" align="left"><FONT size="2"></FONT></TD>-->
						<TD style="WIDTH: 69px; HEIGHT: 13px" align="left"><FONT size="2"></FONT></TD>
						<TD style="HEIGHT: 13px" vAlign="top" align="left"><FONT size="1"><FONT size="1"><asp:label id="lblShip" runat="server"></asp:label></FONT></FONT></TD>
						<TD style="WIDTH: 57px; HEIGHT: 13px" vAlign="top" align="left"><FONT size="1"><FONT size="1"><asp:label id="lblBill" runat="server"></asp:label></FONT></FONT></TD>
					</TR>
				</TABLE>
				<TABLE id="Table2" style="WIDTH: 769px; HEIGHT: 25px" borderColor="black" height="25" width="769"
					border="0">
					<TR>
						<TD align="center">&nbsp; <INPUT id="btnDeny" class="Content Color2" style="WIDTH: 73px; HEIGHT: 24px" type="button" value="Deny" name="btnDeny"
								runat="server" onserverclick="btnDeny_ServerClick"> <INPUT id="btnApprove" class="Content Color2" style="WIDTH: 88px; HEIGHT: 24px" type="button" value="Approve"
								name="btnApprove" runat="server" onserverclick="btnApprove_ServerClick">
						</TD>
					</TR>
				</TABLE>
				<P align="center"><asp:label id="lblMessage" runat="server" Font-Bold="True" ForeColor="Red"></asp:label></P>
			</TD>
		</TR>
	</TABLE>
</asp:Content>
