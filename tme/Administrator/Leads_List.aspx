<%@ Page language="c#" CodeBehind="Leads_List.aspx.cs" AutoEventWireup="false" Inherits="localhost.Administrator.Leads_List" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Leads_List</title>
		<script runat="server">

		</script>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
		<style type="text/css">BODY { MARGIN: 0px }
	</style>
		<LINK href="../styleIE.css" type="text/css" rel="stylesheet">
  </HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TPE:TEMPLATE id="TEMPLATE1" title="Filled Orders" Runat="server" Width=850></TPE:TEMPLATE>
			<asp:panel id="Panel1" runat="server">
<TABLE cellSpacing=0 cellPadding=0 border=0>
  <TR>
    <TD class=S1>&nbsp;type: 
<asp:dropdownlist id=DropDownList1 runat="server" Width="80px" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
								<asp:ListItem Value=" ">all</asp:ListItem>
								<asp:ListItem Value="HDPE">HDPE</asp:ListItem>
								<asp:ListItem Value="HMWPE">HMWPE</asp:ListItem>
								<asp:ListItem Value="Rotomolding">Rotomolding</asp:ListItem>
								<asp:ListItem Value="Blow mold">Blow Molding</asp:ListItem>
								<asp:ListItem Value="LLDPE">LLDPE</asp:ListItem>
							</asp:dropdownlist>&nbsp;Lbs. Per Year: 
<asp:dropdownlist id=DropDownList2 runat="server" Width="80px" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
								<asp:ListItem Value=" ">all</asp:ListItem>
								<asp:ListItem Value="100000">&gt;100,000</asp:ListItem>
								<asp:ListItem Value="500000">&gt;500,000</asp:ListItem>
								<asp:ListItem Value="1000000">&gt;1,000,000</asp:ListItem>
								<asp:ListItem Value="5000000">&gt;5,000,000</asp:ListItem>
							</asp:dropdownlist>&nbsp;Packaging: 
<asp:dropdownlist id=DropDownList3 runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged">
								<asp:ListItem Value=" ">All</asp:ListItem>
								<asp:ListItem Value="Truck">Truck</asp:ListItem>
								<asp:ListItem Value="rail">Rail Car</asp:ListItem>
								<asp:ListItem Value="gaylord">Gaylords</asp:ListItem>
								<asp:ListItem Value="bag">Bags</asp:ListItem>
							</asp:dropdownlist>&nbsp;Search: 
<asp:textbox id=TextBox1 runat="server" Width="80px"></asp:textbox>into 
<asp:dropdownlist id=DropDownList4 runat="server" Width="70px">
								<asp:ListItem Value="ALL">All</asp:ListItem>
								<asp:ListItem Value="COMP">Company</asp:ListItem>
								<asp:ListItem Value="City">City</asp:ListItem>
								<asp:ListItem Value="State">State</asp:ListItem>
								<asp:ListItem Value="VarPhone">Phone</asp:ListItem>
								<asp:ListItem Value="VarEmail">Email</asp:ListItem>
								<asp:ListItem Value="VarName">Name</asp:ListItem>
								<asp:ListItem Value="VarTitle">Title</asp:ListItem>
							</asp:dropdownlist>
<asp:button id=Button1 onclick=Button1_Click runat="server" Width="50px" Text="Show"></asp:button>
<asp:button id=Button2 onclick=Button2_Click runat="server" Width="60px" Text="ShowAll"></asp:button></TD></TR></TABLE>
<TABLE cellSpacing=0 cellPadding=0 border=0>
  <TR>
    <TD align=center>&nbsp; 
<asp:datagrid id=DataGrid2 runat="server" Width="822px" DataKeyField="loc_id" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="True" BorderColor="Transparent" GridLines="None" PageSize="60" OnPageIndexChanged="DataGrid2_PageIndexChanged" OnSortCommand="DataGrid2_SortCommand">
<AlternatingItemStyle CssClass="DataGridRow_Alternate" BackColor="Gainsboro">
</AlternatingItemStyle>

<HeaderStyle CssClass="DataGridHeader">
</HeaderStyle>

<Columns>
<asp:TemplateColumn SortExpression="Resin" HeaderText="Type">
<ItemStyle CssClass="DataGridRow">
</ItemStyle>

<ItemTemplate>
											<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="../Images/Icons/whchange.gif" OnClick="Loadinfor" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.LOC_ID") %>'>
											</asp:ImageButton>
											<%# DataBinder.Eval(Container, "DataItem.Resin") %>
										
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="COMP" HeaderText="Company">
<ItemStyle CssClass="DataGridRow">
</ItemStyle>

<ItemTemplate>
											<asp:LinkButton Text='<%# DataBinder.Eval(Container, "DataItem.COMP") %>' CommandName="Select" CausesValidation="false" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.company") %>' runat="server" OnClick="Load_Comment">
											</asp:LinkButton>
										
</ItemTemplate>
</asp:TemplateColumn>
<asp:BoundColumn DataField="city" SortExpression="city" HeaderText="City">
<ItemStyle CssClass="DataGridRow">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VarPhone" SortExpression="VarPhone" HeaderText="Phone">
<ItemStyle Wrap="False" CssClass="DataGridRow">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="state" SortExpression="state" HeaderText="State">
<ItemStyle HorizontalAlign="Center" CssClass="DataGridRow">
</ItemStyle>
</asp:BoundColumn>
<asp:TemplateColumn SortExpression="LbsPerYr" HeaderText="Lbs/Year">
<ItemStyle HorizontalAlign="Center" CssClass="DataGridRow">
</ItemStyle>

<ItemTemplate>
											<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LbsPerYr", "{0:#,###} ") %>'>
											</asp:Label>
										
</ItemTemplate>
</asp:TemplateColumn>
<asp:BoundColumn DataField="varemail" SortExpression="varemail" HeaderText="Email">
<ItemStyle CssClass="DataGridRow">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="varname" SortExpression="varname" HeaderText="Name">
<ItemStyle CssClass="DataGridRow">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="vartitle" SortExpression="vartitle" HeaderText="Title">
<ItemStyle CssClass="DataGridRow">
</ItemStyle>
</asp:BoundColumn>
</Columns>

<PagerStyle Position="TopAndBottom" Mode="NumericPages">
</PagerStyle>
</asp:datagrid></TD></TR></TABLE>
			</asp:panel>
			<asp:panel id="Panel2" runat="server">
<TABLE cellSpacing=0 cellPadding=0 width=600 align=center border=0>
  <TR>
    <TD class=S4>message: 
  <asp:label id=Label1 runat="server"></asp:label></TD></TR></TABLE>
<TABLE height=250 cellSpacing=0 cellPadding=0 width=600 align=center border=0>
  <TR>
    <TD class=S1 align=center colSpan=6>Location Information </TD></TR>
  <TR>
    <TD class=S2 width=70>&nbsp;<FONT face=宋体>&nbsp; Resin</FONT></TD>
    <TD class=S2 width=101>
<asp:textbox id=resin runat="server" Width="100px"></asp:textbox></TD>
    <TD class=S2 width=78>&nbsp;<FONT face=宋体>&nbsp; Email</FONT></TD>
    <TD class=S2 colSpan=3>
<asp:textbox id=LocationEmail runat="server" Width="200px"></asp:textbox></TD></TR>
  <TR>
    <TD class=S2>&nbsp;<FONT face=宋体>&nbsp; City</FONT></TD>
    <TD class=S2>
<asp:textbox id=city runat="server" Width="100px"></asp:textbox></TD>
    <TD class=S2>&nbsp;<FONT face=宋体>&nbsp; State</FONT></TD>
    <TD class=S2 width=122>
<asp:textbox id=state runat="server" Width="150px"></asp:textbox></TD>
    <TD class=S2 width=85>&nbsp;<FONT face=宋体>&nbsp; ZIP </FONT></TD>
    <TD class=S2 width=144>
<asp:textbox id=zip runat="server" Width="100px"></asp:textbox></TD></TR>
  <TR>
    <TD class=S2>&nbsp;<FONT face=宋体>&nbsp; Name</FONT></TD>
    <TD class=S2>
<asp:textbox id=name runat="server" Width="100px"></asp:textbox></TD>
    <TD class=S2>&nbsp;<FONT face=宋体>&nbsp; Title</FONT></TD>
    <TD class=S2>
<asp:textbox id=TextBox8 runat="server" Width="150px"></asp:textbox></TD>
    <TD class=S2>&nbsp;<FONT face=宋体>&nbsp; Phone</FONT></TD>
    <TD class=S2>
<asp:textbox id=LocationPhone runat="server" Width="100px"></asp:textbox></TD></TR>
  <TR>
    <TD class=S2>&nbsp;<FONT face=宋体>&nbsp; Package</FONT></TD>
    <TD class=S2>
<asp:textbox id=package runat="server" Width="100px"></asp:textbox></TD>
    <TD class=S2>&nbsp;<FONT face=宋体>&nbsp; Users</FONT></TD>
    <TD class=S2 colSpan=3>
<asp:textbox id=users runat="server" Width="200px"></asp:textbox></TD></TR>
  <TR>
    <TD class=S2 style="HEIGHT: 23px">&nbsp;<FONT face=宋体>&nbsp; 
      Lbs/Year</FONT></TD>
    <TD class=S2 style="HEIGHT: 23px">
<asp:textbox id=lbs runat="server" Width="100px"></asp:textbox></TD>
    <TD class=S2 style="HEIGHT: 23px">&nbsp;<FONT face=宋体>&nbsp; 
    Items</FONT></TD>
    <TD class=S2 style="HEIGHT: 23px" colSpan=3>
<asp:textbox id=items runat="server" Width="250px"></asp:textbox></TD></TR>
  <TR align=center>
    <TD class=S1 colSpan=6>Company Information </TD></TR>
  <TR>
    <TD class=S2>&nbsp;<FONT face=宋体>&nbsp;content</FONT></TD>
    <TD class=S2>
<asp:textbox id=contect runat="server" Width="100px"></asp:textbox></TD>
    <TD class=S2>&nbsp;<FONT face=宋体>&nbsp; Email</FONT></TD>
    <TD class=S2>
<asp:textbox id=companyEmail runat="server" Width="200px"></asp:textbox></TD>
    <TD class=S2>CompanyName: </TD>
    <TD class=S2>
<asp:textbox id=companyName runat="server" Width="200px"></asp:textbox></TD></TR>
  <TR>
    <TD class=S2>&nbsp;<FONT face=宋体>&nbsp; Title</FONT></TD>
    <TD class=S2>
<asp:textbox id=companyTitle runat="server" Width="100px"></asp:textbox></TD>
    <TD class=S2>&nbsp;<FONT face=宋体>&nbsp; Phone</FONT></TD>
    <TD class=S2 colSpan=3>
<asp:textbox id=CompanyPhone runat="server" Width="200px"></asp:textbox></TD></TR>
  <TR>
    <TD class=S2 align=center colSpan=6>&nbsp; 
<asp:button id=infosub onclick=modifyInfo runat="server" Text="Submit"></asp:button><FONT 
      face=宋体>&nbsp;</FONT> 
<asp:button id=infocan onclick=info_backup runat="server" Text="Cancel"></asp:button></TD></TR></TABLE>
			</asp:panel>
			<asp:panel id="Panel3" runat="server">
<TABLE cellSpacing=0 cellPadding=0 width=600 align=center border=0>
  <TR>
    <TD height=20>&nbsp;</TD></TR></TABLE>
<TABLE cellSpacing=0 cellPadding=0 width=600 align=center border=0>
  <TR>
    <TD class=S5 bgColor=black colSpan=2><IMG src="/images/1x1.gif" 
      width=5>Comments for: 
  <asp:Label id=L1 runat="server"></asp:Label></TD></TR></TABLE>
<TABLE cellSpacing=0 cellPadding=0 width=600 align=center border=0>
  <TR>
    <TD vAlign=top>
<asp:DataGrid id=DataGrid1 runat="server" Width="600px" AutoGenerateColumns="False" BorderWidth="0px" CellPadding="0">
								<AlternatingItemStyle BackColor="#E8E8E8"></AlternatingItemStyle>
								<Columns>
									<asp:BoundColumn DataField="Date"></asp:BoundColumn>
									<asp:BoundColumn DataField="CMNT_TEXT"></asp:BoundColumn>
								</Columns>
							</asp:DataGrid></TD></TR></TABLE>
<TABLE cellSpacing=0 cellPadding=0 width=600 align=center border=0>
  <TR>
    <TD class=S5 colSpan=2>
<asp:Label id=L2 runat="server" BackColor="White" ForeColor="Red"></asp:Label></TD></TR></TABLE>
<TABLE cellSpacing=0 cellPadding=0 width=600 align=center border=0>
  <TR>
    <TD align=center>&nbsp; 
<asp:TextBox id=T1 runat="server" Width="400px" TextMode="MultiLine" Height="60px"></asp:TextBox></TD></TR></TABLE>
<TABLE cellSpacing=0 cellPadding=0 width=600 align=center border=0>
  <TR>
    <TD align=center>
<asp:RadioButtonList id=RadioButtonList1 runat="server" RepeatDirection="Horizontal">
								<asp:ListItem Value="1">Gread</asp:ListItem>
								<asp:ListItem Value="2">Fair</asp:ListItem>
								<asp:ListItem Value="3">Poor</asp:ListItem>
								<asp:ListItem Value="4">Message</asp:ListItem>
							</asp:RadioButtonList>&nbsp;</TD></TR>
  <TR>
    <TD align=center>
<asp:Button id=B_Sub onclick=modify_comment runat="server" Text="Submit"></asp:Button>&nbsp; 
<asp:Button id=B_Can onclick=info_backup runat="server" Text="Cancel"></asp:Button></FONT></TD></TR></TABLE>
			</asp:panel>
			<TPE:TEMPLATE id="Template2" Runat="server" Footer="true"></TPE:TEMPLATE></form>
	</body>
</HTML>
