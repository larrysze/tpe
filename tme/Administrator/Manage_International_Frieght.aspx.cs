using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;
using TPE.Utility;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Manage_International_Frieght.
	/// </summary>
	public partial class Manage_International_Frieght : System.Web.UI.Page
	{
		protected void Page_Load(object sender, System.EventArgs e)
		{
            Master.Width = "750px";
			if (Session["Typ"].ToString() != "A" )
			{
				Response.Redirect("/default.aspx");
			}
			
			Build_Table();
			
			

		}
		private void Build_Table()
		{

			ArrayList ALPortName = new ArrayList();
			ArrayList ALPortID = new ArrayList();
			ArrayList ALPortGroupID = new ArrayList();
			ArrayList ALGroupName = new ArrayList();
			ArrayList ALGroupID = new ArrayList();
			
				

			/*	SqlConnection conn;
				SqlDataReader dtrPorts;
				conn = new SqlConnection(Application["DBconn"].ToString());
				conn.Open();
				SqlCommand cmd = new SqlCommand("select *, PORT= PORT_CITY +', ' + (SELECT CTRY_NAME FROM COUNTRY WHERE CTRY_CODE=PORT_COUNTRY)from INTERNATIONAL_PORT where PORT_ENBL ='1' ORDER BY PORT",conn);
				dtrPorts = cmd.ExecuteReader(); */

			using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				string strSQL = "select *, PORT= PORT_CITY +', ' + (SELECT CTRY_NAME FROM COUNTRY WHERE CTRY_CODE=PORT_COUNTRY)from INTERNATIONAL_PORT where PORT_ENBL ='1' ORDER BY PORT";
				using (SqlDataReader dtrPorts = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
				{

				
					while(dtrPorts.Read())
					{
						ALPortName.Add(dtrPorts["PORT"].ToString());
						ALPortID.Add(dtrPorts["PORT_ID"].ToString());
						ALPortGroupID.Add(dtrPorts["PORT_GROUP"].ToString());
					}
					ALPortName.TrimToSize();
					ALPortID.TrimToSize();
					ALPortGroupID.TrimToSize();       
				}
			}
			

			/*	SqlConnection conn2;
				SqlDataReader dtrGroups;
				conn2 = new SqlConnection(Application["DBconn"].ToString());
				conn2.Open();
				cmd = new SqlCommand("SELECT * FROM PORT_GROUP ORDER BY PORT_GROUP_NAME",conn2);
				dtrGroups = cmd.ExecuteReader(); */

			using (SqlConnection conn2 = new SqlConnection(Application["DBconn"].ToString()))
			{
				string strSQL2="SELECT * FROM PORT_GROUP ORDER BY PORT_GROUP_NAME";
				using (SqlDataReader dtrGroups = DBLibrary.GetDataReaderFromSelect(conn2, strSQL2))
				{
		
					while(dtrGroups.Read())
					{
						ALGroupName.Add(dtrGroups["PORT_GROUP_NAME"].ToString());
						ALGroupID.Add(dtrGroups["PORT_GROUP_ID"].ToString());
					}
					ALGroupName.TrimToSize();
					ALGroupID.TrimToSize();
				}
			}
			TableRow tr = new TableRow();
			// Create column 1
			TableCell td1 = new TableCell();
			TableCell td2;

			tr.Cells.Add(td1);
			for ( int k = 0; k < ALPortName.Capacity; k++ )
			{
				// Create Group Headers
				td2 = new TableCell();
				td2.HorizontalAlign = HorizontalAlign.Center;
                td2.BackColor = Color.Orange;

				Label lblHeader = new Label();
				lblHeader.ID = "Header_" + k.ToString();
				lblHeader.Text = ALPortName[k].ToString();
				lblHeader.Font.Size =7;
				// Add control to the table cell
				td2.Controls.Add(lblHeader);
				// Add cell to the row
				tr.Cells.Add(td2);
				// Add row to the table.
			
			}
				tblDynamic.Rows.Add(tr);
			
			for ( int i = 0; i < ALGroupName.Capacity; i++ )
			{
				tr = new TableRow();
			
				// Create column 1
				td1 = new TableCell();
				td1.Wrap = false;
                td1.BackColor = Color.Orange;
                td1.HorizontalAlign = HorizontalAlign.Right;
                td1.CssClass = "Content Color2";
				// Create a label control dynamically
				Label lblGroupName = new Label();
                lblGroupName.ID = "lbl" + i.ToString();
                lblGroupName.Text = ALGroupName[i].ToString();
				// Add control to the table cell
                td1.Controls.Add(lblGroupName);
				tr.Cells.Add(td1);
				for ( int k = 0; k < ALPortName.Capacity; k++ )
				{
					// Create column 2
					td2 = new TableCell();
			
			

					TextBox txt = new TextBox();
			
			
			
					txt.ID = "txt_" + ALGroupID[i].ToString()+"_"+ALPortID[k].ToString();
					// Add control to the table cell
					txt.Width=60;
					txt.Font.Size =7;
                   
					// only add group that the port is not part of 
					//if (ALPortGroupID[i].ToString() !=ALGroupID[k].ToString())
					//{
			
					CompareValidator Validator = new CompareValidator();
					Validator.ID = "validate_" + ALGroupID[i].ToString()+"_"+ALPortID[k].ToString();
					//Validator.Text=;
					Validator.ErrorMessage ="Only Intergers!";
					Validator.Type =ValidationDataType.Integer;
					Validator.Operator = ValidationCompareOperator.DataTypeCheck;
					Validator.ControlToValidate = txt.ID;
					// pull current data from db
							
					/*conn = new SqlConnection(Application["DBconn"].ToString());
							conn.Open();
							cmd = new SqlCommand("SELECT CONVERT(INT,IFM_PRICE) FROM FREIGHT_MATRIX_INTERNATIONAL WHERE IFM_ORIGIN_PORT='"+ALPortID[i].ToString()+"' AND IFM_DESTINATION_GROUP='"+ALGroupID[k].ToString()+"';",conn);
							*/

					string strSQL="SELECT CONVERT(INT,IFM_PRICE) FROM FREIGHT_MATRIX_INTERNATIONAL WHERE IFM_ORIGIN_PORT=@ifm_orign_port AND IFM_DESTINATION_GROUP=@ifm_destination_group";
					Hashtable htParams = new Hashtable();
                    htParams.Add("@ifm_destination_group", ALGroupID[i].ToString());
                    htParams.Add("@ifm_orign_port", ALPortID[k].ToString());
					object o = DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(),strSQL,htParams);
					if (o !=null)
					{
						txt.Text = o.ToString();
					}
				
					td2.Controls.Add(txt);
					td2.Controls.Add(Validator);
					//}
				
					// Add cell to the row
					td2.HorizontalAlign = HorizontalAlign.Center;
                    td2.BackColor = Color.Silver;
                    td2.Height = 20;
					tr.Cells.Add(td2);
					// Add row to the table.					
					
		    			tblDynamic.Rows.Add(tr);
					
				}
			}
		}

		/// <summary>
		/// interate through all controls and create sql statement to pass the updates to the db.
		/// Function is called recursivly so the string is stored within another variable
		/// </summary>
		/// <param name="parent"></param>
		///				
		StringBuilder sbUpdateSQL = new StringBuilder();
		void InterateControls(Control parent)
		{
			foreach (Control child in parent.Controls)
			{
				if (child.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox") && child.ID.IndexOf("txt_") == 0)
				{
					TextBox textbox = (TextBox)child;
					string[] arrTextboxID;
					Regex r = new Regex("_"); // Split apart id to reveal the needed values.
					arrTextboxID = r.Split(child.ID.ToString()) ;
					if (textbox.Text != "") // delete then insert new
					{
						sbUpdateSQL.Append("DELETE FROM FREIGHT_MATRIX_INTERNATIONAL WHERE IFM_ORIGIN_PORT='"+DBLibrary.ScrubSQLStringInput(arrTextboxID[2].ToString())+"' AND IFM_DESTINATION_GROUP='"+DBLibrary.ScrubSQLStringInput(arrTextboxID[1].ToString())+"';");
						sbUpdateSQL.Append("INSERT INTO FREIGHT_MATRIX_INTERNATIONAL (IFM_ORIGIN_PORT,IFM_DESTINATION_GROUP,IFM_PRICE) VALUES('"+DBLibrary.ScrubSQLStringInput(arrTextboxID[2].ToString())+"','"+DBLibrary.ScrubSQLStringInput(arrTextboxID[1].ToString())+"',"+DBLibrary.ScrubSQLStringInput(textbox.Text)+") ");
					}
					else
					{
						// delete any instances
						sbUpdateSQL.Append("DELETE FROM FREIGHT_MATRIX_INTERNATIONAL WHERE IFM_ORIGIN_PORT='"+DBLibrary.ScrubSQLStringInput(arrTextboxID[1].ToString())+"' AND IFM_DESTINATION_GROUP='"+DBLibrary.ScrubSQLStringInput(arrTextboxID[1].ToString())+"';");
					}					
				}
        
				if (child.Controls.Count > 0)
				{          
					InterateControls(child);          
				}
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
		  /*InterateControls(this);
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			SqlCommand cmd = new SqlCommand(sbUpdateSQL.ToString(),conn);
			cmd.ExecuteNonQuery();
			conn.Close();  */
			InterateControls(this);
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),sbUpdateSQL.ToString());
		}
	}
}
