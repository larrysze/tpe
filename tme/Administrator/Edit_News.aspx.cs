using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Xml;
using System.IO;
using TPE.Utility;

namespace localhost.Administrator
{
	/// <summary>
	/// CreatXml 
	/// </summary>
	public partial class Edit_News : System.Web.UI.Page
	{
		new public string ID
		{
			get 
			{
				return (Request.QueryString["Id"] == null) ? "" : Request.QueryString["Id"].ToString();
			}
		}
		
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// 
			// 
			if(!Page.IsPostBack)
			{
                if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "L"))
				{
					Response.Redirect("/default.aspx");
				}
				if (ID != "")
				{
					// load fields with data from existing xml file					
					string FilePath = Server.MapPath("/News/Current/"+ ID +".xml");
                    if (!File.Exists(FilePath))
                    {
                        this.Message.Text = "Error: News file is missing (" + FilePath + ")";
                    }
                    else
                    {
                        txtStory.Text = ReadXML(FilePath);
                        txtHeadline.Text = ReadHead(FilePath);
                        txtDate.Text = GetDate(ID);
                    }
				}
			}
		}
		public string GetDate(string id)
		{
			string sql = "Select convert(nvarchar,DATE,101) as DATE from NEWS where ID=@id;";
			Hashtable htParam = new Hashtable();
			htParam.Add("@id",id);
			using (DataTable dtNews = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(),sql,htParam))
			{
				return dtNews.Rows[0]["DATE"].ToString();
			}
		}


		#region 
		override protected void OnInit(EventArgs e)
		{
			//
			//
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// 
		/// 
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		public string ReadXML(string UrlToXmlFile)
		{
			// this string will concatenate all the xml values
			System.Text.StringBuilder sbXML = new System.Text.StringBuilder();
			XmlTextReader reader = null;

			// load the file from the URL
            try
            {
                reader = new XmlTextReader(UrlToXmlFile);

                //Read news based on those XML nodes
                object oName = reader.NameTable.Add("HeadLine");
                object dateline = reader.NameTable.Add("DateLine");
                object content = reader.NameTable.Add("p");
                object copyright = reader.NameTable.Add("CopyrightLine");
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        //Fill the page with news content
                        if (reader.Name.Equals(content))
                        {
                            sbXML.Append(reader.ReadString());
                        }

                    }
                }
            }
            finally
            {
                reader.Close();
            }
			return sbXML.ToString();
		}

		public string ReadHead(string UrlToXmlFile)
		{
			//only read headline node
			System.Text.StringBuilder sbXML = new System.Text.StringBuilder();
			XmlTextReader reader = null;
			reader = new XmlTextReader(UrlToXmlFile);
			object oName = reader.NameTable.Add("HeadLine");
			while (reader.Read())
			{
				if (reader.NodeType == XmlNodeType.Element)
				{
					if (reader.Name.Equals(oName))
					{
						sbXML.Append(reader.ReadString()).Append("");
					}
				}
			}
			reader.Close();
			return sbXML.ToString();
		}


		//read the templet of xml file,use the new data  instead of the placeholder(##XXX##).creat a new xml file write the data to the xml file,and save headline,date,filename to news.
		protected void UpdateCustomNews_Click(object sender, System.EventArgs e)
		{
			
			string filename="";
			if (!System.IO.File.Exists(Server.MapPath("../news/Current/")+"0000.xml"))
			{
				this.Message.Text="<font color=red>Error: Missing Xml Template File!!</font>";
			}
			else
			{
				try
				{
                    if (this.txtHeadline.Text.Trim().Length == 0 || this.txtStory.Text.Trim().Length == 0)//headline,story could not empty.
                    {
                        this.Message.Text = "<font color=red>You must input a headline and story</font>";
                    }
                    else
                    {
                        //creat a filename .the format is MMDDYYYYXXX where XXX is a random number
                        string name = System.DateTime.Now.Month.ToString() + System.DateTime.Now.Day.ToString() + System.DateTime.Now.Year.ToString();
                        filename = ID + ".xml";

                        string headline = this.txtHeadline.Text;
                        string date = this.txtDate.Text;
                        string body = this.FormatStory(this.txtStory.Text);//format story
                        System.Globalization.CultureInfo myculture = new System.Globalization.CultureInfo("en-US");
                        UpdateXMLFile(ID, headline, date, body);
                        UpdateDB(ID, this.FormatHeadline(headline));
                        if (this.Message.Text == "")
                            this.Message.Text = "update successfull!";

                        this.txtDate.Text = "";
                        this.txtHeadline.Text = "";
                        this.txtStory.Text = "";
                    }    
				}
				catch(Exception message)
				{
					this.Message.Text=message.ToString();
				}
			}
		}

		/// <summary>
		/// delete and recreate xml file with new title and/or body
		/// </summary>
		/// <param name="id"></param>
		/// <param name="headline"></param>
		/// <param name="date"></param>
		/// <param name="body"></param>
		private void UpdateXMLFile(string id,string headline,string date,string body)
		{
			string filename = id + ".xml";
			string path=Server.MapPath("/news/Current/");
			if (File.Exists(path + filename))
				File.Delete(path + filename);

			string temppath=path+"0000.xml";
			path=path+filename;            
			System.Text.StringBuilder sb=new System.Text.StringBuilder("");
			try 
			{
				// Create an instance of StreamReader to read from the templet of xml file.
				// The using statement also closes the StreamReader.
				string line=null;
				try 
				{
					using (System.IO.StreamReader sr = new System.IO.StreamReader(temppath)) 
					{					
						//load the xml file to string 
						while ((line=sr.ReadLine()) != null) 
						{
							sb.Append(line);
						}
						sr.Close();	
					}
				}
				catch 
				{
					throw new Exception("Missing xml template file");
				}

				//replace the placeholder(##XXX##) to the new data.
				sb.Replace("##filename##",filename+".xml");
				sb.Replace("##HeadLine##",headline);
				sb.Replace("##TransmissionId##",filename);
				sb.Replace("##DateLine##",date);
				sb.Replace("##content##",body);
				sb.Replace("##DateAndTime##",this.CreatLongDataTime());
				sb.Replace("##DateId##",this.CreatDataTime());
				sb.Replace("##FirstCreated##",this.CreatLongDataTime());
				System.IO.StreamWriter wr = System.IO.File.CreateText(path);
				wr.Write(sb.ToString());		
				wr.Close();
			}
			catch (Exception e) 
			{
				// Let the user know what went wrong.
				this.Message.Text=e.Message.ToString();
			}			                   
		}
		private string CreatDataTime()//format datatime just like 20050712
		{
			string datatime=System.DateTime.Now.Year.ToString()+System.DateTime.Now.Month.ToString()+System.DateTime.Now.Day.ToString();
			return datatime;
		}
		private string CreatLongDataTime()//format datatime just like 20050712T135431
		{
			string datatime=this.CreatDataTime()+"T"+System.DateTime.Now.Hour.ToString()+System.DateTime.Now.Minute.ToString()+System.DateTime.Now.Second.ToString();
			return datatime;
		}
/*
		private bool CheckFileNameFromDB(string name)//ensure the name is only one.
		{
			Hashtable htParams = new Hashtable();
			htParams.Add("@id",name);
			int n = (int) DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(),"select count(*) from news where NEWS_ID=@id",htParams);
			return (n==0);			
		}
		*/
		private void UpdateDB(string id,string title)//update the news table with new title
		{
			Hashtable htParams = new Hashtable();
			//htParams.Add("@title",title);	// 
			htParams.Add("@id",id);
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"update news set headline='"+ title + "' where id=@id",htParams);			
		}
		
		private bool CheckData(string date)
		{
			bool flag=true;
			System.DateTime d;
			try
			{
				d=System.DateTime.Parse(date);
			}
			catch
			{
				flag=false;
			}
			return flag;
		}		

		private string FormatCharacter(string ch)
		{
			ch=ch.Trim();
			ch=Server.HtmlEncode(ch);
			ch=ch.Replace("'","''");
			ch=ch.Replace(@"\",@"\\");
			ch=ch.Replace("<","&lt;");
			ch=ch.Replace(">","&gt;");          
			return ch;
		}
		private string FormatStory(string ch)
		{
			ch=this.FormatCharacter(ch);
			ch="<p>"+ch+"</p>";
			ch=ch.Replace("\r","</p><p>");
			return ch;
		}
		private string FormatHeadline(string ch)
		{
			ch=FormatCharacter(ch);
			return ch;
		}

		protected void CancelNews_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("/Public/Public_News.aspx");
		}
	}
}
