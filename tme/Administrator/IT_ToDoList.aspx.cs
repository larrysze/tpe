using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for IT_ToDoList.
	/// </summary>
	public partial class IT_ToDoList : System.Web.UI.Page
	{

	#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_ItemCommand);
			this.dg.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_PageIndexChanged);
			this.dg.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_CancelCommand);
			this.dg.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_EditCommand);
			this.dg.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dg_SortCommand);
			this.dg.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_UpdateCommand);
            this.dg.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_ItemDataBound);
		}
		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            Master.Width = "1200px";

            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "L"))
			{
				Response.Redirect("../default.aspx");
			}
			
			if (!IsPostBack)
			{
                
                this.BindAssignedTo(ddlNewAssignedTo);
                this.BindDeveloperList(ddlDeveloperFilter);                

				if (ViewState["SortExpression"] == null)
				{
					ViewState["SortExpression"] = "OPEN_DATE";
                    ViewState["SortDirection"] = "DESC";
				}
				BindData();                
			}
		}

		protected ICollection CreateDataSource()
		{			
			DataTable dt = new DataTable();

			Hashtable htParam = new Hashtable();
			switch (ddlStatusFilter.SelectedValue.ToString())
			{
				case "All":
					break;
				case "Open":
					htParam.Add("@Status",true);
					break;
				case "Closed":
					htParam.Add("@Status",false);
					break;
			}
            
            if (ddlDeveloperFilter.SelectedValue.ToString() != "0")
            {
                htParam.Add("@AssignedTo", ddlDeveloperFilter.SelectedValue.ToString());
            }
			if (htParam.Count > 0)
            {
				string Sql= 
                    "SELECT DO_ID, OPEN_DATE, CLOSE_DATE, DO_DESC, DO_SCREEN, DO_STATUS, DO_PRIORITY, " +
                    "(select PERS_FRST_NAME + ' ' + PERS_LAST_NAME FROM PERSON WHERE PERS_ID = OPENED_BY_ID) OPENED_BY, " +
                    "(select PERS_FRST_NAME + ' ' + PERS_LAST_NAME FROM PERSON WHERE PERS_ID = ASSIGNED_TO_ID) ASSIGNED_TO, " + 
                    "ASSIGNED_TO_ID " + 
                    "FROM TODOLIST WHERE ";
                if (htParam.ContainsKey("@Status"))
                {
                   Sql += "DO_STATUS = @Status ";
                   if (htParam.ContainsKey("@AssignedTo"))
                       Sql += "AND ";
                }
                if (htParam.ContainsKey("@AssignedTo"))
                {
                    Sql += "ASSIGNED_TO_ID=@AssignedTo";
                }
                dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(),Sql,htParam);
            }
			else
            {
				dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(),
                    "SELECT DO_ID, OPEN_DATE, CLOSE_DATE, DO_DESC, DO_SCREEN, DO_STATUS, DO_PRIORITY, " + 
                    "(select PERS_FRST_NAME + ' ' + PERS_LAST_NAME FROM PERSON WHERE PERS_ID = OPENED_BY_ID) OPENED_BY, " +
                    "(select PERS_FRST_NAME + ' ' + PERS_LAST_NAME FROM PERSON WHERE PERS_ID = ASSIGNED_TO_ID) ASSIGNED_TO, " + 
                    "ASSIGNED_TO_ID " + 
                    "FROM TODOLIST Order by DO_PRIORITY ASC");
			}
			DataView dv = new DataView(dt);
			if (ViewState["SortExpression"] != null)
			{
				dv.Sort=ViewState["SortExpression"].ToString();
				if (ViewState["SortDirection"] != null)
					dv.Sort += " " + ViewState["SortDirection"].ToString();
			}
			return dv;
		}

		private void BindData()
		{
			dg.DataSource = CreateDataSource();
			dg.DataBind();
		}

        protected void dg_ItemDataBound(object source, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            //SelectedValue='<%# DataBinder.Eval(Container.DataItem, "ASSIGNED_TO_ID").ToString() %>'>
            if (e.Item.ItemType == ListItemType.EditItem)
            {
                if (DataBinder.Eval(e.Item.DataItem, "ASSIGNED_TO_ID").ToString().Length > 0)
                    ((DropDownList)e.Item.FindControl("ddlAssignedTo")).SelectedValue = DataBinder.Eval(e.Item.DataItem, "ASSIGNED_TO_ID").ToString();
            }
        }

		private void dg_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			// We use CommandEventArgs e to get the row which is being clicked 
			// This also changes the DataGrid labels into Textboxes so user can edit them 
			dg.EditItemIndex = e.Item.ItemIndex; 
			// Always bind the data so the datagrid can be displayed. 
			BindData(); 
			btnAddNew.Enabled = false;
		}


		private void dg_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			// All we do in the cancel method is to assign '-1' to the datagrid editItemIndex 
			// Once the edititemindex is set to '-1' the datagrid returns back to its original condition 
			dg.EditItemIndex = -1; 
			BindData();
			btnAddNew.Enabled = true;
		}

		private void dg_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			string strScreenName = ((TextBox) e.Item.Cells[1].Controls[0]).Text;
            string strDescription = ((TextBox) e.Item.Cells[2].Controls[1]).Text;
			//string strPriority = ((DropDownList) e.Item.Cells[3].Controls[1]).SelectedValue;
            string strPriority = ((DropDownList)e.Item.Cells[3].FindControl("ddlPriority")).SelectedValue;
            string strAssignedTo = ((DropDownList) e.Item.Cells[8].FindControl("ddlAssignedTo")).SelectedValue;
			Hashtable htParams = new Hashtable();
			htParams.Add("@ScreenName",strScreenName);
			htParams.Add("@Description",strDescription);
			htParams.Add("@Priority",strPriority);
            htParams.Add("@AssignedTo", strAssignedTo);
				
			// updating existing item
			string strSqlUpdate = "Update TODOLIST Set DO_SCREEN=@ScreenName, DO_DESC=@Description, DO_PRIORITY=@Priority, DO_STATUS=@Status, ASSIGNED_TO_ID=@AssignedTo where DO_ID=@ID";			
			int id = Convert.ToInt32(dg.DataKeys[e.Item.ItemIndex]);

			htParams.Add("@ID",id.ToString());
			htParams.Add("@Status",ConvertStatusToBit(((DropDownList) e.Item.Cells[6].Controls[1]).SelectedValue));
			DBLibrary.ExecuteSqlWithoutScrub(Application["DBconn"].ToString(),strSqlUpdate,htParams);
		
			dg.EditItemIndex = -1; 
			BindData(); 
			btnAddNew.Enabled = true;
			SendEmailNotification("update", strScreenName, strDescription,strAssignedTo);
		}


		protected void btnAddNewItem_Click(object sender, System.EventArgs e)
		{
			btnAddNew.Enabled = false;
			pnlAddNew.Visible = true;            
		} 

		public string ConvertStatusToBit(string status)
		{
			if (status == "Open")
				return "1";
			else
				return "0";
		}
		public string ConvertStatus(object  status)
		{
			if (bool.Parse(status.ToString()))
				return "Open";
			else
				return "Closed";
		}

		protected void ddlStatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dg.EditItemIndex = -1;
            dg.CurrentPageIndex = 0;
			BindData();
		}

		private void dg_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName == "Close")
			{
				CloseStatus(e.Item.ItemIndex);
			}
		}

		private void CloseStatus(int index)
		{
			if (index >= 0)
			{
				string strSqlUpdate = "Update TODOLIST Set DO_STATUS=@Status, CLOSE_DATE=getDate() where DO_ID=@ID";				
				int id = Convert.ToInt32(dg.DataKeys[index]);

				Hashtable htParams = new Hashtable();
				htParams.Add("@ID",id.ToString());
				htParams.Add("@Status",ConvertStatusToBit("Closed"));
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),strSqlUpdate,htParams);
				dg.EditItemIndex = -1; 
				BindData(); 	
			}
		}

		private void InsertToDoItem(string Description, string ScreenName, string Priority, string AssignedToID)
		{
			Hashtable htParams = new Hashtable();
			htParams.Add("@Description",Description);
			htParams.Add("@ScreenName",ScreenName);
			htParams.Add("@Priority",Priority);
            htParams.Add("@OpenedByID",Session["ID"].ToString());
            htParams.Add("@AssignedToID", AssignedToID);
			string strSqlInsert = "Insert into TODOLIST (OPEN_DATE,CLOSE_DATE,DO_DESC,DO_SCREEN,DO_STATUS,DO_PRIORITY,OPENED_BY_ID,ASSIGNED_TO_ID) " +
                                  "Values (getDate(), null, @Description, @ScreenName, 1, @Priority, @OpenedByID, @AssignedToID)";
			DBLibrary.ExecuteSqlWithoutScrub(Application["DBconn"].ToString(),strSqlInsert,htParams);
			SendEmailNotification("insert", ScreenName, Description, AssignedToID);
            BindData();
		}
        
        private void BindAssignedTo(DropDownList ddl)
        {
            ddl.DataSource = DeveloperList();
            ddl.DataBind();
        }
        
        protected DataView DeveloperList()
        {
            string sql = "select PERSON_ID AS ASSIGNED_TO_ID, (Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME FROM PERSON where PERS_ID = PERSON_ID) ASSIGNED_TO from IT_DEVELOPERS";
            DataTable dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), sql);
            return dt.DefaultView;
        }

        private void BindDeveloperList(DropDownList ddl)
        {
            ddl.DataSource = DeveloperListFilter();
            ddl.DataBind();
        }

        protected DataView DeveloperListFilter()
        {
            string sql = "SELECT '0' AS PERSON_ID, 'ALL' AS PERSON_NAME UNION SELECT d.PERSON_ID, p.PERS_FRST_NAME + ' ' + p.PERS_LAST_NAME AS PERSON_NAME FROM dbo.PERSON AS p INNER JOIN dbo.IT_DEVELOPERS AS d ON p.PERS_ID = d.PERSON_ID";
            DataTable dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), sql);
            return dt.DefaultView;
        }

		private void dg_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			if (ViewState["SortDirection"] == null)
				ViewState.Add("SortDirection", "ASC");
			else
			{
				if (ViewState["SortDirection"].ToString() == "ASC")
				{
					ViewState["SortDirection"] = "DESC";
				}
				else
				{
					ViewState["SortDirection"] = "ASC";
				}
			}
			ViewState["SortExpression"] = e.SortExpression.ToString();
			dg.CurrentPageIndex = 0;
			BindData();
		}

		private void dg_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dg.CurrentPageIndex = e.NewPageIndex;
			BindData();
		}

		protected void btnInsert_Click(object sender, System.EventArgs e)
		{
			if (NewDescription.Text.ToString().Length > 0)
			{
				pnlAddNew.Visible = false;
				btnAddNew.Enabled = true;
				InsertToDoItem(NewDescription.Text,NewScreenName.Text,ddlNewPriority.SelectedValue.ToString(),ddlNewAssignedTo.SelectedValue.ToString());
				dg.CurrentPageIndex = 0;
				BindData();
			}
		}

		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			pnlAddNew.Visible = false;
			btnAddNew.Enabled = true;
			NewDescription.Text = "";
			NewScreenName.Text = "";
			ddlNewPriority.SelectedValue="1";
		}

		private void SendEmailNotification(string status, string screenName, string description, string AssignedToID)
		{
            string strEmailTo = HelperFunction.EmailPerson(Application["DBconn"].ToString(), AssignedToID);
            strEmailTo += ";" + Application["strEmailAdmin"].ToString();
            string body = "ScreenName: " + screenName + "\n" + "Description: " + description + "\n\n" + "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Administrator/IT_ToDoList.aspx";;
			if (status == "insert")
				body = "New To Do List item entered: \n\n" + body;
			else
				body = "To Do List item updated: \n\n" + body;

            if  (!bool.Parse(Application["IsDebug"].ToString()))
			    TPE.Utility.EmailLibrary.Send(this.Context,"ToDoList@theplasticsexchange.com",strEmailTo,"IT_ToDoList change",body);
		}

        protected void ddlDeveloperFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }
	}
}
