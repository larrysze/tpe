<%@ Page language="c#" Codebehind="Allocate_Lead.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.Allocate_Lead" ValidateRequest="false" MasterPageFile="~/MasterPages/Menu.Master" Title="Allocate Leade" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
<table align="center" width="780" cellpadding="0">
		<tr>
			    <td align="left" style="width: 531px; height: 19px;">
			        <span class="Content Bold Color2">1) Allocate To:</span>
				    <asp:DropDownList CssClass="InputForm" id="ddlAllocate" runat="server"></asp:DropDownList></td>
			    
		</tr>
		<tr>
			<td align="left" style="width: 531px">
				 <span class="Content Bold Color2">2) Select Users to send email:</span>
			</td>
		</tr>
		<tr>
			<td align="left" style="width: 531px; height: 37px">
				<table>
					<tr align="left">
						<td><span class="Content Color2 Bold">Name</span></td>
						<td align="center" ><span class="Content Color2 Bold">Email User</span></td>
						<td align="center"><span class="Content Color2 Bold">BCC Broker</span></td>
					</tr>
					<asp:PlaceHolder id="phAllocate" runat="server"></asp:PlaceHolder>
				</table>
			</td>
		</tr>
		<tr>
			<td align="left" style="width: 531px; height: 20px">
			<span class="Content Color2 Bold">3) Email Subject:</span>
				<asp:TextBox CssClass="InputForm" id="txtSubject" runat="server" Width="256px"></asp:TextBox></td>
		</tr>
		<tr>
			<td style="width: 531px">
				<asp:TextBox CssClass="InputForm" Width="770" id="txtBody" runat="server" TextMode="MultiLine" Rows="25" Columns="85"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td align="center" style="width: 770px">
				<asp:Button class="tpebutton" id="Button8" onclick="Submit_Allocate" runat="server" Text="Allocate"></asp:Button>
				<asp:Button class="tpebutton" id="Button9" onclick="Cancel_Allocate" runat="server" Text="Cancel"></asp:Button>
			</td>
		</tr>
	</table>

</asp:Content>