<%@ Page language="c#" Codebehind="ApproveCredit.aspx.cs" AutoEventWireup="false" Inherits="pes1.Administrator.ApproveCredit" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<script language="javascript">
function ValidateCtls()
			{
			if (document.Form1.txtPers_Fname.value=="")
				{
					alert ("Fill Person First Name.");
					document.Form1.txtConfrm.value=0;			
					document.Form1.txtPers_Fname.focus();
					return false;
				}
			if (document.Form1.txtPers_Lname.value=="")
				{
					alert ("Fill Person Last Name.");
					document.Form1.txtConfrm.value=0;			
					document.Form1.txtPers_Lname.focus();
					return false;
				}
			if (document.Form1.txtPers_Phone.value=="")
				{
					alert ("Enter Contact No.");
					document.Form1.txtConfrm.value=0;			
					document.Form1.txtPers_Phone.focus();
					return false;
				}
			if (document.Form1.txtPers_Fax.value=="")
				{
					alert ("Enter Fax No.");
					document.Form1.txtConfrm.value=0;			
					document.Form1.txtPers_Fax.focus();
					return false;
				}
			if (document.Form1.txtPers_Email.value=="")
				{
					alert ("Enter E-mail id of the Person.");
					document.Form1.txtConfrm.value=0;			
					document.Form1.txtPers_Email.focus();
					return false;
				}
			if (document.Form1.txtComp_Cname.value=="")
				{
					alert ("Enter Company Name.");
					document.Form1.txtConfrm.value=0;			
					document.Form1.txtComp_Cname.focus();
					return false;
				}
			
			if (document.Form1.txtComp_Address.value=="")
				{
					alert ("Fill Company Address.");
					document.Form1.txtConfrm.value=0;			
					document.Form1.txtComp_Address.focus();
					return false;
				}
			if (document.Form1.txtComp_City.value=="")
				{
					alert ("Fill City.");
					document.Form1.txtConfrm.value=0;			
					document.Form1.txtComp_City.focus();
					return false;
				}
			if (document.Form1.txtComp_Zip.value=="")
				{
					alert ("Enter Zip Code.");
					document.Form1.txtConfrm.value=0;			
					document.Form1.txtComp_Zip.focus();
					return false;
				}
				
			document.Form1.txtConfrm.value=1;
			}	
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TPE:TEMPLATE id="Template1" PageTitle="Inquire" Runat="Server"></TPE:TEMPLATE>
			<TABLE id="Table101" style="WIDTH: 588px; HEIGHT: 28px" height="28" width="584" border="1">
				<tr>
					<td align="center">
						<TABLE id="Table11" height="30" width="598" bgColor="activeborder" border="1">
							<TR>
								<TD align="center"><FONT face="Century gothic" size="4"><B>Credit Application</B> </FONT>
								</TD>
							</TR>
						</TABLE>
						<TABLE id="Table12" style="WIDTH: 584px; HEIGHT: 630px" height="630" width="584" border="1">
							<TR>
								<TD colSpan="2" width="279" height="22"><FONT face="Century gothic" color="red" size="3"><u><b>Personal 
												Information:</b></u></FONT>
								</TD>
							</TR>
							<TR>
								<TD align="left" colSpan="2" width="279"><FONT face="Century gothic" size="2"><B>First Name</B></FONT></TD>
								<TD align="left" colSpan="1" width="193"><FONT face="Century gothic" size="2"><B>Last Name</B></FONT></TD>
								<TD align="left" colSpan="1"><FONT face="Century gothic" size="2"><B>Title </B></FONT>
								</TD>
							</TR>
							<TR>
								<TD colSpan="2" width="279"><asp:textbox id="txtPers_Fname" runat="server" Width="272px" Height="24px" MaxLength="40"></asp:textbox></TD>
								<TD colSpan="1" width="193"><asp:textbox id="txtPers_Lname" runat="server" Width="144px" MaxLength="40"></asp:textbox></TD>
								<TD colSpan="1" width="150"><asp:textbox id="txtPers_Title" runat="server" Width="144px" MaxLength="20"></asp:textbox></TD>
							<TR>
								<TD align="left" colSpan="2" width="279"><FONT face="Century gothic" size="2"><B>Phone</B></FONT></TD>
								<TD align="left" colSpan="1" width="193"><FONT face="Century gothic" size="2"><B>Fax</B></FONT></TD>
								<TD align="left" colSpan="1" width="158"><FONT face="Century gothic" size="2"><B>Email</B></FONT></TD>
							</TR>
							<TR>
								<TD colSpan="2" width="279" height="27"><asp:textbox id="txtPers_Phone" runat="server" Width="272px" MaxLength="15"></asp:textbox></TD>
								<TD colSpan="1" width="193" height="27"><asp:textbox id="txtPers_Fax" runat="server" Width="144px" MaxLength="15"></asp:textbox></TD>
								<TD colSpan="1" width="150" height="27"><asp:textbox id="txtPers_Email" runat="server" Width="144px" MaxLength="30"></asp:textbox></TD>
							</TR>
							<TR>
								<TD colSpan="2" width="279"><FONT face="Century gothic" color="red" size="3"><u><b>Company 
												Information:</b></u></FONT>
								</TD>
								<TD colSpan="2" width="237"></TD>
							</TR>
							<TR>
								<TD align="left" colSpan="2" width="279"><FONT face="Century gothic" size="2"><B>Company 
											Name</B></FONT></TD>
							</TR>
							<TR>
								<TD colSpan="2" width="279"><asp:textbox id="txtComp_Cname" runat="server" Width="272px" Height="24px" MaxLength="50"></asp:textbox></TD>
							<TR>
							</TR>
							<TR>
								<TD align="left" colSpan="2" width="279"><FONT face="Century gothic" size="2"><B>Address</B></FONT></TD>
								<TD align="left" colSpan="2" width="289"><FONT face="Century gothic" size="2"><B>City</B></FONT></TD>
							</TR>
							<TR>
								<TD colSpan="2" width="279"><asp:textbox id="txtComp_Address" runat="server" Width="272px" Height="24px" MaxLength="70"></asp:textbox></TD>
								<TD colSpan="2" width="289"><asp:textbox id="txtComp_City" runat="server" Width="304px" MaxLength="50"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="left" colSpan="2" width="279"><FONT face="Century gothic" size="2"><B>Zip Code</B></FONT></TD>
								<TD align="left" colSpan="1" width="193"><FONT face="Century gothic" size="2"><B>State</B></FONT></TD>
								<TD align="left" colSpan="1" width="158"><FONT face="Century gothic" size="2"><B>Country</B></FONT></TD>
							</TR>
							<TR>
								<TD colSpan="2" width="279" height="8"><asp:textbox id="txtComp_Zip" runat="server" MaxLength="10" Width="273px"></asp:textbox></TD>
								<TD width="193" height="8"><asp:dropdownlist id="ddlComp_State" runat="server" Width="152px"></asp:dropdownlist></TD>
								<TD width="91" height="8"><asp:dropdownlist id="ddlComp_Country" runat="server" Width="144px"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD colSpan="2" width="279"><FONT face="Century gothic" color="red" size="3"><u><b>Bank 
												Information</b></u></FONT>
								</TD>
								<TD colSpan="2" width="289"><FONT face="Century gothic" color="red" size="3"><u><b>Trade 
												Information 1</b></u></FONT>
								</TD>
							</TR>
							<TR>
								<TD align="left" colSpan="2" width="279"><FONT face="Century gothic" size="2"><B>Bank Name</B></FONT></TD>
								<TD align="left" colSpan="2" width="289"><FONT face="Century gothic" size="2"><B>Company 
											Name</B></FONT></TD>
							</TR>
							<TR>
								<TD colSpan="2" width="279"><asp:textbox id="txtBank_Bname" runat="server" Width="272px" Height="24px" MaxLength="40"></asp:textbox></TD>
								<TD colSpan="2" width="289"><asp:textbox id="txtTrad1_Cname" runat="server" Width="304px" Height="24px" MaxLength="40"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="left" colSpan="2" width="279" height="19"><FONT face="Century gothic" size="2"><B>Contact</B></FONT></TD>
								<TD align="left" colSpan="2" width="289" height="19"><FONT face="Century gothic" size="2"><B>Contact</B></FONT></TD>
							</TR>
							<TR>
								<TD colSpan="2" width="279"><asp:textbox id="txtBank_Contact" runat="server" Width="272px" Height="24" MaxLength="15"></asp:textbox></TD>
								<TD colSpan="2" width="289"><asp:textbox id="txtTrad1_Contact" runat="server" Width="304px" Height="24px" MaxLength="15"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="left" colSpan="1" width="125" height="14"><FONT face="Century gothic" size="2"><B>Phone</B></FONT></TD>
								<TD align="left" colSpan="1" width="138" height="14"><FONT face="Century gothic" size="2"><B>Fax</B></FONT></TD>
								<TD align="left" colSpan="1" width="193" height="14"><FONT face="Century gothic" size="2"><B>Phone</B></FONT></TD>
								<TD align="left" colSpan="1" width="117" height="14"><FONT face="Century gothic" size="2"><B>Fax</B></FONT></TD>
							</TR>
							<TR>
								<TD colSpan="1" width="125" height="26"><asp:textbox id="txtBank_Phone" runat="server" Width="128px" MaxLength="15"></asp:textbox></TD>
								<TD colSpan="1" width="138" height="26"><asp:textbox id="txtBank_Fax" runat="server" Width="128px" MaxLength="20"></asp:textbox></TD>
								<TD colSpan="1" width="193" height="26"><asp:textbox id="txtTrad1_Phone" runat="server" Width="144px" MaxLength="15"></asp:textbox></TD>
								<TD colSpan="1" width="117" height="26"><asp:textbox id="txtTrad1_Fax" runat="server" Width="144px" MaxLength="15"></asp:textbox></TD>
							</TR>
							<TR>
								<TD colSpan="2" width="279"><FONT face="Century gothic" color="red" size="3"><u><b>Trade 
												Information 2</b></u></FONT>
								</TD>
								<TD colSpan="2" width="289"><FONT face="Century gothic" color="red" size="3"><u><b>Trade 
												Information 3</b></u></FONT>
								</TD>
							</TR>
							<TR>
								<TD align="left" colSpan="2" width="279"><FONT face="Century gothic" size="2"><B>Company 
											Name</B></FONT></TD>
								<TD align="left" colSpan="2" width="289"><FONT face="Century gothic" size="2"><B><FONT face="Century gothic" size="2"><B>Company 
													Name</B></FONT></B></FONT></TD>
							</TR>
							<TR>
								<TD colSpan="2" width="279"><asp:textbox id="txtTrad2_Cname" runat="server" Width="273px" Height="24px" MaxLength="40"></asp:textbox></TD>
								<TD colSpan="2" width="289"><asp:textbox id="txtTrad3_Cname" runat="server" Width="304px" Height="24px" MaxLength="40"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="left" colSpan="2" width="279"><FONT face="Century gothic" size="2"><B>Contact</B></FONT></TD>
								<TD align="left" colSpan="2" width="289"><FONT face="Century gothic" size="2"><B>Contact</B></FONT></TD>
							</TR>
							<TR>
								<TD colSpan="2" width="279"><asp:textbox id="txtTrad2_Contact" runat="server" Width="273px" Height="24px" MaxLength="15"></asp:textbox></TD>
								<TD colSpan="2" width="289"><asp:textbox id="txtTrad3_Contact" runat="server" Width="304px" Height="24px" MaxLength="15"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="left" colSpan="1" width="125"><FONT face="Century gothic" size="2"><B>Phone</B></FONT></TD>
								<TD align="left" colSpan="1" width="138"><FONT face="Century gothic" size="2"><B>Fax</B></FONT></TD>
								<TD align="left" colSpan="1" width="193"><FONT face="Century gothic" size="2"><B>Phone</B></FONT></TD>
								<TD align="left" colSpan="1" width="117"><FONT face="Century gothic" size="2"><B>Fax</B></FONT></TD>
							</TR>
							<TR>
								<TD colSpan="1" width="125"><asp:textbox id="txtTrad2_Phone" runat="server" Width="128px" MaxLength="15"></asp:textbox></TD>
								<TD colSpan="1" width="138"><asp:textbox id="txtTrad2_Fax" runat="server" Width="128px" MaxLength="15"></asp:textbox></TD>
								<TD colSpan="1" width="193"><asp:textbox id="txtTrad3_Phone" runat="server" Width="145px" MaxLength="15"></asp:textbox></TD>
								<TD colSpan="1" width="117"><asp:textbox id="txtTrad3_Fax" runat="server" Width="144px" MaxLength="15"></asp:textbox></TD>
							</TR>
						</TABLE>
						<TABLE id="Table2" borderColor="black" height="50" width="600" bgColor="activeborder" border="0">
							<TR>
								<TD align="center">
									<asp:Button id="btnDeny" runat="server" Width="90px" Text="Deny"></asp:Button>&nbsp;<INPUT id="btnApprove1" type="submit" value="Approve" runat="server" onclick="ValidateCtls()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<asp:TextBox id="txtConfrm" runat="server" Height="1px" Width="1px"></asp:TextBox>
								</TD>
							</TR>
						</TABLE>
					</td>
				</tr>
			</TABLE>
			<TPE:TEMPLATE id="Template2" Runat="server" Footer="true"></TPE:TEMPLATE>
		</form>
	</body>
</HTML>
