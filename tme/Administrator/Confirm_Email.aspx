<%@ Register TagPrefix="TPE" TagName="Web_Box" 
Src="/include/Box_Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" 
%>
<%@ Page language="c#" Codebehind="Confirm_Email.aspx.cs" 
AutoEventWireup="false" Inherits="localhost.Administrator.offers_mail" 
responseEncoding="unicode" ValidateRequest=false%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<style type="text/css">.border1 { BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; FONT-SIZE: 12px; BORDER-LEFT: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid; FONT-FAMILY: Arial, Helvetica, sans-serif }
		</style>
	</HEAD>
	<body ms_positioning="GridLayout">
		<form id="Confirm_Mail" method="post" runat="server">
			<TPE:TEMPLATE id="cm" Runat="server" PageTitle="Confirm &#13;&#10;Email"></TPE:TEMPLATE>
			<table height="200" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<tr>
					<td align="center" colSpan="3"><asp:label id="errorMessage" runat="server"></asp:label></td>
				</tr>
				<tr>
					<td class="PageHeader" align="center" colSpan="3">Send Email
					</td>
				</tr>
				<tr>
					<td vAlign="top" align="left">&nbsp;&nbsp;<b>Subject:</b>
					</td>
					<td><asp:textbox id="lblBidOffers" runat="server"></asp:textbox></td>
					<td>&nbsp;
						<asp:Button id="exchange" runat="server" Width="337px" Text="Show &#13;&#10;International"></asp:Button></td>
				</tr>
				<tr>
					<td vAlign="top" align="left">&nbsp;&nbsp;<b>Body:</b>
					</td>
					<td align="left" colSpan="2" height="160"><asp:textbox id="txtBody" runat="server" Width="550px" TextMode="MultiLine" Height="150px"></asp:textbox></td>
				</tr>
				<tr>
					<td vAlign="top" align="left" colSpan="3"><br>
						<b>Select Items to Send:</b>
					</td>
				</tr>
				<tr>
					<td vAlign="top" align="left" colSpan="3"><FONT face="����"></FONT><br>
						<hr>
						<asp:datagrid id="mainGrid" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="0"
							BorderWidth="0px" ShowHeader="False" HorizontalAlign="Center">
							<Columns>
								<asp:TemplateColumn>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:Label runat="server" ID="Label1" Visible="False"></asp:Label>
										<asp:Label runat="server" ID="title"></asp:Label>
										<asp:DataGrid id="mygrid" runat="server" ShowHeader="true" Width="550" AutoGenerateColumns="False"
											BorderWidth="1" CellPadding="0" CellSpacing="0" OnItemDataBound="KeepRunningSubGrid" HorizontalAlign="Center"
											GridLines="Horizontal" BorderColor="#000000">
											<HeaderStyle HorizontalAlign="Center" ForeColor="#ffffff" BackColor="#BEB8A2" Height="20"></HeaderStyle>
											<AlternatingItemStyle BackColor="#D9D7D3"></AlternatingItemStyle>
											<Columns>
												<asp:TemplateColumn>
													<ItemTemplate>
														<asp:CheckBox id="mycheck" runat="server" Checked="true"></asp:CheckBox>
														<asp:Label runat="server" ID="mylab" Visible="false"></asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn ItemStyle-Font-Size="10" ItemStyle-Font-Names="arial,verdana" ItemStyle-HorizontalAlign="Center"
													HeaderText="Inquire" ItemStyle-Wrap="false" HeaderStyle-ForeColor="#ffffff" ItemStyle-Width="80" />
												<asp:BoundColumn ItemStyle-Font-Size="10" ItemStyle-Font-Names="arial,verdana" ItemStyle-HorizontalAlign="left"
													HeaderText="Size" ItemStyle-Wrap="false" DataField="size" HeaderStyle-ForeColor="#ffffff" />
												<asp:BoundColumn ItemStyle-Font-Size="10" ItemStyle-HorizontalAlign="left" HeaderText="Product" ItemStyle-Wrap="false"
													DataField="product" HeaderStyle-ForeColor="#ffffff" />
												<asp:BoundColumn ItemStyle-Font-Size="10" ItemStyle-HorizontalAlign="right" HeaderText="Melt" ItemStyle-Wrap="false"
													DataField="melt" HeaderStyle-ForeColor="#ffffff" ItemStyle-Width="80" />
												<asp:BoundColumn ItemStyle-Font-Size="10" HeaderText="Density" ItemStyle-HorizontalAlign="Center"
													ItemStyle-Wrap="false" DataField="density" HeaderStyle-ForeColor="#ffffff" ItemStyle-Width="60" />
												<asp:BoundColumn ItemStyle-Font-Size="10" HeaderText="Adds" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false"
													DataField="adds" HeaderStyle-ForeColor="#ffffff" ItemStyle-Width="80" />
												<asp:TemplateColumn ItemStyle-Width="80" HeaderText="Price" ItemStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="#ffffff">
													<ItemTemplate>
														<asp:textbox id="price" Width="70" BorderWidth="1" runat="server" BorderColor="#000000"></asp:textbox>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn ItemStyle-Font-Size="10" HeaderText="FOB" ItemStyle-HorizontalAlign="right" ItemStyle-Wrap="false"
													DataField="fob" HeaderStyle-ForeColor="#ffffff" />
											</Columns>
										</asp:DataGrid>
										<asp:Label runat="server" ID="Label3"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td vAlign="top" align="left">&nbsp;&nbsp;<b>Salutation:</b>
					</td>
					<td align="left" colSpan="3"><asp:textbox id="SecTextBox" runat="server" Width="550px" TextMode="MultiLine" Height="125px"></asp:textbox></td>
					</TD></tr>
				<tr>
					<td vAlign="top" align="center" colSpan="3"><br>
						<asp:button id="sentMail" runat="server" Text="Send Mail" CausesValidation="False"></asp:button></td>
				</tr>
			</table>
			<TPE:TEMPLATE id="cm2" Runat="server" Footer="true"></TPE:TEMPLATE></form>
	</body>
</HTML>
