using System;
using System.Configuration;
using System.Collections;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Data.SqlClient;



namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Demo_Leads.
	/// </summary>
	public class Demo_Leads : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblPage;
		protected System.Web.UI.WebControls.Label lblSort;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.Panel pnEmail_Offer;
		protected System.Web.UI.WebControls.DropDownList ddlResinFilter;
		protected System.Web.UI.WebControls.DropDownList ddlTypeFilter;
		protected System.Web.UI.WebControls.Label lblAccess;
		protected System.Web.UI.WebControls.DropDownList ddlAccess;
		protected System.Web.UI.WebControls.TextBox txtSearch;
		protected System.Web.UI.WebControls.Button Button2;
		protected System.Web.UI.WebControls.Button Button3;
		protected System.Web.UI.WebControls.Button Button4;
		protected System.Web.UI.WebControls.Button btnAllocate;
		protected System.Web.UI.WebControls.Button btnLast_Page;
		protected System.Web.UI.WebControls.Button btnNext_Page;
		protected System.Web.UI.WebControls.LinkButton Linkbutton1;
		protected System.Web.UI.WebControls.LinkButton Linkbutton2;
		protected System.Web.UI.WebControls.LinkButton Linkbutton3;
		protected System.Web.UI.WebControls.LinkButton Linkbutton4;
		protected System.Web.UI.WebControls.LinkButton Linkbutton5;
		protected System.Web.UI.WebControls.PlaceHolder phContent;
		protected System.Web.UI.WebControls.Button Button5;
		protected System.Web.UI.WebControls.Button Button6;
		protected System.Web.UI.WebControls.Button Button7;
		protected System.Web.UI.WebControls.Button btnLast_Page2;
		protected System.Web.UI.WebControls.Button btnNext_Page2;
		protected System.Web.UI.WebControls.Panel pnMain;
		protected System.Web.UI.WebControls.DropDownList ddlAllocate;
		protected System.Web.UI.WebControls.PlaceHolder phAllocate;
		protected System.Web.UI.WebControls.Button Button8;
		protected System.Web.UI.WebControls.Button Button9;
		protected System.Web.UI.WebControls.Panel pnAllocate;

		/************************************************************************
		* 1. File Name : Demo_Leads.aspx                                        *
		* 2. Description : List of the the people who have signed               *
		*                          as demo users                                *
		* 4. Modification Log:                                                  *
		*                                                                       *
		* Ver No. Date Author Modification                                      *
		*  ----------------------------------------------------------------     *
		*      1.00      11-5-2003       Zach             First Baseline (as .Net*
		*      1.01      28-07-2004       Matthieu        merge demo and production tables*
		************************************************************************/


		// objects kept outside to keep them within the scope
		//--SqlConnection conn;
		SqlConnection connGenius;
		SqlCommand cmdContent;
		SqlDataReader dtrContent;
		string strOrderBy; // used for sorting funtions
		int iPage; // the dbase page
		public void Page_Load(object sender, EventArgs e)
		{

			// kick user out if they aren't allowed
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B"))
			{
				Response.Redirect("/default.aspx");
			}
			// opening the connections

			//--conn = new SqlConnection(Application["DBConnDemo"].ToString());
			//--conn.Open();

			connGenius = new SqlConnection(Application["DBconn"].ToString());
			connGenius.Open();

			if (Session["Id"].ToString() != "2")
			{

				btnAllocate.Visible = false;
			}

			// we have offers to be emailed then display dialog box on the top

			if (Session["strEmailOffer_IDS"]!=null)
			{
				pnEmail_Offer.Visible = true;
			}
			// create content

			cmdContent= new SqlCommand("spDemo_Leads", connGenius); //--
			cmdContent.CommandType = CommandType.StoredProcedure;
			if (!IsPostBack)
			{
				iPage = 0; // default db page to one
				lblSort.Text ="PERS_ID DESC"; // the lbl for the default sort

				SqlCommand cmdContract;
				SqlDataReader dtrContract;
				// opening contracts list
				cmdContract = new SqlCommand("SELECT CONT_LABL,CONT_ID FROM CONTRACT ORDER BY CONT_ORDR",connGenius);
				dtrContract = cmdContract.ExecuteReader();
				//binding company dd list
				ddlResinFilter.Items.Add ( new ListItem ("Show All","All" ) );
				while (dtrContract.Read())
				{
					ddlResinFilter.Items.Add ( new ListItem ((string) dtrContract["CONT_LABL"],dtrContract["CONT_ID"].ToString() ) );
				}

				dtrContract.Close();
				// binding allocation
				SqlCommand cmdAllocate;
				SqlDataReader dtrAllocate;
				// selecting admins
				cmdAllocate = new SqlCommand("SELECT NAME=PERS_FRST_NAME+' '+PERS_LAST_NAME, PERS_ID FROM PERSON WHERE PERS_ID<>2 AND (PERS_TYPE='B' OR PERS_TYPE='A') ORDER BY NAME ASC",connGenius);
				dtrAllocate = cmdAllocate.ExecuteReader();
				//binding the allocate dd list
				while (dtrAllocate.Read())
				{
					ddlAllocate.Items.Add ( new ListItem ((string) dtrAllocate["Name"],dtrAllocate["PERS_ID"].ToString() ) );
				}
				dtrAllocate.Close();


				// binding access
				SqlCommand cmdAccess;
				SqlDataReader dtrAccess;
				// selecting admins
				cmdAccess = new SqlCommand("SELECT NAME=PERS_FRST_NAME+' '+PERS_LAST_NAME, PERS_ID FROM PERSON WHERE PERS_ID<>2 AND (PERS_TYPE='B' OR PERS_TYPE='A') ORDER BY NAME ASC",connGenius);
				dtrAccess = cmdAccess.ExecuteReader();
				//binding the access dd list
				if (Session["Id"].ToString() != "2" && Request.QueryString["State"] ==null )
				{ // every admin but mike should default to their personal list
					ddlAccess.Items.Add ( new ListItem ("Show Mine",Session["Id"].ToString() ) );
				}
				ddlAccess.Items.Add ( new ListItem ("Show All","*" ) );
				while (dtrAccess.Read())
				{
					ddlAccess.Items.Add ( new ListItem ((string) dtrAccess["Name"],dtrAccess["PERS_ID"].ToString() ) );
				}
				ddlAccess.Items.Add(new ListItem("Unassigned","-1"));
				dtrAccess.Close();

				if ((string)Session["Typ"] == "A")
				{
					// only admins should have access to this box
					lblAccess.Visible = true;
					ddlAccess.Visible = true;
				}

				Bind_Content();
			}

		}
		// <summary>
		//  This function clears the session varible that contains the spot offers
		//  being sent.
		public void Cancel_Sending_Offers(object sender, EventArgs e)
		{
			Session["strEmailOffer_IDS"]=null;
			Session["strCompId"]=null;
			Session["strExport"] =null;
			pnEmail_Offer.Visible = false;
			Bind_Content();
		}


		// <summary>
		//  Function is called whenever the page loads.
		//  This function must be called on ever load otherwise the page will be practically empty
		// </summary>
		public void Bind_Content()
		{
			// limiting the leads to those allocated for the current user
			if (Session["Typ"].ToString() != "A")
			{
				cmdContent.Parameters.Add("@Id", Session["Id"]);
			}
			// filter by Type of leads whether they are suppliers of producers
			if (ddlTypeFilter.SelectedItem.Value != "A")
			{
				cmdContent.Parameters.Add("@Type", ddlTypeFilter.SelectedItem.Value);
			}
			// filter by Type of access the brokers have
			if (ddlAccess.SelectedItem.Value != "*")
			{
				cmdContent.Parameters.Add("@Access", ddlAccess.SelectedItem.Value);
			}
			// filter by state if at varible is passed
			if (Request.QueryString["State"] != null)
			{
				cmdContent.Parameters.Add("@State", Request.QueryString["State"].ToString());
			}
			// filter by Type of leads whether they are suppliers of producers
			if (ddlResinFilter.SelectedItem.Value != "All")
			{
				cmdContent.Parameters.Add("@resin_filter",Convert.ToInt32(ddlResinFilter.SelectedItem.Value)-1);
			}
			if (!txtSearch.Text.Equals(""))
			{
				cmdContent.Parameters.Add("@Search",txtSearch.Text);
			}
			dtrContent = cmdContent.ExecuteReader();
			lblPage.Text = iPage.ToString();

			if (Int32.Parse (lblPage.Text) > 0)
			{
				btnLast_Page.Visible = true;
				btnLast_Page2.Visible = true;
			}
			else
			{
				btnLast_Page.Visible = false;
				btnLast_Page2.Visible = false;
			}

			string strOutput;
			bool AltColor;
			AltColor = true;
			int iCount; // the counter for how many records are displayed on a page
			iCount = 0;
			// if the page isn't 1 then it  advances the reader to the correct record

			for (int k=1;k<((iPage)*100);k++)
			{
				dtrContent.Read();
			}


			while (dtrContent.Read() && iCount < 100)
			{
				//Response.Write(iCount.ToString());
				// swaps background color
				if (AltColor)
				{
					AltColor = false;
				}
				else
				{
					AltColor = true;
				}
				if (AltColor)
				{
					strOutput = "<tr bgcolor=white>";
				}
				else
				{
					strOutput = "<tr bgcolor=#E8E8E8>";
				}


				strOutput+= "<td valign=\"top\"><input name=\"CB_"+dtrContent["PERS_ID"]+"\" type=\"checkbox\"></td>";
				strOutput+="<td valign=\"top\">&nbsp<a href=\"../administrator/Update_Contact_Info.aspx?Id="+dtrContent["PERS_ID"]+"\">Update</a>&nbsp</td>";
				//strOutput+="<td valign=\"top\">&nbsp<a href=\"../administrator/MB_Resins.aspx?Pref="+dtrContent["PERS_PREF"]+"\">Resins</a>&nbsp</td>";
				strOutput+="<td valign=\"top\">&nbsp<a onmouseover=this.style.cursor='hand' onclick=javascript:Resin_Popup("+dtrContent["PERS_PREF"]+")><u>Resins</u></a>&nbsp</td>";
				strOutput+="<td valign=\"top\">&nbsp";
				if ((bool)dtrContent["Email_ENBL"])
				{
					//if (dtrContent["Email_ENBL"].ToString().Equals("0")){
					strOutput+="<img src=\"../images/email/email_enbl.bmp\" border=\"0\">&nbsp";
				}

				// adds link in case there is a comment
				if (dtrContent["PERS_CMNT"].ToString() != "")
				{
					strOutput+="<a href='../Administrator/MB_Lead_Comment.aspx?Id="+dtrContent["PERS_ID"]+"'>"+dtrContent["PERS_FRST_NAME"]+ " " + dtrContent["PERS_LAST_NAME"]+"</a>&nbsp</td><td valign=\"top\">";
				}
				else
				{
					strOutput+=dtrContent["PERS_FRST_NAME"]+ " " + dtrContent["PERS_LAST_NAME"]+"&nbsp</td><td valign=\"top\">";
				}


				if(dtrContent["PERS_STAT"]!= null && dtrContent["PERS_STAT"].ToString() != "" )
				{
					if (Convert.ToInt16(dtrContent["PERS_STAT"]) > 0)
					{
						switch (Convert.ToInt16(dtrContent["PERS_STAT"]))
						{
							case 1:
								strOutput+="<a style='color:red' href='../Administrator/Contact_Comment.aspx?ID="+dtrContent["PERS_ID"]+"'>"+dtrContent["PERS_TITL"]+"</a></td>";
								break;
							case 2:
								strOutput+="<a style='color:green' href='../Administrator/Contact_Comment.aspx?ID="+dtrContent["PERS_ID"]+"'>"+dtrContent["PERS_TITL"]+"</a></td>";
								break;
							case 3:
								strOutput+="<a style='color:orange' href='../Administrator/Contact_Comment.aspx?ID="+dtrContent["PERS_ID"]+"'>"+dtrContent["PERS_TITL"]+"</a></td>";
								break;
							case 4:
								strOutput+="<a style='color:blue' href='../Administrator/Contact_Comment.aspx?ID="+dtrContent["PERS_ID"]+"'>"+dtrContent["PERS_TITL"]+"</a></td>";
								break;
						}
					}
					else
					{
						strOutput+="<a style='color:black' href='../Administrator/Contact_Comment.aspx?ID="+dtrContent["PERS_ID"]+"'>"+dtrContent["PERS_TITL"]+"</a></td>";
					}
				}


				strOutput+="<td valign=\"top\">&nbsp"+dtrContent["COMP_NAME"]+"&nbsp("+dtrContent["PERS_TYPE"].ToString()+")</td>";

				strOutput+="<td valign=\"top\">&nbsp"+dtrContent["PERS_PHON"]+"</td>";
				strOutput+="<td valign=\"top\"><a href=\"mailto:"+dtrContent["PERS_MAIL"]+"\">"+dtrContent["PERS_MAIL"]+"</a>&nbsp</td>";
				strOutput+="<td valign=\"top\">&nbsp"+dtrContent["VARDATE"]+"&nbsp</td>";
				// only daboss account see the access column
				if (Session["Id"].ToString() == "2")
				{
					strOutput+="<td valign=\"top\">"+dtrContent["VARACES"]+"<td></tr>";
				}
				phContent.Controls.Add (new LiteralControl(strOutput));
				iCount ++; // increment counter
			}

			if (dtrContent.Read())
			{
				// there are still remaining items to be displayed
				// allow next page to be displayed
				btnNext_Page.Visible = true;
				btnNext_Page2.Visible = true;
			}
			else
			{
				btnNext_Page.Visible = false;
				btnNext_Page2.Visible = false;
			}
			// close stuff down
			dtrContent.Close();
			//--conn.Close();
			connGenius.Close();

		}

		// <summary>
		//  Function deletes the selected items in the list
		// </summary>
		public void Click_Delete(object sender, EventArgs e)
		{
			SqlCommand cmdDelete;
			foreach (string item in Request.Form)
			{
				if (item.Substring(0,3) == "CB_")
				{
					cmdDelete= new SqlCommand("spDelContact", connGenius);
					cmdDelete.CommandType = CommandType.StoredProcedure;
					cmdDelete.Parameters.Add("@PersId",item.Substring(3,item.Length -3));
					cmdDelete.ExecuteNonQuery();
				}
			}
			// rebuilds the list
			Bind_Content();
		}
		// <summary>
		//  Returns the user from the allocation screen to the main screen.
		// </summary>
		public void Cancel_Allocate(object sender, EventArgs e)
		{
			Bind_Content();
			pnMain.Visible =  true;
			pnAllocate.Visible = false;
		}
		// <summary>
		//  Updates then allocated broker in the DB.
		//  It then email the user and/or the broker with a welcome message
		// </summary>

		public void Submit_Allocate(object sender, EventArgs e)
		{
			
			SqlConnection conn;
			
				
			SqlCommand cmdUpdateAllocation;
			SqlCommand cmdEmail;
			SqlDataReader dtrEmail;
			SqlCommand cmdBrokerEmail;
			SqlDataReader dtrBrokerEmail;
			cmdBrokerEmail = new SqlCommand("Select PERS_FRST_NAME+ ' ' + PERS_LAST_NAME AS NAME, PERS_MAIL from PERSON WHERE PERS_ID="+ddlAllocate.SelectedItem.Value,connGenius);
			dtrBrokerEmail = cmdBrokerEmail.ExecuteReader();
			dtrBrokerEmail.Read();
			// flags that determine whether to email users
			bool bToUser;
			bool bCCBroker;
			string MailStr; // text of email body
			// loop through all objects on the form looking for the hidden id
			foreach (string item in Request.Form)
			{
				conn = new SqlConnection(Application["DBconn"].ToString());
				conn.Open();
				if (item.Substring(0,3) == "Id_")
				{ // then it found a user that needs to be updated
					// update the users allocation
					cmdUpdateAllocation = new SqlCommand("UPDATE PERSON SET PERS_ACES="+ddlAllocate.SelectedItem.Value+" WHERE PERS_ID="+item.Substring(3,item.Length -3),conn);
					cmdUpdateAllocation.ExecuteNonQuery();
					// gets broker info needed for the emails
					cmdEmail = new SqlCommand("Select * From PERSON WHERE PERS_ID="+item.Substring(3,item.Length -3),conn);
					dtrEmail = cmdEmail.ExecuteReader();
					dtrEmail.Read();
					// defaults boolean flags to no
					bToUser = false;
					bCCBroker = false;
					MailStr ="";

					// determines if the user has checked the boxes to indicate that the user/brokers should recieve mail
					foreach (string Email in Request.Form)
					{
						if (Email.Substring(0,3) == "To_")
						{
							if (item.Substring(3,item.Length -3).Equals(Email.Substring(3,Email.Length -3)))
							{
								bToUser = true;
							}
						}
						if (Email.Substring(0,3) == "CC_")
						{
							if (item.Substring(3,item.Length -3).Equals(Email.Substring(3,Email.Length -3)))
							{
								bCCBroker = true;
							}
						}
					}
					// begin building mail body
					if (bToUser)
					{
						StringBuilder sbEmailText = new StringBuilder();
						// removed because it causes hotmail to not display this message
						//sbEmailText.Append("<HTML><HEAD><TITLE>TPE</TITLE>");
						//sbEmailText.Append("<BODY bgColor=#ffffff>");
						sbEmailText.Append("<table border=0 cellspacing=0 cellpadding=0>");
						sbEmailText.Append("<tr><td><img src=https://" + ConfigurationSettings.AppSettings["SecureDomainName"].ToString() + "//images/email/tpelogo.gif></td>");
						sbEmailText.Append("<td>");
						sbEmailText.Append("<font face=arial black size=4 color=Black>The</font><font face=arial black size=4 color='#4A4AFE'>Plastics</font><font face=arial black size=4 color=Black>Exchange</font><font face=arial black size=4 color='#4A4AFE'>.</font><font face=arial black size=4 color=Black>com</font>");
						sbEmailText.Append("</td>");
						sbEmailText.Append("</tr></table>");
						sbEmailText.Append("<br>");
						sbEmailText.Append("Dear "+dtrEmail["PERS_FRST_NAME"].ToString()+",<br>");
						sbEmailText.Append("<br>");

						// create text for mail to be sent to user
						sbEmailText.Append("Thank you for registering with <a href=\"https://" + ConfigurationSettings.AppSettings["SecureDomainName"].ToString() + "\">The Plastics Exchange</a>. We are all about resin and we know...price matters! <BR><BR> ");
						sbEmailText.Append("We are interested in working with " +dtrEmail["COMP_NAME"].ToString() +", and hope you will find The Plastics Exchange to be your one stop for market intelligence and the place you source commodity grade resin. ");
						// since the mail is from mike.  there needs to be slightly differnet verbage when he is assigned
						if (dtrBrokerEmail["PERS_MAIL"].ToString().ToLower() == "mike@theplasticexchange.com")
						{
							sbEmailText.Append("If you have any immediate resin needs or if you ever have any questions please call me at 1.800.851.7303 or reply to this email.  <BR><BR>");
						}
						else
						{
							sbEmailText.Append("If you have any immediate resin needs or if you ever have any questions please call "+dtrBrokerEmail["Name"]+ " at 1.800.851.7303 or reply to this email.  <BR><BR>");
						}


						sbEmailText.Append("In addition to the prime commodity grade resin on the site, we also have excellent access to a full range of domestic wide spec as well as international prime resin for import/export.<BR><BR>");
						sbEmailText.Append("Crazy resin market, huh?!?  Feel free to visit the website to view our historic price charts, daily industry news and login to see our prime and wide-spec markets as they move around quite often!<BR><BR>");

						sbEmailText.Append("<B>Email: "+dtrEmail["PERS_MAIL"].ToString()+"</B><br>");
						sbEmailText.Append("<B>Password: "+Crypto.Decrypt(dtrEmail["PERS_PSWD"].ToString())+ "</B><br><br>");
						sbEmailText.Append("Sincerely,<BR><BR>");
						sbEmailText.Append("Michael A. Greenberg, CEO<BR>");
						sbEmailText.Append("The Plastics Exchange<BR>");
						sbEmailText.Append("Tel: 312.202.0002<BR>");
						sbEmailText.Append("Fax: 312.202.0174<BR>");
						sbEmailText.Append("<BR><BR></BODY></HTML>");


						/*
						old text
							MailStr += "<HTML><HEAD><TITLE>TPE</TITLE>";
							MailStr +="<BODY bgColor=#ffffff>";
							MailStr += "<table border=0 cellspacing=0 cellpadding=0>";
							MailStr += "<tr><td><img src=https://" + ConfigurationSettings.AppSettings["SecureDomainName"].ToString() + "//images/email/tpelogo.gif></td>";
							MailStr +="<td>";
							MailStr += "<font face=arial black size=4 color=Black>The</font><font face=arial black size=4 color=red>Plastics</font><font face=arial black size=4 color=Black>Exchange</font><font face=arial black size=4 color=red>.</font><font face=arial black size=4 color=Black>com</font>";
							MailStr += "</td>";
							MailStr += "</tr></table>";
							MailStr += "<br>";
							MailStr +="Dear "+dtrEmail["PERS_FRST_NAME"].ToString()+",<br>";
							MailStr += "<br>";
							MailStr += "Thank you for registering with <a href=http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + ">The Plastics Exchange</a>. We are all about resin and we know...price matters!<br><br>";

									MailStr += "We are interested in working with " +dtrEmail["COMP_NAME"] +", Please accept a call from me,  I will explain a bit more about The Plastics Exchange and see how we might best serve your company's needs.<br><br>";
							} else {
									MailStr += "We are interested in working with " +dtrEmail["COMP_NAME"] +", Please accept contact from "+dtrBrokerEmail["Name"]+",  he will explain a bit more about The Plastics Exchange and see how we might best serve your company's needs.<br><br>";
							}
							MailStr += "In the meantime... or if you ever have any questions concerning the use of the site, please call us at 1.800.851.7303 or reply to this email. In addition to the prime commodity grade resin on the site, we also have excellent access to a full range of domestic prime and wide spec resin.<br><br>";
							MailStr += "Crazy resin market, huh?!? Feel free to visit the website to view our price charts, industry news and login to see our prime and wide-spec markets as they move around quite often; then let us know when you are ready to source resin on The Plastics Exchange.<br><br>";
							MailStr += "<br>";
							MailStr += "Sincerely,<br>";
							MailStr += "<br>";
							MailStr += "Michael A. Greenberg, CEO<BR>The Plastics Exchange<br>Tel: 312.202.0002<BR>Fax: 312.202.0174<br>";
							MailStr += "<br><b><hr>";

							MailStr += " "+dtrEmail["PERS_MAIL"].ToString()+"";
							MailStr += "<font face=arial size=2 color=black></b>";
							MailStr += "</BODY></HTML>";
							*/
						// now to create mail message
						MailMessage mail = new MailMessage();
						mail.From = Application["strEmailOwner"].ToString();
						// if the flags have been set to true the users have elected to recieve the mails

						//mail.To = Application["strEmailAdmin"].ToString();
						mail.To = dtrEmail["PERS_MAIL"].ToString();
						mail.Subject = "Welcome to the Plastics Exchange";
						
						mail.Body = sbEmailText.ToString();
						mail.BodyFormat = MailFormat.Html;
						//SmtpMail.SmtpServer = "localhost";
						TPE.Utility.EmailLibrary.Send(Context,mail);

					}




					// now send email to mike and the broker

					StringBuilder sbBrokerMail = new StringBuilder();

					sbBrokerMail.Append("The following lead has been allocated to "+dtrBrokerEmail["Name"].ToString());
					sbBrokerMail.Append("<br><b>First Name: "+dtrEmail["PERS_FRST_NAME"].ToString()+"</b>");
					sbBrokerMail.Append("<br><b>Last Name: "+dtrEmail["PERS_LAST_NAME"].ToString()+"</b>");
					sbBrokerMail.Append("<br><b>Title: "+dtrEmail["PERS_TITL"].ToString()+"</b>");
					sbBrokerMail.Append("<br><b>Company Name: "+dtrEmail["COMP_NAME"].ToString()+"</b>");
					sbBrokerMail.Append("<br><b>Phone: "+dtrEmail["PERS_PHON"].ToString()+"</b>");
					sbBrokerMail.Append("<br><b>Email: "+dtrEmail["PERS_MAIL"].ToString() +"</b>");
					//sbBrokerMail.Append("<br><b>User Name: "+dtrEmail["PERS_LOGN"].ToString() +"</b>");
					sbBrokerMail.Append("<br><b>Password: " + Crypto.Decrypt(dtrEmail["PERS_PSWD"].ToString()) +"</b>");
					sbBrokerMail.Append(CreateResinList(dtrEmail["PERS_PREF"].ToString()));
					sbBrokerMail.Append("<br><b>Comments:</b><BR> "+dtrEmail["PERS_CMNT"].ToString());
					MailMessage b_mail = new MailMessage();
					b_mail.From = Application["strEmailOwner"].ToString();
					// mike always gets the mail
					
					b_mail.To=Application["strEmailOwner"].ToString();
					// broker only gets cced when they are selecte
					if (bCCBroker)
					{
						b_mail.Cc = dtrBrokerEmail["PERS_MAIL"].ToString();

					}
					b_mail.Subject = "Lead Allocated";
					b_mail.Body = sbBrokerMail.ToString();
					b_mail.BodyFormat = MailFormat.Html;
					//SmtpMail.SmtpServer="localhost";
					TPE.Utility.EmailLibrary.Send(Context,b_mail);
					dtrEmail.Close();
					conn.Close();


				}

			}
			dtrBrokerEmail.Close();
			Bind_Content();
			pnMain.Visible =  true;
			pnAllocate.Visible = false;

		}
		// <summary>
		//  function returns a HTML list of the resin as specified by the passed PREF ID
		// </summary>
		private string CreateResinList(string iPref)
		{

			//Database connection
			SqlConnection conn;
			SqlCommand cmdResin;
			SqlDataReader dtrResin;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();

			//string Pref; // The users preference
			//'get the value of preference from querystring passed by Demo_Lead.aspx
			//Pref = Request.QueryString("Pref")

			string strSQL;
			string strOutput;

			//Use "POWER" method to find preference
			strSQL="SELECT CONT_LABL FROM CONTRACT WHERE POWER(2,CONT_ID-1)&("+iPref+")<>0";
			cmdResin = new SqlCommand(strSQL,conn);
			dtrResin = cmdResin.ExecuteReader();
			strOutput ="<table>";
			while (dtrResin.Read())
			{
				strOutput +="<tr><td>"+dtrResin["CONT_LABL"].ToString()+"</td></tr>";
			}
			strOutput +="</table>";
			dtrResin.Close();
			conn.Close();
			return strOutput;

		}



		// <summary>
		//  Switchs the panel that brings up the screen that allows users to send emails when allocated
		// users check boxes to inicate that a mail should be sent
		// </summary>
		public void Click_Allocate(object sender, EventArgs e)
		{
			string strSQL;
			string strOutput;
			int iCount;
			iCount = 0; // number of users selected
			SqlCommand cmdAllocateList;
			SqlDataReader dtrAllocateList;

			strSQL= "Select PERS_ID,PERS_FRST_NAME+ ' ' + PERS_LAST_NAME AS Name FROM PERSON WHERE PERS_ID IN (";
			foreach (string item in Request.Form)
			{
				if (item.Substring(0,3) == "CB_")
				{
					iCount ++;
					strSQL += item.Substring(3,item.Length -3)+",";
				}
			}
			strSQL = strSQL.Substring(0,strSQL.Length -1);
			strSQL +=")";
			// count must be greater than one.  otherwise the users hasn't selected anybody
			// and should return to the original main screen
			if (iCount > 0)
			{
				cmdAllocateList = new SqlCommand(strSQL,connGenius);
				dtrAllocateList = cmdAllocateList.ExecuteReader();
				strOutput ="";

				while (dtrAllocateList.Read())
				{
					strOutput += "<tr><td>"+dtrAllocateList["Name"].ToString()+"<td align=\"center\"><input type=\"hidden\" name=\"Id_"+dtrAllocateList["PERS_ID"].ToString()+"\"> <input name=\"To_"+dtrAllocateList["PERS_ID"].ToString()+"\" type=\"checkbox\"></td><td align=\"center\"><input name=\"CC_"+dtrAllocateList["PERS_ID"].ToString()+"\" type=\"checkbox\" checked></td></td></tr>";
				}
				phAllocate.Controls.Add (new LiteralControl(strOutput));
				pnMain.Visible =  false;
				pnAllocate.Visible = true;
				dtrAllocateList.Close();
			}
			else
			{
				Bind_Content();
			}


		}
		// <summary>
		//  These functions add the sort parameter and then rebind the content
		//  I apologize to whomever is maintaing this app cuz this approach sucks
		// </summary>

		public void SortName(object sender, EventArgs e)
		{
			switch(lblSort.Text)
			{
				case "PERS_LAST_NAME DESC":
					cmdContent.Parameters.Add("@Sort"," ORDER By PERS_LAST_NAME ASC");
					lblSort.Text ="PERS_LAST_NAME ASC";
					break;
				case "PERS_LAST_NAME ASC":
					cmdContent.Parameters.Add("@Sort"," ORDER By PERS_LAST_NAME DESC");
					lblSort.Text ="PERS_LAST_NAME DESC";
					break;
				default:
					cmdContent.Parameters.Add("@Sort"," ORDER By PERS_LAST_NAME ASC");
					lblSort.Text ="PERS_LAST_NAME ASC";
					break;
			}
			Bind_Content();
		}
		public void SortTitle(object sender, EventArgs e)
		{
			switch(lblSort.Text)
			{
				case "PERS_TITL DESC":
					cmdContent.Parameters.Add("@Sort"," ORDER By PERS_TITL ASC");
					lblSort.Text ="PERS_TITL ASC";
					break;
				case "PERS_TITL ASC":
					cmdContent.Parameters.Add("@Sort"," ORDER By PERS_TITL DESC");
					lblSort.Text ="PERS_TITL DESC";
					break;
				default:
					cmdContent.Parameters.Add("@Sort"," ORDER By PERS_TITL ASC");
					lblSort.Text ="PERS_TITL ASC";
					break;
			}
			Bind_Content();
		}
		public void SortCompany(object sender, EventArgs e)
		{
			switch(lblSort.Text)
			{
				case "COMP_NAME DESC":
					cmdContent.Parameters.Add("@Sort"," ORDER By COMP_NAME ASC");
					lblSort.Text ="COMP_NAME ASC";
					break;
				case "COMP_NAME ASC":
					cmdContent.Parameters.Add("@Sort"," ORDER By COMP_NAME DESC");
					lblSort.Text ="COMP_NAME DESC";
					break;
				default:
					cmdContent.Parameters.Add("@Sort"," ORDER By COMP_NAME ASC");
					lblSort.Text ="COMP_NAME ASC";
					break;
			}
			Bind_Content();
		}
		public void SortDate(object sender, EventArgs e)
		{
			switch(lblSort.Text)
			{
				case "PERS_DATE DESC":
					cmdContent.Parameters.Add("@Sort"," ORDER By PERS_DATE ASC");
					lblSort.Text ="PERS_DATE ASC";
					break;
				case "PERS_DATE ASC":
					cmdContent.Parameters.Add("@Sort"," ORDER By PERS_DATE DESC");
					lblSort.Text ="PERS_DATE DESC";
					break;
				default:
					cmdContent.Parameters.Add("@Sort"," ORDER By PERS_DATE ASC");
					lblSort.Text ="PERS_DATE ASC";
					break;
			}
			Bind_Content();
		}
		public void SortAccess(object sender, EventArgs e)
		{
			switch(lblSort.Text)
			{
				case "VARACES DESC":
					cmdContent.Parameters.Add("@Sort"," ORDER By VARACES ASC");
					lblSort.Text ="VARACES ASC";
					break;
				case "VARACES ASC":
					cmdContent.Parameters.Add("@Sort"," ORDER By VARACES DESC");
					lblSort.Text ="VARACES DESC";
					break;
				default:
					cmdContent.Parameters.Add("@Sort"," ORDER By VARACES ASC");
					lblSort.Text ="VARACES ASC";
					break;
			}
			Bind_Content();
		}
		// <summary>
		//  When the users clicks the 'next button this function fires
		// </summary>
		public void Next_Page(object sender, EventArgs e)
		{
			iPage= Int32.Parse (lblPage.Text) +1;
			cmdContent.Parameters.Add("@Sort"," ORDER By " + lblSort.Text);
			Bind_Content();
		}
		// <summary>
		//  When the users clicks the 'last button this function fires
		// </summary>
		public void Last_Page(object sender, EventArgs e)
		{
			iPage= Int32.Parse (lblPage.Text) -1;
			cmdContent.Parameters.Add("@Sort"," ORDER By " + lblSort.Text);
			Bind_Content();
		}


		// <summary>
		//  Handlers for the filters
		// </summary>
		public void Resin_Filter(object sender, EventArgs e)
		{
			Bind_Content();
		}

		public void Type_Filter(object sender, EventArgs e)
		{
			Bind_Content();
		}
		public void Access_Filter(object sender, EventArgs e)
		{
			Bind_Content();
		}
		public void Click_Search(object sender, EventArgs e)
		{
			Bind_Content();
		}
		// <summary>
		//   adds a list of users to the a session variable to be emailed
		//   this list is passed to the confirmation page to be process
		// </summary>
		public void Click_Email_Offers(object sender, EventArgs e)
		{
			string strUsers;
			strUsers = "";
			foreach (string item in Request.Form)
			{
				if (item.Substring(0,3) == "CB_")
				{

					strUsers += item.Substring(3,item.Length -3)+",";
				}
			}
			Session["strEmailOffer_Address"]= strUsers;
			//lbltest.Text = strUsers;
			//lbltest.Text =Session["strEmailOffer_Address"].ToString();
			//Bind_Content();
			Response.Redirect ("../Administrator/Confirm_Email.aspx");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
