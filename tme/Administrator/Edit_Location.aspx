<%@ Page language="c#" Codebehind="Edit_Location.aspx.cs" AutoEventWireup="True" Inherits="localhost.Edit_Location" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">	
		<TABLE id="Table3" height="400" align="center" border="0" class="Content Color2">
			<tr>
			    <td>&nbsp;</td>
			</tr>
			<tr>
				<TD class="Content" align="left">
					<asp:label id="lblCompanyName" runat="server" CssClass="Content Color2" Width="100%" ></asp:label><br><br>
					<asp:label id="lblAction" runat="server"  Font-Bold="True"></asp:label><br><br>
					<asp:label id="lblCompanyID" runat="server" Visible="False"></asp:label>
					<asp:label id="lblLocationID" runat="server" Visible="False"></asp:label>
				</TD>
			</tr> 
			<TR>
				<TD align="left" width="120">
					<asp:Label id="lblType" runat="server" Font-Bold="True">Type:</asp:Label>
					<asp:Label id="Test" runat="server"></asp:Label></TD>
				<TD align="left">
					<asp:DropDownList id="ddlType" runat="server" CssClass="InputForm" Width="160px">
						<asp:ListItem Value="D">Delivery</asp:ListItem>
						<asp:ListItem Value="H">Headquarters</asp:ListItem>
						<asp:ListItem Value="S">Shipping</asp:ListItem>
					</asp:DropDownList><FONT color="red">
						<asp:Label id="lblwrong" runat="server"></asp:Label></FONT></TD>
			</TR>
			<TR>
				<TD align="left" width="103">
					<asp:Label id="lblLocationName" runat="server" Font-Bold="True">Location Name:</asp:Label></TD>
				<TD align="left">
					<asp:TextBox id="txtLocationName" CssClass="InputForm" runat="server" Width="160px" MaxLength="250"></asp:TextBox></TD>
			</TR>
		
			<TR>
				<TD align="left" width="103">
					<asp:Label id="lblContact" runat="server" Font-Bold="True">Contact:</asp:Label></TD>
				<TD align="left">
					<asp:TextBox id="txtContact" runat="server" CssClass="InputForm" Width="160px" maxlength="50"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD align="left" width="103">
					<asp:Label id="lblEmail" runat="server" Font-Bold="True">Email:</asp:Label></TD>
				<TD align="left">
					<asp:TextBox id="txtEmail" runat="server" CssClass="InputForm" Width="160px" maxlength="90"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD align="left" width="103" height="26">
					<asp:Label id="lblPhone" runat="server" Font-Bold="True">Phone:</asp:Label></TD>
				<TD align="left" height="26">
					<asp:TextBox id="txtPhone" runat="server" CssClass="InputForm" Width="160px" MaxLength="15"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD align="left" vAlign="top" width="103">
					<asp:Label id="lblFax" runat="server" Font-Bold="True">Fax:</asp:Label></TD>
				<TD align="left">
					<asp:TextBox id="txtFax" runat="server" CssClass="InputForm" Width="160px" MaxLength="15"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD align="left" vAlign="top" width="103">
					<asp:Label id="lblAddress" runat="server" Font-Bold="True">Address:</asp:Label></TD>
				<TD align="left">
					<asp:TextBox id="txtAddress1" CssClass="InputForm" runat="server" Width="160px" MaxLength="50"></asp:TextBox><BR>
					<asp:TextBox id="txtAddress2" CssClass="InputForm" runat="server" Width="160px" MaxLength="50"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD align="left" width="103">
					<asp:Label id="lblCity" runat="server" Font-Bold="True">City:</asp:Label></TD>
				<TD align="left">
					<asp:TextBox id="txtCity" runat="server" CssClass="InputForm" Width="160px" MaxLength="20"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD align="left" width="103">
					<asp:Label id="lblStateProvince" runat="server" Font-Bold="True">State/Providence:</asp:Label></TD>
				<TD align="left">
					<asp:DropDownList id="ddlState" runat="server" CssClass="InputForm" Width="160px"></asp:DropDownList></TD>
			</TR>
			<TR>
				<TD align="left" width="103">
					<asp:Label id="lblZip" runat="server" Font-Bold="True">Zip:</asp:Label></TD>
				<TD align="left">
					<asp:TextBox id="txtZip" runat="server" CssClass="InputForm" Width="160px" MaxLength="10"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD align="left" width="103" height="20">
					<asp:Label id="lblCountry" runat="server" Font-Bold="True">Country:</asp:Label></TD>
				<TD align="left" height="20">
					<asp:DropDownList id="ddlCountry" runat="server" CssClass="InputForm" Width="160px" AutoPostBack="true" onselectedindexchanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList></TD>
			</TR>
			<TR>
				<TD align="left" width="103">
					<asp:Label id="lblRailNumber" runat="server" Font-Bold="True">Rail #:</asp:Label></TD>
				<TD align="left">
					<asp:TextBox id="txtRailNumber" runat="server" CssClass="InputForm" Width="160px" MaxLength="50"></asp:TextBox></TD>
			</TR>
			<TR>
				<TD align="center" colSpan="2"><br>
					<asp:Button  id="cmdSave" Width="70px" runat="server" Text="Save" CssClass="Content Color2" onclick="cmdSave_Click"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:Button  id="cmdClose" Width="70px" runat="server" Text="Close" CssClass="Content Color2" onclick="cmdClose_Click"></asp:Button><br><br>
				</TD>
			</TR>
		</TABLE>
</asp:Content>		