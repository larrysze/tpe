<%@ Page Language="c#" CodeBehind="Bill_Of_Lading_BKUP.aspx.cs" AutoEventWireup="false" Inherits="localhost.Administrator.Bill_Of_Lading_BKUP" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Web.Mail" %>
<%'send bill of lading to someone in the company from drop down list%>
<script>
function submit_email(){

	{
		document.Form.send.value='1';
		document.Form.submit();
	}

}

</script>
<form runat="server" id="Form">
    <TPE:Template PageTitle="" Runat="Server" />
<%
'only administrator and creditor can access this page
IF (Session("Typ")<>"C" AND Session("Typ")<>"A") THEN Response.Redirect("../default.aspx")

'database connections
Dim conn as SqlConnection
Dim cmdContent as SQLCommand
Dim Rec0 as SqlDataReader
conn = new SqlConnection(ConfigurationSettings.AppSettings("DBConn"))
conn.Open()

Dim connSHIP as SqlConnection
Dim cmdContentSHIP as SQLCommand
Dim RecSHIP as SqlDataReader
connSHIP = new SqlConnection(ConfigurationSettings.AppSettings("DBConn"))
connSHIP.Open()

Dim connAddress2 as SqlConnection
Dim Address2 as SqlDataReader
connAddress2 = new SqlConnection(ConfigurationSettings.AppSettings("DBConn"))
connAddress2.Open()

Dim Str 'build the content of bill of lading as a string(for email function)
Dim i 'get the order ID from "Filled Orders" page


Dim arrID(2) As String
arrID = Request.QueryString("ID").ToString().Split("-")
Dim cmdShipmentID = new SqlCommand("Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID = '"+arrID(0)+"' and SHIPMENT_SKU ='"+arrID(1)+"'",conn)
i= cmdShipmentID.ExecuteScalar() 
'i =	Request.QueryString("Id")



    Str=""

    'email header
    Str="<table border=0 cellspacing=0 cellpadding=0 align=center><tr><td><img src='/images/email/tpelogo.gif'></td><td STYLE='FONT: 14pt ARIAL BLACK;COLOR=BLACK'>The<font color=red>Plastics</font>Exchange<font color=red>.</font>com</td></tr></table><br><br>"
    Str=Str+"<table border=0 cellspacing=0 cellpadding=0 width='650' align='center'>"
    Str=Str+"<tr><td colspan=3><center><SPAN STYLE='BACKGROUND-COLOR:#CCCCCC;FONT: BOLD 10pt ARIAL'>"
    Str=Str+"Bill of Lading * Short Form * Non Negotiable"
    Str=Str+"</SPAN></center><br><br></td></tr></table>"
    Str=Str+"<table border=0 align=center width='450'> <tr valign=top>"
    Str=Str+"<td width='50%'><SPAN STYLE='FONT: 8pt ARIAL'><b><i><font size=3>Sold to:</i></b></td>"
    Str=Str+"<td width='50%'><SPAN STYLE='FONT: 8pt ARIAL'><b><i><font size=3>Ship to:</i></b></td></tr></table>"


    Str=Str+"<table border=1 align=center width='450'> <tr valign=top>"
    'Query for company info
    cmdContent= new SqlCommand("SELECT (SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=PLAC_COMP),PLAC_ADDR_ONE,PLAC_ADDR_TWO,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL),PLAC_ZIP,(SELECT (SELECT CTRY_NAME FROM COUNTRY WHERE CTRY_CODE=LOCL_CTRY) FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) FROM PLACE WHERE PLAC_TYPE='H' AND PLAC_COMP=(SELECT (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR) FROM SHIPMENT WHERE SHIPMENT_ID="+i.ToString()+")", conn)
    Rec0= cmdContent.ExecuteReader()
    Rec0.Read

    IF Rec0.Hasrows THEN
          'Sold to address

	   Str=Str+"<td width='50%'><SPAN STYLE='FONT: 8pt ARIAL'><b><i>"
	   Str=Str+""
	   Str=Str+"</i></b>"&Rec0(0)&"<br>"&Rec0("PLAC_ADDR_ONE")
	   If Rec0("PLAC_ADDR_TWO") Is DBNull.Value Then
	   Else
	        IF Rec0("PLAC_ADDR_TWO")<>"" THEN
	        Str=Str+"<br>"&Rec0("PLAC_ADDR_TWO")
	        End IF
	   End IF
	   Str=Str+"<br>"&Rec0(3)&"  "&Rec0("PLAC_ZIP")&"<br><br></SPAN></td>"
    END IF
    Rec0.Close
      cmdContent= new SqlCommand("SELECT PLAC_ADDR_ONE,PLAC_ADDR_TWO,PLAC_INST,PLAC_PHON,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) As Location,(SELECT PLAC_ZIP FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) As Zip FROM PLACE WHERE PLAC_ID=(SELECT SHIPMENT_TO FROM SHIPMENT WHERE SHIPMENT_ID="+i.ToString()+")", connAddress2)
      Address2= cmdContent.ExecuteReader()
      Address2.Read

          'Ship to address

      Str=Str+"<td width='50%'><SPAN STYLE='FONT: 8pt ARIAL'><b><i>"
      Str=Str+"</i></b>"

           'only display the record which is not Null or empty


	   IF Address2("PLAC_INST") Is DBNull.Value Then
	   Else
	     IF Address2("PLAC_INST")<>"" Then
	     Str=Str+""&Address2("PLAC_INST")&""
	     End If
	   End IF
      	   Str=Str+"<br>"&Address2("PLAC_ADDR_ONE")&"<br>"

           If Address2("PLAC_ADDR_TWO") Is DBNull.Value Then
           Else
               IF Address2("PLAC_ADDR_TWO")<>"" THEN
               Str=Str+Address2("PLAC_ADDR_TWO")&"<br>"
	       End IF
	   End IF
	   Str=Str+Address2("Location")&"  "&Address2("Zip")&""
	  IF Address2("PLAC_PHON") Is DBNull.Value Then
	   Else
	   IF Address2("PLAC_PHON")<>"" Then
	   	     Str=Str+"<br>"&Address2("PLAC_PHON")&"<br>"
	   	     End If
	   End IF
	   Str=Str+"<br><br></SPAN></td>"
     Str=Str+"</tr></table><br><br>"
     Address2.Close



'     cmdContent= new SqlCommand("SELECT LEFT(CONVERT(varchar(14),ORDR_DATE,101), 10),ORDR_ID,ORDR_BID,(ORDR_PRCE+ORDR_SHIP_PRCE+ORDR_MARK+ORDR_COMM)*(ISNULL(ORDR_WGHT,ORDR_QTY*(ORDR_SIZE)))*ISNULL((SELECT CRED_RATE+(CASE WHEN CAST(ORDR_TERM AS INTEGER)<=30 THEN CRED_RATE_1*CAST(ORDR_TERM AS INTEGER) WHEN CAST(ORDR_TERM AS INTEGER)>30 AND CAST(ORDR_TERM AS INTEGER)<=45 THEN 30*CRED_RATE_1+(CAST(ORDR_TERM AS INTEGER)-30)*CRED_RATE_2 WHEN CAST(ORDR_TERM AS INTEGER)>45 AND CAST(ORDR_TERM AS INTEGER)<=60 THEN 30*CRED_RATE_1+15*CRED_RATE_2+(CAST(ORDR_TERM AS INTEGER)-45)*CRED_RATE_3 WHEN CAST(ORDR_TERM AS INTEGER)>60 THEN 30*CRED_RATE_1+15*CRED_RATE_2+15*CRED_RATE_3+(CAST(ORDR_TERM AS INTEGER)-60)*CRED_RATE_4 END) FROM CREDIT WHERE CRED_COMP=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=ORDR_BUYR)),'1'),ISNULL(ORDR_TERM,'30'),ORDR_QTY,ORDR_SIZE,ORDR_WGHT,ORDR_PRCE+ORDR_SHIP_PRCE+ORDR_MARK+ORDR_COMM,(SELECT CONT_LABL FROM CONTRACT WHERE CONT_ID=ORDR_CONT),(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=ORDR_TO)),DATEADD(day,30,ORDR_DATE_FACT),VARDATE=(SELECT MAX(SHIP_DATE_TAKE) FROM SHIP WHERE SHIP_ORDR=ORDR_ID),(SELECT CASE WHEN ORDR_WGHT IS NULL THEN '0' ELSE '1' END),NULDATE=(SELECT CASE WHEN ORDR_SIZE=42000 THEN CONVERT(varchar(14),DATEADD(day,3,ORDR_DATE),101) WHEN ORDR_SIZE=190000 THEN CONVERT(varchar(14),DATEADD(day,10,ORDR_DATE),101) END),ISNULL(ORDR_WGHT,ORDR_QTY*(ORDR_SIZE)) AS Lbs,VARMONTH=(SELECT FWD_MNTH+' '+RIGHT(CONVERT(varchar,FWD_YEAR),2) FROM FWDMONTH WHERE FWD_ID=ORDR_MNTH),INVOICE_SENT,INVOICE_EMAIL_TO,ORDR_SKU,ORDR_PRCE,PO_SENT,PO_EMAIL_TO, SHIP=(SELECT TOP 1 SHIP_DATE_TAKE FROM SHIP WHERE SHIP_ORDR=ORDR_ID), ORDR_PROD_SPOT, ORDR_ISSPOT, ORDR_SKU, ORDR_PAYMNT, ORDR_FOB, CAST(ORDR_TERM AS INTEGER) AS DAYS, ORDR_MELT, ORDR_DENS, ORDR_ADDS, ORDR_PO_NUMBER FROM ORDERS WHERE ORDR_ID="&i.ToString(), conn)



     cmdContent= new SqlCommand("SELECT LEFT(CONVERT(varchar(14),ORDR_DATE,101), 10),ORDR_ID,ORDR_BID,(SHIPMENT_PRCE+SHIPMENT_SHIP_PRCE+SHIPMENT_MARK+SHIPMENT_COMM)*(ISNULL(SHIPMENT_WEIGHT,SHIPMENT_QTY*(SHIPMENT_SIZE)))*ISNULL((SELECT CRED_RATE+(CASE WHEN CAST(SHIPMENT_BUYER_TERMS AS INTEGER)<=30 THEN CRED_RATE_1*CAST(SHIPMENT_BUYER_TERMS AS INTEGER) WHEN CAST(SHIPMENT_BUYER_TERMS AS INTEGER)>30 AND CAST(SHIPMENT_BUYER_TERMS AS INTEGER)<=45 THEN 30*CRED_RATE_1+(CAST(SHIPMENT_BUYER_TERMS AS INTEGER)-30)*CRED_RATE_2 WHEN CAST(SHIPMENT_BUYER_TERMS AS INTEGER)>45 AND CAST(SHIPMENT_BUYER_TERMS AS INTEGER)<=60 THEN 30*CRED_RATE_1+15*CRED_RATE_2+(CAST(SHIPMENT_BUYER_TERMS AS INTEGER)-45)*CRED_RATE_3 WHEN CAST(SHIPMENT_BUYER_TERMS AS INTEGER)>60 THEN 30*CRED_RATE_1+15*CRED_RATE_2+15*CRED_RATE_3+(CAST(SHIPMENT_BUYER_TERMS AS INTEGER)-60)*CRED_RATE_4 END) FROM CREDIT WHERE CRED_COMP=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)),'1'),ISNULL(SHIPMENT_BUYER_TERMS,'30'),SHIPMENT_QTY,SHIPMENT_SIZE,SHIPMENT_WEIGHT,SHIPMENT_PRCE+SHIPMENT_SHIP_PRCE+SHIPMENT_MARK+SHIPMENT_COMM,(SELECT CONT_LABL FROM CONTRACT WHERE CONT_ID=ORDR_CONT),(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=SHIPMENT_TO)),DATEADD(day,30,SHIPMENT_DATE_TAKE),VARDATE=(SELECT MAX(SHIP_DATE_TAKE) FROM SHIP WHERE SHIP_ORDR=ORDR_ID),(SELECT CASE WHEN SHIPMENT_WEIGHT IS NULL THEN '0' ELSE '1' END),NULDATE=(SELECT CASE WHEN SHIPMENT_SIZE=42000 THEN CONVERT(varchar(14),DATEADD(day,3,ORDR_DATE),101) WHEN SHIPMENT_SIZE=190000 THEN CONVERT(varchar(14),DATEADD(day,10,ORDR_DATE),101) END),ISNULL(SHIPMENT_WEIGHT,SHIPMENT_QTY*(SHIPMENT_SIZE)) AS Lbs,VARMONTH=(SELECT FWD_MNTH+' '+RIGHT(CONVERT(varchar,FWD_YEAR),2) FROM FWDMONTH WHERE FWD_ID=SHIPMENT_MNTH),SHIPMENT_INVOICE_SENT,SHIPMENT_INVOICE_EMAIL_TO,SHIPMENT_SKU,SHIPMENT_PRCE,SHIPMENT_PO_SENT,SHIPMENT_PO_EMAIL_TO, SHIP=(SELECT TOP 1 SHIP_DATE_TAKE FROM SHIP WHERE SHIP_ORDR=ORDR_ID), ORDR_PROD_SPOT, ORDR_ISSPOT, SHIPMENT_SKU, SHIPMENT_SELLER_TERMS, SHIPMENT_FOB, CAST(SHIPMENT_BUYER_TERMS AS INTEGER) AS DAYS, ORDR_MELT, ORDR_DENS, ORDR_ADDS, SHIPMENT_PO_NUM FROM ORDERS , SHIPMENT  WHERE SHIPMENT_ID ="&i.ToString() , conn)
     Rec0= cmdContent.ExecuteReader()
     Rec0.Read


     'ship date

     Str=Str+"<table border=1 align=center width='450'>"
     Str=Str+" <tr valign=top><td width='50%'><SPAN STYLE='FONT: 8pt ARIAL'><b><i>"
     Str=Str+"Ship Date:"
     IF Rec0("SHIP") Is DBNull.Value Then
     Str=Str+"</i></b></td><td width='50%'><font color=red>"&Rec0("NULDATE")
     Else
     Str=Str+"</i></b></td><td width='50%'>"&Rec0("VARDATE")
     End IF
     Str=Str+"<br></SPAN></td></tr>"


     'Your PO, currentlly unavailable!

     Str=Str+"<tr valign=top><td width='50%'><SPAN STYLE='FONT: 8pt ARIAL'><b><i>"
     Str=Str+"Your PO #"
     IF Rec0("SHIPMENT_PO_NUM") Is DBNull.Value Then
     Str=Str+"</i></b></td><td width='50%'>&nbsp&nbsp&nbsp&nbsp"
     Else
     Str=Str+"</i></b></td><td width='50%'>"&Rec0("SHIPMENT_PO_NUM")
     End IF

     Str=Str+"<br></SPAN></td></tr>"

     'Order # and Order Sku#

     Str=Str+" <tr valign=top><td width='50%'><SPAN STYLE='FONT: 8pt ARIAL'><b><i>"
     Str=Str+"Our Order #"
     Str=Str+"</i></b></td><td width='50%'>"&Rec0("ORDR_ID")&"-"&Rec0("SHIPMENT_SKU")&""
     Str=Str+"<br></SPAN></td></tr>"

     'Order date
     Str=Str+" <tr valign=top><td width='50%'><SPAN STYLE='FONT: 8pt ARIAL'><b><i>"
     Str=Str+"Order Date"
     IF Rec0(0) Is DBNull.Value Then
     Str=Str+"</i></b></td><td width='50%'>&nbsp"
     Else
     Str=Str+"</i></b></td><td width='50%'>"&Rec0(0)
     End IF
     Str=Str+"<br></SPAN></td></tr></table><br><br>"

     'order detail, if it's from bborders, display ORDR_PROD_SPOT

     Str=Str+"<table border=1 align=center width='450'>"
     Str=Str+" <tr valign=top><td width='50%'><table border=1 width='100%'><tr valign=top><td width='50%'><SPAN STYLE='FONT: 8pt ARIAL'><b><i>"
     Str=Str+"Description:"
     IF Rec0("ORDR_ISSPOT") THEN
     Str=Str+"</i></b></td><td width='50%'>"&Rec0("ORDR_PROD_SPOT")
     Else
     Str=Str+"</i></b></td><td width='50%'>"&Rec0(9)
     End IF

     'Order weight, it would be null if it hasn't been shipped.
     'will be kicked out if it's null

     Str=Str+"<br></SPAN></td></tr>"
     Str=Str+"<tr valign=top><td width='50%'><SPAN STYLE='FONT: 8pt ARIAL'><b><i>Net Weight"
     Str=Str+"</i></b></td><td width='50%'>"&Rec0("SHIPMENT_WEIGHT")
     Str=Str+"<br></SPAN></td></tr></table></td>"
     Str=Str+"<td width='25%' valign=center><SPAN STYLE='FONT: 8pt ARIAL'><b><i>Railcar #</td>"
     Str=Str+"<td width='25%' valign=center>&nbsp"

'will be kicked out if ship_ID_CH is null


'query to find the ship_ID_CH

Dim ship
IF Rec0("SHIPMENT_SIZE")=190000 Then
cmdContentSHIP= new SqlCommand("SELECT SHIP_ID_CH FROM SHIP WHERE SHIP_ORDR="+i+"", connSHIP)
RecSHIP= cmdContentSHIP.ExecuteReader()
RecSHIP.Read()


   IF RecSHIP.Hasrows Then
   IF RecSHIP("SHIP_ID_CH") IS DBNull.Value Then
   ship=""
   Else
		   If RecSHIP.Hasrows Then

		   RecSHIP.Close()
		   cmdContentSHIP= new SqlCommand("SELECT SHIP_ID_CH FROM SHIP WHERE SHIP_ORDR="+i+"", connSHIP)
		   RecSHIP= cmdContentSHIP.ExecuteReader()
		   WHile RecSHIP.Read()
		   Str=Str+""&RecSHIP("SHIP_ID_CH")&""
		     ship=RecSHIP("SHIP_ID_CH")
		  END While
		  Else
		   Str=Str+"<Br>"
		  End IF

       End IF
       End IF

   RecSHIP.Close()
connSHIP.Close()
Else
End IF

    Str=Str+" </td></tr></table>"
    Str=Str+"<table align=center width='100%'><tr><td colspan=3 valign=top align=center><hr></td></tr>"
    Str=Str+"<tr><td colspan=3 valign=top align=center><font face=arial size=1 color=Black>710 North Dearborn</font>&nbsp;&nbsp;<font face=arial size=1 color=red>Chicago, IL 60610</font>&nbsp;&nbsp;<font face=arial size=1 color=Black><b>tel 312.202.0002</b></font>&nbsp;&nbsp;<font face=arial size=1 color=red>fax 312.202.0174</font></td></tr>"
    Str=Str+"</table>"
    Str=Str+"</BODY></HTML>"


'Bill of lading is only available for railcar order has been shipped.
'IF Rec0("SHIPMENT_SIZE")=190000 Then
                'order weight will not be null if it is shipped.
		IF Rec0("SHIPMENT_WEIGHT") IS DBNull.Value OR Rec0("SHIPMENT_WEIGHT").Tostring()="" Then
		Response.write ("<center><font color=red>Your order hasn't been shipped yet!</font><br><input type=button class=tpebutton value='Back' onClick='Javascript:history.back()'>")
		Elseif ship="" Then
		Response.write ("<center><font color=red>Your order hasn't been shipped yet!</font><br><input type=button class=tpebutton value='Back' onClick='Javascript:history.back()'>")
		Else
		Response.write (Str)


		'if ordr_wght or ship_ID_ch is null, display error message
		'otherwise display the bill and email option

		%>

		<input type=hidden name=send value=''></input>
		<table border=0 align=center cellspacing=0 cellpadding=0 >
		<tr>
		<td colspan=6  Class="Header" ><img src=/images/1x1.gif height=1>
		<font color=white>Send Bill of Lading by Email</font>
		</td>
		</tr>
		<tr>
			<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
			<td width=3><img src=/images/1x1.gif width=1></td>
			<td><strong>From:</strong></td>
			<td >
				<select name=EmailFrom>
				<option value="michael@theplasticsexchange.com">michael@theplasticsexchange.com
				<option value="chris@theplasticsexchange.com">chris@theplasticsexchange.com
				<option value="brian@theplasticsexchange.com">brian@theplasticsexchange.com
				<option value="Kevin@@theplasticsexchange.com">Kevin@@theplasticsexchange.com
				<option value="Accounting@ThePlasticsExchange.com" selected>Accounting@theplasticsexchange.com
				</select>
			</td>

			<td width=3><img src=/images/1x1.gif width=1></td>
			<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
		 </tr>
		 <tr>
			<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
			<td width=3><img src=/images/1x1.gif width=1></td>
			<td><strong>To:</strong></td>
			<td ><input maxlength=80 size=30 name=EmailTo></input></td>
			<td width=3><img src=/images/1x1.gif width=1></td>
			<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
		</tr>

		<tr>
		        <td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
		        <td align="center" valign="top" colspan=4>
		        <BR><input type=button class=tpebutton value="Send" onClick="Javascript:submit_email()"></input>
		        <BR>
		        </td>
		        <td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
		</tr>
		<tr>
			<td colspan=6  Class="Header" ><img src=/images/1x1.gif height=1>
			</td>
		</tr>
		</table>


		<%
		IF  Request.Form("send")=1 Then
		Dim mail1 AS MailMessage
		mail1 = new MailMessage()
		mail1.From = Request.Form("EmailFrom")
		mail1.To = Request.Form("EmailTo")
		mail1.Subject = "Bill of Lading"
		mail1.Body = str
		mail1.BodyFormat = MailFormat.Html
		TPE.Utility.EmailLibrary.Send(Context,mail1)
		Response.Redirect("../Common/Filled_Orders.aspx")
		End IF
                %>




<%
End IF
'Else
'Response.write ("<center><font color=red>It must be a railcar!</font><br><input type=button class=tpebutton value='Back' onClick='Javascript:history.back()'>")
'End IF
%>

<TPE:Template Footer="true" Runat="Server" />

</form>
