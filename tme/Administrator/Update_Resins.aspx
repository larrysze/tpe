<%@ Page Language="c#" Codebehind="Update_Resins.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.Update_Resins" Title="Update Resins" MasterPageFile="~/MasterPages/Menu.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
    <asp:Panel ID="pnMain" runat="server">
        <table id="Table2" cellspacing="0" cellpadding="0" width="100%" align="center" border="0" class=Content>
            <tr>
                <td>
                <td>
                </td>
            </tr>
        </table>
        <center>
            <h3>
                <asp:Label ID="lblName" runat="server"></asp:Label></h3>
        </center>
        <table id="Table1" cellspacing="0" cellpadding="0" width="75%" align="center" border="0" class=Content>
            <tr>
                <td align="center" colspan="2">
                    <b>Resin preferences:</b></td>
            </tr>
            <tr align=left>
                <td valign="top" style="height: 479px">
                    <asp:Table ID="tblDynamic" runat="server" CellSpacing="0" CellPadding="0" Width="100%" HorizontalAlign="Center">
                    </asp:Table>
                </td>
                <td valign="top" align="right" style="height: 479px">
                    <table width="100%" align="left">
                        <tr align=left>
                            <td style="height: 119px">
                                &nbsp;&nbsp;&nbsp;&nbsp;<b>Sizes:</b>
                                <ul>
                                    <li>
                                        <asp:CheckBox ID="chkSize1" runat="server" Text="Rail Cars"></asp:CheckBox>
                                    <li>
                                        <asp:CheckBox ID="chkSize2" runat="server" Text="Bulk Trucks"></asp:CheckBox>
                                    <li>
                                        <asp:CheckBox ID="chkSize3" runat="server" Text="Truckload Boxes\Bags"></asp:CheckBox></li></ul>
                            </td>
                        </tr>
                        <tr align=left>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;<b>Quality:</b>
                                <ul>
                                    <li>
                                        <asp:CheckBox ID="chkQuality1" runat="server" Text="Prime"></asp:CheckBox>
                                    <li>
                                        <asp:CheckBox ID="chkQuality2" runat="server" Text="Good Offgrade"></asp:CheckBox>
                                    <li>
                                        <asp:CheckBox ID="chkQuality3" runat="server" Text="Regrind/Repro"></asp:CheckBox></li></ul>
                            </td>
                        </tr>
                        <tr align=left>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;<b>Frequency of emails:</b><br>
                                <br>
                                <asp:RadioButtonList ID="cbEmailSubscriber" align="center" runat="server" Width="80%" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="2">Daily</asp:ListItem>
                                    <asp:ListItem Value="1" Selected="True">Weekly</asp:ListItem>
                                    <asp:ListItem Value="0">Never</asp:ListItem>
                                </asp:RadioButtonList></td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr align=left>
                            <td align="center">
                                <button id="btnSave" type="button" runat="server" onserverclick="btnSave_ServerClick">
                                    Update preferences</button></td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnThankYou" runat="server" Visible="False">
        <br>
        <br>
        <br>
        <center class=Content>
            <p>
                Thank you for updating your Resin Preferences!</p>
            <br>
            <br>
            <a href="/default.aspx" class=LinkNormal>Home page</a><br>
            <br>
        </center>
    </asp:Panel>
</asp:Content>
