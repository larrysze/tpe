<%@ Page language="c#" Codebehind="US_Map.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.US_Map" MasterPageFile="~/MasterPages/Menu.Master" Title="US Map" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
<div style="width:780">
	<map name="usmap">
		<area alt="" shape="RECT" coords="499,187,531,204" href="/Administrator/CRM.aspx?State=dc">
		<area alt="" shape="RECT" coords="510,163,539,183" href="/Administrator/CRM.aspx?State=md">
		<area alt="" shape="RECT" coords="512,145,543,158" href="/Administrator/CRM.aspx?State=de">
		<area alt="" shape="RECT" coords="500,127,527,144" href="/Administrator/CRM.aspx?State=nj">
		<area alt="" shape="RECT" coords="529,121,552,139" href="/Administrator/CRM.aspx?State=ct">
		<area alt="" shape="RECT" coords="537,103,559,120" href="/Administrator/CRM.aspx?State=ri">
		<area alt="" shape="CIRCLE" coords="544,81,14" href="/Administrator/CRM.aspx?State=ma">
		<area alt="" shape="CIRCLE" coords="488,34,14" href="/Administrator/CRM.aspx?State=nh">
		<area alt="" shape="CIRCLE" coords="454,48,13" href="/Administrator/CRM.aspx?State=vt">
		<area alt="" shape="CIRCLE" coords="371,207,16" href="/Administrator/CRM.aspx?State=tn">
		<area alt="" shape="CIRCLE" coords="52,78,16" href="/Administrator/CRM.aspx?State=or">
		<area alt="" shape="CIRCLE" coords="39,173,16" href="/Administrator/CRM.aspx?State=ca">
		<area alt="" shape="CIRCLE" coords="81,135,16" href="/Administrator/CRM.aspx?State=nv">
		<area alt="" shape="CIRCLE" coords="111,93,16" href="/Administrator/CRM.aspx?State=id">
		<area alt="" shape="CIRCLE" coords="156,57,16" href="/Administrator/CRM.aspx?State=mt">
		<area alt="" shape="CIRCLE" coords="178,111,16" href="/Administrator/CRM.aspx?State=wy">
		<area alt="" shape="CIRCLE" coords="125,155,16" href="/Administrator/CRM.aspx?State=ut">
		<area alt="" shape="CIRCLE" coords="114,217,16" href="/Administrator/CRM.aspx?State=az">
		<area alt="" shape="CIRCLE" coords="394,181,16" href="/Administrator/CRM.aspx?State=ky">
		<area alt="" shape="CIRCLE" coords="185,164,16" href="/Administrator/CRM.aspx?State=co">
		<area alt="" shape="CIRCLE" coords="174,222,16" href="/Administrator/CRM.aspx?State=nm">
		<area alt="" shape="CIRCLE" coords="241,60,16" href="/Administrator/CRM.aspx?State=nd">
		<area alt="" shape="CIRCLE" coords="237,98,16" href="/Administrator/CRM.aspx?State=sd">
		<area alt="" shape="CIRCLE" coords="244,136,16" href="/Administrator/CRM.aspx?State=ne">
		<area alt="" shape="CIRCLE" coords="254,174,16" href="/Administrator/CRM.aspx?State=ks">
		<area alt="" shape="CIRCLE" coords="265,213,16" href="/Administrator/CRM.aspx?State=ok">
		<area alt="" shape="CIRCLE" coords="242,260,16" href="/Administrator/CRM.aspx?State=tx">
		<area alt="" shape="CIRCLE" coords="293,71,16" href="/Administrator/CRM.aspx?State=mn">
		<area alt="" shape="CIRCLE" coords="304,130,16" href="/Administrator/CRM.aspx?State=ia">
		<area alt="" shape="CIRCLE" coords="313,177,16" href="/Administrator/CRM.aspx?State=mo">
		<area alt="" shape="CIRCLE" coords="316,221,16" href="/Administrator/CRM.aspx?State=ar">
		<area alt="" shape="CIRCLE" coords="319,262,16" href="/Administrator/CRM.aspx?State=la">
		<area alt="" shape="CIRCLE" coords="386,114,16" href="/Administrator/CRM.aspx?State=mi">
		<area alt="" shape="CIRCLE" coords="349,242,16" href="/Administrator/CRM.aspx?State=ms">
		<area alt="" shape="CIRCLE" coords="334,93,16" href="/Administrator/CRM.aspx?State=wi">
		<area alt="" shape="CIRCLE" coords="341,153,16" href="/Administrator/CRM.aspx?State=il">
		<area alt="" shape="CIRCLE" coords="376,153,16" href="/Administrator/CRM.aspx?State=in">
		<area alt="" shape="CIRCLE" coords="407,142,16" href="/Administrator/CRM.aspx?State=oh">
		<area alt="" shape="CIRCLE" coords="429,164,15" href="/Administrator/CRM.aspx?State=wv">
		<area alt="" shape="CIRCLE" coords="380,239,16" href="/Administrator/CRM.aspx?State=al">
		<area alt="" shape="CIRCLE" coords="414,239,16" href="/Administrator/CRM.aspx?State=ga">
		<area alt="" shape="CIRCLE" coords="441,225,16" href="/Administrator/CRM.aspx?State=sc">
		<area alt="" shape="CIRCLE" coords="445,300,16" href="/Administrator/CRM.aspx?State=fl">
		<area alt="" shape="CIRCLE" coords="455,171,14" href="/Administrator/CRM.aspx?State=va">
		<area alt="" shape="CIRCLE" coords="454,128,16" href="/Administrator/CRM.aspx?State=pa">
		<area alt="" shape="CIRCLE" coords="474,95,16" href="/Administrator/CRM.aspx?State=ny">
		<area alt="" shape="CIRCLE" coords="521,55,16" href="/Administrator/CRM.aspx?State=me">
		<area alt="" shape="CIRCLE" coords="461,198,14" href="/Administrator/CRM.aspx?State=nc">
		<area alt="" shape="CIRCLE" coords="70,33,16" href="/Administrator/CRM.aspx?State=wa">
		<area alt="" shape="CIRCLE" coords="121,306,16" href="/Administrator/CRM.aspx?State=ak">
		<area alt="" shape="CIRCLE" coords="342,344,16" href="/Administrator/CRM.aspx?State=hi">
	</map>
	<div class="DivTitleBarMenu"><span class="Header Bold Color1">Map of Demo Leads. Click on the State to see a list of leads within the state</span></div>
	<table align="center" width="780">
		<tr>
			<td><center></center></td>
		</tr>
		<tr>
			<td align="center"><img src="/pics/US_MAP.gif" border="0" usemap="#usmap"></td>
		</tr>
	</table>
	<table cellpadding="2" cellspacing="2" width="360" align="center">
		<tr>
			<td><asp:Label CssClass="LinkNormal" id="lblContent" runat="server" BorderWidth="1" BorderStyle="Solid" /></td>
		</tr>
	</table>
	</div>
</asp:Content>