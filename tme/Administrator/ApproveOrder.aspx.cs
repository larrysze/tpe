using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.Mail;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text ;
using System.Threading;
using System.Configuration;
using TPE.Utility;
          /******************************************************************************
          '*   1. File Name       : ApproveOrder.aspx                                   * 
          '*   2. Description     : This page shows all the details that is related to  * 
		  '*                        that paticular offer as on order confirmation page  *
		  '*	    				and in this page admin can approve or deny the      * 
		  '*					    order.if admin deny the order then that particular  *
		  '*    					order is removed from the table and user get the    *
		  '*						information through mail.if order approve the user  *
		  '*					    get the approve  order information mail and data    *
		  '*						stores in the database                              *                                 
          '*						                                                    *
          '*   3. Creation Details :                                                     *
          '*     Ver No.       Date          Author             Modification            *
          '*   -----------------------------------------------------------------        *
          '*      1.00        23 jan       Hanu Software         Comment                *
          '*                                                                            *
          '******************************************************************************/

namespace pes1.Administrator
{
	/// <summary>
	/// Summary description for ApproveOrder.
	/// </summary>
	public partial class ApproveOrder : System.Web.UI.Page
	{
		private string TmpSId;
		private string TmpBId; 
	


		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["ID"] == null)
			{
				Response.Redirect("/default.aspx");
			}
            Master.Width = "770px";
	
			if (!IsPostBack )
			{
				Showorder(Convert.ToInt32(Request.QueryString["ORD_SRNO"].ToString()));
				FillAddresses();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		private void FillAddresses()
		{
			StringBuilder SbSql = new StringBuilder();
			SbSql.Append("SELECT DISTINCT PLACE.PLAC_ID, PLACE.PLAC_PERS +CASE ");
			SbSql.Append("when PLACE.PLAC_ADDR_ONE is not Null or ltrim(rtrim(PLACE.PLAC_ADDR_ONE))<>'' ");
			SbSql.Append("then + ' - '  + PLACE.PLAC_ADDR_ONE else '' end +CASE ");
			SbSql.Append("when PLACE.PLAC_CITY is not Null or ltrim(rtrim( PLACE.PLAC_CITY))<>'' ");
			SbSql.Append("then + ' - '  +  PLACE.PLAC_CITY else '' end +CASE ");
			SbSql.Append("when PLACE.PLAC_STAT is not Null or ltrim(rtrim( PLACE.PLAC_STAT))<>'' ");
			SbSql.Append("then + ' - '  +  PLACE.PLAC_STAT else '' end +CASE ");
			SbSql.Append("when PLACE.PLAC_CTRY  is not Null or ltrim(rtrim( PLACE.PLAC_CTRY ))<>'' ");
			SbSql.Append("then + ' - '  +  PLACE.PLAC_CTRY else '' end +CASE ");
			SbSql.Append("when PLACE.PLAC_ZIP is not Null or ltrim(rtrim(PLACE.PLAC_ZIP))<>'' ");
			SbSql.Append("then + ' - '  + PLACE.PLAC_ZIP else '' end +CASE ");
			SbSql.Append("when PLACE.PLAC_RAIL_NUM is not Null or ltrim(rtrim(PLACE.PLAC_RAIL_NUM))<>'' ");
			SbSql.Append("then + ' - '  + PLACE.PLAC_RAIL_NUM else '' end AS ADDRESS ");
			SbSql.Append("FROM PLACE where PLACE.PLAC_TYPE='W' ");
			SbSql.Append("UNION ");
			SbSql.Append("SELECT DISTINCT PLACE.PLAC_ID, PLACE.PLAC_PERS +CASE ");
			SbSql.Append("when PLACE.PLAC_ADDR_ONE is not Null or ltrim(rtrim(PLACE.PLAC_ADDR_ONE))<>'' ");
			SbSql.Append("then + ' - '  + PLACE.PLAC_ADDR_ONE else '' end +CASE ");
			SbSql.Append("when PLACE.PLAC_CITY is not Null or ltrim(rtrim( PLACE.PLAC_CITY))<>'' ");
			SbSql.Append("then + ' - '  +  PLACE.PLAC_CITY else '' end +CASE ");
			SbSql.Append("when PLACE.PLAC_STAT is not Null or ltrim(rtrim( PLACE.PLAC_STAT))<>'' ");
			SbSql.Append("then + ' - '  +  PLACE.PLAC_STAT else '' end +CASE ");
			SbSql.Append("when PLACE.PLAC_CTRY  is not Null or ltrim(rtrim( PLACE.PLAC_CTRY ))<>'' ");
			SbSql.Append("then + ' - '  +  PLACE.PLAC_CTRY else '' end +CASE ");
			SbSql.Append("when PLACE.PLAC_ZIP is not Null or ltrim(rtrim(PLACE.PLAC_ZIP))<>'' ");
			SbSql.Append("then + ' - '  + PLACE.PLAC_ZIP else '' end +CASE ");
			SbSql.Append("when PLACE.PLAC_RAIL_NUM is not Null or ltrim(rtrim(PLACE.PLAC_RAIL_NUM))<>'' ");
			SbSql.Append("then + ' - '  + PLACE.PLAC_RAIL_NUM else '' end AS ADDRESS ");
			SbSql.Append("FROM PLACE INNER JOIN COMPANY ON PLACE.PLAC_COMP = COMPANY.COMP_ID ");
			SbSql.Append("where COMPANY.COMP_ID = (Select OFFR_COMP_ID from BBOFFER where OFFR_ID=" + Convert.ToInt32(DBLibrary.ScrubSQLStringInput(lblId.Text.ToString())) + ")");
			
			using (SqlConnection Conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				Conn.Open();
				using (SqlDataReader sdrAddress = DBLibrary.GetDataReaderFromSelect(Conn,SbSql.ToString()))
				{
					ddlAdd.DataSource=sdrAddress;
					ddlAdd.DataTextField="ADDRESS";
					ddlAdd.DataValueField="PLAC_ID";
					ddlAdd.DataBind();
				}
			}
		}

		public static Random GetRandom(int seed) 
		{
			Random r = (Random)HttpContext.Current.Cache.Get("RandomNumber");
			if (r == null)
			{
				if (seed == 0)
					r = new Random();
				else
					r = new Random(seed);
				HttpContext.Current.Cache.Insert("RandomNumber",r);
			}
			return r;
		}


		private void Showorder( int ParamOrder)
		{
			string TmpStr=string.Empty;
			using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				conn.Open();
				String strSql = "select ORDR_BID,ORDR_OFFR,ORDR_PROD_SPOT,ORDR_SIZE,ORDR_MELT,ORDR_DENS,ORDR_ADDS,ORDR_TERMS,ORDR_TOTAL,ORDR_SHIPTO,ORDR_BILTO from pending_orders  where ORDR_SRNO=" + ParamOrder;
			
				using (SqlDataReader DR = DBLibrary.GetDataReaderFromSelect(conn,strSql))
				{
				
					while (DR.Read())
					{
						if (DR.IsDBNull(0))
						{
							lblId.Text= DR.GetValue(1).ToString();
							ViewState["BidOffrFlag"]="O";
						}
						else
						{
							ViewState["BidOffrFlag"]="B";
							lblId.Text= DR.GetValue(0).ToString();
						}

						lblProduct.Text= DR.GetValue(2).ToString();
						lblSize.Text= DR.GetValue(3).ToString();
						lblMelt.Text= DR.GetValue(4).ToString();
						lblDens.Text= DR.GetValue(5).ToString();
						lblAdds.Text= DR.GetValue(6).ToString();
						lblTerms.Text= DR.GetValue(7).ToString();
						lblTotal.Text= string.Format("{0:c}", Convert.ToDouble(DR.GetValue(8).ToString()));
						TmpSId= DR.GetValue(9).ToString();
						TmpBId= DR.GetValue(10).ToString();				
					}
				}
			}
			FillShipAddress(TmpSId.ToString());			
			FillBillAddress(TmpBId.ToString());
		}

		private void FillShipAddress(string TmpSId)
		{			
			string ShipAdd = String.Empty;
			
			StringBuilder SbSql = new StringBuilder();
			SbSql.Append("SELECT DISTINCT PLACE.PLAC_ID, PLACE.PLAC_PERS,");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ADDR_ONE is not Null or ltrim(rtrim(PLACE.PLAC_ADDR_ONE))<>'' ");
			SbSql.Append("then  PLACE.PLAC_ADDR_ONE else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CITY is not Null or ltrim(rtrim( PLACE.PLAC_CITY))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CITY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_STAT is not Null or ltrim(rtrim( PLACE.PLAC_STAT))<>'' ");
			SbSql.Append("then + PLACE.PLAC_STAT else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CTRY  is not Null or ltrim(rtrim( PLACE.PLAC_CTRY ))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CTRY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ZIP is not Null or ltrim(rtrim(PLACE.PLAC_ZIP))<>'' ");
			SbSql.Append("then PLACE.PLAC_ZIP else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_RAIL_NUM is not Null or ltrim(rtrim(PLACE.PLAC_RAIL_NUM))<>'' ");
			SbSql.Append("then  PLACE.PLAC_RAIL_NUM else '' end  ");
			SbSql.Append("FROM PLACE ");
			SbSql.Append("WHERE PLACE.PLAC_ID = '" + DBLibrary.ScrubSQLStringInput(TmpSId) + "'");

			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
			
				conn.Open();
				using (SqlDataReader DR = DBLibrary.GetDataReaderFromSelect(conn,SbSql.ToString()))
				{
					//CHANGES
			
					//SqlCommand Cmd = new SqlCommand(StrCmd,conn);
					if (DR.Read())
					{
						// ShipAdd+=DR.GetValue(8).ToString()+ "<br>" ;
						ShipAdd+=DR.GetValue(1).ToString()+ "<br>" ;
						ShipAdd+=DR.GetValue(2).ToString()+ "<br>" ;
						ShipAdd+=DR.GetValue(3).ToString()+ "<br>" ;
						ShipAdd+=DR.GetValue(4).ToString()+ "<br>" ;
						ShipAdd+=DR.GetValue(5).ToString()+ "<br>" ;
						ShipAdd+=DR.GetValue(6).ToString()+ "<br>" ;
						ShipAdd+=DR.GetValue(7).ToString()  ;
					}
				}
			}
			//shows the shipto address in label
			lblShip.Text=ShipAdd;			
		}

		private void FillBillAddress(string TmpBId)
		{
			string BillAdd = String.Empty;
			
			StringBuilder SbSql = new StringBuilder();
			SbSql.Append("SELECT DISTINCT PLACE.PLAC_ID, PLACE.PLAC_PERS,");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ADDR_ONE is not Null or ltrim(rtrim(PLACE.PLAC_ADDR_ONE))<>'' ");
			SbSql.Append("then  PLACE.PLAC_ADDR_ONE else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CITY is not Null or ltrim(rtrim( PLACE.PLAC_CITY))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CITY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_STAT is not Null or ltrim(rtrim( PLACE.PLAC_STAT))<>'' ");
			SbSql.Append("then + PLACE.PLAC_STAT else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CTRY  is not Null or ltrim(rtrim( PLACE.PLAC_CTRY ))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CTRY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ZIP is not Null or ltrim(rtrim(PLACE.PLAC_ZIP))<>'' ");
			SbSql.Append("then PLACE.PLAC_ZIP else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_RAIL_NUM is not Null or ltrim(rtrim(PLACE.PLAC_RAIL_NUM))<>'' ");
			SbSql.Append("then  PLACE.PLAC_RAIL_NUM else '' end  ");
			SbSql.Append("FROM PLACE ");
			SbSql.Append("WHERE PLACE.PLAC_ID = '" + DBLibrary.ScrubSQLStringInput(TmpBId) + "'");

			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
				using (SqlDataReader DR = DBLibrary.GetDataReaderFromSelect(conn,SbSql.ToString()))
				{
					if (DR.Read())
					{
						//BillAdd+=DR.GetValue(8).ToString()+"<br>" ;
						BillAdd+=DR.GetValue(1).ToString()+"<br>" ;
						BillAdd+=DR.GetValue(2).ToString()+"<br>" ;
						BillAdd+=DR.GetValue(3).ToString()+"<br>" ;
						BillAdd+=DR.GetValue(4).ToString()+"<br>" ;
						BillAdd+=DR.GetValue(5).ToString()+"<br>" ;
						BillAdd+=DR.GetValue(6).ToString()+"<br>" ;
						BillAdd+=DR.GetValue(7).ToString()  ;


					}
				}
			}
			//shows the billto address in label
			lblBill.Text=BillAdd;
		}


		protected void btnApprove_ServerClick(object sender, System.EventArgs e)
		{
			bool exec = true;
			if (ViewState["BidOffrFlag"].ToString()=="O")
			{
				exec = FillFinalOrder();
			}
			else
			{
				exec = FillFinalBid();
			}
			
			if (!exec)
			{
				//Message saying the offer is no longer available
				lblMessage.Text = "You cannot approve this order. The entry is no longer available.";
			}
			else
			{			
				//	mail code is given below
				DoApproveDeny(1);
				Response.Redirect("PendingTransactions.aspx");
			}
		}

		private bool FillFinalBid()
		{
			SqlTransaction trans = null;
			string SbSql = string.Empty;
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			SqlCommand cmdInsert= new SqlCommand();
			cmdInsert.CommandType=CommandType.Text;
			SqlCommand GetCmd = new SqlCommand();
			GetCmd.CommandType=CommandType.Text;
			bool ret = false;

			try
			{
				conn.Open();
				
				using (SqlConnection conGet = new SqlConnection(Application["DBconn"].ToString()))
				{
					conGet.Open();

					trans = conn.BeginTransaction("StartInsertion");		//TRANSACTION BEGINS

					bool bCheck = false;
					SqlCommand cmdCheck = new SqlCommand();
					cmdCheck.Connection = conGet;
					cmdCheck.CommandType=CommandType.Text;
					cmdCheck.CommandText = "SELECT 1 FROM BBID WHERE BID_ID = " + DBLibrary.ScrubSQLStringInput(Request.QueryString["BIDOFFR_ID"].ToString());
					SqlDataReader drCheck = cmdCheck.ExecuteReader();
					if (drCheck.Read()) bCheck=true;
					drCheck.Close();
					cmdCheck.Dispose();
					
					if (bCheck)
					{
						cmdInsert.Connection=conn;
						cmdInsert.Transaction=trans;
				
						GetCmd.Connection=conGet;

						//here1
						GetCmd.CommandText="SELECT   BBID.BID_ID, GETDATE() AS ORDR_DATE, BBID.BID_PROD, BBID.BID_MELT, BBID.BID_DENS, BBID.BID_ADDS, 1 AS ORDR_ISSPOT,  ";
						GetCmd.CommandText+="  BBID.BID_QTY, BBID.BID_PRCE, BBID.BID_PRCE_SELL, BBID.BID_SIZE, BBID.BID_TERM, BBID.BID_PAY, BBID.BID_PERS_ID, BBID.BID_TO, 0 AS COMMISSION,  ";
						GetCmd.CommandText+="   PENDING_ORDERS.ORDR_FREIGHT, PENDING_ORDERS.ORDR_SHIPFROM, PENDING_ORDERS.ORDR_SHIPTO, PENDING_ORDERS.CREDIT_ID,BBID.BID_GRADE, BBID.BID_QUALT, ORDR_PO_NUM ";
						GetCmd.CommandText+=" FROM  PENDING_ORDERS INNER JOIN BBID ON PENDING_ORDERS.ORDR_BID = BBID.BID_ID and PENDING_ORDERS.ORDR_SRNO = " + Convert.ToInt32(DBLibrary.ScrubSQLStringInput(Request.QueryString["ORD_SRNO"].ToString()));
						SqlDataReader DRBid;
						DRBid = GetCmd.ExecuteReader();
						while (DRBid.Read())
						{ 
				
							//here2
							SbSql="Insert into ORDERS(ORDR_BID,ORDR_DATE,ORDR_PROD_SPOT,ORDR_MELT,ORDR_DENS,ORDR_ADDS,ORDR_ISSPOT,ORDR_GRADE, ORDR_PRIME) values (" ;
							SbSql+= DRBid["BID_ID"] + ",'" + DRBid["ORDR_DATE"] + "','" + DRBid["BID_PROD"]+ "','" ;
											
							if (DRBid.IsDBNull(3))
							{
								SbSql+= "','";
							}
							else
							{
								SbSql+=	DRBid["BID_MELT"] + "','";
							}
							if (DRBid.IsDBNull(4))
							{
								SbSql+= "','";
							}
							else
							{
								SbSql+=	DRBid["BID_DENS"] + "','";
							}
							if (DRBid.IsDBNull(5))
							{
								SbSql+= "',";
							}
							else
							{
								SbSql+=	DRBid["BID_ADDS"] + "',";
							}
						
							SbSql+=	DRBid["ORDR_ISSPOT"]+ ",";	
															
							if (DRBid.IsDBNull(19))
							{
								SbSql+= ",";
							}
							else
							{
								SbSql+=	DRBid["BID_GRADE"] + ",";
							}

							//here3
							if (DRBid.IsDBNull(20))
							{
								SbSql+= "0)";
							}
							else
							{
								if(DRBid[20].ToString() == "P"  )
								{
									SbSql+=	"1)";
								}
								else
								{
									SbSql+= "0)";
								}
							}
							cmdInsert.CommandText=SbSql.ToString();
							cmdInsert.ExecuteNonQuery();
							cmdInsert.CommandText="Select @@Identity from ORDERS";
							int TmpOrdId = Convert.ToInt32(cmdInsert.ExecuteScalar());
					

							//double ShipBuyPrice=0;
							//double ShipPrice=Convert.ToDouble(DRBid["BID_PRCE"]);
							int myNumber=0;
							//ShipBuyPrice= ShipPrice + 0.02 + (Convert.ToDouble(DRBid["ORDR_FREIGHT"])/Convert.ToDouble(DRBid["BID_SIZE"]) );
						
						
							if  (Convert.ToInt32(DRBid["BID_QTY"])>1)
							{
								for (int i=1;i<=Convert.ToInt32(DRBid["BID_QTY"]);i++)
								{
									Random r = GetRandom(0);

									// Produce a number from 00000 through 99999
									lock (r) 
									{
										//String myNumber = r.Next(100,999).ToString("000");
										myNumber = r.Next(100,999);
									}
									SbSql="Insert into SHIPMENT(SHIPMENT_ORDR_ID,SHIPMENT_SKU,SHIPMENT_BUYR,SHIPMENT_SELR,SHIPMENT_FROM,SHIPMENT_TO,SHIPMENT_QTY,";
									SbSql+="SHIPMENT_PRCE,SHIPMENT_COMM,SHIPMENT_SHIP_PRCE,SHIPMENT_SIZE,SHIPMENT_SHIP_STATUS,SHIPMENT_FOB,SHIPMENT_SELLER_TERMS,SHIPMENT_BUYR_PRCE,SHIPMENT_BUYER_TERMS, SHIPMENT_PO_NUM) values (";
									SbSql+=TmpOrdId + ",'" + myNumber + "'," + Convert.ToInt32(Request.QueryString["USER_ID"].ToString()) + "," +  DRBid["BID_PERS_ID"] + "," ;
									SbSql+=DRBid["ORDR_SHIPFROM"] + "," + DRBid["ORDR_SHIPTO"] + "," + DRBid["BID_QTY"] + "," + DRBid["BID_PRCE"] + "," ;
									SbSql+=DRBid["COMMISSION"] + "," + DRBid["ORDR_FREIGHT"] + "," + DRBid["BID_SIZE"] + ",1,'" + DRBid["BID_TERM"] + "'," +  DRBid["BID_PAY"] + ",";
									SbSql+=DRBid["BID_PRCE_SELL"] + "," + DRBid["BID_PAY"] + ", '" + DRBid["ORDR_PO_NUM"] + "')";
									cmdInsert.CommandText=SbSql.ToString();
									cmdInsert.ExecuteNonQuery();
								}
							}
							else
							{
								Random r = GetRandom(0);

								// Produce a number from 00000 through 99999
								lock (r) 
								{
									//String myNumber = r.Next(100,999).ToString("000");
									myNumber = r.Next(100,999);
								}
								SbSql="Insert into SHIPMENT(SHIPMENT_ORDR_ID,SHIPMENT_SKU,SHIPMENT_BUYR,SHIPMENT_SELR,SHIPMENT_FROM,SHIPMENT_TO,SHIPMENT_QTY,";
								SbSql+="SHIPMENT_PRCE,SHIPMENT_COMM,SHIPMENT_SHIP_PRCE,SHIPMENT_SIZE,SHIPMENT_SHIP_STATUS,SHIPMENT_FOB,SHIPMENT_SELLER_TERMS,SHIPMENT_BUYR_PRCE,SHIPMENT_BUYER_TERMS, SHIPMENT_PO_NUM) values (";
								SbSql+=TmpOrdId + ",'"+ myNumber +"'," + Convert.ToInt32(DBLibrary.ScrubSQLStringInput(Request.QueryString["USER_ID"].ToString())) + "," +  DRBid["BID_PERS_ID"] + "," ;
								SbSql+=DRBid["ORDR_SHIPFROM"] + "," + DRBid["ORDR_SHIPTO"] + "," + DRBid["BID_QTY"] + "," + DRBid["BID_PRCE"] + "," ;
								SbSql+=DRBid["COMMISSION"] + "," + DRBid["ORDR_FREIGHT"] + "," + DRBid["BID_SIZE"] + ",1,'" + DRBid["BID_TERM"] + "'," +  DRBid["BID_PAY"] + ",";
								SbSql+=DRBid["BID_PRCE_SELL"] + "," + DRBid["BID_PAY"] + ",'"  + DRBid["ORDR_PO_NUM"] + "')";
								cmdInsert.CommandText=SbSql.ToString();
								cmdInsert.ExecuteNonQuery();
							}
							cmdInsert.CommandText="Update Place set Plac_comp=(Select Pers_Comp from Person where Pers_ID=( Select Person_Id from Pending_Orders where ORDR_SRNO = " + Convert.ToInt32(DBLibrary.ScrubSQLStringInput(Request.QueryString["ORD_SRNO"].ToString())) + ")) where Place.Plac_Comp is Null and Place.PLAC_ID =" + DRBid["ORDR_SHIPTO"].ToString() ;
							cmdInsert.ExecuteNonQuery();
							DeleteRecord(Convert.ToInt32(DRBid["CREDIT_ID"]));
						}
						DRBid.Close();
					}
					trans.Commit();

					//Sending the bid to Archive
					SqlCommand cmd = new SqlCommand("exec spSendToArchive @Is_Bid='1',@ID='"+DBLibrary.ScrubSQLStringInput(Request.QueryString["BIDOFFR_ID"].ToString())+"'", conn);
					cmd.ExecuteNonQuery();
					ret = true;
				}
			}
			catch (Exception e)
			{
				trans.Rollback("StartInsertion");
				throw (e);
			} 
			finally 
			{
				if (conn != null) 
				{
					conn.Close();
				}
			}
			return ret;
		}
		private bool FillFinalOrder()
		{
			SqlTransaction trans = null;
			string SbSql = string.Empty;
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			SqlConnection conGet = new SqlConnection(Application["DBConn"].ToString());
			SqlCommand cmdInsert= new SqlCommand();
			cmdInsert.CommandType=CommandType.Text;
			SqlCommand GetCmd = new SqlCommand();
			GetCmd.CommandType=CommandType.Text;
			bool ret = false;

			try
			{
				conn.Open();
				conGet.Open();
				
				trans = conn.BeginTransaction("StartInsertion");		//TRANSACTION BEGINS

				bool bCheck = false;
				SqlCommand cmdCheck = new SqlCommand();
				cmdCheck.CommandType=CommandType.Text;
				cmdCheck.CommandText = "SELECT 1 FROM BBOFFER WHERE OFFR_ID = " + DBLibrary.ScrubSQLStringInput(Request.QueryString["BIDOFFR_ID"].ToString());
				cmdCheck.Connection = conGet;
				SqlDataReader drCheck = cmdCheck.ExecuteReader();
				if (drCheck.Read()) bCheck=true;
				drCheck.Close();
				cmdCheck.Dispose();
				
				if (bCheck)
				{
					cmdInsert.Connection=conn;
					cmdInsert.Transaction=trans;
			
					GetCmd.Connection=conGet;

					//here4
					GetCmd.CommandText="SELECT BBOFFER.OFFR_ID, GETDATE() AS ORDR_DATE, BBOFFER.OFFR_PROD, BBOFFER.OFFR_MELT, BBOFFER.OFFR_DENS, BBOFFER.OFFR_ADDS,1 AS ORDR_ISSPOT, ";
					GetCmd.CommandText+=" BBOFFER.OFFR_QTY, BBOFFER.OFFR_PRCE, BBOFFER.OFFR_SIZE, BBOFFER.OFFR_TERM, BBOFFER.OFFR_PAY,BBOFFER.OFFR_PERS_ID, BBOFFER.OFFR_FROM, ";
					GetCmd.CommandText+=" 0 AS COMMISSION ,PENDING_ORDERS.ORDR_FREIGHT,PENDING_ORDERS.ORDR_SHIPFROM,PENDING_ORDERS.ORDR_SHIPTO, PENDING_ORDERS.CREDIT_ID,BBOFFER.OFFR_GRADE, BBOFFER.OFFR_PRCE_BUY, BBOFFER.OFFR_QUALT, ORDR_PO_NUM";
					GetCmd.CommandText+=" FROM BBOFFER INNER JOIN  PENDING_ORDERS ON BBOFFER.OFFR_ID = PENDING_ORDERS.ORDR_OFFR and PENDING_ORDERS.ORDR_SRNO = " + Convert.ToInt32(DBLibrary.ScrubSQLStringInput(Request.QueryString["ORD_SRNO"].ToString()));
					SqlDataReader DROffr;
					DROffr = GetCmd.ExecuteReader();
					while (DROffr.Read())
					{ 

						//here5
						SbSql="Insert into ORDERS(ORDR_OFFR,ORDR_DATE,ORDR_PROD_SPOT,ORDR_MELT,ORDR_DENS,ORDR_ADDS,ORDR_ISSPOT,ORDR_GRADE, ORDR_PRIME) values " ;
						SbSql+="(@field1, @field2,@field3, @field4, @field5, @field6, @field7, @field8, @field9)";
						cmdInsert.CommandText=SbSql.ToString();
						//Creating Parameters for the command
						cmdInsert.Parameters.Add("@field1",SqlDbType.Int);
						cmdInsert.Parameters.Add("@field2",SqlDbType.DateTime);
						cmdInsert.Parameters.Add("@field3",SqlDbType.VarChar);
						cmdInsert.Parameters.Add("@field4",SqlDbType.Char);
						cmdInsert.Parameters.Add("@field5",SqlDbType.Char);
						cmdInsert.Parameters.Add("@field6",SqlDbType.Char);
						cmdInsert.Parameters.Add("@field7",SqlDbType.Bit);
						cmdInsert.Parameters.Add("@field8",SqlDbType.Int);
						//here6
						cmdInsert.Parameters.Add("@field9",SqlDbType.Int);
						//Assigning Values to the Parameters 
						cmdInsert.Parameters["@field1"].Value=DROffr["OFFR_ID"];
						cmdInsert.Parameters["@field2"].Value=DROffr["ORDR_DATE"];
						cmdInsert.Parameters["@field3"].Value=DROffr["OFFR_PROD"];

						if (DROffr.IsDBNull(3))
						{
							cmdInsert.Parameters["@field4"].Value=System.DBNull.Value;
						}
						else
						{
							cmdInsert.Parameters["@field4"].Value=DROffr["OFFR_MELT"];
						}
						if (DROffr.IsDBNull(4))
						{
							cmdInsert.Parameters["@field5"].Value=System.DBNull.Value;
						}
						else
						{
							cmdInsert.Parameters["@field5"].Value=DROffr["OFFR_DENS"];
						}
						if (DROffr.IsDBNull(5))
						{
							cmdInsert.Parameters["@field6"].Value=System.DBNull.Value;
						}
						else
						{
							cmdInsert.Parameters["@field6"].Value=DROffr["OFFR_ADDS"];
						}

						cmdInsert.Parameters["@field7"].Value=DROffr["ORDR_ISSPOT"];
						if (DROffr.IsDBNull(19))
						{
							cmdInsert.Parameters["@field8"].Value=System.DBNull.Value;
						}
						else
						{
							cmdInsert.Parameters["@field8"].Value=DROffr["OFFR_GRADE"] ;
						}

						//here7
						if (DROffr.IsDBNull(20))
						{
							cmdInsert.Parameters["@field9"].Value="0";
						}
						else
						{
							if(DROffr[20].ToString() == "P"  )
							{
								cmdInsert.Parameters["@field9"].Value="1";
							}
							else
							{
								cmdInsert.Parameters["@field9"].Value="0";
							}
						}

						//Addition Over

						cmdInsert.ExecuteNonQuery();
						cmdInsert.CommandText="Select @@Identity from ORDERS";
						int TmpOrdId = Convert.ToInt32(cmdInsert.ExecuteScalar());
				
						//Calculating Shipment_Buyr_Prce;
						//double ShipPrice=Convert.ToDouble(DROffr["OFFR_PRCE"]);
						int myNumber=0;
						//ShipBuyPrice= ShipPrice + 0.02 + (Convert.ToDouble(DROffr["ORDR_FREIGHT"])/Convert.ToDouble(DROffr["OFFR_SIZE"]) );
						//SHIPMENT_QTY will always be one expect when the size=1 (lbs) Assumed 1 for each order

						if  (Convert.ToInt32(DROffr["OFFR_QTY"])>1)
						{
							for (int i=1;i<=Convert.ToInt32(DROffr["OFFR_QTY"]);i++)

							{
								Random r = GetRandom(0);

								// Produce a number from 00000 through 99999
								lock (r) 
								{
									//String myNumber = r.Next(100,999).ToString("000");
									myNumber = r.Next(100,999);
								}

								SbSql="Insert into SHIPMENT(SHIPMENT_ORDR_ID,SHIPMENT_SKU,SHIPMENT_BUYR,SHIPMENT_SELR,SHIPMENT_FROM,SHIPMENT_TO,SHIPMENT_QTY,";
								SbSql+="SHIPMENT_PRCE,SHIPMENT_COMM,SHIPMENT_SHIP_PRCE,SHIPMENT_SIZE,SHIPMENT_SHIP_STATUS,SHIPMENT_FOB,SHIPMENT_SELLER_TERMS,SHIPMENT_BUYR_PRCE,SHIPMENT_BUYER_TERMS, SHIPMENT_PO_NUM) values (";
								SbSql+=TmpOrdId + ",'"+ myNumber +"'," + Convert.ToInt32(DBLibrary.ScrubSQLStringInput(Request.QueryString["USER_ID"].ToString())) + "," +  DROffr["OFFR_PERS_ID"] + "," ;
								SbSql+= ddlAdd.SelectedValue.ToString()  + "," + DROffr["ORDR_SHIPTO"] + ",1," + DROffr["OFFR_PRCE_BUY"] + "," ; //DROffr["ORDR_SHIPFROM"]
								SbSql+=DROffr["COMMISSION"] + "," + DROffr["ORDR_FREIGHT"] + "," + DROffr["OFFR_SIZE"] + ",1,'" + DROffr["OFFR_TERM"] + "'," +  DROffr["OFFR_PAY"] + ",";
								SbSql+=DROffr["OFFR_PRCE"] + "," + DROffr["OFFR_PAY"] + ",'" + DROffr["ORDR_PO_NUM"] + "')";
								cmdInsert.CommandText=SbSql.ToString();
								cmdInsert.ExecuteNonQuery();
							}
						}//" + DROffr["OFFR_QTY"] + "  ######### Qty field gets inserted as 1 Same to be implemented in below query for qty.
						else
						{
							Random r = GetRandom(0);

							// Produce a number from 00000 through 99999
							lock (r) 
							{
								//String myNumber = r.Next(100,999).ToString("000");
								myNumber = r.Next(100,999);
							}
							SbSql="Insert into SHIPMENT(SHIPMENT_ORDR_ID,SHIPMENT_SKU,SHIPMENT_BUYR,SHIPMENT_SELR,SHIPMENT_FROM,SHIPMENT_TO,SHIPMENT_QTY,";
							SbSql+="SHIPMENT_PRCE,SHIPMENT_COMM,SHIPMENT_SHIP_PRCE,SHIPMENT_SIZE,SHIPMENT_SHIP_STATUS,SHIPMENT_FOB,SHIPMENT_SELLER_TERMS,SHIPMENT_BUYR_PRCE,SHIPMENT_BUYER_TERMS, SHIPMENT_PO_NUM) values (";
							SbSql+=TmpOrdId + ",'"+ myNumber +"'," + Convert.ToInt32(DBLibrary.ScrubSQLStringInput(Request.QueryString["USER_ID"].ToString())) + "," +  DROffr["OFFR_PERS_ID"] + "," ;
							SbSql+=ddlAdd.SelectedValue.ToString() + "," + DROffr["ORDR_SHIPTO"] + ",1," + DROffr["OFFR_PRCE_BUY"] + "," ;
							SbSql+=DROffr["COMMISSION"] + "," + DROffr["ORDR_FREIGHT"] + "," + DROffr["OFFR_SIZE"] + ",1,'" + DROffr["OFFR_TERM"] + "'," +  DROffr["OFFR_PAY"] + ",";
							SbSql+=DROffr["OFFR_PRCE"] + "," + DROffr["OFFR_PAY"] + ",'" + DROffr["ORDR_PO_NUM"] + "')";
							cmdInsert.CommandText=SbSql.ToString();
							cmdInsert.ExecuteNonQuery();
						}
						cmdInsert.CommandText="Update Place set Plac_comp=(Select Pers_Comp from Person where Pers_ID=( Select Person_Id from Pending_Orders where ORDR_SRNO = " + Convert.ToInt32(DBLibrary.ScrubSQLStringInput(Request.QueryString["ORD_SRNO"].ToString())) + ")) where Place.Plac_Comp is Null and Place.PLAC_ID =" + DROffr["ORDR_SHIPTO"].ToString() ;
						cmdInsert.ExecuteNonQuery();
						DeleteRecord(Convert.ToInt32(DROffr["CREDIT_ID"]));
				
					}
					DROffr.Close();
					conGet.Close();
					trans.Commit();

					//Sending the offer to Archive
					SqlCommand cmd = new SqlCommand("exec spSendToArchive  @ID='"+DBLibrary.ScrubSQLStringInput(Request.QueryString["BIDOFFR_ID"].ToString())+"'", conn);
					cmd.ExecuteNonQuery();
					ret = true;
				}
			}
			catch (Exception e)
			{
				trans.Rollback("StartInsertion");
				throw (e);
			} 
			finally 
			{
				if (conn != null) 
				{
					conn.Close();
				}
			}  
			return ret;
		}

		protected void btnDeny_ServerClick(object sender, System.EventArgs e)
		{
			//Subroutine for denying the order approval
	
			DeleteRecord(Convert.ToInt32(DBLibrary.ScrubSQLStringInput(Request.QueryString["CREDIT_ID"].ToString())));
			//*******************************/
			//		Mailing Code  Below //
			//*******************************/
			DoApproveDeny(0);
			Response.Redirect("PendingTransactions.aspx");
			


		}
		private void DoApproveDeny(int DenyApprove)
		{
			//Mailing Code 
			StringBuilder sbHTML =  new StringBuilder() ;
			string RecId = String.Empty;

			using (SqlConnection Conn =  new SqlConnection(Application["DBConn"].ToString()))
			{
				Conn.Open();
				SqlCommand SqlChkType = new SqlCommand("Select PERS_MAIL from person where PERS_ID=" + Convert.ToInt32(DBLibrary.ScrubSQLStringInput(Request.QueryString["USER_ID"].ToString())) ,Conn);
				RecId = Convert.ToString(SqlChkType.ExecuteScalar());
			}
			MailMessage EmailOrdr = new MailMessage();
			
			if (DenyApprove==1)
			{
				EmailOrdr.Subject = "Order Approved .";

				sbHTML.Append("<Table><TR><TD><h3>Congratulations...! </h3></TD></TR>");
				sbHTML.Append("<TR><TD>Dear Customer...</TD></TR>");
				sbHTML.Append("<tr><td align='left' colspan='2'><font size='3'>Your Order has been accepted and under process.  </font></td></tr>");
				sbHTML.Append("<tr><td align='left' colspan='2'><font size='3'>You will be Contacted soon. </font></td></tr><tr></tr><tr></tr>");
				sbHTML.Append("<tr><td align='left' colspan='2'><font size='3'><b>Regards </b></font></td></tr></Table>");
				sbHTML.Append("<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "'>The Plastics Exchange</a>");
		
			}
			else
			{
				EmailOrdr.Subject = "Order denied .";

				sbHTML.Append("<Table><TR><TD>Dear Customer...</TD></TR>");
				sbHTML.Append("<tr><td align='left' colspan='2'><font size='3'>We regret to inform that your Order has been denied for further processing.  </font></td></tr><tr></tr><tr></tr>");
				sbHTML.Append("<tr><td align='left' colspan='2'><font size='3'><b>Regards </b></font></td></tr></Table>");
				sbHTML.Append("<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "'>The Plastics Exchange</a>");


			}
			EmailOrdr.From = Application["strEmailAdmin"].ToString();
			EmailOrdr.To = RecId;
			EmailOrdr.Cc = Application["strEmailOwner"].ToString();
			
			EmailOrdr.Body = sbHTML.ToString();
			EmailOrdr.BodyFormat = MailFormat.Html;
			//SmtpMail.SmtpServer="caesar";
			//TPE.Utility.EmailLibrary.Send(Context,EmailOrdr);
		}
		private void DeleteRecord(int ParaCredit)
		{
			if (ParaCredit!=0)
			{
				DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), "Delete from Credit_App where Id=" + ParaCredit);
			}			
			DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(),"Delete from Pending_Orders where ORDR_SRNO=" + Convert.ToInt32(DBLibrary.ScrubSQLStringInput(Request.QueryString["ORD_SRNO"].ToString())));
		}
	}
}
