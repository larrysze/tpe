using System;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.Mail;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Threading;
using System.Configuration;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class SendEmail : System.Web.UI.Page 
	{
		private DataSet dsEmails;
		public string EmailTitle;
		public string EmailBody;
		public string EmailSalutation;
		private int totalUsers = 0;
		private int MinLeadsToEmailInThread;
		private int MaxLeadsToDisplay;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{			
			if (bool.Parse(Application["IsDebug"].ToString()))
			{
				MinLeadsToEmailInThread = 5;
			}
			else
			{
				MinLeadsToEmailInThread = Int32.Parse(Application["MinLeadsToEmailInThread"].ToString());
			}
			MaxLeadsToDisplay= Int32.Parse(Application["MaxLeadsToDisplayOnSendEmailScreen"].ToString());
				
			if(!Page.IsPostBack)
			{
				string strEmailType = "";
				if(Request.QueryString["Letter"] != "1")
					strEmailType = (Session["strCompId"] == null ? "Offers": "Bids");
				else
					strEmailType = "Letters";

				SetEmailFields(strEmailType);
				//Displaying info on the page
				txtSubject.Text = EmailTitle;
				txtBody.Text = EmailBody;
				txtSalutation.Text = EmailSalutation;				
				
				string strUserIds= "";
				if (Session["strEmailOffer_Address"]!= null)
				{
					strUserIds = Session["strEmailOffer_Address"].ToString();
					if (strUserIds.Length > 0)
					{
                        if (strUserIds.Substring(strUserIds.Length - 1, 1) == ",")
                        {
                            strUserIds = strUserIds.Substring(0, strUserIds.Length - 1);
                            string[] users = strUserIds.Split(',');
                            totalUsers = users.Length;
                        }
                        else 
                        {
                            totalUsers = 1;

                        }
                        Session["totalUsers"] = totalUsers;
					}
				}

//				Session["UserIds"] = strUserIds;
				if(Request.QueryString["Letter"] == "1")
				{
					lblItens.Visible = false;
					Session["dsEmails"] = GetUsers(strUserIds);
					// rg changed:
					Session["UserIds"] = strUserIds;
					Session["EmailType"] = strEmailType;					

				}
				else
				{					
					ViewState["DisplayPreferences"]=true; //user's preferences

					string BidsOffersCondition = "";
					if (Session["strEmailOffer_IDS"] != null)
					{
						//ViewState["DisplayPreferences"]=false;
						BidsOffersCondition = Session["strEmailOffer_IDS"].ToString();
						if (BidsOffersCondition.Length > 0)
						{
							if (BidsOffersCondition.Substring(BidsOffersCondition.Length-1,1)==",")
							{
								BidsOffersCondition = BidsOffersCondition.Substring(0,BidsOffersCondition.Length-1);
							}
						}
					}
					

					string GradesCondition = "";
					if (Session["strEmailGrades"] != null)
					{
						GradesCondition = Session["strEmailGrades"].ToString();
						if (GradesCondition.Length > 0)
						{
							if (GradesCondition.Substring(GradesCondition.Length-1,1)==",")
							{
								GradesCondition = GradesCondition.Substring(0,GradesCondition.Length-1);
							}
						}
					}

					bool useUserPreferences = (bool)ViewState["DisplayPreferences"];
					//if (BidsOffersCondition.Length>0) useUserPreferences = false;

					Session["EmailType"] = strEmailType;					
					if(totalUsers >= MinLeadsToEmailInThread)
					{
						Session["EmailDataReady"] = 0;
						Session["useUserPreferences"] = useUserPreferences;
						Session["UserIds"] = strUserIds;
						Session["BidsOffersCondition"] = BidsOffersCondition;
						Session["GradesCondition"] = GradesCondition;
						lblItens.Visible = true;
						lblItens.Text = totalUsers + " users selected. <BR>" +
							"Since number of leads you selected is more than " + (MinLeadsToEmailInThread - 1) + " you can only change the text of the email." ;
					}
					else
					{
						ContainerClassForEmails emails = new ContainerClassForEmails(Application["DBConn"].ToString(), bool.Parse(Application["IsDebug"].ToString()));

						dsEmails = emails.GetOffersBids(useUserPreferences,strUserIds,BidsOffersCondition,GradesCondition,strEmailType);				
						dsEmails = emails.PrepareTransactions(dsEmails, strEmailType);
						
						dgUsers.DataSource = dsEmails.Tables[0];
						dgUsers.DataBind();

						//Keep info about Items to send
						Session["dsEmails"] = dsEmails;
					}	
				}
			}
		}
		//
		//		private void PrepareDataForEmails()
		//		{
		//			Response.Write(DateTime.Now);
		//
		//			bool useUserPreferences = Convert.ToBoolean(Session["useUserPreferences"]);
		//			string usersCondition = Session["usersCondition"].ToString();
		//			string BidsOffersCondition = Session["BidsOffersCondition"].ToString();
		//			string GradesCondition = Session["GradesCondition"].ToString();
		//			string transaction = Session["transaction"].ToString();
		//
		//			dsEmails = GetOffersBids(useUserPreferences,usersCondition,BidsOffersCondition,GradesCondition,transaction);				
		//			dsEmails = PrepareTransactions(dsEmails, transaction);
		////			SetEmailFields(dsEmails,transaction);
		//			Response.Write(DateTime.Now);
		//
		//			//Keep info about Itens to send
		//			Session["dsEmails"] = dsEmails;
		//			Session["EmailDataReady"] = 1;
		//		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgUsers.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgUsers_ItemDataBound);

		}
		#endregion

		public DataSet GetUsers(string usersCondition)
		{
			DataSet ds = new DataSet();
			using (SqlConnection Conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				SqlDataAdapter dataADP = new SqlDataAdapter();
				Conn.Open();

				//Getting User's info as Name, Email, etc.
				string sqlStatement = "select pers_id, pers_frst_name, pers_last_name, pers_mail, pers_pswd, pers_pref, pers_intrst_size, pers_intrst_qualt, pers_prmly_intrst, Email_ENBL, distance_city + ', ' + distance_state + '(' + distance_zipcode + ')' pers_zip, international_port.port_city + ', ' + international_port.port_country pers_port ";
				sqlStatement = sqlStatement + "from person, international_port, distance_zipcode ";
				sqlStatement = sqlStatement + "where person.pers_port *= international_port.port_id ";
				sqlStatement = sqlStatement + "and person.pers_zip *= distance_zipcode.distance_zipcode";
				if (usersCondition!="") sqlStatement += " and pers_id in (" + usersCondition + ")";
				sqlStatement += " order by pers_frst_name, pers_last_name";
				dataADP = new SqlDataAdapter();
				dataADP.SelectCommand=new SqlCommand(sqlStatement,Conn);
				dataADP.SelectCommand.CommandType=CommandType.Text;
				dataADP.Fill(ds,"Users");
			}
			return ds;
		}

		private void dgUsers_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				Label title = ((System.Web.UI.WebControls.Label)e.Item.FindControl("lblTitle"));
				title.Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"pers_frst_name"))+" "+String.Format("{0}",DataBinder.Eval(e.Item.DataItem, "pers_last_name"));
				
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblPersID")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"pers_id"));
				
				if (String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Email_ENBL"))=="False")
				{
					title.Text +=":";
					((System.Web.UI.WebControls.Label)e.Item.FindControl("lblMessage")).Text = "This user has chosen do not receive emails.";
				}
				else
				{
					string pers_id = String.Format("{0}",DataBinder.Eval(e.Item.DataItem, "pers_id"));
					System.Web.UI.WebControls.DataGrid currentGrid=(System.Web.UI.WebControls.DataGrid)e.Item.FindControl("dgEmail");
					DataTable dtBind = dsEmails.Tables[1].Clone();
					DataRow[] dtRows = dsEmails.Tables[1].Select("pers_id = '" + String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"pers_id")) + "'","Grade, Price");
					CopyRowsToDataTable(dtRows, dtBind);

					if (dtRows.Length<=0)
					{
						title.Text +=":";
						((System.Web.UI.WebControls.Label)e.Item.FindControl("lblMessage")).Text = "Resins not found using this user's preferences.";
					}
					else
					{
						if (totalUsers < MinLeadsToEmailInThread)
						{
							if (MaxLeadsToDisplay>0)
							{
								currentGrid.DataSource = dtBind;
								currentGrid.DataBind();
								//if (currentGrid.Items.Count==0)
								MaxLeadsToDisplay--;
							}
						}
						else
						{
							currentGrid.DataSource = dtBind;
							currentGrid.DataBind();
							//if (currentGrid.Items.Count==0)
						}
					}
				}
			}
		}

		public void dgEmail_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				((System.Web.UI.WebControls.TextBox)e.Item.FindControl("txtPrice")).Text= String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"price"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblPrice")).Text= String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"price"));
				
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblTransactionID")).Text= String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"TransactionID"));

				((System.Web.UI.HtmlControls.HtmlInputCheckBox)e.Item.Cells[1].Controls[0]).Checked= System.Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem,"selected"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblSelected")).Text= System.Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem,"selected")).ToString();
			}
		}

		public DataSet updateItems(DataSet dsEmail)
		{
			DataTable dtTransaction = dsEmail.Tables[1];

			foreach(DataGridItem userEmail in dgUsers.Items)
			{
				string persID = ((System.Web.UI.WebControls.Label)userEmail.FindControl("lblPersID")).Text;

				System.Web.UI.WebControls.DataGrid emailGrid =(System.Web.UI.WebControls.DataGrid)userEmail.FindControl("dgEmail");
				foreach(DataGridItem emailItem in emailGrid.Items)
				{
					string transactionID = ((System.Web.UI.WebControls.Label)emailItem.FindControl("lblTransactionID")).Text;
					bool selected = ((System.Web.UI.HtmlControls.HtmlInputCheckBox)emailItem.Cells[1].Controls[0]).Checked;
					string oldSelected = ((System.Web.UI.WebControls.Label)emailItem.FindControl("lblSelected")).Text;
					if(selected.ToString().ToLower() != oldSelected.ToLower())
					{
						//update item into DataTable
						DataRow dtRow = (DataRow)dtTransaction.Select("pers_id = '" + persID + "' and TransactionID = '" + transactionID + "'").GetValue(0);
						dtRow["selected"] = System.Convert.ToBoolean(selected);
						dtRow.AcceptChanges();
					}
					
					string price=((System.Web.UI.WebControls.TextBox)emailItem.FindControl("txtPrice")).Text;
					string oldPrice=((System.Web.UI.WebControls.Label)emailItem.FindControl("lblPrice")).Text;
					if (price!=oldPrice)
					{
						//update item into DataTable
						DataRow dtRow = (DataRow)dtTransaction.Select("pers_id = '" + persID + "' and TransactionID = '" + transactionID + "'").GetValue(0);
						dtRow["Price"] = price;
						dtRow.AcceptChanges();
					}
				}
			}
			return dsEmail;
		}

		protected void btnSendMail_Click(object sender, System.EventArgs e)
		{
			int totalUsers = (int)Session["totalUsers"];

			ContainerClassForEmails emails = new ContainerClassForEmails(Application["DBConn"].ToString(),bool.Parse(Application["IsDebug"].ToString()));
			string DBString = Application["DBconn"].ToString();
			string strEmailOwner = Application["strEmailOwner"].ToString();
//			string strEmail = Session["Email"].ToString();
			string strID = Session["id"].ToString();
			
/*			string strEmailBody = "";
			string strEmailTitle = "";
			string strEmailSalutation = "";
*/			


			if(totalUsers >= MinLeadsToEmailInThread)
			{
//				DataSet dsEmails;
/*
				string emailType = Session["EmailType"].ToString();
				bool useUserPreferences = Session["EmailType"].ToString();
				string userIds = Session["UserIds"].ToString();
				string BidsOffersCondition = Session["BidsOffersCondition"].ToString();
				string GradesCondition = Session["GradesCondition"].ToString();

//				dsEmails = emails.GetOffersBids(useUserPreferences, userIds, BidsOffersCondition, GradesCondition, emailType);				
//				dsEmails = emails.PrepareTransactions(dsEmails, emailType);
*/

				// threaded version

				if(Request.QueryString["Letter"] != "1")
				{
					emails.Init(Session["EmailType"].ToString(), Convert.ToBoolean(Session["useUserPreferences"]), Session["UserIds"].ToString(), Session["BidsOffersCondition"].ToString(), Session["GradesCondition"].ToString(), Session["Email"].ToString(), Application["strEmailOwner"].ToString(), txtSubject.Text, txtBody.Text, txtSalutation.Text, strID);
					Thread threadSendEmails = new Thread(new ThreadStart(emails.SendEmailsFromThread));
					threadSendEmails.Priority = ThreadPriority.Lowest;
					threadSendEmails.Start();					
				}
				else
				{
					emails.InitNoOffers( (DataSet)Session["dsEmails"], Session["Email"].ToString(), Application["strEmailOwner"].ToString(), txtSubject.Text, txtBody.Text, txtSalutation.Text, strID);
					Thread threadSendEmails = new Thread(new ThreadStart(emails.SendEmailsNoOffersFromThread));
					threadSendEmails.Priority = ThreadPriority.Lowest;
					threadSendEmails.Start();										
				}
			}
			else
			{
				//non-threaded version
				emails.InitWithoutThread(Session["EmailType"].ToString(), Session["Email"].ToString(), Application["strEmailOwner"].ToString(), txtSubject.Text, txtBody.Text, txtSalutation.Text, strID);
				if(Request.QueryString["Letter"] != "1")
				{
					updateItems((DataSet)Session["dsEmails"]);
			
					EmailTitle = txtSubject.Text;
					EmailBody = txtBody.Text;
					EmailSalutation = txtSalutation.Text;

					emails.SendEmails((DataSet)Session["dsEmails"], false);

//					emails.SendEmails(GetUsers(Session["UserIds"].ToString()), false);
				}
				else
				{
					EmailTitle = txtSubject.Text;
					EmailBody = txtBody.Text;
					EmailSalutation = txtSalutation.Text;

					emails.SendEmailsNoOffers((DataSet)Session["dsEmails"], false);
//					emails.SendEmailsNoOffers(GetUsers(Session["UserIds"].ToString()), false);
				}
			}

			Session["strCompId"] = null;
			Session["strEmailOffer_Address"] = null;
			Session["strEmailOffer_IDS"]= null;
			Session["strEmailGrades"] = null;
			Response.Redirect("/administrator/SendEmailDisclaimer.aspx");
		}

		private void SetEmailFields(string transactionType)
		{
			StringBuilder strBody = new StringBuilder("");
			StringBuilder strSalutation = new StringBuilder("");
			
			//			if (dsEmail.Tables["Users"].Rows.Count == 1)
			//			{
			//				strBody.Append("Dear "+dsEmail.Tables["Users"].Rows[0]["pers_frst_name"].ToString().Trim()+","+((char)(13)).ToString()+((char)(13)).ToString());
			//			}
			//			else
			//			{
			strBody.Append("Dear <PERSON FIRST NAME>,"+((char)(13)).ToString()+((char)(13)).ToString());
			//			}

			if (transactionType == "Offers")
			{
				EmailTitle = "Resin Offers";
				//strBody.Append("We found <TOTAL_OFFERS> offers that are currently available which matched your requirements:"+((char)(13)).ToString()+((char)(13)).ToString());
				//strBody.Append("Hurry, these offers are subject to prior sale!");
				strSalutation.Append("These and all offers are subject to prior sale."+((char)(13)).ToString()+((char)(13)).ToString());
			}
			else if(transactionType == "Letters")
			{
				EmailTitle = "The Plastic Exchange";
				strBody.Append("");
			}
			else // Bids
			{
				EmailTitle = "Resin Requests";
				strBody.Append("We are looking for the following resin today.  Please let us know if you have something close."+((char)(13)).ToString()+((char)(13)).ToString());
			}
			EmailBody = strBody.ToString();
			
			strSalutation.Append("Sincerely,"+((char)(13)).ToString()+((char)(13)).ToString());
			strSalutation.Append(Session["Name"].ToString()+((char)(13)).ToString());
			strSalutation.Append("The Plastics Exchange"+((char)(13)).ToString());
			strSalutation.Append(ConfigurationSettings.AppSettings["PhoneNumber"].ToString());
			EmailSalutation = strSalutation.ToString();
		}


		private void CopyRowsToDataTable(DataRow[] dtRows, DataTable dtTable)
		{
			foreach(DataRow dtRow in dtRows)
			{
				DataRow newRow = dtTable.NewRow();
				foreach(DataColumn column in dtTable.Columns)
				{
					newRow[column.ColumnName] = dtRow[column.ColumnName].ToString();
				}
				dtTable.Rows.Add(newRow);
			}
		}



	}	

	class ContainerClassForEmails
	{
		private string DBString = "";
		private string strEmailOwner = ""; //Context["strEmailOwner"]
		private string strEmailFrom = ""; //Context["Email"]
		private string strID = ""; //Context["id"]

		private string strEmailTitle = "";
		
		private string strSalutation = " ";
		
		private DataSet dsEmails;
		bool useUserPreferences = false; //Convert.ToBoolean(Session["useUserPreferences"]);
		string userIds = ""; //Session["UserIds"].ToString();
		string BidsOffersCondition = ""; //Session["BidsOffersCondition"].ToString();
		string GradesCondition = ""; //Session["GradesCondition"].ToString();
		string emailType = ""; //Session["EmailType"].ToString();

		string strBccEmail = "";

		string _Body = "";
		bool _IsDebug = false;

		public string Body 
		{
			get {return _Body;}
			set {_Body = value;}
		}


		public ContainerClassForEmails(string DBConn,bool IsDebug)
		{
			DBString = DBConn;
			_IsDebug = IsDebug;		
		}
		
		public void Init(string p_emailType, bool useUserPref, string p_userIds, string bidsOffersCond, string gradesCond, string p_strEmailFrom, string p_strEmailBcc, string eTitle, string eBody, string eSalutation, string p_strID)
		{
			emailType = p_emailType;
			useUserPreferences = useUserPref;
			userIds = p_userIds;
			BidsOffersCondition = bidsOffersCond;
			GradesCondition = gradesCond;
			strEmailFrom = p_strEmailFrom;
			strEmailOwner = p_strEmailBcc;
			strEmailTitle = eTitle;
			strID = p_strID;
			
			strBccEmail = p_strEmailBcc;

			Body = eBody;

			strSalutation = eSalutation;
			strSalutation = strSalutation.Replace("The Plastics Exchange","<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "'>The Plastics Exchange</a>");
			strSalutation = strSalutation.Replace("\r\n","<br>");
			strSalutation+="<BR><BR><BR><BR><BR>";
		}

		public void InitNoOffers(DataSet p_dsEmails, string p_strEmailFrom, string p_strEmailBcc, string eTitle, string eBody, string eSalutation, string p_strID)
		{
			dsEmails = p_dsEmails;
			strEmailFrom = p_strEmailFrom;
			strEmailOwner = p_strEmailBcc;
			strEmailTitle = eTitle;
			strID = p_strID;
			
			strBccEmail = p_strEmailBcc;

			Body = eBody;

			strSalutation = eSalutation;
			strSalutation = strSalutation.Replace("The Plastics Exchange","<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "'>The Plastics Exchange</a>");
			strSalutation = strSalutation.Replace("\r\n","<br>");
			strSalutation+="<BR><BR><BR><BR><BR>";
		}

		public void InitWithoutThread(string p_emailType, string p_strEmailFrom, string p_strEmailBcc, string eTitle, string eBody, string eSalutation, string p_strID)
		{
			emailType = p_emailType;
			strEmailFrom = p_strEmailFrom;
			strEmailOwner = p_strEmailBcc;
			strEmailTitle = eTitle;
			strID = p_strID;
			
			strBccEmail = p_strEmailBcc;

			Body = eBody;

			strSalutation = eSalutation;
			strSalutation = strSalutation.Replace("The Plastics Exchange","<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "'>The Plastics Exchange</a>");
			strSalutation = strSalutation.Replace("\r\n","<br>");
			strSalutation+="<BR><BR><BR><BR><BR>";

		}

		private string EmailSalutationHtml()
		{
			return strSalutation;
		}

		private string EmailHeaderHtml()
		{
			string strDomainName = ConfigurationSettings.AppSettings["DomainName"].ToString();
			string strHeader = "";
			strHeader +="		<BODY>".Trim();
			strHeader +="		<DIV id=Template1_pnHeader>".Trim();
			strHeader +="		<STYLE type=text/css>BODY {".Trim();
			strHeader +="			MARGIN: 0px; BACKGROUND-COLOR: #ffffff".Trim();
			strHeader +="		}".Trim();
			strHeader +="		#menu {".Trim();
			strHeader +="			Z-INDEX: 1; LEFT: 351px; VISIBILITY: hidden; WIDTH: 128px; POSITION: absolute; TOP: 10px; HEIGHT: 97px".Trim();
			strHeader +="		}".Trim();
			strHeader +="		#Layer1 {".Trim();
			strHeader +="			Z-INDEX: 1; LEFT: 124px; WIDTH: 779px; POSITION: absolute; TOP: 124px; HEIGHT: 155px".Trim();
			strHeader +="		}".Trim();
			strHeader +="		.border {".Trim();
			strHeader +="			BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; BACKGROUND: #dbdcd7; BORDER-LEFT: #000000 1px solid; WIDTH: 778px; TEXT-ALIGN: center".Trim();
			strHeader +="		}".Trim();
			strHeader +="		</DIV>".Trim();
			strHeader +="		<DIV id=Template1_pnContextMenu>".Trim();
			strHeader +="		<STYLE>.skin0 {".Trim();
			strHeader +="			BORDER-RIGHT: black 2px solid; BORDER-TOP: black 2px solid; FONT-SIZE: 12px; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: black 2px solid; WIDTH: 165px; CURSOR: default; LINE-HEIGHT: 20px; BORDER-BOTTOM: black 2px solid; FONT-FAMILY: Verdana; POSITION: absolute; BACKGROUND-COLOR: menu".Trim();
			strHeader +="		}".Trim();
			strHeader +="		.menuitems {".Trim();
			strHeader +="			PADDING-RIGHT: 10px; PADDING-LEFT: 10px".Trim();
			strHeader +="		}".Trim();
			strHeader +="		</STYLE>".Trim();
			strHeader +="		</DIV>".Trim();
			strHeader +="		<TABLE width=780 cellSpacing=0 cellPadding=0 align=center border=0>".Trim();
			strHeader +="		<TBODY>".Trim();
			strHeader +="		<TR>".Trim();
			strHeader +="			<TD>".Trim();
			strHeader +="			<DIV class=border align=center>".Trim();
			strHeader +="			<TABLE id=table1 cellSpacing=0 cellPadding=0 width=780 align=center ".Trim();
			strHeader +="			border=0>".Trim();
			strHeader +="				<TBODY>".Trim();
			strHeader +="				<TR><TD vAlign=top width=\"100%\">".Trim();
			strHeader +="					<TABLE cellSpacing=0 cellPadding=0 align=center border=0>".Trim();
			strHeader +="					<TBODY>".Trim();
			strHeader +="					<TR><TD height=21><A href=\"http://theplasticsexchange.com\"><IMG height=51 src=\"http://www.theplasticsexchange.com/images2/loggedmenu/logo_top_r1_c1.jpg\" border=0></A></TD> " .Trim();
			strHeader +="						<TD background=http://www.theplasticsexchange.com/images2/loggedmenu/logo_top_r1_c23.gif></TD>".Trim();
			strHeader +="						<TD class=loggedname vAlign=bottom background=http://www.theplasticsexchange.com/images2/loggedmenu/logo_top_r1_c23.gif></TD></TR> ".Trim();
			strHeader +="					<TR><TD><A href=\"http://www.theplasticsexchange.com\"><IMG height=53 src=\"http://www.theplasticsexchange.com/images2/loggedmenu/logo_top_r2_c1.jpg\" width=588 border=0></A></TD> ".Trim();
			strHeader +="						<TD background=http://www.theplasticsexchange.com/images2/loggedmenu/logo_top_r2_c1.jpg></TD>".Trim();
			strHeader +="						<TD vAlign=top width=\"100%\" ".Trim();
			strHeader +="						background=http://www.theplasticsexchange.com/images2/loggedmenu/logo_top_r2_c23.gif></TD></TR>".Trim();
			strHeader +="					<DIV></DIV></TBODY></TABLE>".Trim();
			strHeader +="					<TABLE cellSpacing=0 cellPadding=0 align=center border=0>".Trim();
			strHeader +="					<TBODY>".Trim();
			strHeader +="					<TR><TD width=\"100%\" background=http://www.theplasticsexchange.com/Pics/menu/background.jpg></TD>".Trim();
			strHeader +="						<TD><IMG src=\"http://www.theplasticsexchange.com/Pics/menu/menu_orange_phone-1.jpg\"></TD></TR></TBODY></TABLE></TD></TR>".Trim();					
			strHeader +="					<TR><TD vAlign=top width=\"100%\" bgColor=#dbdcd7 height=\"100%\">".Trim();
			strHeader +="				</TD></TR></TBODY></TABLE>".Trim();
            strHeader +="               </TD></TR>".Trim();
			strHeader +="					<tr>".Trim();
			strHeader +="						<td bgColor='#e8e9e4'>".Trim();
			strHeader +="							<table>".Trim();
			strHeader +="								<tr>".Trim();
			strHeader +="									<td>&nbsp;&nbsp;&nbsp;</td>".Trim();
			strHeader +="									<td valign='top'>".Trim();
			strHeader +=@"										<BR>".Trim();
				

	    	return strHeader.ToString();
		
		}

		private string EmailFooterHtml(string hrefUpdateRequirements)
		{
			string strFooter = "";
			strFooter = "							</td>".Trim();
			strFooter +="					</tr>".Trim();
			strFooter +="					<tr>".Trim();
			strFooter +="						<td>&nbsp;&nbsp;&nbsp;</td>".Trim();
			strFooter +="						<td>".Trim();
			strFooter +="							<FONT size='2'><UPDATES TO YOUR REQUIREMENTS> to update your resin preferences.</FONT><BR>".Trim();
			strFooter +="							<FONT size='2'>If you wish to unsubscribe to this notification service, please <a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/public/ML_Unsubscribe.aspx'>click here</a>.</FONT>".Trim();
			strFooter +="						</td>".Trim();
			strFooter +="					</tr>".Trim();
			strFooter +="					<tr>".Trim();
			strFooter +="						<td colspan=2>".Trim();
			strFooter +="							<TABLE id='Table3' width='100%' border='0'><TR><TD><hr></TD></TR></TABLE>".Trim();
			strFooter +="							<CENTER><FONT color='black' size='1'>710 North Dearborn</FONT>&nbsp;&nbsp;".Trim();
			strFooter +="								<FONT color='black' size='1'>Chicago, IL 60610</FONT>&nbsp;&nbsp;".Trim();
			strFooter +="								<FONT color='black' size='1'>tel " + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + "</FONT>&nbsp;&nbsp;".Trim();
			strFooter +="								<FONT color='black' size='1'>fax " + ConfigurationSettings.AppSettings["FaxNumber"].ToString() + "</FONT>".Trim();
			strFooter +="							</CENTER>".Trim();
			strFooter +="						</td>".Trim();
			strFooter +="					</tr>".Trim();
			strFooter +="				</table>".Trim();
			strFooter +="			</td>".Trim();
			strFooter +="		</tr>".Trim();
			strFooter +="	</table>".Trim();
            strFooter +=" </TD></TR></TABLE>".Trim();
			strFooter +="</body>".Trim();
			strFooter = strFooter.Replace("<UPDATES TO YOUR REQUIREMENTS>",hrefUpdateRequirements);
			return strFooter.ToString();

		
		}

		private void Delay(int seconds)
		{
			DateTime Final = DateTime.Now.AddSeconds((double)seconds);
			while (DateTime.Now<=Final)
			{
				//do nothing.. delay
			}
		}

		private string UserGrades(string PersonID)
		{

			string grade = "";

			Hashtable param = new Hashtable();
			param.Add("@PersID", PersonID);
			using (SqlConnection conn = new SqlConnection(DBString))
			{
				conn.Open();				
				string sqlShip="select g.grade_name from person p, contract c, grade g where POWER(2,c.cont_id-1)&(p.pers_pref)<>0 and c.cont_grade = g.grade_id and p.pers_id = @PersonID group by g.grade_name";
				using (SqlDataReader rd= DBLibrary.GetDataReaderFromSelect(conn,sqlShip,param))
				{
					while(rd.Read())
					{
						grade += HelperFunction.getSafeStringFromDB(rd["grade_name"]) + ", ";
					}
				}					
			}			

			
//			SqlDataReader dtrPerson = DBLibrary.ExecuteReader("select g.grade_name from person p, contract c, grade g where POWER(2,c.cont_id-1)&(p.pers_pref)<>0 and c.cont_grade = g.grade_id and p.pers_id = '" + PersonID + "' group by g.grade_name", DBString);
//			while(dtrPerson.Read())
//			{
//				grade += HelperFunction.getSafeStringFromDB(dtrPerson["grade_name"]) + ", ";
//			}
//			dtrPerson.Close();
//			db.CloseConnection();
			
			if (grade.Length>0)
			{
				grade = grade.Substring(0, grade.Length-2);
			}
			else
			{
				grade = "-";
			}
			
			return grade;
		}

		public void SendEmailsNoOffers(DataSet Emails,bool inThread)
		{
			int emailsSent = 0;
			int emailsNotWanted = 0;
			
			Hashtable ht = new Hashtable(1);
			ht.Add("@BrokerID",strID);
			int idReport = (int)DBLibrary.ExecuteScalarStoredProcedure(DBString, "spAddOffersReport", ht);
			
			for(int iUser=0;iUser<Emails.Tables["Users"].Rows.Count;iUser++)
			{
				if (Emails.Tables["Users"].Rows[iUser]["Email_enbl"].ToString() != "0")
				{
					string PersonFirstName = Emails.Tables["Users"].Rows[iUser]["pers_frst_name"].ToString();
//					string QualityPreferences = Emails.Tables["Users"].Rows[iUser]["pers_intrst_qualt"].ToString().Trim();
//					string SizePreferences = Emails.Tables["Users"].Rows[iUser]["pers_intrst_size"].ToString().Trim();
//					string GradePreferences = Emails.Tables["Users"].Rows[iUser]["pers_pref"].ToString().Trim();
//					string MarketPreference = Emails.Tables["Users"].Rows[iUser]["pers_prmly_intrst"].ToString().Trim();
//					string ZipCode = Emails.Tables["Users"].Rows[iUser]["pers_zip"].ToString();
//					string Port = Emails.Tables["Users"].Rows[iUser]["pers_port"].ToString();
					string EmailAddressPerson = Emails.Tables["Users"].Rows[iUser]["pers_mail"].ToString();
					string PersonID = Emails.Tables["Users"].Rows[iUser]["pers_id"].ToString();
					string PersonPassword = Emails.Tables["Users"].Rows[iUser]["pers_pswd"].ToString();

					string hrefUpdateRequirements = HelperFunction.getEmailLink(EmailAddressPerson, PersonPassword, "Click here", "/Administrator/Update_Resins.aspx?UserID=" + Crypto.Encrypt(PersonID),7);
					string hrefLinkEditPreferences = HelperFunction.getEmailLink(EmailAddressPerson, PersonPassword, "Edit", "/Administrator/Update_Resins.aspx?UserID=" + Crypto.Encrypt(PersonID),7);
				
					StringBuilder MailBody = new StringBuilder("");
					MailBody.Append(EmailHeaderHtml());
					MailBody.Append(EmailBodyHtml(PersonFirstName,"0"));					
					MailBody.Append("<BR><BR>");					
					MailBody.Append(EmailSalutationHtml());
					MailBody.Append(EmailFooterHtml(hrefUpdateRequirements));

					MailMessage mail=new MailMessage();
					if (this._IsDebug)
					{
						mail.From = "resin@theplaticsexchange.com";
						mail.To = "rickg@theplasticexchange.com";
						mail.Bcc = "amre@theplasticexchange.com";
					}
					else
					{
						mail.From=strEmailFrom;
						mail.Bcc= strEmailFrom + ";" + strEmailOwner;
						mail.To=EmailAddressPerson;
					}
					
					mail.Subject= strEmailTitle;
				
					mail.Body=MailBody.ToString();
					mail.BodyFormat=System.Web.Mail.MailFormat.Html;
				
					if (inThread)
						TPE.Utility.EmailLibrary.SendWithinThread(mail);
					else
						TPE.Utility.EmailLibrary.Send(mail);

					emailsSent++;

					//Saving Email into System
					int EmailSystemID = HelperFunction.SaveEmailSystem(mail.Body, "L", idReport, DBString);
				
					string sqlQuery="";
					
					string EmailID = "NULL";
					if (EmailSystemID!=0) EmailID = EmailSystemID.ToString();
					sqlQuery="INSERT INTO COMMENT (CMNT_PERS,CMNT_TEXT,CMNT_DATE, CMNT_EMAIL_SYSTEM_ID) VALUES ('"+PersonID+"','Email Subject: "+strEmailTitle+"', getDate(), " + EmailID + ")";
					DBLibrary.ExecuteSQLStatement(DBString,sqlQuery);
				}
				else
				{
					emailsNotWanted++;
				}
			}

			MailMessage resultEmail = new MailMessage();
			resultEmail.From = strEmailFrom;			
			resultEmail.To= strEmailOwner + ";" + strEmailFrom;
			resultEmail.Subject= "Send Letter Report";
					
			resultEmail.Body = emailsSent + " emails were sent to " + Emails.Tables["Users"].Rows.Count + " users.<BR> " 				
				+ emailsNotWanted + " emails were not sent because users had chosen to not receive emails.<BR>";
			resultEmail.BodyFormat=System.Web.Mail.MailFormat.Html;

			ht = new Hashtable();
			ht.Add("@EmailsSent",emailsSent);
			ht.Add("@EmailsNotWanted",emailsNotWanted);			
			ht.Add("@ReportID",idReport);

			DBLibrary.ExecuteStoredProcedure(DBString, "spUpdateOffersReport", ht);
					
			if (inThread)
				TPE.Utility.EmailLibrary.SendWithinThread(resultEmail);
			else
				TPE.Utility.EmailLibrary.Send(resultEmail);
		}

		private string EmailBodyHtml(string UserFirstName, string TotalTransactions)
		{
			//EmailBody
			string strEmailBody = "";
			strEmailBody = Body.Replace("The Plastics Exchange", "<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "'>The Plastics Exchange</a>");
			strEmailBody = strEmailBody.Replace("\r\n","<br>");
			strEmailBody = strEmailBody.Replace("<PERSON FIRST NAME>","&nbsp;"+UserFirstName);
			//strEmailBody = strEmailBody.Replace("<TOTAL_OFFERS>",TotalTransactions);
			if (Convert.ToInt16(TotalTransactions) < 2) 
			{// only one offer. adjust text
				strEmailBody = strEmailBody.Replace("offers that are","offer that is");
				strEmailBody = strEmailBody.Replace("offers are","offer is");
				strEmailBody = strEmailBody.Replace("offers","offer");
				strEmailBody = strEmailBody.Replace("these","this");
			}
//			strEmailBody += "<BR>";
			return strEmailBody;
		}

		private string EmailTransactionHtml(DataRow[] DataRows, string EmailPerson, string PasswordPerson, string TransactionType)
		{
			string strEmailTransaction = ""; //"<B>Search Results:</B>".Trim();
			string strDomainName = ConfigurationSettings.AppSettings["DomainName"].ToString();
			strEmailTransaction += "					<BR><BR>".Trim();
			strEmailTransaction += "					<table cellSpacing='1' cellPadding='3' border='0' width='750' bgcolor='#9a9b96' ID='Table2'>".Trim();
			strEmailTransaction += "						<TR>".Trim();
			if (TransactionType=="Bids")
				strEmailTransaction += "							<TD align='left' nowrap='true' background='http://" + strDomainName + "/Pics/menu/background.jpg' style='FONT-SIZE: 12px;'><B>Request ID</B></TD>".Trim();
			else
				strEmailTransaction += "							<TD align='left' nowrap='true' background='http://" + strDomainName + "/Pics/menu/background.jpg' style='FONT-SIZE: 12px;'><B>Offer ID</B></TD>".Trim();
			//strEmailTransaction += "							<TD align='left' nowrap='true' background='http://" + strDomainName + "/Pics/menu/background.jpg' style='FONT-SIZE: 12px;'><B>Inquire</B></TD>".Trim();
			strEmailTransaction += "							<TD align='left' background='http://" + strDomainName + "/Pics/menu/background.jpg' style='FONT-SIZE: 12px;'><B>Product</B></TD>".Trim();
			strEmailTransaction += "							<TD align='center' background='http://" + strDomainName + "/Pics/menu/background.jpg' style='FONT-SIZE: 12px;'><B>Size</B></TD>".Trim();
			strEmailTransaction += "							<TD align='center' background='http://" + strDomainName + "/Pics/menu/background.jpg' style='FONT-SIZE: 12px;'><B>Melt</B></TD>".Trim();
			strEmailTransaction += "							<TD align='center' background='http://" + strDomainName + "/Pics/menu/background.jpg' style='FONT-SIZE: 12px;'><B>Density</B></TD>".Trim();
			strEmailTransaction += "							<TD align='center' background='http://" + strDomainName + "/Pics/menu/background.jpg' style='FONT-SIZE: 12px;'><B>Adds</B></TD>".Trim();
			strEmailTransaction += "							<TD align='center' background='http://" + strDomainName + "/Pics/menu/background.jpg' style='FONT-SIZE: 12px;'><B>Price</B></TD>".Trim();
			strEmailTransaction += "							<TD align='left' nowrap='true' background='http://" + strDomainName + "/Pics/menu/background.jpg' style='FONT-SIZE: 12px;'><B>Location Delivered</B></TD>".Trim();
			strEmailTransaction += "						</TR>".Trim();
			string color = "#B4B4B4";
			foreach(DataRow currentRow in DataRows)
			{
				if (currentRow["selected"].ToString() == true.ToString())
				{
					string transaction = "";
					if (TransactionType=="Bids") transaction = "&BidOffer=B";
					if (currentRow["Export"].ToString() == true.ToString()) transaction += "&Export=true";
					
				//	string hrefLinkMoreInfo = HelperFunction.getEmailLink(EmailPerson, PasswordPerson, "More Info", "/Spot/Inquire.aspx?Id=" + currentRow["TransactionID"].ToString() + transaction,7);
					string hrefLinkMoreInfo = HelperFunction.getEmailLink(EmailPerson, PasswordPerson, currentRow["TransactionID"].ToString(), "/Spot/Inquire.aspx?Id=" + currentRow["TransactionID"].ToString() + transaction,7);
					strEmailTransaction+= "					    <TR bgColor='".Trim() + color + @"'>".Trim();
		//			strEmailTransaction+="						<TD align='left' nowrap='true'><FONT size='2'>".Trim() + currentRow["TransactionID"] + @"</FONT></TD>".Trim();
	        		strEmailTransaction+="						<TD align='left' nowrap='true'><FONT size='2'>".Trim() + hrefLinkMoreInfo + @"</FONT></TD>".Trim();
	                
				

	   //			strEmailTransaction+="						<TD align='center' nowrap='true'><FONT size='2'>".Trim() + hrefLinkMoreInfo + @"</FONT></TD>".Trim();
					strEmailTransaction+="						<TD align='left' width='200'><FONT size='2'>".Trim() + currentRow["Product"].ToString() + @"</FONT></TD>".Trim();
					strEmailTransaction+="						<TD align='left'><FONT size='2'>".Trim() + currentRow["Size"].ToString() + @"</FONT></TD>".Trim();
					strEmailTransaction+="						<TD align='right'><FONT size='2'>".Trim() + currentRow["Melt"].ToString() + @"</FONT></TD>".Trim();
					strEmailTransaction+="						<TD align='right'><FONT size='2'>".Trim() + currentRow["Density"].ToString() + @"</FONT></TD>".Trim();
					strEmailTransaction+="						<TD align='left'><FONT size='2'>".Trim() + currentRow["Adds"].ToString() + @"</FONT></TD>".Trim();
					strEmailTransaction+="						<TD align='left'><FONT size='2'><B>".Trim() + currentRow["Price"].ToString() + @"</B></FONT></TD>".Trim();
					strEmailTransaction+="						<TD align='left' nowrap='true'><FONT size='2'>".Trim() + currentRow["FOB"].ToString() + @"</FONT></TD>".Trim();
					strEmailTransaction+="					</TR>".Trim();
				
					if (color=="#B4B4B4")
						color="#E8E9E4";
					else
						color="#B4B4B4";
				}
			}
			strEmailTransaction+="</table>".Trim();
			return strEmailTransaction;
		}

		private string EmailPreferencesHtml(string QualityPreferences, string SizePreferences, string GradePreferences, string MarketPreference, string hrefLinkEditPreferences, string ZipCode, string Port, string PersonID)
		{
			string strEmailPreferences = "<BR>".Trim();
			strEmailPreferences +="		<DIV><B>Search Critieria (<U><FONT size='2'>".Trim() + hrefLinkEditPreferences + "</FONT></U>):</B>".Trim();
			strEmailPreferences +="			<BR><BR>".Trim();
			strEmailPreferences +="			<TABLE id='Table1' cellSpacing='1' cellPadding='3' width='500' bgColor='#9a9b96' border='0'>".Trim();
			strEmailPreferences +="				<TR>".Trim();
			strEmailPreferences +="					<TD bgColor='#dbdcd7'><B><FONT size='2'>Resin Grade:</FONT></B></TD>".Trim();
			strEmailPreferences +="					<TD bgColor='#f5f5f5'><FONT style='COLOR: #616c9a' size='2'><RESIN_GRADE></FONT></TD>".Trim();
			strEmailPreferences +="				</TR>".Trim();
			strEmailPreferences +="				<TR>".Trim();
			strEmailPreferences +="					<TD bgColor='#dbdcd7'><B><FONT size='2'>Size:</FONT></B></TD>".Trim();
			strEmailPreferences +="					<TD bgColor='#f5f5f5'><FONT style='COLOR: #616c9a' size='2'><SIZE></FONT></TD>".Trim();
			strEmailPreferences +="				</TR>".Trim();
			strEmailPreferences +="				<TR>".Trim();
			strEmailPreferences +="					<TD bgColor='#dbdcd7'><B><FONT size='2'>Quality:</FONT></B></TD>".Trim();
			strEmailPreferences +="					<TD bgColor='#f5f5f5'><FONT style='COLOR: #616c9a' size='2'><QUALITY></FONT></TD>".Trim();
			strEmailPreferences +="				</TR>".Trim();
			strEmailPreferences +="				<TR>".Trim();
			strEmailPreferences +="					<TD bgColor='#dbdcd7'><B><FONT size='2'>Delivered To:</FONT></B></TD>".Trim();
			strEmailPreferences +="					<TD bgColor='#f5f5f5'><FONT style='COLOR: #616c9a' size='2'><DELIVERED_TO></FONT></TD>".Trim();
			strEmailPreferences +="				</TR>".Trim();
			strEmailPreferences +="			</TABLE>".Trim();
			strEmailPreferences +="			<BR>".Trim();
			strEmailPreferences +="		</DIV>".Trim();

			//Replacing vaiables
			if (QualityPreferences=="") QualityPreferences = "All Qualities";
			if (QualityPreferences=="111") QualityPreferences = "All Qualities";

			if (SizePreferences=="") SizePreferences = "All Sizes";
			if (SizePreferences=="111") SizePreferences = "All Sizes";

			if (GradePreferences=="") GradePreferences = "All Grades";
			if (GradePreferences=="268435455") GradePreferences = "All Grades";

			if (MarketPreference=="") MarketPreference = "1";


			//Quality
			if (QualityPreferences != "All Qualities")
			{
				string quality=QualityPreferences.ToString();
				QualityPreferences = "";
				if (quality.Substring(0,1)=="1") QualityPreferences+="Prime, ";
				if (quality.Substring(1,1)=="1") QualityPreferences+="OffGrade, ";
				if (quality.Substring(2,1)=="1") QualityPreferences+=@"Regrind/Repro, ";
				if (QualityPreferences.Length > 0)
				{
					QualityPreferences = QualityPreferences.Substring(0,QualityPreferences.Length-2);
				}
			}
			strEmailPreferences = strEmailPreferences.Replace("<QUALITY>", QualityPreferences);
			
			//Size
			if (SizePreferences != "All Sizes")
			{
				string size=SizePreferences.ToString();
				SizePreferences = "";
				if (size.Substring(0,1)=="1") SizePreferences+="Rail Cars, ";
				if (size.Substring(1,1)=="1") SizePreferences+="Bulk Trucks, ";
				if (size.Substring(2,1)=="1") SizePreferences+=@"Truckload Boxes\Bags, ";
				if (SizePreferences.Length > 0)
				{
					SizePreferences = SizePreferences.Substring(0,SizePreferences.Length-2);
				}
			}
			strEmailPreferences = strEmailPreferences.Replace("<SIZE>", SizePreferences);
			
			//Resin Grade
			if (GradePreferences != "All Grades")
			{
				GradePreferences = UserGrades(PersonID); //pulling out grades
			}
			strEmailPreferences = strEmailPreferences.Replace("<RESIN_GRADE>", GradePreferences);
			
			//Delivered To
			string DeliveredTo = MarketPreference=="1" ? ZipCode : Port;
			if (DeliveredTo == "") DeliveredTo = "-";
			strEmailPreferences = strEmailPreferences.Replace("<DELIVERED_TO>", DeliveredTo);

			return strEmailPreferences;
		}
		
		public void SendEmails(DataSet Emails, bool inThread)
		{
			int emailsSent = 0;
			int emailsNotWanted = 0;
			int usersWithNoOffer = 0;
			int emailsSentPreviously = 0;

			Hashtable ht = new Hashtable(1);
			ht.Add("@BrokerID",strID);
			int idReport = (int)DBLibrary.ExecuteScalarStoredProcedure(DBString, "spAddOffersReport", ht);

			for(int iUser=0;iUser<Emails.Tables["Users"].Rows.Count;iUser++)
			{
				if (Emails.Tables["Users"].Rows[iUser]["Email_enbl"].ToString() != "0")
				{
					string PersonFirstName = Emails.Tables["Users"].Rows[iUser]["pers_frst_name"].ToString();
					string QualityPreferences = Emails.Tables["Users"].Rows[iUser]["pers_intrst_qualt"].ToString().Trim();
					string SizePreferences = Emails.Tables["Users"].Rows[iUser]["pers_intrst_size"].ToString().Trim();
					string GradePreferences = Emails.Tables["Users"].Rows[iUser]["pers_pref"].ToString().Trim();
					string MarketPreference = Emails.Tables["Users"].Rows[iUser]["pers_prmly_intrst"].ToString().Trim();
					string ZipCode = Emails.Tables["Users"].Rows[iUser]["pers_zip"].ToString();
					string Port = Emails.Tables["Users"].Rows[iUser]["pers_port"].ToString();
					string EmailAddressPerson = Emails.Tables["Users"].Rows[iUser]["pers_mail"].ToString();
					string PersonID = Emails.Tables["Users"].Rows[iUser]["pers_id"].ToString();
					string PersonPassword = Emails.Tables["Users"].Rows[iUser]["pers_pswd"].ToString();

					string hrefUpdateRequirements = HelperFunction.getEmailLink(EmailAddressPerson, PersonPassword, "Click here", "/Administrator/Update_Resins.aspx?UserID=" + Crypto.Encrypt(PersonID),7);
					string hrefLinkEditPreferences = HelperFunction.getEmailLink(EmailAddressPerson, PersonPassword, "Edit", "/Administrator/Update_Resins.aspx?UserID=" + Crypto.Encrypt(PersonID),7);

					//Looks for selected offers or bids for this user
					DataRow[] drTransactions = Emails.Tables[1].Select("pers_id = '" + PersonID + "'", "Grade, Price");
					if(drTransactions.Length == 0) 
						usersWithNoOffer++;

					int iTotalSelected = 0;
					foreach(DataRow dtCurrentTransaction in drTransactions)
					{
						if (dtCurrentTransaction["selected"].ToString() == true.ToString())
						{
							iTotalSelected++;
						}
						else
						{
							emailsSentPreviously++;
						}
					}

					//If there is at least one offer selected the email is sent
					if (iTotalSelected>0)
					{
						StringBuilder MailBody = new StringBuilder("");
						MailBody.Append(EmailHeaderHtml());
						MailBody.Append(EmailBodyHtml(PersonFirstName,iTotalSelected.ToString()));
						MailBody.Append(EmailTransactionHtml(drTransactions, EmailAddressPerson, PersonPassword, Emails.Tables[1].TableName));
						//						if ((bool)ViewState["DisplayPreferences"])
						//						{
						//							MailBody.Append(EmailPreferencesHtml(QualityPreferences, SizePreferences, GradePreferences, MarketPreference, hrefLinkEditPreferences, ZipCode, Port, PersonID));
						//						}
						//						else
						//						{
						MailBody.Append("<BR>");
						//						}
						MailBody.Append(EmailSalutationHtml());
						MailBody.Append(EmailFooterHtml(hrefUpdateRequirements));
					
						MailMessage mail=new MailMessage();
						
						
						mail.Subject= strEmailTitle;
					
						if (this._IsDebug)
						{
							mail.From = "resin@theplaticsexchange.com";
							mail.To = "rickg@theplasticexchange.com";
							mail.Bcc = "amre@theplasticexchange.com";
						}
						else
						{
							mail.From= strEmailFrom;
							mail.Bcc= strEmailFrom + ";" + strEmailOwner;
							mail.To=EmailAddressPerson;						
						}

						mail.Body=MailBody.ToString();
						mail.BodyFormat=System.Web.Mail.MailFormat.Html;
					
						if (inThread)
							TPE.Utility.EmailLibrary.SendWithinThread(mail);
						else
							TPE.Utility.EmailLibrary.Send(mail);

						emailsSent++;

						//Saving Email into System
						int EmailSystemID = HelperFunction.SaveEmailSystem(mail.Body, Emails.Tables[1].TableName == "Offers" ? "O":"B", idReport, DBString);
					
						using (SqlConnection myConn = new SqlConnection(DBString))
						{
							SqlCommand mycomm;
							string sqlQuery="";
							myConn.Open();

							foreach(DataRow dtCurrentTransaction in drTransactions)
							{
								//save erery item into table "email_sent_to_lead".
								if (dtCurrentTransaction["selected"].ToString()==true.ToString())
								{
									string TransactionID = dtCurrentTransaction["TransactionID"].ToString();
									sqlQuery = "select Count(1) from email_sent_to_lead where email_person='" + PersonID + "' and ";
									if (Emails.Tables[1].TableName == "Offers")
									{
										sqlQuery += " email_offr='" + TransactionID + "'";
									}
									else
									{
										sqlQuery += " email_bid='" + TransactionID + "'";
									}
							
									mycomm = new SqlCommand(sqlQuery,myConn);
									if(Convert.ToInt32(mycomm.ExecuteScalar())==0)
									{
										if (Emails.Tables[1].TableName == "Offers")
											sqlQuery="insert into email_sent_to_lead (email_person,email_offr) values ('"+PersonID+"','"+TransactionID+"')";
										else
											sqlQuery="insert into email_sent_to_lead (email_person,email_bid) values ('"+PersonID+"','"+TransactionID+"')";
								
										mycomm = new System.Data.SqlClient.SqlCommand(sqlQuery,myConn);
										mycomm.ExecuteNonQuery();
									}
								}
							}
					
							string EmailID = "NULL";
							if (EmailSystemID!=0) EmailID = EmailSystemID.ToString();
							sqlQuery="INSERT INTO COMMENT (CMNT_PERS,CMNT_TEXT,CMNT_DATE, CMNT_EMAIL_SYSTEM_ID) VALUES ('"+PersonID+"','Email Subject: "+strEmailTitle+"', getDate(), " + EmailID + ")";
						
							mycomm = new System.Data.SqlClient.SqlCommand(sqlQuery, myConn);
							mycomm.ExecuteNonQuery();
						}
					}
				}
				else
				{
					emailsNotWanted++;
				}
			}

			MailMessage resultEmail=new MailMessage();
			resultEmail.From="research@theplasticexchange.com";			
			resultEmail.To=strEmailFrom;
			resultEmail.Bcc = strBccEmail;
			
			resultEmail.Subject= "Send Offers Report";
					
			resultEmail.Body = emailsSent + " emails were sent to " + Emails.Tables["Users"].Rows.Count + " users.<BR> " 
				+ emailsSentPreviously + " emails were altready sent in a previous transaction.<BR>"
				+ emailsNotWanted + " emails were not sent because users had chosen to not receive emails.<BR>" +
				+ usersWithNoOffer + " emails were not sent because no resin found using user's preferences.<BR>";
			resultEmail.BodyFormat=System.Web.Mail.MailFormat.Html;

			ht = new Hashtable();
			ht.Add("@EmailsSent",emailsSent);
			ht.Add("@EmailsNotWanted",emailsNotWanted);
			ht.Add("@UsersWithouthOffers",usersWithNoOffer);
			ht.Add("@EmailsSentPreviously",emailsSentPreviously);
			ht.Add("@ReportID",idReport);

			DBLibrary.ExecuteStoredProcedure(DBString, "spUpdateOffersReport", ht);
								
			if (inThread)
				TPE.Utility.EmailLibrary.SendWithinThread(resultEmail);
			else
				TPE.Utility.EmailLibrary.Send(resultEmail);
		}

		public void SendEmailsFromThread()
		{
/*
			bool useUserPreferences = Convert.ToBoolean(Session["useUserPreferences"]);
			string userIds = Session["UserIds"].ToString();
			string BidsOffersCondition = Session["BidsOffersCondition"].ToString();
			string GradesCondition = Session["GradesCondition"].ToString();
			string emailType = Session["EmailType"].ToString();
*/
								
			//			updateItens(dsEmails);

/*			
			EmailTitle = txtSubject.Text;
			EmailBody = txtBody.Text;
			EmailSalutation = txtSalutation.Text;
			*/

			dsEmails = GetOffersBids(useUserPreferences, userIds, BidsOffersCondition, GradesCondition, emailType);				
			dsEmails = PrepareTransactions(dsEmails, emailType);

			SendEmails(dsEmails, true);
		}

		public void SendEmailsNoOffersFromThread()
		{
/*
			bool useUserPreferences = Convert.ToBoolean(Session["useUserPreferences"]);
			string userIds = Session["UserIds"].ToString();
			string BidsOffersCondition = Session["BidsOffersCondition"].ToString();
			string GradesCondition = Session["GradesCondition"].ToString();
			string emailType = Session["EmailType"].ToString();
*/
			//			updateItens(dsEmails);

/*			
			EmailTitle = txtSubject.Text;
			EmailBody = txtBody.Text;
			EmailSalutation = txtSalutation.Text;
*/
//			dsEmails = GetOffersBids(useUserPreferences, userIds, BidsOffersCondition, GradesCondition, emailType);				
//			dsEmails = PrepareTransactions(dsEmails, emailType);

			SendEmailsNoOffers(dsEmails, true);
		}

		
		public DataSet PrepareTransactions(DataSet dataSet, string BidsOrOffers)
		{
			DataTable dtEmailTransactions = new DataTable();
			dtEmailTransactions.Columns.Add("selected", typeof(System.Boolean));
			dtEmailTransactions.Columns.Add("Size", typeof(System.String));
			dtEmailTransactions.Columns.Add("Product", typeof(System.String));
			dtEmailTransactions.Columns.Add("Melt", typeof(System.String));
			dtEmailTransactions.Columns.Add("Density", typeof(System.String));
			dtEmailTransactions.Columns.Add("Adds", typeof(System.String));
			dtEmailTransactions.Columns.Add("Price", typeof(System.String));
			dtEmailTransactions.Columns.Add("Export", typeof(System.String));
			dtEmailTransactions.Columns.Add("FOB", typeof(System.String));
			dtEmailTransactions.Columns.Add("pers_id", typeof(System.Int32)); //PersID
			dtEmailTransactions.Columns.Add("TransactionID", typeof(System.Int32)); //BidID or OfferID
			dtEmailTransactions.Columns.Add("Grade", typeof(System.Int32)); //Grade

			string prenome = (BidsOrOffers == "Bids" ? "bid" : "offr");
			string lastPersID = "";
			Hashtable hsEmailsSent = new Hashtable();
			
			DataTable dtTransaction = (DataTable)dataSet.Tables[BidsOrOffers];
			for (int i=0;i<dtTransaction.Rows.Count;i++)
			{
				bool selected = true;
				string Size;
				string Product;
				string Melt;
				string Density;
				string Adds;
				string Price;
				string FOB;
				long pers_id = 0;
				long TransactionID = 0;
				long grade = 0;
				bool DomesticMarket = true;
				
				DataRow currentRow = (DataRow)dtTransaction.Rows[i];

				//Bring info about emails already sent
				string UserTransactions = "";
				if (lastPersID!=currentRow["pers_id"].ToString())
				{
					lastPersID = currentRow["pers_id"].ToString();
					//Bring TransactionsID of all transactions of this user
					for (int y=i;y<dtTransaction.Rows.Count;y++)
					{
						if (lastPersID==(dtTransaction.Rows[y])["pers_id"].ToString())
						{
							UserTransactions += (dtTransaction.Rows[y])[prenome + "_id"].ToString() + ",";
						}
						else
						{
							break;
						}
					}
					if (UserTransactions.Length>0) 
					{
						UserTransactions=UserTransactions.Substring(0,UserTransactions.Length-1);
						hsEmailsSent = EmailsAlreadySent(lastPersID,UserTransactions,dataSet.Tables[1].TableName);
					}
					else
					{
						hsEmailsSent = new Hashtable();
					}
				}

				//Market
				DataTable dtUsers = (DataTable)dataSet.Tables["Users"];
				DataRow dtRow = (DataRow)dtUsers.Select("pers_id = '" + currentRow["pers_id"].ToString() + "'").GetValue(0);
				if (dtRow["pers_prmly_intrst"].ToString() == "2")
				{
					DomesticMarket = false;
				}
				
				//Product
				Product = currentRow[prenome + "_prod"].ToString();
				if (currentRow[prenome + "_detl"].ToString()!="")
				{
					Product += " " + "<BR>&nbsp;" + currentRow[prenome + "_detl"];
				}

				//Size
				if (DomesticMarket == true)
				{
					Size = currentRow[prenome + "_size"].ToString();
					switch(Size)
					{
						case "1":
							Size = "lb";
							break;
						case "2.205":
							Size = "Kilo";
							break;
						case "2205":
							Size = "Metric Ton";
							break;
						case "42000":
							Size = "T/L - Boxes";
							break;
						case "45000":
							Size = "Bulk Truck";
							break;
						case "44092":
							Size = "T/L Bags";
							break;
						case "190000":
							Size = "Rail Car";
							break;
					}

					if ((System.Convert.ToInt32(currentRow[prenome + "_qty"])>1) && (currentRow[prenome + "_size"].ToString()!="44092") && (currentRow[prenome + "_size"].ToString()!="42000"))
					{
						Size += "s";
					}
					Size = currentRow[prenome + "_qty"] + " " + Size;
					
					//					switch(currentRow[prenome + "_qualt"].ToString())
					//					{
					//						case "P":
					//							Product += "(Prime)";
					//							break;
					//						case "O":
					//							Product += "(OffGrade)";
					//							break;
					//						case "R":
					//							Product += "(Regrind/Repro)";
					//							break;
					//						default:
					//							break;
					//					}
					Price = currentRow[prenome + "_prce"].ToString();
				}
				else //International
				{
					Size = System.Convert.ToString(Math.Round(System.Convert.ToDecimal(currentRow[prenome + "_qty"]) * System.Convert.ToDecimal(System.Convert.ToInt32(currentRow[prenome + "_size"].ToString()) * 0.00045359237),0));
					Size += " MT";
					
					Price = System.Convert.ToString(Math.Round(System.Convert.ToDouble(currentRow[prenome + "_prce"])/0.00045359237,4));
				}

				Melt = currentRow[prenome + "_melt"].ToString();
				Density = currentRow[prenome + "_dens"].ToString();
				Adds = currentRow[prenome + "_adds"].ToString();
				pers_id = System.Convert.ToInt32(currentRow["pers_id"].ToString());
				TransactionID = System.Convert.ToInt32(currentRow[prenome + "_id"].ToString());
				if (currentRow[prenome + "_grade"]!=System.DBNull.Value) grade = System.Convert.ToInt32(currentRow[prenome + "_grade"].ToString());
				
				//If the email has been already sent I set selected = false
				if(hsEmailsSent.Contains(TransactionID.ToString()))				
					selected = false;				
				
				//selected = !EmailAlreadySent(pers_id.ToString(), TransactionID.ToString(), dataSet.Tables[1].TableName);
				
				//FOB
				if (DomesticMarket == true) //Domestic
				{
					FOB = currentRow[prenome + "_term"].ToString();
					if (FOB == "FOB/Shipping")
					{
                        if (prenome == "offr")
                        {
                            if (currentRow["distance_city"].ToString() != "")
                            {
                                FOB = "FOB " + currentRow["distance_city"] + ", " + currentRow["distance_state"];
                            }
                            else
                            {
                                FOB = "FOB " + currentRow[prenome + "_zip"];
                            }
                        }
                        else
                        {
                            FOB = "FOB " + currentRow["distance_city"] + ", " + currentRow["distance_state"];
                        }
					}
					else
					{
						FOB = "Delivered";
					}
				}
				else //International
				{
					FOB = currentRow[prenome + "_term"].ToString();
                    if (prenome == "offr")
                    {
                        if (FOB.Substring(0, 3) == "FOB")
                        {
                            FOB = "FOB Houston Warehouse";
                        }
                        else
                        {
                            FOB += " " + currentRow["port_city"] + ", " + currentRow["port_country"];
                        }
                    }
                    else
                    {
                        FOB = "FOB Houston Warehouse";
                    }
				}

				//Calculating Freight
				string destination = currentRow["destination"].ToString();
				if (destination!="")
				{
					HelperFunctionDistances hfd = new HelperFunctionDistances();
					decimal freight = 0;
					bool freightAvailable = false;
					decimal valFreight = 0;
					if (DomesticMarket == true) //Demestic Market
					{
						string ZipCode= destination;
						string placeFrom = "";
						string placeTo = "";
						if (currentRow[prenome + "_size"].ToString()=="45000") //BulkTruck
						{
                            if (prenome == "offr")
                            {
                                freightAvailable = hfd.calculateFreightByZipCode(currentRow[prenome + "_zip"].ToString(), ZipCode, ref placeFrom, ref placeTo, ref freight, TruckType.BulkTruck, DBString);
                            }
						}
						else if ((currentRow[prenome + "_size"].ToString()=="42000") || (currentRow[prenome + "_size"].ToString()=="44092")) //TruckLoad
						{
                            if (prenome == "offr")
                            {
                                freightAvailable = hfd.calculateFreightByZipCode(currentRow[prenome + "_zip"].ToString(), ZipCode, ref placeFrom, ref placeTo, ref freight, TruckType.TruckLoad, DBString);
                            }
						}						
						else if (currentRow[prenome + "_size"].ToString()=="1") //Pounds
						{
                            if (prenome == "offr")
                            {
                                freightAvailable = hfd.calculateFreightByZipCode(currentRow[prenome + "_zip"].ToString(), ZipCode, ref placeFrom, ref placeTo, ref freight, TruckType.TruckLoad, DBString);
                            }
						}

						
						if (freightAvailable)
						{
							if (currentRow[prenome + "_size"].ToString()=="1") //Pounds
							{
								
								int lbs = System.Convert.ToInt32(currentRow[prenome + "_qty"]);
								double truck =  42000;
								int num_of_trucks = (int)Math.Ceiling (lbs / truck); 
								valFreight = num_of_trucks * Math.Round(freight / lbs ,4);
							}
							else
							{
								valFreight = Math.Round(freight / System.Convert.ToInt32(currentRow[prenome + "_size"]),4);
							}

							if (prenome=="offr")
							{
								Price = System.Convert.ToString(Math.Round(System.Convert.ToDecimal(Price) + System.Convert.ToDecimal(valFreight),4));
								FOB = "Delivered " + placeTo;
							}
//							else
//							{
//								Price = System.Convert.ToString(Math.Round(System.Convert.ToDecimal(Price) - System.Convert.ToDecimal(valFreight),4));
								//10/09/2006 should we change from FOB dest city to FOB Shipping???
//                                FOB = "FOB Shipping";
//							}
						}
					}
					else //International Market
					{
						int PortID = System.Convert.ToInt32(destination);
						if (currentRow[prenome + "_port"].ToString()!="")
						{
                            if (prenome == "offr")
                            {
                                freightAvailable = hfd.calculateInternationalFreight(System.Convert.ToInt32(currentRow[prenome + "_port"]), PortID, ref freight, DBString);
                            }
						}
                        else if (FOB == "FOB Houston Warehouse")
						{
							//Adding Packing costs 1.5 cents/lbs
							if ((currentRow[prenome + "_size"].ToString()=="190000") || (currentRow[prenome + "_size"].ToString()=="45000"))
							{
								Price = Convert.ToString(Convert.ToDouble(Price) + Convert.ToDouble(0.015/0.00045359237));
							}

							int houstonPortID = HelperFunction.getHoustonPortID(DBString);
                            if (prenome == "offr")
                            {
                                freightAvailable = hfd.calculateInternationalFreight(houstonPortID, PortID, ref freight, DBString);
                            }
						}

						if (freightAvailable)
						{
							valFreight = Math.Round(freight,2);
							FOB = "CFR " + dtRow["pers_port"].ToString();

							if (prenome=="offr")
							{
								Price = String.Format("{0:#,###}",Math.Round(System.Convert.ToDecimal(Price) + System.Convert.ToDecimal(valFreight),0));
							}
						}
						else
						{
							Price = String.Format("{0:#,###}",Math.Round(System.Convert.ToDecimal(Price),0));
						}
					}
				}
				else
				{
					if (DomesticMarket == false) //International Markey
					{
						Price = String.Format("{0:#,###}",Math.Round(System.Convert.ToDecimal(Price),0));
					}
				}
				Price = "$" + Price;

				//Inserting a new row into the final table
				DataRow newEmailRow = dtEmailTransactions.NewRow();
				newEmailRow["selected"] = selected;
				newEmailRow["Size"] = Size;
				newEmailRow["Export"] = Convert.ToString(!DomesticMarket);
				newEmailRow["Product"] = Product;
				newEmailRow["Melt"] = Melt;
				newEmailRow["Density"] = Density;
				newEmailRow["Adds"] = Adds;
				newEmailRow["Price"] = Price;
				newEmailRow["FOB"] = FOB;
				newEmailRow["pers_id"] = pers_id;
				newEmailRow["TransactionID"] = TransactionID;
				newEmailRow["Grade"] = grade;
				dtEmailTransactions.Rows.Add(newEmailRow);
			}
			//Change DataTable
			dataSet.Tables.Remove(BidsOrOffers);
			dataSet.Tables.Add(dtEmailTransactions);
			dataSet.Tables[1].TableName = BidsOrOffers;
			
			//Creating a relationship between tables
			dataSet.Relations.Add("persID",dataSet.Tables["Users"].Columns["pers_id"], dataSet.Tables[BidsOrOffers].Columns["pers_id"]);
			
			//Ready to display or send emails
			return dataSet;
		}

		/// <summary>
		/// That method returns info about Offers/Bids that are gonna be sent to users
		/// </summary>
		/// <returns></returns>
		public DataSet GetOffersBids(bool useUserPreferences, string usersCondition, string BidsOffersCondition, string gradesCondition, string BidsOrOffers)
		{
			DataSet ds = new DataSet();
			SqlDataAdapter dataADP = new SqlDataAdapter();
			using (SqlConnection Conn = new SqlConnection(DBString))
			{
				Conn.Open();

				//Getting User's info as Name, Email, etc.
				string sqlStatement = "select pers_id, pers_frst_name, pers_last_name, pers_mail, pers_pswd, pers_pref, pers_intrst_size, pers_intrst_qualt, pers_prmly_intrst, Email_ENBL, distance_city + ', ' + distance_state + '(' + distance_zipcode + ')' pers_zip, international_port.port_city + ', ' + international_port.port_country pers_port ";
				sqlStatement = sqlStatement + "from person, international_port, distance_zipcode ";
				sqlStatement = sqlStatement + "where person.pers_port *= international_port.port_id ";
				sqlStatement = sqlStatement + "and person.pers_zip *= distance_zipcode.distance_zipcode";
				if (usersCondition!="") sqlStatement += " and pers_id in (" + usersCondition + ")";
				sqlStatement += " order by pers_frst_name, pers_last_name";
				dataADP = new SqlDataAdapter();
				dataADP.SelectCommand=new SqlCommand(sqlStatement,Conn);
				dataADP.SelectCommand.CommandType=CommandType.Text;
				dataADP.Fill(ds,"Users");
			
				DataTable dtTransaction = new DataTable(BidsOrOffers);

				for(int i=0;i<ds.Tables["Users"].Rows.Count;i++)
				{
					bool DomesticMarket = true;
					if  (ds.Tables["Users"].Rows[i]["pers_prmly_intrst"].ToString() == "2")
					{
						DomesticMarket = false;
					}
			
					string userCondition = ds.Tables["Users"].Rows[i]["pers_id"].ToString();

					//Getting Offers/Bids that need to be send out
					dataADP.SelectCommand=new SqlCommand("spEmail",Conn);
					dataADP.SelectCommand.CommandType=CommandType.StoredProcedure;
					string type = "O";
					if (BidsOrOffers=="Bids") type = "B";
					dataADP.SelectCommand.Parameters.Add("@Type", type);
					dataADP.SelectCommand.Parameters.Add("@Export", DomesticMarket ? "0":"1");
					dataADP.SelectCommand.Parameters.Add("@UserPreferences", useUserPreferences == true ? "1":"0");
					dataADP.SelectCommand.Parameters.Add("@InUsersFilter", userCondition);
					dataADP.SelectCommand.Parameters.Add("@InGradesFilter", gradesCondition=="" ? null : gradesCondition);
					dataADP.SelectCommand.Parameters.Add("@InInputsFilter", BidsOffersCondition =="" ? null : BidsOffersCondition);
					dataADP.Fill(dtTransaction);
				}					

				ds.Tables.Add(dtTransaction);			
			}
			return ds;
		}

		private Hashtable EmailsAlreadySent(string PersonID, string TransactionIDs, string TransactionType)
		{
			Hashtable hsEmailsSent = new Hashtable();				
			string fieldName = "email_offr";
			if (TransactionType != "Offers") fieldName = "email_bid";

			string sqlQuery = "select " + fieldName + " from email_sent_to_lead where email_person='" + PersonID + "' and " + fieldName + " in (" + TransactionIDs + ")";
			using (SqlConnection myConn = new SqlConnection(DBString))
			{
				myConn.Open();

				SqlCommand mycomm = new SqlCommand(sqlQuery,myConn);
				SqlDataReader dtEmail = mycomm.ExecuteReader();
				while (dtEmail.Read())
				{
					hsEmailsSent.Add(dtEmail[fieldName].ToString(),"");
				}
			}
			return hsEmailsSent;
		}

	}
}