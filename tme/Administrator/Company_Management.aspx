<%@ Page Language="c#" CodeBehind="Company_Management.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.Company_Management" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
<div style="text-align:center; margin:10px, 0, 10px, 0">
    <span class="Content"><br><b>Accounts Summary</b><br>Show</span>
    <asp:dropdownlist id=ddlCompanyType CssClass="InputForm" runat="server" AutoPostBack="True" onselectedindexchanged="ddlCompanyType_SelectedIndexChanged">
	    <asp:ListItem Value="Buyers">Buyers</asp:ListItem>
	    <asp:ListItem Value="Sellers">Sellers</asp:ListItem>
	</asp:dropdownlist>
</div>
<asp:datagrid id="dg" runat="server" BorderWidth="0" BackColor="#000000" CellSpacing="1" 
    ShowFooter="True" Width="780px" HorizontalAlign="Center" AllowSorting="True" 
    onSortCommand="SortDG" AutoGenerateColumns="False" OnItemCommand="dg_ItemCommand">
<AlternatingItemStyle CssClass="LinkNormal Color2 DarkGray">
</AlternatingItemStyle>

<ItemStyle CssClass="LinkNormal Color2 LightGray">
</ItemStyle>

<HeaderStyle CssClass="LinkNormal Color2 OrangeColor">
</HeaderStyle>

<FooterStyle CssClass="Content Bold Color4 FooterColor">
</FooterStyle>

<Columns>
<asp:BoundColumn DataField="COMP_ID" SortExpression="COMP_ID ASC" HeaderText="Company ID"></asp:BoundColumn>
<asp:HyperLinkColumn  DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/administrator/Company_Info.aspx?Id={0}" 
    DataTextField="COMP_NAME" SortExpression="COMP_NAME DESC" HeaderText="Company Name">
<ItemStyle Wrap="False" HorizontalAlign="Left">
</ItemStyle>
</asp:HyperLinkColumn>
<asp:BoundColumn Visible="False" DataField="COMP_TYPE" SortExpression="COMP_TYPE DESC" HeaderText="Type">
<ItemStyle Wrap="False">
</ItemStyle>
</asp:BoundColumn>
<asp:TemplateColumn>
    <ItemTemplate>
        <asp:ImageButton id="Image1" runat="server" ImageUrl="/Pics/icons/comment.gif" 
        CommandName="NoComments" ToolTip="Company Comments"></asp:ImageButton>				
    </ItemTemplate>
</asp:TemplateColumn>


<asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/spot/Spot_Floor.aspx?Filter=-1&amp;COMP_ID={0}" DataTextField="WO_TOTAL_LBS" SortExpression="WO_TOTAL_LBS ASC" HeaderText="Total Working Orders" ItemStyle-HorizontalAlign="Right" DataTextFormatString="{0:#,###}"></asp:HyperLinkColumn>
<asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/spot/Spot_Floor.aspx?Filter=-1&amp;COMP_ID={0}" DataTextField="WO_NUM" SortExpression="WO_NUM ASC" HeaderText="#">
    <ItemStyle Wrap="False">
    </ItemStyle>
</asp:HyperLinkColumn>
<asp:TemplateColumn>
<HeaderTemplate>
					 &nbsp;&nbsp;&nbsp;&nbsp;
				
</HeaderTemplate>
</asp:TemplateColumn>
<asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/Creditor/AccountSummary.aspx?Id={0}&amp;fct=all_hist" DataTextField="FO_TOTAL_LBS" ItemStyle-HorizontalAlign="Right" SortExpression="FO_TOTAL_LBS ASC" HeaderText="Total Filled Orders"  DataTextFormatString="{0:#,###}"></asp:HyperLinkColumn>
<asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/Creditor/AccountSummary.aspx?Id={0}&amp;fct=all_hist" DataTextField="FO_NUM" SortExpression="FO_NUM ASC" HeaderText="#">
<ItemStyle Wrap="False">
</ItemStyle>
</asp:HyperLinkColumn>
<asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/Creditor/AccountSummary.aspx?Id={0}&amp;fct=all_hist" DataTextField="FO_TOTAL_DOLLARS" ItemStyle-HorizontalAlign="Right" SortExpression="FO_TOTAL_DOLLARS ASC" HeaderText="Total $" DataTextFormatString="{0:c}">
<ItemStyle Wrap="False">
</ItemStyle>
</asp:HyperLinkColumn>
<asp:BoundColumn DataField="FO_TOTAL_PROFIT" SortExpression="FO_TOTAL_PROFIT ASC" HeaderText="Profit Margin" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:c}"></asp:BoundColumn>
<asp:BoundColumn DataField="Balance" SortExpression="BALANCE ASC" HeaderText="Balance" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:c}">
<ItemStyle Wrap="False">
</ItemStyle>
</asp:BoundColumn>
<asp:TemplateColumn Visible="False">
<HeaderTemplate>
Enabled 
</HeaderTemplate>
</asp:TemplateColumn>
</Columns>
</asp:DataGrid>
</asp:Content>
