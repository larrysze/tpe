using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using MetaBuilders.WebControls;

namespace localhost
{
	/// <summary>
	/// Summary description for task_list.
	/// </summary>
	public partial class task_list : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
            if (Session["Typ"].ToString() != "A" && Session["Typ"].ToString() != "B" && Session["Typ"].ToString() != "T" && Session["Typ"].ToString() != "L")
			{
				Response.Redirect("/default.aspx");

			}
			
			if (!IsPostBack)
			{
				Bind();
			}
		}
		private void Bind()
		{
			SqlDataAdapter dadContent;
			DataSet dstContent;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			StringBuilder sbSQL = new StringBuilder();
			sbSQL.Append("");

			sbSQL.Append(" SELECT ");
			sbSQL.Append(" 	TASK_ID, ");
			sbSQL.Append(" 	TASK_DESCRIPTION, ");
			sbSQL.Append(" 	TASK_PRIORITY= (CASE TASK_PRIORITY WHEN '1' THEN 'Low' WHEN '2' THEN 'Medium' WHEN '3' THEN 'High' ELSE '' END), ");
			sbSQL.Append(" 	TASK_DUE_DATE, ");
			sbSQL.Append(" 	TASK_LEAD=(SELECT PERS_FRST_NAME +' ' + PERS_LAST_NAME FROM PERSON WHERE PERS_ID=TASK_LEAD_ID), ");
			sbSQL.Append(" 	TASK_LEAD_ID ");
			sbSQL.Append(" FROM TASK_LIST ");
			sbSQL.Append(" WHERE TASK_PERSON ='"+Session["ID"].ToString()+"' ");
			if (!ddlDueDate.SelectedValue.ToString().Equals("All"))
			{
				if (ddlDueDate.SelectedValue.ToString().Equals("Overdue"))
				{
					sbSQL.Append(" AND TASK_DUE_DATE < getdate()");
				}
				else // ending in the next 7 days
				{
					sbSQL.Append(" AND DATEDIFF(DAY, getdate(),TASK_DUE_DATE) BETWEEN 0 AND 7");
				}
			}
			sbSQL.Append(" ORDER BY TASK_DUE_DATE DESC ");


			dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dgTask.DataSource = dstContent;
			dgTask.DataBind();
			conn.Close();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnDelete_Click(object sender, System.EventArgs e)
		{
			StringBuilder sbSQL = new StringBuilder();	
			RowSelectorColumn rsc;
			rsc = RowSelectorColumn.FindColumn(dgTask);
			bool bFound =false;
			for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++)
			{
				int selIndex = rsc.SelectedIndexes[selectedIndex];
				sbSQL.Append("DELETE FROM TASK_LIST  WHERE TASK_ID='"+dgTask.DataKeys[selIndex].ToString()+"';");
				bFound = true;
			}
			if (bFound) // ONLY EXECUTES IF AT LEAST ONE INSTANCE has been selected.
			{
				SqlConnection conn;
				conn = new SqlConnection(Application["DBconn"].ToString());
				conn.Open();
				SqlCommand cmd = new SqlCommand(sbSQL.ToString(),conn);
				cmd.ExecuteNonQuery();

				//Response.Write(sbSQL.ToString());
				//cmd.Dispose();
				conn.Close();
				conn.Dispose();
			}
			Bind();
		}

		protected void ddlDueDate_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Bind();
		}


	}
}
