<%@ Page Language="VB" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Web.Mail" %>
<form runat="server" id="Form">
<!--#include FILE="../include/VBLegacy_Code.aspx"-->
    <TPE:Template PageTitle="Contact Us" Runat="Server" />
<%
IF Session("Typ")<>"A" AND Session("Typ")<>"M" THEN Response.Redirect ("../default.aspx")
DIM i,Str,Height
'Database connection'
Dim conn as SqlConnection
Dim cmdContent as SQLCommand
Dim Rec0 as SqlDataReader
conn = new SqlConnection(Application("DBconn"))
conn.Open()
Height=260

IF Request.Form("Decline")<>"" THEN
	Application.Lock
	Application("Flag")=1
	cmdContent= new SqlCommand("DELETE FROM TEMP_PLACE WHERE TEMP_PLAC_PERS="&Request.Form("Decline")&";DELETE FROM TEMP_BANK WHERE TEMP_BANK_PERS="&Request.Form("Decline")&";DELETE FROM TEMP_ASSOCIATE WHERE TEMP_ASSO_PERS="&Request.Form("Decline")&";DELETE FROM TEMP_COMPANY WHERE TEMP_COMP_PERS="&Request.Form("Decline")&";DELETE FROM TEMP_PERSON WHERE TEMP_PERS_ID="&Request.Form("Decline"), conn)
	cmdContent.ExecuteNonQuery()

	Application("Flag")=0
	Application.UnLock

END IF
%>
<!--#INCLUDE FILE="../include/Body.inc"-->
<!--#INCLUDE FILE="../include/Loading.inc"-->
<!--#INCLUDE FILE="../include/PopUp.inc"-->
<SPAN ID=Main name=Main style=' x:0px; y:0px; visibility: hidden'>
<script>
function Answer(Flag)
	{if(Flag) window.document.Form.submit()}
i=0
function L(A,B,C,D,E,F)
{
	StrForm='window.document.Form'
	F.write('<tr',(i%2==0)?'>':' bgcolor=#E8E8E8>',Path.Strg0,'<td><center><a href="Javascript:',StrForm,'.Decline.value=',A,';Path.Mess(window,\'Are you sure to want to decline this Application?\',1)">...</a></td><td><a href=Javascript:with('+StrForm+'){Id.value="',A,'";action="../Common/MB_Temp_Company_Info.aspx";submit()}>',B,'</a></td><td>&nbsp;',C,'&nbsp;</td><td>',D,'</td><td>',E,'</td>',Path.Strg0,'</tr>')
	i=i+1
}
</script>

<input type=hidden name=Column value='<%=Request.Form("Column")%>'>
<input type=hidden name=Order value='<%=Request.Form("Order")%>'>
<input type=hidden name=Id value=''>
<input type=hidden name=Accept value=''>
<input type=hidden name=Switch value=''>
<input type=hidden name=Decline value=''>
<input type=hidden name=SearchSubmit value=''>
<input type=hidden name=ShowAll value=''>
<table border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td colspan=9><center>Search:&nbsp;<input type=text name=Search size=8 value='<%IF Request.Form("ShowAll")="" THEN Response.write (Request.Form("Search"))%>'>into
<script>Path.SO(new Array('ALL','TEMP_COMP_NAME','TEMP_COMP_TYPE','TEMP_DATE','TEMP_PERS_ASKD'),new Array('All','Name','Type','Date','Asked Credit'),'<%IF Request.Form("ShowAll")="" THEN Response.write (Request.Form("SearchColumn"))%>',document,'SearchColumn')</script>
<input type=button class=tpebutton value="Search" onClick="Javascript:if(Path.Format(document.Form)==true){Path.Wait(window); document.Form.SearchSubmit.value='Submit';document.Form.Switch.value='';document.Form.Decline.value='';document.Form.submit()}">
<input type=button class=tpebutton value="Show All" onclick="Javascript:Path.Wait(window);document.Form.ShowAll.value='Show';document.Form.Switch.value='';document.Form.Decline.value='';document.Form.submit()">
		</td>
	</tr>
<script>
Path.Menu(new Array('Decline','Name','Type','Date','Credit&nbsp;Request'),new Array('','','TEMP_COMP_NAME','TEMP_COMP_TYPE','TEMP_COMP_DATE','TEMP_PERS_ASKD'),new Array('','','300','80','120','100','100'),'<%IF Request.Form("Search")<>"" AND Request.Form("ShowAll")="" THEN Response.write (Request.Form("Search"))%>',document);
<%
Str="SELECT * FROM (SELECT TEMP_COMP_PERS,TEMP_COMP_NAME,TEMP_COMP_TYPE=(CASE TEMP_COMP_TYPE WHEN 'P' THEN 'Purchaser' WHEN 'S' THEN 'Supplier' ELSE 'Distributor' END),TEMP_DATE=CONVERT(VARCHAR,TEMP_COMP_DATE,101),TEMP_PERS_ASKD=(SELECT TEMP_PERS_ASKD FROM TEMP_PERSON WHERE TEMP_PERS_ID=TEMP_COMP_PERS),TEMP_COMP_DATE FROM TEMP_COMPANY WHERE TEMP_COMP_DONE=0)Q0"
IF Request.Form("SearchSubmit")<>"" AND Request.Form("ShowAll")="" THEN
	IF Request.Form("SearchColumn")="ALL" THEN
		Str=Str+" WHERE (TEMP_COMP_NAME LIKE '%"+Request.Form("Search")+"%'"
		Str=Str+" OR CONVERT(VARCHAR,TEMP_COMP_DATE,101) LIKE '%"+Request.Form("Search")+"%'"
		Str=Str+" OR TEMP_COMP_TYPE LIKE '%"+Request.Form("Search")+"%'"
		Str=Str+" OR TEMP_PERS_ASKD LIKE '%"+Request.Form("Search")+"%')"
	ELSE
		Str=Str+" WHERE "+Request.Form("SearchColumn")+" LIKE '%"+Request.Form("Search")+"%'"
	END IF
END IF
Str=Str+" ORDER BY "
IF Request.Form("Column")<>"" THEN
	Str=Str+Request.Form("Column")+" "+Request.Form("Order")
ELSE
	Str=Str+"TEMP_COMP_NAME,TEMP_COMP_DATE ASC"
END IF
cmdContent= new SqlCommand(Str, conn)
Rec0= cmdContent.ExecuteReader()
While Rec0.Read()
	%>L('<%=Rec0(0)%>','<%=Rec0(1)%>','<%=Rec0(2)%>','<%=Rec0(3)%>','<%IF IsNumeric(Rec0(4)) THEN Response.write (FormatCurrency(Rec0(4),0,-2,-2,-2)) ELSE Response.write (Rec0(4))%>',document);<%

End While
%></script>
	<tr height=2 bgcolor=#CCCCCC>
		<td colspan=9><img name=I width=1 height=2>
		</td>
	</tr>
</table>
<%
Rec0.Close()
conn.Close()
%>


</SPAN>
<TPE:Template Footer="true" Runat="Server" />

</form>


<script>Path.Start(window)</script>
