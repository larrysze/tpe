using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Text;
using TPE.Utility;

namespace localhost
{
	/// <summary>
	/// Summary description for Order_Documents.
	/// </summary>
	public partial class Edit_Location : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.HyperLink Hyperlink1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl FileDetails;
		protected System.Web.UI.HtmlControls.HtmlGenericControl FileName;
		protected System.Web.UI.HtmlControls.HtmlGenericControl MyContentType;
		protected System.Web.UI.HtmlControls.HtmlGenericControl ContentLength;
		protected System.Web.UI.WebControls.ListBox ListBox1;
		protected System.Web.UI.WebControls.ListBox ListBox2;
		protected System.Web.UI.WebControls.ListBox Inbox;
		protected System.Web.UI.WebControls.ListBox Employeebox;
		protected System.Web.UI.WebControls.ListBox ddlInbox;
		protected System.Web.UI.WebControls.ListBox ddlEmployeebox;
	
		
		protected System.Web.UI.WebControls.HyperLink del;

		protected void Page_Load(object sender, System.EventArgs e)
		{
            Master.Width = "700px";
			if (!IsPostBack)
			{
				//only adminstrator and broker can access this page.
                if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "L"))
				{
					Response.Redirect("/default.aspx");
				}
				
				if (!IsPostBack)
				{
					Bind_Controls();
				}
			}
		}

		public void Bind_Controls()
		{
			// Binding Company Info
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			Hashtable param = new Hashtable();
			SqlDataReader dtr;
			try
			{
				conn.Open();
				param.Add("@CompID",Request.QueryString["CompanyID"].ToString());

				dtr = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn,"Select COMP_NAME from COMPANY Where COMP_ID=@CompID",param);
				dtr.Read();
				lblCompanyID.Text = Request.QueryString["CompanyID"].ToString();
				lblCompanyName.Text = HelperFunction.getSafeStringFromDB(dtr["COMP_NAME"]);
				dtr.Close();
			}
			finally
			{
				conn.Close();
			}
			
			lblLocationID.Text = Request.QueryString["LocationID"].ToString();
			if (lblLocationID.Text=="0")
			{
				//Action: AddNew
				lblAction.Text = "Add New Location";

				BindCountry(ddlCountry);
				BindState(ddlState,ddlCountry.SelectedItem.Value);
			}
			else
			{
				//Action: Update
				lblAction.Text = "Update Location";

				try 
				{
					conn.Open();
					param.Clear();
					param.Add("@LocationID",lblLocationID.Text);
					//Binding Location Details
					//dtr = lib.ExecuteReader("select *,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) AS Location from place where plac_id='"+lblLocationID.Text+"'");
//					dtr = DBLibrary.GetDataReaderFromSelect(conn,"select *, LOCL_CITY, LOCL_STAT from place, LOCALITY where LOCL_ID=PLAC_LOCL AND plac_id='@LocationID'", param);
					dtr = DBLibrary.GetDataReaderFromSelect(conn,"select *, LOCL_CITY, LOCL_STAT from place, LOCALITY where LOCL_ID=PLAC_LOCL AND plac_id=@LocationID", param);					
					dtr.Read();

					//S:shipping, H: Headquarters, D: Delivery, W: Warehouse
					
					ddlState.Items.Add(HelperFunction.getSafeStringFromDB(dtr["LOCL_STAT"]));
					ddlState.Enabled = false;
					txtCity.Text = HelperFunction.getSafeStringFromDB(dtr["LOCL_CITY"]);
					txtCity.Enabled = false;

				
					lblCountry.Visible = false;
					ddlCountry.Visible = false;
					ddlType.Items.FindByValue(HelperFunction.getSafeStringFromDB(dtr["PLAC_TYPE"])).Selected = true;
		
					txtRailNumber.Text = HelperFunction.getSafeStringFromDB(dtr["PLAC_RAIL_NUM"]);
					txtContact.Text = HelperFunction.getSafeStringFromDB(dtr["PLAC_PERS"]);
					txtPhone.Text = HelperFunction.getSafeStringFromDB(dtr["PLAC_PHON"]);
					txtFax.Text = HelperFunction.getSafeStringFromDB(dtr["PLAC_FAX"]);
					txtEmail.Text = HelperFunction.getSafeStringFromDB(dtr["PLAC_EMAIL"]);
					txtAddress1.Text = HelperFunction.getSafeStringFromDB(dtr["PLAC_ADDR_ONE"]);
					txtAddress2.Text = HelperFunction.getSafeStringFromDB(dtr["PLAC_ADDR_TWO"]);
					txtZip.Text = HelperFunction.getSafeStringFromDB(dtr["PLAC_ZIP"]);
					txtLocationName.Text = HelperFunction.getSafeStringFromDB(dtr["PLAC_INST"]);
				}
				finally
				{
					conn.Close();
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void cmdSave_Click(object sender, System.EventArgs e)
		{
			if (Page.IsValid)
			{
				Hashtable param = new Hashtable();
				//Check if this company already has a headquarters, only one H.Q is allowed in system
				bool bHeadQuarter = false;
				SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
				try 
				{
					conn.Open();
					param.Add("@CompID",lblCompanyID.Text);
//					SqlDataReader dtHQ = DBLibrary.GetDataReaderFromSelect(conn,"select PLAC_ID from PLACE Where PLAC_COMP='@CompID' and plac_type='H'",param);
					SqlDataReader dtHQ = DBLibrary.GetDataReaderFromSelect(conn,"select PLAC_ID from PLACE Where PLAC_COMP=@CompID and plac_type='H'",param);
					if (dtHQ.Read()) bHeadQuarter = true;
					dtHQ.Close();
				}
				finally
				{
					conn.Close();
				}
				if (lblLocationID.Text!="0")
				{	//Update Location Information
					if((ddlType.SelectedItem.Value=="H") && (bHeadQuarter)) 
					{
						lblwrong.Text="You already have a headquarter in our database!";
					}
					else
					{
						string strSQL = "UPDATE PlACE SET PLAC_PERS=@Contact, PLAC_PHON=@Phone, PLAC_FAX=@Fax, PLAC_EMAIL=@Email, PLAC_ADDR_ONE=@Address1, PLAC_TYPE=@Type,PLAC_ADDR_TWO=@Address2,PLAC_ZIP=@Zip,PLAC_RAIL_NUM=@RailNumber,PLAC_INST=@LocationName Where PLAC_ID=@LocationID";
						//string strSQL = "UPDATE PlACE SET PLAC_PERS='"+txtContact.Text+"', PLAC_PHON='"+txtPhone.Text+"', PLAC_FAX='"+txtFax.Text+"', PLAC_EMAIL='"+txtEmail.Text+"', PLAC_ADDR_ONE='"+txtAddress1.Text+"', PLAC_TYPE='"+ddlType.SelectedValue.ToString()+"',PLAC_ADDR_TWO='"+txtAddress2.Text+"',PLAC_ZIP='"+txtZip.Text+"',PLAC_RAIL_NUM='"+txtRailNumber.Text+"',PLAC_INST='"+txtLocationName.Text+"' Where PLAC_ID=" + lblLocationID.Text;
						param.Clear();
						param.Add("@Contact",txtContact.Text);
						param.Add("@Phone",txtPhone.Text);
						param.Add("@Fax",txtFax.Text);
						param.Add("@Email",txtEmail.Text);
						param.Add("@Address1",txtAddress1.Text);
						param.Add("@Type",ddlType.SelectedValue.ToString());
						param.Add("@Address2",txtAddress2.Text);
						param.Add("@Zip",txtZip.Text);
						param.Add("@RailNumber",txtRailNumber.Text);
						param.Add("@LocationName",txtLocationName.Text);
						param.Add("@LocationID",lblLocationID.Text);
						DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), strSQL, param);
						cmdClose_Click(sender, e);
					}
				}
				else
				{	//Insert a new Location
					
					if((ddlType.SelectedItem.Value=="H") && (bHeadQuarter)) 
					{
						lblwrong.Text="You already have a headquarter in our database!";
					}
					else
					{
						
						SqlConnection connAddLocation = new SqlConnection(Application["DBconn"].ToString());
						connAddLocation.Open();
						SqlCommand cmdAddLocation = new SqlCommand("SELECT LOCL_ID FROM LOCALITY WHERE LOCL_CITY='"+txtCity.Text+"' AND LOCL_STAT='"+ddlState.SelectedItem.Value+"' AND LOCL_CTRY='"+ddlCountry.SelectedItem.Value+"'",connAddLocation);
						// Check if the location already exist in shipping matrix
						string LocID = "";
						if (cmdAddLocation.ExecuteScalar() == null)
						{
							// add the locality. then select the correct one
							cmdAddLocation = new SqlCommand("INSERT INTO LOCALITY (LOCL_CITY,LOCL_STAT,LOCL_CTRY) VALUES ('"+txtCity.Text+"','"+ddlState.SelectedItem.Value+"','"+ddlCountry.SelectedItem.Value+"')",connAddLocation);
							cmdAddLocation.ExecuteNonQuery();
							cmdAddLocation = new SqlCommand("SELECT @@IDENTITY AS NewID",connAddLocation);
							LocID= cmdAddLocation.ExecuteScalar().ToString();

							
						}
						else
						{
							LocID= cmdAddLocation.ExecuteScalar().ToString();
						}
						connAddLocation.Close();
						if((ddlType.SelectedItem.Value=="H") && (txtLocationName.Text!=""))
						{
							lblwrong.Text="A headquarters can not have name attached to it!";
						}
						else
						{
							string strSQL="";
							strSQL="INSERT INTO PLACE (PLAC_COMP,PLAC_ADDR_ONE,PLAC_ADDR_TWO,PLAC_ZIP,PLAC_LOCL,PLAC_TYPE,PLAC_PERS,PLAC_PHON,PLAC_FAX,PLAC_EMAIL,PLAC_ENBL,PLAC_INST,PLAC_RAIL_NUM) VALUES('"+lblCompanyID.Text+"','"+txtAddress1.Text+"','"+txtAddress2.Text+"','"+txtZip.Text+"',"+LocID+",'"+ddlType.SelectedItem.Value+"','"+txtContact.Text+"','"+txtPhone.Text+"','"+txtFax.Text+"','"+txtEmail.Text+"',1,'"+txtLocationName.Text+"','"+txtRailNumber.Text+"')";
							DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), strSQL);
							cmdClose_Click(sender, e);
						}
						//}
					/*	
					else //Deson't exist!, insert into TMP_PLACE table, email alert Zach to run add location script(EXCEL)
						{
							strSQL="INSERT INTO TMP_PLACE (TMP_PLAC_COMP,TMP_PLAC_ADDR_ONE,TMP_PLAC_ADDR_TWO,TMP_PLAC_ZIP,TMP_PLAC_TYPE,TMP_PLAC_PERS,TMP_PLAC_PHON,TMP_PLAC_FAX,TMP_PLAC_EMAIL,TMP_PLAC_INST,TMP_PLAC_CITY,TMP_PLAC_STAT,TMP_PLAC_CTRY,TMP_PLAC_RAIL_NUM) VALUES('"+lblCompanyID.Text+"','"+txtAddress1.Text+"','"+txtAddress2.Text+"','"+txtZip.Text+"','"+ddlType.SelectedItem.Value+"','"+txtContact.Text+"','"+txtPhone.Text+"','"+txtFax.Text+"','"+txtEmail.Text+"','"+txtLocationName.Text+"','"+txtCity.Text+"','"+ddlState.SelectedItem.Value+"','"+ddlCountry.SelectedItem.Value+"','"+txtRailNumber.Text+"')";
							DBLib.ExecuteSQLStatement(this.Context, strSQL);
							
							SqlDataReader dtPL = lib.ExecuteReader("SELECT MAX(TMP_PLAC_ID) AS MAXI FROM TMP_PLACE");
							while(dtPL.Read())
							{
								Response.Redirect("./Add_location_calc_distances.aspx?id="+dtPL["MAXI"].ToString());
							}
							dtPL.Close();
						}
					*/
					}
				}
			}
		}

		protected void cmdClose_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("../administrator/Company_Info.aspx?Id="+lblCompanyID.Text+"");
		}

		private void BindCountry(DropDownList ddlContryControl)
		{
			DBLibrary.BindDropDownList(Application["DBConn"].ToString(),ddlContryControl,"SELECT CTRY_NAME,CTRY_CODE From COUNTRY ORDER BY CTRY_NAME ASC",
									"CTRY_NAME", "CTRY_CODE");
			// Select US in the dll
			ListItem li1 = ddlCountry.Items.FindByValue("US");
			ddlCountry.SelectedIndex = -1;
			li1.Selected = true;
		}

		private void BindState(DropDownList ddlStateControl, string strCountry)
		{
			DBLibrary.BindDropDownList(Application["DBConn"].ToString(),ddlStateControl,"SELECT STAT_NAME,STAT_CODE From STATE WHERE STAT_CTRY ='"+strCountry.ToString()+"'",
									"STAT_NAME", "STAT_CODE");
			if(ddlState.SelectedValue.ToString() == "") ddlState.Items.Add(new ListItem ("No State available",""));
		}

		protected void ddlCountry_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BindState(ddlState,ddlCountry.SelectedItem.Value);
		}
	}
}