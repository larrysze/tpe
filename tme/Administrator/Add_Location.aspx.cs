	using System;
	using System.Collections;
	using System.ComponentModel;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.SessionState;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Web.Mail;
	using System.Data.SqlClient;
	using TPE.Utility;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Add_Location.
	/// </summary>
	public class Add_Location : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Test;
		protected System.Web.UI.WebControls.DropDownList ddlType;
		protected System.Web.UI.WebControls.Label lblwrong;
		protected System.Web.UI.WebControls.TextBox txtComments;
		protected System.Web.UI.WebControls.TextBox txtContact;
		protected System.Web.UI.WebControls.TextBox txtPhone;
		protected System.Web.UI.WebControls.TextBox txtAddress1;
		protected System.Web.UI.WebControls.TextBox txtAddress2;
		protected System.Web.UI.WebControls.TextBox txtCity;
		protected System.Web.UI.WebControls.DropDownList ddlState;
		protected System.Web.UI.WebControls.TextBox txtZip;
		protected System.Web.UI.WebControls.DropDownList ddlCountry;
		protected System.Web.UI.WebControls.Button Button5;
		protected System.Web.UI.WebControls.Button Button6;
	
		/************************************************************************
		 *   1. File Name       :Administrator\Add_Location.aspx                 *
		 *   2. Description     :Add new location to company(only one HQ allowed)*
		 *   3. Modification Log:                                                *
		 *     Ver No.       Date          Author             Modification       *
		 *   -----------------------------------------------------------------   *
		 *      1.00      2-22-2004      Zach                First Baseline      *
		 *                                                                       *
		 ************************************************************************/
		//database connection


		public void Page_Load(object sender, EventArgs e)
		{
			//only adminstrator and borker can access this page.
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B"))
			{
				Response.Redirect("/default.aspx");
			}
			if (!IsPostBack)
			{
				CreateCountryDDL();
				CreateStateDDL();
			}
		}
		public void CompanyBack(object sender, EventArgs e)
		{
			//back to previous page if "Cancel" button is clicked
			Server.Transfer("/administrator/Update_Company_Info.aspx");
		}
		// <summary>
		// wrapper that updates the list of states
		// </summary>
		protected void ChangeCountry(object sender, EventArgs e)
		{
			CreateStateDDL();
		}
		private void CreateStateDDL()
		{
			SqlDataReader dtrStates;
			Hashtable param = new Hashtable();
			
			SqlConnection mySqlConnection = new SqlConnection(Application["DBConn"].ToString());
			try
			{
				mySqlConnection.Open();
				
				// opening state drop-down list
				//cmdStates = new SqlCommand("SELECT STAT_NAME,STAT_CODE From STATE WHERE STAT_CTRY ='"+ddlCountry.SelectedItem.Value.ToString()+"'",conn);
				string sql = "SELECT STAT_NAME,STAT_CODE From STATE WHERE STAT_CTRY =@country";
				param.Add("@country", this.ddlCountry.SelectedItem.Value.ToString());
				dtrStates = DBLibrary.GetDataReaderFromSelect(mySqlConnection,sql,param);

				// binding user drop-down list
				ddlState.DataSource = dtrStates;
				ddlState.DataTextField= "STAT_NAME";
				ddlState.DataValueField= "STAT_CODE";
				ddlState.DataBind();
				dtrStates.Close();
				//Response.Write("ddlstate = " + ddlState.SelectedValue.ToString());
				if(ddlState.SelectedValue.ToString() == "")
					ddlState.Items.Add(new ListItem ("No State available",""));
			}
			finally
			{
				mySqlConnection.Close();
			}
		}
		private void CreateCountryDDL()
		{
			SqlDataReader dtrCountry;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			try
			{
				conn.Open();

				// opening state dorp-down list
				string strSQL = "SELECT CTRY_NAME,CTRY_CODE From COUNTRY ORDER BY CTRY_NAME ASC";
				dtrCountry = DBLibrary.GetDataReaderFromSelect(conn,strSQL, null);

				// binding user dorp-down list
				ddlCountry.DataSource = dtrCountry;
				ddlCountry.DataTextField= "CTRY_NAME";
				ddlCountry.DataValueField= "CTRY_CODE";
				ddlCountry.DataBind();
				dtrCountry.Close();
			}
			finally
			{
				conn.Close();
			}
			// select us in the ddl
			ListItem li1 = ddlCountry.Items.FindByValue("US");
			ddlCountry.SelectedIndex = -1;
			li1.Selected = true;
		}


		public void Save(object sender, EventArgs e)
		{
			string strSQL = "";
			Hashtable param = new Hashtable();
			SqlDataReader dtrHeadquarter;

			//Check if this company already has a headquarters, only one H.Q is allowed in system
			string CheckHead;
			CheckHead="False";
			//cmdHeadquarter= new SqlCommand("select PLAC_ID from PLACE Where PLAC_COMP='"+Request.QueryString["Id"]+"' and plac_type='H'",conn);
			strSQL = "select PLAC_ID from PLACE Where PLAC_COMP=@Id and plac_type='H'";
			param.Add("@Id", Int32.Parse(Request.QueryString["Id"]));

			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			try
			{
				conn.Open();
				dtrHeadquarter= DBLibrary.GetDataReaderFromSelect(conn,strSQL, param);
				if (dtrHeadquarter.Read())
				{
					CheckHead="True";
				}
				else
				{
					CheckHead="False";
				}
				dtrHeadquarter.Close();
			}
			finally
			{
				conn.Close();
			}

			// Check if the location already exist in shipping matrix
			//strSQL="SELECT LOCL_ID,(SELECT COUNT(*) FROM SHIPPING WHERE SHIP_TO_LOCL=LOCL_ID)FROM LOCALITY WHERE LOCL_CITY='"+txtCity.Text+"' AND LOCL_STAT='"+ddlState.SelectedItem.Value+"' AND LOCL_CTRY='"+ddlCountry.SelectedItem.Value+"'";			
			strSQL="SELECT LOCL_ID,(SELECT COUNT(*) FROM SHIPPING WHERE SHIP_TO_LOCL=LOCL_ID)FROM LOCALITY WHERE LOCL_CITY=@city AND LOCL_STAT=@state AND LOCL_CTRY=@country";		
			//Response.Write(strSQL.ToString());
			SqlDataReader dtrcheck;
			param.Clear();
			param.Add("@city",txtCity.Text);
			param.Add("@state",ddlState.SelectedItem.Value);
			param.Add("@country",ddlCountry.SelectedItem.Value);

			try
			{
				conn.Open();
				dtrcheck= DBLibrary.GetDataReaderFromSelect(conn,strSQL,param);
				strSQL="";
				//exist!, insert into PLACE table
				if (dtrcheck.Read())
				{
					//dtrcheck.Close();
					//strSQL="INSERT INTO PLACE (PLAC_COMP,PLAC_ADDR_ONE,PLAC_ADDR_TWO,PLAC_ZIP,PLAC_LOCL,PLAC_TYPE,PLAC_PERS,PLAC_PHON,PLAC_ENBL,PLAC_INST) VALUES('"+Request.QueryString["Id"]+"','"+txtAddress1.Text+"','"+txtAddress2.Text+"','"+txtZip.Text+"',"+dtrcheck["LOCL_ID"]+",'"+ddlType.SelectedItem.Value+"','"+txtContact.Text+"','"+txtPhone.Text+"',1,'"+txtComments.Text+"')";
					strSQL="INSERT INTO PLACE (PLAC_COMP,PLAC_ADDR_ONE,PLAC_ADDR_TWO,PLAC_ZIP,PLAC_LOCL,PLAC_TYPE,PLAC_PERS,PLAC_PHON,PLAC_ENBL,PLAC_INST) VALUES(@Id,@Address1,@Address2,@Zip,@LoclId,@Type,@Contact,@Phone,1,@Comments)";
									
					if((ddlType.SelectedItem.Value=="H") && (txtComments.Text!="")) 
					{
						lblwrong.Text="A headquarters can not have name attached to it!";
					}
					else
					{
						if((ddlType.SelectedItem.Value=="H") && (CheckHead=="True")) 
						{
							lblwrong.Text="You already have a headquarter in our database!";
						}
						else
						{
							param.Clear();
							param.Add("@Id",Int32.Parse(Request.QueryString["Id"]));
							param.Add("@Address1",txtAddress1.Text);
							param.Add("@Address2",txtAddress2.Text);
							param.Add("@Zip",txtZip.Text);
							param.Add("@LoclId",dtrcheck["LOCL_ID"]);
							param.Add("@Type",ddlType.SelectedItem.Value);
							param.Add("@Contact",txtContact.Text);
							param.Add("@Phone",txtPhone.Text);
							param.Add("@Comments",txtComments.Text);
							DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(),strSQL,param);

							dtrcheck.Close();
							Server.Transfer("/administrator/Update_Company_Info.aspx");
						}
					}

				}
					//Deson't exist!, insert into TMP_PLACE table, email alert Zach to run add location script(EXCEL)
				else
				{
					dtrcheck.Close();
					if((ddlType.SelectedItem.Value=="H") && (CheckHead=="True")) 
					{
						lblwrong.Text="You already have a headquarter in our database!";
					}
					else
					{
						//strSQL="INSERT INTO TMP_PLACE (TMP_PLAC_COMP,TMP_PLAC_ADDR_ONE,TMP_PLAC_ADDR_TWO,TMP_PLAC_ZIP,TMP_PLAC_TYPE,TMP_PLAC_PERS,TMP_PLAC_PHON,TMP_PLAC_INST,TMP_PLAC_CITY,TMP_PLAC_STAT,TMP_PLAC_CTRY) VALUES('"+Request.QueryString["Id"]+"','"+txtAddress1.Text+"','"+txtAddress2.Text+"','"+txtZip.Text+"','"+ddlType.SelectedItem.Value+"','"+txtContact.Text+"','"+txtPhone.Text+"','"+txtComments.Text+"','"+txtCity.Text+"','"+ddlState.SelectedItem.Value+"','"+ddlCountry.SelectedItem.Value+"')";
						strSQL="INSERT INTO TMP_PLACE (TMP_PLAC_COMP,TMP_PLAC_ADDR_ONE,TMP_PLAC_ADDR_TWO,TMP_PLAC_ZIP,TMP_PLAC_TYPE,TMP_PLAC_PERS,TMP_PLAC_PHON,TMP_PLAC_INST,TMP_PLAC_CITY,TMP_PLAC_STAT,TMP_PLAC_CTRY) VALUES(@Id,@Address1,@Address2,@Zip,@Type,@Contact,@Phone,@Comments,@City,@State,@Country)";
						param.Clear();
						param.Add("@Id",Int32.Parse(Request.QueryString["Id"]));
						param.Add("@Address1",txtAddress1.Text);
						param.Add("@Address2",txtAddress2.Text);
						param.Add("@Zip",txtZip.Text);
						param.Add("@Type",ddlType.SelectedItem.Value);
						param.Add("@Contact",txtContact.Text);
						param.Add("@Phone",txtPhone.Text);
						param.Add("@Comments",txtComments.Text);
						param.Add("@City",txtCity.Text);
						param.Add("@State",ddlState.SelectedItem.Value);
						param.Add("@Country", ddlCountry.SelectedItem.Value);
						DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(),strSQL,param);

						string strSQL2="SELECT MAX(TMP_PLAC_ID) AS MAXI FROM TMP_PLACE";
						SqlDataReader dtrSelectMax= DBLibrary.GetDataReaderFromSelect(conn,strSQL2,null);
						while(dtrSelectMax.Read())
						{
							Response.Redirect("./Add_location_calc_distances.aspx?id="+dtrSelectMax["MAXI"].ToString());
						}					
						
						// email admin to let him know that a location needs to be added into the system
						/* MailMessage mail = new MailMessage();
						mail.From = "Locations@theplasticsexchange.com";
						mail.To = Application["strEmailAdmin"].ToString();
						mail.Subject = "New Location to be added ";
						mail.Body = txtCity.Text+" "+ddlState.SelectedItem.Value;
						mail.BodyFormat = MailFormat.Html;
						//TPE.Utility.EmailLibrary.Send(mail);
						Server.Transfer("/administrator/Update_Company_Info.aspx"); */		
					}
				}
			}
			finally
			{
				conn.Close();
			}			
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
