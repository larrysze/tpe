using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Data.SqlClient;



namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Leads_List.
	/// </summary>
	public class Leads_List : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DropDownList DropDownList1;
		protected System.Web.UI.WebControls.DropDownList DropDownList2;
		protected System.Web.UI.WebControls.DropDownList DropDownList3;
		protected System.Web.UI.WebControls.TextBox TextBox1;
		protected System.Web.UI.WebControls.DropDownList DropDownList4;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.Button Button2;
		protected System.Web.UI.WebControls.DataGrid DataGrid2;
		protected System.Web.UI.WebControls.Panel Panel1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox resin;
		protected System.Web.UI.WebControls.TextBox LocationEmail;
		protected System.Web.UI.WebControls.TextBox city;
		protected System.Web.UI.WebControls.TextBox state;
		protected System.Web.UI.WebControls.TextBox zip;
		protected System.Web.UI.WebControls.TextBox name;
		protected System.Web.UI.WebControls.TextBox TextBox8;
		protected System.Web.UI.WebControls.TextBox LocationPhone;
		protected System.Web.UI.WebControls.TextBox package;
		protected System.Web.UI.WebControls.TextBox users;
		protected System.Web.UI.WebControls.TextBox lbs;
		protected System.Web.UI.WebControls.TextBox items;
		protected System.Web.UI.WebControls.TextBox contect;
		protected System.Web.UI.WebControls.TextBox companyEmail;
		protected System.Web.UI.WebControls.TextBox companyName;
		protected System.Web.UI.WebControls.TextBox companyTitle;
		protected System.Web.UI.WebControls.TextBox CompanyPhone;
		protected System.Web.UI.WebControls.Button infosub;
		protected System.Web.UI.WebControls.Button infocan;
		protected System.Web.UI.WebControls.Panel Panel2;
		protected System.Web.UI.WebControls.Label L1;
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
		protected System.Web.UI.WebControls.Label L2;
		protected System.Web.UI.WebControls.TextBox T1;
		protected System.Web.UI.WebControls.RadioButtonList RadioButtonList1;
		protected System.Web.UI.WebControls.Button B_Sub;
		protected System.Web.UI.WebControls.Button B_Can;
		protected System.Web.UI.WebControls.Panel Panel3;
	
		/************************************************************************
  1. File Name       :Administrator\Leads_List.aspx  
  2. Description     :Display informaion of company and Edit lead info, accessed from "Lead_List" 
  3. Modification Log:
   Ver No.       Date          Author             Modification   
   3.0          2004-07-03     Xiaoda             Second Baseline
************************************************************************/
		private System.Data.SqlClient.SqlDataAdapter ad;
		private System.Data.SqlClient.SqlConnection con;
		private System.Data.DataSet ds;
		private System.Data.SqlClient.SqlCommand com;
		private System.Data.SqlClient.SqlDataReader rd;
		public void Page_Load(object sender, System.EventArgs e)
		{
			//only adminstrator and borker can access this page.
			if((Session["Typ"].ToString()!="A")&&(Session["Typ"].ToString()!="B"))
			{
				Response.Redirect("../default.aspx");
			}
			if(!Page.IsPostBack)
			{				
				LoadDataGrid();
				LoadSortState();
				//display leads_list hidden lead_comment and lead_info
				this.Panel1.Visible=true;
				this.Panel2.Visible=false;
				this.Panel3.Visible=false;
			}
		}
		public virtual string setPutText(object strob)//
		{
			if(strob!=null)
			{
				try
				{
					return strob.ToString().Trim();
				}
				catch
				{
					return "item must be string";
				}
			}
			else
			{
				return String.Empty;
			}
		}
		private void LoadSortState()
		{    
			ViewState["SortExpressionType"]="COMP";  //add viewstate for sorting ,and default expression "COMP"
			ViewState["SortDirection"]="ASC";
		}
		private string Search//ViewState that save in query string
		{
			get
			{
				object o=ViewState["Search"];
				return((o==null)?String.Empty:o.ToString());
			}
			set
			{
				ViewState["Search"]=value;
			}
		}
		//Execute store procedure spGLeads to query all leads
		private void LoadDataGrid()
		{
			this.con=new System.Data.SqlClient.SqlConnection(Application["DBconnDemo"].ToString());
			ds=new DataSet();
			ad=new System.Data.SqlClient.SqlDataAdapter();
			com=con.CreateCommand();
			com.CommandType=CommandType.StoredProcedure;
			com.CommandText="spGLeads";
			if(this.DropDownList1.SelectedItem.Value.ToString().Trim().Length!=0)
			{
				com.Parameters.Add("@Type",this.DropDownList1.SelectedItem.Value.ToString());
			}
			if(this.DropDownList2.SelectedItem.Value.ToString().Trim().Length!=0)
			{
				com.Parameters.Add("@Lbs",this.DropDownList2.SelectedItem.Value.ToString());
			}
			if(this.DropDownList3.SelectedItem.Value.Trim().Length!=0)
			{
				com.Parameters.Add("@Pack",this.DropDownList3.SelectedItem.Value.ToString());
			}
			if(this.Search.Length!=0)
			{
				com.Parameters.Add("@Search",this.Search);
				com.Parameters.Add("@SearchCol",this.DropDownList4.SelectedItem.Value.ToString());
			}
			ad.SelectCommand=com;
			ad.Fill(ds,"table1");	
			//sort by the type user selected		
			DataView dv=ds.Tables["table1"].DefaultView;
			if(ViewState["SortExpression"]!=null)
			{
				dv.Sort=ViewState["SortExpression"].ToString();
			}
			//change company color 
			foreach(DataRow row in dv.Table.Rows)
			{
				switch(row["varcomm"].ToString())
				{
					case "1":
						row["COMP"]="<font color=red>"+row["COMP"].ToString()+"</font>";
						break;
					case "2":
						row["COMP"]="<font color=green>"+row["COMP"].ToString()+"</font>";
						break;
					case "3":
						row["COMP"]="<font color=orange>"+row["COMP"].ToString()+"</font>";
						break;	
					case "4":
						row["COMP"]="<font color=blue>"+row["COMP"].ToString()+"</font>";
						break;
					default:
						row["COMP"]="<font color=black>"+row["COMP"].ToString()+"</font>";
						break;
				}
				
			}
			this.DataGrid2.DataSource=dv;
			this.DataGrid2.DataBind();

		}
		//execute seaching
		protected void info_backup(object sender, System.EventArgs e)
		{
			changePanel(true,false,false);
			this.DataGrid2.CurrentPageIndex=Convert.ToInt32(ViewState["pageNo"].ToString());
			LoadDataGrid();			
		}
		protected void Button1_Click(object sender, System.EventArgs e)
		{
			if(this.TextBox1.Text.Trim().ToString().Length!=0)
			{
				this.Search=this.TextBox1.Text.Trim();
				this.DataGrid2.CurrentPageIndex=0;
				LoadDataGrid();
			}
		}		
		protected void DropDownList1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.DataGrid2.CurrentPageIndex=0;
			LoadDataGrid();
		}
		protected void DropDownList2_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.DataGrid2.CurrentPageIndex=0;
			LoadDataGrid();
		}

		protected void DropDownList3_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.DataGrid2.CurrentPageIndex=0;
			LoadDataGrid();
		}
		//search all 
		protected void Button2_Click(object sender, System.EventArgs e)
		{
			this.Search=null;
			this.TextBox1.Text="";
			this.DataGrid2.CurrentPageIndex=0;
			LoadDataGrid();
		}
		//sort
		protected void DataGrid2_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			string SortExpression=e.SortExpression.ToString();  //get expression of current sort?????????
			string SortDirection="ASC"; //init var of sorting derection
			if(SortExpression==ViewState["SortExpressionType"].ToString())  //selected column equal to current SortExpressionType
			{
				SortDirection=(ViewState["SortDirection"].ToString()==SortDirection?"DESC":"ASC");     //get expression of next sort
			}
			ViewState["SortExpressionType"]=SortExpression;
			ViewState["SortDirection"]=SortDirection;
			ViewState["SortExpression"]=SortExpression+" "+SortDirection;
			LoadDataGrid();
		}
		//turn over the page
		protected void DataGrid2_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			this.DataGrid2.CurrentPageIndex=e.NewPageIndex;
			LoadDataGrid();
		}
		//change the 
		public void changePanel(bool p1,bool p2,bool p3)
		{
			this.Panel1.Visible=p1;
			this.Panel2.Visible=p2;
			this.Panel3.Visible=p3;    
		}
		//-----------------------lead_info--------------------------------//
		protected void Loadinfor(object sender,System.Web.UI.ImageClickEventArgs e)
		{
			ViewState["pageNo"]=this.DataGrid2.CurrentPageIndex.ToString();
			changePanel(false,true,false);
			Change=((System.Web.UI.WebControls.ImageButton)sender).CommandArgument.ToString();
			this.con=new System.Data.SqlClient.SqlConnection(Application["DBconnDemo"].ToString());
			//Query lead info
			Label1.Text="";
			string Str="SELECT a.Resin, a.City, a.State, a.Zip, a.Phone, a.Email, a.Name,a.Title, a.LbsPerYr, a.EndUseDesc, a.PkgingDesc, a.PlasItemDesc, lead_comp.comp_name, lead_comp.comp_cont, lead_comp.comp_titl, lead_comp.comp_mail,lead_comp.comp_phon from LEAD_LOC a INNER JOIN LEAD_COMP lead_comp ON (a.company=lead_comp.comp_id) where a.loc_id="+Change;
			com=new System.Data.SqlClient.SqlCommand(Str,con);
			con.Open();
			rd=com.ExecuteReader(CommandBehavior.CloseConnection);
			if(rd.Read())
			{
				this.resin.Text=this.setPutText(rd["Resin"]);
				this.city.Text=this.setPutText(rd["City"]);
				this.state.Text=this.setPutText(rd["state"]);
				this.zip.Text=this.setPutText(rd["zip"]);
				this.LocationPhone.Text=this.setPutText(rd["phone"]);
				this.LocationEmail.Text=this.setPutText(rd["email"]);
				this.name.Text=this.setPutText(rd["Name"]);	
				this.TextBox8.Text=setPutText(rd["title"]);
				lbs.Text=this.setPutText(rd["LbsPerYr"]);
				this.users.Text=this.setPutText(rd["EndUseDesc"]);
				this.package.Text=this.setPutText(rd["PkgingDesc"]);
				this.items.Text=this.setPutText(rd["PlasItemDesc"]);
				this.companyTitle.Text=this.setPutText(rd["comp_titl"]);
				this.CompanyPhone.Text=this.setPutText(rd["comp_phon"]);
				this.companyEmail.Text=this.setPutText(rd["comp_mail"]);
				this.contect.Text=this.setPutText(rd["comp_cont"]);
				this.companyName.Text=this.setPutText(rd["comp_NAME"]);
			}
			rd.Close();			
		}
		protected string Change
		{
			get
			{
				object o=ViewState["change"];
				return((o==null)?String.Empty:o.ToString());
			}
			set
			{
				ViewState["change"]=value;
			}
		}
		protected void modifyInfo(object sender, System.EventArgs e)
		{
			try
			{
				this.con=new System.Data.SqlClient.SqlConnection(Application["DBconnDemo"].ToString());
				com=con.CreateCommand();
				com.CommandType=CommandType.StoredProcedure;
				com.CommandText="spUpdLeads";
				com.Parameters.Add("@LocId",this.Change.Trim());
				if(this.resin.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@resin",this.resin.Text.Trim());
				}
				if(this.city.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@city",this.city.Text.Trim());
				}
				if(this.state.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@state",this.state.Text.Trim());
				}
				if(this.zip.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@zip",this.zip.Text.Trim());
				}
				if(this.LocationPhone.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@phone",this.LocationPhone.Text.Trim());
				}
				if(this.lbs.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@lbs",this.lbs.Text.Trim());
				}
				if(this.LocationEmail.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@email",this.LocationEmail.Text.Trim());
				}
				if(this.name.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@name",this.name.Text.Trim());
				}
				if(this.TextBox8.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@title",this.TextBox8.Text.Trim());
				}
				if(this.package.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@pack",this.package.Text.Trim());
				}
				if(this.users.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@enduser",this.users.Text.Trim());
				}
				if(this.items.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@item",this.items.Text.Trim());
				}
				if(this.contect.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@ccont",this.contect.Text.Trim());
				}
				if(this.companyEmail.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@cmail",this.companyEmail.Text.Trim());
				}
				if(this.companyTitle.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@ctitle",this.companyTitle.Text.Trim());
				}
				if(this.CompanyPhone.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@cphone",this.CompanyPhone.Text.Trim());
				}
				if(this.CompanyPhone.Text.Trim().Length!=0)
				{
					com.Parameters.Add("@ccompany",this.companyName.Text.Trim());
				}
				con.Open();
				com.ExecuteNonQuery();
				this.Label1.Text="Succeed in Modifying Record ";
			}
			catch(Exception m)
			{
				this.Label1.Text=m.Message.ToString();
			}
			finally
			{
				con.Close();
			}			
		}		
		//------------------lead_commend--------------------------//
		private string Id
		{
			get
			{
				object o=ViewState["Id"];
				return((o==null)?String.Empty:o.ToString());
			}
			set
			{
				ViewState["Id"]=value;
			}
		}
		protected void Load_Comment(object sender, System.EventArgs e)
		{
			ViewState["pageNo"]=this.DataGrid2.CurrentPageIndex.ToString();
			changePanel(false,false,true);
			Id=((System.Web.UI.WebControls.LinkButton)sender).CommandArgument.ToString();
			T1.Text="";
			if(this.RadioButtonList1.SelectedItem!=null)
			{
				this.RadioButtonList1.SelectedItem.Selected=false;
			}			
			LoadTitle();
			Comment_LoadDataGrid();
		}
		protected void LoadTitle()
		{
			this.con=new System.Data.SqlClient.SqlConnection(Application["DBconnDemo"].ToString());
			con.Open();
			string str="SELECT COMP_NAME,COMP_STAT FROM LEAD_COMP WHERE COMP_ID="+this.Id;
			com=new System.Data.SqlClient.SqlCommand(str,con);
			rd=com.ExecuteReader(CommandBehavior.CloseConnection);
			if(rd.Read())
			{
				this.L1.Text=this.setPutText(rd["COMP_NAME"]);
				string state=this.setPutText(rd["COMP_STAT"]);
				switch(state)
				{
					case "1":
						this.RadioButtonList1.Items[0].Selected=true;
						break;
					case "2":
						this.RadioButtonList1.Items[1].Selected=true;
						break;
					case "3":
						this.RadioButtonList1.Items[2].Selected=true;
						break;
					case "4":
						this.RadioButtonList1.Items[3].Selected=true;
						break;                    
				}
			}
			rd.Close();
		}
		protected void Comment_LoadDataGrid()
		{			
			string str="SELECT CMNT_TEXT,DATE=CONVERT(VARCHAR,CMNT_DATE,101),CMNT_DATE FROM LEAD_CMNT WHERE CMNT_COMP="+Id+" ORDER BY CMNT_DATE DESC";
			this.con=new System.Data.SqlClient.SqlConnection(Application["DBconnDemo"].ToString());
			con.Open();
			com=new System.Data.SqlClient.SqlCommand(str,con);
			rd=com.ExecuteReader(CommandBehavior.CloseConnection);
			this.DataGrid1.DataSource=rd;
			this.DataGrid1.DataBind();
			rd.Close();
		}
		protected void modify_comment(object sender, System.EventArgs e)
		{
			if(this.T1.Text.IndexOf("'")==-1) 
			{
				ModifyRecord();
				this.L2.Text="";
			}
			else
			{
				this.L2.Text="No quotes are allowed in this field.";
			}
		}
		protected void ModifyRecord()
		{
			this.con=new System.Data.SqlClient.SqlConnection(Application["DBconnDemo"].ToString());
			con.Open();
			string str="";
			try
			{
				if((this.T1.Text.Trim().Length==0)&&(this.RadioButtonList1.SelectedItem.Value.Length!=0))
				{
					str="UPDATE LEAD_COMP SET COMP_STAT="+this.RadioButtonList1.SelectedItem.Value+" WHERE COMP_ID="+this.Id;
					com=new System.Data.SqlClient.SqlCommand(str,con);
					com.ExecuteNonQuery();
				}
				if((this.T1.Text.Trim().Length!=0)&&(this.RadioButtonList1.SelectedItem.Value.Length!=0))
				{
					int leng=((this.T1.Text.Trim().Length>750)?750:this.T1.Text.Trim().Length);
					string text=this.T1.Text.Trim().Substring(0,leng).ToString();
					str="INSERT INTO LEAD_CMNT (CMNT_COMP,CMNT_TEXT,CMNT_DATE) VALUES ("+this.Id+",'"+text+"',getDate());UPDATE LEAD_COMP SET COMP_STAT="+this.RadioButtonList1.SelectedItem.Value+" WHERE COMP_ID="+this.Id;
					com=new System.Data.SqlClient.SqlCommand(str,con);
					com.ExecuteNonQuery();
				}
				if((this.T1.Text.Trim().Length!=0)&&(this.RadioButtonList1.SelectedItem.Value.Length==0))
				{
					int leng=((this.T1.Text.Trim().Length>750)?750:this.T1.Text.Trim().Length);
					string text=this.T1.Text.Trim().Substring(0,leng).ToString();
					str="INSERT INTO LEAD_CMNT (CMNT_COMP,CMNT_TEXT,CMNT_DATE) VALUES ("+this.Id+",'"+text+"',getDate())";
					com=new System.Data.SqlClient.SqlCommand(str,con);
					com.ExecuteNonQuery();
				}
				Comment_LoadDataGrid();
				LoadTitle();
			}
			catch(Exception m)
			{
				this.L1.Text=m.Message.ToString();
			}
			finally
			{
				con.Close();
			}
		}	

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
