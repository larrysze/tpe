<%@ Page Language="c#" Codebehind="Add_Warehouse.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.AddWarehouse" MasterPageFile="~/MasterPages/Menu.Master" Title="Add Warehouse" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
    <asp:Panel ID="pnlDefault" runat="server" Visible="true">
        <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td align="center" class="Header">
                    <h3>List of Warehouses</h3>
                </td>
            </tr> 
            <tr>
                <td align="center">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dg" runat="server"  Width="85%" BorderWidth="0" BackColor="#000000" CellSpacing="1" AllowSorting="True" AllowPaging="True" PageSize="50" ItemStyle-CssClass="Content" AlternatingItemStyle-CssClass="Content" HeaderStyle-CssClass="LinkNormal" HorizontalAlign="Center" CellPadding="2" AutoGenerateColumns="False">
                        <AlternatingItemStyle CssClass="LinkNormal Color2 DarkGray" />
                        <ItemStyle CssClass="LinkNormal Color2 LightGray">
                        </ItemStyle>
                        <HeaderStyle CssClass="LinkNormal Color2 OrangeColor">
                        </HeaderStyle>
                        <FooterStyle CssClass="Content Bold Color4 FooterColor">
                        </FooterStyle>
                        
                        <Columns>
                            <asp:BoundColumn Visible="False" DataField="PLAC_ID" HeaderText="ID"></asp:BoundColumn>
                            <asp:ButtonColumn Text="Edit" CommandName="Select" ItemStyle-CssClass="LinkNormal"></asp:ButtonColumn>
                            <asp:BoundColumn DataField="PLAC_PERS" SortExpression="PLAC_PERS  DESC" HeaderText="Person "></asp:BoundColumn>
                            <asp:BoundColumn DataField="PLAC_PHON" SortExpression="PLAC_PHON  DESC" HeaderText="Phone"></asp:BoundColumn>
                            <asp:BoundColumn DataField="PLAC_ADDR_ONE" SortExpression="PLAC_ADDR_ONE  DESC" HeaderText="Address"></asp:BoundColumn>
                            <asp:BoundColumn DataField="PLAC_ZIP" SortExpression="PLAC_ZIP  ASC" HeaderText="Zip"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="PLAC_TYPE" SortExpression="PLAC_TYPE  DESC" HeaderText="Type"></asp:BoundColumn>
                            <asp:BoundColumn DataField="PLAC_INST" SortExpression="PLAC_INST  DESC" HeaderText="Place Name"></asp:BoundColumn>
                            <asp:BoundColumn DataField="City" SortExpression="city  DESC" HeaderText="City"></asp:BoundColumn>
                            <asp:BoundColumn DataField="State" SortExpression="state  DESC" HeaderText="State"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Country" SortExpression="country  DESC" HeaderText="Country"></asp:BoundColumn>
                        </Columns>
                        <PagerStyle NextPageText="&amp;vt" Position="TopAndBottom" Mode="NumericPages" CssClass="LinkNormal LightGray"></PagerStyle>
                    </asp:DataGrid></td>
            </tr>
        </table>
        <br>
        <center>
            <asp:Button ID="btncreate" runat="server" Text="Create a New WareHouse" CssClass="Content Color2" OnClick="btncreate_Click"></asp:Button><br /><br /></center>
    </asp:Panel>
    <asp:Panel ID="pnlSave" runat="server" Visible="false">
        <center>
            <h3>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h3>
        </center>
        <table style="width: 464px; height: 332px" align="center" border="0" class="Content">
            <tr>
                <td colspan="2">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="487px" Height="92px" HeaderText="There are problems with the following fields. Please correct the errors below."></asp:ValidationSummary>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr align=left>
                <td style="height: 25px">
                    Contact:</td>
                <td style="height: 25px">
                    <asp:TextBox ID="txtContact" CssClass="InputForm" runat="server" Width="184px" MaxLength="30" size="20" maxsize="40"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="txtContact" Display="None" ErrorMessage="Contact is required"></asp:RequiredFieldValidator></td>
            </tr>
            <tr align=left>
                <td>
                    Phone:</td>
                <td>
                    <asp:TextBox ID="txtPhone" CssClass="InputForm" runat="server" MaxLength="15" size="15" maxsize="40"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" ControlToValidate="txtPhone" Display="None" ErrorMessage="Phone is required"></asp:RequiredFieldValidator></td>
            </tr>
            <tr align=left>
                <td>
                    Address:</td>
                <td>
                    <asp:TextBox ID="txtAddr" CssClass="InputForm" runat="server" MaxLength="30" size="25" maxsize="40"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ControlToValidate="txtAddr" Display="None" ErrorMessage="Address is required"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
            <tr align=left>
                <td>
                </td>
                <td>
                    <asp:TextBox ID="txtAddr2" CssClass="InputForm" runat="server" MaxLength="30" size="25" maxsize="40"></asp:TextBox></td>
            </tr>
            <tr align=left>
                <td>
                    Zip:</td>
                <td>
                    <asp:TextBox ID="txtZip" CssClass="InputForm" runat="server" Width="120px" MaxLength="10" size="20" maxsize="30"></asp:TextBox></td>
            </tr>
            <tr align=left>
                <td style="height: 45px">
                    Warehouse Name:</td>
                <td style="height: 45px">
                    <asp:TextBox ID="txtWName" runat="server" CssClass="InputForm" MaxLength="30" size="25" maxsize="40" row="2"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtWName" Display="None" ErrorMessage="Warehouse Name is required"></asp:RequiredFieldValidator></td>
            </tr>
            <tr align=left>
                <td>
                    Warehouse Label:</td>
                <td>
                    <asp:TextBox ID="txtLabel" CssClass="InputForm" runat="server" Width="184px" MaxLength="30" size="20" maxsize="30"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLabel" Display="None" ErrorMessage="Warehouse Label is required"></asp:RequiredFieldValidator></td>
            </tr>
            <tr align=left>
                <td>
                    Rail #:</td>
                <td>
                    <asp:TextBox ID="txtRail" runat="server" CssClass="InputForm" Width="184px" MaxLength="30" size="20" maxsize="30"></asp:TextBox></td>
            </tr>
            <tr align=left>
                <td>
                    Country:</td>
                <td>
                    <asp:DropDownList ID="ddlCountry" CssClass="InputForm" runat="server" Visible="True" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr align=left>
                <td style="height: 15px">
                    State:</td>
                <td style="height: 15px">
                    <asp:DropDownList ID="ddlState" CssClass="InputForm" runat="server" Visible="True">
                    </asp:DropDownList></td>
            </tr>
            <tr align=left>
                <td>
                    City:</td>
                <td>
                    <asp:TextBox ID="txtCity" runat="server" CssClass="InputForm" MaxLength="25" size="20" maxsize="30" Visible="True"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ControlToValidate="txtCity" Display="None" ErrorMessage="City is required"></asp:RequiredFieldValidator></td>
            </tr>
        </table>
        <p align="center">
            <asp:Button ID="btnSave" runat="server" CssClass="Content Color2" Text="Save" OnClick="btnSave_Click"></asp:Button>&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnCancel" runat="server" CssClass="Content Color2" Text="Cancel" CausesValidation="False" OnClick="btnCancel_Click"></asp:Button><br /><br /></p>
    </asp:Panel>
</asp:Content>
