using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using localhost;
using System.Text.RegularExpressions;
using TPE.Utility;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Contact_Comment.
	/// </summary>
	public partial class User_Details : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.RadioButton rbtnGreat;
		protected System.Web.UI.WebControls.RadioButton rbtnFair;
		protected System.Web.UI.WebControls.RadioButton rbtnPoor;
		protected System.Web.UI.WebControls.RadioButton rbtnMessage;
		protected System.Web.UI.WebControls.RadioButton rbtnEmail;
		protected System.Web.UI.WebControls.CheckBox cbEmailSubscriber;
		
		protected System.Web.UI.WebControls.Button btnSendEmail;
		protected System.Web.UI.WebControls.Label lblGradeID;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			HelperFunction.InsertConfirmationCode(this);
			
			if (Request.UserHostAddress.ToString() != ConfigurationSettings.AppSettings["OfficeIP"].ToString())
			{
                if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "T") && ((string)Session["Typ"] != "L"))
				{
					Response.Redirect("/default.aspx");
				}
			}
			ViewState["ID"] = Request.QueryString["ID"].ToString();
			if (!IsPostBack)
			{
				// defaults to the logged in user
				if (Session["ID"] != null)
				{
					ViewState["BrokerID"] = Session["ID"].ToString();
				}
				else
				{
					ViewState["BrokerID"] = "-1";					
				}
				this.btnCancel.Attributes.Add("OnClick","window.close();");
				
				Bind();
				
				
			}
			// create list of resins
			Build_Resin_Table();
			
			
		}
		
		private void Bind()
		{
			SqlDataAdapter dadContent;
			DataSet dstContent;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			
			BindNotes();

			SqlDataReader dtrPerson;
			SqlCommand cmdPerson = new SqlCommand("SELECT *,CITY=(select DISTANCE_CITY +', '+DISTANCE_STATE from DISTANCE_ZIPCODE WHERE DISTANCE_ZIPCODE =P.PERS_ZIP ),(SELECT PERS_FRST_NAME FROM PERSON B WHERE B.PERS_ID = P.PERS_ACES ) AS BROKER FROM PERSON P WHERE PERS_ID = '"+ViewState["ID"]+"'",conn);
			dtrPerson = cmdPerson.ExecuteReader();

			if (dtrPerson.Read())
			{
				if (dtrPerson["PERS_ACES"].ToString() !="-1") // overwrite the BrokerID with a allocated broker
				{
					ViewState["BrokerID"] = dtrPerson["PERS_ACES"].ToString() ;
				}
				// adds ports 
				SqlConnection conn2;
				conn2 = new SqlConnection(Application["DBconn"].ToString());
				conn2.Open();
				SqlDataReader dtrPort;
				SqlCommand cmdPort = new SqlCommand("select PORT_ID, PORT= PORT_CITY +', ' + (SELECT CTRY_NAME FROM COUNTRY WHERE CTRY_CODE=PORT_COUNTRY)from INTERNATIONAL_PORT ORDER BY PORT",conn2);
				dtrPort = cmdPort.ExecuteReader();
				// preselecting the Lead Category
				try
				{
					ddlLeadCategory.SelectedIndex = ddlLeadCategory.Items.IndexOf(ddlLeadCategory.Items.FindByText(dtrPerson["PERS_CATEGORY"].ToString()));
						
				}
				catch
				{
					ddlLeadCategory.SelectedIndex = ddlLeadCategory.Items.IndexOf(ddlLeadCategory.Items.FindByText("Please Select"));
				}
				// build the port drop down
				ddlPort.Items.Clear();
				ddlPort.Items.Add(new ListItem ("Select Port","*"));		
				ListItem lstPort;
				while (dtrPort.Read())
				{
					lstPort = new ListItem(dtrPort["PORT"].ToString(),dtrPort["PORT_ID"].ToString());
					if (dtrPort["PORT_ID"].ToString() ==dtrPerson["PERS_PORT"].ToString() )
					{
						lstPort.Selected = true;
					}
					ddlPort.Items.Add(lstPort);
				}
				dtrPort.Close();
				conn2.Close();
				// finish adding ports
				
				

				ViewState["Pref"] = dtrPerson["PERS_PREF"].ToString();
				// add the broker lable 
				txtZip.Text = getSafeString(dtrPerson["PERS_ZIP"]);
				lblBroker.Text = getSafeString(dtrPerson["BROKER"]);
				txtFirstName.Text = getSafeString(dtrPerson["PERS_FRST_NAME"]);
				txtLastName.Text = getSafeString(dtrPerson["PERS_LAST_NAME"]);
				txtTitle.Text = getSafeString(dtrPerson["PERS_TITL"]);
				lblCity.Text = getSafeString(dtrPerson["CITY"]);
				if (!(getSafeString(dtrPerson["PERS_TYPE"])=="D"))
				{
					// exchange users (defined by PERS_TYPE ="D")
					txtCompanyName.Visible = false;
					lblCompanyName.Visible = false;
				}
				else
				{
					txtCompanyName.Text = getSafeString(dtrPerson["COMP_NAME"]);
				}
				lblCustomerName.Text = "Customer Details: "+getSafeString(dtrPerson["PERS_FRST_NAME"]) +" "+getSafeString(dtrPerson["PERS_LAST_NAME"]);
				txtPhone.Text = getSafeString(dtrPerson["PERS_PHON"]);
				txtEmail.Text = getSafeString(dtrPerson["PERS_MAIL"]);
				lblOldEmail.Text = getSafeString(dtrPerson["PERS_MAIL"]);
				if (dtrPerson["PERS_LOGN"] == null)
				{
					// user are based on emails addresses and not usernames
					// this is only displayed for legacy users 
					lblUsername.Visible = false;
					lblUsernameLable.Visible = false;
				}
				else
				{
					lblUsername.Text = getSafeString(dtrPerson["PERS_LOGN"]).ToString();
				}
				
				txtPassword.Text = Crypto.Decrypt(getSafeString(dtrPerson["PERS_PSWD"]));
				lblLastLogin.Text = getSafeString(dtrPerson["PERS_LAST_CHKD"]);
				lblDateRegistered.Text = getSafeString(dtrPerson["PERS_DATE"]);
				lblTotalLogin.Text = getSafeString(dtrPerson["PERS_NUMBER_LOGINS"]);

				rblEmailFrequency.SelectedValue = dtrPerson["Email_ENBL"].ToString();
				
				if (getSafeString(dtrPerson["PERS_PRMLY_INTRST"]) == "2")
					rdbOutsideUS.Checked = true;
				else
					rdbInsideUS.Checked = true;
				
				string strSize = getSafeString(dtrPerson["PERS_INTRST_SIZE"]).Trim();
				string strQuality = getSafeString(dtrPerson["PERS_INTRST_QUALT"]).Trim();
				
				if (strSize.Length==3)
				{
					chkSize1.Checked = strSize[0].Equals('1')? true:false;
					chkSize2.Checked = strSize[1].Equals('1')? true:false;
					chkSize3.Checked = strSize[2].Equals('1')? true:false;
				}
				else
				{
					chkSize1.Checked = false;
					chkSize2.Checked = false;
					chkSize3.Checked = false;
				}

				if (strQuality.Length==3)
				{
					chkQuality1.Checked = strQuality[0].Equals('1')? true:false;
					chkQuality2.Checked = strQuality[1].Equals('1')? true:false;
					chkQuality3.Checked = strQuality[2].Equals('1')? true:false;
				}
				else
				{
					chkQuality1.Checked = false;
					chkQuality2.Checked = false;
					chkQuality3.Checked = false;
				}
			}

			// sets up the ddl the the persons 'status'.  this individualy adds the list items and set them selected 
			// when they are = to the already stored value
			ddlGrade.Items.Clear();
			ListItem lst;
			lst = new ListItem ("Please Select","0" );
			if (getSafeString(dtrPerson["PERS_STAT"])=="0")
			{
				lst.Selected = true;
			}
			ddlGrade.Items.Add (lst );


			lst = new ListItem ("Great (red)","1" );
			if (getSafeString(dtrPerson["PERS_STAT"])=="1")
			{
				lst.Selected = true;
			}
			ddlGrade.Items.Add (lst );

			lst = new ListItem ("Fair (green)","2" );
			if (getSafeString(dtrPerson["PERS_STAT"])=="2")
			{
				lst.Selected = true;
			}
			ddlGrade.Items.Add (lst );

			lst = new ListItem ("Unlikely (orange)","3" );
			if (getSafeString(dtrPerson["PERS_STAT"])=="3")
			{
				lst.Selected = true;
			}
			ddlGrade.Items.Add (lst );

			lst = new ListItem ("Message (blue)","4" );
			if (getSafeString(dtrPerson["PERS_STAT"])=="4")
			{
				lst.Selected = true;
			}
			ddlGrade.Items.Add (lst );
			// List count of Resins
			//lblResinCount.Text = HelperFunction.CountSelectedResins(this.Context,Convert.ToInt32(dtrPerson["PERS_PREF"]));

			// defins the cmdResin before dtrPerson 
			//SqlCommand cmdResin =  new SqlCommand("SELECT CONT_LABL,CONT_ID, CONT_GRADE FROM CONTRACT WHERE POWER(2,CONT_ID-1)&("+getSafeString(dtrPerson["PERS_PREF"])+")<>0 ORDER BY CONT_ORDR",conn);
			
			dtrPerson.Close();

			// binding resin list repeater control
			//SqlDataReader dtrResin;
			
			//dtrResin = cmdResin.ExecuteReader();
			//rptResins.DataSource = dtrResin;
			//rptResins.DataBind();

			//dtrResin.Close();

			//Calendar1.SelectedDate.Month ="11";
			//Calendar1.SelectedDate.Year ="2005";
			//Calendar1.SelectedDate.Day ="20";
			
			conn.Close();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_ItemCommand);
		}
		#endregion
		private void dg_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName == "Delete")
			{
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), "DELETE FROM COMMENT WHERE CMNT_ID = " + e.Item.Cells[4].Text);
				BindNotes();
			}
		}
		/// <summary>
		///	Event that fires when submit is clicked.
		/// Saves task info in db
		/// </summary>
		protected void btnSubmit_Click(object sender, System.EventArgs e)
		{
			SqlConnection conn;
			string strSQL;
		 string test = ViewState["BrokerID"].ToString();
			if(!Calendar1.SelectedDate.ToString().Trim().Equals("0001-1-1"))
			{
				strSQL = "INSERT INTO TASK_LIST (TASK_PRIORITY,TASK_DESCRIPTION,TASK_PERSON,TASK_DUE_DATE,TASK_LEAD_ID) VALUES ('2','"+HelperFunction.RemoveSqlEscapeChars(txtBody.Text)+"','"+ViewState["BrokerID"].ToString()+"','"+Calendar1.SelectedDate.ToString()+"','"+ViewState["ID"].ToString()+"');";
				conn = new SqlConnection(Application["DBconn"].ToString());
				conn.Open();
				SqlCommand cmdNewComment = new SqlCommand(strSQL,conn);
				try
				{
					cmdNewComment.ExecuteNonQuery();
					this.Label1.Text="";
				}
				catch
				{
					this.Label1.Text="please select correctly date";
				}
				conn.Close();
				Bind();
			}
		}

		protected void btnAddNote_Click(object sender, System.EventArgs e)
		{
			

			if (txtComments.Text !="")
			{
				SqlConnection conn;
				string strSQL="";
				if (txtComments.Text.Length > 1000) txtComments.Text = txtComments.Text.Substring(0,1000);
				strSQL = "INSERT INTO COMMENT (CMNT_PERS,CMNT_TEXT,CMNT_DATE) VALUES ("+ViewState["ID"]+",'"+HelperFunction.RemoveSqlEscapeChars(txtComments.Text)+"',getDate());";
				conn = new SqlConnection(Application["DBconn"].ToString());
				conn.Open();
				SqlCommand cmdNewComment = new SqlCommand(strSQL,conn);
				cmdNewComment.ExecuteNonQuery();
				conn.Close();
				Bind();
			}
			
		}
		public void btnUpdate_Click(object sender, System.EventArgs e)
		{			
			
			// insert updated code 
			SqlConnection conn;
			string strSQL;
			string strAllowEmail;
			bool bValidEmail=true;
			
			//Checking if the e-mail address was changed
			if (lblOldEmail.Text.ToLower()!=txtEmail.Text.ToLower())
			{
				//Checking if the e-mail already exists in the Database
				if(HelperFunction.ExistEmail(this.Context, txtEmail.Text))
				{
					bValidEmail = false;
					lblMessage.Text = "E-mail already in use!";
					lblMessage.Visible = true;
				}
			}

			if (bValidEmail)
			{
				lblMessage.Text = "";
				lblMessage.Visible = false;

				strAllowEmail = rblEmailFrequency.SelectedValue.ToString();
				
				
				string strSQL2 = "";
				strSQL2 += ", PERS_PRMLY_INTRST = " + (rdbInsideUS.Checked ? "'1'": "'2'");
				strSQL2 += ", PERS_INTRST_SIZE = " + "'" + Bit(chkSize1.Checked)+ Bit(chkSize2.Checked)+ Bit(chkSize3.Checked) + "'";
				strSQL2 += ", PERS_INTRST_QUALT = " + "'" + Bit(chkQuality1.Checked)+ Bit(chkQuality2.Checked)+ Bit(chkQuality3.Checked) + "'";
				if (txtZip.Text != "")
				{
					strSQL2 += ", PERS_ZIP='"+txtZip.Text+"' ";
					
				}
				else
				{
					strSQL2 += ", PERS_ZIP=null ";
				}
				if (ddlPort.SelectedValue.ToString() !="*")
				{
					strSQL2 += ", PERS_PORT='"+ddlPort.SelectedValue.ToString()+"' ";
				}
				else
				{
					strSQL2 += ", PERS_PORT=null ";
				}

				strSQL = "UPDATE PERSON set PERS_FRST_NAME='"+HelperFunction.RemoveSqlEscapeChars(txtFirstName.Text.ToString())+"',PERS_LAST_NAME='"+HelperFunction.RemoveSqlEscapeChars(txtLastName.Text.ToString())+"',PERS_TITL='"+HelperFunction.RemoveSqlEscapeChars(txtTitle.Text.ToString())+"',COMP_NAME='"+HelperFunction.RemoveSqlEscapeChars(txtCompanyName.Text.ToString())+"',PERS_PHON='"+HelperFunction.RemoveSqlEscapeChars(txtPhone.Text.ToString())+"' ,PERS_PSWD='"+ Crypto.Encrypt(HelperFunction.RemoveSqlEscapeChars(txtPassword.Text)) + "' ,EMAIL_ENBL='"+strAllowEmail+"', PERS_MAIL = '" + txtEmail.Text +"'" + strSQL2 + " WHERE PERS_ID ='"+ViewState["ID"]+"';";
				InterateControls(this); // iterate through cb controls to determine the resin preferences.  stored as iPref
				// add Person Status
				strSQL += "UPDATE PERSON set PERS_STAT='"+ddlGrade.SelectedValue.ToString()+"', PERS_PREF='"+iPref.ToString()+"' WHERE PERS_ID ='"+ViewState["ID"]+"'";
				//update User Category 
				if (ddlLeadCategory.SelectedValue !="Please Select")
				{
					strSQL += " UPDATE PERSON set PERS_CATEGORY='"+ddlLeadCategory.SelectedValue+"' WHERE PERS_ID ='"+ViewState["ID"]+"'";
				}
				else
				{
					strSQL += " UPDATE PERSON set PERS_CATEGORY=null WHERE PERS_ID ='"+ViewState["ID"]+"'";
				}
				conn = new SqlConnection(Application["DBconn"].ToString());
				conn.Open();
				SqlCommand cmdNewComment = new SqlCommand(strSQL,conn);
				cmdNewComment.ExecuteNonQuery();
				conn.Close();
				Bind();	
				
			}
		}
		
		protected void db_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				if ((e.Item.Cells[3].Text.ToString()!="&nbsp;") && (e.Item.Cells[3].Text.ToString()!=""))
				{
					e.Item.Cells[2].Text = "<a href=javascript:sealWin=window.open('./EmailSystem.aspx?ID="+e.Item.Cells[3].Text.ToString()+ "&DATE=" + e.Item.Cells[1].Text.ToString() + "','win','toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=1000,height=800');void(0)>" + 
					e.Item.Cells[2].Text+"</a>";
					// remove delete button. customers should not be able to delete notes generated by the system 
					e.Item.Cells[0].Controls.Clear(); 
				}
				// remove delete button. customers should not be able to delete notes generated by the system 
				if (e.Item.Cells[2].Text.StartsWith("User Login"))
					e.Item.Cells[0].Controls.Clear();
				
			}
		}
		

		protected void MyCalendar_DayRender (object sender, System.Web.UI.WebControls.DayRenderEventArgs e) 
		{

			/*
			// define a new Style for the current date
			Style todayStyle = new Style();
			todayStyle.BackColor = System.Drawing.Color.Gray;
			todayStyle.ForeColor = System.Drawing.Color.White;
		

			if (e.Day.IsToday) 
			{
				// apply the todayStyle style to the current style dynamically
				e.Cell.ApplyStyle(todayStyle);
			} 
			
			*/
		} 

		private string getSafeString(object objString)
		{
			string ret = "";
			try
			{	
				if (objString!=null)
				{
					ret = objString.ToString();
				}
			}
			finally
			{
				//do nothing
			}
			
			return ret;
		}

		private string Bit(bool condition)
		{
			if (condition)
				return "1";
			else
				return "0";
		}

		private void BindNotes()
		{
			SqlDataAdapter dadContent;
			DataSet dstContent;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();

			dadContent = new SqlDataAdapter("SELECT CMNT_ID, CMNT_TEXT,CMNT_DATE, CMNT_EMAIL_SYSTEM_ID FROM COMMENT WHERE CMNT_PERS ='"+ViewState["ID"].ToString()+"' ORDER BY CMNT_DATE DESC",conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dg.DataSource = dstContent;
			dg.DataBind();

			conn.Close();
		}



		protected void dg_PreRender(object sender, System.EventArgs e)
		{
			foreach(DataGridItem item in dg.Items)
			{
				if (item.Cells[0].Controls.Count > 0) // only add if the button has not been removed
					HelperFunction.InsertControlConfirmationCode((WebControl)item.Cells[0].Controls[0],"Are you sure you want to delete this note?");
			}
		}

//		private void rptResins_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
//		{
//			((CheckBox)e.Item.FindControl("CheckBoxResin")).Text = "<a href='/research/dashboard.aspx?Cont_Id=" + DataBinder.Eval(e.Item.DataItem, "CONT_ID") + "' target='_blank'>" + DataBinder.Eval(e.Item.DataItem, "CONT_LABL") + "</a><br>";
//			((Label)e.Item.FindControl("lblGradeID")).Text = System.Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CONT_GRADE"));
//		}

//		private void btnSendEmail_Click(object sender, System.EventArgs e)
//		{
//			string strEmailGrades = "";
//			for(int i=0;i<rptResins.Items.Count;i++)
//			{
//				if (((CheckBox)rptResins.Items[i].FindControl("CheckBoxResin")).Checked)
//				{
//					strEmailGrades += ((Label)rptResins.Items[i].FindControl("lblGradeID")).Text + ",";
//				}
//			}
//
//			if (strEmailGrades != "")
//			{
//				strEmailGrades = strEmailGrades.Substring(0, strEmailGrades.Length-1);
//				Session["strEmailOffer_Address"] = ViewState["ID"].ToString();
//				Session["strEmailGrades"] = strEmailGrades;
//				Response.Redirect ("/Administrator/SendEmail.aspx");
//			}
//		}
		private void Build_Resin_Table()
		{

			ArrayList ALResin = new ArrayList();
						
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();

			// defins the cmdResin before dtrPerson 
			SqlCommand cmdResin =  new SqlCommand("SELECT CONT_LABL,CONT_ID, CONT_GRADE FROM CONTRACT WHERE POWER(2,CONT_ID-1)&("+ViewState["Pref"].ToString()+")<>0 ORDER BY CONT_ORDR",conn);
			
			// binding resin list repeater control
			SqlDataReader dtrResin;
			
			dtrResin = cmdResin.ExecuteReader();
			while(dtrResin.Read())
			{
				ALResin.Add(dtrResin["CONT_ID"].ToString());

			}

			dtrResin.Close();

			ALResin.TrimToSize();
				
			TableRow tr;

			TableCell td1;
			TableCell td2;
//
//			tr.Cells.Add(td1);
//			td2 = new TableCell();
//			td2.HorizontalAlign = HorizontalAlign.Center;
//			td2.VerticalAlign = VerticalAlign.Middle;
//
//			Label lblHeader = new Label();
//			lblHeader.ID = "Header_" ;
//			lblHeader.Text = "Resins";
//			lblHeader.Font.Size =7;
//			// Add control to the table cell
//			td2.Controls.Add(lblHeader);
//			// Add cell to the row
//			tr.Cells.Add(td2);
//			// Add row to the table.
//
//			tblDynamic.Rows.Add(tr);

			// select all contract
			// objects are being reused
			cmdResin =  new SqlCommand("SELECT CONT_LABL,CONT_ID, CONT_GRADE FROM CONTRACT  ORDER BY CONT_ORDR",conn);
			dtrResin = cmdResin.ExecuteReader();
			while(dtrResin.Read())
			{
				tr = new TableRow();
				td1 = new TableCell();
				td1.Wrap = false;
				CheckBox cb = new CheckBox();
				cb.ID = "cbResin_" + dtrResin["CONT_ID"].ToString().Trim();
				if (ALResin.IndexOf(dtrResin["CONT_ID"].ToString().Trim()) >= 0)
				{
					cb.Checked =true;
				}
				//cb.
				td1.Controls.Add(cb);
				td2 = new TableCell();
				td2.Text= dtrResin["CONT_LABL"].ToString().Trim();
				td2.Wrap = false;
				
				
				tr.Controls.Add(td1);
				tr.Controls.Add(td2);
				tblDynamic.Rows.Add(tr);
			}
			// close datareaders
			dtrResin.Close();
			conn.Close();
		
		}
		/// <summary>
		/// interate through all controls and create sql statement to pass the updates to the db.
		/// Function is called recursivly so the string is stored within another variable
		/// </summary>
		/// <param name="parent"></param>
		///		
		int iPref=0;
		void InterateControls(Control parent)
		{
		
			foreach (Control child in parent.Controls)
				
			{
				
				if (child.GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox") && child.ID.IndexOf("cbResin_") == 0)
				{
					CheckBox checkbox  = (CheckBox)child;
					if (checkbox.Checked)

					{
						string[] arrCheckBoxID;
						Regex r = new Regex("_"); // Split apart id to reveal the needed values.
						arrCheckBoxID = r.Split(child.ID.ToString()) ;
						iPref += (int)Math.Pow(2.0,(Convert.ToInt32(arrCheckBoxID[1])-1)); // checked box found! add to total
						
						
		//				arrCheckBoxID[1].ToString()+"<BR>");
					

					}
					
				}
        
				if (child.Controls.Count > 0)
				{          
					InterateControls(child);          
				}
			}
		}
	
	}
}