using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Text;
using TPE.Utility;

namespace localhost.Common
{
	/// <summary>
	/// Summary description for Order_Documents.
	/// </summary>
	public partial class Company_Info : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.HyperLink Hyperlink1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl FileDetails;
		protected System.Web.UI.HtmlControls.HtmlGenericControl FileName;
		protected System.Web.UI.HtmlControls.HtmlGenericControl MyContentType;
		protected System.Web.UI.HtmlControls.HtmlGenericControl ContentLength;
		protected System.Web.UI.WebControls.ListBox ListBox1;
		protected System.Web.UI.WebControls.ListBox ListBox2;
		protected System.Web.UI.WebControls.ListBox Inbox;
		protected System.Web.UI.WebControls.ListBox Employeebox;
		protected System.Web.UI.WebControls.ListBox ddlInbox;
		protected System.Web.UI.WebControls.ListBox ddlEmployeebox;
		protected System.Web.UI.WebControls.HyperLink del;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				//only adminstrator and borker can access this page.
                if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "L"))
				{
					Response.Redirect("../default.aspx");
				}
				
				if (!IsPostBack)
				{
					Bind_Controls();
				}
			}
		}

		public void Bind_Controls()
		{
			// Binding Company Info and Bank Info
			Hashtable param = new Hashtable();
			param.Add("@Id",Request.QueryString["Id"].ToString());
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			try
			{
				conn.Open();
				SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn,"Select COMP_NAME, COMP_URL,COMP_TYPE, BANK_NAME, BANK_ACNT_NB, BANK_PERS, BANK_ADDR_ONE, BANK_ADDR_TWO, BANK_CITY, BANK_ZIP, BANK_STAT, BANK_CTRY, BANK_PHON, BANK_FAX, BANK_ABA, BANK_REP_NUMBER,(SELECT CAST(ROUND( CRED_AMNT, 0) AS int) FROM CREDIT WHERE CRED_COMP=COMP_ID) As CRED_AMNT,(SELECT CAST(ROUND( CRED_AVLB, 0) AS int) FROM CREDIT WHERE CRED_COMP=COMP_ID) As CRED_AVLB, COMP_CREDIT_LIMIT, COMP_AVRG_PAY_DAYS from COMPANY, BANK Where COMP_ID=@Id And BANK_COMP=COMP_ID", param);
                try
                {
                    while (dtr.Read())
                    {
                        lblCompanyID.Text = Request.QueryString["Id"].ToString();
                        txtCompanyName.Text = HelperFunction.getSafeStringFromDB(dtr["COMP_NAME"]);
                        lblCompanyName.Text = txtCompanyName.Text;
                        txtWebSite.Text = HelperFunction.getSafeStringFromDB(dtr["COMP_URL"]);
                        txtBankName.Text = HelperFunction.getSafeStringFromDB(dtr["BANK_NAME"]);
                        txtABANumber.Text = HelperFunction.getSafeStringFromDB(dtr["BANK_ABA"]);
                        txtRepetitiveNumber.Text = HelperFunction.getSafeStringFromDB(dtr["BANK_REP_NUMBER"]);
                        lblOriginalValue.Text = HelperFunction.getSafeDecimalFromDB(dtr["COMP_CREDIT_LIMIT"]).ToString();
                        txtCreditLimit.Text = String.Format("{0:c}", HelperFunction.getSafeDecimalFromDB(dtr["COMP_CREDIT_LIMIT"]));
                        txtTerms.Text = HelperFunction.getSafeLongFromDB(dtr["COMP_AVRG_PAY_DAYS"]).ToString();
                        lblOriginalTermsValue.Text = HelperFunction.getSafeDecimalFromDB(dtr["COMP_AVRG_PAY_DAYS"]).ToString();
                        txtContact.Text = HelperFunction.getSafeStringFromDB(dtr["BANK_PERS"]);
                        txtAccountNumber.Text = HelperFunction.getSafeStringFromDB(dtr["BANK_ACNT_NB"]);
                        txtAddress.Text = HelperFunction.getSafeStringFromDB(dtr["BANK_ADDR_ONE"]);
                        txtAddress2.Text = HelperFunction.getSafeStringFromDB(dtr["BANK_ADDR_TWO"]);
                        txtCity.Text = HelperFunction.getSafeStringFromDB(dtr["BANK_CITY"]);
                        txtZip.Text = HelperFunction.getSafeStringFromDB(dtr["BANK_ZIP"]);
                        txtState.Text = HelperFunction.getSafeStringFromDB(dtr["BANK_STAT"]);
                        txtPhone.Text = HelperFunction.getSafeStringFromDB(dtr["BANK_PHON"]);
                        txtFax.Text = HelperFunction.getSafeStringFromDB(dtr["BANK_FAX"]);
                    }
                }
                finally
                {
                    dtr.Close();
                }
			}
			finally
			{
				conn.Close();
			}

			// Binding Location Info
			string sql = "SELECT (CASE PLAC_TYPE WHEN 'H' THEN 'Headquarters' WHEN 'S' THEN 'Shipping Point' ELSE 'Delivery Point' END) AS PLAC_TYPE, PLAC_ID, PLAC_ZIP, " +
								"(SELECT LOCL_CITY + ', ' + LOCL_STAT FROM LOCALITY WHERE LOCL_ID = PLAC_LOCL) AS PLAC_LOCATION, PLAC_COMP " +
						 "FROM PLACE WHERE (PLAC_TYPE IS NOT NULL) AND (PLAC_COMP = "+ Request.QueryString["Id"].ToString() + ") ORDER BY PLAC_LOCATION";
			DBLibrary.BindDataGrid(Application["DBConn"].ToString(),dgLocations,sql);
    
			// Binding User Info
			sql ="SELECT PERS_COMP,PERS_TYPE,PERS_ID,(PERS_FRST_NAME + ' ' + PERS_LAST_NAME) AS PERS_NAME, (CASE PERS_TYPE WHEN 'O' THEN 'Officer' WHEN 'P' THEN 'Purchaser' WHEN 'S' THEN 'Supplier' WHEN 'A' THEN 'Admin' WHEN 'B' THEN 'Broker' END) AS PERS_TYPE_DESC " +
				 "FROM PERSON WHERE PERS_COMP=" + Request.QueryString["Id"].ToString()+" ORDER BY PERS_NAME";
			DBLibrary.BindDataGrid(Application["DBConn"].ToString(),dgUsers,sql);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgUsers.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgUsers_ItemCommand);
			this.dgLocations.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgLocations_ItemCommand);

		}
		#endregion

		protected void dgLocations_PreRender(object sender, System.EventArgs e)
		{
			for(int i=0;i<dgLocations.Items.Count;i++)
			{
				((LinkButton)dgLocations.Items[i].Cells[0].Controls[0]).Text = dgLocations.Items[i].Cells[1].Text;
				if (dgLocations.Items[i].Cells[3].Text == "Headquarters")
				{
					((LinkButton)dgLocations.Items[i].Cells[5].Controls[0]).Visible = false;
				}
				else
				{ //if Location has been used, cannot delete as well
					if (UsedLocation(dgLocations.Items[i].Cells[2].Text))
					{
						((LinkButton)dgLocations.Items[i].Cells[5].Controls[0]).Visible = false;
					}
				}
			}
		}

		private bool UsedLocation(string PlaceID)
		{
			bool bExist = false;
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			Hashtable param = new Hashtable();
			//string strSelect = "SELECT 1 FROM SHIPMENT WHERE SHIPMENT_FROM = " + PlaceID + " OR SHIPMENT_TO = " + PlaceID;
			string strSelect = "SELECT 1 FROM SHIPMENT WHERE SHIPMENT_FROM = @PlaceID OR SHIPMENT_TO = @PlaceID";
			param.Add("@PlaceID",PlaceID);
			try
			{
				conn.Open();
				SqlDataReader dtPlace = DBLibrary.GetDataReaderFromSelect(conn,strSelect,param);
				if (dtPlace.Read()) bExist = true;
			}
			finally
			{
				conn.Close();
			}
			return bExist;
		}

		protected void btnAddNewUser_Click(object sender, System.EventArgs e)
		{
			//Go to Add Location Screen. LocationID=0 means the action is 'add new'
			//Server.Transfer("../administrator/Edit_User.aspx?UserID=0&CompanyID=" + lblCompanyID.Text);
            Response.Redirect("../administrator/Edit_User.aspx?UserID=0&CompanyID=" + lblCompanyID.Text);
		}

		protected void dgUsers_PreRender(object sender, System.EventArgs e)
		{
			for(int i=0;i<dgUsers.Items.Count;i++)
			{
				((LinkButton)dgUsers.Items[i].Cells[0].Controls[0]).Text = dgUsers.Items[i].Cells[1].Text;
			}
		}

		protected void btnUpdateCompanyInfo_Click(object sender, System.EventArgs e)
		{
			if (btnUpdateCompanyInfo.Text == "Update")
			{
				btnUpdateCompanyInfo.Text = "Save";
				txtCreditLimit.Text = lblOriginalValue.Text;
				txtTerms.Text = lblOriginalTermsValue.Text;
				ScreenMode("EditCompanyInfo");
			}
			else
			{
				// Update Company table
				string strSQL;
				Hashtable param = new Hashtable();
				//strSQL="UPDATE COMPANY SET COMP_NAME= '"+txtCompanyName.Text+"', COMP_CREDIT_LIMIT= '"+txtCreditLimit.Text +"', COMP_AVRG_PAY_DAYS= '"+txtTerms.Text +"', COMP_URL='"+txtWebSite.Text+"' Where COMP_ID=" + lblCompanyID.Text;
				strSQL="UPDATE COMPANY SET COMP_NAME= @CompanyName, COMP_CREDIT_LIMIT= @CreditLimit, COMP_AVRG_PAY_DAYS= @Terms, COMP_URL=@WebSite Where COMP_ID=@CompanyID";
				param.Add("@CompanyName",txtCompanyName.Text);
				param.Add("@CreditLimit",txtCreditLimit.Text);
				param.Add("@Terms",txtTerms.Text);
				param.Add("@WebSite",txtWebSite.Text);
				param.Add("@CompanyID",lblCompanyID.Text);
				
				DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), strSQL, param);

				// Update Credit table.
				if(txtCreditLimit.Text == "") txtCreditLimit.Text = "0";				
				//strSQL="UPDATE CREDIT SET CRED_AMNT= '" + txtCreditLimit.Text + "' Where CRED_COMP="  + lblCompanyID.Text;
				strSQL="UPDATE CREDIT SET CRED_AMNT= @CreditLimit Where CRED_COMP=@CompanyID";
				param.Clear();//txtCreditLimit.Text + "' Where CRED_COMP="  + lblCompanyID.Text;
				param.Add("@CreditLimit",txtCreditLimit.Text);
				param.Add("@CompanyID",lblCompanyID.Text);				
				DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), strSQL, param);
    
				//Update Information in Database
				btnCancelUpdateCompany_Click(sender, e);
			}
		}

		private void dgLocations_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName == "Delete")
			{
				Hashtable param = new Hashtable();
				
				String strSQL = "DELETE PLACE WHERE PLAC_ID = @PlacID";
				param.Add("@PlacID",e.Item.Cells[2].Text);
				DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), strSQL, param);
				Bind_Controls();
			}
			else if (e.CommandName == "Edit")
			{
				//Go to Edit Screen
				//Server.Transfer("../administrator/Edit_Location.aspx?LocationID="+e.Item.Cells[2].Text + "&CompanyID=" + lblCompanyID.Text);
                Response.Redirect("../administrator/Edit_Location.aspx?LocationID=" + e.Item.Cells[2].Text + "&CompanyID=" + lblCompanyID.Text);
			}
		}

		private void dgUsers_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName == "Delete")
			{
				//Before delete, is necessary to check if it can be deleted... action inactive for awhile
//				String strSQL = "DELETE PERSON WHERE PERS_ID = " + e.Item.Cells[2].Text;
//				DBLib.ExecuteSQLStatement(this.Context,strSQL);
//				Bind_Controls();
			}
			else if (e.CommandName == "Edit")
			{
				//Go to Edit Screen
				Server.Transfer("../administrator/Edit_User.aspx?UserID="+e.Item.Cells[2].Text + "&CompanyID=" + lblCompanyID.Text);
			}
		}

		protected void btnAddNewLocation_Click(object sender, System.EventArgs e)
		{
			//Go to Add Location Screen. LocationID=0 means the action is 'add new'
			Server.Transfer("../administrator/Edit_Location.aspx?LocationID=0&CompanyID=" + lblCompanyID.Text);
		}

		/// <summary>
		/// Permit set up the screen according the edit/consult mode
		/// You may you: "EditCompanyInfo", "EditBankInfo" or "Consult"
		/// </summary>
		private void ScreenMode(string strMode)
		{
			bool enableCompanyInfo = strMode=="EditCompanyInfo";
			txtCompanyName.Enabled = enableCompanyInfo;
			txtWebSite.Enabled = enableCompanyInfo;
			txtCreditLimit.Enabled = enableCompanyInfo;
			txtTerms.Enabled = enableCompanyInfo;
			btnCancelUpdateCompany.Visible = enableCompanyInfo;

			bool enableBankInfo = strMode=="EditBankInfo";
			txtBankName.Enabled = enableBankInfo;
			txtABANumber.Enabled = enableBankInfo;
			txtRepetitiveNumber.Enabled = enableBankInfo;
			txtContact.Enabled = enableBankInfo;
			txtAccountNumber.Enabled = enableBankInfo;
			txtAddress.Enabled = enableBankInfo;
			txtAddress2.Enabled = enableBankInfo;
			txtCity.Enabled = enableBankInfo;
			txtZip.Enabled = enableBankInfo;
			txtState.Enabled = enableBankInfo;
			txtPhone.Enabled = enableBankInfo;
			txtFax.Enabled = enableBankInfo;
			btnCancelUpdateBankInfo.Visible = enableBankInfo;

			bool enableUsersLocations = strMode=="Consult";
			dgUsers.Enabled = enableUsersLocations;
			btnAddNewUser.Enabled = enableUsersLocations;
			dgLocations.Enabled = enableUsersLocations;
			btnAddNewLocation.Enabled = enableUsersLocations;

			//Exceptions
			btnUpdateCompanyInfo.Enabled = enableUsersLocations || enableCompanyInfo;
			btnUpdateBankInfo.Enabled = enableUsersLocations || enableBankInfo;
		}

		protected void btnUpdateBankInfo_Click(object sender, System.EventArgs e)
		{
			if (btnUpdateBankInfo.Text == "Update")
			{
				btnUpdateBankInfo.Text = "Save";
				ScreenMode("EditBankInfo");
			}
			else
			{
				// Update Bank table
				string strSQL;
				Hashtable param = new Hashtable();
				
				strSQL="Update BANK set BANK_NAME=@BankName, BANK_ACNT_NB=@AccountNumber, BANK_REP_NUMBER=@RepetitiveNumber, BANK_PERS=@Contact, BANK_ADDR_ONE=@Address, BANK_ADDR_TWO=@Address2, BANK_CITY=@City, BANK_ZIP=@Zip, BANK_STAT=@State, BANK_PHON=@Phone, BANK_FAX=@Fax, BANK_ABA=@ABANumber Where BANK_COMP=@CompanyID";
				param.Add("@BankName",txtBankName.Text);
				param.Add("@AccountNumber",txtAccountNumber.Text);
				param.Add("@RepetitiveNumber",txtRepetitiveNumber.Text);
				param.Add("@Contact",txtContact.Text);
				param.Add("@Address",txtAddress.Text);
				param.Add("@Address2",txtAddress2.Text);
				param.Add("@City",txtCity.Text);
				param.Add("@Zip",txtZip.Text);
				param.Add("@State",txtState.Text);
				param.Add("@Phone",txtPhone.Text);
				param.Add("@Fax",txtFax.Text);
				param.Add("@ABANumber",txtABANumber.Text);
				param.Add("@CompanyID",lblCompanyID.Text);
								
				DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), strSQL, param);
    
				//Update Information in Database
				btnCancelUpdateBankInfo_Click(sender, e);
			}
		}

		protected void btnCancelUpdateCompany_Click(object sender, System.EventArgs e)
		{
			btnUpdateCompanyInfo.Text = "Update";
			ScreenMode("Consult");
			Bind_Controls();
		}

		protected void btnCancelUpdateBankInfo_Click(object sender, System.EventArgs e)
		{
			btnUpdateBankInfo.Text = "Update";
			ScreenMode("Consult");
			Bind_Controls();
		}

		protected void txtAccountNumber_TextChanged(object sender, System.EventArgs e)
		{
		
		}
	}
}