<%@ Page Language="c#" Codebehind="User_Requests.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.User_Requests" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/TemplateWithoutInstructions.Master" %>
<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
    <style>
    .menuskin { BORDER-RIGHT: black 2px solid; BORDER-TOP: black 2px solid; Z-INDEX: 100; VISIBILITY: hidden; FONT: 12px/18px Verdana; BORDER-LEFT: black 2px solid; WIDTH: 365px; BORDER-BOTTOM: black 2px solid; POSITION: absolute; BACKGROUND-COLOR: menu }
	.menuskin A { PADDING-RIGHT: 10px; PADDING-LEFT: 10px; COLOR: black; TEXT-DECORATION: none }
	#mouseoverstyle { BACKGROUND-COLOR: menu }
	#mouseoverstyle A { COLOR: red }
	</style>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphJavaScript">

    <script language="JavaScript1.2">

        //Pop-it menu- By Dynamic Drive
        //For full source code and more DHTML scripts, visit http://www.dynamicdrive.com
        //This credit MUST stay intact for use

        var linkset=new Array()
        var companylinkset=new Array()
        //SPECIFY MENU SETS AND THEIR LINKS. FOLLOW SYNTAX LAID OUT

        <asp:PlaceHolder id="phActionButton"  runat="server" />
        ////No need to edit beyond here

        var ie4=document.all&&navigator.userAgent.indexOf("Opera")==-1
        var ns6=document.getElementById&&!document.all
        var ns4=document.layers

        function showmenu(e,which){

        if (!document.all&&!document.getElementById&&!document.layers)
        return

        clearhidemenu()

        menuobj=ie4? document.all.popmenu : ns6? document.getElementById("popmenu") : ns4? document.popmenu : ""
        menuobj.thestyle=(ie4||ns6)? menuobj.style : menuobj

        if (ie4||ns6)
        menuobj.innerHTML=which
        else{
        menuobj.document.write('<layer name=gui bgColor=#E6E6E6 width=165 onmouseover="clearhidemenu()" onmouseout="hidemenu()">'+which+'</layer>')
        menuobj.document.close()
        }

        menuobj.contentwidth=(ie4||ns6)? menuobj.offsetWidth : menuobj.document.gui.document.width
        menuobj.contentheight=(ie4||ns6)? menuobj.offsetHeight : menuobj.document.gui.document.height
        eventX=ie4? event.clientX : ns6? e.clientX : e.x
        eventY=ie4? event.clientY : ns6? e.clientY : e.y

        //Find out how close the mouse is to the corner of the window
        var rightedge=ie4? document.body.clientWidth-eventX : window.innerWidth-eventX
        var bottomedge=ie4? document.body.clientHeight-eventY : window.innerHeight-eventY

        //if the horizontal distance isn't enough to accomodate the width of the context menu
        if (rightedge<menuobj.contentwidth)
        //move the horizontal position of the menu to the left by it's width
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX-menuobj.contentwidth : ns6? window.pageXOffset+eventX-menuobj.contentwidth : eventX-menuobj.contentwidth
        else
        //position the horizontal position of the menu where the mouse was clicked
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX : ns6? window.pageXOffset+eventX : eventX

        //same concept with the vertical position
        if (bottomedge<menuobj.contentheight)
        menuobj.thestyle.top=ie4? document.body.scrollTop+eventY-menuobj.contentheight : ns6? window.pageYOffset+eventY-menuobj.contentheight : eventY-menuobj.contentheight
        else
        menuobj.thestyle.top=ie4? document.body.scrollTop+event.clientY : ns6? window.pageYOffset+eventY : eventY
        menuobj.thestyle.visibility="visible"
        return false
        }

        function contains_ns6(a, b) {
        //Determines if 1 element in contained in another- by Brainjar.com
        while (b.parentNode)
        if ((b = b.parentNode) == a)
        return true;
        return false;
        }

        function hidemenu(){
        if (window.menuobj)
        menuobj.thestyle.visibility=(ie4||ns6)? "hidden" : "hide"
        }

        function dynamichide(e){
        if (ie4&&!menuobj.contains(e.toElement))
        hidemenu()
        else if (ns6&&e.currentTarget!= e.relatedTarget&& !contains_ns6(e.currentTarget, e.relatedTarget))
        hidemenu()
        }

        function delayhidemenu(){
        if (ie4||ns6||ns4)
        delayhide=setTimeout("hidemenu()",500)
        }

        function clearhidemenu(){
        if (window.delayhide)
        clearTimeout(delayhide)
        }

        function highlightmenu(e,state){
        if (document.all)
        source_el=event.srcElement
        else if (document.getElementById)
        source_el=e.target
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        else{
        while(source_el.id!="popmenu"){
        source_el=document.getElementById? source_el.parentNode : source_el.parentElement
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        }
        }
        }

        if (ie4||ns6)
        document.onclick=hidemenu

    </script>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
    <div class="menuskin" id="popmenu" onmouseover="clearhidemenu();highlightmenu(event,'on')" onmouseout="highlightmenu(event,'off');dynamichide(event)">
    </div>
    <asp:Panel ID="pnlConfirm" runat="server" Height="104px">
        <table id="Table1" height="91" cellspacing="1" cellpadding="1" align="center" border="0" class=Content>
            <tr>
                <td align="center">
                    <asp:Label ID="lblConfirmMessage" runat="server">The request is allocated to someone else. Do you want to continue anyway?</asp:Label></td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr> 
                <td align="center">
                    <asp:Button ID="btnSubmit" runat="server" Width="80px" Text="Yes" OnClick="btnSubmit_Click_1"></asp:Button>&nbsp;
                    <asp:Button ID="btnBack" runat="server" Width="80px" Text="No"></asp:Button></td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlDataChanged" runat="server" HorizontalAlign="Center">
        <asp:Label ID="lblDataChanged" runat="server" ForeColor="Red" Font-Bold="True">The status of this request has been changed. Please redo the operation.</asp:Label>
    </asp:Panel>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" class="Content">
        <tr>
            <td colspan="2" class="Header Bold">
                Date:
                <asp:DropDownList ID="ddlDate" runat="server" Width="120px" OnSelectedIndexChanged="DropDownDate_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList><br>
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:DataGrid ID="dg" BorderWidth="0" BackColor="#000000" CellSpacing="1" runat="server" HorizontalAlign="Center" ShowFooter="True" CellPadding="2" AutoGenerateColumns="False" HeaderStyle-CssClass="Content" AlternatingItemStyle-CssClass="Content" ItemStyle-CssClass="Content" AllowSorting="True">
                <AlternatingItemStyle CssClass="LinkNormal Color2 DarkGray" />
                <ItemStyle CssClass="LinkNormal Color2 LightGray"></ItemStyle>
                <HeaderStyle CssClass="LinkNormal Color2 OrangeColor"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="REQ_ID" SortExpression="REQ_ID ASC" HeaderText="ID">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="false" DataField="REQ_DATE_TIME_ALLOCATED" SortExpression="REQ_DATE_TIME_ALLOCATED ASC" HeaderText="Date Allocated">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                <asp:Label ID="HeaderDropDown" runat="server" Text="Broker"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlBrokers" runat="server" OnSelectedIndexChanged="DropDown_SelectedIndexChanged" AutoPostBack="True" Width="90px">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="REQ_DATE_TIME" SortExpression="REQ_DATE_TIME ASC" HeaderText="Time">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="REQ_NAME" SortExpression="REQ_NAME ASC" HeaderText="Name">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="REQ_COMPANY" SortExpression="REQ_COMPANY ASC" HeaderText="Company">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="REQ_PHONE" HeaderText="Phone"></asp:BoundColumn>
                        <asp:HyperLinkColumn HeaderText="Email" ItemStyle-Wrap="false" DataNavigateUrlField="REQ_EMAIL" DataNavigateUrlFormatString="mailto:{0}" DataTextField="REQ_EMAIL" ItemStyle-CssClass=LinkNormal />
                        <asp:BoundColumn DataField="REQ_LOCATION" SortExpression="REQ_LOCATION ASC" HeaderText="Location">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="REQ_TRUNC_MESSAGE" HeaderText="Message"></asp:BoundColumn>
                        <asp:BoundColumn DataField="REQ_OFFER_ID" SortExpression="REQ_OFFER_ID ASC" HeaderText="Offer#">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="OFFR_PROD" SortExpression="OFFR_PROD ASC" HeaderText="Product">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="OFFR_PRCE" SortExpression="OFFR_PRCE ASC" HeaderText="Price">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="OFFR_SIZE" SortExpression="OFFR_SIZE ASC" HeaderText="Size">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="OFFR_MELT" SortExpression="OFFR_MELT ASC" HeaderText="Melt">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="OFFR_DENS" SortExpression="OFFR_DENS ASC" HeaderText="Density">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid></td>
        </tr>
    </table>
</asp:Content>
