<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page Language="c#" CodeBehind="Users_Management.aspx.cs" AutoEventWireup="false" Inherits="localhost.Administrator.Users_Management" %>



<form id=Form runat="server"><TPE:TEMPLATE id=Template1 Runat="server" PageTitle="User List"></TPE:TEMPLATE>
<center><span class=PageHeader>Company 
List</SPAN></CENTER><asp:datagrid id=dg runat="server" ShowFooter="false" Width="400px" HorizontalAlign="Center" CellPadding="2" OnItemDataBound="UporDown" AllowSorting="true" onSortCommand="SortDG" AutoGenerateColumns="false" HeaderStyle-CssClass="DataGridHeader" AlternatingItemStyle-CssClass="DataGridRow_Alternate" ItemStyle-CssClass="DataGridRow" CssClass="DataGrid">
<AlternatingItemStyle CssClass="DataGridRow_Alternate">
</AlternatingItemStyle>

<ItemStyle CssClass="DataGridRow">
</ItemStyle>

<HeaderStyle CssClass="DataGridHeader">
</HeaderStyle>

<Columns>
<asp:BoundColumn DataField="COMP_NAME" SortExpression="COMP_NAME ASC" HeaderText="Company Name">
<ItemStyle Wrap="False">
</ItemStyle>
</asp:BoundColumn>
<asp:HyperLinkColumn DataNavigateUrlField="PERS_ID" DataNavigateUrlFormatString="../Common/System_Messages.asp?Id={0}" DataTextField="PERS_LOGN" SortExpression="PERS_LOGN ASC" HeaderText="User Name">
<ItemStyle Wrap="False">
</ItemStyle>
</asp:HyperLinkColumn>
<asp:TemplateColumn HeaderText="Password">
<ItemTemplate>
<asp:Label id=Label1 runat="server">
							<%# Crypto.Decrypt((string)DataBinder.Eval(Container.DataItem,"PERS_PSWD"))%>
					</asp:Label>
</ItemTemplate>
</asp:TemplateColumn>


<asp:HyperLinkColumn DataNavigateUrlField="PERS_ID" DataNavigateUrlFormatString="../common/MB_Specs_User.aspx?ID={0}" DataTextField="PERS_TYPE" SortExpression="PERS_TYPE ASC" HeaderText="Type">
<ItemStyle Wrap="False">
</ItemStyle>
</asp:HyperLinkColumn>
<asp:TemplateColumn>
<HeaderTemplate>
Enabled
</HeaderTemplate>

<ItemTemplate>

</ItemTemplate>
</asp:TemplateColumn>
</Columns>
	</asp:datagrid><TPE:TEMPLATE id=Template2 Runat="server" Footer="true"></TPE:TEMPLATE></FORM>
<!--
<asp:HyperLinkColumn HeaderText="Working Orders" ItemStyle-Wrap="false" DataNavigateUrlField="PERS_ID" DataNavigateUrlFormatString="/Common/Common_Working_Orders.aspx?Id={0}" DataTextField="PERS_WO" SortExpression="PERS_WO ASC" DataTextFormatString="{0:c}" />
			<asp:HyperLinkColumn HeaderText="Filled Orders" ItemStyle-Wrap="false" DataNavigateUrlField="PERS_ID" DataNavigateUrlFormatString="/Common/Filled_Orders.aspx?Id={0}" DataTextField="PERS_FO" SortExpression="PERS_FO ASC" DataTextFormatString="{0:c}" />
-->
