<%@ Page Language="VB" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Web.Mail" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>

<form runat="server" id="Form">
    <TPE:Template PageTitle="" Runat="Server" />
<!--#include FILE="../include/VBLegacy_Code.aspx"-->


<%
'only administrator can access this page
IF Session("Typ")<>"A" THEN Response.Redirect ("../default.aspx")
Dim conn as SqlConnection
Dim cmdContent as SQLCommand
Dim Rec0 as SqlDataReader
conn = new SqlConnection(Application("DBconn"))
conn.Open()
Dim conn1 as SqlConnection
Dim cmdContent1 as SQLCommand
Dim Rec1 as SqlDataReader
conn1 =  new SqlConnection(Application("DBconn"))
conn1.Open()



DIM i,j,k,l
dim Str,Str0,Height,NStr,NStr0, ii,Strin,pi

'To create a new brand, insert it to TEMP_BRAND table, wait for apprival from system administrator
IF Request.Form("NewB")<>"" THEN
	IF Request.Form("NBrand")<>"" THEN
	Application.Lock
	Application("Flag")=1

	cmdContent= new SqlCommand("INSERT INTO TEMP_BRAND (TEMP_BRAN_CONT,TEMP_BRAN_COMP,TEMP_BRAN_NAME,TEMP_BRAN_MELT,TEMP_BRAN_DENS,TEMP_BRAN_IZOD,TEMP_BRAN_DATE) VALUES("&Request.Form("NContract")&","&Request.Form("NCompany")&",'"&Request.Form("NBrand")&"','"&Request.Form("NMelt")&"','"&Request.Form("NDensity")&"','"&Request.Form("NIZOD")&"',GETDATE())", conn)
        cmdContent.ExecuteNonQuery()

	Application("Flag")=0
	Application.UnLock
	END IF
END IF

'To decline a brand, delete it from TEMP_BRAND table
IF Request.Form("Decline")<>"" THEN
	Application.Lock
	Application("Flag")=1
	cmdContent= new SqlCommand("DELETE FROM TEMP_BRAND WHERE TEMP_BRAN_ID="&Request.Form("Decline"), conn)
        cmdContent.ExecuteNonQuery()

	Application("Flag")=0
	Application.UnLock
END IF

'Destruction of all the the offers on this brand without any message!!
IF Request.Form("Delete")<>"" THEN
	Application.Lock
	Application("Flag")=1
	cmdContent= new SqlCommand("UPDATE BRAND SET BRAN_ENBL=0 WHERE BRAN_ID="&Request.Form("Delete"), conn)
        cmdContent.ExecuteNonQuery()

	cmdContent= new SqlCommand("DELETE FROM OFFER WHERE OFFR_BRAN="&Request.Form("Delete"), conn)
        cmdContent.ExecuteNonQuery()
	Application("Flag")=0
	Application.UnLock
END IF

' To approve a brand, move it from TEMP_BRAND to BRAND table, then delete it in TEMP_BRAND
IF Request.Form("Submit")<>"" THEN
	Application.Lock
	Application("Flag")=1

	cmdContent= new SqlCommand("INSERT INTO BRAND (BRAN_CONT,BRAN_COMP,BRAN_NAME,BRAN_ENBL) VALUES("&Request.Form("brand-cont")&","&Request.Form("brand-comp")&",'"&Request.Form("brand-name")&"',1)", conn)
        cmdContent.ExecuteNonQuery()

	cmdContent= new SqlCommand("DELETE FROM TEMP_BRAND WHERE TEMP_BRAN_ID="&Request.Form("Id"), conn)
        cmdContent.ExecuteNonQuery()
	Application("Flag")=0
	Application.UnLock
END IF
Height=260
%>

<!--#INCLUDE FILE="../include/Loading.inc"-->
<!--#INCLUDE FILE="../include/PopUp.inc"-->
<SPAN ID=Main name=Main style=' x:0px; y:0px; visibility: hidden'>
<form method=post name=Form>
<input type=hidden name=SearchSubmit value=''>
<input type=hidden name=ShowAll value=''>
<input type=hidden name=Submit value=''>
<input type=hidden name=NewB value=''>
<%IF Request.Form("Id")<>"" AND Request.Form("Submit")="" AND Request.Form("Delete")="" THEN
IF Request.Form("brand-cont")="" THEN

        cmdContent= new SqlCommand("SELECT TEMP_BRAN_CONT,TEMP_BRAN_COMP,TEMP_BRAN_NAME,TEMP_BRAN_MELT,TEMP_BRAN_DENS,TEMP_BRAN_IZOD,(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=TEMP_BRAN_COMP) FROM TEMP_BRAND WHERE TEMP_BRAN_ID="&Request.Form("Id"), conn)
	Rec0= cmdContent.ExecuteReader()
	Rec0.Read

ELSE

         Dim temp
         temp=Request.Form("brand-cont")
         temp=temp+", "
         temp=temp+Request.Form("brand-comp")
         temp=temp+", "
         temp=temp+Request.Form("brand-name")
         temp=temp+", "
         temp=temp+Request.Form("brand-melt")
         temp=temp+", "
         temp=temp+Request.Form("brand-density")
         temp=temp+", "
         temp=temp+Request.Form("brand-izod")
         temp=temp+", "
         temp=temp+Request.Form("comp-name")


         Dim RecArray() As String=Split(temp,",")


         'Rec0=Array(Request.Form("brand-cont"),Request.Form("brand-comp"),Request.Form("brand-name"),Request.Form("brand-melt"),Request.Form("brand-density"),Request.Form("brand-izod"),Request.Form("comp-name"))
         'response.write (Rec0(0))
END IF

cmdContent= new SqlCommand("SELECT CONT_ID,CONT_LABL,CONT_IZOD_RNG,CONT_MELT_RNG,CONT_DENS_RNG FROM CONTRACT ORDER BY CONT_ORDR", conn1)
Rec1= cmdContent.ExecuteReader()
%>
<table cellspacing=0 border=0>
	<tr>
		<td CLASS=S9>
Contract :&nbsp;<br>
<%
Str=""
Str0=""

While Rec1.Read()

IF Request.Form("brand-cont")="" THEN
	IF CDbl(Rec0(0))=CDbl(Rec1(0)) THEN
		i=Rec1(2)
		j=Rec1(3)
		k=Rec1(4)
	END IF

	Str=Str+"'"&Rec1(0)&"',"
	Str0=Str0+"'"&Rec1(1)&"',"
Else
        Dim temp
	         temp=Request.Form("brand-cont")
	         temp=temp+", "
	         temp=temp+Request.Form("brand-comp")
	         temp=temp+", "
	         temp=temp+Request.Form("brand-name")
	         temp=temp+", "
	         temp=temp+Request.Form("brand-melt")
	         temp=temp+", "
	         temp=temp+Request.Form("brand-density")
	         temp=temp+", "
	         temp=temp+Request.Form("brand-izod")
	         temp=temp+", "
	         temp=temp+Request.Form("comp-name")


         Dim RecArray() As String=Split(temp,",")

        IF CDbl(RecArray(0))=CDbl(Rec1(0)) THEN
		i=Rec1(2)
		j=Rec1(3)
		k=Rec1(4)
	END IF

	Str=Str+"'"&Rec1(0)&"',"
	Str0=Str0+"'"&Rec1(1)&"',"


End If

End While
Rec1.Close

%><script>
<%IF Request.Form("brand-cont")="" THEN%>
Path.SOS(new Array(<%=Left(Str,Len(Str)-1)%>),new Array(<%=Left(Str0,Len(Str0)-1)%>),'<%=Rec0(0)%>',document,'brand-cont','with(window.document){if(Path.Format(Form)){Form.submit()}else{Form.reset()}}')</script>
<%Else
Dim temp
temp=Request.Form("brand-cont")
temp=temp+", "
temp=temp+Request.Form("brand-comp")
temp=temp+", "
temp=temp+Request.Form("brand-name")
temp=temp+", "
temp=temp+Request.Form("brand-melt")
temp=temp+", "
temp=temp+Request.Form("brand-density")
temp=temp+", "
temp=temp+Request.Form("brand-izod")
temp=temp+", "
temp=temp+Request.Form("comp-name")
Dim RecArray() As String=Split(temp,",")
%>
Path.SOS(new Array(<%=Left(Str,Len(Str)-1)%>),new Array(<%=Left(Str0,Len(Str0)-1)%>),'<%=RecArray(0)%>',document,'brand-cont','with(window.document){if(Path.Format(Form)){Form.submit()}else{Form.reset()}}')</script>
<%End IF%>


		</td>
		<td CLASS=S9>
Company :&nbsp;<br><select name=brand-comp><%
cmdContent= new SqlCommand("SELECT COMP_ID,COMP_NAME FROM COMPANY Q0 WHERE (COMP_TYPE='S' OR COMP_TYPE='D') AND (CASE WHEN (SELECT COUNT(*) FROM COMPANY WHERE (COMP_TYPE='S' OR COMP_TYPE='D') AND COMP_NAME LIKE Q0.COMP_NAME)>1 THEN (SELECT COMP_ID FROM COMPANY WHERE COMP_REG=1 AND (COMP_TYPE='S' OR COMP_TYPE='D') AND COMP_NAME LIKE Q0.COMP_NAME) ELSE Q0.COMP_ID END)=COMP_ID ORDER BY COMP_NAME", conn1)
Rec1= cmdContent.ExecuteReader()


Str=""
Str0=""
While Rec1.Read()
	Str=Str+"'"&Rec1(0)&"',"
	Str0=Str0+"'"&Rec1(1)&"',"
End While
Rec1.Close

%>
<%IF Request.Form("brand-cont")="" THEN%>
<script>Path.SO(new Array(<%=Left(Str,Len(Str)-1)%>),new Array(<%=Left(Str0,Len(Str0)-1)%>),'<%=Rec0(1)%>',document)</script>

     	</td>
		<td CLASS=S9>
Name :&nbsp;<br><input type=text name=brand-name value="<%=Rec0(2)%>">
		</td>
	</tr>
	<tr>
		<td CLASS=S9>Izod: <%=i%>&nbsp;<br><input type=text name=brand-izod value="<%=Rec0(5)%>">
		</td>
		<td CLASS=S9>Melt: <%=j%>&nbsp;<br><input type=text name=brand-melt value="<%=Rec0(3)%>">
		</td>
		<td CLASS=S9>Density: <%=k%>&nbsp;<br><input type=text name=brand-density value="<%=Rec0(4)%>">
		</td>
	</tr>

<%Else
Dim temp
temp=Request.Form("brand-cont")
temp=temp+", "
temp=temp+Request.Form("brand-comp")
temp=temp+", "
temp=temp+Request.Form("brand-name")
temp=temp+", "
temp=temp+Request.Form("brand-melt")
temp=temp+", "
temp=temp+Request.Form("brand-density")
temp=temp+", "
temp=temp+Request.Form("brand-izod")
temp=temp+", "
temp=temp+Request.Form("comp-name")
Dim RecArray() As String=Split(temp,",")
%>

<script>Path.SO(new Array(<%=Left(Str,Len(Str)-1)%>),new Array(<%=Left(Str0,Len(Str0)-1)%>),'<%=RecArray(1)%>',document)</script>

     	</td>
		<td CLASS=S9>
Name :&nbsp;<br><input type=text name=brand-name value="<%=RecArray(2)%>">
		</td>
	</tr>
	<tr>
		<td CLASS=S9>Izod: <%=i%>&nbsp;<br><input type=text name=brand-izod value="<%=RecArray(5)%>">
		</td>
		<td CLASS=S9>Melt: <%=j%>&nbsp;<br><input type=text name=brand-melt value="<%=RecArray(3)%>">
		</td>
		<td CLASS=S9>Density: <%=k%>&nbsp;<br><input type=text name=brand-density value="<%=RecArray(4)%>">
		</td>
	</tr>



<%End If%>


	<tr>
		<td colspan=3><center>
<input type=button class=tpebutton value='SUBMIT' onClick="Javascript:with(window.document){if(Path.Format(Form)){Form.Submit.value='Submit';Form.submit()}}">
		</td>
	</tr>
<input type='hidden' name='Id' value='<%=Request.Form("Id")%>'>
</table>
<%

	Rec0.Close


ELSE%>
<input type=hidden name=Column value='<%=Request.Form("Column")%>'>
<input type=hidden name=Order value='<%=Request.Form("Order")%>'>
<input type=hidden name=Id value=''>
<input type=hidden name=Delete value=''>
<input type=hidden name=Decline value=''>
<table border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td colspan=8><center>
Search:&nbsp;<input type=text name=Search size=8 value='<%IF Request.Form("ShowAll")="" THEN Response.write (Request.Form("Search"))%>'>into
<script>Path.SO(new Array('ALL','VARCONT','VARCOMP','TEMP_BRAN_NAME','TEMP_BRAN_DATE'),new Array('All','Contract','Company','Name','Date'),'<%IF Request.Form("ShowAll")="" THEN Response.write (Request.Form("SearchColumn"))%>',document,'SearchColumn')</script>
<input type=button class=tpebutton value="Search" onClick="Javascript:if(Path.Format(document.Form)==true){Path.Wait(window); document.Form.SearchSubmit.value='Submit';document.Form.submit()}">
<input type=button class=tpebutton value="Show All" onclick="Javascript:Path.Wait(window);document.Form.ShowAll.value='Show';document.Form.submit()">
		</td>
	</tr></table>
<script>
function NewB()
{

	{
			if(NBrand.value=='')	return Path.Mess(window,'Please enter the name of the brand.','0')
			Decline.value=''
			Delete.value=''
			Submit.value=''
			NewB.value='Go'
			submit()
	}
}
function Answer(Flag)
	{if(Flag) <%Response.Write (N1)%>Form.submit()}
i=0
function L(A,B,C,D,E,F)
{
	T='</td><td>'
	StrForm='window.document.Form'
	F.write('<tr',(i%2==0)?'>':' bgcolor=#E8E8E8>',Path.Strg0,'<td><center><a href="Javascript:',StrForm,'.Decline.value=',A,';Path.Mess(window,\'Are you sure to want to delete this Brand?\',1)">...</a>',T,'<a href=Javascript:with('+StrForm+'){Id.value="',A,'";submit()}>...</a>',T,B,T,'&nbsp;',C,'&nbsp;',T,D,T,E,'</td>',Path.Strg0,'</tr>')
	i=i+1
}
function L2(A,B,C,D,E,F)
{
	E.write('<tr><td CLASS=S7>',A,'<br><input type=text name=',B,' maxlength=',C,' size=',D,' value="',F,'"></td></tr>')
}

</script>
<%Str="SELECT * FROM (SELECT TEMP_BRAN_ID,(SELECT CONT_LABL FROM CONTRACT WHERE CONT_ID=TEMP_BRAN_CONT) AS VARCONT,(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=TEMP_BRAN_COMP) AS VARCOMP,TEMP_BRAN_NAME, TEMP_BRAN_DATE=LEFT(CONVERT(VARCHAR, TEMP_BRAN_DATE, 101), 10) FROM TEMP_BRAND)Q "

IF Request.Form("SearchColumn")<>"" AND Request.Form("ShowAll")="" THEN
	IF Request.Form("SearchColumn")="ALL" THEN
		Str=Str+" WHERE (VARCOMP LIKE '%"+Request.Form("Search")+"%'"
		Str=Str+" OR VARCONT LIKE '%"+Request.Form("Search")+"%'"
		Str=Str+" OR TEMP_BRAN_DATE LIKE '%"+Request.Form("Search")+"%'"
		Str=Str+" OR TEMP_BRAN_NAME LIKE '%"+Request.Form("Search")+"%')"
	ELSE
		Str=Str+" WHERE "+Request.Form("SearchColumn")+" LIKE '%"+Request.Form("Search")+"%'"
	END IF
END IF
Str=Str+" ORDER BY "
IF Request.Form("Column")<>"" THEN
	Str=Str+Request.Form("Column")+" "+Request.Form("Order")
ELSE
	Str=Str+"VARCONT ASC,VARCOMP ASC"
END IF
cmdContent= new SqlCommand(Str, conn)
Rec0= cmdContent.ExecuteReader()

%>
<!-- Main table-->
<table border=0 align=center cellspacing=0 cellpadding=0 bgcolor="#E8E8E8">
	<tr>
		<td bgcolor=#000000 colspan=10 height=1>
		</td>
	</tr>
    <tr>
        <td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
        <td width=3  Class="TableHeader" ><img src=/images/1x1.gif width=1></td>
        <TD align=center  Class="TableHeader" ><strong><font color=white>&nbsp;&nbsp;&nbsp;Decline</a></font></strong></td>

        <TD align=center  Class="TableHeader" ><strong><font color=white>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Action</a> </td>
        <TD align=center  Class="TableHeader" ><strong><a Class="TableHeader" href="JavaScript:document.Form.Column.value='VARCONT';Form.submit()") >&nbsp;&nbsp;&nbsp;Contract</td>
        <TD align=center  Class="TableHeader" ><strong><a Class="TableHeader" href="JavaScript:document.Form.Column.value='VARCOMP';Form.submit()") >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Company Name</td>
        <TD align=center  Class="TableHeader" ><strong><a Class="TableHeader" href="JavaScript:document.Form.Column.value='TEMP_BRAN_NAME';Form.submit()") >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Brand Name</td>
        <TD align=center  Class="TableHeader" ><strong><a Class="TableHeader" href="JavaScript:document.Form.Column.value='TEMP_BRAN_DATE';Form.submit()") >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date</td>

        <td width=3  Class="TableHeader" ><img src=/images/1x1.gif width=1></td>
        <td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
    </tr>

<%
'<tr> has different background color each row'
	 Dim Count, LineTot, LineTotOS, StrColor, Rec
	 Count = 1
	 Dim AltColor
	 AltColor = false
	 Dim iPixel_Down
	 iPixel_Down = 75

     While Rec0.Read()

     if AltColor Then
     AltColor = false
     Else
     AltColor = true
     End If
     if AltColor Then Response.Write("<TR bgcolor=white>") Else Response.Write("<TR>")
     Count = Count +1
     iPixel_Down = iPixel_Down + 20

%>

<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
<td width=3 ><img src=/images/1x1.gif width=1></td>


<td align=center>
<a href="Javascript:document.Form.Decline.value=<%=Rec0(0)%>;Path.Mess(window,'Are you sure to want to delete this Brand?',1)">...</a>
</td>


<td align=center>
<a href=Javascript:with(document.Form){Id.value="<%=Rec0(0)%>";submit()}>...</a>
</td>

<td align=center>
<%=Rec0(1)%>
</td>

<td align=center>
<%=Rec0(2)%>
</td>

<td align=center>
<%=Rec0(3)%>
</td>

<td align=center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%=Rec0(4)%>
</td>


<td width=3 ><img src=/images/1x1.gif width=1></td>
<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
</tr>
<%

End While
Rec0.Close
%>
	<tr>
		<td bgcolor=#000000 colspan=10 height=1>
		</td>
	</tr>
</table>



<br><br><br>
<table>
	<tr>
		<td valign="top">
		  <TPE:Web_Box Heading="Company & Current Brand" Runat="Server" />
			<table border=0 cellpadding=0 cellspacing=0 align="left">

                    <%

                    cmdContent= new SqlCommand("SELECT COMP_ID,COMP_NAME FROM COMPANY WHERE COMP_TYPE='S' OR COMP_TYPE='D' AND EXISTS(SELECT PERS_ID FROM PERSON WHERE PERS_COMP=COMP_ID) ORDER BY COMP_NAME", conn)
                    Rec0= cmdContent.ExecuteReader()
                    Rec0.Read()

                    IF Rec0.Hasrows THEN
                    IF Request.Form("MCompany")="" THEN
	                   ii="0"
                    ELSE
	                   ii=Request.Form("MCompany")
                    END IF
                    Str="'0',"
                    Str0="'ALL',"
                    Rec0.Close()

                    cmdContent= new SqlCommand("SELECT COMP_ID,COMP_NAME FROM COMPANY WHERE COMP_TYPE='S' OR COMP_TYPE='D' AND EXISTS(SELECT PERS_ID FROM PERSON WHERE PERS_COMP=COMP_ID) ORDER BY COMP_NAME", conn)
                    Rec0= cmdContent.ExecuteReader()
                    While Rec0.Read()
	                   Str=Str+"'"&Rec0(0)&"',"
	                   Str0=Str0+"'"&Rec0(1)&"',"
                    End While
                    IF Str="" THEN Str=","
                    IF Str0="" THEN Str0=","
                    Rec0.Close

                    END IF%>
				<tr>
					<td Class=S7><br>Company:<br>
                        <script>Path.SOS(new Array(<%=Left(Str,Len(Str)-1)%>),new Array(<%=Left(Str0,Len(Str0)-1)%>),'<%=ii%>',document,'MCompany','Path.Wait(window);document.Form.Decline.value="";document.Form.Submit.value="";document.Form.Delete.value="";document.Form.Id.value="";')</script><br><br>
					</td>
				</tr>
				<tr>
					<td CLASS=S7>Contract:<br><%
                    IF Request.Form("Contract2")="" THEN
	                   i="0"
                    ELSE
	                   i=Request.Form("Contract2")
                    END IF
                    cmdContent= new SqlCommand("SELECT CONT_ID,CONT_LABL FROM CONTRACT WHERE CONT_ID<29 ORDER BY CONT_ORDR", conn)
                    Rec0= cmdContent.ExecuteReader()

                    Str="'0',"
                    Str0="'ALL',"

                    While Rec0.Read()
	                   Str=Str+"'"&Rec0(0)&"',"
	                   Str0=Str0+"'"&Rec0(1)&"',"
                    End While
                    NStr=Str
                    NStr0=Str0
                    Rec0.Close
                    %>
                    <script>Path.SOS(new Array(<%=Left(Str,Len(Str)-1)%>),new Array(<%=Left(Str0,Len(Str0)-1)%>),'<%=i%>',document,'Contract2','Path.Wait(window);document.Form.Decline.value="";document.Form.Submit.value="";document.Form.Delete.value="";document.Form.Id.value="";')</script><br><br>
                    <%
                    Strin="select bran_id, bran_name From brand"
                    IF i>0 and ii=0 THEN
                    Strin=Strin+" where bran_cont="&i
                    ELSEIF i>0 and ii>0 THEN
                    Strin=Strin+" where bran_cont="+i+" and  bran_comp="&ii
                    ELSEIF i=0 and ii>0 THEN
                    Strin=Strin+" where bran_comp="&ii
                    END IF
                    Strin=Strin+" ORDER BY BRAN_NAME"
                    cmdContent= new SqlCommand(Strin, conn)
                    Rec0= cmdContent.ExecuteReader()
                    Rec0.Read()
                    IF Rec0.Hasrows THEN
                    Str=""
                    Str0=""
                    Rec0.Close()
                    cmdContent= new SqlCommand(Strin, conn)
                    Rec0= cmdContent.ExecuteReader()
                    While Rec0.Read()
	                   Str=Str+"'"&Rec0(0)&"',"
	                   Str0=Str0+"'"&Rec0(1)&"',"

                    End While
                    IF Str="" THEN Str=","
                    IF Str0="" THEN Str0=","
                    Rec0.Close
                    %>

                    Brands:<br>
                    <select name=BrandDrop>
                    <script>Path.SO(new Array(<%=Left(Str,Len(Str)-1)&"),new Array("&Left(Str0,Len(Str0)-1)%>),'',document)</script><br><br>
                    <center>
                    <input type=button class=tpebutton value='Specifications' onClick="Javascript:Path.Show(window,'../Administrator/MB_Specs_Brand.aspx',Form.BrandDrop[Form.BrandDrop.selectedIndex].value)"></input></center>
                    <br>
                    <center><input type=button class=tpebutton value='DECLINE' onClick="Javascript:with(document){Form.Delete.value=Form.BrandDrop[Form.BrandDrop.selectedIndex].value;Path.Mess(window,'Are you sure to want to delete this brand?',1)}"></input></center>
                    <%ELSE%> <b>No Brand Selected</b>
                    <%END IF%>
					</td>
				</tr>
				<tr>
					<td colspan=3 height=10>
					</td>
				</tr>
			</table>
			<TPE:Web_Box Footer="true" Runat="Server" />
		</td>
		<td width=30>
		</td>
		<td valign=top>





        <TPE:Web_Box Heading="Create a New Brand" Runat="Server" />
			<table border=0 cellpadding=0 cellspacing=0 align="CENTER">
				<tr>
					<td width=10 rowspan=8>
					</td>
					<td width=280 height=6>
					</td>
					<td width=10 rowspan=8>
					</td>
				</tr>
                    <%
                    Rec0.Close
                    cmdContent= new SqlCommand("SELECT COMP_ID,COMP_NAME FROM COMPANY WHERE COMP_TYPE='S' OR COMP_TYPE='D' AND EXISTS(SELECT PERS_ID FROM PERSON WHERE PERS_COMP=COMP_ID) ORDER BY COMP_NAME", conn)
                    Rec0= cmdContent.ExecuteReader()
                    Rec0.Read()
                    IF Rec0.Hasrows THEN
                    IF Request.Form("NCompany")<>"" THEN
	                   pi=Request.Form("NCompany")
                    ELSE
	                   pi=Rec0(0)
                    END IF
                    Str=""
                    Str0=""
                    Rec0.Close()
                    cmdContent= new SqlCommand("SELECT COMP_ID,COMP_NAME FROM COMPANY WHERE COMP_TYPE='S' OR COMP_TYPE='D' AND EXISTS(SELECT PERS_ID FROM PERSON WHERE PERS_COMP=COMP_ID) ORDER BY COMP_NAME", conn)
                    Rec0= cmdContent.ExecuteReader()
                    While Rec0.Read()
	                   Str=Str+"'"&Rec0(0)&"',"
	                   Str0=Str0+"'"&Rec0(1)&"',"

                    End While
                    IF Str="" THEN Str=","
                    IF Str0="" THEN Str0=","
                    END IF
                    Rec0.Close
                    %>
				<tr>
					<td CLASS=S7><br>Company:<br>
                        <script>Path.SO(new Array(<%=Left(Str,Len(Str)-1)%>),new Array(<%=Left(Str0,Len(Str0)-1)%>),'<%=pi%>',document,'NCompany')</script><br><img height=4 src=../images/1x1.gif>
					</td>
				</tr>
				<tr>
					<td CLASS=S7>Contract:<br>
                        <%
                        cmdContent= new SqlCommand("Exec spGContract @CompId="&Session("Comp"), conn)
                        Rec0= cmdContent.ExecuteReader()
                        Rec0.Read()

                        IF Request.Form("NContract")="" THEN
	                       i=Rec0(0)
                        ELSE
	                       i=Request.Form("NContract")
                        END IF
                        Str=""
                        Str0=""
                        Rec0.Close()
                        cmdContent= new SqlCommand("Exec spGContract @CompId="&Session("Comp"), conn)
                        Rec0= cmdContent.ExecuteReader()
                        While Rec0.Read()
	                       IF CInt(Rec0(0))=CInt(i) THEN j=Rec0(2):k=Rec0(3):l=Rec0(4)
	                       Str=Str+"'"&Rec0(0)&"',"
	                       Str0=Str0+"'"&Rec0(1)&"',"

                        End While
                        Rec0.Close
                        %>
                        <script>
                        TabA=new Array(<%=Left(Str,Len(Str)-1)%>)
                        TabB=new Array(<%=Left(Str0,Len(Str0)-1)%>)
                        Path.SOS(TabA,TabB,'<%=i%>',document,'NContract','Path.Wait(window);document.Form.Decline.value="";document.Form.Submit.value="";document.Form.Delete.value="";document.Form.Id.value="";');
                        document.write('<br><img height=4 src=../images/1x1.gif></td></tr>');
                        L2('Brand Name','NBrand','50','21',document,'');
                        L2('Melt (<%=j%>)','NMelt','10','21',document,'');
                        L2('Density (<%=k%>)','NDensity','10','21',document,'');
                        L2('IZOD (<%=l%>)','NIZOD','10','21',document,'');
                        </script><br><br><br>
                    </td>
               </tr>
                <tr>
                    <td>
                        <center>
                            <input type=button class=tpebutton value='ADD' onClick="Javascript:document.Form.NewB.value='G0';document.Form.Decline.value='';document.Form.Delete.value='';document.Form.Submit.value='';submit()">
                            </center><br><br>
					</td>
				</tr>
			</table>
		<TPE:Web_Box Footer="true" Runat="Server" />

		</td>
	</tr>
</table></form>

<%END IF%>

<TPE:Template Footer="true" Runat="Server" />

</form>

<script>Path.Start(window)</script>
