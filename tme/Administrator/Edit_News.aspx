<%@ Page language="c#" Codebehind="Edit_News.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.Edit_News" MasterPageFile="~/MasterPages/Menu.Master" Title="Edit News" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">

	
			<table height="30" cellspacing="0" cellpadding="0" width="780" border="0">
				<tr>
					<td align="center" width="641"><span class="Content Bold Color2"><asp:label id="Message" runat="server"></asp:label></span>&nbsp;&nbsp;</td>
				</tr>
			</table>
			<table height="300" cellspacing="0" cellpadding="0" width="780" border="0">
				<tr>
					<td valign="top" align="left" width="49" rowSpan="3">&nbsp;</td>
					<td valign="middle" align="left" width="79" height="20"><span class="Content Bold Color2">Headline</span></td>
					<td valign="top" align="left" width="513" height="20"><asp:textbox CssClass="InputForm" id="txtHeadline" runat="server" Width="450px"></asp:textbox></td>
				</tr>
				<tr>
					<td valign="top" align="left"><span class="Content Bold Color2">Date</span></td>
					<td valign="top" align="left" height="20"><asp:textbox CssClass="InputForm" id="txtDate" runat="server" Width="120px" ReadOnly="True"></asp:textbox></td>
				</tr>
				<tr>
					<td valign="top" align="left"><span class="Content Bold Color2">Story</span></td>
					<td valign="top" align="left" height="260"><asp:textbox CssClass="InputForm" id="txtStory" runat="server" Width="450px" Height="300px" TextMode="MultiLine"></asp:textbox></td>
				</tr>
			</table>
			<table cellspacing="0" cellpadding="0" width="780" border="0">
				<tr>
					<td align="center"><br /><asp:button Height="20" id="UpdateNews" runat="server" Width="80px" Text="Update" onclick="UpdateCustomNews_Click"></asp:button>
						<asp:Button Height="20" id="CancelNews" runat="server" Width="80px" Text="Cancel" onclick="CancelNews_Click"></asp:Button><br /><br /></td>
				</tr>
			</table>

</asp:Content>