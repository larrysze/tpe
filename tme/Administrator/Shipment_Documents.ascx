<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Shipment_Documents.ascx.cs" Inherits="localhost.Administrator.Shipment_Documents" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<FIELDSET style="WIDTH: 468px; HEIGHT: 176px"><LEGEND><STRONG>Create Shipment Documents</STRONG>
	</LEGEND>
	<TABLE id="Table1" style="WIDTH: 472px; HEIGHT: 179px">
		<TR>
			<TD style="WIDTH: 140px; HEIGHT: 19px"><STRONG><asp:label id="lblType" runat="server">Type</asp:label></STRONG></TD>
			<TD style="WIDTH: 64px; HEIGHT: 19px"><STRONG><asp:label id="lblSendTo" runat="server">Send To:</asp:label></STRONG></TD>
			<TD style="HEIGHT: 19px"><asp:dropdownlist id="ddlSendTo" runat="server" AutoPostBack="True" Width="245px"></asp:dropdownlist></TD>
		</TR>
		<TR>
			<TD style="WIDTH: 140px; HEIGHT: 14px"><asp:dropdownlist id="ddlType" runat="server" AutoPostBack="True" Width="136px">
					<asp:ListItem Value="1">Invoice</asp:ListItem>
					<asp:ListItem Value="2">Purchase Order</asp:ListItem>
					<asp:ListItem Value="3">Certificate</asp:ListItem>
					<asp:ListItem Value="4">Sales Confirmation</asp:ListItem>
					<asp:ListItem Value="5">Customer Release</asp:ListItem>
					<asp:ListItem Value="6">HC Title Transfer</asp:ListItem>
					<asp:ListItem Value="8">Warehouse Release</asp:ListItem>
				</asp:dropdownlist></TD>
			<TD style="WIDTH: 64px; HEIGHT: 14px"><STRONG><asp:label id="lblCC" runat="server">Cc:</asp:label></STRONG></TD>
			<TD style="HEIGHT: 14px"><asp:dropdownlist id="ddlCC" runat="server" AutoPostBack="True" Width="245px"></asp:dropdownlist></TD>
		</TR>
		<TR>
			<TD style="WIDTH: 140px"><STRONG></STRONG></TD>
			<TD style="WIDTH: 64px"><STRONG></STRONG></TD>
			<TD></TD>
		</TR>
		<TR>
			<TD style="WIDTH: 176px; HEIGHT: 3px" colSpan="3"><STRONG><asp:label id="lblCustom" runat="server"></asp:label></STRONG></TD>
		</TR>
		<TR>
			<TD style="WIDTH: 176px" colSpan="3"><STRONG><asp:textbox id="txtCustom" runat="server" Width="336px" size="4"></asp:textbox></STRONG></TD>
		</TR>
		<TR>
			<TD style="WIDTH: 140px"><asp:label id="lblOrderID" runat="server" Visible="False"></asp:label></TD>
			<TD style="WIDTH: 64px"></TD>
			<TD></TD>
		</TR>
		<TR>
			<TD style="WIDTH: 140px"></TD>
			<TD colSpan="2">
				<asp:button id="btnPreview" runat="server" Width="105px" Text="Preview Email"></asp:button>&nbsp;
				<asp:button id="btnEmail" runat="server" Width="105px" Text="Send Email"></asp:button></TD>
		</TR>
	</TABLE>
</FIELDSET>
