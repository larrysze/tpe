<%@ import Namespace="localhost" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ Page Language="C#" %>
<script runat="server">

    /*****************************************************************************
    '*   1. File Name       : Spot\Admin_Order_Entry.aspx                        *
    '*   2. Description     : spot offers/ bids	entry screen for adminstrator    *
    '*						                                 *
    '*   3. Modification Log:                                                    *
    '*     Ver No.       Date          Author             Modification           *
    '*   -----------------------------------------------------------------       *
    '*      1.00      11-26-2003       Zach                  First Baseline      *
    '*                                                                           *
    '*****************************************************************************/
    string location;
    SqlConnection conn;
    public void Page_Load(object sender, EventArgs e){
		Response.Redirect("resin_entry.aspx");
        if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B")){
            Response.Redirect("../default.aspx");
        }
        // opening the connections
        conn = new SqlConnection(Application["DBconn"].ToString());
        conn.Open();

		//Disabling enter key
		//HelperFunction.RegisterBlockDisableEnterKey(this);
		HelperFunction.RegisterBlockNoEnterKeyPress(this);
		
        if (!IsPostBack){

            if (Request.QueryString["Export"] !=null){
                ViewState["Export"] =true;
            }else{
                ViewState["Export"] =false;
            }

            SqlCommand cmdCompanies;
            SqlDataReader dtrCompanies;

            // opening Companies drop-down list
            //Top 10 looking at BBOFFERARCH
            //cmdCompanies = new SqlCommand("SELECT * FROM (SELECT COMP_ID,COMP_NAME,COMP_TYPE FROM COMPANY WHERE COMP_REG=1 AND COMP_TYPE IN ('P','S','D')AND EXISTS(SELECT PERS_ID FROM PERSON WHERE PERS_COMP=COMP_ID) )Q ORDER BY COMP_NAME",conn);
            cmdCompanies = new SqlCommand("SELECT     TOP 10 C.COMP_ID, C.COMP_NAME, C.COMP_TYPE, QTT.SUM_QTD " +
											"FROM         COMPANY C INNER JOIN " +
																	"(SELECT     ID, SUM(QTTF) AS SUM_QTD " +
																		"FROM          (SELECT     COUNT(OFFR_ID) AS QTTF, OFFR_COMP_ID AS ID " +
																								"FROM          BBOFFERARCH " +
																								"WHERE      (OFFR_DATE BETWEEN DATEADD(day, - 90, GETDATE()) AND GETDATE()) AND OFFR_COMP_ID IS NOT NULL AND  " +
																													"OFFR_COMP_ID <> 0 " +
																								"GROUP BY OFFR_COMP_ID " +
																								"UNION " +
																								"SELECT     COUNT(BID_ID) AS QTTF, BID_COMP_ID AS ID " +
																								"FROM         BBIDARCH " +
																								"WHERE     (BID_DATE BETWEEN DATEADD(day, - 90, GETDATE()) AND GETDATE()) AND BID_COMP_ID IS NOT NULL AND  " +
																													"BID_COMP_ID <> 0 " +
																								"GROUP BY BID_COMP_ID) DERIVEDTBL " +
																		"GROUP BY ID) QTT ON C.COMP_ID = QTT.ID " +
											"WHERE     (C.COMP_REG = 1) AND (C.COMP_TYPE IN ('P', 'S', 'D')) AND EXISTS " +
																	"(SELECT PERS_ID FROM PERSON WHERE PERS_COMP = COMP_ID) " +
											"ORDER BY QTT.SUM_QTD DESC, C.COMP_NAME",conn);
            dtrCompanies = cmdCompanies.ExecuteReader();
            //binding company dd list
            ddlCompanies.Items.Add ( new ListItem ("Select Company","default" ) );
            while (dtrCompanies.Read()){
                ddlCompanies.Items.Add ( new ListItem ((string) dtrCompanies["COMP_NAME"],dtrCompanies["COMP_ID"].ToString() ) );
            }
            dtrCompanies.Close();
            
            //Rest of them, excluding top 10
            cmdCompanies = new SqlCommand("SELECT     COMP_ID, COMP_NAME, COMP_TYPE " +
											"FROM         COMPANY C " +
											"WHERE     (COMP_REG = 1) AND (COMP_TYPE IN ('P', 'S', 'D')) AND EXISTS " +
																	"(SELECT     PERS_ID " +
																		"FROM          PERSON " +
																		"WHERE      PERS_COMP = COMP_ID) AND (COMP_ID NOT IN " +
																	"(SELECT     TOP 10 C.COMP_ID " +
																		"FROM          COMPANY C INNER JOIN " +
																								"(SELECT     ID, SUM(QTTF) AS SUM_QTD " +
																									"FROM          (SELECT     COUNT(OFFR_ID) AS QTTF, OFFR_COMP_ID AS ID " +
																															"FROM          BBOFFERARCH " +
																															"WHERE      (OFFR_DATE BETWEEN DATEADD(day, - 90, GETDATE()) AND GETDATE()) AND OFFR_COMP_ID IS NOT NULL " +
																																					"AND OFFR_COMP_ID <> 0 " +
																															"GROUP BY OFFR_COMP_ID " +
																															"UNION " +
																															"SELECT     COUNT(BID_ID) AS QTTF, BID_COMP_ID AS ID " +
																															"FROM         BBIDARCH " +
																															"WHERE     (BID_DATE BETWEEN DATEADD(day, - 90, GETDATE()) AND GETDATE()) AND BID_COMP_ID IS NOT NULL AND " +
																																				"BID_COMP_ID <> 0 " +
																															"GROUP BY BID_COMP_ID) AS TABLE1 " +
																									"GROUP BY ID) QTT ON C.COMP_ID = QTT.ID " +
																		"WHERE      (C.COMP_REG = 1) AND (C.COMP_TYPE IN ('P', 'S', 'D')) AND EXISTS " +
																								"(SELECT PERS_ID FROM PERSON WHERE PERS_COMP = COMP_ID) " +
																		"ORDER BY QTT.SUM_QTD DESC, C.COMP_NAME)) " +
											"ORDER BY COMP_NAME",conn);
            dtrCompanies = cmdCompanies.ExecuteReader();
            ddlCompanies.Items.Add ( new ListItem (" --------------------","default" ) );
            while (dtrCompanies.Read()){
                ddlCompanies.Items.Add ( new ListItem ((string) dtrCompanies["COMP_NAME"],dtrCompanies["COMP_ID"].ToString() ) );
            }
            dtrCompanies.Close();
            /*
            // opening contract list
            SqlCommand cmdContract;
            SqlDataReader dtrContract;

            cmdContract = new SqlCommand("SELECT CONT_ID,CONT_LABL FROM Contract ORDER BY CONT_ORDR",conn);
            dtrContract = cmdContract.ExecuteReader();

           //binding company dd list
            ddlProduct.DataSource = dtrContract;
            ddlProduct.DataTextField= "CONT_LABL";
                   //ddlProduct.DataValueField= "CONT_ID";
            ddlProduct.DataBind();
            dtrContract.Close();

            */
            // opening product drop-down list manually

            ddlProduct.Items.Add("HMWPE - Film Grade");
            ddlProduct.Items.Add("HDPE - Inj");
            ddlProduct.Items.Add("HDPE - Blow Mold");
            ddlProduct.Items.Add("LDPE - Film");
            ddlProduct.Items.Add("LDPE - Inj");
            ddlProduct.Items.Add("LLDPE - Film");
            ddlProduct.Items.Add("LLDPE - Inj");
            ddlProduct.Items.Add("PP Homopolymer - Inj");
            ddlProduct.Items.Add("PP Copolymer - Inj");
            ddlProduct.Items.Add("GPPS");
            ddlProduct.Items.Add("HIPS");
            //ddlProduct.Items.Add("PET");
            //ddlProduct.Items.Add("PVC");


            // populate other info
            if ((bool)ViewState["Export"]){

                //Container is available for Export
                ddlQty.Items.Add(new ListItem ("20' Container","38500"));
                ddlQty.Items.Add(new ListItem ("40' Container","44500"));

                //Extra terms for Export
                ddlTerms.Items.Add(new ListItem ("CFR","CFR"));
                ddlTerms.Items.Add(new ListItem ("CIF","CIF"));
                ddlTerms.Items.Add(new ListItem ("FAS","FAS"));

                //ddlPayment.Items.Add(new ListItem ("LC Sight","7"));
                //ddlPayment.Items.Add(new ListItem ("CIA","0"));
                //ddlPayment.Items.Add(new ListItem ("LC 30","30"));
                //ddlPayment.Items.Add(new ListItem ("LC 60","60"));
                //ddlPayment.Items.Add(new ListItem ("LC 90","90"));


            }else{
                //Quantity drop-down list
                ddlQty.Items.Add(new ListItem ("Rail Car","190000"));
                ddlQty.Items.Add(new ListItem ("Metric Ton","2205"));
                ddlQty.Items.Add(new ListItem ("Truckload Boxes","42000"));
                ddlQty.Items.Add(new ListItem ("Truckload Bags","44092"));
                ddlQty.Items.Add(new ListItem ("Bulk Truck","45000"));
                ddlQty.Items.Add(new ListItem ("Pounds (lbs)","1"));

                //payment terms list
                ddlTerms.Items.Add(new ListItem ("FOB Delivered","FOB/Delivered"));
                ddlTerms.Items.Add(new ListItem ("FOB Shipping","FOB/Shipping"));

                //ddlPayment.Items.Add(new ListItem ("30","30"));
                //ddlPayment.Items.Add(new ListItem ("Cash","0"));
                //ddlPayment.Items.Add(new ListItem ("45","45"));
                //ddlPayment.Items.Add(new ListItem ("60","60"));
                //ddlPayment.Items.Add(new ListItem ("90","90"));
            }
            //Expire drop-down list
            ddlexpires.Items.Add(new ListItem ("One week","7"));
            ddlexpires.Items.Add(new ListItem ("End of Day","0"));
            ddlexpires.Items.Add(new ListItem ("Two weeks","14"));
            ddlexpires.Items.Add(new ListItem ("One month","30"));
            //ddlexpires.Items.Add(new ListItem ("Two months","60"));
            //ddlexpires.Items.Add(new ListItem ("Three months","90"));

            ddlOrderType.Items.Add(new ListItem ("Buy","P"));
            ddlOrderType.Items.Add(new ListItem ("Sell","S"));

           ddlAction.Items.Add(new ListItem ("Sell","S"));
           ddlAction.Items.Add(new ListItem ("Buy","P"));
           ddlAction.SelectedItem.Value="S";

            ddlAction.Visible=true;
            // binds the additional fields
            Bind_Info();

        }


    }
    // <summary>
    //  This function is a wrapper for calling the Bind Function
    //  All the controls that auto post back call this function
    // </summary>




    public void ddlActionChange(Object sender, EventArgs e)
       {
           Bind_Info();
       }
    public void PostBack(object sender, EventArgs e){

        Bind_Info();
    }
    // <summary>
    //  Function populates the melt density and adds fields depending on the currently selected contract
    // </summary>
    /* not implemented at the moment
    public void Change_Contract(object sender, EventArgs e){
        SqlCommand cmdContract;
        SqlDataReader dtrContract;
        cmdContract = new SqlCommand("select * from CONTRACT WHERE CONT_ID='"+ddlProduct.SelectedItem.Value+"'",conn);
        dtrContract = cmdContract.ExecuteReader();

        dtrContract.Read();
        txtMelt.Text = dtrContract["Cont_Melt_TGT"].ToString();
        txtDensity.Text = dtrContract["Cont_Dens_TGT"].ToString();
        txtAdds.Text = dtrContract["Cont_IZOD_TGT"].ToString();
        dtrContract.Close();
    }
    */
    // <summary>
    // Primary function for binding content to the controls
    // </summary>


    public void Bind_Info(){

               String action;
               if (!IsPostBack)
                   action="";
               else
                   action=ddlAction.SelectedItem.Value.ToString();

                if (IsPostBack)
               {
                   ddlAction.Visible=true;
                   //ddlAction.SelectedItem.Value;
                   ddlAction.Items.Clear();

               }


        // clear the current list boxes to prevent multiple adds
              ddlUser.Items.Clear();
        ddlDelivery_Point.Items.Clear();
        // determine the type of user was selected
        String[] arrComp_Names= ddlCompanies.SelectedItem.ToString().Split('/');
        // change the lables depending on the type of user
        switch(arrComp_Names[0]){
            default:
                if (action.Equals("P"))
                {
                    WebBox1.Heading = "Buy Order Entry";
                    lblPrice.Text = "TPE Buy";
                    lblExpires.Text = "Bid Expiration";
                    lblPoint.Text="Delivery Point";
                     ddlAction.Items.Add(new ListItem ("Buy","P"));
                     ddlAction.Items.Add(new ListItem ("Sell","S"));

                }else{
                    WebBox1.Heading = "Sell Order Entry";
                    lblPrice.Text = "TPE Buy Price";
                    lblExpires.Text = "Offer Expiration";
                    lblPoint.Text="Shipping Point";
                    ddlAction.Items.Add(new ListItem ("Sell","S"));
                    ddlAction.Items.Add(new ListItem ("Buy","P"));

                }
                break;
            case "Distributor":

                 if (action.Equals("S"))
                 {
                  ddlAction.Items.Add(new ListItem ("Sell","S"));
                  ddlAction.Items.Add(new ListItem ("Buy","P"));


                  WebBox1.Heading = "Sell Order Entry";
                  lblPrice.Text = "TPE Buy Price";
                  lblExpires.Text = "Offer Expiration";
                  lblPoint.Text="Shipping Point";
                 }
                 else
                       {
                            ddlAction.Items.Add(new ListItem ("Buy","P"));
                            ddlAction.Items.Add(new ListItem ("Sell","S"));

                           WebBox1.Heading = "Buy Order Entry";
                           lblPrice.Text = "TPE Buy";
                           lblExpires.Text = "Bid Expiration";
                           lblPoint.Text="Delivery Point";

                       }


                break;
            case "Purchaser":
                WebBox1.Heading = "Buy Order Entry";
                lblPrice.Text = "TPE Buy";
                lblExpires.Text = "Bid Expiration";
                lblPoint.Text="Delivery Point";
                ddlAction.Items.Add(new ListItem ("Buy","P"));
                break;
            case "Supplier":
                WebBox1.Heading = "Sell Order Entry";
                lblPrice.Text = "TPE Buy Price";
                lblExpires.Text = "Offer Expiration";
                lblPoint.Text="Shipping Point";
                   ddlAction.Items.Add(new ListItem ("Sell","S"));
                break;
        }


        // if a company has been selected add all the appropriate info
        if (!ddlCompanies.SelectedItem.Value.Equals("default")){

            //  change visible fields since we are working within a company
            pnDelivery_Point.Visible = true;
            txtCompany.Text = "";
            pnAltCompany.Visible = false;
            ddlUser.Visible = true;

            // adding all the possible users within the company
             SqlCommand cmdUsers;
            SqlDataReader dtrUsers;

            cmdUsers = new SqlCommand("Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME as PERS_NAME,PERS_ID FROM PERSON WHERE PERS_COMP="+ddlCompanies.SelectedItem.Value,conn);
            dtrUsers = cmdUsers.ExecuteReader();
            // adding to control
            while(dtrUsers.Read()){
                ddlUser.Items.Add ( new ListItem ((string) dtrUsers["PERS_NAME"],dtrUsers["PERS_ID"].ToString() ) );

            }
            dtrUsers.Close();

            // adding all the possible locations within the company
            SqlCommand cmdLocations;
            SqlDataReader dtrLocations;

            cmdLocations = new SqlCommand("select PLAC_ID ,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) AS Location from place where PLAC_TYPE<>'H' AND plac_comp='"+ddlCompanies.SelectedItem.Value+"'",conn);
            dtrLocations = cmdLocations.ExecuteReader();
            while(dtrLocations.Read()){
                ddlDelivery_Point.Items.Add ( new ListItem ((string) dtrLocations["LOCATION"],dtrLocations["PLAC_ID"].ToString() ) );
            }
            dtrLocations.Close();
            cmdLocations = new SqlCommand("select  PLAC_ID, LABEL = (select LOCL_CITY +', '+ LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) + ' ('+PLAC_LABL+')'  from place where plac_type='W'  ORDER BY LABEL",conn);
           dtrLocations = cmdLocations.ExecuteReader();
           while(dtrLocations.Read()){
				ddlDelivery_Point.Items.Add ( new ListItem (dtrLocations["LABEL"].ToString(),dtrLocations["PLAC_ID"].ToString() ) );
           }
			dtrLocations.Close();




        } else {
            // no company has been selected.  Therefore, only show the needed user
            pnAltCompany.Visible = true;
            txtDelivery_Point.Text = "";
            pnDelivery_Point.Visible = false;
            ddlUser.Visible = false;

        }

    }
    // <summary>
    //  Function enters the order into the system
    // </summary>
    public void Click_Cancel(object sender, EventArgs e){
        pnMain.Visible= true;
        pnConfirm.Visible = false;
    }
    // <summary>
    //  Creates and displays the confirmation screen
    // </summary>
    public void Click_Submit(object sender, EventArgs e){
        pnMain.Visible= false;
        string strText;

        // builds the confirmation string
        strText = " Your are going to place an order to ";


         // selects the user type
        String[] arrCompanyName= ddlCompanies.SelectedItem.ToString().Split('/');
        // automatically determines the type of user
        switch(arrCompanyName[0]){
            default:
                if (ddlAction.SelectedItem.Value.Equals("P")){
                    strText += "Buy ";
                }else{
                    strText += "Sell ";
                }
                break;
            case "Distributor":

                    if (ddlAction.SelectedItem.Value.Equals("P")){
                    strText += "Buy ";
                }else{
                    strText += "Sell ";
                }

                break;
            case "Purchaser":
                strText += "Buy ";
                break;
            case "Supplier":
                strText += "Sell ";
                break;
        }
        strText += txtQty.Text + " " + ddlQty.SelectedItem.ToString() + " of ";

        if (radio_Other.Checked){
                strText += txtProduct.Text;
        }else{
                strText +=ddlProduct.SelectedItem.ToString();
        }
        strText +=" for $ 0." + txtPrice.Text +" per pound ";
        strText +=" and markup of $ 0." + txtMarkup.Text +" per pound. ";
        
        if (!txtMelt.Text.Equals("")){
            strText +="Melt: " + txtMelt.Text +". ";
        }
        if (!txtDensity.Text.Equals("")){
             strText +="Density: 0." + txtDensity.Text +". ";
        }
        if (!txtAdds.Text.Equals("")){
             strText +="Adds: " +txtAdds.Text +". ";
        }
        strText += "Shipped from ";

        if (txtDelivery_Point.Text.Equals("")){
            strText += ddlDelivery_Point.SelectedItem + ". ";
        }else{
            strText += txtDelivery_Point.Text +". ";
        }
        strText +=" Expiration: In "+ddlexpires.SelectedItem.ToString()+". Terms: "+ddlTerms.SelectedItem.ToString()+".";
        // Payment: "+ddlPayment.SelectedItem+" days.
        lblConfirm.Text = strText;
        pnConfirm.Visible = true;
    }

    // <summary>
    //  Function enters the order into the system
    // </summary>
    public void Save(object sender, EventArgs e){
            SqlCommand cmdAddNew;  // stored proc adding record
            lblMessage.Text = ""; // reset the label to nothing
            cmdAddNew= new SqlCommand("spAddtoBB", conn);
            cmdAddNew.CommandType = CommandType.StoredProcedure;

            if (ddlCompanies.SelectedItem.Value.Equals("default")){
                // use the alternate company
                cmdAddNew.Parameters.Add("@Comp",txtCompany.Text.ToString());
                cmdAddNew.Parameters.Add("@Comp_Id",null);
                cmdAddNew.Parameters.Add("@Pers_id",null);
                lblMessage.Text += "<font color=red>"+txtCompany.Text.ToString() + "</font> offers ";
            }else{
                // use the company drop down
                cmdAddNew.Parameters.Add("@Comp",null);
                cmdAddNew.Parameters.Add("@Comp_Id",ddlCompanies.SelectedItem.Value);
                cmdAddNew.Parameters.Add("@Pers_id",ddlUser.SelectedItem.Value);
                lblMessage.Text +="<font color='red'>"+ddlCompanies.SelectedItem.ToString() +"</font> offers ";
            }
            // # of items
            cmdAddNew.Parameters.Add("@Qty",txtQty.Text);
            lblMessage.Text += " "+"<font color='red'>"+txtQty.Text+"</font> ";
            // of size
            cmdAddNew.Parameters.Add("@Size",Int32.Parse(ddlQty.SelectedItem.Value));
            lblMessage.Text += "<font color='red'>" + ddlQty.SelectedItem.ToString() +"</font> of ";

              // other product overwrite the dropdown list
            if (radio_Other.Checked){
                cmdAddNew.Parameters.Add("@Prod",txtProduct.Text);
                lblMessage.Text += "<font color='red'>"+txtProduct.Text+"</font>";
            }else{
                cmdAddNew.Parameters.Add("@Prod",ddlProduct.SelectedItem.ToString());
                lblMessage.Text += "<font color='red'>"+ddlProduct.SelectedItem.ToString()+"</font>";
            }

            cmdAddNew.Parameters.Add("@Price","0." + txtPrice.Text);
            lblMessage.Text += " at <font color='red'>$0." + txtPrice.Text +"</font>" ;
			
			decimal markut = System.Convert.ToDecimal("0." + txtMarkup.Text) * 100;
			cmdAddNew.Parameters.Add("@Markup",markut);
			lblMessage.Text += " and markup of <font color='red'>$0." + txtMarkup.Text +"</font>. </BR> " ;

            if (!txtMelt.Text.Equals("")){
                cmdAddNew.Parameters.Add("@Melt",txtMelt.Text);
                lblMessage.Text += " Melt: " +"<font color='red'>"+txtMelt.Text+"</font>";
            }
            if (!txtDensity.Text.Equals("")){
                cmdAddNew.Parameters.Add("@Dens","0."+txtDensity.Text);
                lblMessage.Text += " Density: " +"<font color='red'>0."+txtDensity.Text+"</font>";
            }
            if (!txtAdds.Text.Equals("")){
                cmdAddNew.Parameters.Add("@Adds",txtAdds.Text);
                lblMessage.Text += " Adds: " +"<font color='red'>"+txtAdds.Text+"</font>";
            }

            // selects the user type
            String[] arrCompanyName= ddlCompanies.SelectedItem.ToString().Split('/');
            // automatically determines the type of user
            switch(arrCompanyName[0]){
                default:
                    if (ddlAction.SelectedItem.Value.Equals("P")){
                        cmdAddNew.Parameters.Add("@Type","P");
                    }else{
                        cmdAddNew.Parameters.Add("@Type","S");
                    }
                    break;
                case "Distributor":

                        if (ddlAction.SelectedItem.Value.Equals("P")){
                           cmdAddNew.Parameters.Add("@Type","P");
                }else{
                   cmdAddNew.Parameters.Add("@Type","S");
                }

                    break;
                case "Purchaser":
                    cmdAddNew.Parameters.Add("@Type","P");
                    break;
                case "Supplier":
                    cmdAddNew.Parameters.Add("@Type","S");
                    break;
            }
            if (txtDelivery_Point.Text.Equals("")){
                // use the drop down location
                SqlCommand cmdLocality;
                // select the locality
                cmdLocality = new SqlCommand("Select PLAC_LOCL FROM Place WHERE PLAC_ID='"+ddlDelivery_Point.SelectedItem.Value+"'",conn);
                // add locality id
                cmdAddNew.Parameters.Add("@Locl0",cmdLocality.ExecuteScalar().ToString());
                // add place id
                cmdAddNew.Parameters.Add("@Locl1",ddlDelivery_Point.SelectedItem.Value);
                cmdAddNew.Parameters.Add("@Local",null);
                lblMessage.Text += " Offer out of <font color='red'> " +ddlDelivery_Point.SelectedItem.ToString()+"</font> ";

            }else{
               // use the text box to manually enter the location
               cmdAddNew.Parameters.Add("@Locl0",null);
               cmdAddNew.Parameters.Add("@Local",txtDelivery_Point.Text.ToString());
               lblMessage.Text += " Offer out of <font color='red'>" +txtDelivery_Point.Text.ToString()+"</font> ";
            }



            cmdAddNew.Parameters.Add("@Detail",txtDetail.Text);

            cmdAddNew.Parameters.Add("@Terms",ddlTerms.SelectedItem.Value);
            lblMessage.Text += "<font color='red'>"+ddlTerms.SelectedItem.ToString()+"</font>";
            if (txtDelivery_Point.Text.Equals("") && txtCompany.Text.Equals("")){
                cmdAddNew.Parameters.Add("@Firm","1"); // firm
            }else{
                cmdAddNew.Parameters.Add("@Firm","0"); // not firm
            }
            cmdAddNew.Parameters.Add("@Payment","0");
            cmdAddNew.Parameters.Add("@Expr",ddlexpires.SelectedItem.Value);

            //Store the PERS_ID only if the user is a Broker
            if ((string)Session["Typ"] == "B")
            {
				cmdAddNew.Parameters.Add("@Broker",Session["Id"].ToString());
			}

           cmdAddNew.ExecuteNonQuery();

           //SqlCommand cmdIdentity;
           //cmdIdentity = new SqlCommand("SELECT @@IDENTITY AS NewID",conn);
           //lblMessage.Text ="<font color=red>Order "+cmdIdentity.ExecuteScalar().ToString()+" entered!</font>";



           pnMain.Visible= true;
           pnConfirm.Visible = false;


    }

</script>
<form runat="server" id="form">
	<TPE:Template PageTitle="Admin Order Entry" Runat="server" id="Template1" />
	<BR>
	<script language="javascript">
        <!--
        function Swap_Other(Flag)
        {
	       if(Flag)
	       {
		      document.form.ddlProduct.disabled=true;
		      document.form.txtProduct.disabled = false;
		      document.form.txtMelt.Value ='';
		      document.form.txtDensity.Value ='';
		      document.form.txtAdds.Value ='';

	       }
	       else
	       {
		      document.form.ddlProduct.disabled = false;
		      document.form.txtProduct.disabled = true;

	       }
        }

        function Address_PopUp()
        // This function opens up a pop up window and passes the appropiate ID number for the window
        {


        if (document.all)
            var xMax = screen.width, yMax = screen.height;
        else
            if (document.layers)
                var xMax = window.outerWidth, yMax = window.outerHeight;
            else
                var xMax = 640, yMax=480;

        var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
        var str = "../administrator/Location_Details.aspx?ID=" + document.form.ddlDelivery_Point.options[document.form.ddlDelivery_Point.selectedIndex].value;
        window.open(str,'Address','location=true,toolbar=true,width=275,height=370,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');

        }


    //-->
	</script>
	<TPE:Web_Box id="WebBox1" Width="750" Heading="Buy Order Entry" Runat="Server" />
	<asp:Panel runat="server" id="pnMain"> <!-- flag for the radio selection -->
		<asp:Label id="lblMessage" runat="server"></asp:Label>
		<asp:ValidationSummary id="ValidationSummary1" runat="server" HeaderText="There are problems with the following fields. Please correct the errors below."></asp:ValidationSummary>
		<TABLE cellSpacing="0" cellPadding="0" align="center" border="0">
			<TR>
				<TD colSpan="10">
					<TABLE cellSpacing="0" cellPadding="0" align="center" border="0">
						<TR>
							<TD class="S1" colSpan="2"><B>Company</B>
								<asp:DropDownList id="ddlCompanies" runat="server" OnSelectedIndexChanged="PostBack" AutoPostBack="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;
								<asp:DropDownList id="ddlUser" runat="server"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD>
								<TABLE cellSpacing="0" cellPadding="0" width="350" border="0">
									<asp:Panel id="pnAltCompany" runat="server">
										<TBODY>
											<TR>
												<TD class="S1">Company</TD>
												<TD><!-- validation occurs here only because the panel makes the control visible only when the field is required -->
													<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ErrorMessage="Location cannot be blank"
														Display="none" ControlToValidate="txtDelivery_Point"></asp:RequiredFieldValidator>
													<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ErrorMessage="Company cannot be blank"
														Display="none" ControlToValidate="txtCompany"></asp:RequiredFieldValidator>
													<asp:TextBox onkeypress="return noenter()" id="txtCompany" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;
													<asp:DropDownList id="ddlOrderType" runat="server" OnSelectedIndexChanged="PostBack" AutoPostBack="true"
														visible="false"></asp:DropDownList></TD>
											</TR>
									</asp:Panel>
									<TR>
										<TD align="left" colSpan="2">
											<TABLE cellSpacing="0" cellPadding="0" border="0">
												<TR>
													<TD class="S1">Product
													</TD>
													<TD>
														<asp:RadioButton id="radio_Product" onclick="JavaScript:Swap_Other(0)" runat="server" GroupName="RadioGroup1"
															checked="true"></asp:RadioButton></TD>
													<TD>
														<asp:DropDownList id="ddlProduct" runat="server"></asp:DropDownList>
														<asp:DropDownList id="ddlAction" runat="server" OnSelectedIndexChanged="ddlActionChange" AutoPostBack="true"></asp:DropDownList></TD>
												</TR>
												<TR>
													<TD class="S1">Other
													</TD>
													<TD class="S1">
														<asp:RadioButton id="radio_Other" onclick="JavaScript:Swap_Other(1)" runat="server" GroupName="RadioGroup1"></asp:RadioButton></TD>
													<TD class="S1">
														<asp:TextBox onkeypress="return noenter()" id="txtProduct" runat="server"></asp:TextBox></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD class="S1">Details
										</TD>
										<TD>
											<asp:TextBox onkeypress="return noenter()" id="txtDetail" runat="server" size="30" maxlength="75"></asp:TextBox></TD>
									</TR>
									<TR>
										<TD class="S1" colSpan="2">Melt
											<asp:TextBox onkeypress="return noenter()" id="txtMelt" runat="server" size="4" maxlength="16"
												type="text"></asp:TextBox>
											<asp:CompareValidator id="CompareValidator1" Runat="server" ErrorMessage="Density must be a number." Display="none"
												ControlToValidate="txtDensity" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
											<asp:RangeValidator id="RangeValidator1" runat="server" ErrorMessage="Density must be less than one."
												Display="none" ControlToValidate="txtDensity" type="Integer" MaximumValue="999" MinimumValue="1"></asp:RangeValidator>&nbsp;&nbsp;&nbsp;Density 
											.
											<asp:TextBox onkeypress="return noenter()" id="txtDensity" runat="server" size="4" maxlength="16"
												type="text"></asp:TextBox>&nbsp;&nbsp;&nbsp;Adds
											<asp:TextBox onkeypress="return noenter()" id="txtAdds" runat="server" size="4" maxlength="20"
												type="text"></asp:TextBox></TD>
									</TR>
									<TR>
										<TD class="S1">Quantity
										</TD>
										<TD>
											<asp:CompareValidator id="CompareValidator2" Runat="server" ErrorMessage="Quantity must be a number."
												Display="none" ControlToValidate="txtQty" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>
											<asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" ErrorMessage="Quanity cannot be blank"
												Display="none" ControlToValidate="txtQty"></asp:RequiredFieldValidator>
											<asp:TextBox onkeypress="return noenter()" id="txtQty" runat="server" size="3" maxlength="5"></asp:TextBox>&nbsp;&nbsp;&nbsp;
											<asp:DropDownList id="ddlQty" runat="server"></asp:DropDownList></TD>
									</TR>
								</TABLE>
							</TD>
							<TD>
								<TABLE>
									<TR>
										<TD class="S1" vAlign="top">
											<asp:Label id="lblPoint" runat="server"></asp:Label></TD>
										<TD>
											<asp:TextBox onkeypress="return noenter()" id="txtDelivery_Point" runat="server"></asp:TextBox><BR>
											<asp:Panel id="pnDelivery_Point" runat="server">
<asp:DropDownList id="ddlDelivery_Point" runat="server"></asp:DropDownList>&nbsp;&nbsp;<A onmouseover="this.style.cursor='hand'" onclick="javascript:Address_PopUp()">
													<U>Details</U></A> 
                  </asp:Panel></TD>
									</TR>
									<TR>
										<TD class="S1">Terms
										</TD>
										<TD>
											<asp:DropDownList id="ddlTerms" runat="server"></asp:DropDownList></TD>
									</TR>
									<TR>
										<TD class="S1">
											<asp:Label id="lblPrice" runat="server"></asp:Label></TD>
										<TD>
											<asp:CompareValidator id="CompareValidator3" Runat="server" ErrorMessage="Price must be a number." Display="none"
												ControlToValidate="txtPrice" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>
											<asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" ErrorMessage="Price cannot be blank"
												Display="none" ControlToValidate="txtPrice"></asp:RequiredFieldValidator>$<B>.</B>
											<asp:TextBox onkeypress="return noenter()" id="txtPrice" runat="server" size="4" maxlength="4"></asp:TextBox>per 
											lb.
										</TD>
									</TR>
									<TR>
										<TD class="S1">Markup</TD>
										<TD>
											<asp:CompareValidator id="CompareValidator4" Runat="server" ErrorMessage="Markup must be a number." Display="none"
												ControlToValidate="txtMarkup" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>
											<asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server" ErrorMessage="Markup cannot be blank"
												Display="none" ControlToValidate="txtMarkup"></asp:RequiredFieldValidator>$<B>.</B>
											<asp:TextBox onkeypress="return noenter()" id="txtMarkup" runat="server" size="4" maxlength="4">02</asp:TextBox>per 
											lb.</TD>
									</TR>
									<TR>
										<TD class="S1">
											<asp:Label id="lblExpires" runat="server"></asp:Label></TD>
										<TD>
											<asp:DropDownList id="ddlexpires" runat="server"></asp:DropDownList></TD>
									</TR>
								</TABLE>
							</TD>
						</TR>
						<TR vAlign="middle" height="25">
							<TD colSpan="3">
								<CENTER><BR>
									<asp:Button class="tpebutton" id="Button1" onclick="Save" runat="server" Text="Submit Order"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<asp:Button class="tpebutton" id="Button2" onclick="Click_Submit" runat="server" Text="Review Order"></asp:Button></CENTER>
								<BR>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
			</TBODY></TABLE>
	</asp:Panel>
	<asp:Panel id="pnConfirm" Visible="false" runat="server">
		<CENTER><BR>
			<BR>
			<BR>
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<TR vAlign="top">
					<TD>
						<asp:Label id="lblConfirm" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD colSpan="7"><BR>
						<CENTER>
							<asp:Button class="tpebutton" id="btnSubmit" onclick="Save" runat="server" Text="Submit"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:Button class="tpebutton" id="btnCancel" onclick="Click_Cancel" runat="server" Text="Cancel"></asp:Button></CENTER>
						<BR>
						<BR>
					</TD>
				</TR>
			</TABLE>
		</CENTER>
		<BR>
		<BR>
		<BR>
		<BR>
		<BR>
		<BR>
	</asp:Panel>
	<TPE:Web_Box Footer="true" Runat="server" id="Web_Box1" />
	<TPE:Template Footer="true" Runat="server" id="Template2" />
</form>
