using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Mail;
using System.Text;
using TPE.Utility;

namespace localhost.Spot
{
	/// <summary>
	/// Summary description for Inquire. 
	/// </summary>
	public partial class Inquire : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Label lblEmail;
		protected System.Web.UI.WebControls.Label lblType;
		protected System.Web.UI.WebControls.Label lblComment;
		protected System.Web.UI.WebControls.Label lblOrigin;
		protected System.Web.UI.WebControls.Label lblExpiration;
		protected System.Web.UI.WebControls.Label lblCompany;
		protected System.Web.UI.WebControls.Label lblPayment;
		protected System.Web.UI.WebControls.Label lblFirm;
		protected System.Web.UI.WebControls.Label lblDescription;
		protected System.Web.UI.WebControls.LinkButton hplReturnSpotFlor;
		protected System.Web.UI.WebControls.Button btnCancel;
		//protected System.Web.UI.WebControls.Label lblTitleDetail;
		protected System.Web.UI.WebControls.Panel Panel2;
		protected System.Web.UI.WebControls.Label Label26;
		protected System.Web.UI.WebControls.Label Label25;
		//protected System.Web.UI.WebControls.TextBox txtZipCode_t;
		//protected System.Web.UI.WebControls.Label lblPrice;
         
		
		protected void Page_Load(object sender, System.EventArgs e)
		{
            Master.Width = "780px";
         	ltLivePerson.Text =HelperFunction.getLivePersonButton();

			btnUpdate.Attributes.Add("onMouseOver", "document.getElementById('" + btnUpdate.ClientID + "').src ='/images/buttons/update_pressed_bkgrey.jpg';");
			btnUpdate.Attributes.Add("onMouseOut", "document.getElementById('" + btnUpdate.ClientID + "').src ='/images/buttons/update_unpress_bkgrey.jpg';");
			ImageButton2.Attributes.Add("onMouseOver", "document.getElementById('" + ImageButton2.ClientID +"').src ='/images/buttons/submit_pressed_bkdark.jpg';");
            ImageButton2.Attributes.Add("onMouseOut", "document.getElementById('" + ImageButton2.ClientID + "').src ='/images/buttons/submit_unpress_bkdark.jpg';");
            ImageButton1.Attributes.Add("onMouseOver", "document.getElementById('" + ImageButton1.ClientID + "').src ='/images/buttons/clickhere_press.jpg';");
            ImageButton1.Attributes.Add("onMouseOut", "document.getElementById('" + ImageButton1.ClientID + "').src ='/images/buttons/clickhere_unpress.jpg';");
            hlpMessage1.Attributes.Add("onMouseOver", "document.getElementById('"+ hlpMessage1.ClientID +"').src ='/images/buttons/spotfloor_press.jpg';");
            hlpMessage1.Attributes.Add("onMouseOut", "document.getElementById('" + hlpMessage1.ClientID + "').src ='/images/buttons/spotfloor_unpress.jpg';");
            hlpMessage2.Attributes.Add("onMouseOver", "document.getElementById('" + hlpMessage2.ClientID + "').src ='/images/buttons/request_press.jpg';");
            hlpMessage2.Attributes.Add("onMouseOut", "document.getElementById('" + hlpMessage2.ClientID + "').src ='/images/buttons/request_unpress.jpg';");

			if (Session["ID"] == null)
			{
				//	Response.Redirect("/default.aspx");
			}
			//btnBuyOnline.Visible =false;
			if (!IsPostBack)
			{
				bool International = false;
				if (Request.QueryString["Export"]!=null)
				{
					if (Request.QueryString["Export"].ToString()=="true")
					{
						International = true;
					}
				}
                Master.Width = "780px";

				ViewState["BidOffer"] = "O";
				if (Request.QueryString["BidOffer"]!=null)
				{
					ViewState["BidOffer"] = Request.QueryString["BidOffer"].ToString();
				}

				string DestinationFreight = "";
				if (Request.QueryString["DestinationFreight"]!=null)
				{
					DestinationFreight = Request.QueryString["DestinationFreight"].ToString();
				}

				SqlConnection conn;
				string strSQL;
				conn = new SqlConnection(Application["DBConn"].ToString());
				try
				{
					conn.Open();
					//SqlCommand cmdTransaction;
					SqlDataReader drTransaction;

					Hashtable parameters = new Hashtable();

					strSQL = "Exec spSpot_Floor ";
					if (ViewState["BidOffer"].ToString()=="O")
					{
						//strSQL +="@UserType='S'";
						parameters.Add("@UserType","S");
						ViewState["TYPE"] = "Offer";
					}
					else
					{
						//strSQL +="@UserType='P'";
						parameters.Add("@UserType","P");
						ViewState["TYPE"] = "Request";
					}

					if (International) 
						//strSQL +=" ,@Export='true'";
						parameters.Add("@Export",true);
				
					// add the sort command					
					parameters.Add("@Select_Single_Offer",Request.QueryString["ID"].ToString());					
					parameters.Add("@Sort","VARID DESC");

					//dadContent = new SqlDataAdapter(strSQL,conn);
					//cmdTransaction = new SqlCommand(strSQL, conn);
					drTransaction = DBLibrary.GetDataReaderStoredProcedure(conn,"spSpot_Floor", parameters);
					//drTransaction = cmdTransaction.ExecuteReader();
					if (drTransaction.Read())
					{
						lblProduct.Text = drTransaction["VARCONTRACT"].ToString();
						lblOrderID.Text = drTransaction["VARID"].ToString();
						lblSize.Text = drTransaction["VARSIZE"].ToString();
						lblMelt.Text = drTransaction["VARMELT"].ToString();
						lblDens.Text = drTransaction["VARDENS"].ToString();
						lblAdds.Text = drTransaction["VARADDS"].ToString();
						lblTerms.Text = drTransaction["VARTERM"].ToString();
						ViewState["lblTerms"] = drTransaction["VARTERM"].ToString();
						lblPrice.Text = drTransaction["VARPRICE2"].ToString();


						string realsize = drTransaction["VARREALSIZE"].ToString();
						int realqty= Convert.ToInt32(drTransaction["VARREALQTY"].ToString());
						if(realsize == "190000" || realsize == "42000")//rail car
						{
							if(realqty > 1)
							{
								lblSize.Visible = false;
								lblRailCars.Visible = true;
								if(realsize == "42000")
									lblRailCars.Text = "T/L Boxes";
								ddlNumRC.Visible = true;
								for(int i = 1; i <= realqty; i++)
								{
									ddlNumRC.Items.Add(i.ToString());
								}
								ddlNumRC.SelectedIndex = ddlNumRC.Items.Count - 1;
							}
						}

						ViewState["lblPrice"] = drTransaction["VARPRICE2"].ToString();
						if (International) 
						{
							lblPrice.Text = "$" + String.Format("{0:#,###}", Math.Round(System.Convert.ToDecimal(lblPrice.Text),0));
							lblSize.Text = Math.Round(Convert.ToDecimal(lblSize.Text),0) + " Metric Tons";
						}
						else
						{
							lblPrice.Text = "$" + lblPrice.Text;
						}

						ViewState["Firm"] = drTransaction["VARFIRM"].ToString();
						ViewState["Origin"] = drTransaction["VARPLACE"].ToString();
						ViewState["Expiration"] = drTransaction["VAREXPR"].ToString();
						ViewState["TERMS"] = drTransaction["VARPAY"].ToString();
						ViewState["ID"] = drTransaction["VARID"].ToString();
						ViewState["Product"] = drTransaction["VARCONTRACT"].ToString();
						ViewState["PERS_ID"] = drTransaction["VARUSRID"].ToString();
						ViewState["COMP"] = drTransaction["VARUSR"].ToString();
                        ViewState["VARFREIGHTPOINT"] = (drTransaction["VARFREIGHTPOINT"].ToString() == String.Empty) ? "0" : drTransaction["VARFREIGHTPOINT"].ToString();
						ViewState["VARREALSIZE"] = drTransaction["VARREALSIZE"].ToString();
						ViewState["VARPRICE"] = drTransaction["VARPRICE2"].ToString();
			
						//Display roles for Density and Adds
						//					if (lblDens.Text.Trim()=="")
						//					{
						//						lbAdds.Visible = false;
						//						lblAdds.Visible = false;
						//						if (lblAdds.Text.Trim()=="")
						//						{
						//							lbDensity.Visible = false;
						//							lblDens.Visible = false;
						//						}
						//						else
						//						{
						//							lbDensity.Text = lbAdds.Text;
						//							lblDens.Text = lblAdds.Text;
						//						}
						//					}
						//					else
						//					{
						//						if (lblAdds.Text.Trim()=="")
						//						{
						//							lbAdds.Visible = false;
						//							lblAdds.Visible = false;
						//						}
						//					}

						if (LoggedOn())
						{
							lbName.Visible = false;
							lbCompany.Visible = false;
							lbEmail.Visible = false;
							lbPhone.Visible = false;
							lblLocation.Visible = false;
							txtName.Visible = false;
							txtCompany.Visible = false;
							txtEmail.Visible = false;
							txtPhone.Visible = false;
							txtLocation.Visible = false;
							ImageButton2.CausesValidation = false;

							ltLivePerson.Visible = true;
						}
						else
						{
							//pnlBuyOnline.Visible = false;
							ltLivePerson.Visible = false;
						}

					

						if (ViewState["BidOffer"].ToString()=="O")
						{
							//if (LoggedOn())
							pnlBuyOnline.Visible = true;
							lblTotalLbs.Text = String.Format("{0:#,###}",HelperFunction.totalPoundsOffers(this.Context));

							//hlpMessage2.NavigateUrl = "/spot/Resin_Entry.aspx";
						}
						else
						{
							pnlBuyOnline.Visible = false;
							Label11.Text = "<font size=4>The Plastics Exchange is looking to purchase the following resin.</font><BR>  If you have any question contact us by phone at";
							lblMessage1.Text = "lbs of resin that The Plastic Exchange is currently looking to buy:";
							lblMessage2.Text = "Click below and have The Plastics Exchange sell your surplus resin for you!";							
							//lblTitleDetail.Text = "Request Details";
							divDetails.InnerText = "Request Details";
							lblTotalLbs.Text = String.Format("{0:#,###}",HelperFunction.totalPoundsBids(this.Context)) + " lbs";
							
							//hlpMessage2.Text = "Sell Resin";
							//hlpMessage2.NavigateUrl = "/spot/Resin_Entry.aspx";
						}

						ResetFreightBar(International, DestinationFreight);
						//if (pnlFreight.Visible == true) btnUpdate_Click(this,null);
					}
					else
					{
						pnMain.Visible = false;
						pnError.Visible = true;
					}
				}
				finally
				{
					conn.Close();
				}

			}
		}
		
		private void SendEmail()
		{
//			string emailsCC = ";helio@theplasticsexchange.com;christian@theplasticsexchange.com;tom@theplasticsexchange.com";

			if (!LoggedOn())
			{

				saveFields();

				string emailTO="";
				string emailCC="";
				string emailSubject="Resin Inquiry";
				string buyerName = "";

				if ((ViewState["PERS_ID"] == null) || (ViewState["PERS_ID"].ToString() == ""))
				{
					buyerName = ViewState["COMP"].ToString();
					emailTO = Application["strEmailInfo"].ToString();
					emailCC = Application["strEmailAdmin"]+";"+Application["strEmailOwner"].ToString();
				}
				else
				{
					emailSubject = emailSubject + "   " + PersEmail(System.Convert.ToInt64(ViewState["PERS_ID"]),ref emailTO,ref buyerName);
				}

				if (emailTO=="")
				{
					emailTO = Application["strEmailInfo"].ToString();
					emailCC = Application["strEmailAdmin"]+";"+Application["strEmailOwner"].ToString();
				}
				else
				{
					//If the email is send to a Broker...
					if (emailTO.Trim()!=Application["strEmailInfo"].ToString().Trim())
					{
						//include EmailOwner and Email Admin in CC
						emailCC = Application["strEmailOwner"].ToString() + ";" + Application["strEmailAdmin"].ToString()+ ";" +Application["strEmailInfo"].ToString();
					}
				}

				StringBuilder sbHTML= new StringBuilder();
				sbHTML.Append("<h3>Resin Inquiry Regarding - <a href='" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Spot/Inquire.aspx?Id=" + ViewState["ID"].ToString() + "' target='_blank'>"+ViewState["TYPE"].ToString()+" #"+ViewState["ID"].ToString()+ "</a></h3>");
				sbHTML.Append("<table><tr><td align='left'><font size='3'><b>Name: </b></font></td><td>"+txtName.Text+" </td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Company: </b></font></td><td>"+txtCompany.Text+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Email: </b></font></td><td><a href='mailto:"+txtEmail.Text+"'>"+txtEmail.Text+"</a></td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Phone: </b></font></td><td>"+txtPhone.Text+"</td></tr>");
				sbHTML.Append("<tr><td colspan='2'><font align='left' size='4'><b><U>Product Specifications</U></b></font></td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Product: </b></font></td><td>"+lblProduct.Text+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Size: </b></font></td><td>"+lblSize.Text+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Price: </b></font></td><td>"+lblPrice.Text+"</td></tr>");
				//sbHTML.Append("<tr><td align='left'><font size='3'><b>Location: </b></font></td><td>"+ViewState["Origin"].ToString()+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Expiration: </b></font></td><td>"+ViewState["Expiration"].ToString()+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Adds: </b></font></td><td>"+lblAdds.Text+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Melt: </b></fontsize></td><td>"+lblMelt.Text+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Density: </b></font></td><td>"+lblDens.Text+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Terms: </b></font></td><td>"+lblTerms.Text+"</td></tr>");
				//				sbHTML.Append("<tr><td align='left'><font size='3'><b>Payment: </b></font></td><td>"+ViewState["TERMS"].ToString()+" days</td></tr>");
				//				sbHTML.Append("<tr><td align='left'><font size='3'><b>Firm: </b></font></td><td>"+ViewState["Firm"].ToString()+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Comments: </b></font></td><td>"+txtComments.Text+"</td></tr></table>");
			
				MailMessage EmailSignUp;
				EmailSignUp = new MailMessage();
				EmailSignUp.From = "resin@theplasticsexchange.com";
				EmailSignUp.To = emailTO;
				EmailSignUp.Cc = emailCC;
				EmailSignUp.Subject = emailSubject;
				EmailSignUp.Body = sbHTML.ToString();
				EmailSignUp.BodyFormat = MailFormat.Html;
				TPE.Utility.EmailLibrary.Send(EmailSignUp);
				pnMain.Visible = false;
				pnThankYou.Visible = true;

				copyValues();

			}
			else
			{
				string emailTO="";
				string emailCC="";
				string emailSubject="Resin Inquiry";
				string buyerName = "";

				if ((ViewState["PERS_ID"] == null) || (ViewState["PERS_ID"].ToString() == ""))
				{
					buyerName = ViewState["COMP"].ToString();
					emailTO = Application["strEmailInfo"].ToString();
					emailCC = Application["strEmailAdmin"]+";"+Application["strEmailOwner"].ToString();
				}
				else
				{
					emailSubject = emailSubject + "   " + PersEmail(System.Convert.ToInt64(ViewState["PERS_ID"]),ref emailTO,ref buyerName);
				}

				if (emailTO=="")
				{
					emailTO = Application["strEmailInfo"].ToString();
					emailCC = Application["strEmailAdmin"]+";"+Application["strEmailOwner"].ToString();
				}
				else
				{
					//If the email is send to a Broker...
					if (emailTO.Trim()!=Application["strEmailInfo"].ToString().Trim())
					{
						//include EmailOwner and Email Admin in CC
						emailCC = Application["strEmailOwner"].ToString() + ";" + Application["strEmailAdmin"].ToString()+ ";" +Application["strEmailInfo"].ToString();
					}
				}

				StringBuilder sbHTML= new StringBuilder();
				sbHTML.Append("<h3><U>Resin Inquiry Regarding - <a href='" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Spot/Inquire.aspx?Id=" + ViewState["ID"].ToString() + "' target='_blank'>"+ViewState["TYPE"].ToString()+" #"+ViewState["ID"].ToString()+ "</a></U></h3>");
				
				sbHTML.Append(HelperFunction.getUserInformationHTML(Session["UserName"].ToString(), Session["Password"].ToString(),emailTO,this.Context));
				
				sbHTML.Append("<br>");
				sbHTML.Append("<table><tr><td colspan='2'><font align='left' size='4'><b><U>Product Specifications</U></b></font></td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Product: </b></font></td><td>"+lblProduct.Text+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Size: </b></font></td><td>"+lblSize.Text+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Price: </b></font></td><td>"+lblPrice.Text+"</td></tr>");
				//sbHTML.Append("<tr><td align='left'><font size='3'><b>Location: </b></font></td><td>"+ViewState["Origin"].ToString()+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Expiration: </b></font></td><td>"+ViewState["Expiration"].ToString()+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Adds: </b></font></td><td>"+lblAdds.Text+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Melt: </b></fontsize></td><td>"+lblMelt.Text+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Density: </b></font></td><td>"+lblDens.Text+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Terms: </b></font></td><td>"+lblTerms.Text+"</td></tr>");
				//				sbHTML.Append("<tr><td align='left'><font size='3'><b>Payment: </b></font></td><td>"+ViewState["TERMS"].ToString()+" days</td></tr>");
				//				sbHTML.Append("<tr><td align='left'><font size='3'><b>Firm: </b></font></td><td>"+ViewState["Firm"].ToString()+"</td></tr>");
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Comments: </b></font></td><td>"+txtComments.Text+"</td></tr></table>");
			

				System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
				mail.From="resin@theplasticsexchange.com";
				mail.To = emailTO;
				mail.Cc = emailCC;
				mail.Body=sbHTML.ToString();
				mail.Subject=emailSubject;
				mail.BodyFormat = MailFormat.Html;
				TPE.Utility.EmailLibrary.Send(Context,mail);

				pnMain.Visible = false;
				pnThanks.Visible = true;
			

			}
		}


		private void SaveUserRequest()
		{								
			string strSQL;
			Hashtable param = new Hashtable();
			if(LoggedOn())
			{					
				param.Add("@REQ_MESSAGE", txtComments.Text);
				param.Add("@REQ_PERS_ID", Session["ID"]);					
				param.Add("@REQ_OFFER_ID", Request.QueryString["ID"].ToString());

				strSQL = "INSERT INTO USER_REQUEST (REQ_MESSAGE, REQ_PERS_ID , REQ_DATE_TIME, REQ_OFFER_ID)" +
					" VALUES (@REQ_MESSAGE,@REQ_PERS_ID, getdate(), @REQ_OFFER_ID)";
			}
			else
			{					
				param.Add("@REQ_MESSAGE", txtComments.Text);
				param.Add("@REQ_NAME", txtName.Text);					
				param.Add("@REQ_PHONE", txtPhone.Text);					
				param.Add("@REQ_EMAIL", txtEmail.Text);					
				param.Add("@REQ_LOCATION", txtLocation.Text);					
				param.Add("@REQ_OFFER_ID", Request.QueryString["ID"].ToString());

				strSQL = "INSERT INTO USER_REQUEST (REQ_MESSAGE, REQ_NAME, REQ_PHONE, REQ_EMAIL, REQ_LOCATION, REQ_DATE_TIME, REQ_OFFER_ID)" +
					" VALUES (@REQ_MESSAGE, @REQ_NAME, @REQ_PHONE, @REQ_EMAIL, @REQ_LOCATION, getdate(), @REQ_OFFER_ID)";
			}

			DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), strSQL, param);							
		}

		void copyValues()
		{
			lblAdds_t.Text = lblAdds.Text;
			lblDensity_t.Text = lblDens.Text;
			lblOrderID_t.Text = lblOrderID.Text;
			lblMelt_t.Text = lblMelt.Text;
			lblProduct_t.Text = lblProduct.Text;
			lblPrice_t.Text = lblPrice.Text;
			lblTerms_t.Text = lblTerms.Text;
			lblSize_t.Text = lblSize.Text;
			lblRailCars_t.Text = lblRailCars.Text;
			lblTotalPrice_t.Text = lblTotalPrice.Text;

			//txtZipCode_t.Text = txtZipCode.Text;

			
		}

		void saveFields(){
			Session["saved_info"] = "true";

			Session["svd_company"] = txtCompany.Text;
			Session["svd_name"] = txtName.Text;
			Session["svd_phone"] = txtPhone.Text;
			Session["svd_email"] = txtEmail.Text;
//			Session["svd_location"] = txtLocation.Text;
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnUpdate.Click += new System.Web.UI.ImageClickEventHandler(this.btnUpdate_Click);
			this.ImageButton1.Click += new System.Web.UI.ImageClickEventHandler(this.ImageButton1_Click);
			this.hlpMessage1.Click += new System.Web.UI.ImageClickEventHandler(this.hlpMessage1_Click);
			this.ImageButton2.Click += new System.Web.UI.ImageClickEventHandler(this.ImageButton2_Click);

		}
		#endregion

		private bool LoggedOn()
		{
			return ((Session["ID"]!=null) && (Session["ID"].ToString()!=""));
		}

		private string PersEmail(long PersID, ref string emailBroker, ref string firsName)
		{
			
			string email =""; 
			firsName = "";
			emailBroker = "";
			
			DataTable dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), "SELECT P.PERS_MAIL, P.PERS_FRST_NAME, B.PERS_MAIL FROM PERSON P left join (SELECT PERS_ID, PERS_MAIL FROM PERSON) B on P.PERS_ACES = B.PERS_ID WHERE P.PERS_ID = " + PersID.ToString());
			foreach(DataRow dr in dt.Rows)
			{
				email = HelperFunction.getSafeStringFromDB(dr[0]);
				emailBroker = HelperFunction.getSafeStringFromDB(dr[2].ToString());
				firsName = HelperFunction.getSafeStringFromDB(dr[1].ToString());
			}
			
			return email;
		}

		

		private void btnBuyOnline_Click(object sender, System.EventArgs e)
		{
			
		}

		private void ResetFreightBar(bool InternationalMarket, string DestinationFreight)
		{
			if (InternationalMarket)
			{
				if (!lblZipCode.Visible == false) //screen mode was not International
				{
					lblZipCode.Visible = false;
					txtZipCode.Visible = false;
					txtZipCode.Text = "";
					lblCityPort.Text = "Port:";
					LoadPorts();
					if (DestinationFreight!="")
					{
						ddlCitiesPorts.SelectedItem.Selected = false;
						ddlCitiesPorts.Items.FindByValue(DestinationFreight).Selected = true;
					}
				}
			}
			else
			{
				if ((lblZipCode.Visible == false) || (ddlCitiesPorts.Items.Count==0)) //screen mode was not Domestic
				{
					lblZipCode.Visible = true;
					txtZipCode.Visible = true;
					lblCityPort.Visible = false;
					
					lblCityPort.Visible = false;
					ddlCitiesPorts.Visible = false;

					//lblCityPort.Text = "or City:";
					//LoadCities();

					if (DestinationFreight!="")
					{
						//ddlCitiesPorts.SelectedItem.Selected = false;
						//if (ddlCitiesPorts.Items.FindByValue(DestinationFreight)!=null)
						//	ddlCitiesPorts.Items.FindByValue(DestinationFreight).Selected = true;
						//else
							txtZipCode.Text = DestinationFreight;
					}
				}
			}
			
			if (lblTerms.Text=="Delivered")
			{
				lblFreight.Text = "$0.00";
				lblTotalPrice.Text = lblPrice.Text;
				pnlFreight.Visible = false;
			}
			else
			{
				pnlFreight.Visible = true;
			}
		}
			
		private void ImageButton2_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			SendEmail();

			//It saves the user request into the DB
			SaveUserRequest();
		}

		private void LoadCities()
		{
			HelperFunctionDistances hfd = new HelperFunctionDistances();
			string userType = "";
			if (Session["Typ"]!=null) userType = Session["Typ"].ToString();
			hfd.loadMostPopularCities(ddlCitiesPorts, userType);
		}

		private void LoadPorts()
		{
			ddlCitiesPorts.Items.Clear();
			
			ddlCitiesPorts.Items.Add(new ListItem("Select Port...",""));
			DataTable dtPorts = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), "SELECT IP.PORT_ID, C.CTRY_NAME  + ISNULL(' - '+NULLIF(IP.PORT_CITY,'') ,'')  PORT FROM INTERNATIONAL_PORT IP, COUNTRY C WHERE IP.PORT_COUNTRY = C.CTRY_CODE ORDER BY C.CTRY_NAME");
			foreach(DataRow dr in dtPorts.Rows)
			{
				ddlCitiesPorts.Items.Add(new ListItem(dr["PORT"].ToString(),dr["PORT_ID"].ToString()));
			}
		}
		
		private void ImageButton1_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Session.Contents["ProductId"] = ViewState["BidOffr"]+ "^"+ViewState["ID"].ToString() ; //lblID.Text.Trim() ;
			if((Session["RC_changed"] != null) && (Session["RC_changed"].ToString() == "true"))
			{
				Session["price"] = lblPrice.Text.ToString().Remove(0, 1);
				Session["qty"] = ddlNumRC.SelectedItem.Text;
			}
			Response.Redirect("ShipDetails.aspx");
		}

		protected void ddlNumRC_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			double dParam = 0.01;
			
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			SqlCommand sqlComm = new SqlCommand("SELECT PARAM_VALUE FROM PARAMETERS WHERE PARAM_NAME='SEPARATERC_FEE'", conn);
			SqlDataReader dtrFee = sqlComm.ExecuteReader();

			if(dtrFee.Read()){
				dParam = Convert.ToDouble(dtrFee["PARAM_VALUE"].ToString());
			}
			


			if(ddlNumRC.SelectedIndex != (ddlNumRC.Items.Count - 1)) // not the last item means different number of RC
			{
				lblPrice.Text = Convert.ToString(Convert.ToDouble(ViewState["VARPRICE"].ToString()) + dParam);
				//lblTotalPrice.Text = Convert.ToString(Convert.ToDouble(lblTotalPrice.Text.ToString().Remove(0, 1)) + dParam);
				
				//Total price should be lblPrice + freight (based on new number of railcars)
				lblTotalPrice.Text = Convert.ToString(Convert.ToDouble(lblPrice.Text.ToString().Remove(0, 1)) + dParam);
				
				Session["RC_changed"] = "true";
												 
			}
			else
			{
				lblPrice.Text = Convert.ToString(Convert.ToDouble(ViewState["VARPRICE"].ToString()));
				lblTotalPrice.Text = Convert.ToString(Convert.ToDouble(lblPrice.Text.ToString().Remove(0, 1)) - dParam);
				Session["RC_changed"] = "false";

			}
		}

		private void btnUpdate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if ((txtZipCode.Text!="") || (ddlCitiesPorts.SelectedIndex!=0))
			{
				HelperFunctionDistances hfd = new HelperFunctionDistances();
				decimal freight = 0;
				bool freightAvailable = false;
				if (txtZipCode.Visible == false) //International Market
				{
					int PortID = System.Convert.ToInt32(ddlCitiesPorts.SelectedValue);
					if (ViewState["VARFREIGHTPOINT"]!="")
					{
						if (ViewState["BidOffer"].ToString()=="O")
							freightAvailable = hfd.calculateInternationalFreight(Int32.Parse(ViewState["VARFREIGHTPOINT"].ToString().Trim()),PortID, ref freight, Application["DBconn"].ToString());
						else
							freightAvailable = hfd.calculateInternationalFreight(PortID, System.Convert.ToInt32(ViewState["VARFREIGHTPOINT"].ToString().Trim()), ref freight, Application["DBconn"].ToString());
					}
                    else if (ViewState["lblTerms"].ToString() == "FOB Houston Warehouse")
                    {
						int houstonPortID = HelperFunction.getHoustonPortID(Application["DBConn"].ToString());
						if (ViewState["BidOffer"].ToString()=="O")
							freightAvailable = hfd.calculateInternationalFreight(houstonPortID, PortID, ref freight, Application["DBconn"].ToString());
						else
							freightAvailable = hfd.calculateInternationalFreight(PortID, houstonPortID, ref freight, Application["DBconn"].ToString());
					}

					lblFreight.Text =  "$" + String.Format("{0:#,###}",Math.Round(freight,2));
					if (freightAvailable)
					{
						lblTerms.Text = "CFR " + ddlCitiesPorts.SelectedItem.Text;
						if (ViewState["BidOffer"].ToString()=="O")
							lblTotalPrice.Text = "$" + String.Format("{0:#,###}",Math.Round(System.Convert.ToDecimal(ViewState["lblPrice"].ToString()) + System.Convert.ToDecimal(Math.Round(freight,2)),2));
						else
							lblTotalPrice.Text = "$" + String.Format("{0:#,###}",Math.Round(System.Convert.ToDecimal(ViewState["lblPrice"].ToString()) - System.Convert.ToDecimal(Math.Round(freight,2)),2));
					}
					else
					{
						lblFreight.Text =  "Freight not available. Select another Port.";
						lblTotalPrice.Text =  " - ";
						lblTerms.Text = ViewState["lblTerms"].ToString();
					}
				}
				else //Domestic Market
				{
					string ZipCode="";
					if (ddlCitiesPorts.SelectedIndex!=0) ZipCode = ddlCitiesPorts.SelectedValue;
					if (txtZipCode.Text!="") ZipCode = txtZipCode.Text;
						
					string placeFrom = "";
					string placeTo = "";
					if (ViewState["VARREALSIZE"].ToString()=="45000")
					{						
						if (ViewState["BidOffer"].ToString()=="O")
							freightAvailable = hfd.calculateFreightByZipCode(ViewState["VARFREIGHTPOINT"].ToString().Trim(),ZipCode, ref placeFrom, ref placeTo, ref freight, TruckType.BulkTruck, Application["DBconn"].ToString());
						else
							freightAvailable = hfd.calculateFreightByZipCode(ZipCode, ViewState["VARFREIGHTPOINT"].ToString().Trim(), ref placeFrom, ref placeTo, ref freight, TruckType.BulkTruck, Application["DBconn"].ToString());
					}
					else if ((ViewState["VARREALSIZE"].ToString()=="42000") || (ViewState["VARREALSIZE"].ToString()=="44092"))
					{
						if (ViewState["BidOffer"].ToString()=="O")
							freightAvailable = hfd.calculateFreightByZipCode(ViewState["VARFREIGHTPOINT"].ToString().Trim(),ZipCode, ref placeFrom, ref placeTo, ref freight, TruckType.TruckLoad, Application["DBconn"].ToString());
						else
							freightAvailable = hfd.calculateFreightByZipCode(ZipCode, ViewState["VARFREIGHTPOINT"].ToString().Trim(), ref placeFrom, ref placeTo, ref freight, TruckType.TruckLoad, Application["DBconn"].ToString());
					}
					else if (ViewState["VARREALSIZE"].ToString()=="1")
					{
						if (ViewState["BidOffer"].ToString()=="O")
							freightAvailable = hfd.calculateFreightByZipCode(ViewState["VARFREIGHTPOINT"].ToString().Trim(),ZipCode, ref placeFrom, ref placeTo, ref freight, TruckType.TruckLoad, Application["DBconn"].ToString());
						else
							freightAvailable = hfd.calculateFreightByZipCode(ZipCode, ViewState["VARFREIGHTPOINT"].ToString().Trim(), ref placeFrom, ref placeTo, ref freight, TruckType.TruckLoad, Application["DBconn"].ToString());
					}

					if (freightAvailable)
					{
						pnlFreight.Visible = false;
						
						decimal varfreight = 0;
						if (ViewState["VARREALSIZE"].ToString()=="1")
						{
                            int lbs = 0;
                            if (lblSize.Text.IndexOf("lbs")>0)
                            {
                                lbs = System.Convert.ToInt32(lblSize.Text.Replace("lbs", "").Trim());
                            }
                            else if (lblSize.Text.IndexOf("lb") > 0)
                            {
                                lbs = System.Convert.ToInt32(lblSize.Text.Replace("lb", "").Trim());
                            }
                            
                            double truck =  42000;
							int num_of_trucks = (int)Math.Ceiling (lbs / truck); 
							varfreight = num_of_trucks * Math.Round(freight / lbs ,4);
						}
						else
						{
							varfreight = Math.Round(freight / System.Convert.ToInt32(ViewState["VARREALSIZE"].ToString()),4);
						}


						
						lblFreight.Text =  "$" + varfreight + " (<a href='"+Request.Url.AbsoluteUri.ToString()+"'>Remove Freight</a>)";
						if (ViewState["BidOffer"].ToString()=="O")
						{
							lblTotalPrice.Text = "$" + Convert.ToString(Math.Round(System.Convert.ToDecimal(ViewState["lblPrice"].ToString()) + varfreight,4));
							lblTerms.Text = "Delivered " + placeTo;
						}
						else
						{
							lblTotalPrice.Text = "$" + Convert.ToString(Math.Round(System.Convert.ToDecimal(ViewState["lblPrice"].ToString()) - varfreight,4));
							lblTerms.Text = "FOB Shipping";
						}
					}
					else
					{
						lblFreight.Text =  "Price Not Available";
						lblTotalPrice.Text =  " - ";
						lblTerms.Text = ViewState["lblTerms"].ToString();
					}
				}
			}
		}

		private void hlpMessage1_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if (ViewState["BidOffer"].ToString()=="O")
			{
				Response.Redirect("/spot/Spot_Floor.aspx?Filter=-1");
			}
			else
			{
				Response.Redirect("/spot/Spot_Floor.aspx?Filter=-1&StartOn=bid");
			}
		}

        protected void hlpMessage2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("/spot/Resin_Entry.aspx");
        }
	}
}
