<%@ Page Language="c#" CodeBehind="Archive.aspx.cs" AutoEventWireup="True" Inherits="localhost.Spot.Archive" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" Title="The Plastics Exchange: Commodity Plastic Resin, Buy Resin, Sell Resin, Plastic Materials"%>
<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master"%>


<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">

<style type="text/css">

.Orange {background:#FD9D00;}

</style>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphHeading"><span class="Header Bold Color1">Spot Archive</span></asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">

	<table cellspacing="0" cellpadding="0" border="0" width="1050">
		<tr>
			<td align="center"><span class="PageHeader"></span></td>
		</tr>
		<tr>
			<td align="center">
				<table border="0">
					<tr>
						<td><span class="Content Bold Color2">Resin</span><br />
							<asp:dropdownlist CssClass="InputForm" id="ddlResinType" runat="server" AutoPostBack="True" onselectedindexchanged="ddlResinType_SelectedIndexChanged"></asp:dropdownlist></td>
						<td><span class="Content Bold Color2">Date</span><br />
							<asp:dropdownlist CssClass="InputForm" id="ddlMonth" runat="server" AutoPostBack="True" onselectedindexchanged="ddlMonth_SelectedIndexChanged"></asp:dropdownlist></td>
						<td><span class="Content Bold Color2">Type</span><br />
							<asp:dropdownlist CssClass="InputForm" id="ddlSelector" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSelector_SelectedIndexChanged">
								<asp:ListItem Text="Offers" Value="1" />
								<asp:ListItem Text="Bids" Value="-1" />
							</asp:dropdownlist></td>
						<td><span class="Content Bold Color2">Company</span><br />
							<asp:dropdownlist CssClass="InputForm" id="ddlCompanies" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ChangeType"></asp:dropdownlist></td>
						<td><span class="Content Bold Color2">Search</span><br />
							<asp:textbox CssClass="InputForm" id="txtSearch" runat="server"></asp:textbox><!--Within:
							<asp:dropdownlist id="ddlSeachCol" runat="server">
								<asp:ListItem Text="All" Value="ALL" />
								<asp:ListItem Text="Expired" Value="VAREXPR" />
								<asp:ListItem Text="Size" Value="VARWGHT" />
								<asp:ListItem Text="Product" Value="VARCONTRACT" />
								<asp:ListItem Text="Terms" Value="VARTERM" />
								<asp:ListItem Text="Payment" Value="VARPAY" />
								<asp:ListItem Text="Price" Value="VARPRICE" />
								<asp:ListItem Text="Value" Value="VARVALUE" />
								<asp:ListItem Text="Origin" Value="VARPLACE" />
								<asp:ListItem Text="User" Value="VARUSR" /> 
								<asp:ListItem Text="Melt" Value="VARMELT" />
								<asp:ListItem Text="Density" Value="VARDENS" />
								<asp:ListItem Text="Adds" Value="VARADDS" />
								</asp:dropdownlist>--></td>
								<td valign="middle"><asp:button Height="22" class="tpebutton" id="Button1" onclick="Click_Search" runat="server" Text="Search"></asp:button></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><asp:datagrid BackColor="#000000" CellSpacing="1" BorderWidth="0" id="dg" runat="server" CssClass="DataGrid" ItemStyle-CssClass="LinkNormal LightGray" AlternatingItemStyle-CssClass="LinkNormal DarkGray"
					HeaderStyle-CssClass="LinkNormal Orange" AutoGenerateColumns="False" OnItemDataBound="AddOnDataBind" DataKeyField="VARID"
					onSortCommand="SortDG" AllowPaging="True" PagerStyle-Mode="NumericPages" PageSize="50" OnPageIndexChanged="dgPageChange"
					AllowSorting="True" Width="100%" onselectedindexchanged="dg_SelectedIndexChanged">
					
<AlternatingItemStyle CssClass="LinkNormal DarkGray">
</AlternatingItemStyle>

<ItemStyle CssClass="LinkNormal LightGray">
</ItemStyle>

<HeaderStyle CssClass="LinkNormal Orange">
</HeaderStyle>

<Columns>
<asp:ButtonColumn Text="Reinstate" HeaderText="Reinstate"></asp:ButtonColumn>
<asp:BoundColumn DataField="VARID" SortExpression="VARID ASC" HeaderText="#">
<ItemStyle Wrap="False">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARSIZE" SortExpression="VARSIZE ASC" HeaderText="Size">
<ItemStyle Wrap="False">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARCONTRACT" SortExpression="VARCONTRACT ASC" HeaderText="Product">
<ItemStyle Wrap="False">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARMELT" SortExpression="VARMELT ASC" HeaderText="Melt">
<ItemStyle Wrap="False">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARDENS" SortExpression="VARDENS ASC" HeaderText="Density"></asp:BoundColumn>
<asp:BoundColumn DataField="VARADDS" SortExpression="VARADDS ASC" HeaderText="Adds"></asp:BoundColumn>
<asp:BoundColumn DataField="VARTERM" SortExpression="VARTERM ASC" HeaderText="Terms"></asp:BoundColumn>
<asp:BoundColumn DataField="VARPAY" SortExpression="VARPAY ASC" HeaderText="Payment"></asp:BoundColumn>
<asp:BoundColumn DataField="VARPRICE" SortExpression="VARPRICE ASC" HeaderText="Price" DataFormatString="{0:#,###}"></asp:BoundColumn>
<asp:BoundColumn DataField="VARPRICE2" SortExpression="VARPRICE2 ASC" HeaderText="Price2" DataFormatString="{0:#,###}"></asp:BoundColumn>
<asp:BoundColumn DataField="VARVALUE" SortExpression="VARVALUE ASC" HeaderText="Value" DataFormatString="{0:C}"></asp:BoundColumn>
<asp:BoundColumn DataField="VARPLACE" SortExpression="VARPLACE ASC" HeaderText="Origin"></asp:BoundColumn>
<asp:BoundColumn DataField="VARUSR" SortExpression="VARUSR ASC" HeaderText="User"></asp:BoundColumn>
<asp:BoundColumn DataField="VAREXPR" SortExpression="VAREXPR ASC" HeaderText="Expire" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
<asp:TemplateColumn Visible="False">
<HeaderTemplate>
Reinstate 
</HeaderTemplate>
</asp:TemplateColumn>
</Columns>
<PagerStyle NextPageText="&amp;vt" Position="Bottom" Mode="NumericPages" CssClass="Content Color4 LinkWhite"></PagerStyle>

				</asp:datagrid></td>
		</tr>
	</table>

</asp:Content>