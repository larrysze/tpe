<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page language="c#" Codebehind="ChangeShipping.aspx.cs" AutoEventWireup="false" Inherits="PlasticsExchange.ChangeShipping" %>
<HTML>
	<HEAD>
		<script language="javascript">
		
		function SetforEditing(img,thisform)
		{
			//form11.HId.value=img.alt; 
			window.open("EditLocation.aspx?HID=" + img.alt );
			//form11.submit();
		}
		
		function setBilling(img,thisform)
		{
			form11.BId.value=img.alt; 
		}
		
		function setShipping(img,thisform)
		{
			form11.SId.value=img.alt; 
		}
		//function submit( thisform)
		//{
		//	thisform.submit();
		//}
		
function ShowName(a,b)
{
//alert(a.alt);
/*
alert(b.alt);
alert(b.length);*/
form11.BId.value=a.alt;
form11.SId.value=a.alt; 
 
}

function bc(d)
{
//alert(d.name);
d.value=d.name;
}
function OpenURL(val)
{
	//Callin ShippingDetails after setting SId query parameter
	var Loc_Id;
	Loc_Id=val.name;
	window.open("ShippingDetails.aspx?SId="+ Loc_Id,"_self" ) ;// target=self );
}
function OpenURLBill(val)
{
	//Callin ShippingDetails after setting BId query parameter
	var Loc_Id;
	Loc_Id=val.name;
	window.open("ShippingDetails.aspx?BId="+ Loc_Id ,"_self" ) ;// target=self );
}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form name="form11"  method="post"> <!-- action="EditLocation.aspx"  this was earlier entered in the previous tags-->
			<TPE:TEMPLATE id="Template3" NAME="Template3" Runat="Server" PageTitle="Change Shipping "></TPE:TEMPLATE>
			<TABLE height="66" cellSpacing="0" cellPadding="0" width="533" border="0" ms_2d_layout="TRUE">
				<tr>
					<td style="WIDTH: 590px" align="left"><span class="PageHeader"> Change Shipping </span>
						<BR>
						<font color="red"></font>
					</td>
				</tr><TR >
					<TD align="center">
						<font color="#0033ff" size="5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;Address Book</font>
					</TD>
				</TR>
				<%
			//#####New Code Addition
			string BillAdd=string.Empty;
			string ShipAdd=string.Empty;
			BillAdd = Request.QueryString["BillAdd"].ToString();
			ShipAdd = Request.QueryString["ShipAdd"].ToString();
						
			if (BillAdd.Length < 1)
			{
			//##### Generating Corresponding Addresses at run time
			string StrCmd;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();		 
			SqlCommand Cmd = new SqlCommand();
			SqlDataAdapter DA = new SqlDataAdapter() ;
			 
			DataTable DT = new DataTable();
			Cmd.CommandType=CommandType.Text;
			Cmd.Connection=conn;
			StrCmd="SELECT DISTINCT PLACE.PLAC_PERS, PLACE.PLAC_ADDR_ONE, PLACE.PLAC_ADDR_TWO, LOCALITY.LOCL_CITY, PLACE.PLAC_ZIP, STATE.STAT_NAME,";
			StrCmd+="COUNTRY.CTRY_NAME,PLACE.PLAC_ID, COMPANY.COMP_NAME  FROM  PLACE INNER JOIN  LOCALITY ON PLACE.PLAC_LOCL = LOCALITY.LOCL_ID AND PLACE.PLAC_LOCL = LOCALITY.LOCL_ID INNER JOIN ";
			StrCmd+="STATE ON LOCALITY.LOCL_STAT = STATE.STAT_CODE AND LOCALITY.LOCL_CTRY = STATE.STAT_CTRY INNER JOIN  COUNTRY ON LOCALITY.LOCL_CTRY = COUNTRY.CTRY_CODE ";
			StrCmd+="INNER JOIN COMPANY ON PLACE.PLAC_COMP = COMPANY.COMP_ID INNER JOIN  PERSON ON COMPANY.COMP_ID = PERSON.PERS_COMP Where PERSON.PERS_ID="+Session["ID"].ToString() + " And PLACE.PLAC_TYPE='D'" ;
			Cmd.CommandText=StrCmd;
			DA.SelectCommand= Cmd ;
			//DR = Cmd.ExecuteReader();
			DA.Fill(DT);
  
			Response.Write("<table width=750 cellpadding=0 rightmargin=0 cellspacing=0 align=left border=1 bordercolor=black>");
    		Response.Write("<INPUT TYPE='hidden' NAME='SId' onfocus='bc(this)'><INPUT TYPE='hidden' NAME='HId' onfocus='bc(this)'><INPUT TYPE='hidden' NAME='BId' onfocus='bc(this)'>");
		
		for (int rr=0; rr<=DT.Rows.Count-1; rr++     ) 
			{	
			 
				DataRow ro ;
				ro= DT.Rows[rr]; 			 
				Response.Write("<tr><td>");
				Response.Write("<input type=button value='Ship to This Address' name='"+ro[7]+"' onclick='OpenURL(this)' ><BR>");	
				Response.Write("<b>"+"Name   : " +"</b>"+ro[0]+"<BR>");			
				Response.Write("<b>"+"Add1   : "+"</b>"+ro[1]+"<BR>");			
				Response.Write("<b>"+"Add2   : "+"</b>"+ro[2]+"<BR>");			
				Response.Write("<b>"+"City   : "+"</b>"+ro[3]+"<BR>");			
				Response.Write("<b>"+"Zip    : "+"</b>"+ro[4]+"<BR>");			
				Response.Write("<b>"+"State  : "+"</b>"+ro[5]+"<BR>");			
				Response.Write("<b>"+"Country: "+"</b>"+ro[6]+"<BR>");
				Response.Write("<img src='/images/Edit1.gif' id='Edit"+ro[7]+"' name='Edit"+ro[7]+"' onclick='SetforEditing(this,this.form)'   alt='"+ro[7]+"'  ><BR><BR>");
				//Response.Write("<img src='/images/PrimaryForBilling.jpg' id='Bill"+ro[7]+"' onclick='setBilling(this,this.form)'  alt='"+ro[7]+"'  name='Bill"+ro[7]+"'><BR>");
				//Response.Write("<img src='/images/PrimaryForShipping1.gif' id='Ship"+ro[7]+"' onclick='setShipping(this,this.form)' alt='"+ro[7]+"' name='Ship"+ro[7]+"'><BR>");							
				Response.Write("</td>");
				// onclick='Server.Transfer(EditLocation.aspx?LocId="+ro[7]+")'
				//DataRow ro ;
				rr++;
			try
			{
				ro= DT.Rows[rr]; 			
				Response.Write("<td>");
				Response.Write("<input type=button value='Ship to This Address' name='"+ro[7]+"' onclick='OpenURL(this)' ><BR>");	
				Response.Write("<b>"+"Name   : "+"</b>"+ro[0]+"<BR>");			
				Response.Write("<b>"+"Add1   : "+"</b>"+ro[1]+"<BR>");			
				Response.Write("<b>"+"Add2   : "+"</b>"+ro[2]+"<BR>");			
				Response.Write("<b>"+"City   : "+"</b>"+ro[3]+"<BR>");			
				Response.Write("<b>"+"Zip    : "+"</b>"+ro[4]+"<BR>");			
				Response.Write("<b>"+"State  : "+"</b>"+ro[5]+"<BR>");			
				Response.Write("<b>"+"Country: "+"</b>"+ro[6]+"<BR>");
				Response.Write("<img src='/images/Edit1.gif' id='Edit"+ro[7]+"' name='Edit"+ro[7]+"' onclick='SetforEditing(this,this.form)'   alt='"+ro[7]+"'  ><BR><BR>");
				//Response.Write("<img src='/images/PrimaryForBilling.jpg' id='Bill"+ro[7]+"' onclick='setBilling(this,this.form)'  alt='"+ro[7]+"'  name='Bill"+ro[7]+"'><BR>");
				//Response.Write("<img src='/images/PrimaryForShipping1.gif' id='Ship"+ro[7]+"' onclick='setShipping(this,this.form)' alt='"+ro[7]+"' name='Ship"+ro[7]+"'><BR>");							
				Response.Write("</td>");
			}
			catch (IndexOutOfRangeException	e)
			{
			
			}
				Response.Write("</tr><tr><td align='center' colspan='2'>");
			
			}
				Response.Write("<input type=button value='New Address' onclick=window.open('NewLocation.aspx?PLAC_TYPE=D')> &nbsp");	
				Response.Write("<input type=button value='   Back    ' onclick ='history.back()'>&nbsp");	
				//Response.Write("<input type=button value='Continue'>");	
				
				Response.Write("</td></tr></form>");
				
			
		///###New Code addition begins here
		}
		else
		{
		string StrCmd;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();		 
			SqlCommand Cmd = new SqlCommand();
			SqlDataAdapter DA = new SqlDataAdapter() ;
			 
			DataTable DT = new DataTable();
			Cmd.CommandType=CommandType.Text;
			Cmd.Connection=conn;
			StrCmd="SELECT DISTINCT PLACE.PLAC_PERS, PLACE.PLAC_ADDR_ONE, PLACE.PLAC_ADDR_TWO, LOCALITY.LOCL_CITY, PLACE.PLAC_ZIP, STATE.STAT_NAME,";
			StrCmd+="COUNTRY.CTRY_NAME,PLACE.PLAC_ID, COMPANY.COMP_NAME  FROM  PLACE INNER JOIN  LOCALITY ON PLACE.PLAC_LOCL = LOCALITY.LOCL_ID AND PLACE.PLAC_LOCL = LOCALITY.LOCL_ID INNER JOIN ";
			StrCmd+="STATE ON LOCALITY.LOCL_STAT = STATE.STAT_CODE AND LOCALITY.LOCL_CTRY = STATE.STAT_CTRY INNER JOIN  COUNTRY ON LOCALITY.LOCL_CTRY = COUNTRY.CTRY_CODE ";
			StrCmd+="INNER JOIN COMPANY ON PLACE.PLAC_COMP = COMPANY.COMP_ID INNER JOIN  PERSON ON COMPANY.COMP_ID = PERSON.PERS_COMP Where PERSON.PERS_ID="+Session["ID"].ToString() + " And PLACE.PLAC_TYPE='H'" ;
			Cmd.CommandText=StrCmd;
			DA.SelectCommand= Cmd ;
			//DR = Cmd.ExecuteReader();
			DA.Fill(DT);
  
			Response.Write("<table width=750 cellpadding=0 rightmargin=0 cellspacing=0 align=left border=1 bordercolor=black>");
    		Response.Write("<INPUT TYPE='hidden' NAME='SId' onfocus='bc(this)'><INPUT TYPE='hidden' NAME='HId' onfocus='bc(this)'><INPUT TYPE='hidden' NAME='BId' onfocus='bc(this)'>");
		
		for (int rr=0; rr<=DT.Rows.Count-1; rr++     ) 
			{	
			 
				DataRow ro ;
				ro= DT.Rows[rr]; 			 
				Response.Write("<tr><td>");
				Response.Write("<input type=button value='Bill to This Address' name='"+ro[7]+"' onclick='OpenURLBill(this)' ><BR>");	
				Response.Write("<b>"+"Name   : "+"</b>"+ro[0]+"<BR>");			
				Response.Write("<b>"+"Add1   : "+"</b>"+ro[1]+"<BR>");			
				Response.Write("<b>"+"Add2   : "+"</b>"+ro[2]+"<BR>");			
				Response.Write("<b>"+"City   : "+"</b>"+ro[3]+"<BR>");			
				Response.Write("<b>"+"Zip    : "+"</b>"+ro[4]+"<BR>");			
				Response.Write("<b>"+"State  : "+"</b>"+ro[5]+"<BR>");			
				Response.Write("<b>"+"Country: "+"</b>"+ro[6]+"<BR>");
				Response.Write("<img src='/images/Edit1.gif' id='Edit"+ro[7]+"' name='Edit"+ro[7]+"' onclick='SetforEditing(this,this.form)'   alt='"+ro[7]+"'  ><BR><BR>");
				//Response.Write("<img src='/images/PrimaryForbILLING1.gif' id='Bill"+ro[7]+"' onclick='setBilling(this,this.form)'  alt='"+ro[7]+"'  name='Bill"+ro[7]+"'><BR>");
				//Response.Write("<img src='/images/PrimaryForShipping.jpg' id='Ship"+ro[7]+"' onclick='setShipping(this,this.form)' alt='"+ro[7]+"' name='Ship"+ro[7]+"'><BR>");							
				Response.Write("</td>");
				// onclick='Server.Transfer(EditLocation.aspx?LocId="+ro[7]+")'
				//DataRow ro ;
				rr++;
			try
			{
				ro= DT.Rows[rr]; 			
				Response.Write("<td>");
				Response.Write("<input type=button value='Bill to This Address' name='"+ro[7]+"' onclick='OpenURLBill(this)' ><BR>");	
				Response.Write("<b>"+"Name   : "+"</b>"+ro[0]+"<BR>");			
				Response.Write("<b>"+"Add1   : "+"</b>"+ro[1]+"<BR>");			
				Response.Write("<b>"+"Add2   : "+"</b>"+ro[2]+"<BR>");			
				Response.Write("<b>"+"City   : "+"</b>"+ro[3]+"<BR>");			
				Response.Write("<b>"+"Zip    : "+"</b>"+ro[4]+"<BR>");			
				Response.Write("<b>"+"State  : "+"</b>"+ro[5]+"<BR>");			
				Response.Write("<b>"+"Country: "+"</b>"+ro[6]+"<BR>");
				Response.Write("<img src='/images/Edit1.gif' id='Edit"+ro[7]+"' name='Edit"+ro[7]+"' onclick='SetforEditing(this,this.form)'   alt='"+ro[7]+"'  ><BR><BR>");
				//Response.Write("<img src='/images/PrimaryForbILLING1.gif' id='Bill"+ro[7]+"' onclick='setBilling(this,this.form)'  alt='"+ro[7]+"'  name='Bill"+ro[7]+"'><BR>");
				//Response.Write("<img src='/images/PrimaryForShipping.jpg' id='Ship"+ro[7]+"' onclick='setShipping(this,this.form)' alt='"+ro[7]+"' name='Ship"+ro[7]+"'><BR>");							
				Response.Write("</td>");
			}
			catch (IndexOutOfRangeException	e)
			{
			
			}
				Response.Write("</tr><tr><td align='center' colspan='2'>");
			
			}
				
				Response.Write("<input type=button value='New Address' onclick=window.open('NewLocation.aspx?PLAC_TYPE=H') > &nbsp");	
				Response.Write("<input type=button value='    Back   ' onclick='history.back()'>&nbsp");	
				//Response.Write("<input type=button value='Continue'>");	
				Response.Write("</td></tr></form>");
				
		}
		///###New Code addition ends here 
		%>
			</TABLE>
		<TPE:TEMPLATE id="Template2" Runat="Server" Footer="true"></TPE:TEMPLATE></form>
	</body>
</HTML>
