<%@ import Namespace="System.Web.Mail" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page Language="C#" %>
<script runat="server">

    ////////////////////////////////////////////////
    //Modified Latest version
    ////////////////////////////////////////////////
         string location;

          SqlConnection conn;
          SqlConnection connLocation;
          //string strId;// the id that is initially passed in the querystring
          //string ordertype;// offer or bid


          public void Page_Load(object sender, EventArgs e){

              if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "L"))
              {
                  Response.Redirect("/default.aspx");
              }
              conn = new SqlConnection(Application["DBconn"].ToString());
              conn.Open();
              connLocation = new SqlConnection(Application["DBconn"].ToString());
              connLocation.Open();
              // read querystring variables

              ViewState["strId"] = (string)Request.QueryString["Change"];
              ViewState["ordertype"] = (string)Request.QueryString["type"];
              //strId = (string)Request.QueryString["Change"];
              //ordertype=(string)Request.QueryString["type"];
              SqlCommand cmdCheck;
              SqlDataReader dtrCheck;
              SqlCommand cmdLocl;
              SqlDataReader dtrLocl;
			  SqlCommand cmdCompanies;
			  SqlDataReader  dtrCompanies;			  
			  if (!IsPostBack)
			  {            
			     detail.Attributes["OnClick"]="javascript:Address_PopUp()";//use the Attributes to set the javascript to the link control.
				 detail.Attributes["onmouseover"]="this.style.cursor='hand'";		
				 //get the companies' name and id,the companies must be the type of 'p','s','d' and one person at lest.
				 cmdCompanies = new SqlCommand("SELECT * FROM (SELECT COMP_ID,COMP_NAME,COMP_TYPE FROM COMPANY WHERE COMP_REG=1 AND COMP_TYPE IN ('P','S','D')AND EXISTS(SELECT PERS_ID FROM PERSON WHERE PERS_COMP=COMP_ID) )Q ORDER BY COMP_NAME",conn);
            dtrCompanies = cmdCompanies.ExecuteReader();
            //binding company dd list
            ddlCompany.Items.Add ( new ListItem ("Select Company","default" ) );
            while (dtrCompanies.Read()){
                ddlCompany.Items.Add ( new ListItem ((string) dtrCompanies["COMP_NAME"],dtrCompanies["COMP_ID"].ToString() ) );
            }
            dtrCompanies.Close();
			 //query the bid or offer info
                 if (ViewState["ordertype"].ToString()=="bid"){
                     cmdCheck = new SqlCommand("Select ISNULL(BID_PRCE_SELL-BID_PRCE,0) MARKUP,*, EXPR=convert(varchar, BID_EXPR, 101) FROM BBID WHERE BID_ID=@Id",conn);
                 }
                 else
				 {
                     cmdCheck = new SqlCommand("Select ISNULL(OFFR_PRCE-OFFR_PRCE_BUY,0.02) MARKUP,ISNULL(OFFR_PRCE_BUY, OFFR_PRCE-ISNULL(OFFR_PRCE-OFFR_PRCE_BUY,0.02)) OFFR_PRCE_BUY_REAL,*, EXPR=convert(varchar, OFFR_EXPR, 101) FROM BBOFFER WHERE OFFR_ID=@Id",conn);
                 }				 
                 cmdCheck.Parameters.Add("@Id",ViewState["strId"].ToString());
                 dtrCheck = cmdCheck.ExecuteReader();
                 dtrCheck.Read();
                 string size;
                 string product;
                 if (ViewState["ordertype"].ToString()=="bid"){

                     txtDetail.Text=dtrCheck["BID_DETL"].ToString();
                     txtQty.Text=dtrCheck["BID_QTY"].ToString();
                     txtMelt.Text = dtrCheck["BID_MELT"].ToString();
                     txtDensity.Text = dtrCheck["BID_DENS"].ToString();
                     txtAdds.Text = dtrCheck["BID_ADDS"].ToString();					
                     ViewState["CompId"]= dtrCheck["BID_COMP_ID"].ToString();
                     ViewState["PersId"]= dtrCheck["BID_PERS_ID"].ToString();
                     ViewState["Comp"]= dtrCheck["BID_COMP"].ToString();
					   //Broker ID
                     if(dtrCheck["BID_COMP_ID"]!= DBNull.Value){
                         ViewState["BrokerId"]= dtrCheck["BID_BROKER_ID"].ToString();
                     }else{
                         ViewState["BrokerId"]= "null";
                     }
					 if(dtrCheck["BID_TO"]!=DBNull.Value)
					 {
					     ViewState["ComPlace"]=dtrCheck["BID_TO"].ToString();
					 }else
					 {
					     ViewState["ComPlace"]="";
					 }
                     /*
                     if(dtrCheck["BID_COMP_ID"]!= DBNull.Value){
                         ViewState["CompId"]= dtrCheck["BID_COMP_ID"].ToString();
                     }else{
                         ViewState["CompId"]= "null";
                     }
                     if(dtrCheck["BID_PERS_ID"]!= DBNull.Value){
                         ViewState["PersId"]= dtrCheck["BID_PERS_ID"].ToString();
                     }else{
                         ViewState["PersId"]= "null";
                     }
                     if(dtrCheck["BID_COMP"]!= DBNull.Value){
                         ViewState["Comp"]= dtrCheck["BID_COMP"].ToString();
                     }else{
                         ViewState["Comp"]="null";
                     }
                     */
                     ddlTerms.Items.Add(new ListItem (dtrCheck["BID_TERM"].ToString(),dtrCheck["BID_TERM"].ToString()));
                     txtPrice.Text = dtrCheck["BID_PRCE"].ToString();
                     txtMarkup.Text = dtrCheck["MARKUP"].ToString();
                     //txtPrice.Text  = txtPrice.Text .Replace(".", "");
                     //ddlPayment.Items.Add(new ListItem (dtrCheck["BID_PAY"].ToString(),dtrCheck["BID_PAY"].ToString()));
					 ViewState["payMent"]=dtrCheck["BID_PAY"].ToString();			 
                     txtExpr.Text = dtrCheck["EXPR"].ToString();

                     size=dtrCheck["BID_SIZE"].ToString();
                     product=dtrCheck["BID_PROD"].ToString();

                     if(dtrCheck["BID_TO_LOCL_NAME"]!= DBNull.Value)
					 {
                         txtDelivery_Point.Text=dtrCheck["BID_TO_LOCL_NAME"].ToString();
                     }else
					 {
                         cmdLocl = new SqlCommand("SELECT LOCATION=LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID='"+dtrCheck["BID_TO_LOCL"].ToString()+"'",connLocation);
                         dtrLocl = cmdLocl.ExecuteReader();
                         dtrLocl.Read();
                         ddlDelivery_Point.Items.Add ( new ListItem ((string) dtrLocl["LOCATION"],dtrCheck["BID_TO"].ToString() ) );
                         dtrLocl.Close();
                         connLocation.Close();
                     }
                }
				else
				{//offer
				     if(dtrCheck["OFFR_FROM"]!=DBNull.Value)
					 {
					     ViewState["ComPlace"]=dtrCheck["OFFR_FROM"].ToString();
					 }else
					 {
					     ViewState["ComPlace"]="";
					 }					
                     txtDetail.Text=dtrCheck["OFFR_DETL"].ToString();
                     txtQty.Text=dtrCheck["OFFR_QTY"].ToString();
                     txtMelt.Text = dtrCheck["OFFR_MELT"].ToString();
                     txtDensity.Text = dtrCheck["OFFR_DENS"].ToString();
                     txtAdds.Text = dtrCheck["OFFR_ADDS"].ToString();
                     ViewState["CompId"]= dtrCheck["OFFR_COMP_ID"].ToString();
                     ViewState["PersId"]= dtrCheck["OFFR_PERS_ID"].ToString();
                     ViewState["Comp"]= dtrCheck["OFFR_COMP"].ToString();
					  //Broker ID
                     if(dtrCheck["OFFR_COMP_ID"]!= DBNull.Value){
                         ViewState["BrokerId"]= dtrCheck["OFFR_BROKER_ID"].ToString();
                     }else{
                         ViewState["BrokerId"]= "null";
                     }
                     ddlTerms.Items.Add(new ListItem (dtrCheck["OFFR_TERM"].ToString(),dtrCheck["OFFR_TERM"].ToString()));
                     txtPrice.Text = dtrCheck["OFFR_PRCE_BUY_REAL"].ToString();
                     txtMarkup.Text = dtrCheck["MARKUP"].ToString();
                     //txtPrice.Text  = txtPrice.Text .Replace(".", "");
                     //ddlPayment.Items.Add(new ListItem (dtrCheck["OFFR_PAY"].ToString(),dtrCheck["OFFR_PAY"].ToString()));
					 ViewState["payMent"]=dtrCheck["OFFR_PAY"].ToString();	
                     txtExpr.Text = dtrCheck["EXPR"].ToString();
                     size=dtrCheck["OFFR_SIZE"].ToString();
                     product=dtrCheck["OFFR_PROD"].ToString();
                     if(dtrCheck["OFFR_FROM_LOCL_NAME"]!= DBNull.Value)
					 {
                         txtDelivery_Point.Text=dtrCheck["OFFR_FROM_LOCL_NAME"].ToString();
                     }
					 else
					 {
                         cmdLocl = new SqlCommand("SELECT LOCATION=LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID='"+dtrCheck["OFFR_FROM_LOCL"].ToString()+"'",connLocation);
                         dtrLocl = cmdLocl.ExecuteReader();
                         if (dtrLocl.HasRows)
						 {
						    dtrLocl.Read();
                            ddlDelivery_Point.Items.Add ( new ListItem ((string) dtrLocl["LOCATION"],dtrCheck["OFFR_FROM"].ToString() ) );
                             dtrLocl.Close();
                         }
                         connLocation.Close();
                     }

                 }
				 //set the company which had been selected default and load user list to ddlUser.
				 if( ViewState["CompId"].ToString().Trim().Length>0)
				 {
				    ddlCompany.Items.FindByValue( ViewState["CompId"].ToString().Trim()).Selected=true;
					ddlUser.Visible=true;
					pnAltCompany.Visible=false;
					ddlUser.Items.Clear();
					SqlCommand cmdUsers;
                    SqlDataReader dtrUsers;		
					ddlDelivery_Point.Visible=true;	
					ddlDelivery_Point.Visible=true;							
					detail.Visible=true;										
                    cmdUsers = new SqlCommand("Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME as PERS_NAME,PERS_ID FROM PERSON WHERE PERS_COMP="+ddlCompany.SelectedItem.Value,connLocation);
					if(connLocation.State.ToString().Equals("Closed"))
					{
				        connLocation.Open();
				     }
                    dtrUsers = cmdUsers.ExecuteReader();					
                   // adding to control
				   ddlUser.Items.Clear();
                   while(dtrUsers.Read())
				   {
                        ddlUser.Items.Add ( new ListItem ((string) dtrUsers["PERS_NAME"],dtrUsers["PERS_ID"].ToString() ) );

                    }
					ddlUser.Items.FindByValue( ViewState["PersId"].ToString().Trim()).Selected=true;//ViewState["PersId"]
					dtrUsers.Close();
					connLocation.Close();
				 }
				 else
				 {
				     comMess.Visible=true;		
				     pnAltCompany.Visible=true;
				     ddlUser.Visible=false;
					 txtCompany.Text=ViewState["Comp"].ToString().Trim();
					 ddlDelivery_Point.Visible=false;
					 detail.Visible=false;
				 }
				
                 switch(size){
                     case "190000":
                     ddlQty.Items.Add(new ListItem ("Rail Car","190000"));
                     break;
                     case "2205":
                     ddlQty.Items.Add(new ListItem ("Metric Ton","2205"));
                     break;
                     case "42000":
                     ddlQty.Items.Add(new ListItem ("Truckload Boxes","42000"));
                     break;
                     case "44092":
                     ddlQty.Items.Add(new ListItem ("Truckload Bags","44092"));
                     break;
                     case "45000":
                     ddlQty.Items.Add(new ListItem ("Bulk Truck","45000"));
                     break;

                     case "1":
                     ddlQty.Items.Add(new ListItem ("Pounds (lbs)","1"));
                     break;
                     case "38500":
                     ddlQty.Items.Add(new ListItem ("20' Container","38500"));
                     break;
                     case "44500":
                     ddlQty.Items.Add(new ListItem ("40' Container","44500"));
                     break;
                 }
                 switch(product){
                     case "HMWPE - Film Grade":
                     ddlProduct.Items.Add("HMWPE - Film Grade");
                     break;
                     case "HDPE - inj":
                     ddlProduct.Items.Add("HDPE - inj");
                     break;
                     case "HDPE - Blow Mold":
                     ddlProduct.Items.Add("HDPE - Blow Mold");
                     break;
                     case "LDPE - Film":
                     ddlProduct.Items.Add("LDPE - Film");
                     break;
                     case "LDPE - inj":
                     ddlProduct.Items.Add("LDPE - inj");
                     break;

                     case "LLPDE - film":
                     ddlProduct.Items.Add("LLPDE - film");
                     break;
                     case "LLDPE - inj":
                     ddlProduct.Items.Add("LLDPE - inj");
                     break;
                     case "PP Homopolymer - inj":
                     ddlProduct.Items.Add("PP Homopolymer - inj");
                     break;
                     case "PP Copolymer - inj":
                     ddlProduct.Items.Add("PP Copolymer - inj");
                     break;
                     case "GPPS":
                     ddlProduct.Items.Add("GPPS");
                     break;
                     case "HIPS":
                     ddlProduct.Items.Add("HIPS");
                         break;
                     case "PET":
                         ddlProduct.Items.Add("PET");
                         break;
                     case "PVC":
                         ddlProduct.Items.Add("PVC");
                         break;;
                     default:
                        // alternate that the preset ones
                        radio_Product.Checked = false;
                        radio_Other.Checked = true;
                        txtProduct.Text = product;
                         break;
                 }



                  dtrCheck.Close();

             }

              if (!IsPostBack){

                  if (Request.QueryString["Export"] !=null){
                      ViewState["Export"] =true;
                  }else{
                      ViewState["Export"] =false;

                  }

                  //building the contract list
                  ddlProduct.Items.Add("HMWPE - Film Grade");
                  ddlProduct.Items.Add("HDPE - inj");
                  ddlProduct.Items.Add("HDPE - Blow Mold");
                  ddlProduct.Items.Add("LDPE - Film");
                  ddlProduct.Items.Add("LDPE - inj");
                  ddlProduct.Items.Add("LLPDE - film");
                  ddlProduct.Items.Add("LLDPE - inj");
                  ddlProduct.Items.Add("PP Homopolymer - inj");
                  ddlProduct.Items.Add("PP Copolymer - inj");
                  ddlProduct.Items.Add("GPPS");
                  ddlProduct.Items.Add("HIPS");
                  ddlProduct.Items.Add("PET");
                  ddlProduct.Items.Add("PVC");

                  // populate other info

                  if ((bool)ViewState["Export"]){
                  //make container available for export transaction
                  ddlQty.Items.Add(new ListItem ("20' Container","38500"));
                  ddlQty.Items.Add(new ListItem ("40' Container","44500"));

                  //extra payment terms for export transaction
                  ddlTerms.Items.Add(new ListItem ("CFR","CFR"));
                  ddlTerms.Items.Add(new ListItem ("CIF","CIF"));
                  ddlTerms.Items.Add(new ListItem ("FAS","FAS"));

                  //ddlPayment.Items.Add(new ListItem ("LC Sight","7"));
                  //ddlPayment.Items.Add(new ListItem ("CIA","0"));
                  //ddlPayment.Items.Add(new ListItem ("LC 30","30"));
                  //ddlPayment.Items.Add(new ListItem ("LC 60","60"));
                  //ddlPayment.Items.Add(new ListItem ("LC 90","90"));


                  }else{

                  //not an export transaction! different order size, terms
                  ddlQty.Items.Add(new ListItem ("Rail Car","190000"));
                  ddlQty.Items.Add(new ListItem ("Metric Ton","2205"));
                  ddlQty.Items.Add(new ListItem ("Truckload Boxes","42000"));
                  ddlQty.Items.Add(new ListItem ("Truckload Bags","44092"));
                  ddlQty.Items.Add(new ListItem ("Bulk Truck","45000"));
                  ddlQty.Items.Add(new ListItem ("Pounds (lbs)","1"));
                  ddlTerms.Items.Add(new ListItem ("FOB Delivered","FOB/Delivered"));
                  ddlTerms.Items.Add(new ListItem ("FOB Shipping","FOB/Shipping"));
                  //ddlPayment.Items.Add(new ListItem ("30","30"));
                  //ddlPayment.Items.Add(new ListItem ("Cash","0"));
                  //ddlPayment.Items.Add(new ListItem ("45","45"));
                  //ddlPayment.Items.Add(new ListItem ("60","60"));
                  //ddlPayment.Items.Add(new ListItem ("90","90"));
                  }
                  // binds the additional fields
				  //ddlPayment.Items.FindByValue( ViewState["payMent"].ToString().Trim()).Selected=true;
                  Bind_Info();
              }
          }
          // <summary>
          //  This function is a wrapper for calling the Bind Function
          //  All the controls that auto post back call this function
          // </summary>
		  public void ChangeCompany(object sender, EventArgs e)//when the company has been selected changed the user,location where the company lie changed also.
		  {
		       if(!ddlCompany.SelectedItem.Value.Equals("default"))
			   {

                   //change visible fields since we are working within a company
                    ddlDelivery_Point.Visible = true;
					ddlUser.Visible=false;
                    txtCompany.Text = "";
                    pnAltCompany.Visible = false;
                    ddlUser.Visible = true;
					comMess.Visible=false;
					detail.Visible=true;
                    // adding all the possible users within the company
                    SqlCommand cmdUsers;
                    SqlDataReader dtrUsers;					
                    cmdUsers = new SqlCommand("Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME as PERS_NAME,PERS_ID FROM PERSON WHERE PERS_COMP="+ddlCompany.SelectedItem.Value,conn);
                    dtrUsers = cmdUsers.ExecuteReader();
                   // adding to control
				   ddlUser.Items.Clear();
                   while(dtrUsers.Read())//get the users whoes is in the company selected.
				   {
                        ddlUser.Items.Add ( new ListItem ((string) dtrUsers["PERS_NAME"],dtrUsers["PERS_ID"].ToString() ) );

                    }
                  dtrUsers.Close();
            // adding all the possible locations within the company
                SqlCommand cmdLocations;
                SqlDataReader dtrLocations;
                cmdLocations = new SqlCommand("select PLAC_ID ,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) AS Location from place where PLAC_TYPE<>'H' AND plac_comp='"+ddlCompany.SelectedItem.Value+"'",conn);
                dtrLocations = cmdLocations.ExecuteReader();
				ddlDelivery_Point.Items.Clear();
                while(dtrLocations.Read())//get the location .
				{
                    ddlDelivery_Point.Items.Add ( new ListItem ((string) dtrLocations["LOCATION"],dtrLocations["PLAC_ID"].ToString() ) );
                }
                dtrLocations.Close();
                cmdLocations = new SqlCommand("select  PLAC_ID, LABEL = (select LOCL_CITY +', '+ LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) + ' ('+PLAC_LABL+')'  from place where plac_type='W'  ORDER BY LABEL",conn);
               dtrLocations = cmdLocations.ExecuteReader();
               while(dtrLocations.Read())
			   {
				    ddlDelivery_Point.Items.Add ( new ListItem (dtrLocations["LABEL"].ToString(),dtrLocations["PLAC_ID"].ToString() ) );
               }
			   dtrLocations.Close();
            } 
			else
			{
            // no company has been selected.  Therefore, only show the needed user
                ddlCompany.Visible = true;
				detail.Visible=false;
				pnAltCompany.Visible=true;
                ddlDelivery_Point.Visible = false;
                ddlUser.Visible = false;
				comMess.Text="Company:";
				comMess.Visible=true;
            }
		  }
		  public void PostBack(object sender, EventArgs e){

              Bind_Info();
          }



          // <summary>
          // Primary function for binding content to the controls
          // </summary>
          public void Bind_Info(){

             if (!IsPostBack)
			 {

                // not needed
                //ddlDelivery_Point.Items.Clear();
                // change the lables depending on the type of order
                if (ViewState["ordertype"].ToString()=="bid")
				{
                      WebBox1.Heading = "Change Bid";
                }
                else
				{
                      WebBox1.Heading = "Change Offer";
                }


               //adding all the possible locations within the company
                 SqlCommand cmdLocations;
                 SqlDataReader dtrLocations;
				 ddlDelivery_Point.Items.Clear();
                 if (ViewState["CompId"].ToString()!="")
				 {
                      cmdLocations = new SqlCommand("select PLAC_ID ,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) AS Location from place where PLAC_TYPE<>'H' AND plac_comp="+ViewState["CompId"].ToString()+"",conn);

                     dtrLocations = cmdLocations.ExecuteReader();
                     while(dtrLocations.Read())
				     {
                          ddlDelivery_Point.Items.Add ( new ListItem ((string) dtrLocations["LOCATION"],dtrLocations["PLAC_ID"].ToString() ) );
                     }
                  dtrLocations.Close();
                  }//add the warehouses!!
                 cmdLocations = new SqlCommand("select  PLAC_ID, PLAC_LABL, (select LOCL_CITY FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) AS CITY, (select LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) AS STATE  from place where plac_type='W'",conn);
                 dtrLocations = cmdLocations.ExecuteReader();
                 while(dtrLocations.Read()){

                     location=dtrLocations["CITY"].ToString();
                     location=location+", ";
                     location=location+dtrLocations["STATE"].ToString();
                     location=location+", ";
                     location=location+dtrLocations["PLAC_LABL"].ToString();
                     ddlDelivery_Point.Items.Add ( new ListItem (location,dtrLocations["PLAC_ID"].ToString() ) );
                 }
                 dtrLocations.Close();
				 if ((ViewState["ComPlace"].ToString().Trim()!="") && (ddlDelivery_Point.Items.FindByValue( ViewState["ComPlace"].ToString().Trim()) != null))
				 {
					 	
				     ddlDelivery_Point.Items.FindByValue( ViewState["ComPlace"].ToString().Trim()).Selected=true;
				 }
				 

             }
          }
          // <summary>
          //  Function enters the order into the system
          // </summary>
          public void Click_Cancel(object sender, EventArgs e){
              pnMain.Visible= true;
              pnConfirm.Visible = false;
          }
          // <summary>
          //  Creates and displays the confirmation screen
          // </summary>
           public void Click_Cancel2(object sender, EventArgs e){
                if (Request.QueryString["Screen"] != null){
                    if (Request.QueryString["Screen"].ToString().Equals("Dashboard") ){
                        Response.Redirect("/Research/Dashboard.aspx");
                    }else{
                        Response.Redirect("Spot_Floor.aspx?StartOn=" + ViewState["ordertype"].ToString() );
                    }
                }else{
                    Response.Redirect("Spot_Floor.aspx?StartOn=" + ViewState["ordertype"].ToString() );
                }


           }
          public void Click_Submit(object sender, EventArgs e){
              pnMain.Visible= false;
              string strText;
              WebBox1.Heading = "Confirm Order";

              // builds the confirmation string
              strText = " Your are going to place an order to ";


              // selects the user type

              if (Session["Typ"].ToString().Equals("P")){
                  strText += "Buy ";
              }else{
                  strText += "Sell ";
              }


              strText += txtQty.Text + " " + ddlQty.SelectedItem.ToString() + " of ";

              if (radio_Other.Checked){
                      strText += txtProduct.Text;
              }else{
                      strText +=ddlProduct.SelectedItem.ToString();
              }      
              strText +=" for $ " + txtPrice.Text +" per pound ";
			  strText +=" and markup of $ " + txtMarkup.Text +" per pound. ";
              
              if (!txtMelt.Text.Equals("")){
                  strText +="Melt: " + txtMelt.Text +". ";
              }
              if (!txtDensity.Text.Equals("")){
                  strText +="Density: " + txtDensity.Text +". ";
              }
              if (!txtAdds.Text.Equals("")){
                  strText +="Density: " +txtAdds.Text +". ";
              }
              strText += "Shipped from ";

              if (txtDelivery_Point.Text.Equals("")){
                  strText += ddlDelivery_Point.SelectedItem + ". ";
              }else{
                  strText += txtDelivery_Point.Text +". ";
              }
              strText +=" Expiration: In "+txtExpr.Text +". Terms: "+ddlTerms.SelectedItem.ToString()+".";
              // Payment: "+ddlPayment.SelectedItem+" days.
              lblConfirm.Text = strText;
              pnConfirm.Visible = true;
          }
          // <summary>
          //  Function enters the order into the system
          // </summary>


          // <summary>
          //  Function enters the order into the system
          // </summary>
          public void Save(object sender, EventArgs e){
                  string SQL;
                  SqlCommand cmdAddNew;  // stored proc adding record
                  cmdAddNew= new SqlCommand("spAddtoBB", conn);
                  cmdAddNew.CommandType = CommandType.StoredProcedure;
                  if (txtDelivery_Point.Text.Equals("") ){
                     cmdAddNew.Parameters.Add("@Firm","1"); // firm
                  }else{
                     cmdAddNew.Parameters.Add("@Firm","0"); // not firm
                  }
                  if (ViewState["ordertype"].ToString()=="bid"){
                     cmdAddNew.Parameters.Add("@Type","P");
                  }else{
                     cmdAddNew.Parameters.Add("@Type","S");
                  }

                 cmdAddNew.Parameters.Add("@Detail",txtDetail.Text);
                 cmdAddNew.Parameters.Add("@Qty",txtQty.Text);
                 cmdAddNew.Parameters.Add("@Size",Int32.Parse(ddlQty.SelectedItem.Value));
                 cmdAddNew.Parameters.Add("@Terms",ddlTerms.SelectedItem.Value);
                 cmdAddNew.Parameters.Add("@Payment","0");
                 //cmdAddNew.Parameters.Add("@Expire",txtExpr.Text);
                 cmdAddNew.Parameters.Add("@Price", txtPrice.Text);
                 cmdAddNew.Parameters.Add("@Markup", System.Convert.ToString((System.Convert.ToDouble(txtMarkup.Text)*100)));
                 // other product overwrite the dropdown list
                 if (radio_Other.Checked){
                      cmdAddNew.Parameters.Add("@Prod",txtProduct.Text);
                 }else{
                      cmdAddNew.Parameters.Add("@Prod",ddlProduct.SelectedItem.ToString());
                 }

                 if (txtDelivery_Point.Text.Equals("")){
                     // use the drop down location
                     SqlCommand cmdLocality;
                     // select the locality
                     cmdLocality = new SqlCommand("Select PLAC_LOCL FROM Place WHERE PLAC_ID='"+ddlDelivery_Point.SelectedItem.Value+"'",conn);
                     // add locality id
                     cmdAddNew.Parameters.Add("@Locl0",cmdLocality.ExecuteScalar().ToString());
                     // add place id
                     cmdAddNew.Parameters.Add("@Locl1",ddlDelivery_Point.SelectedItem.Value);
                     cmdAddNew.Parameters.Add("@Local",null);

                 }else{
                     // use the text box to manually enter the location
                     cmdAddNew.Parameters.Add("@Locl0",null);
                     cmdAddNew.Parameters.Add("@Local",txtDelivery_Point.Text.ToString());
                 }
                 if (!txtMelt.Text.Equals("")){
                     cmdAddNew.Parameters.Add("@Melt",txtMelt.Text);
                 }
                 if (!txtDensity.Text.Equals("")){
                     cmdAddNew.Parameters.Add("@Dens",txtDensity.Text);
                 }
                 if (!txtAdds.Text.Equals("")){
                     cmdAddNew.Parameters.Add("@Adds",txtAdds.Text);
                 }

                 cmdAddNew.Parameters.Add("@Ex",txtExpr.Text);
                 if(!ddlCompany.SelectedItem.Value.Equals("default")){
                     cmdAddNew.Parameters.Add("@Comp",null);
					 cmdAddNew.Parameters.Add("@Comp_Id",ddlCompany.SelectedItem.Value);
					 cmdAddNew.Parameters.Add("@Pers_id",ddlUser.SelectedItem.Value);					 
                 }
				 else
				 {
                     cmdAddNew.Parameters.Add("@Comp_Id",null);
					 cmdAddNew.Parameters.Add("@Pers_id",null);					 
					 cmdAddNew.Parameters.Add("@Comp",txtCompany.Text);				  				 
                 }  
				 if((ViewState["BrokerId"].ToString().Trim()=="null")||(ViewState["BrokerId"].ToString().Trim()=="0")){
                     cmdAddNew.Parameters.Add("@Broker",null);
                 }else{
                     cmdAddNew.Parameters.Add("@Broker",ViewState["BrokerId"].ToString());
                 }                 
                // Response.Write("Person Id"+ViewState["PersId"].ToString());
                // Response.Write("CompId" + ViewState["CompId"].ToString());
                // Response.Write("Comp" +ViewState["Comp"].ToString());

                 cmdAddNew.ExecuteNonQuery();
                 if (ViewState["ordertype"].ToString()=="bid"){
                     cmdAddNew= new SqlCommand("DELETE FROM BBID WHERE BID_ID="+ViewState["strId"].ToString()+"", conn);
                 }else{
                     cmdAddNew= new SqlCommand("DELETE FROM BBOFFER WHERE OFFR_ID="+ViewState["strId"].ToString()+"", conn);
                 }
                 cmdAddNew.ExecuteNonQuery();
                 pnMain.Visible= true;
                 pnConfirm.Visible = false;

                 if (Request.QueryString["Screen"] != null){
                    if (Request.QueryString["Screen"].ToString().Equals("Dashboard") ){
                        Response.Redirect("/Research/Dashboard.aspx");
                    }else{
                        Response.Redirect("Spot_Floor.aspx?StartOn=" + ViewState["ordertype"].ToString() );
                    }
                 }else{
                    Response.Redirect("Spot_Floor.aspx?StartOn=" + ViewState["ordertype"].ToString() );
                 }
                 conn.Close();
          }

</script>
<form runat="server" id="form1">
	<TPE:Template PageTitle="Order Entry" Runat="server" id="Template1" />
	<script language="javascript">
        <!--
        function Swap_Other(Flag)
        {
	       if(Flag)
	       {
		      document.form.ddlProduct.disabled=true;
		      document.form.txtProduct.disabled = false;
		      document.form.txtMelt.Value ='';
		      document.form.txtDensity.Value ='';
		      document.form.txtAdds.Value ='';

	       }
	       else
	       {
		      document.form.ddlProduct.disabled = false;
		      document.form.txtProduct.disabled = true;

	       }
        }

        function Address_PopUp()
        // This function opens up a pop up window and passes the appropiate ID number for the window
        {


        if (document.all)
            var xMax = screen.width, yMax = screen.height;
        else
            if (document.layers)
                var xMax = window.outerWidth, yMax = window.outerHeight;
            else
                var xMax = 640, yMax=480;

        var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
        var str = "../administrator/Location_Details.aspx?ID=" + document.form.ddlDelivery_Point.options[document.form.ddlDelivery_Point.selectedIndex].value;
        window.open(str,'Address','location=true,toolbar=true,width=275,height=370,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');

        }


    //-->
	</script>
	<!-- flag for the radio selection -->
	<TPE:Web_Box id="WebBox1" Heading="Change Offer" Runat="Server" />
	<asp:Panel id="pnMain" runat="server">
		<asp:Label id="lblMessage" runat="server"></asp:Label>
		<asp:ValidationSummary id="ValidationSummary1" runat="server" HeaderText="There are problems with the following fields. Please correct the errors below."></asp:ValidationSummary>
		<TABLE borderColor="black" cellSpacing="0" cellPadding="0" align="center" border="0">
			<TR>
				<TD>
					<TABLE height="40" cellSpacing="0" cellPadding="0" width="380" border="0">
						<TR>
							<TD class="s1" width="80">Company:<BR>
								<BR>
								<asp:Label id="comMess" runat="server"></asp:Label></TD>
							<TD width="150">
								<asp:DropDownList id="ddlCompany" runat="server" OnSelectedIndexChanged="ChangeCompany" AutoPostBack="true"></asp:DropDownList><BR>
								<asp:Panel id="pnAltCompany" runat="server">
									<asp:TextBox id="txtCompany" runat="server" size="20" maxlength="20" type="text"></asp:TextBox>
									<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ErrorMessage="Company cannot be blank"
										Display="none" ControlToValidate="txtCompany"></asp:RequiredFieldValidator>
								</asp:Panel><BR>
								<asp:DropDownList id="ddlUser" runat="server"></asp:DropDownList></TD>
							<TD width="150"></TD>
						</TR>
					</TABLE>
					<TABLE width="350">
						<TR>
							<TD class="S1">Product:
							</TD>
							<TD>
								<TABLE border="0">
									<TR>
										<TD>
											<asp:RadioButton id="radio_Product" onclick="JavaScript:Swap_Other(0)" runat="server" GroupName="RadioGroup1"
												checked="true"></asp:RadioButton></TD>
										<TD>
											<asp:DropDownList id="ddlProduct" runat="server"></asp:DropDownList></TD>
									</TR>
									<TR>
										<TD class="S1">
											<asp:RadioButton id="radio_Other" onclick="JavaScript:Swap_Other(1)" runat="server" GroupName="RadioGroup1"></asp:RadioButton></TD>
										<TD class="S1">
											<asp:TextBox id="txtProduct" runat="server"></asp:TextBox></TD>
									</TR>
								</TABLE>
							</TD>
						</TR>
						<TR>
							<TD class="S1">Details:
							</TD>
							<TD>
								<asp:TextBox id="txtDetail" runat="server" size="30" maxlength="75"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="S1" colSpan="2">Melt:
								<asp:TextBox id="txtMelt" runat="server" size="4" maxlength="16" type="text"></asp:TextBox>&nbsp;&nbsp;&nbsp;Density:
								<asp:TextBox id="txtDensity" runat="server" size="4" maxlength="16" type="text"></asp:TextBox>&nbsp;&nbsp;&nbsp;Adds:
								<asp:TextBox id="txtAdds" runat="server" size="4" maxlength="20" type="text"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="S1">Quantity:
							</TD>
							<TD>
								<asp:CompareValidator id="CompareValidator1" Runat="server" ErrorMessage="Quantity must be a number."
									Display="none" ControlToValidate="txtQty" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator>
								<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ErrorMessage="Quanity cannot be blank"
									Display="none" ControlToValidate="txtQty"></asp:RequiredFieldValidator>
								<asp:TextBox id="txtQty" runat="server" size="3" maxlength="5"></asp:TextBox>&nbsp;&nbsp;&nbsp;
								<asp:DropDownList id="ddlQty" runat="server"></asp:DropDownList></TD>
						</TR>
					</TABLE>
				</TD>
				<TD>
					<TABLE>
						<TR>
							<TD class="S1" vAlign="top">Dev. Pnt:
								<asp:Label id="lblPoint" runat="server"></asp:Label></TD>
							<TD><%if (Session["Typ"].ToString().Equals("P")  || Session["Typ"].ToString().Equals("S")){%>
								<asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" ErrorMessage="Delivery Point cannot be blank"
									Display="none" ControlToValidate="txtDelivery_Point"></asp:RequiredFieldValidator><%} %>
								<asp:TextBox id="txtDelivery_Point" runat="server"></asp:TextBox><BR>
								<asp:Panel id="pnDelivery_Point" runat="server">
<asp:DropDownList id="ddlDelivery_Point" runat="server"></asp:DropDownList><BR>
<asp:HyperLink id="detail" runat="server">Details</asp:HyperLink></A></asp:Panel></TD>
						</TR>
						<TR>
							<TD class="S1">Terms:
							</TD>
							<TD>
								<asp:DropDownList id="ddlTerms" runat="server"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD class="S1">
								<asp:Label id="lblPrice" runat="server"></asp:Label>
								<asp:Label id="lbltest" runat="server"></asp:Label>
								<asp:Label id="lbltest2" runat="server"></asp:Label>
								<asp:Label id="lbltest3" runat="server"></asp:Label>TPE Buy Price:
							</TD>
							<TD>
								<asp:CompareValidator id="CompareValidator2" Runat="server" ErrorMessage="Price must be a number." Display="none"
									ControlToValidate="txtPrice" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
								<asp:RangeValidator id="RangeValidator1" runat="server" type="Double" ErrorMessage="Price must be less than one dollar per pound."
									Display="none" ControlToValidate="txtPrice" MaximumValue=".9999" MinimumValue=".0001"></asp:RangeValidator>
								<asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" ErrorMessage="Price cannot be blank"
									Display="none" ControlToValidate="txtPrice"></asp:RequiredFieldValidator>
								<asp:TextBox id="txtPrice" runat="server" size="6" maxlength="6"></asp:TextBox>per 
								lb.
							</TD>
						</TR>
						<TR>
							<TD class="S1">Markup:</TD>
							<TD>
								<asp:CompareValidator id="CompareValidator4" Runat="server" ErrorMessage="Markup must be a number." Display="none"
									ControlToValidate="txtMarkup" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
								<asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server" ErrorMessage="Markup cannot be blank"
									Display="none" ControlToValidate="txtMarkup"></asp:RequiredFieldValidator>
								<asp:TextBox id="txtMarkup" runat="server" size="4" maxlength="6"></asp:TextBox>per 
								lb.</TD>
						</TR>
						<TR>
							<TD class="S1">
								<asp:Label id="lblExpires" runat="server"></asp:Label>Expires:
							</TD>
							<TD>
								<asp:TextBox id="txtExpr" runat="server"></asp:TextBox></TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
			<TR vAlign="middle" height="25">
				<TD colSpan="3">
					<CENTER>
						<asp:Button class="tpebutton" id="Button1" onclick="Save" runat="server" Text="Submit"></asp:Button>
						<asp:Button class="tpebutton" id="Button2" onclick="Click_Cancel2" runat="server" Text="Cancel"
							CausesValidation="false"></asp:Button></CENTER>
				</TD>
			</TR>
		</TABLE>
	</asp:Panel>
	<!--panel>
            <!--<asp:Panel id="pnConfirm" Visible="false" runat="server">

			         <table border=0 cellpadding=0 cellspacing=0 width=350>

				            <tr valign=top>
				                <td><br><br>
				                   <asp:Label id="lblConfirm" runat="server" />

					           </td>
				            </tr>
				            <tr>
					           <td colspan=7>
					             <center><br><br>
                                 <asp:Button runat="server" id="btnSubmit" class="tpebutton" onclick="Save" Text="Submit" />
                                 &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                 <asp:Button runat="server" id="btnCancel" class="tpebutton" onclick="Click_Cancel" Text="Cancel" />
                                 </center>
					           </td>
				            </tr>

			         </table>

            <br><br>





        </asp:Panel>-->
	<TPE:Web_Box Footer="true" Runat="server" id="Web_Box1" />
	<br>
	<br>
	<TPE:Template Footer="true" Runat="server" id="Template2" />
</form>
