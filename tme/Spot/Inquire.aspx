
<%@ Page language="c#" Codebehind="Inquire.aspx.cs" AutoEventWireup="True" Inherits="localhost.Spot.Inquire" MasterPageFile="~/MasterPages/Menu.Master" Title="Spot Offer Inquiry"%>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphCssLink">
<style type="text/css">BODY { MARGIN: 0px; BACKGROUND-COLOR: #ffffff }
	#menu { Z-INDEX: 1; LEFT: 678px; VISIBILITY: hidden; WIDTH: 222px; POSITION: absolute; TOP: 127px; HEIGHT: 24px }    
</style>
</asp:Content>


 
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">

<asp:panel id="pnMain" runat="server">
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
  <tr>
    <td>
      <table id="Table3" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
        <tr>
          <td valign="top">
              <table class="Content">
                  <tr>
                      <td> 
                          <br>
                          <br>
                          &nbsp; 
                          <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="There are problems with the following fields. Please correct the errors below."
                              ForeColor=" " CssClass="ErrorText"></asp:ValidationSummary>
                          <span id="Span8" style="font-weight: bold">
                              <asp:Label ID="Label11" runat="server" Font-Bold="True">If you have any question contact us by phone at</asp:Label></span>
                          <asp:Label ID="Label1" runat="server" ForeColor="Red" Font-Bold="True"><%=ConfigurationSettings.AppSettings["PhoneNumber"].ToString()%></asp:Label>
                          <asp:Label ID="Label12" runat="server" Font-Bold="True">or ask below.</asp:Label><br>
                          <br>
                          <br>
                      </td>
                  </tr>
              </table>
              <div runat="server" id="divDetails" class="DivTitleBar Header Bold Color1">Offer details</div>
              <table id="Table10" cellspacing="0" cellpadding="0" width="100%" border="0" class="Content" bgcolor="#333333">
              <tr>
                  <td>
                      <table id="Table18" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                          <tr>
                              <td>
                                  <table id="Table20" height="255" cellspacing="0" cellpadding="0" width="100%" align="center"
                                      border="0">
                                      <tr>
                                          <td align="center" width="380">
                                              <table id="Table21" height="240" cellspacing="1" cellpadding="0" width="393" border="0">
                                                  <tr>
                                                      <td class="blackbold" align="right" width="122" bgcolor="#dbdcd7" height="20">
                                                          Order ID:
                                                      </td>
                                                      <td class="blackbold" align="left" width="260" bgcolor="#999999">
                                                          &nbsp;
                                                          <asp:Label ID="lblOrderID" runat="server" Font-Bold="True"></asp:Label></td>
                                                  </tr>
                                                  <tr>
                                                      <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                          Produtct:</td>
                                                      <td class="blackbold" align="left" bgcolor="#999999">
                                                          &nbsp;
                                                          <asp:Label ID="lblProduct" runat="server" Font-Bold="True"></asp:Label></td>
                                                  </tr>
                                                  <tr>
                                                      <td class="blackbold" align="right" bgcolor="#dbdcd7" height="22">
                                                          Size:</td>
                                                      <td class="blackbold" align="left" bgcolor="#999999" height="22">
                                                          &nbsp;
                                                          <asp:DropDownList ID="ddlNumRC" CssClass="InputForm" runat="server" Visible="false"
                                                              AutoPostBack="True" OnSelectedIndexChanged="ddlNumRC_SelectedIndexChanged">
                                                          </asp:DropDownList>
                                                          <asp:Label ID="lblRailCars" runat="server" Visible="false">Rail Cars</asp:Label>
                                                          <asp:Label ID="lblSize" runat="server" Font-Bold="True"></asp:Label></td>
                                                  </tr>
                                                  <tr>
                                                      <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                          Melt:</td>
                                                      <td class="blackbold" align="left" bgcolor="#999999">
                                                          &nbsp;
                                                          <asp:Label ID="lblMelt" runat="server" Font-Bold="True"></asp:Label></td>
                                                  </tr>
                                                  <tr>
                                                      <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                          Density:</td>
                                                      <td class="blackbold" align="left" bgcolor="#999999">
                                                          &nbsp;
                                                          <asp:Label ID="lblDens" runat="server" Font-Bold="True"></asp:Label></td>
                                                  </tr>
                                                  <tr>
                                                      <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                          Adds:</td>
                                                      <td class="blackbold" align="left" bgcolor="#999999">
                                                          &nbsp;
                                                          <asp:Label ID="lblAdds" runat="server" Font-Bold="True"></asp:Label></td>
                                                  </tr>
                                                  <tr>
                                                      <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                          Delivery Terms:
                                                      </td>
                                                      <td class="blackbold" align="left" bgcolor="#999999">
                                                          &nbsp;
                                                          <asp:Label ID="lblTerms" runat="server" Font-Bold="True"></asp:Label></td>
                                                  </tr>
                                                  <tr>
                                                      <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                          Price:</td>
                                                      <td class="blackbold" align="left" bgcolor="#999999">
                                                          &nbsp;
                                                          <asp:Label ID="lblPrice" runat="server" Font-Bold="True"></asp:Label></td>
                                                  </tr>
                                                  <tr>
                                                      <td class="blackbold" align="right" bgcolor="#dbdcd7" height="60px">
                                                          Freight:</td>
                                                      <td valign="top" bgcolor="#999999" colspan="1" align="left">
                                                      <table border="0"><tr><td>
                                                          <asp:Panel ID="pnlFreight" runat="server" Width="248px" Height="60px">
                                                              <table id="Table1" cellspacing="0" cellpadding="1" border="0">
                                                                  <tr>
                                                                      <td>
                                                                          &nbsp;
                                                                          <asp:Label ID="lblZipCode" runat="server" Font-Bold="True" Font-Size="X-Small">Zip Code:</asp:Label>
                                                                          <asp:TextBox ID="txtZipCode" runat="server" CssClass="InputForm" Width="70px" MaxLength="5"></asp:TextBox>
                                                                          <asp:Label ID="lblCityPort" runat="server" ForeColor="Black" Font-Bold="True" Font-Size="X-Small">Port:</asp:Label>
                                                                          <asp:DropDownList ID="ddlCitiesPorts" CssClass="InputForm" runat="server">
                                                                          </asp:DropDownList>
                                                                      </td>
                                                                   </tr>
                                                                   <tr>
                                                                      <td align="center">
                                                                          <asp:ImageButton ID="btnUpdate" runat="server" ImageUrl="/images/buttons/update_unpress_bkgrey.jpg"
                                                                              CausesValidation="False">
                                                                          </asp:ImageButton>
                                                                      </td>
                                                                  </tr>                                                                      
                                                              </table>
                                                          </asp:Panel>
                                                          </td>
                                                          </tr>
                                                          <tr>
                                                          <td>
                                                          <asp:Label ID="lblFreight" CssClass="Content" runat="server" Font-Bold="True" Font-Size="X-Small">
                                                          </asp:Label>
                                                          </td>
                                                          </tr>
                                                          </table>
                                                     </td>
                                                  </tr>
                                                  <tr>
                                                      <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                          Total Price:
                                                      </td>
                                                      <td class="blackbold" align="left" bgcolor="#999999">
                                                          &nbsp;
                                                          <asp:Label ID="lblTotalPrice" runat="server" Font-Bold="True"></asp:Label></td>
                                                  </tr>
                                                  <tr>
                                                      <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                          Payment Terms:
                                                      </td>
                                                      <td class="blackbold" align="left" bgcolor="#999999">
                                                          &nbsp;
                                                          <asp:Label ID="Label20" runat="server" Font-Bold="True">net 30 days from Shipment</asp:Label></td>
                                                  </tr>
                                              </table>
                                          </td>
                                          <td valign="middle" align="center" width="380">
                                              <table id="Table23" cellspacing="1" cellpadding="0" border="0">
                                                  <asp:Panel ID="pnlBuyOnline" runat="server" Visible="True">
                                                      <tbody>
                                                          <tr>
                                                              <td class="blackbold" valign="middle" align="center" background="/images2/inquire/banners_backgrounds_r5_c1.jpg"
                                                                  height="80">
                                                                  Ready to buy?<br>
                                                                  <a onmouseover="MM_swapImage('Image19','','/images2/inquire/clickhere_press.jpg',1)"
                                                                      onmouseout="MM_swapImgRestore()" href="#"></a>
                                                                  <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="/images/buttons/clickhere_unpress.jpg"
                                                                      CausesValidation="False"></asp:ImageButton></td>
                                                          </tr>
                                                  </asp:Panel>
                                                  <tr>
                                                      <td class="blackbold" valign="middle" align="center" background="/images2/inquire/banners_backgrounds_r3_c1.jpg"
                                                          height="80">
                                                          <asp:Label ID="Label14" runat="server" Font-Bold="True">Search </asp:Label>
                                                          <asp:Label ID="lblTotalLbs" runat="server" Font-Bold="True">32,000,000</asp:Label>
                                                          <asp:Label ID="lblMessage1" runat="server" Font-Bold="True">lbs currently offered:</asp:Label><strong><font
                                                              face="Arial" color="#ff0000" size="2">
                                                              <asp:ImageButton ID="hlpMessage1" runat="server" ImageUrl="/images/buttons/spotfloor_unpress.jpg">
                                                              </asp:ImageButton></font></strong></td>
                                                  </tr>
                                                  <tr>
                                                      <td class="blackbold" valign="middle" align="center" background="/images2/inquire/banners_backgrounds_r5_c1.jpg"
                                                          height="80">
                                                          Give us the details!
                                                          <asp:Label ID="lblMessage2" runat="server" Font-Bold="True"> We will find it for you! </asp:Label><a
                                                              onmouseover="MM_swapImage('Image21','','/images2/inquire/request_press.jpg',1)"
                                                              onmouseout="MM_swapImgRestore()" href="#"></a><br>
                                                          <strong><font face="Arial" color="#ff0000" size="2">
                                                              <asp:ImageButton ID="hlpMessage2" runat="server" ForeColor="Red" ImageUrl="/images2/inquire/request_unpress.jpg" OnClick="hlpMessage2_Click"></asp:ImageButton></font></strong></td>
                                                  </tr>
                                              </table>
                                          </td>
                                      </tr>
                                  </table>
                                  <table id="Table24" cellspacing="0" cellpadding="2" width="100%" align="center" border="0">
                                      <tr>
                                          <td class="Color4" align="center">
                                              Click <a style="color: #f3c300" href="javascript:void window.open('Agreement.aspx', 'Agreement', 'toolbar=false, scrollbars=1')">
                                                  here</a> to see Conditions of Sales Agreement
                                          </td>
                                      </tr>
                                  </table>
                                  <div class="DivTitleBar Header Bold Color1">
                                      Inquire about this offer</div>
                                  <table class="Content" id="Table26" height="160" cellspacing="0" cellpadding="0"
                                      width="778" align="center" border="0">
                                      <tr>
                                          <td valign="middle" align="center" width="236">
                                             
                                                  <table id="Table22" cellspacing="1" cellpadding="1" width="300" border="0">
                                                      <tr>
                                                          <td>
                                                              <span id="Span30" style="color: #616c9a"><strong><font color="#000000">
                                                                  <asp:Label ID="lbName" runat="server" ForeColor="White" Font-Bold="True" Font-Size="X-Small">Name:</asp:Label></font></strong></span></td>
                                                          <td>
                                                              <asp:TextBox ID="txtName" CssClass="InputForm" runat="server" Width="184px"></asp:TextBox></td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              <span id="Span31" style="color: #616c9a"><strong><font color="#000000">
                                                                  <asp:Label ID="lbCompany" runat="server" ForeColor="White" Font-Bold="True" Font-Size="X-Small">Company:</asp:Label></font></strong></span></td>
                                                          <td>
                                                              <asp:TextBox ID="txtCompany" CssClass="InputForm" runat="server" Width="184px"></asp:TextBox></td>
                                                      </tr>
                                                      <tr>
                                                          <td width="80">
                                                              <span style="color: #616c9a"><strong><font color="#000000">
                                                                  <asp:Label ID="lbEmail" runat="server" ForeColor="White" Font-Bold="True" Font-Size="X-Small">Email:</asp:Label></font></strong></span></td>
                                                          <td>
                                                              <asp:TextBox ID="txtEmail" CssClass="InputForm" runat="server" Width="184px"></asp:TextBox>
                                                              <asp:Literal ID="ltLivePerson" runat="server"></asp:Literal></td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              <span id="Span33" style="color: #616c9a"><strong><font color="#000000">
                                                                  <asp:Label ID="lbPhone" runat="server" ForeColor="White" Font-Bold="True" Font-Size="X-Small">Phone:</asp:Label></font></strong></span></td>
                                                          <td>
                                                              <asp:TextBox ID="txtPhone" CssClass="InputForm" runat="server" Width="184px"></asp:TextBox></td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              <span id="Span32" style="color: #616c9a"><strong><font color="#000000">
                                                                  <asp:Label ID="lblLocation" runat="server" ForeColor="White" Font-Bold="True" Font-Size="X-Small">Location:</asp:Label></font></strong></span></td>
                                                          <td>
                                                              <asp:TextBox ID="txtLocation" CssClass="InputForm" runat="server" Width="184px"></asp:TextBox></td>
                                                      </tr>
                                                  </table>
                                              
                                          </td>
                                          <td width="1">
                                              <img height="160" src="/images2/inquire/yellow_divisor.jpg" width="1"></td>
                                          <td align="center" width="529">
                                              <label>
                                                  &nbsp;
                                                  <asp:Label class="Color4" ID="Label4" runat="server" Font-Bold="True">The more details the better</asp:Label><br>
                                                  <asp:TextBox ID="txtComments" runat="server" CssClass="InputForm" Width="450px" Height="78px"
                                                      TextMode="MultiLine" Rows="5" Columns="55"></asp:TextBox><br>
                                                  <br>
                                                  <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="/images2/inquire/submit_unpress.jpg">
                                                  </asp:ImageButton></label></td>
                                      </tr>
                                  </table>
                                  <asp:RequiredFieldValidator ID="rvf1" runat="server" ForeColor="#C00000" ControlToValidate="txtName"
                                      Display="none" ErrorMessage=" Name can not be blank."></asp:RequiredFieldValidator>
                                  <asp:RequiredFieldValidator ID="rvf2" runat="server" ForeColor="#C00000" ControlToValidate="txtEmail"
                                      Display="none" ErrorMessage="Email can not be blank."></asp:RequiredFieldValidator>
                                  <asp:RequiredFieldValidator ID="rvf3" runat="server" ForeColor="#C00000" ControlToValidate="txtPhone"
                                      Display="none" ErrorMessage="Phone can not be blank."></asp:RequiredFieldValidator>
                                  <asp:RequiredFieldValidator ID="rtf4" runat="server" ForeColor="#C00000" ControlToValidate="txtCompany"
                                      Display="none" ErrorMessage="Company can not be blank."></asp:RequiredFieldValidator></td>
                          </tr>
                      </table>
                  </td>
              </tr>
              <tr>
              </tr>
              </table>
              <tr>
              </tr>
      </table>
    </td>
  </tr>
</table>
</asp:Panel>
    <asp:Panel ID="pnThanks" runat="server" Height="200px" Visible="False">

        <center>
            <br>
            &nbsp;</center>
        <center>
            <br>
            <span class="Content"><b>Thank you. We will be contacting you shortly.</b></span></center>
        <br>
        <br>
        <table border="0" align="center" width="100%">
            <tr>
                <td valign="top" align="center" colspan="3">
                    <hr>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center" colspan="3">
                    <font face="arial" color="black" size="1">710 North Dearborn</font>&nbsp;&nbsp;<font
                        face="arial" color="red" size="1">Chicago, IL 60610</font>&nbsp;&nbsp;<font face="arial"
                            color="black" size="1"><b>tel
                                <%=ConfigurationSettings.AppSettings["PhoneNumber"].ToString() %>
                            </b></font>&nbsp;&nbsp;<font face="arial" color="red" size="1">fax
                                <%=ConfigurationSettings.AppSettings["FaxNumber"].ToString() %>
                            </font>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnError" runat="server" Visible="false">
        <p>
            <br />
            <br />
            Sorry, this offer has been filled. Enter a <a href="/spot/Resin_Entry.aspx">Resin Request</a>
            that we resource it for you.</p>
        <p>
            Please <a href="mailto:webmaster@ThePlasticsExchange.com">contact us</a> if you
            feel that you have reached this message in error.<br>
            <br />
            <br />
        </p>
        <center>
            <a href="/default.aspx">Return to the homepage</a></center>
        <p>
    </asp:Panel>
    <asp:Panel ID="pnThankYou" runat="server" Visible="False">
        <table id="Table11112" cellspacing="0" cellpadding="0" width="100%" border="0" class="Content">
            <tbody>
                <tr>
                    <td>
                        <center>
                            <p>
                                <br>
                                <br>
                                <b>Thank you for your Inquiry. We will respond asap.</b></p>
                            &nbsp;&nbsp;
                        </center>
                        <asp:Label ID="lblDateTitle" runat="server" Visible="True"></asp:Label>
                        <div runat="server" id="div1" class="DivTitleBar Header Bold Color1">Offer details</div>
                        <table id="Table16" cellspacing="1" cellpadding="1" width="778" align="center" border="0">
                            <tr>
                                <td valign="middle" width="433">
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" width="433">
                                    <table id="Table4" height="240" cellspacing="1" cellpadding="0" width="393" border="0">
                                        <tr>
                                            <td class="blackbold" align="right" width="122" bgcolor="#dbdcd7" height="20">
                                                Order ID:
                                            </td>
                                            <td class="blackbold" align="left" width="260" bgcolor="#999999">
                                                &nbsp;
                                                <asp:Label ID="lblOrderID_t" runat="server" Font-Bold="True"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                Product:</td>
                                            <td class="blackbold" align="left" bgcolor="#999999">
                                                &nbsp;
                                                <asp:Label ID="lblProduct_t" runat="server" Font-Bold="True"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                Size:</td>
                                            <td class="blackbold" align="left" bgcolor="#999999">
                                                &nbsp;
                                                <asp:DropDownList ID="ddlNumRC_t" runat="server" CssClass="InputForm" Visible="false"
                                                    AutoPostBack="True">
                                                </asp:DropDownList>
                                                <asp:Label ID="lblRailCars_t" runat="server" Visible="false">Rail Cars</asp:Label>
                                                <asp:Label ID="lblSize_t" runat="server" Font-Bold="True"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                Melt:</td>
                                            <td class="blackbold" align="left" bgcolor="#999999">
                                                &nbsp;
                                                <asp:Label ID="lblMelt_t" runat="server" Font-Bold="True"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                Density:</td>
                                            <td class="blackbold" align="left" bgcolor="#999999">
                                                &nbsp;
                                                <asp:Label ID="lblDensity_t" runat="server" Font-Bold="True"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                Adds:</td>
                                            <td class="blackbold" align="left" bgcolor="#999999">
                                                &nbsp;
                                                <asp:Label ID="lblAdds_t" runat="server" Font-Bold="True"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                Delivery Terms:
                                            </td>
                                            <td class="blackbold" align="left" bgcolor="#999999">
                                                &nbsp;
                                                <asp:Label ID="lblTerms_t" runat="server" Font-Bold="True"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                Price:</td>
                                            <td class="blackbold" align="left" bgcolor="#999999">
                                                &nbsp;
                                                <asp:Label ID="lblPrice_t" runat="server" Font-Bold="True"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                Total Price:
                                            </td>
                                            <td class="blackbold" align="left" bgcolor="#999999">
                                                &nbsp;
                                                <asp:Label ID="lblTotalPrice_t" runat="server" Font-Bold="True"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="blackbold" align="right" bgcolor="#dbdcd7" height="20">
                                                Payment Terms:
                                            </td>
                                            <td class="blackbold" align="left" bgcolor="#999999">
                                                &nbsp;
                                                <asp:Label ID="Label21" runat="server" Font-Bold="True">net 30 days from Shipment</asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    Click <a class="LinkNormal" href="javascript:void window.open('Agreement.aspx', 'Agreement', 'toolbar=false, scrollbars=1')">
                                        here</a> to see Conditions of Sales Agreement
                                </td>
                            </tr>
                        </table>
                        <p>
                        </p>
                        <p>
                        </p>
                        <center>
                            <a class="LinkNormal" href="/default.aspx">Return to the homepage</a></center>
                        <center>
                            &nbsp;</center>
                        <center>
                            <a class="LinkNormal" href="/Public/Registration.aspx?Referrer=default.aspx">Go to the
                                sign up screen</a></center>
                        <center>
                            &nbsp;</center>
                        <table width="100%" align="center">
                            <tr>
                                <td valign="top" align="center" colspan="3">
                                    <hr>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center" colspan="3">
                                    <font face="arial" color="black" size="1">710 North Dearborn</font>&nbsp;&nbsp;<font
                                        face="arial" color="red" size="1">Chicago, IL 60610</font>&nbsp;&nbsp;<font face="arial"
                                            color="black" size="1"><b>tel
                                                <%=ConfigurationSettings.AppSettings["PhoneNumber"].ToString() %>
                                            </b></font>&nbsp;&nbsp;<font face="arial" color="red" size="1">fax
                                                <%=ConfigurationSettings.AppSettings["FaxNumber"].ToString() %>
                                            </font>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>
</asp:Content>
