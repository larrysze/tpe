<%@ Control Language="c#" AutoEventWireup="True" Codebehind="SpotGrid.ascx.cs" Inherits="localhost.Spot.SpotGrid"
    TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
    <tr>
        <td>
            <table height="55" cellspacing="0" cellpadding="0" width="100%" align="center" bgcolor="#000000"
                border="0">
                <tr>
                    <td valign="top" align="right">
                        <asp:Panel ID="pnTopBar" runat="server">
                            <table cellspacing="3" cellpadding="0" width="100%" border="0" align="right">
                                <tr>
                                    <td >&nbsp;
                                    </td>
                                    <td class="Content Color4" align="right" width="117">
                                        Filter by Product:</td>
                                    <td style="height: 20px">
                                    </td>
                                    <td class="Content Color4" align="right" width="50">
                                        &nbsp;
                                        <asp:Label ID="lblSize" runat="server"> Size: </asp:Label></td>
                                    <td width="13px" height="20px" >&nbsp;
                                    </td>
                                    <td class="Content Color4" style="width: 260px; height: 20px" align="center">
                                        Search:</td>
                                    <td valign="bottom" align="left" rowspan="2">
                                        <asp:ImageButton ID="imbFind" runat="server" ImageUrl="/images/buttons/find_unpress.jpg">
                                        </asp:ImageButton>&nbsp;</td>
                                    <td align="left">
                                    </td>
                                </tr>
                                <tr>

                                    <td>
                                        <asp:DropDownList CssClass="InputForm" ID="ddlCompanies" runat="server" AutoPostBack="True"
                                            OnSelectedIndexChanged="Change_Company">
                                        </asp:DropDownList></td>
                                    <td align="center">
                                        <asp:DropDownList CssClass="InputForm" ID="ddlResinType" runat="server" AutoPostBack="True"
                                            OnSelectedIndexChanged="Change_Resin">
                                        </asp:DropDownList></td>
                                    <td>
                                    </td>
                                    <td align="center">
                                        <asp:DropDownList CssClass="InputForm" ID="ddlSize" runat="server" AutoPostBack="True"
                                            OnSelectedIndexChanged="Change_Size">
                                        </asp:DropDownList></td>
                                    <td width="13" >&nbsp;
                                    </td>
                                    <td align="center">
                                        <asp:TextBox CssClass="InputForm" ID="txtSearch" runat="server" Height="14px" Width="152px"></asp:TextBox>&nbsp;&nbsp;</td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <tr>
                            <td>
                                <asp:Button CssClass="Content Color2" ID="btnEmailOffers" runat="server" Text="Email Offers"
                                    OnClick="Click_Send_Mass_Mail"></asp:Button><asp:Button CssClass="Content Color2"
                                        ID="btnArchive" runat="server" Text="Move to Archive" OnClick="Click_Move_To_Archive">
                                    </asp:Button><asp:Button CssClass="Content Color2" ID="btnDelete" runat="server"
                                        Text="Delete" OnClick="Click_Delete"></asp:Button><asp:Button CssClass="Content Color2"
                                            ID="btnHot" runat="server" Text="Make Offer Hot" OnClick="Click_MakeHot"></asp:Button><asp:Button
                                                CssClass="Content Color2" ID="btnCool" runat="server" Text="Cool Offer" OnClick="Click_MakeCool">
                                            </asp:Button></td>
                        </tr>
            </table>
            <br />
            <asp:Panel ID="pnlFreightBat" runat="server">
                <table height="28" cellspacing="0" cellpadding="0" width="100%" align="center" background="/images2/dashboard/orange_bar.jpg"
                    border="0">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblMessageFreight" runat="server" CssClass="Content Color2 Bold">Compare prices by including freight costs!&nbsp;</asp:Label></td>
                        <td align="right">
                            <asp:Label ID="lblZipCode" runat="server" Width="80px" CssClass="Content Color2 Bold">Zip Code:</asp:Label></td>
                        <td style="width: 3px">
                            <asp:TextBox ID="txtZipCode" runat="server" CssClass="Content Color2 Bold" Width="70px"
                                Height="14px" MaxLength="5" Font-Size="XX-Small"></asp:TextBox></td>
                        <td align="right">
                            <asp:Label ID="lblCityPort" runat="server" Width="60px" CssClass="Content Color2 Bold">or City:</asp:Label></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlCitiesPorts" runat="server" CssClass="InputForm">
                            </asp:DropDownList></td>
                        <td align="center" width="76px">
                            <asp:ImageButton ID="imbUpdate" runat="server" ImageUrl="/images2/dashboard/update_red.jpg">
                            </asp:ImageButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <table cellspacing="0" cellpadding="0" width="100%" align="center" bgcolor="#000000"
                border="0">
                <tr>
                    <td height="5">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:DataGrid BorderWidth="0" BackColor="#000000" CellSpacing="1" ID="dgAdmin" runat="server"
                Width="100%" ShowFooter="True" HorizontalAlign="Center" DataKeyField="VARID"
                AllowSorting="True" AutoGenerateColumns="False" OnSelectedIndexChanged="dgAdmin_SelectedIndexChanged">
                <FooterStyle CssClass="Content Bold Color4 FooterColor"></FooterStyle>
                <AlternatingItemStyle CssClass="LinkNormal Color2 DarkGray" />
                <ItemStyle CssClass="LinkNormal Color2 LightGray"></ItemStyle>
                <HeaderStyle CssClass="LinkNormal Color2 OrangeColor"></HeaderStyle>
                <Columns>
                    <mbrsc:RowSelectorColumn AllowSelectAll="True">
                    </mbrsc:RowSelectorColumn>
                    <asp:TemplateColumn>
                        <ItemStyle CssClass="Content"></ItemStyle>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <ItemTemplate>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="VARID" SortExpression="VARID ASC" HeaderText="ID #">
                        <ItemStyle HorizontalAlign="Right" Width="40px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="VARCONTRACT" SortExpression="VARCONTRACT ASC" HeaderText="Product">
                        <ItemStyle Wrap="False" Width="206px" HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="VARSIZE" SortExpression="VARSIZE ASC" HeaderText="Size">
                        <ItemStyle Wrap="False" HorizontalAlign="Right" Width="100px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="VARMELT" SortExpression="VARMELT ASC" HeaderText="Melt">
                        <ItemStyle HorizontalAlign="Right" Width="60px" CssClass="Content"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="VARDENS" SortExpression="VARDENS ASC" HeaderText="Density">
                        <HeaderStyle></HeaderStyle>
                        <ItemStyle HorizontalAlign="Right" Width="60px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="VARADDS" SortExpression="VARADDS ASC" HeaderText="Adds/Izod">
                        <ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="VARTERM" SortExpression="VARTERM ASC" HeaderText="Location">
                        <ItemStyle Wrap="False" HorizontalAlign="Right" Width="110px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="VARUSR" SortExpression="VARUSR ASC" HeaderText="Company">
                        <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="VARPRICE" SortExpression="VARPRICE ASC" HeaderText="Price"
                        DataFormatString="{0:c}">
                        <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="VARPRICE2" SortExpression="VARPRICE2 ASC" HeaderText="Price2"
                        DataFormatString="{0:c}">
                        <ItemStyle Font-Bold="True" Wrap="False" HorizontalAlign="Right"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="VARFREIGHT" SortExpression="VARFREIGHT ASC" HeaderText="Freight">
                        <ItemStyle></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="VARVALUE" SortExpression="VARVALUE ASC" HeaderText="Value"
                        DataFormatString="${0:#,###}">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="VARBIDOFFER" SortExpression="VARBIDOFFER ASC" HeaderText="BID/OFFER DATE">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn Visible="False" DataField="VARFREIGHTPOINT" HeaderText="FreightPoint">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:TemplateColumn>
                        <ItemStyle></ItemStyle>
                        <ItemTemplate>
                            <asp:Image ID="imgUser" runat="server" ImageUrl="../Pics/icons/user.jpg"></asp:Image>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn Visible="False" DataField="VARUSRID" HeaderText="UserID">
                        <ItemStyle></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn Visible="False" DataField="VARBROKERID" HeaderText="BrokerID">
                        <ItemStyle></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn Visible="False" DataField="VARQUALITY" HeaderText="Quality">
                        <ItemStyle></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn Visible="True" DataField="VARDELIVERYMONTH" SortExpression="VARDELIVERYMONTH DESC" HeaderText="Delivery Month">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:BoundColumn>
                    
                </Columns>
            </asp:DataGrid><asp:Label ID="lblGridEmpy" runat="server" CssClass="normalcontent"
                Visible="False">We�re sorry, no records were found that match your criteria</asp:Label></td>
    </tr>
</table>
