<%@ Page language="c#" Codebehind="OrderConfirmation.aspx.cs" AutoEventWireup="false" Inherits="PlasticsExchange.OrderConfirmation" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>OrderConfirmation</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TPE:TEMPLATE id="Template1" Runat="Server" PageTitle="Order Confirmation"></TPE:TEMPLATE>
			<TABLE id="Table11" style="WIDTH: 736px; HEIGHT: 600px" height="28" width="736" border="0">
				<TBODY>
					<TR>
						<TD class="ListHeadlineBold" style="WIDTH: 700px;HEIGHT: 30px" align="center"><asp:label id="lblTitle" runat="server">Order Confirmation</asp:label></TD>
					</TR>
					<TR>
						<TD vAlign="top" align="left">
							<TABLE id="Table12" style="WIDTH: 735px; HEIGHT: 243px" height="243" width="735" border="0">
								<tr>
									<td align="left" colSpan="7"><asp:linkbutton id="LinkButton1" runat="server" ForeColor="Red" Font-Size="X-Small" Height="16px"
											Width="580px">Click here for 'PDF Credit Application' </asp:linkbutton></td>
								</tr>
								<TR>
									<TD align="left" colSpan="7"><FONT color="blue" size="3"><B>Order Summary </B></FONT>
									</TD>
								</TR>
								<TR>
									<TD style="WIDTH: 62px" align="left"><B>ID#</B></TD>
									<TD style="WIDTH: 158px" align="left"><B>Product</B></TD>
									<TD style="WIDTH: 91px" align="left"><B>Size</B></TD>
									<TD align="left"><B>Melt</B></FONT></TD>
									<TD align="left"><B>Density</B></TD>
									<TD align="left"><B>Adds</B></TD>
									<TD align="left"><B>Terms</B></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 62px; HEIGHT: 18px" align="left"><asp:label id="lblID" runat="server"></asp:label></TD>
									<TD style="WIDTH: 158px; HEIGHT: 18px" align="left"><asp:label id="lblProduct" runat="server"></asp:label></TD>
									<TD style="WIDTH: 91px; HEIGHT: 18px" align="left"><asp:label id="lblSize" runat="server"></asp:label></TD>
									<TD style="HEIGHT: 18px" align="left"><asp:label id="lblMelt" runat="server"></asp:label></TD>
									<TD style="HEIGHT: 18px" align="left"><asp:label id="lblDens" runat="server"></asp:label></TD>
									<TD style="HEIGHT: 18px" align="left"><asp:label id="lblAdds" runat="server"></asp:label></TD>
									<TD style="HEIGHT: 18px" align="left"><asp:label id="lblTerms" runat="server"></asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 62px; HEIGHT: 111px" align="left"></TD>
									<TD style="WIDTH: 158px; HEIGHT: 111px" align="left"></TD>
									<TD style="WIDTH: 91px; HEIGHT: 111px" align="left"></TD>
									<TD style="HEIGHT: 111px" align="left"></TD>
									<TD style="HEIGHT: 111px" align="left"></TD>
									<TD style="HEIGHT: 111px" align="left"><FONT color="blue" size="2"><b>TOTAL:</b></FONT></TD>
									<TD style="HEIGHT: 111px" align="left"><b><asp:label id="lblTotal" runat="server"></asp:label></b></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 62px" align="left"></TD>
									<TD style="WIDTH: 158px; HEIGHT: 13px" align="left"></TD>
									<TD style="WIDTH: 91px; HEIGHT: 13px" align="left"></TD>
									<TD style="HEIGHT: 13px" align="left"></TD>
									<TD style="HEIGHT: 13px" align="left"></TD>
									<TD style="HEIGHT: 13px" align="left"><b>Shipping to:</b></TD>
									<TD style="HEIGHT: 13px" align="left"><b>Bill to:</b></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 62px" align="left"></TD>
									<TD style="WIDTH: 158px; HEIGHT: 13px" align="left"></TD>
									<TD style="WIDTH: 91px; HEIGHT: 13px" align="left"></TD>
									<TD style="HEIGHT: 13px" align="left"></TD>
									<TD style="HEIGHT: 13px" align="left"></TD>
									<TD style="HEIGHT: 13px" align="left"><FONT size="1">
											<p><asp:label id="LblShip" runat="server" Width="121px"></asp:label>
										</FONT></P></TD>
									<TD style="HEIGHT: 13px" align="left"><FONT size="1">
											<p><asp:label id="LblBill" runat="server" Width="121px"></asp:label>
										</FONT></P></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2">
							<asp:CheckBox id="cbAgreement" runat="server"></asp:CheckBox>I read and agree 
							with <A href="javascript:window.open('Agreement.aspx', toolbar=false)">Conditions 
								of Sales Agreement</A>
						</TD>
					<TR>
						<TD align="center" class="ListHeadlineBold" style="WIDTH: 700px;HEIGHT: 30px"><asp:button id="btnBackTo" runat="server" Text="  Back  "></asp:button>&nbsp;
							<asp:button id="btnSave" runat="server" Text="Confirm"></asp:button>
						</TD>
					</TR>
				</TBODY>
			</TABLE>
			&nbsp;
			<TPE:TEMPLATE id="Template2" Runat="Server" Footer="true"></TPE:TEMPLATE></form>
	</body>
</HTML>
