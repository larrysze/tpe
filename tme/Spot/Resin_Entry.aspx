<%@ Page Language="c#" Codebehind="Resin_Entry.aspx.cs" AutoEventWireup="True" Inherits="localhost.Spot.Resin_Entry"
    MasterPageFile="~/MasterPages/Template.Master" Title="Order Entry" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="asp" namespace="System.Web.UI" %>

<%@ MasterType VirtualPath="~/MasterPages/Template.Master" %>
<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
    <style type="text/css">
        .LeftAlign {text-align:left;}
    </style>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphJavaScript">

    <script type="text/javascript" language="JavaScript">
		if(document.all && !document.getElementById) 
		{
			document.getElementById = function(id) { 
				return document.all[id]; 
			}
		}

		function Address_PopUp_Details()
		// This function opens up a pop up window and passes the appropiate ID number for the window
		{
			if (document.all)
				var xMax = screen.width, yMax = screen.height;
			else
				if (document.layers)
					var xMax = window.outerWidth, yMax = window.outerHeight;
				else
					var xMax = 640, yMax=480;

			var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
			if (document.Form1.ddlDeliveryPoint.options[document.Form1.ddlDeliveryPoint.selectedIndex].value!="0")
			{
				var str = "../administrator/Location_Details.aspx?ID=" + document.Form1.ddlDeliveryPoint.options[document.Form1.ddlDeliveryPoint.selectedIndex].value;
				window.open(str,'Address','location=true,toolbar=true,width=275,height=370,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');
			}
		}
    </script>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphInstructions">

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphHeading">
    <span class="Header Bold Color1">Request Monomer</span></asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
<asp:ScriptManager runat="server"></asp:ScriptManager>

    <asp:UpdatePanel runat="server">
     <ContentTemplate> 
     
    <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
        <tr>
            <td valign="top">
                <table height="1" cellspacing="0" cellpadding="0" width="100%" bgcolor="#fed400"
                    border="0">
                    <tr>
                        <td id="td2">
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlMain" runat="server">
                    <asp:Label ID="lblMarketMode" runat="server" Visible="False"></asp:Label><asp:Label
                        ID="lblID" runat="server" Visible="False"></asp:Label></asp:Panel>
                <table id="Table1" width="100%" cellspacing="10" border="0" bgcolor="#000000">
                    <tr>
                        <td valign="top" align="center" rowspan="2">
                            <asp:Image ID="imgDomestic" runat="server" ImageUrl="/Pics/icons/icon_US_Flag.jpg"></asp:Image><asp:Image
                                ID="imgInternational" runat="server" ImageUrl="/Pics/icons/icon_Globe.gif" Visible="False">
                            </asp:Image></td>
                        <td nowrap>
                            <asp:Label ID="lblMarket" runat="server" Font-Bold="True" CssClass="Header Bold Color4">U.S. Domestic Market</asp:Label></td>
                        <td class="LinkNormal Color4">                     
                        </td>
                        <td width="300">
                        </td>
                    </tr>
                </table>
                <table height="25" cellspacing="0" cellpadding="0" width="780" border="0">
                    <tr>
                        <td class="Content Color2 Bold">
                            Resin Details</td>
                    </tr>
                </table>
                <table cellspacing="1" cellpadding="0" width="575" bgcolor="000000" align="center">
                    <asp:Panel ID="pnlTypeofEntry" runat="server">
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                <asp:Label ID="lblTypeEntry" runat="server">Type of Entry:</asp:Label></td>
                            <td class="Content Color2" align="left" bgcolor="#9a9b96">
                                <asp:DropDownList ID="ddlTypeEntry" runat="server" AutoPostBack="true" CssClass="InputForm"
                                    OnSelectedIndexChanged="ddlTypeEntry_SelectedIndexChanged">
                                </asp:DropDownList></td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                            <asp:Label ID="lblProduct" runat="server">Product Grade:</asp:Label>
                        </td>
                        <td class="Content Color2" height="29" nowrap bgcolor="#9a9b96" align="left">
                            <asp:DropDownList CssClass="InputForm" ID="ddlProduct" runat="server"  AutoPostBack="True" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" />
                            <%--<asp:Label ID="Label2" runat="server" CssClass="Content Color2 Bold">OR <span class="Content Color2">enter other</SPAN></asp:Label>
                            <asp:TextBox CssClass="InputForm" onkeypress="return noenter()" ID="txtProduct" runat="server"></asp:TextBox>--%>
                            <asp:CustomValidator ID="cmvProduct" runat="server" ErrorMessage="*"></asp:CustomValidator>
                            <asp:CustomValidator ID="cmvProduct2" runat="server" ErrorMessage="*" Display="None"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                            <asp:Label ID="lblDetails" runat="server" Font-Bold="True">Additional Details:</asp:Label>
                        </td>
                        <td class="Content Color2" align="left" width="200" height="29" valign="middle" bgcolor="#9a9b96">
                            <asp:TextBox onkeypress="return noenter()" ID="txtDetail" runat="server" MaxLength="75"
                                size="62" CssClass="InputForm"></asp:TextBox>
                        </td>
                    </tr>
                    
                   
                    <tr>
                        <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                            <asp:Label ID="lblQuantity" runat="server">Amount:</asp:Label></td>
                        <td bgcolor="#9a9b96">
                            <table border="0" width="400">
                                <tr align="left">
                                    <td align="left" class="Content Color2" width="30">
                                        Qty:</td>
                                    <td align="left" width="170">
                                    </td>
                                    <td align="left" class="Content Color2" width="150">
                                        Units:</td>
                                </tr>
                                <tr>
                                    <td align="left" class="Content Color2" width="30">
                                  <asp:DropDownList runat="server" onkeypress="return noenter()" ID="txtQty" runat="server" CssClass="InputForm">
                                        <asp:ListItem Selected="True" Text="1" /> 
                                        <asp:ListItem Text="2" />
                                        <asp:ListItem Text="3" />
                                        <asp:ListItem Text="4" />
                                        <asp:ListItem Text="5" />
                                        <asp:ListItem Text="6" />
                                        <asp:ListItem Text="7" />
                                        <asp:ListItem Text="8" />
                                        <asp:ListItem Text="9" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="11" />
                                        <asp:ListItem Text="12" />
                                        <asp:ListItem Text="13" />
                                        <asp:ListItem Text="14" />
                                        <asp:ListItem Text="15" />
                                        <asp:ListItem Text="16" />
                                        <asp:ListItem Text="17" />
                                        <asp:ListItem Text="18" />
                                        <asp:ListItem Text="19" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="21" />
                                        <asp:ListItem Text="22" />
                                        <asp:ListItem Text="23" />
                                        <asp:ListItem Text="24" />
                                        <asp:ListItem Text="25" />
                                        <asp:ListItem Text="26" />
                                        <asp:ListItem Text="27" />
                                        <asp:ListItem Text="28" />
                                        <asp:ListItem Text="29" />
                                        <asp:ListItem Text="30" />
                                        <asp:ListItem Text="31" />
                                        <asp:ListItem Text="32" />
                                        <asp:ListItem Text="33" />
                                        <asp:ListItem Text="34" />
                                        <asp:ListItem Text="36" />
                                        <asp:ListItem Text="37" />
                                        <asp:ListItem Text="38" />
                                        <asp:ListItem Text="39" />
                                        <asp:ListItem Text="40" />                                                                                
                                        <asp:ListItem Text="41" />
                                        <asp:ListItem Text="42" />
                                        <asp:ListItem Text="43" />
                                        <asp:ListItem Text="44" />
                                        <asp:ListItem Text="45" />
                                        <asp:ListItem Text="46" />
                                        <asp:ListItem Text="47" />
                                        <asp:ListItem Text="48" />
                                        <asp:ListItem Text="49" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                    
                                        <%--<asp:TextBox onkeypress="return noenter()" ID="txtQty" runat="server" MaxLength="8" size="8" CssClass="InputForm" />--%>
                                        &nbsp;<asp:RequiredFieldValidator ID="rfvQuantity"
                                                runat="server" ErrorMessage="*" ControlToValidate="txtQty"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" width="170" nowrap>
                                        <asp:CustomValidator ID="cvQuantity" runat="server" ErrorMessage="Invalid quantity"
                                            ControlToValidate="txtQty" Width="170"></asp:CustomValidator></td>
                                    <td align="left" class="Content Color2" width="150">
                                        <asp:DropDownList ID="ddlQty" runat="server" CssClass="InputForm" />                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <asp:Panel ID="pnlDeliveryTerms" runat="server" Visible="True">
                        <tr>
                            <td class="Content Color2 Bold" align="right" bgcolor="#dbdcd7" height="29">
                                <asp:Label ID="lblTerms" runat="server">Delivery Terms:</asp:Label>
                            </td>
                            <td align="left" class="Content Color2" bgcolor="#9a9b96">
                                <asp:DropDownList ID="ddlTerms" runat="server" CssClass="InputForm">
                                </asp:DropDownList></td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                            <asp:Label ID="lblPoint" runat="server">Shipping Point:</asp:Label></td>
                        <td bgcolor="#9a9b96">
                            <table border="0">
                                <tr>
                                    <td class="Content Color2" width="80">
                                        &nbsp;</td>
                                    <td class="Content Color2" width="20">
                                        <asp:Label ID="lblShippingPointOr" runat="server"><b>OR</b></asp:Label></td>
                                    <td class="Content Color2" width="140">
                                        <asp:Label ID="lblShippingPointDomestic" runat="server">Select a City:</asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="Content Color2">
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td align="left" class="Content Color2">
                                        <br />
                                        <asp:DropDownList ID="ddlDeliveryPoint" runat="server" CssClass="InputForm">
                                        </asp:DropDownList><asp:CustomValidator ID="cmvShipingPoint" runat="server" ErrorMessage="Please enter a valid shipping point."></asp:CustomValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    <asp:Panel ID="pnlDeliveryMonth" runat="server" Visible="true">
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                <asp:Label ID="Label1" runat="server">Delivery Month:</asp:Label>
                            </td>
                             <td class="Content Color2" bgcolor="#9a9b96" align="left"> 
                                                     
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="InputForm LeftAlign" BackColor="#9a9b96" BorderWidth="0">
                                    <asp:ListItem Value="3">3 Month Strip</asp:ListItem>
                                    <asp:ListItem Value="6">6 Month Strip</asp:ListItem>
                                </asp:RadioButtonList>                                                     
                                                     
                                <asp:ListBox ID="lstDeliveryMonth" runat="server" SelectionMode="Multiple" CssClass="InputForm" Height="90">
                                    <asp:ListItem Selected="True" Value="April 2007" />
                                    <asp:ListItem Value="May 2007" />
                                    <asp:ListItem Value="June 2007" />
                                    <asp:ListItem Value="July 2007" />
                                    <asp:ListItem Value="August 2007" />
                                    <asp:ListItem Value="September 2007" />
                                    <asp:ListItem Value="October 2007" />
                                    <asp:ListItem Value="November 2007" />
                                    <asp:ListItem Value="December 2007" />
                                    <asp:ListItem Value="January 2008" />
                                    <asp:ListItem Value="February 2008" />
                                    <asp:ListItem Value="March 2008" />
                                </asp:ListBox>                                
                                <br />
                                    
                                    <asp:Label ID="Label5" runat="server" Font-Size="8">Multi-select message will come here.</asp:Label>
                                                                
                            </td>
                        </tr>                   
                    </asp:Panel>                    
                    <asp:Panel ID="pnlBuyPrice" runat="server" Visible="true">
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                <asp:Label ID="lblPrice" runat="server">TPE Buy Price:</asp:Label></td>
                            <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                <asp:Label ID="lblPricePrefix" runat="server">$0. </asp:Label>
                                <asp:TextBox onkeypress="return noenter()" ID="txtPrice" runat="server" MaxLength="8" size="14" CssClass="InputForm"></asp:TextBox>
                                <asp:Label ID="lblPriceMeasure" runat="server">per lb.</asp:Label>
                                <asp:RequiredFieldValidator ID="rfvPrice" runat="server" ErrorMessage="*" ControlToValidate="txtPrice"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cpvPrice" runat="server" ErrorMessage="Price must be a number."
                                    ControlToValidate="txtPrice" Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                            </td>
                        </tr>
                    </asp:Panel>
                    
                    <asp:Panel ID="pnlCommision" runat="server" Visible="true">
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                <asp:Label ID="Label3" runat="server">Commision:</asp:Label>
                            </td>
                            <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                <asp:Label ID="Label4" runat="server">$0. </asp:Label>
                                <asp:TextBox onkeypress="return noenter()" ID="txtCommision" runat="server" MaxLength="17" size="25" CssClass="InputForm" />
                                <asp:Label ID="lblCommMeasure" runat="server">per lb.</asp:Label>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Price must be a number."
                                    ControlToValidate="txtCommision" Operator="DataTypeCheck" Type="Integer" />
                            </td>
                        </tr>
                    </asp:Panel>
                    
<%--                    <asp:Panel ID="pnlMarkup" runat="server" Visible="false">
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                <asp:Label ID="lblMarkup" runat="server">Markup</asp:Label></td>
                            <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                <asp:Label ID="lblMarkupPrefix" runat="server">$0. </asp:Label>
                                <asp:TextBox onkeypress="return noenter()" ID="txtMarkup" runat="server" MaxLength="4"
                                    size="4" CssClass="InputForm">02</asp:TextBox>
                                <asp:Label ID="lblMarkupMeasure" runat="server">per lb.</asp:Label>
                                <asp:CompareValidator ID="cpvMarkup" runat="server" ErrorMessage="Markup must be a number."
                                    ControlToValidate="txtMarkup" Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                                <asp:RequiredFieldValidator ID="rfvMarkup" runat="server" ErrorMessage="*" ControlToValidate="txtMarkup"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </asp:Panel>--%>
                    <asp:Panel ID="pnlExpiration" runat="server">
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                <asp:Label ID="lblExpiration" runat="server">Offer Expiration:</asp:Label></td>
                            <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                <asp:DropDownList ID="ddlExpires" runat="server" CssClass="InputForm">
                                </asp:DropDownList></td>
                        </tr>
                    </asp:Panel>
                    
                    <asp:Panel id="pnlTransType" runat="server" Visible="true">
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                <asp:Label ID="lblMarketLabel" runat="server">Transaction Type:</asp:Label>
                             </td> 
                                <td class="Content Color2" style="background-color: #9a9b96" align="left">
                                <asp:RadioButtonList ID="rblTrasType" runat="server" CssClass="InputForm LeftAlign"
                                    BackColor="#9a9b96" BorderWidth="0">
                                    <asp:ListItem Value="F">Financial</asp:ListItem>
                                    <asp:ListItem Value="P" Selected="True">Physical</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>                                
                        </tr>                                            
                    </asp:Panel>
                    
                    <asp:Panel ID="pnlPleaseWait" runat="server" Visible="true">
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29" colspan="2">                                    
                                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                    <ProgressTemplate>  
                                        <asp:Image ID="imgPleaseWait" runat="server" ImageUrl="http://www.theplasticexchange.com/images/progressbar_green.gif" />
                                        <br />
                                        <asp:Label ID="lblPleaseWait" runat="server" Font-Names="Arial" Font-Size="12px" Text="Communicating With Server..." />                                
                                    </ProgressTemplate>        
                                </asp:UpdateProgress>                                            
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="pnlSubmitAction" runat="server" Visible="false">
                        <tr>
                            <td class="Content Color2 Bold" nowrap align="right" width="135" bgcolor="#dbdcd7"
                                height="29">
                                <asp:Label ID="lblSubmitAction" runat="server">Submit Action</asp:Label></td>
                            <td class="Content Color2" align="left" bgcolor="#9a9b96">
                                <asp:RadioButton ID="rdbNew" runat="server" Text="Save as a new Entry, archiving this one.  "
                                    Checked="True" GroupName="Save"></asp:RadioButton><br />
                                <asp:RadioButton ID="rdbEdit" runat="server" Text="Save as the same Entry." GroupName="Save">
                                </asp:RadioButton></td>
                        </tr>
                    </asp:Panel>
                </table>
                <br>
                <asp:Panel ID="pnlContactDetails" runat="server">
                    <table height="25" cellspacing="0" cellpadding="0" width="780" border="0">
                        <tr>
                            <td class="Content Color2 Bold">
                                Contact Details</td>
                        </tr>
                    </table>
                    <table cellspacing="1" cellpadding="5" width="575" bgcolor="#000000" border="0" align="center">
                        <asp:Panel ID="pnlSelectCompany" runat="server" Visible="false">
                            <tr>
                                <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                    <asp:Label ID="lblCompany" runat="server" Font-Bold="True">Company:</asp:Label>
                                </td>
                                <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                    <asp:DropDownList ID="ddlCompanies" runat="server" AutoPostBack="true" CssClass="InputForm"
                                        OnSelectedIndexChanged="ddlCompanies_SelectedIndexChanged">
                                    </asp:DropDownList>&nbsp;
                                    <asp:DropDownList ID="ddlUser" runat="server" Visible="False" CssClass="InputForm"
                                        OnSelectedIndexChanged="ddlUser_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <div align="center">
                                        <asp:Label ID="lblUserEditUser" CssClass="LinkNormal" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;<asp:Label
                                            ID="lblUserAddUser" CssClass="LinkNormal" runat="server"></asp:Label></div>
                                    <b>OR</b> enter company
                                    <asp:TextBox onkeypress="return noenter()" ID="txtCompany" runat="server" Width="190px"
                                        CssClass="InputForm"></asp:TextBox>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlNonLoggedContactInfo" runat="server" Visible="false">
                            <tr>
                                <td class="Content Color2 Bold" valign="middle" width="135" align="right" bgcolor="#dbdcd7">
                                    <asp:Label ID="lblName" runat="server">Name:</asp:Label></td>
                                <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                    <asp:TextBox ID="txtName" runat="server" Width="200px" CssClass="InputForm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="*" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="Content Color2 Bold" valign="middle" align="right" bgcolor="#dbdcd7">
                                    <asp:Label ID="lblEmail" runat="server">E-mail:</asp:Label></td>
                                <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                    <asp:TextBox ID="txtEmail" runat="server" Width="200px" CssClass="InputForm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="*" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="Content Color2 Bold" valign="middle" align="right" bgcolor="#dbdcd7">
                                    <asp:Label ID="lblPhone" runat="server" Font-Bold="True">Phone:</asp:Label></td>
                                <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                    <asp:TextBox ID="txtPhone" runat="server" Width="150px" CssClass="InputForm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ErrorMessage="*" ControlToValidate="txtPhone"></asp:RequiredFieldValidator></td>
                            </tr>
                        </asp:Panel>
                    </table>
                </asp:Panel>
                &nbsp;&nbsp;
                <p align="center">
                    <br>
                    <asp:ImageButton ID="cmdSubmit" ImageUrl="../images/buttons/submit_unpressed.jpg"
                        runat="server"></asp:ImageButton></p>
                <br />
                <asp:Panel ID="pnlConfirmation" runat="server">
                    <table>
                        <tr>
                            <td align="center" colspan="43">
                                <asp:Label ID="lblMessage" runat="server" Width="550px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="cmdNewEntry" runat="server" Visible="False" Text="Enter a new one"
                                    OnClick="cmdNewEntry_Click"></asp:Button>
                                <asp:Button ID="cmdNo" runat="server" CausesValidation="False" Text="Back" Width="125px"
                                    OnClick="cmdNo_Click"></asp:Button>&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="cmdYes" runat="server" Text="Continue" Width="125px" OnClick="cmdYes_Click">
                                </asp:Button>
                                <asp:Button ID="cmdSpotFloor" runat="server" Visible="False" Text="Take me to the Spot Floor"
                                    OnClick="cmdSpotFloor_Click"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    
    </ContentTemplate>    
    </asp:UpdatePanel>

</asp:Content>
