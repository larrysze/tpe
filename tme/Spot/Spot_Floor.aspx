<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Spot_Floor.aspx.cs" Inherits="localhost.Spot._Spot_Floor" MasterPageFile="~/MasterPages/Template.Master" Title="Spot Floor"%>
<%@ MasterType virtualPath="~/MasterPages/Template.Master"%>
<%@ Register TagPrefix="uc1" TagName="SpotGrid" Src="SpotGrid.ascx" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphInstructions">
	<table runat="server" id="ResinRequested" width="100%" class='InstructionBullets' style="margin-left:20px">
		<tr> 
			<td align='left'><img src='../pics/bullet.gif'/></td><td>Thank you for submitting a Resin Request.</td>
		</tr>
		<tr>
			<td align='left'><img src='../pics/bullet.gif'/></td><td>The following offers most closely match your request.</td>
		</tr>
		<tr>
			<td align='left'><img src='../pics/bullet.gif'/></td><td>Please click Inquire to pursue a particular offer.</td>
		</tr>
		<tr>
			<td align='left'><img src='../pics/bullet.gif'/></td><td>Our brokers are also searching for other offers that might better suit your needs.</td>
		</tr>
	</table>

	<table width="100%" runat="server" id="International" class='InstructionBullets' style="margin-left:20px">
		<tr>
		    <td><img src='../pics/bullet.gif'/></td>
		    <td>The following table summarizes our current International spot offers.</td>
		</tr>
		<tr>
		    <td><img src='../pics/bullet.gif'/></td>
		    <td>The price range reflects on the low side, offgrade close by and the high side, prime from a far distance.</td>
		</tr>
		<tr>
		    <td><img src='../pics/bullet.gif'/></td>
		    <td>Click on the Grade type to be redirected to detailed spot offers for that category.</td>
		</tr>
		<tr>
		    <td><img src='../pics/bullet.gif'/></td>
		    <td>You can then make purchases through the detailed offers screen.</td>
	    </tr>
		<tr>
		    <td><img src='../pics/bullet.gif'/></td>
		    <td><span style="color:Red">All offers subject to prior sale and credit approval.</span></td>
		</tr>
	</table>
	
    <table width="100%" runat="server" id="Domestic" class='InstructionBullets' style="margin-left:20px;">
		<tr>
		    <td><img src='../pics/bullet.gif'/></td>
		    <td>These are actual lots placed for sale by major industry players.</td>
		</tr>
		<tr>
		    <td><img src='../pics/bullet.gif'/></td>
		    <td>Most lots are day orders only or good until cancelled.</td>
		</tr>
		<tr>
		    <td><img src='../pics/bullet.gif'/></td>
		    <td>You can request additional information or make a transaction by clicking Inquire to the left of the offers.</td>
		</tr>
		<tr>
			<td><img src='../pics/bullet.gif'/></td>
			<td>If you do not see what you need just click Request (above) and we will find it for you!</td>
		</tr>
		<tr>
		    <td><img src='../pics/bullet.gif'/></td>
		    <td><span style="color:Red">All offers subject to prior sale and credit approval.</span></td>
		</tr>
   </table>  
</asp:Content>

<asp:Content ContentPlaceHolderID="cphHeading" runat="server">
    <table border='0' cellpadding='0' cellspacing='0' width="100%">
    <tr>
        <td align="left"><span class="Header Bold Color1">Spot Market</span></td>
        <td align="left">
            <asp:image id="imgInternational" runat="server" Visible="False" ImageUrl="/images2/request_resin/international.jpg" />
            <asp:image id=imgDomestic runat="server" ImageUrl="/images2/request_resin/us.jpg" />
            <A onmouseover="MM_swapImage('Image23','','/images2/request_resin/us_unpressed.jpg',1)" onmouseout=MM_swapImgRestore() href="/Spot/Spot_Floor.aspx?Export=false" ></a>            
         </td>
        <td align="left"><asp:label id="lblMarket" runat="server" CssClass="Header Color4 Bold" Font-Bold="True">U.S. Domestic Market</asp:label>&nbsp;&nbsp; 
            </td>
        <td><asp:label id="lblMarketMode" runat="server" Visible="False"></asp:label></td>    
    </tr>
    </table>
</asp:Content>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
    <table cellspacing="0" cellpadding="0" align="center" border="0" width="100%">
    <tr>
        <td>
          <table width="100%" height="60" cellspacing="0" cellpadding="0" align="center" bgColor="#000000" border="0">
            <tr>
              <td id="tb">
              </td>
            </tr>
            <tr>
              <td colSpan=2><uc1:spotgrid id="SpotGrid1" runat="server"></uc1:spotgrid></td>
            </tr>
          </table>
        </td>
    </tr>
    </table>
</asp:Content>
