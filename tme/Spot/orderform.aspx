<%@ Page Language="C#" MasterPageFile="~/MasterPages/Template.Master" AutoEventWireup="true" CodeBehind="orderform.aspx.cs" Inherits="localhost.Spot.orderform" Title="Lozier Market - Info" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="asp" namespace="System.Web.UI" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphCssLink" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMain" runat="server">

<asp:ScriptManager runat="server"></asp:ScriptManager>

    <asp:UpdatePanel runat="server">
     <ContentTemplate> 

    <!-- Page Title Bar -->
    <div id="TitleBar">
    <img src="../images/title-arrow-white.jpg" alt="Arrow Title Lozier Page" longdesc="http://www.lozierenergy.com" height="25px" width="23px" style="float:left" />
    <h1 style="float:left">Spot Floor</h1>
    </div>

<table cellSpacing="0" cellPadding="0" align="center" border="0" width="780">
<tr>
	<td align="left">
	    <asp:datagrid BorderWidth="0" CellSpacing="1" BackColor="#000000" id="summaryGrid" runat="server" 
	        OnItemDataBound="dg_ItemDataBound"  CssClass="Content LinkNormal" ShowFooter="True"
			HorizontalAlign="Left" AutoGenerateColumns="False" CellPadding="2">            
                <AlternatingItemStyle CssClass="LinkNormal LightGray" HorizontalAlign="Left" />                
                <ItemStyle CssClass="LinkNormal DarkGray" HorizontalAlign="left" />                 
                <HeaderStyle HorizontalAlign="Left" CssClass="LinkNormal Bold OrangeColor" VerticalAlign="Middle" />                
                <FooterStyle HorizontalAlign="Left" CssClass="Content Bold Color4 FooterColor" />
    
                <Columns>
                    <asp:BoundColumn DataField="bid_grade" HeaderText="Contract"  />                            
                    <asp:BoundColumn DataField="bid_size" HeaderText="Qty" DataFormatString="{0:#,##}" />
                    <asp:BoundColumn DataField="bid_max" HeaderText="Best Bid" DataFormatString="{0:c}" />
                    <asp:BoundColumn DataField="offr_min" HeaderText="Best Offer" DataFormatString="{0:c}" />
                    <asp:BoundColumn DataField="offr_size" HeaderText="Qty" DataFormatString="{0:#,##}" />                     
                    <asp:BoundColumn Visible="False" DataField="grade_id" HeaderText="grade_id" />                  
                </Columns>
		</asp:datagrid>
   </td>
    <td align="left" width="250px" valign="top">
        
          <div id="OrderEntry">
          
        <table cellSpacing="2" cellPadding="2" align="center" border="0" width="100%">
            <tr >
                <td colspan="2">
                    <div id="TitleOrderEntry">
                        <h3>Order Entry</h3>
                    </div>                
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:RadioButtonList runat="server" ID="rbOrderType"  RepeatDirection="Horizontal" >
                        <asp:ListItem Text="Bid" Value="B" />
                        <asp:ListItem Text="Offer" Value="O" />
                    </asp:RadioButtonList>                        
                </td>
            </tr>
            <tr>
                <td colspan="2"> 
                    <asp:DropDownList CssClass="form-style" ID="ddlProduct" runat="server"  
                    AutoPostBack="True" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" />
                </td>
            </tr>
            
            <tr>
                <td colspan="2">
                    <asp:DropDownList ID="ddlCompanies" runat="server" AutoPostBack="true" 
                    CssClass="form-style" OnSelectedIndexChanged="ddlCompanies_SelectedIndexChanged" />                                    
                </td>
            </tr>
            
            <tr>
                <td>
                     <asp:DropDownList runat="server" onkeypress="return noenter()" ID="txtQty" CssClass="form-style" />                        
                </td>            
                
                <td>
                    <asp:DropDownList ID="ddlQty" runat="server" CssClass="form-style" />                                        
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <asp:TextBox onkeypress="return noenter()" ID="txtPrice" runat="server" MaxLength="8" size="14" CssClass="form-style" />
                    <asp:Label ID="lblPriceMeasure" runat="server" Text="/lb." />
                </td>
            </tr>
            
            <tr>
                <td colspan="2">
                    <asp:DropDownList ID="ddlDeliveryPoint" runat="server" CssClass="form-style" />                                        
                </td>
            </tr>
            
            
        </table>        
   
   </div>

   
   </td>
</tr>
</table>


</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>
