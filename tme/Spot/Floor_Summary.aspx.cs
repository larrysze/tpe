using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPECharts;
using System.Threading;
using dotnetCHARTING;
using TPE.Utility;
////////////////////////////////////////////////////////////////////////////////////////
//according different data stored in the OFFR_PROD field of the BBOFFER table,show the//
//count of record,maximum price and the minimum price in the grid.                    //
//                                                                                    //
//                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////

namespace localhost.Spot
{
	/// <summary>
	/// 
	/// </summary>
	public partial class summary : System.Web.UI.Page
	{

		private SeriesCollection getRandomData()
		{
			SeriesCollection SC = new SeriesCollection();
			Random myR = new Random();
			for(int a = 0; a < 4; a++)
			{
				Series s = new Series();
				s.Name = "Series " + a;
				for(int b = 0; b < 5; b++)
				{
					Element e = new Element();
					e.Name = "E " + b;
					e.YValue = myR.Next(50);
					s.Elements.Add(e);
				}
				SC.Add(s);
			}

			// Set Different Colors for our Series
			SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
			SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
			SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
			SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);
			return SC;
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//only adminstrator and broker can access this page.
			//if ((string)Session["Typ"] != "A") 
			//{
			//	Response.Redirect("/default.aspx");
			//}

			if(!this.Page.IsPostBack)
			{
				dotnetCHARTING.Chart ChartObj = new dotnetCHARTING.Chart();

				// Add the random data.
				ChartObj.SeriesCollection.Add(getRandomData());

				ChartObj.Width = 10;
				ChartObj.Height = 10;
								
				ChartObj.TempDirectory = "Temp";
    
				// Set the name of the file
				ChartObj.FileName = "Temp";
						
				// Set the format of the file
				ChartObj.FileManager.ImageFormat = ImageFormat.Png;
				Bitmap bmp1 = ChartObj.GetChartBitmap();
				//ChartObj.FileManager.SaveImage(bmp1);
 				
				//Set the DB Connection to the Chart Control. It makes it to be able to re-create all the charts, if necessary.
				TPEChartsControl chartsControl = new TPEChartsControl();
				chartsControl.ConnectionStringDB = Application["DBConn"].ToString();
				chartsControl.setFolder(Server.MapPath("/Research/charts"));
				Thread threadCharts = new Thread(new ThreadStart(chartsControl.check_images));
				threadCharts.Start();

				DataBindGrid();//bind the data with the grid.			
			}
        }
		
        
        double dbTotalOffers = 0.0;
		double dbTotalOffersLBS = 0.0;
		double dbTotalBid = 0.0;
		double dbTotalBidLBS = 0.0;
	
		protected void dg_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
           
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				dbTotalOffers += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "offr_count")==DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "offr_count"));
				dbTotalOffersLBS += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "offr_size")==DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "offr_size"));
				dbTotalBid += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "bid_count")==DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "bid_count"));
				dbTotalBidLBS += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "bid_size")==DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "bid_size"));


                if (Session["Typ"].ToString() != "A" && Session["Typ"].ToString() != "B" && Session["Typ"].ToString() != "L" && Session["Typ"].ToString() != "T" && Session["Typ"].ToString() != "S" && Session["Typ"].ToString() != "D") 
				{
					e.Item.Cells[5].Text = (e.Item.Cells[0].Text=="&nbsp;" ? e.Item.Cells[6].Text :e.Item.Cells[0].Text);
					e.Item.Cells[5].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "'>" + e.Item.Cells[5].Text + "</a>";
				}
				else
				{
					e.Item.Cells[4].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&StartOn=bid'>" + e.Item.Cells[4].Text + "</a>"; //qtd
					e.Item.Cells[3].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&StartOn=bid&Sort=VARBIDOFFER_DESC'>" + e.Item.Cells[3].Text + "</a>"; //lbs
					e.Item.Cells[2].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&StartOn=bid&Sort=VARPRICE_ASC'>" + e.Item.Cells[2].Text + "</a>"; //low
					e.Item.Cells[1].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&StartOn=bid&Sort=VARPRICE_DESC'>" + e.Item.Cells[1].Text + "</a>"; //high
					e.Item.Cells[5].Text = (e.Item.Cells[0].Text=="&nbsp;" ? e.Item.Cells[6].Text :e.Item.Cells[0].Text);
					e.Item.Cells[7].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "'>" + e.Item.Cells[7].Text + "</a>"; //qtd
					e.Item.Cells[8].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&Sort=VARBIDOFFER_DESC'>" + e.Item.Cells[8].Text + "</a>"; //lbs
					e.Item.Cells[9].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&Sort=VARPRICE2_ASC'>" + e.Item.Cells[9].Text + "</a>"; //low
					e.Item.Cells[10].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&Sort=VARPRICE2_DESC'>" + e.Item.Cells[10].Text + "</a>"; //high            

                    //1	Ethylene - Purity
                    //2	Propylene - Refinery
                    //3	Propylene - Chemical
                    //4	Propylene - Polymer
                    //5	Benzene
                    double GradeID = Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "grade_id") == DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "grade_id"));
                    double bidsize = Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "bid_size") == DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "bid_size"));
                    double offersize= Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "offr_size") == DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "offr_size"));

                    if (GradeID == 1 || GradeID == 3 || GradeID == 4)
                    {
                        e.Item.Cells[1].Text = Convert.ToString(bidsize / 5000000);
                        e.Item.Cells[4].Text = Convert.ToString(offersize / 5000000);
                    }
                    else if (GradeID == 2)
                    {
                        e.Item.Cells[1].Text = Convert.ToString(bidsize / 4525000);
                        e.Item.Cells[1].Text = Convert.ToString(offersize / 4525000);
                    }
                    else if (GradeID == 5)
                    {
                        e.Item.Cells[1].Text = Convert.ToString(bidsize / 7725000);
                        e.Item.Cells[4].Text = Convert.ToString(offersize / 7725000);
                    }
                    else
                    {
                        e.Item.Cells[1].Text = "0";
                        e.Item.Cells[4].Text = "0";
                    }
				}
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
                //e.Item.Cells[5].Text = "<b>Totals:</b> " ;
				
                //e.Item.Cells[4].Text = "<b>"+String.Format("{0:#,###}", dbTotalBid)+"</b>";
                //e.Item.Cells[3].Text = "<b>"+String.Format("{0:#,###}", dbTotalBidLBS)+"</b>";

                //e.Item.Cells[7].Text = "<b>"+String.Format("{0:#,###}", dbTotalOffers)+"</b>";
                //e.Item.Cells[8].Text = "<b>"+String.Format("{0:#,###}", dbTotalOffersLBS)+"</b>";
			}


        
		}

		private void DataBindGrid()//bind the data with the grid.
		{			
			//select the data from the bboffer table,and group the record by the data stored in the OFFR_PROD field 
			//of the BBOFFER table.to calculate the count of records,the maximum price,the minimum price and 
			//the count of the weight for each group.			
			DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(),summaryGrid,"spSpotFloorSummary");			
		}
		

    #region Web 
		override protected void OnInit(EventArgs e)
		{
			//

			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent()
		{    

		}
    #endregion


		
	}
}
