<%@ Page language="c#" Codebehind="Floor_Summary.aspx.cs" AutoEventWireup="True" Inherits="localhost.Spot.summary" MasterPageFile="~/MasterPages/Template.Master" Title="Spot Floor Summary"%>
<%@ MasterType virtualPath="~/MasterPages/Template.Master"%>
    <asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphInstructions">
        <table class='InstructionBullets'>
		<tr> 
			<td width=35><img src='../pics/bullet.gif'/></td><td>The following table summarizes our current offers.</td>
		</tr>
		<tr> 
			<td width=35><img src='../pics/bullet.gif'/></td><td>Click on the Grade type to be redirected to detailed spot offers for that category.</td>
		</tr>
		<tr> 
			<td width=35><img src='../pics/bullet.gif'/></td><td>You can then make purchases through the detailed offers screen.</td>
		</tr>
		</table>
    </asp:Content>
    <asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphHeading"><div class="Header Bold Color1">Spot Floor Summary</div></asp:Content>
	<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
		<table cellSpacing="0" cellPadding="0" width="780" align="center" border="0">
			<tr>
				<td>
				    <asp:datagrid BorderWidth="0" CellSpacing="1" BackColor="#000000" id="summaryGrid" runat="server" OnItemDataBound="dg_ItemDataBound"  CssClass="Content LinkNormal" ShowFooter="True"
						Width="100%" HorizontalAlign="Left" AutoGenerateColumns="False" CellPadding="2">
                            <AlternatingItemStyle CssClass="LinkNormal LightGray" HorizontalAlign="Left">
                            </AlternatingItemStyle>

                            <ItemStyle CssClass="LinkNormal DarkGray" HorizontalAlign="left">
                            </ItemStyle>

                            <HeaderStyle HorizontalAlign="Left" CssClass="LinkNormal Bold OrangeColor" VerticalAlign="Middle">
                            </HeaderStyle>

                            <FooterStyle HorizontalAlign="Left" CssClass="Content Bold Color4 FooterColor">
                            </FooterStyle>

                            <Columns>
                            <asp:BoundColumn DataField="bid_grade" HeaderText="Contract"  />                            
                            <asp:BoundColumn DataField="bid_size" HeaderText="Qty" DataFormatString="{0:#,##}" />
                            <asp:BoundColumn DataField="bid_max" HeaderText="Best Bid" DataFormatString="{0:c}" />
                            <asp:BoundColumn DataField="offr_min" HeaderText="Best Offer" DataFormatString="{0:c}" />
                            <asp:BoundColumn DataField="offr_size" HeaderText="Qty" DataFormatString="{0:#,##}" />
                            
                            
                           
                           <asp:BoundColumn HeaderText="Grade" Visible="false" />
                            <asp:BoundColumn Visible="false" DataField="bid_min" HeaderText="Low Price" DataFormatString="{0:c}" />
                            
                            <asp:BoundColumn Visible="false" DataField="bid_count" HeaderText="Bids" />
                            
                            <asp:BoundColumn Visible="False" DataField="offr_grade" HeaderText="Offer Grade" />
                            <asp:BoundColumn Visible="false"  DataField="offr_count" HeaderText="Offers" />                                                       
                            <asp:BoundColumn Visible="false" DataField="offr_max" HeaderText="High Price" DataFormatString="{0:c}" />
                            <asp:BoundColumn Visible="False" DataField="grade_id" HeaderText="g.grade_id" />
                            </Columns>
					</asp:datagrid>
			   </td>
			</tr>
		</table>
	</asp:Content>
