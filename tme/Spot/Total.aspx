<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page language="c#" Codebehind="Total.aspx.cs" AutoEventWireup="false" Inherits="PlasticsExchange.Total" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Total</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript">

		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TPE:TEMPLATE id="Template1" Runat="Server" PageTitle="Total / Method of Payment"></TPE:TEMPLATE>
			<TABLE id="Table11" style="WIDTH: 736px; HEIGHT: 600px"  border="0">
				<TBODY>
				<tr>
					<td  style="HEIGHT: 30px" align=center class="ListHeadlineBold"> Total / Method of Payment
					</td>
				</tr>
				<TR>
					<TD vAlign="top" align="left">
						<TABLE id="Table12" style="WIDTH: 680px; HEIGHT: 136px" height="136" width="680" border="0">
							<TR>
								<TD align="left" colSpan="5"><FONT color="blue" size="3"><B>Order Summary</B></font></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 22px" align="left"><FONT size="2"><B>ID#</B></FONT></TD>
								<TD style="WIDTH: 166px; HEIGHT: 22px" align="left"><FONT size="2"><B>Product</B></FONT></TD>
								<TD style="WIDTH: 176px; HEIGHT: 22px" align="left"><FONT size="2"><B>Size</B></FONT></TD>
								<TD style="HEIGHT: 22px" align="left"><FONT size="2"><B>Terms</B></FONT></TD>
								<TD style="HEIGHT: 22px" align="left"><FONT size="2"><B>Price</B></FONT></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 19px" align="left"><FONT size="2"><asp:label id="lblID" runat="server"></asp:label></FONT></TD>
								<TD style="WIDTH: 166px; HEIGHT: 19px" align="left"><FONT  size="2"><asp:label id="lblProduct" runat="server" Width="148px"></asp:label></FONT></TD>
								<TD style="WIDTH: 176px; HEIGHT: 19px" align="left"><FONT size="2"><asp:label id="lblSize" runat="server" Width="162px"></asp:label></FONT></TD>
								<TD style="HEIGHT: 19px" align="left"><FONT size="2"><asp:label id="lblTerms" runat="server"></asp:label></FONT></TD>
								<TD style="HEIGHT: 19px" align="left"><FONT  size="2"><asp:label id="lblPrice" runat="server"></asp:label></FONT></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 60px" align="left"><FONT size="2"></FONT></TD>
								<TD style="WIDTH: 166px; HEIGHT: 60px" align="left"><FONT size="2"></FONT></TD>
								<TD style="WIDTH: 176px; HEIGHT: 60px" align="left"><FONT size="2"></FONT></TD>
								<TD style="HEIGHT: 60px" vAlign="bottom" align="left"><FONT color="blue" size="2"><b>Freight:</b></FONT></TD>
								<TD style="HEIGHT: 60px" vAlign="bottom" align="left" height="60"><FONT  size="2"><b><asp:label id="lblFreight" runat="server"></asp:label></b></FONT></TD>
							</TR>
							<TR>
								<TD align="left"><FONT size="2"></FONT></TD>
								<TD style="WIDTH: 166px" align="left"><FONT size="2"></FONT></TD>
								<TD style="WIDTH: 176px" align="left"><FONT size="2"></FONT></TD>
								<TD align="left"><FONT color="blue" size="2"><b>Total:</b></FONT></TD>
								<TD align="left"><FONT size="2"><b><asp:label id="lblTotal" runat="server"></asp:label></b></FONT></TD>
							</TR>
						</TABLE>
						<br>
						<asp:panel id="pnlCreditOn" runat="server" >
							<TABLE id="Table2"   height="28" width="100%" border="0">
								<TR>
									<TD class="ListHeadlineBold" colspan="2" align="center">Method of Payment</TD>
								</TR>
							
								<TR>
									<TD align=center><B>Submit Credit Application 
												online.</B></TD>
									<TD align=center><B>I prefer to fax my info.</B></TD>
								</TR>
								<TR>
									<TD align=center>&nbsp;
										<asp:Button id="btnFaxOnline" runat="server" Text="Use Online App"></asp:Button></TD>
									<TD align=center>&nbsp;
										<asp:Button id="btnFaxCredit" runat="server" Text="Fax Credit App"></asp:Button></TD>
								</TR>
								<TR>
									<TD align=center><FONT  size="1">
											(Choose this option to submit your application<BR>
											online using our secure server.)
											</FONT>
									</TD>
									<TD align=center><FONT  size="1">(Choose this option and a Adobe PDF
											<BR>form will open in a new window.)</FONT>
									</TD>
								</TR>
							</TABLE>
						</asp:panel>
				
						
					</TD>
				</TR>
				<asp:panel id="pnlCreditOff" runat="server" Width="688px" Height="40px">
				<TR>
									<td  style="HEIGHT: 30px" align=center class="ListHeadlineBold">
										<asp:Button id="btnGoBack" runat="server" Text="    Back   "></asp:Button>&nbsp;&nbsp;
										<asp:Button id="btnContinue" runat="server" Text=" Continue "></asp:Button></TD>
				</TR>
				</asp:panel><br>
				</tbody>
			</TABLE>
			
		</form>
		<TPE:TEMPLATE id="Template3" Runat="Server" Footer="true"></TPE:TEMPLATE>
	</body>
</HTML>
