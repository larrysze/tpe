<%@ Page Language="c#" Codebehind="Matched_Order.aspx.cs" AutoEventWireup="True" Inherits="localhost.Spot.Matched_Order" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" Title="Matched Order" %>

<%@ MasterType VirtualPath="~/MasterPages/TemplateWithoutInstructions.Master" %>
<asp:Content ContentPlaceHolderID="cphJavaScript" runat="server">

    
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphHeading"><span class="Header Color1 Bold">Matched Orders</span></asp:Content>

<asp:Content ContentPlaceHolderID="cphMain" runat="server">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="There are problems with the following fields. Please correct the errors below." CssClass="Header Color3" ForeColor=" "></asp:ValidationSummary>
    <asp:Label ID="test" runat="server"></asp:Label>
    <!-- flag for the radio selection -->
    <input type="hidden" class="Content" name="Other_Product" id="Other_Product">
    <table bordercolor="#212121" cellspacing="0" cellpadding="0" border="1" width="927" class="Content">
        <tr>
            <td>
                <asp:Panel ID="pnMain" runat="server">
                    <table cellspacing="0" cellpadding="0" border="0" class="Content">
                        <tr valign="middle" bgcolor="#cccccc" height="20">
                            <td class="Header" align="center" width="180">
                                Buyer</td>
                            <td bgcolor="#212121" rowspan="6">
                            </td>
                            <td class="Header" align="center" width="325">
                                Contract</td>
                            <td bgcolor="#212121" rowspan="6">
                            </td>
                            <td class="Header" align="center" width="180">
                                Seller</td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <table cellspacing="0" cellpadding="2" border="0">
                                    <tr>
                                        <td>
                                            Company:<br>
                                            <asp:DropDownList ID="ddlBuyer_Comp" CssClass="InputForm" runat="server" OnSelectedIndexChanged="Change_Buyer" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <br>
                                            User:<br />
                                            <asp:DropDownList ID="ddlBuyer" runat="server" CssClass="InputForm">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblBuyerEditUser" runat="server" CssClass="LinkNormal">Edit User</asp:Label>&nbsp;&nbsp;&nbsp;<asp:Label ID="lblBuyerAddUser" runat="server" CssClass="LinkNormal">Add User</asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RangeValidator ID="RangeValidator1" runat="server" MaximumValue=".9999" MinimumValue=".0001" Type="Double" Display="none" ErrorMessage="Buy price must be less than one dollar per pound." ControlToValidate="txtBuyPrice"></asp:RangeValidator>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" Display="none" ErrorMessage="Buy Price must be a number." ControlToValidate="txtBuyPrice" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="none" ErrorMessage="Buy Price cannot be blank" ControlToValidate="txtBuyPrice"></asp:RequiredFieldValidator><br />
                                            Buy Price $
                                            <asp:TextBox ID="txtBuyPrice" runat="server" CssClass="InputForm" MaxLength="5" size="5"></asp:TextBox><asp:Label ID="lblBuyPriceLabel" runat="server">per pound</asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        PO#
                                                        <asp:TextBox ID="txtPO" runat="server" MaxLength="20" CssClass="InputForm"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                            </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="left">
                                <table cellspacing="0" cellpadding="2" border="0">
                                    <tr>
                                        <td colspan="2">
                                            Product:
                                            <table width="150" border="0">
                                                <tr>
                                                    <td>
                                                        <asp:RadioButton ID="radio_Product" Checked="true"  runat="server" GroupName="RadioGroup1" border="0"></asp:RadioButton></td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlProduct" AutoPostBack="true" CssClass="InputForm" runat="server" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" />
                                                        </td>
                                                </tr>                                                
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            Details:
                                            <asp:TextBox ID="txtDetail" runat="server" CssClass="InputForm"></asp:TextBox>
                                            <asp:CheckBox ID="cbPrime" runat="server" Text="Prime"></asp:CheckBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table>
                                                <tr>
                                                    <td colspan="6">
                                                        
                                                    </td>

                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table>
                                                <tr>
                                                    <asp:CompareValidator ID="CompareValidator2" runat="server" Display="none" ErrorMessage="Quantity must be a number." ControlToValidate="txtQty" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator><td>
                                                        Quantity</td>
                                                    <td>
                                                        <asp:TextBox ID="txtQty" Width="35px" runat="server" CssClass="InputForm" ReadOnly="false" value="1"></asp:TextBox></td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSize" runat="server" CssClass="InputForm" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Broker
                                        </td>
                                        <td>
                                            Commission</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlBroker" runat="server" CssClass="InputForm" AutoPostBack="True" OnSelectedIndexChanged="ddlBroker_SelectedIndexChanged">
                                            </asp:DropDownList></td>
                                        <td>
                                            <asp:RangeValidator ID="Rangevalidator4" runat="server" CssClass="InputForm" MaximumValue="100" MinimumValue="0" Type="Integer" Display="none" ErrorMessage="Broker commission must be between 0% and 100%" ControlToValidate="txtCommission"></asp:RangeValidator>
                                            <asp:TextBox ID="txtCommission" Width="35px" runat="server" CssClass="InputForm"></asp:TextBox>%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblWarehouse" runat="server">Storage Warehouse:</asp:Label><br>
                                            <asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="InputForm" OnSelectedIndexChanged="Change_Warehouse" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <table cellspacing="0" cellpadding="2" border="0" align=left>
                                    <tr>
                                        <td>
                                            Company:
                                            <asp:Label ID="lblSellerNotAvail" runat="server"></asp:Label><br />
                                            <asp:DropDownList ID="ddlSeller_Comp" runat="server" CssClass="InputForm" OnSelectedIndexChanged="Change_Seller" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr  align="left">
                                        <td>
                                            <br />
                                            User:<br />
                                            <asp:DropDownList ID="ddlSeller" runat="server" CssClass="InputForm">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblSellerEditUser" runat="server" CssClass="LinkNormal">Edit User</asp:Label>&nbsp;&nbsp;&nbsp;<asp:Label ID="lblSellerAddUser" runat="server" CssClass="LinkNormal">Add User</asp:Label></td>
                                    </tr>
                                    <tr align="left">
                                        <td>
                                            <asp:RangeValidator ID="RangeValidator2" runat="server" MaximumValue=".9999" MinimumValue=".0001" Type="Double" Display="none" ErrorMessage="Sell price must be less than one dollar per pound." ControlToValidate="txtSellPrice"></asp:RangeValidator>
                                            <asp:CompareValidator ID="CompareValidator3" runat="server" Display="none" ErrorMessage="Sell Price must be a number." ControlToValidate="txtSellPrice" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="none" ErrorMessage="Sell Price cannot be blank" ControlToValidate="txtSellPrice"></asp:RequiredFieldValidator><br>
                                            Sell Price $
                                            <asp:TextBox ID="txtSellPrice" runat="server" Width="55px" MaxLength="5" CssClass="InputForm"></asp:TextBox><asp:Label ID="lblSellPriceLabel" runat="server">per pound</asp:Label></td>
                                    </tr>
                                      <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        PO#
                                                        <asp:TextBox ID="txtSellerPO" runat="server" MaxLength="20" CssClass="InputForm" /></td>
                                                </tr>
                                            </table>
                                            </td>
                                    </tr>  
                                </table>
                            </td>
                        </tr>   
                                             
                        <tr>
                            <td colspan="2" height="30">
                            </td>
                            <td>
                                <center>
                                    <asp:Button ID="Button1" OnClick="Click_Continue" CssClass="Content Color2" runat="server" Text="Continue"></asp:Button></center>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnConfirm" runat="server" Visible="false">
                    <center><br>
                        <p class="Content Bold Color2">
                            Order Confirmation</p>
                    </center>
                    <asp:DropDownList ID="ddlAllocate" runat="server" Visible="false" CssClass="InputForm">
                    </asp:DropDownList>
                    <table cellspacing="4" cellpadding="4" align="center" border="0">
                        <tr valign="middle" bgcolor="#cccccc">
                            <td align="center" colspan="5">
                                <strong>
                                    <asp:Label ID="lblProduct" runat="server"></asp:Label></strong></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <strong>User</strong></td>
                            <td width="309">
                                <strong>Company</strong></td>
                            <td>
                                <strong>Location</strong></td>
                            <td>
                                <strong>Price</strong></td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Buyer</strong></td>
                            <td>
                                <asp:Label ID="lblBuyer" runat="server"></asp:Label>
                            </td>
                            <td width="309">
                                <asp:Label ID="lblBuyerComp" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblBuyerLocal" runat="server"></asp:Label>
                            </td>
                            <td>
                                $<asp:Label ID="lblBuyPrice" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Seller</strong></td>
                            <td>
                                <asp:Label ID="lblSeller" runat="server"></asp:Label>
                            </td>
                            <td width="309">
                                <asp:Label ID="lblSellerComp" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblSellerLocal" runat="server"></asp:Label>
                            </td>
                            <td>
                                <font color="red">$(<asp:Label ID="lblSellPrice" runat="server"></asp:Label>)</font></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTaxName" runat="server" Font-Bold="True"></asp:Label>
                            </td>
                            <td align="center" colspan="3">
                            </td>
                            <td>
                                <asp:Label ID="lblTax" runat="server">0.0</asp:Label>
                                %</td>
                        </tr>
                        <tr>
                            <td height="26">
                                <strong>Commission</strong></td>
                            <td align="center" colspan="3" height="26">
                            </td>
                            <td height="26">
                                <font color="red">&nbsp;(<asp:Label ID="lblComm" runat="server"></asp:Label>)%</font></td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Total Freight</strong></td>
                            <td align="center" colspan="3">
                                <font color="red">
                                    <asp:Label ID="lblFreightMessage" runat="server"></asp:Label>
                                </font>
                            </td>
                            <td>
                                <font color="red">$(<asp:Label ID="lblFreight" runat="server"></asp:Label>)</font></td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Profit</strong></td>
                            <td>
                            </td>
                            <td width="309">
                            </td>
                            <td>
                            </td>
                            <td>
                                <strong>&nbsp;$<asp:Label ID="lblMarkup" runat="server"></asp:Label></strong></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="5">
                                <br>
                                <br>
                                &nbsp;
                                <asp:Label ID="lblCreditInformation" runat="server" Font-Bold="True"></asp:Label>
                                <br>
                                <br>
                                <asp:DataGrid ID="dg" runat="server" Visible="False" AutoGenerateColumns="False" CellPadding="2" ShowFooter="True" HorizontalAlign="Center" Width="800px">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                    <HeaderStyle BackColor="Silver"></HeaderStyle>
                                    <Columns>
                                        <asp:HyperLinkColumn Visible="False" DataTextField="COMP_ID" SortExpression="COMP_ID ASC" HeaderText="Company ID"></asp:HyperLinkColumn>
                                        <asp:BoundColumn DataField="COMP_NAME" SortExpression="COMP_NAME ASC" HeaderText="Company Name">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FO" SortExpression="FO ASC" HeaderText="Outstanding" DataFormatString="{0:c}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="COMP_CREDIT_LIMIT" SortExpression="COMP_CREDIT_LIMIT ASC" HeaderText="Credit Limit" DataFormatString="{0:c}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="COMMINVTRY" SortExpression="COMMINVTRY ASC" HeaderText="Committed Inventory" DataFormatString="{0:c}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="OCURRENT" SortExpression="OCURRENT ASC" HeaderText="Current" DataFormatString="{0:c}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="O30" SortExpression="O30 ASC" HeaderText="1-30" DataFormatString="{0:c}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="O3160" SortExpression="O3160 ASC" HeaderText="30-60" DataFormatString="{0:c}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="O6190" SortExpression="O6190 ASC" HeaderText="60-90" DataFormatString="{0:c}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="O90ORMORE" SortExpression="O90ORMORE ASC" HeaderText="&gt;90" DataFormatString="{0:c}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="5">
                                <asp:Button CssClass="Content Color2" ID="Button2" OnClick="Click_Back" runat="server" Text="Back"></asp:Button>
                                <asp:Button CssClass="Content Color2" ID="Button3" OnClick="Click_Submit" runat="server" Text="Submit"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
