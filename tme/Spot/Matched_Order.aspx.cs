using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Data.SqlClient;
using System.Text;
using TPE.Utility;
using System.Configuration;

namespace localhost.Spot
{
	/// <summary>
	/// Summary description for Matched_Order.
	/// </summary>
	public partial class Matched_Order : System.Web.UI.Page
	{  
	
			  /*****************************************************************************
			  '*   1. File Name       : spot/Matched_Order.aspx 		                   *
			  '*   2. Description     : order entry screen for spot transactions	       *
			  '*																		   *
			  '*   3. Modification Log:                                                    *
			  '*     Ver No.       Date          Author             Modification           *
			  '*   -----------------------------------------------------------------       *
			  '*      1.00      11-26-2003       Zach                  First Baseline      *
			  '*                                                                           *
			  '*****************************************************************************/
    
		public void Page_Load(object sender, EventArgs e)
		{
            Master.Width = "780px";
            Master.Heading = "Matched Order";
            
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "L"))
			{
				Response.Redirect("/default.aspx");
			}
			
            
            if (!IsPostBack)
			{
				using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
				{
					conn.Open();
			
					// Query buyer companies name
					// distributors can buy or sell.  Officers can buy or sell assuming their companies can do so
					using (SqlDataReader dtrBuyer_Comp = DBLibrary.GetDataReaderFromSelect(conn,"SELECT COMP_ID,COMP_NAME FROM COMPANY WHERE COMP_TYPE IN ('D','P') AND COMP_REG ='1' AND COMP_ID IN (SELECT DISTINCT PERS_COMP FROM PERSON WHERE PERS_TYPE = 'O' OR PERS_TYPE='P' OR PERS_TYPE='T') ORDER BY COMP_NAME"))
					{
						//binding buyer company drop-down list
						ddlBuyer_Comp.DataSource = dtrBuyer_Comp;
						ddlBuyer_Comp.DataTextField= "COMP_NAME";
						ddlBuyer_Comp.DataValueField= "COMP_ID";
						ddlBuyer_Comp.DataBind();
					}
    
				
					// Query seller companies name
				
					//            cmdSeller_Comp = new SqlCommand("SELECT COMP_ID,COMP_NAME FROM COMPANY WHERE COMP_TYPE IN ('D','S') AND COMP_ID IN (SELECT DISTINCT PERS_COMP FROM PERSON WHERE PERS_TYPE = 'S') ORDER BY COMP_NAME",conn);
					// distributors can buy or sell.  Officers can buy or sell assuming their companies can do so
				
					using (SqlDataReader dtrSeller_Comp = DBLibrary.GetDataReaderFromSelect(conn,"SELECT COMP_ID,COMP_NAME FROM COMPANY WHERE COMP_TYPE IN ('D','S') AND COMP_REG ='1' AND COMP_ID IN (SELECT DISTINCT PERS_COMP FROM PERSON WHERE PERS_TYPE = 'O'  OR PERS_TYPE='S' OR PERS_TYPE='T') AND COMP_ID<>'148' ORDER BY COMP_NAME"))
					{
						//binding seller company drop-down list
						ddlSeller_Comp.DataSource = dtrSeller_Comp;
						ddlSeller_Comp.DataTextField= "COMP_NAME";
						ddlSeller_Comp.DataValueField= "COMP_ID";
						ddlSeller_Comp.DataBind();
					}
				/*
						// opening contract list
						SqlCommand cmdContract;
						SqlDataReader dtrContract;
    
						cmdContract = new SqlCommand("SELECT CONT_ID,CONT_LABL FROM Contract ORDER BY CONT_ORDR",conn);
						dtrContract = cmdContract.ExecuteReader();
    
						//binding company dd list
						ddlProduct.DataSource = dtrContract;
						ddlProduct.DataTextField= "CONT_LABL";
						ddlProduct.DataValueField= "CONT_ID";
						ddlProduct.DataBind();
						dtrContract.Close();
				*/
	    
					// Populating other info
	    
					//Product drop-down
					HelperFunction.LoadGrades(this.Context, ddlProduct, "Grade_ID");
	    
					//order size drop-down
					

	    
					lblWarehouse.Visible=false;
					ddlWarehouse.Visible=false;
	    
                    ////Buyer terms drop-down
                    //ddlBuyerTerms.Items.Add(new ListItem ("30","30"));
                    //ddlBuyerTerms.Items.Add(new ListItem ("Cash","0"));
                    //ddlBuyerTerms.Items.Add(new ListItem ("45","45"));
                    //ddlBuyerTerms.Items.Add(new ListItem ("60","60"));
                    //ddlBuyerTerms.Items.Add(new ListItem ("90","90"));
	    
                    ////seller terms drop-down
                    //ddlSellerTerms.Items.Add(new ListItem ("30","30"));
                    //ddlSellerTerms.Items.Add(new ListItem ("Cash","0"));
                    //ddlSellerTerms.Items.Add(new ListItem ("45","45"));
                    //ddlSellerTerms.Items.Add(new ListItem ("60","60"));
                    //ddlSellerTerms.Items.Add(new ListItem ("90","90"));
	    
                    ////shipping terms drop-down
                    //ddlShippingTerms.Items.Add(new ListItem ("FOB Delivered","FOB/Delivered"));
                    //ddlShippingTerms.Items.Add(new ListItem ("FOB Shipping","FOB/Shipping"));
                    //ddlShippingTerms.Items.Add(new ListItem ("Delivered by TPE","Delivered Tpe"));
                    //ddlShippingTerms.Items.Add(new ListItem ("CIF","CIF"));
                    //ddlShippingTerms.Items.Add(new ListItem ("CFR","CFR"));
                    //ddlShippingTerms.Items.Add(new ListItem ("FAS","FAS"));
	    
					txtBuyPrice.Text =".000";
					txtSellPrice.Text =".000";
	//				txtCommission.Text ="0";
					// binding allocation
						// selecting admins
					using (SqlDataReader dtrAllocate = DBLibrary.GetDataReaderFromSelect(conn,"SELECT NAME=PERS_FRST_NAME+' '+PERS_LAST_NAME, PERS_ID FROM PERSON WHERE PERS_ID<>2 AND PERS_ENBL='1' AND (PERS_TYPE='B' OR PERS_TYPE='A' OR PERS_TYPE='T') ORDER BY NAME ASC"))
					{
						//binding the allocate dd list
						ddlAllocate.Items.Add ( new ListItem ("None","none" ) );
						while (dtrAllocate.Read())
						{
							ddlAllocate.Items.Add ( new ListItem ((string) dtrAllocate["Name"],dtrAllocate["PERS_ID"].ToString() ) );
						}
					}
					// if the user comes form the floor, pull in the offer details
					if (Request.QueryString["Offer"] !=null)
					{
						using (SqlDataReader dtrOffer = DBLibrary.GetDataReaderFromSelect(conn,"select * from bboffer where OFFR_ID ='"+Request.QueryString["Offer"].ToString()+"'"))
						{
							if (dtrOffer.Read())
							{
								//txtMelt.Text=dtrOffer["OFFR_MELT"].ToString();
								//txtDensity.Text=dtrOffer["OFFR_DENS"].ToString();
								//txtAdds.Text=dtrOffer["OFFR_ADDS"].ToString();
								txtDetail.Text = dtrOffer["OFFR_DETL"].ToString();
								txtQty.Text = dtrOffer["OFFR_QTY"].ToString();
								txtSellPrice.Text = dtrOffer["OFFR_PRCE_BUY"].ToString();
								txtBuyPrice.Text = dtrOffer["OFFR_PRCE"].ToString();
								//if (!ddlSize.Items.FindByValue(dtrOffer["OFFR_SIZE"].ToString()).Selected)
								//{
								

                                //ddlSize.Items.FindByValue(dtrOffer["OFFR_SIZE"].ToString()).Selected = true;
								
                                
                                //}
								if (dtrOffer["OFFR_COMP_ID"] != null)
								{
									try
									{
										ddlSeller_Comp.Items.FindByValue(dtrOffer["OFFR_COMP_ID"].ToString()).Selected = true;
									}
									catch
									{
										// company isn't in our system.  display alert
										lblSellerNotAvail.Text = "<font color=red><B>Not in System: " + dtrOffer["OFFR_COMP"].ToString()+"</B></font>";
									}
								}
								if (dtrOffer["OFFR_COMP_ID"] != null)
								{
									ddlProduct.Items.FindByValue(dtrOffer["OFFR_GRADE"].ToString()).Selected = true;
								}
								else
								{
								//	txtProduct.Text = dtrOffer["OFFR_PROD"].ToString();
								}


                                if (dtrOffer["OFFR_PROD"] != null)
                                {

                                    ddlSize.Items.Clear();

                                    ddlSize.Items.Add(new ListItem("Million Pounds", "1000000"));
                                    //  ddlQty.Items.Add(new ListItem("Thousand Barrels", "182"));
                                    // ddlQty.Items.Add(new ListItem("Thousand Barrels", "182"));

                                    int ProductID;
                                    ProductID = Int32.Parse(ddlProduct.SelectedValue);

                                    switch (ProductID)
                                    {
                                        case 2:
                                            ddlSize.Items.Add(new ListItem("Thousand Barrels", "182"));
                                            break;

                                        case 5:
                                            ddlSize.Items.Add(new ListItem("Thousand Barrels", "309"));
                                            break;

                                        default:
                                            ddlSize.Items.Add(new ListItem("Thousand Barrels", "125.8992"));
                                            break;
                                    }

                                }


							}
						}
					}


                  

                 


					Bind_Buyer();
					Bind_Seller();

                    lblBuyerEditUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=" + ddlBuyer.SelectedValue + "&CompanyID=" + ddlBuyer_Comp.SelectedValue + "\">Edit User</a>";
                    lblBuyerAddUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=0&CompanyID=0" + ddlBuyer_Comp.SelectedValue + "\">Add User</a>";

                    lblSellerEditUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=" + ddlSeller.SelectedValue + "&CompanyID=" + ddlSeller_Comp.SelectedValue + "\">Edit User</a>";
                    lblSellerAddUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=0&CompanyID=0" + ddlSeller_Comp.SelectedValue + "\">Add User</a>";
                    
                    //calculate the freight cost
					Calculate_Freight();
	    
					// binding Broker list info
					using (SqlDataReader dtrBroker= DBLibrary.GetDataReaderFromSelect(conn,"select pers_id, Pers_frst_name+' '+pers_last_name as PersName, BROKER_STANDARD_COMMISION from person where pers_type='b' OR PERS_TYPE='A' OR PERS_TYPE='T'"))
					{
						txtCommission.Text = "0";
						ddlBroker.DataSource = dtrBroker;
						ddlBroker.DataTextField= "PersName";
						ddlBroker.DataValueField= "pers_id";
						ddlBroker.DataBind();
					}
					ddlBroker.Items.Add(new ListItem ("None","" ));
					ddlBroker.SelectedIndex = ddlBroker.Items.Count-1;
	    
	    
					// binding Whwrehouse list
					
    
					using (SqlDataReader dtrWherehouse= DBLibrary.GetDataReaderFromSelect(conn,"select Plac_id, Plac_inst from place where plac_type='w'"))
					{
						ddlWarehouse.DataSource = dtrWherehouse;
						ddlWarehouse.DataTextField= "Plac_inst";
						ddlWarehouse.DataValueField= "Plac_id";
						ddlWarehouse.DataBind();
					}
				}
			}
		}
		// <summary>
		//  Handler that calls the functions to change values based on the
		//  company that is selected
		// </summary>
		public void Change_Seller(object sender, EventArgs e)
		{
			Bind_Seller();
			// freight will invariably change
			Calculate_Freight();

            lblSellerEditUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=" + ddlSeller.SelectedValue + "&CompanyID=" + ddlSeller_Comp.SelectedValue + "\">Edit User</a>";
            lblSellerAddUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=0&CompanyID=0" + ddlSeller_Comp.SelectedValue + "\">Add User</a>";

    
		}
		// <summary>
		//  Handler that calls the functions to change values based on the
		//  company that is selected
		// </summary>
		public void Change_Buyer(object sender, EventArgs e)
		{
			Bind_Buyer();
			// freight will invariably change
			Calculate_Freight();

            lblBuyerEditUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=" + ddlBuyer.SelectedValue + "&CompanyID=" + ddlBuyer_Comp.SelectedValue + "\">Edit User</a>";
            lblBuyerAddUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=0&CompanyID=0" + ddlBuyer_Comp.SelectedValue + "\">Add User</a>";

		}
		// <summary>
		//  Handler that calls the functions to change values based on the
		//  size of the order being selected.  Railcars must set shipping blank
		// </summary>
	
        
		public void Change_Warehouse(object sender, EventArgs e)
		{
			Calculate_Freight();
		}

		// <summary>
		//  Returns user the the primary screen
		// </summary>
		public void Click_Back(object sender, EventArgs e)
		{
			pnMain.Visible = true;
			pnConfirm.Visible = false;
		}
		
        
        // <summary>
		//  Creates and executes the script that processes the transaction
		// </summary>
		
        public void Click_Submit(object sender, EventArgs e)
		{
    
			decimal tax = 0;
			if(ViewState["Canadian_tax"] != null)
			{
				tax = Convert.ToDecimal(ViewState["Canadian_tax"].ToString());
			}
	
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
				SqlCommand cmdAddNew= new SqlCommand("spTransactionEngine_Spot", conn);
				cmdAddNew.CommandType = CommandType.StoredProcedure;
				cmdAddNew.Parameters.Add("@Qty",txtQty.Text);
				cmdAddNew.Parameters.Add("@PO",txtPO.Text);
				cmdAddNew.Parameters.Add("@Tax",tax);
				cmdAddNew.Parameters.Add("@Size",ddlSize.SelectedItem.Value);
			
				// adding locations
				SqlCommand cmdLocality;
				//  4 the buyer
				// select the locality
				//cmdLocality = new SqlCommand("Select PLAC_LOCL FROM Place WHERE PLAC_ID='"+ddlDelivery_Point.SelectedItem.Value+"'",conn);
				// add locality id
				//cmdAddNew.Parameters.Add("@Locl0",cmdLocality.ExecuteScalar().ToString());
				// add place id
				//cmdAddNew.Parameters.Add("@Locl1",ddlDelivery_Point.SelectedItem.Value);
    
				//  4 the Seller
				// select the locality
				//cmdLocality = new SqlCommand("Select PLAC_LOCL FROM Place WHERE PLAC_ID='"+ddlShipping_Point.SelectedItem.Value+"'",conn);
				// add locality id
				//cmdAddNew.Parameters.Add("@Local0",cmdLocality.ExecuteScalar().ToString());
				// add place id
				//cmdAddNew.Parameters.Add("@Local1",ddlShipping_Point.SelectedItem.Value);
    
				cmdAddNew.Parameters.Add("@SellComp",ddlSeller_Comp.SelectedItem.Value);
				cmdAddNew.Parameters.Add("@SellPers",ddlSeller.SelectedItem.Value);
				cmdAddNew.Parameters.Add("@BuyComp",ddlBuyer_Comp.SelectedItem.Value);
				cmdAddNew.Parameters.Add("@BuyPers",ddlBuyer.SelectedItem.Value);
				cmdAddNew.Parameters.Add("@Price", txtBuyPrice.Text);
				cmdAddNew.Parameters.Add("@SellPrice", txtSellPrice.Text);
				cmdAddNew.Parameters.Add("@Comm",txtCommission.Text);
				// other product overwrite the dropdown list
				// lblProduct already contains the correct string so the logic merely pulls it from there
				cmdAddNew.Parameters.Add("@Cont",lblProduct.Text);

				if (cbPrime.Checked)
					cmdAddNew.Parameters.Add("@Prime","1");
				else
					cmdAddNew.Parameters.Add("@Prime","0");


			
					cmdAddNew.Parameters.Add("@Grade",ddlProduct.SelectedValue.ToString());
			
                //cmdAddNew.Parameters.Add("@BuyerTerms",ddlBuyerTerms.SelectedItem.Value);
                //cmdAddNew.Parameters.Add("@SellerTerms",ddlSellerTerms.SelectedItem.Value);
				//cmdAddNew.Parameters.Add("@ShippingTerms",ddlShippingTerms.SelectedItem.Value);
    
				cmdAddNew.Parameters.Add("@Mark", (100 - Convert.ToDouble(txtCommission.Text)));
				cmdAddNew.Parameters.Add("@Detail",txtDetail.Text);
                //if (!txtMelt.Text.Equals(""))
                //{
                //    cmdAddNew.Parameters.Add("@Melt",txtMelt.Text);
                //}
                //if (!txtDensity.Text.Equals(""))
                //{
                //    cmdAddNew.Parameters.Add("@Dens",txtDensity.Text);
                //}
                //if (!txtAdds.Text.Equals(""))
                //{
                //    cmdAddNew.Parameters.Add("@Adds",txtAdds.Text);
                //}
				// freight for railcars it zero, not any more!
				//if (ddlSize.SelectedItem.Value.ToString().Equals("190000")){
				//    cmdAddNew.Parameters.Add("@Freight","0.000");
				//}else{
				//cmdAddNew.Parameters.Add("@Freight", txtFreight.Text);
				//}



                if (ddlSize.SelectedValue.ToString().Equals("290000"))
                {
					cmdAddNew.Parameters.Add("@Rc4Bt", "t");
					cmdAddNew.Parameters.Add("@WarehouseID", ddlWarehouse.SelectedItem.Value);
				}                
                else if (ddlSize.SelectedValue.ToString().Equals("390000"))
                {
                    cmdAddNew.Parameters.Add("@Rc4Tl", "t");
                    cmdAddNew.Parameters.Add("@WarehouseID", ddlWarehouse.SelectedItem.Value);
                }

    
				if(ddlBroker.SelectedItem.Value != "")
					cmdAddNew.Parameters.Add("@ShipBrokerID", ddlBroker.SelectedItem.Value);
				else
					cmdAddNew.Parameters.Add("@ShipBrokerID", null);
    
				// set quantity
				int qty = 0;
				//if(ddlSize.SelectedIndex == 0)
				//	qty = 4 * Convert.ToInt32(txtQty.Text);
				//else
				qty = Convert.ToInt32(txtQty.Text);
				cmdAddNew.Parameters.Add("@Quantity",qty.ToString());

				cmdAddNew.ExecuteNonQuery();
			}
    
    
			StringBuilder sb = new StringBuilder();
    
			sb.Append(" <table border=1 cellspacing=0 cellpadding=0 bordercolor=#212121 > ");
			sb.Append("       <tr> ");
			sb.Append("           <td> ");
    
			sb.Append(" 	         <div > ");
    
			sb.Append("               <center><h3>Spot Order Confirmation</h3></center> ");
			sb.Append("     <table border=0 cellspacing=4 cellpadding=4 align='center'> ");
			sb.Append(" 		            <tr valign=middle bgcolor=#cccccc > ");
    
			sb.Append("                       <td colspan=5 align='center'> ");
			sb.Append("                           <strong><span id='lblProduct'>"+lblProduct.Text+"</span></strong> </td>");
			sb.Append("                       </td>");
			sb.Append("                   </tr>");
			sb.Append("                   <tr>");
			sb.Append("                       <td></td>");
			sb.Append("                      <td><strong>User</strong></td>");
			sb.Append("                       <td><strong>Company</strong></td>");
			sb.Append("                       <td><strong>Location</strong></td>");
			sb.Append("                       <td><strong>Price</strong></td>");
			sb.Append("                   </tr>");
			sb.Append("                   <tr>");
			sb.Append("                       <td><strong>Buyer</strong></td>");
			sb.Append("                       <td><span id='lblBuyer'>"+lblBuyer.Text+"</span></td>");
			sb.Append("                       <td><span id='lblBuyerComp'>"+lblBuyerComp.Text+"</span></td>");
			sb.Append("                       <td><span id='lblBuyerLocal'>"+lblBuyerLocal.Text+"</span></td>");
			sb.Append("                       <td>$<span id='lblBuyPrice'>"+lblBuyPrice.Text+"</span></td>");
			sb.Append("                   </tr>");
			sb.Append("                   <tr>");
			sb.Append("                      <td><strong>Seller</strong></td>");
			sb.Append("                       <td><span id='lblSeller'>"+lblSeller.Text+"</span></td>");
			sb.Append("                       <td><span id='lblSellerComp'>"+lblSellerComp.Text+"</span></td>");
			sb.Append("                      <td><span id='lblSellerLocal'>"+lblSellerLocal.Text+"</span></td>");
			sb.Append("                       <td><font color=red>$(<span id='lblSellPrice'>"+lblSellPrice.Text+"</span>)</font></td>");
			sb.Append("                   </tr>");
			sb.Append("                   <tr>");
			sb.Append(" 	             <td><strong>Commission</strong></td>");
			sb.Append(" 	             <td colspan ='3' align='center'></td>");
			sb.Append(" 	             <td><font color=red>$(<span id='lblComm'>"+lblComm.Text+"</span>)</font></td>");
			sb.Append("                   </tr>");
			sb.Append("                   <tr>");
			sb.Append("                       <td><strong>Total Freight</strong></td>");
			sb.Append("                       <td colspan ='3' align='center'>");
			sb.Append("                           <font color=red><span id='lblFreightMessage'></span></font>");
			sb.Append("                       </td>");
			sb.Append("                       <td><font color=red>$(<span id='lblFreight'>"+lblFreight.Text+"</span>)</font></td>");
			sb.Append("                   </tr>");
			sb.Append("  		           <tr>");
			sb.Append("                       <td><strong>Profit</strong></td>");
			sb.Append("                       <td></td>");
			sb.Append("                       <td></td>");
			sb.Append("                       <td></td>");
			sb.Append("                       <td><strong>$<span id='lblMarkup'>"+String.Format("{0:c}",lblMarkup.Text)+"</span></strong></td>");
			sb.Append("                   </tr>");
			sb.Append("               </table>");
			sb.Append("         </div>");
			sb.Append("               </td>");
			sb.Append("            </tr>");
			sb.Append("         </table>");
    
    
            //MailMessage mail;
            //mail = new MailMessage();
            //mail.From = "Website";
            //mail.To = Application["strEmailOwner"].ToString();
            //mail.Cc = Application["strEmailAdmin"].ToString();
            //mail.Subject = "Spot Transaction - Profit=$"+String.Format("{0:c}",lblMarkup.Text);
            //mail.Body = sb.ToString();
            //mail.BodyFormat = MailFormat.Html;
            ////SmtpMail.SmtpServer="localhost";
            //TPE.Utility.EmailLibrary.Send(Context,mail);
    
			// redirect user
			//Response.Redirect("/Common/Filled_Orders.aspx");

			// swap panels

			pnMain.Visible = true;
			pnConfirm.Visible = false;
    
    
		}
    
		// <summary>
		//  Opens up the confirmation screen
		// </summary>
		public void Click_Continue(object sender, EventArgs e)
		{


			if(ViewState["Canadian_tax"] != null)
			{
				lblTax.Text = ViewState["Canadian_tax"].ToString();
				lblTaxName.Text = ViewState["Canadian_tax_name"].ToString();
			}

			//Response.Write(ddlDelivery_Point.SelectedItem.ToString()+" | "+ddlShipping_Point.SelectedItem.ToString());
			// text box values transfer to the label controls
			lblBuyer.Text = ddlBuyer.SelectedItem.ToString();
			lblBuyerComp.Text = ddlBuyer_Comp.SelectedItem.ToString();
//			lblBuyerLocal.Text = ddlDelivery_Point.SelectedItem.ToString();
			lblBuyPrice.Text = "0" + txtBuyPrice.Text;
    
			lblSeller.Text = ddlSeller.SelectedItem.ToString();
			lblSellerComp.Text = ddlSeller_Comp.SelectedItem.ToString();
//			lblSellerLocal.Text = ddlShipping_Point.SelectedItem.ToString();
			lblSellPrice.Text = "0" + txtSellPrice.Text;
			// freight on rail cars is free
			if (ddlSize.SelectedItem.Value.ToString().Equals("190000"))
			{
				//txtFreight.Text =".000";
				//lblFreightMessage.Text ="Freight disregarded for R.C.";
				lblFreightMessage.Text ="";
			}
			else
			{
				lblFreightMessage.Text ="";
			}
			//lblFreight.Text = (Convert.ToDouble(txtFreight.Text)).ToString();// * Convert.ToDouble(txtQty.Text)).ToString();
			lblComm.Text = Convert.ToString(Convert.ToInt32(txtCommission.Text));

			
            //if (ddlSize.SelectedItem.Value == "290000")
            //{
            //    //freight amount needs to be added 4 times to be accurate for 4 BTs
            //    //lblFreight.Text = ((Convert.ToDouble(txtFreight.Text)) * 4).ToString();
            //    // hard code the value for size since ddlSize doesn't have the true number in it
            //    lblMarkup.Text = Math.Round( ( (Convert.ToDouble(txtBuyPrice.Text)-Convert.ToDouble(txtSellPrice.Text))* Convert.ToDouble(190000) * Convert.ToDouble(txtQty.Text) - Convert.ToDouble(lblFreight.Text)) * (100.0 - Convert.ToDouble(txtCommission.Text)) / 100.0, 2).ToString();

            //}

            //else
            //{
            //    lblMarkup.Text = Math.Round( ( (Convert.ToDouble(txtBuyPrice.Text)-Convert.ToDouble(txtSellPrice.Text))* Convert.ToDouble(ddlSize.SelectedItem.Value) * Convert.ToDouble(txtQty.Text) - Convert.ToDouble(lblFreight.Text)) * (100.0 - Convert.ToDouble(txtCommission.Text)) / 100.0, 2).ToString();
            //}


			
				lblProduct.Text=ddlProduct.SelectedItem.ToString();
			


			BindCompanyDg();

			// swaps panels
			pnMain.Visible = false;
			pnConfirm.Visible = true;
		}


        protected void ddlProduct_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int ProductID;
            ProductID = Int32.Parse(ddlProduct.SelectedValue);

            switch (ProductID)
            {
                case 1:
                    lblBuyPriceLabel.Text = " per lb.";
                    lblSellPriceLabel.Text = " per lb.";
                    //ddlQty.SelectedValue = "1000000";
                    break;

                case 2:
                    lblBuyPriceLabel.Text = " per lb.";
                    lblSellPriceLabel.Text = " per lb.";
                    //ddlQty.SelectedValue = "125.8992";
                    break;

                case 3:
                    lblBuyPriceLabel.Text = " per lb.";
                    lblSellPriceLabel.Text = " per lb.";
                    //ddlQty.SelectedValue = "1000000";
                    break;

                case 4:
                    lblBuyPriceLabel.Text = " per lb.";
                    lblSellPriceLabel.Text = " per lb.";
                    //ddlQty.SelectedValue = "1000000";
                    break;

                case 5:
                    lblBuyPriceLabel.Text = " per gal.";
                    lblSellPriceLabel.Text = " per gal.";
                    //ddlQty.SelectedValue = "125.8992";
                    break;

                default:
                    lblBuyPriceLabel.Text = " per lb.";
                    lblSellPriceLabel.Text = " per lb.";
                    //ddlQty.SelectedValue = "1000000";
                    break;
            }
        }


		private void BindCompanyDg( )
		{
			StringBuilder sbSQL = new StringBuilder();
			SqlDataAdapter dadContent;
			DataSet dstContent;
			
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();

				sbSQL.Append("Exec spCurrARAPAging");

				//Response.Write("viewstate = " + ViewState["Sort"].ToString());

				sbSQL.Append(" @CompanyID='" + ddlBuyer_Comp.SelectedValue + "'");			
						
				sbSQL.Append(" ,@Type='R'");
						
				dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
				//Response.Write(sbSQL.ToString());
				dstContent = new DataSet();
				dadContent.Fill(dstContent);
				dg.DataSource = dstContent;
				dg.DataBind();

				if(dstContent.Tables[0].Rows.Count == 0)
				{
					dg.Visible = false;
					lblCreditInformation.Text = "Credit information of " + ddlBuyer_Comp.SelectedItem.Text + " doesn't exist.";
				}
				else
				{
					dg.Visible = true;
					lblCreditInformation.Text = "Credit information of " + ddlBuyer_Comp.SelectedItem.Text + ".";
				}	
			}		
		}

		// <summary>
		//  Handlers calls the freight function to update the freight
		// </summary>
		public void Change_City(object sender, EventArgs e)
		{
			Calculate_Freight();
		}
		// <summary>
		//  Updates the frieght field based on which two cities are currently selected
		// </summary>
    
		public void Calculate_Freight()
		{
			/*
			if(ddlSize.SelectedItem.Value != "190000" && ddlSize.SelectedItem.Value != "290000")
			{
				//Response.Write("test<br>");
				string state_from = "";
				string state_to = "";
				double price_shipment = 0;

				//split ddlShipping_Point value
				//string delimiter = "-";
				//char[] split1 = delimiter.ToCharArray();
				//string[] tabSplit =	 ddlShipping

				//get PLAC_LOCL using PLAC_ID for shipping_point
				string selectedIdShip = "";
				SqlConnection conn0 = new SqlConnection(Application["DBconn"].ToString());
				conn0.Open();
				string strSQL0="SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID='"+ddlShipping_Point.SelectedItem.Value+"'";
				SqlCommand cmdSelect0;
				cmdSelect0= new SqlCommand(strSQL0, conn0);
				SqlDataReader dtrSelect0= cmdSelect0.ExecuteReader();
				while(dtrSelect0.Read())
				{
					selectedIdShip = dtrSelect0["PLAC_LOCL"].ToString();
				}
				conn0.Close();
				
				// get the state of the shipping point
				SqlConnection conn1 = new SqlConnection(Application["DBconn"].ToString());
				conn1.Open();
				string strSQL1="SELECT LOCL_STAT FROM LOCALITY WHERE LOCL_ID='"+selectedIdShip+"'";
				SqlCommand cmdSelect1;
				cmdSelect1= new SqlCommand(strSQL1, conn1);
				SqlDataReader dtrSelect1= cmdSelect1.ExecuteReader();
				while(dtrSelect1.Read())
				{
					state_from = dtrSelect1["LOCL_STAT"].ToString();
				}
				conn1.Close();

				//get PLAC_LOCL using PLAC_ID for delivery_point
				string selectedIdDel = "";
				SqlConnection conn01 = new SqlConnection(Application["DBconn"].ToString());
				conn01.Open();
				string strSQL01="SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID='"+ddlDelivery_Point.SelectedItem.Value+"'";
				SqlCommand cmdSelect01;
				cmdSelect01= new SqlCommand(strSQL01, conn01);
				SqlDataReader dtrSelect01= cmdSelect01.ExecuteReader();
				while(dtrSelect01.Read())
				{
					selectedIdDel = dtrSelect01["PLAC_LOCL"].ToString();
				}
				conn01.Close();

				// get the state of the delivery point
				SqlConnection conn11 = new SqlConnection(Application["DBconn"].ToString());
				conn11.Open();
				string strSQL11="SELECT LOCL_STAT FROM LOCALITY WHERE LOCL_ID='"+selectedIdDel+"'";
				SqlCommand cmdSelect11;
				cmdSelect11= new SqlCommand(strSQL11, conn11);
				SqlDataReader dtrSelect11= cmdSelect11.ExecuteReader();
				while(dtrSelect11.Read())
				{
					state_to = dtrSelect11["LOCL_STAT"].ToString();
				}
				conn11.Close();

				// get the the price shipment in freight matrix
				SqlConnection conn3 = new SqlConnection(Application["DBconn"].ToString());
				conn3.Open();
				string strSQL3="SELECT price FROM FREIGHT_MATRIX_TRUCKLOAD WHERE state_from LIKE '"+state_from+"%' AND state_to LIKE '"+state_to+"%'";
				SqlCommand cmdSelect3;
				cmdSelect3= new SqlCommand(strSQL3, conn3);
				SqlDataReader dtrSelect3= cmdSelect3.ExecuteReader();
				int cpt=0;
				while(dtrSelect3.Read())
				{
					price_shipment = (Convert.ToDouble(price_shipment) + Convert.ToDouble(dtrSelect3["price"].ToString()));
					++cpt;
				}
				price_shipment = (Convert.ToDouble(price_shipment)/cpt);
				conn3.Close();
				
				SqlConnection conn2 = new SqlConnection(Application["DBconn"].ToString());
				conn2.Open();
				string strSQL2="SELECT DISTANCE FROM DISTANCES WHERE CITY_FROM_ID='"+selectedIdShip+"' AND CITY_TO_ID='"+selectedIdDel+"'";
				SqlCommand cmdSelect;
				cmdSelect= new SqlCommand(strSQL2, conn2);
				SqlDataReader dtrSelect= cmdSelect.ExecuteReader();
				while(dtrSelect.Read())
				{
					double result = Convert.ToDouble(Convert.ToInt32(dtrSelect["DISTANCE"].ToString())*1.10* Convert.ToDouble(price_shipment));// / Convert.ToInt32(ddlSize.SelectedItem.Value));
					txtFreight.Text = result.ToString(".##");
				}  
				conn2.Close();	
			}
			else
				txtFreight.Text = "0";
				*/
    
				
		}
		// <summary>
		//  Primary function that binds content to buyer controls
		// </summary>
		public void Bind_Buyer()
		{
    		//
			// adding all the possible users within the company
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{			
				conn.Open();
				//	Pers_Type<>'O' clause removed.
				//             cmdUsers = new SqlCommand("Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME as PERS_NAME,PERS_ID FROM PERSON WHERE PERS_TYPE<>'O' AND PERS_COMP="+ddlBuyer_Comp.SelectedItem.Value+" ORDER By PERS_TYPE DESC",conn);
				using (SqlDataReader dtrUsers = DBLibrary.GetDataReaderFromSelect(conn,"Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME as PERS_NAME,PERS_ID FROM PERSON WHERE PERS_ENBL=1 AND PERS_COMP="+ddlBuyer_Comp.SelectedItem.Value+" ORDER By PERS_TYPE DESC"))
				{
					ddlBuyer.Items.Clear();
					while(dtrUsers.Read())
					{
						ddlBuyer.Items.Add ( new ListItem ((string) dtrUsers["PERS_NAME"],dtrUsers["PERS_ID"].ToString() ) );
    
					}
				}
    
				//
                //// adding all the possible locations within the company		
                //using (SqlDataReader dtrLocations = DBLibrary.GetDataReaderFromSelect(conn,"select PLAC_LOCL,PLAC_ID ,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) AS Location from place where plac_comp='"+ddlBuyer_Comp.SelectedItem.Value+"'"))
                //{
                //    ddlDelivery_Point.Items.Clear();
                //    while(dtrLocations.Read())
                //    {
                //        ddlDelivery_Point.Items.Add ( new ListItem (dtrLocations["LOCATION"].ToString(),dtrLocations["PLAC_ID"].ToString() ) );   
                //    }
                //}
			
				//add the warehouses!!
                //using (SqlDataReader dtrLocations2 = DBLibrary.GetDataReaderFromSelect(conn,"select  PLAC_ID, LABEL = (select LOCL_CITY +', '+ LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) + ' ('+PLAC_LABL+')'  from place where plac_type='W'  ORDER BY LABEL"))
                //{
                //    while(dtrLocations2.Read())
                //    {
                //        ddlDelivery_Point.Items.Add ( new ListItem (dtrLocations2["LABEL"].ToString(),dtrLocations2["PLAC_ID"].ToString() ) );
                //    }
                //}
			}	
			applyTaxes();
		}
		// <summary>
		//  Primary function that binds content to seller controls
		// </summary>
		public void Bind_Seller()
		{
			//
			// adding all the possible users within the company
			
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
				using (SqlDataReader dtrUsers = DBLibrary.GetDataReaderFromSelect(conn,"Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME as PERS_NAME,PERS_ID FROM PERSON WHERE PERS_ENBL=1 AND PERS_COMP="+ddlSeller_Comp.SelectedItem.Value+" ORDER By PERS_TYPE DESC"))
				{
					ddlSeller.Items.Clear();
					while(dtrUsers.Read())
					{
						ddlSeller.Items.Add ( new ListItem ((string) dtrUsers["PERS_NAME"],dtrUsers["PERS_ID"].ToString() ) );
    
					}
				}
			
				//
				// adding all the possible locations within the company
			
    
                //using (SqlDataReader dtrLocations = DBLibrary.GetDataReaderFromSelect(conn,"select PLAC_LOCL,PLAC_ID ,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) AS Location from place where plac_comp='"+ddlSeller_Comp.SelectedItem.Value+"'"))
                //{
                //    ddlShipping_Point.Items.Clear();
                //    while(dtrLocations.Read())
                //    {
                //        ddlShipping_Point.Items.Add ( new ListItem (dtrLocations["LOCATION"].ToString(),dtrLocations["PLAC_ID"].ToString() ) );
    
                //    }
                //}
			
                ////add the warehouses!!
                //using (SqlDataReader dtrWarehouses = DBLibrary.GetDataReaderFromSelect(conn,"select  PLAC_ID, LABEL = (select LOCL_CITY +', '+ LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) + ' ('+PLAC_LABL+')'  from place where plac_type='W'  ORDER BY LABEL"))
                //{
                //    while(dtrWarehouses.Read())
                //    {
                //        ddlShipping_Point.Items.Add ( new ListItem (dtrWarehouses["LABEL"].ToString(),dtrWarehouses["PLAC_ID"].ToString() ) );
                //    }
                //}
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void ddlBroker_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Hashtable htParam = new Hashtable();
			htParam.Add("@pers_id",ddlBroker.SelectedValue);
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
    
				using (SqlDataReader dtrBroker = DBLibrary.GetDataReaderFromSelect(conn,"select pers_id, BROKER_STANDARD_COMMISION from person where pers_id=@pers_id",htParam))
				{
					if(dtrBroker.Read())
					{
						txtCommission.Text = HelperFunction.getSafeLongFromDB(dtrBroker["BROKER_STANDARD_COMMISION"]).ToString();
					}
				}
			}		
		}

		void applyTaxes()
		{
            //decimal tax = 0;
            //tax = HelperFunction.returnTaxByPlace(ddlDelivery_Point.SelectedValue, this.Context);
            //ViewState["Canadian_tax"] = tax;
            //ViewState["Canadian_tax_name"] = HelperFunction.returnTaxName(ddlDelivery_Point.SelectedValue, this.Context);
		}

		protected void ddlDelivery_Point_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			applyTaxes();
		
		}


        public void LoadQuantityList(object sender, System.EventArgs e)
        {

            ddlSize.Items.Clear();

            ddlSize.Items.Add(new ListItem("Million Pounds", "1000000"));
            //  ddlQty.Items.Add(new ListItem("Thousand Barrels", "182"));
            // ddlQty.Items.Add(new ListItem("Thousand Barrels", "182"));

            int ProductID;
            ProductID = Int32.Parse(ddlProduct.SelectedValue);

            switch (ProductID)
            {
                case 2:
                    ddlSize.Items.Add(new ListItem("Thousand Barrels", "182"));
                    break;

                case 5:
                    ddlSize.Items.Add(new ListItem("Thousand Barrels", "309"));
                    break;

                default:
                    ddlSize.Items.Add(new ListItem("Thousand Barrels", "125.8992"));
                    break;
            }

        }

	}
}
