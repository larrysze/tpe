<%@ Control Language="C#" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Web.Mail" %>
<script runat="server">

		/****************************************************************************
          '*   1. File Name       : Spot/Spot_Grid.ascx                                *
          '*   2. Description     : User control for the grid of the spot floor        *
          '*						                                                   *
          '*   3. Modification Log:                                                    *
          '*     Ver No.       Date          Author             Modification           *
          '*   -----------------------------------------------------------------       *
          '*      1.00      5-25-2004       Zach                  Comment              *
          '*                                                                           *
          '*****************************************************************************/
         //public string UserType;
         public bool International = false;
         public string  Filter ="";
         public string Screen = "Spot_Floor";
         public void Page_Load(object sender, EventArgs e){

             //all user can access spot floor
             if (Session["Typ"]=="" ) Response.Redirect("/default.aspx");
             ViewState["UserType"] = Session["Typ"].ToString();
            
				
             if (!IsPostBack){
                 // populate search box with data from querystring 
				if (Request.QueryString["Filter"] != null && txtSearch.Text ==""  )
				{
					txtSearch.Text = Request.QueryString["Filter"].ToString();
				}
				
				
                //check if it's export transction
                if (International){
                   ViewState["Export"] =true;
                   if (ViewState["UserType"].ToString().Equals("A")){
                       dgAdmin.Columns[4].HeaderText ="Metric Tons";
                   }else{
                       dgUser.Columns[3].HeaderText ="Metric Tons";
                   }
                   //pnInternational.Visible = true;
                   //pnInternationalInstruct.Visible= true;
                   //pnDomesticInstruct.Visible = false;
                }else{
                   ViewState["Export"] =false;
                   //pnDomestic.Visible = true;
                   //pnDomesticInstruct.Visible = true;
                   //pnInternationalInstruct.Visible= false;

                }
                // define the default sort function
                ViewState["Sort"] = "VARID DESC";
                //lblSort.Text = "VARID DESC";
                ddlResinType.Items.Add(new ListItem ("All","*"));
                ddlResinType.Items.Add(new ListItem ("HDPE","HDPE"));
                ddlResinType.Items.Add(new ListItem ("LDPE","LDPE"));
                ddlResinType.Items.Add(new ListItem ("LLDPE","LLDPE"));
                ddlResinType.Items.Add(new ListItem ("PS","PS"));
                ddlResinType.Items.Add(new ListItem ("PP","PP"));
                ddlResinType.Items.Add(new ListItem ("Engineering","Engineering"));

                // add the default value for the drop down lists


                if (Request.QueryString["StartOn"] != null){
                    if (Request.QueryString["StartOn"].ToString().Equals("bid")){
                        ddlCompanies.Items.Add(new ListItem ("All Bids","*Bid"));
                    }
                }
                ddlCompanies.Items.Add(new ListItem ("All Offers","*"));
                ddlCompanies.Items.Add(new ListItem ("All Bids","*Bid"));
                if (ViewState["UserType"].ToString().Equals("A")){
                   SqlConnection conn2;
                   conn2 = new SqlConnection(Application["DBConn"].ToString());
                   conn2.Open();
                   SqlCommand cmdCompanies;
                   SqlDataReader dtrCompanies;
                   cmdCompanies= new SqlCommand("SELECT * FROM (SELECT COMP_ID,COMP_NAME,COMP_TYPE FROM COMPANY WHERE COMP_REG=1 AND COMP_TYPE IN ('P','S','D'))Q ORDER BY COMP_NAME", conn2);
                   dtrCompanies= cmdCompanies.ExecuteReader();
                   while (dtrCompanies.Read()){
                      ddlCompanies.Items.Add(new ListItem (dtrCompanies["COMP_NAME"].ToString(),dtrCompanies["COMP_ID"].ToString()));
                   }

                   dtrCompanies.Close();
                   conn2.Close();
                }else{
                   btnEmailOffers.Visible = false;
                   btnArchive.Visible = false;
                   btnDelete.Visible = false;
                   btnHot.Visible = false;
                   btnCool.Visible = false;
                }

                // if the Parent isn't the spot floor, the following controls must be set to invisible

                if (!Screen.Equals("Spot_Floor")){
                    pnTopBar.Visible = false;
                }

                PutSearchFieldsBackSession();
                ResetHeaders();
                BindDataGrid();
             }
         }

         public string Parse(string TextIN){
          //  remove ' character
           string Parse;
           Parse = TextIN;
           while (Parse.IndexOf("'") > 0){
               Parse = Parse.Replace("'", "");
           }
           // remove " character
           while (Parse.IndexOf("\"") > 0){
               Parse = Parse.Replace("\"", "");
           }
           return Parse;

         }

         private void KeepSearchFieldsSession()
		 {
			if (ddlCompanies.Visible==true) Session["Spot_Grid.ddlCompanies.SelectedItem.Value"] = ddlCompanies.SelectedItem.Value;
			if (ddlResinType.Visible==true) Session["Spot_Grid.ddlResinType.SelectedItem.Value"] = ddlResinType.SelectedItem.Value;
			if (txtSearch.Visible==true && Request.QueryString["Filter"] == null) Session["Spot_Grid.txtSearch.Text"] = txtSearch.Text;
		 }
		 
		 private void PutSearchFieldsBackSession()
		 {
			if (!EmptySession("Spot_Grid.ddlCompanies.SelectedItem.Value") && (ddlCompanies.Visible==true))
			{
				ddlCompanies.SelectedItem.Selected = false;
				ddlCompanies.Items.FindByValue(Session["Spot_Grid.ddlCompanies.SelectedItem.Value"].ToString()).Selected = true;
			}
			
			if (!EmptySession("Spot_Grid.ddlResinType.SelectedItem.Value") && (ddlResinType.Visible==true))
			{
				ddlResinType.SelectedItem.Selected = false;
				ddlResinType.Items.FindByValue(Session["Spot_Grid.ddlResinType.SelectedItem.Value"].ToString()).Selected = true;
			}
			
			if (!EmptySession("Spot_Grid.txtSearch.Text") && (txtSearch.Visible==true))
			{
				txtSearch.Text = Session["Spot_Grid.txtSearch.Text"].ToString();
			}
		 }
         
         private bool EmptySession(string entryName)
         {
			if ((Session[entryName]==null) || Session[entryName]=="")
			{
				return true;
			}
			else
			{
				return false;
			}
         }
         
         public void BindDataGrid(){

             SqlConnection conn;
             string strSQL;
             conn = new SqlConnection(Application["DBConn"].ToString());
             conn.Open();
             SqlDataAdapter dadContent;
             DataSet dstContent;

             strSQL = "Exec spSpot_Floor ";

             // define users
             if (!ddlCompanies.SelectedItem.Value.Equals("*Bid")){
                strSQL +="@UserType='S'";
               // lblSubtitle.Text = "All offers subject to prior sale";
             }else{
                strSQL +="@UserType='P'";
                // update lable

                btnEmailOffers.Text = "Email Bids";


             }
             // flag to be set if this is one the import/export
             if ((bool)ViewState["Export"]){
               strSQL +=" ,@Export='true'";

             }

             // add resin filter

             // dashboard requires that LLDPE be seperate from LPDE.
             // this logic already exists if you use @Resin_Filter instead of the normal @Search
             // i know this is a shitty hack -- zach 7/1/2004
             if (!ddlResinType.SelectedItem.Value.Equals("*") && !(Filter.Equals("LDPE - Film"))){
                strSQL +=" ,@Resin_Filter='"+ddlResinType.SelectedItem.Value.ToString()+"'";
             }
             // add company filter
             if (!ddlCompanies.SelectedItem.Value.Equals("*") && !ddlCompanies.SelectedItem.Value.Equals("*Bid")){
                 strSQL +=",@CompID='"+ddlCompanies.SelectedItem.Value.ToString()+"'";
             }


             // add a search in if the search
             if (!txtSearch.Text.Equals("")){
                strSQL +=",@Search='"+Parse(txtSearch.Text)+"'";

             }
             // the dashboard filters
             if (!Filter.Equals("")){
                if (Filter.Equals("LDPE - Film"))
                {
                    strSQL +=",@Resin_Filter='LDPE',@Search='Film'";
                }
                else if (Filter.Equals("LDPE - inj"))
                {
                    strSQL +=",@Resin_Filter='LDPE',@Search='inj'";
                }
                else
                {
                    strSQL +=",@Search='"+Filter+"'";
                }
             }


             // add the sort command
             strSQL +=",@Sort='"+ViewState["Sort"].ToString()+"'";
             //Response.Write(strSQL);
             dadContent = new SqlDataAdapter(strSQL,conn);
             dstContent = new DataSet();
             dadContent.Fill(dstContent);

             if (ViewState["UserType"].ToString().Equals("A")){
               dgAdmin.DataSource = dstContent;
               dgAdmin.DataBind();
             }else{
               dgUser.DataSource = dstContent;
               dgUser.DataBind();
             }
             conn.Close();
             
             KeepSearchFieldsSession();
         }
         //<summary>
         // Handler for when the user changes the resin filter
         //</summary>
         public void Click_Search(object sender, EventArgs e){
            ResetHeaders();
            BindDataGrid();
         }
         //<summary>
         // Handler for when the user changes the resin filter
         //</summary>
         public void Change_Resin(object sender, EventArgs e){
            ResetHeaders();
            BindDataGrid();
         }
         //<summary>
         // Handler for when the user changes the company
         //</summary>
         public void Change_Company(object sender, EventArgs e){
            ResetHeaders();
            BindDataGrid();
         }
         //<summary>
         // create string of spot bids in a session variable and send the person to the demo leads screen
         //</summary>
         public void Click_Send_Mass_Mail(object sender, EventArgs e){
             RowSelectorColumn rsc;
             if (ViewState["UserType"].ToString().Equals("A")){
                rsc = RowSelectorColumn.FindColumn(dgAdmin);
             }else{
                rsc = RowSelectorColumn.FindColumn(dgUser);
             }
             System.Text.StringBuilder sbOrders;
             sbOrders = new System.Text.StringBuilder();
             for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++){
               int selIndex = rsc.SelectedIndexes[selectedIndex];
               if (ViewState["UserType"].ToString().Equals("A")){
                   sbOrders.Append(dgAdmin.DataKeys[selIndex].ToString() +",");
               }else{
                   sbOrders.Append(dgUser.DataKeys[selIndex].ToString() +",");
               }

             }
               // if bids are selected the 'offers' being sent must be bids
             if (ddlCompanies.SelectedItem.Value.Equals("*Bid")){
               Session["strCompId"] ="-1"; // bids
             }
             else
             {
				 Session["strCompId"]=null; //offers
			 }
             // system must pass whether they are export bids/ offers
             if ((bool)ViewState["Export"]){
                Session["strExport"] ="-1";
             }
             Session["strEmailOffer_IDS"] =  sbOrders.ToString();
             // send the user to the demo leads page to select users
             Response.Redirect ("/Administrator/CRM.aspx?EmailOffers=true");
         }

		 //<summary>
         // moves the item to the archieve
         //</summary>
         public void Click_Move_To_Archive(object sender, EventArgs e){
            RowSelectorColumn rsc;
            rsc = RowSelectorColumn.FindColumn(dgAdmin);
			string SQL;
			
			SqlConnection conn;
            conn = new SqlConnection(Application["DBConn"].ToString());
            conn.Open();
            SqlCommand cmd;
            for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++){
                int selIndex = rsc.SelectedIndexes[selectedIndex];
                   Application.Lock();
                   if (ddlCompanies.SelectedItem.Value.Equals("*Bid"))
                   {
						SQL = "exec spSendToArchive @Is_Bid='1',@ID='"+dgAdmin.DataKeys[selIndex].ToString()+"'";
                   }
                   else
                   {
						SQL = "exec spSendToArchive  @ID='"+dgAdmin.DataKeys[selIndex].ToString()+"'";
                   }
                   
                   
                  
                   cmd= new SqlCommand(SQL, conn);
                   cmd.ExecuteNonQuery();
                   Application.UnLock();


             }
             conn.Close();
             BindDataGrid();
         }
         
         //<summary>
         // makes offer 'hot' which posts the offer to the home page scollbar
         //</summary>
         public void Click_MakeHot(object sender, EventArgs e){
            RowSelectorColumn rsc;
            rsc = RowSelectorColumn.FindColumn(dgAdmin);


            SqlConnection conn;
            conn = new SqlConnection(Application["DBConn"].ToString());
            conn.Open();
            SqlCommand cmd;
            for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++){
                int selIndex = rsc.SelectedIndexes[selectedIndex];
                   Application.Lock();
                   cmd= new SqlCommand("UPDATE BBOFFER SET OFFR_HOT ='1' WHERE OFFR_ID="+ dgAdmin.DataKeys[selIndex].ToString(), conn);
                   cmd.ExecuteNonQuery();
                   Application.UnLock();


             }
             conn.Close();
             BindDataGrid();
         }
          //<summary>
         // makes offer 'ccol' which removes featured offer from the home page scollbar
         //</summary>
         public void Click_MakeCool(object sender, EventArgs e){
          RowSelectorColumn rsc;
            rsc = RowSelectorColumn.FindColumn(dgAdmin);


            SqlConnection conn;
            conn = new SqlConnection(Application["DBConn"].ToString());
            conn.Open();
            SqlCommand cmd;
            for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++){
                int selIndex = rsc.SelectedIndexes[selectedIndex];
                   Application.Lock();
                   cmd= new SqlCommand("UPDATE BBOFFER SET OFFR_HOT ='0' WHERE OFFR_ID="+ dgAdmin.DataKeys[selIndex].ToString(), conn);
                   cmd.ExecuteNonQuery();
                   Application.UnLock();


             }
             conn.Close();
             BindDataGrid();

         }


          //<summary>
         // deletes the offer
         //</summary>
         public void Click_Delete(object sender, EventArgs e){
             RowSelectorColumn rsc;
             if (ViewState["UserType"].ToString().Equals("A")){
                rsc = RowSelectorColumn.FindColumn(dgAdmin);
             }else{
                rsc = RowSelectorColumn.FindColumn(dgUser);
             }

             SqlConnection conn;
             conn = new SqlConnection(Application["DBConn"].ToString());
             conn.Open();
             SqlCommand cmdDelete;
             for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++){
                int selIndex = rsc.SelectedIndexes[selectedIndex];
                   Application.Lock();
                   if (ViewState["UserType"].Equals("P") || ddlCompanies.SelectedItem.Value.Equals("*Bid")){
                      if (ViewState["UserType"].ToString().Equals("A")){
                           cmdDelete= new SqlCommand("Exec spPWOBB @bidid="+ dgAdmin.DataKeys[selIndex].ToString(), conn);
                      }else{
                           cmdDelete= new SqlCommand("Exec spPWOBB @bidid="+ dgUser.DataKeys[selIndex].ToString(), conn);
                      }
                      cmdDelete.ExecuteNonQuery();
                   }else{
                      if (ViewState["UserType"].ToString().Equals("A")){
                           cmdDelete= new SqlCommand("Exec spSWOBB @offrid="+ dgAdmin.DataKeys[selIndex].ToString(), conn);
                      }else{
                           cmdDelete= new SqlCommand("Exec spSWOBB @offrid="+ dgUser.DataKeys[selIndex].ToString(), conn);
                      }
                      cmdDelete.ExecuteNonQuery();
                   }
                   Application.UnLock();

             }
             conn.Close();
             ResetHeaders();
             BindDataGrid();
         }
         // <summary>
         // Sorting function
         // </summary>
         public void SortDG(Object sender, DataGridSortCommandEventArgs e ){
            string[] SortExprs;
            string CurrentSearchMode;
            string NewSearchMode;
            string ColumnToSort;
            string NewSortExpr;
            //  Parse the sort expression - delimiter space
            Regex r = new Regex(" "); // Split on spaces.
            SortExprs = r.Split(e.SortExpression) ;
            ColumnToSort = SortExprs[0];
                // If a sort order is specified get it, else default is descending
            if (SortExprs.Length > 1){
                CurrentSearchMode = SortExprs[1].ToUpper();
                if (CurrentSearchMode == "ASC"){
                    NewSearchMode = "DESC";
                }else{
                    NewSearchMode = "ASC";
                }
            }else{   // If no mode specified, Default is descending
                NewSearchMode = "DESC";
            }
            //  Derive the new sort expression.
            NewSortExpr = ColumnToSort + " " + NewSearchMode;
            // Figure out the column index
            int iIndex;
            iIndex = 1;
                if (ViewState["UserType"].ToString().Equals("A")){
                   switch(ColumnToSort.ToUpper()){
                       case "VARID":
                       iIndex = 2;
                       break;
                       case "VARCONTRACT":
                       iIndex = 3;
                       break;
                       case "VARSIZE":
                       iIndex = 4;
                       break;
                       case "VARMELT":
                       iIndex = 5;
                       break;
                       case "VARDENS":
                       iIndex = 6;
                       break;
                       case "VARADDS":
                       iIndex = 7;
                       break;
                       case "VARTERM":
                       iIndex = 8;
                       break;
                       case "VARUSR":
                       iIndex = 9;
                       break;
                       case "VARPRICE":
                       iIndex = 10;
                       break;
                       case "VARPRICE2":
                       iIndex = 11;
                       break;
                       case "VARVALUE":
                       iIndex = 12;
                       break;
                    }
                 }else{
                   switch(ColumnToSort.ToUpper()){
                       case "VARID":
                       iIndex = 1;
                       break;
                       case "VARCONTRACT":
                       iIndex = 2;
                       break;
                       case "VARSIZE":
                       iIndex = 3;
                       break;
                       case "VARMELT":
                       iIndex = 4;
                       break;
                       case "VARDENS":
                       iIndex = 5;
                       break;
                       case "VARADDS":
                       iIndex = 6;
                       break;
                       case "VARTERM":
                       iIndex = 7;
                       break;
                       case "VARPRICE2":
                       iIndex = 8;
                       break;



                   }
                 }
                // alter the column's sort expression
                if (ViewState["UserType"].ToString().Equals("A")){
                   dgAdmin.Columns[iIndex].SortExpression = NewSortExpr;
                   ResetHeaders();
                   if (NewSearchMode.Equals("DESC")){
                       dgAdmin.Columns[iIndex].HeaderText = "<img border=\"0\" src=\"/pics/icons/icon_down_arrow.gif\">" + dgAdmin.Columns[iIndex].HeaderText;
                   }else{
                       dgAdmin.Columns[iIndex].HeaderText =  "<img border=\"0\" src=\"/pics/icons/icon_up_arrow.gif\">" + dgAdmin.Columns[iIndex].HeaderText;
                   }
                   dgAdmin.CurrentPageIndex = 0;
                }else{
                   dgUser.Columns[iIndex].SortExpression = NewSortExpr;
                   ResetHeaders();
                   if (NewSearchMode.Equals("DESC")){
                       dgUser.Columns[iIndex].HeaderText = "<img border=\"0\" src=\"/pics/icons/icon_down_arrow.gif\">" + dgUser.Columns[iIndex].HeaderText;
                   }else{
                       dgUser.Columns[iIndex].HeaderText =  "<img border=\"0\" src=\"/pics/icons/icon_up_arrow.gif\">" + dgUser.Columns[iIndex].HeaderText;
                   }
                   dgUser.CurrentPageIndex = 0;
                }

                // Sort the data in new order
                ViewState["Sort"] = NewSortExpr;
                // reset dbase page to the first one whether the sorting changes


            BindDataGrid();
         }
         private void ResetHeaders(){
           if (ViewState["UserType"].ToString().Equals("A")){
               dgAdmin.Columns[2].HeaderText = "#";
               dgAdmin.Columns[3].HeaderText = "Product";
               if ((bool)ViewState["Export"]){
                   dgAdmin.Columns[4].HeaderText = "Metric Tons";
               }else{
                   dgAdmin.Columns[4].HeaderText = "Size";
               }

               dgAdmin.Columns[5].HeaderText = "Melt";
               dgAdmin.Columns[6].HeaderText = "Density";
               dgAdmin.Columns[7].HeaderText = "Adds";
               dgAdmin.Columns[8].HeaderText = "Delivery Terms";


               //if (!ddlCompanies.SelectedItem.Value.Equals("*Bid")){
               //     dgAdmin.Columns[9].HeaderText = "Origin";
               //}else{
               //     dgAdmin.Columns[9].HeaderText = "Delivered To";
               //}
               dgAdmin.Columns[9].HeaderText = "Seller";
               if (!ddlCompanies.SelectedItem.Value.Equals("*Bid")){
					//Offers
					dgAdmin.Columns[10].HeaderText = "TPE Buy Price";
					dgAdmin.Columns[11].HeaderText = "TPE Sell Price";
				}else{
					//BIDs
					dgAdmin.Columns[10].HeaderText = "TPE Sell Price";
					dgAdmin.Columns[11].HeaderText = "TPE Buy Price";
				}
				dgAdmin.Columns[12].HeaderText = "Value";
           }else{
               dgUser.Columns[1].HeaderText = "#";
               dgUser.Columns[2].HeaderText = "Product";
               if ((bool)ViewState["Export"]){
                   dgUser.Columns[3].HeaderText = "Metric Tons";
               }else{
                   dgUser.Columns[3].HeaderText = "Size";
               }
               dgUser.Columns[4].HeaderText = "Melt";
               dgUser.Columns[5].HeaderText = "Density";
               dgUser.Columns[6].HeaderText = "Adds";
               dgUser.Columns[7].HeaderText = "Delivery Terms";
               if (!ddlCompanies.SelectedItem.Value.Equals("*Bid")){
					//Offers
					dgUser.Columns[8].HeaderText = "Price";
				}else{
					//BIDs
					dgUser.Columns[8].HeaderText = "Price";
				}
           }

         }


         double dbTotalValue = 0.0;
         double dbTotalWeight = 0.0;
        // calculates total for the footer
        void KeepRunningSum(object sender, DataGridItemEventArgs e)
        {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem){
                // setting the mouseover color
                e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");
                // shows that the offer is hot.  bids can not be made hot
                if (ViewState["UserType"].ToString().Equals("A") && !ddlCompanies.SelectedItem.Value.Equals("*Bid")){
                    if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARHOT")).ToString().Equals("1")){
                         e.Item.Cells[2].Text = "<font color=red>"+Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString()+"</font>";
                    }
                }
                
				string density = e.Item.Cells[6].Text;
				if ((density.Length>2) && (density.Substring(0,2) == "0."))
					e.Item.Cells[6].Text = density.Substring(1);

                if (e.Item.ItemType == ListItemType.Item){
                    e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee'");
                }else{
                    e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3'");
                }

                if (!ViewState["UserType"].ToString().Equals("A") && !ViewState["UserType"].ToString().Equals("B")){
                    e.Item.Attributes.Add("onclick", "window.location ='/Spot/Inquire.aspx?ID="+(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString() +"' ");
                }


                dbTotalValue += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARVALUE"));


                if ((bool)ViewState["Export"]){
                   dbTotalWeight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSIZE"));
                }else{
                   dbTotalWeight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARWGHT"));
                }
                //e.Item.Cells[1].Text = ""; // sets the field blank
                if (ViewState["UserType"].ToString().Equals("A")){
                   // admins can edit all spot order
				   
				   string market = "Domestic";
				   if ((bool)ViewState["Export"]) market = "International";
				   
                   if (!ddlCompanies.SelectedItem.Value.Equals("*Bid")){
                        e.Item.Cells[1].Text = "<a href=\"/spot/Resin_Entry.aspx?Screen="+Screen+"&type=offer&Change=" +(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString() + "&Market=" + market.ToString() + "\">Edit</a><BR>";
                   }else{
                        e.Item.Cells[1].Text = "<a href=\"/spot/Resin_Entry.aspx?Screen="+Screen+"&type=bid&Change=" +(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString() + "&Market=" + market.ToString() + "\">Edit</a><BR>";

                   }
                }else if (ViewState["UserType"].ToString().Equals("B")){
                   // nothing

                }else{
                   e.Item.Cells[0].Text = "<a href=\"/Spot/Inquire.aspx?ID=" +(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString() + "\">Inquire</a><BR>";
                }
                if (!ViewState["UserType"].ToString().Equals("A") && !ViewState["UserType"].ToString().Equals("B")){
                   //if (ViewState["UserType"].ToString().Equals("S") && (bool)(DataBinder.Eval(e.Item.DataItem, "VARFIRM")) && ddlCompanies.SelectedItem.Value.Equals("*Bid")){
                   //    e.Item.Cells[0].Text += "<a href=\"/common/Spot_Order.aspxID="+(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString() + "\"><b>Sell</b></a>";
                   //}
                   //if (ViewState["UserType"].ToString().Equals("P") && (bool)(DataBinder.Eval(e.Item.DataItem, "VARFIRM")) && !ddlCompanies.SelectedItem.Value.Equals("*Bid")){
                   //    e.Item.Cells[0].Text += "<a href=\"/common/Spot_Order.aspx?ID="+(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString() + "\"><b>Buy</b></a>";
                   //}

                }

                if ((bool)ViewState["Export"]){
                   if (ViewState["UserType"].ToString().Equals("A")){
                       e.Item.Cells[4].Text =String.Format("{0:#,###}", (DataBinder.Eval(e.Item.DataItem, "VARSIZE")));
                   }else{
                        e.Item.Cells[3].Text =String.Format("{0:#,###}", (DataBinder.Eval(e.Item.DataItem, "VARSIZE")));
                   }

                }

        }
        else if (e.Item.ItemType == ListItemType.Footer){
            e.Item.Cells[1].Text ="<b>Total</b>";
            if (ViewState["UserType"].ToString().Equals("A")){
               if ((bool)ViewState["Export"]){
                   e.Item.Cells[4].Text = "<b> " + String.Format("{0:#,###}", dbTotalWeight)+" Metric Tons</b>";
               }else{
                    e.Item.Cells[4].Text = "<b> " + String.Format("{0:#,###}", dbTotalWeight)+" lbs</b>";
               }
               e.Item.Cells[12].Text = "<b> " + String.Format("{0:c}", dbTotalValue)+"</b>";
            }else{
               if ((bool)ViewState["Export"]){
                   e.Item.Cells[3].Text = "<b> " + String.Format("{0:#,###}", dbTotalWeight)+" Metric Tons</b>";
               }else{
                    e.Item.Cells[3].Text = "<b> " + String.Format("{0:#,###}", dbTotalWeight)+" lbs</b>";
               }
            }
        }

        else if (e.Item.ItemType == ListItemType.Header){
           if ((bool)ViewState["Export"]){
                   dgAdmin.Columns[4].HeaderText ="Metric Tons";
                   dgUser.Columns[3].HeaderText = "Metric Tons";
           }

        }




    }

</script>
<table>
	<tr>
		<td colspan="2">
			<table>
				<asp:Panel id="pnTopBar" runat="server">
        <TBODY>
        <TR>
          <TD>
<asp:DropDownList id=ddlCompanies runat="server" AutoPostBack="True" OnSelectedIndexChanged="Change_Company"></asp:DropDownList>Filter 
            by Resin: 
<asp:DropDownList id=ddlResinType runat="server" AutoPostBack="True" OnSelectedIndexChanged="Change_Resin"></asp:DropDownList>Search: 
<asp:TextBox id=txtSearch runat="server" maxsize="40" size="15"></asp:TextBox>
<asp:Button class=tpebutton id=Button1 onclick=Click_Search runat="server" Text="Find"></asp:Button></TD></TR>
				</asp:Panel>
				<tr>
					<td>
						<asp:Button runat="server" id="btnEmailOffers" onclick="Click_Send_Mass_Mail" class="tpebutton"
							Text="Email Offers" />
						<asp:Button runat="server" id="btnArchive" onclick="Click_Move_To_Archive" class="tpebutton"
							Text="Move to Archive" />
						<asp:Button runat="server" id="btnDelete" onclick="Click_Delete" class="tpebutton" Text="Delete" />
						<asp:Button runat="server" id="btnHot" onclick="Click_MakeHot" class="tpebutton" Text="Make Offer Hot" />
						<asp:Button runat="server" id="btnCool" onclick="Click_MakeCool" class="tpebutton" Text="Cool Offer" />
					</td>
				</tr></table></TD></TR>
	<tr>
		<td colspan="2">
			<% if (ViewState["UserType"].ToString().Equals("A")){ // working as admin %>
			<asp:DataGrid id="dgAdmin" runat="server" AutoGenerateColumns="False" onSortCommand="SortDG" AllowSorting="True"
				CellPadding="2" OnItemDataBound="KeepRunningSum" DataKeyField="VARID" HorizontalAlign="Center"
				Width="100%" ShowFooter="True" CssClass="DataGrid" ItemStyle-CssClass="DataGridRow" AlternatingItemStyle-CssClass="DataGridRow_Alternate"
				HeaderStyle-CssClass="DataGridHeader">
				<HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
				<AlternatingItemStyle CssClass="DataGridRow_Alternate"></AlternatingItemStyle>
				<ItemStyle CssClass="DataGridRow"></ItemStyle>
				<Columns>
					<mbrsc:RowSelectorColumn AllowSelectAll="True"></mbrsc:RowSelectorColumn>
					<asp:TemplateColumn>
						<HeaderTemplate>
						</HeaderTemplate>
						<ItemTemplate>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="VARID" SortExpression="VARID ASC" HeaderText="ID #">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARCONTRACT" SortExpression="VARCONTRACT ASC" HeaderText="Product">
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARSIZE" SortExpression="VARSIZE ASC" HeaderText="Size">
						<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARMELT" SortExpression="VARMELT ASC" HeaderText="Melt">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARDENS" SortExpression="VARDENS ASC" HeaderText="Density">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARADDS" SortExpression="VARADDS ASC" HeaderText="Adds">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARTERM" SortExpression="VARTERM ASC" HeaderText="Delivery Terms">
						<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARUSR" SortExpression="VARUSR ASC" HeaderText="Company">
						<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARPRICE" SortExpression="VARPRICE ASC" HeaderText="Price" DataFormatString="{0:c}">
						<ItemStyle Font-Bold="True" HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARPRICE2" SortExpression="VARPRICE2 ASC" HeaderText="Price2" DataFormatString="{0:c}">
						<ItemStyle Font-Bold="True" HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARVALUE" SortExpression="VARVALUE ASC" HeaderText="Value" DataFormatString="${0:#,###}">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARBIDOFFER" HeaderText="BID/OFFER DATE"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid>
			<%}else{%>
			<asp:DataGrid id="dgUser" runat="server" AutoGenerateColumns="False" onSortCommand="SortDG" AllowSorting="True"
				CellPadding="2" OnItemDataBound="KeepRunningSum" DataKeyField="VARID" HorizontalAlign="Center"
				Width="100%" ShowFooter="True" BackColor="#EEEEEE" CssClass="DataGrid" ItemStyle-CssClass="DataGridRow"
				AlternatingItemStyle-CssClass="DataGridRow_Alternate" HeaderStyle-CssClass="DataGridHeader">
<HeaderStyle CssClass="DataGridHeader">
</HeaderStyle>

<AlternatingItemStyle CssClass="DataGridRow_Alternate">
</AlternatingItemStyle>

<ItemStyle CssClass="DataGridRow">
</ItemStyle>

<Columns>
<asp:TemplateColumn>
<HeaderTemplate>
						
</HeaderTemplate>

<ItemTemplate>
						
</ItemTemplate>
</asp:TemplateColumn>
<asp:BoundColumn DataField="VARID" SortExpression="VAREXPR ASC" HeaderText="ID #">
<ItemStyle HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARCONTRACT" SortExpression="VARCONTRACT ASC" HeaderText="Product">
<ItemStyle Wrap="False">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARSIZE" SortExpression="VARSIZE ASC" HeaderText="Size">
<ItemStyle Wrap="False" HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARMELT" SortExpression="VARMELT ASC" HeaderText="Melt">
<ItemStyle HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARDENS" SortExpression="VARDENS ASC" HeaderText="Density">
<ItemStyle HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARADDS" SortExpression="VARADDS ASC" HeaderText="Adds">
<ItemStyle HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARTERM" SortExpression="VARTERM ASC" HeaderText="Delivery Terms">
<ItemStyle Wrap="False" HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARPRICE2" SortExpression="VARPRICE2 ASC" HeaderText="Price" DataFormatString="{0:c}">
<ItemStyle Font-Bold="True" HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
</Columns>
			</asp:DataGrid>
			<%}%>
		</td>
	</tr></TBODY></TABLE>
