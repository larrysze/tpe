using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Mail;
using System.Text;
using TPE.Utility;

          /*****************************************************************************
          '*   1. File Name       : Spot/Total.aspx                                    *
          '*   2. Description     : This page shown the  selected offer/bid details    *
          '*						that is passed from OfferDetails/Biddetails.aspx   *
		                            and in this page we can use the web service to     *
									calculate the distance between'offer from/bid from'*
									to shipping addres where user wants to ship the    *
									oredr.and according to this distance we can        *
									calculate the freight and get total price which    *
		                            user pay if he confirm that order through          * 
		                            orderconfirmation.aspx page.                       *
          '*   3. Modification Log:                                                    *
          '*     Ver No.       Date          Author             Modification           *
          '*   -----------------------------------------------------------------       *
          '*      1.00                      Hanu software        Comment               *
          '*                                                                           *
          '*****************************************************************************/
namespace PlasticsExchange
{
	/// <summary>
	/// Summary description for Total.
	/// </summary>
	public class Total : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblID;
		protected System.Web.UI.WebControls.Label lblProduct;
		protected System.Web.UI.WebControls.Label lblSize;
		protected System.Web.UI.WebControls.Label lblPrice;
		protected System.Web.UI.WebControls.Label lblFreight;
		protected System.Web.UI.WebControls.Label lblTotal;
		protected System.Web.UI.WebControls.Label lblTerms;
		private double total;
		private double price;
		protected System.Web.UI.WebControls.Panel pnlCreditOn;
		protected System.Web.UI.WebControls.Panel pnlCreditOff;
		protected System.Web.UI.WebControls.Button btnGoBack;
		protected System.Web.UI.WebControls.Button btnFaxOnline;
		protected System.Web.UI.WebControls.Button btnFaxCredit;
		protected System.Web.UI.WebControls.Button btnContinue;
		//private double TmpFreight;
		 
		
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["ID"] == null)
			{
				Response.Redirect("/default.aspx");
			}

			if  (Session["Typ"].ToString().Equals("Demo")) 
			{
				pnlCreditOn.Visible=true;
				pnlCreditOff.Visible=false;
			}
			else
			{
				pnlCreditOff.Visible=true;
				pnlCreditOn.Visible=false;
			}

			string strSql=string.Empty ;
			double SizeVal=0;
			string[] ArrProd= new string[0] ;
			try
			{
				ArrProd = Session.Contents["ProductID"].ToString().Split(new Char []{'^'}) ;		
				ViewState["ID"]=ArrProd[1].ToString();
			}
			catch  
			{
				Response.Redirect("Spot_Floor.aspx");
			}
			/// check here that page is post back or not if not than make the connection and execute the command
			if (!IsPostBack)
			{
			
				SqlConnection conn= new SqlConnection(Application["DBConn"].ToString());
				conn.Open();

				/// offer read that passed value/parameter from spot_floor.aspx page/dashboard.aspx page				
				Hashtable param = new Hashtable();
				param.Add("@OFFR_ID", "'" + ViewState["ID"].ToString() + "' ");
				
				SqlDataReader dtrOffer;
				StringBuilder sbSQL = new StringBuilder();			
				sbSQL.Append("		SELECT *, ");
				sbSQL.Append("		VARTERM=(CASE WHEN OFFR_TERM= 'FOB/Delivered' THEN 'Delivered' WHEN  ");
				sbSQL.Append("		OFFR_TERM= 'FOB/Shipping' THEN 'FOB '  END ), ");
				sbSQL.Append("		VARSIZE=CAST(OFFR_QTY AS VARCHAR)+' '+(CASE OFFR_SIZE WHEN 1 THEN ");
				sbSQL.Append("		'lb' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 42000 ");
				sbSQL.Append("		THEN 'T/L - Boxes' WHEN 45000 THEN 'Bulk Truck' WHEN 44092 THEN 'T/L Bags' ");
				sbSQL.Append("		WHEN 190000 THEN 'Rail Car'	END)+(CASE WHEN (OFFR_QTY>1 AND OFFR_SIZE <> 42000 AND OFFR_SIZE <> 44092 ) THEN 's' ELSE ' ' END), ");
				sbSQL.Append("		VARVALUE=(OFFR_PRCE*OFFR_SIZE*OFFR_QTY) ");

				sbSQL.Append("		FROM BBOFFER ");
				sbSQL.Append("		WHERE  ");
				sbSQL.Append("		OFFR_ID= @OFFR_ID");
								
				dtrOffer = DBLibrary.GetDataReaderFromSelect(conn,sbSQL.ToString(),param);

				if (dtrOffer.Read())
				{
					// offer details are displayed here in different label that parameter recd. from spot_floor.aspx page/dashboard.aspx page 
					lblID.Text = dtrOffer["OFFR_ID"].ToString();
					lblProduct.Text = dtrOffer["OFFR_PROD"].ToString();
					SizeVal = Convert.ToDouble(dtrOffer["OFFR_SIZE"]);
					lblSize.Text = dtrOffer["VARSIZE"].ToString();
					lblTerms.Text = dtrOffer["VARTERM"].ToString();
					price = Math.Round( Convert.ToDouble(dtrOffer["VARVALUE"].ToString()),3) ;
					lblPrice.Text=String.Format("{0:c}", price );;
					ViewState["Firm"] = dtrOffer["OFFR_FIRM"].ToString();
					ViewState["Origin"] = dtrOffer["OFFR_FROM_LOCL"].ToString();   
					ViewState["Expiration"] = dtrOffer["OFFR_EXPR"].ToString();
					ViewState["TERMS"] = dtrOffer["OFFR_PAY"].ToString();
					ViewState["TYPE"] = "Offer";
					dtrOffer.Close();
				}
				else
				{
					dtrOffer.Close();
					/// Bid read that passed value/parameter from spot_floor.aspx page

					Hashtable param = new Hashtable();
					param.Add("@BID_ID", "'" + ViewState["ID"].ToString() + "' ");

					SqlDataReader dtrBid;
					sbSQL.Remove(0,sbSQL.Length);
					sbSQL.Append("		SELECT *, ");
					sbSQL.Append("		VARTERM=(CASE WHEN BID_TERM= 'FOB/Delivered' THEN 'Delivered' WHEN  ");
					sbSQL.Append("		BID_TERM= 'FOB/Shipping' THEN 'FOB '  END ), ");
					sbSQL.Append("		VARSIZE=CAST(BID_QTY AS VARCHAR)+' '+(CASE BID_SIZE WHEN 1 THEN ");
					sbSQL.Append("		'lb' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 42000 ");
					sbSQL.Append("		THEN 'T/L - Boxes' WHEN 45000 THEN 'Bulk Truck' WHEN 44092 THEN 'T/L Bags' ");
					sbSQL.Append("		WHEN 190000 THEN 'Rail Car'	END)+(CASE WHEN (BID_QTY>1 AND BID_SIZE <> 42000 AND BID_SIZE <> 44092 ) THEN 's' ELSE ' ' END), ");
					sbSQL.Append("		VARVALUE=(BID_PRCE*BID_SIZE*BID_QTY) ");
					sbSQL.Append("		FROM BBID ");
					sbSQL.Append("		WHERE  ");
					sbSQL.Append("		BID_ID = @BID_ID");
					
					dtrBid = DBLibrary.GetDataReaderFromSelect(conn,sbSQL.ToString(),param);

					if (dtrBid.Read())
					{
						/// Bid details are displayed here in different label that parameter recd. from spot_floor.aspx page 
						lblID.Text = dtrBid["BID_ID"].ToString();
						lblProduct.Text = dtrBid["BID_PROD"].ToString();
						lblSize.Text = dtrBid["VARSIZE"].ToString();
						lblTerms.Text = dtrBid["VARTERM"].ToString();				
						price = Math.Round( Convert.ToDouble(dtrBid["VARVALUE"].ToString()),3) ;
						lblPrice.Text=String.Format("{0:c}", price );;
						ViewState["Firm"] = dtrBid["BID_FIRM"].ToString();
						ViewState["Origin"] = dtrBid["BID_TO_LOCL"].ToString();
						ViewState["Expiration"] = dtrBid["BID_EXPR"].ToString();
						ViewState["TERMS"] = dtrBid["BID_PAY"].ToString();
						ViewState["TYPE"] = "Bid";
						dtrBid.Close();					
					}
					else
					{
						// output error
					}
					dtrBid.Close();
		
				}
				conn.Close();

				lblFreight.Text= string.Format("{0:c}",2000);
				Session["freight"]= 2000; 
				total = Math.Round((price + 2000),3) ;
				Session["PayTotal"]= total.ToString();
				lblTotal.Text=String.Format("{0:c}", total);
			}
			
		}

		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnFaxOnline.Click += new System.EventHandler(this.btnFaxOnline_Click);
			this.btnFaxCredit.Click += new System.EventHandler(this.btnFaxCredit_Click);
			this.btnGoBack.Click += new System.EventHandler(this.btnGoBack_Click);
			this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	
		private void btnGoBack_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("ShipDetails.aspx");
		}

		private void btnContinue_Click(object sender, System.EventArgs e)
		{
			Session["FrmName"]="Total";
			Response.Redirect("OrderConfirmation.aspx");
		}

		private void btnFaxOnline_Click(object sender, System.EventArgs e)
		{
			Session["FrmName"]="Total";
			Response.Redirect("CreditApplication.aspx");

			//**********************************************/
			//			Mailing Code  Below					/
			//			mail is sent to the admin that		/
			//			what is the total price of that		/
			//			particular user's selected offer.	/
			//			admin is also know that user fill	/
			//			the  credit application through		/
			//			online credit app.					/
			//**********************************************/

			//          StringBuilder sbHTML= new StringBuilder();
			//			sbHTML.Append("<align='left'><font size='3'><b>Credit Application:</b></font><br>");
			//			sbHTML.Append("<align='left'>A user filed credit Application.<br>");
			//			System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
			//			mail.From=Application["strEmailOwner"].ToString();
			//			mail.To ="user@theplasticsexchange.com";//////shold be change after given new emailid of query
			//			mail.Cc= Application["strEmailAdmin"].ToString();
			//			mail.Body=sbHTML.ToString();
			//			mail.Subject="credit application pdf file is attached with this mail you must fill this and send it through fax";
			//			mail.BodyFormat = MailFormat.Html;
			//          SmtpMail.SmtpServer="localhost";
			//			TPE.Utility.EmailLibrary.Send(mail);

		}

		private void btnFaxCredit_Click(object sender, System.EventArgs e)
		{
			Session["FrmName"]="Total";	
			Response.Redirect("OrderConfirmation.aspx");

			//***********************************/
			//		 Mailing Code  Below         /
			// mail is sent to the admin that    /
			// what is the total price of that   /
			// particular user's selected offer  /
			// admin is also know that user sends/ 
			// the  credit application through   /
			// fax app                           /
			// and one mail is also send to the  /
			// user with the pdf attachment file /
			//***********************************/
									
			//////////          StringBuilder sbHTML= new StringBuilder();
			//////////			sbHTML.Append("<align='left'><font size='3'><b>Credit Application:</b></font><br>");
			//////////			sbHTML.Append("<align='left'>A user filed credit Application.<br>");
			//////////			System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
			//////////			mail.From=Application["strEmailOwner"].ToString();
			//////////			mail.To ="user@theplasticsexchange.com";//////shold be change after given new emailid of query
			//////////			mail.Cc= Application["strEmailAdmin"].ToString();
			//////////			mail.Body=sbHTML.ToString();
			//////////			mail.Subject="credit application pdf file is attached with this mail you must fill this and send it through fax";
			//////////			mail.BodyFormat = MailFormat.Html;
			//////////          mail.attachement="creditapplication.pdf"
			//////////			SmtpMail.SmtpServer="localhost";
			//////////			TPE.Utility.EmailLibrary.Send(mail);
		}

		

	

		
	}
}
