using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPECharts;
using System.Threading;
using dotnetCHARTING;
using TPE.Utility;

namespace localhost.Spot
{
    public partial class orderform : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                
                LoadProductList();
                LoadCompanyList();
                LoadQTY();
                LoadShippingPoint();
                //LoadTermsList();
                DataBindGrid();


            }


        }



        //protected void ddlTypeEntry_SelectedIndexChanged(object sender, System.EventArgs e)
        //{
        //    if (ddlTypeEntry.SelectedValue == "O")
        //    {
        //        SetLabelsSellResin();
        //    }
        //    else
        //    {
        //        SetLabelsRequestResin();
        //    }
        //}

        protected void ddlProduct_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            int ProductID;
            ProductID = Int32.Parse(ddlProduct.SelectedValue);

            LoadQuantityList();

            switch (ProductID)
            {
                case 1:
                    lblPriceMeasure.Text = "/ lb.";
                    ddlQty.SelectedValue = "1000000";
                    break;

                case 2:
                    lblPriceMeasure.Text = "/ lb.";
                    ddlQty.SelectedValue = "182";
                    break;

                case 3:
                    lblPriceMeasure.Text = "/ lb.";
                    ddlQty.SelectedValue = "1000000";
                    break;

                case 4:
                    lblPriceMeasure.Text = "/ lb.";
                    ddlQty.SelectedValue = "1000000";
                    break;

                case 5:
                    lblPriceMeasure.Text = "/ gal.";
                    ddlQty.SelectedValue = "309";
                    break;

                default:
                    lblPriceMeasure.Text = "/ lb.";
                    ddlQty.SelectedValue = "1000000";
                    break;
            }


        }

        private void LoadQTY()
        {

            for (int i = 1; i < 50; i++)
            {
                txtQty.Items.Add(i.ToString());
            }


        }


        private void LoadShippingPoint()
        {
       
                //txtZipCode.Visible = true;
                //lblShippingPointZip.Visible = true;
                //lblShippingPointOr.Visible = true;
                //lblShippingPointDomestic.Visible = true;
                //Load the Most Popular Cities
                HelperFunctionDistances hfd = new HelperFunctionDistances();
                hfd.loadMostPopularCities(ddlDeliveryPoint, Session["Typ"].ToString());
                //if (HelperFunction.getSafeStringFromDB(Session["Typ"]) == "A")
                //{
                //    ddlDeliveryPoint.SelectedIndex = 3;
                //}
           
        }

        private void LoadQuantityList()
        {

            ddlQty.Items.Clear();

            ddlQty.Items.Add(new ListItem("Million Pounds", "1000000"));
            //  ddlQty.Items.Add(new ListItem("Thousand Barrels", "182"));
            // ddlQty.Items.Add(new ListItem("Thousand Barrels", "182"));

            int ProductID;
            ProductID = Int32.Parse(ddlProduct.SelectedValue);

            switch (ProductID)
            {
                case 2:
                    ddlQty.Items.Add(new ListItem("Thousand Barrels", "182"));
                    break;

                case 5:
                    ddlQty.Items.Add(new ListItem("Thousand Barrels", "309"));
                    break;

                default:
                    ddlQty.Items.Add(new ListItem("Thousand Barrels", "125.8992"));
                    break;
            }

        }

        private void LoadProductList()
        {
            // creating product drop-down list manually
            ddlProduct.Items.Add(new ListItem("Select Product Grade", "0"));
            HelperFunction.LoadGrades(this.Context, ddlProduct, "Grade_ID");
        }

        private void LoadCompanyList()
        {
            //Top 10 looking at BBOFFERARCH
            string sqlStatement = "SELECT     TOP 10 C.COMP_ID, C.COMP_NAME, C.COMP_TYPE, QTT.SUM_QTD " +
                "FROM         COMPANY C INNER JOIN " +
                "(SELECT     ID, SUM(QTTF) AS SUM_QTD " +
                "FROM          (SELECT     COUNT(OFFR_ID) AS QTTF, OFFR_COMP_ID AS ID " +
                "FROM          BBOFFERARCH " +
                "WHERE      (OFFR_DATE BETWEEN DATEADD(day, - 90, GETDATE()) AND GETDATE()) AND OFFR_COMP_ID IS NOT NULL AND  " +
                "OFFR_COMP_ID <> 0 " +
                "GROUP BY OFFR_COMP_ID " +
                "UNION " +
                "SELECT     COUNT(BID_ID) AS QTTF, BID_COMP_ID AS ID " +
                "FROM         BBIDARCH " +
                "WHERE     (BID_DATE BETWEEN DATEADD(day, - 90, GETDATE()) AND GETDATE()) AND BID_COMP_ID IS NOT NULL AND  " +
                "BID_COMP_ID <> 0 " +
                "GROUP BY BID_COMP_ID) DERIVEDTBL " +
                "GROUP BY ID) QTT ON C.COMP_ID = QTT.ID " +
                "WHERE     (C.COMP_REG = 1) AND (C.COMP_TYPE IN ('P', 'S', 'D')) AND EXISTS " +
                "(SELECT PERS_ID FROM PERSON WHERE PERS_COMP = COMP_ID) " +
                "ORDER BY QTT.SUM_QTD DESC, C.COMP_NAME";
            DataTable dtCompanies = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), sqlStatement);
            ddlCompanies.Items.Add(new ListItem("Select Company", "0"));
            foreach (DataRow dr in dtCompanies.Rows)
            {
                ddlCompanies.Items.Add(new ListItem((string)dr["COMP_NAME"], dr["COMP_ID"].ToString()));
            }

            //Rest of them, excluding top 10
            sqlStatement = "SELECT     COMP_ID, COMP_NAME, COMP_TYPE " +
                "FROM         COMPANY C " +
                "WHERE     (COMP_REG = 1) AND (COMP_TYPE IN ('P', 'S', 'D')) AND EXISTS " +
                "(SELECT     PERS_ID " +
                "FROM          PERSON " +
                "WHERE      PERS_COMP = COMP_ID) AND (COMP_ID NOT IN " +
                "(SELECT     TOP 10 C.COMP_ID " +
                "FROM          COMPANY C INNER JOIN " +
                "(SELECT     ID, SUM(QTTF) AS SUM_QTD " +
                "FROM          (SELECT     COUNT(OFFR_ID) AS QTTF, OFFR_COMP_ID AS ID " +
                "FROM          BBOFFERARCH " +
                "WHERE      (OFFR_DATE BETWEEN DATEADD(day, - 90, GETDATE()) AND GETDATE()) AND OFFR_COMP_ID IS NOT NULL " +
                "AND OFFR_COMP_ID <> 0 " +
                "GROUP BY OFFR_COMP_ID " +
                "UNION " +
                "SELECT     COUNT(BID_ID) AS QTTF, BID_COMP_ID AS ID " +
                "FROM         BBIDARCH " +
                "WHERE     (BID_DATE BETWEEN DATEADD(day, - 90, GETDATE()) AND GETDATE()) AND BID_COMP_ID IS NOT NULL AND " +
                "BID_COMP_ID <> 0 " +
                "GROUP BY BID_COMP_ID) AS TABLE1 " +
                "GROUP BY ID) QTT ON C.COMP_ID = QTT.ID " +
                "WHERE      (C.COMP_REG = 1) AND (C.COMP_TYPE IN ('P', 'S', 'D')) AND EXISTS " +
                "(SELECT PERS_ID FROM PERSON WHERE PERS_COMP = COMP_ID) " +
                "ORDER BY QTT.SUM_QTD DESC, C.COMP_NAME)) " +
                "ORDER BY COMP_NAME";
            dtCompanies = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), sqlStatement);
            ddlCompanies.Items.Add(new ListItem(" --------------------", "0"));
            foreach (DataRow dr in dtCompanies.Rows)
            {
                ddlCompanies.Items.Add(new ListItem((string)dr["COMP_NAME"], dr["COMP_ID"].ToString()));
            }
        }

        //private void LoadTermsList()
        //{         
        //    //Payment Terms List
        //    ddlTerms.Items.Add(new ListItem("FOB Delivered", "FOB/Delivered"));
        //    ddlTerms.Items.Add(new ListItem("FOB Shipping", "FOB/Shipping"));
            
        //}

        protected void ddlCompanies_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //LoadUsersLocationsList();
        }

        private void DataBindGrid()
        {
            //select the data from the bboffer table,and group the record by the data stored in the OFFR_PROD field 
            //of the BBOFFER table.to calculate the count of records,the maximum price,the minimum price and 
            //the count of the weight for each group.			
            DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(), summaryGrid, "spSpotFloorSummary");
        }

        double dbTotalOffers = 0.0;
        double dbTotalOffersLBS = 0.0;
        double dbTotalBid = 0.0;
        double dbTotalBidLBS = 0.0;

        protected void dg_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //dbTotalOffers += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "offr_count") == DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "offr_count"));
                //dbTotalOffersLBS += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "offr_size") == DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "offr_size"));
                //dbTotalBid += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "bid_count") == DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "bid_count"));
                //dbTotalBidLBS += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "bid_size") == DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "bid_size"));

                e.Item.Cells[0].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[5].Text + "&Sort=VARBIDOFFER_DESC'>" + e.Item.Cells[0].Text + "</a>"; //high

                if (Session["Typ"].ToString() != "A" && Session["Typ"].ToString() != "B" && Session["Typ"].ToString() != "L" && Session["Typ"].ToString() != "T" && Session["Typ"].ToString() != "S" && Session["Typ"].ToString() != "D")
                {
                    //e.Item.Cells[5].Text = (e.Item.Cells[0].Text == "&nbsp;" ? e.Item.Cells[6].Text : e.Item.Cells[0].Text);
                    //e.Item.Cells[5].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "'>" + e.Item.Cells[5].Text + "</a>";
                }
                else
                {
                    //e.Item.Cells[4].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&StartOn=bid'>" + e.Item.Cells[4].Text + "</a>"; //qtd
                    //e.Item.Cells[3].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&StartOn=bid&Sort=VARBIDOFFER_DESC'>" + e.Item.Cells[3].Text + "</a>"; //lbs
                    //e.Item.Cells[2].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&StartOn=bid&Sort=VARPRICE_ASC'>" + e.Item.Cells[2].Text + "</a>"; //low
                    //e.Item.Cells[1].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&StartOn=bid&Sort=VARPRICE_DESC'>" + e.Item.Cells[1].Text + "</a>"; //high
                    //e.Item.Cells[5].Text = (e.Item.Cells[0].Text == "&nbsp;" ? e.Item.Cells[6].Text : e.Item.Cells[0].Text);
                    //e.Item.Cells[7].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "'>" + e.Item.Cells[7].Text + "</a>"; //qtd
                    //e.Item.Cells[8].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&Sort=VARBIDOFFER_DESC'>" + e.Item.Cells[8].Text + "</a>"; //lbs
                    //e.Item.Cells[9].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&Sort=VARPRICE2_ASC'>" + e.Item.Cells[9].Text + "</a>"; //low
                    //e.Item.Cells[10].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&Sort=VARPRICE2_DESC'>" + e.Item.Cells[10].Text + "</a>"; //high            

                    //1	Ethylene - Purity
                    //2	Propylene - Refinery
                    //3	Propylene - Chemical
                    //4	Propylene - Polymer
                    //5	Benzene
                    double GradeID = Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "grade_id") == DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "grade_id"));
                    double bidsize = Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "bid_size") == DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "bid_size"));
                    double offersize = Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "offr_size") == DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "offr_size"));

                    if (GradeID == 1 || GradeID == 3 || GradeID == 4)
                    {
                        e.Item.Cells[1].Text = Convert.ToString(bidsize / 5000000);
                        e.Item.Cells[4].Text = Convert.ToString(offersize / 5000000);
                    }
                    else if (GradeID == 2)
                    {
                        e.Item.Cells[1].Text = Convert.ToString(bidsize / 4525000);
                        e.Item.Cells[1].Text = Convert.ToString(offersize / 4525000);
                    }
                    else if (GradeID == 5)
                    {
                        e.Item.Cells[1].Text = Convert.ToString(bidsize / 7725000);
                        e.Item.Cells[4].Text = Convert.ToString(offersize / 7725000);
                    }
                    else
                    {
                        e.Item.Cells[1].Text = "0";
                        e.Item.Cells[4].Text = "0";
                    }
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                //e.Item.Cells[5].Text = "<b>Totals:</b> " ;

                //e.Item.Cells[4].Text = "<b>"+String.Format("{0:#,###}", dbTotalBid)+"</b>";
                //e.Item.Cells[3].Text = "<b>"+String.Format("{0:#,###}", dbTotalBidLBS)+"</b>";

                //e.Item.Cells[7].Text = "<b>"+String.Format("{0:#,###}", dbTotalOffers)+"</b>";
                //e.Item.Cells[8].Text = "<b>"+String.Format("{0:#,###}", dbTotalOffersLBS)+"</b>";
            }



        }

		
    }
}
