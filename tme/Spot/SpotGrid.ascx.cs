namespace localhost.Spot
{
	using System;
	using System.Collections;
	using System.ComponentModel;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.SessionState;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Data.SqlClient;
	using System.Web.Mail;
	using MetaBuilders.WebControls;
	using System.Text.RegularExpressions;
	using TPE.Utility;
	
	/// <summary>
	///		Summary description for SpotGrid.
	/// </summary>
	public partial class SpotGrid : System.Web.UI.UserControl
	{
		public bool International = false;
		public string  Filter = "";
		public string  Size = "";
		//protected System.Web.UI.WebControls.Literal ltLivePerson;
		public string Screen = "Spot_Floor";
		
		public void Page_Load(object sender, EventArgs e)
		{
			//all user can access spot floor
			if ((Session["Typ"]==null) || (Session["Typ"].ToString()=="") && Filter == "") 
			{
				Session["strLoginURL"] =Request.RawUrl.ToString(); 
				Response.Redirect("/common/FUNCLogin.aspx");
			}
			ViewState["UserType"] = Session["Typ"].ToString();
//			ltLivePerson.Text =HelperFunction.getLivePersonButton();
			lblGridEmpy.Visible = false;
			
			imbFind.Attributes.Add("onMouseOver", "document.getElementById('" + imbFind.ClientID + "').src ='/images/buttons/find_pressed.jpg';");
            imbFind.Attributes.Add("onMouseOut", "document.getElementById('" + imbFind.ClientID + "').src ='/images/buttons/find_unpress.jpg';");
			//imbUpdate.Attributes.Add("onMouseOver", "document.getElementById('SpotGrid1_imbUpdate').src ='/images/buttons/update_red.jpg';");
			//imbUpdate.Attributes.Add("onMouseOut", "document.getElementById('SpotGrid1_imbUpdate').src ='/images/buttons/update_orange.jpg';");
			
			if (!IsPostBack)
			{
							
				//check if it's export transction
				if (International)
				{
					ViewState["Export"] =true;
				}
				else
				{
					ViewState["Export"] =false;
				}
				
				// define the default sort function
				if ((Request.QueryString["Sort"]!=null) && (Request.QueryString["Sort"].ToString()!="")) //Ex. PRICE_ASC
				{
					string sort = Request.QueryString["Sort"].ToString();
					sort = sort.Replace("_"," ").ToString(); //replace "_" by " "
					ViewState["Sort"] = sort;
				}
				else
				{
					String pers_type = Session["Typ"].ToString();
                    if ((pers_type == "A") || (pers_type == "B") || (pers_type == "T") || (pers_type == "L"))
					{
						//ViewState["Sort"] = "VARBIDOFFER DESC";
						ViewState["Sort"] = "VARID DESC";
					}
					else
					{

						ViewState["Sort"] = "VARPRICE2 ASC";
					}
				}
				//lblSort.Text = "VARID DESC";

                ddlResinType.Items.Add(new ListItem("All", ""));
                //ddlResinType.Items.Add(new ListItem ("Customized","-1"));
				
				using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
				{
					conn.Open();
					using (SqlDataReader dtrGrades = DBLibrary.GetDataReaderFromSelect(conn,"SELECT GRADE_ID, GRADE_NAME FROM GRADE"))
						   {
						while (dtrGrades.Read())
						{
							ddlResinType.Items.Add(new ListItem (dtrGrades["GRADE_NAME"].ToString(),dtrGrades["GRADE_ID"].ToString()));
						}
					}
				}
				ddlResinType.Items.Add(new ListItem ("Other","0"));

				// populate search box with data from querystring 
				if (Filter!="")
				{
					txtSearch.Text = Filter;					
					if(ddlResinType.Items.FindByValue(Filter) != null)
						ddlResinType.Items.FindByValue(Filter).Selected = true;					
				}

				if (Request.QueryString["Filter"] != null)
				{
					if (System.Convert.ToInt32(Request.QueryString["Filter"])>=0) //if Filter = -1 measn show all and DO NOT user auto-filter.
					{
						ddlResinType.SelectedItem.Selected = false;
						ddlResinType.Items.FindByValue(Request.QueryString["Filter"].ToString()).Selected = true;
					}
				}

				// add the default value for the drop down lists
                if ((ViewState["UserType"].ToString().Equals("A")) || (ViewState["UserType"].ToString().Equals("B")) || (ViewState["UserType"].ToString().Equals("L")))
				{
					if (International)
					{
						ddlCompanies.Items.Add(new ListItem ("Offers","*"));
						ddlCompanies.Items.Add(new ListItem ("Bids","*Bid"));
						using (SqlConnection conn2 = new SqlConnection(Application["DBConn"].ToString()))
						{
							conn2.Open();
							string strSQL= "SELECT C.COMP_ID, C.COMP_NAME, C.COMP_TYPE FROM COMPANY C, BBOFFER " +
								"WHERE C.COMP_REG=1 AND C.COMP_TYPE IN ('P','S','D') AND C.COMP_ID = BBOFFER.OFFR_COMP_ID " +
								"GROUP BY C.COMP_ID, C.COMP_NAME, C.COMP_TYPE ORDER BY C.COMP_NAME";
							using (SqlDataReader dtrCompanies = DBLibrary.GetDataReaderFromSelect(conn2,strSQL))
							{
								while (dtrCompanies.Read())
								{
									ddlCompanies.Items.Add(new ListItem (dtrCompanies["COMP_NAME"].ToString(),dtrCompanies["COMP_ID"].ToString()));
								}
							}
						}
					}
					else
					{
						using (SqlConnection conn2 = new SqlConnection(Application["DBConn"].ToString()))
						{
							conn2.Open();
							using (SqlDataReader dtrBBOffer = DBLibrary.GetDataReaderFromSelect(conn2,"SELECT COUNT(1) FROM BBOFFER WHERE BBOFFER.OFFR_SIZE <> '38500' AND BBOFFER.OFFR_SIZE <> '44500' AND BBOFFER.OFFR_PORT IS NULL"))
							{
							
								dtrBBOffer.Read();
								ddlCompanies.Items.Add(new ListItem ("Offers (" + dtrBBOffer[0].ToString() + ")","*"));
							}
							
							using (SqlDataReader dtrBBId = DBLibrary.GetDataReaderFromSelect(conn2,"SELECT COUNT(1) FROM BBID  WHERE BBID.BID_SIZE <> '38500' AND BBID.BID_SIZE <> '44500' AND BBID.BID_PORT IS NULL"))
							{
								dtrBBId.Read();
								ddlCompanies.Items.Add(new ListItem ("Bids (" + dtrBBId[0].ToString() + ")","*Bid"));
							}
							
							string strSQL= "SELECT C.COMP_ID, C.COMP_NAME, C.COMP_TYPE, COUNT(OFFR_COMP_ID) FROM COMPANY C, BBOFFER " +
								"WHERE C.COMP_REG=1 AND C.COMP_TYPE IN ('P','S','D') AND C.COMP_ID = BBOFFER.OFFR_COMP_ID " +
								"  AND BBOFFER.OFFR_SIZE <> '38500' AND BBOFFER.OFFR_SIZE <> '44500' AND BBOFFER.OFFR_PORT IS NULL " +
								"GROUP BY C.COMP_ID, C.COMP_NAME, C.COMP_TYPE HAVING (COUNT(OFFR_COMP_ID) ) > 0 " +
								"ORDER BY COUNT(OFFR_COMP_ID)  DESC, C.COMP_NAME";
							using (SqlDataReader dtrCompanies2 =DBLibrary.GetDataReaderFromSelect(conn2,strSQL))
							{
								while (dtrCompanies2.Read())
								{
									ddlCompanies.Items.Add(new ListItem (dtrCompanies2["COMP_NAME"].ToString() +" ("+dtrCompanies2[3].ToString() + ")",dtrCompanies2["COMP_ID"].ToString()));
								}
							}							
						}						
					}
				}
				else
				{
					ddlCompanies.Items.Add(new ListItem ("Offers","*"));
					//ddlCompanies.Items.Add(new ListItem ("Bids","*Bid"));

					btnEmailOffers.Visible = false;
					btnArchive.Visible = false;
					btnDelete.Visible = false;
					btnHot.Visible = false;
					btnCool.Visible = false;					
				}

				//Load all sizes
				ddlSize.Items.Clear();
				ddlSize.Items.Add(new ListItem("All Sizes","*"));
				if (!(bool)ViewState["Export"])
				{
					ddlSize.Visible = true;
					lblSize.Visible = true;

                    ddlSize.Items.Add(new ListItem("Barrel", "125.8992"));
                    ddlSize.Items.Add(new ListItem("Million Pounds", "1000000"));
                    //ddlSize.Items.Add(new ListItem("Pounds (lbs)", "1"));
                    ddlSize.Items.Add(new ListItem("Metric Tonnes", "2205"));

                    //ddlSize.Items.Add(new ListItem("Rail Cars","190000"));
                    //ddlSize.Items.Add(new ListItem ("Truckload Boxes","42000"));
                    //ddlSize.Items.Add(new ListItem ("Truckload Bags","44092"));
                    //ddlSize.Items.Add(new ListItem ("Bulk Trucks","45000"));
                    //ddlSize.Items.Add(new ListItem ("Pounds (lbs)","1"));
                    //ddlSize.Items.Add(new ListItem ("Metric Tons","2205"));

					if(Size != "")
						ddlSize.Items.FindByValue(Size).Selected = true;
				}
				else
				{
					ddlSize.Visible = false;
					lblSize.Visible = false;
				}
				
				ViewState["hsBrokers"] = getBrokersName();

				if (Request.QueryString["COMP_ID"] != null)
				{
					ddlCompanies.Items.FindByValue(Request.QueryString["COMP_ID"].ToString()).Selected = true ;
				}

				if (Request.QueryString["StartOn"] != null)
				{
					if (Request.QueryString["StartOn"].ToString().Equals("bid"))
					{
						ddlCompanies.SelectedItem.Selected = false;
						try
						{
							ddlCompanies.Items.FindByValue("*Bid").Selected = true;
						}
						catch
						{	// do nothing 
						}
					}
				}
			

				PutSearchFieldsBackSession();
				ResetHeaders();
				BindDataGrid();
			}
		}

		public string Parse(string TextIN)
		{
			//  remove ' character
			string Parse;
			Parse = TextIN;
			while (Parse.IndexOf("'") > 0)
			{
				Parse = Parse.Replace("'", "");
			}
			// remove " character
			while (Parse.IndexOf("\"") > 0)
			{
				Parse = Parse.Replace("\"", "");
			}
			return Parse;

		}

		private void KeepSearchFieldsSession()
		{
			if (ddlCompanies.Visible==true) Session["Spot_Grid.ddlCompanies.SelectedItem.Value"] = ddlCompanies.SelectedItem.Value;
			if (ddlResinType.Visible==true) Session["Spot_Grid.ddlResinType.SelectedItem.Value"] = ddlResinType.SelectedItem.Value;
			if (txtSearch.Visible==true && Request.QueryString["Filter"] == null) Session["Spot_Grid.txtSearch.Text"] = txtSearch.Text;
		}
		 
		private void PutSearchFieldsBackSession()
		{
			if (Request.QueryString["Filter"]==null)
			{
				if (!EmptySession("Spot_Grid.ddlCompanies.SelectedItem.Value") && (ddlCompanies.Visible==true))
				{
					ddlCompanies.SelectedItem.Selected = false;
					ddlCompanies.Items.FindByValue(Session["Spot_Grid.ddlCompanies.SelectedItem.Value"].ToString()).Selected = true;
				}
			
				if (!EmptySession("Spot_Grid.ddlResinType.SelectedItem.Value") && (ddlResinType.Visible==true) && Request.QueryString["Filter"] == null)
				{
					ddlResinType.SelectedItem.Selected = false;
					ddlResinType.Items.FindByValue(Session["Spot_Grid.ddlResinType.SelectedItem.Value"].ToString()).Selected = true;
				}
			
				if (!EmptySession("Spot_Grid.txtSearch.Text") && (txtSearch.Visible==true))
				{
					txtSearch.Text = Session["Spot_Grid.txtSearch.Text"].ToString();
				}
			}
		}
         
		private bool EmptySession(string entryName)
		{
			if ((Session[entryName]==null) || Session[entryName].ToString()=="")
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public void BindDataGrid(string sortExpression)
		{
			DataView dv = (DataView)Session["SpotGrid_DataView"];
			dv.Sort = sortExpression;
			dgAdmin.DataSource = dv;

            //dgAdmin.Columns[5].Visible = false;
            //dgAdmin.Columns[6].Visible = false;
            //dgAdmin.Columns[7].Visible = false;

			dgAdmin.DataBind();

 
		}

		public void BindDataGrid()
		{
			using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				conn.Open();
                //SqlDataAdapter dadContent;
                //DataSet dstContent;

				Hashtable htParams = new Hashtable();
				
				// define users
				if (!ddlCompanies.SelectedItem.Value.Equals("*Bid"))
				{
					htParams.Add("@UserType","S");
					btnEmailOffers.Text = "Email Offers";
					// lblSubtitle.Text = "All offers subject to prior sale";
				}
				else
				{
					htParams.Add("@UserType","P");
					btnEmailOffers.Text = "Email Bids";
				}

				// flag to be set if this is one the import/export
				if ((bool)ViewState["Export"])
				{
					htParams.Add("@Export","true");
				}
			
				// add resin filter
				// dashboard requires that LLDPE be seperate from LPDE.
				//			if (!ddlResinType.SelectedItem.Value.Equals("*") && !(Filter.Equals("LDPE - Film")))
				//			{
				//				strSQL +=" ,@Resin_Filter='"+ddlResinType.SelectedItem.Value.ToString()+"'";
				//			}
				if (ddlResinType.SelectedItem.Value != "" && ddlResinType.SelectedItem.Value != "-1")
				{
					htParams.Add("@Resin_Grade",ddlResinType.SelectedItem.Value.ToString());
				}
				else if (ddlResinType.SelectedItem.Value == "-1")
				{
					string strPref = Session["Preferences"].ToString();

					//Use "POWER" method to find preference
					string strSelect ="SELECT CONT_LABL,CONT_ID, CONT_GRADE FROM CONTRACT WHERE POWER(2,CONT_ID-1)&("+strPref+")<>0 ORDER BY CONT_GRADE";
					using (SqlDataReader dtrResin = DBLibrary.GetDataReaderFromSelect(conn,strSelect))
					{
						if(dtrResin.Read())
						{
							string strPrevGrade = dtrResin["CONT_GRADE"].ToString();
							string strResinsList = strPrevGrade;
							while (dtrResin.Read())
							{
								if(strPrevGrade != dtrResin["CONT_GRADE"].ToString())
									strResinsList += ", " + dtrResin["CONT_GRADE"].ToString();
								strPrevGrade = dtrResin["CONT_GRADE"].ToString();
							}

							htParams.Add("@Resin_Grade",strResinsList);
						}
					}
				}

				if ((ddlSize.Visible) && (ddlSize.SelectedItem.Value != "*"))
				{
					htParams.Add("@Resin_Size",ddlSize.SelectedItem.Value.ToString());
				}

				// add company filter
				if (!ddlCompanies.SelectedItem.Value.Equals("*") && !ddlCompanies.SelectedItem.Value.Equals("*Bid"))
				{
					htParams.Add("@CompID",ddlCompanies.SelectedItem.Value.ToString());
				}

				// add a search in if the search
				if (!txtSearch.Text.Equals(""))
				{
					htParams.Add("@Search",Parse(txtSearch.Text));
				}

				// the dashboard filters
				//			if (!Filter.Equals(""))
				//			{
				//				if (Filter.Equals("LDPE - Film"))
				//				{
				//					strSQL +=",@Resin_Filter='LDPE',@Search='Film'";
				//				}
				//				else if (Filter.Equals("LDPE - inj"))
				//				{
				//					strSQL +=",@Resin_Filter='LDPE',@Search='inj'";
				//				}
				//				else
				//				{
				//					strSQL +=",@Search='"+Filter+"'";
				//				}
				//			}

				// add the sort command
				htParams.Add("@Sort",ViewState["Sort"].ToString());
				//Response.Write(strSQL);
				DataTable dtContent = DBLibrary.GetDataTableFromStoredProcedure(conn,"spSpot_Floor",htParams);
				
				//Calculating Freight
				dtContent.Columns.Add("VARFREIGHT",typeof(System.String));
				bool bOffer = !ddlCompanies.SelectedItem.Value.Equals("*Bid");
			
				//Saving ZipCode/Port Information
                if (!((ViewState["UserType"].ToString().Equals("A")) || (ViewState["UserType"].ToString().Equals("B")) || (ViewState["UserType"].ToString().Equals("T")) || (ViewState["UserType"].ToString().Equals("L"))))
				{
					if ((txtZipCode.Text!="") || (ddlCitiesPorts.SelectedIndex!=0))
					{
						if ((bool)ViewState["Export"]) //International Market
						{
							if (ddlCitiesPorts.SelectedIndex!=0) HelperFunction.SavePort(ddlCitiesPorts.SelectedItem.Value, Session["ID"].ToString(), this.Context);
						}
						else
						{
							string ZipCode="";
							if (ddlCitiesPorts.SelectedIndex!=0) ZipCode = ddlCitiesPorts.SelectedValue;
							if (txtZipCode.Text!="") ZipCode = txtZipCode.Text;
							if(Session["ID"] != null)
								if (ZipCode!="") HelperFunction.SaveZipCode(ZipCode, Session["ID"].ToString(), this.Context);
						}
					}
				}

				for(int i=0;i<dtContent.Rows.Count;i++)
				{
					if ((txtZipCode.Text!="") || (ddlCitiesPorts.SelectedIndex!=0))
					{
						HelperFunctionDistances hfd = new HelperFunctionDistances();
						decimal freight = 0;
						bool freightAvailable = false;
						if ((bool)ViewState["Export"]) //International Market
						{
							int PortID = System.Convert.ToInt32(ddlCitiesPorts.SelectedValue);
							if (dtContent.Rows[i]["VARFREIGHTPOINT"]!=System.DBNull.Value)
							{
								if (bOffer)
									freightAvailable = hfd.calculateInternationalFreight(System.Convert.ToInt32(dtContent.Rows[i]["VARFREIGHTPOINT"].ToString().Trim()),PortID, ref freight, Application["DBconn"].ToString());
								else
									freightAvailable = hfd.calculateInternationalFreight(PortID, System.Convert.ToInt32(dtContent.Rows[i]["VARFREIGHTPOINT"].ToString().Trim()), ref freight, Application["DBconn"].ToString());
							}
                            else if (dtContent.Rows[i]["VARTERM"].ToString() == "FOB Houston Warehouse")
                            {
								int houstonPortID = HelperFunction.getHoustonPortID(Application["DBConn"].ToString());
								if (bOffer)
									freightAvailable = hfd.calculateInternationalFreight(houstonPortID, PortID, ref freight, Application["DBconn"].ToString());
								else
									freightAvailable = hfd.calculateInternationalFreight(PortID, houstonPortID, ref freight, Application["DBconn"].ToString());
							}

							if (freightAvailable)
							{
								dtContent.Rows[i]["VARFREIGHT"] = String.Format("{0:#,###}",System.Convert.ToString(Math.Round(freight,2))); /// System.Convert.ToInt32(dtContent.Rows[i]["VARSIZE"])
								dtContent.Rows[i]["VARTERM"] = "CFR " + ddlCitiesPorts.SelectedItem.Text;
								if (bOffer)
									dtContent.Rows[i]["VARPRICE2"] = String.Format("{0:#,###}",Math.Round(System.Convert.ToDecimal(dtContent.Rows[i]["VARPRICE2"]) + System.Convert.ToDecimal(dtContent.Rows[i]["VARFREIGHT"]),2));
								else
									dtContent.Rows[i]["VARPRICE2"] = (String.Format("{0:#,###}",Math.Round(System.Convert.ToDecimal(dtContent.Rows[i]["VARPRICE2"]) - System.Convert.ToDecimal(dtContent.Rows[i]["VARFREIGHT"]),2)));
								//dtContent.Rows[i]["VARVALUE"] = Math.Round(System.Convert.ToDecimal(System.Convert.ToDecimal(dtContent.Rows[i]["VARPRICE2"]) * System.Convert.ToDecimal(dtContent.Rows[i]["VARSIZE"])),2);
								dtContent.Rows[i]["VARFREIGHT"] = "$" + dtContent.Rows[i]["VARFREIGHT"];
							}
							else
							{
								dtContent.Rows[i]["VARFREIGHT"] = "Not Available";
							}
						}
						else //Domestic Market
						{
							string ZipCode="";
							if (ddlCitiesPorts.SelectedIndex!=0) ZipCode = ddlCitiesPorts.SelectedValue;
							if (txtZipCode.Text!="") ZipCode = txtZipCode.Text;
						
							string placeFrom = "";
							string placeTo = "";
							dtContent.Rows[i]["VARFREIGHT"] = "Not Available";
							if (dtContent.Rows[i]["VARREALSIZE"].ToString()=="45000")
							{						
								if (bOffer)
									freightAvailable = hfd.calculateFreightByZipCode(dtContent.Rows[i]["VARFREIGHTPOINT"].ToString().Trim(),ZipCode, ref placeFrom, ref placeTo, ref freight, TruckType.BulkTruck, Application["DBconn"].ToString());
								else
									freightAvailable = hfd.calculateFreightByZipCode(ZipCode, dtContent.Rows[i]["VARFREIGHTPOINT"].ToString().Trim(), ref placeFrom, ref placeTo, ref freight, TruckType.BulkTruck, Application["DBconn"].ToString());
							}
							else if ((dtContent.Rows[i]["VARREALSIZE"].ToString()=="42000") || (dtContent.Rows[i]["VARREALSIZE"].ToString()=="44092"))
							{
								if (bOffer)
									freightAvailable = hfd.calculateFreightByZipCode(dtContent.Rows[i]["VARFREIGHTPOINT"].ToString().Trim(),ZipCode, ref placeFrom, ref placeTo, ref freight, TruckType.TruckLoad, Application["DBconn"].ToString());
								else
									freightAvailable = hfd.calculateFreightByZipCode(ZipCode, dtContent.Rows[i]["VARFREIGHTPOINT"].ToString().Trim(), ref placeFrom, ref placeTo, ref freight, TruckType.TruckLoad, Application["DBconn"].ToString());
							}
							else if (dtContent.Rows[i]["VARREALSIZE"].ToString()=="190000")
							{
								dtContent.Rows[i]["VARFREIGHT"] = "Included";
							}
								//pounds
							else if (dtContent.Rows[i]["VARREALSIZE"].ToString()=="1")
							{

								if (bOffer)
									freightAvailable = hfd.calculateFreightByZipCode(dtContent.Rows[i]["VARFREIGHTPOINT"].ToString().Trim(),ZipCode, ref placeFrom, ref placeTo, ref freight, TruckType.TruckLoad, Application["DBconn"].ToString());
								else
									freightAvailable = hfd.calculateFreightByZipCode(ZipCode, dtContent.Rows[i]["VARFREIGHTPOINT"].ToString().Trim(), ref placeFrom, ref placeTo, ref freight, TruckType.TruckLoad, Application["DBconn"].ToString());
							}
							else
							{
								dtContent.Rows[i]["VARFREIGHT"] = "Not Available";
							}
						
							if (freightAvailable)
							{
								if (dtContent.Rows[i]["VARREALSIZE"].ToString()=="1")
								{
									int lbs = System.Convert.ToInt32(dtContent.Rows[i]["VARSIZE"].ToString().Replace("lbs", "").Trim());
									double truck =  42000;
									int num_of_trucks = (int)Math.Ceiling (lbs / truck); 
									dtContent.Rows[i]["VARFREIGHT"] = num_of_trucks * Math.Round(freight / lbs ,4);
								}
								else
								{
									dtContent.Rows[i]["VARFREIGHT"] = Math.Round(freight / System.Convert.ToInt32(dtContent.Rows[i]["VARREALSIZE"]),4);
								}

								if (bOffer)
								{
									dtContent.Rows[i]["VARPRICE2"] = Math.Round(System.Convert.ToDecimal(dtContent.Rows[i]["VARPRICE2"]) + System.Convert.ToDecimal(dtContent.Rows[i]["VARFREIGHT"]),4);
									dtContent.Rows[i]["VARTERM"] = "Delivered " + placeTo;
								}
								else
								{
									dtContent.Rows[i]["VARPRICE2"] = Math.Round(System.Convert.ToDecimal(dtContent.Rows[i]["VARPRICE2"]) - System.Convert.ToDecimal(dtContent.Rows[i]["VARFREIGHT"]),4);
									dtContent.Rows[i]["VARTERM"] = "FOB Shipping";
								}
								//dtContent.Rows[i]["VARVALUE"] = Math.Round(System.Convert.ToDecimal(System.Convert.ToDecimal(dtContent.Rows[i]["VARPRICE2"]) * System.Convert.ToDecimal(dtContent.Rows[i]["VARREALSIZE"]) * System.Convert.ToDecimal(dtContent.Rows[i]["VARREALQTY"])),4);
							}
						}
					}
					else
					{
						if (dtContent.Rows[i]["VARREALSIZE"].ToString()=="190000")
						{
							dtContent.Rows[i]["VARFREIGHT"] = "Included";
						}
						else
						{
							dtContent.Rows[i]["VARFREIGHT"] = "Not Included";
						}
					}
				}

				if ((txtZipCode.Text!="") || (ddlCitiesPorts.SelectedIndex!=0))
				{
					string placeTo = "";
					if ((bool)ViewState["Export"]) //International
					{
						placeTo = ddlCitiesPorts.SelectedItem.Text;
						lblMessageFreight.Text = "Prices calculated to " + placeTo + " <BR><A style=\"COLOR: red\" href='/spot/Spot_Floor.aspx?Export=true&PreSelect=false'>Remove Freight</a>";
					}
					else
					{
						if (ddlCitiesPorts.SelectedIndex!=0) placeTo = ddlCitiesPorts.SelectedItem.Text;
						if (txtZipCode.Text!="") placeTo = txtZipCode.Text;
						lblMessageFreight.Text = "Prices calculated to " + placeTo + " <BR><A style=\"COLOR: red\" href='/spot/Spot_Floor.aspx'>Remove Freight</a>";
					}
				}
				else
				{
					lblMessageFreight.Text = "Compare prices by including frieght costs!&nbsp;";
				}

				DataView dtView = new DataView(dtContent);
				Session["SpotGrid_DataView"] = dtView.Table.DefaultView;

				if(dtView.Table.Rows.Count != 0)
				{
					dgAdmin.Visible = true;
					dgAdmin.DataSource = Session["SpotGrid_DataView"];
					dgAdmin.DataBind();
					
				}
				else
				{
					dgAdmin.Visible = false;
					lblGridEmpy.Visible = true;
				}
			} // end using conn			
			
			KeepSearchFieldsSession();
		}
		
		//<summary>
		// Handler for when the user changes the resin filter
		//</summary>
		public void Change_Resin(object sender, EventArgs e)
		{
			ResetHeaders();
			BindDataGrid();
		}

		//<summary>
		// Handler for when the user changes the resin filter
		//</summary>
		public void Change_Size(object sender, EventArgs e)
		{
			ResetHeaders();
			BindDataGrid();
		}
		//<summary>
		// Handler for when the user changes the company
		//</summary>
		public void Change_Company(object sender, EventArgs e)
		{
			ResetHeaders();
			BindDataGrid();
		}
		//<summary>
		// create string of spot bids in a session variable and send the person to the demo leads screen
		//</summary>
		public void Click_Send_Mass_Mail(object sender, EventArgs e)
		{
			RowSelectorColumn rsc;
			rsc = RowSelectorColumn.FindColumn(dgAdmin);

			System.Text.StringBuilder sbOrders;
			sbOrders = new System.Text.StringBuilder();
			for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++)
			{
				int selIndex = rsc.SelectedIndexes[selectedIndex];
				sbOrders.Append(dgAdmin.DataKeys[selIndex].ToString() +",");
			}
			// if bids are selected the 'offers' being sent must be bids
			if (ddlCompanies.SelectedItem.Value.Equals("*Bid"))
			{
				Session["strCompId"] ="-1"; // bids
			}
			else
			{
				Session["strCompId"]=null; //offers
			}
			// system must pass whether they are export bids/ offers
			if ((bool)ViewState["Export"])
			{
				Session["strExport"] ="-1";
			}
			Session["strEmailOffer_IDS"] =  sbOrders.ToString();
			// send the user to the demo leads page to select users
			Response.Redirect ("/Administrator/CRM.aspx?EmailOffers=true");
		}

		//<summary>
		// moves the item to the archieve
		//</summary>
		public void Click_Move_To_Archive(object sender, EventArgs e)
		{
			RowSelectorColumn rsc;
			rsc = RowSelectorColumn.FindColumn(dgAdmin);
			string SQL;
			
			for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++)
			{
				int selIndex = rsc.SelectedIndexes[selectedIndex];
				Application.Lock();
				Hashtable htParams = new Hashtable();
				htParams.Add("@ID",dgAdmin.DataKeys[selIndex].ToString());
				if (ddlCompanies.SelectedItem.Value.Equals("*Bid"))
				{
					htParams.Add("@Is_Bid","1");
				}
                
				DBLibrary.ExecuteStoredProcedure(Application["DBconn"].ToString(),"spSendToArchive",htParams);
				Application.UnLock();
			}

			BindDataGrid();
		}
         
		//<summary>
		// makes offer 'hot' which posts the offer to the home page scollbar
		//</summary>
		public void Click_MakeHot(object sender, EventArgs e)
		{
			RowSelectorColumn rsc;
			rsc = RowSelectorColumn.FindColumn(dgAdmin);

			for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++)
			{
				int selIndex = rsc.SelectedIndexes[selectedIndex];
				Hashtable htParam = new Hashtable();
				htParam.Add("@OFFR_ID",dgAdmin.DataKeys[selIndex].ToString());
				Application.Lock();
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"UPDATE BBOFFER SET OFFR_HOT ='1' WHERE OFFR_ID=@OFFR_ID",htParam);
				Application.UnLock();
			}
			BindDataGrid();
		}
		//<summary>
		// makes offer 'ccol' which removes featured offer from the home page scollbar
		//</summary>
		public void Click_MakeCool(object sender, EventArgs e)
		{
			RowSelectorColumn rsc;
			rsc = RowSelectorColumn.FindColumn(dgAdmin);

			for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++)
			{
				int selIndex = rsc.SelectedIndexes[selectedIndex];
				Hashtable htParam = new Hashtable();
				htParam.Add("@OFFR_ID",dgAdmin.DataKeys[selIndex].ToString());
				Application.Lock();
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"UPDATE BBOFFER SET OFFR_HOT ='0' WHERE OFFR_ID=@OFFR_ID",htParam);
				Application.UnLock();
			}
			BindDataGrid();
		}

		//<summary>
		// deletes the offer
		//</summary>
		public void Click_Delete(object sender, EventArgs e)
		{
			RowSelectorColumn rsc;
			rsc = RowSelectorColumn.FindColumn(dgAdmin);

			for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++)
			{
				int selIndex = rsc.SelectedIndexes[selectedIndex];
				Hashtable htParam = new Hashtable();
				htParam.Add("@ID",dgAdmin.DataKeys[selIndex].ToString());
				Application.Lock();
				if (ddlCompanies.SelectedItem.Value.Equals("*Bid"))
				{
					DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"DELETE FROM BBID WHERE BID_ID=@ID",htParam);
				}
				else
				{
					DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"DELETE FROM BBOFFER WHERE OFFR_ID=@ID",htParam);
				}
				Application.UnLock();
			}
			ResetHeaders();
			BindDataGrid();
		}
		// <summary>
		// Sorting function
		// </summary>
		public void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{
			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;
			//  Parse the sort expression - delimiter space
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
			ColumnToSort = SortExprs[0];
			// If a sort order is specified get it, else default is descending
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DESC";
			}
			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;
			// Figure out the column index
			int iIndex;
			iIndex = 1;
			switch(ColumnToSort.ToUpper())
			{
				case "VARID":
					iIndex = 2;
					break;
				case "VARCONTRACT":
					iIndex = 3;
					break;
				case "VARSIZE":
					iIndex = 4;
					break;
                case "VARMELT":
                    iIndex = 5;
                    break;
                case "VARDENS":
                    iIndex = 6;
                    break;
                case "VARADDS":
                    iIndex = 7;
                    break;
				case "VARTERM":
					iIndex = 8;
					break;
				case "VARUSR":
					iIndex = 9;
					break;
				case "VARPRICE":
					iIndex = 10;
					break;
				case "VARPRICE2":
					iIndex = 11;
					break;
				case "VARFREIGHT":
					iIndex = 12;
					break;
				case "VARVALUE":
					iIndex = 13;
					break;
				case "VARBIDOFFER":
					iIndex = 14;
					break;
                case "VARDELIVERYMONTH":
                    iIndex = 15;
                    break;
			}
			// alter the column's sort expression
			dgAdmin.Columns[iIndex].SortExpression = NewSortExpr;
			ResetHeaders();
			if (NewSearchMode.Equals("DESC"))
			{
				dgAdmin.Columns[iIndex].HeaderText = "<img border=\"0\" src=\"/pics/icons/icon_down_arrow_black.gif\">" + dgAdmin.Columns[iIndex].HeaderText;
			}
			else
			{
				dgAdmin.Columns[iIndex].HeaderText =  "<img border=\"0\" src=\"/pics/icons/icon_up_arrow_black.gif\">" + dgAdmin.Columns[iIndex].HeaderText;
			}
			dgAdmin.CurrentPageIndex = 0;

			// Sort the data in new order
			ViewState["Sort"] = NewSortExpr;
			// reset dbase page to the first one whether the sorting changes
			BindDataGrid(NewSortExpr);
		}

		private void ResetFreightBar()
		{
			if ((bool)ViewState["Export"])
			{
				if (!lblZipCode.Visible == false) //screen mode was not International
				{
					lblZipCode.Visible = false;
					txtZipCode.Visible = false;
					txtZipCode.Text = "";
					lblCityPort.Text = "Port:";
					LoadPorts();

					if (!((Request.QueryString["PreSelect"]!=null) && (Request.QueryString["PreSelect"]=="false")))
					{
						int portID = 0;
						if(Session["ID"] != null)
							HelperFunction.getInternationalPort(Application["DBConn"].ToString(), Session["ID"].ToString());

						if (portID != 0)
						{
							ddlCitiesPorts.SelectedItem.Selected = false;
							ddlCitiesPorts.Items.FindByValue(portID.ToString()).Selected = true;
						}
					}
				}
			}
			else
			{
				if ((lblZipCode.Visible == false) || (ddlCitiesPorts.Items.Count==0)) //screen mode was not Domestic
				{
					lblZipCode.Visible = true;
					txtZipCode.Visible = true;
					lblCityPort.Text = "or City:";
					LoadCities();
				}
			}
		}

		private void ResetHeaders()
		{
			ResetFreightBar();

			dgAdmin.Columns[2].HeaderText = "#";
			dgAdmin.Columns[3].HeaderText = "Product";
			if ((bool)ViewState["Export"])
			{
				dgAdmin.Columns[4].HeaderText = "Metric Tons";
			}
			else
			{
				dgAdmin.Columns[4].HeaderText = "Size";
			}

			dgAdmin.Columns[5].HeaderText = "Melt";
			dgAdmin.Columns[6].HeaderText = "Density";
			dgAdmin.Columns[7].HeaderText = "Adds";

            dgAdmin.Columns[5].Visible = false;
            dgAdmin.Columns[6].Visible = false;
            dgAdmin.Columns[7].Visible = false;
            dgAdmin.Columns[12].Visible = false;
            

			dgAdmin.Columns[8].HeaderText = "Location";
			dgAdmin.Columns[14].HeaderText = "Bid/Offer Date";


            if ((ViewState["UserType"].ToString().Equals("A")) || (ViewState["UserType"].ToString().Equals("B")) || (ViewState["UserType"].ToString().Equals("L")))
			{
				dgAdmin.Columns[9].HeaderText = "Seller";
				if (!ddlCompanies.SelectedItem.Value.Equals("*Bid")) //Offers
				{
					dgAdmin.Columns[10].HeaderText = "TPE Buy Price";
					dgAdmin.Columns[11].HeaderText = "TPE Sell Price";
				}
				else //BIDs
				{
					dgAdmin.Columns[10].HeaderText = "TPE Sell Price";
					dgAdmin.Columns[11].HeaderText = "TPE Buy Price";
				}
				dgAdmin.Columns[12].HeaderText = "Freight";
				dgAdmin.Columns[13].HeaderText = "Value";
				dgAdmin.Columns[13].Visible = false;
			}
			else
			{
				dgAdmin.Columns[0].Visible = false;
				dgAdmin.Columns[9].Visible = false;
				dgAdmin.Columns[10].Visible = false;
				//dgAdmin.Columns[11].HeaderText = "Price";
				if ((bool)ViewState["Export"]) //International
					dgAdmin.Columns[11].HeaderText = "Price $/MT";
				else
					dgAdmin.Columns[11].HeaderText = "Price $/lb";
				dgAdmin.Columns[12].Visible = false;
				dgAdmin.Columns[13].Visible = false;
				dgAdmin.Columns[14].Visible = false;
				dgAdmin.Columns[16].Visible = false;
			}
		}
		
		double dbTotalValue = 0.0;
		double dbTotalWeight = 0.0;
		// calculates total for the footer
		void dgAdmin_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				// setting the mouseover color
				e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver';this.style.cursor='hand';");
				// shows that the offer is hot.  bids can not be made hot
				if ((ViewState["UserType"].ToString().Equals("A") || ViewState["UserType"].ToString().Equals("B") || ViewState["UserType"].ToString().Equals("L")) && !ddlCompanies.SelectedItem.Value.Equals("*Bid"))
				{
					if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARHOT")).ToString().Equals("1"))
					{
						e.Item.Cells[2].Text = "<font color=red>"+Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString()+"</font>";
					}
				}

				string density = e.Item.Cells[6].Text;
				if ((density.Length>2) && (density.Substring(0,2) == "0."))
					e.Item.Cells[6].Text = density.Substring(1);

				if (e.Item.ItemType == ListItemType.Item)
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee';this.style.cursor='pointer';");
				}
				else
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3';this.style.cursor='pointer';");
				}

				string InquireParams = "";
				if ((bool)ViewState["Export"]) InquireParams = "&Export=true";
				if (ddlCompanies.SelectedItem.Value.Equals("*Bid")) InquireParams += "&BidOffer=B";
				
				if (txtZipCode.Text != "")
					InquireParams += "&DestinationFreight=" + txtZipCode.Text;
				else if ((ddlCitiesPorts.SelectedItem.Value != "") && (ddlCitiesPorts.SelectedItem.Value != "0"))
					InquireParams += "&DestinationFreight=" + ddlCitiesPorts.SelectedItem.Value;
				
				if (!ViewState["UserType"].ToString().Equals("A") && !ViewState["UserType"].ToString().Equals("B") && !ViewState["UserType"].ToString().Equals("L"))
				{
					e.Item.Attributes.Add("onclick", "window.location ='/Spot/Inquire.aspx?ID="+(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString() +InquireParams+"' ");
				}
				dbTotalValue += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARVALUE"));


				if ((bool)ViewState["Export"])
				{
					dbTotalWeight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSIZE"));
				}
				else
				{
					dbTotalWeight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARWGHT"));
				}

				if ((ViewState["UserType"].ToString().Equals("A")) || (ViewState["UserType"].ToString().Equals("B")) || (ViewState["UserType"].ToString().Equals("L")))
				{
					// admins can edit all spot order
					string market = "Domestic";
					if ((bool)ViewState["Export"]) market = "International";

					if (!ddlCompanies.SelectedItem.Value.Equals("*Bid"))
					{
						e.Item.Cells[1].Text = "<a href=\"/spot/Resin_Entry.aspx?Screen="+Screen+"&type=offer&Change=" +(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString() + "&Market=" + market.ToString() + "\">Edit</a><BR>";
						if (ViewState["UserType"].ToString().Equals("A") || ViewState["UserType"].ToString().Equals("L"))
						{
							e.Item.Cells[1].Text +="<a  href=\"/spot/Matched_Order.aspx?Offer="+(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString() +"\">Sell<a/>";
						}
					}
					else
					{
						e.Item.Cells[1].Text = "<a href=\"/spot/Resin_Entry.aspx?Screen="+Screen+"&type=bid&Change=" +(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString() + "&Market=" + market.ToString() + "\">Edit</a><BR>";
					}
				}
				else
				{
					e.Item.Cells[1].Text = "<a class=blackcontent href=\"/Spot/Inquire.aspx?ID=" +(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString() + InquireParams+"\">Inquire</a><BR>";
				}

				if ((bool)ViewState["Export"])
				{
					e.Item.Cells[4].Text = String.Format("{0:#,###}", (DataBinder.Eval(e.Item.DataItem, "VARSIZE")));
					e.Item.Cells[10].Text = String.Format("{0:#,###}", (DataBinder.Eval(e.Item.DataItem, "VARPRICE")));
					e.Item.Cells[11].Text = String.Format("{0:#,###}", (DataBinder.Eval(e.Item.DataItem, "VARPRICE2")));
				}
//				else
//				{
//					if ((ViewState["UserType"].ToString().Equals("A")) || (ViewState["UserType"].ToString().Equals("B")) || (ViewState["UserType"].ToString().Equals("T")))	
//					{
//						if (e.Item.Cells[19].Text.ToString()!="&nbsp;")
//						{
//							string quality="";
//							switch(e.Item.Cells[19].Text)
//							{
//								case "P":
//									quality = "(Prime)";
//									break;
//								case "O":
//									quality = "(OffGrade)";
//									break;
//								case "R":
//									quality = "(Regrind/Repro)";
//									break;
//								default:
//									break;
//							}			
//							e.Item.Cells[3].Text += quality;
//						}
//					}
//				}			

				//Checking if the Offer/Bid was add by a customer
				if (e.Item.Cells[18].Text != "&nbsp;") //add by admin or broker
				{
					((System.Web.UI.WebControls.Image)e.Item.Cells[16].Controls[1]).Visible = false;
					string firstName = "";
					if (((Hashtable)ViewState["hsBrokers"]).Contains(e.Item.Cells[18].Text)) firstName = ((Hashtable)ViewState["hsBrokers"])[e.Item.Cells[18].Text].ToString();
					e.Item.Cells[16].Text = firstName;
				}

				//Add link to Demo/Exchange user
				if (e.Item.Cells[17].Text != "&nbsp;") //add link
				{
					e.Item.Cells[9].Text = "<a href='/administrator/Contact_Details.aspx?ID=" + e.Item.Cells[17].Text + "'>" + e.Item.Cells[9].Text + "</a>";
				}

				if (e.Item.Cells[8].Text.ToLower().IndexOf("invalid",0)>=0)
				{
					if (ViewState["UserType"].ToString().Equals("A") || ViewState["UserType"].ToString().Equals("B") || ViewState["UserType"].ToString().Equals("L"))
						e.Item.Cells[8].Text = "<font color='red'></b>invalid!<b></font>";
					else
						e.Item.Cells[8].Text = "Unknown";
				}
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[1].Text ="<b>Total</b>";
				if ((bool)ViewState["Export"])
				{
					e.Item.Cells[4].Text = "<b> " + String.Format("{0:#,###}", dbTotalWeight)+" Metric Tons</b>";
				}
				else
				{
					e.Item.Cells[4].Text = "<b> " + String.Format("{0:#,###}", dbTotalWeight)+" lbs</b>";
				}
				//e.Item.Cells[12].Text = "<b> " + String.Format("{0:c}", dbTotalValue)+"</b>";
			}

			else if (e.Item.ItemType == ListItemType.Header)
			{
				if ((bool)ViewState["Export"])
				{
					dgAdmin.Columns[4].HeaderText ="Metric Tons";
				}
			}
		}

		private void LoadCities()
		{
			HelperFunctionDistances hfd = new HelperFunctionDistances();
			hfd.loadMostPopularCities(ddlCitiesPorts, Session["Typ"].ToString());
		}

		private void LoadPorts()
		{
			ddlCitiesPorts.Items.Clear();
						
			ddlCitiesPorts.Items.Add(new ListItem("Select Port...",""));
			DataTable dtPorts = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(),"SELECT IP.PORT_ID, C.CTRY_NAME  + ISNULL(' - '+NULLIF(IP.PORT_CITY,'') ,'')  PORT FROM INTERNATIONAL_PORT IP, COUNTRY C WHERE IP.PORT_COUNTRY = C.CTRY_CODE ORDER BY C.CTRY_NAME");
			foreach(DataRow dr in dtPorts.Rows)
			{
				ddlCitiesPorts.Items.Add(new ListItem(dr["PORT"].ToString(),dr["PORT_ID"].ToString()));
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.imbFind.Click += new System.Web.UI.ImageClickEventHandler(this.imbFind_Click);
			this.imbUpdate.Click += new System.Web.UI.ImageClickEventHandler(this.imbUpdate_Click);
			this.dgAdmin.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.SortDG);
            this.dgAdmin.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgAdmin_ItemDataBound);

		}
		#endregion
		
		protected void dgAdmin_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private Hashtable getBrokersName()
		{
			Hashtable hsResult = new Hashtable();

            DataTable dtBrokers = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), "select pers_id, pers_frst_name from person where pers_type in ('A', 'B', 'T', 'L')");
			foreach(DataRow dr in dtBrokers.Rows)
			{
				hsResult.Add(dr["pers_id"].ToString(),dr["pers_frst_name"].ToString());
			}
			
			return hsResult;
		}

		//<summary>
		// Handler for when the user changes the resin filter
		//</summary>
		private void imbFind_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			ResetHeaders();
			BindDataGrid();
		}

		private void imbUpdate_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			ResetHeaders();
			BindDataGrid();
		}
	}
}