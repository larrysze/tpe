<%@ Page Language="C#" AutoEventWireup="true" Codebehind="EditHistoricalCharts.aspx.cs" Inherits="localhost.Tools.EditHistoricalCharts" MasterPageFile="~/MasterPages/Menu.Master" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<asp:Content runat="server" ContentPlaceHolderID="cphJavaScript">
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
    
    <asp:DropDownList runat="server" ID="ddlChart">
    </asp:DropDownList>
    
    <br />
    
    <dotnet:Chart runat="server" ID="chartObj">
    </dotnet:Chart>
    
    <br />
    
    <asp:DataGrid ID="dg" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="Header" ItemStyle-CssClass="Content">
        <Columns>
            <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ItemStyle-CssClass="LinkNormal"></asp:EditCommandColumn>
            <asp:BoundColumn DataField="DATE" ReadOnly="True" HeaderText="Date" DataFormatString="{0:d}"></asp:BoundColumn>
            <asp:BoundColumn DataField="PRICE_LOW" HeaderText="Low Price"></asp:BoundColumn>
            <asp:BoundColumn DataField="PRICE_HIGH" HeaderText="High Price"></asp:BoundColumn>
            <asp:BoundColumn DataField="Weight" HeaderText="Weight"></asp:BoundColumn>
        </Columns>
    </asp:DataGrid>
    
    <br />
    
    <input type=button onclick="updateChart()" value="Update Chart" />
    
    <br />
    
    <asp:Button runat=server Text="Save Changes" ID=btnSave OnClick="btnSave_Click" />
</asp:Content>
