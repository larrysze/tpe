<%@ Page language="c#" Codebehind="EditMU.aspx.cs" AutoEventWireup="True" Inherits="localhost.Tools.EditMU" MasterPageFile="~/MasterPages/Menu.Master" Title="Edit Market Update" %>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
<br>
<center><span class="Content Color2"><b>TPE Market Update</B></SPAN></CENTER><br><span class="Content">
<asp:placeholder id=htmlPlace Runat="server"></asp:placeholder></span>

<asp:DropDownList runat="server" AutoPostBack="True" CssClass="InputForm" ID="ddlIssue" OnSelectedIndexChanged="ddlIssue_SelectedIndexChanged">
</asp:DropDownList>

<asp:panel id=mainPanel Runat="server">
<BR><FONT class="Content"><B>Feedstock cost</B></FONT> 
<TABLE id=Table1 width="100%" border=0>
  <TR width="100%"></TR></TABLE>
<asp:textbox id=txtComments runat="server" CssClass="InputForm" Width="100%" TextMode="MultiLine" Height="170px"></asp:textbox><BR><BR>
<HR SIZE=2>
<BR>
<TABLE id=Table2 width="100%">
  <TR>
    <TD style="WIDTH: 183px; HEIGHT: 20px">
      <P><FONT class="Content"><B>Polyethylene</B></FONT></P></TD></TR>
  <TR>
    <TD colSpan=2>
<asp:TextBox id=txtPreTextPE Runat="server" CssClass="InputForm" Width="100%" TextMode="MultiLine" Height="130px"></asp:TextBox></TD></TR>
  <TR>
    <TD width="50%">
<asp:image id=imgPolyethyleneYear runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_3_1M.png" height="225"></asp:image></TD>
    <TD width="50%">
<asp:image id=imgPolyethyleneMonth runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_3_1Y.png" height="225"></asp:image></TD></TR>
  <TR>
    <TD style="HEIGHT: 215px" colSpan=2>
<asp:textbox id=txtPolyethylene runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="230px"></asp:textbox></TD>
  <TR></TR>
  <TR>
    <TD style="WIDTH: 183px">
      <P>&nbsp;</P>
      <P><FONT class="Content"><B>Polypropylene</B></FONT></P></TD></TR>
  <TR>
    <TD colSpan=2>
<asp:TextBox id=txtPreTextPP Runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="130px"></asp:TextBox></TD></TR>
  <TR>
    <TD width="50%">
<asp:image id=Image1 runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_26_1M.png" height="225"></asp:image></TD>
    <TD width="50%">
<asp:image id=Image2 runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_26_1Y.png" height="225"></asp:image></TD>
  <TR>
    <TD style="HEIGHT: 215px" colSpan=2>
<asp:textbox id=txtPolypropylene runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="230px"></asp:textbox></TD></TR>
  <TR>
    <TD style="WIDTH: 183px">
      <P>&nbsp;</P>
      <P><FONT class="Content"><B>Polystyrene</B></FONT></P></TD></TR>
  <TR>
    <TD colSpan=2>
<asp:TextBox id=txtPreTextPS Runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="130px"></asp:TextBox></TD></TR>
  <TR>
    <TD width="50%">
<asp:image id=Image3 runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_20_1M.png" height="225"></asp:image></TD>
    <TD width="50%">
<asp:image id=Image4 runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_20_1Y.png" height="225"></asp:image>
  <TR>
    <TD style="HEIGHT: 215px" colSpan=2>
<asp:textbox id=txtPolystyrene CssClass="InputForm" runat="server" Width="100%" TextMode="MultiLine" Height="230px"></asp:textbox></TD></TR></TABLE><BR><BR>
<BR><BR><BR>
<TABLE id=Table3 width="100%" runat="server">
  <TR>
    <TD align=center style="height: 23px">
<asp:button id=btnSave CssClass="Content Color2" runat="server" Text="Save" onclick="btnSave_Click"></asp:button> 
</TD>
</TR></TABLE></asp:panel>

</asp:Content>
