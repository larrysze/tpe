using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using TPE.Utility;
using System.Data.SqlClient;

namespace localhost.Tools
{
    public partial class ConvertMU : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((string)Session["Typ"] != "A")
            {
                Response.Redirect("/default.aspx");
            }
        }

        private void ConvertWRfromDB()
        {
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, "SELECT review_date, spot_floor FROM weekly_reviews WHERE spot_floor is not null");
                while (dtr.Read())
                {
                    string spot_floor = dtr["spot_floor"].ToString();
                    string date = dtr["review_date"].ToString();
                    if (spot_floor.Trim() != "")
                    {
                        insertSpotSnapshot(date, spot_floor, "WeeklyReviewSpotOffersSummary");
                    }
                }
            }
        }

        private void ConvertMUFromXML()
        {
            string strXmlFileName = Server.MapPath("/Administrator/marketUpdate.xml");
            if (System.IO.File.Exists(strXmlFileName.ToString()))
            {
                XmlTextReader xmlreader = new XmlTextReader(strXmlFileName.ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.Load(xmlreader);
                XmlNodeList allrecords = xmldoc.GetElementsByTagName("MU_record");
                
                // last one is empty we don't need it.
                for (int i = 0; i < (allrecords.Count - 1); i++ )
                {
                    string date = allrecords[i]["date"].InnerText;
                    string snapshot = allrecords[i]["snapshot"].InnerText;
                    string path1 = allrecords[i]["path1"].InnerText;
                    string path2 = allrecords[i]["path2"].InnerText;
                    string path3 = allrecords[i]["path3"].InnerText;
                    string path4 = allrecords[i]["path4"].InnerText;
                    string path5 = allrecords[i]["path5"].InnerText;
                    string path6 = allrecords[i]["path6"].InnerText;
                    string comments = allrecords[i]["common"]["comments"].InnerText;
                    string unpublished = allrecords[i]["unpublished"].InnerText;
                    string total_pounds = allrecords[i]["total_pounds"].InnerText;

                    string polyethylene_article = allrecords[i]["polyethylene_article"].InnerText;
                    string polypropylene_article = allrecords[i]["polypropylene_article"].InnerText;
                    string polystyrene_article = allrecords[i]["polystyrene_article"].InnerText;

                    Hashtable param = new Hashtable();
                    string sqlStat = "INSERT INTO MARKET_UPDATES(date, comments, polystyrene, polyethylene, polypropylene, published, PEMonthChartPath, PEYearChartPath, PPMonthChartPath, PPYearChartPath, PSMonthChartPath, PSYearChartPath)" +
                                                        " VALUES(@date,@comments, @polystyrene,  @polyethylene, @polypropylene, @published, @PEMonthChartPath, @PEYearChartPath, @PPMonthChartPath, @PPYearChartPath, @PSMonthChartPath, @PSYearChartPath)";
                    param.Add("@date", date);
                    param.Add("@total", Decimal.Parse (total_pounds.Substring( 0, total_pounds.IndexOf(" lbs") )));
                    param.Add("@comments", comments);
                    param.Add("@polystyrene", polystyrene_article);
                    param.Add("@polyethylene", polyethylene_article);
                    param.Add("@polypropylene", polypropylene_article);
                    param.Add("@published", !Convert.ToBoolean(unpublished));
                    param.Add("@PEMonthChartPath", path1);
                    param.Add("@PEYearChartPath", path2);
                    param.Add("@PPMonthChartPath", path3);
                    param.Add("@PPYearChartPath", path4);
                    param.Add("@PSMonthChartPath", path5);
                    param.Add("@PSYearChartPath", path6);

                    DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlStat, param);

                    
                    insertSpotSnapshot(date, snapshot, "MarketUpdateSpotOffersSummary");
                }

                xmlreader.Close();

            }
        }

        private void insertSpotSnapshot(string date, string snapshot, string strTableName)
        {

            Hashtable param = new Hashtable();
            string sqlStr = "";

            sqlStr = "INSERT INTO " + strTableName + "(date, grade_id, weight, price_low, price_high) " +
                                                "VALUES(@date, @grade_id, @weight, @price_low, @price_high)";

            string[] values = null;
            try
            {
                values = returnValues(snapshot, 6);
                param.Add("@date", date);
                param.Add("@grade_id", 6);
                param.Add("@weight", Decimal.Parse(values[0]));
                param.Add("@price_low", values[1]);
                param.Add("@price_high", values[2]);
                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlStr, param);
            }
            catch (Exception ee)
            { }

            try
            {
                values = returnValues(snapshot, 8);
                param.Clear();
                param.Add("@date", date);
                param.Add("@grade_id", 8);
                param.Add("@weight", Decimal.Parse(values[0]));
                param.Add("@price_low", values[1]);
                param.Add("@price_high", values[2]);
                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlStr, param);
            }
            catch (Exception ee)
            { }


            try
            {
                values = returnValues(snapshot, 4);
                param.Clear();
                param.Add("@date", date);
                param.Add("@grade_id", 4);
                param.Add("@weight", Decimal.Parse(values[0]));
                param.Add("@price_low", values[1]);
                param.Add("@price_high", values[2]);
                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlStr, param);
            }
            catch (Exception ee)
            { }

            try
            {
                values = returnValues(snapshot, 9);
                param.Clear();
                param.Add("@date", date);
                param.Add("@grade_id", 9);
                param.Add("@weight", Decimal.Parse(values[0]));
                param.Add("@price_low", values[1]);
                param.Add("@price_high", values[2]);
                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlStr, param);
            }
            catch (Exception ee)
            { }


            try
            {
                values = returnValues(snapshot, 2);
                param.Clear();
                param.Add("@date", date);
                param.Add("@grade_id", 2);
                param.Add("@weight", Decimal.Parse(values[0]));
                param.Add("@price_low", values[1]);
                param.Add("@price_high", values[2]);
                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlStr, param);
            }
            catch (Exception ee)
            { }


            try
            {
                values = returnValues(snapshot, 3);
                param.Clear();
                param.Add("@date", date);
                param.Add("@grade_id", 3);
                param.Add("@weight", Decimal.Parse(values[0]));
                param.Add("@price_low", values[1]);
                param.Add("@price_high", values[2]);
                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlStr, param);
            }
            catch (Exception ee)
            { }

            try
            {
                values = returnValues(snapshot, 5);
                param.Clear();
                param.Add("@date", date);
                param.Add("@grade_id", 5);
                param.Add("@weight", Decimal.Parse(values[0]));
                param.Add("@price_low", values[1]);
                param.Add("@price_high", values[2]);
                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlStr, param);
            }
            catch (Exception ee)
            { }

            try
            {
                values = returnValues(snapshot, 10);
                param.Clear();
                param.Add("@date", date);
                param.Add("@grade_id", 10);
                param.Add("@weight", Decimal.Parse(values[0]));
                param.Add("@price_low", values[1]);
                param.Add("@price_high", values[2]);
                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlStr, param);
            }
            catch (Exception ee)
            { }

            try
            {
                values = returnValues(snapshot, 7);
                param.Clear();
                param.Add("@date", date);
                param.Add("@grade_id", 7);
                param.Add("@weight", Decimal.Parse(values[0]));
                param.Add("@price_low", values[1]);
                param.Add("@price_high", values[2]);
                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlStr, param);
            }
            catch (Exception ee)
            { }

            try
            {
                values = returnValues(snapshot, 11);
                param.Clear();
                param.Add("@date", date);
                param.Add("@grade_id", 11);
                param.Add("@weight", Decimal.Parse(values[0]));
                param.Add("@price_low", values[1]);
                param.Add("@price_high", values[2]);
                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlStr, param);
            }
            catch (Exception ee)
            { }

            try
            {
                values = returnValues(snapshot, 1);
                param.Clear();
                param.Add("@date", date);
                param.Add("@grade_id", 1);
                param.Add("@weight", Decimal.Parse(values[0]));
                param.Add("@price_low", values[1]);
                param.Add("@price_high", values[2]);
                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlStr, param);
            }
            catch (Exception ee)
            { }

        }

        private string[] returnValues(string str, int num)
        {
            string[] values = new string[3];

            str = str.Substring(str.IndexOf("Filter=" + num));
            str = str.Substring(str.IndexOf("<font size=")).Substring(15);
            values[0] = str.Substring(0, str.IndexOf("</font>"));
            str = str.Substring(str.IndexOf("<b>")).Substring(3);
            values[1] = str.Substring(1, str.IndexOf("</b>") - 1);
            str = str.Substring(str.IndexOf("<b>")).Substring(3);
            values[2] = str.Substring(1, str.IndexOf("</b>") - 1);


            return values;

        }

        protected void btnDo_Click(object sender, EventArgs e)
        {
            ConvertMUFromXML();
            ConvertWRfromDB();
            Response.Output.WriteLine("Succefully done!");

        }
    }
}
