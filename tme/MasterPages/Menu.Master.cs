using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace localhost.MasterPages
{
    public partial class Menu : System.Web.UI.MasterPage
    {
        string _Width = "780px";

        public string Width
        {
            get
            {
                return _Width;
            }
            set
            {
                _Width = value;
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            divBorder.Attributes.Add("style", "width:" + Width + ";" + divBorder.Attributes["style"]);
            createMenu();

            if (Request.UserHostAddress.ToString() == ConfigurationSettings.AppSettings["OfficeIP"].ToString())
            {
                LivePerson.Visible = false;
            }
            LivePerson.Text = "<script language=\"JavaScript1.2\"> ";
            LivePerson.Text += "if (typeof(tagVars) == \"undefined\") tagVars = \"\"; ";
            string strUserName = string.Empty;
            if (Session["UserName"] != null) strUserName = Session["UserName"].ToString();
            LivePerson.Text += "tagVars += \"&VISITORVAR!User%20Name=" + strUserName + "\";";
            LivePerson.Text += "<" + "/SCRIPT>";
            LivePerson.Text += "<script language='javascript' src='https://server.iad.liveperson.net/hc/92199096/x.js?cmd=file&file=chatScript3&site=92199096&&category=en;woman;5'>";
            LivePerson.Text += "<" + "/SCRIPT>";
        }

        private void createMenu()
        {
            string path = "";
            string menu_type = "";
            
            if (Session["Id"] == null)
            {
                menu_type = "DefaultMenu";
            }
            else
            {
                pnLogin.Visible = false;
                pnLogout.Visible = true;
                
                lblUser.Text = Session["Name"].ToString();
                
                switch (Session["Typ"].ToString())
                {
                    case "A":
                        menu_type = "AdminMenu";
                        break;
                    case "B":
                        menu_type = "BrokerMenu";
                        break;
                    case "T":
                        menu_type = "BrokerMenu";
                        break;
                    case "P":
                    case "O":
                        menu_type = "PurchaserMenu";
                        break;
                    case "S":
                        menu_type = "SellerMenu";
                        break;
                    case "D":
                        menu_type = "DistributorMenu";
                        break;
                    case "Demo":
                        menu_type = "PurchaserMenu";
                        break;
                    case "L":
                        menu_type = "LeadMenu";
                        break;
                }

            }


            path = Server.MapPath("../menu/" + menu_type + ".xml");
            
            XmlDataSource objData = new XmlDataSource();
            objData.DataFile = path;
            objData.XPath = "TPEmenu/*";
            mnuMain.DataSource = objData;
            mnuMain.DataBind();
        }


     

    }
}
