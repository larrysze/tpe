using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace localhost.MasterPages
{
    public partial class Template : System.Web.UI.MasterPage
    {

        string _Width = "780px";
        
        public string Width
        {
            get
            {
                return _Width;
            }
            set
            {
                _Width = value;
            }
        }

        string _Heading = "";

        public string Heading
        {
            get
            {
                return _Heading;
            }
            set
            {
                _Heading = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            divBorder.Attributes.Add("style", "width:" + Width + ";" + divBorder.Attributes["style"]);
            createMenu();

            if (Request.UserHostAddress.ToString() == ConfigurationSettings.AppSettings["OfficeIP"].ToString())
            {
                LivePerson.Visible = false;
            }
            LivePerson.Text = "<script language=\"JavaScript1.2\"> ";
            LivePerson.Text += "if (typeof(tagVars) == \"undefined\") tagVars = \"\"; ";
            string strUserName = string.Empty;
            if (Session["UserName"] != null) strUserName = Session["UserName"].ToString();
            LivePerson.Text += "tagVars += \"&VISITORVAR!User%20Name=" + strUserName + "\";";
            LivePerson.Text += "<" + "/SCRIPT>";
            LivePerson.Text += "<script language='javascript' src='https://server.iad.liveperson.net/hc/92199096/x.js?cmd=file&file=chatScript3&site=92199096&&category=en;woman;5'>";
            LivePerson.Text += "<" + "/SCRIPT>";
        }

        private void createMenu()
        {
            string path = "";
            string menu_type = "";
            if (Session["Id"] == null)
            {
                menu_type = "DefaultMenu";
            }
            else
            {
                pnLogin.Visible = false;
                pnLogout.Visible = true;
                lblUser.Text = Session["Name"].ToString();
//                pnDefault.Visible = false;

                switch (Session["Typ"].ToString())
                {
                    case "A":
                        menu_type = "AdminMenu";
                        // add in right click menu
                        //pnContextMenu.Visible = true;
                        break;
                    case "B":
                        menu_type = "BrokerMenu";
                        break;
                    case "T":
                        menu_type = "BrokerMenu";
                        break;
                    case "P":
                    case "O":
                        menu_type = "PurchaserMenu";
                        break;
                    case "S":
                        menu_type = "SellerMenu";
                        //						pnPurchaserNav.Visible = true;
                        break;
                    case "D":
                        menu_type = "DistributorMenu";
                        break;
                    case "Demo":
                        menu_type = "PurchaserMenu";
                        break;
                    case "L":
                        menu_type = "LeadMenu";
                        //pnContextMenu.Visible = true;
                        break;
                }

            }


            path = Server.MapPath("../menu/" + menu_type + ".xml");

            DataSet ds = new DataSet();
//            ds.ReadXml(path);
//            Menu menu = new Menu();
//            mnuMain.MenuItemClick += new MenuEventHandler(menu_MenuItemClick);
            XmlDataSource objData = new XmlDataSource();
            objData.DataFile = path;
            objData.XPath = "TPEmenu/*";
            mnuMain.DataSource = objData;
            mnuMain.DataBind();
        }


        protected void Button_Login(object sender, EventArgs e)
        {
            // Saving information into session object to be passed on to the login function
            Session["UserName"] = UserName.Text;
            Session["Password"] = Crypto.Encrypt(Password.Text);
            Response.Redirect("Common/FUNCLogin.aspx");

        }

/*
        void ShowMenu()
        {
            if (Session["Id"] == null)
            {
                pnLogin.Visible = true;
                pnLogout.Visible = false;
                pnDefault.Visible = true;
            }
            else
            {
                pnLogin.Visible = false;
                pnLogout.Visible = true;
                lblUser.Text = Session["Name"].ToString();
                pnDefault.Visible = false;

                switch (Session["Typ"].ToString())
                {
                    case "A":
                        pnAdminMenu.Visible = true;
                        // add in right click menu
                        pnContextMenu.Visible = true;
                        break;
                    case "B":
                        pnBrokerNav.Visible = true;
                        break;
                    case "T":
                        pnBrokerNav.Visible = true;
                        break;
                    case "P":
                    case "O":
                        pnPurchaserNav.Visible = true;
                        break;
                    case "S":
                        pnSellerNav.Visible = true;
                        //						pnPurchaserNav.Visible = true;
                        break;
                    case "D":
                        pnDistributorNav.Visible = true;
                        break;
                    case "Demo":
                        pnPurchaserNav.Visible = true;
                        break;
                }

            }

        }
    */
    }
}
