using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
namespace localhost.Common
{
	/// <summary>
	/// </summary>
	public partial class MB_Specs_Invoice : System.Web.UI.Page
	{
        protected System.Data.SqlClient.SqlConnection conn;
		protected System.Data.SqlClient.SqlCommand com;
		protected System.Data.SqlClient.SqlDataReader rd;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
		protected System.Text.StringBuilder Content;
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["Typ"].ToString() != "A")
			{
				Response.Redirect("/default.aspx");
			}

			if(!Page.IsPostBack)
			{
				if(this.Request["Id"]!=null)
				{
					arrayId=this.Request["Id"].ToString();
				}
				else
				{
					Response.Redirect("/common/Filled_Orders.aspx");
				}
				IstheSpotOrPo();
				CreatDbConnect();
				ShowTitle(conn);
				this.Label2.Text=ShowContent(conn);
				this.Literal1.Text=showBotton("");
				this.showHtml.Text=this.MakeString(this.Label2.Text+this.Literal1.Text);//add the templet
                initialize();//set panel1 and panel2
                LoadEmailList();
			}
		}
		private void LoadEmailList()
		{
			string strEmail="";
			if(this.Po.Equals("true"))
			{
				strEmail="select PERS_MAIL from PERSON where PERS_ID=(Select SHIPMENT_SELR from SHIPMENT WHERE SHIPMENT_Id='"+this.shipId.ToString()+"')";
			}
			else
			{
				strEmail="select PERS_MAIL from PERSON where PERS_ID=(Select SHIPMENT_BUYR from SHIPMENT WHERE SHIPMENT_Id='"+this.shipId.ToString()+"')";
			}
			conn.Open();
			com=new System.Data.SqlClient.SqlCommand(strEmail,conn);
			string es=com.ExecuteScalar().ToString();
			this.DropDownList2.Items.Add(new ListItem(es,es));
		
			this.DropDownList2.Items.Add(new ListItem("Mike","mike@theplasticsexchange.com"));
			this.DropDownList2.Items.Add(new ListItem("Christian","christian@theplasticsexchange.com"));
			this.DropDownList2.Items.Add(new ListItem("Helio","helio@theplasticsexchange.com"));
			this.DropDownList2.Items.Add(new ListItem("Niraj","niraj@theplasticsexchange.com"));
			this.DropDownList2.Items[0].Selected=true;
			conn.Close();
		}
		private void initialize()
		{
			this.Panel1.Visible=true;
			this.Panel2.Visible=false;
		}
		private string arrayId
		{
			get
			{
				object o=ViewState["arrayId"];
				return((o==null)?String.Empty:o.ToString());
			}
			set
			{
				ViewState["arrayId"]=value;
			}
		}
		private string ExchangeTimeFormat(string str)
		{
			string mystr="";
			if (str.Length > 2)
			{
				mystr=Convert.ToDateTime(str).ToString("MM/dd/yyyy");
			}
			return mystr.Replace("-","/");
		}
		private string shipId
		{
			get
			{
				object o=ViewState["shipId"];
				return((o==null)?String.Empty:o.ToString());
			}
			set
			{
				ViewState["shipId"]=value;
			}
		}
		private string SalseCon
		{
			get
			{
				object o=ViewState["SalseCon"];
				return((o==null)?String.Empty:o.ToString());
			}
			set
			{
				ViewState["SalseCon"]=value;
			}
		}
		protected void CreatDbConnect()
		{
			conn = new 
System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString());
		}
		protected void IstheSpotOrPo()
		{
			string 
spot=((this.Request["Spot"])==null?String.Empty:this.Request["Spot"].ToString());
			string 
po=((this.Request["PO"])==null?String.Empty:this.Request["PO"].ToString());
			string 
sal=((this.Request["SalesCon"])==null?String.Empty:this.Request["SalesCon"].ToString());
			if(spot.Length>0)
			{
				Spot="true";
			}
			else
			{
				Spot="false";
			}
			if(po.Length>0)
			{
				Po="true";
			}
			else
			{
				Po="false";
			}
			if(sal.Length>0)
			{
				this.SalseCon="true";
			}
			else
			{
				this.SalseCon="false";
			}
		}
		private string Spot
		{
			get
			{
				object o=ViewState["Spot"];
				return((o==null)?String.Empty:o.ToString());
			}
			set
			{
				ViewState["Spot"]=value;
			}
		}
		private string Po
		{
			get
			{
				object o=ViewState["Po"];
				return((o==null)?String.Empty:o.ToString());
			}
			set
			{
				ViewState["Po"]=value;
			}
		}
		protected void ShowTitle(System.Data.SqlClient.SqlConnection myconn)//show the title(photo and time)
		{
			conn.Open();
			System.Text.RegularExpressions.Regex reg=new 
			System.Text.RegularExpressions.Regex("-");
			string[] arrayid=reg.Split(this.arrayId.ToString());
			com=new System.Data.SqlClient.SqlCommand("Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID="+arrayid[0].ToString()+ "and SHIPMENT_SKU ='"+arrayid[1].ToString()+"'",myconn);
		    string i=com.ExecuteScalar().ToString();
			this.shipId=i.ToString();
			string Str="Select SHIPMENT_PO_SENT,SHIPMENT_PO_EMAIL_TO,SHIPMENT_INVOICE_SENT,SHIPMENT_INVOICE_EMAIL_TO FROM SHIPMENT where SHIPMENT_ID ="+i.ToString();
			com=new System.Data.SqlClient.SqlCommand(Str,myconn);
			rd=com.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
			if(rd.Read())
			{
				if(this.Po.Equals("true"))
				{
                    if (((this.Session["TYP"].ToString() == "A") || (this.Session["TYP"].ToString() == "B") || (this.Session["TYP"].ToString() == "L")) && (rd["SHIPMENT_INVOICE_SENT"].ToString().Trim().Length != 0))
					{
						this.Label1.Text="<TABLE height=20 cellSpacing=0 cellPadding=0 width=760 align=center border=0><TR><TD align=center><font color=red size=3><strong>This Purchase Order was <U>last</U> sent to "+rd["SHIPMENT_PO_EMAIL_TO"]+" -- " + rd["SHIPMENT_INVOICE_SENT"] +"</strong></font></TD></TR></TABLE>";
					}
				}
				else
				{
					if(this.SalseCon.Equals("false"))
					{
                        if (((this.Session["TYP"].ToString() == "A") || (this.Session["TYP"].ToString() == "B") || (this.Session["TYP"].ToString() == "B")) && (rd["SHIPMENT_INVOICE_SENT"].ToString().Trim().Length != 0))
						{
							this.Label1.Text="<TABLE height=20 cellSpacing=0 cellPadding=0 width=760 align=center border=0><TR><TD align=center><font color=red size=3><strong>This invoice was <U>last</U> sent to "+rd["SHIPMENT_INVOICE_EMAIL_TO"] + " -- " +rd["SHIPMENT_INVOICE_SENT"] +"</strong></font></TD></TR></TABLE>";
						}
					}
				}
			}
			rd.Close();

		}
		protected string ShowContent(System.Data.SqlClient.SqlConnection myconn)//Build content within a string(for email)
		{
			string strLocation="";
			System.DateTime time;
			System.Text.RegularExpressions.Regex reg=new System.Text.RegularExpressions.Regex("-");
			string[] arrayid=reg.Split(this.arrayId.ToString());
			Content=new System.Text.StringBuilder("");
			Content.Append("<table border=0 cellspacing=0 cellpadding=0 align=center><tr><td><img src=http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/images/email/tpelogo.gif></td><td STYLE='FONT: 14pt ARIAL BLACK;COLOR=BLACK'>The<font color='red'>Plastics</font>Exchange<font color='red'>.</font>com</td></tr></table><br><br>");
			Content.Append("<table border=0 cellspacing=0 cellpadding=0 width='650' align='center'>");
			Content.Append("<tr><td colspan=3><center><SPAN STYLE='BACKGROUND-COLOR:#CCCCCC;FONT: BOLD 10pt ARIAL'>");
			if(this.Po.Equals("true"))
			{
				Content.Append("PURCHASE ORDER");
			}
			else
			{
				if(this.SalseCon.Equals("true"))
				{
					Content.Append("SALES CONFIRMATION");
				}
				else
				{
					Content.Append("INVOICE");
				}
			}
			Content.Append("</SPAN></center><br><br></td></tr>");
			Content.Append("<tr valign=top>");
            string strSQL ="";// 'Building SQL query into a string(easy to read) returns company, Headquarters address info Will be displayed in the left side of the screen, another query for right side
            strSQL +="   SELECT ";
            strSQL +="   COMP_NAME=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=PLAC_COMP),";
            strSQL +="   PLAC_ADDR_ONE,";
			strSQL +="   PLAC_ADDR_TWO,";
			strSQL +="   (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL),";
			strSQL +="   PLAC_ZIP,";
			strSQL +="   (SELECT ";
			strSQL +="	 (SELECT CTRY_NAME FROM COUNTRY WHERE CTRY_CODE=LOCL_CTRY) ";
			strSQL +="	 FROM LOCALITY";
			strSQL +="	 WHERE LOCL_ID=PLAC_LOCL)";
			strSQL +="   FROM PLACE ";
			strSQL +="   WHERE PLAC_TYPE='H' ";
			strSQL +="   AND PLAC_COMP=(SELECT ";
			strSQL +="	(SELECT PERS_COMP FROM PERSON ";
			if(this.Po.Equals("true"))//select seller's company
			{
				strSQL +=" WHERE PERS_ID=SHIPMENT_SELR) FROM ORDERS,SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ID="+this.shipId.ToString()+") ";
			}
			else//select buyer's company
			{
				strSQL +=" WHERE PERS_ID=SHIPMENT_BUYR) FROM ORDERS,SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ID="+this.shipId.ToString()+") ";
			}
			myconn.Open();
			com=new System.Data.SqlClient.SqlCommand(strSQL,myconn);
			rd=com.ExecuteReader(CommandBehavior.CloseConnection);
			if(rd.Read())
			{
				Content.Append("<td><SPAN STYLE='FONT: 8pt ARIAL'><b><i>");
				if(this.Po.Equals("true"))
				{
					Content.Append("Purchased From:");
					Content.Append("</i></b><br>"+rd["COMP_NAME"].ToString()+"<br>"+rd["PLAC_ADDR_ONE"].ToString());
				}
				else
				{
					Content.Append("Sold to:");
					Content.Append("</i></b><br>"+rd["COMP_NAME"].ToString()+"<br>"+rd["PLAC_ADDR_ONE"].ToString());
				}
				if(rd["PLAC_ADDR_TWO"]!=null)
				{
					if(rd["PLAC_ADDR_TWO"].ToString().Length!=0)
					{
						Content.Append("<br>"+rd["PLAC_ADDR_TWO"].ToString());
					}
				}
				Content.Append("<br>"+rd[3]+"  "+rd["PLAC_ZIP"]+"</SPAN></td>");
			}
			rd.Close();
			myconn.Open();
			//determines the ship term is FOB/Shipping or FOB/Delivered or Delivered Tpe
            string ShipMethod="";
			com=new System.Data.SqlClient.SqlCommand("SELECT SHIPMENT_FOB FROM SHIPMENT WHERE SHIPMENT_ID="+this.shipId.ToString(), myconn);
			rd=com.ExecuteReader(CommandBehavior.CloseConnection);
			if(rd.Read())
			{
				if(!(rd["SHIPMENT_FOB"]is DBNull))
				{
					ShipMethod=rd["SHIPMENT_FOB"].ToString();
				}
			}
			rd.Close();
			//Building SQL query into a string(easy to read) returns company, Headquarters address info will be displayed in the right side of the screen
			strSQL ="";
			strSQL +="SELECT ";
			strSQL +="   PLAC_ADDR_ONE, ";
			strSQL +="   PLAC_ADDR_TWO,";
			strSQL +="   PLAC_INST,";
			strSQL +="   PLAC_PHON,";
			strSQL +="	 PLAC_RAIL_NUM,";
			strSQL +="   (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) As Location,";
			strSQL +="   (SELECT PLAC_ZIP FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) As Zip";
			strSQL +=" FROM ";
			strSQL +=" PLACE ";
			//display address based on ship term for FOB/shipping

			if(ShipMethod=="FOB/Shipping")
			{
				strSQL += " WHERE PLAC_ID=(SELECT SHIPMENT_FROM FROM SHIPMENT WHERE SHIPMENT_ID="+this.shipId.ToString()+")";
			}
			else
			{
				if(ShipMethod=="FOB/Delivered")
				{
					strSQL += " WHERE PLAC_ID=(SELECT SHIPMENT_TO FROM SHIPMENT WHERE SHIPMENT_ID="+this.shipId.ToString()+")";
				}
				else
				{
					 strSQL += " WHERE PLAC_ID=(SELECT SHIPMENT_TO FROM SHIPMENT WHERE SHIPMENT_ID="+this.shipId.ToString()+")";
				}
			}
			myconn.Open();
			com=new System.Data.SqlClient.SqlCommand(strSQL,myconn);
			rd=com.ExecuteReader(CommandBehavior.CloseConnection);
			rd.Read();
			string dress2=((rd["PLAC_INST"]is DBNull)?String.Empty:rd["PLAC_INST"].ToString());
			string PlaceAddOne=((rd["PLAC_ADDR_ONE"]is DBNull)?String.Empty:rd["PLAC_ADDR_ONE"].ToString());
			string PlaceAddTwo=((rd["PLAC_ADDR_TWO"]is DBNull)?String.Empty:rd["PLAC_ADDR_TWO"].ToString());
			string Location=rd["Location"].ToString();
			string Phone=((rd["PLAC_PHON"]==null)?String.Empty:rd["PLAC_PHON"].ToString());
			string Zip=((rd["Zip"]==null)?String.Empty:rd["Zip"].ToString());
			string RailNum=((rd["PLAC_RAIL_NUM"]==null)?String.Empty:rd["PLAC_RAIL_NUM"].ToString());
			rd.Close();
			Content.Append("<td><SPAN STYLE='FONT: 8pt ARIAL'><b><i>");
			if(this.Po.Equals("true"))
			{
				if(ShipMethod=="FOB/Shipping")
				{
					Content.Append("Our Pickup:</i></b>");
				}
				else
				{
					if(ShipMethod=="FOB/Delivered")
					{
						Content.Append("Delivered To:</i></b>");
					}
					else
					{
						if(ShipMethod=="Delivered Tpe")
						{
							Content.Append("Our Pickup:</i></b>");
						}
						else
						{
							Content.Append("Our Pickup:</i></b>");
						}
					}
				}
			}
			else
			{
				if(ShipMethod=="FOB/Shipping")
				{
					Content.Append("Your Pickup:</i></b>");
				}
				else
				{
					if(ShipMethod=="FOB/Delivered")
					{
						Content.Append("Delivered To:</i></b>");
					}
					else
					{
						if(ShipMethod=="Delivered Tpe")
						{
							Content.Append("Delivered To:</i></b>");
						}
						else
						{
							Content.Append("Ship to:</i></b>");
						}
					}
				}
			}
			if(ShipMethod=="Delivered Tpe" )
			{
				strSQL ="";
				strSQL +="SELECT ";
				strSQL +="   PLAC_ADDR_ONE, ";
				strSQL +="   PLAC_ADDR_TWO,";
				strSQL +="   PLAC_INST,";
				strSQL +="   PLAC_PHON,";
				strSQL +="	 PLAC_RAIL_NUM,";
				strSQL +="   (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) As Location,";
				strSQL +="   (SELECT PLAC_ZIP FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) As Zip";
				strSQL +=" FROM ";
				strSQL +=" PLACE ";
				strSQL += " WHERE PLAC_ID=(SELECT SHIPMENT_FROM FROM SHIPMENT WHERE SHIPMENT_ID="+this.shipId.ToString()+")";
				myconn.Open();
				com=new System.Data.SqlClient.SqlCommand(strSQL,myconn);
				rd=com.ExecuteReader(CommandBehavior.CloseConnection);
				rd.Read();
				if(this.Po.Equals("true"))
				{
					if(!(rd["PLAC_INST"]is DBNull))
					{
						if(rd["PLAC_INST"].ToString().Trim().Length>0)
						{
							Content.Append("<br>"+rd["PLAC_INST"].ToString()+"");
						}
					}
					Content.Append("<br>"+rd["PLAC_ADDR_ONE"].ToString()+"<br>");
					if(rd["PLAC_ADDR_TWO"]!=null)
					{
						if(rd["PLAC_ADDR_TWO"].ToString().Trim().Length>0)
						{
							Content.Append(rd["PLAC_ADDR_TWO"].ToString()+"<br>");
						}
					}
					strLocation = rd["Location"].ToString();
					Content.Append(rd["Location"].ToString()+"  "+rd["Zip"].ToString()+"");
					if(rd["PLAC_PHON"]!=null)
					{
						if(rd["PLAC_PHON"].ToString().Trim().Length>0)
						{
							Content.Append("<br>"+rd["PLAC_PHON"].ToString()+"<br>");
						}
					}
					if(rd["PLAC_RAIL_NUM"]!=null)
					{
						if(rd["PLAC_RAIL_NUM"].ToString().Trim().Length>0)
						{
							Content.Append(rd["PLAC_RAIL_NUM"].ToString()+"<br>");
						}
					}
					Content.Append("</SPAN></td>");
					Content.Append("<td align=right>");
				}
				else
				{
					if(dress2.Length>0)
					{
						Content.Append("<br>"+dress2+"");
					}
					Content.Append("<br>"+PlaceAddOne+"<br>");
					if(PlaceAddTwo.Length>0)
					{
						Content.Append(PlaceAddTwo+"<br>");
					}
					strLocation=Location;
					Content.Append(Location+"  "+Zip+"<br>");
					if(Phone.Length>0)
					{
						Content.Append(Phone+"<br>");
					}
					if(rd["PLAC_RAIL_NUM"]!=null)
					{
						if(rd["PLAC_RAIL_NUM"].ToString().Trim().Length>0)
						{
							Content.Append(rd["PLAC_RAIL_NUM"].ToString()+"<br>");
						}
					}
					Content.Append("</SPAN></td>");
					Content.Append("<td align=right>");
				}
				rd.Close();
			}
			else
			{
				if(dress2.Length>0)
				{
					Content.Append("<br>"+dress2+"");
				}
				Content.Append("<br>"+PlaceAddOne+"<br>");
				if(PlaceAddTwo.Length>0)
				{
					Content.Append(PlaceAddTwo+"<br>");
				}
				strLocation=Location;
				Content.Append(Location+"  "+Zip+"<br>");
				if(Phone.Length>0)
				{
					Content.Append(Phone+"<br>");
				}
				if(RailNum.Length>0)
				{
					Content.Append(RailNum+"<br>");
				}

				
			
				Content.Append("</SPAN></td>");
				Content.Append("<td align=right>");
			}
			strSQL="";
			strSQL +=" SELECT ";
			strSQL +="    ORDR_DATE = CONVERT(varchar,ORDR_DATE,101), ";
			strSQL +="    ORDR_ID, ";
			strSQL +="    ORDR_BID, ";
			strSQL +="    SHIPMENT_BUYR_PRCE*(ISNULL(SHIPMENT_WEIGHT,SHIPMENT_QTY*(SHIPMENT_SIZE))), ";
			strSQL +="    ISNULL(SHIPMENT_BUYER_TERMS,'30'), ";
			strSQL +="    SHIPMENT_QTY, ";
			strSQL +="    SHIPMENT_SIZE, ";
			strSQL +="    SHIPMENT_WEIGHT, ";
			strSQL +="    SHIPMENT_PRCE, ";
			strSQL +="    SHIPMENT_DATE_TAKE, ";
			strSQL +="    (SELECT CONT_LABL FROM CONTRACT WHERE CONT_ID=ORDR_CONT), ";
			strSQL +="    (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=SHIPMENT_TO)), ";
			strSQL +="    BUYERDUEDATE = DATEADD(day,CAST(SHIPMENT_BUYER_TERMS AS Integer),SHIPMENT_DATE_TAKE), ";
			//strSQL +="    SELLERDUEDATE = DATEADD(day,SHIPMENT_SELLER_TERMS,CAST(SHIPMENT_DATE_TAKE AS INTEGER)), ";
			strSQL +="    VARDATE=CONVERT(varchar(14),SHIPMENT_DATE_TAKE,101), ";
			strSQL +="    (SELECT CASE WHEN SHIPMENT_WEIGHT IS NULL THEN '0' ELSE '1' END), ";
			strSQL +="    ISNULL(SHIPMENT_WEIGHT,SHIPMENT_QTY*(SHIPMENT_SIZE)) AS Lbs, ";
			strSQL +="    VARMONTH=(SELECT FWD_MNTH+' '+RIGHT(CONVERT(varchar,FWD_YEAR),2) FROM FWDMONTH WHERE FWD_ID=SHIPMENT_MNTH), ";
			strSQL +="    SHIPMENT_INVOICE_SENT, ";
			strSQL +="    SHIPMENT_INVOICE_EMAIL_TO, ";
			strSQL +="    SHIPMENT_SKU, ";
			strSQL +="    SHIPMENT_PRCE, ";
			strSQL +="    SHIPMENT_PO_SENT, ";
			strSQL +="    SHIPMENT_PO_EMAIL_TO, ";
			strSQL +="    SHIP=(SELECT TOP 1 SHIPMENT_DATE_TAKE FROM SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID), ";
			strSQL +="    ORDR_PROD_SPOT, ";
			strSQL +="    ORDR_ISSPOT, ";
			strSQL +="    SHIPMENT_SKU, ";
			strSQL +="    SHIPMENT_SELLER_TERMS, " ;
			strSQL +="    SHIPMENT_BUYER_TERMS, " ;
			strSQL +="    SHIPMENT_FOB, ";
			strSQL +="    ORDR_MELT, ";
			strSQL +="    ORDR_DENS, ";
			strSQL +="    SHIPMENT_PO_NUM, ";
			strSQL +="    ORDR_ADDS, ";
			strSQL +="    SHIPMENT_BUYR_PRCE ";
			strSQL +=" FROM ORDERS, SHIPMENT ";
			strSQL +=" WHERE SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ID="+this.shipId.ToString();
			myconn.Open();
			com=new System.Data.SqlClient.SqlCommand(strSQL,myconn);
			rd=com.ExecuteReader(CommandBehavior.CloseConnection);
			rd.Read();
			Content.Append("<table border=1 cellspacing=0 >");
			//Display P.O number if it's available, only for Invoices and Sales Confirmation
			if(this.Po.Equals("false"))
			{
				if(rd["SHIPMENT_PO_NUM"]!=null&&rd["SHIPMENT_PO_NUM"].ToString().Trim().Length>0)
				{
					Content.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC width=100>P.O. #</td><td STYLE='FONT: 8pt ARIAL' align=left>&nbsp;"+rd["SHIPMENT_PO_NUM"].ToString()+"");
					Content.Append("</td></tr>");
				}
			}
			//Display transaction number
			Content.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC width=100>Transaction #</td><td STYLE='FONT: 8pt ARIAL' align=left>&nbsp;"+rd["ORDR_ID"].ToString()+"");
			if(rd["SHIPMENT_SKU"]==null)
			{
				Content.Append("</td></tr>");
			}
			else
			{
				Content.Append("-"+rd["SHIPMENT_SKU"].ToString()+"</td></tr>");
			}
			Content.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Transaction Date</td><td STYLE='FONT: 8pt ARIAL' align=left>&nbsp;"+ String.Format("{0:MM/dd/yyyy}",rd["ORDR_DATE"].ToString()) +"</td></tr>");
			bool checkIsShipping=false;
			if(this.SalseCon.Equals("false"))
			{
				if(rd["SHIP"].ToString().Length==0)
				{
					Content.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>");
					if(ShipMethod=="Delivered Tpe")
					{
						if(this.Po.Equals("true"))
						{
							Content.Append("Pickup Date");
						}
						else
						{
							Content.Append("Shipping Date");
							checkIsShipping=true;
						}
					}
					else
					{
						if(ShipMethod=="FOB/Delivered")
						{
							Content.Append("Shipping Date");
							checkIsShipping=true;
						}
						else
						{
							if(ShipMethod=="FOB/Shipping")
							{
								if(this.Po.Equals("true"))
								{
									Content.Append("Pickup Date");
								}
								else
								{
									Content.Append("Release Date");
								}
							}
							else
							{
								Content.Append("Shipping Date");
								checkIsShipping=true;
							}
						}
					}
					
					if(this.Po.Equals("true"))
					{
						Content.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left><font color=red>&nbsp;ASAP</td></tr>");
					}
					else
					{
						if(rd["SHIPMENT_DATE_TAKE"] is DBNull)
						{
							time=Convert.ToDateTime(rd["ORDR_DATE"].ToString());
							time.AddDays(10);
							Content.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left>&nbsp;"+ExchangeTimeFormat(time.ToString())+"</font></td></tr>");
							
						}
						else
						{
							Content.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left>&nbsp;"+rd["SHIPMENT_DATE_TAKE"].ToString()+"</font></td></tr>");

						}
					}
					
					
				

				}
				else
				{
					Content.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>");
					if(ShipMethod=="Delivered Tpe")
					{
						if(this.Po.Equals("true"))
						{
							Content.Append("Pickup Date");
						}
						else
						{
							Content.Append("Shipping Date");
						}
					}
					else
					{
						if(ShipMethod=="FOB/Delivered")
						{
							Content.Append("Shipping Date");
						}
						else
						{
							if(ShipMethod=="FOB/Shipping")
							{
								if(this.Po.Equals("true"))
								{
									Content.Append("Pickup Date");
								}
								else
								{
									Content.Append("Release Date");
								}
							}
							else
							{
								Content.Append("Shipping Date");
							}
						}
					}
					if (this.Po.Equals("true"))
					{
						Content.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left><font color=red>&nbsp;ASAP</td></tr>");
					}
					else
					{
						if(rd["SHIPMENT_DATE_TAKE"] is DBNull)
						{
							Content.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left><font color=red>&nbsp;"+ExchangeTimeFormat(rd["VARDATE"].ToString())+"</td></tr>");
						}
						else
						{
							Content.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left><font color=red>&nbsp;"+ExchangeTimeFormat(rd["SHIPMENT_DATE_TAKE"].ToString())+"</td></tr>");
						}
					}
				}
			}
			Content.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Transaction Value</td><td STYLE='");
			if(this.Po.Equals("true"))
			{
				double 
				money=Convert.ToDouble(rd["lbs"].ToString())*Convert.ToDouble(rd["SHIPMENT_PRCE"].ToString());
				Content.Append("FONT: 8pt ARIAL' align=left>&nbsp"+money.ToString("c")+"</td></tr>");
			}
			else
			{
				double m=Convert.ToDouble(rd[3].ToString());
				Content.Append("FONT: 8pt ARIAL' align=left>&nbsp"+m.ToString("c")+"</td></tr>");
			}
			if(this.SalseCon.Equals("false"))
			{
				if(this.Po.Equals("false"))
				{
					if(rd["SHIPMENT_DATE_TAKE"] is DBNull)
					{
						//if(rd["DAYS"].ToString().Trim().Length==0)
						//{
						time=Convert.ToDateTime(rd["ORDR_DATE"].ToString());
						time.AddDays(30);
						Content.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Payment Due</td><td STYLE='FONT: 8pt ARIAL' align=left><b>&nbsp;"+ExchangeTimeFormat(time.ToString())+"</b></td></tr>");
					}
					else
					{
						//time=Convert.ToDateTime(rd["SHIPMENT_DATE_TAKE"].ToString());
						//time.AddDays(Convert.ToInt32(rd["SHIPMENT_BUYER_TERMS"].ToString()));
						Content.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Payment Due</td><td STYLE='FONT: 8pt ARIAL' align=left><b>&nbsp;"+ExchangeTimeFormat(rd["BUYERDUEDATE"].ToString())+"</b></td></tr>");
					}
				}
				/*
				else
				{
					if(rd["VARDATE"].ToString().Trim().Length==0)
					{
						time=Convert.ToDateTime(rd["VARDATE"].ToString());
						time.AddDays(Convert.ToInt32(rd["DAYS"].ToString()));
						Content.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Payment Due</td><td STYLE='FONT: 8pt ARIAL' align=left><b>&nbsp;"+ExchangeTimeFormat(time.ToString())+"</b></td></tr>");
					}
					else
					{
						time=Convert.ToDateTime(rd["VARDATE"].ToString());
						time.AddDays(Convert.ToInt32(rd["DAYS"].ToString()));
						Content.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Payment Due</td><td STYLE='FONT: 8pt ARIAL' align=left><b>&nbsp;"+ExchangeTimeFormat(time.ToString())+"</b></td></tr>");
					}
				}
				}
				*/
				
			}
			Content.Append("</table>");
			Content.Append("</tr>");
			Content.Append("<tr valign=top><td align=center colspan=3>");
			Content.Append("<br>");
			Content.Append("<table border=1 cellspacing=0 width=100% >");
			if(rd["SHIPMENT_SIZE"].ToString()=="1")
			{
				Content.Append("<tr bgcolor=#CCCCCC>");
				Content.Append("<td STYLE='FONT: 8pt ARIAL'>Size (lbs)</td>");
			}
			else
			{
				Content.Append("<tr bgcolor=#CCCCCC>");
				Content.Append("<td STYLE='FONT: 8pt ARIAL'>Amount</td>");
				Content.Append("<td STYLE='FONT: 8pt ARIAL'>Size</td>");
				if(rd["SHIPMENT_WEIGHT"] is DBNull)
				{
					Content.Append("<td STYLE='FONT: 8pt ARIAL'>Lbs<font color=red>&nbsp(Est)</td>");
				}
				else
				{
					Content.Append("<td STYLE='FONT: 8pt ARIAL'>Lbs</td>");
				}
			}
			Content.Append("<td STYLE='FONT: 8pt ARIAL'>$/Lb</td>");
			Content.Append("<td STYLE='FONT: 8pt ARIAL'>Product</td>");
			if(this.Po.Equals("true"))
			{
				if(ShipMethod=="FOB/Shipping")
				{
					Content.Append("<td STYLE='FONT: 8pt ARIAL'>Our Pickup</td>");
				}
				else
				{
					if( ShipMethod=="FOB/Delivered")
					{
						Content.Append("<td STYLE='FONT: 8pt ARIAL'>Delivered To</td>");
					}
					else
					{
						if(ShipMethod=="Delivered Tpe")
						{
							Content.Append("<td STYLE='FONT: 8pt ARIAL'>Our Pickup</td>");
						}
						else
						{
							Content.Append("<td STYLE='FONT: 8pt ARIAL'>Our Pickup</td>");
						}
					}
				}
			}
			else
			{
				if(ShipMethod=="FOB/Shipping")
				{
					Content.Append("<td STYLE='FONT: 8pt ARIAL'>Your Pickup</td>");
				}
				else
				{
					if( ShipMethod=="FOB/Delivered")
					{
						Content.Append("<td STYLE='FONT: 8pt ARIAL'>Delivered To</td>");
					}
					else
					{
						if(ShipMethod=="Delivered Tpe")
						{
							Content.Append("<td STYLE='FONT: 8pt ARIAL'>Delivered To</td>");
						}
						else
						{
							Content.Append("<td STYLE='FONT: 8pt ARIAL'>Ship to</td>");
						}
					}
				}
			}
			Content.Append("<td STYLE='FONT: 8pt ARIAL'>Terms</td>");
			Content.Append("<td STYLE='FONT: 8pt ARIAL'>Total Amount</td>");
			Content.Append("</tr>");
			Content.Append("<tr>");
			if(rd["SHIPMENT_SIZE"].ToString()=="1")
			{
				if(rd["SHIPMENT_WEIGHT"].ToString().Trim().Length==0)
				{
					Content.Append("<td STYLE='FONT: 8pt ARIAL'>"+String.Format("{0:#,###}",rd["SHIPMENT_QTY"].ToString()).ToString()+"<font color=red>&nbsp(Est)</font></td>");
				}
				else
				{
					Content.Append("<td STYLE='FONT: 8pt ARIAL'>"+String.Format("{0:#,###}",rd["SHIPMENT_WEIGHT"].ToString()).ToString()+"</td>");
				}
			}
			else
			{
				Content.Append("<td STYLE='FONT: 8pt ARIAL'>"+String.Format("{0:#,###}", 
					rd["SHIPMENT_QTY"].ToString()).ToString()+"</td>");
				if(rd["SHIPMENT_SIZE"]is DBNull )
				{
					Content.Append("<td STYLE='FONT: 8pt ARIAL'>");
				}
				else
				{
					if(rd["SHIPMENT_SIZE"].ToString()=="42000")
					{
						Content.Append("<td STYLE='FONT: 8pt ARIAL'>Truckload");
					}
					if(rd["SHIPMENT_SIZE"].ToString()=="1")
					{
						Content.Append("<td STYLE='FONT: 8pt ARIAL'>Lb");
					}
					if(rd["SHIPMENT_SIZE"].ToString()=="2.205")
					{
						Content.Append("<td STYLE='FONT: 8pt ARIAL'>Kilo");
					}
					if(rd["SHIPMENT_SIZE"].ToString()=="190000")
					{
						Content.Append("<td STYLE='FONT: 8pt ARIAL'>Railcar");
					}
					if(rd["SHIPMENT_SIZE"].ToString()=="45000")
					{
						Content.Append("<td STYLE='FONT: 8pt ARIAL'>Bulk Truck");
					}
					if(rd["SHIPMENT_SIZE"].ToString()=="44092")
					{
						Content.Append("<td STYLE='FONT: 8pt ARIAL'>Truckload Bag");
					}
					if(rd["SHIPMENT_SIZE"].ToString()=="2205")
					{
						Content.Append("<td STYLE='FONT: 8pt ARIAL'>Metric Ton");
					}
					if(rd["SHIPMENT_SIZE"].ToString()=="38500")
					{
						Content.Append("<td STYLE='FONT: 8pt ARIAL'>20 Container");
					}
					if(rd["SHIPMENT_SIZE"].ToString()=="44500")
					{
						Content.Append("<td STYLE='FONT: 8pt ARIAL'>30 Container");
					}
				}
				if(rd["SHIPMENT_QTY"].ToString()!="1")
				{
					Content.Append("s");
					Content.Append("</td>");
				}
					if(rd["SHIPMENT_WEIGHT"] is DBNull)
					{
						Content.Append("<td STYLE='FONT: 8pt ARIAL'><font color=red>"+String.Format("{0:#,###}",Convert.ToInt32(rd["lbs"].ToString())).ToString()+"</td>");
					}
					else
					{
						Content.Append("<td STYLE='FONT: 8pt ARIAL'>"+String.Format("{0:#,###}",Convert.ToInt32(rd["lbs"].ToString())).ToString()+"</td>");
					}

			}
			if(this.Po.Equals("true"))
			{
				Content.Append("<td STYLE='FONT: 8pt ARIAL'>"+String.Format("${0:#,###0.0000}",Convert.ToDouble(rd["SHIPMENT_PRCE"].ToString()))+"</td>");
			}
			else
			{
				Content.Append("<td STYLE='FONT: 8pt ARIAL'>"+String.Format("{0:#,###0.0000}",Convert.ToDouble(rd["SHIPMENT_BUYR_PRCE"].ToString()))+"</td>");
			}
			if(Convert.ToBoolean(rd["ORDR_ISSPOT"]))
			{
				Content.Append("<td STYLE='FONT: 8pt ARIAL'>"+rd["ORDR_PROD_SPOT"].ToString());
				if(!(rd["ORDR_MELT"] is DBNull))
				{
					
					if(rd["ORDR_MELT"].ToString().Trim().Length!=0)
					{
						Content.Append(" - "+rd["ORDR_MELT"].ToString()+" melt");
					}
				}
				if(!(rd["ORDR_DENS"]is DBNull))
				{
					if(rd["ORDR_DENS"].ToString().Trim().Length!=0)
					{
						Content.Append(" - "+rd["ORDR_DENS"].ToString()+" density");
					}
				}
				if(!(rd["ORDR_ADDS"]is DBNull))
				{
					if((rd["ORDR_ADDS"].ToString().Trim().Length>0))
					{
						Content.Append(" - "+rd["ORDR_ADDS"].ToString());
					}
				}
			}
			else
			{
				Content.Append("<td STYLE='FONT: 8pt ARIAL'>"+rd[9].ToString());
			}
			Content.Append("</td>");
			Content.Append("<td STYLE='FONT: 8pt ARIAL'>"+ strLocation + "</td>");
			if(this.Po.Equals("true"))
			{
				if(rd["SHIPMENT_SELLER_TERMS"].ToString()=="0")
				{
					Content.Append("<td STYLE='FONT: 8pt ARIAL'>Cash</td>");
				}
				else
				{
					Content.Append("<td STYLE='FONT: 8pt ARIAL'>"+rd["SHIPMENT_SELLER_TERMS"].ToString()+" days</td>");
				}
			}
			else
			{
				if(rd["SHIPMENT_BUYER_TERMS"].ToString()=="0")
				{
					Content.Append("<td STYLE='FONT: 8pt ARIAL'>Cash</td>");
				}
				else
				{
					Content.Append("<td STYLE='FONT: 8pt ARIAL'>"+rd["SHIPMENT_BUYER_TERMS"].ToString()+" days</td>");
				}
			}
			Content.Append("<td STYLE='");
			if(rd[13].ToString()=="0")
			{
				Content.Append("");
			}
			if(this.Po.Equals("true"))
			{
                double 
money=Convert.ToDouble(rd["lbs"].ToString())*Convert.ToDouble(rd["SHIPMENT_PRCE"].ToString());
				Content.Append("FONT: 8pt ARIAL'>"+money.ToString("C")+"</td>");
			}
			else
			{
				Content.Append("FONT: 8pt ARIAL'>"+String.Format("{0:C}",Convert.ToDouble(rd[3].ToString()))+"</td>");
			}
			Content.Append("</tr>");
			Content.Append("</table><br>");
			Content.Append("</td></tr>");
			Content.Append("<br><tr><td colspan=4 Style'FONT: 8pt ARIAL'>");
			string shipment_size=rd["SHIPMENT_SIZE"].ToString();
			string shipment_fob=rd["SHIPMENT_FOB"].ToString();
			rd.Close();
			if(this.Po.Equals("true"))
			{
				if(shipment_fob!="FOB/Delivered")
				{
					Content.Append("<span STYLE='FONT: 8pt ARIAL'>Please provide release # ASAP.</span><BR><BR>");
				}
			}
			else
			{
				if(this.SalseCon.Equals("false"))
				{
					if(shipment_size=="190000")
					{
						myconn.Open();
						com=new System.Data.SqlClient.SqlCommand("SELECT SHIPMENT_COMMENT FROM SHIPMENT WHERE SHIPMENT_ID="+this.shipId.ToString()+"",myconn);
						rd=com.ExecuteReader(CommandBehavior.CloseConnection);
						bool f=rd.Read();
						if(f)
						{
							if(!(rd["SHIPMENT_COMMENT"]is DBNull))
							{
								Content.Append("<span STYLE='FONT: 8pt ARIAL'>Railcar # ");
								f=true;
							}
							else
							{
								f=false;
							}
						}
						rd.Close();
						if(f)
						{
							myconn.Open();
							com=new System.Data.SqlClient.SqlCommand("SELECT SHIPMENT_COMMENT FROM SHIPMENT WHERE SHIPMENT_ID="+this.shipId.ToString()+"",myconn);
							rd=com.ExecuteReader(CommandBehavior.CloseConnection);
							while(rd.Read())
							{
								Content.Append(""+rd["SHIPMENT_COMMENT"].ToString()+"</span><br>");
								//Content.Append("&nbsp");
							}
							rd.Close();
						}
						else
						{
							Content.Append("</span><Br>");
						}
					}
				}
			}
			return Content.ToString();
		}
		protected string showBotton(string command)//Build content within astring(for email)
		{
			Content=new System.Text.StringBuilder("");
			if(this.Po.Equals("false"))
			{
				Content.Append("<span STYLE='FONT: 8pt ARIAL'>Thank you for your order.");
				if(command.Length!=0)
				{
					Content.Append("<BR><b>"+command+"</span><br></td></tr>");
				}
				else
				{
					Content.Append("</span><br>");
				}
				if(this.SalseCon.Equals("false"))
				{
					Content.Append("<tr><td colspan=2 valign=top align=left><span STYLE='FONT: 8pt ARIAL'>Please reference transaction # in your payments.</span><br><br><br></span></td></tr>");
				}
			}
			Content.Append("<tr valign=top>");
			if(this.Po.Equals("false")&&this.SalseCon.Equals("false"))
			{
				Content.Append("<td colspan=1><SPAN STYLE='FONT: 8pt ARIAL'><b><i>CHECK REMITTANCE INSTRUCTIONS:</i></b><br>The Plastics Exchange<br>Department 20-3041<BR>PO Box 5977<BR>Carol Stream, IL 60197-5977</SPAN></td>");
				Content.Append("<td colspan=1><SPAN STYLE='FONT: 8pt ARIAL'><b><i>WIRE TRANSFER INSTRUCTIONS:</i></b><br>American Chartered Bank<br>Elk Grove Village, IL 60007<BR>ABA # 071 925 046<BR><BR>For Further Credit:<BR>The Plastics Exchange<BR>Account #: 1130245</SPAN><BR><BR><BR></td>");
			}
			Content.Append("</tr>");
			//Content.Append("<tr><td colspan=3 valign=top align=center></td></tr>");
			//Content.Append("<tr><td colspan=3 valign=top align=center><font face=arial size=1 color=Black>730 North LaSalle, 3rd Floor</font>&nbsp;&nbsp;<font face=arial size=1 color=red>Chicago, IL 60610</font>&nbsp;&nbsp;<font face=arial size=1 color=Black><b>tel 312.202.0002</b></font>&nbsp;&nbsp;<font face=arial size=1 color=red>fax 312.202.0174</font></td></tr>");
			Content.Append("</table>");
			return Content.ToString();
		}
		#region Web
		override protected void OnInit(EventArgs e)
		{
			//
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		protected void showInterface_Click(object sender, System.EventArgs e)//show the interface for sent mail
		{
			this.Panel1.Visible=false;
			this.Panel2.Visible=true;
		}
		protected void back_Click(object sender, System.EventArgs e)//back
		{
			Response.Redirect("/common/Filled_Orders.aspx");
		}
		private string MakeString(string str)//add the template.the parameter 'str' is the content
		{
			System.Text.StringBuilder sbHTML=new System.Text.StringBuilder("");

			sbHTML.Append(str);
			sbHTML.Append("<table width='100%'><TR> ");
			sbHTML.Append("<TD vAlign=top align=middle colSpan=3> ");
			sbHTML.Append("<HR> ");
			sbHTML.Append("</TD></TR> ");
			sbHTML.Append("<TR> ");
			sbHTML.Append("<TD vAlign=top align=middle colSpan=3><FONT face=arial color=black size=1>");
			sbHTML.Append("710 North Dearborn</FONT>  <FONT face=arial color=red size=1>Chicago, IL  ");
			sbHTML.Append("60610</FONT>  <FONT face=arial color=black size=1><B>tel  ");
			sbHTML.Append(ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + "</B></FONT>  <FONT face=arial color=red size=1>fax ");
			sbHTML.Append(ConfigurationSettings.AppSettings["FaxNumber"].ToString() + "</FONT></td></tr></table> ");

			return sbHTML.ToString();

		}
		protected void sentMail_Click(object sender, System.EventArgs e)//sent mail
		{

			if(Page.IsValid)
			{
				CreatDbConnect();
				MailMessage mail;
				string mailto="";
				if(this.Po.Equals("true"))
				{
					mail=new MailMessage();
					mail.From=this.DropDownList1.SelectedItem.Value.ToString();
					//mail.To = "zach@theplasticsexchange.com";
					mail.To=this.DropDownList2.SelectedValue.ToString();
					mail.Subject= "Purchase Order";
					mail.Body=MakeString(this.Label2.Text+this.showBotton(this.TextBox1.Text));
					mail.BodyFormat= MailFormat.Html;
					//SmtpMail.SmtpServer="localhost";
					TPE.Utility.EmailLibrary.Send(Context,mail);
                    if (Session["TYP"].ToString() == "A" || Session["TYP"].ToString() == "B" || Session["TYP"].ToString() == "L")
					{
						conn.Open();
						if(this.DropDownList2.SelectedValue.Length>35)
						{
							mailto=this.DropDownList2.SelectedValue.ToString().Substring(0,35);
						}
						else
						{
							mailto=this.DropDownList2.SelectedValue.ToString();
						}
						com=new System.Data.SqlClient.SqlCommand("UPDATE SHIPMENT SET SHIPMENT_PO_SENT=GETDATE(), SHIPMENT_PO_EMAIL_TO='"+mailto+"' WHERE SHIPMENT_ID="+this.shipId,conn);
						com.ExecuteNonQuery();
						conn.Close();
					}
				}
				else
				{
					mail=new MailMessage();
					mail.From=this.DropDownList1.SelectedItem.Value.ToString();
					//mail.To = "zach@theplasticsexchange.com";
					mail.To=this.DropDownList2.SelectedValue.ToString();
					if(this.SalseCon.Equals("true"))
					{
						mail.Subject="Sales Confirmation";
					}
					else
					{
						mail.Subject="Invoice";
					}
					mail.Body=MakeString(this.Label2.Text+this.showBotton(this.TextBox1.Text));
					mail.BodyFormat= MailFormat.Html;
					//SmtpMail.SmtpServer="localhost";
					TPE.Utility.EmailLibrary.Send(Context,mail);
                    if (Session["TYP"].ToString() == "A" || Session["TYP"].ToString() == "B" || Session["TYP"].ToString() == "L")
					{
						conn.Open();
						if(this.DropDownList2.SelectedValue.Length>35)
						{
							mailto=this.DropDownList2.SelectedValue.ToString().Substring(0,35);
						}
						else
						{
							mailto=this.DropDownList2.SelectedValue.ToString();
						}
						com=new System.Data.SqlClient.SqlCommand("UPDATE SHIPMENT SET SHIPMENT_INVOICE_SENT=GETDATE(), SHIPMENT_INVOICE_EMAIL_TO='"+mailto+"' WHERE SHIPMENT_ID="+this.shipId,conn);
						com.ExecuteNonQuery();
						conn.Close();
					}
				}
				Response.Redirect("MB_Specs_Invoice.aspx");
			}
		}
	}
}

