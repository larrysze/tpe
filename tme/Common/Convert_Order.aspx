<%@ Page language="c#" Codebehind="Convert_Order.aspx.cs" AutoEventWireup="True" Inherits="localhost.Common.MB_Specs_Order" MasterPageFile="~/MasterPages/Menu.Master" Title="The Plastics Exchange: Commodity Plastic Resin, Buy Resin, Sell Resin, Plastic Materials"%>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
<style type="text/css">
.Left {text-align:right;}

</style>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">

<table id="tMain" cellspacing="0" cellpadding="0" width="780px" align="center" border="0">
	
			<tr>
				<td>
					<fieldset>
						<table border="0" cellpadding="0" cellspacing="2" width="100%" align="center">
							<tr>
								<td valign="bottom"><span class="Content Bold Color2">Shipment</span></td>
								<td valign="bottom"><span class="Content Bold Color2">Weight</span></td>
								<td valign="bottom"><span class="Content Bold Color2">Ship Date</span></td>
								<td valign="bottom"><span class="Content Bold Color2">Delivered</span></td>
								<td valign="bottom"><span class="Content Bold Color2">Freight</span></td>
							</tr>
							<tr>
								<td align="left" valign="bottom"><asp:Label CssClass="Content Color2 Left" id="lblRCName" runat="server"></asp:Label></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="RequiredFieldValidator1" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtRCWeight" />
									<asp:TextBox CssClass="InputForm" size="7" id="txtRCWeight" runat="server"></asp:TextBox>
								</td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator6" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtRCShipDate" />
									<asp:TextBox CssClass="InputForm" size="10" id="txtRCShipDate" runat="server"></asp:TextBox></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator7" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtRCDelDate" />
									<asp:TextBox CssClass="InputForm" size="10" id="txtRCDelDate" runat="server"></asp:TextBox></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator8" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtRCFreight" />
									<asp:TextBox CssClass="InputForm" size="7" id="txtRCFreight" runat="server">0</asp:TextBox></td>
							</tr>
							<tr>
								<td align="left" valign="bottom"><asp:Label CssClass="Content Color2 Left" id="lblBT1Name" runat="server"></asp:Label></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator2" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT1Weight" />
									<asp:TextBox CssClass="InputForm" size="7" id="txtBT1Weight" runat="server"></asp:TextBox>
								</td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator9" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT1ShipDate" />
									<asp:TextBox CssClass="InputForm" size="10" id="txtBT1ShipDate" runat="server"></asp:TextBox></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator10" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT1DelDate" />
									<asp:TextBox CssClass="InputForm" size="10" id="txtBT1DelDate" runat="server"></asp:TextBox></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator11" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT1Freight" />
									<asp:TextBox CssClass="InputForm" size="7" id="txtBT1Freight" runat="server"></asp:TextBox></td>
							</tr>
							<tr>
								<td align="left" valign="bottom"><asp:Label CssClass="Content Color2 Left" id="lblBT2Name" runat="server"></asp:Label></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator3" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT2Weight" />
									<asp:TextBox CssClass="InputForm" size="7" id="txtBT2Weight" runat="server"></asp:TextBox></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator12" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT2ShipDate" />
									<asp:TextBox CssClass="InputForm" size="10" id="txtBT2ShipDate" runat="server"></asp:TextBox></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator13" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT2DelDate" />
									<asp:TextBox CssClass="InputForm" size="10" id="txtBT2DelDate" runat="server"></asp:TextBox></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator14" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT2Freight" />
									<asp:TextBox CssClass="InputForm" size="7" id="txtBT2Freight" runat="server"></asp:TextBox></td>
							</tr>
							<tr>
								<td align="left" valign="bottom"><asp:Label CssClass="Content Color2 Left" id="lblBT3Name" runat="server"></asp:Label></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator4" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT3Weight" />
									<asp:TextBox CssClass="InputForm" size="7" id="txtBT3Weight" runat="server"></asp:TextBox></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator15" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT3ShipDate" />
									<asp:TextBox CssClass="InputForm" size="10" id="txtBT3ShipDate" runat="server"></asp:TextBox></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator16" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT3DelDate" />
									<asp:TextBox CssClass="InputForm" size="10" id="txtBT3DelDate" runat="server"></asp:TextBox></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator17" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT3Freight" />
									<asp:TextBox CssClass="InputForm" size="7" id="txtBT3Freight" runat="server"></asp:TextBox></td>
							</tr>
							<tr>
								<td align="left" valign="bottom"><asp:Label CssClass="Content Color2 Left" id="lblBT4Name" runat="server"></asp:Label></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator5" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT4Weight" />
									<asp:TextBox CssClass="InputForm" size="7" id="txtBT4Weight" runat="server"></asp:TextBox></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator18" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT4ShipDate" />
									<asp:TextBox CssClass="InputForm" size="10" id="txtBT4ShipDate" runat="server"></asp:TextBox></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator19" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT4DelDate" />
									<asp:TextBox CssClass="InputForm" size="10" id="txtBT4DelDate" runat="server"></asp:TextBox></td>
								<td valign="bottom">
									<asp:RequiredFieldValidator CssClass="Content Color3" id="Requiredfieldvalidator20" runat="server" ErrorMessage="Required" Display="Dynamic"
										ControlToValidate="txtBT4Freight" />
									<asp:TextBox CssClass="InputForm" size="7" id="txtBT4Freight" runat="server"></asp:TextBox></td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td>
					<br />
					<span class="Content Bold Color2">Storage Warehouse:</span>
					<asp:DropDownList CssClass="InputForm" id="ddlWarehouse" runat="server"></asp:DropDownList>
					<br />
					<br />
				</td>
			</tr>
			<tr>
				<td>
					<asp:datalist id="dl" runat="server" Width="100%" CssClass="DataGrid" CellPadding="2" ShowFooter="True"
						DataKeyField="PAY_ID">
						<HeaderTemplate>
							<table id="t">
								<tr>
									<td width="50"><span class="Content Color2">#</span></td>
									<td width="100"><span class="Content Color2">Date</span></td>
									<td width="100"><span class="Content Color2">Amount</span></td>
									<td><span class="Content Color2">Allocate to</span></td>
								</tr>
							</table>
						</HeaderTemplate>
						<AlternatingItemStyle CssClass="DataGridRow_Alternate"></AlternatingItemStyle>
						<ItemStyle CssClass="DataGridRow"></ItemStyle>
						<ItemTemplate>
							<table border="0">
								<tr>
									<td width="50">
										<asp:Label runat="server" id="lblPay_ID">
											<%# DataBinder.Eval(Container.DataItem,"PAY_ID")%>
										</asp:Label></td>
									<td width="100"><%# DataBinder.Eval(Container.DataItem,"PAY_DATE")%></td>
									<td width="100"><%# DataBinder.Eval(Container.DataItem,"PAY_AMNT") %></td>
									<td>
										<asp:DropDownList AutoPostBack="false" id="Dropdownlist1" runat="server">
											<asp:ListItem Text="BT1" />
											<asp:ListItem Text="BT2" />
											<asp:ListItem Text="BT3" />
											<asp:ListItem Text="BT4" />
										</asp:DropDownList>
									</td>
								</tr>
							</table>
						</ItemTemplate>
						<HeaderStyle Wrap="False" Width="100%" CssClass="DataListHeader"></HeaderStyle>
					</asp:datalist></td>
			</tr>
			<tr>
				<td align="center"><br />
					<asp:Button id="btnConvert" OnClick="Convert_Order" runat="server" Text="Convert Order" CommandName="Convert" />
				</td>
			</tr>
		</table>
	<br />
	<br />
	<center>
	</center>

</asp:Content>