
<%@ Page Language="VB" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Web.Mail" %>
<%
   '*****************************************************************************
   '*   1. File Name       : common/Spot_Order.aspx                             *
   '*   2. Description     : Purchaser and seller can buy or sell               *
   '*		             Get here from "Spot floor " page                   *
   '*			     			                                *
   '*   3. Modification Log:                                                    *
   '*     Ver No.       Date          Author             Modification           *
   '*   -----------------------------------------------------------------       *
   '*      1.00       2-19-2004       Xiaoda             ASPX Converted         *
   '*                                                                           *
   '*****************************************************************************
%>
<form runat="server" id="Form">
    <TPE:Template Title="User Details" Runat="Server" />
     <!--#include FILE="../include/VBLegacy_Code.aspx"-->
<%
IF Session("Typ")<>"P" AND Session("Typ")<>"S" AND Session("Typ")<>"D" AND Session("Typ")<>"A" AND Session("Typ")<>"B" THEN Response.Redirect("../default.aspx")
Dim Id,plactype,l,j,Str1,Str2,CompId,Str,StrBis,Flag,OId,SessId,SessComp
Dim Locl2
Dim Locl() As String
Dim conn as SqlConnection
Dim cmdContent as SQLCommand
conn = new SqlConnection(Application("DBconn").ToString())
conn.Open()

Dim Rec0 as SqlDataReader
Dim Rec01 as SqlDataReader
Dim Rec1 as SqlDataReader
Dim conn1 as SqlConnection
Dim cmdContent1 as SQLCommand
conn1 = new SqlConnection(Application("DBconn").ToString())
conn1.Open()

IF Session("Typ")="A" OR Session("Typ")="B" THEN
	OId=Request.Form("OId")
	SessId=Request.Form("Id")
	SessComp=Request.Form("CompId")
ELSE
	OId=Request.QueryString("Id")
	SessId=Session("Id")
	SessComp=Session("Comp")
END IF

IF Request.Form("Submit")="Submit" THEN
        'Locl(0):Locality
	'Locl(1):Place
	 Dim temp
	 temp=Request.Form("Place")
	 Locl=Split(temp,",")       
	 'Locl=Split(Request.Form("Place"),",")
	IF Session("Typ")="P" OR Request.Form("TFlag")="P" THEN
		cmdContent= new SqlCommand("Exec spPBBOrder @Id="+OId.ToString()+", @PersId="+SessId.ToString()+", @CompId="+SessComp.ToString()+", @Qty="&Request.Form("Quantity")&", @Locl0="+Locl(0).ToString()+" , @Locl1="&Locl(1), conn)
	        cmdContent.ExecuteNonQuery()
	ELSE
		Locl=Split(Request.Form("Place"),",")
		cmdContent= new SqlCommand("Exec spSBBOrder @Id="+OId.ToString()+", @PersId="+SessId.ToString()+", @CompId="+SessComp.ToString()+", @Qty="&Request.Form("Quantity")&", @Locl0="+Locl(0).ToString()+" , @Locl1="&Locl(1), conn)
	        cmdContent.ExecuteNonQuery()
	END IF
	response.redirect ("../Common/Order_Thank_You.aspx")
END IF
%>

<%
IF Session("Typ")="P" OR Request.Form("TFlag")="P" THEN
	cmdContent1= new SqlCommand("SELECT 'Offer',OFFR_ID,OFFR_QTY,(CASE OFFR_SIZE WHEN 1 THEN 'lb' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 42000 THEN 'Pack Truck' WHEN 45000 THEN 'Bulk Truck' WHEN 190000 THEN 'Rail Car'  END)+(CASE WHEN OFFR_QTY>1 THEN 's' END),(OFFR_PROD),(OFFR_DETL), CAST(OFFR_PRCE AS VARCHAR), OFFR_TERM, (CASE OFFR_PAY WHEN 0 THEN 'Cash' WHEN '30' THEN '30 days' WHEN '45' THEN '45 days' WHEN '60' THEN '60 days' WHEN '90' THEN '90 days' WHEN 'L0' THEN 'L/C on site' WHEN 'L30' THEN 'L/C 30 days' WHEN 'L45' THEN 'L/C 45 days' WHEN 'L60' THEN 'L/C 60 days' WHEN'L90' THEN 'L/C 90 days' END),(ISNULL(OFFR_FROM_LOCL_NAME,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=OFFR_FROM)))) FROM BBOFFER WHERE OFFR_ID="+OId, conn1)
        Rec01= cmdContent1.ExecuteReader()

ELSE
        cmdContent1= new SqlCommand("SELECT 'Bid',BID_ID, BID_QTY,(CASE BID_SIZE WHEN 1 THEN 'lb' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 42000 THEN 'Pack Truck' WHEN 45000 THEN 'Bulk Truck' WHEN 190000 THEN 'Rail Car'  END)+(CASE WHEN BID_QTY>1 THEN 's' END),(BID_PROD),(BID_DETL), CAST(BID_PRCE AS VARCHAR), BID_TERM, (CASE BID_PAY WHEN 0 THEN 'Cash' WHEN '30' THEN '30 days' WHEN '45' THEN '45 days' WHEN '60' THEN '60 days' WHEN '90' THEN '90 days' WHEN 'L0' THEN 'L/C on site' WHEN 'L30' THEN 'L/C 30 days' WHEN 'L45' THEN 'L/C 45 days' WHEN 'L60' THEN 'L/C 60 days' WHEN'L90' THEN 'L/C 90 days' END),(ISNULL(BID_TO_LOCL_NAME,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=BID_TO)))) FROM BBID WHERE BID_ID="+OId, conn1)
        Rec01= cmdContent1.ExecuteReader()
END IF
        Rec01.Read()
%>



<script>
function Check(A)
{
	with(A)
	{
		<%IF Session("Typ")="A" OR Session("Typ")="B" THEN%>
		if (CompId.value=='') return Path.Mess(window,'Please select a company.','0')
		if (Id.value=='*') return Path.Mess(window,'Please select a person.','0')
		<%END IF%>
		if (Place.options[Place.options.selectedIndex].text.substring(Place.options[Place.options.selectedIndex].text.length-3,Place.options[Place.options.selectedIndex].text.length)=="(U)")
		return Path.Mess(window,'You have selected an undefined location, please define this location on the <a href=../Officer/Officer_Add_Location.asp>Add Location</a> page before placing an order.','0')
		<%IF Rec01(2)>20 THEN%>
		for(j=0;j<Quantity.value.length;j++)
			if (Quantity.value.charAt(j)<"0"||Quantity.value.charAt(j)>"9")
				return Path.Mess(window,'Please, enter a valid quantity.','0')
		if (Quantity.value><%=Rec01(2)%>||Quantity.value==""||Quantity.value<=0) return Path.Mess(window,'Please enter a valid quantity.','0')
		<%END IF%>
	}
	return true
}
</script>

<input type=hidden name=Submit value=''>
<input type=hidden name=TFlag value='<%=Request.Form("TFlag")%>'>
<input type=hidden name=OId value='<%=Request.Form("OId")%>'>
<BR><BR><BR>
<table cellpadding=0 cellspacing=0 width=100% height=100% border=0>
<%IF NN6 THEN%>
	<tr>
		<td colspan=2><img src=/images/1x1.gif height=1 width=540>
		</td>
	</tr>
<%END IF%>
	<tr valign=top>
		<td align=bottom><div align=center valign=center>
			<table border=0 cellpadding=0 cellspacing=0 width=540>
				<tr valign=top>
					<td colspan=4 CLASS=S5 background="../images/Bars/BG.gif"><img src=/images/1x1.gif width=3><%IF Session("Typ")="P" OR Request.Form("TFlag")="P" THEN%>Purchase Offer<%ELSE%>Sell Bid<%END IF%> #<%Response.write (OId)%>
					</td>
				</tr>
				<tr>
					<td width=1 rowspan=4 bgcolor=black><img width=1 src=/images/1x1.gif>
					</td>
					<td colspan=2><img width=1 src=/images/1x1.gif>
					</td>
					<td width=1 rowspan=4 bgcolor=black><img width=1 src=/images/1x1.gif>
					</td>
				</tr>
<%IF Session("Typ")="A" OR Session("Typ")="B" THEN%>
		<%
		IF Request.Form("CompId")<>"" AND Request.Form("CompId")<>"*Bid" AND Request.Form("CompId")<>"*Offer" THEN
			CompId=Request.Form("CompId")
		END IF
		IF Request.Form("TFlag")="P" THEN
		cmdContent= new SqlCommand("SELECT * FROM (SELECT COMP_ID,COMP_NAME=COMP_NAME,COMP_TYPE FROM COMPANY WHERE COMP_REG=1 AND COMP_TYPE IN ('P')AND EXISTS(SELECT PLAC_ID FROM PLACE WHERE PLAC_COMP=COMP_ID AND PLAC_TYPE!='H'))Q ORDER BY COMP_NAME", conn)
                Rec0= cmdContent.ExecuteReader()
		ELSE
		cmdContent= new SqlCommand("SELECT * FROM (SELECT COMP_ID,COMP_NAME=(CASE COMP_TYPE WHEN 'S' THEN 'Supplier' WHEN 'D' THEN 'Distributor' END)+'/'+COMP_NAME,COMP_TYPE FROM COMPANY WHERE COMP_REG=1 AND COMP_TYPE IN ('S','D')AND EXISTS(SELECT PLAC_ID FROM PLACE WHERE PLAC_COMP=COMP_ID AND PLAC_TYPE!='H'))Q ORDER BY COMP_NAME", conn)
                Rec0= cmdContent.ExecuteReader()
		END IF
		Str=""
		StrBis=""
		Flag=""
		While Rec0.Read()
			Str=Str+"'"+Rec0(0)+"',"
			StrBis=StrBis+"'"+Rec0(1)+"',"
			IF CStr(Rec0("COMP_ID"))=CStr(CompId) THEN Flag=CStr(Rec0("COMP_TYPE"))
			
		End While
		IF Str="" THEN Str="," ELSE Str=","&Str
		IF StrBis="" THEN StrBis="," ELSE StrBis=","&StrBis
		Rec0.Close
				%>
				<tr>
					<td colspan=2 CLASS=S1><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Company: <select name=CompId onChange=Form.Id.selectedIndex=0;Path.Wait(window);Form.submit()>
					<script>Path.SO(new Array(''<%=Left(Str,Len(Str)-1)%>),new Array('Select a Company'<%=Left(StrBis,Len(StrBis)-1)%>),'<%=CompId%>',document)</script>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					User:
		 <%
		Str=""
		StrBis=""
		IF CompId<>"" THEN
		        cmdContent= new SqlCommand("SELECT PERS_ID,PERS_LOGN FROM PERSON WHERE PERS_COMP="+CompId+" AND PERS_TYPE<>'O' AND PERS_REG=1 ORDER BY PERS_LOGN", conn)
                        Rec0= cmdContent.ExecuteReader()
			While Rec0.read()
				Str=Str+"'"+Rec0(0)+"',"
				StrBis=StrBis+"'"+Rec0(1)+"',"
				
			End While
			Rec0.Close
			
		END IF
		IF Str="" THEN Str="," ELSE Str=","&Str
		IF StrBis="" THEN StrBis="," ELSE StrBis=","&StrBis
		%>
				<select name=Id><script>Path.SO(new Array('*'<%=Left(Str,Len(Str)-1)%>),new Array('Select a User'<%=Left(StrBis,Len(StrBis)-1)%>),'<%=Id%>',document)</script>
					</td>
				</tr>
<%ELSE
	CompId=Session("Comp")
END IF%>
				<tr>
					<td><br>
						<table cellpadding=3 cellspacing=2>
							<tr>
								<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Product
								</td>
								<td><%=Rec01(4)%>
								</td>
							</tr>
							<tr>
								<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Details
								</td>
								<td><%=Rec01(5)%>
								</td>
							</tr>
							<tr>
								<td CLASS=S1>&nbsp;&nbsp;&nbsp;
<%IF Session("Typ")="P" OR Request.Form("TFlag")="P" THEN
response.write ("Origin")
ELSE
response.write ("Destination")
END IF%>
								</td>
								<td><%=Rec01(9)%>
								</td>
							</tr>
							<tr>
								<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Quantity&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
								<td>
<%IF Rec01(2)>20 THEN%>
<input type=text name=Quantity maxlength=6 size=4 value='<%=Request.Form("Quantity")%>'>&nbsp;&nbsp;&nbsp;<%=Rec01(3)%>&nbsp;&nbsp;&nbsp;&nbsp;(Max <%=Rec01(2)%>)
<%ELSE%>
<script>Path.SO(1,<%=Rec01(2)%>,'<%=Request.Form("Quantity")%>',document,'Quantity')</script>&nbsp;&nbsp;&nbsp;<%=Rec01(3)%>
<%END IF%>
								</td>
							</tr>
						</table>
					</td>
					<td><br>
						<table cellpadding=3 cellspacing=2>
							<tr>
								<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Terms
								</td>
								<td><%=Rec01(7)%>
								</td>
							</tr>
							<tr>
								<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Payment
								</td>
								<td><%=Rec01(8)%>
								</td>
							</tr>
							<tr>
								<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;
<%
IF Session("Typ")="P" OR Request.Form("TFlag")="P" THEN
	response.write ("Sell Price")
ELSE
	response.write ("Buy Price")
END IF
%>&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
								<td>$<%=Rec01(6)%> per lb.
<%Rec01.Close()%>
<%IF Session("Typ")="P" OR Request.Form("TFlag")="P" THEN%>
	<br>(Shipping not included)
<%END IF%>
								</td>
							</tr>
						</table>
					</td>
				</tr>
<%IF CompId.ToString()<>"" THEN%>
				<tr>
					<td CLASS=S1 colspan=2 valign=bottom>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%IF Session("Typ")="P" OR Request.Form("TFlag")="P" THEN
		response.write ("Delivery Point")
	ELSE
		response.write ("Shipping Point")
	END IF%>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%
	IF Session("Typ")="P" OR Request.Form("TFlag")="P" THEN
		plactype="D"
	ELSE
		plactype="S"
	END IF
	cmdContent= new SqlCommand("SELECT PLAC_ID,PLAC_NAME=(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL),PLAC_LOCL, PLAC_ADDR_ONE=(CASE WHEN PLAC_ADDR_ONE IS NULL THEN 1 ELSE 0 END) FROM PLACE WHERE PLAC_COMP="+CompId.ToString()+" AND PLAC_TYPE='"+plactype.ToString()+"' AND PLAC_ENBL=1 ORDER BY PLAC_LABL DESC, PLAC_NAME", conn)
        Rec1= cmdContent.ExecuteReader()
	Rec1.Read()
	
	IF Request.Form("Place")<>"" THEN
	        Dim k() As String
	   	
		k=Split(Request.Form("Place"),",")
		j=k(0)
		Locl=j+","+k(1)
	ELSE
	        
		j=Rec1("PLAC_LOCL").ToString()
		Locl2=j+","+Rec1("PLAC_ID")
	END IF
        Dim k2
	k2=Rec1("PLAC_LOCL")
	l=0
	Str1=""
	Str2=""
	Rec1.Close()
	cmdContent= new SqlCommand("SELECT PLAC_ID,PLAC_NAME=(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL),PLAC_LOCL, PLAC_ADDR_ONE=(CASE WHEN PLAC_ADDR_ONE IS NULL THEN 1 ELSE 0 END) FROM PLACE WHERE PLAC_COMP="+CompId.ToString()+" AND PLAC_TYPE='"+plactype.ToString()+"' AND PLAC_ENBL=1 ORDER BY PLAC_LABL DESC, PLAC_NAME", conn)
        Rec1= cmdContent.ExecuteReader()
	
	While Rec1.Read()
		Str1=Str1+"'"+Rec1("PLAC_LOCL").ToString()+","+Rec1("PLAC_ID").ToString()+"',"
		l=l*CByte(CInt(Rec1("PLAC_LOCL"))=Convert.ToInt32(k2))/255+1
		k2=Rec1("PLAC_LOCL")
		IF l>1 THEN 
		Str2=Str2+"'"+Rec1("PLAC_NAME").ToString()+" ("+l.ToString()+")'," 
		ELSE IF Rec1("PLAC_ADDR_ONE")="1" THEN 
		Str2=Str2+"'"+Rec1("PLAC_NAME")+" (U)'," 
		ELSE Str2=Str2+"'"+Rec1("PLAC_NAME")+"',"
		End IF
	End While
	Str1=Left(Str1,Len(Str1)-1)&"),new Array("&Left(Str2,Len(Str2)-1)
	Rec1.Close()
	%>
	<%IF Request.Form("Place")<>"" THEN%>
	<script>Path.SO(new Array(<%=Str1%>),'<%=Locl%>',document,'Place')</script>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%Else%>
	<script>Path.SO(new Array(<%=Str1%>),'<%=Locl2%>',document,'Place')</script>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<%End IF%>
	
	
	
	
	
	
	
	<input type=button class=tpebutton value="Specifications" onclick="Javascript:Path.Show(window,'../Common/MB_Specs_Location.aspx',document.Form.Place.options[document.Form.Place.selectedIndex].value.split(',')[1])"><br><br>
					</td>
				</tr>
<%END IF%>
				<tr bgcolor=white>
					<td colspan=2><center><input type=button class=tpebutton value="SUBMIT" onclick="Javascript:with(window.document){if(Check(document.Form)&&Path.Format(document.Form)){Form.Submit.value='Submit';Form.submit()}else{Form.reset()}}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type=button class=tpebutton value="CANCEL" onclick=<%IF Session("Typ")="A" OR Session("Typ")="B" THEN%>"Javascript:document.location='../Spot/Spot_Floor.aspx'" <%ELSE%>"Javascipt:history.back()" <%END IF%>>
					</td>
				</tr>
				<tr>
					<td colspan=4 height=1 bgcolor=black>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table></form>
<%
conn.Close
conn1.Close
%></center>

<TPE:Template Footer="true" Runat="Server" />

</form>

