<%
   '*****************************************************************************
   '*   1. File Name       : Common\MB_Specs_Contract.aspx                      *
   '*   2. Description     : Display contract details.                          *
   '*			     Accessed from Trading Floor's pop menu when logged as buyer or seller*
   '*						                                *
   '*   3. Modification Log:                                                    *
   '*     Ver No.       Date          Author             Modification           *
   '*   -----------------------------------------------------------------       *
   '*      1.00       2-24-2004       Xiaoda             Commented              *
   '*                                                                           *
   '*****************************************************************************
%>
<%@ Page Language="VB" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<!--#INCLUDE FILE="../include/Head.inc"-->
<%
'Database connection 
Dim conn as SqlConnection
Dim cmdContent as SQLCommand
Dim Rec0 as SqlDataReader
conn = new SqlConnection(Application("DBconn"))
conn.Open()

'Query the ocntract details based on contract ID
cmdContent= new SqlCommand("SELECT CONT_FMLY,CONT_CATG,CONT_PROC,CONT_MELT_RNG,CONT_DENS_RNG,CONT_MELT_TGT,CONT_DENS_TGT,CONT_IZOD_TGT,CONT_IZOD_RNG,CONT_ADDS_SLIP_TGT,CONT_ADDS_SLIP_RNG,CONT_ADDS_BLCK_TGT,CONT_ADDS_BLCK_RNG,CONT_FEAT,CONT_LABL FROM CONTRACT WHERE CONT_ID="&Request.QueryString("Id"), conn)
Rec0= cmdContent.ExecuteReader()
Rec0.Read()
%>
<center>
<form>
<center>
<table cellspacing=0 cellpading=0 border=0 width=580>
<tr>
<td  Class="Header"  colspan=2><SPAN CLASS=S8><center><font color=white><%=Rec0(14)%></center></font></SPAN>
</td>
</tr>
<tr><td><div align=left>Quality</div></td><td><div align=left><i>Virgin Prime Only</div></td></tr>
<tr><td><div align=left>Family</div></td><td><div align=left><i><%=Rec0(0)%></div></td></tr>
<tr><td><div align=left>Category</div></td><td><div align=left><i><%=Rec0(1)%></div></td></tr>
<tr><td><div align=left>Process</div></td><td><div align=left><i><%=Rec0(2)%></div></td></tr>
<tr><td><div align=left>Features</div></td><td><div align=left><i><%=Rec0(13)%></div></td></tr>
<tr><td><div align=left>Melt Flow Target</div></td><td><div align=left><i><%=Rec0(5)%></div></td></tr>
<tr><td><div align=left>Melt Flow Range</div></td><td><div align=left><i><%=Rec0(3)%></div></td></tr>
<tr><td><div align=left>Density Target</div></td><td><div align=left><i><%=Rec0(6)%></div></td></tr>
<tr><td><div align=left>Density Range</div></td><td><div align=left><i><%=Rec0(4)%></div></td></tr>
<tr><td><div align=left>IZOD Target</div></td><td><div align=left><i><%=Rec0(7)%></div></td></tr>
<tr><td><div align=left>IZOD Range</div></td><td><div align=left><i><%=Rec0(8)%></div></td></tr>
<tr><td><div align=left>Slip Adds Target</div></td><td><div align=left><i><%=Rec0(9)%></div></td></tr>
<tr><td><div align=left>Slip Adds Range</div></td><td><div align=left><i><%=Rec0(10)%></div></td></tr>
<tr><td><div align=left>Anti-Block Adds Target</div></td><td><div align=left><i><%=Rec0(11)%></div></td></tr>
<tr><td><div align=left>Anti-Block Adds Range</div></td><td><div align=left><i><%=Rec0(12)%></div></td></tr>
<%
Rec0.Close()
'If it not demo user
IF Session("Typ")="" THEN
	IF Session("CompTyp")="S" THEN
		cmdContent= new SqlCommand("SELECT BRAN_NAME FROM BRAND WHERE BRAN_ENBL=1 AND BRAN_CONT="&Request.QueryString("Id")&" AND BRAN_COMP="&Session("Comp"), conn)
                
	ELSE
		cmdContent= new SqlCommand("SELECT BRAN_NAME FROM BRAND WHERE BRAN_ENBL=1 AND BRAN_CONT="&Request.QueryString("Id"), conn)
                
	END IF
'it's Demo user
ELSE
        cmdContent= new SqlCommand("SELECT BRAN_NAME FROM BRAND WHERE BRAN_ENBL=1 AND BRAN_CONT="&Request.QueryString("Id"), conn)
END IF
%>
</script>
<tr bgcolor=#E8E8E8>
<td colspan=2  Class="Header" ><center>
<%
Rec0= cmdContent.ExecuteReader()
Rec0.Read()
If Rec0.Hasrows Then
%>
<font color=white>Conforming Brands</font><br></td></tr>

<%
Rec0.Close()
Rec0= cmdContent.ExecuteReader()
response.write ("<tr>")
While Rec0.Read()
response.write ("<td>")
response.write (Rec0(0))
response.write ("</td>")
If Rec0.Read() Then
IF Rec0(0) Is DBNull.Value Then
response.write ("</tr>")
Else
   IF Rec0(0)="" Then
   response.write ("</tr>")
   Else
	response.write ("<td>")
	response.write (Rec0(0))
	response.write ("</td></tr>")
   End IF
End IF	
Else
response.write ("</tr>")
End IF

	End While
	END IF
Rec0.Close()
conn.Close()
%>
</tr>
<tr>
<td colspan=5  Class="Header" ><img src=/images/1x1.gif height=1>
</td>
</tr>
</table>
	<br><br><br>
	<input type=button class=tpebutton value="OK" onclick="Javascript:history.back()">
</td>
</tr>

</table>

</form>
</center>
<!--#INCLUDE FILE="../include/Bottom.inc"-->