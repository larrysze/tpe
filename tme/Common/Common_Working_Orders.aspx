<%@ Page Language="VB" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<form runat="server" id="Form">
    <TPE:Template PageTitle="" Runat="Server" />
    <!--#include FILE="../include/VBLegacy_Code.aspx"-->

<%
'database connection
Dim conn as SqlConnection
Dim cmdContent as SQLCommand
Dim Rec0 as SqlDataReader
conn = new SqlConnection(Application("DBconn"))
conn.Open()
 Session("Typ")="P"
'This page is available for system administrator, buyers, sellers, officers and brokers
IF Session("Typ")="" THEN
	Response.Redirect ("../default.aspx")
END IF

DIM i,Str,StrBis,Height,Flag,CompId,Id,Number,totval,item,recID,chkd,StrDel,ORDS,a
' ORDR_TERM 
'IF Session("Typ")="P" THEN
'
'	'if purchaser has an order in ORDERS talbe which has no order term, take them to Purchaser_Terms.aspx
'	'to set the order term
'	cmdContent= new SqlCommand("SELECT TOP 1 ORDR_ID FROM ORDERS WHERE ORDR_TERM IS NULL AND ORDR_BUYR="&Session("Id"), conn)
'	Rec0= cmdContent.ExecuteReader()
'	If Rec0.Read()
'		i=Rec0(0)
'		Rec0.Close
'		Response.Redirect("/Purchaser/Purchaser_Terms.aspx?Id="&i)
'	END IF
'	Rec0.Close
'END IF
Height=260


'get the person id and company id
IF Request.Form("CompId")="" AND Request.QueryString("Id")<>"" AND Request.Form("Id")="" THEN
	IF Session("Typ")="A" OR Session("Typ")="C" OR Session("Typ")="B" THEN
		cmdContent= new SqlCommand("SELECT PERS_COMP FROM PERSON WHERE PERS_ID="&Request.QueryString("Id"), conn)
		Rec0= cmdContent.ExecuteReader()
		Rec0.Read
		CompId=Rec0(0).ToString()
		Rec0.Close
	ELSE
		CompId=Session("Comp").ToString()
	END IF
	Id=Request.QueryString("Id")
ELSEIF Session("Typ")="P" OR Session("Typ")="S" THEN
	Id=Session("Id").ToString()
	CompId=Session("Comp").ToString()
ELSE
	Id=Request.Form("Id")
	IF Session("Typ")="A" OR Session("Typ")="C" OR Session("Typ")="B" THEN
		IF Request.QueryString("type")<>"" AND Request.Form("CompId")="" THEN
			CompId=Request.QueryString("type")
		ELSE
			CompId=Request.Form("CompId")
		END IF
	ELSE
		CompId=Session("Comp").ToString()
	END IF
END IF


'Preare the company list
'only administrator, creditor and borker can see this company list
IF Session("Typ")="A" OR Session("Typ")="C" OR Session("Typ")="B" THEN

        cmdContent= new SqlCommand("SELECT * FROM (SELECT COMP_ID,COMP_NAME=(CASE COMP_TYPE WHEN 'P' THEN 'Purchaser' WHEN 'S' THEN 'Supplier' WHEN 'D' THEN 'Distributor' END)+'/'+COMP_NAME,COMP_TYPE FROM COMPANY WHERE COMP_REG=1 AND COMP_TYPE IN ('P','S','D'))Q ORDER BY COMP_NAME", conn)
        Rec0= cmdContent.ExecuteReader()
	Str=""
	StrBis=""
	Flag=""
	   While Rec0.Read()
		Str=Str+"'"&Rec0(0)&"',"
		StrBis=StrBis+"'"&Rec0(1)&"',"
	     IF CStr(Rec0("COMP_ID"))=CStr(CompId) THEN
	     Flag=CStr(Rec0("COMP_TYPE"))
	     End IF
	   End While
	IF Str="" THEN
	   Str=","
	ELSE Str=","&Str
	End If
	IF StrBis="" THEN
	StrBis=","
	ELSE StrBis=","&StrBis
	End If
	Rec0.Close

END IF


IF Flag="" AND (Session("Typ")="P" OR Session("Typ")="S") THEN
Flag=Session("CompTyp")
END IF


'Delete the record
IF Request.Form("Delete")<>"" THEN
	Application("Flag")=1
	Application.Lock()
	IF Session("Typ")="P" OR Flag="P" OR Request.Form("CompId")="*Bid" THEN

	'use procedure to delete based on value of perimeter "VARID"
	cmdContent= new SqlCommand("Exec spPWO "&Request.Form("Delete"), conn)
        Rec0= cmdContent.ExecuteReader()
	Rec0.Close
	ELSE
	cmdContent= new SqlCommand("Exec spSWO "&Request.Form("Delete"), conn)
        Rec0= cmdContent.ExecuteReader()
	Rec0.Close
	END IF
	Application("Flag")=0
	Application.UnLock()
END IF

'Update multiple records
IF Request.Form("ChgVals")="1" THEN
ORDS=""
a=0

'Get VARID values from form and assign them to string ORDS
For Each item In Request.Form
    If Left( item, 3 ) = "CB_" AND Request.Form(item)="TRUE" AND a<25 Then
		a=a+1
        recID = Mid( item, 4 )
		ORDS=ORDS+","&recID
	END IF
NEXT

    'redirect user to different page to perform update based on login info'
	IF Session("Typ")="A" OR Session("Typ")="B" THEN
		IF Flag<>"P" AND CompId<>"*Bid" THEN
			Response.Redirect ("../Administrator/Supplier_list_update.aspx?ORDS="+ORDS)
		ELSE
			Response.Redirect ("../Administrator/Purchaser_list_update.aspx?ORDS="+ORDS)
		END IF
	ELSE
		IF Session("Typ")="P" OR Flag="P" THEN
		    response.redirect("/purchaser/Purchaser_List Order_Update.aspx?ORDS="+ORDS)
		ELSE
			response.redirect("/Supplier/Supplier_List_Order_Update.aspx?ORDS="+ORDS)
		END IF
	END IF
END IF

'Delete multiple records
IF Request.Form("DelVals")="1" THEN
StrDel=""

For Each item In Request.Form
    If Left( item, 3 ) = "CB_" Then
        recID = Mid( item, 4 )
        chkd = Request.Form(item)
		If chkd="TRUE" Then
				IF Session("Typ")="P" OR Flag="P" OR CompId="*Bid" OR Request.Form("CompId")="*Bid" THEN
				cmdContent= new SqlCommand("Exec spPWO "&recID, conn)
                                Rec0= cmdContent.ExecuteReader()
				Rec0.Close
			ELSE
			        cmdContent= new SqlCommand("Exec spSWO "&recID, conn)
                                Rec0= cmdContent.ExecuteReader()
				Rec0.Close
			END IF
		End If
    End If
NEXT
END IF

'Update single record
IF Request.Form("Modify")<>"" THEN
	IF Flag<>"P" AND CompId<>"*Bid" THEN
		response.redirect("/Forward/Place_Offer.aspx?IdOrder="&Request.Form("Modify"))
	ELSE
		response.redirect("/Forward/Place_Bid.aspx?IdOrder="&Request.Form("Modify"))
	END IF
END IF
%>

<DIV id=specs style='position:absolute; width:100%; height:50%; visibility: hidden; overflow: visible;'>
<table border=0 cellspacing=0 cellpading=0 width=100% height=100%><tr><td width=100% height=100%  align=left><left>
<iframe TopMargin=0 RightMargin=0 marginheight=0 border=0 scrolling=no frameborder=0 WIDTH=1020 HEIGHT=530 name=description ></iframe>
</left></td></tr></table>
</DIV>
<!--#INCLUDE FILE="../include/Loading.inc"-->
<!--#INCLUDE FILE="../include/Body.inc"-->
<script>
function Delete_Lead(ID)
// function promtes user and then delete record
{
	var x =window.confirm("Are you sure to want to delete this working order?");
	if (x) // set delete flag and submit form
	{
		document.Form.Delete.value= ID
				document.Form.submit();
	}
}
// function to select all the checkbox
function checkall()
{
	dml=document.Form;
	len = dml.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++) {if (dml.elements[i].name.substring(0,3)=='CB_'){dml.elements[i].checked=1}}}
i=0
function Modify(Id)
{
	with(document.Form)
	{
		<%IF Flag<>"P" THEN%>
			action="/Supplier/Supplier_Sell_Order_Entry.aspx";
		<%ELSE%>
			action="/Purchaser/Purchaser_Buy_Order_Entry.aspx";
		<%END IF%>
		IdOrder.value=Id;
		submit();
	}
}
function Answer(Flag)
{
	with(document.Form)
		if(Flag)
			submit()
}
</script>
<SPAN ID=Main name=Main style='x:0px; y:0px;'>
<input type=hidden name=Column value='<%
IF Request.Form("Column")="OFFR_DATE" AND (Flag="P" OR CompId="*Bid" OR Session("Typ")<>"C") THEN
	%>BID_DATE<%
ELSEIF Request.Form("Column")="BID_DATE" AND (Flag<>"P" OR CompId="*Offer" OR Session("Typ")<>"C") THEN
	%>OFFR_DATE<%
ELSE	%><%=Request.Form("Column")%><%
END IF%>'>
<input type=hidden name=Order value='<%=Request.Form("Order")%>'>
<input type=hidden name=Delete value=''>
<input type=hidden name=Modify value=''>
<input type=hidden name=DelVals value=''>
<input type=hidden name=IdOrder value=''>
<input type=hidden name=ChgVals value=''>
<input type=hidden name=ChgOrds value=''>
<input type=hidden name=SearchSubmit value=''>
<input type=hidden name=param value='<%=Request.Form("param")%>'>
<input type=hidden name=ShowAll value=''>
<table border=0 cellpadding=0 cellspacing=0 align=center>
<%
'More options for administrator'
IF Session("Typ")<>"P" AND Session("Typ")<>"S" THEN%>
	<tr>
	<td colspan=12><%IF Session("Typ")="A" OR Session("Typ")="C" OR Session("Typ")="B" THEN%>
	&nbsp;&nbsp;&nbsp;&nbsp;Company:<script>Path.SOS(new Array('*Offer','*Bid'<%=Left(Str,Len(Str)-1)%>),new Array('All Offers','All Bids'<%=Left(StrBis,Len(StrBis)-1)%>),'<%=CompId%>',document,'CompId','document.Form.Delete.value="";Form.Id.selectedIndex=0;Path.Wait(window);')</script>&nbsp;<%END IF%>
User: <%
Str=""
StrBis=""

'prepare the list of company user.
IF CompId<>"*Offer" AND CompId<>"*Bid" AND CompId<>"" THEN
	Str=""
	StrBis=""
	dim Strtemp
	Strtemp="SELECT PERS_ID,PERS_LOGN FROM PERSON WHERE PERS_COMP="+CompId+" AND PERS_TYPE<>'O' AND PERS_REG=1 ORDER BY PERS_LOGN"
	cmdContent= new SqlCommand(Strtemp, conn)
        Rec0= cmdContent.ExecuteReader()
	While Rec0.Read()
		Str=Str+"'"&Rec0(0)&"',"
		StrBis=StrBis+"'"&Rec0(1)&"',"
	End While
	Rec0.Close
END IF
IF Str="" THEN Str="," ELSE Str=","&Str
IF StrBis="" THEN StrBis="," ELSE StrBis=","&StrBis
%><select name=Id onChange=Form.submit()><script>Path.SO(new Array('*'<%=Left(Str,Len(Str)-1)%>),new Array('All'<%=Left(StrBis,Len(StrBis)-1)%>),'<%=Id%>',document)</script>
</select>
        </td>
	</tr>
<%END IF%>


	<tr>
		<td colspan=12><br>
&nbsp;&nbsp;&nbsp;&nbsp;Resin Type:
<script>Path.SOS(new Array('','HDPE','LDPE','LLDPE','PS','PP'),new Array('All','HDPE','LDPE','LLDPE','PS','PP'),'<%=Request.Form("AType")%>',document,'AType','document.Form.Delete.value="";')</script>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<script>Path.SOS(new Array('','0','1'),new Array('All','Truckload','Rail Car'),'<%=Request.Form("ASize")%>',document,'ASize','document.Form.Delete.value="";')</script>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Search:&nbsp;
<input type=text name=Search size=8 value=''>&nbsp;&nbsp;

<input type=button class=tpebutton value="Search" onClick="Javascript:if(Path.Format(document.Form)==true){Path.Wait(window); document.Form.SearchSubmit.value='Submit';document.Form.Delete.value='';document.Form.submit()}">
<input type=button class=tpebutton value="Show All" onclick="Javascript:Path.Wait(window);document.Form.ShowAll.value='Show';document.Form.Delete.value='';document.Form.submit()">

		</td>
	</tr>

<%
'additional parameters for SQL query

StrBis=""
IF Request.QueryString("Number")<>"" THEN
	Number=Request.QueryString("Number")
ELSE
	Number=-1
END IF
dim temp
IF Session("Typ")="O" AND Session("CompTyp")="P" THEN Flag="P"
IF CompId="*Bid" THEN
	CompId="-1"

ELSEIF CompId="*Offer" THEN
	CompId=""
END IF


'Execute the store procedure spGWO1 on Genius
Str="Exec spGWO1 @Flag='"+Flag+"', @SesTyp='"+Session("Typ")+"', @Number="&Number
IF Id<>"" THEN
	IF Id="*" THEN Id=-1
	Str=Str+", @Id="&Id
END IF

'Display orders occured in certain Month
If request.querystring ("month") <> "" Then
dim m, monthchar
m=request.querystring ("month")
Str=Str+", @Wmonth="&m
End IF

IF CompId<>""THEN
Str=Str+", @CompId="&CompId
END IF

'Display orders meet requirements of "search"
IF Request.Form("Search")<>"" THEN
	Str=Str+", @Search='"&Request.Form("Search")&"'"
END IF

'Display all the orders
IF Request.Form("ShowAll")<>"" THEN
	Str=Str+", @ShowAll='"&Request.Form("ShowAll")&"'"
END IF
Str=Str+", @SearchCol='All'"
IF Request.Form("Column")<>"" THEN
	Str=Str+", @Col='"&Request.Form("Column")&"', @Order='"&Request.Form("Order")&"'"
END IF

'Display orders has certain Resin type
IF Request.Form("AType")<>"" THEN
	Str=Str+", @AType='"&Request.Form("AType")&"'"
END IF

'Display orders has certain order size
IF Request.Form("ASize")<>"" THEN
	Str=Str+", @ASize='"&Request.Form("ASize")&"'"
END IF

'Exxcute store procedure with parameters to query orders
cmdContent= new SqlCommand(Str, conn)
Rec0= cmdContent.ExecuteReader()
totval=0
%>
</table>

<%'Begin the table for record output'%>
<table border=0 align=center cellspacing=0 cellpadding=0 bgcolor="#E8E8E8">
<%

'display buttons based on user login'
'Regular user has no access to Multiple Delete and Update functions
IF Session("Typ")="P" OR Session("Typ")="S" OR Session("Typ")="A" OR Session("Typ")="B" THEN%>
<tr   Class="TableHeader" >
<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
<td width=3  Class="TableHeader" ><img src=/images/1x1.gif width=1></td>
<td colspan=10  Class="TableHeader">
<input type=button value="Delete Selected" class=tpebutton onclick="Javascript:document.Form.DelVals.value='1';document.Form.Delete.value='';Path.Mess(window,'Are you sure to want to delete all the selected working orders?',1)">
<input type=button value="Change Selected" class=tpebutton onclick="Javascript:document.Form.ChgVals.value='1';document.Form.Delete.value='';document.Form.submit()"><font color=white size=1>(Please select only 25 at a time)</font>
</td>
<td width=3  Class="TableHeader" ><img src=/images/1x1.gif width=1></td>
<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
</tr>
<%End IF%>

<tr>
<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
<td width=3   Class="TableHeader" ><img src=/images/1x1.gif width=1></td>
<td align=left   Class="TableHeader" ><input onclick="javascript:checkall()" type=checkbox> All</td>
<TD align=center   Class="TableHeader" ><strong><a href="JavaScript:document.Form.Column.value='VARDATE';Form.submit()")>&nbsp;&nbsp;&nbsp;Date</a></strong></td>
<%

'When CompId="*Bid"---display bids
'When CompId="*Offer"---display offers
IF Flag="P" OR Flag="" AND CompId="*Bid" OR Session("Typ")="OP" OR Session("Typ")="P" THEN%>
<TD align=center  Class="TableHeader" ><strong>
<%'Display different table header'%>
<%If request.form("CompID")="*Bid" Then %>
<a  href="JavaScript:document.Form.Column.value='VARID';Form.submit()") >&nbsp;&nbsp;&nbsp;BID # </td></td>
<%Else%>
<a  href="JavaScript:document.Form.Column.value='VARDATE';Form.submit()") >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Offer # </td></td>
<%End IF%>
<%ELSE%>
<TD align=center   Class="TableHeader" ><strong>
<%If request.form("CompID")="*Bid" Then %>
<a  href="JavaScript:document.Form.Column.value='VARID';Form.submit()") >&nbsp;&nbsp;&nbsp;BID # </td></td>
<%Else%>
<a  href="JavaScript:document.Form.Column.value='VARDATE';Form.submit()") >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Offer # </td></td>
<%End IF%>
<%END IF%>
<TD align=center   Class="TableHeader" ><strong><a  href="JavaScript:document.Form.Column.value='VARWGHT';Form.submit()") >&nbsp;&nbsp;&nbsp;Size</td>
<TD align=center   Class="TableHeader" ><strong><a href="JavaScript:document.Form.Column.value='VARCONTRACT';Form.submit()") >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contract</td>
<TD align=center   Class="TableHeader" ><strong><a  href="JavaScript:document.Form.Column.value='VARMONTH';Form.submit()") >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Month</td>
<TD align=center  Class="TableHeader" ><strong><a  href="JavaScript:document.Form.Column.value='VARPRICE';Form.submit()") >&nbsp;&nbsp;&nbsp;Price</td>
<TD align=center  Class="TableHeader" ><strong><a  href="JavaScript:document.Form.Column.value='VARVALUE';Form.submit()" >&nbsp;&nbsp;&nbsp;Value</td>
<%IF Flag="P" OR Flag="" AND CompId="*Bid" OR Session("Typ")="OP" OR Session("Typ")="P" THEN%>
<%If request.form("CompID")="*Bid" Then %>
<TD align=center  Class="TableHeader" ><strong><a href="JavaScript:document.Form.Column.value='VARPLACE';Form.submit()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Destination Point</td>
<TD align=center  Class="TableHeader" ><strong><a href="JavaScript:document.Form.Column.value='VARUSR';Form.submit()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Buyer</td>
<%Else%>
<TD align=center  Class="TableHeader" ><strong><a href="JavaScript:document.Form.Column.value='VARPLACE';Form.submit()">&nbsp;&nbsp;&nbsp;Origin Point</td>
<TD align=center  Class="TableHeader" ><strong><a href="JavaScript:document.Form.Column.value='VARUSR';Form.submit()">&nbsp;&nbsp;&nbsp;Seller</td>
<%End IF%>
<%ELSE%>
<%If request.form("CompID")="*Bid" Then %>
<TD align=center  Class="TableHeader" ><strong><a href="JavaScript:document.Form.Column.value='VARPLACE';Form.submit()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Destination Point</td>
<TD align=center  Class="TableHeader" ><strong><a href="JavaScript:document.Form.Column.value='VARUSR';Form.submit()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Buyer</td>
<%Else%>
<TD align=center  Class="TableHeader" ><strong><a href="JavaScript:document.Form.Column.value='VARPLACE';Form.submit()">&nbsp;&nbsp;&nbsp;Origin Point</td>
<TD align=center  Class="TableHeader" ><strong><a href="JavaScript:document.Form.Column.value='VARUSR';Form.submit()">&nbsp;&nbsp;&nbsp;Seller</td>
<%End IF%>
<%END IF%>
<td width=3  Class="TableHeader" ><img src=/images/1x1.gif width=1></td>
<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
</tr>

<%
'Display "no record found" message if no record(FieldCount=0) retrieved from database'
If Rec0.FieldCount=0 Then
%>
</table>
<P align=center><font size=3 color=red><b>
There is No Record based on your choice.
</p>
<%
'display the table with data
Else
%>
<%
'<tr> has different background color each row'
Dim Count
Count = 1
Dim AltColor
AltColor = false
Dim iPixel_Down
iPixel_Down = 75

While Rec0.Read()

if AltColor Then
AltColor = false
Else
AltColor = true
End If
if AltColor Then Response.Write("<TR bgcolor=white>") Else Response.Write("<TR>")
Count = Count +1
iPixel_Down = iPixel_Down + 20
%>
<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
<td width=3 ><img src=/images/1x1.gif width=1></td>
<TD align=center>
<a style="color:white;text-decoration:none;font-size=2;font-face:arial,verdana">
<input type=checkbox <%IF MSIE THEN%>STYLE="BORDER:NONE;BACKGROUND-COLOR:TRANSPARENT;"<%END IF%> name=CB_<%=Rec0("VARID")%> value=TRUE>
<a href="JavaScript:Delete_Lead(<%=Rec0("VARID")%>)">
<img src=../Pics/Icons/icon_delete.gif border=0 Alt=Delete></a>
<%IF Session("Typ")="P" OR Session("Typ")="S" THEN%>
<a href=Javascript:Modify(<%=Rec0("VARID")%>)>
<img src=../pics/icons/icon_modify.gif border=0 Alt=Change></a>
<%ELSEIF Session("Typ")="A" OR Session("Typ")="B" THEN%>
<a href="Javascript:document.Form.Modify.value=<%=Rec0("VARID")%>;document.Form.submit()">
<img src=../pics/icons/icon_modify.gif border=0 Alt=Change></a>
<%END IF%>
</td>
<td align=center><%=Rec0("VARDATE")%></td>
<td align=center>&nbsp;&nbsp;&nbsp;<%=Rec0("VARID")%></td>
<td align=center>&nbsp;&nbsp;&nbsp;<%=Rec0("VARSIZE")%></td>
<td align=center>&nbsp;&nbsp;&nbsp;<%=Rec0("VARCONTRACT")%></td>
<td align=center>&nbsp;&nbsp;&nbsp;<%=Rec0("VARMONTH")%></td>
<td align=center>&nbsp;&nbsp;&nbsp;<%=FormatNumber(Rec0("VARPRICE"),3,0,-2,-2)%></td>
<td align=center>&nbsp;&nbsp;&nbsp;<%=FormatNumber(Rec0("VARVALUE"),0,0,-2,-2)%></td>
<td align=center>&nbsp;&nbsp;&nbsp;
<a href=Javascript:window.Path.Show(window,"../Common/MB_Specs_Location.aspx",<%=Rec0("VARPLACEID")%>)><%=Rec0("VARPLACE")%></a>
</td>
<td align=center>&nbsp;&nbsp;&nbsp;
<a href="../Common/MB_Specs_User.aspx?Id=<%=Rec0("VARUSRID")%>"><%=Rec0("VARUSR")%></a>

</td>
<td width=3><img src=/images/1x1.gif width=1></td>
<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
</tr>
<%
'calculate total value'
totval=totval+(FormatNumber(Rec0("VARVALUE"),0,0,-2,-2))

End While
%>
<tr>
<td colspan=10 Class="TableHeader">
<font color=white><b>Total</b>
</td>
<td colspan=6 Class="TableHeader"><%=FormatCurrency(totval,0,-2,-2,-2)%>
<img src=/images/1x1.gif height=1>
</td>
</tr>
</table>
<%
'end for if rec.eof
End If
Rec0.Close
Conn.Close
%>

</SPAN>
<TPE:Template Footer="true" Runat="Server" />

</form>
<script>Path.Start(window)</script>
