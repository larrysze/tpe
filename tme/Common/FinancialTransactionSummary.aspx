<%@ Page Language="c#" CodeBehind="FinancialTransactionSummary.aspx.cs" AutoEventWireup="True" Inherits="localhost.Common.FinancialTransactionSummary" MasterPageFile="~/MasterPages/Menu.Master" Title="The Plastics Exchange: Commodity Plastic Resin, Buy Resin, Sell Resin, Plastic Materials"%>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<asp:Content runat="server" ContentPlaceHolderID=cphCssLink>
<style>
.menuskin { BORDER-RIGHT: black 2px solid; BORDER-TOP: black 2px solid; Z-INDEX: 100; VISIBILITY: hidden; FONT: 12px/18px Verdana; BORDER-LEFT: black 2px solid; WIDTH: 165px; BORDER-BOTTOM: black 2px solid; POSITION: absolute; BACKGROUND-COLOR: menu }
.menuskin A { PADDING-RIGHT: 10px; PADDING-LEFT: 10px; COLOR: black; TEXT-DECORATION: none }
#mouseoverstyle { BACKGROUND-COLOR: highlight }
#mouseoverstyle A { COLOR: white }
.Justify { text-align:center; vertical-align:center;}
.Orange {background-color:#FD9D00;}
</style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphJavaScript">
<script language="JavaScript1.2">

        //Pop-it menu- By Dynamic Drive
        //For full source code and more DHTML scripts, visit http://www.dynamicdrive.com
        //This credit MUST stay intact for use

        var linkset=new Array()
        //SPECIFY MENU SETS AND THEIR LINKS. FOLLOW SYNTAX LAID OUT

        <asp:PlaceHolder id="phActionButton"  runat="server" />
        ////No need to edit beyond here

        var ie4=document.all&&navigator.userAgent.indexOf("Opera")==-1
        var ns6=document.getElementById&&!document.all
        var ns4=document.layers

        function showmenu(e,which){

        if (!document.all&&!document.getElementById&&!document.layers)
        return

        clearhidemenu()

        menuobj=ie4? document.all.popmenu : ns6? document.getElementById("popmenu") : ns4? document.popmenu : ""
        menuobj.thestyle=(ie4||ns6)? menuobj.style : menuobj

        if (ie4||ns6)
        menuobj.innerHTML=which
        else{
        menuobj.document.write('<layer name=gui bgColor=#E6E6E6 width=165 onmouseover="clearhidemenu()" onmouseout="hidemenu()">'+which+'</layer>')
        menuobj.document.close()
        }

        menuobj.contentwidth=(ie4||ns6)? menuobj.offsetWidth : menuobj.document.gui.document.width
        menuobj.contentheight=(ie4||ns6)? menuobj.offsetHeight : menuobj.document.gui.document.height
        eventX=ie4? event.clientX : ns6? e.clientX : e.x
        eventY=ie4? event.clientY : ns6? e.clientY : e.y

        //Find out how close the mouse is to the corner of the window
        var rightedge=ie4? document.body.clientWidth-eventX : window.innerWidth-eventX
        var bottomedge=ie4? document.body.clientHeight-eventY : window.innerHeight-eventY

        //if the horizontal distance isn't enough to accomodate the width of the context menu
        if (rightedge<menuobj.contentwidth)
        //move the horizontal position of the menu to the left by it's width
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX-menuobj.contentwidth : ns6? window.pageXOffset+eventX-menuobj.contentwidth : eventX-menuobj.contentwidth
        else
        //position the horizontal position of the menu where the mouse was clicked
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX : ns6? window.pageXOffset+eventX : eventX

        //same concept with the vertical position
        if (bottomedge<menuobj.contentheight)
        menuobj.thestyle.top=ie4? document.body.scrollTop+eventY-menuobj.contentheight : ns6? window.pageYOffset+eventY-menuobj.contentheight : eventY-menuobj.contentheight
        else
        menuobj.thestyle.top=ie4? document.body.scrollTop+event.clientY : ns6? window.pageYOffset+eventY : eventY
        menuobj.thestyle.visibility="visible"
        return false
        }

        function contains_ns6(a, b) {
        //Determines if 1 element in contained in another- by Brainjar.com
        while (b.parentNode)
        if ((b = b.parentNode) == a)
        return true;
        return false;
        }

        function hidemenu(){
        if (window.menuobj)
        menuobj.thestyle.visibility=(ie4||ns6)? "hidden" : "hide"
        }

        function dynamichide(e){
        if (ie4&&!menuobj.contains(e.toElement))
        hidemenu()
        else if (ns6&&e.currentTarget!= e.relatedTarget&& !contains_ns6(e.currentTarget, e.relatedTarget))
        hidemenu()
        }

        function delayhidemenu(){
        if (ie4||ns6||ns4)
        delayhide=setTimeout("hidemenu()",500)
        }

        function clearhidemenu(){
        if (window.delayhide)
        clearTimeout(delayhide)
        }

        function highlightmenu(e,state){
        if (document.all)
        source_el=event.srcElement
        else if (document.getElementById)
        source_el=e.target
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        else{
        while(source_el.id!="popmenu"){
        source_el=document.getElementById? source_el.parentNode : source_el.parentElement
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        }
        }
        }

        if (ie4||ns6)
        document.onclick=hidemenu

	</script>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">

	<div class="menuskin" id="popmenu" onmouseover="clearhidemenu();highlightmenu(event,'on')"
		onmouseout="highlightmenu(event,'off');dynamichide(event)"></div>
	<center></center>
			<TABLE width="1024px" border="0">
							<TR>
								<TD align="left"><SPAN class="Header Bold Color2">Filled Orders</SPAN></TD>
								<TD align="left"><span class="Content Bold Color2">Search:</span>
									<asp:TextBox CssClass="InputForm" id="txtSearch" runat="server" size="10"></asp:TextBox>
								
									<asp:Button Height="22px" id="bSearch" onclick="Click_Search" runat="server" text="Search"></asp:Button></TD>
								<TD align="right"><span class="Content Bold Color2">Filters</span>
									<asp:DropDownList CssClass="InputForm" id="ddlMonth" runat="server" OnSelectedIndexChanged="Change_Month" AutoPostBack="True"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<asp:DropDownList CssClass="InputForm" id="ddlCompanies" runat="server" OnSelectedIndexChanged="Change_Comp" AutoPostBack="True"></asp:DropDownList></TD>
							</TR>
						</TABLE>
<div class="DivScrollBarsAdm">
	<table cellSpacing="0" cellPadding="0" width="85%" align="center" middle="center">
		<asp:panel id="pnAdminFunctions" runat="server">

				<TR>
					<TD colSpan="1" align="left">
				
					</TD>
				</TR>
		</asp:panel>
		<tr>
		<tr>
			<td>
				
			</td>
		<TR>
			<TD>
			<asp:panel id="pnContent" runat="server"><%if ((Session["Typ"].ToString().Equals("S")) || (Session["Typ"].ToString().Equals("O"))){%><%}else if ((Session["Typ"].ToString().Equals("P")) || (Session["Typ"].ToString().Equals("O"))){%><%}else{%>
					<asp:DataGrid id="dg" runat="server" EnableViewState="False" CssClass="LinkNormal LightGray" HeaderStyle-CssClass="LinkNormal"
						OnItemDataBound="KeepRunningSum" ShowFooter="True" AutoGenerateColumns="False" onSortCommand="SortDG"
						AllowSorting="True" DataKeyField="TRANS_NUM" CellPadding="2" HorizontalAlign="Center" Width="100%">
						<AlternatingItemStyle CssClass="LinkNormal Color2 DarkGray"></AlternatingItemStyle>
						<ItemStyle CssClass="LinkNormal Color2 LightGray"></ItemStyle>
						<HeaderStyle CssClass="LinkNormal Justify Orange"></HeaderStyle>
						<Columns>
							<asp:HyperLinkColumn DataNavigateUrlField="TRANS_NUM" DataNavigateUrlFormatString="/administrator/Transaction_Details.aspx?ID={0}&amp;Referer=/Common/Filled_Orders.aspx"
								DataTextField="TRANS_NUM" SortExpression="TRANS_NUM ASC" HeaderText="#">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:HyperLinkColumn>
							<asp:BoundColumn DataField="VARDATE" SortExpression="VARDATE ASC" HeaderText="Date"></asp:BoundColumn>
							<asp:BoundColumn DataField="VARSHIP" SortExpression="VARSHIP ASC" HeaderText="Inventory or delivered"></asp:BoundColumn>
							<asp:BoundColumn DataField="VARWGHT" SortExpression="VARWGHT ASC" HeaderText="Weight(lbs)" DataFormatString="{0:#,###}">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="VARCONTRACT" SortExpression="VARCONTRACT ASC" HeaderText="Product">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="BUYING_PRICE" SortExpression="BUYING_PRICE ASC" HeaderText="Buying price"
								DataFormatString="{0:c}">
								<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="TOTAL_BUYING_PRICE" SortExpression="TOTAL_BUYING_PRICE ASC" HeaderText="Total buying price"
								DataFormatString="{0:c}">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="SELLING_PRICE" SortExpression="SELLING_PRICE ASC" HeaderText="Selling price"
								DataFormatString="{0:c}">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="TOTAL_SELLING_PRICE" SortExpression="TOTAL_SELLING_PRICE ASC" HeaderText="Total selling price"
								DataFormatString="{0:c}">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="FREIGHT_COST" SortExpression="FREIGHT_COST ASC" HeaderText="Freight cost"
								DataFormatString="{0:c}">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="COMMISSIONS" SortExpression="COMMISSIONS ASC" HeaderText="Commissions"
								DataFormatString="{0:c}">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="STATUS" SortExpression="STATUS ASC" HeaderText="Status">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="ORIGIN_POINT" SortExpression="ORIGIN_POINT ASC" HeaderText="Origin point">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="LABEL2" SortExpression="LABEL2 ASC" HeaderText="Warehouse name"></asp:BoundColumn>
							<asp:BoundColumn DataField="DELIVERY_POINT" SortExpression="DELIVERY_POINT ASC" HeaderText="Delivery point">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="LABEL1" SortExpression="LABEL1 ASC" HeaderText="Warehouse name"></asp:BoundColumn>
							<asp:BoundColumn DataField="FOB_TERMS" SortExpression="FOB_TERMS ASC" HeaderText="FOB Terms">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="SHIPMENT_DATE_DELIVERED" SortExpression="SHIPMENT_DATE_DELIVERED ASC"
								HeaderText="Delivery date">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn Visible="False" DataField="SHIPMENT_PO_NUM" SortExpression="SHIPMENT_PO_NUM ASC"
								HeaderText="Buyer PO Num">
								<ItemStyle HorizontalAlign="Center"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="BROKER" SortExpression="BROKER ASC" HeaderText="Broker">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="SELLER_USER" SortExpression="SELLER_USER ASC" HeaderText="Seller user">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="SHIPMENT_COMM" SortExpression="SHIPMENT_COMM ASC" HeaderText="Commissions %">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="ORDR_MELT" SortExpression="ORDR_MELT ASC" HeaderText="Melt">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="ORDR_DENS" SortExpression="ORDR_DENS ASC" HeaderText="Dens">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="ORDR_ADDS" SortExpression="ORDR_ADDS ASC" HeaderText="Adds">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="ORDR_PRIME" SortExpression="ORDR_PRIME ASC" HeaderText="Prime/Offgrade">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="SHIPMENT_COMMENT" SortExpression="SHIPMENT_COMMENT ASC" HeaderText="Shipment comment">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="BUYER_PAID" SortExpression="BUYER_PAID ASC" HeaderText="Buyer paid" DataFormatString="{0:c}">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="SELLER_PAID" SortExpression="SELLER_PAID ASC" HeaderText="Paid to seller"
								DataFormatString="{0:c}">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="GROSS_PROFIT" SortExpression="GROSS_PROFIT ASC" HeaderText="Gross Profit"
								DataFormatString="{0:c}">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="DAYSPASTDUE_RECEIVABLE" SortExpression="DAYSPASTDUE_RECEIVABLE ASC" HeaderText="Days past due (receivable)">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:HyperLinkColumn DataNavigateUrlField="SHIPMENT_BUYR" DataNavigateUrlFormatString="../common/MB_Specs_User.aspx?ID={0}"
								DataTextField="BUYER" SortExpression="BUYER ASC" HeaderText="Buyer">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:HyperLinkColumn>
							<asp:HyperLinkColumn DataNavigateUrlField="SHIPMENT_SELR" DataNavigateUrlFormatString="../common/MB_Specs_User.aspx?ID={0}"
								DataTextField="SELLER" SortExpression="SELLER ASC" HeaderText="Seller">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:HyperLinkColumn>
						</Columns>
					</asp:DataGrid>
					<%}%>
				</asp:panel>
				<asp:panel id="pnNoContent" runat="server" Visible="False">
					<CENTER>There are no transactions recorded in our system
					</CENTER>
				</asp:panel></TD>
		</TR>
		</table>
	</div>
</asp:Content>