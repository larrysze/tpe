<%@ Page language="c#" Codebehind="MB_Specs_Invoice.aspx.cs" AutoEventWireup="True" Inherits="localhost.Common.MB_Specs_Invoice" MasterPageFile="~/MasterPages/Menu.Master" Title="MB_Specs_Invoice" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">


			<asp:panel id="Panel1" runat="server">
				<asp:Label id="Label1" runat="server"></asp:Label>
				<asp:Literal id="Label2" runat="server" Visible="False"></asp:Literal>
				<asp:Literal id="Literal1" runat="server" Visible="False"></asp:Literal>
				<asp:Literal id="showHtml" runat="server"></asp:Literal>
				<TABLE cellSpacing="0" cellPadding="0" align="center" border="0">
					<TR>
						<TD height="1"><A href="javascript:window.print()"><IMG src="/images/icons/print.gif" border="0"></A>&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:Button id="Button2" runat="server" Text="Sent Invoice By Email" onclick="showInterface_Click"></asp:Button>&nbsp;</TD>
					</TR>
				</TABLE>
			</asp:panel><asp:panel id="Panel2" runat="server">
				<TABLE cellSpacing="0" cellPadding="0" align="center" bgColor="#e8e8e8" border="0">
					<TR>
						<TD bgColor="#000000" colSpan="6" height="1"></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 16px" width="1" bgColor="#000000"><IMG src="/images/1x1.gif" width="1"></TD>
						<TD style="HEIGHT: 16px" width="3"><IMG src="/images/1x1.gif" width="1"></TD>
						<TD style="HEIGHT: 16px"><STRONG>From:</STRONG></TD>
						<TD style="HEIGHT: 16px" bgColor="#e8e8e8">
							<asp:DropDownList id="DropDownList1" runat="server">
								<asp:ListItem Value="michael@theplasticsexchange.com">Mike</asp:ListItem>
								<asp:ListItem Value="niraj@theplasticsexchange.com">Niraj</asp:ListItem>
								<asp:ListItem Value="helio@theplasticsexchange.com">Helio</asp:ListItem>
								<asp:ListItem Value="chris@theplasticsexchange.com">Christian</asp:ListItem>
							</asp:DropDownList>
							<asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" Visible="False" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
								ControlToValidate="DropDownList2">Email Error</asp:RegularExpressionValidator></TD>
						<TD style="HEIGHT: 16px" width="3"><IMG src="/images/1x1.gif" width="1"></TD>
						<TD style="HEIGHT: 16px" width="1" bgColor="#000000"><IMG src="/images/1x1.gif" width="1"></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 19px" width="1" bgColor="#000000"><IMG src="/images/1x1.gif" width="1"></TD>
						<TD style="HEIGHT: 19px" width="3"><IMG src="/images/1x1.gif" width="1"></TD>
						<TD style="HEIGHT: 19px"><STRONG>To:</STRONG></TD>
						<TD style="HEIGHT: 19px" bgColor="#e8e8e8">
							<asp:DropDownList id="DropDownList2" runat="server"></asp:DropDownList></TD>
						<TD style="HEIGHT: 19px" width="3"><IMG src="/images/1x1.gif" width="1"></TD>
						<TD style="HEIGHT: 19px" width="1" bgColor="#000000"><IMG src="/images/1x1.gif" width="1"></TD>
					</TR>
					<TR>
						<TD width="1" bgColor="#000000"><IMG src="/images/1x1.gif" width="1"></TD>
						<TD width="3"><IMG src="/images/1x1.gif" width="1"></TD>
						<TD vAlign="top"><STRONG>Comments:</STRONG></TD>
						<TD bgColor="#e8e8e8">
							<asp:TextBox id="TextBox1" runat="server" cols="60" TextMode="MultiLine" Rows="4"></asp:TextBox></TD>
						<TD width="3"><IMG src="/images/1x1.gif" width="1"></TD>
						<TD width="1" bgColor="#000000"><IMG src="/images/1x1.gif" width="1"></TD>
					</TR>
					<TR>
						<TD width="1" bgColor="#000000"><IMG src="/images/1x1.gif" width="1"></TD>
						<TD vAlign="top" align="center" colSpan="4"><BR>
							<asp:Button id="Button1" runat="server" Text="Sent" onclick="sentMail_Click"></asp:Button>&nbsp;
							<asp:Button id="Button3" runat="server" Text="Back" CausesValidation="False" onclick="back_Click"></asp:Button>
							<asp:ValidationSummary id="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary><BR>
						</TD>
						<TD width="1" bgColor="#000000"><IMG src="/images/1x1.gif" width="1"></TD>
					</TR>
					<TR>
						<TD bgColor="#000000" colSpan="6" height="1"></TD>
					</TR>
				</TABLE>
			</asp:panel>
</asp:Content>