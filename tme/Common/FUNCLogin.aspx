<%@ Page Language="c#" CodeBehind="FUNCLogin.aspx.cs" AutoEventWireup="True" Inherits="localhost.Common.FUNCLogin" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master"%>
<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphHeading"><span class="Header Color1 Bold">Not a member?</span></asp:Content>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">

<table width="780" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#333333">
<tr><td>
	<p align="center"><font color="red"><asp:label id="lblPleaseResubmit" runat="server"></asp:label></font></p>
	<asp:panel id="pnlMessage" runat="server" visible="false">
	<br />
<table cellspacing="1" cellpadding="0" width="430" align="center" bgColor="#000000">
  <tr>
    <td class="Content" valign="middle" align="center" width="80" bgColor="#dbdcd7">
<asp:Image id="Image1" runat="server" ImageUrl="../Pics/icons/alert.gif"></asp:Image></td>
    <td class="Content" valign="middle" width="350" bgColor="#dbdcd7">
      <div 
      style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px"><STRONG 
      style="COLOR: #cc6600">
<asp:Label id="lblMessageTitle" runat="server"></asp:Label></STRONG><br />
<asp:Label id="lblMessage" runat="server"></asp:Label></div></td></tr></table>
	</asp:panel>
	<table cellspacing="1" cellpadding="0" width="430" align="center" bgcolor="#000000">
		<tr>
			<td class="Content" valign="top" align ="left" width="255" bgcolor="#999999">
				<div style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px; PADDING-BOTTOM: 5px; PADDING-TOP: 5px"><span class="Header Color4 Bold">Not a Member?</span><br /><br />
					Membership is FREE and gives you access to the latest news research and trader 
					tools. <span class="LinkNormal"><A href="/Public/Registration.aspx?Referrer=<%=Request.Url.ToString()%>" >Joining 
						only takes a minute - become a member today!</A></span>
					<br />
					<br />
					<span class="Header Color4 Bold">Member benefits...</span><br />
					<ul>
					<li>
					Access to exclusive plastic resin offers
					<li>
					Historical pricing information
					<li>
						Regular updates of the resin market delivered right to your inbox
						<br />
						<br />
						<center>
						<span class="LinkNormal"><a href="/Public/Registration.aspx?Referrer=<%=Request.Url.ToString()%>" >
							<IMG src="/images2/register/register.jpg" border="0" width="90">
						</a></span>
						</center>
					</li>
					</ul>
				</div>
			</td>
			<td width="175"  bgColor="#dbdcd7">
					<span class="Header Color3 Bold">Already Signed Up?</span>
					<br /><br />
					<span class="Header Bold Color2">Sign in below:<br /><br /></span>
					<span class="Content Bold Color2">Email:</span><br />
					<asp:textbox CssClass="InputForm" id="txtUserName" runat="server" size="20"></asp:textbox><br />
					<span class="Content Bold Color2">Password:</span><br />
					<asp:textbox CssClass="InputForm" id="txtPassword" runat="server" size="20" TextMode="Password"></asp:textbox><br />
					<br />
					<asp:button CssClass="Content Color2" id="btnLogin" runat="server" Text="Login" onclick="btnLogin_Click"></asp:button><br />

					<br />
					<span class="LinkNormal"><a href="/Public/Forgot_Password.aspx"><B>Forgot your password?</B></a></span>
					<br />
					<br />
				
			</td>
		</tr>
	</table>
	<br />
</td></tr>	
</table>

</asp:Content>	

