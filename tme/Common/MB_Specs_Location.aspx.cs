using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;

namespace localhost.Common
{
    public partial class MB_Specs_Location : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.Width = "760px";
            string SQL = string.Empty;
            SQL = "SELECT  PLAC_TYPE,PLAC_PERS,PLAC_PHON,PLAC_ADDR_ONE,PLAC_ADDR_TWO,PLAC_ZIP,LOCL_CITY,LOCL_STAT,LOCL_CTRY,PLAC_INST,(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=PLAC_COMP) FROM PLACE,LOCALITY WHERE LOCL_ID=PLAC_LOCL AND PLAC_ID=" + Request.QueryString["Id"].ToString();
            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();
                using (SqlDataReader Rec0 = DBLibrary.GetDataReaderFromSelect(conn, SQL))
                {
                    while (Rec0.Read())
                    {
                        if (Rec0[0] != DBNull.Value)
                        {
                            if (Rec0[0] != "")
                            {
                                if (Rec0[0] == "D") lblType.Text = "Delivery point";
                                if (Rec0[0] == "S") lblType.Text = "Shipping point";
                                if (Rec0[0] == "H") lblType.Text = "Headquarter";
                            }
                        }

                        if (Rec0[1] != DBNull.Value)
                        {
                            if (Rec0[1] != "")
                            {
                                lblContact.Text = Rec0[1].ToString();
                            }
                        }

                        if (Rec0[2] != DBNull.Value)
                        {
                            if (Rec0[2] != "")
                            {
                                lblPhone.Text = Rec0[2].ToString();
                            }
                        }

                        if (Rec0[3] != DBNull.Value)
                        {
                            if (Rec0[3] != "")
                            {
                                lblAdress1.Text = Rec0[3].ToString();
                            }
                        }

                        if (Rec0[4] != DBNull.Value)
                        {
                            if (Rec0[4] != "")
                            {
                               lblAdress2.Text = Rec0[4].ToString();
                            }
                        }

                        if (Rec0[5] != DBNull.Value)
                        {
                            if (Rec0[5] != "")
                            {
                                lblZip.Text = Rec0[5].ToString();
                            }
                        }

                        if (Rec0[6] != DBNull.Value)
                        {
                            if (Rec0[6] != "")
                            {
                                lblCity.Text= Rec0[6].ToString();
                            }
                        }

                        if (Rec0[7] != DBNull.Value)
                        {
                            if (Rec0[7] != "")
                            {
                               lblState.Text = Rec0[7].ToString();
                            }
                        }

                        if (Rec0[8] != DBNull.Value)
                        {
                            if (Rec0[8] != "")
                            {
                                lblCountry.Text = Rec0[8].ToString(); 
                            }
                        }

                        if (Rec0[9] != DBNull.Value)
                        {
                            if (Rec0[9] != "")
                            {
                                lblInstructions.Text = Rec0[9].ToString();
                            }
                        }
    

                    }
                }
            }

        }
    }
}
