using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.Mail;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;

namespace localhost.Common
{
	/// <summary>
	/// Summary description for Filled_Orders.
	/// </summary>
	public class FreightCompany : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid contacted;
		protected System.Web.UI.WebControls.DataGrid uncontacted;
		protected System.Web.UI.WebControls.Panel pnSizeNotFound;
		protected System.Web.UI.WebControls.Panel pnContacted;
		protected System.Web.UI.WebControls.Panel pnUnContacted;
		protected System.Web.UI.WebControls.Button btnSendMail;
		protected System.Web.UI.WebControls.Panel pnEmailSent;

		//protected string DBConn = Application["DBConn"].ToString();	
	
		protected string LOCL_CITY0 = "";
		protected string LOCL_CITY1 = "";
		protected string LOCL_STATE0 = "";
		protected string LOCL_STATE1 = "";
		protected string PLAC_PERS0 = "";
		protected string PLAC_PERS1 = "";
		protected string PLAC_ADDR_ONE0 = "";
		protected string PLAC_ZIP0 = "";
		protected string PLAC_ADDR_ONE1 = "";
		protected string PLAC_ZIP1 = "";

		protected string Shipment_Id = "";
		protected string[] arrID;
		protected string Shipment_Ordr_Id = "";
		protected string Shipment_Sku = "";
		protected string Shipment_From = "";
		protected string Shipment_To = "";

		protected string PLAC_LOCL0 = "";
		protected string PLAC_LOCL1 = "";

		/*******************************************************************************/
		//								Mail creation function
		/*******************************************************************************/

		private int Send_mail(string name_to,string email_to)
		{	
			try 
			{				
				//string smtpemail = "localhost"; //ConfigurationSettings.AppSettings["smtpemail"];
				//string smtpemail = "192.168.0.10";	  //using jefferson smtp server
				string strsubject = "RFQ from " + LOCL_CITY0 + ", " + LOCL_STATE0 + " to " + LOCL_CITY1 + ", " + LOCL_STATE1;

				MailMessage maildef=new MailMessage();
				maildef.From = "<helio@theplasticsexchange.com>Helio Pimentel";
				maildef.To= email_to;
				maildef.Subject = strsubject;

				// message body definition
				maildef.Body = "<HTML><head><title>The Plastics Exchange</title><link type='text/css' rel='stylesheet' href='http://wifi.tiscali.fr/css/ie.css'></head><body topmargin='5' leftmargin='5' marginheight='5' marginwidth='5'>"           
					+ "Hello " + name_to + ",<br><br>"
					+ "Can you quote the cheapest way possible for this truckload boxes,<br>" 
					+ "Pick up at:<br>"
					+ PLAC_ADDR_ONE0 + "<br>"
					+ LOCL_CITY0 + " - " + PLAC_ZIP0 + ", " + LOCL_STATE0
					+ "<br><br>"
					+ "Deliver at:<br>"
					+ PLAC_ADDR_ONE1 + "<br>"
					+ LOCL_CITY1 + " - " + PLAC_ZIP1 + ", " + LOCL_STATE1
					+ "<br><br>"
					+ "Please let me know when can you move this load, I need one load ASAP.<br><br>"
					+ "Thank you very much.<br><br>"
					+ "Sincerely,<br><br><br>"
					+ "Helio Pimentel<br>"
					+ "<a href='https://" + ConfigurationSettings.AppSettings["SecureDomainName"].ToString() + "' >The Plastics Exchange</a> <br>"
					+ "Tel: 312.202.0002<br><br><br>"
					+ "</body>" 
					+ "</html>";

				maildef.BodyFormat = MailFormat.Html;
				//SmtpMail.SmtpServer = smtpemail;  
				TPE.Utility.EmailLibrary.Send(Context,maildef);

				return(1);
			} 

			catch(Exception exMail) 
			{ 
				//Response.Write("ERROR SEND MAIL "+ exMail.Message);
				return(-1);
			}        
		}




		/*******************************************************************************/
		//								Shipment_Info_Collect function
		/*******************************************************************************/

		private void Shipment_Info_Collect()
		{	
			try 
			{				
				arrID = Shipment_Id.Split('-');
				Shipment_Ordr_Id = arrID[0].ToString();
				Shipment_Sku = arrID[1].ToString();

				SqlConnection conn;
				SqlCommand cmdSQL;
				SqlDataReader dtrSQL;
				string SQL = "Select SHIPMENT_FROM,SHIPMENT_TO,SHIPMENT_SIZE FROM SHIPMENT WHERE SHIPMENT_ORDR_ID = '" + Shipment_Ordr_Id + "' AND SHIPMENT_SKU = '" + Shipment_Sku + "'";
				
				//conn = new SqlConnection(Application["DBConn"].ToString());
				conn = new SqlConnection(Application["DBConn"].ToString());
				conn.Open();

				cmdSQL = new SqlCommand(SQL,conn);
				dtrSQL = cmdSQL.ExecuteReader();
				
				if (dtrSQL.Read())
				{  
					Shipment_From = dtrSQL["SHIPMENT_FROM"].ToString();
					Shipment_To = dtrSQL["SHIPMENT_TO"].ToString();
					//Response.Write("Shipment_Id found!!!<br><br>");
					//Response.Write(Shipment_From+"<br>"+Shipment_To+"<br>end");
				}
				else
				{
					//Response.Write("Wrong Shipment_Id, try again...<br><br>");
				}
				conn.Close();
			} 

			catch(Exception SQLShipmentError) 
			{ 
				//Response.Write("SQL SHIPMENT ERROR! "+ SQLShipmentError.Message + "<br>");
			}        
		}

		/*******************************************************************************/
		//								Place_Info_Collect function
		/*******************************************************************************/

		private void Place_Info_Collect()
		{	
			try 
			{				
				SqlConnection conn0,conn1;
				SqlCommand cmdSQL0,cmdSQL1;
				SqlDataReader dtrSQL0,dtrSQL1;
				string SQL0 = "Select PLAC_PERS,PLAC_ADDR_ONE,PLAC_ZIP,PLAC_LOCL FROM PLACE WHERE PLAC_ID = '" + Shipment_From + "'";
				string SQL1 = "Select PLAC_PERS,PLAC_ADDR_ONE,PLAC_ZIP,PLAC_LOCL FROM PLACE WHERE PLAC_ID = '" + Shipment_To + "'";
				
				//FROM
				conn0 = new SqlConnection(Application["DBConn"].ToString());
				conn0.Open();

				cmdSQL0 = new SqlCommand(SQL0,conn0);
				dtrSQL0 = cmdSQL0.ExecuteReader();
				
				if (dtrSQL0.Read())
				{  
					//Response.Write("Place Info <FROM> found!!!<br><br>");
					PLAC_PERS0 = dtrSQL0["PLAC_PERS"].ToString();
					PLAC_ADDR_ONE0 = dtrSQL0["PLAC_ADDR_ONE"].ToString();
					PLAC_ZIP0 = dtrSQL0["PLAC_ZIP"].ToString();
					PLAC_LOCL0 = dtrSQL0["PLAC_LOCL"].ToString();
				}
				else
				{
					//Response.Write("No attibuted place <FROM>, try again...<br><br>");
				}

				//TO
				conn1 = new SqlConnection(Application["DBConn"].ToString());
				conn1.Open();

				cmdSQL1 = new SqlCommand(SQL1,conn1);
				dtrSQL1 = cmdSQL1.ExecuteReader();
				
				if (dtrSQL1.Read())
				{  
					//Response.Write("Place Info <TO> found!!!<br><br>");
					PLAC_PERS1 = dtrSQL1["PLAC_PERS"].ToString();
					PLAC_ADDR_ONE1 = dtrSQL1["PLAC_ADDR_ONE"].ToString();
					PLAC_ZIP1 = dtrSQL1["PLAC_ZIP"].ToString();
					PLAC_LOCL1 = dtrSQL1["PLAC_LOCL"].ToString();
				}
				else
				{
					//Response.Write("No attibuted place <TO>, try again...<br><br>");
				}
				conn0.Close();
				conn1.Close();
				//Response.Write(PLAC_ADDR_ONE0+"<br>"+PLAC_ADDR_ONE1+"<br>end");
			} 

			catch(Exception SQLPlaceError) 
			{ 
				//Response.Write("SQL PLACE ERROR! "+ SQLPlaceError.Message + "<br>");
			}    
		}




		/*******************************************************************************/
		//								Place_Locality_Collect function
		/*******************************************************************************/


		private void Locality_Info_Collect()
		{	
			try 
			{				
				SqlConnection conn0,conn1;
				SqlCommand cmdSQL0,cmdSQL1;
				SqlDataReader dtrSQL0,dtrSQL1;
				string SQL0 = "Select LOCL_CITY,LOCL_STAT FROM LOCALITY WHERE LOCL_ID = '" + PLAC_LOCL0 + "'";
				string SQL1 = "Select LOCL_CITY,LOCL_STAT FROM LOCALITY WHERE LOCL_ID = '" + PLAC_LOCL1 + "'";
				
				//FROM
				conn0 = new SqlConnection(Application["DBConn"].ToString());
				conn0.Open();

				cmdSQL0 = new SqlCommand(SQL0,conn0);
				dtrSQL0 = cmdSQL0.ExecuteReader();
				
				if (dtrSQL0.Read())
				{  
					//Response.Write("Locality Info <FROM> found!!!<br><br>");
					LOCL_CITY0 = dtrSQL0["LOCL_CITY"].ToString();
					LOCL_STATE0 = dtrSQL0["LOCL_STAT"].ToString();
				}
				else
				{
					//Response.Write("No attibuted Locality <FROM>, try again...<br><br>");
				}
				
				//TO
				conn1 = new SqlConnection(Application["DBConn"].ToString());
				conn1.Open();

				cmdSQL1 = new SqlCommand(SQL1,conn1);
				dtrSQL1 = cmdSQL1.ExecuteReader();

				if (dtrSQL1.Read())
				{  
					//Response.Write("Locality Info <TO> found!!!<br><br>");
					LOCL_CITY1 = dtrSQL1["LOCL_CITY"].ToString();
					LOCL_STATE1 = dtrSQL1["LOCL_STAT"].ToString();
				}
				else
				{
					//Response.Write("No attibuted Locality <TO>, try again...<br><br>");
				}
				conn0.Close();
				conn1.Close();
			} 

			catch(Exception SQLLocalityError) 
			{ 
				//Response.Write("SQL LOCALITY ERROR! "+ SQLLocalityError.Message + "<br>");
			}        
		}




		/*******************************************************************************/
		//								getValues from database function
		/*******************************************************************************/

		public void getValues()
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			SqlCommand cmdship;
			SqlDataReader dtrship;
			string splits = "-";
			char[] splitc =  splits.ToCharArray();
			string [] splitTab = Request.QueryString["ID"].ToString().Split(splitc);
			//Response.Write("SELECT SHIPMENT_SIZE FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='" + splitTab[0].ToString()+ "'");
			cmdship= new SqlCommand("SELECT SHIPMENT_SIZE FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='" + splitTab[0].ToString()+"'", conn);
			dtrship= cmdship.ExecuteReader();
			while(dtrship.Read() )
			{
				string search = "";
				string searchUnContacted ="";
				int foundSize = 0;
				if(dtrship["SHIPMENT_SIZE"].ToString()== "42000" )
				{
					search = "WHERE BOXES='1' AND EMAIL <>'' ";
					searchUnContacted = "WHERE BOXES='1' AND (EMAIL ='' OR EMAIL IS NULL)";
					foundSize = 1;
				}
				else if(dtrship["SHIPMENT_SIZE"].ToString()== "45000" )
				{
					search = "WHERE BULKS='1' AND EMAIL <>''";
					searchUnContacted = "WHERE BULKS='1' AND (EMAIL ='' OR EMAIL IS NULL)";
					foundSize = 1;
				}
				else if(dtrship["SHIPMENT_SIZE"].ToString()== "44092" )
				{
					search = "WHERE BAGS='1' AND EMAIL <>''";
					searchUnContacted = "WHERE BAGS='1' AND (EMAIL ='' OR EMAIL IS NULL)";
					foundSize = 1;
				}
				else
				{
					searchUnContacted = search = " WHERE BAGS='5'";//impossible case. when shipment sipze doesn't correspond. No results from the sql request will be given.
					foundSize = 0;
					//searchUnContacted = "WHERE BAGS<>'1' AND BOXES <> '1' AND BULKS <>'1'"; 
				}
				//Response.Write(searchUnContacted.ToString());
				
				if(Request.QueryString["send"]=="1")
				{// if button "send emails" has been clicked
					SqlConnection conn2;
					conn2 = new SqlConnection(Application["DBConn"].ToString());
					conn2.Open();
					SqlCommand cmdCompanies;
					SqlDataReader dtrCompanies;
					cmdCompanies= new SqlCommand("SELECT CONTACT_NAME,EMAIL,COMP_NAME FROM FREIGHT_COMPANY "+search, conn2);
					dtrCompanies= cmdCompanies.ExecuteReader();
					while(dtrCompanies.Read() )
					{
						send_email_step1(dtrCompanies["CONTACT_NAME"].ToString(),dtrCompanies["EMAIL"].ToString());
					}
					//set panels invisible
					pnSizeNotFound.Visible=false;
					pnContacted.Visible=false;
					pnUnContacted.Visible=false;
					pnEmailSent.Visible=true;
					conn2.Close();
				}
				
				// bind the contacted company's list
				SqlConnection conn3 = new SqlConnection(Application["DBConn"].ToString());
				conn3.Open();
				SqlDataAdapter dadContent = new SqlDataAdapter("SELECT CONTACT_NAME,EMAIL,COMP_NAME,PHONE,FAX,ADRESSE,CITY,STATE,ZIP FROM FREIGHT_COMPANY "+search,conn3);
				DataSet dstContent = new DataSet();
				dadContent.Fill(dstContent);
				contacted.DataSource = dstContent;
				contacted.DataBind();
				conn3.Close();
				// bind the uncontacted company's list
				if(foundSize == 0)
				{
					pnSizeNotFound.Visible=true;
					pnUnContacted.Visible=false;
					pnContacted.Visible=false;
				}
				SqlConnection conn4 = new SqlConnection(Application["DBConn"].ToString());
				conn4.Open();
				SqlDataAdapter dadContent4 = new SqlDataAdapter("SELECT CONTACT_NAME,EMAIL,COMP_NAME,PHONE,FAX,ADRESSE,CITY,STATE,ZIP FROM FREIGHT_COMPANY "+searchUnContacted,conn4);
				DataSet dstContent4 = new DataSet();
				dadContent4.Fill(dstContent4);
				uncontacted.DataSource = dstContent4;
				uncontacted.DataBind();
				conn4.Close();
			}	  
		}
		protected void Click_SendEmail(object sender,EventArgs e)
		{
			Response.Redirect("/common/FreightCompany.aspx?ID="+Request.QueryString["ID"].ToString()+"&send=1");
		}

		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}

		private void send_email_step1(string contact, string email)
		{
			int etat = Send_mail(contact,email);	
			//int etat = Send_mail("Matthieu LALANNE","matthieu@theplasticsexchange.com");	
		}


		private void Page_Load(object sender, System.EventArgs e)
		{

			Shipment_Id = Request.QueryString["ID"];
			//Response.Write("shipment id = " +Shipment_Id);
			Shipment_Info_Collect();
			Place_Info_Collect();
			Locality_Info_Collect();
			getValues();
		}
	}
}
