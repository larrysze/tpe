//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.42
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace localhost.Common {
    
    public partial class MB_Specs_Location {
        protected System.Web.UI.WebControls.Label lblType;
        protected System.Web.UI.WebControls.Label lblContact;
        protected System.Web.UI.WebControls.Label lblPhone;
        protected System.Web.UI.WebControls.Label lblAdress1;
        protected System.Web.UI.WebControls.Label lblAdress2;
        protected System.Web.UI.WebControls.Label lblZip;
        protected System.Web.UI.WebControls.Label lblCity;
        protected System.Web.UI.WebControls.Label lblState;
        protected System.Web.UI.WebControls.Label lblCountry;
        protected System.Web.UI.WebControls.Label lblInstructions;
        public new localhost.MasterPages.Menu Master {
            get {
                return ((localhost.MasterPages.Menu)(base.Master));
            }
        }
    }
}
