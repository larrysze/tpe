using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Text;
using TPE.Utility;

namespace localhost.Common
{
	/// <summary>
	/// Summary description for Order_Documents.
	/// </summary>
	public partial class Order_Documents : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.HyperLink Hyperlink1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl FileDetails;
		protected System.Web.UI.HtmlControls.HtmlGenericControl FileName;
		protected System.Web.UI.HtmlControls.HtmlGenericControl MyContentType;
		protected System.Web.UI.HtmlControls.HtmlGenericControl ContentLength;
		protected System.Web.UI.WebControls.HyperLink del;


		//protected void Click_Upload(object sender, System.EventArgs e)
		

		
		public void Bind()
		{   	
			string strSQL="";
			strSQL="SELECT CAST(FILE_ID AS VARCHAR)AS ID,FILE_NAME,FILE_DESCRIPT,(SELECT PERS_FRST_NAME FROM PERSON WHERE PERS_ID=FILE_OWNER) AS FILE_OWNER,FILE_DATE,(CASE WHEN  FILE_BUYER='1' THEN '<B>X </B>' END) AS FILE_BUYER,(CASE WHEN FILE_SELLER='1' THEN '<B>X </B>' END) AS FILE_SELLER FROM UPLOADED_FILES WHERE FILE_SHIPMENT=@ShipmentID";
			Hashtable htParams = new Hashtable();
			htParams.Add("@ShipmentID",ViewState["SHIPMENT_ID"].ToString());
			DBLibrary.BindDataGrid(Application["DBConn"].ToString(),dg,strSQL,htParams);
			
			if (dg.Items.Count==0)
				dg.Visible=false;
			else
				dg.Visible=true;		
		}


		public void File_Delete(object sender, DataGridCommandEventArgs e)
		{	
			ViewState["To_Delete"] = e.Item.Cells[8].Text;
			pnlDelete.Visible = true;  
		}

		public void File_Delete_Yes(object sender, System.EventArgs e)
		{
			string SQL="";
			SQL = "Delete From UPLOADED_FILES WHERE FILE_ID=@FileID";
			Hashtable htParam = new Hashtable();
			htParam.Add("@FileID",ViewState["To_Delete"]);
			DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), SQL, htParam);
			Bind();
			pnlDelete.Visible = false;
		}

		public void File_Delete_No(object sender, System.EventArgs e)
		{
			pnlDelete.Visible = false;
			ViewState["To_Delete"] = null;
		}

		
		public void Download_File(object sender, DataGridCommandEventArgs e)
		{
			using (SqlConnection myConnection = new SqlConnection(Application["DBConn"].ToString()))
			{
				myConnection.Open();
				String strSQL = "select * from UPLOADED_FILES WHERE FILE_ID=@FileID"; //'"+e.Item.Cells[8].Text+"'";
				Hashtable htParam = new Hashtable();
				htParam.Add("@FileID",e.Item.Cells[8].Text);
				using (SqlDataReader myDataReader = DBLibrary.GetDataReaderFromSelect(myConnection,strSQL,htParam))
				{
               
					myDataReader.Read();
					Response.ContentType = myDataReader["FILE_TYPE"].ToString();
					Response.AppendHeader("Content-Disposition", "attachment;filename=" + myDataReader["FILE_NAME"].ToString());	
				
					Response.BinaryWrite((byte [])myDataReader["FILE_DATA"]);
					Response.End();
				}
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//must logged in to access this page
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "L"))
			{
				Response.Redirect("/default.aspx");
			}
			
			lblPageHeader.Text ="Documents for Order Number #" + Request.QueryString["ID"].ToString();
			
			//Get the order ID from previous Filled Order page
			string[] arrID;
			Regex r = new Regex("-"); // Split on dashes.
		

				
		    arrID = r.Split(Request.QueryString["ID"].ToString());
			String strSQL="Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID =@SHIPMENT_ORDR_ID and SHIPMENT_SKU =@SHIPMENT_SKU";
			Hashtable htParam = new Hashtable();
			htParam.Add("@SHIPMENT_ORDR_ID",arrID[0]);			
			htParam.Add("@SHIPMENT_SKU",arrID[1]);	
			ViewState["SHIPMENT_ID"]= DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(),strSQL,htParam).ToString();
		
										// retrieve buyer and seller information
			StringBuilder sbSQL = new StringBuilder();
			sbSQL.Append("SELECT VARSELR=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)),");
			sbSQL.Append("	VARBUYR=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR))");
			sbSQL.Append("  FROM ORDERS O, SHIPMENT S WHERE O.ORDR_ID=S.SHIPMENT_ORDR_ID and S.SHIPMENT_ID=@ShipmentID");
			htParam.Clear();                                      

			htParam.Add("@ShipmentID",ViewState["SHIPMENT_ID"].ToString());

			using (SqlConnection conn=new SqlConnection(Application["DBConn"].ToString()))
			{			
				using (SqlDataReader dtrUsers = DBLibrary.GetDataReaderFromSelect(conn,sbSQL.ToString(),htParam))
				{						
					dtrUsers.Read();
					lblBuyer.Text = dtrUsers["VARBUYR"].ToString() +" (Buyer)";
					lblSeller.Text = dtrUsers["VARSELR"].ToString() +" (Seller)";
				}
			}					


			if (!IsPostBack)
			{
				Bind();
			}			
		}

		// <summary>
		// OndataItemBound Method
		// </summary>

		protected void OnDataBound(object sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)

			{
				
				// retrieving the file extension
				
				
					   string strExtension = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "FILE_NAME"));
				try
				{
					strExtension = strExtension.Substring(strExtension.LastIndexOf("."),strExtension.Length-strExtension.LastIndexOf("."));
					// adding picture to match the file extension
					switch (strExtension)
					{
						case ".doc":
							e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_doc.gif\">";
							break;
						case ".xls":
							e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_xls.gif\">";
							break;
						case ".pdf":
							e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_pdf.gif\">";
							break;
						case ".mdb":
							e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_mdb.gif\">";
							break;
						case ".htm":
							e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_htm.gif\">";
							break;
						case ".html":
							e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_htm.gif\">";
							break;
						default:
							e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_other.gif\">";
							break;

					}
				}
				catch
				{
					e.Item.Cells[1].Text ="<img src=\"/pics/icons/icon_other.gif\">";
				}
				
				
			}


	}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void dg_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		protected void btnReturn_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("/common/Filled_Orders.aspx");
		}

		protected void btnUpload_Click(object sender, System.EventArgs e)
		{
			int intImageSize;
			string strImageType;
			Stream ImageStream;
			string Path_FName;

			if (File_to_add.PostedFile.FileName.Length > 0)
			{
				intImageSize = File_to_add.PostedFile.ContentLength;
				strImageType = File_to_add.PostedFile.ContentType;
				Path_FName = File_to_add.PostedFile.FileName;

				string [] strFName;
				char [] r = {'\\'};
				strFName=Path_FName.Split(r);
				int strFName_lengh = strFName.Length;
				string FName = strFName[strFName_lengh-1];	                 
       
				// Reads the Image
				ImageStream = File_to_add.PostedFile.InputStream;
 
				byte [] ImageContent = new byte [intImageSize];
				int intStatus;
				intStatus = ImageStream.Read(ImageContent, 0, intImageSize);
 
				//Create Instance of Connection and Command 
				using (SqlConnection myConnection = new  SqlConnection(Application["DBConn"].ToString()))
				{
					//SqlCommand myCommand = new SqlCommand("Insert into  File_Person (PersonEmail, PersonName, PersonSex, PersonDOB, PersonImage, PersonImageType) Values (@PersonEmail, @PersonName, @PersonSex, @PersonDOB, @PersonImage, @PersonImageType)", myConnection);


					SqlCommand myCommand = new SqlCommand("INSERT INTO UPLOADED_FILES (FILE_SHIPMENT,FILE_NAME,FILE_TYPE,FILE_DATA,FILE_LENGTH,FILE_OWNER,FILE_DESCRIPT,FILE_DATE,FILE_BUYER,FILE_SELLER) VALUES (@FILE_SHIPMENT,@FILE_NAME,@FILE_TYPE,@FILE_DATA,@FILE_LENGTH,@FILE_OWNER,@FILE_DESCRIPT,@FILE_DATE,@FILE_BUYER,@FILE_SELLER)", myConnection );
					// NOTE: can't use DBLibrary function since it can't handle binary parms (like ImageContent)
			
					SqlParameter prmName = new SqlParameter("@FILE_NAME", SqlDbType.VarChar, 255);
					prmName.Value = DBLibrary.ScrubSQLStringInput(FName);
					myCommand.Parameters.Add(prmName);
				
					SqlParameter prmDescript = new SqlParameter("@FILE_DESCRIPT", SqlDbType.VarChar, 255);
					prmDescript.Value = DBLibrary.ScrubSQLStringInput(Description.Text);
					myCommand.Parameters.Add(prmDescript);
				
					SqlParameter prmOwner = new SqlParameter("@FILE_OWNER", SqlDbType.Int, 4);
					prmOwner.Value = Convert.ToInt32(Session["ID"]);
					myCommand.Parameters.Add(prmOwner);

					SqlParameter prmShipment = new SqlParameter("@FILE_SHIPMENT", SqlDbType.Int, 4);
					prmShipment.Value = Convert.ToInt32(ViewState["SHIPMENT_ID"]);
					myCommand.Parameters.Add(prmShipment);

					SqlParameter prmLength = new SqlParameter("@FILE_LENGTH", SqlDbType.Int, 9);
					prmLength.Value = intImageSize;
					myCommand.Parameters.Add(prmLength);

					SqlParameter prmBuyer = new SqlParameter("@FILE_BUYER", SqlDbType.VarChar, 50);
					if(CheckBuyer.Checked == true)
						prmBuyer.Value = "1";
					else
						prmBuyer.Value = "0";

					SqlParameter prmSeller = new SqlParameter("@FILE_SELLER", SqlDbType.VarChar, 50);
					if(CheckSeller.Checked == true)
						prmSeller.Value = "1";
					else
						prmSeller.Value = "0";
				
					myCommand.Parameters.Add(prmBuyer);
					myCommand.Parameters.Add(prmSeller);

					SqlParameter prmDate = new SqlParameter("@FILE_DATE", SqlDbType.DateTime);
					prmDate.Value = DateTime.Now;
					myCommand.Parameters.Add(prmDate);
 
					SqlParameter prmImage = new SqlParameter("@FILE_DATA", SqlDbType.Image);
					prmImage.Value = ImageContent;
					myCommand.Parameters.Add(prmImage);
 
					SqlParameter prmImageType = new SqlParameter("@FILE_TYPE", SqlDbType.VarChar, 255);
					prmImageType.Value = strImageType;
					myCommand.Parameters.Add(prmImageType);
 
					myConnection.Open();
					myCommand.ExecuteNonQuery();
				}
				Description.Text="";
				Bind();
			}
		}
	}
}
