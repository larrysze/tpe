
<%@ Page language="c#" Codebehind="Order_Documents.aspx.cs" AutoEventWireup="True" Inherits="localhost.Common.Order_Documents" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" Title="Filled Orders"%>
<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master"%>
<asp:Content runat="server" ContentPlaceHolderID="cphHeading"><span class="Header Color1 Bold">Order Documents</span></asp:Content>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
	<table width="780px" border="0" align="center" class="Content">
		<tr>
			<td class="ListHeadlineBold">
				<asp:Label id="lblPageHeader" runat="server" />
			</td>
		</tr>
		<tr>
			<td><br>
			</td>
		</tr>
		<asp:panel id="pnlDelete" Runat="Server" Visible="False">
			<TR>
				<TD align="center"><FONT color="red">Are you sure you want to delete this file?</FONT><BR>
				</TD>
			</TR>
			<TR>
				<TD align="center">
					<asp:button id="btnYes" onclick="File_Delete_Yes" runat="server" Text="Yes"></asp:button>
					<asp:button id="btnNo" onclick="File_Delete_No" runat="server" Text="No"></asp:button></TD>
			</TR>
		</asp:panel>
		<tr>
			<td align="center">
				<asp:DataGrid CellSpacing="1" BorderWidth="0" CellPadding="2" BackColor="#000000" runat="server" width="700" Id="dg" AutoGenerateColumns="false" OnEditCommand="Download_File"
					 ItemStyle-CssClass="LinkNormal DarkGray" AlternatingItemStyle-CssClass="LinkNormal LightGray"
					HeaderStyle-CssClass="LinkNormal Color2 Bold OrangeColor" OnDeleteCommand="File_Delete" OnItemDataBound="OnDataBound" onselectedindexchanged="dg_SelectedIndexChanged">
					<Columns>
						<asp:buttoncolumn text="Delete" commandname="Delete" />
						<asp:templatecolumn>
							<headertemplate></headertemplate>
							<itemtemplate></itemtemplate>
						</asp:templatecolumn>
						<asp:buttoncolumn text="FILE_NAME" DataTextField="FILE_NAME" HeaderText="File" commandname="Edit" />
						<asp:BoundColumn DataField="FILE_DESCRIPT" HeaderText="Description" SortExpression="FILE_DESCRIPT ASC"
							ItemStyle-HorizontalAlign="left" />
						<asp:BoundColumn DataField="FILE_OWNER" HeaderText="Owner" SortExpression="FILE_OWNER ASC" ItemStyle-HorizontalAlign="left" />
						<asp:BoundColumn DataField="FILE_DATE" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}" />
						<asp:BoundColumn DataField="FILE_BUYER" HeaderText="Buyer" ItemStyle-HorizontalAlign="center" />
						<asp:BoundColumn DataField="FILE_SELLER" HeaderText="Seller" ItemStyle-HorizontalAlign="center" />
						<asp:BoundColumn DataField="ID" Visible="false" HeaderText="File_Id" SortExpression="ID ASC" ItemStyle-HorizontalAlign="right" />
					</Columns>
				</asp:DataGrid>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" align="left" border="0">
					<tr>
						<td colspan="2"><br>
							<br>
						</td>
					</tr>
					<tr>
						<td align="left" class="ListHeadlineBold" colspan="2">Upload File:</td>
					</tr>
					<tr>
						<td align="left" valign="top">
							<table width="100%" border="0">
								<tr>
									<td>File to Add:</td>
									<td>Description:</td>
								</tr>
								<tr>
									<td>
										<Input ID="File_to_add" Type="file" class="InputForm Content Color2" Runat="Server" NAME="MyFile">
									</td>
									<td>
										<asp:textbox id="Description" CssClass="InputForm" runat="server" size="35" />
									</td>
								</tr>
								<tr>
									<td>
										<table cellspacing="0" cellpadding="0" border="0">
											<tr>
												<td>
													Access Rights:
												</td>
											</tr>
											<tr>
												<td><asp:CheckBox id="CheckBuyer" runat="server"></asp:CheckBox><B><asp:Label id="lblBuyer" runat="server" /></B></td>
											</tr>
											<tr>
											</tr>
											<TR>
												<td><asp:CheckBox id="CheckSeller" runat="server"></asp:CheckBox><B><asp:Label id="lblSeller" runat="server" /></B></td>
											</TR>
										</table>
									</td>
									<td valign="bottom" align="center">
										<asp:Button id="btnUpload" CssClass="Content Color2" runat="server" Text="Upload" onclick="btnUpload_Click"></asp:Button>&nbsp;
										<asp:Button id="btnReturn" CssClass="Content Color2" runat="server" Text="Return to Filled Orders" onclick="btnReturn_Click"></asp:Button>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</asp:Content>	
