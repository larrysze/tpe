using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using MetaBuilders.WebControls;
using System.Text.RegularExpressions;
using TPE.Utility;


namespace localhost.Common
{
	/// <summary>
	/// Summary description for Filled_Orders.
	/// </summary>
	public partial class FinancialTransactionSummary : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnlogistics;
		protected System.Web.UI.WebControls.PlaceHolder phActionButton;
		string search;
		protected string SortExpression;
		protected string SDirection;
	
		/*****************************************************************************
		  '*   1. File Name       : Common/Filled_Orders.aspx                          *
		  '*   2. Description     : Filled orders                                      *
		  '*				                                                  *
		  '*   3. Modification Log:                                                    *
		  '*     Ver No.       Date          Author             Modification           *
		  '*   -----------------------------------------------------------------       *
		  '*      1.00       2-25-2004       Zach                Comment               *
		  '*                                                                           *
		  '*****************************************************************************/


		protected void Page_Load(object sender, EventArgs e)
		{
			//must logged in to access this page
			if ((string)Session["Typ"] != "A")
			{
				Response.Redirect("/default.aspx");
			}

			if (!Page.IsPostBack)
			{
				// db connection for a list of companies
				using (SqlConnection conn2 = new SqlConnection(Application["DBConn"].ToString()))
				{
					conn2.Open();
					SqlCommand cmdCompanies;
					string sql = "SELECT * FROM (SELECT COMP_ID,COMP_NAME,COMP_TYPE FROM COMPANY WHERE COMP_REG=1 AND COMP_TYPE IN ('P','S','D'))Q ORDER BY COMP_NAME";
					using (SqlDataReader dtrCompanies = DBLibrary.GetDataReaderFromSelect(conn2,sql))
					{				
						// add the default value(display all companies)
						ddlCompanies.Items.Add(new ListItem ("Show all Companies","*"));
						while (dtrCompanies.Read())
						{
							ddlCompanies.Items.Add(new ListItem (dtrCompanies["COMP_NAME"].ToString(),dtrCompanies["COMP_ID"].ToString()));
						}
					}
				}
				CreateDateList();
								
				search="";
			}

			BindDataGrid();

			// bind datagrid with default sorting
			//restricting the buttons based upon which type of user is looking at it.

			//OP: officer and his company type is P(Purchaser)					
		}

		// <summary>
		//  searches past months to see which months have transactions pending
		// </summary>
		protected void CreateDateList()
		{
			ddlMonth.Items.Clear(); // starts the list out fresh
			DateTime moment = DateTime.Now;
			//start out with today's dates
			int iYY = moment.Year;
			int iMM = moment.Month;
			string strCompSQL; // holds the syntax of the company should it be needed
			if (!ddlCompanies.SelectedItem.Value.Equals("*"))
			{
				strCompSQL =" AND ((SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)="+ddlCompanies.SelectedItem.Value.ToString()+" OR (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)="+ddlCompanies.SelectedItem.Value.ToString()+")";
			}
			else
			{
				strCompSQL ="";
			}
			using (SqlConnection conn2 = new SqlConnection(Application["DBConn"].ToString()))
			{
				conn2.Open();
			
				while (iYY >= 2000)
				{

					string strSql = "SELECT TOP 1 LEFT(CONVERT(varchar,ORDR_DATE,107),3) + ' ' + Right(CONVERT(varchar,ORDR_DATE,107),4) AS Date FROM ORDERS,SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID AND Month(ORDR_DATE) =@Month AND Year(ORDR_DATE) = @Year AND SHIPMENT_BUYR IS NOT NULL" + strCompSQL;
					Hashtable htParams = new Hashtable();
					htParams.Add("@Month",iMM);
					htParams.Add("@Year",iYY);
					using (SqlDataReader dtrMonth = DBLibrary.GetDataReaderFromSelect(conn2,strSql,htParams))
					{
						if (dtrMonth.HasRows)
						{
							dtrMonth.Read();
							ddlMonth.Items.Add(new ListItem (dtrMonth["Date"].ToString(),iMM.ToString() +"-"+iYY.ToString()));
						}
					}
					iMM--;
					if (iMM == 0)
					{
						iMM = 12;
						iYY--;
					}
				}
				ddlMonth.Items.Add(new ListItem ("Show All Dates","null"));
			}
		}
		// <summary>
		//  Wrapper when the ddl event is fired.
		// </summary>

		protected void Click_Search(object sender, EventArgs e)
		{
			char [] sep=new Char[1];
			sep[0]='-';
			String [] aux=txtSearch.Text.ToString().Split(sep);
			search=txtSearch.Text;

			ListItem liMonth = ddlMonth.Items.FindByValue("null");
			ddlMonth.SelectedIndex = -1;
			liMonth.Selected = true;
			ListItem liComp = ddlCompanies.Items.FindByValue("*");
			ddlCompanies.SelectedIndex = -1;
			liComp.Selected = true;
		
			BindDataGrid();
			//search=search+"  S.SHIPMENT_ORDR_ID LIKE '%"+aux[0]+"%' ";
			//search=search+" OR VARNETDUEBUYR LIKE '%"+txtSearch.Text+"%'";
			//search=search+" OR VARNETDUESELR LIKE '%"+txtSearch.Text+"%'";
			//search=search+" OR VAROWESBUYR LIKE '%"+txtSearch.Text+"%'";
			//search=search+" OR VAROWESSELR LIKE '%"+txtSearch.Text+"%'";
			//search=search+" OR VARTVBUYR LIKE '%"+txtSearch.Text+"%'";
			//search=search+" OR VARTVSELR LIKE '%"+txtSearch.Text+"%'";
			//search=search+" OR VARGROSS LIKE '%"+txtSearch.Text+"%'";
			//search=search+" OR VARSPAID LIKE '%"+txtSearch.Text+"%'";
			//search=search+" OR VARPPAID LIKE '%"+txtSearch.Text+"%'";
			//search=search+" OR VARBUYR LIKE '%"+txtSearch.Text+"%'";
			//search=search+" OR VARSELR LIKE '%"+txtSearch.Text+"%'";

		}

		protected void Change_Comp(object sender, EventArgs e)
		{
			// date list needs to be rebuild according to the new company
			CreateDateList();
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
			txtSearch.Text="";
			BindDataGrid();
		}
		// <summary>
		//  filters out the month
		// </summary>
		protected void Change_Month(object sender, EventArgs e)
		{
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
			txtSearch.Text="";
			BindDataGrid();
		}

		// <summary>
		//  Primary function that binds the grid
		// </summary>
		protected void BindDataGrid()
		{
			using (SqlConnection conn= new SqlConnection(Application["DBConn"].ToString()))
			{
				string strStoredProcName;
				//conn.Open();
				//SqlDataAdapter dadContent;
				//DataSet dstContent;
				//SqlDataReader dtrAnyTrans;
				//SqlCommand cmdAnyTrans;

				SortExpression = (SortExpression == null) ? "TRANS_NUM" : SortExpression;
				SDirection = (SDirection == null) ? "DESC" : SDirection;

				strStoredProcName = "spFinancial_Transaction_summary";
				Hashtable htParams = new Hashtable();
				htParams.Add("@Sort", SortExpression + " " + SDirection);
				if (!ddlCompanies.SelectedItem.Value.Equals("*"))
				{
					htParams.Add("@Comp",ddlCompanies.SelectedItem.Value.ToString());
				}
				if (!ddlMonth.SelectedItem.Value.Equals("null"))
				{
					// value passed is in MM-YYYY format. needs to broken up into separate parameters
					string[] strDate;
					Regex r = new Regex("-"); // Split on spaces.
					strDate = r.Split(ddlMonth.SelectedItem.Value.ToString()) ;
					htParams.Add("@MM",strDate[0]);
					htParams.Add("@YY", strDate[1]);

				}
				htParams.Add("@SEARCH",search);

				//strSQL +=",@SEARCH='S.SHIPMENT_ORDR_ID='3055'";
				//Response.End();
				DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(),dg,strStoredProcName,htParams);
				
				// start off by making them all visible
				pnContent.Visible = true;
				pnNoContent.Visible = false;
			

				if (dg.Items.Count > 0)
				{
					// make sure the right panels are visible
					pnContent.Visible = true;
					pnNoContent.Visible = false;
				}
				else
				{
					// there are no transactions so we need to display a different panel
					pnContent.Visible = false;
					pnNoContent.Visible = true;				
				}			
			}
			search="";			
		}
		
		// <summary>
		// Sorting function
		// </summary>
		protected void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{
			string[] SortExprs;
			string CurrentSortDirection;
			string NewSortDirection;
			string ColumnToSort;
			string NewSortExpr;


			//make sure the session variables have values
			if (Session["SortExpression"] == null)
			{
				Session["SortExpression"] = "TRANS_NUM";
			}
			if (Session["SDirection"] == null)
			{
				Session["SDirection"] = "DESC";
			}

			//change sort direction if the previous column sorted is the same as the current column sorted
			SortExpression = e.SortExpression.Split(' ')[0].ToString();
			if ( SortExpression.Equals(Session["SortExpression"].ToString()) )
			{
				SDirection = (Session["SDirection"].ToString().StartsWith("ASC")) ? "DESC" : "ASC";
			}
			else
			{
				SDirection = "ASC";
			}

			//set the session variables to new value
			Session["SortExpression"] = SortExpression;
			Session["SDirection"] = SDirection;

			//Set Datagrid
			BindDataGrid();
			
		}

		//double runningSum = 0.0;
		//int iLinkSetCount = 1;

		// <summary>
		// calculates total for the footer
		// adds in the script for the 'action box' functionality
		// </summary>

		protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
			//			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			//			{
			//				runningSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARVALUE"));
			//				string strActionBoxHTML = "";
			//				// get Order id
			//				//Response.Write(e.Item.DataItem.);
			//				string strID = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ORDR_ID"));
			//				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]='<div class=\"menuitems\"><a href=\"/administrator/Transaction_Details.aspx?Market=Spot&Referer=/Common/Filled_Orders.aspx&ID="+strID+"\">Edit # "+strID+"</a></div>'" + ((char)13).ToString();
			//
			//				// remove extra info
			//				//strID = strID.Substring(0,strID.LastIndexOf("-"));
			//				
			//				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<hr>' //Optional Separator" + ((char)13).ToString();
			//				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=1\">Invoice</a></div>'" + ((char)13).ToString();
			//				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=2\">Purchase Order</a></div>'" + ((char)13).ToString();
			//				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<hr>' //Optional Separator" + ((char)13).ToString();
			//				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=3\">Certificate</a></div>'" + ((char)13).ToString();
			//				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=4\">Sale Confirmation</a></div>'" + ((char)13).ToString();
			//				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/hauler/Freight_Company.aspx?ID="+strID+"\">Freight R.F.Q</a></div>'" + ((char)13).ToString();
			//				//strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=6\">Title Transfer</a></div>'" + ((char)13).ToString();
			//				
			//				//if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARWGHT")).ToString() == "190000")
			//				//{
			//					strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/common/Convert_Order.aspx?ID="+strID+"\">Convert to 4 BT</a></div>'" + ((char)13).ToString();
			//				//}
			//				phActionButton.Controls.Add (new LiteralControl(strActionBoxHTML));
			//
			//
			//				// setting the mouseover color
			//				e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");
			//				if (e.Item.ItemType == ListItemType.Item)
			//				{
			//					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee'");
			//				}
			//				else
			//				{
			//					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3'");
			//				}
			//				// add scripts for 'action boxes'
			//				e.Item.Cells[1].Attributes.Add("onmouseover","showmenu(event,linkset["+iLinkSetCount.ToString()+"])");
			//				e.Item.Cells[1].Attributes.Add("onMouseout", "delayhidemenu()");
			//				//e.Item.Cells[7].Attributes.Add("onmouseover","showmenu(event,linkset["+iLinkSetCount.ToString()+"])");
			//				//e.Item.Cells[7].Attributes.Add("onMouseout", "delayhidemenu()");
			//
			//				iLinkSetCount ++;
			//
			//				// determines whether the uploaded docs folder is empty or full
			//				if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VARNUMFILES")) !="0")
			//				{
			//					e.Item.Cells[4].Text ="<a href=\"/common/Order_Documents.aspx?ID="+strID+"\"><img border=\"0\" src=\"/pics/icons/icon_filled_folder.gif\"></a>";
			//				}
			//				else
			//				{
			//					e.Item.Cells[4].Text ="<a href=\"/common/Order_Documents.aspx?ID="+strID+"\"><img border=\"0\" src=\"/pics/icons/icon_folder.gif\"></a>";
			//				}
			//
			//
			//
			//			}
			//			else if (e.Item.ItemType == ListItemType.Footer)
			//			{
			//				e.Item.Cells[1].Text ="<b>Total</b>";
			//				e.Item.Cells[8].Text = "<b> " + String.Format("{0:c}", runningSum)+"</b>";
			//			}


		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
