using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace TPE.Utility
{
    /// <summary>
    /// That class contain methods and function to handle database operation.
    /// </summary>
    public class DBLibrary
    {
        public static void BindDataGrid(string connectionString, DataGrid dtGrid, string sqlSelectStatement)
        {
            BindDataGrid(connectionString, dtGrid, sqlSelectStatement, null);
        }

        public static void BindDataGrid(string connectionString, DataGrid dtGrid, string sqlSelectStatement, Hashtable parameters)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand sqlCommand = new SqlCommand(sqlSelectStatement, conn);

                //Add parameters
                AddParameters(ref sqlCommand, parameters);

                dtGrid.DataSource = sqlCommand.ExecuteReader();
                dtGrid.DataBind();
            }
        }

        public static void BindDataGridWithStoredProcedure(string connectionString, DataGrid dtGrid, string storedProcedureName, Hashtable parameters)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                dtGrid.DataSource = GetDataReaderStoredProcedure(conn, storedProcedureName, parameters);
                dtGrid.DataBind();
            }
        }

        public static void BindDataGridWithStoredProcedure(string connectionString, DataGrid dtGrid, string storedProcedureName)
        {
            BindDataGrid(connectionString, dtGrid, storedProcedureName, null);
        }

        public static void ExecuteSQLStatement(string connectionString, string SqlStatement)
        {
            using (SqlConnection mySqlConnection = new SqlConnection(connectionString))
            {
                mySqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand(SqlStatement, mySqlConnection);
                sqlCommand.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Execute a SQL statement with parameters
        /// </summary>
        /// <param name="httpContext">The page context (From the inside of a page is: this.Context)</param>
        /// <param name="SqlStatement">Query</param>
        /// <param name="parameters">Parameters list. To add a parameter: hashtable.Add("@ParamName", "Value")</param>
        public static void ExecuteSQLStatement(string connectionString, string SqlStatement, Hashtable parameters)
        {
            using (SqlConnection mySqlConnection = new SqlConnection(connectionString))
            {
                mySqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand(SqlStatement, mySqlConnection);

                //Add parameters
                AddParameters(ref sqlCommand, parameters);

                sqlCommand.ExecuteNonQuery();
            }
        }

        public static void ExecuteSqlWithoutScrub(string connectionString, string SqlStatement, Hashtable parameters)
        {
            using (SqlConnection mySqlConnection = new SqlConnection(connectionString))
            {
                mySqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand(SqlStatement, mySqlConnection);

                //Add parameters
                AddBinaryParameters(ref sqlCommand, parameters);

                sqlCommand.ExecuteNonQuery();
            }
        }

        public static object ExecuteScalarSQLStatement(string connectionString, string SqlStatement, Hashtable parameters)
        {
            using (SqlConnection mySqlConnection = new SqlConnection(connectionString))
            {
                mySqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand(SqlStatement, mySqlConnection);

                //Add parameters
                AddParameters(ref sqlCommand, parameters);

                object result = sqlCommand.ExecuteScalar();
                return result;
            }
        }

        public static object ExecuteScalarSQLStatement(string ConnectionString, string SqlStatement)
        {
            return ExecuteScalarSQLStatement(ConnectionString, SqlStatement, null);
        }

        public static DataTable GetDataTableFromSelect(string connectionString, string SelectStatement, Hashtable parameters)
        {
            using (SqlConnection mySqlConnection = new SqlConnection(connectionString))
            {
                mySqlConnection.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter(SelectStatement, mySqlConnection);

                AddParameters(ref sqlDa, parameters);

                DataTable dt = new DataTable();
                sqlDa.Fill(dt);

                return dt;
            }
        }

        public static DataTable GetDataTableFromSelect(string connectionString, string selectStatement)
        {
            return GetDataTableFromSelect(connectionString, selectStatement, null);
        }

        public static DataTable GetDataTableFromStoredProcedure(SqlConnection conn, string storedProcName, Hashtable parameters)
        {
            return ((DataTable)GetDataSetFromStoredProcedure(conn, storedProcName, parameters).Tables[0]);
        }

        public static DataTable GetDataTableFromStoredProcedure(SqlConnection conn, string storedProcName)
        {
            return GetDataTableFromStoredProcedure(conn, storedProcName, null);
        }

        public static SqlDataReader GetDataReaderFromSelect(SqlConnection connection, string SelectStatement)
        {
            return GetDataReaderFromSelect(connection, SelectStatement, null);
        }

        public static SqlParameterCollection ExecuteStoredProcedure(string connectionString, string storedProcedureName)
        {
            return ExecuteStoredProcedure(connectionString, storedProcedureName, null);
        }

        public static bool HasRows(string connectionString, string selectStatement, Hashtable param)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                return GetDataReaderFromSelect(conn, selectStatement, param).HasRows;
            }
        }

        public static bool HasRows(string connectionString, string selectStatement)
        {
            return HasRows(connectionString, selectStatement, null);
        }

        public static SqlDataReader GetDataReaderFromSelect(SqlConnection connection, string SelectStatement, Hashtable parameters)
        {
            if (connection.State == ConnectionState.Closed)
                connection.Open();

            SqlCommand sqlCommand = new SqlCommand(SelectStatement, connection);
            AddParameters(ref sqlCommand, parameters);

            return (SqlDataReader)sqlCommand.ExecuteReader();
        }

        public static SqlParameterCollection ExecuteStoredProcedure(string connectionString, string storedProcedureName, Hashtable parameters)
        {
            using (SqlConnection mySqlConnection = new SqlConnection(connectionString))
            {
                mySqlConnection.Open();
                SqlCommand sqlCommand;
                sqlCommand = new SqlCommand(storedProcedureName, mySqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                AddParameters(ref sqlCommand, parameters);

                sqlCommand.ExecuteNonQuery();
                return sqlCommand.Parameters;
            }
        }

        public static object ExecuteScalarStoredProcedure(string connectionString, string storedProcedureName, Hashtable parameters)
        {
            using (SqlConnection mySqlConnection = new SqlConnection(connectionString))
            {
                mySqlConnection.Open();
                object result;
                SqlCommand sqlCommand = new SqlCommand(storedProcedureName, mySqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                AddParameters(ref sqlCommand, parameters);

                result = sqlCommand.ExecuteScalar();
                return result;
            }
        }

        public static SqlParameterCollection ExecuteStoredProcedureWithoutScrub(string connectionString, string storedProcedureName, Hashtable parameters)
        {
            using (SqlConnection mySqlConnection = new SqlConnection(connectionString))
            {
                mySqlConnection.Open();
                SqlCommand sqlCommand;
                sqlCommand = new SqlCommand(storedProcedureName, mySqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                AddBinaryParameters(ref sqlCommand, parameters);

                sqlCommand.ExecuteNonQuery();
                return sqlCommand.Parameters;
            }
        }

        public static object ExecuteScalarStoredProcedureWithoutScrub(string connectionString, string storedProcedureName, Hashtable parameters)
        {
            using (SqlConnection mySqlConnection = new SqlConnection(connectionString))
            {
                mySqlConnection.Open();
                object result;
                SqlCommand sqlCommand = new SqlCommand(storedProcedureName, mySqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                AddBinaryParameters(ref sqlCommand, parameters);

                result = sqlCommand.ExecuteScalar();
                return result;
            }
        }

        public static object ExecuteScalarStoredProc(string connectionString, string storedProcedureName, Hashtable parameters)
        {
            using (SqlConnection mySqlConnection = new SqlConnection(connectionString))
            {
                mySqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand(storedProcedureName, mySqlConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;

                AddParameters(ref sqlCommand, parameters);

                object result = sqlCommand.ExecuteScalar();
                return result;
            }
        }
        public static DataSet GetDataSetFromStoredProcedure(SqlConnection connection, string storedProcName, Hashtable parameters)
        {
            DataSet dataset = new DataSet();

            if (connection.State == ConnectionState.Closed)
                connection.Open();

            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand sqlCommand = new SqlCommand(storedProcName, connection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            AddParameters(ref sqlCommand, parameters);

            adapter.SelectCommand = sqlCommand;
            adapter.Fill(dataset);
            return dataset;
        }
        public static DataSet GetDataSetFromStoredProcedure(SqlConnection connection, string storedProcName)
        {
            return GetDataSetFromStoredProcedure(connection, storedProcName, null);
        }

        public static DataSet GetDataSetFromSelect(SqlConnection connection, string strSql, Hashtable parameters)
        {
            DataSet dataset = new DataSet();

            if (connection.State == ConnectionState.Closed)
                connection.Open();

            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand sqlCommand = new SqlCommand(strSql, connection);

            AddParameters(ref sqlCommand, parameters);

            adapter.SelectCommand = sqlCommand;
            adapter.Fill(dataset);
            return dataset;
        }

        public static DataSet GetDataSetFromSelect(SqlConnection connection, string strSql)
        {
            return GetDataSetFromSelect(connection, strSql, null);
        }

        public static SqlDataReader GetDataReaderStoredProcedure(SqlConnection connection, string storedProcedureName, Hashtable parameters)
        {
            if (connection.State == ConnectionState.Closed)
                connection.Open();

            SqlCommand sqlCommand = new SqlCommand(storedProcedureName, connection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            AddParameters(ref sqlCommand, parameters);

            return (SqlDataReader)sqlCommand.ExecuteReader();
        }

        public static void BindDropDownList(string connectionString, DropDownList dropDownList, string sqlStatement, string textField, string textValue)
        {
            using (SqlConnection mySqlConnection = new SqlConnection(connectionString))
            {
                mySqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand(sqlStatement, mySqlConnection);
                dropDownList.DataSource = (SqlDataReader)sqlCommand.ExecuteReader(); ;
                dropDownList.DataTextField = textField;
                dropDownList.DataValueField = textValue;
                dropDownList.DataBind();
            }
        }

        public static void BindListBox(string connectionString, ListBox ListBoxName, string sqlStatement, string textField, string textValue)
        {
            using (SqlConnection mySqlConnection = new SqlConnection(connectionString))
            {
                mySqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand(sqlStatement, mySqlConnection);
                ListBoxName.DataSource = (SqlDataReader)sqlCommand.ExecuteReader(); ;
                ListBoxName.DataTextField = textField;
                ListBoxName.DataValueField = textValue;
                ListBoxName.DataBind();
            }
        }

        public static int InsertAndReturnIdentity(string connectionString, string sqlStatement, Hashtable parameters)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand sqlCommand = new SqlCommand(sqlStatement, conn);

                AddParameters(ref sqlCommand, parameters);

                sqlCommand.ExecuteNonQuery();

                SqlCommand cmdBill = new SqlCommand("SELECT @@IDENTITY AS NewID", conn);
                return Int32.Parse(cmdBill.ExecuteScalar().ToString());
            }
        }

        public static int InsertAndReturnIdentity(string connectionString, string sqlStatement)
        {
            return InsertAndReturnIdentity(connectionString, sqlStatement, null);
        }

        public static string ScrubSQLStringInput(string input)
        {
            //Regex expression =
            //    new Regex(";|=|<|>| or | and |select |insert |update |drop |xp_|--|exec |union select");

            //string result =
            //    expression.Replace(input, new
            //    MatchEvaluator(MatchEvaluatorHandler));

            return input;
        }

        public static string MatchEvaluatorHandler(Match match)
        {
            //Replace the matched items with a blank string of 
            //equal length
            return new string(' ', match.Length);
        }

        /// <summary>
        /// only remove obvious sql commands; don't remove semicolon, equals, etc for encrypted passwords
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string ScrubSQLStringPassword(string password)
        {
            //Regex expression =
            //    new Regex(" or | and |select |insert |update |drop |xp_|exec");

            //string result =
            //    expression.Replace(password, new
            //    MatchEvaluator(MatchEvaluatorHandler));

            return password;
        }

        private static void AddParameters(ref SqlCommand cmd, Hashtable parameters)
        {
            // NOTE: special case for parameter "@password": don't scrub for illegal sql since encrypted 
            // form may contain things like '=', ';', etc.
            //
            //Add parameters
            if (parameters != null)
            {
                foreach (string p in parameters.Keys)
                    if (parameters[p] != null)
                    {
                        if (p.ToLower() == "@password")
                            cmd.Parameters.Add(p, ScrubSQLStringPassword(parameters[p].ToString()));	// special scrub for encrypted password							
                        else
                            cmd.Parameters.Add(p, ScrubSQLStringInput(parameters[p].ToString()));
                    }
                    else
                    {
                        cmd.Parameters.Add(p, null);
                    }
            }
        }

        private static void AddBinaryParameters(ref SqlCommand cmd, Hashtable parameters)
        {
            //Add parameters
            if (parameters != null)
            {
                foreach (string p in parameters.Keys)
                    if (parameters[p] != null)
                    {
                        cmd.Parameters.Add(p, parameters[p].ToString());
                    }
                    else
                    {
                        cmd.Parameters.Add(p, null);
                    }
            }
        }

        private static void AddParameters(ref SqlDataAdapter da, Hashtable parameters)
        {
            //Add parameters
            if (parameters != null)
            {
                foreach (string p in parameters.Keys)
                {
                    if (parameters[p] != null)
                    {
                        da.SelectCommand.Parameters.Add(p, ScrubSQLStringInput(parameters[p].ToString()));
                    }
                    else
                    {
                        da.SelectCommand.Parameters.Add(p, null);
                    }
                }
            }
        }
    }
}