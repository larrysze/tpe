using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;

namespace tpeDataShare
{
    public class clsCommon
    {

        public int GetTPEForwardMonth(int sMonth, string sYear)
        {
            Hashtable htParam = new Hashtable();
            htParam.Add("@month",GetMonth(sMonth));
            htParam.Add("@year", sYear);

            string sSQL = "SELECT FWD_ID FROM FWDMONTH WHERE FWD_MNTH=@MONTH AND FWD_YEAR=@year";

            int forwardID = int.Parse(DBLibrary.ExecuteScalarSQLStatement(HttpContext.Current.Application["TPE"].ToString(), sSQL, htParam).ToString());

            return forwardID;
        }


        public int GetForwardMonth(string sMonth, string sYear)
        {
            Hashtable htParam = new Hashtable();
            htParam.Add("@month", sMonth);
            htParam.Add("@year", sYear);

            string sSQL = "SELECT FWD_ID FROM FWDMONTH WHERE FWD_MNTH=@MONTH AND FWD_YEAR=@year";

            int forwardID = int.Parse(DBLibrary.ExecuteScalarSQLStatement(HttpContext.Current.Application["MonoDB"].ToString(), sSQL, htParam).ToString());

            return forwardID;
        }

        public int GetForwardMonth(int sMonth, string sYear)
        {
            Hashtable htParam = new Hashtable();
            htParam.Add("@month", GetMonth(sMonth));
            htParam.Add("@year", sYear);

            string sSQL = "SELECT FWD_ID FROM FWDMONTH WHERE FWD_MNTH=@MONTH AND FWD_YEAR=@year";

            int forwardID = int.Parse(DBLibrary.ExecuteScalarSQLStatement(HttpContext.Current.Application["MonoDB"].ToString(), sSQL, htParam).ToString());

            return forwardID;
        }

        public string GetMonth(int month)
        {
            string sMonthName = "";

            switch (month)
            {
                case 1:
                    sMonthName = "January";
                    break;
                case 2:
                    sMonthName = "February";
                    break;
                case 3:
                    sMonthName = "March";
                    break;
                case 4:
                    sMonthName = "April";
                    break;
                case 5:
                    sMonthName = "May";
                    break;
                case 6:
                    sMonthName = "June";
                    break;
                case 7:
                    sMonthName = "July";
                    break;
                case 8:
                    sMonthName = "August";
                    break;
                case 9:
                    sMonthName = "September";
                    break;
                case 10:
                    sMonthName = "October";
                    break;
                case 11:
                    sMonthName = "November";
                    break;
                case 12:
                    sMonthName = "December";
                    break;

            }

            return sMonthName;


        }

        public string GetSmallMonth(int month)
        {
            string sMonthName = "";

            switch (month)
            {
                case 1:
                    sMonthName = "Jan.";
                    break;
                case 2:
                    sMonthName = "Feb.";
                    break;
                case 3:
                    sMonthName = "Mar.";
                    break;
                case 4:
                    sMonthName = "Apr.";
                    break;
                case 5:
                    sMonthName = "May";
                    break;
                case 6:
                    sMonthName = "June";
                    break;
                case 7:
                    sMonthName = "July";
                    break;
                case 8:
                    sMonthName = "Aug.";
                    break;
                case 9:
                    sMonthName = "Sep.";
                    break;
                case 10:
                    sMonthName = "Oct.";
                    break;
                case 11:
                    sMonthName = "Nov.";
                    break;
                case 12:
                    sMonthName = "Dec.";
                    break;
            }

            return sMonthName;

        }

        public bool IsNumeric(object Expression)
        {
            bool isNum;
            double retNum;
            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public string GetTPEContName(int contID)
        {

            switch (contID)
            {
                case 1:
                    return "HMWPE - Film";
                    break;
                case 2:
                    return "HDPE - Inj";
                    break;
                case 3:
                    return "HDPE - Blow Mold";
                    break;
                case 4:
                    return "LDPE - Film";
                    break;
                case 6:
                    return "LLDPE - Film";
                    break;
                case 8:
                    return "HoPP - Inj";
                    break;
                case 9:
                    return "CoPP - Inj";
                    break;
                case 10:
                    return "GPPS";
                    break;
                case 11:
                    return "HIPS";
                    break;

                default:
                    return "";
                    break;
            }



        }

        public bool CheckWeekend(DateTime CurrDate)
        {
            if (CurrDate.DayOfWeek == DayOfWeek.Saturday || CurrDate.DayOfWeek == DayOfWeek.Sunday)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public DateTime GetLasyDayOfMonth(DateTime CurrDate)
        {
            int DaysInMonth = System.DateTime.DaysInMonth(CurrDate.Date.Year, CurrDate.Date.Month);

            return Convert.ToDateTime(CurrDate.Date.Month + "/" + DaysInMonth + "/" + CurrDate.Date.Year);
        }


    }
}
