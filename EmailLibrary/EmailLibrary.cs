using System;
using System.Web;
using System.Diagnostics;
using System.Web.Mail;

namespace TPE.Utility
{

	/*
	class SendEmailException: Exception
	{
		public SendEmailException(string strUserType): base("SendEmailException," + strUserType)
		{
		}
	}
	*/

	/// <summary>
	/// Summary description for EmailLibrary.
    ///     wrapper class for SmtpMail.  If an exception occurrs, error is logged.
    ///     Note: Email errors are logged to event viewer
    ///		this requires following additions to registry: run regedit then,
    ///		in HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Eventlog\Application
    ///		add Keys: EmailLibrary
	///
	/// </summary>
	public class EmailLibrary
	{		
		public EmailLibrary()
		{
			
		}
		
		public static void Send(HttpContext context, string strFrom, string strTo, string strSubject, string strMessage)
		{
			MailMessage msg = new MailMessage();
			msg.From = strFrom; 
			msg.To = strTo;
			msg.Subject = strSubject;
			msg.Body = strMessage;
			EmailLibrary.Send(context, msg);
		}
		

		public static void Send(HttpContext context, string strFrom, string strTo, string strCc, string strSubject, string strMessage)
		{
			MailMessage msg = new MailMessage();
			msg.From = strFrom; 
			msg.To = strTo;
			msg.Cc = strCc;			
			msg.Subject = strSubject;
			msg.Body = strMessage;
			EmailLibrary.Send(context, msg);
		}

		public static void Send(HttpContext context, string strFrom, string strTo, string strCc, string strBcc, string strSubject, string strMessage)
		{
			MailMessage msg = new MailMessage();
			msg.From = strFrom; 
			msg.To = strTo;
			msg.Cc = strCc;
			msg.Bcc = strBcc;
			msg.Subject = strSubject;
			msg.Body = strMessage;
			EmailLibrary.Send(context, msg);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="strUserType">A=admin,U=user, etc.</param>
		/// <param name="msg"></param>
		public static void Send(HttpContext context, MailMessage msg)
		{
			string strUserType = "";
			
			if (context != null)
				if (context.Session["Typ"] != null)
					strUserType = context.Session["Typ"].ToString();

			try
			{	
				SmtpMail.Send(msg);
			}
			catch (Exception ex)
			{
				//Log error in Event Viewer
				EventLog el = new EventLog();
				el.Log = "Application";
				el.Source = "EmailLibrary";
				el.MachineName = ".";
				string sInnerExceptions = String.Empty;
				while (ex.InnerException != null)
				{
					sInnerExceptions += ex.InnerException.ToString() + "  ";
					ex = ex.InnerException;
				}
				el.WriteEntry(ex.Message + sInnerExceptions,EventLogEntryType.Error);				
				
				if ((strUserType == "A") || (strUserType == "B")) // only show error for admin or broker								
					context.Response.Redirect("/common/error.aspx?EmailError=1");
			}
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="msg"></param>
		public static void Send(MailMessage msg)
		{
			try
			{	
				SmtpMail.Send(msg);
			}
			catch (Exception ex)
			{
				//Log error in Event Viewer
				EventLog el = new EventLog();
				el.Log = "Application";
				el.Source = "EmailLibrary";
				el.MachineName = ".";
				string sInnerExceptions = String.Empty;
				while (ex.InnerException != null)
				{
					sInnerExceptions += ex.InnerException.ToString() + "  ";
					ex = ex.InnerException;
				}
				el.WriteEntry(ex.Message + sInnerExceptions,EventLogEntryType.Error);				
			}
		}

		// can't redirect without context so just log error if email fails
		public static void SendWithoutContext(MailMessage msg)
		{
			try
			{
				SmtpMail.Send(msg);
			}
			catch (Exception ex)
			{
				//Log error in Event Viewer
				EventLog el = new EventLog();
				el.Log = "Application";
				el.Source = "EmailLibrary";
				el.MachineName = ".";
				string sInnerExceptions = String.Empty;
				while (ex.InnerException != null)
				{
					sInnerExceptions += ex.InnerException.ToString() + "  ";
					ex = ex.InnerException;
				}
				el.WriteEntry(ex.Message + sInnerExceptions,EventLogEntryType.Error);				

				//throw new Exception("A problem has occured in EmailLibrary.Send", ex);
			}
		}

		// no context for redirect if in separate worker thread
		//
		public static void SendWithinThread(MailMessage msg)
		{
			try
			{
				SmtpMail.Send(msg);
			}
			catch (Exception ex)
			{
				//Log error in Event Viewer
				EventLog el = new EventLog();
				el.Log = "Application";
				el.Source = "EmailLibrary";
				el.MachineName = ".";
				string sInnerExceptions = String.Empty;
				while (ex.InnerException != null)
				{
					sInnerExceptions += ex.InnerException.ToString() + "  ";
					ex = ex.InnerException;
				}
				el.WriteEntry(ex.Message + sInnerExceptions,EventLogEntryType.Error);				
			}
		}
	}
}
