using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;

namespace localhost.Hauler
{
	/// <summary>
	/// Summary description for Track_Rail_Car.
	/// </summary>
	public partial class BalanceSheetReport : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.HyperLink All;
	
		
		public void Page_Load(object sender, EventArgs e)
		{
            Master.Width = "1200px";
			if ((string)Session["Typ"] != "A")
			{
				 Response.Redirect("../default.aspx");
			}

			if (!IsPostBack)
			{
				lblStartDate.Text = Request.QueryString["StartDate"].ToString();
				lblEndDate.Text = Request.QueryString["EndDate"].ToString();
				//lblAvarageInventoryValuation.Text = Request.QueryString["AvarageInventoryValuation"].ToString();

				Bind();
			}
		}
		private void Bind()
		{
			StringBuilder sbSQL = new StringBuilder();
			SqlDataAdapter dadContent;
			DataSet dstContent;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();

			sbSQL.Append("Exec spBalanceSheetReport");
			sbSQL.Append(" @StartDate='"+ lblStartDate.Text +"'");
			sbSQL.Append(" ,@EndDate='" + lblEndDate.Text + "'");
			//sbSQL.Append(" ,@AvarageValuation=" + lblAvarageInventoryValuation.Text);
			
			dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dgReport.DataSource = dstContent;
			dgReport.DataBind();
			conn.Close();
		}		

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

	}
}
