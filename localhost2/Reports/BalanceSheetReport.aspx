<%@ Page Language="c#" CodeBehind="BalanceSheetReport.aspx.cs" AutoEventWireup="True" Inherits="localhost.Hauler.BalanceSheetReport" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
	<table cellspacing="0" cellpadding="0" border="0" width="780px">
		<tr>
			<td>
				<div class="DivTitleBarMenu"><span class="Header Color1 Bold"><asp:Label id="Label1" runat="server">Balance Sheet Report</asp:Label></span></div><br />
				<asp:Label id="Label2" runat="server" CssClass="Content Color2 Bold">Start Date:</asp:Label>&nbsp;
				<asp:Label id="lblStartDate" runat="server" CssClass="Content Color2">08/01/2005</asp:Label>&nbsp;&nbsp;&nbsp;
				<asp:Label id="Label4" runat="server" CssClass="Content Color2 Bold">End Date:</asp:Label>&nbsp;
				<asp:Label id="lblEndDate" runat="server" CssClass="Content Color2">08/31/2005</asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:Label id="Label6" runat="server" CssClass="Content Color2 Bold" Visible="False">Average Inventory Valuation:</asp:Label>
				<asp:Label id="lblAvarageInventoryValuation" runat="server" CssClass="Content Color2" Visible="False">5</asp:Label>
				<asp:Label id="Label8" runat="server" CssClass="Content Color2" Visible="False">%</asp:Label><br /><br />
			</td>
		</tr>
		<tr>
			<td><asp:datagrid BackColor="#000000" CellSpacing="1" BorderWidth="0" id="dgReport" runat="server" CssClass="DataGrid" AutoGenerateColumns="False" CellPadding="4"
					HorizontalAlign="Left">
					<SelectedItemStyle Wrap="False"></SelectedItemStyle>
					<EditItemStyle Wrap="False"></EditItemStyle>
					<AlternatingItemStyle Wrap="False" CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
					<ItemStyle Wrap="False" CssClass="LinkNormal LightGray"></ItemStyle>
					<HeaderStyle CssClass="LinkNormal Bold OrangeColor" HorizontalAlign="Center"></HeaderStyle>
					<Columns>
						<asp:BoundColumn DataField="ACCOUNT_DATE" HeaderText="Date" DataFormatString="{0:d}">
							<HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
							<ItemStyle CssClass="LinkNormal Bold OrangeColor" Wrap="False" HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="ACCOUNT_TRADING" HeaderText="Trading ACB" DataFormatString="{0:N0}">
							<HeaderStyle Width="80px"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="ACCOUNT_OPERATION" HeaderText="Operating ACB" DataFormatString="{0:N0}">
							<HeaderStyle Width="80px"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="ACCOUNT_COLLATERAL" HeaderText="Collateral ACB" DataFormatString="{0:N0}">
							<HeaderStyle Width="80px"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="ACCOUNT_RECEIVABLE" HeaderText="Accounts Receivable" DataFormatString="{0:N0}">
							<HeaderStyle Width="80px"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="ACCOUNT_COMMITED_INVENTORY" HeaderText="Committed Inventory" DataFormatString="{0:N0}">
							<HeaderStyle Width="80px"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="ACCOUNT_UNCOMMITED_INVENTORY" HeaderText="Uncommitted Inventory" DataFormatString="{0:N0}">
							<HeaderStyle  Width="80px"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="TOTAL_ASSETS" HeaderText="Total Assets" DataFormatString="{0:N0}">
							<HeaderStyle Width="80px"></HeaderStyle>
							<ItemStyle Font-Bold="True" Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="ACCOUNT_TPE_CREDIT" HeaderText="TPE Credit ACB" DataFormatString="{0:N0}">
							<HeaderStyle Width="80px"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="ACCOUNT_MAG_CREDIT" HeaderText="MAG Credit ACB" DataFormatString="{0:N0}">
							<HeaderStyle Width="80px"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="ACCOUNT_PAYABLE" HeaderText="Trade Accounts Payable" DataFormatString="{0:N0}">
							<HeaderStyle Width="80px"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="ACCOUNT_COMMITED_INVENTORY_PAYABLE" HeaderText="Committed Inventory"
							DataFormatString="{0:N0}">
							<HeaderStyle Width="80px"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="ACCOUNT_ALLOWANCE_DOUBTFUL" HeaderText="Allowance Doubtful Accounts"
							DataFormatString="{0:N0}">
							<HeaderStyle Width="80px"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="TOTAL_LIABILITIES" HeaderText="Total Liabilities" DataFormatString="{0:N0}">
							<HeaderStyle Width="80px"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="NETBALANCE" HeaderText="Net Balance" DataFormatString="{0:N0}">
							<HeaderStyle CssClass="LinkNormal Bold OrangeColor" Width="80px"></HeaderStyle>
							<ItemStyle CssClass="LinkNormal Bold OrangeColor" Wrap="False" HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
					</Columns>
				</asp:datagrid>
				<!----></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td align="center"></td>
		</tr>
	</table>

</asp:Content>