using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;

namespace localhost.Hauler
{
	/// <summary>
	/// Summary description for Track_Rail_Car.
	/// </summary>
	public partial class IncomeStatementReport : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label8;
		int line = 1;

		public void Page_Load(object sender, EventArgs e)
		{
			if ((string)Session["Typ"] != "A")
			{
				 Response.Redirect("../default.aspx");
			}

			if (!IsPostBack)
			{
				lblStartDate.Text = Request.QueryString["StartDate"].ToString();
				lblEndDate.Text = Request.QueryString["EndDate"].ToString();
				lblOverhead.Text = Request.QueryString["Overhead"].ToString();
				lblOverheadDisplay.Text = String.Format("{0:##,###}", Convert.ToInt32(lblOverhead.Text));

				Bind();
			}
		}

		private void calculateFooter(int day, int all_days)
		{
			for(int i = 2; i < dgReport.Items[0].Cells.Count; i++)
			{
				double s = 0;
				bool end = false;
				int k = 0;
				while(!end)
				{
					int this_day = 0;
					if(dgReport.Items.Count > k + 1)
					{
						this_day = (Convert.ToDateTime(dgReport.Items[k].Cells[1].Text)).Day;
					}
					else
					{
						end = true;
					}
					if(day <= this_day)
					{
						end = true;
					}
					try
					{
						s += Convert.ToDouble(nullValuesHTML(dgReport.Items[k].Cells[i].Text));
						
					}
					catch(Exception e)
					{
					}
					k++;
				}

				//String.Format("{0:##,###}", Convert.ToInt32(lblOverhead.Text));

				string total = String.Format("{0:#,#0}", s);
				string avarage = String.Format("{0:#,#0}", s / k);
				string estimate = String.Format("{0:#,#0}", (s / k) * all_days);
				string footer = total + "<BR>" + avarage + "<BR>" + estimate;

				try
				{
					/*
					Label lblTmp = new Label();
					lblTmp.Text = footer;
*/
					DataGridItem itm = (DataGridItem)dgReport.Controls[0].Controls[dgReport.Controls[0].Controls.Count-1];
					itm.Cells[i].Text = footer;
//					dgReport.Items[dgReport.Items.Count - 1].Controls.AddAt(0, lblTmp);
					dgReport.ShowFooter = true;
//					dgReport.Columns[i].FooterText = footer;
				}
				catch(Exception er)
				{
				}
				
			}

		}

		private void Bind()
		{
			StringBuilder sbSQL = new StringBuilder();
			SqlDataAdapter dadContent;
			DataSet dstContent;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();

			sbSQL.Append("Exec spIncomeStatementReport");
			sbSQL.Append(" @StartDate='"+ lblStartDate.Text +"'");
			sbSQL.Append(" ,@EndDate='" + lblEndDate.Text + "'");
			sbSQL.Append(" ,@Overhead='" + lblOverhead.Text +"'");
			
			dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dgReport.DataSource = dstContent;
			dgReport.DataBind();
			conn.Close();

			// add 3 footers
			int day = Convert.ToInt32(Request.QueryString["day"]);
			int all_days = Convert.ToInt32(Request.QueryString["alldays"]);
			calculateFooter(day, all_days);


				
		}		

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgReport.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgReport_ItemDataBound);

		}
		#endregion

		private void dgReport_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				e.Item.Cells[0].Text =  line.ToString();
				line++;
			}
		}

		private string nullValuesHTML(string input)
		{
			string str_return = "0";

			if (input != null)
			{
				if ((input != "&nbsp;") && (input.Trim() != ""))
				{
					str_return = Convert.ToInt32(Convert.ToDouble(input)).ToString();
				}
			}
			return str_return;
		}

	}
}
