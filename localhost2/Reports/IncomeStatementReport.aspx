<%@ Page Language="c#" Codebehind="IncomeStatementReport.aspx.cs" AutoEventWireup="True" Inherits="localhost.Hauler.IncomeStatementReport" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
 <div class="DivTitleBarMenu"><asp:Label CssClass="Header Color1 Bold" ID="Label1" runat="server">Income Statement Report</asp:Label></div>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
            <br />
                    <asp:Label CssClass="Content Bold Color2" ID="Label2" runat="server">Start Date:</asp:Label>&nbsp;
                    <asp:Label CssClass="Content Color2" ID="lblStartDate" runat="server">08/01/2005</asp:Label>&nbsp;&nbsp;&nbsp;
                    <asp:Label CssClass="Content Bold Color2" ID="Label4" runat="server">End Date:</asp:Label>&nbsp;
                    <asp:Label CssClass="Content Color2" ID="lblEndDate" runat="server">08/31/2005</asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                    <asp:Label CssClass="Content Bold Color2" ID="Label6" runat="server">Over Head:</asp:Label>&nbsp;
                    <asp:Label CssClass="Content Color2" ID="lblOverheadDisplay" runat="server">42,000</asp:Label>
                    <asp:Label ID="lblOverhead" runat="server" CssClass="Content Color2" Visible="False">42000</asp:Label>
              <br /><br />
              <asp:DataGrid Width="100%" BackColor="#000000" BorderWidth="0" CellSpacing="1" ID="dgReport" runat="server" ShowFooter="True" HorizontalAlign="Left" CellPadding="4" AutoGenerateColumns="False">
                    <SelectedItemStyle Wrap="False"></SelectedItemStyle>
                    <EditItemStyle Wrap="False" CssClass="LinkNormal"></EditItemStyle>
                    <AlternatingItemStyle Wrap="False" CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
                    <ItemStyle Wrap="False" CssClass="LinkNormal LightGray"></ItemStyle>
                    <HeaderStyle HorizontalAlign="Center" CssClass="LinkNormal Color1 Bold OrangeColor"></HeaderStyle>
                    <FooterStyle HorizontalAlign="Right" CssClass="Content Bold Color4 FooterColor"></FooterStyle>
                    <Columns>
                        <asp:BoundColumn>
                            <ItemStyle Wrap="False" CssClass="Bold OrangeColor"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ORDER_DATE" HeaderText="Date" FooterText="Total&lt;BR&gt;Average&lt;BR&gt;Estimate" DataFormatString="{0:d}">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Center" CssClass="Bold OrangeColor"></ItemStyle>
                            <FooterStyle ></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="POUNDS" HeaderText="Pounds" DataFormatString="{0:N0}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SALES" HeaderText="Sales" DataFormatString="{0:N0}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="COST_GOODS" HeaderText="Cost of Goods" DataFormatString="{0:N0}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="GROSS_TRADING_PROFIT" HeaderText="Gross Trading Profit" DataFormatString="{0:N0}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right" CssClass="Bold"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="FREIGHT" HeaderText="Freight" DataFormatString="{0:N0}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" ></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="COMISSION" HeaderText="Commission" DataFormatString="{0:N0}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="NET_TRADING_PROFIT" HeaderText="Net Trading Profit" DataFormatString="{0:N0}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right" CssClass="Bold"></ItemStyle>
                            <FooterStyle Font-Size="8pt" Font-Bold="True"></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="INTEREST" HeaderText="Interest" DataFormatString="{0:N0}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="OVERHEAD" HeaderText="Overhead" DataFormatString="{0:N0}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Font-Size="8pt" Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="NET_PROFIT" HeaderText="Net Profit" DataFormatString="{0:N0}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right" CssClass="Bold OrangeColor"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>

    </table>
</asp:Content>
