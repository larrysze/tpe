<%@ Page Language="C#" MasterPageFile="~/MasterPages/Menu.Master" EnableEventValidation = "false" AutoEventWireup="true" CodeBehind="CashPaid.aspx.cs" Inherits="localhost.Creditor.CashPaid" Title="Cash Paid" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="asp" namespace="System.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCssLink" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMain" runat="server">

<table id="Table1" class="Content LinkNormal" align="center" width="800px" cellspacing="0" cellpadding="0" runat="server">
    <tr align="center">
        <td align="center">
            <p><b>Cash Paid</b></p>&nbsp;&nbsp;&nbsp;
            <asp:Button  ID="btnAdd" runat="server" Text="Add Transaction" OnClick="btnAdd_OnClick"/>&nbsp;&nbsp;&nbsp;<asp:Button ID="btnExcel" runat="server" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
            &nbsp;&nbsp;&nbsp;<asp:dropdownlist id="ddlMonth" runat="server" CssClass="InputForm" onselectedindexchanged="ddlMonth_SelectedIndexChanged" AutoPostBack="true"></asp:dropdownlist>
            &nbsp;&nbsp;&nbsp;Search:&nbsp;<asp:TextBox ID="txtSearch" runat="server" Width="80px" ></asp:TextBox>
            &nbsp;<asp:DropDownList ID="ddlSearchIndex" runat="server" AutoPostBack="False">
                    <asp:ListItem Value="*" Text="Select Field" Selected="true"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Invoice #"></asp:ListItem>
                    <asp:ListItem Value="2" Text="ID"></asp:ListItem>
                    <asp:ListItem Value="3" Text="Seller Name"></asp:ListItem>
                    <asp:ListItem Value="4" Text="Payment Date"></asp:ListItem>
                    <asp:ListItem Value="5" Text="Amount Due"></asp:ListItem>
                    <asp:ListItem Value="6" Text="Amount Paid"></asp:ListItem>
                    </asp:DropDownList>
            &nbsp;<asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_OnClick" Text="Search" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
</table>

<asp:ScriptManager id="ScriptManager1" runat="Server" />    
<asp:UpdatePanel id="UpdatePanel1" runat="Server" >
        <ContentTemplate>
<table id="Table2" class="Content LinkNormal" align="center" width="800px" cellspacing="0" cellpadding="0" runat="server">    
    <tr align="center">
        <td align="center">           
            <asp:GridView ID="dg" CssClass="DataGrid" AllowSorting="true" AllowPaging="true" runat="server" AutoGenerateEditButton="true" DataKeyNames="Transaction_ID" 
                AutoGenerateColumns="False" ShowFooter="true" PageSize="25" OnSorting="dg_Sorting" OnPageIndexChanging="dg_PageIndexChanging" onrowcommand="dg_RowCommand"
                OnRowEditing="dg_RowEditing" OnRowCancelingEdit="dg_RowCancelingEdit" OnRowUpdating="dg_RowUpdating"   OnRowDataBound="KeepRunningSum">                 				
				<HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
				<FooterStyle CssClass="Content Color4 Bold FooterColor" />
                <Columns>
                  <asp:BoundField ReadOnly="true" DataField="Transaction_ID" SortExpression="Transaction_ID" Visible="false"/>
                  <asp:HyperLinkField HeaderText="ID" DataNavigateUrlFields="Transaction_Number" DataNavigateUrlFormatString="../administrator/Transaction_Details.aspx?Referer=/Creditor/Transaction_Summary.aspx&amp;ID={0}" DataTextField="Transaction_Number"  />
                  <asp:BoundField ReadOnly="true" HeaderText="Seller Invoice #" DataField="Seller_Invoice_Number" SortExpression="Seller_Invoice_Number" />
                  <asp:BoundField ReadOnly="true" HeaderText="Seller Name" DataField="Seller_Name" SortExpression="Seller_Name" />  
                  <asp:BoundField HeaderText="Payment Number" DataField="Payment_Number" SortExpression="Payment_Number" />                
                  <asp:BoundField HeaderText="Payment Reference" DataField="Wire_Payment_Number" SortExpression="Wire_Payment_Number" />
                  <asp:BoundField ReadOnly="true" HeaderText="Shipment Date" DataField="Shipment_Date" SortExpression="Shipment_Date" DataFormatString="{0:MM/dd/yyyy}"/>                                                       
                  <asp:BoundField HeaderText="Payment Date" DataField="Wire_Date" SortExpression="Wire_Date" DataFormatString="{0:MM/dd/yyyy}"/>                                                                                           
                  <asp:BoundField ReadOnly="true" HeaderText="Amount Due" DataField="Amount_Due" SortExpression="Amount_Due" DataFormatString="{0:c}"/>                  
                  <asp:BoundField HeaderText="Amount Paid" DataField="Amount_Paid" SortExpression="Amount_Paid" DataFormatString="{0:c}"/>                  
                  <asp:BoundField HeaderText="Comment" DataField="Comment" SortExpression="Comment" />
                  <asp:ButtonField buttontype="Button" DataTextField="Transaction_Number" HeaderText="Delete" CommandName="Select"/>
                  <asp:BoundField ReadOnly="true" HeaderText="Seller_Comp_ID" DataField="Seller_Comp_ID" Visible="false" /> 
                </Columns>
            </asp:GridView>          
        </td>
    </tr>
</table>
<br />


            <asp:Panel id="pnlPanel" runat="server" Visible="false" style="POSITION: absolute; TOP: 200px; LEFT: 600px" >
                <table width="200px" style="background-color:White" border="0" >
                    <tbody>
                        <tr>
                            <td align="center"><asp:Label id="lblAddTransaction" runat="server" CssClass="Content Color3 Bold"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:TextBox id="txtID" runat="server" CssClass="InputForm" TextMode="SingleLine"></asp:TextBox> 
                                <asp:Button id="btnSubmit" AutoPostBack="False" onclick="btnSubmit_Click" runat="server" CssClass="Content Color2" Text="Submit"></asp:Button> 
                                <asp:Button id="btnCancel" AutoPostBack="False" onclick="btnCancel_Click" runat="server" CssClass="Content Color2" Text="Cancel"></asp:Button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel> 
            <asp:Panel id="pnlPanel2" runat="server" Visible="false" style="POSITION: absolute; TOP: 300px; LEFT: 600px" >
                <table width="200px" style="background-color:White" border="0" >
                    <tbody>
                        <tr>
                            <td align="center"><asp:Label id="lblErrorMessage" runat="server" CssClass="Content Color3 Bold"></asp:Label>
                            </td>                        
                        </tr>
                        <tr>
                            <td align="center">
                            <asp:Button id="btnClose" AutoPostBack="False" onclick="btnClose_Click" runat="server" CssClass="Content Color2" Text="Close" />
                            <asp:Button id="btnStillAdd" AutoPostBack="False" onclick="btnStillAdd_Click" runat="server" CssClass="Content Color2" Text="Still Add"></asp:Button> 
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <asp:Panel id="pnlPanel3" runat="server" Visible="false" style="POSITION: absolute; TOP: 300px; LEFT: 600px" >
                <table width="200px" style="background-color:White" border="0" >
                    <tbody>
                        <tr>
                            <td align="center"><asp:Label id="lblMessage" runat="server" CssClass="Content Color3 Bold"></asp:Label>
                            </td>                        
                        </tr>
                        <tr>
                            <td align="center">
                            <asp:Button id="btnClose2" AutoPostBack="False" onclick="btnClose2_Click" runat="server" CssClass="Content Color2" Text="Close" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </ContentTemplate>   
    </asp:UpdatePanel> 
</asp:Content>
