using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using TPE.Utility;

namespace localhost.Creditor
{
	public partial class Current_Payments : System.Web.UI.Page
	{
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dg.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dg_SortCommand);
		}
		#endregion



		enum ColHeaders : int
		{
			CUST_ID = 0,
			AMOUNT_DUE = 4,
			AMOUNT_PAID = 5,
			AMOUNT_NET = 6
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			Master.Width = "1300px";


			//Administrator and Creditor have access
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C") && ((string)Session["Typ"] != "L"))
			{
				Response.Redirect("/default.aspx");
			}	

			if (!IsPostBack)
			{
				Bind();
			}			
		}

		private void Bind()
		{
			DataTable dtContent = null;

			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				
				if (ViewState["ReportView"] == null)	//Set default view
				{
					rbtnWeekReport.Checked = true;
					ViewState.Add("ReportView", "CrWeekReport");
				}
				
				DateTime baseTime = new DateTime(2009, 8, 2, 23, 0, 0);
				DateTime curTime = new DateTime();
				curTime = DateTime.Now;
							

				if (ViewState["ReportView"].ToString() == "CrWeekReport")
				{

					while( baseTime <= curTime.AddDays(-7) )
						baseTime = baseTime.AddDays(7);
					
					Hashtable htParams = new Hashtable();
					htParams.Add("@CurrentDate", baseTime);
					dtContent = DBLibrary.GetDataTableFromStoredProcedure(conn, "spCurrentReceivables", htParams);
					lblTimeLine.Text = "Shipment&Payment Date Line: " + baseTime.ToString();
				}
				else if( ViewState["ReportView"].ToString() == "CrReport")
				{
					// go with DateTime.Now value
					Hashtable htParams = new Hashtable();
					htParams.Add("@CurrentDate", curTime);
					dtContent = DBLibrary.GetDataTableFromStoredProcedure(conn, "spCurrentReceivables", htParams);
					lblTimeLine.Text = "Shipment&Payment Date Line: " + curTime.ToString();
				}
				else if (ViewState["ReportView"].ToString() == "CpReport")
				{
					dtContent = DBLibrary.GetDataTableFromStoredProcedure(conn, "spCurrentPayables");
					//this procedure work within live data. it doesn't get cut by specific time.
					lblTimeLine.Text = "Shipment&Payment Date Line: " + curTime.ToString();
				}

				else if( ViewState["ReportView"].ToString() == "CrReportCi" )
				{
					Hashtable htParams = new Hashtable();
					htParams.Add("@Type", "R");
					dtContent = DBLibrary.GetDataTableFromStoredProcedure(conn, "spCurrentCommittedInventory", htParams);
					lblTimeLine.Text = "(Commited Inventory)Shipment&Payment Date Line: " + curTime.ToString();
				}
				else if( ViewState["ReportView"].ToString() == "CpReportCi" )
				{
					Hashtable htParams = new Hashtable();
					htParams.Add("@Type", "P");
					dtContent = DBLibrary.GetDataTableFromStoredProcedure(conn, "spCurrentCommittedInventory", htParams);
					lblTimeLine.Text = "(Commited Inventory)Shipment&Payment Date Line: " + curTime.ToString();
				}

				DataView dv = new DataView(dtContent);

				if (ViewState["SortExpression"] != null)
				{
					dv.Sort = ViewState["SortExpression"].ToString();
					if (ViewState["SortDirection"] != null)
						dv.Sort += " " + ViewState["SortDirection"].ToString();
				}		
				dg.DataSource = dv;
				dg.DataBind();
			}
		}

		double sumAmountDue = 0;
		double sumAmountPaid = 0;
		double sumAmountNet = 0;

		protected void dg_SumDataBinder(object sender, DataGridItemEventArgs e){

			if( ( e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem ) )
			{
				sumAmountDue += HelperFunction.ConvertToDouble(DataBinder.Eval(e.Item.DataItem, "OUTSTANDING"));
				sumAmountPaid += HelperFunction.ConvertToDouble(DataBinder.Eval(e.Item.DataItem, "TOTAL"));
				sumAmountNet += HelperFunction.ConvertToDouble(DataBinder.Eval(e.Item.DataItem, "NET"));
			}
			else if( e.Item.ItemType == ListItemType.Footer ){

				e.Item.Cells[(int)ColHeaders.CUST_ID].Text = "<b>Total:</b> ";
				e.Item.Cells[(int)ColHeaders.AMOUNT_DUE].Text = "<b>" + String.Format("{0:c}", sumAmountDue) + "</b>";
				e.Item.Cells[(int)ColHeaders.AMOUNT_PAID].Text = "<b>" + String.Format("{0:c}", sumAmountPaid) + "</b>";
				e.Item.Cells[(int)ColHeaders.AMOUNT_NET].Text = "<b>" + String.Format("{0:c}", sumAmountNet) + "</b>";
			}
			
		}

		private void dg_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			if (ViewState["SortDirection"] == null)
				ViewState.Add("SortDirection", "ASC");
			else
			{
				if (ViewState["SortDirection"].ToString() == "ASC")
				{
					ViewState["SortDirection"] = "DESC";
				}
				else
				{
					ViewState["SortDirection"] = "ASC";
				}
			}
			ViewState["SortExpression"] = e.SortExpression.ToString();
			Bind();
		}


		protected void btnExportToExcel_Click(object sender, System.EventArgs e)
		{
			Bind();

			Response.Clear();
			Response.Buffer = true;
			Response.ContentType = "application/vnd.ms-excel";
			Response.AddHeader("Content-Disposition", "attachment; filename=current_receivables.xls");

			Response.BufferOutput = true;
			Response.ContentEncoding = System.Text.Encoding.UTF8;
			Response.Charset = "";
			
			EnableViewState = false;

			System.IO.StringWriter tw = new System.IO.StringWriter();
			System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

			FormatExportedGrid(dg);
			this.ClearControls(dg);
			dg.RenderControl(hw);

			Response.Write(tw.ToString());
			Response.End();
		}


		public void FormatExportedGrid(DataGrid dg1)
		{
			dg1.BackColor = System.Drawing.Color.White;
		}

	

		private void ClearControls(Control control)
		{
			for (int i = control.Controls.Count - 1; i >= 0; i--)
			{
				ClearControls(control.Controls[i]);
			}
			if (!(control is TableCell))
			{
				if (control.GetType().GetProperty("SelectedItem") != null)
				{
					LiteralControl literal = new LiteralControl();
					control.Parent.Controls.Add(literal);
					try
					{
						literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
					}
					catch
					{
					}
					control.Parent.Controls.Remove(control);
				}
				else
					if (control.GetType().GetProperty("Text") != null)
					{
						LiteralControl literal = new LiteralControl();
						control.Parent.Controls.Add(literal);
						literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);
						control.Parent.Controls.Remove(control);
					}
			}
			return;
		}

		protected void rbtnWeekReport_CheckedChanged(object sender, EventArgs e)
		{
			ViewState["ReportView"] = "CrWeekReport";

			Bind();
		}

		protected void rbtnCurrentReceivables_CheckedChanged(object sender, EventArgs e)
		{
			ViewState["ReportView"] = "CrReport";
		
			Bind();
		}
		protected void rbtnCurrentPayables_CheckedChanged(object sender, EventArgs e)
		{
			ViewState["ReportView"] = "CpReport";
			Bind();
		}

		protected void rbtnCurrentReceivablesCi_CheckedChanged( object sender, EventArgs e )
		{
			ViewState["ReportView"] = "CrReportCi";

			Bind();
		}
		protected void rbtnCurrentPayablesCi_CheckedChanged( object sender, EventArgs e )
		{
			ViewState["ReportView"] = "CpReportCi";
			Bind();
		}

	}
}
