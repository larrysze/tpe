using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Text;

namespace localhost.Creditor
{
	/// <summary>
	/// Summary description for Credit_Management.
	/// </summary>
	public class Credit_Management : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DropDownList ddl;
		protected System.Web.UI.WebControls.DataGrid dg;
	
		/************************************************************************
		 *   1. File Name       :Creditor\Credit_Management.aspx                 *
		 *   2. Description     :Check Company Info, outstandings, send statement*
		 *   3. Modification Log:                                                *
		 *     Ver No.       Date          Author             Modification       *
		 *   -----------------------------------------------------------------   *
		 *      1.00      10-30-2003      Zach                                   *
		 *                                                                       *
		 ************************************************************************/



		private void Page_Load(object sender, EventArgs e)
		{
			//Administrator and Creditor have access
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C"))
			{
				Response.Redirect("/default.aspx");
			}
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			if (!IsPostBack)
			{
				// populating drop-down box for days late
				ViewState["Sort"] = "COMP_NAME ASC";
				ddl.Items.Add(new ListItem ("All","*"));
				ddl.Items.Add(new ListItem ("Receivables","R"));
				ddl.Items.Add(new ListItem ("Payables","P"));

			}
			Bind();

		}
		private void Bind()
		{
			StringBuilder sbSQL = new StringBuilder();
			SqlDataAdapter dadContent;
			DataSet dstContent;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();

			sbSQL.Append("Exec spCredit_Management");
			sbSQL.Append(" @ORDER='"+ViewState["Sort"].ToString()+"'");
			if (!ddl.SelectedItem.Value.Equals("*"))
			{
				// either recievables or payables must be selected
				if (ddl.SelectedItem.Value.Equals("R"))
				{
					sbSQL.Append(" ,@Recievables='1'");
				}
				else
				{
					sbSQL.Append(" ,@Payables='1'");
				}
			}

			dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dg.DataSource = dstContent;
			dg.DataBind();
			conn.Close();


		}


		double runningSum = 0.0;
		// <summary>
		//  calculates the amount outstanding.  shows statement
		// </summary>
		protected void OnDataGridBind(object sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				// sums the items for calculation at bottom
				runningSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "FO"));

				if  (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "FO")) !=0.0)
				{
					if ((DataBinder.Eval(e.Item.DataItem, "COMP_TYPE")).ToString() == "P"  &&  Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "color")) !=0   )
					{
						e.Item.Cells[3].Text = "<a href=\"/Creditor/Creditor_Payment.aspx?id="+(DataBinder.Eval(e.Item.DataItem, "COMP_ID")).ToString()+"&Type="+(DataBinder.Eval(e.Item.DataItem, "COMP_TYPE")).ToString()+"\"><font color=red>"+String.Format("{0:c}",(DataBinder.Eval(e.Item.DataItem, "FO")))+"</font></a>";
					}
					else if ((DataBinder.Eval(e.Item.DataItem, "COMP_TYPE")).ToString() != "P"  &&  Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "color2")) !=0   )
					{
						e.Item.Cells[3].Text = "<a href=\"/Creditor/Creditor_Payment.aspx?id="+(DataBinder.Eval(e.Item.DataItem, "COMP_ID")).ToString()+"&Type="+(DataBinder.Eval(e.Item.DataItem, "COMP_TYPE")).ToString()+"\"><font color=blue>"+String.Format("{0:c}",(DataBinder.Eval(e.Item.DataItem, "FO")))+"</font></a>";
					}
					else
					{
						e.Item.Cells[3].Text = "<a href=\"/Creditor/Creditor_Payment.aspx?id="+(DataBinder.Eval(e.Item.DataItem, "COMP_ID")).ToString()+"&Type="+(DataBinder.Eval(e.Item.DataItem, "COMP_TYPE")).ToString()+"\">"+String.Format("{0:c}",(DataBinder.Eval(e.Item.DataItem, "FO")))+"</a>";
					}

					e.Item.Cells[4].Text ="<a href=\"/Creditor/Statement.aspx?Spot=0&Id="+(DataBinder.Eval(e.Item.DataItem, "COMP_ID")).ToString()+"&Type="+(DataBinder.Eval(e.Item.DataItem, "COMP_TYPE")).ToString()+"\">Statement</a>";
				}


			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[0].Text = "<b>Total:</b> ";
				e.Item.Cells[3].Text = String.Format("{0:c}", runningSum);
			}

            

		}
		// <summary>
		// Sorting function
		// </summary>
		public void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{
			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;
			//  Parse the sort expression - delimiter space
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
			ColumnToSort = SortExprs[0];
			// If a sort order is specified get it, else default is descending
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DESC";
			}
			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;
			// Figure out the column index
			int iIndex;
			iIndex = 0;

			switch(ColumnToSort.ToUpper())
			{
				case "COMP_NAME":
					iIndex = 0;
					break;
				case "COMP_TYPE":
					iIndex = 2;
					break;
				case "FO":
					iIndex = 3;
					break;

			}


			// alter the column's sort expression


			dg.Columns[iIndex].SortExpression = NewSortExpr;
			//ResetHeaders();
			//if (NewSearchMode.Equals("DESC")){
			//    dg.Columns[iIndex].HeaderText = "<img border=\"0\" src=\"/pics/icons/icon_down_arrow.gif\">" + dgr.Columns[iIndex].HeaderText;
			//}else{
			//    dg.Columns[iIndex].HeaderText =  "<img border=\"0\" src=\"/pics/icons/icon_up_arrow.gif\">" + dg.Columns[iIndex].HeaderText;
			//}
			dg.CurrentPageIndex = 0;


			// Sort the data in new order
			ViewState["Sort"] = NewSortExpr;
			// reset dbase page to the first one whether the sorting changes


			Bind();
		}



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
