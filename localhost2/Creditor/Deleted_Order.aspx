<%@ Register TagPrefix="mbdb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.DefaultButtons" %>
<%@ Page Language="C#" CodeBehind="Deleted_Order.aspx.cs" AutoEventWireup="true" Inherits="localhost.Creditor.Deleted_Order" MasterPageFile="~/MasterPages/Menu.Master" EnableViewState="True" EnableEventValidation="false" Title="Deleted Order" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content ID="ContentDeletedOrder" runat="server" ContentPlaceHolderID="cphCssLink">
	<style type="text/css">
	table{ align:center }
	.menuskin { BORDER-RIGHT: black 2px solid; BORDER-TOP: black 2px solid; Z-INDEX: 100; VISIBILITY: hidden; FONT: 12px/18px Verdana; BORDER-LEFT: black 2px solid; WIDTH: 165px; BORDER-BOTTOM: black 2px solid; POSITION: absolute; BACKGROUND-COLOR: menu }
	.menuskin A { PADDING-RIGHT: 10px; PADDING-LEFT: 10px; COLOR: black; TEXT-DECORATION: none }
	#mouseoverstyle { BACKGROUND-COLOR: highlight }
	#mouseoverstyle A { COLOR: white }
	</style>
</asp:Content>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
	<table cellspacing="0" cellpadding="0" align="center" border="0" class="Content LinkNormal" width="1200px">
		<tr>
			<td>
			<div class="DivTitleBarMenu"><span class="Header Color1 Bold">Deleted Orders</span></div>
			</td>
		</tr>
		<tr>
		    <td align="center">
			         <table width="100%" align="left">
                    <tbody>
                		<tr valign="bottom">
                            <td valign="top" align="left"><b>Filters:</b><br /></td>
                            <td>
				<b>Date</b><br />
				<asp:dropdownlist id="ddlMonth" runat="server" CssClass="InputForm" onselectedindexchanged="ddlMonth_SelectedIndexChanged" EnableViewState="True" AutoPostBack="True"></asp:dropdownlist>
                            </td>
                            <td valign="top"> <br />  
				<asp:Button runat="server" ID="cmdExportToExcel" Text="Export to Excel"  CssClass="Content Color2" onclick="btnExportToExcel_Click" />
				</td>
                            <td valign="top" align="left"><b>Search</b><br /><mbdb:defaultbuttons id="DefaultSearchButtons" runat="server">
								<mbdb:DefaultButtonSetting button="btnSearch" parent="txtSearch"></mbdb:DefaultButtonSetting>
							</mbdb:defaultbuttons><asp:label id="lblSort" runat="server" visible="false"></asp:label><asp:textbox id="txtSearch" runat="server" CssClass="InputForm" size="8"></asp:textbox>
							    <asp:dropdownlist id="ddlIndex" runat="server" CssClass="InputForm" ></asp:dropdownlist>
							    <asp:button id="btnSearch" onclick="btnSearch_Click" runat="server" CssClass="Content Color2" Text="Go"></asp:button></td>
						</tr>
						<tr><asp:Label ID="lblResult" runat="Server" Text="Result:" ForeColor="red" Font-Size="X-Small"></asp:Label></tr>
					</tbody>
				</table>
		    </td>
		</tr>
		
		<tr>
			<td align="center"><asp:datagrid BorderWidth="0" BackColor="#000000" CellSpacing="1" id="dg" runat="server" Width="85%" CssClass="DataGrid" HorizontalAlign="Center"
					CellPadding="2" DataKeyField="TRANSACTION_NUMBER" ShowFooter="True" OnItemDataBound="ItemDataBounder" AllowSorting="True" 
					AutoGenerateColumns="False" AllowPaging="false" >
					<AlternatingItemStyle CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
					<ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
					<HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
					<FooterStyle CssClass="Content Color4 Bold FooterColor" />
					<Columns>						
						<asp:ButtonColumn Text="UnDelete" ButtonType="PushButton" CommandName="UnDelete">
						</asp:ButtonColumn>	
						<asp:HyperLinkColumn DataNavigateUrlField="TRANSACTION_NUMBER" HeaderText="TN#" SortExpression="TRANSACTION_NUMBER" DataNavigateUrlFormatString="../administrator/Transaction_Details_Deleted.aspx?Referer=/Creditor/Transaction_Summary.aspx&amp;ID={0}" DataTextField="TRANSACTION_NUMBER">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:BoundColumn DataField="TRANSACTION_DATE" SortExpression="TRANSACTION_DATE" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
						<asp:BoundColumn DataField="PRODUCT" SortExpression="PRODUCT" HeaderText="Product" >
							<ItemStyle  HorizontalAlign="Left" Wrap="False" ></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="WEIGHT_LBS" SortExpression="WEIGHT_LBS" HeaderText="Weight(lbs)" DataFormatString="{0:#,###}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="SHIPMENT_CMT" SortExpression="SHIPMENT_CMT" HeaderText="RC#">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="TVBUYR" SortExpression="TVBUYR" HeaderText="From Buyer" DataFormatString="{0:c}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
							<asp:BoundColumn DataField="PPAID" SortExpression="PPAID" HeaderText="Buyer Paid" DataFormatString="{0:c}">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="BUYPRICE" SortExpression="BUYPRICE" HeaderText="Buy Price">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="SELLPRICE" SortExpression="SELLPRICE" HeaderText="Sell Price">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="TVSELR" SortExpression="TVSELR" HeaderText="To Seller" DataFormatString="{0:c}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="SPAID" SortExpression="SPAID" HeaderText="Seller Paid" DataFormatString="{0:c}">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>	
						<asp:BoundColumn DataField="BROKER" SortExpression="BROKER" HeaderText="Broker"></asp:BoundColumn>		
						<asp:HyperLinkColumn DataNavigateUrlField="BUYR_ID" DataNavigateUrlFormatString="/Creditor/AccountSummary.aspx?Id={0}"
							DataTextField="BUYR" SortExpression="BUYR" HeaderText="Buyer">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="SELR_ID" DataNavigateUrlFormatString="/Creditor/AccountSummary.aspx?Id={0}"
							DataTextField="SELR" SortExpression="SELR" HeaderText="Seller">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
					</Columns>
				</asp:datagrid><asp:label id="lblMessage" runat="server">There are no transactions listed that match this request!</asp:label>
			</td>
		</tr>
	</table>
</asp:Content>