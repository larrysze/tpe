<%@ Page Language="c#" Codebehind="IncomeStatementReport.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.IncomeStatementReport" MasterPageFile="~/MasterPages/Menu.Master" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
 <div class="DivTitleBarMenu"><asp:Label CssClass="Header Color1 Bold" ID="Label1" runat="server" Text="Financial Report"></asp:Label></div>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
        <tbody>
        <tr>
            <td>
            <br />
                &nbsp;<asp:Button ID="btnExcel" runat="server" Text="Export to Excel" OnClick="btnExportToExcel_Click" />&nbsp;&nbsp;<asp:DropDownList runat="server" ID="ddlMonth" AutoPostBack="False" >                    
                </asp:DropDownList>&nbsp;&nbsp;<asp:Button runat="server" ID="btnReport" OnClick="btnReport_OnClick" Text="Get Report"/>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnl1" Visible="false" runat="server">
                <asp:Label CssClass="Content Bold Color2" ID="Label9" runat="server" Text="Adjustment:"></asp:Label>&nbsp;<asp:TextBox runat="server" ID="txtAdj" Width="80px" Text="0"></asp:TextBox>
                <asp:Label CssClass="Content Bold Color2" ID="Label5" runat="server" Text="SGA:"></asp:Label>&nbsp;<asp:TextBox runat="server" ID="txtSGA" Width="80px" Text="0"></asp:TextBox>
                <asp:Label CssClass="Content Bold Color2" ID="Label7" runat="server" Text="Bad Debts:"></asp:Label>&nbsp;<asp:TextBox runat="server" ID="txtBD" Width="80px" Text="0"></asp:TextBox>
                <asp:Label CssClass="Content Bold Color2" ID="Label8" runat="server" Text="Interest:"></asp:Label>&nbsp;<asp:TextBox runat="server" ID="txtInterest" Width="80px" Text="0"></asp:TextBox>                
                <asp:Button runat="server" ID="btnUpdate" OnClick="btnUpdate_OnClick" Text="Update"/></asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
            <br />
                   <%-- <asp:Label CssClass="Content Bold Color2" ID="Label2" runat="server">Start Date:</asp:Label>&nbsp;
                    <asp:Label CssClass="Content Color2" ID="lblStartDate" runat="server">08/01/2005</asp:Label>&nbsp;&nbsp;&nbsp;
                    <asp:Label CssClass="Content Bold Color2" ID="Label4" runat="server">End Date:</asp:Label>&nbsp;
                    <asp:Label CssClass="Content Color2" ID="lblEndDate" runat="server">08/31/2005</asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                    <asp:Label CssClass="Content Bold Color2" ID="Label6" runat="server">Over Head:</asp:Label>&nbsp;
                    <asp:Label CssClass="Content Color2" ID="lblOverheadDisplay" runat="server">42,000</asp:Label>
                    <asp:Label ID="lblOverhead" runat="server" CssClass="Content Color2" Visible="False">42000</asp:Label>
              <br /><br />--%>
              <asp:DataGrid Width="100%" BackColor="#000000" BorderWidth="0" CellSpacing="1" ID="dgReport" runat="server" ShowFooter="True" HorizontalAlign="Left" CellPadding="4" AutoGenerateColumns="False">
                    <SelectedItemStyle Wrap="False"></SelectedItemStyle>
                    <EditItemStyle Wrap="False" CssClass="LinkNormal"></EditItemStyle>
                    <AlternatingItemStyle Wrap="False" CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
                    <ItemStyle Wrap="False" CssClass="LinkNormal LightGray"></ItemStyle>
                    <HeaderStyle HorizontalAlign="Center" CssClass="LinkNormal Color1 Bold OrangeColor"></HeaderStyle>
                    <FooterStyle HorizontalAlign="Right" CssClass="Content Bold Color4 FooterColor"></FooterStyle>
                    <Columns>
                        <asp:BoundColumn>
                            <ItemStyle Wrap="False" CssClass="Bold OrangeColor"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DATE" HeaderText="Date" FooterText="Total&lt;BR&gt;Average&lt;BR&gt;Estimate" DataFormatString="{0:d}">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Center" CssClass="Bold OrangeColor"></ItemStyle>
                            <FooterStyle ></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="WEIGHT" HeaderText="Weight" DataFormatString="{0:#,###}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SALE_TOTAL" HeaderText="Buyer Toal" DataFormatString="{0:#,###}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="PRICE_PER_LB" HeaderText="Price per #" DataFormatString="{0:0.0000}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right" CssClass="Bold OrangeColor"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="BUY_TOTAL" HeaderText="Seller Total" DataFormatString="{0:#,###}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right" CssClass="Bold"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SPREAD_PER_LB" HeaderText="Spread Per #" DataFormatString="{0:0.0000}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" CssClass="Bold OrangeColor"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="WAREHOUSE" HeaderText="Warehouse" DataFormatString="{0:#,###}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="FREIGHT" HeaderText="Freight" DataFormatString="{0:#,###}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right" CssClass="Bold"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MISC" HeaderText="Misc" DataFormatString="{0:#,###}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="COMMISSION" HeaderText="Commission" DataFormatString="{0:#,###}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Font-Size="8pt" Wrap="False" HorizontalAlign="Right"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        
                        <asp:BoundColumn DataField="ADJUSTMENT" HeaderText="Adjustment" DataFormatString="{0:#,###}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right" ></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MARGIN" HeaderText="Margin Amount" DataFormatString="{0:#,###}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right" ></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MARGIN_PER_LB" HeaderText="Margin Per #" DataFormatString="{0:0.0000}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right" CssClass="Bold OrangeColor"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SGA" HeaderText="S, G & A" DataFormatString="{0:#,###}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right" ></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="BD" HeaderText="Bad Debts" DataFormatString="{0:#,###}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right" ></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="INTEREST" HeaderText="Interest" DataFormatString="{0:#,###}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right" ></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="NET_INCOME" HeaderText="Net Income" DataFormatString="{0:#,###}">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle Wrap="False" HorizontalAlign="Right" CssClass="Bold OrangeColor"></ItemStyle>
                            <FooterStyle></FooterStyle>
                        </asp:BoundColumn>                        
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
        </tbody>
    </table>
</asp:Content>
