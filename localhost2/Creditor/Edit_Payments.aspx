<%@ Page Language="c#" CodeBehind="Edit_Payments.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.Edit_Payments" MasterPageFile="~/MasterPages/Menu.Master"%>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
	<div style="width:780px"><center>	    
		<asp:Button CssClass="Content Color2" id="btnCloseTransaction" runat="server" Text="Close transaction" Width="123px" onclick="btnCloseTransaction_Click">
		</asp:Button>&nbsp;
		<asp:button CssClass="Content Color2" id="Button1" onclick="Add_New" runat="server" Text="Add Payment"></asp:button>&nbsp;
		<asp:button CssClass="Content Color2" id="btnWriteOff" onclick="Write_Off" runat="server" Text="Write Off"></asp:button>		
		<br />
		<br />
		<asp:Panel id="pnlConfirmationPanel" runat="server" Visible="false">
            <table id="tbComment" cellspacing="1" cellpadding="1" width="100%" border="0">
                <tr>
                    <td valign="top" align="center">
                        <fieldset width="200">
                            <legend style="text-align:center">
                                <span class="Header Color3 Bold" >WriteOff Comment</span>
                            </legend><br />
                            <asp:Panel id="pnlSubPanel" runat="server" Visible="true">
                                <table width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="center"><asp:Label id="lblWriteOffMessage" runat="server" CssClass="Content Color2 Bold" Text="Please comment the reason of this write off."></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:TextBox id="txtWriteOffComment" runat="server" CssClass="InputForm" Height="60px" Width="350px"  TextMode="SingleLine" Rows="5" Columns="80"></asp:TextBox> <br />
                                                <asp:Button id="btnSubmit" onclick="btnSubmit_Click" runat="server" CssClass="Content Color2" Text="Confirm"></asp:Button> 
                                                <asp:Button id="btnCancel" onclick="btnCancel_Click" runat="server" CssClass="Content Color2" Text="Cancel"></asp:Button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </asp:Panel> 
                        </fieldset>
                    </td> 
                </tr>
            </table>
        </asp:Panel>
		<br />
		Total Amount: <asp:Label runat="Server" id="lblTotalAmount" Font-Bold="true"  />
		<br />
		<br />
	</center>
	<asp:datagrid BackColor="#000000" CellSpacing="1" BorderWidth="0" id="dg" runat="server" AutoGenerateColumns="false" ShowFooter="True" EditItemStyle-BackColor="Silver"
		Font-Size="9pt" horizontalalign="Center" CellPadding="2" OnDeleteCommand="DG_Delete" OnUpdateCommand="DG_Update"
		OnCancelCommand="DG_Cancel" OnEditCommand="DG_Edit" Width="770px" HeaderStyle-CssClass="LinkNormal Bold OrangeColor"
		AlternatingItemStyle-CssClass="LinkNormal DarkGray" ItemStyle-CssClass="LinkNormal LightGray" FooterStyle-CssClass="Content Color4 Bold FooterColor">
		<Columns>
			<asp:editcommandcolumn edittext="Edit" canceltext="Cancel" updatetext="Update" />
			<asp:buttoncolumn text="Delete" commandname="Delete" />
			<asp:BoundColumn DataField="PAY_ID" HeaderText="Payment Number" ReadOnly="True" ItemStyle-Wrap="false"
				ItemStyle-HorizontalAlign="center" />
			<asp:BoundColumn DataField="PAY_ORDR" HeaderText="Order Number" ReadOnly="True" ItemStyle-Wrap="false"
				ItemStyle-HorizontalAlign="left" />
			<asp:BoundColumn DataField="COMPANY" HeaderText="Company" ReadOnly="True" ItemStyle-Wrap="false"
				ItemStyle-HorizontalAlign="center" />
			<asp:BoundColumn DataField="PAY_DATE" DataFormatString="{0:MM/dd/yyyy}" ReadOnly="false" HeaderText="Date"
				ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
			<asp:BoundColumn DataField="PAY_AMNT" HeaderText="Amount" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
<%--			<asp:BoundColumn DataField="PAY_CMNT"  HeaderText="Comment" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />--%>
						<asp:TemplateColumn HeaderText="Comment">
				<ItemTemplate>
					<asp:Label ID="lblComment" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.pay_cmnt") %>'>
					</asp:Label>
				</ItemTemplate>
				<EditItemTemplate>
					<asp:TextBox ID="txtComment" CssClass="InputForm" runat="server" 
					    MaxLength="4000" Rows="20" TextMode="MultiLine" Columns="70" 
					    Text='<%# DataBinder.Eval(Container, "DataItem.pay_cmnt")%>'>
					</asp:TextBox>
				</EditItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
			</asp:TemplateColumn>
		</Columns>
	</asp:datagrid>
	<center><br>
		<br>        
		<br>
		<asp:button CssClass="Content Color2" id="Button2" onclick="Navigate_Back" runat="server" Text="Return To Transaction Summary"></asp:button>
		<asp:button CssClass="Content Color2" id="Button3" onclick="Navigate_Back2" runat="server" Text="Return To Account Summary"></asp:button></center>
	<br>
	<br>
	<br>
	<br>
	<br>
	</div>
</asp:Content>

