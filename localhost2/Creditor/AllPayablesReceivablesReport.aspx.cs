using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;

namespace localhost.Creditor
{
	/// <summary>
	/// Summary description for AllPayablesReport.
	/// </summary>
	public partial class AllPayablesReport : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((string)Session["Typ"] != "A")
			{
				Response.Redirect("/default.aspx");
			}		

			// Put user code to initialize the page here
			Bind();
		}

		private void Bind()
		{
			SqlDataAdapter dadContent;
			DataSet dstContent;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();

			StringBuilder sbSQL = new StringBuilder("");
			if(ddlPR.SelectedValue == "1")
			{
				////Payables without COMMENTS (by transaction)
				
				sbSQL.Append(" SELECT * ");
				sbSQL.Append(" FROM (SELECT *, ");
				sbSQL.Append("		    VARID = CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR), ");
				sbSQL.Append(" 		VAROWES=VARTV30-ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_SELR),0), ");
				sbSQL.Append(" 		VARTV=VARTV30, ");
				sbSQL.Append("         VARNETDUE=ISNULL (CONVERT(VARCHAR,DATEADD(d,CAST(SHIPMENT_SELLER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)),101),0) FROM (SELECT BOOL=ISNULL(SHIPMENT_WEIGHT,0), ");
				sbSQL.Append("         VARDIFF =ISNULL(DATEDIFF(d, DATEADD(d,CAST(SHIPMENT_SELLER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)), getdate()),0), ");
				sbSQL.Append(" 		SHIPMENT_SELLER_TERMS=ISNULL(SHIPMENT_SELLER_TERMS,30), ");
				sbSQL.Append(" 		SHIPMENT_SIZE, ");
				sbSQL.Append(" 		SHIPMENT_DATE_TAKE=ISNULL(SHIPMENT_DATE_TAKE,GETDATE()), ");
				sbSQL.Append(" 		SHIPMENT_ORDR_ID, ");
				sbSQL.Append(" 		SHIPMENT_SELR, ");
				sbSQL.Append(" 		VARCOMPANY =(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)), ");
				//sbSQL.Append(" 		VARFEE=(CASE SHIPMENT_MARK WHEN 0 THEN (SHIPMENT_PRCE*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)*(CASE SHIPMENT_SIZE WHEN 42000 THEN 0.04 ELSE 0.03 END)) ELSE (SHIPMENT_MARK*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)) END), ");
				sbSQL.Append(" 		VARTV30=(SHIPMENT_PRCE)*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY) * (( 100 + ISNULL(SHIPMENT_TAX,0) ) / 100), ");
				sbSQL.Append(" 		SHIPMENT_ID,");	
				sbSQL.Append(" 		VARAMOUNT=ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_SELR),0),");

				sbSQL.Append(" 		SHIPMENT_SKU, PAY_CMNT='', PAY_DATE='', PAY_AMNT='' ");
				sbSQL.Append(" 		FROM SHIPMENT ");
				//			sbSQL.Append(" 		ORDER BY VARID");
				sbSQL.Append(" )Q0)Q1 ORDER BY VARID");

			}
			else if(ddlPR.SelectedValue == "2")
			{
				//Payables with COMMENTS (by payment)
				sbSQL.Append(" SELECT * ");
				sbSQL.Append(" FROM (SELECT *, ");
				sbSQL.Append("		    VARID = CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR), ");
				sbSQL.Append(" 		VAROWES=VARTV30-ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_SELR),0), ");
				sbSQL.Append(" 		VARTV=VARTV30, ");
				sbSQL.Append("         VARNETDUE=ISNULL (CONVERT(VARCHAR,DATEADD(d,CAST(SHIPMENT_SELLER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)),101),0) FROM (SELECT BOOL=ISNULL(SHIPMENT_WEIGHT,0), ");
				sbSQL.Append("         VARDIFF =ISNULL(DATEDIFF(d, DATEADD(d,CAST(SHIPMENT_SELLER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)), getdate()),0), ");
				sbSQL.Append(" 		SHIPMENT_SELLER_TERMS=ISNULL(SHIPMENT_SELLER_TERMS,30), ");
				sbSQL.Append(" 		SHIPMENT_SIZE, ");
				sbSQL.Append(" 		SHIPMENT_DATE_TAKE=ISNULL(SHIPMENT_DATE_TAKE, ''), ");
				sbSQL.Append(" 		SHIPMENT_ORDR_ID, ");
				sbSQL.Append(" 		SHIPMENT_SELR, ");
				sbSQL.Append(" 		VARCOMPANY =(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)), ");
				//sbSQL.Append(" 		VARFEE=(CASE SHIPMENT_MARK WHEN 0 THEN (SHIPMENT_PRCE*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)*(CASE SHIPMENT_SIZE WHEN 42000 THEN 0.04 ELSE 0.03 END)) ELSE (SHIPMENT_MARK*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)) END), ");
				sbSQL.Append(" 		VARTV30=(SHIPMENT_PRCE)*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY) * (( 100 + ISNULL(SHIPMENT_TAX,0) ) / 100), ");
				sbSQL.Append(" 		SHIPMENT_ID,");	
				sbSQL.Append(" 		VARAMOUNT=ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_SELR),0),");
				sbSQL.Append(" 		PAY_CMNT, PAY_DATE, PAY_AMNT, ");
				sbSQL.Append(" 		SHIPMENT_SKU ");
				sbSQL.Append(" 		FROM SHIPMENT, PAYMENT WHERE PAY_SHIPMENT = SHIPMENT_ID AND PAY_PERS=SHIPMENT_SELR");
				//			sbSQL.Append(" 		ORDER BY VARID");
				sbSQL.Append(" )Q0)Q1 ORDER BY VARID");
			}
			else if(ddlPR.SelectedValue == "3")
			{
				////Receivabes without comments (by transaction)
				
				sbSQL.Append(" SELECT * ");
				sbSQL.Append(" FROM (SELECT *, ");
				sbSQL.Append("		VARID = CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR), ");
				sbSQL.Append(" 		VAROWES=VARTV30-ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),0), ");
				sbSQL.Append(" 		VARTV=VARTV30, ");
				sbSQL.Append("		VARNETDUE=ISNULL (CONVERT(VARCHAR,DATEADD(d,CAST(SHIPMENT_BUYER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)),101),0) FROM (SELECT BOOL=ISNULL(SHIPMENT_WEIGHT,0), ");
				sbSQL.Append("      VARDIFF =ISNULL(DATEDIFF(d, DATEADD(d,CAST(SHIPMENT_BUYER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)), getdate()),0), ");
				sbSQL.Append(" 		SHIPMENT_BUYER_TERMS=ISNULL(SHIPMENT_BUYER_TERMS,30), ");
				sbSQL.Append(" 		SHIPMENT_SIZE, ");
				sbSQL.Append(" 		SHIPMENT_DATE_TAKE=ISNULL(SHIPMENT_DATE_TAKE,GETDATE()), ");
				sbSQL.Append(" 		SHIPMENT_ORDR_ID, ");
				sbSQL.Append(" 		SHIPMENT_BUYR, ");
				sbSQL.Append(" 		VARCOMPANY =(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)), ");
				//sbSQL.Append(" 		VARFEE=(CASE SHIPMENT_MARK WHEN 0 THEN (SHIPMENT_PRCE*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)*(CASE SHIPMENT_SIZE WHEN 42000 THEN 0.04 ELSE 0.03 END)) ELSE (SHIPMENT_MARK*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)) END), ");
				sbSQL.Append(" 		VARTV30=(SHIPMENT_BUYR_PRCE)*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)* (( 100 + ISNULL(SHIPMENT_TAX,0) ) / 100), ");
				sbSQL.Append(" 		SHIPMENT_ID,");	
				sbSQL.Append(" 		VARAMOUNT=ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),0),");
				sbSQL.Append(" 		SHIPMENT_SKU, PAY_CMNT='', PAY_DATE='', PAY_AMNT=''  ");
				sbSQL.Append(" 		FROM SHIPMENT ");

				sbSQL.Append(" )Q0)Q1 ");
			}
			else if (ddlPR.SelectedValue == "4")
			{
				//Receivabes with comments (by payment)
				sbSQL.Append(" SELECT * ");
				sbSQL.Append(" FROM (SELECT *, ");
				sbSQL.Append("		VARID = CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR), ");
				sbSQL.Append(" 		VAROWES=VARTV30-ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),0), ");
				sbSQL.Append(" 		VARTV=VARTV30, ");
				sbSQL.Append("		VARNETDUE=ISNULL (CONVERT(VARCHAR,DATEADD(d,CAST(SHIPMENT_BUYER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)),101),0) FROM (SELECT BOOL=ISNULL(SHIPMENT_WEIGHT,0), ");
				sbSQL.Append("      VARDIFF =ISNULL(DATEDIFF(d, DATEADD(d,CAST(SHIPMENT_BUYER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)), getdate()),0), ");
				sbSQL.Append(" 		SHIPMENT_BUYER_TERMS=ISNULL(SHIPMENT_BUYER_TERMS,30), ");
				sbSQL.Append(" 		SHIPMENT_SIZE, ");
				sbSQL.Append(" 		SHIPMENT_DATE_TAKE=ISNULL(SHIPMENT_DATE_TAKE, ''), ");
				sbSQL.Append(" 		SHIPMENT_ORDR_ID, ");
				sbSQL.Append(" 		SHIPMENT_BUYR, ");
				sbSQL.Append(" 		VARCOMPANY =(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)), ");
				//sbSQL.Append(" 		VARFEE=(CASE SHIPMENT_MARK WHEN 0 THEN (SHIPMENT_PRCE*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)*(CASE SHIPMENT_SIZE WHEN 42000 THEN 0.04 ELSE 0.03 END)) ELSE (SHIPMENT_MARK*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)) END), ");
				sbSQL.Append(" 		VARTV30=(SHIPMENT_BUYR_PRCE)*ISNULL(SHIPMENT_WEIGHT,(SHIPMENT_SIZE)*SHIPMENT_QTY)* (( 100 + ISNULL(SHIPMENT_TAX,0) ) / 100), ");
				sbSQL.Append(" 		SHIPMENT_ID,");	
				sbSQL.Append(" 		VARAMOUNT=ISNULL((SELECT SUM(P.PAY_AMNT) FROM PAYMENT AS P WHERE P.PAY_SHIPMENT=SHIPMENT_ID AND P.PAY_PERS=SHIPMENT_BUYR),0),");
				sbSQL.Append(" 		SHIPMENT_SKU, ");
				sbSQL.Append(" 		PAY_CMNT, PAY_DATE, PAY_AMNT ");
				sbSQL.Append(" 		FROM SHIPMENT, PAYMENT WHERE PAY_SHIPMENT = SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR");
				sbSQL.Append(" )Q0)Q1 ");

			}

			dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
			dstContent = new DataSet();
			//Response.Write(sbSQL.ToString());
			dadContent.Fill(dstContent);
			
			//cpt2 = dstContent.Tables[0].Rows.Count +1 ;

			//Response.Write("get data from database... | ");
			dgPayables.DataSource = dstContent;
			dgPayables.DataBind();

		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void ddlPR_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Bind();
		}
	}
}
