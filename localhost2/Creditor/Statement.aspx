<%@ Page Language="c#" CodeBehind="Statement.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.Statement" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
		<TABLE align="center" border="0" class="Content" width="780">
			<tr>
				<td colspan='2'>
					<TABLE id="tMail" align="left">
						<tr>
							<td colspan='2' align="left" width="80"><span class="Content Color2">Send Email:</span>
							</td>
							<td><asp:label CssClass="Content Color2" id="lblFrom" runat="server" text="From "></asp:label></td>
							<td><asp:dropdownlist CssClass="InputForm" id="ddlFrom" runat="server">
									<asp:ListItem value="mike@ThePlasticExchange.com">Michael Greenberg</asp:ListItem>
									<asp:ListItem value="helio@ThePlasticExchange.com">Helio</asp:ListItem>
									<asp:ListItem Value="brett@ThePlasticExchange.com">Brett</asp:ListItem>									
								</asp:dropdownlist></td>
							<td><asp:label CssClass="Content Color2" id="lblTo" runat="server" text="   To "></asp:label></td>
							<td width="100"><asp:dropdownlist CssClass="InputForm" id="ddlTo" runat="server"></asp:dropdownlist></td>
							<td align="center" colSpan="2"><asp:button CssClass="Content Color2" id="bSend" onclick="bSend_Click" runat="server" text="Send" align="center"></asp:button></td>
						</tr>
					</TABLE>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<hr>
				</td>
			</tr>
			<tr>
				<td width="100%" colspan='2'>
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td valign="top" align="left">
								<TABLE id="tHeader" border="0">
									<tr>
										<td>
											<P><asp:label CssClass="Header Bold Color2" id="lblNameCompany" runat="server" align="left"></asp:label></P>
										</td>
									</tr>
									<tr>
										<td><asp:label CssClass="Content Color2" id="lblAddress" runat="server"></asp:label></td>
									</tr>
									<tr>
										<td><asp:label CssClass="Content Color2" id="lblPlace" runat="server"></asp:label></td>
									</tr>
								</TABLE>
							</td>
							<td valign="top" align="right">
								<asp:label CssClass="Content Color2" id="lblDate" runat="server"></asp:label>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" colSpan="2">
                    <asp:TextBox ID="txtBody" runat="server" CssClass="InputForm" Width="700px" Height="112px" TextMode="MultiLine"></asp:TextBox><BR>
					<br>
					<br>
					<P align="center"><span class="Header Bold Color2">Statement of Account from The Plastics Exchange</span>
				    <br /><br /></P>
				</td>
			<tr>
				<td align="center" colSpan="2"><asp:datagrid BackColor="000000" CellSpacing="1" BorderWidth="0" id="dg" runat="server"  AutoGenerateColumns="false" AllowSorting="false"
						ShowFooter="True" OnItemDataBound="KeepRunningSum" DataKeyField="ID" CellPadding="2" HorizontalAlign="Center" Width="85%"
						HeaderStyle-CssClass="LinkNormal Bold OrangeColor" AlternatingItemStyle-CssClass="LinkNormal DarkGray" ItemStyle-CssClass="LinkNormal LightGray"
						CssClass="Content" FooterStyle-CssClass="Content Bold Color4 FooterColor" >
						<columns>
							<asp:BoundColumn DataField="ID" ItemStyle-Width="27%" HeaderText="ORDER ID" ItemStyle-Wrap="false" />
							<asp:BoundColumn DataField="SHIPMENT_PO_NUM" HeaderText="PO#" ItemStyle-Wrap="false" />
							<asp:BoundColumn DataField="ORDR_PROD_SPOT" HeaderText="PRODUCT SPEC" ItemStyle-Wrap="false" />
							<asp:BoundColumn DataField="VARDATETAKEN" HeaderText="SHIP DATE" />
							<asp:BoundColumn HeaderText="DUE DATE" />
							<asp:BoundColumn DataField="SHIPMENT_BUYER_TERMS" HeaderText="TERMS" />
							<asp:BoundColumn DataField="DAYSSOFAR" HeaderText="DAYS LATE"  />
							<asp:BoundColumn DataField="OPEN_BALANCE" DataFormatString="{0:c}" HeaderText="OPEN BALANCE" />
						</columns>
					</asp:datagrid></td>
			</tr>
			<tr>
				<td  colSpan="1">
					<TABLE id="tCheck" align="center" border="0" class="Content Color2">
						<tr>
							<td align="left"><span class="Content Color2 Bold"><i>CHECK REMITTANCE INSTRUCTIONS:</i></span></td>
						</tr>
						<tr>
							<td align="left">The Plastics Exchange
							</td>
						</tr>						
						<tr>
							<td align="left">16510 N. 92nd Street. #1010
							</td>
						</tr>
						<tr>
							<td align="left">Scottsdale, AZ 85260
							</td>
						</tr>
					</TABLE>
				</td>
				<td colSpan="1">
					<table id="tTransfer" align="left" class="Content Color2">
						<tr vAlign="top">
							<td><br />
								<br />
								<br />
								<span class="Content Bold Color2"><i>WIRE TRANSFER INSTRUCTIONS: </i></span></td>
						</tr>
						<tr>
							<td align="left">Citibank
							</td>
						</tr>
						<tr>
							<td align="left">Chicago, IL 60654
							</td>
						</tr>
						<tr>
							<td align="left">A.B.A. #271070 801<br />
							</td>
						</tr>
						<tr>
							<td align="left">For Futher Credit:
							</td>
						</tr>
						<tr>
							<td align="left">The Plastics Exchange
							</td>
						</tr>
						<tr>
							<td align="left">Account# 801086470
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</TABLE>
	
</asp:Content>