using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using TPE.Utility;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace localhost.Creditor
{
    public partial class AccountReceivables : System.Web.UI.Page
    {

        double dbTotalWeight = 0;
        double dbTotalPrice = 0.0;
        double dbTotalTax = 0.0;
        double dbTotal = 0.0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C") && ((string)Session["Typ"] != "L"))
            {
                Response.Redirect("../default.aspx");
            }
            if (!IsPostBack)
            {
                HelperFunction.fillDatesDesc(01, 2000, ddlMonth);
                ListItem lstItem = new ListItem("Show All Dates", "*",true);
                ddlMonth.Items.Add(lstItem);
                ddlMonth.SelectedItem.Selected = false;
                ddlMonth.Items[ddlMonth.Items.Count-1].Selected = true;
                
                BindDG();
                lblAddTransaction.Text = "Enter transaction number.";
                lblErrorMessage.Text = "It is already entered into the table.";
                ViewState["Sort"] = "Transaction_ID DESC";
            }
        }

        protected void BindDG()
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append(" SELECT *, VARRESIN = (Buy_Weight * Unit_Price), VARDUE = (Buy_Weight * Unit_Price + Tax) ");
            sbSql.Append(" FROM ACCOUNT_RECEIVABLES ");

            if (!ddlMonth.SelectedItem.Value.Equals("*"))
            {
                int currentMonth = Convert.ToInt32(ddlMonth.SelectedValue.Substring(4, 2).ToString());
                int currentYear = Convert.ToInt32(ddlMonth.SelectedValue.Substring(0, 4).ToString());
                string bymonth = "WHERE ";
                bymonth += " Month(SHIPMENT_DATE)='" + currentMonth + "'";
                bymonth += " AND Year(SHIPMENT_DATE)='" + currentYear + "'";

                sbSql.Append(bymonth);
            }

            if (!ddlSearchIndex.SelectedValue.Equals("*"))
            {
                if (!ddlMonth.SelectedItem.Value.Equals("*"))
                {
                    if (ddlSearchIndex.SelectedValue.Equals("1"))
                        sbSql.Append(" AND Buyer_PO_Number like '%" + txtSearch.Text + "%' ");
                    if (ddlSearchIndex.SelectedValue.Equals("2"))
                        sbSql.Append(" AND Transaction_Number = '" + txtSearch.Text + "' ");
                    if (ddlSearchIndex.SelectedValue.Equals("3"))
                        sbSql.Append(" AND Buyer_Name like '%" + txtSearch.Text + "%' ");
                    if (ddlSearchIndex.SelectedValue.Equals("4"))
                        sbSql.Append(" AND (Buy_Weight * Unit_Price + Tax) = '" + txtSearch.Text + "' ");
                }
                else
                {
                    sbSql.Append(" WHERE ");
                    if (ddlSearchIndex.SelectedValue.Equals("1"))
                        sbSql.Append(" Buyer_PO_Number like '%" + txtSearch.Text + "%' ");
                    if (ddlSearchIndex.SelectedValue.Equals("2"))
                        sbSql.Append(" Transaction_Number = '" + txtSearch.Text + "' ");
                    if (ddlSearchIndex.SelectedValue.Equals("3"))
                        sbSql.Append(" Buyer_Name like '%" + txtSearch.Text + "%' ");
                    if (ddlSearchIndex.SelectedValue.Equals("4"))
                        sbSql.Append(" (Buy_Weight * Unit_Price + Tax) = '" + txtSearch.Text + "' ");
                }
            }

            if (ViewState["Sort"] != null)
            {
                sbSql.Append(" ORDER BY " + ViewState["Sort"].ToString() + " ");
            }
            else
            {
                sbSql.Append(" ORDER BY Transaction_ID DESC");
            }

            SqlDataAdapter dadContent;
            DataSet dstContent;

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                dadContent = new SqlDataAdapter(sbSql.ToString(), conn);
                dstContent = new DataSet();
                dadContent.Fill(dstContent);

                dg.DataSource = dstContent;
                dg.DataBind();
            }
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BindDG();
        }

        protected void dg_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dg.PageIndex = e.NewPageIndex;
            BindDG();
        }

        protected void dg_Sorting(object sender, GridViewSortEventArgs e)
        {
            string[] SortExprs;
            string[] ViewStateExprs;
            string NewSearchMode = "";
            string NewSortExpr;

            Regex r = new Regex(" ");
            SortExprs = r.Split(e.SortExpression);

            Regex v = new Regex(" ");
            ViewStateExprs = v.Split(ViewState["Sort"].ToString());

            if (ViewStateExprs[0] == SortExprs[0])
            {
                if (ViewStateExprs[1].ToString() == "ASC") NewSearchMode = "DESC";
                else NewSearchMode = "ASC";
            }
            else NewSearchMode = "ASC";

            NewSortExpr = SortExprs[0] + " " + NewSearchMode;

            ViewState["Sort"] = NewSortExpr;

            BindDG();

        }

        protected void btnAdd_OnClick(object sender, EventArgs e)
        {
            pnlPanel.Visible = true;
        }

        protected void dg_RowCommand(Object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                //int index = Convert.ToInt32(e.CommandArgument);

                //GridViewRow selectedRow = dg.Rows[index];
                //TableCell trans_ID = selectedRow.Cells[2];                
                //string ID = trans_ID.Text;
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("DELETE FROM ACCOUNT_RECEIVABLES ");
                sbSQL.Append(" WHERE TRANSACTION_ID = '" + dg.DataKeys[Convert.ToInt32(e.CommandArgument)][0].ToString() + "' ");
                DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sbSQL.ToString());
                BindDG();
            }

        }

        protected void dg_RowEditing(object sender, GridViewEditEventArgs e)
        {
            dg.EditIndex = e.NewEditIndex;
            BindDG();
        }

        protected void dg_RowEditing(int rowIndex)
        {
            dg.EditIndex = rowIndex;
            BindDG();
        }

        protected void dg_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            dg.EditIndex = -1;
            BindDG();
        }

        protected void dg_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            StringBuilder sbSQL = new StringBuilder();
            GridViewRow row = dg.Rows[e.RowIndex];
            string Trans_Num, Buyer_PO_Num, Buyer_Name, Ship_Date, Term, Buy_Weight, Unit_Price, Tax, Comment, Flag;
            string Amount_Due = "";
            Trans_Num = ((HyperLink)(row.Cells[2].Controls[0])).Text;
            Buyer_PO_Num = ((TextBox)(row.Cells[3].Controls[0])).Text;
            //Buyer_Name = ((TextBox)(row.Cells[4].Controls[0])).Text;
            Ship_Date = ((TextBox)(row.Cells[5].Controls[0])).Text;            
            Term = ((TextBox)(row.Cells[6].Controls[0])).Text;
            //Buy_Weight = ((TextBox)(row.Cells[7].Controls[0])).Text;
            Buy_Weight = "";
            Unit_Price = "";
            Tax = ((TextBox)(row.Cells[10].Controls[0])).Text;
            Comment = ((TextBox)(row.Cells[12].Controls[0])).Text;

            Flag = "";
            Regex r = new Regex("-");
            string[] id = r.Split(Trans_Num);
            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();
                string strSQL = "select Buyer_Name=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)), Shipment_Date = SHIPMENT_DATE_DELIVERED, Buy_Weight = ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY), Unit_Price = SHIPMENT_BUYR_PRCE, Tax =  ISNULL((SHIPMENT_WEIGHT*SHIPMENT_BUYR_PRCE*SHIPMENT_TAX/100),0), SHIPMENT_BUYER_TERMS from shipment where shipment_ordr_id = '" + id[0].ToString() + "' and shipment_sku = '" + id[1].ToString() + "'";
                using (SqlDataReader dtrTrans = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
                {
                    if (dtrTrans.Read())
                    {
                        //if (Buyer_Name != dtrTrans["Buyer_Name"].ToString().Trim())
                        //{
                        //    Flag = Flag + "N,";
                        //}
                        //if (Ship_Date != dtrTrans["Shipment_Date"].ToString())
                        //{
                        //    Flag = Flag + "D,";
                        //}
                        if (Term != dtrTrans["SHIPMENT_BUYER_TERMS"].ToString())
                        {
                            Flag = Flag + "ST,";
                        }
                        //if (Buy_Weight != dtrTrans["Buy_Weight"].ToString())
                        //{
                        //    Flag = Flag + "W,";
                        //}
                        //if (Unit_Price != dtrTrans["Unit_Price"].ToString())
                        //{
                        //    Flag = Flag + "P,";
                        //}
                        if (dtrTrans["Tax"].ToString() != "")
                        {
                            if (Convert.ToDouble(Tax) != Convert.ToDouble(dtrTrans["Tax"].ToString()))
                            {
                                Flag = Flag + "T,";
                            }
                        }
                        if (Flag != "")
                        {
                            Flag = Flag.Substring(0, Flag.Length - 1);                            
                        }
                        Buy_Weight = dtrTrans["Buy_Weight"].ToString();
                        Unit_Price = dtrTrans["Unit_Price"].ToString();
                        Tax = dtrTrans["Tax"].ToString();
                        Amount_Due = Convert.ToString(Convert.ToDouble(Buy_Weight) * Convert.ToDouble(dtrTrans["Unit_Price"].ToString()) + Convert.ToDouble(Tax));
                        if (dtrTrans["Shipment_Date"].ToString() != "")
                        {
                            Ship_Date = dtrTrans["Shipment_Date"].ToString();
                        }
                    }
                }
            }            

            string SQL = "";
            SQL += "Update shipment ";
            SQL += " Set SHIPMENT_BUYER_TERMS='" + Term + "' ";
            SQL += " Where Shipment_ordr_id='" + id[0].ToString() + "' AND Shipment_sku='" + id[1].ToString() + "' "; 
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), SQL);

            string SQL2 = "";
            SQL2 += "Update cash_received ";
            SQL2 += " Set SHIPMENT_TERM='" + Term + "', SHIPMENT_DATE='" + Ship_Date + "', Amount_Due ='" + Amount_Due + "', Buyer_PO_Number= '" + Buyer_PO_Num + "' ";
            SQL2 += " Where Transaction_Number='" + Trans_Num + "' ";
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), SQL2);

            sbSQL.Append("UPDATE ACCOUNT_RECEIVABLES ");
            sbSQL.Append(" SET Transaction_Number = '" + Trans_Num + "', Buyer_PO_Number= '" + Buyer_PO_Num + "', Buy_Weight= '" + Buy_Weight + "', Unit_Price = '" + Unit_Price + "', ");
            //sbSQL.Append(" SET Transaction_Number = '" + Trans_Num + "', Buyer_PO_Number= '" + Buyer_PO_Num + "', Buyer_Name = '" + Buyer_Name + "',");
            sbSQL.Append("  Shipment_Date = '" + Ship_Date + "', Shipment_Term = '" + Term + "', Tax = '" + Tax + "', Comment = '" + Comment + "',Flag = '" + Flag + "' ");
            //sbSQL.Append(" Shipment_Date = '" + Ship_Date + "', Shipment_Term = '" + Term + "', Buy_Weight = '" + Buy_Weight + "', Unit_Price = '" + Unit_Price + "', Tax = '" + Tax + "', Comment = '" + Comment + "',Flag = '" + Flag + "' ");            
            sbSQL.Append(" WHERE Transaction_ID='" + dg.DataKeys[e.RowIndex][0].ToString() + "';");
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sbSQL.ToString());

            dg.EditIndex = -1;

            BindDG();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            pnlPanel.Visible = false;
            string Trans_Num = "";
            string Buyer_Name = "";
            string Buyer_Comp_ID = "";
            string Ship_Date = "";
            string Term = "";
            string Buy_Weight = "";
            string Unit_Price = "";
            string Tax = "";
            Regex r = new Regex("-");
            string[] id = r.Split(txtID.Text);
            int rowIndex = -1;
            bool foundRecord = false;
            if (id[0] != "" && id[1] != "")
            {
                using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
                {
                    conn.Open();

                    string strSQL1 = "SELECT * from ACCOUNT_RECEIVABLES WHERE Transaction_Number = '" + txtID.Text + "'";
                    using (SqlDataReader dtrTrans = DBLibrary.GetDataReaderFromSelect(conn, strSQL1))
                    {
                        if (dtrTrans.HasRows)
                        {
                            foundRecord = true;
                        }
                    }

                    if (!foundRecord)
                    {
                        string strSQL = "select Transaction_Number = ( CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR) ), Buyer_Name=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)), Buyer_Comp_ID = (select pers_comp from person where pers_id = shipment_buyr), Shipment_Date = ISNULL(SHIPMENT_DATE_TAKE,'01/01/1900'), SHIPMENT_BUYER_TERMS, Buy_Weight = ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY), Unit_Price = SHIPMENT_BUYR_PRCE, Tax =  ISNULL((SHIPMENT_WEIGHT*SHIPMENT_BUYR_PRCE*SHIPMENT_TAX/100),0) from shipment where shipment_ordr_id = '" + id[0].ToString() + "' and shipment_sku = '" + id[1].ToString() + "'";
                        using (SqlDataReader dtrTrans = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
                        {
                            if (dtrTrans.Read())
                            {
                                Trans_Num = dtrTrans["Transaction_Number"].ToString();
                                Buyer_Name = dtrTrans["Buyer_Name"].ToString();
                                Buyer_Comp_ID = dtrTrans["Buyer_Comp_ID"].ToString();
                                Ship_Date = dtrTrans["Shipment_Date"].ToString();
                                Term = dtrTrans["SHIPMENT_BUYER_TERMS"].ToString();
                                Buy_Weight = dtrTrans["Buy_Weight"].ToString();
                                Unit_Price = dtrTrans["Unit_Price"].ToString();
                                Tax = dtrTrans["Tax"].ToString();
                            }
                        }

                        StringBuilder sbSQL = new StringBuilder();
                        sbSQL.Append("INSERT INTO ACCOUNT_RECEIVABLES ");
                        sbSQL.Append(" (Transaction_Number, Buyer_Name, Buyer_Comp_ID, Shipment_Date, Shipment_Term, Buy_Weight, Unit_Price, Tax) ");
                        sbSQL.Append(" VALUES ( '" + Trans_Num + "','" + Buyer_Name.Trim() + "','" + Buyer_Comp_ID + "','" + Ship_Date + "','" + Term + "','" + Buy_Weight + "','" + Unit_Price + "','" + Tax + "' ) ");
                        DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sbSQL.ToString());
                        BindDG();

                        //for (int i = 0; i < dg.Rows.Count; i++)
                        //{
                        //    if (((HyperLink)(row.Cells[2].Controls[0])).Text == Trans_Num)
                        //    {
                        //        rowIndex = i;
                        //    }
                        //}
                        dg_RowEditing(0);
                    }
                    else
                    {
                        pnlPanel2.Visible = true;
                    }   
                }
            }            
            txtID.Text = "";
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtID.Text = "";
            pnlPanel.Visible = false;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            pnlPanel2.Visible = false;
        }

        protected void btnExportToExcel_Click(object sender, System.EventArgs e)
        {
            int iPagS = dg.PageSize;
            dg.PageSize = 100000;

            BindDG();

            //dg.Columns[0].Visible = false;

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=AccountReceivables.xls");

            Response.BufferOutput = true;
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Charset = "UTF-8";
            EnableViewState = false;

            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

            FormatExportedGrid(dg);
            this.ClearControls(dg);
            dg.RenderControl(hw);

            Response.Write(tw.ToString());
            Response.End();

            dg.PageSize = iPagS;
        }

        public void FormatExportedGrid(GridView dg1)
        {
            dg1.BackColor = System.Drawing.Color.White;
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        private void ClearControls(Control control)
        {
            for (int i = control.Controls.Count - 1; i >= 0; i--)
            {
                ClearControls(control.Controls[i]);
            }
            if (!(control is TableCell))
            {
                if (control.GetType().GetProperty("SelectedItem") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    try
                    {
                        literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
                    }
                    catch
                    {
                    }
                    control.Parent.Controls.Remove(control);
                }
                else
                    if (control.GetType().GetProperty("Text") != null)
                    {
                        LiteralControl literal = new LiteralControl();
                        control.Parent.Controls.Add(literal);
                        literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);
                        control.Parent.Controls.Remove(control);
                    }
            }
            return;
        }

        protected void KeepRunningSum(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                dbTotalWeight = 0;
                dbTotalPrice = 0;
                dbTotalTax = 0;
                dbTotal = 0;
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                dbTotalWeight += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Row.DataItem, "Buy_Weight"));
		dbTotalPrice += HelperFunction.ConvertToDouble(DataBinder.Eval(e.Row.DataItem, "VARRESIN"));
		dbTotalTax += HelperFunction.ConvertToDouble(DataBinder.Eval(e.Row.DataItem, "Tax"));
		dbTotal += HelperFunction.ConvertToDouble(DataBinder.Eval(e.Row.DataItem, "VARDUE"));

                if (e.Row.Cells[14] != null)
                    ((Button)e.Row.Cells[14].Controls[0]).Attributes.Add("onClick", "if(!confirm('Are you sure you want to delete the transaction " + ((Button)e.Row.Cells[14].Controls[0]).Text + "?'))return false;");
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[3].Text = "<b>Totals:</b> ";
                e.Row.Cells[7].Text = "<b>" + String.Format("{0:#,###}", dbTotalWeight) + "</b>";
                e.Row.Cells[9].Text = "<b>" + String.Format("{0:c}", dbTotalPrice) + "</b>";
                e.Row.Cells[10].Text = "<b>" + String.Format("{0:c}", dbTotalTax) + "</b>";
                e.Row.Cells[11].Text = "<b>" + String.Format("{0:c}", dbTotal) + "</b>";

            }
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            BindDG();
        }
    }
}
