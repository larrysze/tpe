<%@ Page Language="C#" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" EnableEventValidation = "false" AutoEventWireup="true" CodeBehind="Return_Reject.aspx.cs" Inherits="localhost.Creditor.Return_Reject" Title="Return Reject" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="asp" namespace="System.Web.UI" %>
<%@ MasterType VirtualPath="~/MasterPages/TemplateWithoutInstructions.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCssLink" runat="server">
</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphHeading">
    <span class="Header Bold Color1">Return/Reject Transactions</span></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMain" runat="server">

<table id="Table1" class="Content LinkNormal" align="center" width="100%" cellspacing="0" cellpadding="0" runat="server">
    <tr align="center">
        <td align="center">
            <p><b>Return/Reject</b></p>&nbsp;&nbsp;&nbsp;
            <asp:Button  ID="btnAdd" runat="server" Text="Add Transaction" OnClick="btnAdd_OnClick"/>&nbsp;&nbsp;&nbsp;<asp:Button ID="btnExcel" runat="server" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
            &nbsp;&nbsp;&nbsp;<asp:dropdownlist id="ddlMonth" runat="server" CssClass="InputForm" onselectedindexchanged="ddlMonth_SelectedIndexChanged" AutoPostBack="true"></asp:dropdownlist>
            &nbsp;&nbsp;&nbsp;Search:&nbsp;<asp:TextBox ID="txtSearch" runat="server" Width="80px" ></asp:TextBox>
            &nbsp;<asp:DropDownList ID="ddlSearchIndex" runat="server" AutoPostBack="False">
                    <asp:ListItem Value="*" Text="Select Field" Selected="true"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Product"></asp:ListItem>
                    <asp:ListItem Value="2" Text="Transaction Number"></asp:ListItem>
                    <asp:ListItem Value="3" Text="Seller Name"></asp:ListItem>
                    <asp:ListItem Value="4" Text="Buyer Name"></asp:ListItem>
                    <%-- <<asp:ListItem Value="5" Text="Amount Due"></asp:ListItem>
                    <asp:ListItem Value="6" Text="Amount Paid"></asp:ListItem>--%>
                    </asp:DropDownList>
            &nbsp;<asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_OnClick" Text="Search" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
</table>

<asp:ScriptManager id="ScriptManager1" runat="Server" />    
<asp:UpdatePanel id="UpdatePanel1" runat="Server" >
        <ContentTemplate>
            <table id="Table2" class="Content LinkNormal" align="center" width="100%" cellspacing="0" cellpadding="0" runat="server">    
                <tr align="center">
                    <td align="center">           
                        <asp:GridView ID="dg" CssClass="DataGrid" AllowSorting="true" AllowPaging="true" runat="server" AutoGenerateEditButton="true" DataKeyNames="Transaction_ID" 
                            AutoGenerateColumns="False" ShowFooter="true" PageSize="25" OnSorting="dg_Sorting" OnPageIndexChanging="dg_PageIndexChanging" onrowcommand="dg_RowCommand"
                            OnRowEditing="dg_RowEditing" OnRowCancelingEdit="dg_RowCancelingEdit" OnRowUpdating="dg_RowUpdating"   OnRowDataBound="KeepRunningSum">                 				
				            <HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
				            <FooterStyle CssClass="Content Color4 Bold FooterColor" />
                            <Columns>
                              <asp:BoundField ReadOnly="true" DataField="Transaction_ID" SortExpression="Transaction_ID" Visible="false"/>
                              <asp:HyperLinkField HeaderText="ID" DataNavigateUrlFields="Transaction_Number" DataNavigateUrlFormatString="../administrator/Transaction_Details.aspx?Referer=/Creditor/Transaction_Summary.aspx&amp;ID={0}" DataTextField="Transaction_Number"  />
                              <asp:BoundField ReadOnly="true" HeaderText="Shipment Date" DataField="Shipment_Date" SortExpression="Shipment_Date" DataFormatString="{0:MM/dd/yyyy}"/>                                                       
                              <asp:BoundField ReadOnly="true" HeaderText="Product" DataField="Product" SortExpression="Product" />
                              <asp:BoundField ReadOnly="true" HeaderText="Weight" DataField="Weight" SortExpression="Weight" />
                              <asp:BoundField ReadOnly="true" HeaderText="From Buyer" DataField="From_Buyer" SortExpression="From_Buyer" />
                              <asp:BoundField ReadOnly="true" HeaderText="Buyer Paid" DataField="Buyer_Paid" SortExpression="Buyer_Paid" />
                              <asp:BoundField ReadOnly="true" HeaderText="Buy Price" DataField="Buy_Price" SortExpression="Buy_Price" />
                              <asp:BoundField ReadOnly="true" HeaderText="Sell Price" DataField="Sell_Price" SortExpression="Sell_Price" />
                              <asp:BoundField ReadOnly="true" HeaderText="To Seller" DataField="To_Seller" SortExpression="To_Seller" />
                              <asp:BoundField ReadOnly="true" HeaderText="Seller Paid" DataField="Seller_Paid" SortExpression="Seller_Paid" />
                              <asp:BoundField ReadOnly="true" HeaderText="Buyer Name" DataField="Buyer_Name" SortExpression="Buyer_Name" />
                              <asp:BoundField ReadOnly="true" DataField="Buyer_Comp_ID" SortExpression="Buyer_Comp_ID" Visible="false"/>
                              <asp:BoundField ReadOnly="true" HeaderText="Seller Name" DataField="Seller_Name" SortExpression="Seller_Name" />
                              <asp:BoundField ReadOnly="true" DataField="Seller_Comp_ID" SortExpression="Seller_Comp_ID" Visible="false"/>
                              <asp:BoundField HeaderText="Reason" DataField="Reason" SortExpression="Reason" />                              
                              <asp:BoundField HeaderText="Action Taken" DataField="Result" SortExpression="Result" />                                
                              <%--<asp:ButtonField buttontype="Button" DataTextField="Transaction_Number" HeaderText="Delete" CommandName="Select"/>--%>                              
                            </Columns>
                        </asp:GridView>          
                    </td>
                </tr>
            </table>
            <br />

            <asp:Panel id="pnlPanel" runat="server" Visible="false" style="POSITION: absolute; TOP: 200px; LEFT: 600px" >
                <table width="200px" style="background-color:White" border="0" >
                    <tbody>
                        <tr>
                            <td align="center"><asp:Label id="lblAddTransaction" runat="server" CssClass="Content Color3 Bold"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:TextBox id="txtID" runat="server" CssClass="InputForm" TextMode="SingleLine"></asp:TextBox> 
                                <asp:Button id="btnSubmit" AutoPostBack="False" onclick="btnSubmit_Click" runat="server" CssClass="Content Color2" Text="Submit"></asp:Button> 
                                <asp:Button id="btnCancel" AutoPostBack="False" onclick="btnCancel_Click" runat="server" CssClass="Content Color2" Text="Cancel"></asp:Button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel> 
            <asp:Panel id="pnlPanel2" runat="server" Visible="false" style="POSITION: absolute; TOP: 300px; LEFT: 600px" >
                <table width="200px" style="background-color:White" border="0" >
                    <tbody>
                        <tr>
                            <td align="center"><asp:Label id="lblErrorMessage" runat="server" CssClass="Content Color3 Bold"></asp:Label>
                            </td>                        
                        </tr>
                        <tr>
                            <td align="center">
                            <asp:Button id="btnClose" AutoPostBack="False" onclick="btnClose_Click" runat="server" CssClass="Content Color2" Text="Close" />                            
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </ContentTemplate>   
    </asp:UpdatePanel> 
</asp:Content>
