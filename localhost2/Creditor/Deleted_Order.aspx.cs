using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.Web.SessionState;
using System.Text.RegularExpressions;
using TPE.Utility;

namespace localhost.Creditor
{
	public partial class Deleted_Order : System.Web.UI.Page
	{

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{	
			this.dg.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dg_SortCommand);
			this.dg.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_ItemCommand);
		}
		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			
			string sessionType = (string)Session["Typ"];	//Authority Checking
			if ((sessionType != "A") && (sessionType != "C"))
				Response.Redirect("../default.aspx");

			Session["CompID"] = "";
			Master.Width = "1200px";

			if (!IsPostBack)
			{
				CreateIndexList();
				CreateDateList();
				Bind();
			}
		}

		protected void CreateIndexList()
		{
			ddlIndex.Items.Clear(); // starts the list out fresh
			ddlIndex.Items.Add(new ListItem("Select Field", "*"));
			ddlIndex.Items.Add(new ListItem("Order ID", "1"));
			ddlIndex.Items.Add(new ListItem("PO number", "2"));
			ddlIndex.Items.Add(new ListItem("Shipment FOB", "3"));
			ddlIndex.Items.Add(new ListItem("Buyer Comp.", "4"));
			ddlIndex.Items.Add(new ListItem("Seller Comp.", "5"));
			ddlIndex.Items.Add(new ListItem("Freight Cost", "6"));
			ddlIndex.Items.Add(new ListItem("Warehouse Cost", "7"));
			ddlIndex.Items.Add(new ListItem("Broker Name", "8"));
			ddlIndex.Items.Add(new ListItem("Railcar #", "9"));
			ddlIndex.Items.Add(new ListItem("Transaction Date", "10"));
		}

		protected void CreateDateList()
		{
			ddlMonth.Items.Clear(); // starts the list out fresh
			HelperFunction.fillDatesDesc(01, 2000, ddlMonth);
			ListItem lstItem = new ListItem("Show All Dates", "*", true);
			ddlMonth.Items.Add(lstItem);
			ddlMonth.SelectedItem.Selected = false;
			ddlMonth.Items[0].Selected = true;
		}

		private void Bind()
		{
			StringBuilder sbSql = new StringBuilder();
			sbSql.Append(GetSQL());


			if( !ddlMonth.SelectedItem.Value.Equals("*") || txtSearch.Text != "" )
				sbSql.Append(" WHERE transaction_date > ='12/1/97' ");  // random date prior to the company founding.  this will always be true

			if( !ddlMonth.SelectedItem.Value.Equals("*") )
			{
				string bymonth = "";
				string strDate = ddlMonth.SelectedItem.Value.ToString();
				bymonth += " AND MONTH(transaction_date)='" + strDate.Substring(4, 2) + "'";
				bymonth += " AND YEAR(transaction_date)='" + strDate.Substring(0, 4) + "'";

				sbSql.Append(bymonth);
			}

			if( txtSearch.Text != "" )
			{		
				string searchString = txtSearch.Text;
				string search = "";
				String[] aux;

				if( searchString.Contains("-") )
				{
					char[] sep = new Char[1];
					sep[0] = '-';
					aux = searchString.Split(sep);
				}
				else
				{
					aux = new string[1];
					aux[0] = searchString;
				}
				
				if( !ddlIndex.SelectedValue.Equals("*") )
				{
					int fieldIndex = Convert.ToInt32(ddlIndex.SelectedValue);

					switch(fieldIndex){
						case 1: search = search + " SHIPMENT_ORDR_ID LIKE '%" + aux[0] + "%'"; break;
						case 2: search = search + " SHIPMENT_PO_NUM LIKE '%" + searchString + "%'"; break;
						case 3: search = search + " SHIPMENT_FOB LIKE '%" + searchString + "%'"; break;
						case 4: search = search + " BUYR LIKE '%" + searchString + "%'"; break;
						case 5: search = search + " SELR LIKE '%" + searchString + "%'"; break;
						case 6: search = search + " VARFREIGHT LIKE '%" + searchString + "%'"; break;
						case 7: search = search + " VARWHAREHOUSE LIKE '%" + searchString + "%'"; break;
						case 8: search = search + " BROKER LIKE '%" + searchString + "%'"; break;
						case 9: search = search + " shipment_comment LIKE '%" + searchString + "%'"; break;
						case 10:
							DateTime searchDate;
							if( DateTime.TryParse(searchString, out searchDate) )
							{
								search = search + " DAY(transaction_date)='" + searchDate.Day + "'";
								search = search + " AND MONTH(transaction_date)='" + searchDate.Month + "'";
								search = search + " AND YEAR(transaction_date)='" + searchDate.Year + "'";
							}
							break;
					}
				}
				else
				{
					search = search + "  SHIPMENT_ORDR_ID LIKE '%" + aux[0] + "%'";
					search = search + " OR BUYR LIKE '%" + searchString + "%'";
					search = search + " OR SELR LIKE '%" + searchString + "%'";
					search = search + " OR BROKER LIKE '%" + searchString + "%'";
					search = search + " OR VARFREIGHT LIKE '%" + searchString + "%'";
					search = search + " OR VARWAREHOUSE LIKE '%" + searchString + "%'";
					search = search + " OR SHIPMENT_FOB LIKE '%" + searchString + "%'";
					search = search + " OR SHIPMENT_PO_NUM LIKE '%" + searchString + "%'";
					search = search + " OR shipment_comment LIKE '%" + searchString + "%'";
					DateTime searchDate;
					if( DateTime.TryParse(searchString, out searchDate) )
					{
						search = search + " OR DAY(transaction_date)='" + searchDate.Day + "'";
						search = search + " AND MONTH(transaction_date)='" + searchDate.Month + "'";
						search = search + " AND YEAR(transaction_date)='" + searchDate.Year + "'";
					}
				}
				sbSql.Append(" AND " + search);
			}

			DataTable dtContent;
			SqlDataAdapter dadContent;

			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				dadContent = new SqlDataAdapter(sbSql.ToString(), conn);
				dtContent = new DataTable();
				dadContent.Fill(dtContent);
				
				DataView dv = new DataView(dtContent);

				if (ViewState["SortExpression"] != null)
				{
					dv.Sort = ViewState["SortExpression"].ToString();
					if (ViewState["SortDirection"] != null)
						dv.Sort += " " + ViewState["SortDirection"].ToString();
				}
				dg.DataSource = dv;
				dg.DataBind();

				if ((dg.Items.Count == 0))
				{
					dg.Visible = false;
					lblMessage.Visible = true;
				}
				else
				{
					dg.Visible = true;
					lblMessage.Visible = false;
				}
			}
		}

		private string GetSQL()
		{
			string sbSql = " SELECT * FROM ( SELECT *, VARFREIGHT = ISNULL(SHIPMENT_SHIP_PRCE_INV,SHIPMENT_SHIP_PRCE_EST), VARWAREHOUSE = ISNULL(SHIPMENT_WHAREHOUSE_PRCE_INV,SHIPMENT_WHAREHOUSE_PRCE_EST), buyr=(SELECT LEFT(comp_name,12)+'...' FROM company WHERE comp_id=(SELECT pers_comp FROM person WHERE pers_id=shipment_buyr)),  selr=(SELECT LEFT(comp_name,12)+'...' FROM company WHERE comp_id=(SELECT pers_comp FROM person WHERE pers_id=shipment_selr)), selr_id=(SELECT pers_comp FROM person WHERE pers_id=shipment_selr), buyr_id=(SELECT pers_comp FROM person WHERE pers_id=shipment_buyr), broker=(SELECT pers_frst_name FROM person WHERE pers_id=shipment_broker_id ), tvselr=shipment_prce*ISNULL(shipment_weight,shipment_size*shipment_qty) * (100 + ISNULL(shipment_tax, 0)) / 100, sellprice = LEFT(shipment_prce,6), buyprice = LEFT(shipment_buyr_prce,6), ppaid=ISNULL((SELECT SUM(pay_amnt) FROM cancelled_payment WHERE pay_shipment=shipment_id AND pay_pers=shipment_buyr),0), spaid=ISNULL((SELECT SUM(pay_amnt) FROM cancelled_payment WHERE pay_shipment=shipment_id AND pay_pers=shipment_selr),0), tvbuyr=((100 + ISNULL(shipment_tax, 0)) / 100) * shipment_buyr_prce*ISNULL(shipment_weight,shipment_size*shipment_qty), shipment_cmt=ISNULL(shipment_comment, '-'), weight_lbs=ISNULL(shipment_weight,shipment_qty*shipment_size) , product=LEFT(ISNULL(Shipment_Product_Spot,ISNULL(ORDR_PROD_SPOT,(SELECT CONT_LABL FROM CONTRACT WHERE CONT_ID=ORDR_CONT))),20)+'...' , transaction_date=ISNULL(shipment_transaction_date,ordr_date), transaction_number=(CAST(shipment_ordr_id AS VARCHAR) + '-' + CAST(shipment_sku AS VARCHAR)) FROM orders o, cancelled_shipment cs, place p WHERE o.ordr_id=cs.shipment_ordr_id AND p.plac_id=cs.shipment_from )S1 ";

			return sbSql;
		}

		protected void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
			Bind();
			
		}

		private void UndeleteOrder( System.Web.UI.WebControls.DataGridCommandEventArgs e )
		{
			//find the order from cancel_shipment
			string shipmentID = null;
			string shipmentOrderID = null;
			string shipmentSKU = null;
            string fieldString = ((HyperLink)e.Item.Cells[1].Controls[0]).Text;
			shipmentOrderID = fieldString.Substring(0, fieldString.IndexOf("-"));
			shipmentSKU = fieldString.Substring(fieldString.IndexOf("-") + 1, fieldString.Length - fieldString.IndexOf("-") - 1);


			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			try
			{
				conn.Open();
				SqlCommand cmdShipment;
				SqlDataReader dtrShipment;
				cmdShipment = new SqlCommand("SELECT * FROM SHIPMENT WHERE SHIPMENT_ORDR_ID = " + Convert.ToInt32(shipmentOrderID) + " AND SHIPMENT_SKU = '" + shipmentSKU + "' ", conn);
				dtrShipment = cmdShipment.ExecuteReader();
				
				try
				{
					if( dtrShipment.HasRows )
					{
						lblResult.Text = "Result: Undelete Failed , Transaction " + shipmentOrderID + "-" + shipmentSKU + " already exists.";
						dtrShipment.Close();
						conn.Close();
						return; 
					}
				}
				finally
				{
					dtrShipment.Close();
				}
			}
			finally
			{
				conn.Close();
			}


			conn = new SqlConnection(Application["DBConn"].ToString());
			try
			{
				conn.Open();
				SqlCommand cmdShipment;
				SqlDataReader dtrShipment;
				cmdShipment = new SqlCommand("SELECT * FROM CANCELLED_SHIPMENT WHERE SHIPMENT_ORDR_ID = " + Convert.ToInt32(shipmentOrderID) + " AND SHIPMENT_SKU = '" + shipmentSKU + "' ", conn);
				dtrShipment = cmdShipment.ExecuteReader();
				try
				{
					while( dtrShipment.Read() )
					{
						shipmentID = dtrShipment["SHIPMENT_ID"].ToString();
						lblResult.Text = "Result: You have just undeleted the Transaction " + shipmentOrderID + "-" + shipmentSKU + "";
					}
				}
				finally
				{
					dtrShipment.Close();
				}
			}
			finally
			{
				conn.Close();
			}


			//copy the order to shipment from cancel_shipment
			//delete the order from cancel_shipment
			//reset the flag at orders to 0
			Hashtable ht = new Hashtable();
			ht.Add("@ShipmentID", shipmentID);
			ht.Add("@OrderID", shipmentOrderID);
			DBLibrary.ExecuteStoredProcedure(Application["DBConn"].ToString(), "spUnDeleteTransaction", ht);

			Bind();
		}

		private void dg_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName == "UnDelete")
			{
				this.UndeleteOrder(e);
			}
		}

		private void dg_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			if (ViewState["SortDirection"] == null)
				ViewState.Add("SortDirection", "ASC");
			else
			{
				if (ViewState["SortDirection"].ToString() == "ASC")
				{
					ViewState["SortDirection"] = "DESC";
				}
				else
				{
					ViewState["SortDirection"] = "ASC";
				}
			}
			ViewState["SortExpression"] = e.SortExpression.ToString();
			dg.CurrentPageIndex = 0;
			Bind();
		}

		protected void ItemDataBounder(object sender, DataGridItemEventArgs e)
		{
			// setting the mouseover color
			if( e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer )
			{
				e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");
				if( e.Item.ItemType == ListItemType.Item )
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee'");
				}
				else
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3'");
				}
			}
		}

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			dg.CurrentPageIndex = 0;
			// set all drop down lists to the default. Search against everything.
			ddlMonth.SelectedItem.Selected = false;
			ddlMonth.Items.FindByValue("*").Selected = true;
			Bind();
			txtSearch.Text = null;
		}

		protected void btnExportToExcel_Click(object sender, System.EventArgs e)
		{
			Bind();

			dg.Columns[0].Visible = false;
			Response.Clear();
			Response.Buffer = true;
			Response.ContentType = "application/vnd.ms-excel";
			Response.AddHeader("Content-Disposition", "attachment; filename=DeletedOrder_Summary.xls");

			Response.BufferOutput = true;
			Response.ContentEncoding = System.Text.Encoding.UTF8;
			Response.Charset = "";
			EnableViewState = false;

			System.IO.StringWriter tw = new System.IO.StringWriter();
			System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

			dg.BackColor = System.Drawing.Color.White;
			this.ClearControls(dg);
			dg.RenderControl(hw);

			Response.Write(tw.ToString());
			Response.End();
		}

		private void ClearControls( Control control )
		{
			for( int i = control.Controls.Count - 1; i >= 0; i-- )
			{
				ClearControls(control.Controls[i]);
			}
			if( !( control is TableCell ) )
			{
				if( control.GetType().GetProperty("SelectedItem") != null )
				{
					LiteralControl literal = new LiteralControl();
					control.Parent.Controls.Add(literal);
					try
					{
						literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
					}
					catch
					{
					}
					control.Parent.Controls.Remove(control);
				}
				else
					if( control.GetType().GetProperty("Text") != null )
					{
						LiteralControl literal = new LiteralControl();
						control.Parent.Controls.Add(literal);
						literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);
						control.Parent.Controls.Remove(control);
					}
			}
			return;
		}
	}
}
