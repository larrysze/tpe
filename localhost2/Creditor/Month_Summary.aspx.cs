using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Text;
using TPE.Utility;

namespace localhost.Creditor
{
	/// <summary>
	/// Summary description for Month_Summary.
	/// </summary>
	public partial class Month_Summary : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, EventArgs e)
		{
			//Administrator and Creditor have access
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C"))
			{
				Response.Redirect("/default.aspx");
			}
            Master.Width = "1000px";
			if (!IsPostBack)
			{
				ViewState["Sort"] ="VARYEAR DESC,VARMONTH DESC";
			
			}


			dbMargin = 0.0;            
			dbTotalCommision = 0.0;
			dbTotalTVBuyr = 0.0;
			dbTotalTVSelr = 0.0;
			dbTotalFreight = 0.0;
			iTotalWeight = 0;
			dWharehouse = 0;

			Bind();
    
		}
		private void Bind()
		{
			double interest = HelperFunction.getInterestBankParameter(this.Context);
			double limit = HelperFunction.getInterestLimitParameter(this.Context);
			
			DataSet dstContent;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();

            Hashtable htParams = new Hashtable();
            htParams.Add("@interest", interest.ToString());
            htParams.Add("@limit", limit.ToString());
            htParams.Add("@sort", ViewState["Sort"].ToString());

            dstContent = DBLibrary.GetDataSetFromStoredProcedure(conn, "spMonth_Summary", htParams);

            dg.DataSource = dstContent;

            try
            {
                dg.DataBind();
            }
            catch
            {
                //Response.Write(ex.Message + " " + ex.InnerException);
            }
            
			conn.Close();
    
		}
		// <summary>
		// Sorting function
		// </summary>
		public void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{
    
			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;
    
			//  Parse the sort expression - delimiter space
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
			ColumnToSort = SortExprs[0];
			// If a sort order is specified get it, else default is descending
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DeSC";
			}
    
			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;
    
			// Figure out the column index
			int iIndex;
			iIndex = 1;
			switch(ColumnToSort.ToUpper())
			{
				case "VARWEIGHT":
					iIndex = 1;
					break;
				case "VARRECEIVABLES":
					iIndex = 2;
					break;
				case "VARPAYABLES":
					iIndex = 3;
					break;
				case "VARWHAREHOUSE":
				    iIndex = 4;
				    break;
				case "VARFREIGHT":
					iIndex = 5;
					break;
				case "VARCOMM":
					iIndex = 6;
					break;
				//case "VARINTEREST":
				//    iIndex = 7;
				//    break;

				case "VARMARGIN":
					iIndex = 7;
					break;
			}
			// alter the column's sort expression
			dg.Columns[iIndex].SortExpression = NewSortExpr;
    
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
    
			// store the new SortExpr in a labl for use by page function
			ViewState["Sort"] =NewSortExpr;
			// Sort the data in new order
    
			Bind();
		}
    
		double dbMargin = 0.0;
		double dbTotalCommision = 0.0;
		double dbTotalTVBuyr = 0.0;
		double dbTotalTVSelr = 0.0;
		double dbTotalFreight = 0.0;
		double iTotalWeight = 0;
		double dWharehouse = 0;

	
    
    
		protected void OnDataGridBind(object sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");
				e.Item.Attributes.Add("onclick", "window.location ='/Creditor/Transaction_Summary.aspx?ShowDate="+ (DataBinder.Eval(e.Item.DataItem, "VARMONTH")).ToString()+"-"+(DataBinder.Eval(e.Item.DataItem, "VARYEAR")).ToString() +"' ");
				if (e.Item.ItemType == ListItemType.Item)
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee'");
				}
				else
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3'");
				}
    
    
				// sums the items for calculation at bottom
				dbMargin += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARMARGIN"));
				dbTotalCommision += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARCOMM"));
				dbTotalFreight += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARFREIGHT"));
				iTotalWeight += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARWEIGHT"));
				dbTotalTVBuyr += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARRECEIVABLES"));
				dbTotalTVSelr += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARPAYABLES"));

				dWharehouse += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARWHAREHOUSE"));
    
				e.Item.Cells[0].Text =(DataBinder.Eval(e.Item.DataItem, "VARMONTH")).ToString()+"/"+(DataBinder.Eval(e.Item.DataItem, "VARYEAR")).ToString();
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[0].Text = "<b>Totals:</b> " ;
				e.Item.Cells[1].Text = "<b>"+String.Format("{0:#,###}", iTotalWeight)+"</b>";
				e.Item.Cells[2].Text = "<b>"+String.Format("{0:c}", dbTotalTVBuyr)+"</b>";
				e.Item.Cells[3].Text = "<b>"+String.Format("{0:c}", dbTotalTVSelr)+"</b>";
				e.Item.Cells[4].Text = "<b>" + String.Format("{0:c}", dWharehouse) + "</b>";
				e.Item.Cells[5].Text = "<b>"+String.Format("{0:c}", dbTotalFreight)+"</b>";
				e.Item.Cells[6].Text = "<b>"+String.Format("{0:c}", dbTotalCommision)+"</b>";				
				e.Item.Cells[7].Text = "<b>"+String.Format("{0:c}", dbMargin)+"</b>";
    
			}
		}

		protected void btnExportToExcel_Click(object sender, System.EventArgs e)
		{
		    //export to excel

		
		    Bind();

		    Response.Clear();
		    Response.Buffer = true;
		    Response.ContentType = "application/vnd.ms-excel";
		    Response.AddHeader("Content-Disposition", "attachment; filename=monthly_summary.xls");

		    Response.BufferOutput = true;
		    Response.ContentEncoding = System.Text.Encoding.UTF8;
		    Response.Charset = "";
		    EnableViewState = false;

		    System.IO.StringWriter tw = new System.IO.StringWriter();
		    System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

		    FormatExportedGrid(dg);
		    this.ClearControls(dg);
		    dg.RenderControl(hw);



		    Response.Write(CleanHTML(tw.ToString()));
		    Response.End();

		}

		public void FormatExportedGrid(DataGrid dg1)
		{
		    dg1.BackColor = System.Drawing.Color.White;
		}


		private string CleanHTML(string OutOut)
		{

		    //will add some code to fix gridlines

		    return OutOut;
		}


		public override void VerifyRenderingInServerForm(Control control)
		{
		    /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
		       server control at run time. */
		}

		private void ClearControls(Control control)
		{
		    for (int i = control.Controls.Count - 1; i >= 0; i--)
		    {
			ClearControls(control.Controls[i]);
		    }
		    if (!(control is TableCell))
		    {
			if (control.GetType().GetProperty("SelectedItem") != null)
			{
			    LiteralControl literal = new LiteralControl();
			    control.Parent.Controls.Add(literal);
			    try
			    {
				literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
			    }
			    catch
			    {
			    }
			    control.Parent.Controls.Remove(control);
			}
			else
			    if (control.GetType().GetProperty("Text") != null)
			    {
				LiteralControl literal = new LiteralControl();
				control.Parent.Controls.Add(literal);
				literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);
				control.Parent.Controls.Remove(control);
			    }
		    }
		    return;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
