using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;
using TPE.Utility;
using System.Text.RegularExpressions;

namespace localhost.Creditor
{
    public partial class CashPaid : System.Web.UI.Page
    {            
        double dbTotalDue = 0;
        double dbTotalPaid = 0.0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C") && ((string)Session["Typ"] != "L"))
            {
                Response.Redirect("../default.aspx");
            }
            if (!IsPostBack)
            {
                HelperFunction.fillDatesDesc(01, 2000, ddlMonth);
                ListItem lstItem = new ListItem("Show All Dates", "*", true);
                ddlMonth.Items.Add(lstItem);
                ddlMonth.SelectedItem.Selected = false;
                ddlMonth.Items[ddlMonth.Items.Count - 1].Selected = true;

                BindDG();
                lblAddTransaction.Text = "Enter transaction number.";
                lblErrorMessage.Text = "It is already entered into the table.";
                lblMessage.Text = "There is no entry in the system, please add it at the account payables page first.";
                ViewState["Sort"] = "Transaction_ID DESC";
            }
        }

        protected void BindDG()
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append(" SELECT * FROM CASH_PAID ");
            if (!ddlMonth.SelectedItem.Value.Equals("*"))
            {
                int currentMonth = Convert.ToInt32(ddlMonth.SelectedValue.Substring(4, 2));
                int currentYear = Convert.ToInt32(ddlMonth.SelectedValue.Substring(0, 4));
                string bymonth = "WHERE ";
                bymonth += " Month(WIRE_DATE)='" + currentMonth + "'";
                bymonth += " AND Year(WIRE_DATE)='" + currentYear + "'";

                sbSql.Append(bymonth);
            }

            if (!ddlSearchIndex.SelectedValue.Equals("*"))
            {
                if (!ddlMonth.SelectedItem.Value.Equals("*"))
                {
                    if (ddlSearchIndex.SelectedValue.Equals("1"))
                        sbSql.Append(" AND Seller_Invoice_Number like '%" + txtSearch.Text + "%' ");
                    if (ddlSearchIndex.SelectedValue.Equals("2"))
                        sbSql.Append(" AND Transaction_Number = '" + txtSearch.Text + "' ");
                    if (ddlSearchIndex.SelectedValue.Equals("3"))
                        sbSql.Append(" AND Seller_Name like '%" + txtSearch.Text + "%' ");
                    if (ddlSearchIndex.SelectedValue.Equals("4"))
                        sbSql.Append(" AND Wire_Date = '" + txtSearch.Text + "' ");
                    if (ddlSearchIndex.SelectedValue.Equals("5"))
                        sbSql.Append(" AND Amount_Due = '" + txtSearch.Text + "' ");
                    if (ddlSearchIndex.SelectedValue.Equals("6"))
                        sbSql.Append(" AND Amount_Paid = '" + txtSearch.Text + "' ");
                }
                else
                {
                    sbSql.Append(" WHERE ");
                    if (ddlSearchIndex.SelectedValue.Equals("1"))
                        sbSql.Append(" Seller_Invoice_Number like '%" + txtSearch.Text + "%' ");
                    if (ddlSearchIndex.SelectedValue.Equals("2"))
                        sbSql.Append(" Transaction_Number = '" + txtSearch.Text + "' ");
                    if (ddlSearchIndex.SelectedValue.Equals("3"))
                        sbSql.Append(" Seller_Name like '%" + txtSearch.Text + "%' ");
                    if (ddlSearchIndex.SelectedValue.Equals("4"))
                        sbSql.Append(" Wire_Date = '" + txtSearch.Text + "' ");
                    if (ddlSearchIndex.SelectedValue.Equals("5"))
                        sbSql.Append(" Amount_Due = '" + txtSearch.Text + "' ");
                    if (ddlSearchIndex.SelectedValue.Equals("6"))
                        sbSql.Append(" Amount_Paid = '" + txtSearch.Text + "' ");
                }
            }

            if (ViewState["Sort"] != null)
            {
                sbSql.Append(" ORDER BY "+ViewState["Sort"].ToString()+" ");
            }
            else 
            {
                sbSql.Append(" ORDER BY Transaction_ID DESC");
            }            

            SqlDataAdapter dadContent;
            DataSet dstContent;

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                dadContent = new SqlDataAdapter(sbSql.ToString(), conn);
                dstContent = new DataSet();
                dadContent.Fill(dstContent);

                dg.DataSource = dstContent;
                dg.DataBind();                
            }
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BindDG();
        }

        protected void dg_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dg.PageIndex = e.NewPageIndex;
            BindDG();
        }

        protected void dg_Sorting(object sender, GridViewSortEventArgs e)
        {
            string[] SortExprs;
            string[] ViewStateExprs;
            string NewSearchMode = "";
            string NewSortExpr;

            Regex r = new Regex(" ");
            SortExprs = r.Split(e.SortExpression);

            Regex v = new Regex(" ");
            ViewStateExprs = v.Split(ViewState["Sort"].ToString());

            if (ViewStateExprs[0] == SortExprs[0])
            {
                if (ViewStateExprs[1].ToString() == "ASC") NewSearchMode = "DESC";
                else NewSearchMode = "ASC";
            }
            else NewSearchMode = "ASC";

            NewSortExpr = SortExprs[0] + " " + NewSearchMode;

            ViewState["Sort"] = NewSortExpr;

            BindDG();

        }

        protected void btnAdd_OnClick(object sender, EventArgs e)
        {
            pnlPanel.Visible = true;
        }

        protected void dg_RowCommand(Object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                //int index = Convert.ToInt32(e.CommandArgument);

                //GridViewRow selectedRow = dg.Rows[index];
                //TableCell trans_ID = selectedRow.Cells[2];
                //TableCell Comp_ID = selectedRow.Cells[13];
                //string ID = trans_ID.Text;
                //Session["CompID"] = Comp_ID.Text;
                //Response.Redirect("/Creditor/Edit_Payments.aspx?ID=" + ID);

                StringBuilder sbSQL = new StringBuilder();
                string Payment_ID = dg.Rows[Convert.ToInt32(e.CommandArgument)].Cells[5].Text;
                if (Payment_ID != "&nbsp;")
                {
                    sbSQL.Append("DELETE FROM PAYMENT ");
                    sbSQL.Append("WHERE PAY_ID = '" + Payment_ID + "' ");
                    DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sbSQL.ToString());
                } 

                StringBuilder sbSQL2 = new StringBuilder();
                sbSQL2.Append("DELETE FROM CASH_PAID ");
                sbSQL2.Append(" WHERE TRANSACTION_ID = '" + dg.DataKeys[Convert.ToInt32(e.CommandArgument)][0].ToString() + "' ");
                DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sbSQL2.ToString());
                BindDG();

            }

        }

        protected void dg_RowEditing(object sender, GridViewEditEventArgs e)
        {           
            dg.EditIndex = e.NewEditIndex;            
            BindDG();
        }

        protected void dg_RowEditing(int rowIndex)
        {
            dg.EditIndex = rowIndex;
            BindDG();
        }

        protected void dg_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {         
            dg.EditIndex = -1;         
            BindDG();
        }

	protected void dg_RowUpdating(object sender, GridViewUpdateEventArgs e)
	{
		StringBuilder sbSQL = new StringBuilder();            
		GridViewRow row = dg.Rows[e.RowIndex];
		string Trans_Num, Seller_Inv_Num, Seller_Name, Wire_Payment_Num, Payment_Number, Ship_Date, Wire_Date, Comment, Amount_Paid;
		Trans_Num = ((HyperLink)(row.Cells[2].Controls[0])).Text;
		//Seller_Inv_Num = ((TextBox)(row.Cells[3].Controls[0])).Text;
		//Seller_Name = ((TextBox)(row.Cells[4].Controls[0])).Text;
		Payment_Number = ((TextBox)(row.Cells[5].Controls[0])).Text;
		Wire_Payment_Num = ((TextBox)(row.Cells[6].Controls[0])).Text;
		//Ship_Date = ((TextBox)(row.Cells[7].Controls[0])).Text;
		Wire_Date = ((TextBox)(row.Cells[8].Controls[0])).Text;
		//Amount_Due = ((TextBox)(row.Cells[9].Controls[0])).Text;
		Amount_Paid = ((TextBox)(row.Cells[10].Controls[0])).Text;            
		Comment = ((TextBox)(row.Cells[11].Controls[0])).Text;

		//Larry--Update the payment to Payment table.
		//===================================================================================
		SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
		conn.Open();

		SqlCommand cmdSQL = new SqlCommand("Select  Amount_Due,Amount_Paid,Transaction_Id from Cash_Paid WHERE transaction_number='" + Trans_Num  + "'", conn);
		SqlDataReader dtrSQL = cmdSQL.ExecuteReader();

		string strAmount_Due=null;
		double totalAmount_Paid=0;

		for( int i = 0; dtrSQL.Read(); i++ )
		{
			if( i == 0 )
				strAmount_Due = dtrSQL["Amount_Due"].ToString();
			if( Convert.ToString(dtrSQL["Transaction_Id"]) != dg.DataKeys[e.RowIndex][0].ToString())
				totalAmount_Paid += Convert.ToDouble(Convert.ToString(dtrSQL["Amount_Paid"]));
		}
		totalAmount_Paid += HelperFunction.ConvertToDouble(Amount_Paid);
		dtrSQL.Close();

		string[] arrID;
		Regex r = new Regex("-");
		arrID = r.Split(Trans_Num);
		String Received_In_Full = "0";
		
		if (Convert.ToDouble(strAmount_Due) <= totalAmount_Paid )
		{
			SqlCommand cmdUpdate;
			cmdUpdate = new SqlCommand("Update SHIPMENT SET SHIPMENT_SELLER_CLOSED_DATE1 =getDate() WHERE SHIPMENT_ORDR_ID = '" + arrID[0].ToString() + "' and SHIPMENT_SKU ='" + arrID[1].ToString() + "'", conn);
			cmdUpdate.ExecuteNonQuery();

			cmdUpdate = new SqlCommand("Update account_payables SET transaction_close='True' WHERE transaction_number='" + Trans_Num + "'", conn);
			cmdUpdate.ExecuteNonQuery();

			cmdUpdate = new SqlCommand("Update cash_paid SET transaction_close='True' WHERE transaction_number='" + Trans_Num + "'", conn);
			cmdUpdate.ExecuteNonQuery();

			if (Convert.ToDouble(strAmount_Due) == totalAmount_Paid) 
				Received_In_Full = "1";
			
		}            

		StringBuilder sbSQL2 = new StringBuilder();
		sbSQL2.Append("UPDATE PAYMENT ");
		sbSQL2.Append(" SET PAY_AMNT = '" + Amount_Paid + "', PAY_COM_TRANS = '" + Received_In_Full + "', PAY_DATE = '" + Wire_Date + "',PAY_CMNT = '" + Comment.Trim() + "' WHERE PAY_ID = '" + Payment_Number + "' "); 
		DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sbSQL2.ToString());

		SqlCommand cmdShipDate = new SqlCommand("SELECT SHIPMENT_DATE FROM ACCOUNT_PAYABLES where TRANSACTION_NUMBER='" + Trans_Num + "'", conn);
		Ship_Date = Convert.ToString(cmdShipDate.ExecuteScalar());

		sbSQL.Append("UPDATE CASH_PAID ");
		sbSQL.Append(" SET Transaction_Number = '" + Trans_Num + "',Payment_Number = '" + Payment_Number + "', ");
		//sbSQL.Append(" SET Transaction_Number = '" + Trans_Num + "', Seller_Invoice_Number= '" + Seller_Inv_Num + "', Seller_Name = '" + Seller_Name + "',");
		sbSQL.Append(" Wire_Payment_Number = '" + Wire_Payment_Num + "', Shipment_Date = '" + Ship_Date + "',Wire_Date = '" + Wire_Date + "', Comment = '" + Comment + "' ");
		//sbSQL.Append(" Wire_Payment_Number = '" + Wire_Payment_Num + "',Shipment_Date = '" + Ship_Date + "',Wire_Date = '" + Wire_Date + "', Amount_Due = '" + Amount_Due + "', Comment = '" + Comment + "' ");
		
		if (Amount_Paid != "") 
			sbSQL.Append(" , Amount_Paid = '" + Amount_Paid + "' ");
		else 
			sbSQL.Append(" , Amount_Paid = '" + 0 + "' ");
		
		sbSQL.Append(" WHERE Transaction_ID='" + dg.DataKeys[e.RowIndex][0].ToString() + "' ");
		DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sbSQL.ToString());

		conn.Close();
		//===================================================================================
		dg.EditIndex = -1;

		BindDG();
	}

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            pnlPanel.Visible = false;
            string Trans_Num = "";
            string Seller_Invoice_Num = "";
            string Seller_Name = "";
            string Seller_Comp_ID = "";
            string Shipment_Term = "";
            string Ship_Date = "";
            string Amount_Due = ""; 
            //int rowIndex=-1;
            bool foundRecord = false;
            if (txtID.Text != "")
            {
                ViewState["ID"] = txtID.Text;
                using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
                {
                    conn.Open();

                    string strSQL1 = "SELECT * from CASH_PAID WHERE Transaction_Number = '"+txtID.Text+"'";
                    using (SqlDataReader dtrTrans = DBLibrary.GetDataReaderFromSelect(conn, strSQL1))
                    {
                        if (dtrTrans.HasRows)
                        {
                            foundRecord = true;
                        }
                    }

                    if (!foundRecord)
                    {
                        string strSQL = "select * from ACCOUNT_PAYABLES where Transaction_Number = '" + txtID.Text + "' ";
                        using (SqlDataReader dtrTrans = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
                        {
                            if (dtrTrans.Read())
                            {
                                Trans_Num = dtrTrans["Transaction_Number"].ToString();
                                Seller_Invoice_Num = dtrTrans["Seller_Invoice_Number"].ToString();
                                Seller_Name = dtrTrans["Seller_Name"].ToString();
                                Seller_Comp_ID = dtrTrans["Seller_Comp_ID"].ToString();
                                Shipment_Term = dtrTrans["Shipment_Term"].ToString();
                                Ship_Date = dtrTrans["Shipment_Date"].ToString();
                                Amount_Due = Convert.ToString(Convert.ToSingle(dtrTrans["Sell_Weight"].ToString()) * Convert.ToSingle(dtrTrans["Unit_Price"].ToString())+Convert.ToSingle(dtrTrans["Tax"]));                                
                            }
                        }

                        if (Trans_Num != "")
                        {                            
                            //Larry--Add the payment to Payment table.
                            //===================================================================================                                        
                            string[] arrID;
                            Regex r = new Regex("-"); 
                            arrID = r.Split(Trans_Num);                            
                            
                            SqlCommand cmdSQL = new SqlCommand("Select SHIPMENT_BUYR,SHIPMENT_SELR, SHIPMENT_ID from SHIPMENT where SHIPMENT_ORDR_ID = '" + arrID[0].ToString() + "' and SHIPMENT_SKU ='" + arrID[1].ToString() + "'", conn);
                            SqlDataReader dtrSQL = cmdSQL.ExecuteReader();
                            dtrSQL.Read();
                            
                            string strPersID = dtrSQL["SHIPMENT_SELR"].ToString();
                            string strShipment_ID = dtrSQL["SHIPMENT_ID"].ToString();
                            string strOrdr_ID = arrID[0].ToString();
                            dtrSQL.Close();                           

                            string strDate = DateTime.Now.ToString();
                            string strSQL2 = "";
                            strSQL2 += "INSERT INTO PAYMENT ";
                            strSQL2 += " (PAY_ORDR,PAY_SHIPMENT,PAY_PERS,PAY_AMNT,PAY_DATE,PAY_CMNT, PAY_COM_TRANS) ";
                            strSQL2 += " VALUES('" + strOrdr_ID + "','" + strShipment_ID + "','" + strPersID + "','" + 0 + "','" + strDate + "','" + "" + "','" + 0 + "') ";
                            SqlCommand cmdAddPayment = new SqlCommand(strSQL2, conn);
                            cmdAddPayment.ExecuteNonQuery();

                            SqlCommand cmdPaymentID = new SqlCommand("SELECT TOP 1 PAY_ID FROM PAYMENT WHERE PAY_SHIPMENT = '" + strShipment_ID + "' AND PAY_PERS = '" + strPersID + "' AND PAY_ORDR = '" + strOrdr_ID + "' ORDER BY PAY_ID DESC", conn);
                            string strPayment_ID = cmdPaymentID.ExecuteScalar().ToString();
                            //==================================================================

                            StringBuilder sbSQL = new StringBuilder();
                            sbSQL.Append("INSERT INTO CASH_PAID ");
                            sbSQL.Append(" (Transaction_Number, Seller_Invoice_Number, Seller_Name, Seller_Comp_ID, Shipment_Term, Shipment_Date, Amount_Due, Amount_Paid, Payment_Number) ");
                            sbSQL.Append(" VALUES ( '" + Trans_Num + "','" + Seller_Invoice_Num.Trim() + "','" + Seller_Name.Trim() + "','" + Seller_Comp_ID + "','" + Shipment_Term + "','" + Ship_Date + "','" + Amount_Due + "','" + 0 + "','" + strPayment_ID + "' ) ");
                            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sbSQL.ToString());
                            BindDG();
                           
                            dg_RowEditing(0);
                        }
                        else 
                        {
                            pnlPanel3.Visible = true;
                            txtID.Text = "";
                            ViewState["ID"] = "";
                        }                        
                    }
                    else
                    {
                        pnlPanel2.Visible = true;
                    }   
                 
                }
            }

            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtID.Text = "";
            pnlPanel.Visible = false;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            pnlPanel2.Visible = false;
        }

        protected void btnClose2_Click(object sender, EventArgs e)
        {
            pnlPanel3.Visible = false;
        }

        protected void btnStillAdd_Click(object sender, EventArgs e)
        {
            string Trans_Num = "";
            string Seller_Invoice_Num = "";
            string Seller_Name = "";
            string Seller_Comp_ID = "";
            string Shipment_Term = "";
            string Ship_Date = "";
            string Amount_Due = "";            
            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();
                pnlPanel2.Visible = false;
                string strSQL = "SELECT * from CASH_PAID WHERE Transaction_Number = '" + ViewState["ID"].ToString() + "'";                
                using (SqlDataReader dtrTrans = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
                {
                    if (dtrTrans.Read())
                    {
                        Trans_Num = dtrTrans["Transaction_Number"].ToString();
                        Seller_Invoice_Num = dtrTrans["Seller_Invoice_Number"].ToString();
                        Seller_Name = dtrTrans["Seller_Name"].ToString();
                        Seller_Comp_ID = dtrTrans["Seller_Comp_ID"].ToString();
                        Shipment_Term = dtrTrans["Shipment_Term"].ToString();
                        Ship_Date = dtrTrans["Shipment_Date"].ToString();
                        Amount_Due = dtrTrans["Amount_Due"].ToString();
                    }
                }

                if (Trans_Num != "")
                {
                    //Larry--Add the payment to Payment table.
                    //===================================================================================                                        
                    string[] arrID;
                    Regex r = new Regex("-");
                    arrID = r.Split(Trans_Num);

                    SqlCommand cmdSQL = new SqlCommand("Select SHIPMENT_BUYR,SHIPMENT_SELR, SHIPMENT_ID from SHIPMENT where SHIPMENT_ORDR_ID = '" + arrID[0].ToString() + "' and SHIPMENT_SKU ='" + arrID[1].ToString() + "'", conn);
                    SqlDataReader dtrSQL = cmdSQL.ExecuteReader();
                    dtrSQL.Read();

                    string strPersID = dtrSQL["SHIPMENT_SELR"].ToString();
                    string strShipment_ID = dtrSQL["SHIPMENT_ID"].ToString();
                    string strOrdr_ID = arrID[0].ToString();
                    dtrSQL.Close();

                    string strDate = DateTime.Now.ToString();
                    string strSQL2 = "";
                    strSQL2 += "INSERT INTO PAYMENT ";
                    strSQL2 += " (PAY_ORDR,PAY_SHIPMENT,PAY_PERS,PAY_AMNT,PAY_DATE,PAY_CMNT, PAY_COM_TRANS) ";
                    strSQL2 += " VALUES('" + strOrdr_ID + "','" + strShipment_ID + "','" + strPersID + "','" + 0 + "','" + strDate + "','" + "" + "','" + 0 + "') ";
                    SqlCommand cmdAddPayment = new SqlCommand(strSQL2, conn);
                    cmdAddPayment.ExecuteNonQuery();

                    SqlCommand cmdPaymentID = new SqlCommand("SELECT TOP 1 PAY_ID FROM PAYMENT WHERE PAY_SHIPMENT = '" + strShipment_ID + "' AND PAY_PERS = '" + strPersID + "' AND PAY_ORDR = '" + strOrdr_ID + "' ORDER BY PAY_ID DESC ", conn);
                    string strPayment_ID = cmdPaymentID.ExecuteScalar().ToString();
                    //==================================================================

                    StringBuilder sbSQL = new StringBuilder();
                    sbSQL.Append("INSERT INTO CASH_PAID ");                    
                    sbSQL.Append(" (Transaction_Number, Seller_Invoice_Number, Seller_Name, Seller_Comp_ID, Shipment_Term, Shipment_Date, Amount_Due, Amount_Paid, Payment_Number) ");
                    sbSQL.Append(" VALUES ( '" + Trans_Num + "','" + Seller_Invoice_Num.Trim() + "','" + Seller_Name.Trim() + "','" + Seller_Comp_ID + "','" + Shipment_Term + "','" + Ship_Date + "','" + Amount_Due + "','" + 0 + "','" + strPayment_ID + "' ) ");
                    DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sbSQL.ToString());
                    BindDG();

                    dg_RowEditing(0);
                }
            }
        }

        protected void btnExportToExcel_Click(object sender, System.EventArgs e)
        {
            int iPagS = dg.PageSize;
            dg.PageSize = 100000;

            BindDG();

            //dg.Columns[0].Visible = false;            

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=CashPaid.xls");

            Response.BufferOutput = true;
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Charset = "UTF-8";
            EnableViewState = false;

            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

            FormatExportedGrid(dg);
            this.ClearControls(dg);
            dg.RenderControl(hw);

            Response.Write(tw.ToString());
            Response.End();

            dg.PageSize = iPagS;
        }        

        public void FormatExportedGrid(GridView dg1)
        {
            dg1.BackColor = System.Drawing.Color.White;
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        private void ClearControls(Control control)
        {
            for (int i = control.Controls.Count - 1; i >= 0; i--)
            {
                ClearControls(control.Controls[i]);
            }
            if (!(control is TableCell))
            {
                if (control.GetType().GetProperty("SelectedItem") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    try
                    {
                        literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
                    }
                    catch
                    {
                    }
                    control.Parent.Controls.Remove(control);
                }
                else
                    if (control.GetType().GetProperty("Text") != null)
                    {
                        LiteralControl literal = new LiteralControl();
                        control.Parent.Controls.Add(literal);
                        literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);
                        control.Parent.Controls.Remove(control);
                    }
            }
            return;
        }

        protected void KeepRunningSum(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                dbTotalDue = 0;
                dbTotalPaid = 0;
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {                
                dbTotalDue += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Row.DataItem, "Amount_Due"));
		dbTotalPaid += HelperFunction.ConvertToDouble(DataBinder.Eval(e.Row.DataItem, "Amount_Paid"));

                if (e.Row.Cells[12] != null)
                    ((Button)e.Row.Cells[12].Controls[0]).Attributes.Add("onClick", "if(!confirm('Are you sure you want to delete the transaction " + ((Button)e.Row.Cells[12].Controls[0]).Text + "?'))return false;");
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[3].Text = "<b>Totals:</b> ";
                e.Row.Cells[9].Text = "<b>" + String.Format("{0:c}", dbTotalDue) + "</b>";
                e.Row.Cells[10].Text = "<b>" + String.Format("{0:c}", dbTotalPaid) + "</b>";     
               
            }                
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            BindDG();
        }

    }
}
