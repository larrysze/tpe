//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.1433
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace localhost.Creditor {
    
    public partial class ManageCommissions {
        protected System.Web.UI.WebControls.Label lblMonth;
        protected System.Web.UI.WebControls.DropDownList ddlMonth;
        protected System.Web.UI.WebControls.Button btnSummary;
        protected System.Web.UI.WebControls.Label lblBrokerName;
        protected System.Web.UI.WebControls.DataGrid dgPaid;
        protected System.Web.UI.WebControls.DataGrid dgCurrent;
        protected System.Web.UI.WebControls.Button btnPaid;
        protected System.Web.UI.WebControls.Button btnDelete;
        protected System.Web.UI.WebControls.DataGrid dgOpen;
    }
}
