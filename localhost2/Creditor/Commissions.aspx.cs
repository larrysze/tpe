using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using MetaBuilders.WebControls;
using TPE.Utility;
using System.Text.RegularExpressions;

namespace localhost.Creditor
{
	public partial class Commission : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((string)Session["Typ"] != "A")
			{
				Response.Redirect("../default.aspx");
			}
			
			if (!IsPostBack)
			{				
				Bind();
			}
		}
		
		private void Bind()
		{   			
			StringBuilder sbSQL = new StringBuilder();			
			sbSQL.Append("SELECT ");
			sbSQL.Append("DISTINCT BROKER_ID = S.SHIPMENT_BROKER_ID, ");
			sbSQL.Append("BROKER_NAME = PERS_FRST_NAME + ' ' + PERS_LAST_NAME ");			
			sbSQL.Append("FROM ");
			sbSQL.Append("PAYMENT P, SHIPMENT S, PERSON ");
			sbSQL.Append("WHERE ");
			sbSQL.Append("P.PAY_COM_TRANS = 1 ");
			sbSQL.Append("AND S.SHIPMENT_BROKER_ID IS NOT NULL ");
			sbSQL.Append("AND P.PAY_SHIPMENT = S.SHIPMENT_ID ");
			sbSQL.Append("AND PERS_ID = SHIPMENT_BROKER_ID ");
			sbSQL.Append("ORDER BY BROKER_NAME ");

			SqlDataAdapter dadContent;
			DataSet dstContent;
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				dadContent = new SqlDataAdapter(sbSQL.ToString(), conn);
				dstContent = new DataSet();
				dadContent.Fill(dstContent);

				dg.DataSource = dstContent;
				dg.DataBind();
			}			
		}

		        
		protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
		    if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
		    {

			string transfer = "BrokerID=" + e.Item.Cells[0].Text;                
			Regex r = new Regex(" ");
			string[] SortExprs;
			SortExprs = r.Split(e.Item.Cells[1].Text);
			transfer += "&FirstName=" + SortExprs[0];
			transfer += "&LastName=" + SortExprs[1];                
			transfer += "&currentMonth=" + DateTime.Now.Month;
			transfer += "&currentYear=" + DateTime.Now.Year;
	                            

			//((Button)e.Item.Cells[3].Controls[1]).Attributes.Add("onClick",
			//	"javascript: window.open('" + "../Creditor/CommissionStatement.aspx?" + transfer + "','','toolbar=false,resizable=1,scrollbars=1,menubar=false,width=1000,height=950,top=20,left=100')");

			e.Item.Cells[2].Text = "<a href=../Creditor/ManageCommissions.aspx?" + transfer + ">Manage Commissions</a>";
		    }           
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		
		private void dg_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			
		}

	}
}