using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using TPE.Utility;

namespace localhost.Creditor
{
    public partial class Return_Reject : System.Web.UI.Page
    {
        double dbTotalDue = 0;
        double dbTotalPaid = 0.0;
        static string GReason = "";
        static string GResult = "";


        protected void Page_Load(object sender, EventArgs e)
        {
            Master.Width = "1200px";
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C") && ((string)Session["Typ"] != "L"))
            {
                Response.Redirect("../default.aspx");
            }
            if (!IsPostBack)
            {
                HelperFunction.fillDatesDesc(01, 2000, ddlMonth);
                ListItem lstItem = new ListItem("Show All Dates", "*", true);
                ddlMonth.Items.Add(lstItem);
                ddlMonth.SelectedItem.Selected = false;
                ddlMonth.Items[ddlMonth.Items.Count - 1].Selected = true;

                BindDG();
                lblAddTransaction.Text = "Enter transaction number.";
                lblErrorMessage.Text = "It is already entered into the table.";                
                ViewState["Sort"] = "Transaction_ID DESC";
                ViewState["ID"] = Session["ID"].ToString();
            }
        }

        protected void BindDG()
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append(" SELECT * FROM RETURN_REJECT ");
            if (!ddlMonth.SelectedItem.Value.Equals("*"))
            {
                int currentMonth = Convert.ToInt32(ddlMonth.SelectedValue.Substring(4, 2).ToString());
                int currentYear = Convert.ToInt32(ddlMonth.SelectedValue.Substring(0, 4).ToString());
                string bymonth = "WHERE ";
                bymonth += " Month(Shipment_Date)='" + currentMonth + "'";
                bymonth += " AND Year(Shipment_Date)='" + currentYear + "'";

                sbSql.Append(bymonth);
            }

            if (!ddlSearchIndex.SelectedValue.Equals("*"))
            {
                if (!ddlMonth.SelectedItem.Value.Equals("*"))
                {
                    if (ddlSearchIndex.SelectedValue.Equals("1"))
                        sbSql.Append(" AND Product like '%" + txtSearch.Text + "%' ");
                    if (ddlSearchIndex.SelectedValue.Equals("2"))
                        sbSql.Append(" AND Transaction_Number = '" + txtSearch.Text + "' ");
                    if (ddlSearchIndex.SelectedValue.Equals("3"))
                        sbSql.Append(" AND Seller_Name like '%" + txtSearch.Text + "%' ");
                    if (ddlSearchIndex.SelectedValue.Equals("4"))
                        sbSql.Append(" AND Buyer_Name = '" + txtSearch.Text + "' ");
                    //if (ddlSearchIndex.SelectedValue.Equals("5"))
                    //    sbSql.Append(" AND Amount_Due = '" + txtSearch.Text + "' ");
                    //if (ddlSearchIndex.SelectedValue.Equals("6"))
                    //    sbSql.Append(" AND Amount_Paid = '" + txtSearch.Text + "' ");
                }
                else
                {
                    sbSql.Append(" WHERE ");
                    if (ddlSearchIndex.SelectedValue.Equals("1"))
                        sbSql.Append(" Product like '%" + txtSearch.Text + "%' ");
                    if (ddlSearchIndex.SelectedValue.Equals("2"))
                        sbSql.Append(" Transaction_Number = '" + txtSearch.Text + "' ");
                    if (ddlSearchIndex.SelectedValue.Equals("3"))
                        sbSql.Append(" Seller_Name like '%" + txtSearch.Text + "%' ");
                    if (ddlSearchIndex.SelectedValue.Equals("4"))
                        sbSql.Append(" Buyer_Name = '" + txtSearch.Text + "' ");
                    //if (ddlSearchIndex.SelectedValue.Equals("5"))
                    //    sbSql.Append(" Amount_Due = '" + txtSearch.Text + "' ");
                    //if (ddlSearchIndex.SelectedValue.Equals("6"))
                    //    sbSql.Append(" Amount_Paid = '" + txtSearch.Text + "' ");
                }
            }

            if (ViewState["Sort"] != null)
            {
                sbSql.Append(" ORDER BY " + ViewState["Sort"].ToString() + " ");
            }
            else
            {
                sbSql.Append(" ORDER BY Transaction_ID DESC");
            }

            SqlDataAdapter dadContent;
            DataSet dstContent;

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                dadContent = new SqlDataAdapter(sbSql.ToString(), conn);
                dstContent = new DataSet();
                dadContent.Fill(dstContent);

                dg.DataSource = dstContent;
                dg.DataBind();

                if (dstContent.Tables[0].Rows.Count > 0)
                {
                    GReason = dstContent.Tables[0].Rows[0]["Reason"].ToString();
                    GResult = dstContent.Tables[0].Rows[0]["Result"].ToString();
                }
                    
            }
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BindDG();
        }

        protected void dg_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dg.PageIndex = e.NewPageIndex;
            BindDG();
        }

        protected void dg_Sorting(object sender, GridViewSortEventArgs e)
        {
            string[] SortExprs;
            string[] ViewStateExprs;
            string NewSearchMode = "";
            string NewSortExpr;

            Regex r = new Regex(" ");
            SortExprs = r.Split(e.SortExpression);

            Regex v = new Regex(" ");
            ViewStateExprs = v.Split(ViewState["Sort"].ToString());

            if (ViewStateExprs[0] == SortExprs[0])
            {
                if (ViewStateExprs[1].ToString() == "ASC") NewSearchMode = "DESC";
                else NewSearchMode = "ASC";
            }
            else NewSearchMode = "ASC";

            NewSortExpr = SortExprs[0] + " " + NewSearchMode;

            ViewState["Sort"] = NewSortExpr;

            BindDG();

        }

        protected void btnAdd_OnClick(object sender, EventArgs e)
        {
            pnlPanel.Visible = true;
        }

        protected void dg_RowCommand(Object sender, GridViewCommandEventArgs e)
        {
            //if (e.CommandName == "Select")
            //{
            //    //int index = Convert.ToInt32(e.CommandArgument);

            //    //GridViewRow selectedRow = dg.Rows[index];
            //    //TableCell trans_ID = selectedRow.Cells[2];
            //    //TableCell Comp_ID = selectedRow.Cells[13];
            //    //string ID = trans_ID.Text;
            //    //Session["CompID"] = Comp_ID.Text;
            //    //Response.Redirect("/Creditor/Edit_Payments.aspx?ID=" + ID);

            //    StringBuilder sbSQL = new StringBuilder();
            //    string Payment_ID = dg.Rows[Convert.ToInt32(e.CommandArgument)].Cells[5].Text;
            //    if (Payment_ID != "&nbsp;")
            //    {
            //        sbSQL.Append("DELETE FROM PAYMENT ");
            //        sbSQL.Append("WHERE PAY_ID = '" + Payment_ID + "' ");
            //        DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sbSQL.ToString());
            //    }

            //    StringBuilder sbSQL2 = new StringBuilder();
            //    sbSQL2.Append("DELETE FROM CASH_PAID ");
            //    sbSQL2.Append(" WHERE TRANSACTION_ID = '" + dg.DataKeys[Convert.ToInt32(e.CommandArgument)][0].ToString() + "' ");
            //    DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sbSQL2.ToString());
            //    BindDG();

            //}

        }

        protected void dg_RowEditing(object sender, GridViewEditEventArgs e)
        {
            dg.EditIndex = e.NewEditIndex;
            BindDG();
        }

        protected void dg_RowEditing(int rowIndex)
        {
            dg.EditIndex = rowIndex;
            BindDG();
        }

        protected void dg_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            dg.EditIndex = -1;
            BindDG();
        }

        protected void dg_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {            
            StringBuilder sbSQL = new StringBuilder();
            GridViewRow row = dg.Rows[e.RowIndex];
            bool hasReason = false;
            bool hasResult = false;
            string Reason, Result;            
            Reason = ((TextBox)(row.Cells[16].Controls[0])).Text;
            Result = ((TextBox)(row.Cells[17].Controls[0])).Text;

            if (Reason != "" && Reason != GReason) hasReason = true;
            if (Result != "" && Result != GResult) hasResult = true;

            if (hasReason || hasResult)
            {
                SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
                conn.Open();

                SqlCommand cmdPerson;
                cmdPerson = new SqlCommand("select pers_frst_name from person where pers_id = '" + ViewState["ID"].ToString() + "'", conn);
                string first_name = cmdPerson.ExecuteScalar().ToString();

                sbSQL.Append("UPDATE return_reject ");                
                sbSQL.Append("SET ");
                if (hasReason && hasResult)
                {
                    sbSQL.Append(" Reason = '" + Reason + "-" + first_name + "-" + DateTime.Now.ToShortDateString() + "'");
                    sbSQL.Append(" , Result = '" + Result + "-" + first_name + "-" + DateTime.Now.ToShortDateString() + "'");
                }
                else
                {
                    if (hasReason) sbSQL.Append(" Reason = '" + Reason + "-" + first_name + "-" + DateTime.Now.ToShortDateString() + "'");
                    if (hasResult) sbSQL.Append(" Result = '" + Result + "-" + first_name + "-" + DateTime.Now.ToShortDateString() + "'");
                }                
                sbSQL.Append(" WHERE Transaction_ID='" + dg.DataKeys[e.RowIndex][0].ToString() + "' ");
                DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sbSQL.ToString());


                conn.Close();
            }
            


            dg.EditIndex = -1;
            BindDG();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            pnlPanel.Visible = false;
            string Transaction_Number = "";
            string Shipment_Date = "";
            string Product = "";
            string Weight = "";
            string From_Buyer = "";            
            string Buyer_Paid = "";
            string Buy_Price = "";
            string Sell_Price = "";
            string To_Seller = "";
            string Seller_Paid = "";
            string Buyer_Name = "";           
            string Seller_Name = "";
            string Buyer_Comp_ID = "";
            string Seller_Comp_ID = "";                                    
            
            
            Regex r = new Regex("-");
            string[] id = r.Split(txtID.Text);           
            bool foundRecord = false;
            if (id[0] != "" && id[1] != "")
            {
                using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
                {
                    conn.Open();

                    string strSQL1 = "SELECT * from RETURN_REJECT WHERE Transaction_Number = '" + txtID.Text + "'";
                    using (SqlDataReader dtrTrans = DBLibrary.GetDataReaderFromSelect(conn, strSQL1))
                    {
                        if (dtrTrans.HasRows)
                        {
                            foundRecord = true;
                        }
                    }

                    if (!foundRecord)
                    {
                        string strSQL = GetSQL();
                        strSQL += " and shipment_ordr_id = '" + id[0].ToString() + "'";
                        strSQL += " and shipment_sku = '" + id[1].ToString() + "'";

                        using (SqlDataReader dtrTrans = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
                        {
                            if (dtrTrans.Read())
                            {
                                Transaction_Number = dtrTrans["Transaction_Number"].ToString();
                                Shipment_Date = dtrTrans["Shipment_Date"].ToString();
                                Product = dtrTrans["Product"].ToString(); 
                                Weight = dtrTrans["Weight"].ToString();
                                From_Buyer = dtrTrans["From_Buyer"].ToString();
                                Buyer_Paid = dtrTrans["Buyer_Paid"].ToString();
                                Buy_Price = dtrTrans["Buy_Price"].ToString();
                                Sell_Price = dtrTrans["Sell_Price"].ToString();
                                To_Seller = dtrTrans["To_Seller"].ToString();
                                Seller_Paid = dtrTrans["Seller_Paid"].ToString();
                                Buyer_Name = dtrTrans["Buyer_Name"].ToString();
                                Seller_Name = dtrTrans["Seller_Name"].ToString();
                                Buyer_Comp_ID = dtrTrans["Buyer_Comp_ID"].ToString();                          
                                Seller_Comp_ID = dtrTrans["Seller_Comp_ID"].ToString();
                            }
                        }

                        StringBuilder sbSQL = new StringBuilder();
                        sbSQL.Append("INSERT INTO RETURN_REJECT ");
                        sbSQL.Append(" (Transaction_Number,Shipment_Date, Product, Weight, From_Buyer, Buyer_Paid, Buy_Price, Sell_Price, To_Seller,Seller_Paid, Buyer_Name,  Seller_Name, Buyer_Comp_ID, Seller_Comp_ID) ");
                        sbSQL.Append(" VALUES ( '" + Transaction_Number + "','" + Shipment_Date.Trim() + "','" + Product + "','" + Weight + "','" + From_Buyer + "','" + Buyer_Paid + "','" + Buy_Price + "','" + Sell_Price + "','" + To_Seller + "','" + Seller_Paid + "','" + Buyer_Name + "','" + Seller_Name + "','" + Buyer_Comp_ID + "','" + Seller_Comp_ID + "' ) ");
                        DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sbSQL.ToString());
                        BindDG();                        
                        dg_RowEditing(0);
                    }
                    else
                    {
                        pnlPanel2.Visible = true;
                    }
                }
            }
            txtID.Text = "";
        }

        private string GetSQL()
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("select ");
            sbSql.Append(" CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR) AS Transaction_Number,");
            sbSql.Append("Shipment_transaction_date AS Shipment_date,");
            sbSql.Append("Ordr_prod_spot AS Product,");
            sbSql.Append("Weight = ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY),");
            sbSql.Append("From_Buyer=(SHIPMENT_BUYR_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (100 + ISNULL(SHIPMENT_TAX, 0)) / 100),");
            sbSql.Append("Buyer_Paid=ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),0),");
            sbSql.Append("Buy_Price = LEFT(SHIPMENT_BUYR_PRCE,6),");
            sbSql.Append("Sell_Price = LEFT(SHIPMENT_PRCE,6),");
            sbSql.Append("To_Seller=SHIPMENT_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (100 + ISNULL(SHIPMENT_TAX, 0)) / 100,");
            sbSql.Append("Seller_Paid=ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_SELR),0),");
            sbSql.Append("Buyer_Name=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)),");
            sbSql.Append("Seller_Name=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)),");
            sbSql.Append("Buyer_Comp_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR),");
            sbSql.Append("Seller_Comp_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)  ");
            sbSql.Append(" from orders, shipment ");
            sbSql.Append(" where shipment_ordr_id = ordr_id");
            return sbSql.ToString();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtID.Text = "";
            pnlPanel.Visible = false;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            pnlPanel2.Visible = false;
        }

        protected void btnExportToExcel_Click(object sender, System.EventArgs e)
        {
            int iPagS = dg.PageSize;
            dg.PageSize = 100000;

            BindDG();

            //dg.Columns[0].Visible = false;            

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=Return_Reject.xls");

            Response.BufferOutput = true;
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Charset = "UTF-8";
            EnableViewState = false;

            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

            FormatExportedGrid(dg);
            this.ClearControls(dg);
            dg.RenderControl(hw);

            Response.Write(tw.ToString());
            Response.End();

            dg.PageSize = iPagS;
        }

        public void FormatExportedGrid(GridView dg1)
        {
            dg1.BackColor = System.Drawing.Color.White;
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        private void ClearControls(Control control)
        {
            for (int i = control.Controls.Count - 1; i >= 0; i--)
            {
                ClearControls(control.Controls[i]);
            }
            if (!(control is TableCell))
            {
                if (control.GetType().GetProperty("SelectedItem") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    try
                    {
                        literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
                    }
                    catch
                    {
                    }
                    control.Parent.Controls.Remove(control);
                }
                else
                    if (control.GetType().GetProperty("Text") != null)
                    {
                        LiteralControl literal = new LiteralControl();
                        control.Parent.Controls.Add(literal);
                        literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);
                        control.Parent.Controls.Remove(control);
                    }
            }
            return;
        }

        protected void KeepRunningSum(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                dbTotalDue = 0;
                dbTotalPaid = 0;
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //dbTotalDue += Convert.ToSingle(DataBinder.Eval(e.Row.DataItem, "Amount_Due"));
               // dbTotalPaid += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "Amount_Paid"));

                //if (e.Row.Cells[12] != null)
                    //((Button)e.Row.Cells[12].Controls[0]).Attributes.Add("onClick", "if(!confirm('Are you sure you want to delete the transaction " + ((Button)e.Row.Cells[12].Controls[0]).Text + "?'))return false;");
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[3].Text = "<b>Totals:</b> ";
                e.Row.Cells[9].Text = "<b>" + String.Format("{0:c}", dbTotalDue) + "</b>";
                e.Row.Cells[10].Text = "<b>" + String.Format("{0:c}", dbTotalPaid) + "</b>";

            }
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            BindDG();
        }


    }
}
