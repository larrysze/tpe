<%@ Page Language="c#" Codebehind="WriteOff.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.WriteOff" MasterPageFile="~/MasterPages/Menu.Master" Title="The Plastics Exchange: Commodity Plastic Resin, Buy Resin, Sell Resin, Plastic Materials" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphCssLink">
    <style type="text/css">
        .menuskin { BORDER-RIGHT: black 2px solid; BORDER-TOP: black 2px solid; Z-INDEX: 100; VISIBILITY: hidden; FONT: 12px/18px Verdana; BORDER-LEFT: black 2px solid; WIDTH: 165px; BORDER-BOTTOM: black 2px solid; POSITION: absolute; BACKGROUND-COLOR: menu }
        .menuskin A { PADDING-RIGHT: 10px; PADDING-LEFT: 10px; COLOR: black; TEXT-DECORATION: none }
        #mouseoverstyle { BACKGROUND-COLOR: highlight }
        #mouseoverstyle A { COLOR: white }
        .Left { text-align:left}
</style>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphMain">
    <asp:Panel ID="pnContent" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
        <tr align="center">                      
            <td align="center">
                <span class="LinkNormal">
                    <asp:HyperLink ID="Receivables" runat="server" Text="Show Receivables" NavigateUrl="CurrARAPAging.aspx?etat=R">
                    </asp:HyperLink>
                </span>&nbsp;&nbsp;&nbsp;
                <span class="LinkNormal">
                    <asp:HyperLink ID="Payables1" runat="server" Text="Show Payables" NavigateUrl="CurrARAPAging.aspx?etat=P" color="#BEB8A2" Font-Size="11px">
                    </asp:HyperLink>
                </span>&nbsp;&nbsp;&nbsp;
                <span class="Content">Write Off</span>    
            </td>
        </tr>
    </table>
    <br />
        <asp:DataGrid ID="dg" runat="server" BackColor="#000000" BorderWidth="0" CellSpacing="1" Width="100%" OnItemDataBound="KeepRunningSum"
        HorizontalAlign="Center" ShowFooter="True" CellPadding="2" AllowSorting="true" AutoGenerateColumns="false">
            <HeaderStyle CssClass="LinkNormal Bold OrangeColor" HorizontalAlign="Center" />
            <AlternatingItemStyle CssClass="LinkNormal DarkGray" />
            <ItemStyle CssClass="LinkNormal LightGray" />
            <FooterStyle CssClass="Content Bold Color4 FooterColor" />
            <Columns>      
                <asp:BoundColumn DataField="COMPNAME" HeaderText="Company Name" SortExpression="COMPNAME ASC" />      
                <asp:HyperLinkColumn DataNavigateUrlField="ID" HeaderText="Shipment ID"
                    DataNavigateUrlFormatString="../administrator/Transaction_Details.aspx?Referer=/Creditor/Transaction_Summary.aspx&amp;ID={0}"
							DataTextField="ID" SortExpression="ID ASC">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>                          
                <asp:BoundColumn DataField="SHIPMENT_DATE_TAKE" HeaderText="Shipment Date" SortExpression="SHIPMENT_DATE_TAKE ASC" />
                <asp:BoundColumn DataField="VARWEIGHT" HeaderText="Shipment Weight(lbs)" SortExpression="VARWEIGHT ASC" />
                <asp:BoundColumn DataField="VARWRITEOFFAMOUNT" HeaderText="Amount Written Off" DataFormatString="{0:c}" ItemStyle-Wrap="false" SortExpression="VARWRITEOFFAMOUNT ASC" />                                
                <asp:BoundColumn DataField="VARWRITEOFFReceived" HeaderText="Received Written Off" DataFormatString="{0:c}" ItemStyle-Wrap="false" SortExpression="VARWRITEOFFReceived ASC" />                                
                <asp:BoundColumn DataField="VARWRITEOFFNet" HeaderText="Net Written Off" DataFormatString="{0:c}" ItemStyle-Wrap="false" SortExpression="VARWRITEOFFNet ASC" />                                
                <asp:BoundColumn DataField="SHIPMENT_WRITEOFF_COMMENT" HeaderText="Write Off Comment" SortExpression="SHIPMENT_WRITEOFF_COMMENT ASC" />
                <asp:BoundColumn DataField="SHIPMENT_WRITEOFF_DATE" HeaderText="Write Off Date" SortExpression="SHIPMENT_WRITEOFF_DATE ASC" />
            </Columns>
        </asp:DataGrid>
    </asp:Panel>
</asp:Content>
