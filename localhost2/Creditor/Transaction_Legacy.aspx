<%@ Register TagPrefix="mbdb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.DefaultButtons" %>
<%@ Page Language="c#" CodeBehind="Transaction_Legacy.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.Transaction_Legacy" MasterPageFile="~/MasterPages/Menu.Master" EnableViewState="True" EnableEventValidation="false"  %>

<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphCssLink">
	<style type="text/css">
	.menuskin { BORDER-RIGHT: black 2px solid; BORDER-TOP: black 2px solid; Z-INDEX: 100; VISIBILITY: hidden; FONT: 12px/18px Verdana; BORDER-LEFT: black 2px solid; WIDTH: 165px; BORDER-BOTTOM: black 2px solid; POSITION: absolute; BACKGROUND-COLOR: menu }
	.menuskin A { PADDING-RIGHT: 10px; PADDING-LEFT: 10px; COLOR: black; TEXT-DECORATION: none }
	#mouseoverstyle { BACKGROUND-COLOR: highlight }
	#mouseoverstyle A { COLOR: white }
	</style>
</asp:Content>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
 <table class="Content LinkNormal" cellspacing="0" cellpadding="0" width="100%" style="text-align:center" border="0">
    <tbody>
        <tr>
            <td align="center">
                <span class="Content"><b>Transaction Legacy</b></span>&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
	</tr>
        <tr>
		<td align="center" style="height: 69px">
			<table width="100%" style="text-align:left">
                    <tbody>
                        <tr valign="bottom">
                            <td valign="top" colspan="4" align="left"> <b>What is the transaction number?</b><br />
                            TN#:
                                <asp:TextBox ID="txtTransactionNumber" runat="server" Width="230px"></asp:TextBox>
                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" /><br />  
                                <asp:Label ID="lblSearchResult" runat="server" EnableTheming="True" ForeColor="Red" Width="334px"></asp:Label>
                            </td>
                        </tr>
                    </tbody>                         
                 </table>
            </td>
        </tr>
        <tr>		                        		
		   <td align="center" style="height: 50px">
		    <asp:datagrid id="dg" runat="server" Width="85%" CssClass="DataGrid" AutoGenerateColumns="False" ShowFooter="True" DataKeyField="ORDR_ID" CellPadding="2" HorizontalAlign="Center" CellSpacing="1" BackColor="#000000" BorderWidth="0" Visible="false">
				<AlternatingItemStyle CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
				<ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
				<HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
				<FooterStyle CssClass="Content Color4 Bold FooterColor" />
				<Columns>
					<asp:HyperLinkColumn DataNavigateUrlField="ID" DataNavigateUrlFormatString="../administratoit'r/Transaction_Details.aspx?Referer=/Creditor/Transaction_Summary.aspx&amp;ID={0}"
						DataTextField="ID">
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:HyperLinkColumn>
					<asp:BoundColumn DataField="ORDR_DTE" SortExpression="ORDR_DTE ASC" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
					<asp:BoundColumn DataField="VARCONTRACT" SortExpression="VARCONTRACT ASC" HeaderText="Product">
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARWEIGHT" SortExpression="VARWEIGHT ASC" HeaderText="Weight(lbs)" DataFormatString="{0:#,###}">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARRCNUMBER" SortExpression="VARRCNUMBER ASC" HeaderText="RC#">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARTVBUYR" SortExpression="VARTVBUYR ASC" HeaderText="From Buyer" DataFormatString="{0:c}">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:HyperLinkColumn DataNavigateUrlField="ID" DataNavigateUrlFormatString="Edit_Payments.aspx?Receivable=true&amp;ID={0}"
						DataTextField="VARPPAID" SortExpression="VARPPAID ASC" HeaderText="Buyer Paid" DataTextFormatString="{0:c}">
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:HyperLinkColumn>
					<asp:BoundColumn DataField="VARBUYPRICE" SortExpression="VARBUYPRICE ASC" HeaderText="Buy Price">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARSELLPRICE" SortExpression="VARSELLPRICE ASC" HeaderText="Sell Price">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARTVSELR" SortExpression="VARTVSELR ASC" HeaderText="To Seller" DataFormatString="{0:c}">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:HyperLinkColumn DataNavigateUrlField="ID" DataNavigateUrlFormatString="Edit_Payments.aspx?ID={0}"
						DataTextField="VARSPAID" SortExpression="VARSPAID ASC" HeaderText="Seller Paid" DataTextFormatString="{0:c}">
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:HyperLinkColumn>
					<asp:BoundColumn DataField="VARFREIGHT" SortExpression="VARFREIGHT ASC" HeaderText="Frieght Cost"
						DataFormatString="{0:c}">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARWHAREHOUSE" SortExpression="VARWHAREHOUSE ASC" HeaderText="Warehouse"
						DataFormatString="{0:c}">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARMISC" SortExpression="VARMISC ASC" HeaderText="Misc"
						DataFormatString="{0:c}">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARCOMMISION" SortExpression="VARCOMMISION ASC" HeaderText="Commission"
						DataFormatString="{0:c}"  Visible="false">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:HyperLinkColumn DataNavigateUrlField="VARBUYR_ID" DataNavigateUrlFormatString="/Creditor/AccountSummary.aspx?Id={0}"
						DataTextField="VARBUYR" SortExpression="VARBUYR ASC" HeaderText="Buyer">
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:HyperLinkColumn>
					<asp:HyperLinkColumn DataNavigateUrlField="VARSELR_ID" DataNavigateUrlFormatString="/Creditor/AccountSummary.aspx?Id={0}"
						DataTextField="VARSELR" SortExpression="VARSELR ASC" HeaderText="Seller">
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:HyperLinkColumn>
					<asp:BoundColumn Visible="True" DataField="SHIPMENT_DATE_DELIVERED" HeaderText="Shipment Date" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>						
					<asp:BoundColumn Visible="False" DataField="VARPPAIDDATE" HeaderText="Payment Date"></asp:BoundColumn>												
                    <asp:TemplateColumn HeaderText="Lagacy Number" Visible="false">
						<ItemTemplate>
							<center>
							    <asp:Label runat="server" ID="lblInvoinced" />								
							</center>
						</ItemTemplate>
					</asp:TemplateColumn>						
					<asp:BoundColumn DataField="SHIPMENT_LEGACY_NUMBER" HeaderText="Legacy Number"></asp:BoundColumn>					                        
				</Columns>
			</asp:datagrid>
		    </td>
	    </tr>
		<tr>
            <td align="center" style="height: 69px">
	            <table width="100%" style="text-align:left">              
                    <tr valign="bottom">
                        <td valign="top" colspan="4" align="left" style="font-size: 10pt">
                            <asp:Label ID="lblLegacyNumber" runat="server" Text="LN#: " Visible="false"></asp:Label>
                            <asp:TextBox ID="txtLegacyNumber" runat="server" Width="230px" Visible="false"></asp:TextBox>
                           <asp:Button ID="btnAdd" runat="server" Text="Add the number" OnClick="btnAdd_Click" Visible="false"/><br />
                            <asp:Label ID="lblAddResult" runat="server" Width="688px" ForeColor="Red" Visible="False"></asp:Label>
                        </td> 
                     </tr>
                </table>      
            </td>
        </tr>
    </tbody>
 </table>
</asp:Content>
                
				
				