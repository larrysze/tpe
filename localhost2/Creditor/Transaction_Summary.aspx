<%@ Register TagPrefix="mbdb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.DefaultButtons" %>
<%@ Page Language="c#" CodeBehind="Transaction_Summary.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.Transaction_Summary" MasterPageFile="~/MasterPages/Menu.Master" EnableViewState="True" EnableEventValidation="false"  %>

<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphCssLink">
	<style>
	.menuskin { BORDER-RIGHT: black 2px solid; BORDER-TOP: black 2px solid; Z-INDEX: 100; VISIBILITY: hidden; FONT: 12px/18px Verdana; BORDER-LEFT: black 2px solid; WIDTH: 165px; BORDER-BOTTOM: black 2px solid; POSITION: absolute; BACKGROUND-COLOR: menu }
	.menuskin A { PADDING-RIGHT: 10px; PADDING-LEFT: 10px; COLOR: black; TEXT-DECORATION: none }
	#mouseoverstyle { BACKGROUND-COLOR: highlight }
	#mouseoverstyle A { COLOR: white }
	</style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">
	<script language="JavaScript1.2">

        //Pop-it menu- By Dynamic Drive
        //For full source code and more DHTML scripts, visit http://www.dynamicdrive.com
        //This credit MUST stay intact for use

        var linkset=new Array()
        //SPECIFY MENU SETS AND THEIR LINKS. FOLLOW SYNTAX LAID OUT

        <asp:PlaceHolder id="phActionButton"  runat="server" />
        ////No need to edit beyond here

        var ie4=document.all&&navigator.userAgent.indexOf("Opera")==-1
        var ns6=document.getElementById&&!document.all
        var ns4=document.layers

        function showmenu(e,which){

        if (!document.all&&!document.getElementById&&!document.layers)
        return

        clearhidemenu()

        menuobj=ie4? document.all.popmenu : ns6? document.getElementById("popmenu") : ns4? document.popmenu : ""
        menuobj.thestyle=(ie4||ns6)? menuobj.style : menuobj

        if (ie4||ns6)
        menuobj.innerHTML=which
        else{
        menuobj.document.write('<layer name=gui bgColor=#E6E6E6 width=165 onmouseover="clearhidemenu()" onmouseout="hidemenu()">'+which+'</layer>')
        menuobj.document.close()
        }

        menuobj.contentwidth=(ie4||ns6)? menuobj.offsetWidth : menuobj.document.gui.document.width
        menuobj.contentheight=(ie4||ns6)? menuobj.offsetHeight : menuobj.document.gui.document.height
       
      
        eventX=ie4? event.clientX : ns6? e.clientX : e.pageX? e.pageX: e.x
        eventY=ie4? event.clientY : ns6? e.clientY : e.pageY? e.pageY: e.y
        

	var scrollOffset = getScrollXY();
        eventX += scrollOffset[0]+10;
        

        //Find out how close the mouse is to the corner of the window
        var rightedge=ie4? document.body.clientWidth-eventX : window.innerWidth-eventX
        var bottomedge=ie4? document.body.clientHeight-eventY : window.innerHeight-eventY

        //if the horizontal distance isn't enough to accomodate the width of the context menu
        if (rightedge<menuobj.contentwidth)
        //move the horizontal position of the menu to the left by it's width
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX-menuobj.contentwidth : ns6? window.pageXOffset+eventX-menuobj.contentwidth : eventX-menuobj.contentwidth
        else
        //position the horizontal position of the menu where the mouse was clicked
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX : ns6? window.pageXOffset+eventX : eventX

        //same concept with the vertical position
        if (bottomedge<menuobj.contentheight)
        menuobj.thestyle.top=ie4? document.body.scrollTop+eventY-menuobj.contentheight : ns6? window.pageYOffset+eventY-menuobj.contentheight : eventY-menuobj.contentheight
        else
        menuobj.thestyle.top=ie4? document.body.scrollTop+event.clientY : ns6? window.pageYOffset+eventY : eventY
        menuobj.thestyle.visibility="visible"
        
        menuobj.thestyle.top = eventY + scrollOffset[1];
                
        return false;
        }
        

	function getScrollXY() {
	  var scrOfX = 0, scrOfY = 0;
	  if( typeof( window.pageYOffset ) == 'number' ) {
	    //Netscape compliant
	    scrOfY = window.pageYOffset;
	    scrOfX = window.pageXOffset;
	  } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
	    //DOM compliant
	    scrOfY = document.body.scrollTop;
	    scrOfX = document.body.scrollLeft;
	  } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
	    //IE6 standards compliant mode
	    scrOfY = document.documentElement.scrollTop;
	    scrOfX = document.documentElement.scrollLeft;
	  }
	  return [ scrOfX, scrOfY ];
	}
	
        function contains_ns6(a, b) {
        //Determines if 1 element in contained in another- by Brainjar.com
        while (b.parentNode)
        if ((b = b.parentNode) == a)
        return true;
        return false;
        }

        function hidemenu(){
        if (window.menuobj)
        menuobj.thestyle.visibility=(ie4||ns6)? "hidden" : "hide"
        }

        function dynamichide(e){
        if (ie4&&!menuobj.contains(e.toElement))
        hidemenu()
        else if (ns6&&e.currentTarget!= e.relatedTarget&& !contains_ns6(e.currentTarget, e.relatedTarget))
        hidemenu()
        }

        function delayhidemenu(){
        if (ie4||ns6||ns4)
        delayhide=setTimeout("hidemenu()",500)
        }

        function clearhidemenu(){
        if (window.delayhide)
        clearTimeout(delayhide)
        }

        function highlightmenu(e,state){
        if (document.all)
        source_el=event.srcElement
        else if (document.getElementById)
        source_el=e.target
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        else{
        while(source_el.id!="popmenu"){
        source_el=document.getElementById? source_el.parentNode : source_el.parentElement
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        }
        }
        }

        if (ie4||ns6)
        document.onclick=hidemenu

	</script>
</asp:Content>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
 <div id="popmenu" onmouseover="clearhidemenu();highlightmenu(event,'on')" class="menuskin" onmouseout="highlightmenu(event,'off');dynamichide(event)"></DIV>
 
 <table class="Content LinkNormal" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
    <tbody>
        <tr>
            <td style=" text-align:center">
                <span class="Content">Transaction Summary</span>&nbsp;&nbsp;&nbsp;&nbsp; <a class="LinkNormal" href="/Creditor/Month_Summary.aspx">Monthly Summary</a>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            </tr>
        <tr>
            <td style=" text-align:left">
                            <span><b>Month</b>&nbsp;<asp:dropdownlist id="ddlMonth" runat="server" CssClass="InputForm" ></asp:dropdownlist></span>&nbsp;&nbsp;
                            <span><b>Year</b>&nbsp;<asp:dropdownlist id="ddlYear" runat="server" CssClass="InputForm" ></asp:dropdownlist></span> &nbsp;&nbsp;                           
                            <span><b>Company</b>&nbsp;<asp:dropdownlist id="ddlCompanies" runat="server" CssClass="InputForm" ></asp:dropdownlist></span>&nbsp;&nbsp;
                            <span><b>Broker</b>&nbsp;<asp:dropdownlist id="ddlBroker" runat="server" CssClass="InputForm" ></asp:dropdownlist></span>&nbsp;&nbsp;
                            <span><b>Grade</b>&nbsp;<asp:dropdownlist id="ddlGrade" runat="server" CssClass="InputForm" ></asp:dropdownlist></span>&nbsp;
                            
                             <br /> <br />
                           <span><b>Search</b>&nbsp;<mbdb:defaultbuttons id="DefaultSearchButtons" runat="server" >
								<mbdb:DefaultButtonSetting button="btnSearch" parent="txtSearch"></mbdb:DefaultButtonSetting>
							</mbdb:defaultbuttons><asp:label id="lblSort" runat="server" visible="false" ></asp:label><asp:textbox id="txtSearch" runat="server" CssClass="InputForm"  Width="130px" size="8" ></asp:textbox>
							    <asp:dropdownlist id="ddlIndex" runat="server" CssClass="InputForm" ></asp:dropdownlist>&nbsp;&nbsp;&nbsp;
							    <asp:button id="btnSearch" onclick="btnSearch_Click" runat="server" CssClass="Content Color2" style="background-color:orange;font-weight:bold;" Text="SUBMIT!" ></asp:button></span>&nbsp;&nbsp;&nbsp;
                            <span> <asp:Button runat="server" ID="cmdExportToExcel" Text="Export to Excel"  CssClass="Content Color2" style="background-color:forestgreen;color:white;" onclick="btnExportToExcel_Click" /></span><br /><br />
               </td></tr><tr>
							
							<td align="center">
							<asp:datagrid id="dg" runat="server" Width="85%" CssClass="DataGrid" AutoGenerateColumns="False" AllowSorting="True" OnItemDataBound="KeepRunningSum" ShowFooter="True" DataKeyField="ORDR_ID" CellPadding="2" HorizontalAlign="Center" CellSpacing="1" BackColor="#000000" BorderWidth="0" __designer:wfdid="w56">
					<alternatingItemStyle CssClass="LinkNormal DarkGray"></alternatingItemStyle>
					<ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
					<HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
					<FooterStyle CssClass="Content Color4 Bold FooterColor" />
					<Columns>
						<asp:TemplateColumn>
							<ItemTemplate>
								<center><IMG src="/pics/icons/icon_magnifying_glass.gif"></CENTER>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:HyperLinkColumn SortExpression="ID" HeaderText="Trans #" DataNavigateUrlField="ID" DataNavigateUrlFormatString="../administrator/Transaction_Details.aspx?Referer=/Creditor/Transaction_Summary.aspx&amp;ID={0}"
							DataTextField="ID">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:BoundColumn DataField="ORDR_DTE" SortExpression="ORDR_DTE" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
						<asp:BoundColumn DataField="VARCONTRACT" SortExpression="VARCONTRACT ASC" HeaderText="Product">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARWEIGHT" SortExpression="VARWEIGHT" HeaderText="Weight(lbs)" DataFormatString="{0:#,###}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARRCNUMBER" SortExpression="VARRCNUMBER" HeaderText="RC#">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARTVBUYR" SortExpression="VARTVBUYR" HeaderText="From Buyer" DataFormatString="{0:c}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="ID" DataNavigateUrlFormatString="Edit_Payments.aspx?Receivable=true&amp;ID={0}"
							DataTextField="VARPPAID" SortExpression="VARPPAID ASC" HeaderText="Buyer Paid" DataTextFormatString="{0:c}">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:BoundColumn DataField="VARBUYPRICE" SortExpression="VARBUYPRICE" HeaderText="Buy Price">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARSELLPRICE" SortExpression="VARSELLPRICE" HeaderText="Sell Price">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARTVSELR" SortExpression="VARTVSELR" HeaderText="To Seller" DataFormatString="{0:c}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="ID" DataNavigateUrlFormatString="Edit_Payments.aspx?ID={0}"
							DataTextField="VARSPAID" SortExpression="VARSPAID" HeaderText="Seller Paid" DataTextFormatString="{0:c}">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:BoundColumn DataField="VARFREIGHT" SortExpression="VARFREIGHT" HeaderText="Frieght Cost"
							DataFormatString="{0:c}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARWAREHOUSE" SortExpression="VARWAREHOUSE" HeaderText="Warehouse">
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARWHAREHOUSE" SortExpression="VARWHAREHOUSE" HeaderText="Warehouse Price"
							DataFormatString="{0:c}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARMISC" SortExpression="VARMISC" HeaderText="Misc"
							DataFormatString="{0:c}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARCOMMISION" SortExpression="VARCOMMISION" HeaderText="Commission"
							DataFormatString="{0:c}"  Visible="false">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARBROKER" SortExpression="VARBROKER" HeaderText="Broker"></asp:BoundColumn>
						<asp:BoundColumn DataField="SHIPMENT_BUYER_TERMS" SortExpression="SHIPMENT_BUYER_TERMS ASC" HeaderText="Terms">
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn HeaderText="Days Past Due">
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn Visible="false" HeaderText="Interest" DataFormatString="{0:c}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="VARFEE" SortExpression="VARFEE" HeaderText="Margin" DataFormatString="{0:c}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="VARBUYR_ID" DataNavigateUrlFormatString="/Creditor/AccountSummary.aspx?Id={0}"
							DataTextField="VARBUYR" SortExpression="VARBUYR" HeaderText="Buyer">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="VARSELR_ID" DataNavigateUrlFormatString="/Creditor/AccountSummary.aspx?Id={0}"
							DataTextField="VARSELR" SortExpression="VARSELR" HeaderText="Seller">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:BoundColumn Visible="True" DataField="SHIPMENT_DATE_DELIVERED" HeaderText="Shipment Date" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>						
						<asp:BoundColumn Visible="False" DataField="VARPPAIDDATE" HeaderText="Payment Date"></asp:BoundColumn>												
						<asp:TemplateColumn HeaderText="Lagacy Number" Visible="false">
							<ItemTemplate>
								<center>
								    <asp:Label runat="server" ID="lblInvoinced" />								
								</CENTER>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="FR" ItemStyle-HorizontalAlign="Center" Visible="false"> 
							<ItemTemplate>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="WH" ItemStyle-HorizontalAlign="Center" Visible="false">
							<ItemTemplate>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn DataField="SHIPMENT_LEGACY_NUMBER" HeaderText="Legacy Number"></asp:BoundColumn>					                        
					</Columns>
				</asp:datagrid><asp:label id="lblMessage" runat="server" __designer:wfdid="w57">There are no transactions listed that match this request!</asp:label></td></tr></TBODY></TABLE>
	
</asp:Content>
