<%@ Page Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="ManageCommissions.aspx.cs" Inherits="localhost.Creditor.ManageCommissions" Title="Untitled Page" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCssLink" runat="server">
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="cphMain" runat="server">


<table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">	
	<tr>
		<td align="left">
			<p align="center"><br/>
				<asp:Label id="lblMonth" runat="server"><span class="Content Color2">Month:</span></asp:Label>&nbsp;
				<asp:DropDownList CssClass="InputForm" id="ddlMonth" runat="server" AutoPostBack="True" onselectedindexchanged="ddlMonth_SelectedIndexChanged"></asp:DropDownList>&nbsp;&nbsp;
				<asp:Button ID="btnSummary" runat="server" Text="Year Summary" OnClick="btnSummary_OnClick" Visible="true" />
			</p>
		</td>
		<td>
		    
		</td>
	</tr>
	<tr>
	    <td>
	        &nbsp;<asp:Label runat="server" ID="lblBrokerName" Text="" CssClass="Content Color2" Font-Size="Large"  Font-Underline="true"></asp:Label>
	    </td>
	</tr>
	<tr>
		<td>
			<p align="left">
				<font size="4"><u>Transaction Paid This Month</u></font></p>
		</td>
	</tr>
	<tr>
		<td>
			<div align="left">
				<asp:datagrid id="dgPaid" runat="server" CellPadding="2" ShowFooter="True" OnItemDataBound="KeepRunningSumPaid"
					AutoGenerateColumns="False" BorderColor="Black" BorderWidth="1px" HeaderStyle-CssClass="LinkNormal Bold OrangeColor" 
					ItemStyle-CssClass="LinkNormal DarkGray" CssClass="Content LinkNormal" AlternatingItemStyle-CssClass="LinkNormal LightGray">										
					<Columns>
						<asp:BoundColumn DataField="CUSTOMER" HeaderText="Customer">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
						</asp:BoundColumn>
						<asp:HyperLinkColumn HeaderText="Transaction Number" DataNavigateUrlField="TRANS_NUMBER" DataNavigateUrlFormatString="../administrator/Transaction_Details.aspx?Referer=/Creditor/ManageCommissions.aspx&amp;ID={0}"
							DataTextField="TRANS_NUMBER">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>						
						<asp:BoundColumn DataField="WEIGHT" HeaderText="Weight" DataFormatString="{0:#,###}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="BUY" HeaderText="Buy" DataFormatString="{0:#,###.0000}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="SELL" HeaderText="Sell" DataFormatString="{0:#,###.0000}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="FREIGHT" HeaderText="Freight" DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="LOGISTICS_COST" HeaderText="Logistics" DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="MISC" HeaderText="Misc" DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="GROSS_PROFIT_B4_CC_COMM" HeaderText="GP B4 CC Comm" DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
							<FooterStyle HorizontalAlign="Right"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="DAYS_PAST_DUE" HeaderText="Days Past Due">
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="CAPITAL_CHARGE" HeaderText="Capital Charge" DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="GROSS_PROFIT_B4_COMM" HeaderText="GP B4 Comm" DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
							<FooterStyle HorizontalAlign="Right"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="COMMISSION" HeaderText="Commission" DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
							<FooterStyle HorizontalAlign="Right"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="TRANSACTION_ID" HeaderText="" Visible="False">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
							<FooterStyle HorizontalAlign="Right"></FooterStyle>
						</asp:BoundColumn>
					</Columns>
				</asp:datagrid></div>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<p align="left"><font size="4"><u>Transaction With A/R But Not Paid</u></font></p>
		</td>
	</tr>
	<tr>
		<td>
			<div align="left"><asp:datagrid id="dgCurrent" runat="server" CellPadding="2" ShowFooter="True" OnItemDataBound="KeepRunningSum"
					AutoGenerateColumns="False" BorderColor="Black" BorderWidth="1px" HeaderStyle-CssClass="LinkNormal Bold OrangeColor" 
					ItemStyle-CssClass="LinkNormal DarkGray" CssClass="Content LinkNormal" AlternatingItemStyle-CssClass="LinkNormal LightGray" >																														
					<Columns>
					    <mbrsc:RowSelectorColumn AllowSelectAll="True"></mbrsc:RowSelectorColumn>
						<asp:BoundColumn DataField="CUSTOMER" HeaderText="Customer">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
						</asp:BoundColumn>
						<asp:HyperLinkColumn HeaderText="Transaction Number" DataNavigateUrlField="TRANS_NUMBER" DataNavigateUrlFormatString="../administrator/Transaction_Details.aspx?Referer=/Creditor/ManageCommissions.aspx&amp;ID={0}"
							DataTextField="TRANS_NUMBER">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:BoundColumn DataField="WEIGHT" HeaderText="Weight" DataFormatString="{0:#,###}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="BUY" HeaderText="Buy" DataFormatString="{0:#,###.0000}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="SELL" HeaderText="Sell" DataFormatString="{0:#,###.0000}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="FREIGHT" HeaderText="Freight" DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="LOGISTICS_COST" HeaderText="Logistics" DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="MISC" HeaderText="Misc" DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="GROSS_PROFIT_B4_CC_COMM" HeaderText="GP B4 CC Comm" DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
							<FooterStyle HorizontalAlign="Right"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="DAYS_PAST_DUE" HeaderText="Days Past Due">
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:BoundColumn>											
						<asp:BoundColumn DataField="CAPITAL_CHARGE" HeaderText="Capital Charge" DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="GROSS_PROFIT_B4_COMM" HeaderText="GP B4 Comm" DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
							<FooterStyle HorizontalAlign="Right"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="COMMISSION" HeaderText="Commission" DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
							<FooterStyle HorizontalAlign="Right"></FooterStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="TRANSACTION_ID" HeaderText="TRANSACTION_ID" Visible="false">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
						</asp:BoundColumn>
					</Columns>
				</asp:datagrid></div>
		</td>
	</tr>
	<tr>
	    <td>
	        <asp:Button id="btnPaid" runat="server" OnClick="btnPaid_OnClick" Text="Paid Commission" />&nbsp;<asp:Button id="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_OnClick" />
	    </td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<p align="left"><font size="4"><u>Transaction With No A/R</u></font></p>
		</td>
	</tr>
	<tr>
		<td>
			<asp:datagrid id="dgOpen" runat="server" CellPadding="2" ShowFooter="True" OnItemDataBound="KeepRunningSumOpen"
				AutoGenerateColumns="False" BorderColor="Black" BorderWidth="1px" HeaderStyle-CssClass="LinkNormal Bold OrangeColor" 
					ItemStyle-CssClass="LinkNormal DarkGray" CssClass="Content LinkNormal" AlternatingItemStyle-CssClass="LinkNormal LightGray">
				
				<Columns>
					<asp:BoundColumn DataField="CUSTOMER" HeaderText="Customer">
						<HeaderStyle Wrap="False"></HeaderStyle>
						<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
					</asp:BoundColumn>
					<asp:HyperLinkColumn HeaderText="Transaction Number" DataNavigateUrlField="TRANS_NUMBER" DataNavigateUrlFormatString="../administrator/Transaction_Details.aspx?Referer=/Creditor/ManageCommissions.aspx&amp;ID={0}"
							DataTextField="TRANS_NUMBER">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
					<asp:BoundColumn DataField="WEIGHT" HeaderText="Weight" DataFormatString="{0:#,###}">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="BUY" HeaderText="Buy" DataFormatString="{0:#,###.0000}">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="SELL" HeaderText="Sell" DataFormatString="{0:#,###.0000}">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="FREIGHT" HeaderText="Freight" DataFormatString="{0:#,###.00}">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="LOGISTICS_COST" HeaderText="Logistics" DataFormatString="{0:#,###.00}">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="MISC" HeaderText="Misc" DataFormatString="{0:#,###.00}">
							<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="GROSS_PROFIT_B4_CC_COMM" HeaderText="GP B4 CC Comm" DataFormatString="{0:#,###.00}">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
						<FooterStyle HorizontalAlign="Right"></FooterStyle>
					</asp:BoundColumn>					
				</Columns>
			</asp:datagrid></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>

</asp:Content>
