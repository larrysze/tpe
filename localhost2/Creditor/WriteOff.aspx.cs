using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;

namespace localhost.Creditor
{
    public partial class WriteOff : System.Web.UI.Page
    {
        double dbTotalWeight = 0;
        double dbTotalPrice = 0.0;
        double dbTotalReceived = 0.0;
        double dbTotalNet = 0.0;

        protected void Page_Load(object sender, EventArgs e)
        {
            Master.Width = "1300px";            

            //Administrator and Creditor have access
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C") && ((string)Session["Typ"] != "L"))
            {
                Response.Redirect("/default.aspx");
            }
            if (!IsPostBack)
            {
                // populating drop-down box for days late
                ViewState["Sort"] = "COMPNAME ASC";               

                Bind();
            }	
        }

        private void Bind()
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append(" SELECT ");
            sbSql.Append("COMPNAME =(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)), ");
            sbSql.Append("(CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+ CAST(SHIPMENT_SKU AS VARCHAR) )AS ID, ");
            sbSql.Append("SHIPMENT_DATE_TAKE,");
            sbSql.Append("VARWEIGHT = ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY),");
            sbSql.Append("VARWRITEOFFAMOUNT = ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * SHIPMENT_BUYR_PRCE, ");
            sbSql.Append("ISNULL((select sum(amount_received) from cash_received where transaction_number=(CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+ CAST(SHIPMENT_SKU AS VARCHAR) )), 0) as VARWRITEOFFReceived , ");
            sbSql.Append("(ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * SHIPMENT_BUYR_PRCE - ISNULL((select sum(amount_received) from cash_received where transaction_number=(CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+ CAST(SHIPMENT_SKU AS VARCHAR) )), 0) ) as VARWRITEOFFNet, ");
            sbSql.Append("SHIPMENT_WRITEOFF_COMMENT, ");
            sbSql.Append("SHIPMENT_WRITEOFF_DATE ");
            sbSql.Append("FROM ORDERS, SHIPMENT WHERE SHIPMENT_BUYR is not null and SHIPMENT_ORDR_ID=ORDR_ID and SHIPMENT_WRITEOFF = '1' ");
            sbSql.Append("ORDER BY COMPNAME");

            //<asp:BoundColumn DataField="SHIPMENTID" HeaderText="Shipment ID" SortExpression="SHIPMENTID ASC" />

            SqlDataAdapter dadContent;
            DataSet dstContent;

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                dadContent = new SqlDataAdapter(sbSql.ToString(), conn);
                dstContent = new DataSet();
                dadContent.Fill(dstContent);

                dg.DataSource = dstContent;
                dg.DataBind();

                if ((dg.Items.Count == 0))
                {
                    dg.Visible = false;                    
                }
                else
                {
                    dg.Visible = true;                    
                }
            }
        }


        protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
        {
            //Response.Write("1:<BR>");
            if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                //Response.Write("1:<BR>");
                dbTotalWeight += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARWEIGHT"));
		dbTotalPrice += HelperFunction.ConvertToDouble(DataBinder.Eval(e.Item.DataItem, "VARWRITEOFFAMOUNT"));
                
                //if (DataBinder.Eval(e.Item.DataItem, "SHIPMENT_DATE_TAKE") is DBNull)
                //{
                //    e.Item.Cells[14].Text = "---";
                //}
                //else
                //{
                //    e.Item.Cells[14].Text = String.Format("{0:c}", Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARSELLPRICE")) * Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARWEIGHT")));
                //}


            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                e.Item.Cells[1].Text = "<b>Totals:</b> ";
                e.Item.Cells[3].Text = "<b>" + String.Format("{0:#,###}", dbTotalWeight) + "</b>";
                e.Item.Cells[4].Text = "<b>" + String.Format("{0:c}", dbTotalPrice) + "</b>";                
            }
        }
    }
}
