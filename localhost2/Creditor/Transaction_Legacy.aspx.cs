using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using TPE.Utility;

namespace localhost.Creditor
{
    public partial class Transaction_Legacy : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C"))
            {
                Response.Redirect("../default.aspx");
            }
            Session["CompID"] = "";
            Master.Width = "1500px";
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtTransactionNumber.Text != "")
            {
                char[] sep = { '-' };
                string[] aux = txtTransactionNumber.Text.Split(sep);

		if(aux.Length!=2)
		{
			lblSearchResult.Text = "Invalid Transaction Number";
			return;
		}

                ViewState["ShipmentOrderId"] = aux[0];
                ViewState["ShipmentSku"] = aux[1];
            }

            Bind();

            if ((dg.Items.Count == 0))
            {
                dg.Visible = false;
                lblSearchResult.Text = "There are no transactions listed that match this request.";
            }
            else
            {
                dg.Visible = true;
                lblSearchResult.Text = "Yes, we have it. ";

                txtLegacyNumber.Visible = true;
                btnAdd.Visible = true;
                lblAddResult.Visible = true;
                lblLegacyNumber.Visible = true;
            }
        }


        protected void Bind()
        {
            System.Text.StringBuilder sbSql = new StringBuilder();
            sbSql.Append(GetSQL());

            sbSql.Append(" WHERE SHIPMENT_ORDR_ID='" + (string)ViewState["ShipmentOrderId"] + "' AND SHIPMENT_SKU='" + (string)ViewState["ShipmentSku"] + "' ");

            SqlDataAdapter dadContent;
            DataSet dstContent;

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                dadContent = new SqlDataAdapter(sbSql.ToString(), conn);
                dstContent = new DataSet();
                dadContent.Fill(dstContent);

                dg.DataSource = dstContent;
                dg.DataBind();
            }
        }


        private string GetSQL()
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append(" SELECT *,");
            sbSql.Append("CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR) AS ID, ");
            sbSql.Append("VARWEIGHT = ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY),");
            sbSql.Append("VARRCNUMBER = ISNULL(SHIPMENT_COMMENT, '-'),");
            
            sbSql.Append("SHIPMENT_TRANSACTION_DATE AS ORDR_DTE FROM (SELECT *,");
            sbSql.Append("VARTPE=VARFEE-VARCREDIT FROM (SELECT *,");
            sbSql.Append("DATEADD(d,CAST(VARTERM AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)) As VARNETDUEBUYR,");
            sbSql.Append("VARSPAID=ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_SELR),0),");
            sbSql.Append("VARPPAID=ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),0),");
            sbSql.Append("VARPPAIDDATE=ISNULL((SELECT CONVERT(VARCHAR,MAX(PAY_DATE),101) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),'-'),");
            sbSql.Append("DATEADD(d,CAST(SHIPMENT_SELLER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)) AS VARNETDUESELR,");
            sbSql.Append("VAROWESBUYR=VARTVBUYR,");
            sbSql.Append("VAROWESSELR=VARTVSELR,");
            sbSql.Append("VARGROSS=(CASE WHEN VARTVBUYR-VARTVSELR<0 THEN 0 ELSE VARTVBUYR-VARTVSELR END),");
            //Larry -- get the correct price with estimate and invoice
            sbSql.Append("VARCOMMISION=SHIPMENT_COMM * ((( SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) * (ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-ISNULL(SHIPMENT_SHIP_PRCE_INV,SHIPMENT_SHIP_PRCE_EST))- ISNULL(SHIPMENT_MISC_INV,SHIPMENT_MISC_EST)-ISNULL(SHIPMENT_WHAREHOUSE_PRCE_INV,SHIPMENT_WHAREHOUSE_PRCE_EST)) / 100,");
            
            sbSql.Append("VARFEE=((SHIPMENT_BUYR_PRCE-SHIPMENT_PRCE)*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-ISNULL(SHIPMENT_SHIP_PRCE_INV,SHIPMENT_SHIP_PRCE_EST) - ISNULL(SHIPMENT_WHAREHOUSE_PRCE_INV,SHIPMENT_WHAREHOUSE_PRCE_EST)- ISNULL(SHIPMENT_MISC_INV,SHIPMENT_MISC_EST) - (SHIPMENT_COMM * ((( SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) * ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-ISNULL(SHIPMENT_SHIP_PRCE_INV,SHIPMENT_SHIP_PRCE_EST) - ISNULL(SHIPMENT_MISC_INV,SHIPMENT_MISC_EST) - ISNULL(SHIPMENT_WHAREHOUSE_PRCE_INV,SHIPMENT_WHAREHOUSE_PRCE_EST))/100),");
            
            sbSql.Append("VARFREIGHT = ISNULL(SHIPMENT_SHIP_PRCE_INV,SHIPMENT_SHIP_PRCE_EST),");
            
            sbSql.Append("VARWHAREHOUSE = ISNULL(SHIPMENT_WHAREHOUSE_PRCE_INV,SHIPMENT_WHAREHOUSE_PRCE_EST),");
            sbSql.Append("VARMISC = ISNULL(SHIPMENT_MISC_INV,SHIPMENT_MISC_EST),");
            sbSql.Append("VARCREDIT=VARTVBUYR*0.001,");
            sbSql.Append("VARHANOVER=VARTVBUYR*0.005 FROM (SELECT *,");
            sbSql.Append("VARBUYPRICE = LEFT(SHIPMENT_BUYR_PRCE,6),");
            sbSql.Append("VARSELLPRICE = LEFT(SHIPMENT_PRCE,6),");
            sbSql.Append("VARTVBUYR=(SHIPMENT_BUYR_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (100 + ISNULL(SHIPMENT_TAX, 0)) / 100),");
            sbSql.Append("VARTVSELR=SHIPMENT_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (100 + ISNULL(SHIPMENT_TAX, 0)) / 100,");
            sbSql.Append("VARBUYR=(SELECT LEFT(COMP_NAME,10)+'...' FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)),");
            sbSql.Append("VARSELR=(SELECT LEFT(COMP_NAME,10)+'...' FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)),");
            sbSql.Append("VARBUYR_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR),");
            sbSql.Append("VARSELR_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR),");
            sbSql.Append("BPAYMENTS=(SELECT TOP 1 PAY_ORDR FROM PAYMENT WHERE PAY_ORDR=SHIPMENT_ORDR_ID AND PAY_PERS=SHIPMENT_BUYR),");
            sbSql.Append("SPAYMENTS=(SELECT TOP 1 PAY_ORDR FROM PAYMENT WHERE PAY_ORDR=SHIPMENT_ORDR_ID AND PAY_PERS=SHIPMENT_SELR) FROM (SELECT *,");
            sbSql.Append("VARTERM=ISNULL(SHIPMENT_BUYER_TERMS,30),");
            sbSql.Append("VARDATE=CONVERT(VARCHAR,ORDR_DATE,101),");
            sbSql.Append("VARCONTRACT=LEFT(ISNULL(Shipment_Product_Spot,ORDR_PROD_SPOT),10)+'...' ");
            sbSql.Append("FROM ORDERS, SHIPMENT WHERE SHIPMENT_BUYR is not null and SHIPMENT_ORDR_ID=ORDR_ID  )Q0)Q1)Q2 )Q3   ");

            return sbSql.ToString();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (ViewState["ShipmentOrderId"] == null || ViewState["ShipmentSku"] == null)
            {
                lblAddResult.Text = "No transaction number";
               return;
            }

            string strSqlInsert = "update SHIPMENT set  SHIPMENT_LEGACY_NUMBER=@LEGACY_NUMBER where SHIPMENT_ORDR_ID=@SHIPMENT_ID and SHIPMENT_SKU=@SHIPMENT_SKU;";

            Hashtable ht = new Hashtable();
            ht.Add("@LEGACY_NUMBER", txtLegacyNumber.Text);
            ht.Add("@SHIPMENT_ID", (string)ViewState["ShipmentOrderId"] );
            ht.Add("@SHIPMENT_SKU", (string)ViewState["ShipmentSku"] );
            
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlInsert, ht);
            
            Bind();

            lblAddResult.Text = "You have just updated the transaction " + (string)ViewState["ShipmentOrderId"] + "-" + (string)ViewState["ShipmentSku"] + " \n with legacy number " + txtLegacyNumber.Text;
        }
    }
}