<%@ Page language="c#" Codebehind="AllPayablesReceivablesReport.aspx.cs" AutoEventWireup="True" EnableViewState="false" Inherits="localhost.Creditor.AllPayablesReport" MasterPageFile="~/MasterPages/Menu.Master" Title="The Plastics Exchange" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
  
   	
<asp:DropDownList CssClass="InputForm" ID=ddlPR Runat=server AutoPostBack="True" onselectedindexchanged="ddlPR_SelectedIndexChanged">
<asp:ListItem Value="1" Selected="True">Payables</asp:ListItem>
<asp:ListItem Value="2">Payables (with details)</asp:ListItem>
<asp:ListItem Value="3">Receivables</asp:ListItem>
<asp:ListItem Value="4">Receivables (with details)</asp:ListItem>
   	</asp:DropDownList>

<asp:DataGrid id=dgPayables BackColor="#000000" CellSpacing="1" BorderWidth="0" runat="server" DataKeyField="VARID" Width="900px" CssClass="DataGrid" ItemStyle-CssClass="DataGridRow" AlternatingItemStyle-CssClass="DataGridRow_Alternate" HeaderStyle-CssClass="DataGridHeader" CellPadding="2" horizontalalign="Center" ShowFooter="True" AutoGenerateColumns="False">
<AlternatingItemStyle CssClass="LinkNormal Color2 DarkGray"></AlternatingItemStyle>
<ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
<HeaderStyle CssClass="Header Color2 Bold OrangeColor"></HeaderStyle>
<FooterStyle CssClass="Content Color4 Bold FooterColor" />

<Columns>
<asp:HyperLinkColumn DataNavigateUrlField="VARID" DataNavigateUrlFormatString="../administrator/Transaction_Details.aspx?Referer=/Creditor/Transaction_Summary.aspx&amp;ID={0}" DataTextField="VARID" HeaderText="Order Number">
<ItemStyle Wrap="False" HorizontalAlign="Center">
</ItemStyle>
</asp:HyperLinkColumn>
<asp:BoundColumn DataField="VARCOMPANY" HeaderText="Buyer/Seller">
<ItemStyle Wrap="False" HorizontalAlign="Left">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VAROWES" HeaderText="Value Owed" DataFormatString="{0:c}">
<ItemStyle Wrap="False" HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARTV" HeaderText="Transaction Value" DataFormatString="{0:c}">
<ItemStyle Wrap="False" HorizontalAlign="Right">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARNETDUE" HeaderText="Due Date">
<ItemStyle Wrap="False" HorizontalAlign="Center">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="PAY_DATE" HeaderText="Pay Date"></asp:BoundColumn>
<asp:BoundColumn DataField="VARDIFF" HeaderText="Days Late">
<ItemStyle Wrap="False" HorizontalAlign="Center">
</ItemStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="VARAMOUNT" HeaderText="Amount Paid" DataFormatString="{0:c}"></asp:BoundColumn>
<asp:BoundColumn DataField="PAY_CMNT" HeaderText="Comment"></asp:BoundColumn>
<asp:BoundColumn DataField="PAY_AMNT" HeaderText="Payment Amount" DataFormatString="{0:c}"></asp:BoundColumn>
</Columns>
	</asp:DataGrid>
 

	
</asp:Content>