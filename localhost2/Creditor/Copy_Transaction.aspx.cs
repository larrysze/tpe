using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Creditor
{
	public partial class Copy_Transaction : System.Web.UI.Page
	{
		protected void Page_Load( object sender, EventArgs e )
		{

			if( ( (string)Session["Typ"] != "A" ) && ( (string)Session["Typ"] != "C" ) )
			{
				Response.Redirect("../default.aspx");
			}
			Session["CompID"] = "";
			Master.Width = "1500px";
		}

		protected void btnSearch_Click( object sender, EventArgs e )
		{
			char[] sep = { '-' };
			string[] transactionNumber = txtTransactionNumber.Text.Split(sep);
			
			if( txtTransactionNumber.Text != "" )
			{
				if( transactionNumber.Length != 2 )
				{
					lblSearchResult.Text = "Invalid Transaction Number";
					return;
				}
				ViewState["ShipmentOrderId"] = transactionNumber[0];
				ViewState["ShipmentSku"] = transactionNumber[1];
			}
			Bind();

			if( ( dg.Items.Count == 0 ) )
			{
				dg.Visible = false;
				lblSearchResult.Text = "There are no transactions listed that match this request.";
			}
			else
			{
				dg.Visible = true;
				lblSearchResult.Text = "Yes, we have it. ";

				lblOrderNumber.Text = transactionNumber[0]+" - ";
				lblOrderNumber.Visible = true;
				txtNewSkuNumber.Visible = true;
				
				btnAdd.Visible = true;
				lblAddResult.Visible = true;
				lblNewTransactionNumber.Visible = true;
			}
		}


		protected void Bind()
		{
			System.Text.StringBuilder sbSql = new StringBuilder();
			sbSql.Append(GetSQL());

			sbSql.Append(" WHERE SHIPMENT_ORDR_ID='" + (string)ViewState["ShipmentOrderId"] + "' AND SHIPMENT_SKU='" + (string)ViewState["ShipmentSku"] + "' ");

			SqlDataAdapter dadContent;
			DataSet dstContent;

			using( SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()) )
			{
				dadContent = new SqlDataAdapter(sbSql.ToString(), conn);
				dstContent = new DataSet();
				dadContent.Fill(dstContent);

				dg.DataSource = dstContent;
				dg.DataBind();
			}
		}


		private string GetSQL()
		{
			StringBuilder sbSql = new StringBuilder();
			sbSql.Append(" SELECT *,");
			sbSql.Append("CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR) AS ID, ");
			sbSql.Append("VARWEIGHT = ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY),");
			sbSql.Append("VARRCNUMBER = ISNULL(SHIPMENT_COMMENT, '-'),");
			sbSql.Append("SHIPMENT_TRANSACTION_DATE AS ORDR_DTE FROM (SELECT *,");
			sbSql.Append("VARTPE=VARFEE-VARCREDIT FROM (SELECT *,");
			sbSql.Append("DATEADD(d,CAST(VARTERM AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)) As VARNETDUEBUYR,");
			sbSql.Append("VARSPAID=ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_SELR),0),");
			sbSql.Append("VARPPAID=ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),0),");
			sbSql.Append("VARPPAIDDATE=ISNULL((SELECT CONVERT(VARCHAR,MAX(PAY_DATE),101) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),'-'),");
			sbSql.Append("DATEADD(d,CAST(SHIPMENT_SELLER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)) AS VARNETDUESELR,");
			sbSql.Append("VAROWESBUYR=VARTVBUYR,");
			sbSql.Append("VAROWESSELR=VARTVSELR,");
			sbSql.Append("VARGROSS=(CASE WHEN VARTVBUYR-VARTVSELR<0 THEN 0 ELSE VARTVBUYR-VARTVSELR END),");
			//Larry -- get the correct price with estimate and invoice
			sbSql.Append("VARCOMMISION=SHIPMENT_COMM * ((( SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) * (ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-ISNULL(SHIPMENT_SHIP_PRCE_INV,SHIPMENT_SHIP_PRCE_EST))- ISNULL(SHIPMENT_MISC_INV,SHIPMENT_MISC_EST)-ISNULL(SHIPMENT_WHAREHOUSE_PRCE_INV,SHIPMENT_WHAREHOUSE_PRCE_EST)) / 100,");
			sbSql.Append("VARFEE=((SHIPMENT_BUYR_PRCE-SHIPMENT_PRCE)*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-ISNULL(SHIPMENT_SHIP_PRCE_INV,SHIPMENT_SHIP_PRCE_EST) - ISNULL(SHIPMENT_WHAREHOUSE_PRCE_INV,SHIPMENT_WHAREHOUSE_PRCE_EST)- ISNULL(SHIPMENT_MISC_INV,SHIPMENT_MISC_EST) - (SHIPMENT_COMM * ((( SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) * ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-ISNULL(SHIPMENT_SHIP_PRCE_INV,SHIPMENT_SHIP_PRCE_EST) - ISNULL(SHIPMENT_MISC_INV,SHIPMENT_MISC_EST) - ISNULL(SHIPMENT_WHAREHOUSE_PRCE_INV,SHIPMENT_WHAREHOUSE_PRCE_EST))/100),");
			sbSql.Append("VARFREIGHT = ISNULL(SHIPMENT_SHIP_PRCE_INV,SHIPMENT_SHIP_PRCE_EST),");
			sbSql.Append("VARWHAREHOUSE = ISNULL(SHIPMENT_WHAREHOUSE_PRCE_INV,SHIPMENT_WHAREHOUSE_PRCE_EST),");
			sbSql.Append("VARMISC = ISNULL(SHIPMENT_MISC_INV,SHIPMENT_MISC_EST),");
			sbSql.Append("VARCREDIT=VARTVBUYR*0.001,");
			sbSql.Append("VARHANOVER=VARTVBUYR*0.005 FROM (SELECT *,");
			sbSql.Append("VARBUYPRICE = LEFT(SHIPMENT_BUYR_PRCE,6),");
			sbSql.Append("VARSELLPRICE = LEFT(SHIPMENT_PRCE,6),");
			sbSql.Append("VARTVBUYR=(SHIPMENT_BUYR_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (100 + ISNULL(SHIPMENT_TAX, 0)) / 100),");
			sbSql.Append("VARTVSELR=SHIPMENT_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (100 + ISNULL(SHIPMENT_TAX, 0)) / 100,");
			sbSql.Append("VARBUYR=(SELECT LEFT(COMP_NAME,10)+'...' FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)),");
			sbSql.Append("VARSELR=(SELECT LEFT(COMP_NAME,10)+'...' FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)),");
			sbSql.Append("VARBUYR_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR),");
			sbSql.Append("VARSELR_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR),");
			sbSql.Append("BPAYMENTS=(SELECT TOP 1 PAY_ORDR FROM PAYMENT WHERE PAY_ORDR=SHIPMENT_ORDR_ID AND PAY_PERS=SHIPMENT_BUYR),");
			sbSql.Append("SPAYMENTS=(SELECT TOP 1 PAY_ORDR FROM PAYMENT WHERE PAY_ORDR=SHIPMENT_ORDR_ID AND PAY_PERS=SHIPMENT_SELR) FROM (SELECT *,");
			sbSql.Append("VARTERM=ISNULL(SHIPMENT_BUYER_TERMS,30),");
			sbSql.Append("VARDATE=CONVERT(VARCHAR,ORDR_DATE,101),");
			sbSql.Append("VARCONTRACT=LEFT(ISNULL(Shipment_Product_Spot,ORDR_PROD_SPOT),10)+'...' ");
			sbSql.Append("FROM ORDERS, SHIPMENT WHERE SHIPMENT_BUYR is not null and SHIPMENT_ORDR_ID=ORDR_ID  )Q0)Q1)Q2 )Q3   ");

			return sbSql.ToString();
		}

		protected void btnAdd_Click( object sender, EventArgs e )
		{
			if( ViewState["ShipmentOrderId"] == null || ViewState["ShipmentSku"] == null )
			{
				lblAddResult.Text = "No transaction number";
				return;
			}
			
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			try
			{
				conn.Open();
				SqlCommand cmdShipment;
				SqlDataReader dtrShipment;
				cmdShipment = new SqlCommand("SELECT * FROM SHIPMENT WHERE SHIPMENT_ORDR_ID = " + ViewState["ShipmentOrderId"].ToString() + " AND SHIPMENT_SKU = '" + txtNewSkuNumber.Text + "' ", conn);
				dtrShipment = cmdShipment.ExecuteReader();

				try
				{
					if( dtrShipment.HasRows )
					{
						lblAddResult.Text = "Creating Failed , Transaction " + ViewState["ShipmentOrderId"].ToString() + "-" + txtNewSkuNumber.Text + " already exists.";
						dtrShipment.Close();
						conn.Close();
						return;
					}
				}
				finally
				{
					dtrShipment.Close();
				}
			}
			finally
			{
				conn.Close();
			}


            string strSqlInsert = "INSERT INTO shipment SELECT @NEW_ORDR_ID,@NEW_SKU,shipment_buyr,shipment_selr,shipment_from,shipment_to,shipment_qty,shipment_prce,shipment_comm,shipment_mark,shipment_ship_prce_est,shipment_ship_price_status,shipment_size,shipment_ship_status,shipment_date_take,shipment_date_delivered,shipment_weight,shipment_fob,shipment_mnth, shipment_invoice_email_to, shipment_invoice_sent, shipment_po_email_to, shipment_po_sent, shipment_po_num, shipment_broker_id,shipment_enabled, shipment_updated, shipment_buyer_terms, shipment_seller_terms, shipment_buyer_closed_date, shipment_seller_closed_date1, shipment_comment, shipment_buyr_prce, shipment_date_emptied, shipment_tax, shipment_wharehouse_prce_est, internal_notes, shipment_wharehouse_price_status, shipment_problemflag, shipment_wharehouse_prce_inv, shipment_ship_prce_inv, shipment_freight_comp_id,shipment_wharehouse_comp_id,shipment_customs, shipment_freight_inv_num, shipment_wharehouse_inv_num,shipment_freight_inv_date, shipment_wharehouse_inv_date, shipment_misc_est, shipment_misc_inv,shipment_misc_description, shipment_writeoff, shipment_writeoff_comment, shipment_writeoff_date,shipment_transaction_date, shipment_legacy_number, shipment_lot_number, shipment_product_spot, shipment_release_number, shipment_qtax, shipment_htax FROM shipment WHERE shipment_ordr_id=@SHIPMENT_ORDR_ID and shipment_sku=@SHIPMENT_SKU ";

			Hashtable ht = new Hashtable();
			ht.Add("@NEW_ORDR_ID", ViewState["ShipmentOrderId"].ToString());
			ht.Add("@NEW_SKU", txtNewSkuNumber.Text);
			ht.Add("@SHIPMENT_ORDR_ID", (string)ViewState["ShipmentOrderId"]);
			ht.Add("@SHIPMENT_SKU", (string)ViewState["ShipmentSku"]);

			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlInsert, ht);

			Bind();

			lblAddResult.Text = "You have just created the transaction " + ViewState["ShipmentOrderId"].ToString() + "-" + txtNewSkuNumber.Text + " from " + (string)ViewState["ShipmentOrderId"] + "-" + (string)ViewState["ShipmentSku"];
		}
	}
}
