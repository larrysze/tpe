using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using TPE.Utility;

namespace localhost.Creditor
{
    public partial class CanadianReport : System.Web.UI.Page
    {

        public void Page_Load(object sender, EventArgs e)
        {
            Master.Width = "1300px";

            if ((string)Session["Typ"] != "A")
            {
                Response.Redirect("../default.aspx");
            }

            if (!IsPostBack)
            {
                //Populate ddlYear:
                using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
                {
                    conn.Open();

                    string strSQL = "select t.y from (select year(ordr_date)y from Orders group by year(ordr_date))t order by t.y desc";
                    using (SqlDataReader dtrTrans = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
                    {
                        while (dtrTrans.Read())
                        {
                            ddlYear.Items.Add(dtrTrans["y"].ToString());
                        }
                    }
                    //First generation of DataGrid:
                    ViewState["LastSort"] = "'Order #' DESC";
                    if (ddlYear.Items.Count > 0)
                        ddlYear_SelectedIndexChanged(sender, e);
                }
            }
        }

        protected DataSet DS(string Sort)
        {
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                string sSql = string.Format("select (CONVERT(VARCHAR,s.shipment_ordr_id) + '-' + s.shipment_sku) 'Order #', CONVERT(VARCHAR,o.ordr_date,101) 'Order Date', CONVERT(VARCHAR,SHIPMENT_DATE_DELIVERED,101) 'Shipment Date', (ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY)* SHIPMENT_BUYR_PRCE) 'Total Owed', ISNULL(p.pay_amnt,0) 'Total Received', (ISNULL(p.pay_amnt,0) - (ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY)* SHIPMENT_BUYR_PRCE)) Balance, c.comp_name Buyer, (l2.locl_city + ', ' + l2.locl_stat + ', ' + l2.locl_ctry) 'To', c2.comp_name Seller, (l1.locl_city + ', ' + l1.locl_stat + ', ' + l1.locl_ctry) 'From' from shipment s, orders o, person pe, person pe2, company c, company c2, (select pay_ordr, pay_pers, pay_shipment, sum(pay_amnt) pay_amnt from payment group by pay_ordr, pay_pers, pay_shipment) p, place p1, locality l1, place p2, locality l2 where s.shipment_id *= p.pay_shipment and s.shipment_buyr = pe.pers_id and pe.pers_comp = c.comp_id and s.shipment_selr = pe2.pers_id and pe2.pers_comp = c2.comp_id and o.ordr_id = s.shipment_ordr_id and s.shipment_buyr *= p.pay_pers and s.shipment_from = p1.plac_id and p1.plac_locl = l1.locl_id and s.shipment_to = p2.plac_id and p2.plac_locl = l2.locl_id and (l1.locl_ctry='CA' or l2.locl_ctry='CA') and year(o.ordr_date)={0} order by " + Sort, ddlYear.SelectedValue);
                SqlDataAdapter MyCommand = new SqlDataAdapter(sSql, conn);
                DataSet DSet = new DataSet();
                MyCommand.Fill(DSet, "tblDynamicText");
                conn.Close();
                return DSet;
            }
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            dg.DataSource = DS(ViewState["LastSort"].ToString());
            dg.DataBind();
        }

        protected void SortDG(Object sender, DataGridSortCommandEventArgs e)
        {
            Regex r = new Regex(" ");
            string[] OldSort = r.Split(ViewState["LastSort"].ToString());
            if (OldSort.Length > 2)
            {
                for (int i = 1; i < OldSort.Length - 1; i++)
                    OldSort[0] += " " + OldSort[i];
                OldSort[1] = OldSort[2];
            }
            string NewSortParam = "DESC";
            if (OldSort[0] == e.SortExpression && OldSort[1] == NewSortParam)
                NewSortParam = "ASC";
            ViewState["LastSort"] = e.SortExpression + " " + NewSortParam;

            dg.DataSource = DS(ViewState["LastSort"].ToString());
            dg.DataBind();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=CanadianTransactions.xls");

            Response.BufferOutput = true;
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Charset = "";
            EnableViewState = false;

            DataGrid dg = new DataGrid();
            dg.DataSource = DS(ViewState["LastSort"].ToString()).Tables[0].DefaultView;

            dg.DataBind();
            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(tw);
            dg.RenderControl(hw);
            Response.Write(tw.ToString());
            Response.End();
        }

    }
}
