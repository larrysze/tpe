using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using TPE.Utility;

namespace localhost.Creditor
{
	/// <summary>
	/// Summary description for Transaction_Summary.
	/// </summary>
	public partial class Transaction_Summary : System.Web.UI.Page
	{

		protected System.Web.UI.WebControls.Button Button3;
		protected System.Web.UI.WebControls.Button Button4;
		protected System.Web.UI.WebControls.PlaceHolder phActionButton;

		#region Web Form Designer generated code
		override protected void OnInit( EventArgs e )
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dg.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dg_SortCommand);
		}
		#endregion

		double interest = 0.0;
		double limit = 0.0;

		enum ColHeaders : int
		{
			ID=1,
			ORDR_DTE=2,
			CONTRACT=3,
			WEIGHT=4,
			RCNUMBER=5,
			FROMBUYER=6,
			BUYERPAID=7,
			BUYPRICE=8,
			SELLPRICE=9,
			TOSELLER=10,
			SELLERPAID=11,
			FREIGHT=12,
            WAREHOUSE = 13,
			WHAREHOUSE=14,
			MISC=15,
			COMMISION=16,
			BROKER=17,
			SHIPMENT_BUYER_TERMS=18,
			DAYSPASTDUE=19,
			INTEREST=20,
			FEE=21,
			BUYER=22,
			SELLER=23,
			ShipmentDate=24,
			PAIDDATE=25            
		}


		protected void Page_Load(object sender, EventArgs e)
		{
            
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C") && ((string)Session["Typ"] != "X"))
			{
				Response.Redirect("../default.aspx");
			}
			Session["CompID"] = "";
			Master.Width = "1500px";
		
			if (!IsPostBack)
			{
				HelperFunction.fillCompany(ddlCompanies, this.Context);
				
				CreateDateList();
				CreateBrokerList();
				CreateIndexList();
                CreateGradeList();

                lblSort.Text = "ORDR_ID DESC";
				Bind();
			}
			HelperFunction.renderDisabledCompanyList(ddlCompanies, this.Context);
		}


		private void dg_SortCommand( object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e )
		{
			if( ViewState["SortDirection"] == null )
				ViewState.Add("SortDirection", "ASC");
			else
			{
				if( ViewState["SortDirection"].ToString() == "ASC" )
				{
					ViewState["SortDirection"] = "DESC";
				}
				else
				{
					ViewState["SortDirection"] = "ASC";
				}
			}
			ViewState["SortExpression"] = e.SortExpression.ToString();
			Bind();
		}

		// <summary>
		//  searches past months to see which months have transactions pending
		// </summary>
		protected void CreateDateList()
		{
			ddlMonth.Items.Clear(); // starts the list out fresh
            ddlMonth.Items.Add(new ListItem("All Months", "*"));
            ddlMonth.Items.Add(new ListItem("January", "1"));
            ddlMonth.Items.Add(new ListItem("February", "2"));
            ddlMonth.Items.Add(new ListItem("March", "3"));
            ddlMonth.Items.Add(new ListItem("April", "4"));
            ddlMonth.Items.Add(new ListItem("May", "5"));
            ddlMonth.Items.Add(new ListItem("June", "6"));
            ddlMonth.Items.Add(new ListItem("July", "7"));
            ddlMonth.Items.Add(new ListItem("August", "8"));
            ddlMonth.Items.Add(new ListItem("September", "9"));
            ddlMonth.Items.Add(new ListItem("October", "10"));
            ddlMonth.Items.Add(new ListItem("November", "11"));
            ddlMonth.Items.Add(new ListItem("December", "12"));

            DateTime today = DateTime.Now;
            ddlMonth.Items[today.Month].Selected = true;


            ddlYear.Items.Clear();
            int yearcount = 20;
            ddlYear.Items.Add(new ListItem("All Years", "*"));
            for (int i = 0; i < yearcount; i++)
            {
                int loopyear = DateTime.Now.Year - i;
                ddlYear.Items.Add(new ListItem(loopyear.ToString(), loopyear.ToString()));
            }
            ddlYear.SelectedItem.Selected = false;
            ddlYear.Items[1].Selected = true;
		}

		// <summary>
		// Updated the dbase page of the grid
		// </summary>
		protected void dg_PageIndexChanged(object sender,DataGridPageChangedEventArgs e)
		{
			dg.CurrentPageIndex = e.NewPageIndex;
			// rebind grid
			Bind();
		}
    
		// <summary>
		// Binds the content data.  This is always called after strOrderBy has been determined
		// </summary>
		protected void Bind()
		{    
			interest = HelperFunction.getInterestBankParameter(this.Context);
			limit = HelperFunction.getInterestLimitParameter(this.Context);

			Hashtable htParams = new Hashtable();

            if (Session["Typ"].ToString() == "X")
            {
                string ID = Session["Id"].ToString();
                htParams.Add("@PersId", ID);
            }

            if (!ddlMonth.SelectedItem.Value.Equals("null")&& (txtSearch.Text!="") || !ddlBroker.SelectedItem.Value.Equals("null") || !ddlCompanies.SelectedItem.Value.Equals("*") )
			{
				htParams.Add("@DateBase", "True");
			}			

			if ( !ddlBroker.SelectedItem.Value.Equals("*"))
			{
				htParams.Add("@BrokerSelected", ddlBroker.SelectedItem.Value.ToString());
				
			}
			if ( !ddlCompanies.SelectedItem.Value.Equals("*"))
			{
				htParams.Add("@CompanyId", ddlCompanies.SelectedValue.ToString());
			}

            if (!ddlGrade.SelectedItem.Value.Equals("*"))
            {
                htParams.Add("@GradeSelected", ddlGrade.SelectedItem.Value.ToString());

            }

            if (!ddlMonth.SelectedItem.Value.Equals("*") )
			{
				
				htParams.Add("@CurrentMonth", ddlMonth.SelectedValue.ToString());
			}

            if (!ddlYear.SelectedItem.Value.Equals("*"))
            {
                htParams.Add("@CurrentYear", ddlYear.SelectedValue.ToString());
            }

            if (txtSearch.Text!="")
			{			
				string searchString = DBLibrary.ScrubSQLStringInput(txtSearch.Text);

				if(searchString.Contains("-"))
				{
					char [] sep=new Char[1];
					sep[0]='-';
					String [] aux=searchString.Split(sep);
					searchString = aux[0];
				}

				htParams.Add("@Search", searchString);

				if (!ddlIndex.SelectedValue.Equals("*"))
				{
					if( ddlIndex.SelectedValue.Equals("1") )
						htParams.Add("@SearchField", "SHIPMENT_ORDR_ID");
					if (ddlIndex.SelectedValue.Equals("2"))
						htParams.Add("@SearchField", "SHIPMENT_PO_NUM");	
					if (ddlIndex.SelectedValue.Equals("3"))
						htParams.Add("@SearchField", "SHIPMENT_FOB");
					if (ddlIndex.SelectedValue.Equals("4"))
						htParams.Add("@SearchField", " VARBUYR");
					if (ddlIndex.SelectedValue.Equals("5"))
						htParams.Add("@SearchField", " VARSELR");
					if (ddlIndex.SelectedValue.Equals("6"))
						htParams.Add("@SearchField", "VARFREIGHT");
					if( ddlIndex.SelectedValue.Equals("7") )
						htParams.Add("@SearchField", "VARWAREHOUSE");
					if (ddlIndex.SelectedValue.Equals("8"))
						htParams.Add("@SearchField", "VARWHAREHOUSE");
					if (ddlIndex.SelectedValue.Equals("9"))
						htParams.Add("@SearchField", "VARBROKER");
					if (ddlIndex.SelectedValue.Equals("10"))
						htParams.Add("@SearchField", "shipment_comment");
					if (ddlIndex.SelectedValue.Equals("11"))
						htParams.Add("@SearchField", "ORDR_DTE");
					if( ddlIndex.SelectedValue.Equals("12") )
						htParams.Add("@SearchField", "SHIPMENT_WEIGHT");
				}

                DateTime searchDate;
                if (DateTime.TryParse(txtSearch.Text, out searchDate))
                {
                    if (!ddlMonth.SelectedItem.Value.Equals("*"))
                    {
                        htParams.Remove("CurrentYear");
                        htParams.Remove("CurrentMonth");
                    }
                    htParams.Add("@CurrentDay", searchDate.Day);
                    htParams.Add("@CurrentMonth", searchDate.Month);
                    htParams.Add("@CurrentYear", searchDate.Year);
                    htParams.Remove("@Search");
                }


			}
			
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				DataTable dataTable = DBLibrary.GetDataTableFromStoredProcedure(conn, "spTransactionSummary", htParams);
				DataView dataView = new DataView(dataTable);

				if( ViewState["SortExpression"] != null )
				{
					dataView.Sort = ViewState["SortExpression"].ToString();
					if( ViewState["SortDirection"] != null )
						dataView.Sort += " " + ViewState["SortDirection"].ToString();
				}		

				dg.DataSource = dataView;
				dg.DataBind();
				
				if ((dg.Items.Count==0) )
				{
					dg.Visible=false;	
					lblMessage.Visible = true;
				}
				else
				{
					dg.Visible=true;
					lblMessage.Visible = false;
				}
			}
		}
    
		double dbMargin = 0.0;
		double dbTotalCommision = 0.0;
		double dbTotalTVBuyr = 0.0;
		double dbTotalBuyrPaid = 0.0;
		double dbTotalTVSelr = 0.0;
		double dbTotalSelrPaid = 0.0;
		double dbTotalFreight = 0.0;
		double dbTotalWharehouse = 0.0;
		double dbTotalMisc = 0.0;
		double iTotalWeight = 0;
		int iLinkSetCount = 1;

		protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
			// determines if the order is a railcar end of railcar to bulk truck
	
			int iSKU = HelperFunction.ConvertToString(DataBinder.Eval(e.Item.DataItem, "ID")).LastIndexOf("RC1");
		
			if( ( e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem ) )
			{

				if(HelperFunction.ConvertToBoolean(DataBinder.Eval(e.Item.DataItem, "SHIPMENT_PROBLEMFLAG")) == true )
				{
					for( int i = 0; i < e.Item.Cells.Count; i++ )
					{
						e.Item.Cells[i].BackColor = Color.LightPink;
					}
				}

				if( ( DataBinder.Eval(e.Item.DataItem, "SHIPMENT_SHIP_PRCE_INV") ).ToString() == "" )
				{
					e.Item.Cells[12].Font.Italic = true;
				}
				if( ( DataBinder.Eval(e.Item.DataItem, "SHIPMENT_WHAREHOUSE_PRCE_INV") ).ToString() == "" )
				{
					e.Item.Cells[14].Font.Italic = true;
				}
				if( ( DataBinder.Eval(e.Item.DataItem, "SHIPMENT_MISC_INV") ).ToString() == "" )
				{
					e.Item.Cells[15].Font.Italic = true;
				}

				//e.Item.Cells[25].Text =HelperFunction.ConvertToString(DataBinder.Eval(e.Item.DataItem, "SHIPMENT_SHIP_PRICE_STATUS"));
				//e.Item.Cells[26].Text =HelperFunction.ConvertToString(DataBinder.Eval(e.Item.DataItem, "SHIPMENT_WHAREHOUSE_PRICE_STATUS"));

				double dbInterest = 0;
				int DaysPastDue = 0;
				double percentPaid = 0.0;
				DateTime dueDate;
				TimeSpan ts;

				if( e.Item.Cells[(int)ColHeaders.ShipmentDate].Text != "&nbsp;" && e.Item.Cells[(int)ColHeaders.ShipmentDate].Text != "" )
				{
					dueDate = Convert.ToDateTime(e.Item.Cells[(int)ColHeaders.ShipmentDate].Text).AddDays(Convert.ToInt32(e.Item.Cells[(int)ColHeaders.SHIPMENT_BUYER_TERMS].Text));

					percentPaid = Math.Round(( Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARPPAID")) / Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR")) ) * 100, 2);
					if( percentPaid >= limit ) //WY
						ts = Convert.ToDateTime(e.Item.Cells[(int)ColHeaders.PAIDDATE].Text) - dueDate;
					else
						ts = DateTime.Today - dueDate;

					DaysPastDue = ts.Days <= 0 ? 0 : ts.Days;

					e.Item.Cells[25].Text = "Y";

					if( DaysPastDue > 0 )
						dbInterest = Math.Round(( Convert.ToDouble(DaysPastDue) / 360.00 ) * ( interest / 100 ) *HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR")), 2);//WY
					else
						dbInterest = 0;
				}
				else
				{
					e.Item.Cells[25].Text = "N";
					DaysPastDue = 0;
					dbInterest = 0;
				}

				e.Item.Cells[(int)ColHeaders.DAYSPASTDUE].Text = DaysPastDue.ToString() == "0" ? "-" : DaysPastDue.ToString();

				if( iSKU == -1 ) // railcar in railcar to bulktruck 
				{
					e.Item.Cells[(int)ColHeaders.INTEREST].Text = string.Format("{0:c}", dbInterest);
					e.Item.Cells[(int)ColHeaders.FEE].Text = string.Format("{0:c}", HelperFunction.ConvertToDouble(DataBinder.Eval(e.Item.DataItem, "VARFEE")));
		
					dbMargin += Convert.ToSingle(HelperFunction.ConvertToDouble(DataBinder.Eval(e.Item.DataItem, "VARFEE")));
					dbTotalCommision +=HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARCOMMISION"));
					dbTotalFreight +=HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARFREIGHT"));
					dbTotalWharehouse +=HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARWHAREHOUSE"));
					dbTotalMisc +=HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARMISC"));
					iTotalWeight +=HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARWEIGHT"));
					dbTotalTVBuyr +=HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR"));
					dbTotalTVSelr +=HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVSELR"));

					dbTotalBuyrPaid +=HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"));
					dbTotalSelrPaid +=HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID"));
				}
				else
				{
					e.Item.Cells[(int)ColHeaders.DAYSPASTDUE].Text = "-";
				}


				//e.Item.ForeColor = Color.Firebrick;
				if( Math.Abs(Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID")) -HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVSELR"))) > 0.05 &&HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID")) > 0 )
				{
					e.Item.Cells[11].Text = "<a href=\"Edit_Payments.aspx?ID=" + DataBinder.Eval(e.Item.DataItem, "ID").ToString() + "\"><font color=red>" + String.Format("{0:c}",HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID"))) + "</a></font>";
				}
				if( Math.Abs(Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID")) -HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR"))) > 0.05 &&HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID")) > 0 )
				{
					e.Item.Cells[7].Text = " <a href=\"Edit_Payments.aspx?Receivable=true&ID=" + DataBinder.Eval(e.Item.DataItem, "ID").ToString() + "\"><font color=red>" + String.Format("{0:c}",HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))) + "</a></font>";
				}
				// Any time the Plastics Exchange is involved in a transaction, blank out the buyer/seller paid columns 
				if(HelperFunction.ConvertToString(DataBinder.Eval(e.Item.DataItem, "VARBUYR")) == "The Plastics..." )
				{
					e.Item.Cells[8].Text = " &nbsp;&nbsp;N/A";
					e.Item.Cells[6].Text = " &nbsp;&nbsp;N/A";
					e.Item.Cells[7].Text = " &nbsp;&nbsp;N/A";
					// This displays the amount in the 
					if(HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID")) != 0 )
					{
						e.Item.Cells[7].Text += "<a href=\"Edit_Payments.aspx?ID=" + DataBinder.Eval(e.Item.DataItem, "ID").ToString() + "\"> <font color=red> ($" +HelperFunction.ConvertToString(DataBinder.Eval(e.Item.DataItem, "VARPPAID")) + ")</font></a> ";
					}
				}
				if(HelperFunction.ConvertToString(DataBinder.Eval(e.Item.DataItem, "VARSELR")) == "The Plastics..." )
				{
					e.Item.Cells[9].Text = " &nbsp;&nbsp;N/A";
					e.Item.Cells[10].Text = " &nbsp;&nbsp;N/A";
					e.Item.Cells[11].Text = " &nbsp;&nbsp;N/A";
					if(HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID")) != 0 )
					{
						e.Item.Cells[11].Text += "<a href=\"Edit_Payments.aspx?ID=" + DataBinder.Eval(e.Item.DataItem, "ID").ToString() + "\"> <font color=red> ($" +HelperFunction.ConvertToString(DataBinder.Eval(e.Item.DataItem, "VARSPAID")) + ")</font></a> ";
					}
				}
				
				string strActionBoxHTML = "";
				// get Order id
				//Response.Write(e.Item.DataItem.);
				string strID =HelperFunction.ConvertToString(DataBinder.Eval(e.Item.DataItem, "ID"));
				strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]='<div class=\"menuitems\"><a href=\"/administrator/Transaction_Details.aspx?Market=Spot&Referer=/Common/Filled_Orders.aspx&ID=" + strID + "\">Edit # " + strID + "</a></div>'" + ( (char)13 ).ToString();

				// remove extra info
				//strID = strID.Substring(0,strID.LastIndexOf("-"));

				strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<hr>' //Optional Separator" + ( (char)13 ).ToString();
				strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID=" + strID + "&DocumentType=1\">Invoice</a></div>'" + ( (char)13 ).ToString();
				strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID=" + strID + "&DocumentType=2\">Purchase Order</a></div>'" + ( (char)13 ).ToString();
				strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<hr>' //Optional Separator" + ( (char)13 ).ToString();
				strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID=" + strID + "&DocumentType=3\">Certificate</a></div>'" + ( (char)13 ).ToString();
				strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID=" + strID + "&DocumentType=4\">Sale Confirmation</a></div>'" + ( (char)13 ).ToString();
				strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<div class=\"menuitems\"><a href=\"/hauler/Freight_Company.aspx?ID=" + strID + "\">Freight R.F.Q</a></div>'" + ( (char)13 ).ToString();

				strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<div class=\"menuitems\"><a href=\"/common/Convert_Order.aspx?ID=" + strID + "\">Convert to 4 BT</a></div>'" + ( (char)13 ).ToString();

				phActionButton.Controls.Add(new LiteralControl(strActionBoxHTML));

				// setting the mouseover color
				e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");
				if( e.Item.ItemType == ListItemType.Item )
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee'");
				}
				else
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3'");
				}
				// add scripts for 'action boxes'
				e.Item.Cells[0].Attributes.Add("onmouseover", "showmenu(event,linkset[" + iLinkSetCount.ToString() + "])");
				e.Item.Cells[0].Attributes.Add("onMouseout", "delayhidemenu()");

				iLinkSetCount++;
				
			}
			else if( e.Item.ItemType == ListItemType.Footer )
			{
				e.Item.Cells[1].Text = "<b>Totals:</b> ";
				e.Item.Cells[4].Text = "<b>" + String.Format("{0:#,###}", iTotalWeight) + "</b>";
				e.Item.Cells[6].Text = "<b>" + String.Format("{0:c}", dbTotalTVBuyr) + "</b>";

				e.Item.Cells[7].Text = "<b>" + String.Format("{0:c}", dbTotalBuyrPaid) + "</b>";

				e.Item.Cells[10].Text = "<b>" + String.Format("{0:c}", dbTotalTVSelr) + "</b>";

				e.Item.Cells[11].Text = "<b>" + String.Format("{0:c}", dbTotalSelrPaid) + "</b>";

				e.Item.Cells[(int)ColHeaders.FREIGHT].Text = "<b>" + String.Format("{0:c}", dbTotalFreight) + "</b>";
				e.Item.Cells[(int)ColHeaders.WHAREHOUSE].Text = "<b>" + String.Format("{0:c}", dbTotalWharehouse) + "</b>";
				e.Item.Cells[(int)ColHeaders.MISC].Text = "<b>" + String.Format("{0:c}", dbTotalMisc) + "</b>";
				e.Item.Cells[(int)ColHeaders.COMMISION].Text = "<b>" + String.Format("{0:c}", dbTotalCommision) + "</b>";
				e.Item.Cells[(int)ColHeaders.FEE].Text = "<b>" + String.Format("{0:c}", dbMargin) + "</b>";
			}
		
		}
				
		//protected void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		//{
		//	// if the show all months option is select, user needs to be redirected to a new page
    
		//	if (ddlMonth.SelectedItem.Value.Equals("All Months"))
		//	{
		//		Response.Redirect("/Creditor/Month_Summary.aspx");
		//	}
			
		//	//CreateBrokerList();	// Q: why is this here?? A: was needed to refresh record count which showed in parens after broker name in dropdown ie, John Doe(4) // john doe has 4 transactions 

		//	// reset dbase page to the first one whether the sorting changes
		//	dg.CurrentPageIndex = 0;
		//	//bymonth=bymonth+" WHERE ORDR_ID LIKE '%"+txtSearch.Text+"%'";
		//	Bind();
		//}

  //      protected void ddlYear_SelectedIndexChanged(object sender, System.EventArgs e)
  //      {
  //          // if the show all months option is select, user needs to be redirected to a new page

  //          if (ddlMonth.SelectedItem.Value.Equals("All Months"))
  //          {
  //              Response.Redirect("/Creditor/Month_Summary.aspx");
  //          }

  //          //CreateBrokerList();	// Q: why is this here?? A: was needed to refresh record count which showed in parens after broker name in dropdown ie, John Doe(4) // john doe has 4 transactions 

  //          // reset dbase page to the first one whether the sorting changes
  //          dg.CurrentPageIndex = 0;
  //          //bymonth=bymonth+" WHERE ORDR_ID LIKE '%"+txtSearch.Text+"%'";
  //          Bind();
  //      }

  //      protected void ddlCompanies_SelectedIndexChanged(object sender, System.EventArgs e)
		//{
  //          // date list needs to be rebuild according to the new company
  //          //CreateDateList();
  //          // reset dbase page to the first one whether the sorting changes
  //          ddlMonth.SelectedItem.Selected = false;
  //          ddlMonth.Items[0].Selected = true;
  //          //if (ddlCompanies.SelectedValue != "*")
  //          //{
  //          //    ddlMonth.Items[ddlMonth.Items.Count - 1].Selected = true;
  //          //}
  //          //else
  //          //{
  //          //    ddlMonth.Items[0].Selected = true;
  //          //}

  //          dg.CurrentPageIndex = 0;
		//	txtSearch.Text="";
		//	Bind();
		//}

		//protected void ddlBroker_SelectedIndexChanged(object sender, System.EventArgs e)
		//{
  //          //CreateDateList();
  //          ddlMonth.SelectedItem.Selected = false;
  //          ddlMonth.Items[0].Selected = true;
  //          //if (ddlBroker.SelectedValue != "*")
  //          //{
  //          //    ddlMonth.Items[ddlMonth.Items.Count - 1].Selected = true;
  //          //}
  //          //else
  //          //{
  //          //    ddlMonth.Items[0].Selected = true;
  //          //}
            
  //          dg.CurrentPageIndex = 0;
		//	txtSearch.Text="";
		//	Bind();
		//}

  //      protected void ddlGrade_SelectedIndexChanged(object sender, System.EventArgs e)
  //      {
  //          //CreateDateList();
  //          ddlMonth.SelectedItem.Selected = false;
  //          ddlMonth.Items[0].Selected = true;
  //          //if (ddlGrade.SelectedValue != "*")
  //          //{
  //          //    ddlMonth.Items[ddlMonth.Items.Count - 1].Selected = true;
  //          //}
  //          //else
  //          //{
  //          //    ddlMonth.Items[0].Selected = true;
  //          //}
            
  //          dg.CurrentPageIndex = 0;
  //          txtSearch.Text = "";
  //          Bind();
  //      }


        protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			dg.CurrentPageIndex =0;
			// set all drop down lists to the default. Search against everything.
			//ddlMonth.SelectedItem.Selected = false;
			//ddlCompanies.SelectedItem.Selected = false;
			//ddlBroker.SelectedItem.Selected = false;
			//ddlMonth.Items.FindByValue("*").Selected = true;
			Bind();			
		}

		private void CreateBrokerList()
		{
			string period = "";
			if (!ddlMonth.SelectedItem.Value.Equals("*"))
			{				
				int currentMonth = Convert.ToInt32(ddlMonth.SelectedValue.ToString());
				int currentYear = Convert.ToInt32(ddlMonth.SelectedValue.ToString());
				period += " AND Month(SHIPMENT_TRANSACTION_DATE)='" + currentMonth + "'";
				period += " AND Year(SHIPMENT_TRANSACTION_DATE)='" + currentYear + "'";
			}

			using(SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				conn.Open();

				// selecting admins
				string stringSQL = "SELECT P2.PERS_FRST_NAME, P2.PERS_LAST_NAME , P2.PERS_ID, T.QTD "+
					"FROM PERSON P2, (SELECT COUNT(1) QTD, S.SHIPMENT_BROKER_ID "+
					"		FROM ORDERS O, SHIPMENT S, COMPANY C, PERSON P "+
					"		WHERE S.SHIPMENT_BUYR is not null  "+
					"		AND S.SHIPMENT_ORDR_ID=O.ORDR_ID "+
					"		AND P.PERS_ID=S.SHIPMENT_BUYR "+
					"		AND C.COMP_ID=P.PERS_COMP "+
					"		AND (LEFT(C.COMP_NAME,10)<>'The Plasti' OR (LEFT(C.COMP_NAME,10)='The Plasti' AND SHIPMENT_SKU = 'RC1')) "+
					period + 
					"		GROUP BY S.SHIPMENT_BROKER_ID "+
					"		) T "+
					"WHERE P2.PERS_ID *= T.SHIPMENT_BROKER_ID "+
					"AND (P2.PERS_TYPE='T' OR P2.PERS_TYPE='B' OR P2.PERS_TYPE='A') AND PERS_ENBL='1' "+
					"ORDER BY T.QTD DESC, P2.PERS_FRST_NAME ";
				using (SqlDataReader dtrAllocate = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn,stringSQL))
				{
					//binding the allocate dd list
					ddlBroker.Items.Clear();
					ddlBroker.Items.Add(new ListItem ("Select Broker","*"));
					while (dtrAllocate.Read())
					{
						//ddlBroker.Items.Add ( new ListItem ((string) dtrAllocate[0] + " " + dtrAllocate[1] + "(" + (HelperFunction.getSafeStringFromDB(dtrAllocate[3])=="" ? "0":HelperFunction.getSafeStringFromDB(dtrAllocate[3])) + ")", dtrAllocate[2].ToString() ) );
						ddlBroker.Items.Add( new ListItem(dtrAllocate[0].ToString() + " " + dtrAllocate[1].ToString(),dtrAllocate[2].ToString()));
					}
					ddlBroker.Items.Add(new ListItem ("All Brokers","*"));
				}				
			}		
		}


		protected void CreateIndexList()
		{
		    ddlIndex.Items.Clear(); // starts the list out fresh
		    ddlIndex.Items.Add(new ListItem("Select Field", "*"));
		    ddlIndex.Items.Add(new ListItem("Order ID", "1"));
		    ddlIndex.Items.Add(new ListItem("PO number", "2"));
		    ddlIndex.Items.Add(new ListItem("Shipment FOB", "3"));
		    ddlIndex.Items.Add(new ListItem("Buyer Comp.", "4"));
		    ddlIndex.Items.Add(new ListItem("Seller Comp.", "5"));
		    ddlIndex.Items.Add(new ListItem("Freight Cost", "6"));
		    ddlIndex.Items.Add(new ListItem("Warehouse", "7"));
		    ddlIndex.Items.Add(new ListItem("Warehouse Cost", "8"));
		    ddlIndex.Items.Add(new ListItem("Broker Name", "9"));
		    ddlIndex.Items.Add(new ListItem("Railcar #", "10"));
		    ddlIndex.Items.Add(new ListItem("Transaction Date", "11"));
		    ddlIndex.Items.Add(new ListItem("Shipment Weight", "12"));
		    
	    }

        protected void CreateGradeList()
        {
            ddlGrade.Items.Clear(); // starts the list out fresh
            ddlGrade.Items.Add(new ListItem("Select Grade", "*"));
            ddlGrade.Items.Add(new ListItem("HMWPE - Film", "1"));
            ddlGrade.Items.Add(new ListItem("HDPE - Inj", "2"));
            ddlGrade.Items.Add(new ListItem("HDPE - Blow Mold", "3"));
            ddlGrade.Items.Add(new ListItem("LDPE - Film", "4"));
            ddlGrade.Items.Add(new ListItem("LDPE - Inj", "5"));
            ddlGrade.Items.Add(new ListItem("LLDPE - Film", "6"));
            ddlGrade.Items.Add(new ListItem("LLDPE - Inj", "7"));
            ddlGrade.Items.Add(new ListItem("PP Homopolymer - Inj", "8"));
            ddlGrade.Items.Add(new ListItem("PP Copolymer - Inj", "9"));
            ddlGrade.Items.Add(new ListItem("GPPS", "10"));
            ddlGrade.Items.Add(new ListItem("HIPS", "11"));
            ddlGrade.Items.Add(new ListItem("Monomer", "12"));
            ddlGrade.Items.Add(new ListItem("Scrap/Regrind/Repro", "13"));

        }



        //Excel
        protected void btnExportToExcel_Click(object sender, System.EventArgs e)
		{
	
		    Bind();

		    dg.Columns[0].Visible = false;
		    //dg.Columns[1].Visible = false;
	            
		    Response.Clear();
		    Response.Buffer = true;
		    Response.ContentType = "application/vnd.ms-excel";
		    Response.AddHeader("Content-Disposition","attachment; filename=transaction_summary.xls");

		    Response.BufferOutput = true;
		    Response.ContentEncoding = System.Text.Encoding.UTF8;
		    Response.Charset = "";
		    //Response.Charset = "UTF-8";
		    EnableViewState = false;

		    System.IO.StringWriter tw = new System.IO.StringWriter();
		    System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

		    FormatExportedGrid(dg);
		    this.ClearControls(dg);
		    dg.RenderControl(hw);

		    Response.Write(CleanHTML(tw.ToString()));
		    Response.End();

		}

		public void FormatExportedGrid(DataGrid dg1)
		{
		    dg1.BackColor = System.Drawing.Color.White;
		}

		private string CleanHTML(string OutOut)
		{

		    //will add some code to fix gridlines

		    return OutOut;
		}

		private void ClearControls(Control control)
		{
		    for (int i = control.Controls.Count - 1; i >= 0; i--)
		    {
			ClearControls(control.Controls[i]);
		    }
		    if (!(control is TableCell))
		    {
			if (control.GetType().GetProperty("SelectedItem") != null)
			{
			    LiteralControl literal = new LiteralControl();
			    control.Parent.Controls.Add(literal);
			    try
			    {
				literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
			    }
			    catch
			    {
			    }
			    control.Parent.Controls.Remove(control);
			}
			else
			    if (control.GetType().GetProperty("Text") != null)
			    {
				LiteralControl literal = new LiteralControl();
				control.Parent.Controls.Add(literal);
				literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);
				control.Parent.Controls.Remove(control);
			    }
		    }
		    return;
		}
	}
}
