<%@ Page Language="c#" CodeBehind="Creditor_Payment.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.Creditor_Payment" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>


<asp:Content runat="server" ContentPlaceHolderID="cphMain">

<asp:ValidationSummary runat="server" HeaderText="There are problems with the following fields. Please correct the errors below."
		id="ValidationSummary1" />
		<asp:Panel ID="pnlReceivables" runat="server" Visible="false"><div class="DivTitleBarMenu"><span class="Header Color1 Bold" align="center">
			<asp:label id="lblReceivables" runat="server" text="Receivables"></asp:label>
		</span></div></asp:Panel>
	<asp:DataGrid BackColor="#000000" BorderWidth="0" CellSpacing="1" id="dg" DataKeyField="VARID" runat="server" Width="780px" CssClass="DataGrid" ItemStyle-CssClass="LinkNormal LightGray"
		AlternatingItemStyle-CssClass="LinkNormal DarkGray" HeaderStyle-CssClass="Content Bold OrangeColor" CellPadding="2" horizontalalign="Center" OnItemDataBound="KeepRunningSum" ShowFooter="True"
		AutoGenerateColumns="false">
		<FooterStyle CssClass="Content Color4 FooterColor" />
		<Columns>
			<asp:HyperLinkColumn HeaderText="Order Number" DataNavigateUrlField="VARID" DataNavigateUrlFormatString="../administrator/Transaction_Details.aspx?Referer=/Creditor/Transaction_Summary.aspx&ID={0}"
				DataTextField="VARID" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
			<asp:BoundColumn DataField="VARCOMPANY" HeaderText="Buyer/Seller" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left" />
			<asp:BoundColumn DataField="VAROWES" HeaderText="Value Owed" DataFormatString="{0:c}" ItemStyle-Wrap="false"
				ItemStyle-HorizontalAlign="right" />
			<asp:BoundColumn DataField="VARTV" HeaderText="Transaction Value" DataFormatString="{0:c}" ItemStyle-Wrap="false"
				ItemStyle-HorizontalAlign="right" />
			<asp:BoundColumn DataField="VARNETDUE" HeaderText="Due Date" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
			<asp:BoundColumn DataField="VARDIFF" HeaderText="Days Late" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
			<asp:templatecolumn>
				<headertemplate>Amount Paid</headertemplate>
				<itemtemplate>
                             <asp:CompareValidator ControlToValidate="txtAmount" ErrorMessage="Price must be a number." Display="none"
						Operator="DataTypeCheck" Type="Double" Runat="server" />
								$
<asp:TextBox Width="50px" CssClass="InputForm" id="txtAmount" runat="server" />
                            </itemtemplate>
			</asp:templatecolumn>
			<asp:templatecolumn>
				<headertemplate>Comments</headertemplate>
				<itemtemplate>
					<asp:TextBox CssClass="InputForm" id="txtComments" maxlength="100" runat="server" Rows="2" TextMode="MultiLine" Columns="20" />
				</itemtemplate>
			</asp:templatecolumn>
		</Columns>
	</asp:DataGrid>
			<asp:Panel ID="pnlPayables" runat="server" Visible="false">
			    <div class="DivTitleBarMenu">
			    <span class="Header Color1 Bold">
			    <asp:label id="lblPayables" runat="server" text="Payables">
			    </asp:label>
		    </span></div></asp:Panel>
	<asp:DataGrid BackColor="#000000" CellSpacing="1" BorderWidth="0" id="dg2" DataKeyField="VARID" runat="server" Width="780px" CssClass="DataGrid" ItemStyle-CssClass="LinkNormal DarkGray"
		AlternatingItemStyle-CssClass="LinkNormal LightGray" HeaderStyle-CssClass="Content Bold Color2 OrangeColor"
		CellPadding="2" horizontalalign="Center" OnItemDataBound="KeepRunningSum" ShowFooter="True"
		AutoGenerateColumns="false">
		<FooterStyle CssClass="Content Color4 Bold FooterColor" />
		<Columns>
			<asp:HyperLinkColumn HeaderText="Order Number" DataNavigateUrlField="VARID" DataNavigateUrlFormatString="../administrator/Transaction_Details.aspx?Referer=/Creditor/Transaction_Summary.aspx&ID={0}"
				DataTextField="VARID" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
			<asp:BoundColumn DataField="VARCOMPANY" HeaderText="Buyer/Seller" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left" />
			<asp:BoundColumn DataField="VAROWES" HeaderText="Value Owed" DataFormatString="{0:c}" ItemStyle-Wrap="false"
				ItemStyle-HorizontalAlign="right" />
			<asp:BoundColumn DataField="VARTV" HeaderText="Transaction Value" DataFormatString="{0:c}" ItemStyle-Wrap="false"
				ItemStyle-HorizontalAlign="right" />
			<asp:BoundColumn DataField="VARNETDUE" HeaderText="Due Date" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
			<asp:BoundColumn DataField="VARDIFF" HeaderText="Days Late" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
			<asp:templatecolumn>
				<headertemplate>Amount Paid</headertemplate>
				<itemtemplate>
                             <asp:CompareValidator ControlToValidate="txtAmount" ErrorMessage="Price must be a number." Display="none"
						Operator="DataTypeCheck" Type="Double" Runat="server" ID="Comparevalidator1" />
								$
<asp:TextBox CssClass="InputForm" Width="50px" id="txtAmount" runat="server" />
                            </itemtemplate>
			</asp:templatecolumn>
			<asp:templatecolumn>
				<headertemplate>Comments</headertemplate>
				<itemtemplate>
					<asp:TextBox CssClass="InputForm" id="txtComments" maxlength="100" runat="server" Rows="2" TextMode="MultiLine" Columns="20"/>
				</itemtemplate>
			</asp:templatecolumn>
		</Columns>
	</asp:DataGrid>
		<br>
		<asp:Button runat="server" CssClass ="Content Color2" onclick="Click_Submit" Text="Submit Payment" id="Button1" />
			&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button runat="server" CssClass ="Content Color2" onclick="Navigate_Back" class="tpebutton" Text="Return to Credit Management"
				id="Button2" />
			<BR>
			<BR>
	

</asp:Content>