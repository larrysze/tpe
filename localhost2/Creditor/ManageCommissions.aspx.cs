using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;

namespace localhost.Creditor
{
    public partial class ManageCommissions : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.DataGrid dbCurrent;
        public static int currentMonth;
        public static int currentYear;        
        public static int brokerID;
        public static string brokerName = "";
        public static string monthDesc = "";
        bool monthChange = false;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if ((string)Session["Typ"] != "A")
            {
                Response.Redirect("../default.aspx");
            }

            if (!IsPostBack)
            {
                HelperFunction.fillDatesDesc(01, 2005, ddlMonth);
                ListItem lstItem = new ListItem("Show All Dates", "*", true);
                ddlMonth.Items.Add(lstItem);
                ddlMonth.SelectedItem.Selected = false;
                ddlMonth.Items[ddlMonth.Items.Count - 1].Selected = true;

                dgCurrent.Visible = true;
                btnPaid.Visible = true;
                dgOpen.Visible = true;

                string firstName = Request.QueryString["FirstName"];
                string lastName = Request.QueryString["LastName"];

                lblBrokerName.Text = firstName + " " + lastName;

                BindPaid();
                BindReadyToPayComm();
                BindNotReadyToPayComm();
            }
        }
        
        private void BindPaid()
        {
            if (monthChange) 
            {
                currentMonth = Convert.ToInt32(ddlMonth.SelectedValue.Substring(4, 2).ToString());
                currentYear = Convert.ToInt32(ddlMonth.SelectedValue.Substring(0, 4).ToString());
                monthChange = false;
            }
            else 
            {
                currentMonth = DateTime.Now.Month;
                currentYear = DateTime.Now.Year;
            }            
            brokerID = Convert.ToInt32(Request.QueryString["BrokerID"]);                               
     
            Hashtable htParams = new Hashtable();
            htParams.Add("@Month", currentMonth);
            htParams.Add("@Year", currentYear);
            htParams.Add("@BrokerID", brokerID);

            DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(), dgPaid, "spCommissionsPaid", htParams);
        }

        private void BindReadyToPayComm()
        {
            Hashtable htParams = new Hashtable();
            htParams.Add("@BrokerID", brokerID);

            DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(), this.dgCurrent, "spCommissionsReady", htParams);
        }

        private void BindNotReadyToPayComm()
        {
            Hashtable htParams = new Hashtable();
            htParams.Add("@BrokerID", brokerID);

            DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(), dgOpen, "spCommissionsNotReady", htParams);
        }



        long dbTotalWeight = 0;
        double dbTotalGrossProfitB4CCComm = 0;
        double dbTotalCommissionB4Comm = 0.0;
        double dbTotalCommission = 0.0;
        protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                dbTotalWeight += Convert.ToInt64(HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "WEIGHT")));
		dbTotalGrossProfitB4CCComm += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "GROSS_PROFIT_B4_CC_COMM"));
		dbTotalCommissionB4Comm += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "GROSS_PROFIT_B4_COMM"));
		dbTotalCommission += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "COMMISSION"));
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                e.Item.Cells[2].Text = "<b>Total:</b>";
                e.Item.Cells[3].Text = "<b>" + String.Format("{0:#,###}", dbTotalWeight) + "</b>";
                e.Item.Cells[9].Text = "<b>" + String.Format("{0:#,###.00}", dbTotalGrossProfitB4CCComm) + "</b>";
                e.Item.Cells[12].Text = "<b>" + String.Format("{0:#,###.00}", dbTotalCommissionB4Comm) + "</b>";
                e.Item.Cells[13].Text = "<b>" + String.Format("{0:#,###.00}", dbTotalCommission) + "</b>";
            }
        }

        long dbTotalWeightPaid = 0;
        double dbTotalGrossProfitB4CCCommPaid = 0;
        double dbTotalCommissionB4CommPaid = 0.0;
        double dbTotalCommissionPaid = 0.0;
        protected void KeepRunningSumPaid(object sender, DataGridItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
		    dbTotalWeightPaid += Convert.ToInt64(HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "WEIGHT")));
		    dbTotalGrossProfitB4CCCommPaid += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "GROSS_PROFIT_B4_CC_COMM"));
		    dbTotalCommissionB4CommPaid += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "GROSS_PROFIT_B4_COMM"));
		    dbTotalCommissionPaid += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "COMMISSION"));
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                e.Item.Cells[1].Text = "<b>Total:</b>";
                e.Item.Cells[2].Text = "<b>" + String.Format("{0:#,###}", dbTotalWeightPaid) + "</b>";
                e.Item.Cells[8].Text = "<b>" + String.Format("{0:#,###.00}", dbTotalGrossProfitB4CCCommPaid) + "</b>";
                e.Item.Cells[11].Text = "<b>" + String.Format("{0:#,###.00}", dbTotalCommissionB4CommPaid) + "</b>";
                e.Item.Cells[12].Text = "<b>" + String.Format("{0:#,###.00}", dbTotalCommissionPaid) + "</b>";
            }
        }

        long dbTotalWeightOpen = 0;
        double dbTotalGrossProfitB4CCCommOpen = 0;        
        protected void KeepRunningSumOpen(object sender, DataGridItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
		    dbTotalWeightOpen += Convert.ToInt64(HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "WEIGHT")));
		    dbTotalGrossProfitB4CCCommOpen += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "GROSS_PROFIT_B4_CC_COMM"));                
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                e.Item.Cells[1].Text = "<b>Total:</b>";
                e.Item.Cells[2].Text = "<b>" + String.Format("{0:#,###}", dbTotalWeightOpen) + "</b>";
                e.Item.Cells[8].Text = "<b>" + String.Format("{0:#,###.00}", dbTotalGrossProfitB4CCCommOpen) + "</b>";                
            }
        }

        protected void btnPaid_OnClick(object sender, EventArgs e)
        {
            for ( int i = 0; i < dgCurrent.Items.Count; i++)
            {
                // if the control box is checked
                if (((System.Web.UI.HtmlControls.HtmlInputCheckBox)dgCurrent.Items[i].Cells[0].Controls[0]).Checked == true)
                {
                    string strID = dgCurrent.Items[i].Cells[14].Text;   
                    
                    //set the checkbox to false

                    string SQL = "";
                    using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
                    {
                        conn.Open();
                        SQL = "UPDATE CASH_RECEIVED SET COMM_PAID = 1, COMM_PAID_DATE = GETDATE() WHERE TRANSACTION_ID = '" + strID + "'";
                        DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), SQL);
                    }


                    ((System.Web.UI.HtmlControls.HtmlInputCheckBox)dgCurrent.Items[i].Cells[0].Controls[0]).Checked = false;
                }
            }

            BindPaid();
            BindReadyToPayComm();
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            for (int i = 0; i < dgCurrent.Items.Count; i++)
            {
                // if the control box is checked
                if (((System.Web.UI.HtmlControls.HtmlInputCheckBox)dgCurrent.Items[i].Cells[0].Controls[0]).Checked == true)
                {
                    string strID = dgCurrent.Items[i].Cells[14].Text;

                    //set the checkbox to false

                    string SQL = "";
                    using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
                    {
                        conn.Open();
                        SQL = "UPDATE CASH_RECEIVED SET Comm_Eligible = 0 WHERE TRANSACTION_ID = '" + strID + "'";
                        DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), SQL);
                    }


                    ((System.Web.UI.HtmlControls.HtmlInputCheckBox)dgCurrent.Items[i].Cells[0].Controls[0]).Checked = false;
                }
            }
            
            BindReadyToPayComm();
        }

        protected void btnSummary_OnClick(object sender, EventArgs e)
        {
            string transfer = "currentMonth=" + currentMonth;
            transfer += "&currentYear=" + currentYear;
            transfer += "&BrokerID=" + brokerID;
            transfer += "&FirstName=" + Request.QueryString["FirstName"];
            transfer += "&LastName=" + Request.QueryString["LastName"];
            transfer += "&MonthDesc=" + monthDesc;
            Response.Redirect("../Creditor/CommissionStatement.aspx?" + transfer);
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
        {
		monthChange = true;
		string dateString = ddlMonth.SelectedValue;

		if( dateString != "*" )
		{
			if( !( ( Convert.ToInt32(dateString.Substring(4, 2)) == DateTime.Now.Month ) && ( Convert.ToInt32(dateString.Substring(0, 4)) == DateTime.Now.Year ) ) )
			{
				dgCurrent.Visible = false;
				btnPaid.Visible = false;
				dgOpen.Visible = false;
			}
			else
			{
				dgCurrent.Visible = true;
				btnPaid.Visible = true;
				dgOpen.Visible = true;
			}
			BindPaid();
		}
        }
    }
}
