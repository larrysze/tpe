using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using MetaBuilders.WebControls;
using TPE.Utility;

namespace localhost.Creditor
{
	public partial class CommissionStatement : System.Web.UI.Page
	{
		private int currentMonth;
		private int currentYear;
		protected System.Web.UI.WebControls.DataGrid dbCurrent;
		private int brokerID;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if ((string)Session["Typ"] != "A")
			{
				Response.Redirect("../default.aspx");
			}
			
			if (!IsPostBack)
			{
				currentMonth = Convert.ToInt32(Request.QueryString["currentMonth"]);
				currentYear = Convert.ToInt32(Request.QueryString["currentYear"]);
				brokerID = Convert.ToInt32(Request.QueryString["BrokerID"]);
				lblBrokerName.Text = Request.QueryString["FirstName"] + " " + Request.QueryString["LastName"];
				//lblMonth.Text = Request.QueryString["MonthDesc"];
				
				//BindLastMonth();
				BindCurrent();
				//BindOpenCommissions();
			}
		}
		
		private void BindCurrent()
		{    	
			
			Hashtable htParams = new Hashtable();
			//htParams.Add("@Month",currentMonth.ToString());
			htParams.Add("@Year",currentYear.ToString());
			htParams.Add("@BrokerID",brokerID);

			DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(), this.dgSummary, "spCommissionsPaid", htParams);
		}

        //private void BindLastMonth()
        //{   
        //    //int lastMonth = currentMonth-1;
        //    int lastMonthYear =  currentYear;
			
        //    //if (lastMonth == 0)
        //    //{
        //    //    lastMonth++;
        //    //    lastMonthYear--;
        //    //}
			
        //    Hashtable htParams = new Hashtable();
        //    //htParams.Add("@Month",lastMonth.ToString());
        //    htParams.Add("@Year",lastMonthYear.ToString());
        //    htParams.Add("@BrokerID",brokerID);

        //    DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(),dgLastMonth,"spCommissionsPaid",htParams);			
        //}

        //private void BindOpenCommissions()
        //{   

        //    Hashtable htParams = new Hashtable();
        //    htParams.Add("@BrokerID",brokerID);
			
        //    DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(),dgOpen,"spCommissionsOpen",htParams);
        //}

		
		long dbTotalWeight = 0;
		double dbTotalGrossProfitB4CCComm = 0;
		double dbTotalCommissionB4Comm = 0.0;
		double dbTotalCommission = 0.0;
		protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
			if (  (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
			{
				dbTotalWeight += Convert.ToInt64(HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "WEIGHT")));
				dbTotalGrossProfitB4CCComm += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "GROSS_PROFIT_B4_CC_COMM"));
				dbTotalCommissionB4Comm += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "GROSS_PROFIT_B4_COMM"));
				dbTotalCommission += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "COMMISSION"));
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[2].Text = "<b>Total:</b>" ;
				e.Item.Cells[4].Text = "<b>"+String.Format("{0:#,###}", dbTotalWeight)+"</b>";
				e.Item.Cells[10].Text = "<b>" + String.Format("{0:#,###.00}", dbTotalGrossProfitB4CCComm) + "</b>";
				e.Item.Cells[13].Text = "<b>" + String.Format("{0:#,###.00}", dbTotalCommissionB4Comm) + "</b>";
				e.Item.Cells[14].Text = "<b>" + String.Format("{0:#,###.00}", dbTotalCommission) + "</b>";
			}
		}

		long dbTotalWeightLM = 0;
		double dbTotalGrossProfitB4CCCommLM = 0;
		double dbTotalCommissionB4CommLM = 0.0;
		double dbTotalCommissionLM = 0.0;
		protected void KeepRunningSumLM(object sender, DataGridItemEventArgs e)
		{
			if (  (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
			{
				dbTotalWeightLM += Convert.ToInt64(HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "WEIGHT")));
				dbTotalGrossProfitB4CCCommLM += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "GROSS_PROFIT_B4_CC_COMM"));
				dbTotalCommissionB4CommLM += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "GROSS_PROFIT_B4_COMM"));
				dbTotalCommissionLM += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "COMMISSION"));
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[1].Text = "<b>Total:</b>" ;
				e.Item.Cells[2].Text = "<b>"+String.Format("{0:#,###}", dbTotalWeightLM)+"</b>";
				e.Item.Cells[8].Text = "<b>" + String.Format("{0:#,###.00}", dbTotalGrossProfitB4CCCommLM) + "</b>";
				e.Item.Cells[11].Text = "<b>" + String.Format("{0:#,###.00}", dbTotalCommissionB4CommLM) + "</b>";
				e.Item.Cells[12].Text = "<b>" + String.Format("{0:#,###.00}", dbTotalCommissionLM) + "</b>";
			}
		}

		long dbTotalWeightOpen = 0;
		double dbTotalGrossProfitB4CCCommOpen = 0;
	        double dbTotalGrossProfitB4CommOpen = 0;
		double dbTotalCommissionOpen = 0.0;
		protected void KeepRunningSumOpen(object sender, DataGridItemEventArgs e)
		{
			if (  (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
			{
				dbTotalWeightOpen += Convert.ToInt64(HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "WEIGHT")));
				dbTotalGrossProfitB4CCCommOpen += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "GROSS_PROFIT_B4_CC_COMM"));
				dbTotalGrossProfitB4CommOpen += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "GROSS_PROFIT_B4_COMM"));
				dbTotalCommissionOpen += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "COMMISSION"));
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[1].Text = "<b>Total:</b>" ;
				e.Item.Cells[2].Text = "<b>"+String.Format("{0:#,###}", dbTotalWeightOpen)+"</b>";
				e.Item.Cells[8].Text = "<b>" + String.Format("{0:#,###.00}", dbTotalGrossProfitB4CCCommOpen) + "</b>";
				e.Item.Cells[11].Text = "<b>" + String.Format("{0:#,###.00}", dbTotalGrossProfitB4CommOpen) + "</b>";
				e.Item.Cells[12].Text = "<b>"+String.Format("{0:#,###.00}", dbTotalCommissionOpen)+"</b>";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

	}
}