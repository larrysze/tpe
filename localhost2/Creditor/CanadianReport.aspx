<%@ Page Language="c#" Codebehind="CanadianReport.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.CanadianReport" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphMain">
    <div class="DivTitleBarMenu"><asp:Label CssClass="Header Color1 Bold" ID="Label1" runat="server" Text="Canadian Transactions Report"></asp:Label></div>
    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
            <br />&nbsp;<asp:Label CssClass="Content Bold Color2" ID="Label2" runat="server" Text="Year:"></asp:Label>&nbsp;
                <asp:DropDownList runat="server" ID="ddlYear" AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnExcel" runat="server" Text="Export to Excel" OnClick="btnExcel_Click" />
                <br /><br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:datagrid id="dg" runat="server" Width="100%" CssClass="DataGrid" AutoGenerateColumns="False" onSortCommand="SortDG" AllowSorting="True" CellPadding="2" HorizontalAlign="Left" CellSpacing="1" BackColor="Black" BorderWidth="0px">
                    <SelectedItemStyle Wrap="False"></SelectedItemStyle>
                    <EditItemStyle Wrap="False" CssClass="LinkNormal"></EditItemStyle>
                    <AlternatingItemStyle Wrap="False" CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
                    <ItemStyle Wrap="False" CssClass="LinkNormal LightGray"></ItemStyle>
                    <HeaderStyle HorizontalAlign="Center" CssClass="LinkNormal Color1 Bold OrangeColor"></HeaderStyle>
                    <Columns>
                        <asp:HyperLinkColumn ItemStyle-Wrap="false" DataNavigateUrlField="Order #" DataTextField="Order #" HeaderText="Order #"
                            SortExpression="'Order #'" DataNavigateUrlFormatString="../administrator/Transaction_Details.aspx?Referer=/Creditor/Transaction_Summary.aspx&amp;ID={0}"></asp:HyperLinkColumn>
                        <asp:BoundColumn DataField="Order Date" SortExpression="'Order Date'" HeaderText="Order Date">
                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Shipment Date" SortExpression="'Shipment Date'" HeaderText="Shipment Date">
                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Total Owed" SortExpression="'Total Owed'" HeaderText="Total Owed" DataFormatString="{0:c}">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Total Received" SortExpression="'Total Received'" HeaderText="Total Received" DataFormatString="{0:c}">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Balance" SortExpression="Balance" HeaderText="Balance" DataFormatString="{0:c}">
                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Buyer" SortExpression="Buyer" HeaderText="Buyer">
                            <ItemStyle HorizontalAlign="Center" Width="300px"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="To" SortExpression="'To'" HeaderText="To">
                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Seller" SortExpression="Seller" HeaderText="Seller">
                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="From" SortExpression="'From'" HeaderText="From">
                            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                    </Columns>
                </asp:datagrid>
            </td>
        </tr>
    </table>
</asp:Content>