using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Web.Mail;
using TPE.Utility;


namespace localhost.Creditor
{
	/// <summary>
	/// Summary description for Statement.
	/// </summary>
	public partial class Statement : System.Web.UI.Page
	{

		public string EmailBody;  
		protected String mailBody;
		protected Single TotalBalance;	
		protected System.TimeSpan days;
		protected string[] userInfo=new string[20];

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C"))
			{
				Response.Redirect("../default.aspx");
			}
			if (!IsPostBack)
			{
				// save important variables into viewstate
				//TotalBalance			
				ViewState["Id"] = Request.QueryString["Id"].ToString();				
			
				
				
			}   
			TotalBalance=0;
			Bind();
			lblDate.Text="STATEMENT DATE: "+ DateTime.Today.ToShortDateString();
		}


		protected void bSend_Click(object sender, EventArgs e)
		{
			int style=0;	

			StringBuilder sbHTML = new StringBuilder();	

			sbHTML.Append("<table width=\"779\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"1\" bordercolor=\"#000000\" bgcolor=\"#000000\"><tr><td width=\"779\" bgcolor=\"#333333\"><img src=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/images/email/EmailHeadImage.png\" width=\"779\" height=\"94\" /></td></tr><tr><td width=\"779\" height=\"20\" background=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/images/email/EmailOrangeBar.png\" bgcolor=\"#FFD900\"></td></tr></table>");

			sbHTML.Append("<table border=0 bgcolor=\"#DBDCD7\" align=center cellspacing=2 cellpadding=2 width=\"779\">");
			// account info table
			sbHTML.Append("<tr><td align=left colspan='1' width=100%>");
			sbHTML.Append("<TABLE id='tMail' border=0 cellspacing=0 cellpadding=0 align=left bgcolor=\"#DBDCD7\" width=100%>");		   
			
			sbHTML.Append("<tr><td align='left' colspan='2'><P align='left'><Font size='4'><b>");
			sbHTML.Append(lblNameCompany.Text+"</b></Font></P></td></tr><tr><td align='left' colspan='2'>");
			sbHTML.Append(lblAddress.Text+"</td></tr><tr><td align='left'colspan='2'>");
			sbHTML.Append(lblPlace.Text+"</td></tr><tr><td align='left'colspan='2'>United States</td></tr></TABLE>");
			sbHTML.Append("</td><td align=right colspan='1' width=80% valign=top>");
			sbHTML.Append(lblDate.Text+"</td></tr>");
			// end account info
			sbHTML.Append("<tr><td><br>");
			EmailBody = txtBody.Text;
			sbHTML.Append(EmailBodyHtml(EmailBody));
			sbHTML.Append("</td></tr>");
			sbHTML.Append("<tr><td align='center' colspan='2'><P align='center'><Font size='4'></br><b> Statement of Account from The Plastics Exchange </b></br></Font></P></td></tr>");
			
			// begin main table
			sbHTML.Append("<tr><td align='center' colSpan='2'>");
			sbHTML.Append("<Table  bgcolor='#eeeeee' align='center' width='95%'>");
			sbHTML.Append("<tr bgcolor=#BEB8A2><td STYLE='FONT: 8pt ARIAL'>Order #</td><td STYLE='FONT: 8pt ARIAL'>PO#</td><td STYLE='FONT: 8pt ARIAL'>Product</td><td STYLE='FONT: 8pt ARIAL'>Ship Date</td><td STYLE='FONT: 8pt ARIAL'>Due Date</td><td STYLE='FONT: 8pt ARIAL'>Terms</td><td STYLE='FONT: 8pt ARIAL'>Days Late</td><td STYLE='FONT: 8pt ARIAL'>Open Balance</td></tr>");

			for (int i=0;i<dg.Items.Count;i++)
			{
				if (dg.Items[i].Visible==true)
				{
					
					if (style==0)
					{
						style=1;
					}
					else
					{
						style=0;
					}
					sbHTML.Append("<tr>");

					for (int j=0;j<8;j++)
					{
						
						if (style==0)	
						{
							sbHTML.Append("<td STYLE='FONT: 8pt ARIAL' bgcolor='#D9D7D3'>");	
							sbHTML.Append(dg.Items[i].Cells[j].Text);
							sbHTML.Append("</td>");	

						}
						else 
						{
							sbHTML.Append("<td STYLE='FONT: 8pt ARIAL' >");	
							sbHTML.Append(dg.Items[i].Cells[j].Text);
							sbHTML.Append("</td>");	
						}
						
					}
					sbHTML.Append("</tr>");
					
					
				}	
			}
			
			sbHTML.Append("<tr><td colspan='7'align='right'><b>Totals:</b></td>");
			sbHTML.Append( "<td><b>"+String.Format("{0:c}", TotalBalance)+"</b></td></tr>");
			sbHTML.Append("</Table>");
			sbHTML.Append("</td></tr>");
			sbHTML.Append("<tr><td><BR><BR></td></tr>");
			// end main table 

			
			
			sbHTML.Append("<tr><td colSpan='2' vAlign='top'>");
			sbHTML.Append("<TABLE width='500' align='center' border='0' ><TR><TD valign=top>");
			sbHTML.Append("<TABLE id='tCheck' align='left' border='0' >");
			sbHTML.Append("<tr>");
			sbHTML.Append("<td valign=top><font size='2'><b><i>CHECK REMITTANCE INSTRUCTIONS: </i></b></font></td></tr>");
			sbHTML.Append("<tr><td><font size='2'>The Plastics Exchange</font></td></tr>");
            sbHTML.Append("<tr><td><font size='2'>(16510 N. 92nd Street. #1010</font></td></tr>");
			sbHTML.Append("<tr><td><font size='2'>Scottsdale, AZ 85260</td></tr>	");
			sbHTML.Append("</TABLE>");
			sbHTML.Append("</td><td vAlign='top' colSpan='1'>");
			sbHTML.Append("<table id='tTransfer' align='right'>");
			sbHTML.Append("<tr vAlign='top'><td align='right'><font size='2'><b><i>WIRE TRANSFER INSTRUCTIONS: </b></I></font></td></tr>");
			
			sbHTML.Append("<tr><td><font size='2'>Citibank</font></td></tr>");
            sbHTML.Append("<tr><td><font size='2'>Chicago, IL 60654</font></td></tr>");
			sbHTML.Append("<tr><td><font size='2'>A.B.A. #271070 801</font><BR></td></tr>");
			sbHTML.Append("<tr><td><font size='2'>For Futher Credit:</font></td></tr>");
			
			sbHTML.Append("<tr><td><font size='2'>The Plastics Exchange</font></td></tr>");
            sbHTML.Append("<tr><td><font size='2'>Account# 801086470</font></td></tr>");
			sbHTML.Append("</table>");
			sbHTML.Append("</td></tr></table>");
			sbHTML.Append("</TD></TR>");
			
			// close container table
			sbHTML.Append("</TABLE>");
			// END CONTENT

            sbHTML.Append("<table width=\"779\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"1\" bordercolor=\"#000000\" bgcolor=\"#000000\"><tr><td height=\"18\" bgcolor=\"#333333\"><div align=\"center\"><p align=\"center\" style=\"font-size: 10px; color: #FFFFFF; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold\">16510 N. 92nd Street. #1010  Scottsdale, AZ 85260  tel 312.202.0002  fax   312.202.0174</p></div></td></tr></table>");
			// END FOOTER



			//Saving Email into System
			string broker_id = Session["ID"].ToString();
			Hashtable ht = new Hashtable(1);
			ht.Add("@BrokerID", broker_id);
			int idReport = (int)DBLibrary.ExecuteScalarStoredProcedure(Application["DBconn"].ToString(), "spAddOffersReport", ht);
			int EmailSystemID = HelperFunction.SaveEmailSystem(sbHTML.ToString(), "L",idReport,Application["DBConn"].ToString());

			string sqlQuery = "";
			string strEmailTitle = "Transaction Statement";
			string personID = "";
			for (int i = 0; i < userInfo.Length;i++ )
			{
				if (userInfo[i].ToString().Equals(ddlTo.SelectedValue.ToString()))
				{
				    personID = userInfo[i + 1].ToString();
				    break;
				} 
			}
			string EmailID = "NULL";
			if (EmailSystemID != 0) EmailID = EmailSystemID.ToString();
			//Saving the detail into comment table
			sqlQuery = "INSERT INTO COMMENT (CMNT_PERS,CMNT_TEXT,CMNT_DATE, CMNT_EMAIL_SYSTEM_ID) VALUES ('" + personID + "','Email Subject: " + strEmailTitle + "', getDate(), " + EmailID + ")";
			DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlQuery);




			System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
			mail.From=ddlFrom.SelectedValue.ToString();
			mail.To = ddlTo.SelectedValue.ToString();
			//mail.To="tim@theplasticsexchange.com";
			mail.Cc=Application["strEmailOwner"].ToString();
			mail.Body=sbHTML.ToString();
			mail.Subject="Statement of Account from The Plastics Exchange";
			mail.BodyFormat = MailFormat.Html;
			//SmtpMail.SmtpServer="caesar";
			
			TPE.Utility.EmailLibrary.Send(Context,mail);
			Response.Write(sbHTML.ToString());            	

			Response.Redirect("AccountSummary.aspx?Id="+Request.QueryString["Id"].ToString());
            
		}

		private string EmailBodyHtml(string EmailBody)
		{
			EmailBody = EmailBody.Replace("\r\n", "<br>");
			return EmailBody;
		}

		private void Bind()
		{
			SqlDataAdapter dadContent;
			SqlDataAdapter dadContent2;
			DataSet dstContent2;
			DataSet dstContent;
			SqlConnection conn;
			SqlConnection conn2;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn2 = new SqlConnection(Application["DBConn"].ToString());
			try
			{
				conn.Open();
				conn2.Open();

           
				StringBuilder sbSQL = new StringBuilder();
				StringBuilder sbSQL2 = new StringBuilder();

				sbSQL.Append(" Select Company.Comp_Name,PERS_LAST_NAME,PERS_FRST_NAME,PERS_ID,PERS_MAIL,PLAC_ZIP,LOCL_STAT,LOCL_CITY,PLAC_ADDR_ONE from Person,Place,Company,Locality,Shipment WHERE ");
				sbSQL.Append(" COMP_ID='"+ ViewState["Id"].ToString() +"' and ");
				sbSQL.Append(" Person.PERS_COMP=Company.COMP_ID  and ");
				sbSQL.Append(" Place.PLAC_COMP=Company.COMP_ID and PLAC_TYPE='h' and ");
				sbSQL.Append(" PLAC_LOCL= LOCL_ID and SHIPMENT_BUYER_CLOSED_DATE is null ");
				sbSQL.Append(" group by Company.Comp_Name,PERS_ID,PERS_MAIL,PLAC_ZIP,LOCL_STAT,LOCL_CITY,LOCL_CTRY,PLAC_ADDR_ONE,PERS_LAST_NAME, PERS_FRST_NAME ");
				
				dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
				dstContent = new DataSet();
				dadContent.Fill(dstContent);
    
				Object [] array;
	
				if (!dstContent.Tables[0].Rows.Count.ToString().Equals("0"))
				{
					array=dstContent.Tables[0].Rows[0].ItemArray;			
					lblNameCompany.Text= array[0].ToString();

					int rowCount = dstContent.Tables[0].Rows.Count;
					int a = 0;

					while (a <= rowCount - 1)
					{
						Object[] Userarray;
						Userarray = dstContent.Tables[0].Rows[a].ItemArray;

						ddlTo.Items.Add(new ListItem(Userarray[4].ToString(), Userarray[4].ToString()));

						//Store user email and id 
						userInfo[2 * a] = Userarray[4].ToString();
						userInfo[2 * a + 1] = Userarray[3].ToString();

						a++;
					}

					ddlTo.Items.Add(new ListItem("Helio","helio@ThePlasticsExchange.com"));
					ddlTo.Items.Add(new ListItem("Mike","Mike@ThePlasticsExchange.com"));//"Mike@ThePlasticExchange.com"));
					ddlTo.Items.Add(new ListItem("Brett","brett@ThePlasticsExchange.com"));
						
			
					lblAddress.Text=array[8].ToString();
					lblPlace.Text=array[7].ToString()+", "+array[6].ToString()+" - "+array[5].ToString();
				
					sbSQL2.Append("SELECT ");
					sbSQL2.Append("CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR) AS ID, ");
					sbSQL2.Append("O.ORDR_PROD_SPOT , ");
					sbSQL2.Append("O.ORDR_MELT, ");
					sbSQL2.Append("O.ORDR_DENS, ");
					sbSQL2.Append("O.ORDR_ADDS, ");
					sbSQL2.Append("VARDATETAKEN=CONVERT(VARCHAR,S.SHIPMENT_DATE_TAKE,101), ");
					sbSQL2.Append("S.SHIPMENT_BUYER_TERMS, ");
					sbSQL2.Append("S.SHIPMENT_PO_NUM, ");
					sbSQL2.Append("DAYSSOFAR=DATEDIFF(DAY, DATEADD(d,CAST(SHIPMENT_BUYER_TERMS AS INTEGER), CAST(SHIPMENT_DATE_TAKE AS DATETIME)), getdate()) ,");
					sbSQL2.Append(" (SHIPMENT_BUYR_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (100 + ISNULL(SHIPMENT_TAX, 0)) / 100)-ISNULL(P.PAY_AMNT,0) AS OPEN_BALANCE ");
					sbSQL2.Append("FROM SHIPMENT S, ORDERS O, (select P.PAY_SHIPMENT, P.PAY_PERS, P.PAY_ORDR, SUM(P.PAY_AMNT) PAY_AMNT from PAYMENT P group by P.PAY_SHIPMENT, P.PAY_PERS, P.PAY_ORDR) P ");
					sbSQL2.Append("WHERE S.SHIPMENT_ORDR_ID = O.ORDR_ID and S.SHIPMENT_BUYER_CLOSED_DATE is  null ");
					sbSQL2.Append("AND P.PAY_ORDR =* S.SHIPMENT_ORDR_ID AND P.PAY_SHIPMENT =* S.SHIPMENT_ID AND P.PAY_PERS =* S.SHIPMENT_BUYR ");
					sbSQL2.Append("AND S.SHIPMENT_DATE_TAKE is not null AND (SELECT PERS_COMP from PERSON WHERE PERS_ID=SHIPMENT_BUYR) = '"+ViewState["Id"].ToString()+"' ORDER BY ID" );
				
					dadContent2 = new SqlDataAdapter(sbSQL2.ToString(),conn2);
					dstContent2 = new DataSet();
					dadContent2.Fill(dstContent2);
					dg.DataSource=dstContent2;
					dg.DataBind();


		
				}
			}
			finally
			{
				conn2.Close();
				conn.Close();
			}

			if (dstContent.Tables[0].Rows.Count.ToString().Equals("0"))
				Response.Redirect("No_Statement.aspx?Id="+ViewState["Id"].ToString());
		}


		protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
	
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				
				
				
				if (!Convert.IsDBNull(DataBinder.Eval(e.Item.DataItem,"VARDATETAKEN")))
				{								
					System.DateTime dueDate=Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "VARDATETAKEN")).AddDays(Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "SHIPMENT_BUYER_TERMS")));
					e.Item.Cells[4].Text=dueDate.ToShortDateString();
					System.TimeSpan daysLate=System.DateTime.Today.Subtract(dueDate);
					e.Item.Cells[6].Text= daysLate.Days.ToString();
					if (!Convert.IsDBNull(DataBinder.Eval(e.Item.DataItem,"OPEN_BALANCE")))
						TotalBalance =TotalBalance+ Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "OPEN_BALANCE"));

				}
				if ((DataBinder.Eval(e.Item.DataItem,"ORDR_MELT")).ToString().Trim().Length >1 || (DataBinder.Eval(e.Item.DataItem,"ORDR_DENS")).ToString().Trim().Length >1 || (DataBinder.Eval(e.Item.DataItem,"ORDR_ADDS")).ToString().Trim().Length >1)
				{
					e.Item.Cells[2].Text +="<BR>";
					if ((DataBinder.Eval(e.Item.DataItem,"ORDR_MELT")).ToString().Trim().Length >1)
					{
						e.Item.Cells[2].Text +=(DataBinder.Eval(e.Item.DataItem,"ORDR_MELT")).ToString()+" melt";
					}
					if ((DataBinder.Eval(e.Item.DataItem,"ORDR_DENS")).ToString().Trim().Length >1)
					{
						e.Item.Cells[2].Text +=" - " + (DataBinder.Eval(e.Item.DataItem,"ORDR_DENS")).ToString()+" density";
					}
					if ((DataBinder.Eval(e.Item.DataItem,"ORDR_ADDS")).ToString().Trim().Length >1)
					{
						e.Item.Cells[2].Text +=" - " + (DataBinder.Eval(e.Item.DataItem,"ORDR_ADDS")).ToString();
					}

				}
				
			}

			else if (e.Item.ItemType == ListItemType.Footer)
			{		
				e.Item.Cells[6].Text = "<b>Totals:</b>" ;
				e.Item.Cells[7].Text = "<b>"+String.Format("{0:c}", TotalBalance)+"</b>";					
						

			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
