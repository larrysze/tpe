using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Text;

namespace localhost.Creditor
{
	/// <summary>
	/// Summary description for AccountSummary.
	/// </summary>
	public partial class AccountSummary : System.Web.UI.Page
	{
	
		
		/************************************************************************
			  *   1. File Name       :AccountSummary.aspx							 *
			  *   2. Description     :												 *
			  *   3. Modification Log:                                                *
			  *     Ver No.       Date          Author             Modification       *
			  *   -----------------------------------------------------------------   *
			  *                                                                       *
			  *                                                                       *
			  ************************************************************************/
		String  CompType="";
		protected System.Web.UI.WebControls.Label Label1;
		public string fct = ""; 

		double ValueSum = 0.0;
		double WeightSum = 0.0; 		
		double ValueSumP = 0.0;
		double WeightSumP = 0.0;
        double ValueSum2 = 0.0;
        double WeightSum2 = 0.0;
        double ValueSumP2 = 0.0;
        double WeightSumP2 = 0.0;
		long iDG1Weight = 0;
		long iDG2Weight =0;
        long iDG3Weight = 0;
        long iDG4Weight = 0;
        double TotalPaidSum = 0.0;
		double TotalPaidSumP = 0.0;
        double TotalPaidSum2 = 0.0;
        double TotalPaidSumP2 = 0.0;

		protected void Page_Load(object sender, EventArgs e)
		{
			fct = Request.QueryString["fct"];
            Session["fct"] = fct;
			if (!IsPostBack && (Request.QueryString["fct"] != null ) )
			{
                ddlTransaction.SelectedItem.Selected = false;
				ddlTransaction.Items.FindByValue(fct).Selected = true;
			}

			//Administrator and Creditor have access
    
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C"))
			{
				Response.Redirect("/default.aspx");
			}
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			Bind();
            lbWriteOff.Visible = false;
            CalWriteOff();
    
		}
    
		protected void MakePayment(object sender, EventArgs e)
		{
    		Response.Redirect("/Creditor/Creditor_Payment.aspx?id="+Id+"&Type="+CompType+"&fct="+fct);
		}
    		
    	protected void EmailStatement(object sender, EventArgs e)
		{
    		Response.Redirect("/Creditor/Statement.aspx?Id="+Id);
		}
    	String Id, Id2;
		private void Bind()
		{
			//try
			//{

            pnlReceivables.Visible = true;
            lblReceivables.Visible = true;
            lblPayables.Visible = true;
            pnlTitleBar.Visible = true;

				StringBuilder sbSQL = new StringBuilder();
				StringBuilder sbSQL2 = new StringBuilder();
				SqlDataAdapter dadContent;
				DataSet dstContent;

				SqlConnection conn;
				conn = new SqlConnection(Application["DBConn"].ToString());
				conn.Open();
    
				Id = Request.QueryString["Id"];
                Session["CompID"] = Id;
				String Str;
				//Query the company based the Id
				Str="Select COMP_NAME, COMP_TYPE from company where COMP_ID='"+Id+"'";
				SqlDataReader dtr;
				SqlCommand cmd;
				cmd= new SqlCommand(Str, conn);
				cmd.Parameters.Add("@Cid", Id);
				dtr= cmd.ExecuteReader();
				dtr.Read();
				if (dtr["COMP_NAME"] != DBNull.Value )
				{
					CompType=(string)dtr["COMP_TYPE"];
					lblCompName.Text = (string)dtr["COMP_NAME"]+"("+CompType+")";
				}
				else
				{
					lblCompName.Text = "";
				}
				conn.Close();


                

				sbSQL.Append("Exec spAccountNewSummary");
				if ((ViewState["Sort"]!="") && (ViewState["Sort"]!=null))
				{
					sbSQL.Append(" @ORDER='"+ViewState["Sort"].ToString()+"',");
				}
				sbSQL.Append(" @CompID='"+Id+"'");
				sbSQL.Append(" ,@Type='R'");

			
				if (fct == "cur")
					sbSQL.Append(" ,@Funct='cur'");
                else if (fct == "all_history")
                    sbSQL.Append(" ,@Funct='all_history'");
                else if (fct == "out")
                    sbSQL.Append(" ,@Funct='out'");
				else if (fct == "com")
					sbSQL.Append(" ,@Funct='com'");
                else if (fct == "limbo")
                    sbSQL.Append(" ,@Funct='limbo'");
				else if (fct == "030")
					sbSQL.Append(" ,@Funct='030'");
				else if (fct == "03160")
					sbSQL.Append(" ,@Funct='03160'");
				else if (fct == "06190")
					sbSQL.Append(" ,@Funct='06190'");
				else if (fct == "090")
					sbSQL.Append(" ,@Funct='090'");
				else 
					sbSQL.Append(" ,@Funct='all_aging'");

					
				//Response.Write(sbSQL.ToString());
				dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
				dstContent = new DataSet();
				dadContent.Fill(dstContent);
				dg.DataSource = dstContent;
				dg.DataBind();

                if (fct == "out")
                {
                    StringBuilder sbSQL3 = new StringBuilder();
                    sbSQL3.Append("Exec spAccountNewSummary");
                    sbSQL3.Append(" @CompID='" + Id + "'");
                    sbSQL3.Append(" ,@Type='R'");
                    sbSQL3.Append(" ,@Funct='all_aging'");
                    dadContent = new SqlDataAdapter(sbSQL3.ToString(), conn);
                    dstContent = new DataSet();
                    dadContent.Fill(dstContent);
                    dg3.DataSource = dstContent;
                    dg3.DataBind();
                }

                
                
				sbSQL2.Append("Exec spAccountNewSummary");
				if ((ViewState["Sort"]!="") && (ViewState["Sort2"]!=null))
				{
					sbSQL2.Append(" @ORDER='"+ViewState["Sort2"].ToString()+"',");
				}
				sbSQL2.Append(" @CompID='"+Id+"'");
				sbSQL2.Append(" ,@Type='P'");

				if (fct == "cur")
					sbSQL2.Append(" ,@Funct='cur'");
                else if (fct == "all_history")
                    sbSQL2.Append(" ,@Funct='all_history'");
                else if (fct == "out")
                    sbSQL2.Append(" ,@Funct='out'");
                else if (fct == "limbo")
                    sbSQL2.Append(" ,@Funct='limbo'");
				else if (fct == "030")
					sbSQL2.Append(" ,@Funct='030'");
				else if (fct == "03160")
					sbSQL2.Append(" ,@Funct='03160'");
				else if (fct == "06190")
					sbSQL2.Append(" ,@Funct='06190'");
				else if (fct == "090")
					sbSQL2.Append(" ,@Funct='090'");
				else if (fct == "com")
					sbSQL2.Append(" ,@Funct='com'");				
				else
					sbSQL2.Append(" ,@Funct='all_aging'");

				dadContent = new SqlDataAdapter(sbSQL2.ToString(),conn);
				dstContent = new DataSet();
    
				dadContent.Fill(dstContent);               
				dg2.DataSource = dstContent;
				dg2.DataBind();

                if (fct == "out")
                {
                    StringBuilder sbSQL4 = new StringBuilder();
                    sbSQL4.Append("Exec spAccountNewSummary");
                    sbSQL4.Append(" @CompID='" + Id + "'");
                    sbSQL4.Append(" ,@Type='P'");
                    sbSQL4.Append(" ,@Funct='all_aging'");
                    dadContent = new SqlDataAdapter(sbSQL4.ToString(), conn);
                    dstContent = new DataSet();
                    dadContent.Fill(dstContent);
                    dg4.DataSource = dstContent;
                    dg4.DataBind();
                }

				conn.Close();
                
				if (dg.Items.Count==0)
				{
					dg.Visible=false;
				}
				else
				{
					dg.Visible=true;
				}

				if (dg2.Items.Count==0) 
				{
					dg2.Visible=false;						
				}
				else 
				{
					dg2.Visible=true;
				}

                if (dg3.Items.Count == 0)
                {
                    dg3.Visible = false;
                }
                else
                {
                    lblAging1.Visible = true;
                    dg3.Visible = true;
                }

                if (dg4.Items.Count == 0)
                {
                    dg4.Visible = false;                        
                }
                else
                {
                    lblAging2.Visible = true;
                    dg4.Visible = true;
                }
				

                if ((dg.Items.Count == 0) && (dg2.Items.Count == 0) && (dg3.Items.Count == 0) && (dg4.Items.Count == 0))
				{
					lblMessage.Visible = true;
				}
				else
				{
					lblMessage.Visible = false;
				}
			
			//}
			//catch(Exception e)
			//{
			//	Response.Write("exception: "+e+"<br>");
			//}
			
		}
 		// <summary>
		//  calculates the amount outstanding.  shows statement
		// </summary>

		
		protected void OnDataGridBind2(object sender, DataGridItemEventArgs e)
		{	
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{					
				if (e.Item.Cells[4].Text.Equals("N/A"))
				
					e.Item.Cells[4].Text="<font align='center'>-</font>";

				//if (e.Item.Cells[5].Text.Equals("0"))
				//	e.Item.Cells[5].Text="<font  align='center'>-</font>";

				if (e.Item.Cells[7].Text.Length>6)
					e.Item.Cells[7].Text = "$"+(e.Item.Cells[7].Text).ToString().Substring(0,6);
				
				double val1 = Convert.ToDouble(e.Item.Cells[9].Text.Substring(1));
				double val2 = Convert.ToDouble(e.Item.Cells[8].Text.Substring(1));
				if(val1>=val2) e.Item.Cells[6].Text = "-";

				e.Item.Cells[0].Text ="<a href=/administrator/Transaction_Details.aspx?Referer="+ Request.Url.ToString() +"&ID="+DataBinder.Eval(e.Item.DataItem, "TRANS_ID")+">Edit</a>";
				e.Item.Cells[10].Text ="<a href=/common/MB_Specs_Invoice.aspx?PO=True&ID="+DataBinder.Eval(e.Item.DataItem, "TRANS_ID")+">PO</a>";
				if (e.Item.Cells[5].Text.Trim()=="&nbsp;") e.Item.Cells[5].Text = " - ";
				e.Item.Cells[9].Text ="<a href=/Creditor/Edit_Payments.aspx?ID="+DataBinder.Eval(e.Item.DataItem, "TRANS_ID")+">"+e.Item.Cells[9].Text+"</a>";

				try
				{
					e.Item.Cells[2].Text = String.Format("{0:#,##}", System.Convert.ToInt32(e.Item.Cells[2].Text));
				}
				catch{}

				iDG2Weight += Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "SHIPMENT_WEIGHT_NUMBER"));
				ValueSumP += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "OUTSTANDING"));
				TotalPaidSumP += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "TOTAL"));
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[0].Text = "<b>Total:</b> ";
				e.Item.Cells[2].Text = "<b>"+String.Format("{0:#,##}", iDG2Weight)+"</b> ";
				e.Item.Cells[8].Text = "<b>"+String.Format("{0:c}", ValueSumP)+"</b>";
				e.Item.Cells[9].Text = "<b>&nbsp;&nbsp;"+String.Format("{0:c}", TotalPaidSumP)+"</b>";
			}
    	}

        protected void OnDataGridBind4(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[4].Text.Equals("N/A"))

                    e.Item.Cells[4].Text = "<font align='center'>-</font>";

                //if (e.Item.Cells[5].Text.Equals("0"))
                //	e.Item.Cells[5].Text="<font  align='center'>-</font>";

                if (e.Item.Cells[7].Text.Length > 6)
                    e.Item.Cells[7].Text = "$" + (e.Item.Cells[7].Text).ToString().Substring(0, 6);

                double val1 = Convert.ToDouble(e.Item.Cells[9].Text.Substring(1));
                double val2 = Convert.ToDouble(e.Item.Cells[8].Text.Substring(1));
                if (val1 >= val2) e.Item.Cells[6].Text = "-";

                e.Item.Cells[0].Text = "<a href=/administrator/Transaction_Details.aspx?Referer=" + Request.Url.ToString() + "&ID=" + DataBinder.Eval(e.Item.DataItem, "TRANS_ID") + ">Edit</a>";
                e.Item.Cells[10].Text = "<a href=/common/MB_Specs_Invoice.aspx?PO=True&ID=" + DataBinder.Eval(e.Item.DataItem, "TRANS_ID") + ">PO</a>";
                if (e.Item.Cells[5].Text.Trim() == "&nbsp;") e.Item.Cells[5].Text = " - ";
                e.Item.Cells[9].Text = "<a href=/Creditor/Edit_Payments.aspx?ID=" + DataBinder.Eval(e.Item.DataItem, "TRANS_ID") + ">" + e.Item.Cells[9].Text + "</a>";

                try
                {
                    e.Item.Cells[2].Text = String.Format("{0:#,##}", System.Convert.ToInt32(e.Item.Cells[2].Text));
                }
                catch { }

                iDG4Weight += Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "SHIPMENT_WEIGHT_NUMBER"));
                ValueSumP2 += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "OUTSTANDING"));
                TotalPaidSumP2 += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "TOTAL"));
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                e.Item.Cells[0].Text = "<b>Total:</b><BR><BR><b>TOTAL:</b> ";
                e.Item.Cells[2].Text = "<b>" + String.Format("{0:#,##}", iDG4Weight) + "</b><BR><BR><b>" + String.Format("{0:#,##}", iDG4Weight + iDG2Weight) + "</b> ";
                e.Item.Cells[8].Text = "<b>" + String.Format("{0:c}", ValueSumP2) + "</b><BR><BR><b>" + String.Format("{0:c}", ValueSumP2 + ValueSumP) + "</b>";
                e.Item.Cells[9].Text = "<b>&nbsp;&nbsp;" + String.Format("{0:c}", TotalPaidSumP2) + "</b><BR><BR><b>&nbsp;&nbsp;" + String.Format("{0:c}", TotalPaidSumP2 + TotalPaidSumP) + "</b>";
            }
        }

		protected void OnDataGridBind(object sender, DataGridItemEventArgs e)
		{	
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				e.Item.Cells[0].Text ="<a href=/administrator/Transaction_Details.aspx?Referer="+ Request.Url.ToString() +"&ID="+DataBinder.Eval(e.Item.DataItem, "TRANS_ID")+">Edit</a>";
				if (e.Item.Cells[4].Text.Equals("N/A"))
					e.Item.Cells[4].Text="<font  align='center'>-</font>";
				
				//if (e.Item.Cells[5].Text.Equals("0"))
				//	e.Item.Cells[5].Text="<font  align='center'>-</font>";

				if (e.Item.Cells[7].Text.Length>6)
					e.Item.Cells[7].Text = "$"+(e.Item.Cells[7].Text).ToString().Substring(0,6) ;

				double val1 = Convert.ToDouble(e.Item.Cells[9].Text.Substring(1));
				double val2 = Convert.ToDouble(e.Item.Cells[8].Text.Substring(1));
				if(val1>=val2) e.Item.Cells[6].Text = "-";

				e.Item.Cells[10].Text ="<a href=/common/MB_Specs_Invoice.aspx?ID="+DataBinder.Eval(e.Item.DataItem, "TRANS_ID")+">Invoice</a>";
				if (e.Item.Cells[5].Text.Trim()=="&nbsp;") e.Item.Cells[5].Text = " - ";
				e.Item.Cells[9].Text ="<a href=/Creditor/Edit_Payments.aspx?Receivable=true&ID="+DataBinder.Eval(e.Item.DataItem, "TRANS_ID")+">"+e.Item.Cells[9].Text+"</a>";

                //if (e.Item.Cells[11].Text.Equals("N/A"))
                //    e.Item.Cells[11].Text="<font  align='center'>-</font>";
				
				try
				{
					e.Item.Cells[2].Text = String.Format("{0:#,##}", System.Convert.ToInt32(e.Item.Cells[2].Text));
				}
				catch{}

				iDG1Weight += Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "SHIPMENT_WEIGHT_NUMBER"));
				ValueSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "OUTSTANDING"));
				TotalPaidSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "TOTAL"));
			}

			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[0].Text = "<b>Total:</b> ";
                e.Item.Cells[2].Text = "<b>" + String.Format("{0:#,##}", iDG1Weight) + "</b> ";
                e.Item.Cells[8].Text = "<b>" + String.Format("{0:c}", ValueSum) + "</b>";
                e.Item.Cells[9].Text = "<b>&nbsp;&nbsp;" + String.Format("{0:c}", TotalPaidSum) + "</b>";
			}
		}

        protected void OnDataGridBind3(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                e.Item.Cells[0].Text = "<a href=/administrator/Transaction_Details.aspx?Referer=" + Request.Url.ToString() + "&ID=" + DataBinder.Eval(e.Item.DataItem, "TRANS_ID") + ">Edit</a>";
                if (e.Item.Cells[4].Text.Equals("N/A"))
                    e.Item.Cells[4].Text = "<font  align='center'>-</font>";

                //if (e.Item.Cells[5].Text.Equals("0"))
                //	e.Item.Cells[5].Text="<font  align='center'>-</font>";

                if (e.Item.Cells[7].Text.Length > 6)
                    e.Item.Cells[7].Text = "$" + (e.Item.Cells[7].Text).ToString().Substring(0, 6);

                double val1 = Convert.ToDouble(e.Item.Cells[9].Text.Substring(1));
                double val2 = Convert.ToDouble(e.Item.Cells[8].Text.Substring(1));
                if (val1 >= val2) e.Item.Cells[6].Text = "-";

                e.Item.Cells[10].Text = "<a href=/common/MB_Specs_Invoice.aspx?ID=" + DataBinder.Eval(e.Item.DataItem, "TRANS_ID") + ">Invoice</a>";
                if (e.Item.Cells[5].Text.Trim() == "&nbsp;") e.Item.Cells[5].Text = " - ";
                e.Item.Cells[9].Text = "<a href=/Creditor/Edit_Payments.aspx?Receivable=true&ID=" + DataBinder.Eval(e.Item.DataItem, "TRANS_ID") + ">" + e.Item.Cells[9].Text + "</a>";

                //if (e.Item.Cells[11].Text.Equals("N/A"))
                //    e.Item.Cells[11].Text="<font  align='center'>-</font>";

                try
                {
                    e.Item.Cells[2].Text = String.Format("{0:#,##}", System.Convert.ToInt32(e.Item.Cells[2].Text));
                }
                catch { }

                iDG3Weight += Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "SHIPMENT_WEIGHT_NUMBER"));
                ValueSum2 += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "OUTSTANDING"));
                TotalPaidSum2 += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "TOTAL"));
            }

            else if (e.Item.ItemType == ListItemType.Footer)
            {
                e.Item.Cells[0].Text = "<b>Total:</b><BR><BR><b>TOTAL:</b> ";
                e.Item.Cells[2].Text = "<b>" + String.Format("{0:#,##}", iDG3Weight) + "</b><BR><BR><b>" + String.Format("{0:#,##}", iDG3Weight + iDG1Weight) + "</b> ";
                e.Item.Cells[8].Text = "<b>" + String.Format("{0:c}", ValueSum2) + "</b><BR><BR><b>" + String.Format("{0:c}", ValueSum2 + ValueSum) + "</b>";
                e.Item.Cells[9].Text = "<b>&nbsp;&nbsp;" + String.Format("{0:c}", TotalPaidSum2) + "</b><BR><BR><b>&nbsp;&nbsp;" + String.Format("{0:c}", TotalPaidSum2 + TotalPaidSum) + "</b>";
            }
        }
		
		// <summary>
		// Sorting function
		// </summary>
		public void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{
			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
    
			ColumnToSort = SortExprs[0];
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DESC";
			}
			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;
			// Figure out the column index
			int iIndex;
			iIndex = 0;
    
			switch(ColumnToSort.ToUpper())
			{
				case "TRANS_ID":
					iIndex = 1;
					break;
    
				case "SHIPMENT_WEIGHT":
					iIndex = 2;
					break;
    
				case "SHIPMENT_BUYER_TERMS":
					iIndex = 3;
					break;
    
				case "VARDDATE":
					iIndex = 4;
					break;
    
				case "SHIPMENT_BUYER_CLOSED_DATE":
					iIndex = 5;
					break;

				case "VARDATEDIFF":
					iIndex = 6;
					break;
    
				case "SELL_PRICE":
					iIndex = 7;
					break;
    
				case "OUTSTANDING":
					iIndex = 8;
					break;
    
				case "TOTAL":
					iIndex = 9;
					break;
				
				case "DAYSTOPAY":
					iIndex = 11;
					break;
			}
    		// alter the column's sort expression
    		dg.Columns[iIndex].SortExpression = NewSortExpr;
			dg.CurrentPageIndex = 0;
    
			// Sort the data in new order
			ViewState["Sort"] = NewSortExpr;
			// reset dbase page to the first one whether the sorting changes
			
			Bind();
		 }

        public void SortDG3(Object sender, DataGridSortCommandEventArgs e)
        {
            string[] SortExprs;
            string CurrentSearchMode;
            string NewSearchMode;
            string ColumnToSort;
            string NewSortExpr;
            Regex r = new Regex(" "); // Split on spaces.
            SortExprs = r.Split(e.SortExpression);

            ColumnToSort = SortExprs[0];
            if (SortExprs.Length > 1)
            {
                CurrentSearchMode = SortExprs[1].ToUpper();
                if (CurrentSearchMode == "ASC")
                {
                    NewSearchMode = "DESC";
                }
                else
                {
                    NewSearchMode = "ASC";
                }
            }
            else
            {   // If no mode specified, Default is descending
                NewSearchMode = "DESC";
            }
            //  Derive the new sort expression.
            NewSortExpr = ColumnToSort + " " + NewSearchMode;
            // Figure out the column index
            int iIndex;
            iIndex = 0;

            switch (ColumnToSort.ToUpper())
            {
                case "TRANS_ID":
                    iIndex = 1;
                    break;

                case "SHIPMENT_WEIGHT":
                    iIndex = 2;
                    break;

                case "SHIPMENT_BUYER_TERMS":
                    iIndex = 3;
                    break;

                case "VARDDATE":
                    iIndex = 4;
                    break;

                case "SHIPMENT_BUYER_CLOSED_DATE":
                    iIndex = 5;
                    break;

                case "VARDATEDIFF":
                    iIndex = 6;
                    break;

                case "SELL_PRICE":
                    iIndex = 7;
                    break;

                case "OUTSTANDING":
                    iIndex = 8;
                    break;

                case "TOTAL":
                    iIndex = 9;
                    break;

                case "DAYSTOPAY":
                    iIndex = 11;
                    break;
            }
            // alter the column's sort expression
            dg3.Columns[iIndex].SortExpression = NewSortExpr;
            dg3.CurrentPageIndex = 0;

            // Sort the data in new order
            ViewState["Sort"] = NewSortExpr;
            // reset dbase page to the first one whether the sorting changes

            Bind();
        }

		// <summary>
		// Sorting function
		// </summary>
		public void SortDG2(Object sender, DataGridSortCommandEventArgs e )
		{
			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
    
			ColumnToSort = SortExprs[0];
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DESC";
			}
			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;
			// Figure out the column index
			int iIndex;
			iIndex = 0;
    
			switch(ColumnToSort.ToUpper())
			{
				case "TRANS_ID":
					iIndex = 1;
					break;
    
				case "SHIPMENT_WEIGHT":
					iIndex = 2;
					break;
    
				case "SHIPMENT_BUYER_TERMS":
					iIndex = 3;
					break;
    
				case "VARDDATE":
					iIndex = 4;
					break;
    
				case "SHIPMENT_SELLER_CLOSED_DATE1":
					iIndex = 5;
					break;

				case "VARDATEDIFF":
					iIndex = 6;
					break;
    
				case "BUY_PRICE":
					iIndex = 7;
					break;
    
				case "OUTSTANDING":
					iIndex = 8;
					break;
    
				case "TOTAL":
					iIndex = 9;
					break;
			}
			// alter the column's sort expression
			dg2.Columns[iIndex].SortExpression = NewSortExpr;
			dg2.CurrentPageIndex = 0;
    
			// Sort the data in new order
			ViewState["Sort2"] = NewSortExpr;
			// reset dbase page to the first one whether the sorting changes
			
			Bind();
		}

        public void SortDG4(Object sender, DataGridSortCommandEventArgs e)
        {
            string[] SortExprs;
            string CurrentSearchMode;
            string NewSearchMode;
            string ColumnToSort;
            string NewSortExpr;
            Regex r = new Regex(" "); // Split on spaces.
            SortExprs = r.Split(e.SortExpression);

            ColumnToSort = SortExprs[0];
            if (SortExprs.Length > 1)
            {
                CurrentSearchMode = SortExprs[1].ToUpper();
                if (CurrentSearchMode == "ASC")
                {
                    NewSearchMode = "DESC";
                }
                else
                {
                    NewSearchMode = "ASC";
                }
            }
            else
            {   // If no mode specified, Default is descending
                NewSearchMode = "DESC";
            }
            //  Derive the new sort expression.
            NewSortExpr = ColumnToSort + " " + NewSearchMode;
            // Figure out the column index
            int iIndex;
            iIndex = 0;

            switch (ColumnToSort.ToUpper())
            {
                case "TRANS_ID":
                    iIndex = 1;
                    break;

                case "SHIPMENT_WEIGHT":
                    iIndex = 2;
                    break;

                case "SHIPMENT_BUYER_TERMS":
                    iIndex = 3;
                    break;

                case "VARDDATE":
                    iIndex = 4;
                    break;

                case "SHIPMENT_SELLER_CLOSED_DATE1":
                    iIndex = 5;
                    break;

                case "VARDATEDIFF":
                    iIndex = 6;
                    break;

                case "BUY_PRICE":
                    iIndex = 7;
                    break;

                case "OUTSTANDING":
                    iIndex = 8;
                    break;

                case "TOTAL":
                    iIndex = 9;
                    break;
            }
            // alter the column's sort expression
            dg4.Columns[iIndex].SortExpression = NewSortExpr;
            dg4.CurrentPageIndex = 0;

            // Sort the data in new order
            ViewState["Sort2"] = NewSortExpr;
            // reset dbase page to the first one whether the sorting changes

            Bind();
        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void ddlTransaction_SelectedIndexChanged(object sender, System.EventArgs e)
		{
				Response.Redirect("/Creditor/AccountSummary.aspx?id="+Id+"&fct=" + ddlTransaction.SelectedValue);
		}

        private void CalWriteOff()
        {
            SqlConnection conn;
            conn = new SqlConnection(Application["DBConn"].ToString());
            conn.Open();
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("select SUM((CASE ISNULL(SHIPMENT_WEIGHT,-1) WHEN -1 THEN (SHIPMENT_SIZE)*SHIPMENT_QTY ELSE SHIPMENT_WEIGHT END) * SHIPMENT_BUYR_PRCE * (( 100 + ISNULL(SHIPMENT_TAX,0) ) / 100))  AS OUTSTANDING FROM SHIPMENT WHERE (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)='"+Session["CompID"].ToString()+"' and shipment_writeoff = '1'");
            SqlCommand cmdWriteOffTotal;
            SqlDataReader dtrWriteOffTotal;
            cmdWriteOffTotal = new SqlCommand(sbSql.ToString(), conn);
            dtrWriteOffTotal = cmdWriteOffTotal.ExecuteReader();
            try
            {
                while (dtrWriteOffTotal.Read())
                {

                    string temp = dtrWriteOffTotal["OUTSTANDING"].ToString();
                    if (!temp.Equals(""))
                    {
                        lbWriteOffTotal.Text = "$" + temp;
                        lbWriteOff.Visible = true;
                    } 
                }
            }
            finally
            {
                dtrWriteOffTotal.Close();
            }


        }
	}
}
