using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using TPE.Utility;

namespace localhost.Creditor
{
	/// <summary>
	/// Summary description for Track_Rail_Car.
	/// </summary>
	public partial class IncomeStatementReport : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label8;
		int line = 1;
        public static string max_days = "";
        public static string startDate = "";
        public static string endDate = "";

		public void Page_Load(object sender, EventArgs e)
		{
			Master.Width = "1500px";
			if ((string)Session["Typ"] != "A")
			{
				 Response.Redirect("../default.aspx");
			}

			if (!IsPostBack)
			{
				HelperFunction.fillDatesDesc(01, 2005, ddlMonth);                
			}
		}

		private void calculateFooter(int day, int all_days)
		{
			double weight = 0;
			double buytotal = 0;
			double selltotal = 0;
			double margin = 0;

			for(int i = 2; i < dgReport.Items[0].Cells.Count; i++)
			{
				double s = 0;
				bool end = false;
				int k = 0;
				while(!end)
				{
					int this_day = 0;
					if(dgReport.Items.Count > k + 1)
					{
						this_day = (Convert.ToDateTime(dgReport.Items[k].Cells[1].Text)).Day;
					}
					else
					{
						end = true;
					}
					if(day <= this_day)
					{
						end = true;
					}
					try
					{
						s += Convert.ToDouble(nullValuesHTML(dgReport.Items[k].Cells[i].Text));
						
					}
					catch(Exception e)
					{
					}
					k++;
				}

				//String.Format("{0:##,###}", Convert.ToInt32(lblOverhead.Text));

				if (i == 2) weight = s;
				if (i == 3) buytotal = s;
				if (i == 5) selltotal = s;
				if (i == 12) margin = s;

				string total = "";
				string avarage = "";
				string estimate = "";

				if (i == 4)
				{
				    total = String.Format("{0:##.####}", buytotal/weight);
				    avarage = String.Format("{0:##.####}", buytotal / weight);
				    estimate = String.Format("{0:##.####}", buytotal / weight);
				}
				else if (i == 6)
				{
				    total = String.Format("{0:##.####}", (buytotal-selltotal) / weight);
				    avarage = String.Format("{0:##.####}", (buytotal - selltotal) / weight);
				    estimate = String.Format("{0:##.####}", (buytotal - selltotal) / weight);
				}
				else if (i == 13)
				{
				    total = String.Format("{0:##.####}", margin / weight);
				    avarage = String.Format("{0:##.####}", margin / weight);
				    estimate = String.Format("{0:##.####}", margin / weight);
				}
				else
				{
				    total = String.Format("{0:#,####}", s);
				    avarage = String.Format("{0:#,####}", s / k);
				    estimate = String.Format("{0:#,####}", (s / k) * all_days);
				}				

				string footer = total + "<BR>" + avarage + "<BR>" + estimate;

				try
				{
					/*
					Label lblTmp = new Label();
					lblTmp.Text = footer;
*/
					DataGridItem itm = (DataGridItem)dgReport.Controls[0].Controls[dgReport.Controls[0].Controls.Count-1];
					itm.Cells[i].Text = footer;
//					dgReport.Items[dgReport.Items.Count - 1].Controls.AddAt(0, lblTmp);
					dgReport.ShowFooter = true;
//					dgReport.Columns[i].FooterText = footer;
				}
				catch(Exception er)
				{
				}
				
			}

		}

		private void Bind()
		{
			string Adj = "0";
			string SGA = "0";
			string BD = "0";
			string Interest = "0";

			StringBuilder sbSQL = new StringBuilder();
			SqlDataAdapter dadContent;
			DataSet dstContent;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();

			string strSQL1 = "select * from IncomeStatement where CurrentMonth = '" + ddlMonth.SelectedValue.Substring(4, 2).ToString() + "' and CurrentYear = '" + ddlMonth.SelectedValue.Substring(0, 4).ToString() + "' ";
			using (SqlDataReader dtrTrans1 = DBLibrary.GetDataReaderFromSelect(conn, strSQL1))
			{
				if (dtrTrans1.Read())
				{
					txtAdj.Text = dtrTrans1["ADJ"].ToString();
					txtSGA.Text = dtrTrans1["SGA"].ToString();
					txtBD.Text = dtrTrans1["BD"].ToString();
					txtInterest.Text = dtrTrans1["Interest"].ToString();
				}
				else 
				{
					txtAdj.Text = "0";
					txtSGA.Text = "0";
					txtBD.Text = "0";
					txtInterest.Text = "0";
				}
			}
			//string strSQL = "select count(DISTINCT Convert(varchar,shipment_transaction_date,101)) AS DaysCount from shipment, person where  shipment_broker_id *= pers_id AND (SELECT COMP_NAME FROM COMPANY where COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)) <> 'The Plastics Exchange' AND SHIPMENT_SKU <>'RC1' and  shipment_transaction_date BETWEEN '" + startDate + "' AND '" + endDate + "' ";
			string strSQL = "select COUNT(CONVERT(VARCHAR,account_date,101)) DaysCount from acb_accounts where account_date BETWEEN '" + startDate + "' AND '" + endDate + "'";
			using (SqlDataReader dtrTrans = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
			{
				if (dtrTrans.Read())
				{
					string count = dtrTrans["DaysCount"].ToString();
					Adj = Convert.ToString(Convert.ToInt32(txtAdj.Text) / Convert.ToInt32(count));
					SGA = Convert.ToString(Convert.ToInt32(txtSGA.Text) / Convert.ToInt32(count));
					BD = Convert.ToString(Convert.ToInt32(txtBD.Text) / Convert.ToInt32(count));
					Interest = Convert.ToString(Convert.ToInt32(txtInterest.Text) / Convert.ToInt32(count));
				}
			}                      

			sbSQL.Append("Exec spIncomeNewStatementReport");
			sbSQL.Append(" @StartDate='" + startDate.ToString() + "'");
			sbSQL.Append(" ,@EndDate='" + endDate.ToString() + "'");			
			sbSQL.Append(" ,@Adjustment='" + Adj.ToString() + "'");
			sbSQL.Append(" ,@SGA='" + SGA.ToString() + "'");
			sbSQL.Append(" ,@BD='" + BD.ToString() + "'");
			sbSQL.Append(" ,@Interest='" + Interest.ToString() + "'");
			
			dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dgReport.DataSource = dstContent;
			dgReport.DataBind();
			conn.Close();

			// add 3 footers
			string day1 = max_days;
			int day = Convert.ToInt32(day1);
			int all_days = dgReport.Items.Count;
			calculateFooter(day, all_days);
			
		}		

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgReport.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgReport_ItemDataBound);

		}
		#endregion

		private void dgReport_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				e.Item.Cells[0].Text =  line.ToString();
				line++;
			}
		}

		private string nullValuesHTML(string input)
		{
			string str_return = "0";

			if (input != null)
			{
				if ((input != "&nbsp;") && (input.Trim() != ""))
				{                    
                    str_return = Convert.ToInt32(Convert.ToDouble(input)).ToString();
				}
			}
			return str_return;
		}

		protected void btnReport_OnClick(object sender, EventArgs e)
		{
		    int currentMonth = Convert.ToInt32(ddlMonth.SelectedValue.Substring(4, 2).ToString());
		    int currentYear = Convert.ToInt32(ddlMonth.SelectedValue.Substring(0, 4).ToString());

		    DateTime somedate = Convert.ToDateTime(currentMonth.ToString() + "/01/" + currentYear.ToString());
		    startDate = somedate.Month + "/01/" + somedate.Year;
		    max_days = (DateTime.DaysInMonth(somedate.Year, somedate.Month)).ToString();
		    endDate = somedate.Month + "/" + max_days + "/" + somedate.Year;
	                    
		    pnl1.Visible = true;
		    Bind();
		}

		protected void btnUpdate_OnClick(object sender, EventArgs e)
		{
		    bool hasRecord = false;
		    using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
		    {
			conn.Open();

			string strSQL1 = "select * from IncomeStatement where CurrentMonth = '" + ddlMonth.SelectedValue.Substring(4, 2).ToString() + "' and CurrentYear = '" + ddlMonth.SelectedValue.Substring(0, 4).ToString() + "' ";
			using (SqlDataReader dtrTrans1 = DBLibrary.GetDataReaderFromSelect(conn, strSQL1))
			{
			    if (dtrTrans1.Read())
			    {
				if (dtrTrans1["CurrentMonth"].ToString().Length > 0)
				{
				    hasRecord = true;
				}
			    }
			}

			if (hasRecord)
			{
			    string strSQL = "";
			    strSQL += "UPDATE IncomeStatement ";
			    strSQL += " SET ADJ = '" + txtAdj.Text + "', SGA = '" + txtSGA.Text + "', BD = '" + txtBD.Text + "', Interest = '" + txtInterest.Text + "' ";
			    strSQL += " WHERE CurrentMonth = '" + ddlMonth.SelectedValue.Substring(4, 2).ToString() + "' and CurrentYear = '" + ddlMonth.SelectedValue.Substring(0, 4).ToString() + "' ";
			    SqlCommand cmdAddPayment = new SqlCommand(strSQL, conn);
			    cmdAddPayment.ExecuteNonQuery();
			}
			else
			{
			    string strSQL = "";
			    strSQL += "INSERT INTO IncomeStatement ";
			    strSQL += " (CurrentMonth, CurrentYear, ADJ, SGA, BD, Interest) ";
			    strSQL += " VALUES('" + ddlMonth.SelectedValue.Substring(4, 2).ToString() + "','" + ddlMonth.SelectedValue.Substring(0, 4).ToString() + "','" + txtAdj.Text + "','" + txtSGA.Text + "','" + txtBD.Text + "','" + txtInterest.Text + "') ";
			    SqlCommand cmdAddPayment = new SqlCommand(strSQL, conn);
			    cmdAddPayment.ExecuteNonQuery();
			}
	                
		    }            
		}

		protected void btnExportToExcel_Click(object sender, System.EventArgs e)
		{
		    Bind();

		    dgReport.Columns[0].Visible = false;

		    Response.ContentType = "application/vnd.ms-excel";
		    Response.AddHeader("Content-Disposition", "attachment; filename=CashPaid.xls");

		    Response.BufferOutput = true;
		    Response.ContentEncoding = System.Text.Encoding.UTF8;
		    Response.Charset = "UTF-8";
		    EnableViewState = false;

		    System.IO.StringWriter tw = new System.IO.StringWriter();
		    System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

		    FormatExportedGrid(dgReport);            
		    dgReport.RenderControl(hw);

		    Response.Write(tw.ToString());
		    Response.End();
		}

		public void FormatExportedGrid(DataGrid dgReport)
		{
		    dgReport.BackColor = System.Drawing.Color.White;
		}

		public override void VerifyRenderingInServerForm(Control control)
		{
		    /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
		       server control at run time. */
		}
	}
}
