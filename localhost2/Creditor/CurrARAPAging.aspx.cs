using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Text;
using TPE.Utility;

namespace localhost.Creditor
{
	/// <summary>
	/// Summary description for CurrARAPAging.
	/// </summary>
	public partial class CurrARAPAging : System.Web.UI.Page
	{
		/************************************************************************
	*   1. File Name       :CurrAPRPAging.aspx								 *
	*   2. Description     :												 *
	*   3. Modification Log:                                                *
	*     Ver No.       Date          Author             Modification       *
			  *   -----------------------------------------------------------------   *
			  *                                                                       *
			  *                                                                       *
			  ************************************************************************/
		double runningSum = 0.0;
		double CurrentSum = 0.0;
		double LimboSum = 0.0;
		double o30Sum = 0.0;
		double o3160Sum = 0.0;
		double o6190Sum = 0.0;
		double o90orMoreSum = 0.0;
		double CommetedInventory=0.0;
		public int cptOutstanding = 0;
		public string[] outstanding0 = new string[1000];
		public double[] outstanding1 = new double[1000];
		public double totalrunningSum = 0;
		protected System.Web.UI.WebControls.DropDownList ddl;
		public string etat = "";
		
		string  CompType="";

		protected void Page_Load(object sender, EventArgs e)
		{
			Master.Width = "1300px";
			etat = Request.QueryString["etat"];

			//Administrator and Creditor have access
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C") && ((string)Session["Typ"] != "L"))
			{
				Response.Redirect("/default.aspx");
			}
			if (!IsPostBack)
			{
				// populating drop-down box for days late
				ViewState["Sort"] = "COMP_NAME ASC";
				get10firstCOMP();
				Bind();
			}			
		}

		private void get10firstCOMP()
		{// get the 10 first company ordered by outstanding
			using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				conn.Open();
				Hashtable htParams = new Hashtable();
				htParams.Add("@ORDER","FO DESC");
			
				/*******************************************/
				if (etat == "R")
				{
					htParams.Add("@Type","R");
					CompType="R";
				}
				else if (etat == "P")
				{
					htParams.Add("@Type","P");
					CompType="P";
				}
			
				using (SqlDataReader myReader = DBLibrary.GetDataReaderStoredProcedure(conn,"spNewCurrARAPAging",htParams))
				{
					int cpt10first = 0;
					if (myReader.HasRows)
					{
						while (myReader.Read())
						{
							outstanding0[cpt10first]=myReader.GetValue(3).ToString();
							outstanding1[cpt10first]=Convert.ToDouble(myReader.GetValue(6));
							++cpt10first;
						}
					}
				}
			}
			
			runningSum = 0.0;
			CurrentSum = 0.0;
			LimboSum = 0.0;
			o30Sum = 0.0;
			o3160Sum = 0.0;
			o6190Sum = 0.0;
			o90orMoreSum = 0.0;
			CommetedInventory=0.0;
		}

		protected void ddlChanged(object sender, EventArgs e)
		{
			get10firstCOMP();
			Bind ();
		}

		private void Bind( )
		{
			Hashtable htParams = new Hashtable();
			htParams.Add("@ORDER",ViewState["Sort"]);			
			
			if (etat == "R")
			{
				htParams.Add("@Type","R");
				CompType="R";
			}
			else if (etat == "P")
			{
				htParams.Add("@Type","P");			
				CompType="P";
			}	
			
			DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(),dg,"spNewCurrARAPAging",htParams);
			
			runningSum = 0.0;
			CurrentSum = 0.0;
			LimboSum = 0.0;
			o30Sum = 0.0;
			o3160Sum = 0.0;
			o6190Sum = 0.0;
			o90orMoreSum = 0.0;
			CommetedInventory=0.0;
		}



		// <summary>
		//  calculates the amount outstanding.  shows statement
		// </summary>
		protected void OnDataGridBind(object sender, DataGridItemEventArgs e)
		{

			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				// sums the items for calculation at bottom
				
				runningSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "FO"));
				CurrentSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "OCURRENT"));
				LimboSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "OLIMBO"));
				o30Sum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "o30"));
				o3160Sum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "o3160"));
				o6190Sum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "o6190"));
				o90orMoreSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "O90ORMORE"));
				CommetedInventory += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "COMMINVTRY"));
				// Add Payments link
				//e.Item.Cells[12].Text ="<a href=/Creditor/Creditor_Payment.aspx?id="+DataBinder.Eval(e.Item.DataItem, "COMP_ID")+"&Type=" +CompType+"&fct=out>Add Payments</a>";

                if (Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "COMP_PDC")))
                {
                    e.Item.Cells[3].Font.Bold = true;
                }

				if (etat == "P") 
					e.Item.Cells[13].Text = "<a href=/Creditor/CashPaid.aspx>Add Payments</a>";
				else 
					e.Item.Cells[13].Text = "<a href=/Creditor/CashReceived.aspx>Add Payments</a>";

				// values that are equal to zero should not be linked
				if  (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "FO")) ==0.0)
				{
					e.Item.Cells[5].Text = "$0.00";
				}
				if  (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "COMMINVTRY")) ==0.0)
				{
					e.Item.Cells[6].Text = "$0.00";
				}
				if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "OLIMBO")) == 0.0)
				{
					e.Item.Cells[7].Text = "$0.00";
				}
				if  (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "OCURRENT")) ==0.0)
				{
					e.Item.Cells[8].Text = "$0.00";
				}
				if  (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "O30")) ==0.0)
				{
					e.Item.Cells[9].Text = "$0.00";
				}
				if  (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "O3160")) ==0.0)
				{
					e.Item.Cells[10].Text = "$0.00";
				}
				if  (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "O6190")) ==0.0)
				{
					e.Item.Cells[11].Text = "$0.00";
				}
				if  (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "O90ORMORE")) ==0.0)
				{
					e.Item.Cells[12].Text = "$0.00";
				}

				// change image if there is a comment
				if ( (string)(DataBinder.Eval(e.Item.DataItem, "COMMENTSTAT")) =="1")
				{
					e.Item.Cells[1].Text = "<a href=\"/Administrator/Company_Comment.aspx?ID=" + (DataBinder.Eval(e.Item.DataItem, "COMP_ID")).ToString() + "\">" + Convert.ToDateTime((DataBinder.Eval(e.Item.DataItem, "COMMENTDATE"))).ToString("MM/dd/yyyy") + "</a>";
				}
				else
				{
					e.Item.Cells[1].Text = "<a href=\"/Administrator/Company_Comment.aspx?ID="+ (DataBinder.Eval(e.Item.DataItem, "COMP_ID")).ToString() +"\"><img src=/Pics/icons/comment.gif height='14' width='14' border=0></a>";
				}
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[0].Text = "<b>Total:</b> ";
				e.Item.Cells[5].Text = "<b>"+String.Format("{0:c}", runningSum)+"</b>";

				totalrunningSum = runningSum;

				e.Item.Cells[6].Text = "<b>"+String.Format("{0:c}", CommetedInventory)+"</b>";
				e.Item.Cells[7].Text = "<b>" + String.Format("{0:c}", LimboSum) + "</b>";
				e.Item.Cells[8].Text = "<b>"+String.Format("{0:c}", CurrentSum)+"</b>";
				e.Item.Cells[9].Text = "<b>"+String.Format("{0:c}", o30Sum)+"</b>";
				e.Item.Cells[10].Text = "<b>"+String.Format("{0:c}", o3160Sum)+"</b>";
				e.Item.Cells[11].Text = "<b>"+String.Format("{0:c}", o6190Sum)+"</b>";
				e.Item.Cells[12].Text = "<b>"+String.Format("{0:c}", o90orMoreSum)+"</b>";
				
			}

		}

		
		// <summary>
		// Sorting function
		// </summary>
		public void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{
			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;
			//  Parse the sort expression - delimiter space
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;

			ColumnToSort = SortExprs[0];
			// If a sort order is specified get it, else default is descending
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DESC";
			}

			//Response.Write(" search = "+NewSearchMode.ToString());

			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;

			//Response.Write(" ------- newsort = " + NewSortExpr.ToString());

			// Figure out the column index
			int iIndex;
			iIndex = 0;

			switch(ColumnToSort.ToUpper())
			{
				case "COMP_NAME":
					iIndex = 0;
					break;

				case "FO":
					iIndex = 3;
					break;

				case "COMP_CREIDIT_LIMIT":
					iIndex = 4;
					break;

				case "OCURRENT":
					iIndex = 5;
					break;
				case "OLIMBO":
				    iIndex = 6;
				    break;
				case "O30":
					iIndex = 7;
					break;

				case "O3160":
					iIndex = 8;
					break;

				case "O6190":
					iIndex = 9;
					break;

				case "O90ORMORE":
					iIndex = 10;
					break;

				case "COMMINVTRY":
					iIndex = 11;
					break;

			}


			// alter the column's sort expression


			dg.Columns[iIndex].SortExpression = NewSortExpr;
			
			dg.CurrentPageIndex = 0;


			// Sort the data in new order
			ViewState["Sort"] = NewSortExpr;
			// reset dbase page to the first one whether the sorting changes

			get10firstCOMP();
			Bind();
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_ItemCommand);

		}
		#endregion

		private void dg_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName=="Comments")
			{
				Response.Redirect("../Administrator/Company_Comment.aspx?ID=" + e.Item.Cells[12].Text);
			}
		}



		//Excel
		protected void btnExportToExcel_Click(object sender, System.EventArgs e)
		{
		
		    Bind();

		    dg.Columns[1].Visible = false;
		    dg.Columns[2].Visible = false;
		    //dg.Columns[12].Visible = false;
		    dg.Columns[13].Visible = false;
	            
		    Response.Clear();
		    Response.Buffer = true;
		    Response.ContentType = "application/vnd.ms-excel";
		    Response.AddHeader("Content-Disposition", "attachment; filename=creditmanagement.xls");

		    Response.BufferOutput = true;
		    Response.ContentEncoding = System.Text.Encoding.UTF8;
		    Response.Charset = "";
		    EnableViewState = false;

		    System.IO.StringWriter tw = new System.IO.StringWriter();
		    System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

		    FormatExportedGrid(dg);
		    this.ClearControls(dg);
		    dg.RenderControl(hw);



		    Response.Write(CleanHTML(tw.ToString()));
		    Response.End();

		}

		public void FormatExportedGrid(DataGrid dg1)
		{
		    dg1.BackColor = System.Drawing.Color.White;
		}

		private string CleanHTML(string OutOut)
		{

		    //will add some code to fix gridlines

		    return OutOut;


		}

		public override void VerifyRenderingInServerForm(Control control)
		{
		    /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
		       server control at run time. */

		}

		private void ClearControls(Control control)
		{
		    try
		    {

			for (int i = control.Controls.Count - 1; i >= 0; i--)
			{
			    ClearControls(control.Controls[i]);
			}
			if (!(control is TableCell))
			{
			    if (control.GetType().GetProperty("SelectedItem") != null)
			    {
				LiteralControl literal = new LiteralControl();
				control.Parent.Controls.Add(literal);
				try
				{
				    literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
				}
				catch
				{
				}
				control.Parent.Controls.Remove(control);
			    }
			    else
				if (control.GetType().GetProperty("Text") != null)
				{
				    LiteralControl literal = new LiteralControl();
				    control.Parent.Controls.Add(literal);
				    literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);
				    control.Parent.Controls.Remove(control);
				}
			}
			return;

		    }
		    catch (Exception)
		    {
	                
		    }

	       
		}
    }
}