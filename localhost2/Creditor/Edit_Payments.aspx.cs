using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using TPE.Utility;


namespace localhost.Creditor
{
	// Summary description for Edit_Payments.
	
	public partial class Edit_Payments : System.Web.UI.Page
	{
	
	/************************************************************************
	*   1. File Name       :Creditor\Edit_Payments.aspx                     *
	*   2. Description     :Transaction summary                             *
	*   3. Modification Log:                                                *
	*     Ver No.       Date          Author             Modification       *
	*   -----------------------------------------------------------------   *
	*      1.00      3-2-2004      Zach                    Comment          *
	*      1.01		 8-12-2004     Alexis                                   *                        *
	************************************************************************/

		public void Page_Load(object sender, EventArgs e)
		{
			Master.Width = "1024";
			
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C"))
			{
				Response.Redirect("/default.aspx");
			}

			if (!IsPostBack)
			{
				//SHIPMENT ID
				ViewState["OrderNum"] = Request.QueryString["Id"].ToString();
				
				string Id0;
				string[] tempId = new string[2];
				char[] splitter  = {'-'};  				
				Id0 = ViewState["OrderNum"].ToString();
				tempId = Id0.Split(splitter);
				ViewState["Id1"] = tempId[0];
				ViewState["Id2"] = tempId[1];

				if (Session["CompID"].ToString() == "")
				{
				    Button2.Visible = true;
				    Button3.Visible = false;
				}
				else
				{
				    Button2.Visible = false;
				    Button3.Visible = true;
				}
				using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
				{
					conn.Open();
					SqlCommand cmdPerson, cmdClosingDate;
					if (Request.QueryString["Receivable"] != null)
					{
						//Response.Write("BUYER case<br>");
						ViewState["Receivable"] = true;
						cmdPerson = new SqlCommand("SELECT SHIPMENT_BUYR FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='" + ViewState["Id1"] + "' AND SHIPMENT_SKU='" + ViewState["Id2"]+"'",conn);
						cmdClosingDate = new SqlCommand("SELECT SHIPMENT_BUYER_CLOSED_DATE FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='" + ViewState["Id1"] + "' AND SHIPMENT_SKU='" + ViewState["Id2"]+"'",conn);
					}
					else
					{
						ViewState["Receivable"] = false;
						cmdPerson = new SqlCommand("SELECT SHIPMENT_SELR FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='" + ViewState["Id1"] + "' AND SHIPMENT_SKU='" + ViewState["Id2"]+"'",conn);
						cmdClosingDate = new SqlCommand("SELECT SHIPMENT_SELLER_CLOSED_DATE1 FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='" + ViewState["Id1"] + "' AND SHIPMENT_SKU='" + ViewState["Id2"]+"'",conn);
					}

					ViewState["Person"] = cmdPerson.ExecuteScalar();

					//Quick Fix for the CloseTransactionButton issue.
					btnCloseTransaction.Enabled = true;
				}

				//Need SHIPMENT_ID
				using (SqlConnection connex = new SqlConnection(Application["DBConn"].ToString()))
				{
					connex.Open();
	
					SqlCommand cmdShipId;
					cmdShipId= new SqlCommand("SELECT SHIPMENT_ID FROM SHIPMENT WHERE SHIPMENT_ORDR_ID = '" + ViewState["Id1"].ToString() + "' AND SHIPMENT_SKU = '" + ViewState["Id2"].ToString() + "'", connex);
					ViewState["Shipment_Id"] = cmdShipId.ExecuteScalar();
				}
				Bind();
		                lblTotalAmount.Text = GetAmountOwed();
			} 
		}


		private string GetAmountOwed()
		{
		    using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
		    {
			conn.Open();
			SqlCommand cmdAmount;
	                
			string sSQL="";

			if (Request.QueryString.Count > 1)
			{
			    sSQL = "select (CASE ISNULL(SHIPMENT_WEIGHT,-1) WHEN -1 THEN (SHIPMENT_SIZE)*SHIPMENT_QTY ELSE SHIPMENT_WEIGHT END) * SHIPMENT_BUYR_PRCE * (( 100 + ISNULL(SHIPMENT_TAX,0) ) / 100)  AS OUTSTANDING from shipment where shipment_ordr_id=" + ViewState["Id1"] + " and shipment_sku='" + ViewState["Id2"] + "' and shipment_writeoff = 'false'";
			}
			else
			{
			    sSQL = "select (CASE ISNULL(SHIPMENT_WEIGHT,-1) WHEN -1 THEN (SHIPMENT_SIZE)*SHIPMENT_QTY ELSE SHIPMENT_WEIGHT END) * SHIPMENT_PRCE * (( 100 + ISNULL(SHIPMENT_TAX,0) ) / 100)  AS OUTSTANDING from shipment where shipment_ordr_id=" + ViewState["Id1"] + " and shipment_sku='" + ViewState["Id2"] + "' and shipment_writeoff = 'false'";
			}
			cmdAmount = new SqlCommand(sSQL,conn);

			double d = Convert.ToDouble( cmdAmount.ExecuteScalar());
	               
			return d.ToString("C");

		    }
		}

	
		private void Bind()
		{
			
			SqlDataAdapter dadContent;
			DataSet dstContent;
			using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				conn.Open();
				StringBuilder sbSQL = new StringBuilder();
			
				sbSQL.Append(" SELECT PAYMENT.PAY_ORDR, COMPANY.COMP_NAME AS COMPANY, PAYMENT.PAY_AMNT, PAYMENT.PAY_DATE, PAYMENT.PAY_ID, PAYMENT.PAY_CMNT ");
				sbSQL.Append(" FROM COMPANY INNER JOIN ");
				sbSQL.Append(" PERSON ON COMPANY.COMP_ID = PERSON.PERS_COMP CROSS JOIN ");
				sbSQL.Append(" PAYMENT ");
				sbSQL.Append(" WHERE (PAYMENT.PAY_SHIPMENT = '" + ViewState["Shipment_Id"] + "') AND (PAYMENT.PAY_PERS = '"+ViewState["Person"]+"') AND (PERSON.PERS_ID = '"+ViewState["Person"]+"') ");

				dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
				dstContent = new DataSet();
				dadContent.Fill(dstContent);
				dg.DataSource = dstContent;
				dg.DataBind();
			}
			
			if(dstContent.Tables[0].Rows.Count == 0)
				btnCloseTransaction.Enabled = false;
		}
		
		//     Handler -- Selects the row to be edited	
		public void DG_Edit(object sender, DataGridCommandEventArgs e)
		{
			// store the orignal amount for later use
			// if text is blank then the value needs to be set to zero
			if (e.Item.Cells[6].Text.Length >0 )
			{
				ViewState["Orignal_Amount"] = 0;
			}
			else
			{
				ViewState["Orignal_Amount"] = e.Item.Cells[6].Text;
			}
			dg.EditItemIndex = e.Item.ItemIndex; 
			Bind();   
		}
		
		//     Handler --  cancels the row editing
		public void DG_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dg.EditItemIndex =-1;
			Bind(); 
		}

		private void checkPayId(string pay_id)
		{
			string sqlSelect = "select " +
				"		  ISNULL((select (pay_com_trans) " + 
				" from payment " + 
				" where pay_com_trans=1 and pay_shipment=(select pay_shipment from payment where pay_id=" + pay_id + ")),0) as paid," + 

				"	(" +
				"	  (select PARAM_VALUE from parameters where param_name='PERCENT_FULL_PAYMENT') / 100.0 * " +
				"		(select (SHIPMENT_BUYR_PRCE * " +
				"			ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY)) * (100 + ISNULL(SHIPMENT_TAX, 0)) / 100 " +
				"			from shipment " +
				"			where shipment_id=(select pay_shipment from payment where pay_id=" + pay_id + ")" + 
				"	         )" +
				"	  - ISNULL(sum(pay_amnt),0) " + 
				"	  ) as diff " +
				" from payment, shipment  " +
				" where pay_shipment=shipment_id " +
				"	and  " +
				" pay_id <= " + pay_id +
				" and (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR) = (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=pay_pers) " +
				"	and " + 
				"shipment_id = (select pay_shipment from payment where pay_id=" + pay_id + ")";

			bool checkFlag = false;
					
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				using (SqlDataReader dtrDiff = DBLibrary.GetDataReaderFromSelect(conn,sqlSelect))
				{
					if(dtrDiff.Read())
					{
						//> 0 means NOT full ammount with current payment
						if( (HelperFunction.getSafeDoubleFromDB(dtrDiff["diff"]) <= 0) && !HelperFunction.getSafeBooleanFromDB(dtrDiff["paid"]))
						{
							//only if flag is 0
							checkFlag = true;
						}
					}
				}
			}

			string sqlUpdate = "";
			if(checkFlag)
			{
				sqlUpdate = "UPDATE payment set pay_com_trans=1 WHERE pay_id=" + pay_id;
			}
			else
			{
				sqlUpdate = "UPDATE payment set pay_com_trans=1 WHERE pay_id=(select max(pay_id) from payment group by pay_shipment having pay_shipment=(select pay_shipment from payment where pay_id=" + pay_id + "))";
			}
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),sqlUpdate);
					
		}
		
		private void checkFlagFullPayment(string pay_id, string new_ammount)
		{
			string sqlSelect = "select " +
				"		  ISNULL((select (pay_com_trans) " + 
				" from payment " + 
				" where pay_com_trans=1 and pay_shipment=(select pay_shipment from payment where pay_id=" + pay_id + ")),0) as paid," + 
				"	(" +
				"	  (select PARAM_VALUE from parameters where param_name='PERCENT_FULL_PAYMENT') / 100.0 * " +
				"		(select SHIPMENT_BUYR_PRCE * " +
				"			ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) " +
				"			from shipment " +
				"			where shipment_id=(select pay_shipment from payment where pay_id=" + pay_id + ")" + 
				"	         )" +
				"	  - ISNULL(sum(pay_amnt),0) - " + new_ammount +
				"	  ) as diff " +
				" from payment, shipment  " +
				" where pay_shipment=shipment_id " +
				"	and  " +
				" pay_id <> " + pay_id +
				" and (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR) = (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=pay_pers) " +
				"	and " + 
				"shipment_id = (select pay_shipment from payment where pay_id=" + pay_id + ")";

			bool checkFlag = false;
			bool cancelFlag = false;

			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				using (SqlDataReader dtrDiff = DBLibrary.GetDataReaderFromSelect(conn,sqlSelect))
				{
					if(dtrDiff.Read())
					{
						//> 0 means NOT full ammount with current payment
						if( (HelperFunction.getSafeDoubleFromDB(dtrDiff["diff"]) > 0))
						{
							if(HelperFunction.getSafeBooleanFromDB(dtrDiff["paid"]))
							{
								//have to cancel flag
								cancelFlag = true;
							}
						}
						else
						{
							if(!HelperFunction.getSafeBooleanFromDB(dtrDiff["paid"]))
							{
								//only if flag is 0
								checkFlag = true;
							}
						}
					}
				}
			}
			if(cancelFlag)
			{
				string sqlCancel= "Update payment set pay_com_trans=0 where pay_com_trans=1 and pay_shipment=(select pay_shipment from payment where pay_id=" + pay_id + ")";
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),sqlCancel);
			}

			if(checkFlag)
			{
				checkPayId(pay_id);
			}
		}
	
		//     Handler --  deletes the record the row editing
		public void DG_Delete(object sender, DataGridCommandEventArgs e)
		{	
			checkFlagFullPayment(e.Item.Cells[2].Text, "0.0");
			string SQL = "UPDATE SHIPMENT set SHIPMENT_BUYER_CLOSED_DATE = NULL WHERE SHIPMENT_ID = (SELECT PAY_SHIPMENT FROM PAYMENT WHERE PAY_ID=@PayID)";
			string SQL2="Delete From PAYMENT WHERE PAY_ID=@PayID";
			Hashtable htParam = new Hashtable();
			htParam.Add("@PayID", e.Item.Cells[2].Text);  
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), SQL,htParam);
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), SQL2, htParam);
			
			Bind();  
		}

		//     Handler --  updates the tables based on new information		
		public void DG_Update(object sender, DataGridCommandEventArgs e)
		{
			double dbAmount_Diff;

			string new_ammount = ParseMoney(((TextBox) e.Item.Cells[6].Controls[0]).Text).ToString();
			string pay_id = e.Item.Cells[2].Text;

			// calc difference between the new and old amount
			dbAmount_Diff = Convert.ToDouble( ViewState["Orignal_Amount"]) - Convert.ToDouble(ParseMoney(((TextBox) e.Item.Cells[6].Controls[0]).Text));
			string SQL;
			SQL = "Update PAYMENT SET PAY_DATE=@PayDate, PAY_AMNT=@PayAmount, PAY_CMNT=@payComment WHERE PAY_ID=@PayID";
			Hashtable htParams = new Hashtable();
			htParams.Add("@PayDate",HelperFunction.RemoveSqlEscapeChars(((TextBox) e.Item.Cells[5].Controls[0]).Text));
			htParams.Add("@PayAmount",new_ammount);

			string sComment =  (((TextBox)e.Item.Cells[7].FindControl("txtComment")).Text).ToString(); 

			htParams.Add("@PayComment",HelperFunction.RemoveSqlEscapeChars(sComment));
			htParams.Add("@PayID",pay_id);
           
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),SQL,htParams);

			checkFlagFullPayment(pay_id, new_ammount);

			UpdateOrderStatus();
			dg.EditItemIndex =-1;
			Bind();  
		}

		// removes 
		protected string ParseMoney(string TextIN)
		{
			string strTemp = TextIN;
			foreach(char c in TextIN) 
			{
				if((!Char.IsNumber(c)) && (c.ToString() != ".") ) 
				{
					strTemp = strTemp.Replace(c.ToString(),"");
				}
			}
			return strTemp;

		}
	
	
		// Updates the status of both sides of the transactions
		private void UpdateOrderStatus()
		{
			string SQL="";
			string SQL2="";
			bool bClosed;
			StringBuilder sb = new StringBuilder();
	
			sb.Append(" SELECT ");
			sb.Append(" ((SHIPMENT_BUYR_PRCE)*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) - ");
			sb.Append(" ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),0)) AS BUYER, ");
			sb.Append(" (SHIPMENT_PRCE *ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) - ");
			sb.Append(" ISNULL((SELECT SUM(PAY_AMNT) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_SELR),0)) ");
			sb.Append(" AS SELLER from SHIPMENT WHERE SHIPMENT_ORDR_ID = '"+ViewState["Id1"]+"' AND SHIPMENT_SKU='"+ViewState["Id2"]+"'  ");

		

			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				using (SqlDataReader dtrUpdate = DBLibrary.GetDataReaderFromSelect(conn,sb.ToString()))
				{
					dtrUpdate.Read();
		
					SQL = "SELECT GETDATE()";
					SQL2 = "SELECT GETDATE()";
				
					if (Math.Abs(Convert.ToDouble(dtrUpdate["BUYER"])) < 1)
					{
						//Response.Write("Ds update BUYER et < 1 Id1-Id2: "+ViewState["Id1"]+"-"+ViewState["Id2"]+"<br><br>");
						SQL = "UPDATE SHIPMENT SET SHIPMENT_BUYER_CLOSED_DATE=GETDATE() WHERE (SHIPMENT_ORDR_ID = '"+ViewState["Id1"]+"') AND (SHIPMENT_SKU = '"+ViewState["Id2"]+"')";		
					}	
					DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),SQL);
		
					if (Math.Abs(Convert.ToDouble(dtrUpdate["SELLER"])) < 1)
					{
						//Response.Write("Ds update BUYER et < 1 Id1-Id2: "+ViewState["Id1"]+"-"+ViewState["Id2"]+"<br><br>");
						SQL2 = "UPDATE SHIPMENT SET SHIPMENT_SELLER_CLOSED_DATE1=GETDATE() WHERE (SHIPMENT_ORDR_ID = '"+ViewState["Id1"]+"') AND (SHIPMENT_SKU = '"+ViewState["Id2"]+"')";
					}
					DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),SQL2);
				}
			}
		}
		
		// Adds a blank payment to the screen
		public void Add_New(object sender, EventArgs e)
		{
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"INSERT INTO PAYMENT (PAY_PERS,PAY_ORDR,PAY_SHIPMENT) VALUES ('"+ViewState["Person"].ToString()+"','"+ViewState["Id1"].ToString()+"','"+ ViewState["Shipment_Id"].ToString() +"')");
			Bind();

		}

		// Handler for when the user clicks done
	
		
		public void Navigate_Back(object sender, EventArgs e)
		{
			Response.Redirect("/Creditor/Transaction_Summary.aspx");
		}

		public void Navigate_Back2(object sender, EventArgs e)
		{
		    Response.Redirect("/Creditor/AccountSummary.aspx?id=" + Session["CompID"].ToString() + "&fct=" + Session["fct"].ToString());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnCloseTransaction_Click(object sender, System.EventArgs e)
		{
			string SQL = "";
			string strSql1 = "";
			string strSql2 = "";
			StringBuilder sb = new StringBuilder();
			
			using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				conn.Open();
			
				if ((bool)ViewState["Receivable"] == true)
				{
					sb.Append(" SELECT ");			
					sb.Append(" (SELECT MAX(PAY_DATE) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS = SHIPMENT_BUYR) ");
					sb.Append(" AS CLOSINGDATE from SHIPMENT WHERE SHIPMENT_ORDR_ID = '" + ViewState["Id1"] + "' AND SHIPMENT_SKU='" + ViewState["Id2"] + "'  ");
					using (SqlDataReader dtrUpdate = DBLibrary.GetDataReaderFromSelect(conn,sb.ToString()))
					{
						//Response.Write("Ds update BUYER et < 1 Id1-Id2: " + ViewState["Id1"] + "-" + ViewState["Id2"] + "<br><br>");
						if(dtrUpdate.Read())
							SQL = "UPDATE SHIPMENT SET SHIPMENT_BUYER_CLOSED_DATE = '" + Convert.ToDateTime(dtrUpdate["CLOSINGDATE"]).ToShortDateString() + "' WHERE (SHIPMENT_ORDR_ID = '"+ViewState["Id1"]+"') AND (SHIPMENT_SKU = '"+ViewState["Id2"]+"')";
					}

					strSql1 = "UPDATE ACCOUNT_RECEIVABLES SET Transaction_Close = 'true' WHERE TRANSACTION_NUMBER = '" + ViewState["Id1"] + "-" + ViewState["Id2"] + "' ";
					strSql2 = "UPDATE CASH_RECEIVED SET Transaction_Close = 'true' WHERE TRANSACTION_NUMBER = '" + ViewState["Id1"] + "-" + ViewState["Id2"] + "' ";

				}					
				else
				{
					sb.Append(" SELECT ");			
					sb.Append(" (SELECT MAX(PAY_DATE) FROM PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS = SHIPMENT_SELR) ");
					sb.Append(" AS CLOSINGDATE from SHIPMENT WHERE SHIPMENT_ORDR_ID = '" + ViewState["Id1"] + "' AND SHIPMENT_SKU='" + ViewState["Id2"] + "'  ");
					using (SqlDataReader dtrUpdate2 = DBLibrary.GetDataReaderFromSelect(conn,sb.ToString()))
					{
						//Response.Write("Ds update BUYER et < 1 Id1-Id2: "+ViewState["Id1"]+"-"+ViewState["Id2"]+"<br><br>");
						if( dtrUpdate2.Read() )
							SQL = "UPDATE SHIPMENT SET SHIPMENT_SELLER_CLOSED_DATE1= '" + Convert.ToDateTime(dtrUpdate2["CLOSINGDATE"]).ToShortDateString() + "' WHERE (SHIPMENT_ORDR_ID = '" + ViewState["Id1"] + "') AND (SHIPMENT_SKU = '" + ViewState["Id2"] + "')";
					}

					strSql1 = "UPDATE ACCOUNT_PAYABLES SET Transaction_Close = 'true' WHERE TRANSACTION_NUMBER = '" + ViewState["Id1"] + "-" + ViewState["Id2"] + "' ";
					strSql2 = "UPDATE CASH_PAID SET Transaction_Close = 'true' WHERE TRANSACTION_NUMBER = '" + ViewState["Id1"] + "-" + ViewState["Id2"] + "' ";
				}

				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), SQL);
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSql1);
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSql2);
			}
		}

		protected void Write_Off(object sender, EventArgs e)
		{                       
		    //add a flag in the table for WriteOff
		    pnlConfirmationPanel.Visible = true;                                
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
		    string comment = txtWriteOffComment.Text.ToString();

		    string strSQL1 = "UPDATE SHIPMENT SET SHIPMENT_WRITEOFF = 'TRUE' , SHIPMENT_PROBLEMFLAG = 'TRUE', SHIPMENT_WRITEOFF_COMMENT = '" + comment.Trim().ToString() + "', SHIPMENT_WRITEOFF_DATE = '" + System.DateTime.Today.Date + "'  WHERE SHIPMENT_ORDR_ID = '" + ViewState["Id1"].ToString() + "' AND SHIPMENT_SKU = '" + ViewState["Id2"].ToString() + "' ";
		    DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSQL1);

		    String strSQL2 = "INSERT into Shipment_Comment (Comment_Shipment, Comment_PersID, Comment_Date, Comment_Text) values ('" + ViewState["Shipment_Id"].ToString() + "','" + Session["Id"].ToString() + "','" + System.DateTime.Today.Date + "','" + DBLibrary.ScrubSQLStringInput(txtWriteOffComment.Text.Trim()) + "')";
		    DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), strSQL2);

		    Response.Redirect("/Creditor/Edit_Payments.aspx?Receivable=true&ID=" + ViewState["OrderNum"].ToString());            
		}

		protected void btnCancel_Click(object sender, EventArgs e)
		{
		    txtWriteOffComment.Text = "";
		    pnlConfirmationPanel.Visible = false;
		}
	}
}
