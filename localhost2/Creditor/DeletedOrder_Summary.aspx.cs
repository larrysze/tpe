using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using TPE.Utility;

namespace localhost.Creditor
{
    public partial class DeletedOrder_Summary : System.Web.UI.Page
    {
        
        protected System.Web.UI.WebControls.Button Button3;
		protected System.Web.UI.WebControls.Button Button4;
		protected System.Web.UI.WebControls.PlaceHolder phActionButton;
	

		//string for "ODDER BY"
	    //string strOrderBy;
		//string for search function
		
		string bymonth;
		//double TotalSum = 0.0;
		double interest = 0.0;
		double limit = 0.0;

        enum ColHeaders : int
        {
            ID=1,
            ORDR_DTE=2,
            CONTRACT=3,
            WEIGHT=4,
            RCNUMBER=5,
            FROMBUYER=6,
            BUYERPAID=7,
            BUYPRICE=8,
            SELLPRICE=9,
            TOSELLER=10,
            SELLERPAID=11,
            FREIGHT=12,
            WHAREHOUSE=13,
			COMMISION=14,
            BROKER=15,
            VARTERM= 16,
            DAYSPASTDUE=17,
            INTEREST=18,
            FEE=19,
            BUYER=20,
            SELLER=21,
            ShipmentDate=22,
		    PAIDDATE=23
                //case "VARFEE":
                //    iIndex = 17;
                //    break;
                //case "VARBUYR":
                //    iIndex = 18;
                //    break;
                //case "VARSELR":
                //    iIndex = 19;
                //    break;
        }


		protected void Page_Load(object sender, EventArgs e)
		{
            
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C"))
			{
				Response.Redirect("../default.aspx");
			}
            Session["CompID"] = "";

           Master.Width = "1500px";
           //Master.Width = "1350px"; 
			
			if (!IsPostBack)
			{
				// db connection for a list of companies
                SqlConnection conn2;
                conn2 = new SqlConnection(Application["DBConn"].ToString());
                try
                {
                    conn2.Open();
                    SqlCommand cmdCompanies;
                    SqlDataReader dtrCompanies;
                    cmdCompanies = new SqlCommand("SELECT * FROM (SELECT COMP_ID,COMP_NAME,COMP_TYPE FROM COMPANY WHERE COMP_REG=1 AND COMP_TYPE IN ('P','S','D'))Q ORDER BY COMP_NAME", conn2);
                    dtrCompanies = cmdCompanies.ExecuteReader();

                }
                finally
                {
                    conn2.Close();
                }
					
				// setting up date ddl
                CreateDateList();

                ////setting up broker list
                CreateBrokerList();

                CreateIndexList();

                lblSort.Text = "ID DESC";

                Bind(lblSort.Text);
			}
		}

        protected void ButtonClick(Object sender, DataGridCommandEventArgs e)
        {
            //find the order from cancel_shipment
            string shipmentID = null;
            string shipmentOrderID = null;
            string shipmentSKU = null;
            string fieldString = e.Item.Cells[1].Text;            
            shipmentOrderID = fieldString.Substring(0, fieldString.IndexOf("-"));
            shipmentSKU = fieldString.Substring(fieldString.IndexOf("-") + 1, fieldString.Length - fieldString.IndexOf("-")-1 );
            
            SqlConnection conn;
            conn = new SqlConnection(Application["DBConn"].ToString());
            try
            {
                conn.Open();
                SqlCommand cmdShipment;
                SqlDataReader dtrShipment;
                cmdShipment = new SqlCommand("SELECT * FROM CANCELLED_SHIPMENT WHERE SHIPMENT_ORDR_ID = " + Convert.ToInt32(shipmentOrderID) + " AND SHIPMENT_SKU = '" + shipmentSKU + "' ", conn);
                dtrShipment = cmdShipment.ExecuteReader();
                try
				{
					while (dtrShipment.Read())
					{
						shipmentID = dtrShipment["SHIPMENT_ID"].ToString();
					}
				}                
                finally
                {
                    dtrShipment.Close();
                }
            }
            finally
            {
                conn.Close();
            }

            //copy the order to shipment from cancel_shipment
            //delete the order from cancel_shipment
            //reset the flag at orders to 0
            Hashtable ht = new Hashtable();
            ht.Add("@ShipmentID", shipmentID);
            ht.Add("@OrderID", shipmentOrderID);
            DBLibrary.ExecuteStoredProcedure(Application["DBConn"].ToString(), "spUnDeleteShipment", ht);

            Bind(lblSort.Text);
        }
		
	/*	protected void ShowAll(object sender, EventArgs e)
		{
			search="";
			bymonth="";
			ddlMonth.SelectedItem.Value="*";
			
			// changing the db page back to 0
			Bind(lblSort.Text);
		}
		
*/
		// <summary>
		//  searches past months to see which months have transactions pending
		// </summary>
        protected void CreateDateList()
        {
		ddlMonth.Items.Clear(); // starts the list out fresh
		HelperFunction.fillDatesDesc(01, 2000, ddlMonth);
		ListItem lstItem = new ListItem("Show All Dates", "*", true);
		ddlMonth.Items.Add(lstItem);
		ddlMonth.SelectedItem.Selected = false;
		ddlMonth.Items[0].Selected = true;

	    //ddlMonth.Items.Clear(); // starts the list out fresh
	    //DateTime moment = DateTime.Now;
	    ////start out with today's dates
	    //int iYY = moment.Year;
	    //int iMM = moment.Month;
	    //string strCompSQL = ""; // holds the syntax of the company should it be needed

	    //SqlConnection conn2;
	    //conn2 = new SqlConnection(Application["DBConn"].ToString());
	    //try
	    //{
	    //    conn2.Open();
	    //    SqlCommand cmdMonth;
	    //    SqlDataReader dtrMonth;

	    //    while (iYY >= 2000)
	    //    {

	    //        cmdMonth = new SqlCommand("SELECT TOP 1 LEFT(CONVERT(varchar,ORDR_DATE,107),3) + ' ' + Right(CONVERT(varchar,ORDR_DATE,107),4) AS Date FROM ORDERS,SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID AND Month(ORDR_DATE) =@Month AND Year(ORDR_DATE) = @Year AND SHIPMENT_BUYR IS NOT NULL" + strCompSQL, conn2);
	    //        cmdMonth.Parameters.Add("@Month", iMM);
	    //        cmdMonth.Parameters.Add("@Year", iYY);
	    //        dtrMonth = cmdMonth.ExecuteReader();
	    //        try
	    //        {
	    //            if (dtrMonth.HasRows)
	    //            {
	    //                dtrMonth.Read();
	    //                ListItem LIdate = new ListItem(dtrMonth["Date"].ToString(), iMM.ToString() + "-" + iYY.ToString());
	    //                if ((string)Request.QueryString["ShowDate"] == iMM.ToString() + "-" + iYY.ToString())
	    //                {
	    //                    LIdate.Selected = true;
	    //                }
	    //                ddlMonth.Items.Add(LIdate);
	    //            }
	    //        }
	    //        finally
	    //        {
	    //            dtrMonth.Close();
	    //        }

	    //        iMM--;
	    //        if (iMM == 0)
	    //        {
	    //            iMM = 12;
	    //            iYY--;
	    //        }

	    //    }
	    //    ListItem lstItem = new ListItem("Show All Dates", "*");
	    //    //lstItem.Selected = true;
	    //    ddlMonth.Items.Add(lstItem);
	    //}
	    //finally
	    //{
	    //    conn2.Close();
	    //}

        }

		// <summar
		// Updated the dbase page of the grid
		// </summary>
		protected void dg_PageIndexChanged(object sender,DataGridPageChangedEventArgs e)
		{
			dg.CurrentPageIndex = e.NewPageIndex;
			// rebind grid
            Bind(lblSort.Text);
		}
    

		// <summary>
		// Binds the content data.  This is always called after strOrderBy has been determined
		// </summary>
        protected void Bind(string strSort)
		{    
            interest = HelperFunction.getInterestBankParameter(this.Context);
            limit = HelperFunction.getInterestLimitParameter(this.Context);

            StringBuilder sbSql = new StringBuilder();
            sbSql.Append(GetSQL());

            if (!ddlMonth.SelectedItem.Value.Equals("*") || (txtSearch.Text != ""))
            {
                sbSql.Append(" WHERE ORDR_DATE > ='12/1/97' ");  // random date prior to the company founding.  this will always be true
            }


            if (!ddlMonth.SelectedItem.Value.Equals("*"))
            {
                string strDate = ddlMonth.SelectedItem.Value.ToString();
                bymonth += " AND Month(ORDR_DATE)='" + strDate.Substring(4,2) + "'";
                bymonth += " AND Year(ORDR_DATE)='" + strDate.Substring(0,4) + "'";

                sbSql.Append(bymonth);
            }

            if (txtSearch.Text != "")
            {
                string searchString = DBLibrary.ScrubSQLStringInput(txtSearch.Text);
                string search = "";
                char[] sep = new Char[1];
                sep[0] = '-';
                String[] aux = searchString.Split(sep);

                if (!ddlIndex.SelectedValue.Equals("*"))
                {
                    if (ddlIndex.SelectedValue.Equals("1"))
                        search = search + " SHIPMENT_ORDR_ID LIKE '%" + aux[0] + "%'";
                    if (ddlIndex.SelectedValue.Equals("2"))
                        search = search + " SHIPMENT_PO_NUM LIKE '%" + searchString + "%'";
                    if (ddlIndex.SelectedValue.Equals("3"))
                        search = search + " SHIPMENT_FOB LIKE '%" + searchString + "%'";
                    if (ddlIndex.SelectedValue.Equals("4"))
                        search = search + " VARBUYR LIKE '%" + searchString + "%'";
                    if (ddlIndex.SelectedValue.Equals("5"))
                        search = search + " VARSELR LIKE '%" + searchString + "%'";
                    if (ddlIndex.SelectedValue.Equals("6"))
                        search = search + " VARFREIGHT LIKE '%" + searchString + "%'";
                    if (ddlIndex.SelectedValue.Equals("7"))
                        search = search + " VARWHAREHOUSE LIKE '%" + searchString + "%'";
                    if (ddlIndex.SelectedValue.Equals("8"))
                        search = search + " VARBROKER LIKE '%" + searchString + "%'";
                    if (ddlIndex.SelectedValue.Equals("9"))
                        search = search + " shipment_comment LIKE '%" + searchString + "%'";
                    if (ddlIndex.SelectedValue.Equals("10"))
                        search = search + " ORDR_DATE = '" + searchString + "'";
                }
                else
                {
                    search = search + "  SHIPMENT_ORDR_ID LIKE '%" + aux[0] + "%'";
                    search = search + " OR VARNETDUEBUYR LIKE '%" + searchString + "%'";
                    search = search + " OR VARNETDUESELR LIKE '%" + searchString + "%'";
                    search = search + " OR VAROWESBUYR LIKE '%" + searchString + "%'";
                    search = search + " OR VAROWESSELR LIKE '%" + searchString + "%'";
                    search = search + " OR VARTVBUYR LIKE '%" + searchString + "%'";
                    search = search + " OR VARTVSELR LIKE '%" + searchString + "%'";
                    search = search + " OR VARGROSS LIKE '%" + searchString + "%'";
                    search = search + " OR VARSPAID LIKE '%" + searchString + "%'";
                    search = search + " OR VARPPAID LIKE '%" + searchString + "%'";
                    search = search + " OR VARBUYR LIKE '%" + searchString + "%'";
                    search = search + " OR VARSELR LIKE '%" + searchString + "%'";
                    search = search + " OR SHIPMENT_FOB LIKE '%" + searchString + "%'";
                    search = search + " OR SHIPMENT_PO_NUM LIKE '%" + searchString + "%'";
                    search = search + " OR shipment_comment LIKE '%" + searchString + "%'";
                }



                sbSql.Append("AND " + search);
            }


            sbSql.Append(" ORDER BY " + strSort);
            SqlDataAdapter dadContent;
            DataSet dstContent;

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                dadContent = new SqlDataAdapter(sbSql.ToString(), conn);
                dstContent = new DataSet();
                dadContent.Fill(dstContent);

                dg.DataSource = dstContent;
                dg.DataBind();

                if ((dg.Items.Count == 0))
                {
                    dg.Visible = false;
                    lblMessage.Visible = true;
                }
                else
                {
                    dg.Visible = true;
                    lblMessage.Visible = false;
                }
            }
		}
    
		// <summary>
		// Sorting function
		// </summary>
		protected void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{
			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;
    
			//  Parse the sort expression - delimiter space
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
			ColumnToSort = SortExprs[0];
			// If a sort order is specified get it, else default is descending
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DeSC";
			}
    
			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;
    
			// Figure out the column index
			int iIndex;
			iIndex = 0;
			switch(ColumnToSort.ToUpper())
			{
				case "ID":
					iIndex = 1;
					break;
				case "ORDR_DTE":
					iIndex = 2;
					break;
				case "VARCONTRACT":
					iIndex = 3;
					break;
				case "VARWEIGHT":
					iIndex = 4;
					break;
                case "VARRCNUMBER":
                    iIndex = 5;
                    break;
                case "VARTVBUYR":
                    iIndex = 6;
                    break;
                case "VARPPAID":
					iIndex = 7;
					break;
				case "VARBUYPRICE":
					iIndex = 8;
					break;
				case "VARSELLPRICE":
					iIndex = 9;
					break;
				case "VARTVSELR":
					iIndex = 10;
					break;
				case "VARSPAID":
					iIndex = 11;
					break;
				case "VARFREIGHT":
					iIndex = 12;
					break;
                case "VARWHAREHOUSE":
                    iIndex = 13;
                    break;
				case "VARCOMMISION":
					iIndex = 14;
					break;
				case "VARBROKER":
					iIndex = 15;
					break;
                case "VARTERM":
					iIndex = 16;
					break;
				case "VARFEE":
					iIndex = 17;
					break;
				case "VARBUYR":
					iIndex = 20;
					break;
				case "VARSELR":
					iIndex = 21;
					break;

                //case "VARBUYR":
                //    iIndex = 18;
                //    break;
                //case "VARSELR":
                //    iIndex = 19;
                //    break;

			}
			// alter the column's sort expression
			dg.Columns[iIndex].SortExpression = NewSortExpr;
    
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
    
			// store the new SortExpr in a labl for use by page function
//			lblSort.Text =NewSortExpr;
			// Sort the data in new order

            Bind(NewSortExpr);
		}
    
		double dbMargin = 0.0;
		double dbTotalInterest = 0.0;
		double dbTotalCommision = 0.0;
		double dbTotalTVBuyr = 0.0;
		double dbTotalBuyrPaid = 0.0;
		double dbTotalTVSelr = 0.0;
		double dbTotalSelrPaid = 0.0;
		double dbTotalFreight = 0.0;
        double dbTotalWharehouse = 0.0;
		double iTotalWeight = 0;
		int iLinkSetCount = 1;

		protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
            

			// determines if the order is a railcar end of railcar to bulk truck
			int iSKU = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ID")).LastIndexOf("RC1");
			if (  (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
			{
				double dbInterest = 0;
				int DaysPastDue = 0;
				double percentPaid = 0.0;
				DateTime dueDate;
				TimeSpan ts;                

				if (e.Item.Cells[(int)ColHeaders.ShipmentDate].Text != "&nbsp;")
				{
                    dueDate = Convert.ToDateTime(e.Item.Cells[(int)ColHeaders.ShipmentDate].Text).AddDays(Convert.ToInt32(e.Item.Cells[(int)ColHeaders.VARTERM].Text));
					
					percentPaid = Math.Round((Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))/Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR")))*100,2);
					if (percentPaid >= limit) //WY
                        ts = Convert.ToDateTime(e.Item.Cells[(int)ColHeaders.PAIDDATE].Text) - dueDate;
					else
						ts = DateTime.Today - dueDate;
					
					DaysPastDue = ts.Days <= 0 ? 0: ts.Days;

					if (DaysPastDue > 0)
						dbInterest = Math.Round((Convert.ToDouble(DaysPastDue)/ 360.00) * (interest/100) * Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR")),2);//WY
					else
						dbInterest = 0;
				}
				else
				{
					DaysPastDue = 0;
					dbInterest = 0;
				}

                e.Item.Cells[(int)ColHeaders.DAYSPASTDUE].Text = DaysPastDue.ToString() == "0" ? "-" : DaysPastDue.ToString();
                e.Item.Cells[(int)ColHeaders.INTEREST].Text = string.Format("{0:c}", 0);

				if (iSKU ==-1) // railcar in railcar to bulktruck 
				{
                    e.Item.Cells[(int)ColHeaders.INTEREST].Text = string.Format("{0:c}", dbInterest);
                    //e.Item.Cells[(int)ColHeaders.FEE].Text = string.Format("{0:c}", Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARFEE")) - dbInterest);
                    e.Item.Cells[(int)ColHeaders.FEE].Text = "0.00";
					dbTotalInterest += dbInterest;
                    dbMargin=0.00;
					//dbMargin += Convert.ToSingle(Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARFEE")) - dbInterest);
					dbTotalCommision += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARCOMMISION"));
					
                    dbTotalFreight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARFREIGHT"));
                    //dbTotalWharehouse += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARWHAREHOUSE"));
                    dbTotalWharehouse += 0.00;
					iTotalWeight += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARWEIGHT"));
					dbTotalTVBuyr += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR"));
					dbTotalTVSelr += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVSELR"));
				
					dbTotalBuyrPaid += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"));
					dbTotalSelrPaid += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID"));
				}
				else
				{
                    e.Item.Cells[(int)ColHeaders.DAYSPASTDUE].Text = "-";
				}
			
				
				//e.Item.ForeColor = Color.Firebrick;
				if (Math.Abs(Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID"))-Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVSELR"))) > 0.05 && Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID")) >0 )
				{
					e.Item.Cells[11].Text    = "<a href=\"Edit_Payments.aspx?ID="+DataBinder.Eval(e.Item.DataItem, "ID").ToString()+"\"><font color=red>" + String.Format("{0:c}",Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID"))) + "</a></font>";
				}
				if (Math.Abs(Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))-Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARTVBUYR"))) > 0.05 && Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))>0 )
				{
					e.Item.Cells[7].Text    = " <a href=\"Edit_Payments.aspx?Receivable=true&ID="+DataBinder.Eval(e.Item.DataItem, "ID").ToString()+"\"><font color=red>" + String.Format("{0:c}",Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))) + "</a></font>";
				}
				// Any time the Plastics Exchange is involved in a transaction, blank out the buyer/seller paid columns 
				if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VARBUYR"))=="The Plastics...")
				{
					e.Item.Cells[8].Text =" &nbsp;&nbsp;N/A";
					e.Item.Cells[6].Text =" &nbsp;&nbsp;N/A";
					e.Item.Cells[7].Text =" &nbsp;&nbsp;N/A";
					// This displays the amount in the 
					if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARPPAID")) !=0)
					{
						e.Item.Cells[7].Text +="<a href=\"Edit_Payments.aspx?ID="+DataBinder.Eval(e.Item.DataItem, "ID").ToString()+"\"> <font color=red> ($"+Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VARPPAID"))+")</font></a> ";
					}
				}
				if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VARSELR"))=="The Plastics...")
				{
					e.Item.Cells[9].Text =" &nbsp;&nbsp;N/A";
					e.Item.Cells[10].Text =" &nbsp;&nbsp;N/A";
					e.Item.Cells[11].Text =" &nbsp;&nbsp;N/A";
					if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARSPAID")) !=0)
					{
						e.Item.Cells[11].Text +="<a href=\"Edit_Payments.aspx?ID="+DataBinder.Eval(e.Item.DataItem, "ID").ToString()+"\"> <font color=red> ($"+Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VARSPAID"))+")</font></a> ";
					}
				}

				string strActionBoxHTML = "";
				// get Order id
				//Response.Write(e.Item.DataItem.);
				string strID = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ID"));
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]='<div class=\"menuitems\"><a href=\"/administrator/Transaction_Details.aspx?Market=Spot&Referer=/Common/Filled_Orders.aspx&ID="+strID+"\">Edit # "+strID+"</a></div>'" + ((char)13).ToString();

				// remove extra info
				//strID = strID.Substring(0,strID.LastIndexOf("-"));
				
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<hr>' //Optional Separator" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=1\">Invoice</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=2\">Purchase Order</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<hr>' //Optional Separator" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=3\">Certificate</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=4\">Sale Confirmation</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/hauler/Freight_Company.aspx?ID="+strID+"\">Freight R.F.Q</a></div>'" + ((char)13).ToString();
				//strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=6\">Title Transfer</a></div>'" + ((char)13).ToString();
				
				//if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARWGHT")).ToString() == "190000")
				//{
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/common/Convert_Order.aspx?ID="+strID+"\">Convert to 4 BT</a></div>'" + ((char)13).ToString();
				//}
				phActionButton.Controls.Add (new LiteralControl(strActionBoxHTML));


				// setting the mouseover color
				e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");
				if (e.Item.ItemType == ListItemType.Item)
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee'");
				}
				else
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3'");
				}
				// add scripts for 'action boxes'
				e.Item.Cells[0].Attributes.Add("onmouseover","showmenu(event,linkset["+iLinkSetCount.ToString()+"])");
				e.Item.Cells[0].Attributes.Add("onMouseout", "delayhidemenu()");
				//e.Item.Cells[7].Attributes.Add("onmouseover","showmenu(event,linkset["+iLinkSetCount.ToString()+"])");
				//e.Item.Cells[7].Attributes.Add("onMouseout", "delayhidemenu()");

				iLinkSetCount ++;

				
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[1].Text = "<b>Totals:</b> " ;
				e.Item.Cells[4].Text = "<b>"+String.Format("{0:#,###}", iTotalWeight)+"</b>";
				e.Item.Cells[6].Text = "<b>"+String.Format("{0:c}", dbTotalTVBuyr)+"</b>";
				
				e.Item.Cells[7].Text = "<b>"+String.Format("{0:c}", dbTotalBuyrPaid)+"</b>";

				e.Item.Cells[10].Text = "<b>"+String.Format("{0:c}", dbTotalTVSelr)+"</b>";
				
				e.Item.Cells[11].Text = "<b>"+String.Format("{0:c}", dbTotalSelrPaid)+"</b>";

                e.Item.Cells[(int)ColHeaders.FREIGHT].Text = "<b>" + String.Format("{0:c}", dbTotalFreight) + "</b>";
                e.Item.Cells[(int)ColHeaders.WHAREHOUSE].Text = "<b>" + String.Format("{0:c}", dbTotalWharehouse) + "</b>";
                e.Item.Cells[(int)ColHeaders.COMMISION].Text = "<b>" + String.Format("{0:c}", dbTotalCommision) + "</b>";
                e.Item.Cells[(int)ColHeaders.INTEREST].Text = "<b>" + String.Format("{0:c}", dbTotalInterest) + "</b>";
                e.Item.Cells[(int)ColHeaders.FEE].Text = "<b>" + String.Format("{0:c}", dbMargin) + "</b>";
			}
		}
				

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

        protected void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            // if the show all months option is select, user needs to be redirected to a new page

            if (ddlMonth.SelectedItem.Value.Equals("AllMonth"))
            {
                Response.Redirect("/Creditor/Month_Summary.aspx");
            }

            //CreateBrokerList();	// Q: why is this here?? A: was needed to refresh record count which showed in parens after broker name in dropdown ie, John Doe(4) // john doe has 4 transactions 

            // reset dbase page to the first one whether the sorting changes
            dg.CurrentPageIndex = 0;
            //bymonth=bymonth+" WHERE ORDR_ID LIKE '%"+txtSearch.Text+"%'";
            Bind("ID DESC");
        }
		/// <summary>
		/// Handler for when the company ddl changes
		/// </summary>
		/// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

		/*/// <summary>
		/// These functions get call by the Sorting button at the top of the column.
		///  There is probably a more elegant way to do it w/ a switch statement but I didn't
		///  set it up like that.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>@@*/
        protected void btnSearch_Click(object sender, System.EventArgs e)
        {
            dg.CurrentPageIndex = 0;
            // set all drop down lists to the default. Search against everything.
            ddlMonth.SelectedItem.Selected = false;
            ddlMonth.Items.FindByValue("*").Selected = true;
            Bind("ID DESC");
        }

        private void CreateBrokerList()
        {
            string period = "";
            if (!ddlMonth.SelectedItem.Value.Equals("*"))
            {
                string strDate = ddlMonth.SelectedItem.Value.ToString();
                period += " AND MONTH(ORDR_DATE)='" + strDate.Substring(4,2) + "'";
                period += " AND YEAR(ORDR_DATE)='" + strDate.Substring(0,4) + "'";
            }

            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                conn.Open();

                // selecting admins
                string stringSQL = "SELECT P2.PERS_FRST_NAME, P2.PERS_LAST_NAME , P2.PERS_ID, T.QTD " +
                    "FROM PERSON P2, (SELECT COUNT(1) QTD, S.SHIPMENT_BROKER_ID " +
                    "		FROM ORDERS O, SHIPMENT S, COMPANY C, PERSON P " +
                    "		WHERE S.SHIPMENT_BUYR is not null  " +
                    "		AND S.SHIPMENT_ORDR_ID=O.ORDR_ID " +
                    "		AND P.PERS_ID=S.SHIPMENT_BUYR " +
                    "		AND C.COMP_ID=P.PERS_COMP " +
                    "		AND (LEFT(C.COMP_NAME,10)<>'The Plasti' OR (LEFT(C.COMP_NAME,10)='The Plasti' AND SHIPMENT_SKU = 'RC1')) " +
                    period +
                    "		GROUP BY S.SHIPMENT_BROKER_ID " +
                    "		) T " +
                    "WHERE P2.PERS_ID *= T.SHIPMENT_BROKER_ID " +
                    "AND (P2.PERS_TYPE='T' OR P2.PERS_TYPE='B' OR P2.PERS_TYPE='A') " +
                    "ORDER BY T.QTD DESC, P2.PERS_FRST_NAME ";
            }
        }

        protected void CreateIndexList()
        {
            ddlIndex.Items.Clear(); // starts the list out fresh
            ddlIndex.Items.Add(new ListItem("Select Field", "*"));
            ddlIndex.Items.Add(new ListItem("Order ID", "1"));
            ddlIndex.Items.Add(new ListItem("PO number", "2"));
            ddlIndex.Items.Add(new ListItem("Shipment FOB", "3"));
            ddlIndex.Items.Add(new ListItem("Buyer Comp.", "4"));
            ddlIndex.Items.Add(new ListItem("Seller Comp.", "5"));
            ddlIndex.Items.Add(new ListItem("Freight Cost", "6"));
            ddlIndex.Items.Add(new ListItem("Warehouse Cost", "7"));
            ddlIndex.Items.Add(new ListItem("Broker Name", "8"));
            ddlIndex.Items.Add(new ListItem("Railcar #", "9"));
            ddlIndex.Items.Add(new ListItem("Transaction Date", "10"));
        }

        private string GetSQL()
        {

            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("SELECT * FROM (SELECT *,");
            sbSql.Append("DATEADD(d,CAST(VARTERM AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)) As VARNETDUEBUYR,");
            sbSql.Append("DATEADD(d,CAST(SHIPMENT_SELLER_TERMS AS INTEGER),CAST(SHIPMENT_DATE_TAKE AS DATETIME)) AS VARNETDUESELR,");
            sbSql.Append("BPAYMENTS=(SELECT TOP 1 PAY_ORDR FROM CANCELLED_PAYMENT WHERE PAY_ORDR=SHIPMENT_ORDR_ID AND PAY_PERS=SHIPMENT_BUYR),");
            sbSql.Append("SPAYMENTS=(SELECT TOP 1 PAY_ORDR FROM CANCELLED_PAYMENT WHERE PAY_ORDR=SHIPMENT_ORDR_ID AND PAY_PERS=SHIPMENT_SELR),");
            sbSql.Append("VARGROSS=(CASE WHEN VARTVBUYR-VARTVSELR<0 THEN 0 ELSE VARTVBUYR-VARTVSELR END),");
            sbSql.Append("VARHANOVER=VARTVBUYR*0.005,");
            sbSql.Append("VAROWESBUYR=VARTVBUYR,");
            sbSql.Append("VAROWESSELR=VARTVSELR,");
            sbSql.Append("VARCREDIT=VARTVBUYR*0.001,");
            sbSql.Append("VARTPE=VARFEE-VARTVBUYR*0.001 FROM (SELECT *,");
            sbSql.Append("VARPPAIDDATE=ISNULL((SELECT CONVERT(VARCHAR,MAX(PAY_DATE),101) FROM CANCELLED_PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),'-'),");
            sbSql.Append("VARFEE=((SHIPMENT_BUYR_PRCE-SHIPMENT_PRCE)*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-ISNULL(SHIPMENT_SHIP_PRCE_INV,SHIPMENT_SHIP_PRCE_EST) - ISNULL(SHIPMENT_WHAREHOUSE_PRCE_INV,SHIPMENT_WHAREHOUSE_PRCE_EST)- ISNULL(SHIPMENT_MISC_INV,SHIPMENT_MISC_EST) - (SHIPMENT_COMM * ((( SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) * ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-ISNULL(SHIPMENT_SHIP_PRCE_INV,SHIPMENT_SHIP_PRCE_EST) - ISNULL(SHIPMENT_MISC_INV,SHIPMENT_MISC_EST) - ISNULL(SHIPMENT_WHAREHOUSE_PRCE_INV,SHIPMENT_WHAREHOUSE_PRCE_EST))/100),");
            sbSql.Append("VARTVSELR=SHIPMENT_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY) * (100 + ISNULL(SHIPMENT_TAX, 0)) / 100,");
            sbSql.Append("VARSPAID=ISNULL((SELECT SUM(PAY_AMNT) FROM CANCELLED_PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_SELR),0),");
            sbSql.Append("VARFREIGHT = ISNULL(SHIPMENT_SHIP_PRCE_INV,SHIPMENT_SHIP_PRCE_EST),");
            sbSql.Append("VARWHAREHOUSE = ISNULL(SHIPMENT_WHAREHOUSE_PRCE_INV,SHIPMENT_WHAREHOUSE_PRCE_EST),");
            sbSql.Append("VARCOMMISION=ISNULL(SHIPMENT_COMM * ((( SHIPMENT_BUYR_PRCE - SHIPMENT_PRCE) * (ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY))-ISNULL(SHIPMENT_SHIP_PRCE_INV,SHIPMENT_SHIP_PRCE_EST))- ISNULL(SHIPMENT_MISC_INV,SHIPMENT_MISC_EST)-ISNULL(SHIPMENT_WHAREHOUSE_PRCE_INV,SHIPMENT_WHAREHOUSE_PRCE_EST)) / 100, 0),");
            sbSql.Append("VARBROKER=(SELECT PERS_FRST_NAME FROM PERSON WHERE PERS_ID=SHIPMENT_BROKER_ID ),");
            sbSql.Append("VARTERM=ISNULL(SHIPMENT_BUYER_TERMS,30),");
            sbSql.Append("VARBUYPRICE = LEFT(SHIPMENT_BUYR_PRCE,6),");
            sbSql.Append("VARSELLPRICE = LEFT(SHIPMENT_PRCE,6),");
            sbSql.Append("VARPPAID=ISNULL((SELECT SUM(PAY_AMNT) FROM CANCELLED_PAYMENT WHERE PAY_SHIPMENT=SHIPMENT_ID AND PAY_PERS=SHIPMENT_BUYR),0),");
            sbSql.Append("   ORDR_DTE=CONVERT(VARCHAR,ISNULL(SHIPMENT_TRANSACTION_DATE,ORDR_DATE),101),");
            sbSql.Append("	VARRCNUMBER=ISNULL(SHIPMENT_COMMENT, '-'),");
            sbSql.Append("	VAROPEN =(CASE WHEN (SHIPMENT_BUYER_CLOSED_DATE is null OR SHIPMENT_SELLER_CLOSED_DATE1 is null) THEN 'Open' ELSE 'Closed' END  ) ,	");
            sbSql.Append("	CAST(ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR) AS ID,");
            sbSql.Append("   VARSIZE=CAST(SHIPMENT_QTY AS VARCHAR)+' '+(CASE SHIPMENT_SIZE WHEN 1 THEN 'lb' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 44092 THEN 'T.L. Bags' WHEN 42000 THEN 'T.L.' WHEN 45000 THEN 'B.T' WHEN 190000 THEN 'R.C.'  END)+(CASE WHEN SHIPMENT_QTY>1 THEN 's' ELSE ' ' END)+'('+CAST(CAST(SHIPMENT_SIZE AS INTEGER)*SHIPMENT_QTY AS VARCHAR)+' lbs)',");
            sbSql.Append("   VARCONTRACT=LEFT(ISNULL(Shipment_Product_Spot,ISNULL(ORDR_PROD_SPOT,(SELECT CONT_LABL FROM CONTRACT WHERE CONT_ID=ORDR_CONT))),10)+'...',");
            sbSql.Append("   VARNUMFILES = (SELECT COUNT(*) from CANCELLED_UPLOADED_FILES WHERE FILE_SHIPMENT=SHIPMENT_ID),");
            sbSql.Append("   VARPRICE=CAST(SHIPMENT_PRCE+SHIPMENT_SHIP_PRCE_INV+SHIPMENT_MARK+SHIPMENT_COMM AS VARCHAR),");
            sbSql.Append("   VARTVBUYR=((100 + ISNULL(SHIPMENT_TAX, 0)) / 100) * SHIPMENT_BUYR_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY),");
            sbSql.Append("   VARDUE=(CASE WHEN CAST((SHIPMENT_PRCE+SHIPMENT_SHIP_PRCE_INV+SHIPMENT_MARK+SHIPMENT_COMM)*SHIPMENT_WEIGHT AS VARCHAR) IS NULL THEN '-' WHEN ((SHIPMENT_PRCE+SHIPMENT_SHIP_PRCE_INV+SHIPMENT_MARK+SHIPMENT_COMM)*SHIPMENT_WEIGHT)=0 THEN 'paid' ELSE CAST((SHIPMENT_PRCE+SHIPMENT_SHIP_PRCE_EST+SHIPMENT_MARK+SHIPMENT_COMM)*SHIPMENT_WEIGHT AS VARCHAR) END),");
            sbSql.Append("   VARPLACE=(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=SHIPMENT_TO)),");
            sbSql.Append("   VARSELR=(SELECT LEFT(COMP_NAME,12)+'...' FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)),");
            sbSql.Append("   VARBUYR=(SELECT LEFT(COMP_NAME,12)+'...' FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)),");
            sbSql.Append("VARBUYR_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR),");
            sbSql.Append("VARSELR_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR),");
            sbSql.Append("   ORDR_WGHT=(CASE WHEN SHIPMENT_WEIGHT IS NULL THEN 'False' ELSE 'True' END),");
            sbSql.Append("   ORDR_PAYMNT=(CASE SHIPMENT_BUYER_TERMS WHEN 0 THEN 'Cash' WHEN '30' THEN '30 days' WHEN '45' THEN '45 days' WHEN '60' THEN '60 days' WHEN '90' THEN '90 days' END),");
            sbSql.Append("   VARWEIGHT=ISNULL(SHIPMENT_WEIGHT,SHIPMENT_QTY*SHIPMENT_SIZE),");
            sbSql.Append("	INVOICE_SENT = (CASE WHEN SHIPMENT_INVOICE_SENT IS NULL THEN ' ' ELSE '*' END),");
            sbSql.Append("	PO_SENT = (CASE WHEN SHIPMENT_PO_SENT IS NULL THEN ' ' ELSE '*' END),");
            sbSql.Append("   ISNULL(PLAC_LABL, '-') AS VARWAREHOUSE ");
            sbSql.Append(" FROM ORDERS O, CANCELLED_SHIPMENT S, PLACE P WHERE O.ORDR_ID=S.SHIPMENT_ORDR_ID AND P.PLAC_ID=S.SHIPMENT_FROM)Q1)Q2 ");

            return sbSql.ToString();
        }

        //Excel
        protected void btnExportToExcel_Click(object sender, System.EventArgs e)
        {
            //export to excel

            //using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            //{
            //  DataSet Ds = TPE.Utility.DBLibrary.GetDataSetFromStoredProcedure(conn, "spInventory");

            Bind(lblSort.Text);

            dg.Columns[0].Visible = false;
            //dg.Columns[1].Visible = false;

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=DeletedOrder_Summary.xls");

            Response.BufferOutput = true;
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Charset = "";
            //Response.Charset = "UTF-8";
            EnableViewState = false;

            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

            FormatExportedGrid(dg);
            this.ClearControls(dg);
            dg.RenderControl(hw);



            Response.Write(CleanHTML(tw.ToString()));
            Response.End();

            //  ConvertToExcel(Ds, Response);
            //            }



            //TPE.Utility.DBLibrary.BindDataGridWithStoredProcedure(Application["dbConn"].ToString(), this.dg, "spInventory");

            //Response.Clear();
            //Response.Buffer = true;
            //Response.ContentType = "application/vnd.ms-excel";
            //Response.Charset = "";
            //this.EnableViewState = false;
            //System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
            //System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
            //this.ClearControls(dg);
            ////dg.RenderControl(oHtmlTextWriter);
            //Response.Write(oStringWriter.ToString());
            //Response.End();
        }

        public void FormatExportedGrid(DataGrid dg1)
        {
            dg1.BackColor = System.Drawing.Color.White;
        }

        private string CleanHTML(string OutOut)
        {
            //will add some code to fix gridlines
            return OutOut;
        }

        private void ClearControls(Control control)
        {
            for (int i = control.Controls.Count - 1; i >= 0; i--)
            {
                ClearControls(control.Controls[i]);
            }
            if (!(control is TableCell))
            {
                if (control.GetType().GetProperty("SelectedItem") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    try
                    {
                        literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
                    }
                    catch
                    {
                    }
                    control.Parent.Controls.Remove(control);
                }
                else
                    if (control.GetType().GetProperty("Text") != null)
                    {
                        LiteralControl literal = new LiteralControl();
                        control.Parent.Controls.Add(literal);
                        literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);
                        control.Parent.Controls.Remove(control);
                    }
            }
            return;
        }

    }
}
