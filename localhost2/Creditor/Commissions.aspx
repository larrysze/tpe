<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Page language="c#" Codebehind="Commissions.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.Commission" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<%@ Register TagPrefix="mbdb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.DefaultButtons" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="asp" namespace="System.Web.UI" %>


<asp:Content runat="server" ContentPlaceHolderID="cphMain">

        <asp:ScriptManager id="ScriptManager1" runat="Server">
    </asp:ScriptManager>
    
     <asp:UpdatePanel id="UpdatePanel1" runat="Server">
 <ContentTemplate>



<div class="DivTitleBarMenu"><span class="Header Color1 Bold">Commission Payment</span></div>
	<table cellspacing="0" cellpadding="0" width="780" border="0">
		<tr>
			<td>&nbsp;</td>
		</tr>			
		<tr>
			<td align="center">			
			<asp:DataGrid CellSpacing="1" BorderWidth="0" BackColor="#000000" Width="500"  id="dg" runat="server" AutoGenerateColumns="False" OnItemDataBound="KeepRunningSum"
			        ShowFooter="True" CellPadding="2" HorizontalAlign="Center" HeaderStyle-CssClass="DataGridHeader" AlternatingItemStyle-CssClass="DataGridRow_Alternate"
					ItemStyle-CssClass="DataGridRow" CssClass="DataGrid">
					<AlternatingItemStyle CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
                    <ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
                    <HeaderStyle CssClass="LinkNormal OrangeColor"></HeaderStyle>
                    <FooterStyle CssClass="Content Bold Color4 FooterColor"></FooterStyle>
					<Columns>
						<asp:BoundColumn Visible="False" DataField="BROKER_ID" SortExpression="BROKER_ID ASC" HeaderText="Broker ID">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="BROKER_NAME" SortExpression="BROKER_NAME ASC" HeaderText="Broker Name">
							<HeaderStyle Wrap="False"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
						</asp:BoundColumn>
						<asp:TemplateColumn>
							<ItemTemplate>
								Manage Commissions
							</ItemTemplate>
						</asp:TemplateColumn>
					</Columns>
				</asp:DataGrid><br />
				</td>
		</tr>
	</table>

</ContentTemplate>
</asp:UpdatePanel>

	
</asp:Content>
