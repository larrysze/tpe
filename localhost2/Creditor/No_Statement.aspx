<%@ Page language="c#" Codebehind="No_Statement.aspx.cs" AutoEventWireup="True" Inherits="localhost.Creditor.No_Statement" MasterPageFile="~/MasterPages/Menu.Master"%>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
<div style="width:780px">
<br />
	<p align="center" class="Content Bold Color2">No Statement is available for this account.<br />Please make sure that the account has receivables due AND has a headquarters within the system.</p>
	<p align="center"><asp:button CssClass="Content Color2" align="center" id="bReturn" runat="server" Click="bReturn_Click" Text="Return" onclick="bReturn_Click" />
	</p>
	<br />
</div>
</asp:Content>
