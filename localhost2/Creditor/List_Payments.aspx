<%@ Page Language="VB" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Web.Mail" %>
<form runat="server" id="Form">
    <TPE:Template PageTitle="" Runat="Server" />
<%
'Administrator nad creditor have access
IF Session("Typ")<>"C" AND Session("Typ")<>"A"  THEN Response.Redirect ("../default.aspx")
'connection
Dim conn as SqlConnection
Dim cmdContent as SQLCommand
Dim Rec0 as SqlDataReader
conn = new SqlConnection(Application("DBconn").ToString())
conn.Open()
Dim Del
Dim Update
%>
<Script>
function Delete_Payment(ID, PID, AMNT, CID, PTYPE)
{
	var x =window.confirm("Are you sure to want to delete this?");
	if (x) // set delete flag and submit form
	{
		document.Form.Delete.value = ID;
		document.Form.PID.value = PID;
		document.Form.AMNT.value = AMNT;
		document.Form.CID.value = CID;
		document.Form.PTYPE.value = PTYPE;
		document.Form.submit();
	}
}
</script>

<%
'Delete the single payment record
IF Request.Form("Delete")<>"" THEN

	Del="DELETE FROM PAYMENT WHERE PAY_ID="&Request.Form("PID")&" "
	cmdContent= new SqlCommand(Del, conn)
	cmdContent.ExecuteNonQuery()
        response.write(Del)
'***************************************
' ORDR_BUYR_PAID is not used any more in new db 

'	IF Request.Form("PTYPE")="P" THEN
'	Update="UPDATE ORDERS SET ORDR_BUYR_PAID=ORDR_BUYR_PAID-"&Request.Form("AMNT")&" WHERE ORDR_ID="&Request.Form("Delete")&""
'	cmdContent= new SqlCommand(Update, conn)
'	cmdContent.ExecuteNonQuery()
'	End IF
'
'***************************************



'***************************************
' ORDR_SELR_PAID is not used any more in new db 

'	IF Request.Form("PTYPE")="S" THEN
'	Update="UPDATE ORDERS SET ORDR_SELR_PAID=ORDR_SELR_PAID-"&Request.Form("AMNT")&" WHERE ORDR_ID="&Request.Form("Delete")&""
'	cmdContent= new SqlCommand(Update, conn)
'	cmdContent.ExecuteNonQuery()
'	End IF
'	response.write(Update)

'***************************************

Response.Redirect("List_Payments.aspx?Id="&Request.Form("CID")&"")
END IF





Dim Str 'string for SQL

Str = ""
Str = Str+" SELECT "
Str = Str+" 	PAY_ID,"
Str = Str+" 	PAY_ORDR,"
Str = Str+" 	ORDR_ID = (SELECT CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR) FROM SHIPMENT WHERE SHIPMENT_ID = PAY_SHIPMENT),	"
Str = Str+" 	PAY_COMP=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=PAY_PERS)),"
Str = Str+" 	BUYER=(SELECT PERS_FRST_NAME+' '+PERS_LAST_NAME FROM PERSON WHERE PERS_ID=PAY_PERS),"
Str = Str+" 	PAY_AMNT=ISNULL(PAY_AMNT,0),"
Str = Str+" 	PAY_DATE,"
Str = Str+" 	COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=PAY_PERS),"
Str = Str+" 	TYPE=(SELECT PERS_TYPE FROM PERSON WHERE PERS_ID=PAY_PERS),"
Str = Str+" 	PAY_PERS,"
Str = Str+" 	PAY_CMNT,"
Str = Str+" 	PAY_ID"
Str = Str+" FROM PAYMENT "

'limit to a certain company
If Request.QueryString("ID")<>"" Then
Str=Str+" WHERE (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=PAY_PERS)="&Request.QueryString("ID")&""
Else
End IF

'sort the records
IF Request.Form("Column")<>"" Then
		IF Request.Form("Column")="ORDER" THEN
		Str=Str+" ORDER BY PAY_ORDR"
		ElseIF Request.Form("Column")="COMPANY" THEN
		Str=Str+" ORDER BY PAY_COMP"
		ElseIF Request.Form("Column")="BUYER" THEN
		Str=Str+" ORDER BY BUYER"
		ElseIF Request.Form("Column")="AMOUNT" THEN
		Str=Str+" ORDER BY PAY_AMNT"
		ElseIF Request.Form("Column")="DATE" THEN
		Str=Str+" ORDER BY PAY_DATE"
		ElseIF Request.Form("Column")="COMMENT" THEN
		Str=Str+" ORDER BY PAY_CMNT"
		End IF
Else
Str=Str+" ORDER BY PAY_COMP"
End IF

cmdContent= new SqlCommand(Str, conn)
Rec0= cmdContent.ExecuteReader()
%>
<input type=hidden name=Column value='<%=Request.Form("Column")%>'>
<input type=hidden name=Order value='<%=Request.Form("Order")%>'>
<input type=hidden name=Delete value=''>
<input type=hidden name=PID value=''>
<input type=hidden name=AMNT value=''>
<input type=hidden name=CID value=''>
<input type=hidden name=PTYPE value=''>
<table border=0 align=center cellspacing=0 cellpadding=0 bgcolor="#E8E8E8">
    <tr>
        <td bgcolor=#000000 colspan=11 height=1>
		</td>
    </tr>

    <tr>
        <td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
        <td width=3 class="TableHeader"><img src=/images/1x1.gif width=1></td>
        <TD align=center class="TableHeader"><strong><a >Delete</td></td>
        <TD align=center class="TableHeader"><strong><a  href="JavaScript:document.Form.Column.value='ORDER';Form.submit()">Order #</td></td>
        <TD align=center class="TableHeader"><strong><a  href="JavaScript:document.Form.Column.value='COMPANY';Form.submit()">Company</a></strong></td>
        <TD align=center class="TableHeader"><strong><a  href="JavaScript:document.Form.Column.value='BUYER';Form.submit()">&nbsp&nbspUser&nbsp&nbsp</td>
        <TD align=center class="TableHeader"><strong><a  href="JavaScript:document.Form.Column.value='AMOUNT';Form.submit()">&nbsp&nbspAmount Paid&nbsp&nbsp</td>
        <TD align=center class="TableHeader"><strong><a  href="JavaScript:document.Form.Column.value='DATE';Form.submit()">&nbsp&nbspDate&nbsp&nbsp</td>
        <TD align=center class="TableHeader"><strong><a  href="JavaScript:document.Form.Column.value='COMMENT';Form.submit()">&nbsp&nbspComment&nbsp&nbsp</td>

        <td width=3 class="TableHeader"><img src=/images/1x1.gif width=1></td>
        <td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
    </tr>


	<%
	'Each row has different color background
	Dim Total
	Total=0
	Dim bAltColor
	bAltColor = false
	While Rec0.Read()
	'swaps color
	if bAltColor Then
		bAltColor = false
	Else
		bAltColor = true
	End If
	if bAltColor Then Response.Write("<TR bgcolor=white>") Else Response.Write("<TR>")
	%>

        <td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
        <td width=3><img src=/images/1x1.gif width=1></td>

        <td align=center><a href="JavaScript:Delete_Payment(<%=Rec0("PAY_ORDR")%>,<%=Rec0("PAY_ID")%>,<%=Rec0("PAY_AMNT")%>, <%=Rec0("COMP_ID")%>, '<%=Rec0("TYPE")%>' )"><img src=../Pics/Icons/icon_delete.gif border=0 alt="Delete"></a>&nbsp&nbsp</td>


        <td align=center><%=Rec0("ORDR_ID")%>&nbsp&nbsp</td>
        <td align=center><a href=../Administrator/Update_Company_Info.aspx?Id=<%=Rec0("COMP_ID")%>><%=Rec0("PAY_COMP")%></a>&nbsp&nbsp</td>
        <td align=center><a href=../common/MB_Specs_User.aspx?ID=<%=Rec0("PAY_PERS")%>><%=Rec0("BUYER")%> (<%=Rec0("TYPE")%>)</a>&nbsp&nbsp</td>
        <td align=center><%=FormatCurrency(Rec0("PAY_AMNT"))%>&nbsp&nbsp</td>
        <td align=center><%=String.Format("{0:MM/dd/yyyy}",Rec0("PAY_DATE"))  %>&nbsp&nbsp</td>
        <td align=center><%=Rec0("PAY_CMNT")%>&nbsp&nbsp</td>
        <td width=3><img src=/images/1x1.gif width=1></td>
        <td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
	</tr>

	<%
	'Calculate the total amount of payment
	Total=Total+Rec0("PAY_AMNT")
	End While
	Rec0.Close
	%>
    <tr>
        <td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
        <td width=3 class="TableHeader"><img src=/images/1x1.gif width=1></td>
        <td colspan=1 class="TableHeader"><font color=white><b>Total</td>
        <td width=170 class="TableHeader"></td>
        <td colspan=1 class="TableHeader"><font color=white><b></font><img src=/images/1x1.gif height=1></td>
        <td width=140 class="TableHeader"></td>
        <td class="TableHeader"><%=FormatCurrency(Total)%></td>
        <td width=140 class="TableHeader" colspan=2></td>
        <td width=3 class="TableHeader"><img src=/images/1x1.gif width=1></td>
        <td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>

    </tr>
    <tr>
        <td bgcolor=#000000 colspan=11 height=1>
		</td>
    </tr>

</table>
</form>
<center>
<form action=Credit_Management.aspx method=post>
<input type=button class=tpebutton value='CLOSE' onClick='Javascript:history.back()'>

<TPE:Template Footer="true" Runat="Server" />

</form></center>
