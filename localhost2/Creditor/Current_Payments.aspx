<%@ Page Language="C#" Codebehind="Current_Payments.aspx.cs" AutoEventWireup="True"
    Inherits="localhost.Creditor.Current_Payments" MasterPageFile="~/MasterPages/Menu.Master"
    Title="Current Payments" %>

<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphMain">
    <table style="text-align:center;" cellspacing="0" cellpadding="0" border="0" width="100%">
	<tbody> 
	<tr><td><br />Bank AR Report<br /><br /></td></tr>
            <tr>
                <td style="text-align:left; padding-left:250px;">
			<table style="text-align:left;" >
				<tbody>
					<tr>
						<td style="width:300px;">
							<b>
								<asp:RadioButton runat="server" AutoPostBack="true" ID="rbtnWeekReport"  CssClass="Content Bold Color2" Text="Week(Sunday 11:00 PM) CR Report"  GroupName="ReportView" OnCheckedChanged="rbtnWeekReport_CheckedChanged"   />
							</b>
						</td>
					</tr>
					<tr>
						<td style="width:300px;">
							<b>	
								<asp:RadioButton runat="server" AutoPostBack="true" ID="rbtnCurrentPayables"  CssClass="Content Bold Color2" Text="Current Payables" GroupName="ReportView" OnCheckedChanged="rbtnCurrentPayables_CheckedChanged"/>
							</b>
						</td>
						<td style="width:300px;" >
							<b>	
								<asp:RadioButton runat="server" AutoPostBack="true" ID="rbtnCurrentReceivables"  CssClass="Content Bold Color2" Text="Current Receivables" GroupName="ReportView" OnCheckedChanged="rbtnCurrentReceivables_CheckedChanged"/>
							</b>
						</td>
						
					</tr>
					<tr>
						<td style="width:300px;">
							<b>	
								<asp:RadioButton runat="server" AutoPostBack="true" ID="rbtnCurrentReceivablesCi"  CssClass="Content Bold Color2" Text="Current Payables(Commited Inventory)" GroupName="ReportView" OnCheckedChanged="rbtnCurrentReceivablesCi_CheckedChanged"/>
							</b>
						</td>
						<td style="width:300px;">
							<b>	
								<asp:RadioButton runat="server" AutoPostBack="true" ID="rbtnCurrentPayablesCi"  CssClass="Content Bold Color2" Text="Current Payables(Commited Inventory)" GroupName="ReportView" OnCheckedChanged="rbtnCurrentPayablesCi_CheckedChanged"/>
							</b>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
            </tr> 
            <tr>
                <td style="text-align:left; height: 66px; padding-left:250px;" >
			<span class="Header Color2"><b> 			
			</b></span><br />
			<asp:Label ID="lblTimeLine" runat="server" ForeColor="Red" Font-Size="X-Small" ></asp:Label><br /><br />
			<asp:Button runat="server" ID="cmdExportToExcel" Text="Export to Excel"  CssClass="Content Color2" onclick="btnExportToExcel_Click" Width="121px" /><br />
                </td>
            </tr> 
		 <tr>
		    <td style="text-align:center;" >
			<table width="100%" style="text-align:left">
			    <tbody>
				<tr valign="bottom">
					<td colspan="2" align="center">
					    <asp:DataGrid BackColor="#000000" BorderWidth="0" CellSpacing="1" ID="dg" runat="server"
						CssClass="DataGrid"  OnItemDataBound="dg_SumDataBinder" 
						HeaderStyle-CssClass="DataGridHeader" AutoGenerateColumns="False" AllowSorting="True"
						CellPadding="2" ShowFooter="True"
						HorizontalAlign="Center">
						<AlternatingItemStyle CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
						<ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
						<HeaderStyle CssClass="LinkNormal OrangeColor"></HeaderStyle>
						<FooterStyle CssClass="Content Bold Color4 FooterColor"></FooterStyle>
			                        
						<Columns>
							<asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/administrator/Company_Info.aspx?Id={0}"
								DataTextField="COMP_ID" SortExpression="COMP_ID" HeaderText="Cust#" >
								<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
							</asp:HyperLinkColumn>			
							<asp:BoundColumn 
								DataField="COMP_NAME" SortExpression="COMP_NAME" HeaderText="Company Name" >
								<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
							</asp:BoundColumn>			
							<asp:HyperLinkColumn DataTextField="TRANSACTION_NUMBER" SortExpression="TRANSACTION_NUMBER"
								DataNavigateUrlField="TRANSACTION_NUMBER" DataNavigateUrlFormatString="../administrator/Transaction_Details.aspx?Referer=/Creditor/Transaction_Summary.aspx&amp;ID={0}"
								HeaderText="Transaction#"><ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
							</asp:HyperLinkColumn>				
							<asp:BoundColumn DataField="SHIPMENT_DATE" SortExpression="SHIPMENT_DATE" HeaderText="Shipment Date" DataFormatString="{0:MM/dd/yyyy}">
										<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="Outstanding" SortExpression="Outstanding" HeaderText="Amount Due" DataFormatString="{0:c}">
								<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="TOTAL" SortExpression="TOTAL" HeaderText="Amount Received/Paid" DataFormatString="{0:c}">
								<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="NET" SortExpression="NET" HeaderText="Net" DataFormatString="{0:c}">
								<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
							</asp:BoundColumn>				
						</Columns>
					    </asp:DataGrid>
					</td>
				</tr>
			</tbody>
			</table>
		</td>
	    </tr>
        </tbody>
    </table>    
</asp:Content>
