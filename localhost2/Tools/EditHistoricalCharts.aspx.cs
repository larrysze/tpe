using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;
using System.Drawing;
using dotnetCHARTING;
using System.Collections.Specialized;


namespace localhost.Tools
{
    public partial class EditHistoricalCharts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlChartInit();
            drawChartObj("1", "Month");

        }

        private void ddlChartInit()
        {
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtrContract = DBLibrary.GetDataReaderFromSelect(conn, "SELECT CONT_ID,CONT_LABL FROM Contract ORDER BY CONT_ORDR");
                while (dtrContract.Read())
                {
                    ListItem lstrContract = new ListItem(dtrContract["CONT_LABL"].ToString(), dtrContract["CONT_ID"].ToString());
                    ddlChart.Items.Add(lstrContract);
                }
            }
        }

        private void drawChartObj(string strContractID, string strPeriod)
        {
            chartObj = new dotnetCHARTING.Chart();
            chartObj.ChartArea.ClearColors();

            chartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            chartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            chartObj.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            chartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);

            chartObj.XAxis.TimeInterval = TimeInterval.Week;

            



            string fileName2 = "";
            //System.Console.WriteLine("creation bitmap");
            string period1 = "";




//            chartObj.TempDirectory = chartsFolder;

            try
            {
                    chartObj.SeriesCollection.Add(getData(strContractID, strPeriod));


            }
            catch (Exception ex)
            { }
        }

        private SeriesCollection getData(string strContract, string period)
        {
            SqlConnection conn;
            SqlDataReader dtrData;
            SqlCommand cmdData;
            string strSQL;
            conn = new SqlConnection(Application[""].ToString());
            conn.Open();
            int iPreRead = 0; // the number of preloaded items
            int iInterval = 0; // the space between each items

            int range = 1;
            int count = 0;
            string tmpSQL = "";

            switch (period)
            {

                default:
                    strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate()) ";
                    iPreRead = 2;
                    iInterval = 0;
                    range = 4;
                    tmpSQL = "select COUNT(*) from HISTORICAL_PRICES WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate())";
                    cmdData = new SqlCommand(tmpSQL, conn);
                    cmdData.Parameters.AddWithValue("@CONTRACT", strContract);
                    count = Convert.ToInt32(cmdData.ExecuteScalar());

                    break;
                //case "1Y":
                //	strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-1,getdate()) ";
                //	iInterval = 0;
                //	iPreRead = 0;
                //	break;
                case "5Y":
                    strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate()) ";
                    iInterval = 0;
                    iPreRead = 2;
                    range = 4;
                    tmpSQL = "select COUNT(*) from HISTORICAL_PRICES WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate())";
                    cmdData = new SqlCommand(tmpSQL, conn);
                    cmdData.Parameters.AddWithValue("@CONTRACT", strContract);
                    count = Convert.ToInt32(cmdData.ExecuteScalar());

                    break;
                case "10Y":
                    strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-10,getdate()) ";
                    iInterval = 2;
                    iPreRead = 5;
                    range = 10;
                    tmpSQL = "select COUNT(*) from HISTORICAL_PRICES WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-10,getdate())";
                    cmdData = new SqlCommand(tmpSQL, conn);
                    cmdData.Parameters.AddWithValue("@CONTRACT", strContract);
                    count = Convert.ToInt32(cmdData.ExecuteScalar());

                    break;
                case "Forward":
                    strSQL = "Select FWD_ID, DATE= LEFT(FWD_MNTH,3)+' '+RIGHT(FWD_YEAR,2), PRICE = (SELECT RIGHT(CONVERT(varchar,MAX(OFFR_PRCE)),4) FROM OFFER WHERE OFFR_CONT='" + strContract.ToString() + "' AND OFFR_MNTH=FWD_ID AND OFFR_SIZE='1') From FWDMONTH WHERE FWD_ACTV ='1' ORDER BY FWD_ID ASC";
                    break;
            }

            strSQL = "select PRICE,PRICE_DATE AS DATE from HISTORICAL_PRICES " + strSQL + " ORDER BY PRICE_DATE";
            if (period.Equals("1M"))
            {
                // needs to overwrite string completely if it is one month
                strSQL = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(dd,-30,getdate())  ORDER BY DATE";
                iPreRead = 0;
                iInterval = 0;
                range = 4;
                tmpSQL = "select count(*) from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(dd,-30,getdate())";
                cmdData = new SqlCommand(tmpSQL, conn);
                cmdData.Parameters.AddWithValue("@CONTRACT", strContract);
                count = Convert.ToInt32(cmdData.ExecuteScalar());
            }
            //			if (period.Equals("6M"))
            //			{
            //				// needs to overwrite string completely if it is one month
            //				strSQL = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(mm,-6,getdate())  ORDER BY DATE";
            //				iPreRead = 12;
            //				iInterval = 6;
            //			}
            if (period.Equals("1Y"))
            {
                // needs to overwrite string completely if it is one month
                strSQL = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(mm,-12,getdate()) AND DATEPART(Dw,DATE)=DATEPART(Dw, (select max(date) from export_price WHERE CONT_ID=@CONTRACT)) ORDER BY DATE";
                iPreRead = 0; //24;
                iInterval = 0; //6;
                range = 4;

                tmpSQL = "select COUNT(*) from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(mm,-12,getdate()) AND DATEPART(Dw,DATE)=DATEPART(Dw, (select max(date) from export_price WHERE CONT_ID=@CONTRACT))";
                cmdData = new SqlCommand(tmpSQL, conn);
                cmdData.Parameters.AddWithValue("@CONTRACT", strContract);
                count = Convert.ToInt32(cmdData.ExecuteScalar());

            }


            cmdData = new SqlCommand(strSQL, conn);
            cmdData.Parameters.AddWithValue("@CONTRACT", strContract);
            dtrData = cmdData.ExecuteReader();
            SeriesCollection SC = new SeriesCollection();

            for (int k = 0; k < iPreRead; k++)
            {
                if (dtrData.Read())
                { }
            }
            int i = 1;
            int add = 0;

            if (count != 0)
            {
                add = count % range;
            }


            Series s = new Series();

            while (dtrData.Read())
            {
                if (dtrData["DATE"] != DBNull.Value && dtrData["PRICE"] != DBNull.Value)
                {
                    Element e = new Element();
                    //					e.ToolTip = dtrData["PRICE"].ToString();
                    //					e.SmartLabel.DynamicDisplay = false;
                    //					e.SmartLabel.DynamicPosition = true;
                    //					e.ShowValue = false;
                    //e.Name = dtrData["DATE"].ToString();

                    if (((i - add) % range) == 0)
                    {
                        e.Name = Convert.ToDateTime(dtrData["DATE"]).ToString("MMM dd\r\nyyy");
                        //						e.SmartLabel.Text = dtrData["PRICE"].ToString();
                        e.SmartLabel.Color = Color.White;
                        e.XDateTime = (DateTime)dtrData["DATE"];
                    }
                    else
                    {
                        //e.Name = "";
                        e.SmartLabel.Text = "";
                    }

                    i++;


                    //e.Color = Color.Blue;//Color.FromArgb(255, 203, 1);
                    //					e.AxisMarker.Label.Color = Color.Red;

                    e.YValue = Convert.ToDouble(dtrData["PRICE"]);
                    //                    e.Hotspot.ToolTip = Convert.ToDouble(dtrData["PRICE"]).ToString();
                    /* wanted to have 'hand-cursor' while over values
                    e.Hotspot.Attributes.Custom.Add("onmouseover", "this.style.cursor='hand'");
                    e.Hotspot.Attributes.Custom.Add("onmouseout", "this.style.cursor='pointer'");
                    */
                    //                    s.Element.ShowValue = true;
                    s.Elements.Add(e);
                }

                if (period.Equals("6M") || period.Equals("1Y"))// || period.Equals("10Y")
                {
                    for (int k = 0; k < iInterval; k++)
                    {
                        if (dtrData.Read())
                        { }
                    }
                }
            }
            SC.Add(s);

            //chartObj.Depth = 15;
            // Set 3D
            chartObj.Use3D = false;


            chartObj.ChartArea.DefaultElement.Color = Color.Orange;
            chartObj.ChartArea.DefaultSeries.Line.Color = Color.Orange;
            chartObj.ChartArea.DefaultSeries.Line.Width = 2;


            chartObj.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
            chartObj.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            chartObj.ChartArea.YAxis.DefaultTick.Label.Font = new Font(chartObj.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
            chartObj.ChartArea.XAxis.DefaultTick.Label.Font = new Font(chartObj.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);


            chartObj.ChartArea.XAxis.DefaultTick.Line.Color = Color.FromArgb(254, 0, 0); //red
            chartObj.ChartArea.YAxis.DefaultTick.Line.Color = Color.FromArgb(254, 0, 0); //red

            chartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;

            chartObj.ChartArea.XAxis.Line.Width = 4;
            chartObj.ChartArea.YAxis.Line.Width = 2;

            chartObj.YAxis.Interval = 0.01;

            chartObj.ChartArea.XAxis.LabelRotate = true;

            chartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            chartObj.ChartArea.XAxis.TickLabelAngle = 90;

            chartObj.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            chartObj.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);

            // Set a default transparency
            chartObj.DefaultSeries.DefaultElement.Transparency = 20;

            // Set color of axis lines
            Axis AxisObj = new Axis();
            AxisObj.Line.Color = Color.FromArgb(255, 255, 0, 0);

            //            chartObj.ChartArea.XAxis.DefaultTick.Line.Color = Color.FromArgb(254, 165, 3);
            //            chartObj.ChartArea.YAxis.DefaultTick.Label.Color = Color.FromArgb(254, 165, 3);

            //							AxisObj.Line.Color = Color.FromArgb(2, System.Drawing.Color.Red);
            chartObj.DefaultAxis = AxisObj;

            chartObj.MarginLeft = 0;
            chartObj.MarginRight = 0;
            chartObj.MarginTop = 0;
            chartObj.MarginBottom = 0;

            // Set the Default Series Type
            chartObj.DefaultSeries.Type = SeriesType.Line;
            chartObj.LegendBox.Position = LegendBoxPosition.None;

            // Set the y Axis Scale
            chartObj.ChartArea.YAxis.Scale = Scale.Range;

            chartObj.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            chartObj.DefaultSeries.DefaultElement.Marker.Size = 6;
            chartObj.DefaultSeries.DefaultElement.Marker.Color = Color.White;//Color.FromArgb( 255, 255, 153, 9);
            chartObj.XAxis.Label.Color = Color.White;

            //chartObj.XAxis.DefaultTick.Label.Text = "<%Value,mmm>";
            //            chartObj.XAxis.TimeScaleLabels.DayFormatString = "p";
            //			chartObj.YAxis.Minimum = 0.35;
            //			chartObj.YAxis.Maximum = 0.85;
            //            chartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM\r\ndd";
            //            chartObj.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            //            chartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);
            //            chartObj.XAxis.SmartMinorTicks = true;
            //            chartObj.XAxis.TimeInterval = TimeInterval.Month;

            //box around the chartarea
            chartObj.ChartArea.Line.Color = Color.FromArgb(255, 255, 0, 0);//(254, 0, 0)
            chartObj.ChartArea.Line.Width = 2;

            chartObj.ChartArea.Background.Color = Color.FromArgb(0, 0, 0, 0);
            chartObj.ChartArea.Background.Mode = BackgroundMode.Color;
            chartObj.Background.Color = Color.FromArgb(0, 0, 0, 0);

            return (SC);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }
    }
}
