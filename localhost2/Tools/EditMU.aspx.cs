using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Tools
{
    public partial class EditMU : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if( (Session["id"] == null) || (!Session["id"].ToString().Equals("2")))
            //{
            //    Response.Redirect("/default.aspx");
            //}
            
            if (!Page.IsPostBack)
            {
                initDates();
                loadData(ddlIssue.SelectedItem.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                conn.Open();

                DateTime review_date = DateTime.Parse(ddlIssue.SelectedItem.ToString());

                string sqlString = "UPDATE MARKET_UPDATES SET " +
                    " comments=@comments, polypropylene=@polypropylene, polystyrene=@polystyrene, polyethylene=@polyethylene, " +
                    "	pretext_pp=@pretext_pp, pretext_pe=@pretext_pe, pretext_ps=@pretext_ps ";
                sqlString += " WHERE published=1 AND Day(date)=" + review_date.Day + " AND MONTH(date)=" + review_date.Month + " AND YEAR(date)=" + review_date.Year;

                Hashtable param = new Hashtable();
                param.Add("@comments", txtComments.Text);
                param.Add("@polypropylene", txtPolypropylene.Text);
                param.Add("@polystyrene", txtPolystyrene.Text);
                param.Add("@polyethylene", txtPolyethylene.Text);

                param.Add("@pretext_ps", txtPreTextPS.Text);
                param.Add("@pretext_pp", txtPreTextPP.Text);
                param.Add("@pretext_pe", txtPreTextPE.Text);


                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), sqlString, param);


            }
            loadData(ddlIssue.SelectedItem.ToString());


        }

        private void loadData(string strDate)
        {
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                conn.Open();
                string sqlString = "SELECT * FROM MARKET_UPDATES WHERE ";
                if (strDate.Trim() != "")
                {
                    DateTime review_date = DateTime.Parse(strDate);
                    sqlString += " published=1 AND Day(date)=" + review_date.Day + " AND MONTH(date)=" + review_date.Month + " AND YEAR(date)=" + review_date.Year;

                }
                else
                {
                    sqlString += "published <> 1";
                }

                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, sqlString);
                if (dtr.Read())
                {
                    txtPolystyrene.Text = dtr["POLYSTYRENE"].ToString();
                    txtPolyethylene.Text = dtr["POLYETHYLENE"].ToString();
                    txtPolypropylene.Text = dtr["POLYPROPYLENE"].ToString();
                    txtComments.Text = dtr["COMMENTS"].ToString();
                    txtPreTextPE.Text = dtr["pretext_pe"].ToString();
                    txtPreTextPS.Text = dtr["pretext_ps"].ToString();
                    txtPreTextPP.Text = dtr["pretext_pp"].ToString();
                }



            }

        }

        private void initDates()
        {
            string strSql = "SELECT DATENAME(MONTH,date) +  ' ' + DATENAME(DAY,date) + ', ' + DATENAME(YEAR,date) as date, id FROM MARKET_UPDATES WHERE published = 1 ORDER BY id DESC";
            TPE.Utility.DBLibrary.BindDropDownList(Application["DBConn"].ToString(), ddlIssue, strSql, "date", "id");
        }

        protected void ddlIssue_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadData(ddlIssue.SelectedItem.ToString());
        }

      }
   }
   