<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SimpleFillOrder.ascx.cs" Inherits="localhost.Users.SimpleFillOrder" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>


<asp:UpdatePanel runat="server" id="UpdatePanel1">
    <ContentTemplate>
<table cellpadding="0" cellspacing="0" border="0"  align="center" width="100%">                
        <tr align="left">
            <td>
                <table border="0" cellpadding="2" cellspacing="2">
                    <tbody>
                        <tr align="left">
                            <td align="left">
                                <asp:Label cssClass="Content Color2" ID="lbBuyer" Font-Bold="true" runat="server" Text="As Buyer"></asp:Label>
                            </td>     
                            <td>
                                <span class="Header Bold Color2">Filters</span>
                                <asp:DropDownList CssClass="InputForm" ID="ddlMonth" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Change_Month">
                                </asp:DropDownList>
                            </td>               
                        </tr>
                        <tr align="center">                            
                            <td>
                                <asp:Button Height="22"  cssClass="Content Color2" ID="btnInvoice" OnClick="Click_Invoice" runat="server" Text="Invoice"></asp:Button>
                            </td>
                            <td>
                                <asp:Button Height="22" cssClass="Content Color2" ID="btnCert" OnClick="Click_Cert" runat="server" Text="Certificate"></asp:Button>
                            </td>
                            <td>
                                <asp:Button Height="22" cssClass="Content Color2" ID="btnSalesConfirmation" OnClick="Click_SalesConfirmation" runat="server" Text="Sales Confirmation"></asp:Button>
                            </td>                                                          
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnContentBuyer" runat="server">        
                    <asp:DataGrid ID="dgBuyer" runat="server" BackColor="#000000" BorderWidth="1" CellSpacing="1" Width="100%" HorizontalAlign="Center" ShowFooter="True"
                        CellPadding="2" DataKeyField="ORDR_ID" AllowSorting="true" OnSortCommand="SortDG" AutoGenerateColumns="false" OnItemDataBound="KeepRunningSum" >
                        <HeaderStyle CssClass="LinkNormal Bold OrangeColor" HorizontalAlign="Center" />
                        <AlternatingItemStyle CssClass="LinkNormal DarkGray" />
                        <ItemStyle CssClass="LinkNormal LightGray" />
                        <FooterStyle CssClass="Content Bold Color4 FooterColor" />
                        <Columns>
                            <mbrsc:RowSelectorColumn SelectionMode="Single" />
                            <asp:BoundColumn DataField="ORDR_ID" HeaderText="#" ItemStyle-Wrap="false" SortExpression="ORDR_ID" />
                            <asp:BoundColumn DataField="VARSHIP" HeaderText="Status" SortExpression="VARSHIP" />
                            <asp:BoundColumn DataField="VARRCNUMBER" HeaderText="RC#" SortExpression="VARRCNUMBER" />                                
                            <asp:BoundColumn DataField="VARWGHT" DataFormatString="{0:#,###}" HeaderText="Weight(lbs)" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="right" SortExpression="VARWGHT"/>
                            <asp:BoundColumn DataField="VARCONTRACT" HeaderText="Product" ItemStyle-Wrap="false" SortExpression="VARCONTRACT" />
                            <asp:BoundColumn DataField="VARVALUE" HeaderText="Total Value" ItemStyle-Wrap="false" SortExpression="VARVALUE" DataFormatString="{0:c}" ItemStyle-HorizontalAlign="right" />
                            <asp:BoundColumn DataField="VARDATE" HeaderText="Date" SortExpression="ORDR_DATE" />                                  
                        </Columns>
                    </asp:DataGrid>
                </asp:Panel>
                <asp:Panel ID="pnNoContentBuyer" runat="server" Visible="False">
                    <center>
                        There are no transactions recorded in our system
                    </center>
                </asp:Panel>
            </td>
        </tr> 
        <tr align="left">
            <td>
                <table border="0" cellpadding="2" cellspacing="2">
                    <tbody>
                        <tr>
                            <td>
                                <asp:Label cssClass="Content Color2" ID="lbSeller" Font-Bold="true" runat="server" Text="As Seller"></asp:Label>
                            </td>                    
                        </tr>
                        <tr align="center">                            
                            <td>
                                <asp:Button Height="22" cssClass="Content Color2" ID="btnPO" OnClick="Click_PO" runat="server" Text="Purchase Order"></asp:Button>
                            </td>                            
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr> 
        <tr>
            <td>
                <asp:Panel ID="pnContentSeller" runat="server">        
                    <asp:DataGrid ID="dgSeller" runat="server" BackColor="#000000" BorderWidth="1" CellSpacing="1" Width="100%" HorizontalAlign="Center" ShowFooter="True"
                        CellPadding="2" DataKeyField="ORDR_ID" AllowSorting="true" OnSortCommand="SortDG" AutoGenerateColumns="false" OnItemDataBound="KeepRunningSum" >
                        <HeaderStyle CssClass="LinkNormal Bold OrangeColor" HorizontalAlign="Center" />
                        <AlternatingItemStyle CssClass="LinkNormal DarkGray" />
                        <ItemStyle CssClass="LinkNormal LightGray" />
                        <FooterStyle CssClass="Content Bold Color4 FooterColor" />
                        <Columns>
                            <mbrsc:RowSelectorColumn SelectionMode="Single" />
                            <asp:BoundColumn DataField="ORDR_ID" HeaderText="#" ItemStyle-Wrap="false" SortExpression="ORDR_ID" />
                            <asp:BoundColumn DataField="VARSHIP" HeaderText="Status" SortExpression="VARSHIP" />
                            <asp:BoundColumn DataField="VARRCNUMBER" HeaderText="RC#" SortExpression="VARRCNUMBER" />                                
                            <asp:BoundColumn DataField="VARWGHT" DataFormatString="{0:#,###}" HeaderText="Weight(lbs)" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="right" SortExpression="VARWGHT"/>
                            <asp:BoundColumn DataField="VARCONTRACT" HeaderText="Product" ItemStyle-Wrap="false" SortExpression="VARCONTRACT" />
                            <asp:BoundColumn DataField="VARVALUE" HeaderText="Total Value" ItemStyle-Wrap="false" SortExpression="VARVALUE" DataFormatString="{0:c}" ItemStyle-HorizontalAlign="right" />
                            <asp:BoundColumn DataField="VARDATE" HeaderText="Date" SortExpression="ORDR_DATE" />                                  
                        </Columns>
                    </asp:DataGrid>
                </asp:Panel>
                <asp:Panel ID="pnNoContentSeller" runat="server" Visible="False">
                    <center>
                        There are no transactions recorded in our system
                    </center>
                </asp:Panel>
            </td>
        </tr>                      
    </table>
 
     </ContentTemplate>
</asp:UpdatePanel>