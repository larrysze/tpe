<%@ Page Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="UserPortal.aspx.cs" Inherits="localhost.Users.UserPortal" Title="User Portal" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="uc1" TagName="SimpleFillOrder" Src="SimpleFillOrder.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphCssLink" runat="server">
<style type="text/css">
        .WhiteLink a:link {color:#FFFFFF; font-size: 10px;}
        .WhiteLink a:visited {color:#FFFFFF; font-size: 10px;}
        .WhiteLink a:hover {color:#FFFFFF; font-size: 10px;}
        .OrangeLink a:link {color:#FD9D00; font-size: 12px;}
        .OrangeLink a:visited {color:#FD9D00; font-size: 12px;}
        .OrangeLink a:hover {color:#FD9D00; font-size: 12px;}
        .RedLink a:link {color:red; font-size: 12px;}
        .RedLink a:visited {color:red; font-size: 12px;}
        .RedLink a:hover {color:red; font-size: 12px;}
        .LinkNormal { text-align: left;}
        .Content 	{
			    font-family: Verdana, Arial, Helvetica, sans-serif;
			    font-size: 10px;
			    font-style: normal;
			    line-height: normal;
			    font-variant: normal;
			    padding-left: 2px;
			    }
			    
.yui .ajax__tab_header 
{
    font-family:arial,helvetica,clean,sans-serif;
    font-size:small;
    border-bottom:solid 5px #FD9D00;
}
.yui .ajax__tab_header .ajax__tab_outer 
{
    background:url(img/sprite.png) #FFFFFF repeat-x;
    margin:0px 0.16em 0px 0px;
    padding:1px 0px 1px 0px;
    vertical-align:bottom;
    border:solid 1px #a3a3a3;
    border-bottom-width:0px;
}
.yui .ajax__tab_header .ajax__tab_tab
{    
    color:#000;    
    padding:0.35em 0.75em;    
    margin-right:0.01em;
}
.yui .ajax__tab_hover .ajax__tab_outer 
{
    background: url(img/sprite.png) #bfdaff repeat-x left -1300px;
}
.yui .ajax__tab_active .ajax__tab_tab 
{
    color:#fff;
}
.yui .ajax__tab_active .ajax__tab_outer
{
    background:url(img/sprite.png) #FD9D00 repeat-x left -1400px;
}
.yui .ajax__tab_body 
{
    font-family:verdana,tahoma,helvetica;
    font-size:10pt;
    padding:0.25em 0.5em;
    background-color:#ffffff;    
    border:solid 1px #808080;
    border-top-width:0px;
}

    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMain" runat="server">

<asp:ScriptManager runat="server" />
<%--<asp:UpdatePanel runat="server" id="UpdatePanel1">
    <ContentTemplate>--%>

<ajaxToolkit:TabContainer runat="server" ID="tcAccountManagement"   ActiveTabIndex="0" Width="100%" CssClass="yui">

<ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="Transactions" Width="100%">
 <ContentTemplate>    
    <fieldset  style="width:auto">
        <legend>
        <span class="Header Color2 Bold">Fill Orders</span>
            <table cellspacing="0" cellpadding="0" width="100%"  align="center" bgcolor="#dddddd" border="0">                           
                <tr>
                  <td><uc1:SimpleFillOrder id="SimpleFillOrder" runat="server"></uc1:SimpleFillOrder></td>
                </tr>
          </table>
        </legend>
    </fieldset>
 </ContentTemplate>
</ajaxToolkit:TabPanel>
<ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="Offers & Bids" Width="100%" Visible="false">
 <ContentTemplate>    
    <fieldset  style="width:auto">
        <legend>
        <span class="Header Color2 Bold">Offers and Bids</span>
            <table cellspacing="0" cellpadding="0" width="100%"  align="center" bgcolor="#dddddd" border="0">           
                <tr>
                    <td align="left" valign="top">                        
                        <p align="center"><asp:Label runat="server" id="lblWorkingOrders"  Text="Working Orders" /></p>                                 
                            <asp:DataGrid ID="dgOffers" BackColor="#000000" CellSpacing="1" BorderWidth="1" runat="server" 
                                Width="389" HorizontalAlign="Center" CellPadding="2" DataKeyField="varid" AllowSorting="true" 
                                AutoGenerateColumns="false">
                                <HeaderStyle CssClass="LinkNormal Bold OrangeColor" HorizontalAlign="Center" />
                                <AlternatingItemStyle CssClass="LinkNormal DarkGray" />
                                <ItemStyle CssClass="LinkNormal LightGray" />
                                <FooterStyle CssClass="Content Bold Color4 FooterColor" />
                                <Columns>                                
                                    <asp:BoundColumn DataField="varid" HeaderText="Offer #" ItemStyle-Wrap="false" SortExpression="varid ASC" />
                                    <asp:BoundColumn DataField="VARSIZE" HeaderText="Size" SortExpression="VARSHIP ASC" />
                                    <asp:BoundColumn DataField="VARDATE" HeaderText="Date" SortExpression="VARDATE ASC" />                                
                                    <asp:BoundColumn DataField="VAREXPR" HeaderText="Expire" SortExpression="VAREXPR ASC" />                                                    
                                </Columns>
                            </asp:DataGrid>                     
                    </td>
                </tr>
                <tr>
                    <td>
                        <p align="center"><asp:Label runat="server" id="lblWorkingBids"  Text="Working Bids" /></p>                 
                        <asp:DataGrid ID="dgBids" BackColor="#000000" CellSpacing="1" BorderWidth="1" runat="server" 
                            Width="389" HorizontalAlign="Center" CellPadding="2" DataKeyField="varid" AllowSorting="true" 
                            AutoGenerateColumns="false">
                            <HeaderStyle CssClass="LinkNormal Bold OrangeColor" HorizontalAlign="Center" />
                            <AlternatingItemStyle CssClass="LinkNormal DarkGray" />
                            <ItemStyle CssClass="LinkNormal LightGray" />
                            <FooterStyle CssClass="Content Bold Color4 FooterColor" />
                            <Columns>                                
                                <asp:BoundColumn DataField="varid" HeaderText="Bid #" ItemStyle-Wrap="false" SortExpression="varid ASC" />
                                <asp:BoundColumn DataField="VARSIZE" HeaderText="Size" SortExpression="VARSHIP ASC" />
                                <asp:BoundColumn DataField="VARDATE" HeaderText="Date" SortExpression="VARDATE ASC" />                                
                                <asp:BoundColumn DataField="VAREXPR" HeaderText="Expire" SortExpression="VAREXPR ASC" />                                
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>            
            </table>
        </legend>
    </fieldset>
 </ContentTemplate>
</ajaxToolkit:TabPanel>
</ajaxToolkit:TabContainer>

<%--    </ContentTemplate>
</asp:UpdatePanel>
--%>


</asp:Content>
