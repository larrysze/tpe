using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace localhost.Users
{
    public partial class UserPortal : System.Web.UI.Page
    {
        pcwDataShare.pcwdata pcw = new pcwDataShare.pcwdata();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "T") && ((string)Session["Typ"] != "L"))
            {
                Response.Redirect("../default.aspx");
            }
            Master.Width = "850px";         

            
            BindDataGrid();
        }
           

        protected void BindDataGrid()
        {
            Session["Comp_Id"] = "175";

            SqlConnection conn;           
            conn = new SqlConnection(Application["DBConn"].ToString());
            conn.Open();
           

            SqlDataAdapter dadWorking;
            DataSet dstWorking;
            string sSQLWorking = "spSpot_Floor @CompID='" + Session["Comp_Id"] + "',@Sort='VARID DESC',@UserType='S'";

            dadWorking = new SqlDataAdapter(sSQLWorking, conn);
            dstWorking = new DataSet();
            dadWorking.Fill(dstWorking);

            dgOffers.DataSource = dstWorking;
            dgOffers.DataBind();

            sSQLWorking = "spSpot_Floor @CompID='" + Session["Comp_Id"] + "',@Sort='VARID DESC',@UserType='P'";
            dadWorking = new SqlDataAdapter(sSQLWorking, conn);
            dstWorking = new DataSet();
            dadWorking.Fill(dstWorking);

            dgBids.DataSource = dstWorking;
            dgBids.DataBind();

        }

       

    }
}
