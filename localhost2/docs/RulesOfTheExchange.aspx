<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/MasterPages/Template.Master" Title="" %>
<%@ MasterType VirtualPath="~/MasterPages/Template.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphInstructions">
<table cellpadding="5">
        <tr>
            <td width="35">
                <img src='../pics/bullet.gif' /></td>
            <td>
                Set forth below are the policies that govern the trading and operations of The Exchange. Participant, in completing and executing the 
                Participation Application and Agreement, agrees to be subject to and comply with the following Policies.
            </td>
        </tr>
        <tr>
            <td width="35">
                <img src='../pics/bullet.gif' /></td>
            <td>
                These Policies may be amended from time to time by electronic notification to all Participants. 
                Each Participant will be given the opportunity to terminate its participation in The Exchange because of such amendment. 
                Any Participant that does not elect to terminate its participation will be deemed to have accepted the amended terms.
            </td>
        </tr>        
    </table>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphHeading"><span class="Header Color1 Bold">Table of Contents</span></asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
    <td><img alt="" src='../images/icons/arRight.gif' /></td>
    <td align="left"><span class="LinkNormal Bold" style="line-height:20px"><a name="Top" href="#A">Electronic Orders and Information</a></span></td>
</tr>
<tr>
    <td><img alt="" src='../images/icons/arRight.gif' /></td>
    <td align="left"><span class="LinkNormal Bold" style="line-height:20px"><a href="#B">Trade Matching</a></span></td>
</tr>
<tr>
    <td><img alt="" src='../images/icons/arRight.gif' /></td>
    <td align="left"><span class="LinkNormal Bold" style="line-height:20px"><a href="#C">Transportation</a></span></td>
</tr>
<tr>
    <td><img alt="" src='../images/icons/arRight.gif' /></td>
    <td align="left"><span class="LinkNormal Bold" style="line-height:20px"><a href="#D">Right of Rejection</a></span></td>
</tr>
<tr>
    <td><img alt="" src='../images/icons/arRight.gif' /></td>
    <td align="left"><span class="LinkNormal Bold" style="line-height:20px"><a href="#E">Failure to Deliver</a></span></td>
</tr>
<tr>
    <td><img alt="" src='../images/icons/arRight.gif' /></td>
    <td align="left"><span class="LinkNormal Bold" style="line-height:20px"><a href="#F">Dispute Resolution</a></span></td>
</tr>
<tr>
    <td><img alt="" src='../images/icons/arRight.gif' /></td>
    <td align="left"><span class="LinkNormal Bold" style="line-height:20px"><a href="#G">Business Conduct</a></span></td>
</tr>
<tr>
    <td><img alt="" src='../images/icons/arRight.gif' /></td>
    <td align="left"><span class="LinkNormal Bold" style="line-height:20px"><a href="#H">Disclaimer/Limitation of Liability</a></span></td>
</tr>
<tr>
    <td><img alt="" src='../images/icons/arRight.gif' /></td>
    <td align="left"><span class="LinkNormal Bold" style="line-height:20px"><a href="#I">Insolvency and Bankruptcy</a></span></td>
</tr>
<tr>
    <td><img alt="" src='../images/icons/arRight.gif' /></td>
    <td align="left"><span class="LinkNormal Bold" style="line-height:20px"><a href="#J">Emergencies</a></span></td>
</tr>
<tr>
    <td><img alt="" src='../images/icons/arRight.gif' /></td>
    <td align="left"><span class="LinkNormal Bold" style="line-height:20px"><a href="#K">Force Majeure</a></span></td>
</tr>
</table>
<div style="line-height:20px; text-align:left; width:100%">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td class="OrangeColor" valign="middle" align="left" style="width: 650px;"><span class="LinkNormal Bold">&nbsp;Electronic Orders and Information&nbsp;&nbsp;</span></td>
<td class="OrangeColor" valign="middle" align="left" style="width: 107px"><span class="LinkNormal Bold"><a name="A" href="#Top">Back to the Top</a></span></td>
<td class="OrangeColor" valign="middle" align="left"><img alt="" src="../images2/glossary/arrow_top.gif" /></td>
</tr>
</table>
<table cellpadding="0" cellspacing="5" border="0" width="100%">
<tr>
    <td class="Content Color2" style="text-align:Justify">Any orders given by a Participant and any information furnished to a Participant electronically via the Internet shall be subject to the following terms and conditions:
        <ul>
            <li>If an order has been placed through the Internet and a Participant has not received an accurate electronic confirmation of the order or of its execution in a timely manner, the Participant must immediately notify TPE.<br /><br /></li>
            <li>If Participant has received confirmation of an order which Participant did not place or any similar conflicting report, Participant must immediately notify TPE.<br /><br /></li>
            <li>Each Participant must immediately notify TPE if there is unauthorized use of its Password, ID or other security data.<br /><br /></li>
            <li>If a Participant fails to notify TPE in writing when any of the above conditions occur, neither TPE nor any of its employees, agents, affiliates, subsidiaries, or its parent, nor any third parties, can or will have any responsibility or liability to the Participant or to any other person whose claim may arise through the Participant for any claims with respect to the handling, mishandling, or loss of any order. TPE shall not be deemed to have received any order or similar instruction electronically transmitted by Participant until TPE has acknowledged to the Participant that the order or similar instruction has been actually received. Each Participant is fully responsibility for the monitoring of its Account.<br /><br /></li>
            <li>All orders entered on The Exchange shall be good-til-canceled which means that they will be open until they are either 
            canceled or matched.</li>
        </ul> 
    </td>
</tr>
</table>
</div>

<div style="line-height:20px; text-align:left; width:100%">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td class="OrangeColor" valign="middle" align="left" style="width: 650px;"><span class="LinkNormal Bold">&nbsp;Trade Matching&nbsp;&nbsp;</span></td>
<td class="OrangeColor" valign="middle" align="left" style="width: 107px"><span class="LinkNormal Bold"><a name="B" href="#Top">Back to the Top</a></span></td>
<td class="OrangeColor" valign="middle" align="left"><img alt="" src="../images2/glossary/arrow_top.gif" /></td>
</tr>
</table>
<table cellpadding="5" cellspacing="5" border="0" width="100%">
<tr>
    <td class="Content Color2" style="text-align:Justify">
    Participants orders are electronically matched by The Exchange based on price and time of entry, 
    considering freight charges. Upon the matching of a buy and sell order, the transaction becomes a legally binding contract 
    and The Exchange generates an electronic confirmation which is automatically sent to both the buy and sell Participants. 
    TPE, in the absence of such additional delivery instructions, will initiate delivery following Participants default instructions. 
    If necessary, Participants are required to forward any additional delivery instructions to The Exchange immediately following the 
    transaction either via phone at 312.202.0002, e-mail to <span class="LinkNormal"><a href="mailto:Logistics@ThePlasticsExchange.com">Logistics@ThePlasticsExchange.com</a></span> or system message.<br /><br />
    Once a transaction becomes a legally binding contract, both parties to the contract must perform (Seller must deliver, 
    Buyer must take delivery). Netting of transactions is not permitted. TPE reserves the right to mitigate disputed trades in a 
    fair and equitable manner. 
    </td>
</tr>
</table>
</div>

<div style="line-height:20px; text-align:left; width:100%">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td class="OrangeColor" valign="middle" align="left" style="width: 650px;"><span class="LinkNormal Bold">&nbsp;Transportation&nbsp;&nbsp;</span></td>
<td class="OrangeColor" valign="middle" align="left" style="width: 107px"><span class="LinkNormal Bold"><a name="C" href="#Top">Back to the Top</a></span></td>
<td class="OrangeColor" valign="middle" align="left"><img alt="" src="../images2/glossary/arrow_top.gif" /></td>
</tr>
</table>
<table cellpadding="5" cellspacing="5" border="0" width="100%">
<tr>
    <td class="Content Color2" style="text-align:Justify">
     Each Contract includes the provision and cost of delivery. Delivery shall be provided by TPE through its Transportation Partner(s).
     Because transportation is an integral part of each contract, Participant must provide delivery instructions to TPE upon approval 
     as a Participant. Neither the Transportation Partner nor TPE shall be liable for any consequential damages that occur in transit. 
    </td>
</tr>
</table>
</div>

<div style="line-height:20px; text-align:left; width:100%">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td class="OrangeColor" valign="middle" align="left" style="width: 650px;"><span class="LinkNormal Bold">&nbsp;Right of Rejection&nbsp;&nbsp;</span></td>
<td class="OrangeColor" valign="middle" align="left" style="width: 107px"><span class="LinkNormal Bold"><a name="D" href="#Top">Back to the Top</a></span></td>
<td class="OrangeColor" valign="middle" align="left"><img alt="" src="../images2/glossary/arrow_top.gif" /></td>
</tr>
</table>
<table cellpadding="5" cellspacing="5" border="0" width="100%">
<tr>
    <td class="Content Color2" style="text-align:Justify">
        A Participant may only reject delivery of a Contract where the Product delivered does not meet the standardized contract 
        specifications set forth in Appendix II of the Agreement as amended from time to time. The Participant must notify TPE immediately
        of such rejection. TPE, upon such notification, may dispatch an independent party to analyze the Product for compliance with the 
        standardized contract specifications.<br /><br />
        If the Product meets such specifications, Buyer must bear the cost of such independent analysis and must accept delivery.<br /><br />
        If the Product does not meet such specifications, Seller must bear the cost of such independent analysis and the cost of return 
        shipping. Further, the Seller must accept the returned Product and ship to Buyer a Product that meets the standardized contract 
        specifications.<br /><br />
        If Seller is unable to provide such Product, TPE will purchase a substitute Product at the current market price and Seller shall be 
        responsible for all related costs, including any difference in price.<br /><br />
    </td>
</tr>
</table>
</div>

<div style="line-height:20px; text-align:left; width:100%">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td class="OrangeColor" valign="middle" align="left" style="width: 650px;"><span class="LinkNormal Bold">&nbsp;Failure to Deliver&nbsp;&nbsp;</span></td>
<td class="OrangeColor" valign="middle" align="left" style="width: 107px"><span class="LinkNormal Bold"><a name="E" href="#Top">Back to the Top</a></span></td>
<td class="OrangeColor" valign="middle" align="left"><img alt="" src="../images2/glossary/arrow_top.gif" /></td>
</tr>
</table>
<table cellpadding="5" cellspacing="5" border="0" width="100%">
<tr>
    <td class="Content Color2" style="text-align:Justify">
       If a Seller fails to deliver any order pursuant to the terms of the Contract, TPE may purchase the Product at the prevailing 
       market price for the benefit of the Buyer and the Seller shall be responsible for all costs related to such failure, including 
       any price difference. TPE may offset these fees and other payables against payment due to Seller. 
    </td>
</tr>
</table>
</div>

<div style="line-height:20px; text-align:left; width:100%">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td class="OrangeColor" valign="middle" align="left" style="width: 650px;"><span class="LinkNormal Bold">&nbsp;Dispute Resolution&nbsp;&nbsp;</span></td>
<td class="OrangeColor" valign="middle" align="left" style="width: 107px"><span class="LinkNormal Bold"><a name="F" href="#Top">Back to the Top</a></span></td>
<td class="OrangeColor" valign="middle" align="left"><img alt="" src="../images2/glossary/arrow_top.gif" /></td>
</tr>
</table>
<table cellpadding="5" cellspacing="5" border="0" width="100%">
<tr>
    <td class="Content Color2" style="text-align:Justify">
       Each Participant must agree that it will use its best efforts to resolve any dispute that arises between the Participant and 
       TPE and/or B2BExchangeCredit, LLC by participating in a meeting with the management of TPE. If the parties cannot come to an 
       agreement through such meeting, the Participant must agree to submit to binding arbitration that will be conducted by FedNet, 
       Inc&nbsp;<span class="LinkNormal"><a href="#Footer">[1].</a></span> Each Participant must further agree to submit to binding arbitration when a dispute is brought to arbitration that 
       involves the contraparty to any corresponding trade of the Participant and/or B2BExchangeCredit, LLC. 
    </td>
</tr>
</table>
</div>

<div style="line-height:20px; text-align:left; width:100%">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td class="OrangeColor" valign="middle" align="left" style="width: 650px;"><span class="LinkNormal Bold">&nbsp;Business Conduct&nbsp;&nbsp;</span></td>
<td class="OrangeColor" valign="middle" align="left" style="width: 107px"><span class="LinkNormal Bold"><a name="G" href="#Top">Back to the Top</a></span></td>
<td class="OrangeColor" valign="middle" align="left"><img alt="" src="../images2/glossary/arrow_top.gif" /></td>
</tr>
</table>
<table cellpadding="5" cellspacing="5" border="0" width="100%">
<tr>
    <td class="Content Color2" style="text-align:Justify">
      Participants must adhere to the following restrictions and requirements. For violating any of the following, TPE may, in its sole 
      discretion, impose such sanctions against Participant as TPE may deem reasonable and appropriate, including but not limited to fine, 
      suspension, termination or any such other sanction.<br /><br /> 
      <ul>
            <li>Neither a Participant nor any employee of a Participant shall engage in any fraudulent act or to deceive, trick or engage in any 
            scheme to defraud, in connection with or related to any trade on or other activity related to The Exchange. Orders of Contracts 
            entered in The Exchange for the purpose of upsetting the equilibrium of the market and bringing about a condition of 
            demoralization in which prices do not or will not reflect fair market values, are forbidden and any TPE and any employee of a 
            TPE who makes or assists in entering such orders with knowledge of the purpose thereof, or who, with such knowledge shall be a 
            party to assist in carrying out any plan or scheme for the entering of such orders shall be deemed guilty of an act inconsistent 
            with just and equitable principles of trade.<br /><br /></li> 
            <li>Manipulation of the market is prohibited.<br /><br /></li> 
            <li>It shall be an offense to violate any TPE Policy regulating the conduct or business of Participant, or any agreement made with 
            TPE, or to engage in fraud, dishonorable or dishonest conduct, or in conduct or proceedings inconsistent with just and equitable 
            principles of trade, or intentionally default on the delivery of any Contract.<br /><br /></li> 
            <li>It shall be an offense against TPE to make a misstatement of material fact to TPE.<br /><br /></li> 
            <li>Neither a Participant nor any employee of a Participant may use its right of access to The Exchange in any way which would tend 
            to bring disrepute upon the Participant or The Exchange.</li> 
      </ul>
    </td>
</tr>
</table>
</div>

<div style="line-height:20px; text-align:left; width:100%">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td class="OrangeColor" valign="middle" align="left" style="width: 650px;"><span class="LinkNormal Bold">&nbsp;Disclaimer/Limitation of Liability&nbsp;&nbsp;</span></td>
<td class="OrangeColor" valign="middle" align="left" style="width: 107px"><span class="LinkNormal Bold"><a name="H" href="#Top">Back to the Top</a></span></td>
<td class="OrangeColor" valign="middle" align="left"><img alt="" src="../images2/glossary/arrow_top.gif" /></td>
</tr>
</table>
<table cellpadding="5" cellspacing="5" border="0" width="100%">
<tr>
    <td class="Content Color2" style="text-align:Justify">
        TPE shall not be responsible for the accuracy, completeness, or use of any information received by TPE from a Participant. 
        TPE shall not be responsible for any damages caused by communications line failure, unauthorized access, theft, systems failure,
         and other occurrences beyond TPEs control. TPE shall not be responsible for any damages in the event that any order is not received 
         by TPE. TPE shall have no liability if any or all of TPEs systems or the systems of any third party working with TPE goes down or 
         otherwise fails to operate for any period of time. TPE is not liable for a Participants inability to provide such access or for any 
         delay in TPEs providing such access. To the extent that the System utilizes Internet or similar open telecommunication line services 
         to transport data or communications, although TPE will take reasonable security precautions, TPE expressly disclaims any liability 
         for interception of any such data or communications, and TPE shall not be responsible for and TPE makes no warranties regarding, 
         the access, speed or availability of Internet or network services. The use and storage of any information, including without 
         limitation, transaction activity, and any other information or orders available through a Participants right of access to 
         The Exchange (Electronic Access) and all orders placed through the Electronic Access, is at each Participants sole risk and 
         responsibility. <br /><br />THE ELECTRONIC ACCESS IS PROVIDED AT EACH PARTICIPANTS SOLE RISK ON AN AS IS, AS AVAILABLE BASIS. NEITHER TPE 
         NOR ANY ITS DIRECTORS, OFFICERS, EMPLOYEES, AGENTS, CONTRACTORS, AFFILIATES, INFORMATION PROVIDERS, LICENSORS, OR OTHER SUPPLIERS 
         PROVIDING DATA, INFORMATION, ACCESS OR SOFTWARE (COLLECTIVELY, THE TPE PARTIES) MAKES ANY REPRESENTATIONS OR WARRANTIES EXPRESS OR 
         IMPLIED INCLUDING WITHOUT LIMITATION ANY WARRANTY THAT THE ELECTRONIC ACCESS WILL BE UNINTERRUPTED OR ERROR FREE OR MAKES ANY 
         WARRANTIES INCLUDING IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE IN RESPECT OF THE ELECTRONIC ACCESS 
         OR PRODUCTS OBTAINED FROM, THROUGH, OR IN CONNECTION WITH THE ELECTRONIC ACCESS; NOR DO ANY OF THE TPE PARTIES MAKE ANY WARRANTY AS 
         TO THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF SUCH ELECTRONIC ACCESS, OR AS TO THE TIMELINESS, SEQUENCE, ACCURACY, 
         COMPLETENESS, RELIABILITY OR CONTENT OF ANY DATA, INFORMATION, ACCESS OR TRANSACTIONS PROVIDED THROUGH THE ELECTRONIC ACCESS; 
         IN NO EVENT WILL ANY OF THE TPE PARTIES BE LIABLE FOR DIRECT, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES, RESULTING FROM ANY 
         DEFECT IN, OR USE OF, OR INABILITY TO USE THE ELECTRONIC ACCESS (INCLUDING BUT NOT LIMITED TO LOST PROFITS, TRADING LOSSES OR 
         DAMAGES THAT RESULT FROM LOSS OF THE USE OF THE ELECTRONIC ACCESS, INCONVENIENCE OR DELAY), EVEN IF ANY OF THE TPE PARTIES HAVE 
         BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES OR LOSSES.
      
    </td>
</tr>
</table>
</div>

<div style="line-height:20px; text-align:left; width:100%">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td class="OrangeColor" valign="middle" align="left" style="width: 650px;"><span class="LinkNormal Bold">&nbsp;Insolvency and Bankruptcy&nbsp;&nbsp;</span></td>
<td class="OrangeColor" valign="middle" align="left" style="width: 107px"><span class="LinkNormal Bold"><a name="I" href="#Top">Back to the Top</a></span></td>
<td class="OrangeColor" valign="middle" align="left"><img alt="" src="../images2/glossary/arrow_top.gif" /></td>
</tr>
</table>
<table cellpadding="5" cellspacing="5" border="0" width="100%">
<tr>
    <td class="Content Color2" style="text-align:Justify">
     If a Participant fails to perform its contracts or is near insolvency or becomes insolvent, such Participant shall promptly notify
     TPE of such fact. In such event, and even if such Participant fails to give notice to TPE, TPE may terminate or cancel the 
     Participants right of access if it deems it necessary.<br /><br />
     Whenever an order for relief under the United States Bankruptcy Code (hereinafter, the Bankruptcy Code) is entered for a 
     Participant, Participants having Exchange Contracts with the bankrupt Participant may proceed to close the same on The Exchange 
     in accordance with the provisions of this policy. 
    </td>
</tr>
</table>
</div>

<div style="line-height:20px; text-align:left; width:100%">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td class="OrangeColor" valign="middle" align="left" style="width: 650px;"><span class="LinkNormal Bold">&nbsp;Emergencies&nbsp;&nbsp;</span></td>
<td class="OrangeColor" valign="middle" align="left" style="width: 107px"><span class="LinkNormal Bold"><a name="J" href="#Top">Back to the Top</a></span></td>
<td class="OrangeColor" valign="middle" align="left"><img alt="" src="../images2/glossary/arrow_top.gif" /></td>
</tr>
</table>
<table cellpadding="5" cellspacing="5" border="0" width="100%">
<tr>
    <td class="Content Color2" style="text-align:Justify">
      In the event of an Emergency, TPE, may place into immediate effect a temporary emergency policy which may remain in effect for up to thirty (30) Business Days and which may provide for, or may authorize TPE to undertake actions necessary or appropriate to meet the Emergency, including, but not limited to, such actions as: 
        <ul>
        <li>limiting trading to liquidation only, in whole or in part;</li> 
        <li>extending the time of delivery;</li> 
        <li>changing delivery points and/or the means of delivery; </li>
        <li>ordering the liquidation of contracts, the fixing of a settlement price or the reduction in positions; </li>
        <li>extending, limiting or changing hours of trading;</li> 
        <li>suspending trading; or </li>
        <li>modifying or suspending any provision of the Policies of The Exchange.</li>
        <br /><br /></ul>
        If, in the judgment of TPE, the physical functions of The Exchange are, or are threatened to be, severely and adversely 
        affected by a physical emergency, TPE shall have authority to take such action as it deems necessary or appropriate to deal 
        with such physical emergency. Such authorized action shall include, but shall not be limited to, closing The Exchange, delaying 
        the opening of trading in any one or more Contracts and/or suspending trading in or extending trading hours for any one or more 
        Contracts; provided, however, that suspension of trading ordered pursuant to this paragraph shall not continue in effect for more 
        than five (5) Business Days unless TPE approves extending such action.<br /><br />
        In the event such action is taken, thereafter TPE may order restoration of trading on The Exchange or removal of any other 
        restriction heretofore imposed pursuant to this paragraph, upon a determination by TPE that the physical emergency has 
        sufficiently abated to permit the physical functions of The Exchange to continue in an orderly manner.<br /><br />
    </td>
</tr>
</table>
</div>

<div style="line-height:20px; text-align:left; width:100%">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
<td class="OrangeColor" valign="middle" align="left" style="width: 650px;"><span class="LinkNormal Bold">&nbsp;Force Majeure&nbsp;&nbsp;</span></td>
<td class="OrangeColor" valign="middle" align="left" style="width: 107px"><span class="LinkNormal Bold"><a name="K" href="#Top">Back to the Top</a></span></td>
<td class="OrangeColor" valign="middle" align="left"><img alt="" src="../images2/glossary/arrow_top.gif" /></td>
</tr>
</table>
<table cellpadding="5" cellspacing="5" border="0" width="100%">
<tr>
    <td class="Content Color2" style="text-align:Justify">
      If strike, fire, accident, ice, equipment malfunction or other act of God which might otherwise be described as Force Majeure
       results in the failure of seller to deliver on a Contract, The Exchanges obligation to make delivery will be extended daily 
       until such condition of Force Majeure no longer exists.
    </td>
</tr>
</table>
</div>

<div class="Content Color2 Bold" style="width:100%; text-align:Center; font-size:9px"><br />
<a name="Footer">[1]</a> FedNet, Inc., founded in 1998, is a private alternative dispute resolution organization comprised 
exclusively of former federal judges. Its offices are located at The Commons, P.O. Box 470338, Building Two, 8223 Brecksville Road, 
Cleveland, Ohio 44141-0338. <br /><br /></div>
</asp:Content>
