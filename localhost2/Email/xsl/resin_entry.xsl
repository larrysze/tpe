<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:variable name="domain"><xsl:value-of  select="tpe/resin_entry/domain"/></xsl:variable>
<xsl:variable name="link"><xsl:value-of  select="tpe/resin_entry/LinkContact"/></xsl:variable>
<xsl:variable name="ziptrue"><xsl:value-of select="tpe/resin_entry/ziptrue"/></xsl:variable>
<xsl:variable name="offerstrue"><xsl:value-of select="tpe/resin_entry/offerstrue"/></xsl:variable>
<xsl:variable name="domestictrue"><xsl:value-of select="tpe/resin_entry/domestictrue"/></xsl:variable>
<xsl:variable name="internaltrue"><xsl:value-of select="tpe/resin_entry/internaltrue"/></xsl:variable>
<xsl:variable name="melttrue"><xsl:value-of select="tpe/resin_entry/melt"/></xsl:variable>
<xsl:variable name="densitytrue"><xsl:value-of select="tpe/resin_entry/density"/></xsl:variable>
<xsl:variable name="addstrue"><xsl:value-of select="tpe/resin_entry/adds"/></xsl:variable>
<xsl:variable name="deliveredtrue"><xsl:value-of select="tpe/resin_entry/deliveredtrue"/></xsl:variable>
<xsl:include href="template.xsl"/>


<xsl:template match="/">
<xsl:call-template name="header" />

<table width="779" border="0" align="center" cellpadding="0" cellspacing="1" bordercolor="#000000" bgcolor="#000000">
  <tr>
    <td height="227" valign="top" bgcolor="#DBDCD7"><p></p>
      <blockquote  style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; padding-left: 4px">
         <br />
         <p><u><xsl:value-of  select="tpe/resin_entry/request"/> number:<xsl:value-of  select="tpe/resin_entry/OrderNumber"/></u></p>
         <p><b>Confirmation Dialog:</b></p>
         <p>
         <font color='red'><a href="http://{$domain}{$link}"><xsl:value-of  select="tpe/resin_entry/firstname"/>&#160;<xsl:value-of  select="tpe/resin_entry/lastname"/>(<xsl:value-of  select="tpe/resin_entry/company"/>)</a></font>
         <xsl:if  test="$offerstrue = 1">
         offers 
         </xsl:if>
         <xsl:if  test="$offerstrue = 0">
          requests
         </xsl:if>
         <font color='red'><xsl:value-of  select="tpe/resin_entry/quantity"/> &#160;
         <xsl:value-of  select="tpe/resin_entry/sizerequest"/></font> of
         <font color='red'><xsl:value-of  select="tpe/resin_entry/product"/></font> at
        <font color='red'> 
        <xsl:if  test="$domestictrue = 1">
         $0.
         </xsl:if>
         <xsl:if  test="$domestictrue = 0">
          $
         </xsl:if>
         <xsl:value-of  select="tpe/resin_entry/price"/></font>
         <xsl:if  test="$internaltrue = 1">
         and markup of <font color='red'><xsl:value-of  select="tpe/resin_entry/markup"/>.<br /></font>
         </xsl:if>
         <xsl:if  test="$internaltrue = 0">.<br />
         </xsl:if>
         </p>
         <p>
         <xsl:if  test="$melttrue != ''">
         Melt:<font color='red'> <xsl:value-of  select="tpe/resin_entry/melt"/></font>&#x20;
         </xsl:if>
         <xsl:if  test="$densitytrue != ''">
         Density: <font color='red'><xsl:value-of  select="tpe/resin_entry/density"/></font>&#x20; 
         </xsl:if>
         <xsl:if  test="$addstrue != ''">
         Adds:<font color='red'> <xsl:value-of  select="tpe/resin_entry/adds"/></font>
        </xsl:if>
         <xsl:if  test="$deliveredtrue = 1">
        delivered to
        </xsl:if>
        <xsl:if  test="$deliveredtrue = 0">
        out of
        </xsl:if>
        <font color='red'><xsl:value-of  select="tpe/resin_entry/delivered"/></font>.
         </p><br />
       
          <p><b>Market: </b><xsl:value-of  select="tpe/resin_entry/market"/>&#x20;</p> 
          <xsl:if  test="$ziptrue = 1">
          <p><b>Zip Code: </b><xsl:value-of  select="tpe/resin_entry/zip"/></p> 
          </xsl:if>
          <xsl:if  test="$ziptrue = 0">
          <p><b>Port: </b><xsl:value-of  select="tpe/resin_entry/port"/></p> 
          </xsl:if>
          <p><b>User: </b> <xsl:value-of  select="tpe/resin_entry/firstname"/>&#160;<xsl:value-of  select="tpe/resin_entry/lastname"/>   </p> 
         <p><b>Title: </b> <xsl:value-of  select="tpe/resin_entry/title"/> </p>
         <p><b>Company: </b> <xsl:value-of  select="tpe/resin_entry/company"/> </p>
         <p><b> Phone: </b><xsl:value-of  select="tpe/resin_entry/phone"/> </p> 
         <p><b> Email: </b><xsl:value-of  select="tpe/resin_entry/email"/> </p> 
         <p><b> IP Address: </b><xsl:value-of  select="tpe/resin_entry/ip"/> </p>

         <p><b><u> Primarily Interested in</u></b></p>
         <p><b> Size: <xsl:value-of  select="tpe/resin_entry/size"/></b> </p> 
         <p><b>Quality: <xsl:value-of  select="tpe/resin_entry/quality"/></b> </p><br />
         <p><b>Resin Preference:</b></p>

         <xsl:for-each select="tpe/preference">
         <p><xsl:value-of  select="."/></p>
         </xsl:for-each><br />   

         <p><b>Comments: </b><xsl:value-of  select="tpe/resin_entry/comments"/></p>
    </blockquote></td>
  </tr>
</table>

<xsl:call-template name="footer" />

</xsl:template>
</xsl:stylesheet>