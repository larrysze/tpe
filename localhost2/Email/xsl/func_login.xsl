<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:variable name="domain"><xsl:value-of  select="tpe/func_login/domain"/></xsl:variable>
<xsl:variable name="ziptrue"><xsl:value-of select="tpe/func_login/ziptrue"/></xsl:variable>
<xsl:variable name="reftrue"><xsl:value-of select="tpe/func_login/reftrue"/></xsl:variable>
<xsl:include href="template.xsl"/>


<xsl:template match="/">
<xsl:call-template name="header" />

<table width="779" border="0" align="center" cellpadding="0" cellspacing="1" bordercolor="#000000" bgcolor="#000000">
  <tr>
    <td height="227" valign="top" bgcolor="#DBDCD7"><p></p>
      <blockquote  style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; padding-left: 4px">
         <br />
         <p><xsl:value-of  select="tpe/func_login/datetime"/></p>
         <p><b>Market: </b><xsl:value-of  select="tpe/func_login/market"/>&#x20;</p> 
         
         <xsl:if  test="$ziptrue = 1">
         <p><b>Zip Code: </b><xsl:value-of  select="tpe/func_login/zip"/></p> 
         </xsl:if>
         
          <xsl:if  test="$ziptrue = 0">
          <p><b>Port: </b><xsl:value-of  select="tpe/func_login/port"/></p> 
          </xsl:if>
             
         <p><b>User: </b> <xsl:value-of  select="tpe/func_login/firstname"/>&#160;<xsl:value-of  select="tpe/func_login/lastname"/>   </p> 
         <p><b>Title: </b> <xsl:value-of  select="tpe/func_login/title"/> </p>
         <p><b>Company: </b> <xsl:value-of  select="tpe/func_login/company"/> </p>
         <p><b> Phone: </b><xsl:value-of  select="tpe/func_login/phone"/> </p> 
         <p><b> Email: </b><xsl:value-of  select="tpe/func_login/email"/> </p> 
         <p><b> IP Address: </b><xsl:value-of  select="tpe/func_login/ip"/> </p>
         <xsl:if  test="$reftrue = 1">
         <p style="font-family: Verdana, Arial, Helvetica, sans-serif; color: red">Referrer: <xsl:value-of select="tpe/func_login/referrer"/></p>
         </xsl:if>
         <p><b><u> Primarily Interested in</u></b></p>
         <p><b> Size: <xsl:value-of  select="tpe/func_login/size"/></b> </p> 
         <p><b>Quality: <xsl:value-of  select="tpe/func_login/quality"/></b> </p><br />
         <p><b>Resin Preference:</b></p>
         <xsl:for-each select="tpe/preference">
         <p><xsl:value-of  select="."/></p>
         </xsl:for-each><br />   
         <p><b>Comments: </b><xsl:value-of  select="tpe/func_login/comments"/></p>
    </blockquote></td>
  </tr>
</table>

<xsl:call-template name="footer" />

</xsl:template>
</xsl:stylesheet>