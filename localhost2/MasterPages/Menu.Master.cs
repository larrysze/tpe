using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace localhost.MasterPages
{
    public partial class Menu : System.Web.UI.MasterPage
    {
        string _Width = "780px";

        public string Width
        {
            get
            {
                return _Width;
            }
            set
            {
                _Width = value;
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            divBorder.Attributes.Add("style", "width:" + Width + ";" + divBorder.Attributes["style"]);
            createMenu();

            if (Request.UserHostAddress.ToString() == ConfigurationSettings.AppSettings["OfficeIP"].ToString())
            {
                
                divGoogleTrack.Visible = false;
            }
            else
            {
                divGoogleTrack.Visible = true;
            }

            
        }

        private void createMenu()
        {
            string path = "";
            string menu_type = "";
            if (Session["Id"] == null)
            {
                menu_type = "DefaultMenu";
            }
            else
            {
                pnLogin.Visible = false;
                pnLogout.Visible = true;
                lblUser.Text = Session["Name"].ToString();
                //                pnDefault.Visible = false;

                switch (Session["Typ"].ToString())
                {
                    case "A":
                        menu_type = "AdminMenu";
                        // add in right click menu                      
                        pnContextMenu.Visible = true;                        
                        break;

                    case "B":
                        menu_type = "BrokerMenu";
                        break;
                    case "T":
                        menu_type = "BrokerMenu";
                        break;
                    case "P":
                    case "O":
                        menu_type = "PurchaserMenu";
                        break;
                    case "S":
                        menu_type = "SellerMenu";
                        //						pnPurchaserNav.Visible = true;
                        break;
                    case "D":
                        menu_type = "DistributorMenu";
                        break;
                    case "X":
                        menu_type = "SalesMenu";
                        break;
                    case "Demo":
                        menu_type = "PurchaserMenu";
                        break;
                    case "L":
                        menu_type = "LeadMenu";
                        pnContextMenu.Visible = true;
                        break;
		    default: menu_type = "DefaultMenu"; break; //in case of bot(has session id and doesn't have seesion type) - sangwon
                }

                if (Request.Path == "/Administrator/WeeklyMUNotepad.aspx" || Request.Path == "/Administrator/MarketUpdate.aspx")
                {
                    pnContextMenu.Visible = false;
                }
            }


            path = Server.MapPath("/menu/" + menu_type + ".xml");

            DataSet ds = new DataSet();
            //            ds.ReadXml(path);
            //            Menu menu = new Menu();
            //            mnuMain.MenuItemClick += new MenuEventHandler(menu_MenuItemClick);
            XmlDataSource objData = new XmlDataSource();
            objData.DataFile = path;
            objData.XPath = "TPEmenu/*";
            mnuMain.DataSource = objData;
            mnuMain.DataBind();
        }


        protected void Button_Login(object sender, EventArgs e)
        {
            // Saving information into session object to be passed on to the login function
            Session["UserName"] = UserName.Text;
            Session["Password"] = Crypto.Encrypt(Password.Text);
            Response.Redirect("/Common/FUNCLogin.aspx");

        }
    }
}
