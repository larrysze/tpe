using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace localhost.MasterPages
{
    public partial class WIR : System.Web.UI.MasterPage
    {

        string _Width = "780px";

        public string Width
        {
            get
            {
                return _Width;
            }
            set
            {
                _Width = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //divBorder.Attributes.Add("style", "width:" + Width + ";" + divBorder.Attributes["style"]);

            

            if (Request.UserHostAddress.ToString() == ConfigurationSettings.AppSettings["OfficeIP"].ToString())
            {
                
                divGoogleTrack.Visible = false;
            }
            else
            {
                divGoogleTrack.Visible = true;
            }


            
        }
    }
}
