<%@ Page Language="c#" Codebehind="Admin_Forward_Floor.aspx.cs" AutoEventWireup="True" Inherits="localhost.Forward.Admin_Forward_Floor" MasterPageFile="~/MasterPages/Menu.Master" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="asp" namespace="System.Web.UI" %>


<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphJavaScript">

    <script>

    function menuP(n,txt,flag,down)
    // script for adding the raise or lower number in the checkbox by one
    {
	   if(flag==1)
	   {
		  Obj=n
		  wherex=315
		  wherey=60

	  eval(zz+Obj+cc+"<DIV id=Test style=\"position:absolute; left:"+wherex+"px; top:"+wherey+"px; visibility: inherit; overflow: visible\"><img name=P border=0 width=191 src=\"/images/specifications/"+(Obj=='Truc'?"TL":"RC")+"Specs.gif\"></DIV>'");
	   }
	}

    function Trim(Para,Flag)
    {
	   Arr=new Array('0','0','0')
	   for(i=0;i<Para.value.length;i++)
		  if(Para.value.charAt(i)<'0'||Para.value.charAt(i)>'9')
			 break
		  else
			 Arr[i]=Para.value.charAt(i)
	   Val=(i==Para.value.length)?((Arr[0]=='0')?((Arr[1]=='0')?parseInt(Arr[2]):parseInt(Arr[1]+Arr[2])):parseInt(Arr[0]+Arr[1]+Arr[2])):0
	   if(Flag==1)
		  {if(Val<999) Val+=1}
	   else if(Val>0)
		  Val-=1
	   Para.value=("000"+Val).substr(("000"+Val).length-3,3)
    }
     
     function popUp(URL) 
        {          
          window.open(URL, "myWindow", "status = 1, height =600, width = 300, resizable = 0");
        }

    </script>

</asp:Content>


<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphMain">
    <object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" height="155" width="780" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">
        <param name="_cx" value="20638" />
        <param name="_cy" value="4101" />
        <param name="FlashVars" value="" />
        <param name="Movie" value="/swf/contract.swf" />
        <param name="Src" value="/swf/contract.swf" />
        <param name="WMode" value="Transparent" />
        <param name="Play" value="-1" />
        <param name="Loop" value="-1" />
        <param name="Quality" value="High" />
        <param name="SAlign" value="" />
        <param name="Menu" value="-1" />
        <param name="Base" value="" />
        <param name="AllowScriptAccess" value="always" />
        <param name="Scale" value="ShowAll" />
        <param name="DeviceFont" value="0" />
        <param name="EmbedMovie" value="0" />
        <param name="BGColor" value="" />
        <param name="SWRemote" value="" />
        <param name="MovieData" value="" />
        <param name="SeamlessTabbing" value="1" />
        <param name="Profile" value="0" />
        <param name="ProfileAddress" value="" />
        <param name="ProfilePort" value="0" />
        <embed wmode="transparent" src="/swf/contract.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="780" height="155" />                           
    </object>
<script src="/ieupdate.js" type="text/javascript"></script>

        <asp:ScriptManager id="ScriptManager1" runat="Server">
    </asp:ScriptManager>
    
     <asp:UpdatePanel id="UpdatePanel1" runat="Server">
 <ContentTemplate>


<asp:ValidationSummary id="ValidationSummary1" runat="server" HeaderText="There are problems with the following fields. Please correct the errors below."></asp:ValidationSummary> 


<TABLE cellSpacing=0 align=center border=0><TBODY><TR>

<TD class="Header Bold Color4 BackgroundColorBlack" align=left width="50%">Products: <asp:DropDownList id="ddlResinFilter" runat="server" CssClass="InputForm" AutoPostBack="true" OnSelectedIndexChanged="Reset_Grid"></asp:DropDownList> </TD>
<TD class="Header Bold Color4 BackgroundColorBlack" align=left width="50%"><asp:label runat="server" id="lblShipMonth" Text="Ship Month:" /> <asp:DropDownList id="ddlFWDMonth" runat="server" CssClass="InputForm" AutoPostBack="true" OnSelectedIndexChanged="Reset_Grid"></asp:DropDownList> </TD>

<TD>
<asp:UpdateProgress id="UpdateProgress1" runat="server">
                        <ProgressTemplate>  
                            <asp:Image ID="imgPleaseWait" runat="server" ImageUrl="http://www.theplasticexchange.com/images/progressbar_green.gif" />
                        </ProgressTemplate>        
                    </asp:UpdateProgress> 


                    
                    </TD></TR><TR><TD vAlign=top colSpan=2>
                    
                    
                    <asp:DataGrid id="dg" runat="server" Width="550px" ShowFooter="false" EditItemStyle-CssClass="LinkNormal LightGray" OnCancelCommand="DG_Cancel" OnUpdateCommand="DG_Update" OnEditCommand="DG_Edit" DataKeyField="CONT_ID" CellPadding="0"  AutoGenerateColumns="false" HeaderStyle-CssClass="LinkNormal Bold OrangeColor" AlternatingItemStyle-CssClass="LinkNormal DarkGray" ItemStyle-CssClass="LinkNormal LightGray" BorderWidth="0" CellSpacing="1" BackColor="#000000">
                    <Columns>
                        <mbrsc:RowSelectorColumn SelectionMode="Multiple" AllowSelectAll="true" ItemStyle-HorizontalAlign="center" />
                        <asp:EditCommandColumn EditText="Edit" CancelText="Cancel" UpdateText="Update" ItemStyle-CssClass="LinkNormal"/>

                        <asp:TemplateColumn>               
                            <ItemTemplate>
                                <A HREF="javascript:popUp('contractinfo.aspx?gradeid=<%# DataBinder.Eval(Container.DataItem, "Cont_ID") %>')"><asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "Cont_labl") %>'  runat="server"/></A>                                
                            </ItemTemplate> 
                        </asp:TemplateColumn>
                                                
                        <asp:TemplateColumn HeaderText="Truckload">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYBID") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CompareValidator ControlToValidate="txtQTYBID" ErrorMessage="Silly rabit, quantity must be an Integer." Display="none" Operator="DataTypeCheck" Type="Integer" runat="server" />
                                <asp:TextBox ID="txtQTYBID" Size="1" MaxLength="3" Text='<%# DataBinder.Eval(Container, "DataItem.QTYBID") %>' runat="server" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtQTYBID" Display="none" ErrorMessage="Bid quantity can not be blank" />
                                <asp:RangeValidator ControlToValidate="txtQTYBID" runat="server" ErrorMessage="Bid quantity must be greater than one." Display="none" Type="Integer" MinimumValue="1" MaximumValue="100" />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Bid">
                            <ItemTemplate>
                                <asp:Label runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.BID") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CompareValidator ControlToValidate="txtBID" ErrorMessage="Silly rabit, bid must be an Integer." Display="none" Operator="DataTypeCheck" Type="Double" runat="server" />
                                <asp:TextBox Size="1" ID="txtBID" MaxLength="5" Text='<%# DataBinder.Eval(Container, "DataItem.BID") %>' runat="server" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtBID" Display="none" ErrorMessage="Bid price can not be blank" />
                                <asp:RangeValidator ControlToValidate="txtBID" runat="server" ErrorMessage="Invalid bid amount." Display="none" Type="Double" MinimumValue="0" MaximumValue="2.00" />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Ask">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OFFR") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CompareValidator ControlToValidate="txtOFFER" ErrorMessage="Silly rabit, offer must be an Integer." Display="none" Operator="DataTypeCheck" Type="Double" runat="server" />
                                <asp:TextBox Size="1" ID="txtOFFER" MaxLength="5" Text='<%# DataBinder.Eval(Container, "DataItem.OFFR") %>' runat="server" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtOFFER" Display="none" ErrorMessage="Offer price can not be blank" />
                                <asp:RangeValidator ControlToValidate="txtOFFER" runat="server" ErrorMessage="Invalid offer amount." Display="none" Type="Double" MinimumValue="0" MaximumValue="2.00" />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Truckload">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYOFFR") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CompareValidator ControlToValidate="txtQTYOFFER" ErrorMessage="Silly rabit, quantity must be an Integer." Display="none" Operator="DataTypeCheck" Type="Integer" runat="server" />
                                <asp:TextBox ID="txtQTYOFFER" MaxLength="3" Text='<%# DataBinder.Eval(Container, "DataItem.QTYOFFR") %>' runat="server" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtQTYOFFER" Display="none" ErrorMessage="Offer quantity can not be blank" />
                                <asp:RangeValidator ControlToValidate="txtQTYOFFER" runat="server" ErrorMessage="Offer quantity must be greater than one." Display="none" Type="Integer" MinimumValue="1" MaximumValue="100" />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                
                
                 <asp:DataGrid id="dgFuture" runat="server" Width="550px" ShowFooter="false" EditItemStyle-CssClass="LinkNormal LightGray" OnCancelCommand="DG_Cancel" OnUpdateCommand="DG_Update" OnEditCommand="DG_Edit" DataKeyField="CONT_ID" CellPadding="0" AutoGenerateColumns="false" HeaderStyle-CssClass="LinkNormal Bold OrangeColor" AlternatingItemStyle-CssClass="LinkNormal DarkGray" ItemStyle-CssClass="LinkNormal LightGray" BorderWidth="0" CellSpacing="1" BackColor="#000000">
                    <Columns>                        
                        <asp:EditCommandColumn EditText="Edit" CancelText="Cancel" UpdateText="Update" ItemStyle-CssClass="LinkNormal"/>                        
                    <asp:BoundColumn DataField="Month" HeaderText="Month" ReadOnly="True" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left" />     
                                                                   
                        <asp:TemplateColumn HeaderText="Truckload">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYBID") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CompareValidator ControlToValidate="txtQTYBID" ErrorMessage="Silly rabit, quantity must be an Integer." Display="none" Operator="DataTypeCheck" Type="Integer" runat="server" />
                                <asp:TextBox ID="txtQTYBID" Size="1" MaxLength="3" Text='<%# DataBinder.Eval(Container, "DataItem.QTYBID") %>' runat="server" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtQTYBID" Display="none" ErrorMessage="Bid quantity can not be blank" />
                                <asp:RangeValidator ControlToValidate="txtQTYBID" runat="server" ErrorMessage="Bid quantity must be greater than one." Display="none" Type="Integer" MinimumValue="1" MaximumValue="100" />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Bid">
                            <ItemTemplate>
                                <asp:Label runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.BID") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CompareValidator ControlToValidate="txtBID" ErrorMessage="Silly rabit, bid must be an Integer." Display="none" Operator="DataTypeCheck" Type="Double" runat="server" />
                                <asp:TextBox Size="1" ID="txtBID" MaxLength="5" Text='<%# DataBinder.Eval(Container, "DataItem.BID") %>' runat="server" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtBID" Display="none" ErrorMessage="Bid price can not be blank" />
                                <asp:RangeValidator ControlToValidate="txtBID" runat="server" ErrorMessage="Invalid bid amount." Display="none" Type="Double" MinimumValue="0" MaximumValue=".9990" />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Ask">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OFFR") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CompareValidator ControlToValidate="txtOFFER" ErrorMessage="Silly rabit, offer must be an Integer." Display="none" Operator="DataTypeCheck" Type="Double" runat="server" />
                                <asp:TextBox Size="1" ID="txtOFFER" MaxLength="5" Text='<%# DataBinder.Eval(Container, "DataItem.OFFR") %>' runat="server" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtOFFER" Display="none" ErrorMessage="Offer price can not be blank" />
                                <asp:RangeValidator ControlToValidate="txtOFFER" runat="server" ErrorMessage="Invalid offer amount." Display="none" Type="Double" MinimumValue="0" MaximumValue=".9990" />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Truckload">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYOFFR") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:CompareValidator ControlToValidate="txtQTYOFFER" ErrorMessage="Silly rabit, quantity must be an Integer." Display="none" Operator="DataTypeCheck" Type="Integer" runat="server" />
                                <asp:TextBox ID="txtQTYOFFER" MaxLength="3" Text='<%# DataBinder.Eval(Container, "DataItem.QTYOFFR") %>' runat="server" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtQTYOFFER" Display="none" ErrorMessage="Offer quantity can not be blank" />
                                <asp:RangeValidator ControlToValidate="txtQTYOFFER" runat="server" ErrorMessage="Offer quantity must be greater than one." Display="none" Type="Integer" MinimumValue="1" MaximumValue="100" />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        
                        <asp:BoundColumn DataField="Bid_id" HeaderText="BidID" ReadOnly="True" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left" Visible="false" />  
                        <asp:BoundColumn DataField="offr_id" HeaderText="OfferID" ReadOnly="True" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left"  Visible="false" />  
                        <asp:BoundColumn DataField="FWD_ID" HeaderText="FWD_ID" ReadOnly="True" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left"  Visible="false" />                          

                    </Columns>
                </asp:DataGrid>
                
                
        </TD><TD vAlign=top>
                
                <TABLE style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid" cellSpacing=0 cellPadding="1" width=220>
                    <TBODY>
                        <TR>
                            <TD align=center colSpan=2>
                                <asp:DropDownList id="ddlRaiseLower" runat="server" CssClass="InputForm">
                                    <asp:ListItem Value="Raise" Text="Raise" />
                                    <asp:ListItem Value="Lower" Text="Lower" />
                                </asp:DropDownList>
                                 <FONT size=1>selected</FONT> 
                                <asp:DropDownList id="ddlBidOffer" runat="server" CssClass="InputForm">
                                    <asp:ListItem Value="both" Text="both" />
                                    <asp:ListItem Value="0" Text="bids" />
                                    <asp:ListItem Value="1" Text="offers" />
                                </asp:DropDownList> 
                            </TD>
                        </TR>
                        <TR>
                            <TD style="HEIGHT: 24px" align=center colSpan=2>
                                <FONT size=1>by <B>$.</B> </FONT>
                                <asp:TextBox id="UpDownPrc" runat="server" CssClass="InputForm" size="3"></asp:TextBox> 
                                <IMG style="BORDER-TOP-WIDTH: 1px; BORDER-LEFT-WIDTH: 1px; BORDER-LEFT-COLOR: black; BORDER-TOP-COLOR: black; BORDER-BOTTOM: black 1px solid; BORDER-RIGHT-WIDTH: 1px; BORDER-RIGHT-COLOR: black" src="../Images/buttons/white_updn.gif" align=absMiddle useMap="#Map" border=0 /> 
                                <MAP name="Map">
                                    <AREA shape="RECT" coords="0,0,20,9" href="javascript:Trim(document.getElementById('<%= UpDownPrc.ClientID %>'),1)" />
                                    <AREA shape="RECT" coords="0,10,20,20" href="javascript:Trim(document.getElementById('<%= UpDownPrc.ClientID %>'),0)" />
                                </MAP><FONT size=1>per lb. 
                                </FONT>
                            </TD>
                        </TR>
                        <TR>
                            <TD>
                                <FONT size=1>from: </FONT>
                                <asp:DropDownList id="ddlFromMonth" runat="server" CssClass="InputForm"></asp:DropDownList> 
                            </TD>
                            <TD>
                                <FONT size=1>to:</FONT> 
                                <asp:DropDownList id="ddlToMonth" runat="server" CssClass="InputForm"></asp:DropDownList> 
                            </TD>
                        </TR>
                        <TR>
                            <TD align=center colSpan=3>
                                <asp:Button id="Button1" onclick="Update_Price" runat="server" CssClass="Content Color2" Text="Update"></asp:Button> 
                            </TD>
                        </TR>
                    </TBODY>
                </TABLE>
                <TABLE>
                    <TBODY>
                        <TR>
                            <TD>
                                <LI><A class="LinkNormal" href="/docs/userguide.pdf" target="_blank">User Guide</A> <%if (Session["Typ"].ToString() == "S")
                                  {%></LI><LI><A class="LinkNormal" href="/docs/seller.pdf" target="_blank">Seller's Agreement (pdf)</A> </LI><LI><A class="LinkNormal" href="/docs/Seller_Agreement_NewWind.aspx" target="_blank">Seller's Agreement (html)</A> <%}
                                  else if (Session["Typ"].ToString() == "P")
                                  {%></LI><LI><A class="LinkNormal" href="/docs/buyer.pdf" target="_blank">Buyer's Agreement (pdf)</A> </LI><LI><A class="LinkNormal" href="/docs/Buyer_Agreement_NewWind.aspx" target="_blank">Buyer's Agreement (html)</A> <%}%></LI>
                                <LI><A class="LinkNormal" href="javascript:sealWin=window.open(&quot;/docs/Rules_of_the_Exchange4PopUp.aspx&quot;,&quot;win&quot;,'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=805,height=500');void(0)">Rules Of The Exchange</A> </LI>
                            </TD>
                        </TR>
                    </TBODY>
                </TABLE>
            </TD>
        </TR>
    </TBODY>
</TABLE>
                                  
                                  
<br />
   
    <div style="TEXT-ALIGN: left">
    
    <cc1:TabContainer runat="server" ID="tabCharts">
        <cc1:TabPanel runat="server" HeaderText="Forward Curve" ID="ForwardCurve">
            <ContentTemplate> 
                <dotnet:Chart id="Chart" runat="server">
                <TitleBox Position="Left" />            
                    <DefaultLegendBox Padding="4" Visible="True" CornerBottomRight="Cut">
                        <HeaderEntry SortOrder="-1" Visible="False" Value="Value" Name="Name"> 
                            <DividerLine Color="Gray" />
                            <LabelStyle Font="Arial, 8pt, style=Bold" />
                        </HeaderEntry>
                    </DefaultLegendBox>

                    <DefaultTitleBox Visible="True" />

            <DefaultElement>
                <DefaultSubValue>    
                    <Line Color="93, 28, 59" Length="4" StartCap="DiamondAnchor" EndCap="Triangle" />
                </DefaultSubValue>
            </DefaultElement>
        </dotnet:Chart>                                   
            </ContentTemplate>        
        </cc1:TabPanel>
        
        <cc1:TabPanel runat="server" HeaderText="Historical Chart" ID="HistoricalChart">
            <ContentTemplate>
                Contract Month: <asp:DropDownList runat="server" id="ddlHistoricalChartDate"  CssClass="InputForm" AutoPostBack="true"  OnSelectedIndexChanged="Update_HistoricalPriceChart" />
                &nbsp;&nbsp; Length: 
                <asp:DropDownList  runat="server" id="ddlHistoricalChartLength"  CssClass="InputForm" AutoPostBack="true">
                    <asp:ListItem Selected="True" Text="30 Days" value="0" />
                    <asp:listitem text="60 Days" value="1" />
                    <asp:listitem text="180 Days" value="2" />
                    <asp:listitem text="1 Year" value="3" />
                </asp:DropDownList>
                <br />
                <br />
                
                <dotnet:Chart id="ChartObj" runat="server">
                <TitleBox Position="Left" />            
                    <DefaultLegendBox Padding="4" Visible="True" CornerBottomRight="Cut">
                        <HeaderEntry SortOrder="-1" Visible="False" Value="Value" Name="Name"> 
                            <DividerLine Color="Gray" />
                            <LabelStyle Font="Arial, 8pt, style=Bold" />
                        </HeaderEntry>
                    </DefaultLegendBox>

                    <DefaultTitleBox Visible="True" />

            <DefaultElement>
                <DefaultSubValue>    
                    <Line Color="93, 28, 59" Length="4" StartCap="DiamondAnchor" EndCap="Triangle" />
                </DefaultSubValue>
            </DefaultElement>
        </dotnet:Chart>                
            </ContentTemplate>
        </cc1:TabPanel>
    
    </cc1:TabContainer>
    
    
  
    
        
    </div>
    
                                  
</ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>
