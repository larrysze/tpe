using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;

namespace localhost.Forward
{
    public partial class Monomer_Forward_Floor : System.Web.UI.Page
    {
        public void Page_Load(object sender, EventArgs e)
        {
            if ((string)Session["Typ"] == null)
            {
                Response.Redirect("/default.aspx");
            }

            if (!IsPostBack)
            {

                SqlConnection conn;
                conn = new SqlConnection(Application["DBconn"].ToString());
                conn.Open();
                SqlCommand cmdResinFilter;
                SqlDataReader dtrResinFilter;
                cmdResinFilter = new SqlCommand("Select distinct CONT_CATG FROM CONTRACT", conn);
                dtrResinFilter = cmdResinFilter.ExecuteReader();
                ddlResinFilter.Items.Add(new ListItem("Show All", "*"));
                while (dtrResinFilter.Read())
                {
                    ddlResinFilter.Items.Add(new ListItem(dtrResinFilter["CONT_CATG"].ToString()));

                }
                dtrResinFilter.Close();

                SqlCommand cmdFWDMonth;
                SqlDataReader dtrFWDMonth;
                cmdFWDMonth = new SqlCommand("Select FWD_ID,FWD_MNTH,RIGHT(FWD_YEAR,2) AS YEAR FROM FWDMonth WHERE FWD_ACTV='1'", conn);
                dtrFWDMonth = cmdFWDMonth.ExecuteReader();
                while (dtrFWDMonth.Read())
                {
                    ddlFWDMonth.Items.Add(new ListItem(dtrFWDMonth["FWD_MNTH"].ToString() + " " + dtrFWDMonth["YEAR"].ToString(), dtrFWDMonth["FWD_ID"].ToString()));

                }
                dtrFWDMonth.Close();


                conn.Close();

                Data_Bind();

            }
        }
        // <summary>
        //   Handler for resets the data grid
        // </summary>
        public void Reset_Grid(object sender, EventArgs e)
        {
            Data_Bind();
        }
        private void Data_Bind()
        {

            SqlDataAdapter dadContent;
            DataSet dstContent;
            SqlConnection conn;
            conn = new SqlConnection(Application["DBconn"].ToString());
            conn.Open();
            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append("  Select ");
            sbSQL.Append("     CONT_LABL, CONT_ID, ");
            sbSQL.Append("      QTYBID =(SELECT SUM(BID_QTY) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND BID_SIZE='1'), ");
            sbSQL.Append("      BID = (SELECT RIGHT(CONVERT(varchar,MAX(BID_PRCE)),5) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND BID_SIZE='1'), ");
            sbSQL.Append("      OFFR = (SELECT RIGHT(CONVERT(varchar,MAX(OFFR_PRCE)),5) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1'), ");
            sbSQL.Append("      QTYOFFR = (SELECT SUM(OFFR_QTY) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1') ");
            sbSQL.Append("  From CONTRACT ");
            //if (!ddlResinFilter.SelectedItem.Value.ToString().Equals("*"))
            //{
            sbSQL.Append("  WHERE CONT_CATG='Monomer'");
            //}

            sbSQL.Append("  ORDER BY CONT_ORDR ");


            dadContent = new SqlDataAdapter(sbSQL.ToString(), conn);
            dstContent = new DataSet();
            dadContent.Fill(dstContent);
            dg.DataSource = dstContent;
            dg.DataBind();
            conn.Close();

        }
    }
}
