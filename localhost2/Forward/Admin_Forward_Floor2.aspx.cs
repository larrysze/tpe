using System.Data.SqlClient;
using System;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MetaBuilders.WebControls;

namespace localhost.Forward
{
	/// <summary>
	/// Summary description for Admin_Forward_Floor2.
	/// </summary>
	public class Admin_Forward_Floor2 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DropDownList ddlBidOffer;
		protected System.Web.UI.WebControls.DropDownList ddlProduct;
		protected System.Web.UI.WebControls.DropDownList ddlFWDMonth;
		protected System.Web.UI.WebControls.DropDownList ddlRaiseLower;
		protected System.Web.UI.WebControls.DropDownList ddlResinFilter;
		protected System.Web.UI.WebControls.DataGrid dg;
		protected System.Web.UI.WebControls.TextBox UpDownPrc;
		/************************************************************************
	 *   1. File Name       :Administrator\Users_Management.aspx             *
	 *   2. Description     :Exchange user management                        *
	 *   3. Modification Log:                                                *
	 *     Ver No.       Date          Author             Modification       *
	 *   -----------------------------------------------------------------   *
	 *      1.00      2-25-2004      Zach                Comment             *
	 *                                                                       *
	 ************************************************************************/
		public void Page_Load(object sender, EventArgs e)
		{
			if ((string)Session["Typ"] != "A")
			{
				// Response.Redirect("../default.aspx");
			}
			if (!IsPostBack)
			{
				SqlConnection conn;
				conn = new SqlConnection(Application["DBconn"].ToString());
				conn.Open();
    
				// opening contract list
				SqlCommand cmdContract;
				SqlDataReader dtrContract;
    
				cmdContract = new SqlCommand("SELECT CONT_ID,CONT_LABL FROM Contract ORDER BY CONT_ORDR",conn);
				dtrContract = cmdContract.ExecuteReader();
    
				//binding company dd list
				ddlProduct.DataSource = dtrContract;
				ddlProduct.DataTextField= "CONT_LABL";
				ddlProduct.DataValueField= "CONT_ID";
				ddlProduct.DataBind();
				dtrContract.Close();
    
    
				conn.Close();
				Data_Bind();
    
			}
		}
		// <summary>
		//   Handler for resets the data grid
		// </summary>
		public void Reset_Grid(object sender, EventArgs e)
		{
			Data_Bind();
		}
		private void Data_Bind()
		{
    
			SqlDataAdapter dadContent;
			DataSet dstContent;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			StringBuilder sbSQL = new StringBuilder();
    
			sbSQL.Append("  Select FWD_ID, MONTH= FWD_MNTH+' '+RIGHT(FWD_YEAR,2),  ");
			sbSQL.Append("      QTYBID =(SELECT SUM(BID_QTY) FROM BID WHERE BID_CONT='"+ddlProduct.SelectedItem.Value.ToString()+"' AND BID_MNTH=FWD_ID AND BID_SIZE='1'), ");
			sbSQL.Append("      BID = (SELECT RIGHT(CONVERT(varchar,MAX(BID_PRCE)),4) FROM BID WHERE BID_CONT='"+ddlProduct.SelectedItem.Value.ToString()+"' AND BID_MNTH=FWD_ID AND BID_SIZE='1'),  ");
			sbSQL.Append("      OFFR = (SELECT RIGHT(CONVERT(varchar,MAX(OFFR_PRCE)),4) FROM OFFER WHERE OFFR_CONT='"+ddlProduct.SelectedItem.Value.ToString()+"' AND OFFR_MNTH=FWD_ID AND OFFR_SIZE='1'), ");
			sbSQL.Append("      QTYOFFR = (SELECT SUM(OFFR_QTY) FROM OFFER WHERE OFFR_CONT='"+ddlProduct.SelectedItem.Value.ToString()+"' AND OFFR_MNTH=FWD_ID AND OFFR_SIZE='1') ");
			sbSQL.Append("  From FWDMONTH WHERE FWD_ACTV ='1' ");
    
    
    
			//if (!ddlResinFilter.SelectedItem.Value.ToString().Equals("*")){
			//  sbSQL.Append("  WHERE CONT_CATG='"+ddlResinFilter.SelectedItem.Value.ToString()+"'");
			//}
    
			//sbSQL.Append("  ORDER BY CONT_ORDR ");
    
    
			dadContent = new SqlDataAdapter(sbSQL.ToString(),conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dg.DataSource = dstContent;
			dg.DataBind();
			conn.Close();
    
		}
    
		// <summary>
		//   Code to raise and lower the transaction prices.  The script is available only to admnins and the functionality moves the
		//   working orders for mgbuyer/mbseller transperently
		// </summary>
		public void Update_Price(object sender, EventArgs e)
		{
    
			string strUserId;
    
    
			string strChange_Price="";
			string strSQL ="";
			SqlConnection conn;
			SqlCommand cmdChangeRows;
			conn = new SqlConnection(Application["DBconn"].ToString());
    
    
			if (ddlRaiseLower.SelectedItem.Value.ToString().Equals("Lower"))
			{
				strChange_Price="-." + UpDownPrc.Text;
			}
			else
			{
				strChange_Price="+." + UpDownPrc.Text;
			}
			if (ddlBidOffer.SelectedItem.Value.ToString().Equals("0"))
			{ // bids
				strSQL="Update BID SET BID_PRCE = BID_PRCE "+strChange_Price+" WHERE BID_CONT='"+ddlProduct.SelectedItem.Value.ToString()+"' AND BID_SIZE='1' AND BID_MNTH IN (";
			}
			else
			{
				strSQL="Update OFFER SET OFFR_PRCE = OFFR_PRCE "+strChange_Price+" WHERE OFFR_CONT='"+ddlProduct.SelectedItem.Value.ToString()+"' AND OFFR_SIZE='1' AND OFFR_MNTH IN (";
			}
    
    
			RowSelectorColumn rsc;
			rsc = RowSelectorColumn.FindColumn(dg);
			if (rsc.SelectedIndexes.Length >0 )
			{
				for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++)
				{
					int selIndex = rsc.SelectedIndexes[selectedIndex];
					strSQL +=dg.DataKeys[selIndex].ToString() +",";
				}
				// remove last comma and close up statement
				strSQL = strSQL.Substring(0,strSQL.Length -1 )+")";
    
				if  (strSQL !="")
				{
					conn.Open();
					cmdChangeRows= new SqlCommand(strSQL, conn);
					//Response.Write(strSQL);
					cmdChangeRows.ExecuteNonQuery();
					conn.Close();
				}
			}
    
			Data_Bind();
    
		}
		// <summary>
		//     Handler -- Selects the row to be edited
		// </summary>
		public void DG_Edit(object sender, DataGridCommandEventArgs e)
		{
			dg.EditItemIndex = e.Item.ItemIndex;
			Data_Bind();
		}
		// <summary>
		//     Handler --  cancels the row editing
		// </summary>
		public void DG_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dg.EditItemIndex =-1;
			Data_Bind();
		}
		// <summary>
		//     Handler --  updates the tables based on new information
		// </summary>
		public void DG_Update(object sender, DataGridCommandEventArgs e)
		{
    
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			// Constant for the brands
			// we list only TPE prime
			int iBaseBrand = 1090;
    
			// update offers
			SqlCommand cmdUpdate;
			cmdUpdate = new SqlCommand("DELETE FROM OFFER WHERE OFFR_CONT ='"+ddlProduct.SelectedItem.Value.ToString()+"' AND OFFR_MNTH='"+dg.DataKeys[e.Item.ItemIndex].ToString()+"'",conn);
			cmdUpdate.ExecuteNonQuery();
			StringBuilder sbSQL = new StringBuilder();
			sbSQL.Append("Exec spTransactionEngine_S @PersId=131, @CompId=149, @Size=1,@FOB=1, "); // hard coded info
			sbSQL.Append("@Qty="+((TextBox) e.Item.Cells[6].Controls[3]).Text+",@Month='"+dg.DataKeys[e.Item.ItemIndex].ToString()+"',");
			sbSQL.Append("@Price="+((TextBox) e.Item.Cells[5].Controls[3]).Text+", @Brand="+(iBaseBrand  + Convert.ToInt32(dg.DataKeys[e.Item.ItemIndex])).ToString()+", @ContId='"+ddlProduct.SelectedItem.Value.ToString()+"'");
			cmdUpdate = new SqlCommand(sbSQL.ToString(),conn);
			cmdUpdate.ExecuteNonQuery();
    
			// update bids
    
			cmdUpdate = new SqlCommand("DELETE FROM BID WHERE BID_CONT ='"+ddlProduct.SelectedItem.Value.ToString()+"' AND BID_MNTH='"+dg.DataKeys[e.Item.ItemIndex].ToString()+"'",conn);
			cmdUpdate.ExecuteNonQuery();
			StringBuilder sbSQLBid = new StringBuilder();
			sbSQLBid.Append("Exec spTransactionEngine_P @Pers_Id=133, @Comp_Id=148, @Size=1,@ErrMsg=NULL , "); // hard coded info
			sbSQLBid.Append("@Qty="+((TextBox) e.Item.Cells[3].Controls[3]).Text+",@Month='"+dg.DataKeys[e.Item.ItemIndex].ToString()+"',");
			sbSQLBid.Append("@Price="+((TextBox) e.Item.Cells[4].Controls[3]).Text+",  @Cont_Id='"+ddlProduct.SelectedItem.Value.ToString()+"'");
			cmdUpdate = new SqlCommand(sbSQLBid.ToString(),conn);
			cmdUpdate.ExecuteNonQuery();
			//Response.Write(sbSQLBid.ToString());
    
			conn.Close();
    
			dg.EditItemIndex =-1;
			Data_Bind();
    
    
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
