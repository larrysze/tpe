using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using dotnetCHARTING;
using TPE.Utility;


namespace localhost.Forward
{
    public partial class Forward_Floor : System.Web.UI.Page
    {
        public void Page_Load(object sender, EventArgs e)
        {


            if ((string)Session["Typ"] == null)
            {
                Response.Redirect("/default.aspx");
            }

            if (!IsPostBack)
            {

                SqlConnection conn;
                conn = new SqlConnection(Application["DBconn"].ToString());
                conn.Open();
                SqlCommand cmdResinFilter;
                SqlDataReader dtrResinFilter;

                cmdResinFilter = new SqlCommand("select CONT_ID, cont_labl from contract where cont_enabled='True' order by cont_ordr", conn);
                //cmdResinFilter = new SqlCommand("Select distinct CONT_CATG From CONTRACT WHERE CONT_CATG <>'Monomer' AND CONT_ENABLED='True'", conn);
                dtrResinFilter = cmdResinFilter.ExecuteReader();
                ddlResinFilter.Items.Add(new ListItem("Show All", "*"));
                
                //while (dtrResinFilter.Read())
                //{
                //    ddlResinFilter.Items.Add(new ListItem(dtrResinFilter["CONT_CATG"].ToString()));

                //}
                while (dtrResinFilter.Read())
                {
                    ddlResinFilter.Items.Add(new ListItem(dtrResinFilter["cont_labl"].ToString(), dtrResinFilter["CONT_ID"].ToString()));
                }

                dtrResinFilter.Close();


                SqlCommand cmdFWDMonth;
                SqlDataReader dtrFWDMonth;
                cmdFWDMonth = new SqlCommand("Select TOP 3 FWD_ID,FWD_MNTH,RIGHT(FWD_YEAR,2) AS YEAR FROM FWDMonth WHERE FWD_ACTV='1'", conn);
                dtrFWDMonth = cmdFWDMonth.ExecuteReader();
                while (dtrFWDMonth.Read())
                {
                    ddlFWDMonth.Items.Add(new ListItem(dtrFWDMonth["FWD_MNTH"].ToString() + " " + dtrFWDMonth["YEAR"].ToString(), dtrFWDMonth["FWD_ID"].ToString()));
                    ddlHistoricalChartDate.Items.Add(new ListItem(dtrFWDMonth["FWD_MNTH"].ToString() + " " + dtrFWDMonth["YEAR"].ToString(), dtrFWDMonth["FWD_ID"].ToString()));

                }
                dtrFWDMonth.Close();


                conn.Close();

                Data_Bind();

            }

            if (!ddlResinFilter.SelectedItem.Value.ToString().Equals("*"))
            {
                Chart.Visible = true;
                ChartObj.Visible = true;
                tabCharts.Enabled = true;
                createGradeChart();
                createHistoricalGradeChart();
            }
            else
            {
                tabCharts.Enabled = false;
                Chart.Visible = false;
                ChartObj.Visible = false;
            }

        }

        // <summary>
        //   Handler for resets the data grid
        // </summary>
        public void Reset_Grid(object sender, EventArgs e)
        {
            Data_Bind();
        }

        public void Update_HistoricalPriceChart(object sender, EventArgs e)
        {
            // getHistoricalData(ddlResinFilter.SelectedValue);
        }

   
        
        private void Data_Bind()
        {

            StringBuilder sbSQL = new StringBuilder();

            //specific resin
            if (!ddlResinFilter.SelectedItem.Value.ToString().Equals("*"))
            {
                dg.Visible = false;
                dgFuture.Visible = true;

                lblShipMonth.Visible = false;
                ddlFWDMonth.Visible = false;

                sbSQL.Append("SELECT TOP 3 (FWD_MNTH + ' ' + RIGHT(FWD_YEAR,2))as Month, bid.Bid_id, offer.offr_id, FWDMONTH.FWD_ID, CONTRACT.CONT_LABL, BID.BID_PRCE as BID, BID.BID_QTY as QTYBID, OFFER.OFFR_PRCE as OFFR, OFFER.OFFR_QTY as QTYOFFR, CONTRACT.CONT_ID ");
                sbSQL.Append("FROM         BID INNER JOIN ");
                sbSQL.Append("CONTRACT ON BID.BID_CONT = CONTRACT.CONT_ID INNER JOIN ");
                sbSQL.Append("OFFER ON CONTRACT.CONT_ID = OFFER.OFFR_CONT AND BID.BID_MNTH = OFFER.OFFR_MNTH INNER JOIN ");
                sbSQL.Append("FWDMONTH ON BID.BID_MNTH = FWDMONTH.FWD_ID ");
                sbSQL.Append("WHERE     (CONTRACT.CONT_ENABLED = 'True' ");
                sbSQL.Append(" AND CONTRACT.cont_id='" + ddlResinFilter.SelectedItem.Value.ToString() + "' AND FWDMONTH.FWD_ACTV=1 ) ");
                sbSQL.Append("ORDER BY FWDMONTH.FWD_ID");
            }
            else
            {
                lblShipMonth.Visible = true;
                ddlFWDMonth.Visible = true;

                dg.Visible = true;
                dgFuture.Visible = false;

                sbSQL.Append("Select ");
                sbSQL.Append("CONT_LABL, CONT_ID, ");
                sbSQL.Append("QTYBID =(SELECT SUM(BID_QTY) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND BID_SIZE='1'), ");
                sbSQL.Append("BID = (SELECT RIGHT(CONVERT(varchar,MAX(BID_PRCE)),5) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND BID_SIZE='1'), ");
                sbSQL.Append("OFFR = (SELECT RIGHT(CONVERT(varchar,MAX(OFFR_PRCE)),5) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1'), ");
                sbSQL.Append("QTYOFFR = (SELECT SUM(OFFR_QTY) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1') ");
                sbSQL.Append("From CONTRACT WHERE CONT_CATG <>'Monomer' AND CONT_ENABLED='True' ");
                sbSQL.Append("ORDER BY CONT_ORDR");
            }

            if (!ddlResinFilter.SelectedItem.Value.ToString().Equals("*"))
            {
                TPE.Utility.DBLibrary.BindDataGrid(Application["DBconn"].ToString(), dgFuture, sbSQL.ToString());
            }
            else
            {
                TPE.Utility.DBLibrary.BindDataGrid(Application["DBconn"].ToString(), dg, sbSQL.ToString());
            }





            //SqlDataAdapter dadContent;
            //DataSet dstContent;
            //SqlConnection conn;
            //conn = new SqlConnection(Application["DBconn"].ToString());
            //conn.Open();
            //StringBuilder sbSQL = new StringBuilder();

            //sbSQL.Append("  Select ");
            //sbSQL.Append("     CONT_LABL, CONT_ID, ");
            //sbSQL.Append("      QTYBID =(SELECT SUM(BID_QTY) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND BID_SIZE='1'), ");
            //sbSQL.Append("      BID = (SELECT RIGHT(CONVERT(varchar,MAX(BID_PRCE)),5) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND BID_SIZE='1'), ");
            //sbSQL.Append("      OFFR = (SELECT RIGHT(CONVERT(varchar,MAX(OFFR_PRCE)),5) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1'), ");
            //sbSQL.Append("      QTYOFFR = (SELECT SUM(OFFR_QTY) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1') ");
            //sbSQL.Append("  From CONTRACT WHERE CONT_CATG<>'Monomer'  AND CONT_ENABLED='True' ");

            //if (!ddlResinFilter.SelectedItem.Value.ToString().Equals("*"))
            //{
            //    //sbSQL.Append("  WHERE CONT_CATG='" + ddlResinFilter.SelectedItem.Value.ToString() + "'");
            //    sbSQL.Append("  AND CONT_CATG='" + ddlResinFilter.SelectedItem.Value.ToString() + "'");
            //}
            

            //sbSQL.Append("  ORDER BY CONT_ORDR ");


            //dadContent = new SqlDataAdapter(sbSQL.ToString(), conn);
            //dstContent = new DataSet();
            //dadContent.Fill(dstContent);
            //dg.DataSource = dstContent;
            //dg.DataBind();
            //conn.Close();

        }



        //Charts
        int ChartWidth = 640;

        private void createGradeChart()
        {
            string grade_id = ddlResinFilter.SelectedValue;

            Chart.ChartArea.ClearColors();
            Chart.SeriesCollection.Clear();
            Chart.LegendBox.ClearColors();
            //Chart.XAxis.Clear();
            //Chart.YAxis.Clear();

            Chart.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            Chart.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);

            //Chart.ChartArea.YAxis.StaticColumnWidth = 1;

            // Set the chart type            
            Chart.Type = ChartType.Combo;

            //Chart.OverlapFooter = true;
            Chart.Mentor = false;
            // Set the size
            Chart.Width = ChartWidth;
            Chart.Height = 320;
            // Set the temp directory
            Chart.TempDirectory = "temp";
            // Debug mode. ( Will show generated errors if any )
            Chart.Debug = false;
            Chart.Title = "Forward Curve: " + ddlResinFilter.SelectedItem;

            //Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;// = false;
            Chart.TitleBox.Position = TitleBoxPosition.Full;

            Chart.DefaultElement.Marker.Type = ElementMarkerType.Diamond;

            //Chart.YAxis.Line.EndCap = System.Drawing.Drawing2D.LineCap.Di//amondAnchor;

            Chart.DefaultElement.Marker.Visible = true;

            Color[] MyColorObjectArray = new Color[3] { Color.Red, Color.FromArgb(255, 202, 0), Color.Yellow };
            Chart.Palette = MyColorObjectArray;

            Chart.ChartAreaSpacing = 3;
            Chart.LegendBox.Visible = false;
            //Chart.LegendBox.Template = "%Icon%Name";

            // Modify the x axis labels.
            Chart.XAxis.TimeScaleLabels.DayFormatString = "p";
            Chart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Year);
            Chart.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            Chart.ChartArea.XAxis.TickLabelAngle = 90;

            //Chart.DefaultElement.ShowValue = true;
            //Chart.XAxis.DefaultTick.Label.Text = "<%Value,mmm>";
            Chart.ChartArea.XAxis.LabelRotate = true;
            Chart.XAxis.TimeScaleLabels.MonthFormatString = "MMM";
            //Chart.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            //ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            //ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM<BR>yyyy";
            //ChartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>yyyy";
            Chart.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.White;
            Chart.XAxis.TimeInterval = TimeInterval.Month;

            //Chart.DefaultElement.ShowValue = true;

            // Setup the axes.
            Chart.YAxis.Label.Text = "Price";
            Chart.YAxis.FormatString = "$0.000#;";
            Chart.YAxis.Scale = Scale.Range;
            Chart.YAxis.Interval = 0.005;

            //Get Data
            SeriesCollection mySC = getPriceData(grade_id);

            int i = mySC.Count;


            for (int x = 0; x < i; x++)
            {
                mySC[x].Type = SeriesTypeFinancial.Bar;
            }




            // Add the price data.
            Chart.SeriesCollection.Add(mySC);

            //Chart Style
            ChartStyle(Chart);

        }

        private SeriesCollection getPriceData(string grade_id)
        {
            SeriesCollection SC = new SeriesCollection();
            Series s = new Series();
            s.Name = "Price Range";

            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = null;
                string sSql = "SELECT (FWD_MNTH + ' ' + RIGHT(FWD_YEAR,4))as Month, FWDMONTH.FWD_ID, CONTRACT.CONT_LABL, CONTRACT.CONT_ID, BID.BID_PRCE as price_low , OFFER.OFFR_PRCE as price_high, CONTRACT.CONT_ID FROM BID INNER JOIN CONTRACT ON BID.BID_CONT = CONTRACT.CONT_ID INNER JOIN OFFER ON CONTRACT.CONT_ID = OFFER.OFFR_CONT AND BID.BID_MNTH = OFFER.OFFR_MNTH INNER JOIN FWDMONTH ON BID.BID_MNTH = FWDMONTH.FWD_ID WHERE(CONTRACT.CONT_ENABLED = 'True'  AND contract.cont_id='" + grade_id + "') ORDER BY FWDMONTH.FWD_ID";

                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSql);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["month"].ToString());
                    Element e = new Element();
                    e.XDateTime = date;

                    double price_low = Convert.ToDouble(dtr["price_low"].ToString());
                    double price_high = Convert.ToDouble(dtr["price_high"].ToString());

                    //e.YValueStart = price_low;
                    //e.YValue = price_high;


                    e.Close = price_high;
                    e.Open = price_low;

                    e.High = price_high;
                    e.Low = price_low;

                    s.Elements.Add(e);
                }
            }

            SC.Add(s);
            return (SC);
        }

        private void ChartStyle(dotnetCHARTING.Chart CurrChart)
        {

            //Chart
            CurrChart.TitleBox.Background.Color = Color.Orange;
            CurrChart.Background.Color = Color.Black;
            CurrChart.ChartArea.Background.Color = Color.Black;
            CurrChart.ChartArea.DefaultSeries.Line.Color = Color.Red;
            //CurrChart.ChartArea.DefaultSeries.Line.StartCap = System.Drawing.Drawing2D.LineCap.Triangle;
            //CurrChart.ChartArea.DefaultSeries.Line.EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
            //CurrChart.ChartArea.DefaultSeries.Line.Width = 2;

            CurrChart.ChartArea.Line.Color = Color.FromArgb(255, 255, 0, 0);//(254, 0, 0)
            CurrChart.ChartArea.Line.Color = Color.Orange;
            CurrChart.ChartArea.Line.Width = 2;

            //XAxis
            CurrChart.XAxis.Label.Color = Color.White;
            CurrChart.ChartArea.XAxis.Label.Color = Color.White;
            CurrChart.XAxis.Line.Color = Color.Orange;
            CurrChart.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;

            //YAxis
            CurrChart.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            CurrChart.ChartArea.YAxis.Label.Color = Color.White;
            CurrChart.YAxis.Line.Color = Color.Orange;
            CurrChart.YAxis.Label.Color = Color.White;

        }


        //Create Historical Chart Main
        private void createHistoricalGradeChart()
        {
            ChartObj.ChartArea.ClearColors();
            ChartObj.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartObj.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);

            ChartObj.OverlapFooter = true;
            ChartObj.Mentor = false;
            ChartObj.TempDirectory = "temp";
            ChartObj.Width = ChartWidth;
            ChartObj.Height = 320;
            ChartObj.FileManager.ImageFormat = ImageFormat.Png;
            // Set the chart type                        
            ChartObj.Type = ChartType.Combo;

            ChartObj.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            ChartObj.DefaultSeries.DefaultElement.Marker.Size = 6;
            ChartObj.DefaultElement.Marker.Visible = true;
            ChartObj.DefaultSeries.DefaultElement.Marker.Color = Color.White;//Color.FromArgb( 255, 255, 153, 9);
            ChartObj.XAxis.Label.Color = Color.White;

            // Debug mode. ( Will show generated errors if any )
            ChartObj.Debug = true;
            ChartObj.Title = "Historical Pricing: " + ddlResinFilter.SelectedItem + "   Month: " + ddlHistoricalChartDate.SelectedItem;


            ChartObj.TitleBox.Position = TitleBoxPosition.FullWithLegend;
            //ChartObj.DefaultElement.Marker.Visible = false;

            Color[] MyColorObjectArray = new Color[3] { Color.Red, Color.FromArgb(255, 202, 0), Color.Yellow };
            ChartObj.Palette = MyColorObjectArray;

            ChartObj.ChartAreaSpacing = 3;
            ChartObj.LegendBox.Template = "%Icon%Name";
            ChartObj.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;

            //            // Modify the x axis labels.
            ChartObj.XAxis.TimeInterval = TimeInterval.Day;
            ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            ChartObj.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            //ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);
            ChartObj.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartObj.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartObj.ChartArea.XAxis.TickLabelAngle = 90;

            ChartObj.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartObj.ChartArea.XAxis.LabelRotate = true;
            ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            //ChartObj.XAxis.TimeInterval = TimeInterval.Month;


            //XAxis
            ChartObj.XAxis.Label.Color = Color.White;
            ChartObj.ChartArea.XAxis.Label.Color = Color.White;
            ChartObj.XAxis.Line.Color = Color.Orange;
            ChartObj.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;

            ChartObj.YAxis.Interval = 0.01;
            // Setup the axes.
            ChartObj.YAxis.Label.Text = "Price";
            ChartObj.YAxis.FormatString = "Currency";
            ChartObj.YAxis.Scale = Scale.Range;

            ChartObj.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartObj.ChartArea.YAxis.Label.Color = Color.White;
            ChartObj.YAxis.Line.Color = Color.Orange;
            ChartObj.YAxis.Label.Color = Color.White;



            //Chart
            ChartObj.TitleBox.Background.Color = Color.Orange;
            ChartObj.Background.Color = Color.Black;
            ChartObj.ChartArea.Background.Color = Color.Black;
            ChartObj.ChartArea.DefaultSeries.Line.Color = Color.Red;
            //CurrChart.ChartArea.DefaultSeries.Line.StartCap = System.Drawing.Drawing2D.LineCap.Triangle;
            //CurrChart.ChartArea.DefaultSeries.Line.EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
            ChartObj.ChartArea.DefaultSeries.Line.Width = 2;

            ChartObj.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
            ChartObj.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);

            //CurrChart.ChartArea.Line.Color = Color.FromArgb(255, 255, 0, 0);//(254, 0, 0)
            ChartObj.ChartArea.Line.Color = Color.Orange;
            //CurrChart.ChartArea.Line.Width = 2;

            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();
            ChartObj.SeriesCollection.Add(getPriceDataHistoricalChart(ddlResinFilter.SelectedValue));

        }

        //Data - Range
        private SeriesCollection getPriceDataHistoricalChart(string grade_id)
        {
            string sSql = "select (FWD_MNTH + ' ' + RIGHT(FWD_YEAR,4))as Month,forward_price.cont_id,contract.cont_labl,forward_price.bid ,forward_price.ask,forward_price.date from forward_price,fwdmonth,contract where CONTRACT.CONT_ID='" + grade_id + "' and fwdmonth='" + ddlHistoricalChartDate.SelectedItem.Value.ToString() + "' and fwdmonth.fwd_actv='True' and forward_price.fwdmonth=fwdmonth.fwd_id and contract.cont_id=forward_price.cont_id order by fwdmonth ";
            string sSpotMarket = "select distinct EXPORT_PRICE.ask,export_price.date from EXPORT_PRICE, forward_price where EXPORT_PRICE.cont_id='" + grade_id + "'  and forward_price.fwdmonth='" + ddlHistoricalChartDate.SelectedItem.Value.ToString() + "'  and EXPORT_PRICE.cont_id =forward_price.cont_id and export_price.date >=forward_price.date order by EXPORT_PRICE.date ";

            SeriesCollection SC = new SeriesCollection();

            Series sPriceRange = new Series();
            Series sBidPrice = new Series();

            sPriceRange.Name = "Price Range";
            sBidPrice.Name = "Spot Ask Price";

            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = null;
                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSql);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Price Range Element
                    ePriceRange.XDateTime = date;
                    ePriceRange.Close = Convert.ToDouble(dtr["ask"].ToString());
                    ePriceRange.Open = Convert.ToDouble(dtr["bid"].ToString());
                    ePriceRange.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd\r\nyyy");
                    sPriceRange.Elements.Add(ePriceRange);
                }
            }

            SC.Add(sPriceRange);


            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = null;
                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSpotMarket);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Bid Element
                    eBidPrice.ToolTip = dtr["ask"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd\r\nyyy");
                    sBidPrice.AddElements(eBidPrice);
                }

            }

            SC.Add(sBidPrice);

            SC[0].Type = SeriesTypeFinancial.Bar;
            SC[1].Type = SeriesType.Line;

            return (SC);
        }

    }
}
