using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;

namespace localhost.Forward
{
    public partial class contractinfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string GradeID = Request.QueryString["GradeID"];

            SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());
            SqlCommand cmd = new SqlCommand("SELECT * FROM CONTRACT_SPECIFICATIONS WHERE GRADEID='" + GradeID + "'", conn);

            conn.Open();
            // 1.  get an instance of the SqlDataReader
            SqlDataReader rdr = cmd.ExecuteReader();

            rdr.Read();

            lblContractFeatures.Text = Convert.ToString(rdr.GetValue(2));
            lblSpecificGrades.Text = Convert.ToString(rdr.GetValue(3));

            conn.Close();

        }
    }
}
