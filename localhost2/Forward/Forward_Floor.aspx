<%@ Page Language="c#" Codebehind="Forward_Floor.aspx.cs" AutoEventWireup="True" Inherits="localhost.Forward.Forward_Floor" MasterPageFile="~/MasterPages/Template.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Template.Master" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="asp" namespace="System.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>



<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphInstructions" Visible="false">
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphFlash"  Visible="true">
    <object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" height="155" width="780" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">
        <param name="_cx" value="20638" />
        <param name="_cy" value="4101" />
        <param name="FlashVars" value="" />
        <param name="Movie" value="/swf/contract.swf" />
        <param name="Src" value="/swf/contract.swf" />
        <param name="WMode" value="Transparent" />
        <param name="Play" value="-1" />
        <param name="Loop" value="-1" />
        <param name="Quality" value="High" />
        <param name="SAlign" value="" />
        <param name="Menu" value="-1" />
        <param name="Base" value="" />
        <param name="AllowScriptAccess" value="always" />
        <param name="Scale" value="ShowAll" />
        <param name="DeviceFont" value="0" />
        <param name="EmbedMovie" value="0" />
        <param name="BGColor" value="" />
        <param name="SWRemote" value="" />
        <param name="MovieData" value="" />
        <param name="SeamlessTabbing" value="1" />
        <param name="Profile" value="0" />
        <param name="ProfileAddress" value="" />
        <param name="ProfilePort" value="0" />
        <embed wmode="transparent" src="/swf/contract.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="780" height="155" />                           
    </object>
    <script src="/ieupdate.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphHeading">
    <div class="Header Bold Color1">
        Contract Prices
    </div>
</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
    <asp:ScriptManager id="ScriptManager1" runat="Server">
    </asp:ScriptManager>
    
     <asp:UpdatePanel id="UpdatePanel1" runat="Server">
 <ContentTemplate> 
    <div align="left">
    <table border="0" cellpadding="0" cellspacing="0">
    
        <tr>
            <td align="left" class="Header Bold" width="50%"> 
                Products:
                <asp:DropDownList ID="ddlResinFilter" runat="server" OnSelectedIndexChanged="Reset_Grid" AutoPostBack="true" CssClass="InputForm" />                       
                &nbsp;&nbsp;&nbsp;&nbsp;
               <asp:label runat="server" id="lblShipMonth" Text="Ship Month:" /> 
               <asp:DropDownList id="ddlFWDMonth" runat="server" CssClass="InputForm" AutoPostBack="true" OnSelectedIndexChanged="Reset_Grid" />
                
                </td>
            <td>
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>  
                            <asp:Image ID="imgPleaseWait" runat="server" ImageUrl="http://www.theplasticexchange.com/images/progressbar_green.gif" />
                            <br />
                            <asp:Label ID="lblPleaseWait" runat="server" Font-Names="Arial" Font-Size="12px" Text="Communicating With Server..." />                                
                        </ProgressTemplate>        
                    </asp:UpdateProgress>                                            

            </td>
        </tr>
    
    <tr><td valign="top" style="width: 615px">
        <asp:DataGrid ID="dg" CssClass="Content" runat="server" BorderWidth="0" CellSpacing="1" BackColor="#000000" Width="550px" HorizontalAlign="Left" EditItemStyle-BackColor="Silver" DataKeyField="CONT_ID" CellPadding="2" AutoGenerateColumns="False">
            <EditItemStyle BackColor="Silver"></EditItemStyle>
            <AlternatingItemStyle CssClass="LinkNormal LightGray"></AlternatingItemStyle>
            <ItemStyle CssClass="LinkNormal DarkGray" HorizontalAlign="Left"></ItemStyle>
            <HeaderStyle CssClass="LinkNormal Bold OrangeColor" HorizontalAlign="Left"></HeaderStyle>
            <Columns>
                <asp:BoundColumn DataField="Cont_labl" ReadOnly="True" HeaderText="Contract">
                    <ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Railcar">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYBID") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CompareValidator ID="CompareValidator1" ControlToValidate="txtQTYBID" ErrorMessage="Silly rabit, quantity must be an Integer." Display="none" Operator="DataTypeCheck" Type="Integer" runat="server" />
                        <asp:TextBox ID="txtQTYBID" Size="1" MaxLength="4" Text='<%# DataBinder.Eval(Container, "DataItem.QTYBID") %>' runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtQTYBID" Display="none" ErrorMessage="Bid quantity can not be blank" />
                        <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtQTYBID" runat="server" ErrorMessage="Bid quantity must be greater than one." Display="none" Type="Integer" MinimumValue="1" MaximumValue="100" />
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Bid Price">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BID") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CompareValidator ID="CompareValidator2" ControlToValidate="txtBID" ErrorMessage="Silly rabit, bid must be an Integer." Display="none" Operator="DataTypeCheck" Type="Double" runat="server" />
                        <asp:TextBox Size="1" ID="txtBID" MaxLength="6" Text='<%# DataBinder.Eval(Container, "DataItem.BID") %>' runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBID" Display="none" ErrorMessage="Bid price can not be blank" />
                        <asp:RangeValidator ID="RangeValidator2" ControlToValidate="txtBID" runat="server" ErrorMessage="Invalid bid amount." Display="none" Type="Double" MinimumValue="0" MaximumValue=".999" />
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Ask Price">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OFFR") %>' ID="Label33" NAME="Label2" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CompareValidator ControlToValidate="txtOFFER" ErrorMessage="Silly rabit, offer must be an Integer." Display="none" Operator="DataTypeCheck" Type="Double" runat="server" ID="Comparevalidator1" NAME="Comparevalidator1" />
                        <asp:TextBox Size="1" ID="txtOFFER" MaxLength="6" Text='<%# DataBinder.Eval(Container, "DataItem.OFFR") %>' runat="server" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtOFFER" Display="none" ErrorMessage="Offer price can not be blank" ID="Requiredfieldvalidator1" NAME="Requiredfieldvalidator1" />
                        <asp:RangeValidator ControlToValidate="txtOFFER" runat="server" ErrorMessage="Invalid offer amount." Display="none" Type="Double" MinimumValue="0" MaximumValue=".999" ID="Rangevalidator1" NAME="Rangevalidator1" />
                    </EditItemTemplate>
                 </asp:TemplateColumn>
                 <asp:TemplateColumn HeaderText="Railcar">
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYOFFR") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CompareValidator ID="CompareValidator3" ControlToValidate="txtQTYOFFER" ErrorMessage="Silly rabit, quantity must be an Integer." Display="none" Operator="DataTypeCheck" Type="Integer" runat="server" />
                        <asp:TextBox Size="1" ID="txtQTYOFFER" MaxLength="4" Text='<%# DataBinder.Eval(Container, "DataItem.QTYOFFR") %>' runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtQTYOFFER" Display="none" ErrorMessage="Offer quantity can not be blank" />
                        <asp:RangeValidator ID="RangeValidator3" ControlToValidate="txtQTYOFFER" runat="server" ErrorMessage="Offer quantity must be greater than one." Display="none" Type="Integer" MinimumValue="1" MaximumValue="100" />
                    </EditItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid> 
        
          <asp:DataGrid id="dgFuture" runat="server" Width="550px" ShowFooter="false" EditItemStyle-CssClass="LinkNormal LightGray" DataKeyField="CONT_ID" CellPadding="0" AutoGenerateColumns="false" HeaderStyle-CssClass="LinkNormal Bold OrangeColor" AlternatingItemStyle-CssClass="LinkNormal DarkGray" ItemStyle-CssClass="LinkNormal LightGray" BorderWidth="0" CellSpacing="1" BackColor="#000000">
                    <Columns>                        
                        <asp:EditCommandColumn EditText="Edit" CancelText="Cancel" UpdateText="Update" ItemStyle-CssClass="LinkNormal"/>                        
                    <asp:BoundColumn DataField="Month" HeaderText="Month" ReadOnly="True" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left" />     
                                                                   
                        <asp:TemplateColumn HeaderText="Railcar">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYBID") %>' />
                            </ItemTemplate>

                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Bid">
                            <ItemTemplate>
                                <asp:Label runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.BID") %>' />
                            </ItemTemplate>

                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Ask">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OFFR") %>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Railcar">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYOFFR") %>' />
                            </ItemTemplate>

                        </asp:TemplateColumn>
                        
                        <asp:BoundColumn DataField="Bid_id" HeaderText="BidID" ReadOnly="True" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left" Visible="false" />  
                        <asp:BoundColumn DataField="offr_id" HeaderText="OfferID" ReadOnly="True" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left"  Visible="false" />  
                        <asp:BoundColumn DataField="FWD_ID" HeaderText="FWD_ID" ReadOnly="True" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left"  Visible="false" />                          

                    </Columns>
                </asp:DataGrid>
        
        
        
    </td><td  valign="top">
        <div  align="right">
        &nbsp;
        
            
               </div>
        </td></tr>      
        </table> 
   </div>
   
   
   
   <br />
   
    <div style="TEXT-ALIGN: left">
    
    <cc1:TabContainer runat="server" ID="tabCharts">
        <cc1:TabPanel runat="server" HeaderText="Forward Curve" ID="ForwardCurve">
            <ContentTemplate> 
                <dotnet:Chart id="Chart" runat="server">
                <TitleBox Position="Left" />            
                    <DefaultLegendBox Padding="4" Visible="True" CornerBottomRight="Cut">
                        <HeaderEntry SortOrder="-1" Visible="False" Value="Value" Name="Name"> 
                            <DividerLine Color="Gray" />
                            <LabelStyle Font="Arial, 8pt, style=Bold" />
                        </HeaderEntry>
                    </DefaultLegendBox>

                    <DefaultTitleBox Visible="True" />

            <DefaultElement>
                <DefaultSubValue>    
                    <Line Color="93, 28, 59" Length="4" StartCap="DiamondAnchor" EndCap="Triangle" />
                </DefaultSubValue>
            </DefaultElement>
        </dotnet:Chart>                                   
            </ContentTemplate>        
        </cc1:TabPanel>
        
        <cc1:TabPanel runat="server" HeaderText="Historical Chart" ID="HistoricalChart">
            <ContentTemplate>
                Contract Month: <asp:DropDownList runat="server" id="ddlHistoricalChartDate"  CssClass="InputForm" AutoPostBack="true"  OnSelectedIndexChanged="Update_HistoricalPriceChart" />
                &nbsp;&nbsp;
                <br />
                <br />
                
                <dotnet:Chart id="ChartObj" runat="server">
                <TitleBox Position="Left" />            
                    <DefaultLegendBox Padding="4" Visible="True" CornerBottomRight="Cut">
                        <HeaderEntry SortOrder="-1" Visible="False" Value="Value" Name="Name"> 
                            <DividerLine Color="Gray" />
                            <LabelStyle Font="Arial, 8pt, style=Bold" />
                        </HeaderEntry>
                    </DefaultLegendBox>

                    <DefaultTitleBox Visible="True" />

            <DefaultElement>
                <DefaultSubValue>    
                    <Line Color="93, 28, 59" Length="4" StartCap="DiamondAnchor" EndCap="Triangle" />
                </DefaultSubValue>
            </DefaultElement>
        </dotnet:Chart>
            
     
            </ContentTemplate>
        </cc1:TabPanel>
    
    </cc1:TabContainer>
    
    
  
    
        
    </div>
   
    </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
 