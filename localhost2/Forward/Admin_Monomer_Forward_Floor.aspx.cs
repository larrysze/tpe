using System.Data.SqlClient;
using System;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MetaBuilders.WebControls;
using TPE.Utility;

namespace localhost.Forward
{
    public partial class Admin_Monomer_Forward_Floor : System.Web.UI.Page
    {
        public void Page_Load(object sender, EventArgs e)
        {
            if ((string)Session["Typ"] != "A")
            {
                Response.Redirect("/default.aspx");
            }
            if (!IsPostBack)
            {
                //SqlCommand cmdResinFilter;
                using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
                {                   
                    using (SqlDataReader dtrFWDMonth = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn, "Select FWD_ID,FWD_MNTH,RIGHT(FWD_YEAR,2) AS YEAR FROM FWDMonth WHERE FWD_ACTV='1'"))
                    {
                        while (dtrFWDMonth.Read())
                        {
                            ddlFWDMonth.Items.Add(new ListItem(dtrFWDMonth["FWD_MNTH"].ToString() + " " + dtrFWDMonth["YEAR"].ToString(), dtrFWDMonth["FWD_ID"].ToString()));
                        }
                    }
                }
                Data_Bind();
            }
        }
        // <summary>
        //   Handler for resets the data grid
        // </summary>
        public void Reset_Grid(object sender, EventArgs e)
        {
            Data_Bind();
        }
        private void Data_Bind()
        {
            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("Select ");
            sbSQL.Append("CONT_LABL, CONT_ID, ");
            sbSQL.Append("QTYBID =(SELECT SUM(BID_QTY) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND BID_SIZE='1'), ");
            sbSQL.Append("BID = (SELECT RIGHT(CONVERT(varchar,MAX(BID_PRCE)),5) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND BID_SIZE='1'), ");
            sbSQL.Append("OFFR = (SELECT RIGHT(CONVERT(varchar,MAX(OFFR_PRCE)),5) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1'), ");
            sbSQL.Append("QTYOFFR = (SELECT SUM(OFFR_QTY) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1') ");
            sbSQL.Append("From CONTRACT WHERE CONT_CATG='Monomer' ");

            //if (!ddlResinFilter.SelectedItem.Value.ToString().Equals("*"))
            //{
            //    sbSQL.Append(" AND CONT_CATG='" + ddlResinFilter.SelectedItem.Value.ToString() + "' ");
            //}

            sbSQL.Append("ORDER BY CONT_ORDR");

            TPE.Utility.DBLibrary.BindDataGrid(Application["DBconn"].ToString(), dg, sbSQL.ToString());
        }

        // <summary>
        //     Adds all car to a list all screen
        // </summary>

        public void CollectAllRCNum(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                // All.NavigateUrl += DataBinder.Eval(e.Item.DataItem, "SHIP_ID_CH").ToString()+";";
            }

        }
        // <summary>
        //   Code to raise and lower the transaction prices.  The script is available only to admnins and the functionality moves the
        //   working orders for mgbuyer/mbseller transperently
        // </summary>
        public void Update_Price(object sender, EventArgs e)
        {
            string strUserId;

            string strChange_Price = "";
            string strSQL = "";
            string strSQLBid = "";
            string strSQLOffer = "";
            SqlConnection conn;
            SqlConnection conn2;
            SqlCommand cmdChangeRows;
            SqlCommand cmdChangeRowsBids;
            SqlCommand cmdChangeRowsOffers;
            SqlDataReader dtrChangeRows;
            SqlDataReader dtrChangeRowsBids;
            SqlDataReader dtrChangeRowsOffers;
            dtrChangeRowsBids = null;
            dtrChangeRowsOffers = null;
            conn = new SqlConnection(Application["DBconn"].ToString());
            conn2 = new SqlConnection(Application["DBconn"].ToString());

            if (ddlRaiseLower.SelectedItem.Value.ToString().Equals("Lower"))
            {
                strChange_Price = "-." + UpDownPrc.Text;
            }
            else
            {
                strChange_Price = "+." + UpDownPrc.Text;
            }
            if (ddlBidOffer.SelectedItem.Value.ToString().Equals("0"))
            { // bids
                strUserId = "430"; // hard coded for mgbuy
                strSQLBid = "SELECT Bid_Id,Bid_Buyr,Bid_Cont,Bid_To,Bid_To_Locl,Bid_Qty,Bid_Prce,Bid_Size,Bid_Buyr_Comp,Bid_mnth,EXPR = BID_PRCE" + strChange_Price + " FROM BID WHERE BID_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND BID_BUYR='" + strUserId + "' AND BID_SIZE='1' AND  BID_CONT in (";
            }
            else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("1"))
            { // offers
                strUserId = "429"; // hard coded for mgsell
                strSQLOffer = "SELECT Offr_Id,Offr_Selr,Offr_Bran,Offr_Cont,Offr_from,Offr_From_Locl,Offr_Qty,Offr_Prce,Offr_Size,Offr_Gene,Offr_Selr_Comp,Offr_Bran_Comp,Offr_fob,Offr_mnth,EXPR = OFFR_PRCE+" + strChange_Price + " FROM OFFER WHERE OFFR_SELR=" + strUserId + " AND OFFR_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1' AND  OFFR_CONT in (";
            }
            else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("both"))
            { // both
                strUserId = "430"; // hard coded for mgbuy
                strSQLBid = "SELECT Bid_Id,Bid_Buyr,Bid_Cont,Bid_To,Bid_To_Locl,Bid_Qty,Bid_Prce,Bid_Size,Bid_Buyr_Comp,Bid_mnth,EXPR = BID_PRCE" + strChange_Price + " FROM BID WHERE BID_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND BID_BUYR='" + strUserId + "' AND BID_SIZE='1' AND  BID_CONT in (";
                strUserId = "429"; // hard coded for mgsell
                strSQLOffer = "SELECT Offr_Id,Offr_Selr,Offr_Bran,Offr_Cont,Offr_from,Offr_From_Locl,Offr_Qty,Offr_Prce,Offr_Size,Offr_Gene,Offr_Selr_Comp,Offr_Bran_Comp,Offr_fob,Offr_mnth,EXPR = OFFR_PRCE+" + strChange_Price + " FROM OFFER WHERE OFFR_SELR=" + strUserId + " AND OFFR_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1' AND  OFFR_CONT in (";
            }

            RowSelectorColumn rsc;
            rsc = RowSelectorColumn.FindColumn(dg);
            if (rsc.SelectedIndexes.Length > 0)
            {
                for (int selectedIndex = 0; selectedIndex < rsc.SelectedIndexes.Length; selectedIndex++)
                {
                    int selIndex = rsc.SelectedIndexes[selectedIndex];
                    strSQLBid += dg.DataKeys[selIndex].ToString() + ",";
                    strSQLOffer += dg.DataKeys[selIndex].ToString() + ",";
                }
                // remove last comma and close up statement
                strSQLBid = strSQLBid.Substring(0, strSQLBid.Length - 1) + ")";
                strSQLOffer = strSQLOffer.Substring(0, strSQLOffer.Length - 1) + ")";

                conn.Open();
                conn2.Open();
                try
                {
                    if (ddlBidOffer.SelectedItem.Value.ToString().Equals("0"))
                    { // bids
                        cmdChangeRowsBids = new SqlCommand(strSQLBid, conn);
                        dtrChangeRowsBids = cmdChangeRowsBids.ExecuteReader();
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("1"))
                    { // offers
                        cmdChangeRowsOffers = new SqlCommand(strSQLOffer, conn2);
                        dtrChangeRowsOffers = cmdChangeRowsOffers.ExecuteReader();
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("both"))
                    { // both
                        cmdChangeRowsBids = new SqlCommand(strSQLBid, conn);
                        dtrChangeRowsBids = cmdChangeRowsBids.ExecuteReader();
                        cmdChangeRowsOffers = new SqlCommand(strSQLOffer, conn2);
                        dtrChangeRowsOffers = cmdChangeRowsOffers.ExecuteReader();
                    }

                    // placeheld hack
                    string thegene;
                    string thefob;
                    //Response.Write("Bids" +strSQLBid +"<BR><BR>");
                    //Response.Write("Offers" +strSQLOffer);

                    strSQL = ""; // blank the string for reuse
                    if (ddlBidOffer.SelectedItem.Value.ToString().Equals("0"))
                    { // bids
                        while (dtrChangeRowsBids.Read())
                        {
                            strSQL += " Exec spTransactionEngine_P @Comp_Id=" + dtrChangeRowsBids[8].ToString() + ", @Pers_Id=" + dtrChangeRowsBids[1].ToString() + ", @Cont_Id=" + dtrChangeRowsBids[2].ToString() + ", @Qty=" + dtrChangeRowsBids[5].ToString().ToString() + ", @Price=" + dtrChangeRowsBids[10].ToString() + ", @Size='1', @Month=" + dtrChangeRowsBids[9].ToString() + ", @Order_Id=" + dtrChangeRowsBids[0].ToString() + ", @ErrMsg = NULL ";
                        }
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("1"))
                    { // offers
                        while (dtrChangeRowsOffers.Read())
                        {
                            if (dtrChangeRowsOffers[9].ToString() == "False")
                            {
                                thegene = "0";
                            }
                            else
                            {
                                thegene = "1";
                            }
                            if (dtrChangeRowsOffers[12].ToString() == "False")
                            {
                                thefob = "0";
                            }
                            else
                            {
                                thefob = "1";
                            }
                            strSQL += " Exec spTransactionEngine_S @CompId=" + dtrChangeRowsOffers[10].ToString() + ", @PersId=" + dtrChangeRowsOffers[1].ToString() + ", @ContId=" + dtrChangeRowsOffers[3].ToString() + ", @Qty=" + dtrChangeRowsOffers[6].ToString() + ", @Price=" + dtrChangeRowsOffers[14].ToString() + ", @Size='1', @Month=" + dtrChangeRowsOffers[13].ToString() + ", @FOB=" + thefob + ",@IdOrder=" + dtrChangeRowsOffers[0].ToString() + ", @Gene=" + thegene;
                        }
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("both"))
                    { // both
                        while (dtrChangeRowsOffers.Read())
                        {
                            if (dtrChangeRowsOffers[9].ToString() == "False")
                            {
                                thegene = "0";
                            }
                            else
                            {
                                thegene = "1";
                            }
                            if (dtrChangeRowsOffers[12].ToString() == "False")
                            {
                                thefob = "0";
                            }
                            else
                            {
                                thefob = "1";
                            }
                            strSQL += " Exec spTransactionEngine_S @CompId=" + dtrChangeRowsOffers[10].ToString() + ", @PersId=" + dtrChangeRowsOffers[1].ToString() + ", @ContId=" + dtrChangeRowsOffers[3].ToString() + ", @Qty=" + dtrChangeRowsOffers[6].ToString() + ", @Price=" + dtrChangeRowsOffers[14].ToString() + ", @Size='1', @Month=" + dtrChangeRowsOffers[13].ToString() + ", @FOB=" + thefob + ",@IdOrder=" + dtrChangeRowsOffers[0].ToString() + ", @Gene=" + thegene;
                        }
                        while (dtrChangeRowsBids.Read())
                        {
                            strSQL += " Exec spTransactionEngine_P @Comp_Id=" + dtrChangeRowsBids[8].ToString() + ", @Pers_Id=" + dtrChangeRowsBids[1].ToString() + ", @Cont_Id=" + dtrChangeRowsBids[2].ToString() + ", @Qty=" + dtrChangeRowsBids[5].ToString().ToString() + ", @Price=" + dtrChangeRowsBids[10].ToString() + ", @Size='1', @Month=" + dtrChangeRowsBids[9].ToString() + ", @Order_Id=" + dtrChangeRowsBids[0].ToString() + ", @ErrMsg = NULL ";
                        }
                    }

                    if (ddlBidOffer.SelectedItem.Value.ToString().Equals("0"))
                    { // bids
                        dtrChangeRowsBids.Close();
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("1"))
                    { // offers

                        dtrChangeRowsOffers.Close();
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("both"))
                    { // both
                        dtrChangeRowsBids.Close();
                        dtrChangeRowsOffers.Close();
                    }
                    if (strSQL != "")
                    {
                        cmdChangeRows = new SqlCommand(strSQL, conn); // reuse the declared object
                        //Response.Write(strSQL);
                        cmdChangeRows.ExecuteNonQuery();
                    }
                }
                finally
                {
                    conn.Close();
                    conn2.Close();
                }
            }

            Data_Bind();
        }

        // <summary>
        //     Handler -- Selects the row to be edited
        // </summary>
        public void DG_Edit(object sender, DataGridCommandEventArgs e)
        {
            dg.EditItemIndex = e.Item.ItemIndex;
            Data_Bind();
        }
        // <summary>
        //     Handler --  cancels the row editing
        // </summary>
        public void DG_Cancel(object sender, DataGridCommandEventArgs e)
        {
            dg.EditItemIndex = -1;
            Data_Bind();
        }
        // <summary>
        //     Handler --  updates the tables based on new information
        // </summary>
        public void DG_Update(object sender, DataGridCommandEventArgs e)
        {
            // Constant for the brands
            // we list only TPE prime
            int iBaseBrand = 1090;
            // update offers
            Hashtable htParams = new Hashtable();
            htParams.Add("@OFFR_CONT", dg.DataKeys[e.Item.ItemIndex].ToString());
            htParams.Add("@OFFR_MNTH", ddlFWDMonth.SelectedItem.Value.ToString());
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), "DELETE FROM OFFER WHERE OFFR_CONT =@OFFR_CONT AND OFFR_MNTH=@OFFR_MNTH", htParams);

            htParams.Clear();
            htParams.Add("@PersId", "429");
            htParams.Add("@CompId", "149");
            htParams.Add("@Size", "1");
            htParams.Add("@FOB", "1");
            htParams.Add("@Qty", ((TextBox)e.Item.Cells[6].Controls[3]).Text);
            htParams.Add("@Month", ddlFWDMonth.SelectedItem.Value.ToString());
            htParams.Add("@Price", ((TextBox)e.Item.Cells[5].Controls[3]).Text);
            htParams.Add("@Brand", (iBaseBrand + Convert.ToInt32(dg.DataKeys[e.Item.ItemIndex])).ToString());
            htParams.Add("@ContId", dg.DataKeys[e.Item.ItemIndex].ToString());

            //sbSQL.Append("Exec spTransactionEngine_S @PersId=429, @CompId=149, @Size=1,@FOB=1, "); // hard coded info
            //sbSQL.Append("@Qty="+((TextBox) e.Item.Cells[6].Controls[3]).Text+",@Month='"+ddlFWDMonth.SelectedItem.Value.ToString()+"',");
            //sbSQL.Append("@Price="+((TextBox) e.Item.Cells[5].Controls[3]).Text+", @Brand="+(iBaseBrand  + Convert.ToInt32(dg.DataKeys[e.Item.ItemIndex])).ToString()+" , @ContId='"+dg.DataKeys[e.Item.ItemIndex].ToString()+"'");
            DBLibrary.ExecuteStoredProcedure(Application["DBconn"].ToString(), "spTransactionEngine_S", htParams); //EcmdUpdate = new SqlCommand(sbSQL.ToString(),conn);

            // update bids
            htParams.Clear();
            htParams.Add("@BID_CONT", dg.DataKeys[e.Item.ItemIndex].ToString());
            htParams.Add("@BID_MNTH", ddlFWDMonth.SelectedItem.Value.ToString());
            //cmdUpdate = new SqlCommand("DELETE FROM BID WHERE BID_CONT ='@BID_CONT' AND BID_MNTH='@BID_MNTH'");
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), "DELETE FROM BID WHERE BID_CONT =@BID_CONT AND BID_MNTH=@BID_MNTH", htParams);

            htParams.Clear();
            htParams.Add("@Pers_Id", "430");
            htParams.Add("@Comp_Id", "148");
            htParams.Add("@Size", "1");
            htParams.Add("@ErrMsg", "NULL");
            htParams.Add("@Qty", ((TextBox)e.Item.Cells[3].Controls[3]).Text);
            htParams.Add("@Month", ddlFWDMonth.SelectedItem.Value.ToString());
            htParams.Add("@Price", ((TextBox)e.Item.Cells[4].Controls[3]).Text);
            htParams.Add("@Cont_Id", dg.DataKeys[e.Item.ItemIndex].ToString());

            //StringBuilder sbSQLBid = new StringBuilder();
            //sbSQLBid.Append("Exec spTransactionEngine_P @Pers_Id=430, @Comp_Id=148, @Size=1,@ErrMsg=NULL , "); // hard coded info
            //sbSQLBid.Append("@Qty="+((TextBox) e.Item.Cells[3].Controls[3]).Text+",@Month='"+ddlFWDMonth.SelectedItem.Value.ToString()+"',");
            //sbSQLBid.Append("@Price="+((TextBox) e.Item.Cells[4].Controls[3]).Text+",  @Cont_Id='"+dg.DataKeys[e.Item.ItemIndex].ToString()+"'");
            DBLibrary.ExecuteStoredProcedure(Application["DBconn"].ToString(), "spTransactionEngine_P", htParams);

            dg.EditItemIndex = -1;
            Data_Bind();
        }
    }
}
