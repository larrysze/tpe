using System.Data.SqlClient;
using System;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MetaBuilders.WebControls;
using TPE.Utility;
using dotnetCHARTING;

namespace localhost.Forward
{
	/// <summary>
	/// Summary description for Admin_Forward_Floor.
	/// </summary>
	public partial class Admin_Forward_Floor : System.Web.UI.Page
	{

		/************************************************************************
		 *   1. File Name       :Administrator\Users_Management.aspx             *
		 *   2. Description     :Exchange user management                        *
		 *   3. Modification Log:                                                *
		 *     Ver No.       Date          Author             Modification       *
		 *   -----------------------------------------------------------------   *
		 *      1.00      2-25-2004      Zach                Comment             *
		 *                                                                       *
		 ************************************************************************/
		public void Page_Load(object sender, EventArgs e)
		{


            if ((string)Session["Typ"] != "A" && (string)Session["Typ"] != "L")
            {
                Response.Redirect("/default.aspx");
            }

            if (!IsPostBack)
            {
                // SqlCommand cmdResinFilter;
                using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
                {
                    using (SqlDataReader dtrResinFilter = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn, "select CONT_ID, cont_labl from contract where cont_enabled='True' order by cont_ordr"))
                    //using (SqlDataReader dtrResinFilter = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn, "Select distinct CONT_CATG From CONTRACT WHERE CONT_CATG <>'Monomer' AND CONT_ENABLED='True'"))
                    {
                        ddlResinFilter.Items.Add(new ListItem("Show All", "*"));
                        while (dtrResinFilter.Read())
                        {
                            ddlResinFilter.Items.Add(new ListItem(dtrResinFilter["cont_labl"].ToString(), dtrResinFilter["CONT_ID"].ToString()));
                        }
                    }

                    using (SqlDataReader dtrFWDMonth = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn, "Select FWD_ID,FWD_MNTH,RIGHT(FWD_YEAR,2) AS YEAR FROM FWDMonth WHERE FWD_ACTV='1'"))
                    {
                        while (dtrFWDMonth.Read())
                        {
                            ddlFWDMonth.Items.Add(new ListItem(dtrFWDMonth["FWD_MNTH"].ToString() + " " + dtrFWDMonth["YEAR"].ToString(), dtrFWDMonth["FWD_ID"].ToString()));
                            ddlToMonth.Items.Add(new ListItem(dtrFWDMonth["FWD_MNTH"].ToString() + " " + dtrFWDMonth["YEAR"].ToString(), dtrFWDMonth["FWD_ID"].ToString()));
                            ddlFromMonth.Items.Add(new ListItem(dtrFWDMonth["FWD_MNTH"].ToString() + " " + dtrFWDMonth["YEAR"].ToString(), dtrFWDMonth["FWD_ID"].ToString()));
                            ddlHistoricalChartDate.Items.Add(new ListItem(dtrFWDMonth["FWD_MNTH"].ToString() + " " + dtrFWDMonth["YEAR"].ToString(), dtrFWDMonth["FWD_ID"].ToString()));
                        }
                    }
                }
                Data_Bind();
            }

            if (!ddlResinFilter.SelectedItem.Value.ToString().Equals("*"))
            {
                //tabCharts.Visible = true;
                Chart.Visible = true;
                ChartObj.Visible = true;
                tabCharts.Enabled = true;
                createGradeChart();
                createHistoricalGradeChart();
            }
            else
            {
                //tabCharts.Visible = false;
                tabCharts.Enabled = false;
                Chart.Visible = false;
                ChartObj.Visible = false;
            }
            

		}


		// <summary>
		//   Handler for resets the data grid
		// </summary>
		public void Reset_Grid(object sender, EventArgs e)
		{
			Data_Bind();
		}
		
        
        private void Data_Bind()
		{
            StringBuilder sbSQL = new StringBuilder();

            //specific resin
            if (!ddlResinFilter.SelectedItem.Value.ToString().Equals("*"))
            {
                dg.Visible = false;
                dgFuture.Visible = true;

                lblShipMonth.Visible = false;
                ddlFWDMonth.Visible = false;

                sbSQL.Append("SELECT (FWD_MNTH + ' ' + RIGHT(FWD_YEAR,2))as Month, bid.Bid_id, offer.offr_id, FWDMONTH.FWD_ID, CONTRACT.CONT_LABL, BID.BID_PRCE as BID, BID.BID_QTY as QTYBID, OFFER.OFFR_PRCE as OFFR, OFFER.OFFR_QTY as QTYOFFR, CONTRACT.CONT_ID ");
                sbSQL.Append("FROM         BID INNER JOIN ");
                sbSQL.Append("CONTRACT ON BID.BID_CONT = CONTRACT.CONT_ID INNER JOIN ");
                sbSQL.Append("OFFER ON CONTRACT.CONT_ID = OFFER.OFFR_CONT AND BID.BID_MNTH = OFFER.OFFR_MNTH INNER JOIN ");
                sbSQL.Append("FWDMONTH ON BID.BID_MNTH = FWDMONTH.FWD_ID ");
                sbSQL.Append("WHERE     (CONTRACT.CONT_ENABLED = 'True' ");
                sbSQL.Append(" AND CONTRACT.cont_id='" + ddlResinFilter.SelectedItem.Value.ToString() + "' AND FWDMONTH.FWD_ACTV=1 ) ");
                sbSQL.Append("ORDER BY FWDMONTH.FWD_ID");
            }
            else
            {
                lblShipMonth.Visible = true;
                ddlFWDMonth.Visible = true;

                dg.Visible = true;
                dgFuture.Visible = false;

                sbSQL.Append("Select ");
                sbSQL.Append("CONT_LABL, CONT_ID, ");
                sbSQL.Append("QTYBID =(SELECT SUM(BID_QTY) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND BID_SIZE='1'), ");
                sbSQL.Append("BID = (SELECT MAX(BID_PRCE) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND BID_SIZE='1'), ");
                sbSQL.Append("OFFR = (SELECT MAX(OFFR_PRCE) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1'), ");
                sbSQL.Append("QTYOFFR = (SELECT SUM(OFFR_QTY) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1') ");
                sbSQL.Append("From CONTRACT WHERE CONT_CATG <>'Monomer' AND CONT_ENABLED='True' ");
                sbSQL.Append("ORDER BY CONT_ORDR");                       
            }

            if (!ddlResinFilter.SelectedItem.Value.ToString().Equals("*"))
            {
                TPE.Utility.DBLibrary.BindDataGrid(Application["DBconn"].ToString(), dgFuture, sbSQL.ToString());                
            }
            else
            {
                TPE.Utility.DBLibrary.BindDataGrid(Application["DBconn"].ToString(), dg, sbSQL.ToString());
            }
            
		}
		
		// <summary>
		//   Code to raise and lower the transaction prices.  The script is available only to admnins and the functionality moves the
		//   working orders for mgbuyer/mbseller transperently
		// </summary>

		public void Update_Price(object sender, EventArgs e)
		{
            if (!ddlResinFilter.SelectedItem.Value.ToString().Equals("*"))
            {
                UpdateChainResin();
            }
            else
            {
                UpdateNormalResin();
            }
		}

		// <summary>
		//     Handler -- Selects the row to be edited
		// </summary>

		public void DG_Edit(object sender, DataGridCommandEventArgs e)
		{
            if (!ddlResinFilter.SelectedItem.Value.ToString().Equals("*"))
            {
                dgFuture.EditItemIndex = e.Item.ItemIndex;
            }
            else
            {
                dg.EditItemIndex = e.Item.ItemIndex;
            }

			Data_Bind();
		}

		// <summary>
		//     Handler --  cancels the row editing
		// </summary>		        
        public void DG_Cancel(object sender, DataGridCommandEventArgs e)
		{
            if (!ddlResinFilter.SelectedItem.Value.ToString().Equals("*"))
            {
                dgFuture.EditItemIndex = -1;
            }
            else
            {
                dg.EditItemIndex = -1;
            }

			Data_Bind();
		}
		
        
        
        // <summary>
		//     Handler --  updates the tables based on new information
		// </summary>

		public void DG_Update(object sender, DataGridCommandEventArgs e)
		{
            if (!ddlResinFilter.SelectedItem.Value.ToString().Equals("*"))
            {
                UpdateMonthResin(e);
                createGradeChart();
            }
            else
            {
                UpdateOneNormalResin(e);
            }	
		}

        private void UpdateNormalResin()
        {
            string strUserId;

            string strChange_Price = "";
            string strSQL = "";
            string strSQLBid = "";
            string strSQLOffer = "";
            SqlConnection conn;
            SqlConnection conn2;
            SqlCommand cmdChangeRows;
            SqlCommand cmdChangeRowsBids;
            SqlCommand cmdChangeRowsOffers;
            SqlDataReader dtrChangeRows;
            SqlDataReader dtrChangeRowsBids;
            SqlDataReader dtrChangeRowsOffers;
            dtrChangeRowsBids = null;
            dtrChangeRowsOffers = null;
            conn = new SqlConnection(Application["DBconn"].ToString());
            conn2 = new SqlConnection(Application["DBconn"].ToString());

            if (ddlRaiseLower.SelectedItem.Value.ToString().Equals("Lower"))
            {
                strChange_Price = "-." + UpDownPrc.Text;
            }
            else
            {
                strChange_Price = "+." + UpDownPrc.Text;
            }
            if (ddlBidOffer.SelectedItem.Value.ToString().Equals("0"))
            { // bids
                strUserId = "430"; // hard coded for mgbuy
                strSQLBid = "SELECT Bid_Id,Bid_Buyr,Bid_Cont,Bid_To,Bid_To_Locl,Bid_Qty,Bid_Prce,Bid_Size,Bid_Buyr_Comp,Bid_mnth,EXPR = BID_PRCE" + strChange_Price + " FROM BID WHERE BID_MNTH between'" + ddlFromMonth.SelectedItem.Value.ToString() + "' AND '" + ddlToMonth.SelectedItem.Value.ToString() + "' AND BID_BUYR='" + strUserId + "' AND BID_SIZE='1' AND  BID_CONT in (";

                //strSQLBid = "SELECT Bid_Id,Bid_Buyr,Bid_Cont,Bid_To,Bid_To_Locl,Bid_Qty,Bid_Prce,Bid_Size,Bid_Buyr_Comp,Bid_mnth,EXPR = BID_PRCE" + strChange_Price + " FROM BID WHERE BID_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND BID_BUYR='" + strUserId + "' AND BID_SIZE='1' AND  BID_CONT in (";

            }
            else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("1"))
            { // offers
                strUserId = "429"; // hard coded for mgsell

                //strSQLOffer = "SELECT Offr_Id,Offr_Selr,Offr_Bran,Offr_Cont,Offr_from,Offr_From_Locl,Offr_Qty,Offr_Prce,Offr_Size,Offr_Gene,Offr_Selr_Comp,Offr_Bran_Comp,Offr_fob,Offr_mnth,EXPR = OFFR_PRCE+" + strChange_Price + " FROM OFFER WHERE OFFR_SELR=" + strUserId + " AND OFFR_MNTH='" + ddlFWDMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1' AND  OFFR_CONT in (";

                strSQLOffer = "SELECT Offr_Id,Offr_Selr,Offr_Bran,Offr_Cont,Offr_from,Offr_From_Locl,Offr_Qty,Offr_Prce,Offr_Size,Offr_Gene,Offr_Selr_Comp,Offr_Bran_Comp,Offr_fob,Offr_mnth,EXPR = OFFR_PRCE+" + strChange_Price + " FROM OFFER WHERE OFFR_SELR=" + strUserId + " AND OFFR_MNTH between'" + ddlFromMonth.SelectedItem.Value.ToString() + "' AND '" + ddlToMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1' AND  OFFR_CONT in (";
            }
            else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("both"))
            { // both
                strUserId = "430"; // hard coded for mgbuy
                //strSQLBid="SELECT Bid_Id,Bid_Buyr,Bid_Cont,Bid_To,Bid_To_Locl,Bid_Qty,Bid_Prce,Bid_Size,Bid_Buyr_Comp,Bid_mnth,EXPR = BID_PRCE"+strChange_Price+" FROM BID WHERE BID_MNTH='"+ddlFWDMonth.SelectedItem.Value.ToString()+"' AND BID_BUYR='"+strUserId+"' AND BID_SIZE='1' AND  BID_CONT in (";
                strSQLBid = "SELECT Bid_Id,Bid_Buyr,Bid_Cont,Bid_To,Bid_To_Locl,Bid_Qty,Bid_Prce,Bid_Size,Bid_Buyr_Comp,Bid_mnth,EXPR = BID_PRCE" + strChange_Price + " FROM BID WHERE BID_MNTH between'" + ddlFromMonth.SelectedItem.Value.ToString() + "' AND '" + ddlToMonth.SelectedItem.Value.ToString() + "' AND BID_BUYR='" + strUserId + "' AND BID_SIZE='1' AND  BID_CONT in (";
                strUserId = "429"; // hard coded for mgsell
                //strSQLOffer="SELECT Offr_Id,Offr_Selr,Offr_Bran,Offr_Cont,Offr_from,Offr_From_Locl,Offr_Qty,Offr_Prce,Offr_Size,Offr_Gene,Offr_Selr_Comp,Offr_Bran_Comp,Offr_fob,Offr_mnth,EXPR = OFFR_PRCE+"+strChange_Price+" FROM OFFER WHERE OFFR_SELR="+strUserId+" AND OFFR_MNTH='"+ddlFWDMonth.SelectedItem.Value.ToString()+"' AND OFFR_SIZE='1' AND  OFFR_CONT in (";
                strSQLOffer = "SELECT Offr_Id,Offr_Selr,Offr_Bran,Offr_Cont,Offr_from,Offr_From_Locl,Offr_Qty,Offr_Prce,Offr_Size,Offr_Gene,Offr_Selr_Comp,Offr_Bran_Comp,Offr_fob,Offr_mnth,EXPR = OFFR_PRCE+" + strChange_Price + " FROM OFFER WHERE OFFR_SELR=" + strUserId + " AND OFFR_MNTH between'" + ddlFromMonth.SelectedItem.Value.ToString() + "' AND '" + ddlToMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1' AND  OFFR_CONT in (";
            }

            RowSelectorColumn rsc;
            rsc = RowSelectorColumn.FindColumn(dg);
            if (rsc.SelectedIndexes.Length > 0)
            {
                for (int selectedIndex = 0; selectedIndex < rsc.SelectedIndexes.Length; selectedIndex++)
                {
                    int selIndex = rsc.SelectedIndexes[selectedIndex];
                    strSQLBid += dg.DataKeys[selIndex].ToString() + ",";
                    strSQLOffer += dg.DataKeys[selIndex].ToString() + ",";
                }
                // remove last comma and close up statement
                strSQLBid = strSQLBid.Substring(0, strSQLBid.Length - 1) + ")";
                strSQLOffer = strSQLOffer.Substring(0, strSQLOffer.Length - 1) + ")";

                conn.Open();
                conn2.Open();
                try
                {
                    if (ddlBidOffer.SelectedItem.Value.ToString().Equals("0"))
                    { // bids
                        cmdChangeRowsBids = new SqlCommand(strSQLBid, conn);
                        dtrChangeRowsBids = cmdChangeRowsBids.ExecuteReader();
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("1"))
                    { // offers
                        cmdChangeRowsOffers = new SqlCommand(strSQLOffer, conn2);
                        dtrChangeRowsOffers = cmdChangeRowsOffers.ExecuteReader();
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("both"))
                    { // both
                        cmdChangeRowsBids = new SqlCommand(strSQLBid, conn);
                        dtrChangeRowsBids = cmdChangeRowsBids.ExecuteReader();
                        cmdChangeRowsOffers = new SqlCommand(strSQLOffer, conn2);
                        dtrChangeRowsOffers = cmdChangeRowsOffers.ExecuteReader();
                    }

                    // placeheld hack
                    string thegene;
                    string thefob;
                    //Response.Write("Bids" +strSQLBid +"<BR><BR>");
                    //Response.Write("Offers" +strSQLOffer);

                    strSQL = ""; // blank the string for reuse
                    if (ddlBidOffer.SelectedItem.Value.ToString().Equals("0"))
                    { // bids
                        while (dtrChangeRowsBids.Read())
                        {
                            strSQL += " Exec spTransactionEngine_P @Comp_Id=" + dtrChangeRowsBids[8].ToString() + ", @Pers_Id=" + dtrChangeRowsBids[1].ToString() + ", @Cont_Id=" + dtrChangeRowsBids[2].ToString() + ", @Qty=" + dtrChangeRowsBids[5].ToString().ToString() + ", @Price=" + dtrChangeRowsBids[10].ToString() + ", @Size='1', @Month=" + dtrChangeRowsBids[9].ToString() + ", @Order_Id=" + dtrChangeRowsBids[0].ToString() + ", @ErrMsg = NULL ";
                        }
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("1"))
                    { // offers
                        while (dtrChangeRowsOffers.Read())
                        {
                            if (dtrChangeRowsOffers[9].ToString() == "False")
                            {
                                thegene = "0";
                            }
                            else
                            {
                                thegene = "1";
                            }
                            if (dtrChangeRowsOffers[12].ToString() == "False")
                            {
                                thefob = "0";
                            }
                            else
                            {
                                thefob = "1";
                            }
                            strSQL += " Exec spTransactionEngine_S @CompId=" + dtrChangeRowsOffers[10].ToString() + ", @PersId=" + dtrChangeRowsOffers[1].ToString() + ", @ContId=" + dtrChangeRowsOffers[3].ToString() + ", @Qty=" + dtrChangeRowsOffers[6].ToString() + ", @Price=" + dtrChangeRowsOffers[14].ToString() + ", @Size='1', @Month=" + dtrChangeRowsOffers[13].ToString() + ", @FOB=" + thefob + ",@IdOrder=" + dtrChangeRowsOffers[0].ToString() + ", @Gene=" + thegene;
                        }
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("both"))
                    { // both
                        while (dtrChangeRowsOffers.Read())
                        {
                            if (dtrChangeRowsOffers[9].ToString() == "False")
                            {
                                thegene = "0";
                            }
                            else
                            {
                                thegene = "1";
                            }
                            if (dtrChangeRowsOffers[12].ToString() == "False")
                            {
                                thefob = "0";
                            }
                            else
                            {
                                thefob = "1";
                            }
                            strSQL += " Exec spTransactionEngine_S @CompId=" + dtrChangeRowsOffers[10].ToString() + ", @PersId=" + dtrChangeRowsOffers[1].ToString() + ", @ContId=" + dtrChangeRowsOffers[3].ToString() + ", @Qty=" + dtrChangeRowsOffers[6].ToString() + ", @Price=" + dtrChangeRowsOffers[14].ToString() + ", @Size='1', @Month=" + dtrChangeRowsOffers[13].ToString() + ", @FOB=" + thefob + ",@IdOrder=" + dtrChangeRowsOffers[0].ToString() + ", @Gene=" + thegene;
                        }
                        while (dtrChangeRowsBids.Read())
                        {
                            strSQL += " Exec spTransactionEngine_P @Comp_Id=" + dtrChangeRowsBids[8].ToString() + ", @Pers_Id=" + dtrChangeRowsBids[1].ToString() + ", @Cont_Id=" + dtrChangeRowsBids[2].ToString() + ", @Qty=" + dtrChangeRowsBids[5].ToString().ToString() + ", @Price=" + dtrChangeRowsBids[10].ToString() + ", @Size='1', @Month=" + dtrChangeRowsBids[9].ToString() + ", @Order_Id=" + dtrChangeRowsBids[0].ToString() + ", @ErrMsg = NULL ";
                        }
                    }

                    if (ddlBidOffer.SelectedItem.Value.ToString().Equals("0"))
                    { // bids
                        dtrChangeRowsBids.Close();
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("1"))
                    { // offers

                        dtrChangeRowsOffers.Close();
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("both"))
                    { // both
                        dtrChangeRowsBids.Close();
                        dtrChangeRowsOffers.Close();
                    }
                    if (strSQL != "")
                    {
                        cmdChangeRows = new SqlCommand(strSQL, conn); // reuse the declared object
                        //Response.Write(strSQL);
                        cmdChangeRows.ExecuteNonQuery();
                    }
                }
                finally
                {
                    conn.Close();
                    conn2.Close();
                }
            }

            Data_Bind();
        }


        private void UpdateChainResin()
        {
            string strUserId;

            string strChange_Price = "";
            string strSQL = "";
            string strSQLBid = "";
            string strSQLOffer = "";
            SqlConnection conn;
            SqlConnection conn2;
            SqlCommand cmdChangeRows;
            SqlCommand cmdChangeRowsBids;
            SqlCommand cmdChangeRowsOffers;
            SqlDataReader dtrChangeRows;
            SqlDataReader dtrChangeRowsBids;
            SqlDataReader dtrChangeRowsOffers;
            dtrChangeRowsBids = null;
            dtrChangeRowsOffers = null;
            conn = new SqlConnection(Application["DBconn"].ToString());
            conn2 = new SqlConnection(Application["DBconn"].ToString());

            if (ddlRaiseLower.SelectedItem.Value.ToString().Equals("Lower"))
            {
                strChange_Price = "-." + UpDownPrc.Text;
            }
            else
            {
                strChange_Price = "+." + UpDownPrc.Text;
            }
            if (ddlBidOffer.SelectedItem.Value.ToString().Equals("0"))
            { // bids
                strUserId = "430"; // hard coded for mgbuy
                strSQLBid = "SELECT Bid_Id,Bid_Buyr,Bid_Cont,Bid_To,Bid_To_Locl,Bid_Qty,Bid_Prce,Bid_Size,Bid_Buyr_Comp,Bid_mnth,EXPR = BID_PRCE" + strChange_Price + " FROM BID WHERE BID_MNTH between'" + ddlFromMonth.SelectedItem.Value.ToString() + "' AND '" + ddlToMonth.SelectedItem.Value.ToString() + "' AND BID_BUYR='" + strUserId + "' AND BID_SIZE='1' AND  BID_CONT =' " + ddlResinFilter.SelectedValue.ToString() + "'" ;

            }
            else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("1"))
            { // offers
                strUserId = "429"; // hard coded for mgsell                
                strSQLOffer = "SELECT Offr_Id,Offr_Selr,Offr_Bran,Offr_Cont,Offr_from,Offr_From_Locl,Offr_Qty,Offr_Prce,Offr_Size,Offr_Gene,Offr_Selr_Comp,Offr_Bran_Comp,Offr_fob,Offr_mnth,EXPR = OFFR_PRCE+" + strChange_Price + " FROM OFFER WHERE OFFR_SELR=" + strUserId + " AND OFFR_MNTH between'" + ddlFromMonth.SelectedItem.Value.ToString() + "' AND '" + ddlToMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1' AND  OFFR_CONT =' " + ddlResinFilter.SelectedValue.ToString() + "'";
            }
            else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("both"))
            { // both
                strUserId = "430"; // hard coded for mgbuy

                strSQLBid = "SELECT Bid_Id,Bid_Buyr,Bid_Cont,Bid_To,Bid_To_Locl,Bid_Qty,Bid_Prce,Bid_Size,Bid_Buyr_Comp,Bid_mnth,EXPR = BID_PRCE" + strChange_Price + " FROM BID WHERE BID_MNTH between'" + ddlFromMonth.SelectedItem.Value.ToString() + "' AND '" + ddlToMonth.SelectedItem.Value.ToString() + "' AND BID_BUYR='" + strUserId + "' AND BID_SIZE='1' AND  BID_CONT =' " + ddlResinFilter.SelectedValue.ToString() + "'";
                strUserId = "429"; // hard coded for mgsell
                strSQLOffer = "SELECT Offr_Id,Offr_Selr,Offr_Bran,Offr_Cont,Offr_from,Offr_From_Locl,Offr_Qty,Offr_Prce,Offr_Size,Offr_Gene,Offr_Selr_Comp,Offr_Bran_Comp,Offr_fob,Offr_mnth,EXPR = OFFR_PRCE+" + strChange_Price + " FROM OFFER WHERE OFFR_SELR=" + strUserId + " AND OFFR_MNTH between'" + ddlFromMonth.SelectedItem.Value.ToString() + "' AND '" + ddlToMonth.SelectedItem.Value.ToString() + "' AND OFFR_SIZE='1' AND  OFFR_CONT =' " + ddlResinFilter.SelectedValue.ToString() + "'";
            }

            //RowSelectorColumn rsc;
            //rsc = RowSelectorColumn.FindColumn(dg);
            //if (rsc.SelectedIndexes.Length > 0)
            //{
            //    for (int selectedIndex = 0; selectedIndex < rsc.SelectedIndexes.Length; selectedIndex++)
            //    {
            //        int selIndex = rsc.SelectedIndexes[selectedIndex];
            //        strSQLBid += dg.DataKeys[selIndex].ToString() + ",";
            //        strSQLOffer += dg.DataKeys[selIndex].ToString() + ",";
            //    }
            //    // remove last comma and close up statement
            //    strSQLBid = strSQLBid.Substring(0, strSQLBid.Length - 1) + ")";
            //    strSQLOffer = strSQLOffer.Substring(0, strSQLOffer.Length - 1) + ")";

                conn.Open();
                conn2.Open();
                try
                {
                    if (ddlBidOffer.SelectedItem.Value.ToString().Equals("0"))
                    { // bids
                        cmdChangeRowsBids = new SqlCommand(strSQLBid, conn);
                        dtrChangeRowsBids = cmdChangeRowsBids.ExecuteReader();
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("1"))
                    { // offers
                        cmdChangeRowsOffers = new SqlCommand(strSQLOffer, conn2);
                        dtrChangeRowsOffers = cmdChangeRowsOffers.ExecuteReader();
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("both"))
                    { // both
                        cmdChangeRowsBids = new SqlCommand(strSQLBid, conn);
                        dtrChangeRowsBids = cmdChangeRowsBids.ExecuteReader();
                        cmdChangeRowsOffers = new SqlCommand(strSQLOffer, conn2);
                        dtrChangeRowsOffers = cmdChangeRowsOffers.ExecuteReader();
                    }

                    // placeheld hack
                    string thegene;
                    string thefob;
                    
                    strSQL = ""; // blank the string for reuse
                    if (ddlBidOffer.SelectedItem.Value.ToString().Equals("0"))
                    { // bids
                        while (dtrChangeRowsBids.Read())
                        {
                            strSQL += " Exec spTransactionEngine_P @Comp_Id=" + dtrChangeRowsBids[8].ToString() + ", @Pers_Id=" + dtrChangeRowsBids[1].ToString() + ", @Cont_Id=" + dtrChangeRowsBids[2].ToString() + ", @Qty=" + dtrChangeRowsBids[5].ToString().ToString() + ", @Price=" + dtrChangeRowsBids[10].ToString() + ", @Size='1', @Month=" + dtrChangeRowsBids[9].ToString() + ", @Order_Id=" + dtrChangeRowsBids[0].ToString() + ", @ErrMsg = NULL ";
                        }
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("1"))
                    { // offers
                        while (dtrChangeRowsOffers.Read())
                        {
                            if (dtrChangeRowsOffers[9].ToString() == "False")
                            {
                                thegene = "0";
                            }
                            else
                            {
                                thegene = "1";
                            }
                            if (dtrChangeRowsOffers[12].ToString() == "False")
                            {
                                thefob = "0";
                            }
                            else
                            {
                                thefob = "1";
                            }
                            strSQL += " Exec spTransactionEngine_S @CompId=" + dtrChangeRowsOffers[10].ToString() + ", @PersId=" + dtrChangeRowsOffers[1].ToString() + ", @ContId=" + dtrChangeRowsOffers[3].ToString() + ", @Qty=" + dtrChangeRowsOffers[6].ToString() + ", @Price=" + dtrChangeRowsOffers[14].ToString() + ", @Size='1', @Month=" + dtrChangeRowsOffers[13].ToString() + ", @FOB=" + thefob + ",@IdOrder=" + dtrChangeRowsOffers[0].ToString() + ", @Gene=" + thegene;
                        }
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("both"))
                    { // both
                        while (dtrChangeRowsOffers.Read())
                        {
                            if (dtrChangeRowsOffers[9].ToString() == "False")
                            {
                                thegene = "0";
                            }
                            else
                            {
                                thegene = "1";
                            }
                            if (dtrChangeRowsOffers[12].ToString() == "False")
                            {
                                thefob = "0";
                            }
                            else
                            {
                                thefob = "1";
                            }
                            strSQL += " Exec spTransactionEngine_S @CompId=" + dtrChangeRowsOffers[10].ToString() + ", @PersId=" + dtrChangeRowsOffers[1].ToString() + ", @ContId=" + dtrChangeRowsOffers[3].ToString() + ", @Qty=" + dtrChangeRowsOffers[6].ToString() + ", @Price=" + dtrChangeRowsOffers[14].ToString() + ", @Size='1', @Month=" + dtrChangeRowsOffers[13].ToString() + ", @FOB=" + thefob + ",@IdOrder=" + dtrChangeRowsOffers[0].ToString() + ", @Gene=" + thegene;
                        }
                        while (dtrChangeRowsBids.Read())
                        {
                            strSQL += " Exec spTransactionEngine_P @Comp_Id=" + dtrChangeRowsBids[8].ToString() + ", @Pers_Id=" + dtrChangeRowsBids[1].ToString() + ", @Cont_Id=" + dtrChangeRowsBids[2].ToString() + ", @Qty=" + dtrChangeRowsBids[5].ToString().ToString() + ", @Price=" + dtrChangeRowsBids[10].ToString() + ", @Size='1', @Month=" + dtrChangeRowsBids[9].ToString() + ", @Order_Id=" + dtrChangeRowsBids[0].ToString() + ", @ErrMsg = NULL ";
                        }
                    }

                    if (ddlBidOffer.SelectedItem.Value.ToString().Equals("0"))
                    { // bids
                        dtrChangeRowsBids.Close();
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("1"))
                    { // offers

                        dtrChangeRowsOffers.Close();
                    }
                    else if (ddlBidOffer.SelectedItem.Value.ToString().Equals("both"))
                    { // both
                        dtrChangeRowsBids.Close();
                        dtrChangeRowsOffers.Close();
                    }
                    if (strSQL != "")
                    {
                        cmdChangeRows = new SqlCommand(strSQL, conn); // reuse the declared object
                        //Response.Write(strSQL);
                        cmdChangeRows.ExecuteNonQuery();
                    }
                }
                finally
                {
                    conn.Close();
                    conn2.Close();
                }
            

            Data_Bind();
        }

        private void UpdateOneNormalResin(DataGridCommandEventArgs e)
        {
            // Constant for the brands
            // we list only TPE prime
            int iBaseBrand = 1090;
            // update offers
            Hashtable htParams = new Hashtable();
            htParams.Add("@OFFR_CONT", dg.DataKeys[e.Item.ItemIndex].ToString());
            htParams.Add("@OFFR_MNTH", ddlFWDMonth.SelectedItem.Value.ToString());
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), "DELETE FROM OFFER WHERE OFFR_CONT =@OFFR_CONT AND OFFR_MNTH=@OFFR_MNTH", htParams);

            htParams.Clear();
            htParams.Add("@PersId", "429");
            htParams.Add("@CompId", "149");
            htParams.Add("@Size", "1");
            htParams.Add("@FOB", "1");
            htParams.Add("@Qty", ((TextBox)e.Item.Cells[6].Controls[3]).Text);
            htParams.Add("@Month", ddlFWDMonth.SelectedItem.Value.ToString());
            htParams.Add("@Price", ((TextBox)e.Item.Cells[5].Controls[3]).Text);
            htParams.Add("@Brand", (iBaseBrand + Convert.ToInt32(dg.DataKeys[e.Item.ItemIndex])).ToString());
            htParams.Add("@ContId", dg.DataKeys[e.Item.ItemIndex].ToString());
            DBLibrary.ExecuteStoredProcedure(Application["DBconn"].ToString(), "spTransactionEngine_S", htParams); //EcmdUpdate = new SqlCommand(sbSQL.ToString(),conn);

            // update bids
            htParams.Clear();
            htParams.Add("@BID_CONT", dg.DataKeys[e.Item.ItemIndex].ToString());
            htParams.Add("@BID_MNTH", ddlFWDMonth.SelectedItem.Value.ToString());            
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), "DELETE FROM BID WHERE BID_CONT =@BID_CONT AND BID_MNTH=@BID_MNTH", htParams);

            htParams.Clear();
            htParams.Add("@Pers_Id", "430");
            htParams.Add("@Comp_Id", "148");
            htParams.Add("@Size", "1");
            htParams.Add("@ErrMsg", "NULL");
            htParams.Add("@Qty", ((TextBox)e.Item.Cells[3].Controls[3]).Text);
            htParams.Add("@Month", ddlFWDMonth.SelectedItem.Value.ToString());
            htParams.Add("@Price", ((TextBox)e.Item.Cells[4].Controls[3]).Text);
            htParams.Add("@Cont_Id", dg.DataKeys[e.Item.ItemIndex].ToString());
            DBLibrary.ExecuteStoredProcedure(Application["DBconn"].ToString(), "spTransactionEngine_P", htParams);

            dg.EditItemIndex = -1;
            Data_Bind();
        }


        private void UpdateMonthResin(DataGridCommandEventArgs e)
        {

            // Constant for the brands
            // we list only TPE prime
            int iBaseBrand = 1090;
            // update offers
            Hashtable htParams = new Hashtable();
            
            htParams.Add("@OFFR_ID", e.Item.Cells[7].Text);            
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), "DELETE FROM OFFER WHERE offr_id =@OFFR_ID ", htParams);

            htParams.Clear();
            htParams.Add("@PersId", "429");
            htParams.Add("@CompId", "149");
            htParams.Add("@Size", "1");
            htParams.Add("@FOB", "1");
            htParams.Add("@Qty", ((TextBox)e.Item.Cells[5].Controls[3]).Text);
            htParams.Add("@Month", e.Item.Cells[8].Text.ToString());
            htParams.Add("@Price", ((TextBox)e.Item.Cells[4].Controls[3]).Text);
            htParams.Add("@Brand", (iBaseBrand + Convert.ToInt32(dg.DataKeys[e.Item.ItemIndex])).ToString());
            htParams.Add("@ContId", dgFuture.DataKeys[e.Item.ItemIndex].ToString());
            DBLibrary.ExecuteStoredProcedure(Application["DBconn"].ToString(), "spTransactionEngine_S", htParams); //EcmdUpdate = new SqlCommand(sbSQL.ToString(),conn);

            // update bids
            htParams.Clear();
            htParams.Add("@BID_ID", e.Item.Cells[6].Text);
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), "DELETE FROM BID WHERE BID_id =@BID_ID", htParams);

            htParams.Clear();
            htParams.Add("@Pers_Id", "430");
            htParams.Add("@Comp_Id", "148");
            htParams.Add("@Size", "1");
            htParams.Add("@ErrMsg", "NULL");
            htParams.Add("@Qty", ((TextBox)e.Item.Cells[2].Controls[3]).Text);
            htParams.Add("@Month", e.Item.Cells[8].Text.ToString());
            htParams.Add("@Price", ((TextBox)e.Item.Cells[3].Controls[3]).Text);
            htParams.Add("@Cont_Id", dgFuture.DataKeys[e.Item.ItemIndex].ToString());
            DBLibrary.ExecuteStoredProcedure(Application["DBconn"].ToString(), "spTransactionEngine_P", htParams);

            dgFuture.EditItemIndex = -1;
            Data_Bind();
        
        }



        //Charts
        int ChartWidth = 640; 

        private void createGradeChart()
        {
            string grade_id = ddlResinFilter.SelectedValue;

            Chart.ChartArea.ClearColors();
            Chart.SeriesCollection.Clear();
            Chart.LegendBox.ClearColors();
            //Chart.XAxis.Clear();
            //Chart.YAxis.Clear();
            
            Chart.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            Chart.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);

            //Chart.ChartArea.YAxis.StaticColumnWidth = 1;

            // Set the chart type            
            Chart.Type = ChartType.Combo;
            
            //Chart.OverlapFooter = true;
            //Chart.Mentor = false;
            // Set the size
            Chart.Width = ChartWidth;
            Chart.Height = 320;
            // Set the temp directory
            Chart.TempDirectory = "temp";
            // Debug mode. ( Will show generated errors if any )
            Chart.Debug = false;
            Chart.Title = "Forward Curve: " + ddlResinFilter.SelectedItem;
    
            //Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;// = false;
            Chart.TitleBox.Position = TitleBoxPosition.Full;
            
            Chart.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
     
            //Chart.YAxis.Line.EndCap = System.Drawing.Drawing2D.LineCap.Di//amondAnchor;

            Chart.DefaultElement.Marker.Visible = true;

            Color[] MyColorObjectArray = new Color[3] { Color.Red, Color.FromArgb(255, 202, 0), Color.Yellow };
            Chart.Palette = MyColorObjectArray;

            Chart.ChartAreaSpacing = 3;
            Chart.LegendBox.Visible = false;
            //Chart.LegendBox.Template = "%Icon%Name";

            // Modify the x axis labels.
            Chart.XAxis.TimeScaleLabels.DayFormatString = "p";
            Chart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Year);
            Chart.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            Chart.ChartArea.XAxis.TickLabelAngle = 90;

            //Chart.DefaultElement.ShowValue = true;
            //Chart.XAxis.DefaultTick.Label.Text = "<%Value,mmm>";
            Chart.ChartArea.XAxis.LabelRotate = true;
            Chart.XAxis.TimeScaleLabels.MonthFormatString = "MMM";
            //Chart.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            //ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            //ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM<BR>yyyy";
            //ChartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>yyyy";
            Chart.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.White;
            Chart.XAxis.TimeInterval = TimeInterval.Month;

           //Chart.DefaultElement.ShowValue = true;

            // Setup the axes.
            Chart.YAxis.Label.Text = "Price";
            Chart.YAxis.FormatString = "$0.000#;";
            Chart.YAxis.Scale = Scale.Range;
            Chart.YAxis.Interval = 0.005;
            
            //Get Data
            SeriesCollection mySC = getPriceData(grade_id);

            int i = mySC.Count;

            
            for (int x=0; x<i;x++)
            {
                mySC[x].Type = SeriesTypeFinancial.Bar;
            }

            
            

            // Add the price data.
            Chart.SeriesCollection.Add(mySC);

            //Chart Style
            ChartStyle(Chart);

        }

        private SeriesCollection getPriceData(string grade_id)
        {
            SeriesCollection SC = new SeriesCollection();
            Series s = new Series();
            s.Name = "Price Range";

            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = null;   
                string sSql = "SELECT (FWD_MNTH + ' ' + RIGHT(FWD_YEAR,4))as Month, FWDMONTH.FWD_ID, CONTRACT.CONT_LABL, CONTRACT.CONT_ID, BID.BID_PRCE as price_low , OFFER.OFFR_PRCE as price_high, CONTRACT.CONT_ID FROM BID INNER JOIN CONTRACT ON BID.BID_CONT = CONTRACT.CONT_ID INNER JOIN OFFER ON CONTRACT.CONT_ID = OFFER.OFFR_CONT AND BID.BID_MNTH = OFFER.OFFR_MNTH INNER JOIN FWDMONTH ON BID.BID_MNTH = FWDMONTH.FWD_ID WHERE(CONTRACT.CONT_ENABLED = 'True'  AND contract.cont_id='" + grade_id + "') ORDER BY FWDMONTH.FWD_ID";

                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSql);

                while (dtr.Read())
                {          
                    DateTime date = Convert.ToDateTime(dtr["month"].ToString());
                    Element e = new Element();
                    e.XDateTime = date;                    

                    double price_low = Convert.ToDouble(dtr["price_low"].ToString());
                    double price_high = Convert.ToDouble(dtr["price_high"].ToString());

                    //e.YValueStart = price_low;
                    //e.YValue = price_high;


                    e.Close = price_high;
                    e.Open = price_low;

                    e.High = price_high;
                    e.Low = price_low;

                    s.Elements.Add(e);   
                }
            }

            SC.Add(s);            
            return (SC);
        }

        private void ChartStyle(dotnetCHARTING.Chart CurrChart)
        {

            //Chart
            CurrChart.TitleBox.Background.Color = Color.Orange;
            CurrChart.Background.Color = Color.Black;
            CurrChart.ChartArea.Background.Color = Color.Black;
            CurrChart.ChartArea.DefaultSeries.Line.Color = Color.Red;
            //CurrChart.ChartArea.DefaultSeries.Line.StartCap = System.Drawing.Drawing2D.LineCap.Triangle;
            //CurrChart.ChartArea.DefaultSeries.Line.EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
            //CurrChart.ChartArea.DefaultSeries.Line.Width = 2;

            CurrChart.ChartArea.Line.Color = Color.FromArgb(255, 255, 0, 0);//(254, 0, 0)
            CurrChart.ChartArea.Line.Color = Color.Orange;
            CurrChart.ChartArea.Line.Width = 2;

            //XAxis
            CurrChart.XAxis.Label.Color = Color.White;
            CurrChart.ChartArea.XAxis.Label.Color = Color.White;
            CurrChart.XAxis.Line.Color = Color.Orange;
            CurrChart.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;

            //YAxis
            CurrChart.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            CurrChart.ChartArea.YAxis.Label.Color = Color.White;
            CurrChart.YAxis.Line.Color = Color.Orange;
            CurrChart.YAxis.Label.Color = Color.White;

        }


        //Create Historical Chart Main
        private void createHistoricalGradeChart()
        {         
            ChartObj.ChartArea.ClearColors();
            ChartObj.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartObj.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            
            ChartObj.OverlapFooter = true;
            ChartObj.Mentor = false;    
            ChartObj.TempDirectory = "temp";
            ChartObj.Width = ChartWidth;
            ChartObj.Height = 320;           
            ChartObj.FileManager.ImageFormat = ImageFormat.Png;
            // Set the chart type                        
            ChartObj.Type = ChartType.Combo;

            ChartObj.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            ChartObj.DefaultSeries.DefaultElement.Marker.Size = 6;
            ChartObj.DefaultElement.Marker.Visible = true;
            ChartObj.DefaultSeries.DefaultElement.Marker.Color = Color.White;//Color.FromArgb( 255, 255, 153, 9);
            ChartObj.XAxis.Label.Color = Color.White;

            // Debug mode. ( Will show generated errors if any )
            ChartObj.Debug = true;
            //ChartObj.Title = "Historical Pricing: " + ddlResinFilter.SelectedItem + "   Month: " + ddlHistoricalChartDate.SelectedItem;
            ChartObj.Title = "Historical Pricing: " + ddlResinFilter.SelectedItem;


            ChartObj.TitleBox.Position = TitleBoxPosition.FullWithLegend;
            //ChartObj.DefaultElement.Marker.Visible = false;

            Color[] MyColorObjectArray = new Color[3] { Color.Red, Color.FromArgb(255, 202, 0), Color.Yellow };
            ChartObj.Palette = MyColorObjectArray;

            ChartObj.ChartAreaSpacing = 3;
            ChartObj.LegendBox.Template = "%Icon%Name";
            ChartObj.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;

//            // Modify the x axis labels.
            ChartObj.XAxis.TimeInterval = TimeInterval.Day;
            ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            ChartObj.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            //ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);
            ChartObj.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartObj.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartObj.ChartArea.XAxis.TickLabelAngle = 90;

            ChartObj.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartObj.ChartArea.XAxis.LabelRotate = true;            
            ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            //ChartObj.XAxis.TimeInterval = TimeInterval.Month;


            //XAxis
            ChartObj.XAxis.Label.Color = Color.White;
            ChartObj.ChartArea.XAxis.Label.Color = Color.White;            
            ChartObj.XAxis.Line.Color = Color.Orange;
            ChartObj.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;

            ChartObj.YAxis.Interval = 0.01;
            // Setup the axes.
            ChartObj.YAxis.Label.Text = "Price";
            ChartObj.YAxis.FormatString = "Currency";
            ChartObj.YAxis.Scale = Scale.Range;

            ChartObj.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartObj.ChartArea.YAxis.Label.Color = Color.White;
            ChartObj.YAxis.Line.Color = Color.Orange;
            ChartObj.YAxis.Label.Color = Color.White;



            //Chart
            ChartObj.TitleBox.Background.Color = Color.Orange;
            ChartObj.Background.Color = Color.Black;
            ChartObj.ChartArea.Background.Color = Color.Black;  
            ChartObj.ChartArea.DefaultSeries.Line.Color = Color.Red;
            //CurrChart.ChartArea.DefaultSeries.Line.StartCap = System.Drawing.Drawing2D.LineCap.Triangle;
            //CurrChart.ChartArea.DefaultSeries.Line.EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
            ChartObj.ChartArea.DefaultSeries.Line.Width = 2;

            ChartObj.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
            ChartObj.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);

            //CurrChart.ChartArea.Line.Color = Color.FromArgb(255, 255, 0, 0);//(254, 0, 0)
            ChartObj.ChartArea.Line.Color = Color.Orange;
            //CurrChart.ChartArea.Line.Width = 2;

            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();
            ChartObj.SeriesCollection.Add(getPriceDataHistoricalChart(ddlResinFilter.SelectedValue));

        }

        //Data - Range
        private SeriesCollection getPriceDataHistoricalChart(string grade_id)
        {                                    
            string sChartLen="";
            string sChartSpotLen="";
            
            switch (ddlHistoricalChartLength.SelectedValue)
            {
                case "0":
                    sChartLen = "date > DATEADD(dd,-30,getdate())";
                    sChartSpotLen= "export_price.date > DATEADD(dd,-30,getdate())";
                    break;

                case "1":
                    sChartLen = "date > DATEADD(dd,-60,getdate())";
                    sChartSpotLen= "export_price.date > DATEADD(dd,-60,getdate())";
                    break;

                case "2":
                    sChartLen = "date > DATEADD(dd,-180,getdate())";
                    sChartSpotLen= "export_price.date > DATEADD(dd,-180,getdate())";
                    break;

                case "3":
                    sChartLen = "date > DATEADD(dd,-365,getdate())";
                    sChartSpotLen = "export_price.date > DATEADD(dd,-365,getdate())";
                    break;
            }

            //string sSql = "select (FWD_MNTH + ' ' + RIGHT(FWD_YEAR,4))as Month,forward_price.cont_id,contract.cont_labl,forward_price.bid ,forward_price.ask,forward_price.date from forward_price,fwdmonth,contract where CONTRACT.CONT_ID='" + grade_id + "' and fwdmonth='" + ddlHistoricalChartDate.SelectedItem.Value.ToString() + "' and fwdmonth.fwd_actv='True' and forward_price.fwdmonth=fwdmonth.fwd_id and contract.cont_id=forward_price.cont_id and " + sChartLen + " order by fwdmonth ";
            //string sSpotMarket = "select distinct EXPORT_PRICE.ask,export_price.date from EXPORT_PRICE, forward_price where EXPORT_PRICE.cont_id='" + grade_id + "'  and forward_price.fwdmonth='" + ddlHistoricalChartDate.SelectedItem.Value.ToString() + "'  and EXPORT_PRICE.cont_id =forward_price.cont_id and export_price.date >=forward_price.date and " +  sChartSpotLen +" order by EXPORT_PRICE.date ";

            string sSql = "select convert(char,date,110) as finaldate, bid, ask from export_price where cont_id=1 and date > '08/22/2007' and datepart(dw,date)=6 order by date desc ";
            string sSpotMarket = "select ask, weightdate from SpotWeightAvg where contid=1 and weightdate > '08/22/2007' and datepart(dw,weightdate)=6 order by weightdate desc";

            //string sSpotMarket = "Select convert(char,offr_date,110) as date, sum((offr_qty*offr_size)*offr_prce) / sum(offr_qty*offr_size) WAvg from BBOFFERARCH where offr_date > DATEADD(dd,-180,getdate()) and offr_grade=1  group by convert(char,offr_date,110) ";


            SeriesCollection SC = new SeriesCollection();

            Series sPriceRange = new Series();
            Series sBidPrice = new Series();

            sPriceRange.Name = "Contract Range";
            sBidPrice.Name = "Weighted Spot Ask Price";




            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = null;                
                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSql);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["finaldate"].ToString());
                    Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Price Range Element
                    ePriceRange.XDateTime = date;

                    ePriceRange.Close = Convert.ToDouble(dtr["ask"].ToString());
                    ePriceRange.Open = Convert.ToDouble(dtr["bid"].ToString());
                    ePriceRange.Name = Convert.ToDateTime(dtr["finaldate"]).ToString("MMM dd\r\nyyy");
                    sPriceRange.Elements.Add(ePriceRange);
                }
            }

            SC.Add(sPriceRange);

            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = null;
                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSpotMarket);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["weightdate"].ToString());
                    Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Bid Element
                    eBidPrice.ToolTip = dtr["ask"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["weightdate"]).ToString("MMM dd\r\nyyy");
                    sBidPrice.AddElements(eBidPrice);
                }

            }

            SC.Add(sBidPrice);

            SC[0].Type = SeriesTypeFinancial.CandleStick;
            SC[0].Line.Width = 1;
            
            SC[1].Type = SeriesType.Line;

            return (SC);
        }

        public void Update_HistoricalPriceChart(object sender, EventArgs e)
        {
           // getHistoricalData(ddlResinFilter.SelectedValue);
        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
