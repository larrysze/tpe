<%@ Page Language="c#" CodeBehind="Admin_Forward_Floor2.aspx.cs" AutoEventWireup="false" Inherits="localhost.Forward.Admin_Forward_Floor2" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>


<form runat="server" id="Form">
    <TPE:Template PageTitle="Contact Us" Runat="Server" />
    <script>

    function menuP(n,txt,flag,down)
    // script for adding the raise or lower number in the checkbox by one
    {
	   if(flag==1)
	   {
		  Obj=n
		  wherex=315
		  wherey=60

	  eval(zz+Obj+cc+"<DIV id=Test style=\"position:absolute; left:"+wherex+"px; top:"+wherey+"px; visibility: inherit; overflow: visible\"><img name=P border=0 width=191 src=\"/images/specifications/"+(Obj=='Truc'?"TL":"RC")+"Specs.gif\"></DIV>'");
	   }
	}

    function Trim(Para,Flag)
    {
	   Arr=new Array('0','0','0')
	   for(i=0;i<Para.value.length;i++)
		  if(Para.value.charAt(i)<'0'||Para.value.charAt(i)>'9')
			 break
		  else
			 Arr[i]=Para.value.charAt(i)
	   Val=(i==Para.value.length)?((Arr[0]=='0')?((Arr[1]=='0')?parseInt(Arr[2]):parseInt(Arr[1]+Arr[2])):parseInt(Arr[0]+Arr[1]+Arr[2])):0
	   if(Flag==1)
		  {if(Val<999) Val+=1}
	   else if(Val>0)
		  Val-=1
	   Para.value=("000"+Val).substr(("000"+Val).length-3,3)
    }

	</script>

            <asp:ValidationSummary runat="server" HeaderText="There are problems with the following fields. Please correct the errors below." />


            <table border="0" align="center"">
                <tr>
                    <td align="left">
                        Product:
                        <asp:DropDownList id="ddlProduct" runat="server"
                            AutoPostBack="true" OnSelectedIndexChanged="Reset_Grid" />

                    </td>
                    <td align="right">
                        <img src="/images/misc/trainload.gif" border=0>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td valign="top" colspan="2">
                        <asp:DataGrid id="dg" runat="server"
                        CssClass = "DataGrid"
                        ItemStyle-CssClass="DataGridRow"
                        AlternatingItemStyle-CssClass = "DataGridRow_Alternate"
                        HeaderStyle-CssClass="DataGridHeader"
                        AutoGenerateColumns="false"
                        CellPadding="0"
                        DataKeyField="FWD_ID"
                        OnEditCommand="DG_Edit"
                        OnUpdateCommand="DG_Update"
                        OnCancelCommand="DG_Cancel"
                        EditItemStyle-BackColor="Silver"
                        Width="550px"
                        ShowFooter="false"    >
                             <Columns>
                                <mbrsc:RowSelectorColumn SelectionMode="Multiple"
                                        AllowSelectAll="true" ItemStyle-HorizontalAlign="center" />
                                <asp:editcommandcolumn edittext="Edit" canceltext="Cancel" updatetext="Update" />

                                <asp:BoundColumn
                                    DataField="Month" HeaderText="Contract" ReadOnly="True"
                                    ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left" />

                                 <asp:TemplateColumn HeaderText="Qty">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYBID") %>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:CompareValidator ControlToValidate="txtQTYBID" ErrorMessage="Silly rabit, quantity must be an Integer."
                                             Display="none" Operator="DataTypeCheck" Type="Integer" Runat="server" />
                                        <asp:TextBox id="txtQTYBID" Size="1" maxlength="2" Text='<%# DataBinder.Eval(Container, "DataItem.QTYBID") %>' runat="server" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtQTYBID" Display="none" ErrorMessage="Bid quantity can not be blank" />
                                        <asp:RangeValidator ControlToValidate="txtQTYBID" runat="server" ErrorMessage="Bid quantity must be greater than one."
                                            Display="none" type="Integer" MinimumValue ="1" MaximumValue = "100" />
                                    </EditItemTemplate>
                                 </asp:TemplateColumn>

                                 <asp:TemplateColumn HeaderText="Bid">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BID") %>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:CompareValidator ControlToValidate="txtBID" ErrorMessage="Silly rabit, bid must be an Integer."
                                             Display="none" Operator="DataTypeCheck" Type="Double" Runat="server" />
                                        <asp:TextBox Size="1" id="txtBID" maxlength="4" Text='<%# DataBinder.Eval(Container, "DataItem.BID") %>' runat="server" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtBID" Display="none" ErrorMessage="Bid price can not be blank" />
                                        <asp:RangeValidator ControlToValidate="txtBID" runat="server" ErrorMessage="Invalid bid amount."
                                            Display="none" type="Double" MinimumValue ="0" MaximumValue = ".999" />
                                    </EditItemTemplate>
                                 </asp:TemplateColumn>

                                 <asp:TemplateColumn HeaderText="Ask">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OFFR") %>' />
                                    </ItemTemplate>

                                    <EditItemTemplate>
                                        <asp:CompareValidator ControlToValidate="txtOFFER" ErrorMessage="Silly rabit, offer must be an Integer."
                                             Display="none" Operator="DataTypeCheck" Type="Double" Runat="server" />
                                        <asp:TextBox Size="1" id="txtOFFER" maxlength="4" Text='<%# DataBinder.Eval(Container, "DataItem.OFFR") %>' runat="server" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtOFFER" Display="none" ErrorMessage="Offer price can not be blank" />
                                        <asp:RangeValidator ControlToValidate="txtOFFER" runat="server" ErrorMessage="Invalid offer amount."
                                            Display="none" type="Double" MinimumValue ="0" MaximumValue = ".999" />
                                    </EditItemTemplate>
                                 </asp:TemplateColumn>

                                 <asp:TemplateColumn HeaderText="Qty">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTYOFFR") %>' />
                                    </ItemTemplate>

                                    <EditItemTemplate>
                                        <asp:CompareValidator ControlToValidate="txtQTYOFFER" ErrorMessage="Silly rabit, quantity must be an Integer."
                                             Display="none" Operator="DataTypeCheck" Type="Integer" Runat="server" />
                                        <asp:TextBox Size="1" id="txtQTYOFFER" maxlength="2" Text='<%# DataBinder.Eval(Container, "DataItem.QTYOFFR") %>' runat="server" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtQTYOFFER" Display="none" ErrorMessage="Offer quantity can not be blank" />
                                        <asp:RangeValidator ControlToValidate="txtQTYOFFER" runat="server" ErrorMessage="Offer quantity must be greater than one."
                                            Display="none" type="Integer" MinimumValue ="1" MaximumValue = "100" />

                                    </EditItemTemplate>
                                 </asp:TemplateColumn>



                            </Columns>
                        </asp:DataGrid>

                     </td>
                     <td valign="top">


                        <table border=0 cellpadding=0 cellspacing=0 bgcolor=#E8E8E8 width=231>

	                       <tr>
		                      <td bgcolor=white colspan=3 valign=center><br><br>
		                          <asp:DropDownList id="ddlRaiseLower" runat="server">
		                               <asp:ListItem value="Raise" text="Raise" />
		                               <asp:ListItem value="Lower" text="Lower" />
		                          </asp:DropDownList>
		                           selected
		                          <asp:DropDownList id="ddlBidOffer" runat="server">
		                               <asp:ListItem value="0" text="bids" />
		                               <asp:ListItem value="1" text="offers" />
		                          </asp:DropDownList>
		                          by <BR>

		                          $.<asp:TextBox id="UpDownPrc" size="3" runat="server" />
		                          <img src=../Images/buttons/white_updn.gif usemap=#Map border=0 align=absmiddle>
		                          <map name=Map><area shape=rect coords=0,0,20,9 href=Javascript:Trim(Form.UpDownPrc,1)>
		                              <area shape=rect coords=0,10,20,20 href=Javascript:Trim(Form.UpDownPrc,0)></map>
		                           per pound.
		                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		                         <asp:Button runat="server" Text="Update" class="tpebutton" onclick="Update_Price" /><br><br>

		                      </td>
	                       </tr>
                        <table>

                         <TPE:Web_Box Width="100%" Heading="<a href=mailto:info@ThePlasticsExchange.com> Questions? Contact Us</a>" Runat="Server" />


                                <li><a href=/docs/userguide.pdf target="_blank" Class=S1>User Guide</a></li>
                                <%if (Session["Typ"].ToString()=="S"){%>
                                <li><a href=/docs/seller.pdf target="_blank" Class=S1>Seller's Agreement (pdf)</a></li>
                                <li><a href=/docs/Seller_Agreement_NewWind.aspx target="_blank" Class=S1>Seller's Agreement (html)</a></li>
                                <%}else if (Session["Typ"].ToString()=="P"){%>
                                <li><a href=/docs/buyer.pdf target="_blank" Class=S1>Buyer's Agreement (pdf)</a></li>
                                <li><a href=/docs/Buyer_Agreement_NewWind.aspx target="_blank" Class=S1>Buyer's Agreement (html)</a></li>

                                <%}%>
                                <li><a href=javascript:sealWin=window.open("/docs/Rules_of_the_Exchange4PopUp.aspx","win",'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=700,height=500');void(0) Class=S1>Rules Of The Exchange</a></li>

                        <TPE:Web_Box Footer="true" Runat="Server" />
                     </td>
                 </tr>
                 

               </table>


<TPE:Template Footer="true" Runat="Server" />

</form>
