<%@ Register TagPrefix="blong" Namespace="blong.WebControls" Assembly="blong" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="default.aspx.vb" Inherits="WebChartProject.WebForm1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WebChart Demo</title>
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body ms_positioning="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="400" border="0">
				<TBODY>
					<TR>
						<TD colSpan="2"><font face="Arial"><FONT size="2"><STRONG>Change select properties and 
										click&nbsp;the "Make Chart" button to alter WebChart appearance. Many other 
										properties can be implemented for run-time manipulation.</STRONG></FONT>
								<br>
								<br>
								<P><blong:webchart id="WebChart1" title="WebChart Demonstration" runat="server" Format="Jpeg">
										<blong:WebChartItem AlternateValue="10" PrimaryValue="100"></blong:WebChartItem>
										<blong:WebChartItem AlternateValue="20" PrimaryValue="90"></blong:WebChartItem>
										<blong:WebChartItem Explode="True" AlternateValue="30" PrimaryValue="80"></blong:WebChartItem>
										<blong:WebChartItem AlternateValue="40" PrimaryValue="70"></blong:WebChartItem>
										<blong:WebChartItem AlternateValue="50" PrimaryValue="60"></blong:WebChartItem>
										<blong:WebChartItem AlternateValue="60" PrimaryValue="50"></blong:WebChartItem>
										<blong:WebChartItem AlternateValue="70" PrimaryValue="40"></blong:WebChartItem>
										<blong:WebChartItem AlternateValue="80" PrimaryValue="30"></blong:WebChartItem>
										<blong:WebChartItem AlternateValue="90" PrimaryValue="20"></blong:WebChartItem>
										<blong:WebChartItem AlternateValue="100" PrimaryValue="10"></blong:WebChartItem>
									</blong:webchart></P>
								<P>
									<TABLE id="Table2" borderColor="gainsboro" cellSpacing="0" cellPadding="2" width="100%" border="1">
										<TR>
											<TD><FONT size="1">Chart Type:</FONT></TD>
											<TD><asp:dropdownlist id="type" runat="server" Font-Size="XX-Small">
													<asp:ListItem Value="Bar">Bar</asp:ListItem>
													<asp:ListItem Value="Pie">Pie</asp:ListItem>
												</asp:dropdownlist><FONT size="1"></FONT></TD>
										</TR>
										<TR>
											<TD><FONT size="1">Chart Title:</FONT></TD>
											<TD><asp:textbox id="Title" runat="server" Font-Size="XX-Small" MaxLength="30">WebChart Demonstration</asp:textbox><FONT size="1"></FONT></TD>
										</TR>
										<TR>
											<TD><FONT size="1">Show Legend:</FONT></TD>
											<TD><asp:radiobuttonlist id="showlegend" runat="server" Font-Size="XX-Small" RepeatDirection="Horizontal">
													<asp:ListItem Value="true" Selected="True">Yes</asp:ListItem>
													<asp:ListItem Value="false">No</asp:ListItem>
												</asp:radiobuttonlist><FONT size="1"></FONT></TD>
										</TR>
										<TR>
											<TD><FONT size="1">Show Outlines:</FONT></TD>
											<TD><asp:radiobuttonlist id="outlines" runat="server" Font-Size="XX-Small" RepeatDirection="Horizontal">
													<asp:ListItem Value="true" Selected="True">Yes</asp:ListItem>
													<asp:ListItem Value="false">No</asp:ListItem>
												</asp:radiobuttonlist></TD>
										</TR>
										<TR>
											<TD><FONT size="1">Bar Primary Color:</FONT></TD>
											<TD><asp:dropdownlist id="pColor" runat="server" Font-Size="XX-Small"></asp:dropdownlist><FONT size="1"></FONT></TD>
										</TR>
										<TR>
											<TD><FONT size="1">Bar Alternate Color:</FONT></TD>
											<TD><asp:dropdownlist id="aColor" runat="server" Font-Size="XX-Small"></asp:dropdownlist></TD>
										</TR>
										<TR>
											<TD><FONT size="1">Bar Gridlines:</FONT></TD>
											<TD><asp:dropdownlist id="gridlines" runat="server" Font-Size="XX-Small">
													<asp:ListItem Value="Horizontal">Horizontal</asp:ListItem>
													<asp:ListItem Value="Numbered">Numbered</asp:ListItem>
													<asp:ListItem Value="Both" Selected="True">Both</asp:ListItem>
													<asp:ListItem Value="None">None</asp:ListItem>
												</asp:dropdownlist><FONT size="1"></FONT></TD>
										</TR>
										<TR>
											<TD><FONT size="1">Bar Values:</FONT></TD>
											<TD><asp:radiobuttonlist id="barvalues" runat="server" Font-Size="XX-Small" RepeatDirection="Horizontal">
													<asp:ListItem Value="true">Yes</asp:ListItem>
													<asp:ListItem Value="false" Selected="True">No</asp:ListItem>
												</asp:radiobuttonlist></TD>
										</TR>
										<TR>
											<TD><FONT size="1">Pie Chart Size:</FONT></TD>
											<TD>
												<asp:dropdownlist id="piesize" runat="server" Font-Size="XX-Small">
													<asp:ListItem Value="0">Smallest</asp:ListItem>
													<asp:ListItem Value="1">Smaller</asp:ListItem>
													<asp:ListItem Value="2">Small</asp:ListItem>
													<asp:ListItem Value="3" Selected="True">Medium</asp:ListItem>
													<asp:ListItem Value="4">Large</asp:ListItem>
													<asp:ListItem Value="5">Larger</asp:ListItem>
													<asp:ListItem Value="6">Largest</asp:ListItem>
												</asp:dropdownlist></TD>
										</TR>
										<TR>
											<TD><FONT size="1">Pie Chart Thickness:</FONT></TD>
											<TD>
												<asp:dropdownlist id="piethickness" runat="server" Font-Size="XX-Small">
													<asp:ListItem Value="0">None</asp:ListItem>
													<asp:ListItem Value="2">Wafer</asp:ListItem>
													<asp:ListItem Value="4">Thin</asp:ListItem>
													<asp:ListItem Value="8" Selected="True">Medium</asp:ListItem>
													<asp:ListItem Value="16">Thick</asp:ListItem>
													<asp:ListItem Value="32">Thickest</asp:ListItem>
												</asp:dropdownlist></TD>
										</TR>
									</TABLE>
									<BR>
									<asp:button id="submit" runat="server" Text="Make Chart"></asp:button>
							</font></STRONG></P></FONT></TD>
					</TR>
				</TBODY>
			</TABLE>
		</form>
	</body>
</HTML>
