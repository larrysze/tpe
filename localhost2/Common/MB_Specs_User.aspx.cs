using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TPE.Utility;

namespace localhost.Common
{
    public partial class MB_Specs_User : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.Width = "760px";
            string SQL = string.Empty;
            if (Session["Typ"].ToString() == "A" || Session["Typ"].ToString() == "AD" || Session["Typ"].ToString() == "C" || Session["Typ"].ToString() == "T" || Session["Typ"].ToString() == "B" || Session["Typ"].ToString() == "L")
            {
                SQL = "SELECT PERS_FRST_NAME,PERS_LAST_NAME,(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=PERS_COMP),PERS_TITL,PERS_MAIL,PERS_PHON FROM PERSON WHERE PERS_ID=" + Request.QueryString["Id"].ToString();
            }
            else
            {
                SQL = "SELECT PERS_FRST_NAME,PERS_LAST_NAME,(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=PERS_COMP),PERS_TITL,PERS_MAIL,PERS_PHON FROM PERSON WHERE (CASE '" + Session["Comp"] + "' WHEN '' THEN 0 ELSE '" + Session["Comp"] + "' END)=PERS_COMP AND PERS_ID=" + Request.QueryString["Id"];
            }
            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();
                using (SqlDataReader Rec0 = DBLibrary.GetDataReaderFromSelect(conn, SQL))
                {

                    if (!Rec0.HasRows)
                    {
                        Session["Message"] = "Unavailable.<br><font color=red>Need help? Call us at " + ConfigurationSettings.AppSettings["PhoneNumber"];
                        Server.Transfer("MB_Message.aspx");
                    }

                    string temp;
                    temp = "<td align=left nowrap width=100 CLASS='Content Bold Color2'>";
                    temp = temp + ", ";
                    temp = temp + "<td align=left nowrap WIDTH=250 Class='Content Color2'>";
                    string[] Str = temp.Split(',');
                    Rec0.Read();
                    if (Rec0[0] != "")
                    {
                        Str[0] = Str[0] + "First Name<br>";
                        Str[1] = Str[1] + Rec0[0] + "<br>";
                    }
                    if (Rec0[1] != "")
                    {
                        Str[0] = Str[0] + "Last name<br>";
                        Str[1] = Str[1] + Rec0[1] + "<br>";
                    }
                    if (Rec0[2] != "")
                    {
                        Str[0] = Str[0] + "Company<br>";
                        Str[1] = Str[1] + Rec0[2] + "<br>";
                    }
                    if (Rec0[3] != DBNull.Value)
                    {
                        if (Rec0[3] != "")
                        {
                            Str[0] = Str[0] + "Title<br>";
                            Str[1] = Str[1] + Rec0[3] + "<br>";
                        }
                    }
                    if (Rec0[4] != "")
                    {
                        Str[0] = Str[0] + "Email<br>";
                        Str[1] = Str[1] + Rec0[4] + "<br>";
                    }
                    if (Rec0[5] != DBNull.Value)
                    {
                        if (Rec0[5] != "")
                        {
                            Str[0] = Str[0] + "Phone<br>";
                            Str[1] = Str[1] + Rec0[5] + "<br>";
                        }
                        // IF Rec0(3) Is DBNull.Value Then
                    }
                    lblContent.Text = Str[0] + "</td>" + Str[1] + "</td>";
                }
            }
        }
    }
}
