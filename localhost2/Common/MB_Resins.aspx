<%@ Page Language="VB" %>
<%@ import Namespace="System.Data.SqlClient" %>
<script runat="server">

    Public Sub Page_Load
         'Database connection
         Dim conn As SqlConnection
         Dim cmdResin As SqlCommand
         Dim dtrResin As SqlDataReader
         conn = new SqlConnection(Application("DBconn"))
         conn.Open()

         Dim Pref as string ' The users preference
         'get the value of preference from querystring passed by Demo_Lead.aspx
         Pref = Request.QueryString("Pref")

         Dim strSQL As String
         Dim strOutput As String

         'Use "POWER" method to find preference
         strSQL="SELECT CONT_LABL FROM CONTRACT WHERE POWER(2,CONT_ID-1)&("+Pref+")<>0 ORDER BY CONT_ORDR"
         cmdResin = new SqlCommand(strSQL,conn)
         dtrResin = cmdResin.ExecuteReader()
         strOutput ="<table>"
         while dtrResin.Read()
         strOutput +="<tr><td>"&dtrResin("CONT_LABL")&"</td></tr>"
         End While
         strOutput +="</table>"
         phContent.Controls.Add (new LiteralControl(strOutput))
         conn.Close()
         dtrResin.Close()
    End Sub

</script>
<%@ import Namespace="System.Data.SqlClient" %>
<html>
	<head>
		<LINK href="/styleIE.css" rel="STYLESHEET" type="text/css">
	</head>
	<body onblur="focus()">
		<form runat="server">
			<asp:PlaceHolder id="phContent" runat="server" />
		</form>
	</body>
</html>
