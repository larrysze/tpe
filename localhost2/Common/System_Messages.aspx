

<%@ Page Language="VB" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Configuration" %>
<%@ import Namespace="System.Configuration" %>
<%@ import Namespace="System.Web.Mail" %>

<form runat="server" id="Form">
    <TPE:Template PageTitle="System Messsages" Runat="Server" />
    <!--#include FILE="../include/VBLegacy_Code.aspx"-->

<%
    'database connection
    Dim Rec0 as SqlDataReader
    DIM connM as SQLConnection
    dim mycomm as SqlCommand
    DIM Str,Str0,ThisParam,j,param,Id,CompBis,Typ,Labl,messfrom,pers,Sort as string
    dim i as integer
    Dim File As System.Io.StreamReader
    Dim line As String
    Dim mysplit As String ()=nothing

    'email subject
    Dim mail1 AS MailMessage
    mail1 = new MailMessage()

    'check if it's administrator
    IF Session("Typ").tostring()="" THEN
        Response.Redirect("../default.aspx")
    end if
    connM = new SqlConnection(Application("DBconn"))
    connM.open()

    'SET pFile = server.CreateObject("Scripting.FileSystemObject")
    IF Request.Form("Submit")<>"" AND Request.Form("Message")<>"" THEN
        IF Session("Typ")="A" OR Session("Typ")="T" OR Session("Typ")="C" OR Session("Typ")="B" THEN
            IF CDbl(Request.Form("Comp"))<1 THEN
                Str=Request.Form("Comp")
                Str0="0"
            ELSE
                Str=Request.Form("Dest")
                Str0=Request.Form("Comp")
            END IF
        ELSEIF Session("Typ")="P" OR Session("Typ")="S" THEN
            IF Request.Form("Dest")="-6" OR Request.Form("Dest")="-7" THEN Str0="0" ELSE Str0=Session("Comp")
            Str=Request.Form("Dest")
        ELSE
            IF CDbl(Request.Form("Dest"))<-5 THEN
                Str0="0"
            ELSEIF Session("Typ")="OS" AND Request.Form("Dest")="-2" THEN
                mycomm=new SqlCommand("SELECT COMP_ID FROM COMPANY WHERE COMP_NAME LIKE (SELECT COMP_NAME FROM COMPANY WHERE COMP_ID='"&Session("Comp")+"') AND COMP_REG=1 AND COMP_TYPE='P'",connM)
                Rec0=mycomm.ExecuteReader()
                Str0=Rec0(0)
                Rec0.Close()
            ELSEIF  Session("Typ")="OP" AND Request.Form("Dest")="-3" THEN
                mycomm=new SqlCommand("SELECT COMP_ID FROM COMPANY WHERE COMP_NAME LIKE (SELECT COMP_NAME FROM COMPANY WHERE COMP_ID='"+Session("Comp")+"') AND COMP_REG=1 AND COMP_TYPE<>'P'",connM)
                Rec0=mycomm.ExecuteReader()
                Str0=Rec0(0)
                Rec0.Close

            ELSEIF Session("Typ")="O" AND Request.Form("Dest")="-3" OR Request.Form("Dest")="-2" THEN
                Str0=Session("Comp")
            ELSE
                mycomm=new SqlCommand("SELECT PERS_COMP FROM PERSON WHERE PERS_ID="+Request.Form("Dest"),connM)
                Rec0=mycomm.ExecuteReader()
                Str0=Rec0(0)
                Rec0.Close
            END IF
            Str=Request.Form("Dest")
        END IF
        mycomm=new SqlCommand("Exec spGSM @Dest='"+Str+"',@Id='"+Session("Id").tostring()+"',@Comp='"+Str0+"',@Mess='"+Request.Form("Message")+"'",connM)
        mycomm.ExecuteNonQuery()
        Select Case Request.Form("Dest")
            Case "0" : pers="Everybody"
            Case "-2" : pers="Purchasers"
            Case "-3" : pers="Suppliers"
            Case "-5" : pers="Officer"
            Case "-6" : pers="The Plastics Exchange"
            Case "-7" : pers="Creditor"
            Case "-8" : pers="Freight Hauler"
        END Select
        mycomm=new SqlCommand("INSERT INTO MESSAGE (MESS_TO,MESS_FROM,MESS_COMP,MESS_TEXT,MESS_DATE,MESS_TYPE ) VALUES('"+Session("Id").tostring()+"','0','"+Session("Comp").tostring()+"','Message: "+Request.Form("Message")+".  Sent To: "+pers+"',GETDATE(),'6')",connM)
        mycomm.ExecuteNonQuery()
        IF Request.Form("Dest")="-6" THEN
            mycomm=new SqlCommand("SELECT PERS_FRST_NAME, PERS_LAST_NAME FROM person where PERS_ID = "+Session("Id"))
            Rec0=mycomm.ExecuteReader()
            messfrom = Rec0(0)+Rec0(1)
            mail1.From =messfrom
            mail1.To =Application("strEmailAdmin")
            mail1.Subject = "New System Message"
            mail1.Body = "<HTML><HEAD></HEAD><BODY bgColor=#ffffff><table border=0 cellspacing=0 cellpadding=0><tr><td><img src=http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/images/email/tpelogo.gif></td><td STYLE='FONT: 14pt ARIAL BLACK;COLOR=BLACK'>The<font color=red>Plastics</font>Exchange<font color=red>.</font>com</td></tr></table><br><br>"+Request.Form("Message")+"<br><br><br><table width='100%' border=0><tr><Td><img src=http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/images/email/bar.gif width=100% height=3></TD></tr></table><br><center><font face=arial size=1 color=Black>16510 N. 92nd Street. #1010 </font>&nbsp;&nbsp;<font face=arial size=1 color=red>Scottsdale, AZ 85260</font>&nbsp;&nbsp;<font face=arial size=1 color=Black><b>tel " + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + "</b></font>&nbsp;&nbsp;<font face=arial size=1 color=red>fax " + ConfigurationSettings.AppSettings["FaxNumber"].ToString() + "</font></center></BODY></HTML>"
    mail1.BodyFormat = MailFormat.Html
            TPE.Utility.EmailLibrary.Send(mail1)
            Rec0.Close
        END IF
    END IF
    IF Session("Typ")="P" THEN
	%><%
END IF
IF (Session("Typ")="A" OR Session("Typ")="B") AND Request.QueryString("Id")<>"" THEN
    mycomm=new SqlCommand("SELECT PERS_COMP,PERS_TYPE,PERS_LOGN FROM PERSON WHERE PERS_ID="+Request.QueryString("Id"),connM)
	Rec0=mycomm.ExecuteReader()
	Id=Request.QueryString("Id")
	CompBis=Rec0("PERS_COMP")
	Typ=Rec0("PERS_TYPE")
	Labl=" for login: "+Rec0("PERS_LOGN")
	Rec0.Close()
ELSEIF Request.Form("Id")="" THEN
	Id=Session("Id")
	CompBis=Session("Comp")
	Typ=Session("Typ")
	Labl=""
ELSE
	Id=Request.Form("Id")
	CompBis=Request.Form("CompBis")
	Typ=Request.Form("Typ")
	Labl=Request.Form("Labl")
END IF%>
<!--#INCLUDE FILE="../include/Body.inc"-->
<!--#INCLUDE FILE="../include/Loading.inc"-->

<SPAN ID=Main style='position:absolute; x:0px; y:0px; visibility: hidden'>
<!--#INCLUDE FILE="../include/BeginMargin.aspx"-->
<table border=0 cellspacing=0 cellpadding=0 bgcolor=#E8E8E8 align=center>
<input type=hidden name=Submit value=''>
<input type=hidden name=Id value='<%=Id%>'>
<input type=hidden name=CompBis value='<%=CompBis%>'>
<input type=hidden name=Typ value='<%=Typ%>'>
<input type=hidden name=Labl value='<%=Labl%>'>
<input type=hidden name=param value='<%=Request.Form("param")%>'>
	<tr>
		<td bgcolor=white colspan=3><img src=/images/1x1.gif width=1 height=8 border=0>
		</td>
	</tr>
	<tr>
		<td bgcolor=black CLASS=S5 colspan=3><img src=/images/1x1.gif width=3>Send A Message
		</td>
	</tr>
	<tr>
		<td colspan=3>To:<img src=/images/1x1.gif border=0 width=69 height=1>
<%IF Session("Typ")="A" OR Session("Typ")="B" THEN%>
	<select name=Comp onChange=submit()><script>Path.SO(new Array(<%
	IF Request.Form("Comp")<>"" THEN
		i=Request.Form("Comp")
	ELSE
		i="0"
	END IF
	mycomm=new SqlCommand("SELECT (CASE COMP_TYPE WHEN 'S' THEN 'Supplier/' WHEN 'P' THEN 'Purchaser/' WHEN 'D' THEN 'Distributor/' END)+COMP_NAME,COMP_ID,COMP_TYPE FROM COMPANY WHERE COMP_TYPE<>'E' AND COMP_TYPE<>'C' AND COMP_REG=1 ORDER BY COMP_TYPE,COMP_NAME",connM)
	Rec0=mycomm.ExecuteReader()
	Str=""
	Str0=""
	DO UNTIL not Rec0.read()
		IF CDbl(Rec0(1))=CDbl(i) THEN j=Rec0(2)
		Str=Str+",'"+Rec0(1).tostring()+"'"
		Str0=Str0+",'"+Rec0(0).tostring()+"'"
	LOOP
	Rec0.Close
	%>'0','-7','-8','-2','-3','-5'<%=Str%>),new Array('To everybody','To the creditor','The freight hauler','To all the Purchasers','To all the Suppliers','To all the Officers'<%=Str0%>),'<%=i%>',document)</script><br>
	<%IF CDbl(i)>0 THEN%>
	<img src=/images/1x1.gif border=0 width=86 height=1><script>Path.SO(new Array(
	<%
	mycomm=new SqlCommand("SELECT (CASE PERS_TYPE WHEN 'D' THEN 'Distributor/' WHEN 'P' THEN 'Purchaser/' WHEN 'S' THEN 'Supplier/' END)+PERS_LAST_NAME,PERS_ID FROM PERSON WHERE PERS_TYPE<>'O' AND PERS_COMP="+i.tostring()+" ORDER BY PERS_TYPE, PERS_LAST_NAME",connM)
	Rec0=mycomm.ExecuteReader()
	Str=""
	Str0=""
	DO UNTIL not Rec0.read()
		Str=Str+",'"+Rec0(1).tostring()+"'"
		Str0=Str0+",'"+Rec0(0).tostring()+"'"
	LOOP
	Rec0.Close
	%>'-5',<%IF j="P" THEN%>'-2'<%ELSE%>'-3'<%END IF%><%=Str%>),new Array('Your Officer',<%IF j="P" THEN%>'To all the Purchasers'<%ELSE%>'To all the Suppliers'<%END IF%><%=Str0%>),'',document,'Dest')</script><br>
	<%END IF 	' End Admin
ELSEIF Session("Typ")="P" OR Session("Typ")="S" THEN%>
	<script>Path.SO(new Array('-5','-7','-8','-6'),new Array('Your Officer','Your Creditor','The freight hauler','ThePlasticsExchange.com'),'',document,'Dest')</script><br>
<%ELSEIF Session("Typ")="C" OR Session("Typ")="T" THEN%>
<select name=Comp onChange=submit()><script>Path.SO(new Array(<%
IF Request.Form("Comp")<>"" THEN
	i=Request.Form("Comp")
ELSE
	i="0"
END IF
mycomm=new SqlCommand("SELECT (CASE COMP_TYPE WHEN 'S' THEN 'Supplier/' WHEN 'P' THEN 'Purchaser/' WHEN 'D' THEN 'Distributor/' END) +COMP_NAME,COMP_ID,COMP_TYPE FROM COMPANY WHERE COMP_TYPE<>'E' AND COMP_TYPE<>'C' AND COMP_REG=1 ORDER BY COMP_TYPE,COMP_NAME",connM)
Rec0=mycomm.ExecuteReader()
Str=""
Str0=""
DO UNTIL not Rec0.read()
	IF CDbl(Rec0(1))=CDbl(i) THEN j=Rec0(2)
	Str=Str+",'"+Rec0(1)+"'"
	Str0=Str0+",'"+Rec0(0)+"'"
LOOP
Rec0.Close
%>'0','-6','<%IF Session("Typ")="C" THEN%>-8<%ELSE%>-7<%END IF%>','-2','-3','-5'<%=Str%>),new Array('To everybody','To the PlasticsExchange.com','<%IF Session("Typ")="C" THEN%>The freight hauler<%ELSE%>To the creditor<%END IF%>','To all the Purchasers','To all the Suppliers','To all the Officers'<%=Str0%>),'<%=i%>',document)</script><br>
<%IF CDbl(i)>0 THEN%>
<img src=/images/1x1.gif border=0 width=86 height=1><script>Path.SO(new Array(<%
mycomm=new SqlCommand("SELECT (CASE PERS_TYPE WHEN 'D' THEN 'Distributor/' WHEN 'P' THEN 'Purchaser/' WHEN 'S' THEN 'Supplier/' END)+PERS_LAST_NAME,PERS_ID FROM PERSON WHERE PERS_TYPE<>'O' AND PERS_COMP="+i+" ORDER BY PERS_TYPE, PERS_LAST_NAME",connM)
Rec0=mycomm.ExecuteReader()
Str=""
Str0=""
DO UNTIL not Rec0.read()
	Str=Str+",'"+Rec0(1)+"'"
	Str0=Str0+",'"+Rec0(0)+"'"
LOOP
Rec0.Close
%>'-5',<%IF j="P" THEN%>'-2'<%ELSE%>'-3'<%END IF%><%=Str%>),new Array('Your Officer',<%IF j="P" THEN%>'To all the Purchasers'<%ELSE%>'To all the Suppliers'<%END IF%><%=Str0%>),'',document,'Dest')</script><br>
<%END IF
ELSE
    mycomm=new SqlCommand("Exec spOUsers "&Session("Comp"))
	Rec0=mycomm.ExecuteReader()
	Str=","
	Str0=","
	DO UNTIL not Rec0.read()
		Str=Str+"'"+Rec0(0)+"',"
		Str0=Str0+"'"+Rec0(1)+"',"
	LOOP
	Rec0.Close
	SELECT CASE Session("CompTyp")
	CASE "D" :%><script>Path.SO(new Array('-2','-3','-7','-6','-8'<%=Left(Str,Len(Str)-1)%>),new Array('Your own Purchasers','Your own Suppliers','Creditor','ThePlasticsExchange.com','The freight hauler'<%=Left(Str0,Len(Str0)-1)%>),'',document,'Dest')</script><%
	CASE "P" :%><script>Path.SO(new Array('-2','-7','-6','-8'<%=Left(Str,Len(Str)-1)%>),new Array('Your own Purchasers','Creditor','ThePlasticsExchange.com','The freight hauler'<%=Left(Str0,Len(Str0)-1)%>),'',document,'Dest')</script><%
	CASE "S" :%><script>Path.SO(new Array('-3','-7','-6','-8'<%=Left(Str,Len(Str)-1)%>),new Array('Your own Suppliers','Creditor','ThePlasticsExchange.com','The freight hauler'<%=Left(Str0,Len(Str0)-1)%>),'',document,'Dest')</script><%
	END SELECT
	%><br>
<%END IF%>
Message&nbsp;body:&nbsp;<textarea name=Message rows=3 cols=50 align=></textarea><br>
<div align=center><input type=button class=tpebutton value="SEND" onclick="Javascript:if(Path.Format(document.Form)==true){Path.Wait(window);with(document.Form){Submit.value='Submit';submit()}}"></div>
		</td>
	</tr>
	<tr>
		<td bgcolor=white colspan=3><div align=center><br>
<%'---------------------------CALENDAR-----------------------------------------------------------'%>
			<table border=1 cellspacing=0>
				<tr><%
dim td as date
td=today
dim pfile as system.io.file
IF Request.Form("param")="" THEN	ThisParam=td.tostring() ELSE ThisParam=Request.Form("param")
FOR i=-11 TO 0
	param=DateAdd("m",i,today)
	%>				<td<%IF CStr(param)=CStr(ThisParam) THEN%> bgcolor=yellow<%END IF%>>&nbsp;
	<a<%IF (pFile.Exists(Server.mappath("/ArchivesSM/"+Year(param).tostring()+"_"+Right("0"+Month(param).tostring(),2)+"/"+Id.tostring()+".sma")) OR i=0) THEN%> href=Javascript:document.Form.param.value='<%=param%>';document.Form.submit() class=S4><% ELSE Response.write(" class=S3>")
END IF
	%>
	<%=MonthName(Month(param),true)%>.&nbsp;<%=Year(param)%></a>&nbsp;
					</td><%
	IF i=-6 THEN response.write("</tr><tr>")
NEXT
dim flage as boolean
flage=false
flage=(Month(ThisParam)=Month(today))AND(Year(ThisParam)=Year(today))
%>				</tr>
			</table></div><br>
		</td>
	</tr>
<%IF flage THEN%>
	<tr>
		<td colspan=3 align=right>Sort System Messages By:&nbsp;&nbsp;<select name="sort" onChange=submit()>
<option value='0' <%IF Request.form("Sort")="0" THEN%> Selected<%END IF%>>Date</option>
<option value='1' <%IF Request.form("Sort")="1" THEN%> Selected<%END IF%>>Message</option>
		</td>
	</tr>
<%END IF%>
	<tr>
		<td bgcolor=black CLASS=S5 colspan=3><img src=/images/1x1.gif width=3>Message Queue<%=Labl%>
		</td>
	</tr>
<%
IF flage THEN
	IF Request.form("Sort")="1" THEN
		Sort="1"
	ELSE
		Sort="0"
	END IF
	mycomm=new SqlCommand("Exec spGSysMessage @PersId="+Id+",@CompId="+CompBis+",@PersTyp='"+Typ+"',@SortBy='"+Sort+"'",connM)
	Rec0=mycomm.ExecuteReader()
'........................................................................
DO

		IF not Rec0.read() THEN
		  EXIT DO
		end if
		Str=Rec0(1).tostring()
	%>
	<tr>
		<td colspan=2 valign=middle nowrap CLASS=S<%
	IF Rec0(3).tostring()<>"0" AND Rec0(4).tostring()="O" OR Rec0(3).tostring()="0" THEN%>8<%ELSE%>9<%END IF%>><div align=left><%
	IF Rec0(3).tostring()<>"0" THEN
		%><img src=/images/icons/grey_<%
		SELECT CASE Rec0(4).tostring()
		CASE "O" %>o<%
		CASE "A","B" %>a<%
		CASE "T" %>h<%
		CASE "C" %>c<%
		CASE "S" %>s<%
		CASE "P" %>p<%
		END SELECT
		%>.gif>&nbsp;&nbsp;<%
	END IF
	%><%=Mid(FormatDateTime(Rec0(0),vbLongDate),InStr(FormatDateTime(Rec0(0),vbLongDate),",")+2,Len(FormatDateTime(Rec0(0),vbLongDate)))%>&nbsp;<%=FormatDateTime(Rec0(0),vbShortTime)%><%
	IF InStr(",1,2,3,4,5,6,",","+Rec0(6)+",")=0 THEN%>&nbsp;<%ELSE%>&nbsp;&nbsp;<img src=/images/Icons/white_<%
		SELECT CASE Rec0(6)
		CASE "1" %>filled<%
		CASE "2" %>changed<%
		CASE "3" %>placed<%
		CASE "4" %>canceled<%
		CASE "5" %>to<%
		CASE "6" %>sent<%
		END SELECT
		%>.gif>&nbsp;<%
	END IF
	%></div>
		</td>
		<td CLASS=S9><%=Rec0(7)%>:&nbsp;<%=Str%>
		</td>
	</tr><%
	'IF ThisParam THEN Rec0.MoveNext()
LOOP
'........................................................................
ELSE
	file= New System.Io.StreamReader("/ArchivesSM/"+Year(Request.Form("param"))+"_"+Right("0"+Month(Request.Form("param")),2)+"/"+Id+".sma")	'response.Write("/ArchivesSM/"+Year(Request.Form("param"))+"_"+Right("0"+Month(Request.Form("param")),2)+"/"+Id+".sma")
DO
		mysplit=Split(file.readline(),"'")
		IF mysplit(0)="stop" THEN
		  EXIT DO
		end if
	Str=mysplit(1).tostring()
	%>
	<tr>
		<td colspan=2 valign=middle nowrap CLASS=S<%
	IF mysplit(3)<>"0" AND mysplit(4)="O" OR mysplit(3)="0" THEN%>8<%ELSE%>9<%END  IF%>><div align=left><%
	IF mysplit(3)<>"0" THEN
		%><img src=/images/icons/grey_<%
		SELECT CASE mysplit(4)
		CASE "O" %>o<%
		CASE "A","B" %>a<%
		CASE "T" %>h<%
		CASE "C" %>c<%
		CASE "S" %>s<%
		CASE "P" %>p<%
		END SELECT
		%>.gif>&nbsp;&nbsp;<%
	END IF
	%><%=Mid(FormatDateTime(mysplit(0),vbLongDate),InStr(FormatDateTime(mysplit(0),vbLongDate),",")+2,Len(FormatDateTime(mysplit(0),vbLongDate)))%>&nbsp;<%=FormatDateTime(mysplit(0),vbShortTime)%><%
	IF InStr(",1,2,3,4,5,6,",","+mysplit(6)+",")=0 THEN%>&nbsp;<%ELSE%>&nbsp;&nbsp;<img src=/images/Icons/white_<%
		SELECT CASE mysplit(6)
		CASE "1" %>filled<%
		CASE "2" %>changed<%
		CASE "3" %>placed<%
		CASE "4" %>canceled<%
		CASE "5" %>to<%
		CASE "6" %>sent<%
		END SELECT
		%>.gif>&nbsp;<%
	END IF
	%></div>
		</td>
		<td CLASS=S9><%=mysplit(7)%>:&nbsp;<%=Str%>
		</td>
	</tr><%
	'IF ThisParam THEN Rec0.MoveNext()
LOOP
IF flage THEN
	Rec0.Close()
	connM.Close()
ELSE
	File.close()
END IF
end  if
%>



</table>


</SPAN>

</form>
</body></html>
<script>Path.Start(window)</script>
