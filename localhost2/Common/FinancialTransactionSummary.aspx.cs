using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using MetaBuilders.WebControls;
using System.Text.RegularExpressions;
using TPE.Utility;


namespace localhost.Common
{
	/// <summary>
	/// Summary description for Filled_Orders.
	/// </summary>
	public partial class FinancialTransactionSummary : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnlogistics;
		protected System.Web.UI.WebControls.PlaceHolder phActionButton;
		string search;
		protected string SortExpression;
		protected string SDirection;
	
		/*****************************************************************************
		  '*   1. File Name       : Common/Filled_Orders.aspx                          *
		  '*   2. Description     : Filled orders                                      *
		  '*				                                                  *
		  '*   3. Modification Log:                                                    *
		  '*     Ver No.       Date          Author             Modification           *
		  '*   -----------------------------------------------------------------       *
		  '*      1.00       2-25-2004       Zach                Comment               *
		  '*                                                                           *
		  '*****************************************************************************/


		protected void Page_Load(object sender, EventArgs e)
		{
			//must logged in to access this page
			if ((string)Session["Typ"] != "A")
			{
				Response.Redirect("/default.aspx");
			}

			if (!Page.IsPostBack)
			{
				HelperFunction.fillCompany(ddlCompanies, this.Context);
			
				CreateDateList();
								
				search="";
			}

			BindDataGrid();

			// bind datagrid with default sorting
			//restricting the buttons based upon which type of user is looking at it.

			//OP: officer and his company type is P(Purchaser)					
		}

		// <summary>
		//  searches past months to see which months have transactions pending
		// </summary>
		protected void CreateDateList()
		{

			ddlMonth.Items.Clear(); // starts the list out fresh
			HelperFunction.fillDatesDesc(01, 2000, ddlMonth);
			ListItem lstItem = new ListItem("Show All Dates", "*", true);
			ddlMonth.Items.Add(lstItem);
			ddlMonth.SelectedItem.Selected = false;
			ddlMonth.Items[0].Selected = true;

		
		}
		// <summary>
		//  Wrapper when the ddl event is fired.
		// </summary>

		protected void Click_Search(object sender, EventArgs e)
		{
			char [] sep=new Char[1];
			sep[0]='-';
			String [] aux=txtSearch.Text.ToString().Split(sep);
			search=txtSearch.Text;

			ListItem liMonth = ddlMonth.Items.FindByValue("null");
			ddlMonth.SelectedIndex = -1;
			liMonth.Selected = true;
			ListItem liComp = ddlCompanies.Items.FindByValue("*");
			ddlCompanies.SelectedIndex = -1;
			liComp.Selected = true;
		
			BindDataGrid();
	
		}

		protected void Change_Comp(object sender, EventArgs e)
		{
			// date list needs to be rebuild according to the new company
			CreateDateList();
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
			txtSearch.Text="";
			BindDataGrid();
		}
		// <summary>
		//  filters out the month
		// </summary>
		protected void Change_Month(object sender, EventArgs e)
		{
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
			txtSearch.Text="";
			BindDataGrid();
		}

		// <summary>
		//  Primary function that binds the grid
		// </summary>
		protected void BindDataGrid()
		{
			using (SqlConnection conn= new SqlConnection(Application["DBConn"].ToString()))
			{
				string strStoredProcName;
		
				SortExpression = (SortExpression == null) ? "TRANS_NUM" : SortExpression;
				SDirection = (SDirection == null) ? "DESC" : SDirection;

				strStoredProcName = "spFinancial_Transaction_summary";
				Hashtable htParams = new Hashtable();
				htParams.Add("@Sort", SortExpression + " " + SDirection);
				if (!ddlCompanies.SelectedItem.Value.Equals("*"))
				{
					htParams.Add("@Comp",ddlCompanies.SelectedItem.Value.ToString());
				}
				
				if (!ddlMonth.SelectedItem.Value.Equals("*"))
				{
					// value passed is in MM-YYYY format. needs to broken up into separate parameters

					string strDate = ddlMonth.SelectedItem.Value.ToString();
					htParams.Add("@MM",strDate.Substring(4,2));
					htParams.Add("@YY", strDate.Substring(0,4));

				}
				htParams.Add("@SEARCH",search);

				DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(),dg,strStoredProcName,htParams);
				
				// start off by making them all visible
				pnContent.Visible = true;
				pnNoContent.Visible = false;
			

				if (dg.Items.Count > 0)
				{
					// make sure the right panels are visible
					pnContent.Visible = true;
					pnNoContent.Visible = false;
				}
				else
				{
					// there are no transactions so we need to display a different panel
					pnContent.Visible = false;
					pnNoContent.Visible = true;				
				}			
			}
			search="";			
		}
		
		// <summary>
		// Sorting function
		// </summary>
		protected void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{
			string[] SortExprs;
			string CurrentSortDirection;
			string NewSortDirection;
			string ColumnToSort;
			string NewSortExpr;


			//make sure the session variables have values
			if (Session["SortExpression"] == null)
			{
				Session["SortExpression"] = "TRANS_NUM";
			}
			if (Session["SDirection"] == null)
			{
				Session["SDirection"] = "DESC";
			}

			//change sort direction if the previous column sorted is the same as the current column sorted
			SortExpression = e.SortExpression.Split(' ')[0].ToString();
			if ( SortExpression.Equals(Session["SortExpression"].ToString()) )
			{
				SDirection = (Session["SDirection"].ToString().StartsWith("ASC")) ? "DESC" : "ASC";
			}
			else
			{
				SDirection = "ASC";
			}

			//set the session variables to new value
			Session["SortExpression"] = SortExpression;
			Session["SDirection"] = SDirection;

			//Set Datagrid
			BindDataGrid();
			
		}

		
		protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
		
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
