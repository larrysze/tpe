<%@ Page Language="VB" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Configuration" %>
<%@ import Namespace="System.Web.Mail" %>
<script>
function submit_email(){
	{
		document.Form.send.value='1';
		document.Form.submit();
	}
}

</script>
<form runat="server" id="Form">
    <TPE:Template PageTitle="" Runat="Server" />

<%
'Must logged in to access this page
IF Session("Typ")="" THEN Response.Redirect("/default.aspx")
Dim Sent
Sent=false

'Javascript function submit_email will set request.Form("send")=1
IF  Request.Form("send")=1 Then
Dim mail1 AS MailMessage
mail1 = new MailMessage()
mail1.From = Request.Form("EmailFrom")
mail1.To = Request.Form("EmailTo")
mail1.Subject = "Cert"

'Value of Session("EmailCert") will be set in the bottom of this page
'I had problems when using Request.Form instead of session
mail1.Body = Session("EmailCert")
mail1.BodyFormat = MailFormat.Html
TPE.Utility.EmailLibrary.Send(Context,mail1)
'Response.Write(Session("EmailCert"))
'Response.End
Sent=true
End IF

'Database connection'
Dim conn as SqlConnection
Dim cmdContent as SQLCommand
Dim Rec0 as SqlDataReader
conn = new SqlConnection(Application("DBconn").ToString())
conn.Open()
Dim conn1 as SqlConnection
Dim cmdContent1 as SQLCommand
Dim Rec1 as SqlDataReader
conn1 = new SqlConnection(Application("DBconn").ToString())
conn1.Open()
Dim conn2 as SqlConnection
Dim cmdContent2 as SQLCommand
Dim Rec2 as SqlDataReader
conn2 = new SqlConnection(Application("DBconn").ToString())
conn2.Open()
Dim conn3 as SqlConnection
Dim cmdContent3 as SQLCommand
Dim Rec3 as SqlDataReader
conn3 = new SqlConnection(Application("DBconn").ToString())
conn3.Open()
Dim conn4 as SqlConnection
Dim cmdContent4 as SQLCommand
Dim Rec4 as SqlDataReader
conn4 = new SqlConnection(Application("DBconn").ToString())
conn4.Open()
Dim connSHIP as SqlConnection
Dim cmdContentSHIP as SQLCommand
Dim RecSHIP as SqlDataReader
connSHIP = new SqlConnection(Application("DBconn").ToString())
connSHIP.Open()

DIM i, Email, StrEmail, Number, Str, Mail, dat
'Get the order ID from previous Filled Order page
'Order Id
Dim arrID(2) As String
arrID = Request.QueryString("ID").ToString().Split("-")
Dim cmdShipmentID = new SqlCommand("Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID = '"+arrID(0)+"' and SHIPMENT_SKU ='"+arrID(1)+"'",conn)
i= cmdShipmentID.ExecuteScalar()
Dim Order_ID =  arrID(0)


'Get the order ID from previous Filled Order page
'i = 3395

'Put the content of cert into a string
Str=""
StrEmail = StrEmail & "<HTML><HEAD><TITLE>TPE</TITLE>"&Chr(13)
StrEmail = StrEmail & "<BODY bgColor=#ffffff>"&Chr(13)
StrEmail = StrEmail & "<p align=left><table  align=left border=0 cellspacing=0 cellpadding=0>"&Chr(13)
StrEmail = StrEmail & "<tr><td align=left><img src=https://" + ConfigurationSettings.AppSettings["SecureDomainName"].ToString() + "//images/email/tpelogo.gif></td>"&Chr(13)
StrEmail = StrEmail & "<td align=left>"&Chr(13)
StrEmail = StrEmail & "<font face=arial black size=4 color=Black>The</font><font face=arial black size=4 color=red>Plastics</font><font face=arial black size=4 color=Black>Exchange</font><font face=arial black size=4 color=red>.</font><font face=arial black size=4 color=Black>com</font>"&Chr(13)
StrEmail = StrEmail & "</td>"&Chr(13)
StrEmail = StrEmail & "</tr></table>"&Chr(13)
StrEmail = StrEmail & "<br>"&Chr(13)

'Get that person info
cmdContent3= new SqlCommand("Select * from PERSON WHERE PERS_ID=(SELECT SHIPMENT_BUYR FROM SHIPMENT WHERE SHIPMENT_ID ="+i.ToString()+")", conn3)
Rec3= cmdContent3.ExecuteReader()
Rec3.Read

'email adress
StrEmail=StrEmail+"<br></p><br><br>"&Rec3("PERS_MAIL")

'Get company address
cmdContent= new SqlCommand("SELECT (SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=PLAC_COMP),PLAC_ADDR_ONE,PLAC_ADDR_TWO,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL),PLAC_ZIP,(SELECT (SELECT CTRY_NAME FROM COUNTRY WHERE CTRY_CODE=LOCL_CTRY) FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) FROM PLACE WHERE PLAC_TYPE='H' AND PLAC_COMP=(SELECT (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR) FROM SHIPMENT WHERE SHIPMENT_ID="+i.ToString()+")", conn)
Rec0= cmdContent.ExecuteReader()
Rec0.Read
StrEmail=StrEmail+"<br>"&Rec3("PERS_FRST_NAME")&"&nbsp;"&Rec3("PERS_LAST_NAME")
StrEmail=StrEmail+"<br>"&Rec3("PERS_TITL")

'Company name
'StrEmail=StrEmail+"<br>"&Rec0(0)
'StrEmail=StrEmail+"<br>"&Rec0("PLAC_ADDR_ONE")

'show second line of company address if there is one
'IF Rec0("PLAC_ADDR_TWO") Is DBNull.Value Then
'Else
'      IF Rec0("PLAC_ADDR_TWO").ToString()="" Then
'      Else
'      StrEmail=StrEmail+"<br>"&Rec0("PLAC_ADDR_TWO")
'      End iF
'End IF

'City and State
StrEmail=StrEmail+"<br>"&Rec0(3)
StrEmail=StrEmail+"&nbsp;"&Rec0("PLAC_ZIP")
StrEmail = StrEmail & "<br>"&Chr(13)
StrEmail = StrEmail & "<br>"&Chr(13)
StrEmail = StrEmail & "<br>"&Chr(13)

'Use string to build Query
Str=""
Str="SELECT * FROM ORDERS,SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ID="+i.ToString()+""
	cmdContent1= new SqlCommand(Str, conn1)
        Rec1= cmdContent1.ExecuteReader()
        Rec1.Read

        'Check if it's spot order(We merged forward and spot!)
        If Rec1("ORDR_ISSPOT") THEN
        Else
        Number=Rec1(1)
        End IF
Str=""
Str="SELECT LEFT(CONVERT(varchar(14),ORDR_DATE,101), 10) FROM ORDERS WHERE ORDR_ID="+Order_ID.ToString()+""
	cmdContent4= new SqlCommand(Str, conn4)
        Rec4= cmdContent4.ExecuteReader()
        Rec4.Read
StrEmail = StrEmail & "Dear "&Rec3("PERS_FRST_NAME")&",<br><br>"&Chr(13)

'Rec0(0) is company name
StrEmail=StrEmail+"This letter certifies that the resin sold to&nbsp;"&Rec0(0)&" on<font color=navy>&nbsp; "
'Rec4(0) is order date
StrEmail=StrEmail+""&Rec4(0)
StrEmail=StrEmail+"</font>, referenced by transaction #"&Rec1("ORDR_ID")

'190000-pound is railcar, display railcar # if exist
IF Rec1("SHIPMENT_SIZE")=190000 Then
		cmdContentSHIP= new SqlCommand("SELECT SHIPMENT_COMMENT FROM SHIPMENT WHERE SHIPMENT_ORDR_ID="+Rec1("ORDR_ID").ToString()+"", connSHIP)
		RecSHIP= cmdContentSHIP.ExecuteReader()
		RecSHIP.Read()
		IF RecSHIP.Hasrows Then
		IF RecSHIP("SHIPMENT_COMMENT") Is DBNull.Value Then

		Else

		                 If RecSHIP.Hasrows Then
		                 		   StrEmail=StrEmail+", r/c # "
						   RecSHIP.Close()
						   cmdContentSHIP= new SqlCommand("SELECT SHIPMENT_COMMENT FROM SHIPMENT WHERE SHIPMENT_ID="+i.ToString()+"", connSHIP)
						   RecSHIP= cmdContentSHIP.ExecuteReader()
						   WHile RecSHIP.Read()
						   StrEmail=StrEmail+""+RecSHIP("SHIPMENT_COMMENT").ToString()+"<br>"


						  END While
				  Else

				  End IF
                 End IF
		 End IF

		 RecSHIP.Close()
		 connSHIP.Close()
Else
End IF

'if it's spot order, display Dens, Melt
'only spot odder has Dens, Melt, Adds
If Rec1("ORDR_ISSPOT") THEN
	StrEmail=StrEmail+"</font>,&nbsp;is "&Rec1("ORDR_PROD_SPOT")
	StrEmail=StrEmail+"&nbsp;and conforms to the producer's prime specification."
	StrEmail = StrEmail & "<br>"&Chr(13)
	StrEmail = StrEmail & "<br>"&Chr(13)
	   IF Rec1("ORDR_DENS") Is DBNull.Value Then
	   Else
	      IF Rec1("ORDR_DENS")<>"" Then
	      StrEmail=StrEmail+"Density (ASTM D1505)<b>&nbsp;&nbsp;&nbsp;"&Rec1("ORDR_DENS")&"</b>g/cm3&nbsp;&nbsp;&nbsp;ASTM D1505<br>"
	      End IF
	   END iF

	   IF Rec1("ORDR_MELT") Is DBNull.Value Then
	   Else
	      IF Rec1("ORDR_MELT")<>"" Then
	      StrEmail=StrEmail+"Melt Flow Rate (190C/2.16 kg - E) <b>&nbsp;&nbsp;&nbsp;"&Rec1("ORDR_MELT")&"</b>g/cm3&nbsp;&nbsp;&nbsp;ASTM D1238<br>"
	      End IF
	   End IF

Else

'if it's not spot order, have to find order details from CONTRACT table
cmdContent2= new SqlCommand("SELECT * FROM CONTRACT WHERE CONT_ID="+Number.ToString()+"", conn2)
Rec2= cmdContent2.ExecuteReader()
Rec2.Read
	StrEmail=StrEmail+"</font>,&nbsp;is "&Rec2("CONT_LABL")
	StrEmail=StrEmail+"&nbsp;and conforms to the producer's prime specification."
	StrEmail = StrEmail & "<br>"&Chr(13)
	StrEmail = StrEmail & "<br>"&Chr(13)
	   IF Rec2("CONT_DENS_TGT") Is DBNull.Value Then
	   Else
	      IF Rec2("CONT_DENS_TGT")<>"" Then
	      StrEmail=StrEmail+"Density (ASTM D1505)<b>&nbsp;&nbsp;&nbsp;"&Rec2("CONT_DENS_TGT")&"</b>g/cm3&nbsp;&nbsp;&nbsp;ASTM D1505<br>"
	      End IF
	   END iF

	   IF Rec2("CONT_MELT_TGT") Is DBNull.Value Then
	   Else
	      IF Rec2("CONT_MELT_TGT")<>"" Then
	      StrEmail=StrEmail+"Melt Flow Rate (190C/2.16 kg - E) <b>&nbsp;&nbsp;&nbsp;"&Rec2("CONT_MELT_TGT")&"</b>g/cm3&nbsp;&nbsp;&nbsp;ASTM D1238<br>"
	      End IF
	   End IF
          Rec2.Close
 End IF
StrEmail = StrEmail & "<br>"&Chr(13)
StrEmail = StrEmail & "<br>"&Chr(13)
StrEmail = StrEmail & "Sincerely,<br>"&Chr(13)
StrEmail = StrEmail & "<br>"&Chr(13)
StrEmail = StrEmail & "Michael Greenberg, CEO"&Chr(13)
StrEmail = StrEmail & "<br>"&Chr(13)
StrEmail = StrEmail & "The Plastics Exchange"&Chr(13)
StrEmail = StrEmail & "<font face=arial size=2 color=black>"&Chr(13)
StrEmail = StrEmail &"<table align=center width='100%'><tr><td colspan=3 valign=top align=center><hr></td></tr>"
StrEmail = StrEmail &"<tr><td colspan=3 valign=top align=center><font face=arial size=1 color=Black>710 North Dearborn</font>&nbsp;&nbsp;<font face=arial size=1 color=red>Chicago, IL 60610</font>&nbsp;&nbsp;<font face=arial size=1 color=Black><b>tel 312.202.0002</b></font>&nbsp;&nbsp;<font face=arial size=1 color=red>fax 312.202.0174</font></td></tr></table>"
StrEmail = StrEmail & "</BODY></HTML>"&Chr(13)
IF Sent Then
response.write ("<br><br><br><p align=center><Font color=red size=3><b>Email has been sent to "&Request.Form("EmailTo")&"!</b></font></P><br><br>")
Else
End IF

'Display email content before send
response.write (StrEmail)
Session("EmailCert")=StrEmail
Rec0.Close
Rec1.Close
Rec3.Close
Rec4.Close
conn.Close()
conn1.Close()
conn2.Close()
conn3.Close()
conn4.Close()
%>

    <input type=hidden name=send value=''></input>
<table border=0 align=center cellspacing=0 cellpadding=0 >
	<tr>
						<td colspan=6  Class="Header" ><img src=/images/1x1.gif height=1>
		<font color=white>Send Statement by Email</font>				</td>
	</tr>


	<tr>
		<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
		<td width=3><img src=/images/1x1.gif width=1></td>
		<td><strong>From:</strong></td>
		<td >
			<select name=EmailFrom>
			<option value="michael@theplasticsexchange.com">michael@theplasticsexchange.com
			<option value="chris@theplasticsexchange.com">chris@theplasticsexchange.com
			<option value="brian@theplasticsexchange.com">brian@theplasticsexchange.com
			<option value="Kevin@@theplasticsexchange.com">Kevin@@theplasticsexchange.com
			<option value="Accounting@ThePlasticsExchange.com" selected>Accounting@theplasticsexchange.com
			</select>
		</td>

		<td width=3><img src=/images/1x1.gif width=1></td>
		<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
	</tr>
	<tr>
		<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
		<td width=3><img src=/images/1x1.gif width=1></td>
		<td><strong>To:</strong></td>
		<td ><input maxlength=80 size=30 name=EmailTo></input></td>
		<td width=3><img src=/images/1x1.gif width=1></td>
		<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
	</tr>

	<tr>
		<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
	    <td align="center" valign="top" colspan=4>
			<BR><input type=button class=tpebutton value="Send" onClick="Javascript:submit_email()"></input>
			<BR>
		</td>
		<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1></td>
        </tr>
	<tr>
		<td colspan=6  Class="Header" ><img src=/images/1x1.gif height=1>
		</td>
	</tr>
 </table>

<TPE:Template Footer="true" Runat="Server" />

</form>
