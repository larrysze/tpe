using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.IO;
using System.Text;
using TPE.Utility; 
using TPE.BusObjects;




namespace localhost.Common
{
	/// <summary>
	/// Summary description for FUNCLogin.
	/// </summary>
	public partial class FUNCLogin : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.ImageButton ImageButton1;

		protected void Page_Load(object sender, System.EventArgs e)
		{            
			if (!IsPostBack)
			{
				if (Request.QueryString["User"] !=null && Request.QueryString["Token"] !=null)
				{
					ViewState["UserName"] = Crypto.Decrypt(Request.QueryString["User"].ToString());
					//Crypted Password
					ViewState["Password"] = Crypto.Decrypt(Request.QueryString["Token"].ToString().Replace(' ','+')); 
					if ((Request.QueryString["LoginMode"]!=null) && (Request.QueryString["LoginMode"].ToString()=="System"))
					{
						VerifyLogin();
					}
					else
					{				
						DateTime Expiration = System.Convert.ToDateTime(Crypto.Decrypt(Request.QueryString["Exp"].ToString()));
						Session["strLoginURL"] = Crypto.Decrypt(Request.QueryString["LoginURL"].ToString());

						//if date is expired, send an email
						if (Expiration<System.Convert.ToDateTime(DateTime.Today.ToShortDateString()))
						{
							SendFailureLoginEmail("Invalid Login Attempt - Expired Date");
							//Display Warning message
							pnlMessage.Visible = true;
							lblMessageTitle.Text = "Expired Email link";
							lblMessage.Text = "The link you clicked on has expired. Please resubmit your password below.";
						}
						else
						{
							VerifyLogin();
						}
					}
				}
				else if (Session["UserName"] != null)
				{
					if (!(Request.QueryString["Switch"] != null))
					{
						RedirectHttpsProtocol(Session["UserName"].ToString(), Session["Password"].ToString());
					}
				}
				else if (Request.QueryString["User"] ==null && Request.QueryString["Token"] ==null && Session["ID"]==null)
				{
					pnlMessage.Visible = true;
					lblMessageTitle.Text = "Members Only";
					lblMessage.Text = "The page you are trying to access is restricted to registered members.  Please either log in or <a href='/Public/Registration.aspx'>sign up</a> below to view this page.";
				}
			}
		}


		private void RedirectHttpsProtocol(string UserName, string Password)
		{
			string prefix="";
			if ((Request.Url.AbsoluteUri.ToLower().IndexOf("plastic")>= 0) || (Request.Url.AbsoluteUri.IndexOf("205.234.174.242")>=0)) 
				prefix = "https://" + ConfigurationSettings.AppSettings["SecureDomainName"].ToString();
			//prefix = "http://" + ConfigurationSettings.AppSettings["SecureDomainName"].ToString();
			Session["UserName"] = null;
			Session["Password"] = null;
			Response.Redirect(prefix + "/Common/FUNCLogin.aspx?User=" + Crypto.Encrypt(UserName) + "&Token=" + Crypto.Encrypt(Password) + "&LoginMode=System");
		}
		protected void btnLogin_Click(object sender, System.EventArgs e)
		{
			//if (!Request.Url.AbsoluteUri.ToLower().StartsWith("http://"))
			if (!Request.Url.AbsoluteUri.ToLower().StartsWith("https://"))			
			{
				RedirectHttpsProtocol(txtUserName.Text, Crypto.Encrypt(txtPassword.Text));
			}
			else
			{
				// if the protocol is already https(may happen when the log in was unsuccessful)

				ViewState["UserName"] = HelperFunction.RemoveSqlEscapeChars(txtUserName.Text);
				ViewState["Password"] = Crypto.Encrypt(HelperFunction.RemoveSqlEscapeChars(txtPassword.Text));
				VerifyLogin();
			}
		}
		/// <summary>
		/// Login function that signs a user in.  Take two arguments. 
		/// 1)ID= The users ID 2) ReturnUser = boolean flag that signifys that the login is from a returning user
		///  
		/// </summary>
		/// <param name="ID"></param>
		/// <param name="ReturnUser"></param>
		static public void Login(HttpContext webContext,int ID, bool ReturnUser)
		{
			string strSQL;
			string Body;
			int TypeID = 0;
	        
			strSQL ="";
			strSQL +="	SELECT PERS_ACES, PERS_PSWD, PERS_TYPE, PERS_TITL, COMP_NAME, PERS_PHON, PERS_MAIL, PERS_COMP, ";
			strSQL +="		BROKER = (SELECT P.PERS_FRST_NAME+' '+P.PERS_LAST_NAME FROM PERSON P WHERE P.PERS_ID = Q1.PERS_ACES), ";
			strSQL +="		BROKER_MAIL = (SELECT P.PERS_MAIL FROM PERSON P WHERE P.PERS_ID = Q1.PERS_ACES), ";
			strSQL +="		CNAME=ISNULL(Q1.COMP_NAME,(SELECT C.COMP_NAME From COMPANY C Where PERS_COMP=COMP_ID)), ";
			strSQL +="		COMP_TYPE=(SELECT COMP_TYPE FROM COMPANY WHERE COMP_ID=PERS_COMP), ";
			strSQL +="		PERS_ID, ";
			strSQL +="		PERS_FRST_NAME, ";
			strSQL +="		PERS_LAST_NAME, ";
			strSQL +="		PERS_PREF, ";
			strSQL +="		PERS_PRMLY_INTRST ";
			strSQL +="	FROM PERSON Q1  ";
			strSQL +="	WHERE PERS_ID=@ID;";	//"+ID.ToString()+"; ";

			Hashtable htParams = new Hashtable();
			htParams.Add("@ID",ID.ToString());

 

			using (SqlConnection conn = new SqlConnection(webContext.Application["DBConn"].ToString()))
			{            
				conn.Open();
				//TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn,strSQL,htParams);
				using (SqlDataReader Rec0= TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn,strSQL,htParams))
				{
					Rec0.Read();
					webContext.Session["Id"]=Rec0["PERS_ID"];
					webContext.Session["Typ"]=Convert.ToString(Rec0["PERS_TYPE"]);

					//NOTE: "D" has 2 meanings: if PERSON.PERS_TYPE='O' and COMPANY.COMP_TYPE='D' THEN 'D' means Distributor
					//		IF PERSON.PERS_TYPE='D' THEN 'D' means 'Demo' 
					if (Rec0["PERS_TYPE"].ToString() == "D")
					{
						webContext.Session["Typ"] = "Demo";
					}
					// offer's type should defer to company type 
					if (Rec0["PERS_TYPE"].ToString() == "O")
					{
						webContext.Session["Typ"] = Rec0["COMP_TYPE"].ToString();
					}

					webContext.Session["Comp"]=Rec0["COMP_NAME"];
					webContext.Session["Comp_Id"]=Rec0["PERS_COMP"];
					webContext.Session["Name"]=Rec0["PERS_FRST_NAME"] +" " + Rec0["PERS_LAST_NAME"].ToString();
					webContext.Session["Nick"]=Rec0["PERS_FRST_NAME"];
					webContext.Session["Email"]=Rec0["PERS_MAIL"];
					webContext.Session["UserName"]= Rec0["PERS_MAIL"];
					webContext.Session["Password"]= Rec0["PERS_PSWD"];
					webContext.Session["Preferences"]= Rec0["PERS_PREF"];
					string market = HelperFunction.getSafeStringFromDB(Rec0["PERS_PRMLY_INTRST"]);
					webContext.Session["Market"]= market=="2" ? "I" : "D";

					// update user stats
					string strSqlUpdate = "UPDATE PERSON set PERS_LAST_CHKD = getDate(), PERS_NUMBER_LOGINS=(PERS_NUMBER_LOGINS+1) WHERE PERS_ID='" + Rec0["PERS_ID"].ToString() + "'";
					TPE.Utility.DBLibrary.ExecuteSQLStatement(webContext.Application["DBConn"].ToString(), strSqlUpdate);
                    
						
					// send email to alert admins
					//if (webContext.Session["Typ"].ToString() != "A")
					//{


					string XmlFileName = webContext.Server.MapPath("/email/xml/func_login.xml");
					string XslFileName = webContext.Server.MapPath("/email/xsl/func_login.xsl");

					TPE.BusObjects.TPEUser User; 
					User = new TPE.BusObjects.TPEUser(webContext.Application["DBconn"].ToString(), Rec0["PERS_MAIL"].ToString(), Rec0["PERS_PSWD"].ToString());

					XmlDocument xmldoc = new XmlDocument();

					//we are sure that file and "unpublished" record exist (see getUnpublishedID())

					using (XmlTextReader xmlreader = new XmlTextReader(XmlFileName.ToString()))
					{
					    xmldoc.Load(xmlreader);
					}
					XmlNode datetime = xmldoc.SelectSingleNode("//func_login/datetime");
					datetime.InnerText = DateTime.Now.ToString();

					XmlNode domain = xmldoc.SelectSingleNode("//func_login/domain");
					domain.InnerText = ConfigurationSettings.AppSettings["DomainName"].ToString();

					XmlNode ip = xmldoc.SelectSingleNode("//func_login/ip");
					ip.InnerText = webContext.Request.UserHostAddress.ToString();

					XmlNode comments = xmldoc.SelectSingleNode("//func_login/comments");
					comments.InnerText = User.Comments.ToString();

					XmlNode company = xmldoc.SelectSingleNode("//func_login/company");
					company.InnerText = User.Company.ToString();

					XmlNode email = xmldoc.SelectSingleNode("//func_login/email");
					email.InnerText = User.Email.ToString();

					XmlNode firstname = xmldoc.SelectSingleNode("//func_login/firstname");
					firstname.InnerText = User.FirstName.ToString();

					XmlNode lastname = xmldoc.SelectSingleNode("//func_login/lastname");
					lastname.InnerText = User.LastName.ToString();

					XmlNode phone = xmldoc.SelectSingleNode("//func_login/phone");
					phone.InnerText = User.Phone.ToString();

					XmlNode title = xmldoc.SelectSingleNode("//func_login/title");
					title.InnerText = User.Title.ToString();


					if (User.IsDomestic)
					{
					    XmlNode market2 = xmldoc.SelectSingleNode("//func_login/market");
					    market2.InnerText = "Domestic";

					    XmlNode zip = xmldoc.SelectSingleNode("//func_login/zip");
					    zip.InnerText = User.Zip.ToString();

					    XmlNode ziptrue = xmldoc.SelectSingleNode("//func_login/ziptrue");
					    ziptrue.InnerText = "1";

					    XmlNode size = xmldoc.SelectSingleNode("//func_login/size");
					    size.InnerText = User.FreightType.ToString();

					    XmlNode quality = xmldoc.SelectSingleNode("//func_login/quality");
					    quality.InnerText = User.FreightQuality.ToString();
					}
					else
					{
					    XmlNode market2 = xmldoc.SelectSingleNode("//func_login/market");
					    market2.InnerText = "Intrnational";

					    XmlNode port = xmldoc.SelectSingleNode("//func_login/port");
					    port.InnerText = User.Port.ToString();

					    XmlNode ziptrue = xmldoc.SelectSingleNode("//func_login/ziptrue");
					    ziptrue.InnerText = "0";

					    XmlNode size = xmldoc.SelectSingleNode("//func_login/size");
					    size.InnerText = "Metric Tonne";

					    XmlNode quality = xmldoc.SelectSingleNode("//func_login/quality");
					    quality.InnerText = "Prime";
					}

					XmlElement preference;
					XmlText preftext;
					for (int i = 0; i < User.Preferences.Length; i++)
					{
					    preference = xmldoc.CreateElement("", "preference", "");
					    preftext = xmldoc.CreateTextNode(User.Preferences[i]);
					    preference.AppendChild(preftext);
					    xmldoc.ChildNodes.Item(1).AppendChild(preference);
					}

					string XMLoutput;
					StringWriter writer = new StringWriter();
					xmldoc.Save(writer);
					XMLoutput = writer.ToString();
					writer.Close();

					MailMessage mail1;
					mail1 = new MailMessage();
					string strName = Rec0["PERS_FRST_NAME"].ToString() + "_" + Rec0["PERS_LAST_NAME"].ToString();
					////mail1.From = strName; // zach: removed because it was causing error
					//mail1.From = "Website";
								//append broker if broker exists
					if ((Rec0["BROKER_MAIL"].ToString().Length > 1 ) && (webContext.Request.Url.AbsoluteUri.ToLower().IndexOf("plastic")>= 0) || (webContext.Request.Url.AbsoluteUri.IndexOf("205.234.174.242")>=0))
					{
						mail1.To = Convert.ToString(Rec0["BROKER_MAIL"]);
					}

					if (mail1.To == null)
					{
						mail1.To = webContext.Application["strEmailOwner"].ToString();
						mail1.Cc = webContext.Application["strEmailAdmin"].ToString();
					}
					else
					{
						mail1.Cc = webContext.Application["strEmailAdmin"].ToString() + ";" + webContext.Application["strEmailOwner"].ToString();
					}

								// change the subject heading depending on the type of user.  This is used for filters

					if (Rec0["PERS_TYPE"].ToString() == "D")
					{
					    mail1.Subject = "Demo Login: " + Rec0["PERS_FRST_NAME"].ToString() + " " + Rec0["PERS_LAST_NAME"].ToString() + " ";
					    TypeID = 2;
					}
					else if ((Rec0["PERS_TYPE"].ToString() == "O") || (Rec0["PERS_TYPE"].ToString() == "P") || (Rec0["PERS_TYPE"].ToString() == "S"))
					{
					    mail1.Subject = "Exchange Login: " + Rec0["PERS_FRST_NAME"].ToString() + " " + Rec0["PERS_LAST_NAME"].ToString() + " ";
					    TypeID = 7;
					}
					else if ((Rec0["PERS_TYPE"].ToString() == "A"))
					{
					    mail1.Subject = "Admin Login: " + Rec0["PERS_FRST_NAME"].ToString() + " " + Rec0["PERS_LAST_NAME"].ToString() + " ";
					    TypeID = 11;
					}

					else if ((Rec0["PERS_TYPE"].ToString() == "B") || (Rec0["PERS_TYPE"].ToString() == "T"))
					{
					    mail1.Subject = "Broker Login: " + Rec0["PERS_FRST_NAME"].ToString() + " " + Rec0["PERS_LAST_NAME"].ToString() + " ";
					    TypeID = 5;
					}
					else
					    mail1.Subject = "Login: " + Rec0["PERS_FRST_NAME"].ToString() + " " + Rec0["PERS_LAST_NAME"].ToString() + " ";
					if (ReturnUser)
					{
					    mail1.Subject = "Returning " + mail1.Subject;
					    TypeID = 1;
					}



					//mail1.Body = TransformXML(XslFileName, XMLoutput);
					//mail1.BodyFormat = MailFormat.Html;

					Body = "";
					//Body = TransformXML(XslFileName, XMLoutput);

					//TPE.Utility.EmailLibrary.Send(webContext,mail1);

					//HelperFunction.IncludeNoteUser(webContext.Session["Id"].ToString(), "User Login: " + DateTime.Now.ToShortTimeString() ,webContext);

					Hashtable htPar = new Hashtable();
					htPar.Add("@IP", webContext.Request.UserHostAddress.ToString());
					htPar.Add("@PERSON_ID", webContext.Session["Id"]);
					//                    htPar.Add("@BODY", Body.ToString());
					htPar.Add("@ID_TYPE", TypeID);                        
					htPar.Add("@DATE_TIME", DateTime.Now.ToString());


					string strSql = "INSERT DASHBOARD (ID_TYPE, DATE_TIME, PERSON_ID, IP, BODY) VALUES (@ID_TYPE, @DATE_TIME, @PERSON_ID, @IP,'" + Body.ToString() + "')";
					//string strSql = "INSERT DASHBOARD (ID_TYPE, DATE_TIME, PERSON_ID, IP, BODY) VALUES (@ID_TYPE, @DATE_TIME, @PERSON_ID, @IP, @BODY)";
					//string strSql = "INSERT DASHBOARD (ID_TYPE, DATE_TIME, PERSON_ID, IP, BODY) VALUES (@ID_TYPE, @DATE_TIME, @PERSON_ID, '" + webContext.Session["Id"].ToString() + "', '" + Body.ToString() + "')";
					DBLibrary.ExecuteSQLStatement(webContext.Application["DBconn"].ToString(), strSql, htPar);
							//}



					if ((webContext.Session["Typ"].ToString() == "A") || (webContext.Session["Typ"].ToString() == "B") || webContext.Session["Typ"].ToString() == "T" || webContext.Session["Typ"].ToString() == "L")
					{
						webContext.Session.Timeout=1400;
						//admins and brokers have long session but they shouldn't be taken into account when calculating the number of online users
						webContext.Application["iUsersOnline"] = Convert.ToInt32(webContext.Application["iUsersOnline"]) - 1;
					}
					else // end users have a cookie placed on there machines to identify them next time
					{
						//Storing a Cookie
						webContext.Response.Cookies["Id"].Value = webContext.Session["Id"].ToString();
						webContext.Response.Cookies["Id"].Expires = DateTime.MaxValue;
					}
				}
			}			
			if (webContext.Session["strLoginURL"] != null)
			{
				string strLoginURL;
				strLoginURL = webContext.Session["strLoginURL"].ToString();
				webContext.Session["strLoginURL"] = null; 
				webContext.Response.Redirect (strLoginURL);
			}
			else if (ReturnUser)
			{
                webContext.Response.Redirect("https://" + ConfigurationSettings.AppSettings["DomainName"] + "/default.aspx");
			}
			else
			{
                webContext.Response.Redirect("https://" + ConfigurationSettings.AppSettings["DomainName"] + "/Spot/Floor_Summary.aspx");
			}

		}
		private static string TransformXML(string XslFileName, string XMLoutput)
		{
			string HTMLoutput;
			//string XslFileName = Server.MapPath("/email/xsl/func_login.xsl");
			XslTransform xslt = new XslTransform();
			StringReader rdr = new StringReader(XMLoutput);

			xslt.Load(XslFileName.ToString());
			//XPathDocument doc = new XPathDocument(XMLfile);

			StringWriter writer = new StringWriter();
			xslt.Transform(new XPathDocument(rdr), null, writer, null);
			HTMLoutput = writer.ToString();
			return HTMLoutput;
		}
		private void VerifyLogin()
		{
			string strSQL;
			string strLoginError;

			strLoginError = "We did not find your login information in our records, please resubmit.";
                       
			// user name can not be empty or less than one character
			if ((ViewState["UserName"].ToString().Length <1) || (ViewState["UserName"].ToString()==""))
			{
				//lblPleaseResubmit.Text=strLoginError;
				pnlMessage.Visible = true;
				lblMessageTitle.Text = "";
				lblMessage.Text = strLoginError;
			}
		        else
			{
				using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
				{
					conn.Open();
					//strSQL ="SELECT * FROM PERSON  WHERE  ( (PERS_LOGN='"+HelperFunction.RemoveSqlEscapeChars(ViewState["UserName"].ToString())+"') OR (PERS_MAIL='" + HelperFunction.RemoveSqlEscapeChars(ViewState["UserName"].ToString()) +"') )";
					strSQL ="SELECT * FROM PERSON  WHERE  ( (PERS_LOGN=@PERS_LOGN) OR (PERS_MAIL=@PERS_EMAIL) AND (PERS_ACCOUNT_DISABLED=0))";

					Hashtable htParams = new Hashtable();
					// users can enter logon or email
					htParams.Add("@PERS_LOGN",HelperFunction.RemoveSqlEscapeChars(ViewState["UserName"].ToString()));
					htParams.Add("@PERS_EMAIL",HelperFunction.RemoveSqlEscapeChars(ViewState["UserName"].ToString()));
			
					using (SqlDataReader Rec0 = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn,strSQL,htParams))
					{								
						if (Rec0.Read())
						{
							if(Crypto.Decrypt(Rec0["PERS_PSWD"].ToString()).ToLower() != Crypto.Decrypt(ViewState["Password"].ToString()).ToLower())
							{
								SendFailureLoginEmail("Invalid Login Attempt");
								pnlMessage.Visible = true;
								lblMessageTitle.Text = "";
								lblMessage.Text = strLoginError;
							}
							else
							{
								// store id in temporty variable so that we can close the reader and sql connection
								//int Tempory_ID = Convert.ToInt16(Rec0["PERS_ID"]);
                                int Tempory_ID = Convert.ToInt32(Rec0["PERS_ID"]);
								Login(this.Context,Tempory_ID,false);
							}
						}
						else
						{
							SendFailureLoginEmail("Invalid Login Attempt");
							//lblPleaseResubmit.Text=strLoginError;
							pnlMessage.Visible = true;
							lblMessageTitle.Text = "";
							lblMessage.Text = strLoginError;
						}
					}
				}
			}
		}

		private void SendFailureLoginEmail(string subject)
		{
			//login info doesn't match database records, alert Mike'
			int TypeID = 6;
			string Body;
			string XmlFileName = this.Server.MapPath("/email/xml/func_login.xml");
			string XslFileName = this.Server.MapPath("/email/xsl/func_login_failure.xsl");

			XmlDocument xmldoc = new XmlDocument();

			using (XmlTextReader xmlreader = new XmlTextReader(XmlFileName.ToString()))
			{
				xmldoc.Load(xmlreader);
			}
			XmlNode datetime = xmldoc.SelectSingleNode("//func_login/datetime");
			datetime.InnerText = DateTime.Now.ToString();

			XmlNode domain = xmldoc.SelectSingleNode("//func_login/domain");
			domain.InnerText = ConfigurationSettings.AppSettings["DomainName"].ToString();

			XmlNode email = xmldoc.SelectSingleNode("//func_login/email");
			email.InnerText = ViewState["UserName"].ToString();

			XmlNode pass = xmldoc.SelectSingleNode("//func_login/password");
			pass.InnerText = Crypto.Decrypt(ViewState["Password"].ToString());

			string XMLoutput;
			StringWriter writer = new StringWriter();
			xmldoc.Save(writer);
			XMLoutput = writer.ToString();
			writer.Close();

			//MailMessage mail1;
			//mail1 = new MailMessage();
			//mail1.From = Application["strEmailOwner"].ToString();
			//mail1.To = Application["strEmailAdmin"].ToString()+";"+Application["strEmailOwner"].ToString();
			//mail1.Subject = subject;
			//mail1.Body = TransformXML(XslFileName, XMLoutput);
			//mail1.BodyFormat = MailFormat.Html;
			//TPE.Utility.EmailLibrary.SendWithoutContext(mail1);
			//TPE.Utility.EmailLibrary.Send(webContext,mail1);

			//HelperFunction.IncludeNoteUser(webContext.Session["Id"].ToString(), "User Login: " + DateTime.Now.ToShortTimeString() ,webContext);


			Body = TransformXML(XslFileName, XMLoutput);


			Hashtable htPar = new Hashtable();
			htPar.Add("@IP", Request.UserHostAddress.ToString());
			htPar.Add("@ID_TYPE", TypeID);
			htPar.Add("@DATE_TIME", DateTime.Now.ToString());
			htPar.Add("@PASSWORD", ViewState["Password"].ToString());
			htPar.Add("@EMAIL", ViewState["UserName"].ToString());

			string strSql = "INSERT DASHBOARD (ID_TYPE, DATE_TIME, IP, PASSWORD, BODY, EMAIL) VALUES (@ID_TYPE, @DATE_TIME, @IP, @PASSWORD, '" + Body.ToString() + "', @EMAIL)";

			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSql, htPar);

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

//		private void btnCryptPwd_Click(object sender, System.EventArgs e)
//		{
//			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
//			conn.Open();
//
//			string sSql = "SELECT PERS_ID, PERS_PSWD FROM PERSON";
//            
//			SqlDataAdapter dadContent = new SqlDataAdapter(sSql,conn);			
//			DataTable dtRequest = new DataTable();			
//			dadContent.Fill(dtRequest);
//
//			SqlCommand cmd_Update;
//			
//			for(int i=0; i < dtRequest.Rows.Count; i++)
//			{				
//				sSql = "UPDATE PERSON SET PERS_PSWD = '" + Crypto.Encrypt(dtRequest.Rows[i]["PERS_PSWD"].ToString()) + 					
//					"' WHERE PERS_ID = " + dtRequest.Rows[i]["PERS_ID"];
//				
//				cmd_Update = new SqlCommand(sSql, conn);
//				cmd_Update.ExecuteNonQuery();
//			}
//		}		
	}
}
