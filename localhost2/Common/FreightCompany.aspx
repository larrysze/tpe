<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Page language="c#" Codebehind="FreightCompany.aspx.cs" AutoEventWireup="true" Inherits="localhost.Common.FreightCompany" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>FreightCompany</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
	
    <form id="Form1" method="post" runat="server">
<style>
        .menuskin { BORDER-RIGHT: black 2px solid; BORDER-TOP: black 2px solid; Z-INDEX: 100; VISIBILITY: hidden; FONT: 12px/18px Verdana; BORDER-LEFT: black 2px solid; WIDTH: 165px; BORDER-BOTTOM: black 2px solid; POSITION: absolute; BACKGROUND-COLOR: menu }
        .menuskin A { PADDING-RIGHT: 10px; PADDING-LEFT: 10px; COLOR: black; TEXT-DECORATION: none }
        #mouseoverstyle { BACKGROUND-COLOR: highlight }
        #mouseoverstyle A { COLOR: white }
        </style>
        
         <TPE:Template PageTitle="Filled Orders" Runat="server" id=Template1 />
    <div id="popmenu" class="menuskin" onMouseover="clearhidemenu();highlightmenu(event,'on')" onMouseout="highlightmenu(event,'off');dynamichide(event)">
    </div>
    
     </asp:DataGrid>
     
<asp:panel ID="pnContacted" Runat="server" visible="True" ><b>Companies capable of shipping this load:</b>

	<asp:DataGrid id=contacted runat="server" Width="100%" HorizontalAlign="Center" CellPadding="2" DataKeyField="COMP_NAME" AutoGenerateColumns="false" ShowFooter="False" HeaderStyle-CssClass="DataGridHeader" AlternatingItemStyle-CssClass="DataGridRow_Alternate" ItemStyle-CssClass="DataGridRow" CssClass="DataGrid">
            <Columns>
                <asp:BoundColumn DataField="COMP_NAME" HeaderText="Company Name" ItemStyle-Wrap="false" />
                <asp:BoundColumn DataField="CONTACT_NAME" HeaderText="Contact Name" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundColumn DataField="EMAIL" HeaderText="Email" />
                <asp:BoundColumn DataField="PHONE" HeaderText="Phone" ItemStyle-Wrap="false"/>
                <asp:BoundColumn DataField="FAX" HeaderText="Fax" ItemStyle-Wrap="false"/>
                <asp:BoundColumn DataField="ADRESSE" HeaderText="Address" />
                <asp:BoundColumn DataField="CITY" HeaderText="City" />
                <asp:BoundColumn DataField="STATE" HeaderText="State" />
                <asp:BoundColumn DataField="ZIP" HeaderText="Zip" />

            </Columns>
	</asp:DataGrid><asp:Button ID=btnSendMail class="tpebutton" onclick="Click_SendEmail" runat="server" Text="Send Emails to request quote"></asp:Button>
</asp:Panel>

<br>
<asp:panel ID="pnSizeNotFound" Runat="server" visible="False" CssClass="DataGrid"><center><b>Silly Helio, you can't send quotes for this shipment size.</b></center></asp:Panel>
<br>
<asp:panel ID="pnUnContacted" Runat="server" visible="True" ><b>Companies unselected because of an empty Email:</b>

	<asp:DataGrid id="uncontacted" runat="server" Width="100%" HorizontalAlign="Center" CellPadding="2" DataKeyField="COMP_NAME" AutoGenerateColumns="false" ShowFooter="False" HeaderStyle-CssClass="DataGridHeader" AlternatingItemStyle-CssClass="DataGridRow_Alternate" ItemStyle-CssClass="DataGridRow" CssClass="DataGrid">
            <Columns>
                <asp:BoundColumn DataField="COMP_NAME" HeaderText="Company Name" ItemStyle-Wrap="false" />
                <asp:BoundColumn DataField="CONTACT_NAME" HeaderText="Contact Name" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundColumn DataField="EMAIL" HeaderText="Email" />
                <asp:BoundColumn DataField="PHONE" HeaderText="Phone" ItemStyle-Wrap="false"/>
                <asp:BoundColumn DataField="FAX" HeaderText="Fax" ItemStyle-Wrap="false" />
                <asp:BoundColumn DataField="ADRESSE" HeaderText="Address" />
                <asp:BoundColumn DataField="CITY" HeaderText="City" />
                <asp:BoundColumn DataField="STATE" HeaderText="State" />
                <asp:BoundColumn DataField="ZIP" HeaderText="Zip" />
            </Columns>
	</asp:DataGrid>	
</asp:Panel>
<asp:panel ID="pnEmailSent" Runat="server" visible="False" CssClass="DataGrid"><center><b>Emails have been sent succesfully</b></center></asp:Panel>
	<TPE:Template Footer="true" Runat="server" id=Template2 />

     </form>
	
  </body>
</html>
