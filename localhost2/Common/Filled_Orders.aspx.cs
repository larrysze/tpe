using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using MetaBuilders.WebControls;
using System.Text.RegularExpressions;
using TPE.Utility;



namespace localhost.Common
{
	/// <summary>
	/// Summary description for Filled_Orders.
	/// </summary>
	public partial class Filled_Orders : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnlogistics;
		protected System.Web.UI.WebControls.PlaceHolder phActionButton;

		string search;
	
		/*****************************************************************************
		  '*   1. File Name       : Common/Filled_Orders.aspx                          *
		  '*   2. Description     : Filled orders                                      *
		  '*				                                                  *
		  '*   3. Modification Log:                                                    *
		  '*     Ver No.       Date          Author             Modification           *
		  '*   -----------------------------------------------------------------       *
		  '*      1.00       2-25-2004       Zach                Comment               *
		  '*                                                                           *
		  '*****************************************************************************/


		protected void Page_Load(object sender, EventArgs e)
		{
			//must logged in to access this page
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "S") && ((string)Session["Typ"] != "P") && ((string)Session["Typ"] != "O") && ((string)Session["Typ"] != "L"))
			{
				Response.Redirect("/default.aspx");
			}
			Master.Width = "1024px";

			if (!IsPostBack)
			{

				HelperFunction.fillCompany(ddlCompanies, this.Context);
				
				
                
				CreateDateList();


				// set defalut sort by ORDR_ID
				lblSort.Text = "ORDR_ID DESC";

				// bind datagrid with default sorting
				//restricting the buttons based upon which type of user is looking at it.

				//OP: officer and his company type is P(Purchaser)


				search="";
				BindDataGrid(lblSort.Text, true);

			}
			HelperFunction.renderDisabledCompanyList(ddlCompanies, this.Context);

		}
		// <summary>
		//  searches past months to see which months have transactions pending
		// </summary>
	        protected void CreateDateList()
	       {
		       ddlMonth.Items.Clear(); // starts the list out fresh


		       DateTime today = DateTime.Now;
		       string[] all_months = new string[12] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
		       DateTime nextMonth = today.AddMonths(1);
               ListItem item = new ListItem(all_months[nextMonth.Month - 1] + ", " + nextMonth.Year, nextMonth.Year.ToString() + nextMonth.Month.ToString().PadLeft(2, '0'));
		       ddlMonth.Items.Add(item);

		       HelperFunction.fillDatesDesc(01, 2000, ddlMonth);
		       ListItem lstItem = new ListItem("Show All Dates", "*", true);
		       ddlMonth.Items.Add(lstItem);
		       ddlMonth.SelectedItem.Selected = false;
		       ddlMonth.Items[1].Selected = true;
		

	        }
		
		// <summary>
		//  Wrapper when the ddl event is fired.
		// </summary>

		protected void Click_Search(object sender, EventArgs e)
		{
			search=txtSearch.Text;

			ddlMonth.SelectedItem.Selected = false;
			ddlMonth.Items[ddlMonth.Items.Count-1].Selected = true;

	

			ddlCompanies.SelectedItem.Selected = false;
			ddlCompanies.Items[0].Selected = true;

			BindDataGrid(lblSort.Text, true);
		}

		protected void Change_Comp(object sender, EventArgs e)
		{
			// date list needs to be rebuild according to the new company
			//CreateDateList();

			ddlMonth.SelectedItem.Selected = false;
			ddlMonth.Items[0].Selected = true;
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
			txtSearch.Text="";

			BindDataGrid(lblSort.Text, false);
		}
		// <summary>
		//  filters out the month
		// </summary>
		protected void Change_Month(object sender, EventArgs e)
		{
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
			txtSearch.Text="";
			BindDataGrid(lblSort.Text, true);
		}

		// <summary>
		//  Primary function that binds the grid
		// </summary>
		protected void BindDataGrid(string strSort, bool MonthInclude)
		{
			SqlConnection conn;
			string strSQL;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			SqlDataAdapter dadContent;
			DataSet dstContent;
			SqlDataReader dtrAnyTrans;
			SqlCommand cmdAnyTrans;

            Hashtable ht = new Hashtable();
			//strSQL = "spFilled_Orders @Sort='" + strSort+"'";
            ht.Add("@Sort", strSort);
		    
			if (!ddlCompanies.SelectedItem.Value.Equals("*"))
			{

                ht.Add("@Comp", ddlCompanies.SelectedItem.Value.ToString());
			}
			else 
			{
				MonthInclude = true;
			}


			// If non admin is logged on, restrict the filled orders to the current company
			if (Session["Typ"].ToString().Equals("P") || Session["Typ"].ToString().Equals("OP") || Session["Typ"].ToString().Equals("S") || Session["Typ"].ToString().Equals("OS"))
			{
                ht.Add("@Comp", Session["Comp_Id"]);
			}

            DateTime searchDate;
            if (DateTime.TryParse(search, out searchDate))
            {
                ht.Add("@DD", searchDate.Day);
                ht.Add("@MM", searchDate.Month);
                ht.Add("@YY", searchDate.Year);
            }
            else
            {
                if (!ddlMonth.SelectedItem.Value.Equals("*") && MonthInclude)
                {
                    // value passed is in MM-YYYY format. needs to broken up into separate parameters
                    string strMonth;
                    string strYear;
                    string strDate = ddlMonth.SelectedItem.Value.ToString();
                    strYear = strDate.Substring(0, 4);
                    strMonth = strDate.Substring(4, 2);
                    ht.Add("@MM", strMonth);
                    ht.Add("@YY", strYear);
                }

                ht.Add("@SEARCH",search);
            }

            dtrAnyTrans = DBLibrary.GetDataReaderStoredProcedure(conn, "spFilled_Orders", ht);
			// start off by making them all visible
			pnContent.Visible = true;
			pnNoContent.Visible = false;
			btnPO.Visible = true;
			btnInvoice.Visible = true;
			btnEdit.Visible = true;
			btnCert.Visible = true;
			btnSalesCon.Visible = true;
			

			if (dtrAnyTrans.HasRows)
			{
				dtrAnyTrans.Close();
				// make sure the right panels are visible
				pnContent.Visible = true;
				pnNoContent.Visible = false;
				//Response.Write(strSQL);
				//dadContent = new SqlDataAdapter(strSQL,conn);
                dstContent = DBLibrary.GetDataSetFromStoredProcedure(conn, "spFilled_Orders", ht);
				//dadContent.Fill(dstContent);
				
				if ((Session["Typ"].ToString().Equals("S")) || (Session["Typ"].ToString().Equals("OS")))
				{
					dgSeller.DataSource = dstContent;
					dgSeller.DataBind();
				}
				else if ((Session["Typ"].ToString().Equals("P")) || (Session["Typ"].ToString().Equals("OP")))
				{
				    dgBuyer.DataSource = dstContent;
				    dgBuyer.DataBind();
				}
				else
				{
				    dg.DataSource = dstContent;
				    dg.DataBind();
				}
			}
			else
			{
				// there are no transactions so we need to display a different panel
				pnContent.Visible = false;
				pnNoContent.Visible = true;
				btnPO.Visible = false;
				btnInvoice.Visible = false;
				btnEdit.Visible = false;
				btnCert.Visible = false;
				btnSalesCon.Visible = false;
				
			}


			if ((Session["Typ"].ToString().Equals("P")) || (Session["Typ"].ToString().Equals("OP")))
			{
				btnPO.Visible = false;
				btnEdit.Visible = false;
				btnCert.Visible = false;
				btnSalesCon.Visible = false;
				
				pnAdminFunctions.Visible = false;
		                pnlRegularUser.Visible = true;
			}
			if ((Session["Typ"].ToString().Equals("S")) || (Session["Typ"].ToString().Equals("OS")))
			{
				btnInvoice.Visible = false;
				btnEdit.Visible = false;
				btnCert.Visible = false;
				btnSalesCon.Visible = false;
				
				pnAdminFunctions.Visible = false;
		        pnlRegularUser.Visible = true;
			}

			conn.Close();
			search="";
						
		}

		// <summary>
		// Handlers for the button events
		// This isn't the most elegant way of doing it but I'm lazy
		// </summary>

		protected void Click_FreightCompany(object sender,EventArgs e)
		{
			RowSelectorColumn rsc = RowSelectorColumn.FindColumn(dg);
			if (rsc.SelectedIndexes.Length != 0)
			{
				int selIndex = rsc.SelectedIndexes[0];
				Response.Redirect ("/hauler/Freight_Company.aspx?ID="+dg.DataKeys[selIndex].ToString());
			}
		}

		protected void Click_PO(object sender, EventArgs e)
		{
			RowSelectorColumn rsc = RowSelectorColumn.FindColumn(dg);
			if (rsc.SelectedIndexes.Length != 0)
			{
				int selIndex = rsc.SelectedIndexes[0];
				// only the first 4 chars of the Order ID are needed.  After the '-' is noise
				//Get PO  (PO=True)
				Response.Redirect ("/administrator/Send_Documents.aspx?ID="+dg.DataKeys[selIndex].ToString()+"&DocumentType=2");
			}
		}
		protected void Click_Invoice(object sender, EventArgs e)
		{
			RowSelectorColumn rsc = RowSelectorColumn.FindColumn(dg);
			if (rsc.SelectedIndexes.Length != 0)
			{
				int selIndex = rsc.SelectedIndexes[0];
				// only the first 4 chars of the Order ID are needed.  After the '-' is noise
				//Get Invoice (no querystring for PO)
				Response.Redirect ("/administrator/Send_Documents.aspx?ID="+dg.DataKeys[selIndex].ToString()+"&DocumentType=1");
			}
		}
		protected void Click_Cert(object sender, EventArgs e)
		{
			RowSelectorColumn rsc = RowSelectorColumn.FindColumn(dg);
			if (rsc.SelectedIndexes.Length != 0)
			{
				int selIndex = rsc.SelectedIndexes[0];
				// only the first 4 chars of the Order ID are needed.  After the '-' is noise
				//Get Certificate, pass querystring ID(order ID)
				Response.Redirect ("/administrator/Send_Documents.aspx?ID="+dg.DataKeys[selIndex].ToString()+"&DocumentType=3");
			}
		}
		protected void Click_Edit(object sender, EventArgs e)
		{
			RowSelectorColumn rsc = RowSelectorColumn.FindColumn(dg);
			if (rsc.SelectedIndexes.Length != 0)
			{
				int selIndex = rsc.SelectedIndexes[0];
				// only the first 4 chars of the Order ID are needed.  After the '-' is noise
				//Update order, pass querystring ID(order ID)
				Response.Redirect ("/administrator/Transaction_Details.aspx?ID="+dg.DataKeys[selIndex].ToString()+"&Market=Spot&Referer=/Common/Filled_Orders.aspx");
			}
		}
		protected void Click_SalesConfirmation(object sender, EventArgs e)
		{
			RowSelectorColumn rsc = RowSelectorColumn.FindColumn(dg);
			if (rsc.SelectedIndexes.Length != 0)
			{
				int selIndex = rsc.SelectedIndexes[0];
				// only the first 4 chars of the Order ID are needed.  After the '-' is noise
				//Get sales confirmation
				Response.Redirect ("/administrator/Send_Documents.aspx?ID="+dg.DataKeys[selIndex].ToString()+"&DocumentType=4");
			}
		}
		


		// <summary>
		// Sorting function
		// </summary>
		protected void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{

			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;

			//  Parse the sort expression - delimiter space
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
			ColumnToSort = SortExprs[0];
			// If a sort order is specified get it, else default is descending
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DeSC";
			}

			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;

			// Figure out the column index
			int iIndex;
			iIndex = 1;
			
			if ((Session["Typ"].ToString().Equals("A")) || (Session["Typ"].ToString().Equals("B")) || (Session["Typ"].ToString().Equals("L")))
			{
				switch(ColumnToSort.ToUpper())
				{
					case "ORDR_ID":
						iIndex = 2;
						break;
					case "VARSHIP":
						iIndex = 5;
						break;
					case "VARCONTRACT":
						iIndex = 8;
						break;
					case "VARVALUE":
						iIndex = 9;
						break;
					case "ORDR_DATE":
						iIndex = 10;
						break;
					case "VARBUYR":
						iIndex = 11;
						break;
					case "VARSELR":
						iIndex = 12;
						break;
					case "VAROPEN":
						iIndex = 13;
						break;
				}
			}
			else
			{
				switch(ColumnToSort.ToUpper())
				{
					case "ORDER_ID":
						iIndex = 1;
						break;
					case "VARSHIP":
						iIndex = 2;
						break;
					case "VARCONTRACT":
						iIndex = 5;
						break;
					case "ORDR_FOB":
						iIndex = 6;
						break;
					case "ORDR_DATE":
						iIndex = 7;
						break;
				}

			}
			if ((Session["Typ"].ToString().Equals("S")) || (Session["Typ"].ToString().Equals("OS")))
			{
				// alter the column's sort expression
				dgSeller.Columns[iIndex].SortExpression = NewSortExpr;
				// reset dbase page to the first one whether the sorting changes
				dgSeller.CurrentPageIndex = 0;


			}
			else if ((Session["Typ"].ToString().Equals("P")) || (Session["Typ"].ToString().Equals("OP")))
			{
				// alter the column's sort expression
				dgBuyer.Columns[iIndex].SortExpression = NewSortExpr;
				// reset dbase page to the first one whether the sorting changes
				dgBuyer.CurrentPageIndex = 0;
			}
			else 
			{
				// alter the column's sort expression
				dg.Columns[iIndex].SortExpression = NewSortExpr;
				// reset dbase page to the first one whether the sorting changes
				dg.CurrentPageIndex = 0;
			}
			// Sort the data in new order
			BindDataGrid(NewSortExpr, true);
		}


		double runningSum = 0.0;
		int iLinkSetCount = 1;
		// <summary>
		// calculates total for the footer
		// adds in the script for the 'action box' functionality
		// </summary>

		protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{

				if (HelperFunction.ConvertToBoolean(DataBinder.Eval(e.Item.DataItem, "SHIPMENT_PROBLEMFLAG")) == true)
				{
				    for (int i = 0; i < e.Item.Cells.Count; i++)
				    {
					e.Item.Cells[i].BackColor = Color.LightPink;
				    }
				}

				runningSum += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARVALUE"));
				string strActionBoxHTML = "";
				// get Order id
				string strID = HelperFunction.ConvertToString(DataBinder.Eval(e.Item.DataItem, "ORDR_ID"));
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]='<div class=\"menuitems\"><a href=\"/administrator/Transaction_Details.aspx?Market=Spot&Referer=/Common/Filled_Orders.aspx&ID="+strID+"\">Edit # "+strID+"</a></div>'" + ((char)13).ToString();

				// remove extra info
				//strID = strID.Substring(0,strID.LastIndexOf("-"));
				
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<hr>' //Optional Separator" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=1\">Invoice</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=2\">Purchase Order</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<hr>' //Optional Separator" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=3\">Certificate</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=4\">Sale Confirmation</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/hauler/Freight_Company.aspx?ID="+strID+"\">Freight R.F.Q</a></div>'" + ((char)13).ToString();
				
				strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<div class=\"menuitems\"><a href=\"/common/Convert_Order.aspx?type=BT&ID=" + strID + "\">Convert to 5 BT</a></div>'" + ((char)13).ToString();
				strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<div class=\"menuitems\"><a href=\"/common/Convert_Order.aspx?type=TL&ID=" + strID + "\">Convert to 5 TL</a></div>'" + ((char)13).ToString();
				phActionButton.Controls.Add (new LiteralControl(strActionBoxHTML));


				// setting the mouseover color
				e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");
				if (e.Item.ItemType == ListItemType.Item)
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee'");
				}
				else
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3'");
				}
				// add scripts for 'action boxes'
				e.Item.Cells[1].Attributes.Add("onmouseover","showmenu(event,linkset["+iLinkSetCount.ToString()+"])");
				e.Item.Cells[1].Attributes.Add("onMouseout", "delayhidemenu()");

				iLinkSetCount ++;

				// determines whether the uploaded docs folder is empty or full
				if( HelperFunction.ConvertToString(DataBinder.Eval(e.Item.DataItem, "VARNUMFILES")) != "0" )
				{
					e.Item.Cells[4].Text ="<a href=\"/common/Order_Documents.aspx?ID="+strID+"\"><img border=\"0\" src=\"/pics/icons/icon_filled_folder.gif\"></a>";
				}
				else
				{
					e.Item.Cells[4].Text ="<a href=\"/common/Order_Documents.aspx?ID="+strID+"\"><img border=\"0\" src=\"/pics/icons/icon_folder.gif\"></a>";
				}
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[1].Text ="<b>Total</b>";
				e.Item.Cells[8].Text = "<b> " + String.Format("{0:c}", runningSum)+"</b>";
			}
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
