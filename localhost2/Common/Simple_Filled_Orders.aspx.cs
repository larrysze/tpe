using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using TPE.Utility;
using MetaBuilders.WebControls;

namespace localhost.Common
{
    public partial class Simple_Filled_Orders : System.Web.UI.Page
    {
        string comp_id= "";
        double totalSum = 0.0;
        double totalWeight = 0.0;       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //comp_id = Session["Comp_id"].ToString();
                comp_id = "145"; 
                ViewState["Comp_id"] = comp_id;
                CreateDateList();

                string strSort = "VARDATE DESC";
                ViewState["Sort"] = strSort;
                BindDG();
            }
        }

        protected void BindDG()
        {
            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();
                System.Text.StringBuilder sbSQL = new System.Text.StringBuilder();
                DataSet dstContent;

                Hashtable htParams = new Hashtable();
                if (this.ViewState["Comp_id"] != null)
                {
                    htParams.Add("@Comp", ViewState["Comp_id"].ToString());
                }

                if (this.ViewState["Sort"] != null)
                {
                    htParams.Add("@Sort", ViewState["Sort"].ToString());
                }

		if (!ddlMonth.SelectedItem.Value.Equals("*"))
		{
			// value passed is in MM-YYYY format. needs to broken up into separate parameters

			string strDate = ddlMonth.SelectedItem.Value.ToString();
			htParams.Add("@MM", strDate.Substring(4, 2));
			htParams.Add("@YY", strDate.Substring(0, 4));

		}

                htParams.Add("@BuySell", "B");

                dstContent = DBLibrary.GetDataSetFromStoredProcedure(conn, "spSimple_Filled_Orders", htParams);
                dgBuyer.DataSource = dstContent;

                htParams.Remove("@BuySell");
                htParams.Add("@BuySell", "S");
                
                dstContent = DBLibrary.GetDataSetFromStoredProcedure(conn, "spSimple_Filled_Orders", htParams);
                dgSeller.DataSource = dstContent;
                
                try
                {
                    dgBuyer.DataBind();
                    dgSeller.DataBind();
                }
                catch
                {
                   
                }
            }
        }

        protected void KeepRunningSum(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                totalSum += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARVALUE"));
                totalWeight += HelperFunction.ConvertToSingle(DataBinder.Eval(e.Item.DataItem, "VARWGHT"));                
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                e.Item.Cells[1].Text = "<b>Total</b>";
                e.Item.Cells[4].Text = "<b> " + String.Format("{0:#,###}", totalWeight) + "(lbs)</b>";
                e.Item.Cells[6].Text = "<b> " + String.Format("{0:c}", totalSum) + "</b>";
            }
        }

        protected void SortDG(Object sender, DataGridSortCommandEventArgs e)
        {
            string[] SortExprs;
            string[] ViewStateExprs;
            string NewSearchMode = "";
            string NewSortExpr;

            Regex r = new Regex(" ");
            SortExprs = r.Split(e.SortExpression);

            Regex v = new Regex(" ");
            ViewStateExprs = v.Split(ViewState["Sort"].ToString());

            if (ViewStateExprs[0] == SortExprs[0])
            {
                if (ViewStateExprs[1].ToString() == "ASC") NewSearchMode = "DESC";
                else NewSearchMode = "ASC";
            }
            else NewSearchMode = "ASC";

            NewSortExpr = SortExprs[0] + " " + NewSearchMode;

            ViewState["Sort"] = NewSortExpr;

            BindDG();
        }

        protected void CreateDateList()
        {
		ddlMonth.Items.Clear(); // starts the list out fresh
		HelperFunction.fillDatesDesc(01, 2000, ddlMonth);
		ListItem lstItem = new ListItem("Show All Dates", "*", true);
		ddlMonth.Items.Add(lstItem);
		ddlMonth.SelectedItem.Selected = false;
		ddlMonth.Items[0].Selected = true;
	  
        }

        protected void Click_Invoice(object sender, EventArgs e)
        {
            RowSelectorColumn rsc = RowSelectorColumn.FindColumn(dgBuyer);
            if (rsc.SelectedIndexes.Length != 0)
            {
                int selIndex = rsc.SelectedIndexes[0];
                // only the first 4 chars of the Order ID are needed.  After the '-' is noise
                //Get Invoice (no querystring for PO)
                Response.Redirect("/administrator/Send_Documents.aspx?ID=" + dgBuyer.DataKeys[selIndex].ToString() + "&DocumentType=1");
            }
        }

        protected void Click_PO(object sender, EventArgs e)
        {
            RowSelectorColumn rsc = RowSelectorColumn.FindColumn(dgSeller);
            if (rsc.SelectedIndexes.Length != 0)
            {
                int selIndex = rsc.SelectedIndexes[0];
                // only the first 4 chars of the Order ID are needed.  After the '-' is noise
                //Get PO  (PO=True)
                Response.Redirect("/administrator/Send_Documents.aspx?ID=" + dgSeller.DataKeys[selIndex].ToString() + "&DocumentType=2");
            }
        }

        protected void Click_SalesConfirmation(object sender, EventArgs e)
        {
            RowSelectorColumn rsc = RowSelectorColumn.FindColumn(dgBuyer);
            if (rsc.SelectedIndexes.Length != 0)
            {
                int selIndex = rsc.SelectedIndexes[0];
                // only the first 4 chars of the Order ID are needed.  After the '-' is noise
                //Get sales confirmation
                Response.Redirect("/administrator/Send_Documents.aspx?ID=" + dgBuyer.DataKeys[selIndex].ToString() + "&DocumentType=4");
            }
        }

        protected void Click_Cert(object sender, EventArgs e)
        {
            RowSelectorColumn rsc = RowSelectorColumn.FindColumn(dgBuyer);
            if (rsc.SelectedIndexes.Length != 0)
            {
                int selIndex = rsc.SelectedIndexes[0];
                // only the first 4 chars of the Order ID are needed.  After the '-' is noise
                //Get Certificate, pass querystring ID(order ID)
                Response.Redirect("/administrator/Send_Documents.aspx?ID=" + dgBuyer.DataKeys[selIndex].ToString() + "&DocumentType=3");
            }
        }

        protected void Change_Month(object sender, EventArgs e)
        {
            BindDG();
        }
    }
}
