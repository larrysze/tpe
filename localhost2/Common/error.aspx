<%@ Page Language="C#" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" Title="The Plastics Exchange: Commodity Plastic Resin, Buy Resin, Sell Resin, Plastic Materials"%>
<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="cphHeading"><span class="Header Bold Color1">Error Warning!</span></asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">



            <center>
            <br />
            <p class="Content Color2">An error has occured. Technical support has been notified of the problem.  We apologize for the inconvenience.<br /><br /></p>
            <%if ((Request.QueryString["EmailError"] != null) && (Request.QueryString["EmailError"].ToString() == "1")) Response.Write("<span class='Content'>The system is currently not able to send emails. Please contact The Plastics Exchange at " + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + ".</span>"); %>
            <br /><br />
            </center>


</asp:Content>