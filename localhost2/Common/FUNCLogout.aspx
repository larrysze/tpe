<%@ Page Language="C#" %>
<%
Response.Cookies["Id"].Expires = DateTime.Today.AddDays(-1);
Session.Abandon();
Response.Redirect ("http://" + ConfigurationSettings.AppSettings["DomainName"] +  "/default.aspx");
%>
