<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MB_Specs_Location.aspx.cs" Inherits="localhost.Common.MB_Specs_Location" MasterPageFile="~/MasterPages/Menu.Master" Title="Specs Location"%>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphMain">
<div class="DivTitleBarMenu"><span class="Header Color1 Bold">Location Details</span></div>
<div style="width:780px; text-align:center"><br>
			<table border="0" cellpadding="0" cellspacing="0" width="450px" align="center" class="Content">
				<tr>
				    <td align="left" class="Bold" width="100">Type </td>
				    <td align="left" width="250"><asp:Label runat="server" id="lblType"></asp:Label></td>
				</tr>
				<tr>
				    <td align="left" class="Bold" width="100">Contact </td>
				    <td align="left" width="250"><asp:Label runat="server" id="lblContact"></asp:Label></td>
				</tr>
				<tr>
				    <td align="left" class="Bold" width="100">Phone </td>
				    <td align="left" width="250"><asp:Label runat="server" id="lblPhone"></asp:Label></td>
				</tr>
				<tr>
				    <td align="left" class="Bold" width="100">Adress one </td>
				    <td align="left" width="250"><asp:Label runat="server" id="lblAdress1"></asp:Label></td>
				</tr>
				<tr>
				    <td align="left" class="Bold" width="100">Adress two </td>
				    <td align="left" width="250"><asp:Label runat="server" id="lblAdress2"></asp:Label></td>
				</tr>
				<tr>
				    <td align="left" class="Bold" width="100">Zip Code </td>
				    <td align="left" width="250"><asp:Label runat="server" id="lblZip"></asp:Label></td>
				</tr>
								<tr>
				    <td align="left" class="Bold" width="100">City </td>
				    <td align="left" width="250"><asp:Label runat="server" id="lblCity"></asp:Label></td>
				</tr>
								<tr>
				    <td align="left" class="Bold" width="100">State </td>
				    <td align="left" width="250"><asp:Label runat="server" id="lblState"></asp:Label></td>
				</tr>
								<tr>
				    <td align="left" class="Bold" width="100">Country </td>
				    <td align="left" width="250"><asp:Label runat="server" id="lblCountry"></asp:Label></td>
				</tr>
								<tr>
				    <td align="left" class="Bold" width="100">Instructions </td>
				    <td align="left" width="250"><asp:Label runat="server" id="lblInstructions"></asp:Label></td>
				</tr>
 				<tr>
					<td colspan=4><center><br>
                        <input type="button" class="Content Color2" value='OK' onClick="javascript:history.back()"></center><br>
					</td>
				</tr>
			</table>
</div>

</asp:Content>
