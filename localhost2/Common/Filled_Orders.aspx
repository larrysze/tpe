<%@ Page Language="c#" Codebehind="Filled_Orders.aspx.cs" AutoEventWireup="True" Inherits="localhost.Common.Filled_Orders" MasterPageFile="~/MasterPages/Menu.Master" Title="The Plastics Exchange: Commodity Plastic Resin, Buy Resin, Sell Resin, Plastic Materials" %>

<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
    <style type="text/css">
        .menuskin { BORDER-RIGHT: black 2px solid; BORDER-TOP: black 2px solid; Z-INDEX: 100; VISIBILITY: hidden; FONT: 12px/18px Verdana; BORDER-LEFT: black 2px solid; WIDTH: 165px; BORDER-BOTTOM: black 2px solid; POSITION: absolute; BACKGROUND-COLOR: menu }
        .menuskin A { PADDING-RIGHT: 10px; PADDING-LEFT: 10px; COLOR: black; TEXT-DECORATION: none }
        #mouseoverstyle { BACKGROUND-COLOR: highlight }
        #mouseoverstyle A { COLOR: white }
        .Left { text-align:left}
</style>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphJavaScript">
    <script language="JavaScript1.2">

        //Pop-it menu- By Dynamic Drive
        //For full source code and more DHTML scripts, visit http://www.dynamicdrive.com
        //This credit MUST stay intact for use

        var linkset=new Array()
        //SPECIFY MENU SETS AND THEIR LINKS. FOLLOW SYNTAX LAID OUT

        <asp:PlaceHolder id="phActionButton"  runat="server" />
        ////No need to edit beyond here

        var ie4=document.all&&navigator.userAgent.indexOf("Opera")==-1
        var ns6=document.getElementById&&!document.all
        var ns4=document.layers

        function showmenu(e,which){

        if (!document.all&&!document.getElementById&&!document.layers)
        return

        clearhidemenu()

        menuobj=ie4? document.all.popmenu : ns6? document.getElementById("popmenu") : ns4? document.popmenu : ""
        menuobj.thestyle=(ie4||ns6)? menuobj.style : menuobj

        if (ie4||ns6)
        menuobj.innerHTML=which
        else{
        menuobj.document.write('<layer name=gui bgColor=#E6E6E6 width=165 onmouseover="clearhidemenu()" onmouseout="hidemenu()">'+which+'</layer>')
        menuobj.document.close()
        }

        menuobj.contentwidth=(ie4||ns6)? menuobj.offsetWidth : menuobj.document.gui.document.width
        menuobj.contentheight=(ie4||ns6)? menuobj.offsetHeight : menuobj.document.gui.document.height
        eventX=ie4? event.clientX : ns6? e.clientX : e.x
        eventY=ie4? event.clientY : ns6? e.clientY : e.y

        //Find out how close the mouse is to the corner of the window
        var rightedge=ie4? document.body.clientWidth-eventX : window.innerWidth-eventX
        var bottomedge=ie4? document.body.clientHeight-eventY : window.innerHeight-eventY

        //if the horizontal distance isn't enough to accomodate the width of the context menu
        if (rightedge<menuobj.contentwidth)
        //move the horizontal position of the menu to the left by it's width
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX-menuobj.contentwidth : ns6? window.pageXOffset+eventX-menuobj.contentwidth : eventX-menuobj.contentwidth
        else
        //position the horizontal position of the menu where the mouse was clicked
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX : ns6? window.pageXOffset+eventX : eventX

        //same concept with the vertical position
        if (bottomedge<menuobj.contentheight)
        menuobj.thestyle.top=ie4? document.body.scrollTop+eventY-menuobj.contentheight : ns6? window.pageYOffset+eventY-menuobj.contentheight : eventY-menuobj.contentheight
        else
        menuobj.thestyle.top=ie4? document.body.scrollTop+event.clientY : ns6? window.pageYOffset+eventY : eventY
        menuobj.thestyle.visibility="visible"
        return false
        }

        function contains_ns6(a, b) {
        //Determines if 1 element in contained in another- by Brainjar.com
        while (b.parentNode)
        if ((b = b.parentNode) == a)
        return true;
        return false;
        }

        function hidemenu(){
        if (window.menuobj)
        menuobj.thestyle.visibility=(ie4||ns6)? "hidden" : "hide"
        }

        function dynamichide(e){
        if (ie4&&!menuobj.contains(e.toElement))
        hidemenu()
        else if (ns6&&e.currentTarget!= e.relatedTarget&& !contains_ns6(e.currentTarget, e.relatedTarget))
        hidemenu()
        }

        function delayhidemenu(){
        if (ie4||ns6||ns4)
        delayhide=setTimeout("hidemenu()",500)
        }

        function clearhidemenu(){
        if (window.delayhide)
        clearTimeout(delayhide)
        }

        function highlightmenu(e,state){
        if (document.all)
        source_el=event.srcElement
        else if (document.getElementById)
        source_el=e.target
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        else{
        while(source_el.id!="popmenu"){
        source_el=document.getElementById? source_el.parentNode : source_el.parentElement
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        }
        }
        }

        if (ie4||ns6)
        document.onclick=hidemenu

    </script>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
    <div class="menuskin" id="popmenu" onmouseover="clearhidemenu();highlightmenu(event,'on')"
		onmouseout="highlightmenu(event,'off');dynamichide(event)"></div>
    <asp:Label ID="lblSort" runat="server" Visible="false"></asp:Label>
    <table cellpadding="0" cellspacing="0" border="0" align="center" >
        
        <asp:Panel ID="pnAdminFunctions" runat="server">
            <tr>
                <td colspan="1">
                    <table width="100%" border="0">
                        <tr>
                            <td align="left" valign="middle">
                                <span class="Header Bold Color2">Filled Orders</span></td>
                            <td align="left" valign="middle">
                                <span class="Header Bold Color2">Search:</span><asp:TextBox CssClass="InputForm" size="10" ID="txtSearch" runat="server" />
                                <asp:Button Height="22" runat="server" ID="bSearch" CssClass="Content Color2" OnClick="Click_Search" Text="Search" />
                            </td>
                            <td align="right">
                                <span class="Header Bold Color2">Filters</span>
                                <asp:DropDownList CssClass="InputForm" ID="ddlMonth" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Change_Month">
                                </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:DropDownList CssClass="InputForm" ID="ddlCompanies" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Change_Comp">
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </asp:Panel>
        
        <asp:Panel ID="pnlRegularUser" runat="server" Visible="false">
            <tr>
                <td colspan="1">
                    <table width="100%" border="0">
                        <tr>
                            <td align="left" valign="middle">
                                <span class="Header Bold Color2">Filled Orders</span></td>
                            <td align="left" valign="middle">
                                <span class="Header Bold Color2">Search:</span><asp:TextBox CssClass="InputForm" size="10" ID="TextBox1" runat="server" />
                                <asp:Button Height="22" runat="server" ID="Button1" CssClass="Content Color2" OnClick="Click_Search" Text="Search" />
                            </td>
                            <td align="right">
                                <span class="Header Bold Color2">Month: </span>
                                <asp:DropDownList CssClass="InputForm" ID="ddlCustomerMonth" runat="server" 
                                AutoPostBack="True" OnSelectedIndexChanged="Change_Month" />                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </asp:Panel>
        
        
        
        <tr>
            <td>
                <table align="center" border="0" cellpadding="2" cellspacing="2">
                    <tbody>
                        <tr>
                            <td>
                                <asp:Button Height="22" class="Content Color2" ID="btnEdit" OnClick="Click_Edit" runat="server" Text="Transaction Details"></asp:Button>
                            </td>
                            <td>
                                <asp:Button Height="22" class="Content Color2" ID="btnInvoice" OnClick="Click_Invoice" runat="server" Text="Invoice"></asp:Button>
                            </td>
                            <td>
                                <asp:Button Height="22" class="Content Color2" ID="btnPO" OnClick="Click_PO" runat="server" Text="Purchase Order"></asp:Button>
                            </td>
                            <td>
                                <asp:Button Height="22" class="Content Color2" ID="btnCert" OnClick="Click_Cert" runat="server" Text="Certificate"></asp:Button>
                            </td>
                            <td>
                                <asp:Button Height="22" class="Content Color2" ID="btnSalesCon" OnClick="Click_SalesConfirmation" runat="server" Text="Sales Confirmation"></asp:Button>
                            </td>
                            <td>
                                <asp:Button Height="22" class="Content Color2" ID="btnFreightCompany" OnClick="Click_FreightCompany" runat="server" Text="Email Freight Company"></asp:Button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <tr>
                <td>
                    <asp:Panel ID="pnContent" runat="server">
                        <%if ((Session["Typ"].ToString().Equals("S")) || (Session["Typ"].ToString().Equals("O")))
                                                   {%>
                        <asp:DataGrid ID="dgSeller" runat="server" BackColor="#000000" BorderWidth="0" CellSpacing="1" Width="100%" HorizontalAlign="Center" CellPadding="2" DataKeyField="ORDR_ID" AllowSorting="true" OnSortCommand="SortDG" AutoGenerateColumns="false">
                            <HeaderStyle CssClass="LinkNormal Bold OrangeColor" HorizontalAlign="Center" />
                            <AlternatingItemStyle CssClass="LinkNormal DarkGray" />
                            <ItemStyle CssClass="LinkNormal LightGray" />
                            <FooterStyle CssClass="Content Bold Color4 FooterColor" />
                            <Columns>
                                <mbrsc:RowSelectorColumn SelectionMode="Single" />
                                <asp:BoundColumn DataField="ORDR_ID" HeaderText="#" ItemStyle-Wrap="false" SortExpression="ORDR_ID ASC" />
                                <asp:BoundColumn DataField="VARSHIP" HeaderText="Status" SortExpression="VARSHIP ASC" />
                                <asp:BoundColumn DataField="VARRCNUMBER" HeaderText="RC#" SortExpression="VARRCNUMBER ASC" />
                                <asp:BoundColumn DataField="VARWAREHOUSE" HeaderText="Warehouse" SortExpression="VARWAREHOUSE ASC" />
                                <asp:BoundColumn DataField="VARWGHT" DataFormatString="{0:#,###}" HeaderText="Weight(lbs)" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="right" />
                                <asp:BoundColumn DataField="VARCONTRACT" HeaderText="Product" ItemStyle-Wrap="false" SortExpression="VARCONTRACT ASC" />
                                <asp:BoundColumn DataField="VARDATE" HeaderText="Date" SortExpression="ORDR_DATE ASC" />
                                <asp:HyperLinkColumn HeaderText="Seller" DataNavigateUrlField="SHIPMENT_SELR" DataNavigateUrlFormatString="../common/MB_Specs_User.aspx?ID={0}" DataTextField="VARSELR" />
                            </Columns>
                        </asp:DataGrid><%}
                         else if ((Session["Typ"].ToString().Equals("P")) || (Session["Typ"].ToString().Equals("O")))
                         {%>
                        <asp:DataGrid ID="dgBuyer" BackColor="#000000" CellSpacing="1" BorderWidth="1" runat="server" Width="100%" HorizontalAlign="Center" CellPadding="2" DataKeyField="ORDR_ID" AllowSorting="true" OnSortCommand="SortDG" AutoGenerateColumns="false">
                               <HeaderStyle CssClass="LinkNormal Bold OrangeColor" HorizontalAlign="Center" />
                            <AlternatingItemStyle CssClass="LinkNormal DarkGray" />
                            <ItemStyle CssClass="LinkNormal LightGray" />
                            <FooterStyle CssClass="Content Bold Color4 FooterColor" />
                            <Columns>
                                <mbrsc:RowSelectorColumn SelectionMode="Single" />
                                <asp:BoundColumn DataField="ORDR_ID" HeaderText="#" ItemStyle-Wrap="false" SortExpression="ORDR_ID ASC" />
                                <asp:BoundColumn DataField="VARSHIP" HeaderText="Status" SortExpression="VARSHIP ASC" />
                                <asp:BoundColumn DataField="VARRCNUMBER" HeaderText="RC#" SortExpression="VARRCNUMBER ASC" />
                                <asp:BoundColumn DataField="VARWAREHOUSE" HeaderText="Warehouse" SortExpression="VARWAREHOUSE ASC" />
                                <asp:BoundColumn DataField="VARWGHT" DataFormatString="{0:#,###}" HeaderText="Weight(lbs)" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="right" />
                                <asp:BoundColumn DataField="VARCONTRACT" HeaderText="Product" ItemStyle-Wrap="false" SortExpression="VARCONTRACT ASC" />
                                <asp:BoundColumn DataField="VARDATE" HeaderText="Date" SortExpression="ORDR_DATE ASC" />                                
                            </Columns>
                        </asp:DataGrid><%}
                         else
                         {%>
                        <asp:DataGrid CellSpacing="1" BorderWidth="0" ID="dg" runat="server" Width="100%" HorizontalAlign="Center" CellPadding="2" 
                            DataKeyField="ORDR_ID" AllowSorting="true" OnSortCommand="SortDG" AutoGenerateColumns="false" ShowFooter="True" OnItemDataBound="KeepRunningSum"><%--PageSize="100" PagerStyle-Mode="NumericPages" OnPageIndexChanged="dg_PageIndexChanged" AllowPaging="True"--%>
                           <HeaderStyle CssClass="LinkNormal Bold OrangeColor" HorizontalAlign="Center" />
                            <AlternatingItemStyle CssClass="LinkNormal DarkGray" />
                            <ItemStyle CssClass="LinkNormal LightGray" />
                            <FooterStyle CssClass="Content Bold Color4 FooterColor" />
                            <Columns>
                                <mbrsc:RowSelectorColumn SelectionMode="Single" />
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <center>
                                            <img src="/pics/icons/icon_magnifying_glass.gif"></center>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:HyperLinkColumn HeaderText="#" ItemStyle-Wrap="false" DataNavigateUrlField="ORDR_ID" DataNavigateUrlFormatString="/administrator/Transaction_Details.aspx?ID={0}&Referer=/Common/Filled_Orders.aspx" DataTextField="ORDR_ID" SortExpression="ORDR_ID ASC" />
                                <asp:BoundColumn DataField="PO_Sent" Visible="false" HeaderText="PO" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="VARSHIP" HeaderText="Status" SortExpression="VARSHIP ASC" />
                                <asp:BoundColumn DataField="VARRCNUMBER" HeaderText="RC#" SortExpression="VARRCNUMBER ASC" />
                                <asp:BoundColumn DataField="VARWAREHOUSE" HeaderText="Warehouse" SortExpression="VARWAREHOUSE ASC" />
                                <asp:BoundColumn DataField="VARWGHT" DataFormatString="{0:#,###}" HeaderText="Weight(lbs)" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="right" />
                                <asp:BoundColumn DataField="VARCONTRACT" HeaderText="Product" ItemStyle-Wrap="false" SortExpression="VARCONTRACT ASC" />
                                <asp:BoundColumn DataField="VARVALUE" HeaderText="Total Value" ItemStyle-Wrap="false" SortExpression="VARVALUE ASC" DataFormatString="{0:c}" ItemStyle-HorizontalAlign="right" />
                                <asp:BoundColumn DataField="VARDATE" HeaderText="Date" SortExpression="ORDR_DATE ASC" />
                                <asp:HyperLinkColumn HeaderText="Buyer" ItemStyle-Wrap="false" DataNavigateUrlField="SHIPMENT_BUYR" DataNavigateUrlFormatString="../common/MB_Specs_User.aspx?ID={0}" DataTextField="VARBUYR" SortExpression="VARBUYR ASC" />
                                <asp:HyperLinkColumn HeaderText="Seller" ItemStyle-Wrap="false" DataNavigateUrlField="SHIPMENT_SELR" DataNavigateUrlFormatString="../common/MB_Specs_User.aspx?ID={0}" DataTextField="VARSELR" SortExpression="VARSELR ASC" />
                                <asp:BoundColumn DataField="VAROPEN" HeaderText="Status" SortExpression="VAROPEN ASC" />
                                <asp:BoundColumn DataField="SHIPMENT_LEGACY_NUMBER" HeaderText="Legacy Number" SortExpression="SHIPMENT_LEGACY_NUMBER ASC" />
                            </Columns>
                        </asp:DataGrid><%}%>
                    </asp:Panel>
                    <asp:Panel ID="pnNoContent" runat="server" Visible="False">
                        <center>
                            There are no transactions recorded in our system
                        </center>
                    </asp:Panel>
                </td>
            </tr>
    </table>
</asp:Content>
