using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Mail;
using System.Text.RegularExpressions;
using System.Text;


namespace localhost.Common
{
	public partial class MB_Specs_Order : System.Web.UI.Page
	{
		//ArrayList aux;
		//ArrayList ArrayUpdate;
        
		protected System.Web.UI.WebControls.DataListItem dli;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblPay_ID;
		protected System.Web.UI.WebControls.TextBox TextBox1;


		public void Page_Load(object sender, EventArgs e)
		{
			/*
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C"))
			{
				Response.Redirect("/default.aspx");
			} 
			*/
			
			//Response.Write(dl.Items.ToString());
			if (!IsPostBack)
			{
				string[] arr2ID;
				Regex r = new Regex("-"); // Split on dashes.
				SqlConnection conn;

				conn = new SqlConnection(Application["DBConn"].ToString());
				conn.Open();

				arr2ID = r.Split(Request.QueryString["ID"].ToString());
				SqlCommand cmdShipmentID = new SqlCommand("Select SHIPMENT_ID, SHIPMENT_SKU FROM SHIPMENT where SHIPMENT_ORDR_ID = '" + arr2ID[0] + "'", conn);

				string strShipmentId = null;
				using( SqlDataReader dtShipmentId = cmdShipmentID.ExecuteReader() )
				{
					for( int i = 0; dtShipmentId.Read() ; i++ )
					{
						if( dtShipmentId[1].ToString() == arr2ID[1] ) //check shipment_sku
							strShipmentId = dtShipmentId[0].ToString();

						string skuType = dtShipmentId[1].ToString().Substring(0,2);
						if(  skuType== "BT" || skuType=="TL")
						{
							lblWarning.Visible = true;
							lblWarning.Text = "You can not convert this transaction! (Already Converted)";
							btnConvert.Enabled = false;
						}
					}
				}
				ViewState["SHIPMENT_ORDR_ID"] = arr2ID[0].ToString();
				ViewState["SHIPMENT_SKU"] = arr2ID[1].ToString();
				ViewState["SHIPMENT_ID"] = strShipmentId;
			
				
				string strType = Request["type"];

				if ((strType == null) && (!strType.Equals("BT") || !strType.Equals("TL")))
				{
				    Response.Write("ERROR: Incorrect type! Should be RC or TL!");
				    Response.End();
				}
				
				// kick out non rc orders
				//SqlCommand cmdSize;
				//cmdSize = new SqlCommand("SELECT SHIPMENT_SIZE FROM SHIPMENT where SHIPMENT_ID='" +ViewState["SHIPMENT_ID"].ToString()+"'" ,conn);
				//if (cmdSize.ExecuteScalar().ToString() !="190000")
				//{
				//    Response.Write("ERROR: You can't convert non RC orders");
				//    Response.End();
				//}


				SqlDataReader dtrUsers;
				StringBuilder sbSQL = new StringBuilder();
				sbSQL.Append("SELECT VARSELR=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)),");
				sbSQL.Append("	VARBUYR=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR))");
				sbSQL.Append("  FROM ORDERS O, SHIPMENT S WHERE O.ORDR_ID=S.SHIPMENT_ORDR_ID and S.SHIPMENT_ID='"+ViewState["SHIPMENT_ID"].ToString()+"'");
				SqlCommand cmdUsers = new SqlCommand(sbSQL.ToString(),conn);
				dtrUsers = cmdUsers.ExecuteReader();

				dtrUsers.Read();
				string strSuffix = "";

				if (strType == "BT")
				{
				    strSuffix = "BT";
				}
				else
				{
				    strSuffix = "TL";
				}

				Session["convert"] = strSuffix;

				lblBT1Name.Text = ViewState["SHIPMENT_ORDR_ID"].ToString() + "-" + strSuffix + "1 " + dtrUsers["VARSELR"].ToString();
				lblBT2Name.Text = ViewState["SHIPMENT_ORDR_ID"].ToString() + "-" + strSuffix + "2 " + dtrUsers["VARSELR"].ToString();
				lblBT3Name.Text = ViewState["SHIPMENT_ORDR_ID"].ToString() + "-" + strSuffix + "3 " + dtrUsers["VARSELR"].ToString();
				lblBT4Name.Text = ViewState["SHIPMENT_ORDR_ID"].ToString() + "-" + strSuffix + "4 " + dtrUsers["VARSELR"].ToString();
				lblBT5Name.Text = ViewState["SHIPMENT_ORDR_ID"].ToString() + "-" + strSuffix + "5 " + dtrUsers["VARSELR"].ToString();
				lblBT6Name.Text = ViewState["SHIPMENT_ORDR_ID"].ToString() + "-" + strSuffix + "6 " + dtrUsers["VARSELR"].ToString();

				lblRCName.Text = ViewState["SHIPMENT_ORDR_ID"].ToString() + "-RC " + dtrUsers["VARSELR"].ToString();

				dtrUsers.Close();

				// Query warehouses
				SqlDataReader dtrWarehouse;
				SqlCommand cmdWarehouse;
		                cmdWarehouse = new SqlCommand("SELECT PLAC_LABL,PLAC_ID FROM PlACE WHERE PLAC_TYPE='W' ORDER BY PLAC_LABL ", conn);
				dtrWarehouse = cmdWarehouse.ExecuteReader();

				//bind warehouses to list 
				ddlWarehouse.DataSource = dtrWarehouse;
				ddlWarehouse.DataTextField= "PLAC_LABL"; 
				ddlWarehouse.DataValueField= "PLAC_ID";
				ddlWarehouse.DataBind();
				dtrWarehouse.Close();


				Bind();
				conn.Close();
			}
		}
		public void Bind()
		{
			SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();

			string strSQL="SELECT PAYMENT.PAY_ID,PAYMENT.PAY_SHIPMENT, convert(varchar,PAYMENT.PAY_DATE,101) AS Pay_DATE, PAYMENT.PAY_AMNT, SHIPMENT.SHIPMENT_SKU";
			strSQL += " FROM PAYMENT INNER JOIN SHIPMENT ON PAYMENT.PAY_SHIPMENT = SHIPMENT.SHIPMENT_ID";
			strSQL += " WHERE   PAY_PERS=SHIPMENT_BUYR AND   (SHIPMENT.SHIPMENT_ORDR_ID = '"+ViewState["SHIPMENT_ORDR_ID"].ToString()+"')";
			strSQL += " ORDER BY PAYMENT.PAY_SHIPMENT";

			SqlDataAdapter dadContent;
			DataSet dstContent;
			dadContent = new SqlDataAdapter(strSQL ,conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			
			dl.DataSource = dstContent;
			dl.DataBind();
			conn.Close();
		}


		protected void dl_ItemDataBound(object sender, DataListItemEventArgs e) 
		{

			DropDownList ddl;
			if( e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item )
			{
				try
				{
					ddl = (DropDownList)e.Item.FindControl("Dropdownlist1");

					SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());
					conn.Open();
					//---------------
					System.Data.Common.DbDataRecord DataRow = (System.Data.Common.DbDataRecord)e.Item.DataItem;
					//string pay_shipment = Common.MakeStringOr(DataRow["PAY_SHIPMENT"], null);
					//---------------
					SqlCommand cmdContent = new SqlCommand("SELECT DISTINCT(SHIPMENT_SKU) FROM SHIPMENT WHERE (SHIPMENT_ORDR_ID = '" + ViewState["SHIPMENT_ID"].ToString() + "')", conn);
					SqlDataReader dtrContent = cmdContent.ExecuteReader();

					ddl.DataSource = dtrContent;
					ddl.DataTextField = "SHIPMENT_SKU";
					ddl.DataValueField = "SHIPMENT_SKU";

					ddl.DataBind();
					dtrContent.Close();
					conn.Close();
				}
				catch( Exception except )
				{
					Trace.Write("", "oops!", except);
				}
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion



		protected void Convert_Order(object sender, EventArgs e)
		{
			SqlConnection conn;
			conn=new SqlConnection(Application["DBConn"].ToString());	
			conn.Open();
			SqlConnection conn2;
			conn2=new SqlConnection(Application["DBConn"].ToString());	
			conn2.Open();
			SqlCommand cmdUpdate;
			string TPEID = "191";
			SqlDataReader dtr;
			SqlCommand cmdCurrent;
			cmdCurrent = new SqlCommand("SELECT * from SHIPMENT WHERE SHIPMENT_ID='" + ViewState["SHIPMENT_ID"].ToString()+"'",conn);
			dtr = cmdCurrent.ExecuteReader();

			if (dtr.Read())
			{
				int NonRCsize = Convert.ToInt32(dtr["SHIPMENT_SIZE"]);

				string strSuffix = Session["convert"].ToString();
				string size = "45000";
				if (strSuffix.Equals("BT"))
				{
				    size = "45000";
				}
				else if (strSuffix.Equals("TL"))
				{
				    size = "44092";
				}

				if (NonRCsize != 190000)
				{
				    size = Convert.ToString(NonRCsize / 5);
				}

				StringBuilder sb = new StringBuilder();
				StringBuilder sbValue = new StringBuilder();

				sb.Append("	INSERT INTO SHIPMENT ( SHIPMENT_ORDR_ID, SHIPMENT_BUYR,SHIPMENT_SELR,  SHIPMENT_FROM, SHIPMENT_TO, SHIPMENT_QTY,SHIPMENT_BUYR_PRCE, SHIPMENT_PRCE,  SHIPMENT_SIZE,SHIPMENT_SHIP_STATUS, SHIPMENT_BUYER_TERMS, SHIPMENT_FOB, SHIPMENT_SELLER_TERMS, SHIPMENT_SHIP_PRCE_EST, SHIPMENT_SKU,SHIPMENT_DATE_TAKE,SHIPMENT_DATE_DELIVERED,SHIPMENT_WEIGHT ");
				sbValue.Append(" VALUES(" + dtr["SHIPMENT_ORDR_ID"].ToString() + ", " + dtr["SHIPMENT_BUYR"].ToString() + "," + dtr["SHIPMENT_SELR"].ToString() + "," + ddlWarehouse.SelectedItem.Value.ToString() + ", " + dtr["SHIPMENT_TO"].ToString() + ", '" + dtr["SHIPMENT_QTY"].ToString() + "'," + dtr["SHIPMENT_BUYR_PRCE"].ToString() + ", " + dtr["SHIPMENT_PRCE"].ToString() + ", '" + size + "', ");
				sbValue.Append(" '" + dtr["SHIPMENT_SHIP_STATUS"].ToString() + "', '0','FOB/Delivered', " + dtr["SHIPMENT_SELLER_TERMS"].ToString() + ", " + txtBT1Freight.Text + " , '" + strSuffix + "1','" + txtBT1ShipDate.Text + "','" + txtBT1DelDate.Text + "','" + txtBT1Weight.Text + "' ");
				
				if (dtr["SHIPMENT_PO_NUM"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_PO_NUM");
					sbValue.Append(",'"+dtr["SHIPMENT_PO_NUM"].ToString()+"'");
				}
				if (dtr["SHIPMENT_COMM"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_COMM");
					sbValue.Append(",'"+dtr["SHIPMENT_COMM"].ToString()+"'");
				}
				if (dtr["SHIPMENT_BROKER_ID"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_BROKER_ID");
					sbValue.Append(",'"+dtr["SHIPMENT_BROKER_ID"].ToString()+"'");
				}
				if (dtr["SHIPMENT_BUYER_CLOSED_DATE"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_BUYER_CLOSED_DATE");
					sbValue.Append(",'"+dtr["SHIPMENT_BUYER_CLOSED_DATE"].ToString()+"'");
				}
				sb.Append(", SHIPMENT_SELLER_CLOSED_DATE1");
				sbValue.Append(",'"+dtr["SHIPMENT_DATE_TAKE"].ToString()+"'");
				// pulls stuff together 
				sb.Append(") " + sbValue.ToString() +");");

				cmdUpdate = new SqlCommand(sb.ToString(),conn2);
				cmdUpdate.ExecuteNonQuery();
                

				sb.Remove(0,sb.Length);
				sbValue.Remove(0,sbValue.Length);

				// get shipment value 

				SqlCommand cmdIdentity;
				cmdIdentity = new SqlCommand("SELECT @@IDENTITY AS NewID",conn2);
				ViewState["BT1_SHIPMENT_ID"]= cmdIdentity.ExecuteScalar();

				// number 2

				sb.Append("	INSERT INTO SHIPMENT ( SHIPMENT_ORDR_ID, SHIPMENT_BUYR,SHIPMENT_SELR,  SHIPMENT_FROM, SHIPMENT_TO, SHIPMENT_QTY,SHIPMENT_BUYR_PRCE, SHIPMENT_PRCE,  SHIPMENT_SIZE,SHIPMENT_SHIP_STATUS, SHIPMENT_BUYER_TERMS, SHIPMENT_FOB, SHIPMENT_SELLER_TERMS, SHIPMENT_SHIP_PRCE_EST, SHIPMENT_SKU,SHIPMENT_WEIGHT ");
				sbValue.Append(" VALUES(" + dtr["SHIPMENT_ORDR_ID"].ToString() + ", " + dtr["SHIPMENT_BUYR"].ToString() + "," + dtr["SHIPMENT_SELR"].ToString() + "," + ddlWarehouse.SelectedItem.Value.ToString() + ", " + dtr["SHIPMENT_TO"].ToString() + ", '" + dtr["SHIPMENT_QTY"].ToString() + "'," + dtr["SHIPMENT_BUYR_PRCE"].ToString() + ", " + dtr["SHIPMENT_PRCE"].ToString() + ", '" + size + "', ");
				sbValue.Append(" '" + dtr["SHIPMENT_SHIP_STATUS"].ToString() + "', '0', 'FOB/Delivered', " + dtr["SHIPMENT_SELLER_TERMS"].ToString() + ", " + txtBT2Freight.Text + " , '" + strSuffix + "2','" + txtBT2Weight.Text + "' ");
				if (dtr["SHIPMENT_PO_NUM"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_PO_NUM");
					sbValue.Append(",'"+dtr["SHIPMENT_PO_NUM"].ToString()+"'");
				}
				if (dtr["SHIPMENT_COMM"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_COMM");
					sbValue.Append(",'"+dtr["SHIPMENT_COMM"].ToString()+"'");
				}
				if (dtr["SHIPMENT_BROKER_ID"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_BROKER_ID");
					sbValue.Append(",'"+dtr["SHIPMENT_BROKER_ID"].ToString()+"'");
				}
				if (dtr["SHIPMENT_BUYER_CLOSED_DATE"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_BUYER_CLOSED_DATE");
					sbValue.Append(",'"+dtr["SHIPMENT_BUYER_CLOSED_DATE"].ToString()+"'");
				}
				sb.Append(", SHIPMENT_SELLER_CLOSED_DATE1");
				sbValue.Append(",'"+dtr["SHIPMENT_DATE_TAKE"].ToString()+"'");
				// pulls stuff together 
				sb.Append(") " + sbValue.ToString() +");");

				cmdUpdate = new SqlCommand(sb.ToString(),conn2);
				cmdUpdate.ExecuteNonQuery();

				sb.Remove(0,sb.Length);
				sbValue.Remove(0,sbValue.Length);

				// number 3

				sb.Append("	INSERT INTO SHIPMENT ( SHIPMENT_ORDR_ID, SHIPMENT_BUYR,SHIPMENT_SELR,  SHIPMENT_FROM, SHIPMENT_TO, SHIPMENT_QTY,SHIPMENT_BUYR_PRCE, SHIPMENT_PRCE,  SHIPMENT_SIZE,SHIPMENT_SHIP_STATUS, SHIPMENT_BUYER_TERMS, SHIPMENT_FOB, SHIPMENT_SELLER_TERMS, SHIPMENT_SHIP_PRCE_EST, SHIPMENT_SKU,SHIPMENT_WEIGHT ");
				sbValue.Append(" VALUES(" + dtr["SHIPMENT_ORDR_ID"].ToString() + ", " + dtr["SHIPMENT_BUYR"].ToString() + "," + dtr["SHIPMENT_SELR"].ToString() + "," + ddlWarehouse.SelectedItem.Value.ToString() + ", " + dtr["SHIPMENT_TO"].ToString() + ", '" + dtr["SHIPMENT_QTY"].ToString() + "'," + dtr["SHIPMENT_BUYR_PRCE"].ToString() + ", " + dtr["SHIPMENT_PRCE"].ToString() + ", '" + size + "', ");
				sbValue.Append(" '" + dtr["SHIPMENT_SHIP_STATUS"].ToString() + "', '0','FOB/Delivered', " + dtr["SHIPMENT_SELLER_TERMS"].ToString() + ", " + txtBT3Freight.Text + " , '" + strSuffix + "3','" + txtBT3Weight.Text + "' ");
				if (dtr["SHIPMENT_PO_NUM"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_PO_NUM");
					sbValue.Append(",'"+dtr["SHIPMENT_PO_NUM"].ToString()+"'");
				}
				if (dtr["SHIPMENT_COMM"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_COMM");
					sbValue.Append(",'"+dtr["SHIPMENT_COMM"].ToString()+"'");
				}
				if (dtr["SHIPMENT_BROKER_ID"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_BROKER_ID");
					sbValue.Append(",'"+dtr["SHIPMENT_BROKER_ID"].ToString()+"'");
				}
				if (dtr["SHIPMENT_BUYER_CLOSED_DATE"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_BUYER_CLOSED_DATE");
					sbValue.Append(",'"+dtr["SHIPMENT_BUYER_CLOSED_DATE"].ToString()+"'");
				}
				sb.Append(", SHIPMENT_SELLER_CLOSED_DATE1");
				sbValue.Append(",'"+dtr["SHIPMENT_DATE_TAKE"].ToString()+"'");
				// pulls stuff together 
				sb.Append(") " + sbValue.ToString() +");");

				cmdUpdate = new SqlCommand(sb.ToString(),conn2);
				cmdUpdate.ExecuteNonQuery();

				sb.Remove(0,sb.Length);
				sbValue.Remove(0,sbValue.Length);

				// number 4

				sb.Append("	INSERT INTO SHIPMENT ( SHIPMENT_ORDR_ID, SHIPMENT_BUYR,SHIPMENT_SELR,  SHIPMENT_FROM, SHIPMENT_TO, SHIPMENT_QTY,SHIPMENT_BUYR_PRCE, SHIPMENT_PRCE,  SHIPMENT_SIZE,SHIPMENT_SHIP_STATUS, SHIPMENT_BUYER_TERMS, SHIPMENT_FOB, SHIPMENT_SELLER_TERMS, SHIPMENT_SHIP_PRCE_EST, SHIPMENT_SKU,SHIPMENT_WEIGHT ");
				sbValue.Append(" VALUES(" + dtr["SHIPMENT_ORDR_ID"].ToString() + ", " + dtr["SHIPMENT_BUYR"].ToString() + "," + dtr["SHIPMENT_SELR"].ToString() + "," + ddlWarehouse.SelectedItem.Value.ToString() + ", " + dtr["SHIPMENT_TO"].ToString() + ", '" + dtr["SHIPMENT_QTY"].ToString() + "'," + dtr["SHIPMENT_BUYR_PRCE"].ToString() + ", " + dtr["SHIPMENT_PRCE"].ToString() + ", '" + size + "', ");
				sbValue.Append(" '" + dtr["SHIPMENT_SHIP_STATUS"].ToString() + "', '0', 'FOB/Delivered', " + dtr["SHIPMENT_SELLER_TERMS"].ToString() + ", " + txtBT4Freight.Text + " , '" + strSuffix + "4','" + txtBT4Weight.Text + "' ");
				if (dtr["SHIPMENT_PO_NUM"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_PO_NUM");
					sbValue.Append(",'"+dtr["SHIPMENT_PO_NUM"].ToString()+"'");
				}
				if (dtr["SHIPMENT_COMM"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_COMM");
					sbValue.Append(",'"+dtr["SHIPMENT_COMM"].ToString()+"'");
				}
				if (dtr["SHIPMENT_BROKER_ID"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_BROKER_ID");
					sbValue.Append(",'"+dtr["SHIPMENT_BROKER_ID"].ToString()+"'");
				}
				if (dtr["SHIPMENT_BUYER_CLOSED_DATE"] != System.DBNull.Value)	
				{
					sb.Append(", SHIPMENT_BUYER_CLOSED_DATE");
					sbValue.Append(",'"+dtr["SHIPMENT_BUYER_CLOSED_DATE"].ToString()+"'");
				}
				sb.Append(", SHIPMENT_SELLER_CLOSED_DATE1");
				sbValue.Append(",'"+dtr["SHIPMENT_DATE_TAKE"].ToString()+"'");
				// pulls stuff together 
				sb.Append(") " + sbValue.ToString() +");");

				cmdUpdate = new SqlCommand(sb.ToString(),conn2);
				cmdUpdate.ExecuteNonQuery();

				sb.Remove(0,sb.Length);
				sbValue.Remove(0,sbValue.Length);

				 // number 5

				sb.Append("	INSERT INTO SHIPMENT ( SHIPMENT_ORDR_ID, SHIPMENT_BUYR,SHIPMENT_SELR,  SHIPMENT_FROM, SHIPMENT_TO, SHIPMENT_QTY,SHIPMENT_BUYR_PRCE, SHIPMENT_PRCE,  SHIPMENT_SIZE,SHIPMENT_SHIP_STATUS, SHIPMENT_BUYER_TERMS, SHIPMENT_FOB, SHIPMENT_SELLER_TERMS, SHIPMENT_SHIP_PRCE_EST, SHIPMENT_SKU,SHIPMENT_WEIGHT ");
				sbValue.Append(" VALUES(" + dtr["SHIPMENT_ORDR_ID"].ToString() + ", " + dtr["SHIPMENT_BUYR"].ToString() + "," + dtr["SHIPMENT_SELR"].ToString() + "," + ddlWarehouse.SelectedItem.Value.ToString() + ", " + dtr["SHIPMENT_TO"].ToString() + ", '" + dtr["SHIPMENT_QTY"].ToString() + "'," + dtr["SHIPMENT_BUYR_PRCE"].ToString() + ", " + dtr["SHIPMENT_PRCE"].ToString() + ", '" + size + "', ");
				sbValue.Append(" '" + dtr["SHIPMENT_SHIP_STATUS"].ToString() + "', '0','FOB/Delivered', " + dtr["SHIPMENT_SELLER_TERMS"].ToString() + ", " + txtBT5Freight.Text + " , '" + strSuffix + "5','" + txtBT5Weight.Text + "' ");
				if (dtr["SHIPMENT_PO_NUM"] != System.DBNull.Value)
				{
				    sb.Append(", SHIPMENT_PO_NUM");
				    sbValue.Append(",'" + dtr["SHIPMENT_PO_NUM"].ToString() + "'");
				}
				if (dtr["SHIPMENT_COMM"] != System.DBNull.Value)
				{
				    sb.Append(", SHIPMENT_COMM");
				    sbValue.Append(",'" + dtr["SHIPMENT_COMM"].ToString() + "'");
				}
				if (dtr["SHIPMENT_BROKER_ID"] != System.DBNull.Value)
				{
				    sb.Append(", SHIPMENT_BROKER_ID");
				    sbValue.Append(",'" + dtr["SHIPMENT_BROKER_ID"].ToString() + "'");
				}
				if (dtr["SHIPMENT_BUYER_CLOSED_DATE"] != System.DBNull.Value)
				{
				    sb.Append(", SHIPMENT_BUYER_CLOSED_DATE");
				    sbValue.Append(",'" + dtr["SHIPMENT_BUYER_CLOSED_DATE"].ToString() + "'");
				}
				sb.Append(", SHIPMENT_SELLER_CLOSED_DATE1");
				sbValue.Append(",'" + dtr["SHIPMENT_DATE_TAKE"].ToString() + "'");
				// pulls stuff together 
				sb.Append(") " + sbValue.ToString() + ");");

				cmdUpdate = new SqlCommand(sb.ToString(), conn2);
				cmdUpdate.ExecuteNonQuery();

				sb.Remove(0, sb.Length);
				sbValue.Remove(0, sbValue.Length);


				// number 6
				if (txtBT6Weight.Visible)
				{
				    sb.Append("	INSERT INTO SHIPMENT ( SHIPMENT_ORDR_ID, SHIPMENT_BUYR,SHIPMENT_SELR,  SHIPMENT_FROM, SHIPMENT_TO, SHIPMENT_QTY,SHIPMENT_BUYR_PRCE, SHIPMENT_PRCE,  SHIPMENT_SIZE,SHIPMENT_SHIP_STATUS, SHIPMENT_BUYER_TERMS, SHIPMENT_FOB, SHIPMENT_SELLER_TERMS, SHIPMENT_SHIP_PRCE_EST, SHIPMENT_SKU,SHIPMENT_WEIGHT ");
				    sbValue.Append(" VALUES(" + dtr["SHIPMENT_ORDR_ID"].ToString() + ", " + dtr["SHIPMENT_BUYR"].ToString() + "," + dtr["SHIPMENT_SELR"].ToString() + "," + ddlWarehouse.SelectedItem.Value.ToString() + ", " + dtr["SHIPMENT_TO"].ToString() + ", '" + dtr["SHIPMENT_QTY"].ToString() + "'," + dtr["SHIPMENT_BUYR_PRCE"].ToString() + ", " + dtr["SHIPMENT_PRCE"].ToString() + ", '" + size + "', ");
				    sbValue.Append(" '" + dtr["SHIPMENT_SHIP_STATUS"].ToString() + "', '0', 'FOB/Delivered', " + dtr["SHIPMENT_SELLER_TERMS"].ToString() + ", " + txtBT6Freight.Text + " , '" + strSuffix + "6','" + txtBT6Weight.Text + "' ");
				    if (dtr["SHIPMENT_PO_NUM"] != System.DBNull.Value)
				    {
					sb.Append(", SHIPMENT_PO_NUM");
					sbValue.Append(",'" + dtr["SHIPMENT_PO_NUM"].ToString() + "'");
				    }
				    if (dtr["SHIPMENT_COMM"] != System.DBNull.Value)
				    {
					sb.Append(", SHIPMENT_COMM");
					sbValue.Append(",'" + dtr["SHIPMENT_COMM"].ToString() + "'");
				    }
				    if (dtr["SHIPMENT_BROKER_ID"] != System.DBNull.Value)
				    {
					sb.Append(", SHIPMENT_BROKER_ID");
					sbValue.Append(",'" + dtr["SHIPMENT_BROKER_ID"].ToString() + "'");
				    }
				    if (dtr["SHIPMENT_BUYER_CLOSED_DATE"] != System.DBNull.Value)
				    {
					sb.Append(", SHIPMENT_BUYER_CLOSED_DATE");
					sbValue.Append(",'" + dtr["SHIPMENT_BUYER_CLOSED_DATE"].ToString() + "'");
				    }
				    sb.Append(", SHIPMENT_SELLER_CLOSED_DATE1");
				    sbValue.Append(",'" + dtr["SHIPMENT_DATE_TAKE"].ToString() + "'");
				    // pulls stuff together 
				    sb.Append(") " + sbValue.ToString() + ");");

				    cmdUpdate = new SqlCommand(sb.ToString(), conn2);
				    cmdUpdate.ExecuteNonQuery();

				    sb.Remove(0, sb.Length);
				    sbValue.Remove(0, sbValue.Length);
				}
		                

                //done with 6

 
				sb.Append("	Update SHIPMENT ");
				sb.Append("	set SHIPMENT_BUYR ='"+TPEID+"' , ");
				sb.Append("	SHIPMENT_TO='"+ddlWarehouse.SelectedItem.Value.ToString()+"', ");
				sb.Append("	SHIPMENT_SKU='RC1', ");
				sb.Append("	SHIPMENT_PO_NUM=SHIPMENT_ORDR_ID, ");
				sb.Append("	SHIPMENT_QTY='1', ");
				sb.Append("	SHIPMENT_SELLER_TERMS='0', ");
				sb.Append("	SHIPMENT_SIZE='190000', ");
				sb.Append("	SHIPMENT_BUYR_PRCE=SHIPMENT_PRCE, ");
				sb.Append("	SHIPMENT_BUYER_CLOSED_DATE= SHIPMENT_DATE_TAKE, ");
				sb.Append("	SHIPMENT_COMM='0', ");
				sb.Append("	SHIPMENT_BROKER_ID = null, ");
				sb.Append("	SHIPMENT_SHIP_PRCE_EST ='"+txtRCFreight.Text+"', ");
				sb.Append("	SHIPMENT_DATE_TAKE='"+txtRCShipDate.Text+"', ");
				sb.Append("	SHIPMENT_DATE_DELIVERED='"+txtRCDelDate.Text+"', ");
				sb.Append("	SHIPMENT_WEIGHT='"+txtRCWeight.Text+"' ");
				sb.Append("	WHERE SHIPMENT_ID = '"+ViewState["SHIPMENT_ID"].ToString()+"' ");

				cmdUpdate = new SqlCommand(sb.ToString(),conn2);
				cmdUpdate.ExecuteNonQuery();
				//Response.Write(sb.ToString());
				
			
				string strDataKey;
				int k = 0 ;
				string strSQL="";
				SqlCommand cmdUpdatePayments;
				foreach ( DataListItem myDataListItem in dl.Items)
				{
					strDataKey = dl.DataKeys[k].ToString();
					
					switch (((DropDownList)myDataListItem.FindControl("Dropdownlist1")).SelectedItem.Text)
					{
						case "BT1":
						    strSQL = "UPDATE PAYMENT SET PAY_SHIPMENT='" + (Convert.ToInt32(ViewState["BT1_SHIPMENT_ID"].ToString()) + 0).ToString() + "' WHERE PAY_ID='" + strDataKey + "'";
										break;
						case "BT2":
							strSQL = "UPDATE PAYMENT SET PAY_SHIPMENT='" + (Convert.ToInt32(ViewState["BT1_SHIPMENT_ID"].ToString()) + 1).ToString() + "' WHERE PAY_ID='" + strDataKey + "'";
							break;
						case "BT3":
							strSQL = "UPDATE PAYMENT SET PAY_SHIPMENT='" + (Convert.ToInt32(ViewState["BT1_SHIPMENT_ID"].ToString()) + 2).ToString() + "' WHERE PAY_ID='" + strDataKey + "'";
							break;
						case "BT4":
							strSQL = "UPDATE PAYMENT SET PAY_SHIPMENT='" + (Convert.ToInt32(ViewState["BT1_SHIPMENT_ID"].ToString()) + 3).ToString() + "' WHERE PAY_ID='" + strDataKey + "'";
							break;
						case "BT5":
						    strSQL = "UPDATE PAYMENT SET PAY_SHIPMENT='" + (Convert.ToInt32(ViewState["BT1_SHIPMENT_ID"].ToString()) + 4).ToString() + "' WHERE PAY_ID='" + strDataKey + "'";
						    break;
					}
					cmdUpdatePayments = new SqlCommand(strSQL,conn2);
					cmdUpdatePayments.ExecuteNonQuery();
					k++;

				}
				int iDiff;
				iDiff = (Convert.ToInt32(txtRCWeight.Text) - Convert.ToInt32(txtBT1Weight.Text)-Convert.ToInt32(txtBT2Weight.Text)-Convert.ToInt32(txtBT3Weight.Text)-Convert.ToInt32(txtBT4Weight.Text));
				if (System.Math.Abs(iDiff) >= 50 )
				{
					

					MailMessage maildef=new MailMessage();
					maildef.From = "larry@theplasticsexchange.com";
					maildef.To = "larry@theplasticsexchange.com";
					maildef.Cc = Application["strEmailAdmin"].ToString();

					maildef.Subject = "Problem: Not equal weights converting Order #:" + ViewState["SHIPMENT_ORDR_ID"].ToString() + ".  Difference = " + iDiff.ToString();

					// message body definition
					maildef.Body = "<HTML><head><title>The Plastics Exchange</title><link type='text/css' rel='stylesheet' href='http://wifi.tiscali.fr/css/ie.css'></head><body topmargin='5' leftmargin='5' marginheight='5' marginwidth='5'>"           
						+ "Problem: Not equal weights  -  Difference = "+ iDiff.ToString() +", Order Number: " + ViewState["SHIPMENT_ORDR_ID"].ToString() 
						+ "</body>" 
						+ "</html>";

					maildef.BodyFormat = MailFormat.Html;
					//SmtpMail.SmtpServer="localhost";
					TPE.Utility.EmailLibrary.Send(Context,maildef);
				}
				Response.Redirect("/common/Filled_Orders.aspx");
			
				

			}

			dtr.Close();
			conn.Close();
		}


		protected void btnAddBTTL6_OnClick(object sender, EventArgs e)
		{
		    lblBT6Name.Visible = true;
		    txtBT6Weight.Visible = true;
		    txtBT6ShipDate.Visible = true;
		    txtBT6DelDate.Visible = true;
		    txtBT6Freight.Visible = true;
		}

	}
}
