<%@ Page Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="userportal.aspx.cs" Inherits="localhost.userportal" Title="The Plastic Exchange" %>

<%@ Register TagPrefix="uc1" TagName="SpotGrid" Src="/spot/SpotGrid.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">



</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cphCssLink" runat="server">
    <style type="text/css">
        .WhiteLink a:link {color:#FFFFFF; font-size: 10px;}
        .WhiteLink a:visited {color:#FFFFFF; font-size: 10px;}
        .WhiteLink a:hover {color:#FFFFFF; font-size: 10px;}
        .OrangeLink a:link {color:#FD9D00; font-size: 12px;}
        .OrangeLink a:visited {color:#FD9D00; font-size: 12px;}
        .OrangeLink a:hover {color:#FD9D00; font-size: 12px;}
        .RedLink a:link {color:red; font-size: 12px;}
        .RedLink a:visited {color:red; font-size: 12px;}
        .RedLink a:hover {color:red; font-size: 12px;}
        .LinkNormal { text-align: left;}
        .Content 	{
			    font-family: Verdana, Arial, Helvetica, sans-serif;
			    font-size: 10px;
			    font-style: normal;
			    line-height: normal;
			    font-variant: normal;
			    padding-left: 2px;
			    }
    </style>
</asp:Content>






<asp:Content ID="Content3" ContentPlaceHolderID="cphMain" runat="server">

        
<table cellspacing="0" cellpadding="0" width="780"  align="center" bgcolor="#dddddd" border="0">       
    
    <tr>
        <td align="left" valign="top">                        
            <asp:HyperLink  runat="server" ID="hlFilledOrders" Text="Filled Orders...."  NavigateUrl="~/Common/Simple_Filled_Orders.aspx" />                 
            <asp:DataGrid ID="dgBuyer" BackColor="#000000" CellSpacing="1" BorderWidth="1" runat="server" 
                Width="389" HorizontalAlign="Center" CellPadding="2" DataKeyField="ORDR_ID" AllowSorting="true" 
                AutoGenerateColumns="false" OnItemDataBound="ProcessBound" PageSize="9" AllowPaging="true" PagerStyle-Visible="false">
                <HeaderStyle CssClass="LinkNormal Bold OrangeColor" HorizontalAlign="Center" />
                <AlternatingItemStyle CssClass="LinkNormal DarkGray" />
                <ItemStyle CssClass="LinkNormal LightGray" />
                <FooterStyle CssClass="Content Bold Color4 FooterColor" />
                <Columns>                                
                    <asp:BoundColumn DataField="ORDR_ID" HeaderText="#" ItemStyle-Wrap="false" SortExpression="ORDR_ID ASC" />
                    <asp:BoundColumn DataField="VARSHIP" HeaderText="Status" SortExpression="VARSHIP ASC" />
                    <asp:BoundColumn DataField="VARRCNUMBER" HeaderText="RC#" SortExpression="Vno ARRCNUMBER ASC" visible="false" />
                    <asp:BoundColumn DataField="VARWAREHOUSE" HeaderText="Warehouse" SortExpression="VARWAREHOUSE ASC" visible="true" />
                    <asp:BoundColumn DataField="VARWGHT" DataFormatString="{0:#,###}" HeaderText="Weight(lbs)" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="right"  visible="false" />                                        
                    <asp:BoundColumn DataField="VARCONTRACT" HeaderText="Product" ItemStyle-Wrap="false" SortExpression="VARCONTRACT ASC" visible="false" />
                    <asp:BoundColumn DataField="VARDATE" HeaderText="Date" SortExpression="ORDR_DATE ASC" />                              
                </Columns>
            </asp:DataGrid>                     
        </td>        
        <td> &nbsp; </td>
        <td align="left" valign="top">
            <asp:Label runat="server" id="lblWorkingOrders"  Text="Working Orders" />                                 
            <asp:DataGrid ID="dgOffers" BackColor="#000000" CellSpacing="1" BorderWidth="1" runat="server" 
                Width="389" HorizontalAlign="Center" CellPadding="2" DataKeyField="varid" AllowSorting="true" 
                AutoGenerateColumns="false">
                <HeaderStyle CssClass="LinkNormal Bold OrangeColor" HorizontalAlign="Center" />
                <AlternatingItemStyle CssClass="LinkNormal DarkGray" />
                <ItemStyle CssClass="LinkNormal LightGray" />
                <FooterStyle CssClass="Content Bold Color4 FooterColor" />
                <Columns>                                
                    <asp:BoundColumn DataField="varid" HeaderText="Offer #" ItemStyle-Wrap="false" SortExpression="varid ASC" />
                    <asp:BoundColumn DataField="VARSIZE" HeaderText="Size" SortExpression="VARSHIP ASC" />
                    <asp:BoundColumn DataField="VARDATE" HeaderText="Date" SortExpression="VARDATE ASC" />                                
                    <asp:BoundColumn DataField="VAREXPR" HeaderText="Expire" SortExpression="VAREXPR ASC" />                                                    
                </Columns>
            </asp:DataGrid>              
            <asp:Label runat="server" id="lblWorkingBids"  Text="Working Bids" />                 
            <asp:DataGrid ID="dgBids" BackColor="#000000" CellSpacing="1" BorderWidth="1" runat="server" 
                Width="389" HorizontalAlign="Center" CellPadding="2" DataKeyField="varid" AllowSorting="true" 
                AutoGenerateColumns="false">
                <HeaderStyle CssClass="LinkNormal Bold OrangeColor" HorizontalAlign="Center" />
                <AlternatingItemStyle CssClass="LinkNormal DarkGray" />
                <ItemStyle CssClass="LinkNormal LightGray" />
                <FooterStyle CssClass="Content Bold Color4 FooterColor" />
                <Columns>                                
                    <asp:BoundColumn DataField="varid" HeaderText="Bid #" ItemStyle-Wrap="false" SortExpression="varid ASC" />
                    <asp:BoundColumn DataField="VARSIZE" HeaderText="Size" SortExpression="VARSHIP ASC" />
                    <asp:BoundColumn DataField="VARDATE" HeaderText="Date" SortExpression="VARDATE ASC" />                                
                    <asp:BoundColumn DataField="VAREXPR" HeaderText="Expire" SortExpression="VAREXPR ASC" />                                
                </Columns>
            </asp:DataGrid>    
        </td> 
    </tr>


</table>

    <table cellspacing="0" cellpadding="0" width="780" align="center" bgcolor="#666666" border="0">       
        <tr>
            <td width="385">
                <table cellspacing="0" cellpadding="0" width="390" border="0">
                    <tr>
                        <td width="6" background="/pics/home_bar_titles_r1_c1.jpg">
                            <img height="17px" src="/pics/home_bar_titles_r1_c1.jpg" width="6" /></td>
                        <td class="LinkNormal OrangeLink" valign="middle" width="384" background="/pics/home_bar_titles_r1_c1.jpg" height="10px">
                            <a href="/Public/Public_News.aspx">Industry News</a></td>
                    </tr>
                    <tr>
                        <td colspan="3" height=1px>
                            <img height="1" src="/pics/home_bar_titles_r2_c1.jpg" width="390" /></td>
                    </tr>
                    
                    <tr>
                        <td colspan="3" height="16px" bgcolor="black">                        
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top" width="395" bgcolor="#666666">
                <table cellspacing="0" cellpadding="0" width="390" border="0">
                    <tr>
                        <td align="left" width="384" background="/pics/home_bar_titles_r1_c1.jpg" colspan="4" height=10px>                            
                            <span class="Header Bold Color1">Market Research&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span id="lblDateMR" class="Content Bold Color4" runat="server"></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height: 1px">
                            <img height="1" src="/pics/home_bar_titles_r2_c1.jpg" width="390"></td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="black" style="width: 177px">
                            <span class="LinkNormal RedLink" style="text-align: left">
                                <a id="url_mu" href="/Research/WeeklyReview.aspx" class="LinkNormal RedLink">
                                    &nbsp;Market Update
                                </a>
                            </span> 
                        </td>
                        <td align="right" bgcolor="black" height=16px>
                            &nbsp;
                        </td> 
                    </tr>
                    <tr>
                        <td colspan="3">
                            <img height="1" src="/pics/home_bar_titles_r2_c1.jpg" width="390"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="#dbdcd7">
                <asp:placeholder id="phNews" runat="server"></asp:placeholder>
            </td>
            <td valign="top" width="390" bgcolor="#dbdcd7">
                <table cellspacing="0" cellpadding="0" width="390" border="0">
                    <tr>
                        <td width="1" background="/pics/home_research_r1_c2.jpg">
                            <img height="94" src="/pics/home_research_r1_c2.jpg" width="1"></td>
                        <td valign="middle" align="center" style="width: 25px">
                            <img height="94" src="/images2/research_setas_r1_c1.jpg" width="24"></td>
                        <td width="1" bgcolor="#333333">
                            <img height="94" src="/pics/home_research_r1_c2.jpg" width="1"></td>
                        <td valign="middle" align="left" width="337">
                            <table height="90" cellspacing="1" cellpadding="5" width="360" border="0">
                                <tr>
                                    <td valign="top">
                                        <span class="LinkNormal Content"><a href="/Research/WeeklyReview.aspx#Polyethylene" id="lnk1"><span class="blackbold">Polyethylene:</span></a></span> 
                                            <div id="lblPE" class="Content Color2" runat="server">
                                                </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="/pics/home_research_r1_c2.jpg">
                            <img height="1" src="/pics/home_research_r2_c2.jpg" width="1"></td>
                        <td valign="middle" align="center" bgcolor="#333333" style="width: 25px">
                            <img height="1" src="/pics/home_research_r2_c1.jpg" width="24"></td>
                        <td bgcolor="#333333">
                            <img height="1" src="/pics/home_research_r2_c2.jpg" width="1"></td>
                        <td valign="top" bgcolor="#333333">
                            <img height="1" src="/pics/home_research_r2_c1.jpg" width="24"></td>
                    </tr>
                    <tr>
                        <td background="/pics/home_research_r1_c2.jpg">
                            <img height="94" src="/pics/home_research_r1_c2.jpg" width="1"></td>
                        <td valign="middle" align="center" style="width: 25px">
                            <img height="94" src="/images2/research_setas_r1_c1.jpg" width="24"></td>
                        <td bgcolor="#333333">
                            <img height="94" src="/pics/home_research_r1_c2.jpg" width="1"></td>
                        <td valign="middle" align="left">
                            <table height="90" cellspacing="1" cellpadding="5" width="360" border="0">
                                <tr>
                                    <td valign="top" align="left">
                                        <span class="LinkNormal Content"><a href="/Research/WeeklyReview.aspx#Polypropylene" id="lnk2"><span class="blackbold">Polypropylene:</span></a></span>
                                            
                                            <div id="lblPP" class="Content Color2" runat="server">
                                                </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="/pics/home_research_r1_c2.jpg" style="height: 1px">
                            <img height="1" src="/pics/home_research_r2_c2.jpg" width="1" /></td>
                        <td valign="middle" align="center" bgcolor="#333333" style="height: 1px; width: 25px;">
                            <img height="1" src="/pics/home_research_r2_c1.jpg" width="24" /></td>
                        <td bgcolor="#333333" style="height: 1px">
                            <img height="1" src="/pics/home_research_r2_c2.jpg" width="1" /></td>
                        <td valign="top" bgcolor="#333333" style="height: 1px">
                            <img height="1" src="/pics/home_research_r2_c1.jpg" width="24" /></td>
                    </tr>
                    <tr>
                        <td background="/pics/home_research_r1_c2.jpg">
                            <img height="94" src="/pics/home_research_r1_c2.jpg" width="1" /></td>
                        <td valign="middle" align="center" style="width: 25px">
                            <img height="94" src="/images2/research_setas_r1_c1.jpg" width="24" /></td>
                        <td bgcolor="#333333">
                            <img height="94" src="/pics/home_research_r1_c2.jpg" width="1" /></td>
                        <td valign="middle" align="left">
                            <table height="90" cellspacing="1" cellpadding="5" width="360" border="0">
                                <tr>
                                    <td valign="top">
                                        <span class="LinkNormal Content"><a href="/Research/WeeklyReview.aspx#Polystyrene" id="lnk3"><span class="blackbold">Polystyrene:</span></a></span> 
                                            <div id="lblPS" class="Content Color2" runat="server">
                                                </div>
                                        
                                    </td>
                    
                            </table>
                                 
                          
                                
                        </td>
                    </tr>
                    <td background="/pics/home_research_r2_c1.jpg" style="height: 1px" colspan="4">
                            <img height="1" src="/pics/home_research_r2_c2.jpg" width="1" /></td>
                </table>
            </td>
        </tr>
    </table>


</asp:Content>
