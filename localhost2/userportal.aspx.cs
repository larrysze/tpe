using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using MetaBuilders.WebControls;
using System.Text.RegularExpressions;


namespace localhost
{
    public partial class userportal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C") && ((string)Session["Typ"] != "L"))
            {
                Response.Redirect("../default.aspx");
            }
            if (!IsPostBack)
            {
                BindDataGrid();
            }

        }

        protected void BindDataGrid()
        {
            Session["Comp_Id"] = "368";

            SqlConnection conn;
            string strSQL;
            conn = new SqlConnection(Application["DBConn"].ToString());
            conn.Open();
            SqlDataAdapter dadContent;
            DataSet dstContent;
            SqlDataReader dtrAnyTrans;
            SqlCommand cmdAnyTrans;

            strSQL = "spFilled_Orders @Sort='VARDATE ASC'";
            //strSQL = "spUserPortal_FilledOrders @Sort='VARDATE ASC'";
            
            //if (!ddlCompanies.SelectedItem.Value.Equals("*"))
            //{
            //    strSQL += ",@Comp='" + ddlCompanies.SelectedItem.Value.ToString() + "'";
            //}
            
            //if (!ddlMonth.SelectedItem.Value.Equals("null"))
            //{
                // value passed is in MM-YYYY format. needs to broken up into separate parameters
                //string[] strDate;
                //Regex r = new Regex("-"); // Split on spaces.
                //strDate = r.Split(ddlMonth.SelectedItem.Value.ToString());
                //strSQL += ",@MM='" + strDate[0] + "'";
                //strSQL += ",@YY='" + strDate[1] + "'";

                //strSQL += ",@MM='1'";
                //strSQL += ",@YY='2008'";
    
            //}

            // If non admin is logged on, restrict the filled orders to the current company
            //if (Session["Typ"].ToString().Equals("P") || Session["Typ"].ToString().Equals("OP") || Session["Typ"].ToString().Equals("S") || Session["Typ"].ToString().Equals("OS"))
            //{
                
                strSQL += ",@Comp='" + Session["Comp_Id"] + "'";
            //}

            strSQL += ",@SEARCH=''";
            //strSQL +=",@SEARCH='S.SHIPMENT_ORDR_ID='3055'";
            //Response.End();
            
            cmdAnyTrans = new SqlCommand(strSQL, conn);
            dtrAnyTrans = cmdAnyTrans.ExecuteReader();
            
            // start off by making them all visible
            //pnContent.Visible = true;
            //pnNoContent.Visible = false;
            //btnPO.Visible = true;
            //btnInvoice.Visible = true;
            //btnEdit.Visible = true;
            //btnCert.Visible = true;
            //btnSalesCon.Visible = true;


            if (dtrAnyTrans.HasRows)
            {
                dtrAnyTrans.Close();
                // make sure the right panels are visible
                //pnContent.Visible = true;
                //pnNoContent.Visible = false;
            //    //Response.Write(strSQL);
                dadContent = new SqlDataAdapter(strSQL, conn);
                dstContent = new DataSet();
                dadContent.Fill(dstContent);
                
                
            //    //if ((Session["Typ"].ToString().Equals("S")) || (Session["Typ"].ToString().Equals("OS")))
            //    //{
            //    //    dgSeller.DataSource = dstContent;
            //    //    dgSeller.DataBind();
            //    //}
            //    //else            
            ////{
                    dgBuyer.DataSource = dstContent;
                    dgBuyer.DataBind();

                    //dg.DataSource = dstContent;
                    //dg.DataBind();
              }
            //}
            //else
            //{
            //    // there are no transactions so we need to display a different panel
            //    pnContent.Visible = false;
            //    pnNoContent.Visible = true;
            //    btnPO.Visible = false;
            //    btnInvoice.Visible = false;
            //    btnEdit.Visible = false;
            //    btnCert.Visible = false;
            //    btnSalesCon.Visible = false;

            //}

            //if ((Session["Typ"].ToString().Equals("P")) || (Session["Typ"].ToString().Equals("OP")))
            //{
            //    btnPO.Visible = false;
            //    btnEdit.Visible = false;
            //    btnCert.Visible = false;
            //    btnSalesCon.Visible = false;

            //    pnAdminFunctions.Visible = false;

            //}
            //if ((Session["Typ"].ToString().Equals("S")) || (Session["Typ"].ToString().Equals("OS")))
            //{
            //    btnInvoice.Visible = false;
            //    btnEdit.Visible = false;
            //    btnCert.Visible = false;
            //    btnSalesCon.Visible = false;

            //    pnAdminFunctions.Visible = false;

            //}

                    SqlDataAdapter dadWorking;
                    DataSet dstWorking;
                    SqlDataReader dtrWorkingTrans;
                    string sSQLWorking = "spSpot_Floor @CompID='" + Session["Comp_Id"] + "',@Sort='VARID DESC',@UserType='S'";

                    dadWorking = new SqlDataAdapter(sSQLWorking, conn);
                    dstWorking = new DataSet();
                    dadWorking.Fill(dstWorking);

                    dgOffers.DataSource = dstWorking;
                    dgOffers.DataBind();

                    sSQLWorking = "spSpot_Floor @CompID='" + Session["Comp_Id"] + "',@Sort='VARID DESC',@UserType='P'";
                    dadWorking = new SqlDataAdapter(sSQLWorking, conn);
                    dstWorking = new DataSet();
                    dadWorking.Fill(dstWorking);

                    dgBids.DataSource = dstWorking;
                    dgBids.DataBind();


         //   conn.Close();
         ////   search = "";


        }



        protected void ProcessBound(object sender, DataGridItemEventArgs e)
        {
            //Session["Comp_Id"] 
            
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    if (Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "VARBUYCOMP")) == Convert.ToDouble(Session["Comp_Id"]))
            //    {
            //        e.Item.Cells[7].Text = "B";
            //    }
            //    else
            //    {
            //        e.Item.Cells[7].Text = "S";
            //    }

            //    //if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VARNUMFILES")) != "0")
            //    //{
            //    //    e.Item.Cells[4].Text = "<a href=\"/common/Order_Documents.aspx?ID=" + strID + "\"><img border=\"0\" src=\"/pics/icons/icon_filled_folder.gif\"></a>";
            //    //}


            //    //runningSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARVALUE"));
            //}


        }
    
    
    
    }


}
