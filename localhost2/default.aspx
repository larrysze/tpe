<%@ Page Language="c#" Codebehind="default.aspx.cs" AutoEventWireup="True" Inherits="localhost.homepage" MasterPageFile="~/MasterPages/Menu.Master" enableEventValidation="false"  %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="cc1" namespace="System.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">

 <script language="javascript" type="text/javascript">
    function clientActiveTabChanged(sender, args) 
    {         
        var spot = document.getElementById("ctl00_cphMain_lblSpotChartID").innerHTML;     
        var contract = document.getElementById("ctl00_cphMain_lblContractChartID").innerHTML;         
         
        if(sender.get_activeTabIndex() == 0)
        {   
            document.getElementsByName("ChartImg")[0].src = "/Research/Charts/home/Spot_" + spot + ".png";            
        }
        if(sender.get_activeTabIndex() == 1)
        {
            document.getElementsByName("ChartImg")[0].src = "/Research/Charts/home/Contract_" + contract + ".png";                    
        }            
    }
</script>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="cphCssLink" runat="server">
    <style type="text/css">
    .WhiteLink a:link {color:#FFFFFF; font-size: 13px;}
    .WhiteLink a:visited {color:#FFFFFF; font-size: 13px;}
    .WhiteLink a:hover {color:#FFFFFF; font-size: 13px;}
    .OrangeLink a:link {color:#FD9D00; font-size: 12px;}
    .OrangeLink a:visited {color:#FD9D00; font-size: 12px;}
    .OrangeLink a:hover {color:#FD9D00; font-size: 12px;}
    .RedLink a:link {color:red; font-size: 15px;}
    .RedLink a:visited {color:red; font-size: 15px;}
    .RedLink a:hover {color:red; font-size: 15px;}
    .LinkNormal { text-align: left;}
    .Content 	{
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 10px;
			font-style: normal;
			line-height: normal;
			font-variant: normal;
			padding-left: 2px;
			}
			
	.ChartContent 	{
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 13px;
			font-style: normal;
			line-height: normal;
			font-variant: normal;
			padding-left: 2px;
			}		
			
    .yui .ajax__tab_header 
{
    font-family:arial,helvetica,clean,sans-serif;
    font-size:small;
    border-bottom:solid 5px #FF8B00;
}
.yui .ajax__tab_header .ajax__tab_outer 
{
    background: #d8d8d8 repeat-x;
    margin:0px 0.16em 0px 0px;
    padding:.5px 0px .5px 0px;
    vertical-align:bottom;
    border:solid 1px #a3a3a3;
    border-bottom-width:0px;
}
.yui .ajax__tab_header .ajax__tab_tab
{    
    color:#000;    
    padding:0.35em 0.35em;    
    margin-right:0.01em;
}
.yui .ajax__tab_hover .ajax__tab_outer 
{
    background: #bfdaff repeat-x left -1300px;
}
.yui .ajax__tab_active .ajax__tab_tab 
{
    color:#000;    
    font-weight: bold;
}
.yui .ajax__tab_active .ajax__tab_outer
{
    background: #FF8B00 repeat-x left -1400px;
}
.yui .ajax__tab_body 
{
    font-family:verdana,tahoma,helvetica;
    font-size:10pt;
    padding:0.25em 0.5em;
    background-color:#000000;    
    border:solid 1px #808080;
    border-top-width:0px;
}



.news .ajax__tab_header 
{
    font-family:arial,helvetica,clean,sans-serif;
    font-size:small;
    border-bottom:solid 5px #FF8B00;
}
.news .ajax__tab_header .ajax__tab_outer 
{
    background: #d8d8d8 repeat-x;
    margin:0px 0.16em 0px 0px;
    padding:.5px 0px .5px 0px;
    vertical-align:bottom;
    border:solid 1px #a3a3a3;
    border-bottom-width:0px;
}
.news .ajax__tab_header .ajax__tab_tab
{    
    color:#000;    
    padding:0.35em 0.35em;    
    margin-right:0.01em;
}
.news .ajax__tab_hover .ajax__tab_outer 
{
    background: #bfdaff repeat-x left -1300px;
}
.news .ajax__tab_active .ajax__tab_tab 
{
    color:#000;    
    font-weight: bold;
}
.news .ajax__tab_active .ajax__tab_outer
{
    background: #FF8B00 repeat-x left -1400px;
}
.news .ajax__tab_body 
{
    font-family:verdana,tahoma,helvetica;
    font-size:10pt;
    padding:0.25em 0.5em;
    background-color:#DBDCD7;    
    border:solid 1px #808080;
    border-top-width:0px;
}						
    </style>
    
    
    
    
    
</asp:Content>

    
    <asp:content contentplaceholderid="cphMain" runat="server" id="cphMain">
    
    <asp:ScriptManager runat="server" />
    
    <div align="left">
    <%--<iframe src="http://www.bd1.com.br/tpe/flash.html" scrolling="no" width="780px" height="155px" style="border:0px;"></iframe>--%>

        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td>
                    <div id="switcher-wrap">
			            <a id="one" class="image-link" href="#" onClick="return false"><span>Image One</span></a>
			            <a id="two" class="image-link" href="#" onClick="return false"><span>Image Two</span></a>
			            <a id="three" class="image-link" href="#" onClick="return false"><span>Image Three</span></a>
			            <a id="four" class="image-link" href="#" onClick="return false"><span>Image Four</span></a>
			            <a id="five" class="image-link" href="https://www.theplasticsexchange.com/Public/Registration.aspx"><span>Image Five</span></a>
		            </div>	
                    <%--<object codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" height="155" width="780" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">
                        <param name="_cx" value="20638" />
                        <param name="_cy" value="4101" />
                        <param name="FlashVars" value="" />
                        <param name="Movie" value="swf/who_we_are.swf" />
                        <param name="Src" value="swf/who_we_are.swf" />
                        <param name="WMode" value="Transparent" />
                        <param name="Play" value="-1" />
                        <param name="Loop" value="-1" />
                        <param name="Quality" value="High" />
                        <param name="SAlign" value="" />
                        <param name="Menu" value="-1" />
                        <param name="Base" value="" />
                        <param name="AllowScriptAccess" value="always" />
                        <param name="Scale" value="ShowAll" />
                        <param name="DeviceFont" value="0" />
                        <param name="EmbedMovie" value="0" />
                        <param name="BGColor" value="" />
                        <param name="SWRemote" value="" />
                        <param name="MovieData" value="" />
                        <param name="SeamlessTabbing" value="1" />
                        <param name="Profile" value="0" />
                        <param name="ProfileAddress" value="" />
                        <param name="ProfilePort" value="0" />
                        <embed wmode="transparent" src="swf/who_we_are.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="780" height="155" />                           
                    </object>

                    <script src="/ieupdate.js" type="text/javascript"></script>--%>
    </td> </tr> </table>
    
     </div>
   

<TABLE cellSpacing=0 cellPadding=0 width=780 align=center bgColor=#333333 border=0>
    <TBODY>
        <!--<TR>
            <TD width="100%" colSpan=3 height=18>
                <APPLET height=18 width=780 code="menuscroll2.class" viewastext>
                    <PARAM NAME="yposition" VALUE="14">
                    <PARAM NAME="arrow_color" VALUE="FFFFFF">
                    <PARAM NAME="highlight_rect_color" VALUE="CC6600">
                    <PARAM NAME="regcode" VALUE="8mnxzaqre58g">
                    <PARAM NAME="highlight_arrow_color" VALUE="CC6600">
                    <PARAM NAME="font_size" VALUE="10">
                    <PARAM NAME="info" VALUE="Applet by Gokhan Dagli,www.appletcollection.com">
                    <PARAM NAME="applet_height" VALUE="14">
                    <PARAM NAME="text_color" VALUE="000000">
                    <PARAM NAME="font_type" VALUE="Arial,Verdana">
                    <PARAM NAME="space" VALUE="10">
                    <PARAM NAME="bgcolor" VALUE="000000">
                    <PARAM NAME="arrow_bgcolor" VALUE="000000">
                    <PARAM NAME="scroll_delay" VALUE="10">
                    <PARAM NAME="highlight_text_color" VALUE="FFFFFF">
                    <PARAM NAME="mouse_over" VALUE="stop">
                    <PARAM NAME="rect_color" VALUE="CCCCCC">
                    <PARAM NAME="border_color" VALUE="000000">
                    <PARAM NAME="applet_width" VALUE="100%">
                    <PARAM NAME="xspace" VALUE="1">
                    <PARAM NAME="font_style" VALUE="0">
                    <PARAM NAME="scroll_jump" VALUE="1">
                    <asp:placeholder id="phSpotParameters" runat="server" />
                </APPLET>                 
            </TD>
        </TR>-->
                
        <TR><TD class="LinkNormal OrangeLink" vAlign=middle align=left width=184 background="/pics/home_bar_titles_r1_c1.jpg" height=10>
            <STRONG class="Header Color1">Resin Markets</STRONG> </TD>
            <TD background="/pics/home_bar_titles_r1_c1.jpg"></TD>
            <TD style="HEIGHT: 14px" align=left width="50%" background="/pics/home_bar_titles_r1_c1.jpg">
            <DIV id="lblChartName" class="Header Bold Color3"></DIV></TD>
        </TR>
    </TBODY>
</TABLE>
 

          
            <TABLE cellSpacing=0 cellPadding=0 width=780 align=center bgColor=#000000 border=0><TBODY><TR><TD width=390>
                
                          <ajaxToolkit:TabContainer runat="server" ID="tcMarkets" OnClientActiveTabChanged="clientActiveTabChanged" ActiveTabIndex="0" Width="390px" Height="230px" CssClass="yui">
                            <ajaxToolkit:TabPanel runat="server" ID="tbSpot" HeaderText="Spot" Width="390px" Height="230px" >
                                <ContentTemplate>
                                    <table cellSpacing="0" cellPadding="0" width="100%" border="0">
                                  
					                    <tr>
					                        <td>					                
                                                <asp:datagrid id="dgSpot" runat="server" width="100%" bordercolor="Black" enableviewstate="False"
                                                 horizontalalign="Center" autogeneratecolumns="False" cellpadding="2" font-size="Small" 
                                                 gridlines="Horizontal" borderstyle="None" backcolor="black" 
                                                 borderwidth="0px" forecolor="White" cssclass="LinkNormal WhiteLink">
                                                    <AlternatingItemStyle BorderStyle="Solid" BackColor="Black"></AlternatingItemStyle>
                                                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                                                    <Columns>
                                                        <asp:HyperLinkColumn DataNavigateUrlField="offr_grade" DataNavigateUrlFormatString="/spot/Spot_Floor.aspx?Filter={0}" DataTextField="GRADE" HeaderText="Resin">
                                                            <HeaderStyle CssClass="ChartContent Bold Color4"></HeaderStyle>
                                                        </asp:HyperLinkColumn>
                                                        <asp:BoundColumn DataField="varsize" HeaderText="Total lbs" DataFormatString="{0:#,##}">
                                                            <HeaderStyle CssClass="ChartContent Bold Color4"></HeaderStyle>
                                                            <ItemStyle Wrap="False" CssClass="ChartContent Bold Color4"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Min" HeaderText="Low" DataFormatString="{0:0.##0}">
                                                            <HeaderStyle CssClass="ChartContent Bold Color4"></HeaderStyle>
                                                            <ItemStyle Wrap="False" CssClass="ChartContent Bold Color4"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Max" HeaderText="High" DataFormatString="{0:0.##0}">
                                                            <HeaderStyle CssClass="ChartContent Bold Color4"></HeaderStyle>
                                                            <ItemStyle Wrap="False" CssClass="ChartContent Bold Color4"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:TemplateColumn Visible="False" HeaderText="Price Range">
                                                            <HeaderStyle CssClass="ChartContent Bold Color4"></HeaderStyle>
                                                            <ItemStyle Wrap="False" CssClass="ChartContent Bold Color4"></ItemStyle>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                    <ItemStyle BorderColor="Black" BorderWidth="0px" />
                                                </asp:datagrid>                
                                            </td>
					                    </tr>
					                    <tr>
                                            <td>
                                                <span style="HEIGHT: 12px" class="LinkNormal RedLink">
                                                    <asp:hyperlink id="hplTotalLbs" runat="server" onprerender="hplTotalLbs_PreRender" />
                                                </span>
                                            </td>
                                        </tr>
					                </table>
                                </ContentTemplate>                                
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="Contract" Width="390px" Height="230px">
                                <ContentTemplate>
                                    <table cellSpacing="0" cellPadding="3" width="100%" border="0">
					                    <tr>
					                        <td>
					                            <asp:datagrid id="dgContract" runat="server" width="100%" bordercolor="Black" 
					                            enableviewstate="False" horizontalalign="Center" autogeneratecolumns="False" cellpadding="3" font-size="Small" 
					                            gridlines="Horizontal" borderstyle="None" backcolor="black" borderwidth="0px" forecolor="White" 
					                            cssclass="LinkNormal WhiteLink">
                                                    <AlternatingItemStyle BorderStyle="Solid" BackColor="Black"></AlternatingItemStyle>
                                                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                                                    <Columns>
                                                        <asp:HyperLinkColumn DataNavigateUrlField="cont_id" DataNavigateUrlFormatString="/spot/Spot_Floor.aspx?Filter={0}" DataTextField="cont_labl" HeaderText="Resin">
                                                            <HeaderStyle CssClass="ChartContent Bold Color4"></HeaderStyle>
                                                        </asp:HyperLinkColumn>                                                                                                                                                                 
                                                        <asp:BoundColumn DataField="bid" HeaderText="Bid" DataFormatString="{0:0.##0}">
                                                            <HeaderStyle CssClass="ChartContent Bold Color4"></HeaderStyle>
                                                            <ItemStyle Wrap="False" CssClass="ChartContent Bold Color4"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="OFFR" HeaderText="Offer" DataFormatString="{0:0.##0}">
                                                            <HeaderStyle CssClass="ChartContent Bold Color4"></HeaderStyle>
                                                            <ItemStyle Wrap="False" CssClass="ChartContent Bold Color4"></ItemStyle>
                                                        </asp:BoundColumn>
                                                    </Columns>
                                                    <ItemStyle BorderColor="Black" BorderWidth="0px" />
                                                </asp:datagrid>  
					                        
					                        					                                                                
                                            </td>
					                    </tr>
					                </table>
                                    
                                </ContentTemplate>                                
                            </ajaxToolkit:TabPanel>                            
                            
                       </ajaxToolkit:TabContainer>                        
                </TD>
                
                <TD><asp:literal id="LiteralImg" runat="server"></asp:literal> </TD></TR></TBODY></TABLE>
    
    <table cellspacing="0" cellpadding="0" width="780" align="center" bgcolor="#666666" border="0">       
        <tr>
            <td width="385">
                <table cellspacing="0" cellpadding="0" width="390" border="0">
                    <tr>
                        <td width="6" background="/pics/home_bar_titles_r1_c1.jpg">
                            <img height="17px" src="/pics/home_bar_titles_r1_c1.jpg" width="6" /></td>
                        <td class="LinkNormal OrangeLink" valign="middle" width="384" background="/pics/home_bar_titles_r1_c1.jpg" height=10px>
                            <a href="/Public/Public_News.aspx">Industry News</a></td>
                    </tr>
                    <tr>
                        <td colspan="3" height=1px>
                            <img height="1" src="/pics/home_bar_titles_r2_c1.jpg" width="390" /></td>
                    </tr>
                    
               
                </table>
            </td>
            <td valign="top" width="395" bgcolor="#666666">
                <table cellspacing="0" cellpadding="0" width="390" border="0">
                    <tr>
                        <td align="left" width="384" background="/pics/home_bar_titles_r1_c1.jpg" colspan="4" style="height: 10px">                            
                            <table cellspacing="0" cellpadding="0" width="390" border="0">
                                <tr>
                                    <td align="left" class="LinkNormal OrangeLink">
                                        <a href="/Research/WeeklyReview.aspx" >Market Updates</a>
                                    </td>    
                                    <td align="right">
                                        <asp:TextBox runat="server" ID="txtEmailSubs" width="75%" Text="Type Your Email to Join Our List" onfocus="this.value=''" >
                                        </asp:TextBox>
                                        <asp:Button runat="server" ID="btnJoin" Text="Join!" OnClick="btnJoin_Click" />
                                    </td>
                                </tr>
                            </table>
                            
                            
                            <%--<span class="LinkNormal OrangeLink">
                                <a href="/Public/MarketUpdate/MarketUpdate.pdf" target="_blank">
                                    Market Updates</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                
                            </span>
                            
                            
                            <span id="lblDateMR" class="Content Bold Color4" runat="server"></span>--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height: 1px">
                            <img height="1" src="/pics/home_bar_titles_r2_c1.jpg" width="390"></td>
                    </tr>   
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="#dbdcd7">
                <asp:placeholder id="phNews" runat="server"></asp:placeholder>
            </td>
            <td valign="top" width="390" bgcolor="#dbdcd7">
                <table cellspacing="0" cellpadding="0" width="390" border="0">
                    <tr>
                        <td width="1" background="/pics/home_research_r1_c2.jpg">
                            <img height="94" src="/pics/home_research_r1_c2.jpg" width="1"></td>
                        <td valign="middle" align="center" style="width: 25px">
                            <img height="94" src="/images2/research_setas_r1_c1.jpg" width="24"></td>
                        <td width="1" bgcolor="#333333">
                            <img height="94" src="/pics/home_research_r1_c2.jpg" width="1"></td>
                        <td valign="middle" align="left" width="337">
                            <table height="90" cellspacing="1" cellpadding="5" width="360" border="0">
                                <tr>
                                    <td valign="top">
                                        <span class="LinkNormal Content">
                                        <asp:HyperLink ID="hlPe" runat="server"  >Polyethylene</asp:HyperLink>
                                        </span> 
                                            <div id="lblPE" class="Content Color2" runat="server">
                                                </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="/pics/home_research_r1_c2.jpg">
                            <img height="1" src="/pics/home_research_r2_c2.jpg" width="1"></td>
                        <td valign="middle" align="center" bgcolor="#333333" style="width: 25px">
                            <img height="1" src="/pics/home_research_r2_c1.jpg" width="24"></td>
                        <td bgcolor="#333333">
                            <img height="1" src="/pics/home_research_r2_c2.jpg" width="1"></td>
                        <td valign="top" bgcolor="#333333">
                            <img height="1" src="/pics/home_research_r2_c1.jpg" width="24"></td>
                    </tr>
                    <tr>
                        <td background="/pics/home_research_r1_c2.jpg">
                            <img height="94" src="/pics/home_research_r1_c2.jpg" width="1"></td>
                        <td valign="middle" align="center" style="width: 25px">
                            <img height="94" src="/images2/research_setas_r1_c1.jpg" width="24"></td>
                        <td bgcolor="#333333">
                            <img height="94" src="/pics/home_research_r1_c2.jpg" width="1"></td>
                        <td valign="middle" align="left">
                            <table height="90" cellspacing="1" cellpadding="5" width="360" border="0">
                                <tr>
                                    <td valign="top" align="left">
                                        <span class="LinkNormal Content">
                                        <asp:HyperLink ID="hlPp" runat="server">Polypropylene</asp:HyperLink>
                                        </span>
                                            <div id="lblPP" class="Content Color2" runat="server">
                                                </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="/pics/home_research_r1_c2.jpg" style="height: 1px">
                            <img height="1" src="/pics/home_research_r2_c2.jpg" width="1" /></td>
                        <td valign="middle" align="center" bgcolor="#333333" style="height: 1px; width: 25px;">
                            <img height="1" src="/pics/home_research_r2_c1.jpg" width="24" /></td>
                        <td bgcolor="#333333" style="height: 1px">
                            <img height="1" src="/pics/home_research_r2_c2.jpg" width="1" /></td>
                        <td valign="top" bgcolor="#333333" style="height: 1px">
                            <img height="1" src="/pics/home_research_r2_c1.jpg" width="24" /></td>
                    </tr>
                    <%--<tr>
                        <td background="/pics/home_research_r1_c2.jpg">
                            <img height="94" src="/pics/home_research_r1_c2.jpg" width="1" /></td>
                        <td valign="middle" align="center" style="width: 25px">
                            <img height="94" src="/images2/research_setas_r1_c1.jpg" width="24" /></td>
                        <td bgcolor="#333333">
                            <img height="94" src="/pics/home_research_r1_c2.jpg" width="1" /></td>
                        <td valign="middle" align="left">
                            <table height="90" cellspacing="1" cellpadding="5" width="360" border="0">
                                <tr>
                                    <td valign="top">
                                        <span class="LinkNormal Content"><a href="/Public/about_mu.aspx" target="_blank" id="lnk3"><span class="blackbold">Polystyrene:</span></a></span> 
                                            <div id="lblPS" class="Content Color2" runat="server">
                                                </div>
                                        
                                    </td>
                    
                            </table>
                                 
                          
                                
                        </td>
                    </tr>--%>
                    <td background="/pics/home_research_r2_c1.jpg" style="height: 1px" colspan="4">
                            <img height="1" src="/pics/home_research_r2_c2.jpg" width="1" /></td>
                </table>
            </td>
        </tr>
    </table>


    

    <div id="flag" style="visibility: hidden; display: none">
    <asp:Label runat="server" Visible="true" ID="lblContractChartID" />
    <asp:Label runat="server" Visible="true" ID="lblSpotChartID" />
    
       </div>
    <div id="flag2" style="visibility: hidden; display: none">
        1</div>
        


</asp:Content>
