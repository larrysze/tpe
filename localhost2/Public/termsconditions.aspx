<%@ Page Language="C#" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" AutoEventWireup="true" CodeBehind="termsconditions.aspx.cs" Inherits="localhost.Public.termsconditions" Title="Terms and Conditions" %>


<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphCssLink">
	<style type="text/css">
	    br
	    {
		    margin-bottom: 1.2em;
	    }
	    .titletables {
	        font-family: Verdana, Arial, Helvetica, sans-serif;
	        font-size: 12px;
	        font-style: normal;
	        line-height: normal;
	        font-weight: bold;
	        font-variant: normal;
	        color: #FF9900;
	        padding-left: 2px;
        }
        .NewsContent {
            padding-left: 5px;
            padding-right: 5px;
            text-align: left;
        }
        
        .Header {
            color: #FD9D00
        }
        .Header a:link {
	        text-decoration: none;
	        color: #FD9D00;	
        }
        .Header a:visited {
	        text-decoration: none;
	        color: #FD9D00;
        }
        .Header a:hover {
	        text-decoration: underline;
	        color: #FD9D00;
        }      

    </style>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphHeading">
       <table width='100%' border='0' cellspacing="0" cellpadding="0">
    <tr>
        <td align='left'><span class='Header Bold Color1'>Conditions of Sales Agreement</span></td>        
    </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphMain">
   
   
	<div style="margin:6px;">
	<p style="text-align: justify; font-size: 10pt" class="Content">
	</p>
	    <center>
            <span style="font-size: 10pt">Conditions of Sales Agreement </span>
	    </center>
	    <br />	       
	    <span style="text-align: justify; font-size: 10pt">    
	    All orders for goods placed by Purchaser with The Plastics Exchange, LLC (TPE), whether written or verbal or whether made simultaneously with the submission of a credit application or any time thereafter, shall be subject to the terms and conditions set forth below.  ALL ORDERS ARE SUBJECT TO CREDIT APPROVAL.  These terms and conditions shall take precedence over any differing terms in any other documentation of Purchaser including, but not limited to, any other clauses or terms which appear on any correspondence, purchase orders, or order slips of Purchaser. Once a Purchase Order has received a Sales Confirmation from The Plastics Exchange, the order is only cancelable with the expressed written consent of The Plastics Exchange.
	    <br />
        The Purchaser is required to immediately test material upon delivery.  Rejection of nonconforming materials received by Purchaser shall be made by sending written notification of such rejection to TPE within ten (10) days of the Purchaser�s receipt of goods.  Such notification shall state the basis of the nonconformity of the goods and a detailed description of that portion of the shipment being rejected.  Purchaser�s failure to give notice in writing to TPE within ten (10) days of Purchaser�s receipt of goods shall constitute an absolute and unconditional acceptance of such materials and a waiver by Purchaser of all claims with respect thereto.	    
	    <br />
	    Upon receipt of notification of rejection, TPE shall have a reasonable period of time under the circumstances to undertake an inspection of any rejected goods at the point of delivery.  Goods determined to be nonconforming by TPE will be replaced or credit will be issued to Purchaser, at TPE�s option.  At no time will TPE�s liability exceed the amount invoiced on the subject purchase order for the nonconforming goods.  No credit for incidental or consequential damages will be issued by TPE.
	    <br />
	    TPE shall not be liable for delay in performance or nonperformance caused by circumstances beyond its control, including, but not limited to: Acts of God, fire, explosion, flood, natural catastrophe, war, civil disturbance, governmental regulation, direction or request, accident, strike, labor trouble, shortage of or inability to obtain material, equipment, transportation or Force Majeure.
        <br />	    	    	    	    
	    Purchaser agrees to indemnify and hold harmless TPE against any and all claims and inability arising out of any use of the materials or of products made from the material purchased from TPE.
        <br />
	   	    	    	    	    
	    <b>
	    DISCLAIMER OR WARRANTIES: PURCHASER AND TPE AGREE THAT TPE DOES NOT MAKE OR INTEND AND TPE DOES NOT AUTHORIZE ANY AGENT OR REPRESENTATIVE TO MAKE ANY REPRESENTATIONS OR WARRANTIES, OR IMPLIED, OR MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE (WHETHER OR NOT THAT PURPOSE IS KNOWN TO TPE), OR OTHERWISE WITH RESPECT TO ITS PRODUCTS.  ANY SUGGESTIONS BY TPE CONCERNING USES OR APPLICATIONS OF ITS PRODUCTS REFLECT TPE�S OPINION ONLY AND TPE MAKES NO WARRANTY OR RESULTS TO BE OBTAINED.  TPE SHALL NOT BE LIABLE FOR, AND PURCHASER ASSUMES RESPONSIBILITY FOR, ALL PERSONAL AND BODILY INJURY AND PROPERTY DAMAGES RESULTING FROM THE HANDLING, POSSESSION, USE OR RESALE OF TPE�S PRODUCTS.  TPE SHALL NOT BE LIABLE FOR CONSEQUENTIAL DAMAGES, PUNATIVE DAMAGES, OR LOSS OF PROFITS.  IN ANY EVENT, TPE�S LIABILITY SHALL NOT EXCEED THE VALUE OF THE ORDER AT ISSUE.
	    </b>
        
	    <br />	    	    	    	    
	    This agreement shall be governed, construed and enforced in accordance with the substantive laws of the State of Illinois without regard to the principle of conflict of laws.	    	    
        <br />
	      	    	    	    
	    Failure to pay amount due on account will accrue interest at the rate of 0.75% per 15 days until paid, or the maximum interest rate allowable by Law, and Purchaser agrees to pay any and all costs associated with the recovery of such amounts, due on account including principal, interest, and attorney costs.  In the case of rail car purchases, such rail cars must be released within 45 days of placement at Purchaser�s location.  Failure to do so will make Purchaser liable for all demurrage charges, at a minimum rate of $75 per day, arising from such lateness.  Payments are due per the terms printed on the face page of this invoice.
        
	    <br />	    	    	 	    
        During the normal course of business, should The Plastics Exchange, LLC have an amount outstanding due to the Customer, at The Plastics Exchange, LLC discretion, these amounts may be Set-Off against the amounts due from the Customer.	    	    
	    
	    <br />	    	    	 	    
	    The invalidity of any one or more of the clauses or words contained in this Agreement shall not affect the enforceability of the remaining portions of this Agreement, all of which are inserted conditionally on being valid in law, and in the event any part or portions of this Agreement shall be invalid or illegal or unenforceable in whole or in part, neither the validity of the remaining part of such term nor the validity of any other term of this Agreement shall in any way be affected thereby.
	    
	    <br />	    	    	 	    
	    The waiver by any provision of this Agreement shall not operate as, or be construed to be, a waiver of any subsequent breach hereof. The terms of this Conditions of Sales Agreement, combined with any invoice or other similar document issued by TPE with respect to orders for goods placed by Purchaser, are intended by the parties as the complete expression of their agreement, with respect to each order for goods placed by Purchaser with TPE.  Any and all changes, amendments, or modifications of the Agreement shall not be effective unless made in writing and signed by the parties hereto.
	    
	    <br />	    	    	 	    
	    In the event of any litigation of any action, suit, counterclaim or proceeding (i) to enforce or defend any rights under or in connection with this Conditions of  Sale Agreement or any amendment, document or agreement delivered or which may in the future be delivered in connection herewith, or (ii) arising from any dispute or controversy in connection with or related to this Condition of Sale Agreement or any such amendment, document or agreement, the prevailing party in such action, suit, counterclaim or proceeding shall be entitled to recover from the other party in such action, such sum as the court shall fix as reasonable attorneys fees incurred by such prevailing party. Buyer irrevocably waives any right to trial by jury in the aforementioned paragraph for any action, suit counterclaim or proceeding and agrees that any such action, suit, counterclaim or proceeding shall be tried before a court in the City of Chicago, Cook County, Illinois and not before a jury.	    	    	 
	     </span>
        <p>
        </p>
	</div>
   
   
</asp:Content>


