using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mail;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for FAQ2.
	/// </summary>
	public partial class Faq : System.Web.UI.Page
	{
		private const int FaqCount= 36;
		
	    
		protected ArrayList Question = new ArrayList();
		protected ArrayList Answer = new ArrayList();


		private void fillQuestions()
		{
			int q = 0;

			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				using (SqlDataReader dr = DBLibrary.GetDataReaderFromSelect(conn,"Select id,question,answer from FAQ order by id"))
				{
					while (dr.Read())
					{
						Question.Add(dr["question"].ToString());
						Answer.Add(dr["answer"].ToString());
					}
				}
			}
			
			for(int i = 0;i < Question.Count;i++)
			{
				if (Question[i] != "")
				{					
					//					Response.Write("<h5><a href=\"javascript:domCollapse(" + i + ")\">+" + Question[i] + "</a></h5>");
					//					Response.Write("<div><p>" + Answer[i] + "</p></div>");

					HtmlTableRow tr = new HtmlTableRow();
					HtmlTableCell tc = new HtmlTableCell();
					tc.Height="18";
					tc.Width="778";
					tc.BgColor = "#999999";
					
				
					tc.InnerHtml = "<span class=\"Content LinkNormal\"><h4><a href=\"javascript:domCollapse(" + i +")\" >+"+Question[i] + "</a></h4></span>";
					tr.Cells.Add(tc);
					htmlTableFaq.Rows.Add(tr);

					HtmlTableRow tr2 = new HtmlTableRow();
					HtmlTableCell tc2 = new HtmlTableCell();
					tc2.VAlign="top";
					tc2.BgColor="#dbdcd7";
					//tc2.Class = "blackcontent";
					tc2.InnerHtml = "<div class=\"Content\">" + Answer[i]+ "<br></div>";
					tr2.Cells.Add(tc2);
					htmlTableFaq.Rows.Add(tr2);
				}
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
            //Master.Heading = "Frequently Asked Questions (FAQ)";
			btnSubmit.Attributes.Add("onMouseOver", "document.getElementById('" + btnSubmit.ClientID + "').src='/images2/submit_pressed.jpg';");
            btnSubmit.Attributes.Add("onMouseOut", "document.getElementById('" + btnSubmit.ClientID + "').src='/images2/submit_unpressed.jpg';");

			// Put user code to initialize the page here
			fillQuestions();            
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSubmit.Click += new System.Web.UI.ImageClickEventHandler(this.btnSubmit_Click);

		}
		#endregion

		private void btnSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{	
			if (txtNewQuestion.Text != String.Empty)
			{
				MailMessage mail=new MailMessage();
				mail.From = Application["strEmailOwner"].ToString(); //"ander@theplasticexchange.com";
				mail.To = Application["strEmailOwner"].ToString();   //"ander@theplasticexchange.com";
									
				mail.Subject = "Question from FAQ page";
				string header = "";
				if (txtEmail.Text != String.Empty)
					header = "From: " + txtEmail.Text;
				if (txtName.Text != String.Empty)
					header += "(" + txtName.Text + ")";
				if (header != String.Empty)
					header += "<br>";
				mail.Body =  header + txtNewQuestion.Text;
				mail.BodyFormat = System.Web.Mail.MailFormat.Html;
					
				EmailLibrary.Send(mail);
				// need to show thank you!!
                showThankYou(true);

			}
		}
        private void showThankYou(bool show)
        {
            divNewSubmission.Visible = !show;
            divThankYou.Visible = show;
            txtNewQuestion.Text = String.Empty;
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            showThankYou(false);
        }			
	}
}
