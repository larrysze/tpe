<%@ Page Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="ScrollingPageTest.aspx.cs" Inherits="localhost.Public.ScrollingPageTest" Title="Untitled Page" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="cc1" namespace="System.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCssLink" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMain" runat="server">
<asp:ScriptManager runat="server" />
    <div id="appletdiv">
        <APPLET height=18 width=780 code="menuscroll2.class" viewastext>
                <PARAM NAME="yposition" VALUE="14">
                <PARAM NAME="arrow_color" VALUE="FFFFFF">
                <PARAM NAME="highlight_rect_color" VALUE="CC6600">
                <PARAM NAME="regcode" VALUE="8mnxzaqre58g">
                <PARAM NAME="highlight_arrow_color" VALUE="CC6600">
                <PARAM NAME="font_size" VALUE="10">
                <PARAM NAME="info" VALUE="Applet by Gokhan Dagli,www.appletcollection.com">
                <PARAM NAME="applet_height" VALUE="14">
                <PARAM NAME="text_color" VALUE="000000">
                <PARAM NAME="font_type" VALUE="Arial,Verdana">
                <PARAM NAME="space" VALUE="10">
                <PARAM NAME="bgcolor" VALUE="000000">
                <PARAM NAME="arrow_bgcolor" VALUE="000000">
                <PARAM NAME="scroll_delay" VALUE="10">
                <PARAM NAME="highlight_text_color" VALUE="FFFFFF">
                <PARAM NAME="mouse_over" VALUE="stop">
                <PARAM NAME="rect_color" VALUE="CCCCCC">
                <PARAM NAME="border_color" VALUE="000000">
                <PARAM NAME="applet_width" VALUE="100%">
                <PARAM NAME="xspace" VALUE="1">
                <PARAM NAME="font_style" VALUE="0">
                <PARAM NAME="scroll_jump" VALUE="1">
                <asp:placeholder id="phSpotParameters" runat="server" />
            </APPLET>  
    </div>
</asp:Content>
