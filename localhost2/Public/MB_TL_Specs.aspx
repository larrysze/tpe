<%
   '*****************************************************************************
   '*   1. File Name       : Public\MB_RC_Specs.aspx                            *
   '*   2. Description     : Truck load Specifications                          *
   '*			                                                        *
   '*						                                *
   '*   3. Modification Log:                                                    *
   '*     Ver No.       Date          Author             Modification           *
   '*   -----------------------------------------------------------------       *
   '*      1.00       2-27-2004       Xiaoda             Comment                *
   '*                                                                           *
   '*****************************************************************************
%>

<%@ Page Language="VB" %>
<head>
    <title>Truckload Specifications</title>
    <LINK href="/styleIE.css" rel=STYLESHEET type=text/css>
</head>

<body bgcolor="#FFFFFF" topmargin=0 leftmargin=0 marginwidth=0 marginheight=0  <%IF InStr(Request.ServerVariables("HTTP_USER_AGENT"), "MSIE") THEN%> document.onmousedown=noclick;onselectstart="return false" oncontextmenu="return false"<%END IF%>>
<center>
<table border=0 cellspacing=0 cellpadding=0 bgcolor="#E8E8E8">
	<tr>
		<td CLASS=S5 colspan=5 bgcolor=#000000><img src=/images/1x1.gif width=3>Truckload Specifications
		</td>
	</tr>
	<tr>
		<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1>
		</td>
		<td width=3><img src=/images/1x1.gif width=1>
		</td>
		<td bgcolor=#E8E8E8 background=/images/misc/truckBg1.gif width=360><br><img src=/images/icons/arRight.gif border=0> Each truckload's content shall be approximately 42,000 lbs. net.<br><img src=/images/icons/arRight.gif border=0> All truckloads shall be packaged in gaylords that are standard to the industry.<br><img src=/images/icons/arRight.gif border=0> The content's weight shall be clearly marked on each gaylord.<br><img src=/images/icons/arRight.gif border=0> Within each truckload, all gaylord's weights shall be consistent.<br><img src=/images/icons/arRight.gif border=0> The gross weight of each truckload shall be included on the bill of lading.<br>
		</td>
		<td width=3><img src=/images/1x1.gif width=1>
		</td>
		<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1>
		</td>
	</tr>
	<tr>
		<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1>
		</td>
		<td colspan=3><img src=/images/1x1.gif height=6>
		</td>
		<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1>
		</td>
	</tr>
	<tr>
		<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1>
		</td>
		<td colspan=3 align=center><div align=center><input type=button class=tpebutton value='OK' onClick=javascript:window.close()></div>
		</td>
		<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1>
		</td>
	</tr>
	<tr>
		<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1>
		</td>
		<td colspan=3><img src=/images/1x1.gif height=4 >
		</td>
		<td bgcolor=#000000 width=1><img src=/images/1x1.gif width=1>
		</td>
	</tr>
	<tr>
		<td colspan=5 bgcolor=#000000><img src=/images/1x1.gif height=1>
		</td>
	</tr>
</table></center>
</body></html>
