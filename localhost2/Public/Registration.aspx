<%@ Page Language="c#" Codebehind="Registration.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.Registration" MasterPageFile="~/MasterPages/Template.Master" Title="Registration" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/MasterPages/Template.Master" %>

<asp:Content ContentPlaceHolderID="cphHeading" runat="server">
    <span class="Header Bold Color1">Registration</span>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphInstructions" runat="server">
    <table class='InstructionBullets'>
        <tr>
            <td width="35">
                <img src='../pics/bullet.gif' /></td>
            <td>
                Please complete the following form to gain immediate access to our live spot offers and receive our Monthly Market Update by email.</td>
        </tr>
        <tr>
            <td width="35">
                <img src='../pics/bullet.gif' /></td>
            <td>
                You may also elect to receive email offers, customized for those resins you select.</td>
        </tr>
        <tr>
            <td width="35">
                <img src='../pics/bullet.gif' /></td>
            <td>
                So please provide accurate information and select only those resins that you wish to monitor. You may change your preferences in the future.</td>
        </tr>
        <tr>
            <td width="35">
                <img src='../pics/bullet.gif' /></td>
            <td>
                You will need to apply for credit before confirming any purchases.</td>
        </tr>
    </table>
</asp:Content>
<asp:Content runat=server ContentPlaceHolderID=cphJavaScript>
<script language=javascript type="text/javascript">
        
        function changeQuality(typ)
        {
           
            if((typ == 1) || (typ == 2))
            {
                document.getElementById('<%= chkQuality3.ClientID %>').checked = false;
            }
            else
            {
                document.getElementById('<%= chkQuality1.ClientID %>').checked = false;
                document.getElementById('<%= chkQuality2.ClientID %>').checked = false;
            }
        }

		function btnBack_Click()
		{
		    document.getElementById("divForm").style.display = "none";
		    document.getElementById("divForm").style.visibility = "hidden";

    	    document.getElementById("divMarket").style.display = "block";
    	    document.getElementById("divMarket").style.visibility = "visible";

		}

		function lnkDomestic_Click()
		{		    
		    document.getElementById("<%=Market.ClientID%>").value = 'D';
		    
            document.getElementById("divForm").style.display = "block";
		    document.getElementById("divForm").style.visibility = "visible";

		    document.getElementById("divInterested").style.display = "block";
		    document.getElementById("divInterested").style.visibility = "visible";

    	    document.getElementById("divMarket").style.display = "none";
    	    document.getElementById("divMarket").style.visibility = "hidden";

		    document.getElementById("divJobFunction").style.display = "block";
		    document.getElementById("divJobFunction").style.visibility = "visible";

		    document.getElementById("divZipCode").style.display = "block";
		    document.getElementById("divZipCode").style.visibility = "visible";
            
		    document.getElementById("divPorts").style.display = "none";
		    document.getElementById("divPorts").style.visibility = "hidden";
		}

		function lnkInternational_Click()
		{
            document.getElementById("<%=Market.ClientID%>").value = 'I';
            
		    document.getElementById("divForm").style.display = "block";
		    document.getElementById("divForm").style.visibility = "visible";

		    document.getElementById("divInterested").style.display = "none";
		    document.getElementById("divInterested").style.visibility = "hidden";

    	    document.getElementById("divMarket").style.display = "none";
    	    document.getElementById("divMarket").style.visibility = "hidden";

		    document.getElementById("divJobFunction").style.display = "block";
		    document.getElementById("divJobFunction").style.visibility = "visible";

		    document.getElementById("divZipCode").style.display = "none";
		    document.getElementById("divZipCode").style.visibility = "hidden";

		    document.getElementById("divPorts").style.display = "block";
		    document.getElementById("divPorts").style.visibility = "visible";
		}

		
</script>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphMain" runat="server">
    <input type="hidden" id="Market" runat="server"/>
    
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" 
        HeaderText="<span class='Content Color3'>There are problems with the following fields. Please correct the errors below:</span>" />
    
    <table cellspacing="0" cellpadding="0" align="center" border="0">
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" align="center" border="0">
                    <tr>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <table cellspacing="0" cellpadding="0" width="780" align="center" bgcolor="#EEEEEE" border="0">
                                <tr>
                                    <td height="20" valign="middle">
                                        <asp:Label runat="server" ID="lblEmailJoin" ForeColor="Red" Text="Your email has been added to our distribution list, thank you for subscribing. <br />" Visible="false" ></asp:Label>
                                        <asp:Label CssClass="Content" Font-Size="Small" runat="server" ID="lblEmailJoin2" Text="To access the rest of The Plastics Exchange website, please fill out the following registration form. <br />" Visible="false" ></asp:Label>
                                        <span class="Content" style="line-height:25px;">Please setup my account which will give me full access to spot resin offers and market intelligence.</span>
                                        <table height="20" cellspacing="1" cellpadding="0" width="780" align="center" border="0" bgcolor="#333333">
                                            <tr>
                                                <td background="/images2/orangebar_background.jpg" class="Content Bold Color2" style="text-align:left">
                                                    Contact Information
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellspacing="1" cellpadding="3" width="780" border="0" bgcolor="#333333">
                                            <tr>
                                                <td class="Content" align="right" width="134" bgcolor="#dbdcd7" height="29">
                                                    First Name:
                                                </td>
                                                <td width="256" bgcolor="#999999" align="left">
                                                    <asp:TextBox CssClass="InputForm" ID="txtFname" runat="server" Width="250" MaxLength="20"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" DESIGNTIMEDRAGDROP="459" ControlToValidate="txtFname" 
                                                    Display="none" ErrorMessage="<span class='Content color3'>First name can not be blank.</span>" />
                                                </td>
                                                <td class="Content" align="right" bgcolor="#dbdcd7" style="width: 130px">
                                                    Last Name:
                                                </td>
                                                <td width="256" bgcolor="#999999" align="left">
                                                    <asp:TextBox CssClass="InputForm" ID="txtLname" runat="server" Width="250" MaxLength="20"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" DESIGNTIMEDRAGDROP="462" ControlToValidate="txtLname" Display="none" ErrorMessage="<span class='Content color3'>Last name can not be blank.</span>"></asp:RequiredFieldValidator></td>
                                            </tr>
                                            <tr>
                                                <td class="Content" align="right" bgcolor="#dbdcd7" height="29">
                                                    Title:</td>
                                                <td bgcolor="#999999" align="left"width="256">
                                                    <asp:TextBox CssClass="InputForm" ID="txtTitle" runat="server" Width="250" MaxLength="29"></asp:TextBox></td>
                                                <td class="Content" align="right" bgcolor="#dbdcd7" style="width: 130px">
                                                    Company Name:
                                                </td>
                                                <td bgcolor="#999999" align="left">
                                                    <asp:TextBox CssClass="InputForm" ID="txtCname" runat="server" Width="250" MaxLength="40"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtCname" Display="none" ErrorMessage="<span class='Content color3'>Company name can not be blank.</span>"></asp:RequiredFieldValidator></td>
                                            </tr>
                                            <tr>
                                                <td class="Content" align="right" bgcolor="#dbdcd7" height="29">
                                                    Phone:</td>
                                                <td width="256" bgcolor="#999999" align="left">
                                                    <asp:TextBox CssClass="InputForm" ID="txtPhone" runat="server" Width="250" MaxLength="15" DESIGNTIMEDRAGDROP="478"></asp:TextBox><asp:RequiredFieldValidator ID="Requiredfieldvalidator7" runat="server" ControlToValidate="txtPhone" Display="none" ErrorMessage="<span class='Content Color3'>Phone number can not be blank.</span>"></asp:RequiredFieldValidator></td>
                                                <td class="Content" align="right" bgcolor="#dbdcd7" style="width: 130px">
                                                    E-mail:</td>
                                                <td width="256" bgcolor="#999999" align="Left">
                                                    <asp:TextBox CssClass="InputForm" ID="txtEmail" runat="server" Width="250" MaxLength="50"></asp:TextBox><asp:CustomValidator ID="Customvalidator1" runat="server" ErrorMessage="<span class='Content Color3'>Email address already used.</span>" ControlToValidate="txtEmail" Display="none" OnServerValidate="IsDuplicate" NAME="Customvalidator1"></asp:CustomValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtEmail" Display="none" ErrorMessage="<span class='Content Color3'>Email can not be blank.</span>"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" Display="none" ErrorMessage="<span class='Content Color3'>Please enter a valid email address. This message may be caused by extra spaces after the address.</span>" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
                                            </tr>
                                            <tr>
                                                <td class="Content" align="right" bgcolor="#dbdcd7" height="29">
                                                    Password:</td>
                                                <td width="256" bgcolor="#999999" align="left">
                                                    <asp:TextBox CssClass="InputForm" ID="txtPassword" runat="server" Width="250" MaxLength="15" DESIGNTIMEDRAGDROP="481" TextMode=Password></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtPassword" Display="none" ErrorMessage="<span class='Content Color3'>Password can not be blank.</span>"></asp:RequiredFieldValidator></td>
                                                <td style="width: 130px">
                                                    </td>
                                                <td>
                                                    </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <div id="divMarket" style="visibility: visible; display: block">
                                <table height="134" width="100%">
                                        <tr>
                                            <td align="center" colspan="2" height="20">
                                                <p align="left">
                                                    <asp:Label ID="lblMarketUpdateMessage" runat="server"></asp:Label></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">
                                                &nbsp; &nbsp;
                                                <p>
                                                    <asp:Label CssClass="Content Color2 Bold" ID="lblHeadingMarket" runat="server" Font-Size="Medium">Please select which market you are primarily interested:</asp:Label>
                                                    <br>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="bottom" align="center" height="70">
                                                &nbsp;
                                                <img ID="ImageButtonDomestic" onclick="lnkDomestic_Click()" src="/Pics/icons/icon_US_Flag.jpg" Height="30px" onmouseover="this.style.cursor='pointer'" onmouseout="this.style.cursor='default'"/>&nbsp;
                                                <img ID="ImagebuttonCanada" src="/Pics/icons/icon_canada_Flag.gif" Height="30px" onclick="lnkDomestic_Click()" onmouseover="this.style.cursor='pointer'" onmouseout="this.style.cursor='default'"/>&nbsp;<br />
                                                <span class="LinkNormal"><a ID="lnkDomestic" onclick="lnkDomestic_Click()" onmouseover="this.style.cursor='pointer'" onmouseout="this.style.cursor='default'">North American Market</a></span></td>
                                            <td valign="bottom" align="center" height="70">
                                                <img ID="ImageInternational" src="/Pics/icons/Icon_Globe.gif" Height="30px" onclick="lnkInternational_Click()" onmouseover="this.style.cursor='pointer'" onmouseout="this.style.cursor='default'"/>&nbsp;
                                                <br>
                                                <span class="LinkNormal"><a onclick="lnkInternational_Click()" onmouseover="this.style.cursor='pointer'" onmouseout="this.style.cursor='default'">International Market</a></span></td>
                                        </tr>
                                </table>
                            </div>
                            <div id="divForm" style="visibility: hidden; display: none">
                                    <table cellspacing="0" cellpadding="0" width="780" align="center" bgcolor="#333333" border="0">
                                        <tr>
                                            <td valign="top">
                                                <table height="20" cellspacing="0" cellpadding="0" width="778" align="center" border="0">
                                                    <tr>
                                                        <td background="/images2/orangebar_background.jpg" class="Content Bold Color2" style="text-align:left">
                                                            Delivery Information
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div id="divPorts" style="visibility: hidden; display: none">
                                                        <table height="40" cellspacing="1" cellpadding="0" width="780" border="0">
                                                            <tr>
                                                                <td class="Content Bold Color2" style="text-align:left" align="right" width="134" bgcolor="#dbdcd7" height="29">
                                                                    Primary delivery port:
                                                                </td>
                                                                <td width="643" bgcolor="#999999">
                                                                    <table cellspacing="0" cellpadding="0" width="624" align="center" border="0">
                                                                        <tr>
                                                                            <td width="190">
                                                                                <asp:DropDownList CssClass="InputForm" ID="ddlPort" runat="server">
                                                                                </asp:DropDownList></td>
                                                                            <td class="Content" align="right" width="137">
                                                                                or enter the Zip Code:</td>
                                                                            <td width="297" align="left">
                                                                                <asp:TextBox CssClass="InputForm" ID="txtOtherPort" runat="server" Width="80px" MaxLength="8"></asp:TextBox></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                </div>
                                                <div id="divZipCode" style="visibility: hidden; display: none">
                                                        <table height="40" cellspacing="1" cellpadding="0" width="780" border="0">
                                                            <tr>
                                                                <td class="Content" align="right" width="134" bgcolor="#dbdcd7" height="29">
                                                                    Primary delivery location :
                                                                </td>
                                                                <td width="643" bgcolor="#999999">
                                                                    <table cellspacing="0" cellpadding="0" width="624" align="center" border="0">
                                                                        <tr>
                                                                            <td width="190">
                                                                                <asp:DropDownList CssClass="InputForm" ID="ddlZipCode" runat="server">
                                                                                </asp:DropDownList></td>
                                                                            <td class="Content" align="right" width="137">
                                                                                or enter the Zip Code:</td>
                                                                            <td width="297" align="left">
                                                                                <asp:TextBox CssClass="InputForm" ID="txtZipCode" runat="server" Width="80px" MaxLength="8"></asp:TextBox></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                </div>
                                                <div id="divJobFunction" style="visibility: hidden; display: none">
                                                        <table height="20" cellspacing="0" cellpadding="0" width="778" align="center" border="0">
                                                            <tr>
                                                                <td background="/images2/orangebar_background.jpg" class="Content Bold Color2" style="text-align:left">
                                                                    Corporate Identity
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table height="40" cellspacing="1" cellpadding="0" width="780" border="0">
                                                            <tr>
                                                                <td class="Content" align="right" width="134" bgcolor="#dbdcd7" height="29">
                                                                    Corporate Identity:
                                                                </td>
                                                                <td width="643" bgcolor="#999999">
                                                                    <table cellspacing="0" cellpadding="0" width="624" align="center" border="0">
                                                                        <tr>
                                                                            <td width="190">
                                                                                <asp:DropDownList CssClass="InputForm" ID="ddlJobFunction" runat="server">
                                                                                    <asp:ListItem Text="Please Select" Value="1" />
                                                                                    <asp:ListItem Text="Resin Processor" Value="2" />
                                                                                    <asp:ListItem Text="Broker/Distributor/Trader" Value="3" />
                                                                                    <asp:ListItem Text="Resin Producer" Value="4" />
                                                                                    <asp:ListItem Text="Analyst/Market Observer" Value="5" />                                                                                    
                                                                                </asp:DropDownList>
                                                                                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="ddlJobFunction" Display="None" ErrorMessage="<span class='Content Color3'>Corporate Identity needs to be selected.</span>" MinimumValue="2" MaximumValue="7"></asp:RangeValidator></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                </div>
                                                <div id="divInterested" style="visibility: hidden; display: none">
                                                        <table height="20" cellspacing="0" cellpadding="0" width="778" align="center" border="0">
                                                            <tr>
                                                                <td background="/images2/orangebar_background.jpg" class="Content Bold Color2" style="text-align:left">
                                                                    I'm primary interested in:
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table height="61" cellspacing="1" cellpadding="0" width="780" border="0">
                                                            <tr>
                                                                <td class="Content" align="right" width="134" bgcolor="#dbdcd7" height="40">
                                                                    Sizes:
                                                                </td>
                                                                <td valign="middle" width="643" bgcolor="#999999" height="40">
                                                                    <table cellspacing="0" cellpadding="0" width="624" align="center" border="0">
                                                                        <tr>
                                                                            <td width="20">
                                                                            </td>
                                                                            <td class="Content" width="173">
                                                                                <p align="left">
                                                                                    <asp:CheckBox ID="chkSize1" runat="server" Text="Rail Cars" Checked="true" /></p>
                                                                            </td>
                                                                            <td width="20">
                                                                            </td>
                                                                            <td class="Content" width="188">
                                                                                <p align=left>
                                                                                    <asp:CheckBox ID="chkSize2" runat="server" Text="Bulk Trucks" Checked="true" /></p>
                                                                                    </td>
                                                                                
                                                                            <td width="20">
                                                                            </td>
                                                                            <td class="Content" width="188">
                                                                                <p align="left">
                                                                                    <asp:CheckBox ID="chkSize3" runat="server" Text="Truckload Boxes/ Bags" Checked="true" /></p>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Content" align="right" bgcolor="#dbdcd7" height="40">
                                                                    Quality:</td>
                                                                <td bgcolor="#999999">
                                                                    <table cellspacing="0" cellpadding="0" width="624" align="center" border="0">
                                                                        <tr>
                                                                            <td width="20">
                                                                                </td>
                                                                            <td class="Content" width="173">
                                                                                <p align="left">
                                                                                    <asp:CheckBox ID="chkQuality1" runat="server" Text="Prime" Checked="true"></asp:CheckBox>
                                                                                 </p>
                                                                            </td>
                                                                            <td width="20">
                                                                            </td>
                                                                            <td class="Content" width="188">
                                                                                <p align="left">
                                                                                    <asp:CheckBox ID="chkQuality2" runat="server" Text="Offgrade" Checked="true"></asp:CheckBox></p>
                                                                            </td>
                                                                            <td width="20">
                                                                            </td>
                                                                            <td class="Content" width="188">
                                                                                <p align="left">
                                                                                    <asp:CheckBox ID="chkQuality3" runat="server" Text="We do not use resin"></asp:CheckBox></p>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <asp:CustomValidator ID="cvQuality" runat="server" Display="None" ErrorMessage="<span class='Content Color3'>Quality needs to be selected.</span>"></asp:CustomValidator>
                                                                    
                                                                    </td>
                                                            </tr>
                                                        </table>
                                                </div>
                                                <table height="20" cellspacing="0" cellpadding="0" width="778" align="center" border="0">
                                                    <tr>
                                                        <td background="/images2/orangebar_background.jpg" class="Content Bold Color2" style="text-align:left">
                                                            Please select the resins that you use. (Click to visualize the grades)
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellspacing="1" cellpadding="0" width="780" border="0">
                                                    <tr>
                                                        <td class="Content" align="center" width="140" bgcolor="#dbdcd7">
                                                            <input id="chkHDPE" onclick="HDPE_Click()" type="checkbox"><img id="Image17" height="18" src="/images2/register/HDPE.jpg" width="70" border="0" name="Image17"></td>
                                                        <td class="Content" align="center" width="140" bgcolor="#dbdcd7">
                                                            <input id="chkLDPE" onclick="LDPE_Click()" type="checkbox"><img id="Image18" height="18" src="/images2/register/LDPE.jpg" width="70" border="0" name="Image18"></td>
                                                        <td class="Content" align="center" width="150" bgcolor="#dbdcd7">
                                                            <input id="chkLLDPE" onclick="LLDPE_Click()" type="checkbox"><img id="Image19" height="18" src="/images2/register/LLDPE.jpg" width="70" border="0" name="Image19"></td>
                                                        <td class="Content" align="center" width="130" bgcolor="#dbdcd7">
                                                            <input id="chkPS" onclick="PS_Click()" type="checkbox"><img id="Image20" height="18" src="/images2/register/PS.jpg" width="70" border="0" name="Image20"></td>
                                                        <td class="Content" align="center" width="220" bgcolor="#dbdcd7">
                                                            <input id="chkPP" onclick="PP_Click()" type="checkbox"><img id="Image21" height="18" src="/images2/register/PP.jpg" width="70" border="0" name="Image21"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" bgcolor="#999999" height="190" align=left>
                                                            <label class="Content" for="CB_4">
                                                                <asp:Panel ID="pnHDPE" runat="server" Visible="true" CssClass="Content">
                                                                </asp:Panel>
                                                            </label>
                                                        </td>
                                                        <td valign="top" bgcolor="#999999" align=left>
                                                            <label class="Content" for="CB_10">
                                                                <asp:Panel ID="pnLDPE" runat="server" Visible="true" CssClass="Content">
                                                                </asp:Panel>
                                                            </label>
                                                        </td>
                                                        <td valign="top" bgcolor="#999999" align=left>
                                                            <label class="Content" for="CB_14">
                                                                <asp:Panel ID="pnLLDPE" runat="server" Visible="true" CssClass="Content">
                                                                </asp:Panel>
                                                            </label>
                                                        </td>
                                                        <td valign="top" bgcolor="#999999" align=left>
                                                            <label class="Content" for="CB_20">
                                                                <asp:Panel ID="pnPS" runat="server" Visible="true" CssClass="Content">
                                                                </asp:Panel>
                                                            </label>
                                                        </td>
                                                        <td valign="top" bgcolor="#999999" align=left>
                                                            <label class="Content" for="CB_24">
                                                                <asp:Panel ID="pnPP" runat="server" Visible="true" CssClass="Content">
                                                                </asp:Panel>
                                                            </label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table height="20" cellspacing="0" cellpadding="0" width="778" align="center" border="0">
                                                    <tr>
                                                        <td background="/images2/orangebar_background.jpg" class="Content Bold Color2" style="text-align:left">
                                                            Send customized email offers:
                                                    </tr>
                                                </table>
                                                <table cellspacing="1" cellpadding="0" width="780" align="center" border="0">
                                                    <tr>
                                                        <td align="center" width="134" bgcolor="#dbdcd7" class="Content">
                                                            Frequency:
                                                        </td>
                                                        <td align="center" width="624" bgcolor="#999999">
                                                            <asp:RadioButtonList ID="rblEmailSubscriber" runat="server" Width="80%" RepeatDirection="Horizontal" CssClass="Content">
                                                                <asp:ListItem Value="2">Daily</asp:ListItem>
                                                                <asp:ListItem Value="1" Selected="True">Weekly</asp:ListItem>
                                                                <asp:ListItem Value="0">Never</asp:ListItem>
                                                            </asp:RadioButtonList></td>
                                                    </tr>                                                    
                                                </table>
                                                
                                                <table cellspacing="1" cellpadding="0" width="780" align="center" border="0">
                                                    <tr>                                                        
                                                        <td align="center" width="624" bgcolor="#999999" class="Content">
                                                            <h3 >
                                                            <asp:CheckBox runat="server" ID="chkTerms" Checked="false" />
                                                            By checking you are agreeing to the <a href="Termsconditions.aspx" target="_blank"> Terms and Conditions of Service</a>   
                                                            <asp:CustomValidator ID="cvTerms" runat="server" Display="None" ErrorMessage="<span class='Content Color3'>Terms and Conditions needs to be selected.</span>"></asp:CustomValidator>                                                            
                                                            </h3>
                                                        </td>
                                                    </tr>                                                    
                                                </table>
                                                
                                                
                                                <table cellspacing="1" cellpadding="0" width="780" border="0">
                                                    <tr>
                                                        <td class="Content" align="center" bgcolor="#999999">
                                                            Please provide any additional information or comments so that we can better serve you:<br>
                                                            <asp:TextBox ID="txtComment" runat="server" Width="630px" MaxLength="499" DESIGNTIMEDRAGDROP="6194" TextMode="MultiLine" Rows="3" Columns="50"></asp:TextBox>
                                                            <table height="30" cellspacing="0" cellpadding="0" width="300" border="0">
                                                                <tr>
                                                                    <td align="center" width="150">
                                                                        <a onmouseover="MM_swapImage('Image15','','/images2/register/back_pressed.jpg',1)" onmouseout="MM_swapImgRestore()" href="#"></a>
                                                                        
                                                                        <img ID="ImageButton1" src="/images2/register/back.jpg" onclick="btnBack_Click()" onMouseOver="this.style.cursor='pointer';document.getElementById('ImageButton1').src ='/images2/register/back_pressed.jpg';" onMouseOut="this.style.cursor='default';document.getElementById('ImageButton1').src ='/images2/register/back.jpg';" /></td>
                                                                    <td align="center" width="150">
                                                                        <a onmouseover="MM_swapImage('Image16','','/images2/register/register_pressed.jpg',0)" onmouseout="MM_swapImgRestore()" href="#"></a>
                                                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="/images2/register/register.jpg"></asp:ImageButton>
                                                                        
                                                                        </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
