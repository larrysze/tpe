using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Xml;
using TPE.Utility;
using System.Web.Mail;

namespace localhost.Public
{
    /// <summary>
    /// Summary description for Public_Research.
    /// </summary>
    public partial class Public_Research : System.Web.UI.Page
    {
        private DateTime dtFirstArchive = DateTime.Parse("August 10, 2005");
        protected System.Web.UI.WebControls.ImageButton ibutton;
        private bool _IsPreview = false;
        public bool IsPreview
        {
            get
            {
                return _IsPreview;
            }
            set
            {
                _IsPreview = value;
            }
        }


        
        
        // old market update dates (stored in .txt files)
        private readonly string[] OldMUDates = { 
                "August 10, 2005", 
                "July 5, 2005", 
                "June 1, 2005",
                "April 18, 2005",
                "February 28, 2005",
                "January 6, 2005",
                "November 8, 2004",
                "September 9, 2004",
                "July 27, 2004",
                "June 3, 2004",
                "April 27, 2004",
                "March 10, 2004",
                "January 27, 2004", //30
                "December 19, 2003",
                "November 4, 2003",
                "September 15, 2003",
                "July 31, 2003",
                "June 17, 2003",
                "April 24, 2003",
                "March 25, 2003",
                "February 19, 2003",
                "January 23, 2003",
                "December 11, 2002",
                "October 23, 2002",//20
                "September 12, 2002",
                "August 6, 2002",
                "May 22, 2002",
                "March 27, 2002",
                "February 7, 2002",
                "November 30, 2001",
                "October 23, 2001",
                "September 12, 2001",
                "August 15, 2001",
                "July 9, 2001",
                "June 27, 2001",
                "April 24, 2001" ,
                "March 27, 2001",
                "March 8, 2001",
                "February 15, 2001",
                "January 24, 2001",
                "January 5, 2001",
                "December 18, 2000",
                "December 6, 2000"
            };
        /*****************************************************************************
        '*   1. File Name       : Public\Public_Research.aspx                         *
        '*   2. Description     : View the content of current and atchieved Market Update *
        '*			                                                                *
        '*						                                                    *
        '*   3. Modification Log:                                                    *
        '*     Ver No.       Date          Author             Modification           *
        '*   -----------------------------------------------------------------       *
        '*      1.00       2-27-2004       Xiaoda             Comment                *
        '*                                                                           *
        '*****************************************************************************/


        public void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Preview"] != null)
                IsPreview = bool.Parse(Request.QueryString["Preview"].ToString());

            btnEmail.Attributes.Add("OnMouseOver", "document.getElementById('" + btnEmail.ClientID + "').src='/images/buttons/submit_red.jpg';");
            btnEmail.Attributes.Add("OnMouseOut", "document.getElementById('" + btnEmail.ClientID + "').src='/images/buttons/submit_orange.jpg';");

            if (IsPreview)
            {
                pnlRegular.Visible = false;
                pnlPreview.Visible = true;
            }
            else
            {
                pnlRegular.Visible = true;
                pnlPreview.Visible = false;

            }

            if (!Page.IsPostBack)
            {
                initDates();
                pnlOldMarketUpdate.Visible = false;
                pnlNewMarketUpdate.Visible = true;
            }
            else  // post back:
            {
                this.lblEmail.Text = "";	// clear message					
            }

            if ((!IsPreview) && (Request["date"] != null))
            {
                try
                {
                    DateTime review_date = new DateTime(Convert.ToInt64(Request["date"]));
                    string strDate = review_date.ToString("MMMM") + " " + review_date.Day + ", " + review_date.Year;
		    ddlReportDate.ClearSelection();
                    ddlReportDate.Items.FindByText(strDate).Selected = true;
                }
                catch (Exception ee) { }
            }

            if (!IsPreview)
            {
                BindGrid(ddlReportDate.SelectedItem.ToString());
                loadHTML(ddlReportDate.SelectedItem.ToString());

                DateTime selectedDate = Convert.ToDateTime(ddlReportDate.SelectedItem.ToString());
                string dateFileName = selectedDate.Month + "_" + selectedDate.Day + "_" + selectedDate.Year;

                string strPdfFileName = Server.MapPath("/Research/Market_Update/pdf/" + dateFileName + ".pdf");
                if (System.IO.File.Exists(strPdfFileName))
                {
                    lblPrintLink.Text = "Click <a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Market_Update/pdf/" + dateFileName + ".pdf'>here</a> for a printable version";
                }
                else
                {
                    lblPrintLink.Text = "";
                }

                this.trPrint.Visible = (lblPrintLink.Text != "");
            }
            else //preview
            {
                DateTime d = DateTime.Now;
                loadHTML(d.Month + "/" + d.Day + "/" + d.Year);
            }
        }

        private void initDates()
        {
            if (!IsPreview)
            {
                string strSql = "SELECT DATENAME(MONTH,date) +  ' ' + DATENAME(DAY,date) + ', ' + DATENAME(YEAR,date) as date, id FROM MARKET_UPDATES WHERE published = 1 ORDER BY id DESC";
                TPE.Utility.DBLibrary.BindDropDownList(Application["DBConn"].ToString(), ddlReportDate, strSql, "date", "id");

                // now add old MU's still in txt format:
                ListItem li = null;
                for (int i = 0; i < OldMUDates.Length - 1; i++)
                {   // add old dates with negative values ie, -44..0
                    li = new ListItem(OldMUDates[i], (i - OldMUDates.Length).ToString());
                    ddlReportDate.Items.Add(li);
                }
            }
        }

        private void BindGrid(string strDate)
        {
            if (!IsPreview)
            {
                if (DateTime.Parse(strDate).CompareTo(dtFirstArchive) > 0)
                {
                    pnlOldMarketUpdate.Visible = false;
                    pnlNewMarketUpdate.Visible = true;

                    Hashtable htParam = new Hashtable();
                    htParam.Add("@date", strDate);
                    TPE.Utility.DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(), dgSpotFloorSummary, "spMarketUpdateSpotOffersSnapshot", htParam);

                    string seleStr = "SELECT SUM(weight) AS TotalVolume FROM MarketUpdateSpotOffersSummary WHERE datediff(day,date,@date)=0";
                    string total = DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), seleStr, htParam).ToString();
                    if (total != "")
                    {
                        lblTotalVolume.Text = "(" + String.Format("{0:#,###}", decimal.Parse(total)) + " lbs)";
                    }
                }
                else
                {
                    // show archived MU (from .txt file)
                    pnlOldMarketUpdate.Visible = true;
                    pnlNewMarketUpdate.Visible = false;
                }
            }
        }
        private string getSessionVariable(string name)
        {
            if (Session[" + name + "] != null)
                return Session[" + name + "].ToString();
            else
                return "";
        }

        private DataTable getMarketUpdateData(bool isPreview, string review_date)
        {
            DataTable dt = null;
            if (isPreview)
            {   // create datareader from session variables
                dt = new DataTable();
                dt.Columns.Add("TOTAL");
                dt.Columns.Add("date");
                dt.Columns.Add("COMMENTS");
                dt.Columns.Add("pretext_pp");
                dt.Columns.Add("pretext_pe");
                dt.Columns.Add("POLYPROPYLENE");
                dt.Columns.Add("POLYSTYRENE");
                dt.Columns.Add("POLYETHYLENE");
                dt.Columns.Add("PEMonthChartPath");
                dt.Columns.Add("PEYearChartPath");
                dt.Columns.Add("PPMonthChartPath");
                dt.Columns.Add("PPYearChartPath");
                dt.Columns.Add("PSMonthChartPath");
                dt.Columns.Add("PSYearChartPath");

                DataRow dr = dt.NewRow();
                dr["TOTAL"] = getSessionVariable("TOTAL");
                dr["date"] = getSessionVariable("date");
                dr["COMMENTS"] = getSessionVariable("COMMENTS");
                dr["pretext_pp"] = getSessionVariable("pretext_pp");
                dr["pretext_ps"] = getSessionVariable("pretext_ps");
                dr["pretext_pe"] = getSessionVariable("pretext_pe");
                dr["POLYPROPYLENE"] = getSessionVariable("POLYPROPYLENE");
                dr["POLYSTYRENE"] = getSessionVariable("POLYSTYRENE");
                dr["POLYETHYLENE"] = getSessionVariable("POLYETHYLENE");
                dr["PEMonthChartPath"] = getSessionVariable("PEMonthChartPath");
                dr["PEYearChartPath"] = getSessionVariable("PEYearChartPath");
                dr["PPMonthChartPath"] = getSessionVariable("PPMonthChartPath");
                dr["PPYearChartPath"] = getSessionVariable("PPYearChartPath");
                dr["PSMonthChartPath"] = getSessionVariable("PSMonthChartPath");
                dr["PSYearChartPath"] = getSessionVariable("PSYearChartPath");
                dt.Rows.Add(dr);
            }
            else
            {
                //
                using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
                {
                    conn.Open();
                    dt = TPE.Utility.DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), "SELECT * FROM MARKET_UPDATES WHERE published=1 " + review_date);
                }
            }
            return dt;
        }
        private void loadHTML(string review_date)
        {
            if (DateTime.Parse(review_date).CompareTo(dtFirstArchive) > 0)
            {
                // show new market update format
                pnlOldMarketUpdate.Visible = false;
                pnlNewMarketUpdate.Visible = true;

                if (review_date != "")
                {
                    DateTime review = Convert.ToDateTime(review_date);
                    review_date = " AND Year(date)=" + review.Year + " AND Month(date)=" + review.Month + " AND Day(date)=" + review.Day;
                }
                else
                {
                    review_date = " AND date = (select max(date) from weekly_reviews where published=1)";
                }

                DataTable dt = getMarketUpdateData(IsPreview,review_date);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    string total = dr["TOTAL"].ToString();
                    string issue_date = dr["date"].ToString();

                    lblSummary.Text = HelperFunction.ConvertCarriageReturnToBR(Highlight(dr["COMMENTS"].ToString()));

                    string strPreTextPP = HelperFunction.ConvertCarriageReturnToBR(dr["pretext_pp"].ToString());
                    string strPreTextPS = HelperFunction.ConvertCarriageReturnToBR(dr["pretext_ps"].ToString());
                    string strPreTextPE = HelperFunction.ConvertCarriageReturnToBR(dr["pretext_pe"].ToString());

                    string strPP = HelperFunction.ConvertCarriageReturnToBR(dr["POLYPROPYLENE"].ToString());
                    string strPS = HelperFunction.ConvertCarriageReturnToBR(dr["POLYSTYRENE"].ToString());
                    string strPE = HelperFunction.ConvertCarriageReturnToBR(dr["POLYETHYLENE"].ToString());

                    DateTime date_obj = Convert.ToDateTime(issue_date);
                    issue_date = date_obj.ToString("MMMM") + " " + date_obj.Day + ", " + date_obj.Year;

                    lblDate.Text = issue_date;

                    //charts:
                    imgPEMonthChart.ImageUrl = dr["PEMonthChartPath"].ToString();
                    imgPEYearChart.ImageUrl = dr["PEYearChartPath"].ToString();
                    imgPPMonthChart.ImageUrl = dr["PPMonthChartPath"].ToString();
                    imgPPYearChart.ImageUrl = dr["PPYearChartPath"].ToString();
                    imgPSMonthChart.ImageUrl = dr["PSMonthChartPath"].ToString();
                    imgPSYearChart.ImageUrl = dr["PSYearChartPath"].ToString();

                    // Polyethylene:
                    lblPEIntro.Text = Highlight(strPreTextPE);
                    lblPEBody.Text = Highlight(strPE);

                    // Polypropylene:
                    lblPPIntro.Text = Highlight(strPreTextPP);
                    lblPPBody.Text = Highlight(strPP);

                    // Polystyrene:
                    lblPSIntro.Text = Highlight(strPreTextPS);
                    lblPSBody.Text = Highlight(strPS);
                
                }
            }
            else
            {
                // show archived MU (from .txt file)
                pnlOldMarketUpdate.Visible = true;
                pnlNewMarketUpdate.Visible = false;
                lblOldContent.Text = loadTxt(review_date);
            }
         }

        private string loadTxt(string strDate)
        {
            string strFileName;
            StringBuilder sbContent = new StringBuilder();

            int index_value = Math.Abs(Convert.ToInt32(ddlReportDate.SelectedItem.Value));
            strFileName = Server.MapPath("/Research/Market_Update/" + index_value + ".txt");

            FileStream fsIn = File.OpenRead(strFileName);
            StreamReader Reader;
            Reader = new StreamReader(fsIn);
            while (Reader.Peek() > -1)
            {
                string s = Reader.ReadLine();
                sbContent.Append(s);
                //if (s != String.Empty)
                //sbContent.Append("<BR>");
                //sbContent.Append(Reader.ReadLine()+"<BR>");
            }
            return sbContent.ToString();
        }

        private string Highlight(string text)
        {
            if ((Request.QueryString["SearchTerm"] != null) && (Request.QueryString["SearchTerm"].ToString().Length > 2))	// don't highlight short words
            {
                text = HelperFunction.Highlight(Request.QueryString["SearchTerm"].ToString(), text);
            }
            return text;
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnEmail.Click += new ImageClickEventHandler(this.btnEmail_Click);
            cvEmail.ServerValidate += new ServerValidateEventHandler(this.cvEmail_Validate);
        }
        #endregion

        private void cvEmail_Validate(object source, ServerValidateEventArgs args)
        {
            lblEmail.Text = "";
            //Check if the email is in the DB
            Hashtable param = new Hashtable();
            string email = txtEmail.Text;

            string strSql = "select MAI_MAIL FROM mailing WHERE MAI_MAIL = @Email";
            param.Add("@Email", email);
            DataTable dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), strSql, param);

            if (dt.Rows.Count == 0)
                args.IsValid = true;
            else
            {
                args.IsValid = false;
                cvEmail.ErrorMessage = email + " is already in our database";
            }
        }

        private void btnEmail_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (this.IsValid)
            {
                lblEmail.Text = "";
                Hashtable param = new Hashtable();
                string email = txtEmail.Text;
                param.Add("@Email", email);

                string strSqlInsert = "INSERT INTO mailing (MAI_MAIL, MAI_CODE) VALUES (@Email, '0')";
                DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlInsert, param);

                lblEmail.Text = "Thank you! " + email + " has been added to our distribution list.";
                txtEmail.Text = "";

                //Mike wants to receive an email when somebody subscribed to MU
                string txtMessage = "This email: " + email + " was added to the subscription list of Market Update.";
                string txtSubject = email + " in MU list";
                MailMessage mailToMike = new MailMessage();
                mailToMike.From = "research@theplasticsexchange.com";
                mailToMike.To = "michael@theplasticexchange.com";
                mailToMike.Subject = txtSubject;
                mailToMike.Body = txtMessage;
                EmailLibrary.Send(mailToMike);


            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Write("<script language='javascript'> { window.close();}</script>");        
        }
    }
}
