
<%@ Page language="c#" Codebehind="links-new.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.Links.Links_new" MasterPageFile="~/MasterPages/Template.Master" %>
<%@ MasterType virtualPath="~/MasterPages/Template.Master"%>
     
    <asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">
	<script language="javascript">
		<!--
		if(document.all && !document.getElementById) 
		{
			document.getElementById = function(id) { 
				return document.all[id]; 
			}
		}
		//-->
	</script>
	</asp:Content>
    <asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphInstructions">
	<table class='InstructionBullets'>
	<tr>
		<td width=35><img src='/pics/bullet.gif'></td><td>Click on a category below to display a brief description of each company listed in that group</td>
	</tr>
	<tr>
		<td width=35><img src='/pics/bullet.gif'></td><td>Click on Create New Link (on the bottom of the page) to join our directory</td>
	</tr>
	</table>
	</asp:Content>
	
	<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphHeading"><div class="Header Bold Color1">Industry Links - New</div></asp:Content>

     <asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">

		<table class="Content">
			<tbody>
				<tr>
					<td align="left" colSpan="3">
						<p><asp:label id="lblDuplicate" runat="server" Font-Bold="True" ForeColor="Red" visible="false">Duplicate Website entered</asp:label><asp:label id="errorMessage" runat="server" ForeColor="Red"></asp:label></p>
					</td>
				</tr>
				<tr>
					<td height="69"></td>
					<td align ="left" colSpan="2" height="69">Category / SubCategory:<BR>
						<asp:listbox id="lstCategories" CssClass="InputForm" runat="server" Rows="10" Width="500px" SelectionMode="Multiple"></asp:listbox><BR>
						Keep the key "CTRL" pressed for multiples selection.
					</td>
				</tr>
				<tr>
					<td>&nbsp;
					</td>
					<td colSpan="2"></td>
				</tr>
				<tr>
					<td align ="left" height="27">Company:
					</td>
					<td align ="left" colSpan="2" height="27"><asp:textbox id="txtCompany" CssClass="InputForm" runat="server" Width="176px" MaxLength="50"></asp:textbox>&nbsp;URL:
						<asp:textbox id="txtWebsite" CssClass="InputForm" runat="server" Width="250px" MaxLength="150"></asp:textbox></td>
				</tr>
				<tr>
					<td align ="left" width="70">Description:
					</td>
					<td align ="left" colSpan="5"><asp:textbox id="txtDescription" CssClass="InputForm" runat="server" Width="600px" MaxLength="250" TextMode="MultiLine"
							Height="90px"></asp:textbox></td>
				</tr>
			</tbody></table>
		<table class="Content">
			<tbody>
				<tr>
					<td align ="left" width="70">Contact:
					</td>
					<td align ="left"><asp:textbox id="txtContact" CssClass="InputForm" runat="server" Width="227px" MaxLength="50"></asp:textbox></td>
					<td align ="left">Phone:
						<asp:textbox id="txtPhone" CssClass="InputForm" runat="server" MaxLength="20"></asp:textbox></td>
					<td align ="left">Fax:
						<asp:textbox id="txtFax" CssClass="InputForm" runat="server" MaxLength="20"></asp:textbox></td>
				</tr>
				<tr>
					<td align ="left">Address:
					</td>
					<td align ="left" colSpan="3"><asp:textbox id="txtAddress" CssClass="InputForm" runat="server" Width="600px" MaxLength="200"></asp:textbox></td>
				</tr>
				<tr>
					<td align ="left">City:
					</td>
					<td align ="left"><asp:textbox id="txtCity" runat="server" CssClass="InputForm" Width="227px" MaxLength="50"></asp:textbox></td>
					<td align ="left">State:
						<asp:textbox id="ddlState" runat="server" CssClass="InputForm" Width="140px" MaxLength="20" Columns="2"></asp:textbox></td>
					<td align ="left">Zip:
						<asp:textbox id="txtZip" CssClass="InputForm" runat="server" Width="80px" MaxLength="10"></asp:textbox></td>
				</tr>
				<tr>
					<td align ="left">Country:
					</td>
					<td align ="left"><asp:textbox id="txtCountry" CssClass="InputForm" runat="server" MaxLength="50"></asp:textbox></td>
				</tr>
				<tr>
					<td align ="left">Email:
					</td>
					<td align ="left"><asp:textbox id="txtEmail" CssClass="InputForm" runat="server" MaxLength="75"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" colSpan="4"><asp:ImageButton id="btnSave" Runat="server" CssClass="InputForm" Text="Save" ImageUrl="/images/buttons/save_orange.jpg"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" Runat="server" ImageUrl="/images/buttons/cancel_orange.jpg"></asp:ImageButton></td>
				</tr>
			</tbody></table>
		</asp:Content>	
		
