using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace localhost.Public.links
{
	/// <summary>
	/// Summary description for Links_home.
	/// </summary>
	public class Links_home : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblCountResults;
		protected System.Web.UI.WebControls.Label Title;
		protected System.Web.UI.WebControls.Label lblInstructionCategories;
		protected System.Web.UI.WebControls.Label lblInstructionNameURL;
		protected System.Web.UI.WebControls.Label lblCompany;
		protected System.Web.UI.WebControls.TextBox txtCompanyName;
		protected System.Web.UI.WebControls.TextBox txtURL;
		protected System.Web.UI.WebControls.Label lblURL;
		protected System.Web.UI.WebControls.ListBox lstSubCategories;
		protected System.Web.UI.WebControls.ListBox lstCategories;
		protected System.Web.UI.WebControls.Label lblCategories;
		protected System.Web.UI.WebControls.Label lblSubCategories;
		protected System.Web.UI.WebControls.Button cmdSearch;
		protected System.Web.UI.WebControls.RadioButton rdbSearchByCategory;
		protected System.Web.UI.WebControls.DataGrid dgResults;
		protected System.Web.UI.WebControls.RadioButton rdbSearchByCompany;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				//ViewState["Referrer"] = Request.UrlReferrer.AbsoluteUri.ToString();
				//ViewState["LINKS_ID"] = Request.QueryString["ID"];
				Bind();
			}
		}

		public void Bind()
		{
			SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();

			string strSQL="SELECT LINKS_ID,LINKS_COMP,LINKS_WEBSITE,LINKS_DESCRIP,LINKS_CONTACT,LINKS_PHONE,LINKS_FAX,LINKS_GROUP,LINKS_STREET,LINKS_STREET,LINKS_CITY,LINKS_STATE,LINKS_ZIP,LINKS_COUNTRY";
			strSQL += " FROM LINKS";
			//strSQL += " WHERE (LINKS_ID = '"+ViewState["LINKS_ID"].ToString()+"')";
			strSQL += " ORDER BY LINKS_COMP";

			SqlDataAdapter dadContent;
			DataSet dstContent;
			dadContent = new SqlDataAdapter(strSQL ,conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			
			//dlGroups.DataSource = dstContent;
			//dlGroups.DataBind();
			//dlResults.DataSource = dstContent;
			//dlResults.DataBind();
			conn.Close();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.rdbSearchByCategory.CheckedChanged += new System.EventHandler(this.rdbSearchByCategory_CheckedChanged);
			this.rdbSearchByCompany.CheckedChanged += new System.EventHandler(this.rdbSearchByCompany_CheckedChanged);
			this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		private void ScreenMode(bool searchByCategories)
		{
			lblCompany.Visible = !searchByCategories;
			lblURL.Visible = !searchByCategories;
			txtCompanyName.Visible = !searchByCategories;
			txtURL.Visible = !searchByCategories;
			lblInstructionNameURL.Visible = !searchByCategories;
			lblCategories.Visible = searchByCategories;
			lblSubCategories.Visible = searchByCategories;
			lstCategories.Visible = searchByCategories;
			lstSubCategories.Visible = searchByCategories;
			lblInstructionCategories.Visible = searchByCategories;
			dgResults.Visible = false;
		}

		private void rdbSearchByCompany_CheckedChanged(object sender, System.EventArgs e)
		{
			if (rdbSearchByCompany.Checked) ScreenMode(false);
		}

		private void rdbSearchByCategory_CheckedChanged(object sender, System.EventArgs e)
		{
			if (rdbSearchByCategory.Checked) ScreenMode(true);
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			dgResults.Visible = true;
			
		}
	}
}