<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ Page language="c#" Codebehind="Links-home.aspx.cs" AutoEventWireup="false" Inherits="localhost.Public.links.Links_home" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<body>
		<form id="Form1" runat="server">
			<TPE:TEMPLATE id="Template1" Runat="Server" PageTitle="Industry Links"></TPE:TEMPLATE>
			<table width="90%">
				<tbody>
					<tr>
						<td align="center" colSpan="2">
							<p><asp:label id="Title" runat="server" Font-Bold="True" ForeColor="Red">Industry Links</asp:label></p>
						</td>
					</tr>
					<TR>
						<TD colSpan="2" height="20"></TD>
					</TR>
					<tr>
						<td colSpan="2" height="20">
							<P align="center"><asp:radiobutton id="rdbSearchByCategory" runat="server" Text="Search by Category" Checked="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:radiobutton id="rdbSearchByCompany" runat="server" Text="Search by Company Name / URL"></asp:radiobutton></P>
						</td>
					</tr>
					<TR>
						<TD height="20"><asp:label id="lblCompany" runat="server" Visible="False">Company Name:</asp:label><BR>
							<asp:label id="lblURL" runat="server" Visible="False">Web Site(URL):</asp:label><BR>
							<asp:label id="lblCategories" runat="server">Categories:</asp:label><BR>
							<asp:listbox id="lstCategories" runat="server" Width="200px"></asp:listbox></TD>
						<TD height="20"><asp:textbox id="txtCompanyName" runat="server" Width="232px" Visible="False"></asp:textbox><BR>
							<asp:textbox id="txtURL" runat="server" Width="231px" Visible="False"></asp:textbox><BR>
							<asp:label id="lblSubCategories" runat="server">Sub-categories:</asp:label><BR>
							<asp:listbox id="lstSubCategories" runat="server" Width="226px"></asp:listbox>&nbsp;</TD>
					</TR>
					<tr>
						<td colSpan="2" height="20"><asp:label id="lblInstructionCategories" runat="server">Click on the Category and Sub-category before click on Search below.</asp:label><asp:label id="lblInstructionNameURL" runat="server" Visible="False">Type the name of the company or/and the web address before click on Search below.</asp:label></td>
					</tr>
					<tr>
						<td colSpan="2" height="25">
							<P align="center"><asp:button id="cmdSearch" runat="server" Width="88px" Text="Search"></asp:button></P>
						</td>
					</tr>
					<tr>
						<td colSpan="2" height="14">
							<HR>
							<asp:label id="lblCountResults" runat="server"></asp:label></td>
					</tr>
					<tr>
						<td width="180" colSpan="2"><asp:datagrid id="dgResults" runat="server" AllowPaging="True">
								<Columns>
									<asp:TemplateColumn HeaderText="List of Companies">
										<ItemTemplate>
											<TABLE id="Table1" width="500">
												<TR>
													<TD align="left">
														<asp:Label id="lblCompName" runat="server"></asp:Label></TD>
													<TD align="right">
														<asp:Label id="lblLink" runat="server"></asp:Label></TD>
												</TR>
												<TR>
													<TD>
														<asp:Label id="lblCompanyName" runat="server">
															<%# DataBinder.Eval(Container.DataItem,"LINKS_COMP")%>
														</asp:Label></TD>
													<TD>Group:
														<asp:Label id="lblCategory" runat="server">
															<%# DataBinder.Eval(Container.DataItem,"LINKS_GROUP") %>
														</asp:Label></TD>
													<TD>
														<asp:Label id="lblSubCategory" runat="server">
															<%# DataBinder.Eval(Container.DataItem,"LINKS_WEBSITE") %>
														</asp:Label></TD>
												</TR>
												<TR>
													<TD colSpan="3">
														<asp:Label id="lblDescription" runat="server">
															<%# DataBinder.Eval(Container.DataItem,"LINKS_DESCRIP")%>
														</asp:Label></TD>
												</TR>
												<TR>
													<TD>Contact:
														<asp:Label id="lblContact" runat="server">
															<%# DataBinder.Eval(Container.DataItem,"LINKS_CONTACT")%>
														</asp:Label></TD>
													<TD>Phone:
														<asp:Label id="lblPhone" runat="server">
															<%# DataBinder.Eval(Container.DataItem,"LINKS_PHONE")%>
														</asp:Label></TD>
													<TD>Fax:
														<asp:Label id="lblFax" runat="server">
															<%# DataBinder.Eval(Container.DataItem,"LINKS_FAX")%>
														</asp:Label></TD>
												</TR>
												<TR>
													<TD colSpan="3">Address:
														<asp:Label id="lblAddress" runat="server">
															<%# DataBinder.Eval(Container.DataItem,"LINKS_STREET")%>
														</asp:Label>-
														<asp:Label id="lblCity" runat="server">
															<%# DataBinder.Eval(Container.DataItem,"LINKS_CITY")%>
														</asp:Label>,
														<asp:Label id="lblState" runat="server">
															<%# DataBinder.Eval(Container.DataItem,"LINKS_STATE")%>
														</asp:Label>-
														<asp:Label id="lblZip" runat="server">
															<%# DataBinder.Eval(Container.DataItem,"LINKS_ZIP")%>
														</asp:Label>-
														<asp:Label id="lblCountry" runat="server">
															<%# DataBinder.Eval(Container.DataItem,"LINKS_COUNTRY")%>
														</asp:Label></TD>
												</TR>
											</TABLE>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle Position="TopAndBottom" Mode="NumericPages"></PagerStyle>
							</asp:datagrid></td>
					</tr>
				</tbody></table>
			<TPE:TEMPLATE id="Template2" Runat="Server" Footer="true"></TPE:TEMPLATE></form>
	</body>
</HTML>
