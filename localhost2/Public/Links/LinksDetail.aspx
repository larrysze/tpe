
<%@ Page language="c#" Codebehind="LinksDetail.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.Links.LinksDetail" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" Title="Link Details" %>
<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="cphHeading">
<span class="Header Color1 Bold">Plastics Links - Link Details</span>
</asp:Content>
   <asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
	<table cellspacing="1" cellpadding="5" width="100%" bgcolor="#000000">
		<tr>
			    <td class="Content Color2 Bold DarkGray" height="27" align=right width="10%">
			    Company:
			    </td>
			    <td height="27" align="left" class="Content Color2 LightGray" colspan="3">
			    <asp:label id="lblCompany" runat="server" MaxLength="50"></asp:label>
		    	</td>
		</tr>
				<tr>
			<td width="70" align="right" class="Content Color2 Bold DarkGray">Description:
			</td>
			<td align="left" colspan="3" class="Content Color2 LightGray"><asp:label id="lblDescription" runat="server"></asp:label>
			</td>
		</tr>
		<tr>    
		    	<td align="right" class="Content Color2 Bold DarkGray">URL:</td>
			    <td class="Content Color2 LightGray" align="left" colspan="3"> <span class="LinkNormal"><asp:HyperLink id="HLWebsite" runat="server"></asp:HyperLink></span>
			    </td>
		</tr>		
		    <tr>
		    <td align="right" class="Content Color2 Bold DarkGray">Email:
		    </td>
		    <td align=left colSpan="3" class="Content Color2 LightGray"><asp:label id="lblEmail" runat="server"></asp:label></td>
	    </tr>			

		<tr>
		    <td  align="right" class="Content Color2 Bold DarkGray">Contact:
		    </td>
		    <td  align=left class="Content Color2 LightGray" style="width: 155px" colspan="3"><asp:label id="lblContact" runat="server" Width="227px"></asp:label></td>
		</tr>

	    <tr>
		    <td align="right" class="Content Color2 Bold DarkGray">Address:
		    </td>
		    <td align=left colSpan="3" class="Content Color2 LightGray"><asp:label id="lblAddress" runat="server" Width="600px"></asp:label></td>
	    </tr>
	    <tr>
		    <td align="right" class="Content Color2 Bold DarkGray">City:
		    </td>
		    <td align=left class="Content Color2 LightGray"><asp:label id="lblCity" runat="server"></asp:label></td>
		    
		    <td align="right" class="Content Color2 Bold DarkGray">Zip:</td>
			<td align="left" class="Content Color2 LightGray"><asp:label id="lblZip" runat="server"></asp:label></td>
	    </tr>
	    <tr>
	     <td align="right" class="Content Color2 Bold DarkGray">State:</td>
			<td align="left" class="Content Color2 LightGray"><asp:label id="lblState" runat="server" MaxLength="20" Columns="2"></asp:label></td>
		    <td align="right" class="Content Color2 Bold DarkGray">Country:
		    </td>
		    <td align=left class="Content Color2 LightGray"><asp:label id="lblCountry" runat="server"></asp:label></td>
		   
	    </tr>
	    		<tr>
		    <td align="right" class="Content Color2 Bold DarkGray">Phone:
		    </td>
			<td class="Content Color2 LightGray" align="left"><asp:label id="lblPhone" runat="server"></asp:label></td>
		    <td align="right" class="Content Color2 Bold DarkGray" width="10%">Fax:</td>
		    <td class="Content Color2 LightGray" colspan="1" align="left"><asp:label id="lblFax" runat="server"></asp:label></td>
	    </tr>
	
    </table>
    <div style="text-align:center; background-color:#999999">
        <br />
        <asp:Button runat="server" ID="btnDeleteLink" Text="Delete Link"  CssClass="Content Color2" Visible="false" style="background-color:orange;color:white;" onclick="btnDeleteLink_Click" /> 
        <asp:Button runat="server" ID="btnBack" Text="Back"  CssClass="Content Color2" style="background-color:orange;color:white;" onclick="btnBack_Click" /> 
        <br />
        <br />
    </div>			
</asp:Content>
