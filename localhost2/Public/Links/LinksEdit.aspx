<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page language="c#" Codebehind="LinksEdit.aspx.cs" AutoEventWireup="false" Inherits="localhost.Public.links.LinksEdit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>LinksEdit</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<style type="text/css">
.style1 { FONT-WEIGHT: bold; FONT-SIZE: 14px }
		</style>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TPE:TEMPLATE id="Template1" PageTitle="Industry Links - New" Runat="Server"></TPE:TEMPLATE>
			<table width="700" height="300" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td height="27" colspan="2" align="center"><span class="style1">Edit Links </span>
						<asp:Label id="message" runat="server" ForeColor="Red"></asp:Label>
					</td>
				</tr>
				<tr>
					<td width="99">&nbsp;&nbsp;Category:
					</td>
					<td width="601" height="27">
						<asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True"></asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td>&nbsp; SubCategory:</td>
					<td height="27"><asp:DropDownList ID="ddlSubCategory" runat="server"></asp:DropDownList></td>
				</tr>
				<tr>
					<td>&nbsp; Company:
					</td>
					<td height="27"><asp:TextBox id="txtCompany" runat="server" Width="250px"></asp:TextBox>&nbsp; 
						URL:
						<asp:TextBox id="txtWebsite" runat="server" Width="250px"></asp:TextBox></td>
				</tr>
				<tr>
					<td valign="middle">&nbsp; Description:
					</td>
					<td height="27" valign="middle"><asp:TextBox ID="txtDescription" runat="server" Width="500px" TextMode="MultiLine" Height="80px"></asp:TextBox></FONT></td>
				</tr>
				<tr>
					<td>&nbsp; Contact:</td>
					<td height="27"><asp:TextBox id="txtContact" runat="server" Width="250px"></asp:TextBox>&nbsp; 
						Phone:
						<asp:TextBox id="txtPhone" runat="server" Width="100px"></asp:TextBox>&nbsp; 
						Fax:
						<asp:TextBox id="txtFax" runat="server" Width="100px"></asp:TextBox></td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;Address:
					</td>
					<td height="27"><asp:TextBox id="txtAddress" runat="server" Width="500px"></asp:TextBox></td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;City:</td>
					<td height="27"><asp:TextBox id="txtCity" runat="server"></asp:TextBox></td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;Country:
					</td>
					<td height="27"><asp:TextBox id="txtCountry" runat="server"></asp:TextBox>&nbsp; 
						State:
						<asp:TextBox id="txtState" runat="server" Width="100px"></asp:TextBox>&nbsp; 
						ZIP:
						<asp:TextBox id="txtZip" runat="server" Width="50px"></asp:TextBox></td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;Email:
					</td>
					<td height="27"><asp:TextBox id="txtEmail" runat="server" Width="300px"></asp:TextBox></td>
				</tr>
				<tr align="center">
					<td colspan="2">&nbsp;
						<asp:Button id="update" runat="server" Width="80px" Text="update"></asp:Button></td>
				</tr>
			</table>
			<TPE:TEMPLATE id="Template2" Runat="Server" Footer="true"></TPE:TEMPLATE>
		</form>
	</body>
</HTML>
