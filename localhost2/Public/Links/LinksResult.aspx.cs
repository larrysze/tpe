using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace localhost.Public.Links
{
	/// <summary>
	/// Summary description for LinksResult.
	/// </summary>
	public partial class LinksResult : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
            bool cat_safe = false;
            SqlDataAdapter dadContent;
			DataSet dstContent;
			using (SqlConnection conn  = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();

				string sql="select Links_comp,links_id,links_descrip,links_website from links";

                string cat = "";

                if (Request.QueryString["Cat"] != null)
				{                    
                    cat = Request.QueryString["Cat"].ToString();
                    int count = cat.IndexOf(";");
                    if (count > 0)
                    {
                        cat = cat.Substring(0, count);
                    }

                    int cat_int = 100;
                    try
                    {
                        cat_int = Convert.ToInt32(cat);
                    }
                    catch (Exception ex)
                    {
                        Response.Redirect("/default.aspx");
                    }
                    if (cat_int < 54)
                    {
                        cat_safe = true;
                    }
				}

                if (cat_safe)
                {
                    sql += " where exists (select MEMBER_LINK_ID FROM LINK_CATEGORY_MEMBER where MEMBER_CATEGORY =@cat AND MEMBER_LINK_ID=LINKS_ID) ORDER BY Links_comp";

                    dadContent = new SqlDataAdapter(sql, conn);
                    dadContent.SelectCommand.Parameters.Add("@cat", SqlDbType.Int);
                    dadContent.SelectCommand.Parameters["@cat"].Value = cat;

                    dstContent = new DataSet();
                    dadContent.Fill(dstContent);
                    dg.DataSource = dstContent;
                    dg.DataBind();
                    dadContent.Dispose();
                }
                else
                {
                    Response.Redirect("/default.aspx");
                }

				if (Request.QueryString["Cat"] != null)
				{
                    if (cat_safe)
                    {
                        SqlCommand cmd = new SqlCommand("SELECT LINK_CATEGORY +': '+ LINK_SUBCATEGORY FROM LINK_CATEGORY WHERE LINK_CATEGORY_ID =@cat", conn);
                        cmd.Parameters.Add("@cat", SqlDbType.Int);
                        cmd.Parameters["@cat"].Value = cat;
                        lblTitle.Text = cmd.ExecuteScalar().ToString();
                    }
                    else
                    {
                        Response.Redirect("/default.aspx");
                    }
				}
			
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
