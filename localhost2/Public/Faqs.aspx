<%@ Page language="c#" Codebehind="Faqs.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.Faq" MasterPageFile="~/MasterPages/Template.Master" %>
<%@ MasterType virtualPath="~/MasterPages/Template.Master"%>
    
    <asp:Content ID="contentCssLink" runat="server" ContentPlaceHolderID="cphCssLink">
    <style type="text/css">
        h4
        {
	        font-size:11px;
	        line-height:0px;
	        margin-top:3px;
	        margin-bottom:5px;
        }
    </style> 

    </asp:Content>
	<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphInstructions">	    
	    
        <table class='InstructionBullets' style="margin-left:20px">
        <tr>
            <td width="35"><img src='/pics/bullet.gif'></td>
            <td>Welcome to The Plastics Exchange questions and answers section.</td>
        </tr> 
        <tr>
            <td width="35"><img src='/pics/bullet.gif'></td>
            <td>Clicking on a question below will display the answer.</td>
        </tr>
        <tr>
            <td width="35"><img src='/pics/bullet.gif'></td>
            <td>Should your question not be listed below, please ask us in the space provided below or call Member Services at 1.312.202.0002.</td>
        </tr>    
        </table>
    </asp:Content>
    <asp:Content runat="server" ContentPlaceHolderID="cphHeading">
        <div class="Header Bold Color1">Frequently Asked Questions (FAQ)</div>
    </asp:Content>
    <asp:Content runat="server" ContentPlaceHolderID="cphJavaScript">
        
	    <script language="JavaScript" type="text/javascript">			
			<!--		    
			/*
	        domCollapse()
	        written by Christian Heilmann
	        with help from Scott Benish and Craig Saila of the webdesign-L list
	        homepage: http://www.onlinetools.org/tools/domcollapse.php

	        Necessary Variables:

	        normaltext: the text content of the link in the <p></p> when the div is not shown
	        normalcolour: the colour of the text in the <p></p> when the div is not shown
	        normalbackground: the background of the <p></p>	when the div is not shown
	        highlighttext: the text content of the link in the <p></p> when the div is shown
	        highlightcolour: the colour of the text in the <p></p> when the div is shown
	        highlightbackground: the background of the <p></p>	when the div is shown

	        the texts are normal text, HTML tags will not be rendered as such!
	        the colours can be any valid CSS colour definition.

	        function domCollapse (which)
	        Written by Christian Heilmann
	        - collapses or displays <div></div> elements in the "menu" div of the document.
	        - changes the colour and the background of the corresponding <p></p>
	        element in the "menu" div.
	        - changes the text content of the <a></a> inside the <p></p>

	        Variables:
	        which:	integer > shows or collapses the <div></div> with this number in the tree.
			        dispall > shows all divs
			        hideall > collapses all divs
        */
        function domCollapse(which){
	        if (document.getElementById && document.createTextNode){
		        if (which=="dispall") {domCollapseAll(1);}
		        else if (which=="hideall") {domCollapseAll(0);}
		        else {
			        m=document.getElementById("faq");
			        trig=m.getElementsByTagName("div").item(which).style.display;
			        t=m.getElementsByTagName("h4").item(which);
			        h=t.getElementsByTagName("a").item(0).firstChild;
			        if (trig=="block") trig="none";
			        else if (trig=="" || trig=="none") trig="block";
			        if (trig=="none"){
				        h.nodeValue=h.nodeValue.replace(highlighttext,normaltext);
				        t.style.background=normalbackground;
				        t.style.color=normalcolour;
				        }
			        else {
				        h.nodeValue=h.nodeValue.replace(normaltext,highlighttext);
				        t.style.background=highlightbackground;
				        t.style.color=highlightcolour;
				        }
			        m.getElementsByTagName("div").item(which).style.display=trig;
		        }
	        }
        }

        /*
	        function domCollapseAll(show)
	        Written by Christian Heilmann
	        - collapses or displays all <div></div> elements in the "menu" div of the document.
	        - changes the colour and the background of all <p></p> elements in the "menu" div.
	        - changes the text content of the <a></a> inside the <p></p> elements

	        Variables:
	        show:	0 > collapse all divs,set all colours to normal
			        1 > show all divs,set all colours to highlight
        */
        function domCollapseAll(show){
	        if (document.getElementById && document.createTextNode){
		        m=document.getElementById("faq");
		        for (i=0;i<m.getElementsByTagName("div").length;i++){
			        t=m.getElementsByTagName("h4").item(i);
			        h=t.getElementsByTagName("a").item(0).firstChild;
			        if (show==1){
				        h.nodeValue=h.nodeValue.replace(normaltext,highlighttext);
				        t.style.background=highlightbackground;
				        t.style.color=highlightcolour;
				        m.getElementsByTagName("div").item(i).style.display="block";
			        }
			        else {
				        h.nodeValue=h.nodeValue.replace(highlighttext,normaltext);
				        t.style.background=normalbackground;
				        t.style.color=normalcolour;
				        m.getElementsByTagName("div").item(i).style.display="none";
			        }
		        }
	        }
        }
        // Adding backwards compatibility
        if (document.getElementById && document.createTextNode){
	        document.write('<style type="text/css">#faq div{display:none;}</style>')
	        }

			if(document.all && !document.getElementById) 
			{
				document.getElementById = function(id) { 
					return document.all[id]; 
				}
			}
			
    		highlightcolour="#333";
			highlightbackground="#999999";
			highlighttext="-";

    		normalcolour="#333333";
			normalbackground="#999999";
			normaltext="+";
		    //-->
	    </script>	
    </asp:Content>
	<asp:Content runat="server" ContentPlaceHolderID="cphMain">
	
    <table class="faq" width="780" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<table width="780" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#999999">
					<tr>
						<td>
							<div id="faq">
								<table runat="server" id="htmlTableFaq" style="text-align:left">
								</table>
							</div>
						</td>
					</tr>
				</table>
				<div class="DivTitleBar" style="visibility:hidden">
				    <div class="Header Bold Color1">Didn't find your answer?</div>
				</div>
				<br \>
				<table height="160" cellSpacing="1" cellPadding="0" align="center" border="0"  style="visibility:hidden">
					<tr>
						<td width="601" align="center" valign="top" bgcolor="#dbdcd7" class="Content" colSpan="1">
							If you did not find your question/answer�please ask us here and we will get 
							right back to you.
							<asp:TextBox id="txtNewQuestion" runat="server" TextMode="multiline" cols="90" rows="3" Width="568px"
								Height="72px"></asp:TextBox>
							<br>
							<br>
							<table width="150" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<asp:ImageButton id="btnSubmit" runat="server" ImageUrl="/images2/submit_unpressed.jpg" height="22"
											width="161" BorderStyle="None"></asp:ImageButton>
									</td>
								</tr>
							</table>
						</td>
						<td width="173" valign="top">
						    <div runat="server" id="divNewSubmission">
							    <table border="0" cellspacing="0" cellpadding="5">
								    <tr>
									    <td class="Content Bold Color2">Name:</td>
								    </tr>
								    <tr>
									    <td>
                                            <asp:TextBox id="txtName" runat="server" CssClass="InputForm" size="20" maxlength="100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName" ErrorMessage="*"></asp:RequiredFieldValidator></td>
								    </tr>
								    <tr>
									    <td class="Content Bold Color2">E-mail:</td>
								    </tr>
								    <tr>
									    <td><asp:TextBox id="txtEmail" runat="server" CssClass="InputForm" size="20" maxlength="100"></asp:TextBox>
									    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtEmail" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
									    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"  ErrorMessage="*" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
									    </td>
								    </tr>
							    </table>
							</div>
							<div runat="server" id="divThankYou" visible="false">
							    <table border="0" cellspacing="0" cellpadding="5">
								    <tr>
									    <td class="Content Bold Color2">Thank you for your submission!</td>
								    </tr>	
								    <tr>
									    <td>
									        <asp:Button id="btnBack" runat="server" CssClass="InputForm" Text="Back" OnClick="btnContinue_Click"></asp:Button>
									    </td>
								    </tr>							
							    </table>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</asp:Content>
