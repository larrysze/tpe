<%@ Control Language="c#" AutoEventWireup="false" Codebehind="UserTransaction.ascx.cs" Inherits="localhost.Public.UserTransaction" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<TABLE id="Table2" style="WIDTH: 294px; HEIGHT: 160px">
	<TR>
		<TD colSpan="2">
			<P align="right">
				<asp:DropDownList id="DropDownList1" runat="server"></asp:DropDownList></P>
		</TD>
	</TR>
	<TR>
		<TD colSpan="2">
			<asp:datagrid id="dgAdmin" runat="server" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
				CellPadding="2" DataKeyField="VARID" HorizontalAlign="Center" ShowFooter="True" CssClass="DataGrid"
				ItemStyle-CssClass="DataGridRow" AlternatingItemStyle-CssClass="DataGridRow_Alternate" HeaderStyle-CssClass="DataGridHeader">
				<AlternatingItemStyle CssClass="DataGridRow_Alternate"></AlternatingItemStyle>
				<ItemStyle CssClass="DataGridRow"></ItemStyle>
				<HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
				<Columns>
					<asp:TemplateColumn>
						<HeaderTemplate>
						</HeaderTemplate>
						<ItemTemplate>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="VARID" SortExpression="VARID ASC" HeaderText="ID #">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARCONTRACT" SortExpression="VARCONTRACT ASC" HeaderText="Product">
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARSIZE" SortExpression="VARSIZE ASC" HeaderText="Size">
						<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARMELT" SortExpression="VARMELT ASC" HeaderText="Melt">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARDENS" SortExpression="VARDENS ASC" HeaderText="Density">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARADDS" SortExpression="VARADDS ASC" HeaderText="Adds">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARTERM" SortExpression="VARTERM ASC" HeaderText="Delivery Terms">
						<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARPRICE2" SortExpression="VARPRICE2 ASC" HeaderText="Price2" DataFormatString="{0:c}">
						<ItemStyle Font-Bold="True" HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARFREIGHT" SortExpression="VARFREIGHT ASC" HeaderText="Freight"></asp:BoundColumn>
					<asp:BoundColumn DataField="VARVALUE" SortExpression="VARVALUE ASC" HeaderText="Value" DataFormatString="${0:#,###}">
						<ItemStyle HorizontalAlign="Right"></ItemStyle>
					</asp:BoundColumn>
					<asp:BoundColumn DataField="VARBIDOFFER" HeaderText="BID/OFFER DATE"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="VARFREIGHTPOINT" HeaderText="FreightPoint"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="VARUSRID" HeaderText="UserID"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="VARBROKERID" HeaderText="BrokerID"></asp:BoundColumn>
				</Columns>
			</asp:datagrid></TD>
		<asp:panel id="pnlFindIt" runat="server"></TR>
	</asp:panel></TR></TABLE>
