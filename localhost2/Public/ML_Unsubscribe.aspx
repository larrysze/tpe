<%@ Page Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="ML_Unsubscribe.aspx.cs" Inherits="localhost.Public.ML_Unsubscribe" Title="Unsubscribe" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMain" runat="server">

    <br /><br />
    <div class="Content">

    <asp:Label ID="lblEncryptEmail" runat="server" Visible="false"></asp:Label>
    
    <asp:Panel ID="pnlInput" runat="server" Visible="true">
        Enter your email and click Unsubscribe to stop receiving our Market Updates
        <br /><br /><asp:TextBox ID="txtEmail" runat="server" Width="250"></asp:TextBox><br />
        <asp:Button ID="btnUnsub" runat="server" Text="Unsubscribe" OnClick="btnUnsub_Click" />
    </asp:Panel>
    
    <asp:Panel ID="pnlThankYou" runat="server" Visible="false">
        <asp:Label ID="lblThankYouMessage" runat="server"></asp:Label>
    </asp:Panel>
    
    </div>
    <br /><br />   
</asp:Content>
