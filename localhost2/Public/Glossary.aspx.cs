using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for glossary.
	/// </summary>
	public partial class Glossary : System.Web.UI.Page
	{
		public ArrayList Word = new ArrayList();
		public ArrayList Definition = new ArrayList();
		protected System.Web.UI.WebControls.PlaceHolder phActionButton;
		private static string BgColor = "#DBDCD7";
	
		private void fillDefinitions()
		{
			int w = 0;
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				using (SqlDataReader dr = DBLibrary.GetDataReaderFromSelect(conn,"Select word,definition from Glossary order by word"))
				{
					while (dr.Read())
					{
						Word.Add(dr["word"].ToString());
						Definition.Add(dr["definition"].ToString());
					}
				}
			}			
		}

		private void buildWordsTable()
		{

			string strActionBoxHTML;
			HtmlTableRow tr = new HtmlTableRow();
			for (int i=0; i < Word.Count-1; i++)
			{
				if ((i>0) && (i%4 == 0))
				{
					tblWords.Rows.Add(tr);
					tr = new HtmlTableRow();
				}
				//<td class="glossary_arrow">&nbsp;</td>
        
				HtmlTableCell tc = new HtmlTableCell();
				//<td width="16" class="glossary_arrow">&nbsp;</td>
				tc.Width="16";
				tc.Attributes.Add("class","glossary_arrow");
				tc.InnerHtml = "&nbsp;";
				tr.Cells.Add(tc);
				
				HtmlTableCell tc2 = new HtmlTableCell();
				//<td width="178" bgcolor="#DBDCD7"><a href="texto.html" class="menu2">Texto</a></td>
				tc2.Width="178";
				tc2.BgColor=BgColor;

				strActionBoxHTML = "linkset["+ (i).ToString()+"]='<a name=\"#" + Word[i] + "\"><div class=\"menu2\" align=center>"+ Word[i] +"</div></a>'" + ((char)13).ToString() ;
				strActionBoxHTML += "linkset["+(i).ToString()+"]+='<div class=\"blackcontent\">" + HelperFunction.RemoveQuotationMarks(Definition[i].ToString()) + "</div>'" + ((char)13).ToString();

				this.phActionButton.Controls.Add (new LiteralControl(strActionBoxHTML));

				//tc2.InnerHtml = "<a href=#" + Word[i] + " class='menu2' onmouseover=\"showdefinition(event, linkset["+ (i).ToString()+"])\" onMouseout=\"delayhidemenu()\">" + Word[i] + "</a>";
				tc2.InnerHtml = "<a class='menu2' name=\"" + Word[i] + "\" onmouseover=\"showdefinition(event, linkset["+ (i).ToString()+"])\" onMouseout=\"delayhidemenu()\">" + Word[i] + "</a>";
				
				//strActionBoxHTML = "linkset["+ (i+1).ToString()+"]='<div class=\"menu2\" align=center>"+ Word[i] +"</div>'" + ((char)13).ToString() ;
				//strActionBoxHTML += "linkset["+(i+1).ToString()+"]+='<div class=\"blackcontent\">" + Definition[i] + "</div>'" + ((char)13).ToString();
				//this.phActionButton.Controls.Add (new LiteralControl(strActionBoxHTML));

				//tc2.InnerHtml = "<a href=#" + Word[i] + " class='menu2' onmouseover=\"showdefinition(event, linkset["+ (i+1).ToString()+"])\" onMouseout=\"delayhidemenu()\">" + Word[i] + "</a>";
				tr.Cells.Add(tc2);	
				
				
			}
			if (tr.Cells.Count > 0)
			{
				int ColSpanCount = 8 - tr.Cells.Count;
				// fill out the last row
				for (int j=0; j<ColSpanCount / 2; j++)
				{
					HtmlTableCell tc3 = new HtmlTableCell();
					tc3.ColSpan=2;
					tc3.Width="194";
					tc3.BgColor = BgColor;
					tc3.InnerHtml="&nbsp;";
					tr.Cells.Add(tc3);
				}
				// add last row				
				tblWords.Rows.Add(tr);
			}
		}

		private void buildDefinitionsTable()
		{
			/*	<table width="778" height="20" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td width="140" background="/images2/orangebar_background.jpg"><p class="menu2"><a href="top" class="menu2">Actual - Back to Top</a> </p></td>
					<td width="638" background="/images2/orangebar_background.jpg"><img src="/images2/glossary/arrow_top.gif" width="17" height="16" /></td>
				</tr>
				</table>
				<table width="778" height="30" border="0" align="center" cellpadding="0" cellspacing="0">
					<tr>
					<td width="760" bgcolor="#DBDCD7" class="blackcontent">The physical or cash commodity; all contracts being traded on The Plastics Exchange are actuals.</td>
					</tr>
				</table>
				*/
			for (int i=0; i<Word.Count-1;i++)
			{
				HtmlTable tbl = new HtmlTable();
				tbl.Width="100%";
//				tbl.Height="16";
				tbl.Border=0;
				tbl.Align="center";
				tbl.CellPadding=0;
				tbl.CellSpacing=0;

				// add header with link back to top
				HtmlTableRow trHead = new HtmlTableRow();
				HtmlTableCell tc = new HtmlTableCell();
				tc.Attributes.Add("background","/images2/orangebar_background.jpg");
				tc.InnerHtml = "<p class='menu2'>" + Word[i]+ " -<a href='#top' name=#" + Word[i] + " class='menu2'>Back to Top <img border='none' src='/images2/glossary/arrow_top.gif'/></a> </p>";
				trHead.Cells.Add(tc);
				tblDefinitions.Rows.Add(trHead);

				// now add definition
				HtmlTableRow trDef = new HtmlTableRow();
				HtmlTableCell tc2 = new HtmlTableCell();
				tc2.BgColor=BgColor;
				tc2.Attributes.Add("class","blackcontent");
				tc2.InnerHtml=Definition[i].ToString();
				trDef.Cells.Add(tc2);
				tblDefinitions.Rows.Add(trDef);
			}			
		}

        
		protected void Page_Load(object sender, System.EventArgs e)
		{        
			fillDefinitions();
			buildWordsTable();
			//buildDefinitionsTable();
			if (Request.QueryString["Word"] != null) 
			{
				Response.Redirect("Glossary.aspx#" + Request.QueryString["Word"].ToString());	
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
