<%@ Page Language="c#" Codebehind="Resin_Markets.aspx.cs" AutoEventWireup="true" Inherits="localhost.Public.Resin_Markets"  enableEventValidation="false"  %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="cc1" namespace="System.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%--<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>--%>


<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Resin Markets</title>
	<style type="text/css">
	
		.WhiteLink a:link {color:#FFFFFF; font-size: 10px;}
		.WhiteLink a:visited {color:#FFFFFF; font-size: 10px;}
		.WhiteLink a:hover {color:#FFFFFF; font-size: 10px;}
		.OrangeLink a:link {color:#FD9D00; font-size: 12px;}
		.OrangeLink a:visited {color:#FD9D00; font-size: 12px;}
		.OrangeLink a:hover {color:#FD9D00; font-size: 12px;}
		.RedLink a:link {color:red; font-size: 12px;}
		.RedLink a:visited {color:red; font-size: 12px;}
		.RedLink a:hover {color:red; font-size: 12px;}
		.LinkNormal { text-align: left;}
		.Content 	{
					font-family: Verdana, Arial, Helvetica, sans-serif;
					font-size: 10px;
					font-style: normal;
					line-height: normal;
					font-variant: normal;
					padding-left: 2px;
				}
				
		.yui .ajax__tab_header 
		{
		    font-family:arial,helvetica,clean,sans-serif;
		    font-size:small;
		    border-bottom:solid 5px #FF8B00;
		}
		.yui .ajax__tab_header .ajax__tab_outer 
		{
		    background: #d8d8d8 repeat-x;
		    margin:0px 0.16em 0px 0px;
		    padding:.5px 0px .5px 0px;
		    vertical-align:bottom;
		    border:solid 1px #a3a3a3;
		    border-bottom-width:0px;
		}
		.yui .ajax__tab_header .ajax__tab_tab
		{    
		    color:#000;    
		    padding:0.35em 0.35em;    
		    margin-right:0.01em;
		}
		.yui .ajax__tab_hover .ajax__tab_outer 
		{
		    background: #bfdaff repeat-x left -1300px;
		}
		.yui .ajax__tab_active .ajax__tab_tab 
		{
		    color:#000;    
		    font-weight: bold;
		}
		.yui .ajax__tab_active .ajax__tab_outer
		{
		    background: #FF8B00 repeat-x left -1400px;
		}
		.yui .ajax__tab_body 
		{
		    font-family:verdana,tahoma,helvetica;
		    font-size:10pt;
		    padding:0.25em 0.5em;
		    background-color:#000000;    
		    border:solid 1px #808080;
		    border-top-width:0px;
		}



		.news .ajax__tab_header 
		{
		    font-family:arial,helvetica,clean,sans-serif;
		    font-size:small;
		    border-bottom:solid 5px #FF8B00;
		}
		.news .ajax__tab_header .ajax__tab_outer 
		{
		    background: #d8d8d8 repeat-x;
		    margin:0px 0.16em 0px 0px;
		    padding:.5px 0px .5px 0px;
		    vertical-align:bottom;
		    border:solid 1px #a3a3a3;
		    border-bottom-width:0px;
		}
		.news .ajax__tab_header .ajax__tab_tab
		{    
		    color:#000;    
		    padding:0.35em 0.35em;    
		    margin-right:0.01em;
		}
		.news .ajax__tab_hover .ajax__tab_outer 
		{
		    background: #bfdaff repeat-x left -1300px;
		}
		.news .ajax__tab_active .ajax__tab_tab 
		{
		    color:#000;    
		    font-weight: bold;
		}
		.news .ajax__tab_active .ajax__tab_outer
		{
		    background: #FF8B00 repeat-x left -1400px;
		}
		.news .ajax__tab_body 
		{
		    font-family:verdana,tahoma,helvetica;
		    font-size:10pt;
		    padding:0.25em 0.5em;
		    background-color:#DBDCD7;    
		    border:solid 1px #808080;
		    border-top-width:0px;
		}						
	</style> 
</head>
<body>


    <form id="form1" style="width:660px;" runat="server">  
    

		<asp:ScriptManager ID="ScriptManager1" runat="server">
		</asp:ScriptManager>
		<asp:placeholder id="phSpotParameters" runat="server" />
		<table cellspacing="0" cellpadding="0" width="660" align="center" style="background-color:#333333; " border="0">
			<tbody>			    
			    <tr>
			        <td class="LinkNormal OrangeLink" valign="middle" align="left" >
			            <img src="/Pics/Header.jpg" alt="ThePlasticsExchange 312-202-0002" />
			        </td>
			    </tr>
				<tr>
					<td class="LinkNormal OrangeLink" valign="middle" align="left" style="background:/pics/home_bar_titles_r1_c1.jpg; width:184; height:10 ">
					<strong class="LinkNormal OrangeLink"><font color="#FD9D00">Resin Markets - Spot</font></strong> </td>
					<td style="background:/pics/home_bar_titles_r1_c1.jpg"></td>
					<td style="background:/pics/home_bar_titles_r1_c1.jpg; height:14px; width:50%;" align="left"  >
					<div id="lblChartName" class="Header Bold Color3"></div></td>
				</tr>
			</tbody>
		</table>
		<table cellspacing="0" cellpadding="0" width="660" align="center" style="background-color:#000000" border="0">
		 <tbody>
			<tr>
				<td style="width:300">
					<ContentTemplate>
						<table cellspacing="0" cellpadding="0" width="100%" border="0">
							    <tr>
								<td>					                
									<asp:datagrid id="dgSpot" runat="server" width="100%" bordercolor="Black" enableviewstate="False"
									 horizontalalign="Center" autogeneratecolumns="False" cellpadding="2" font-size="Small" 
									 gridlines="Horizontal" borderstyle="None" backcolor="black" 
									 borderwidth="0px" forecolor="White" cssclass="LinkNormal WhiteLink">
									    <AlternatingItemStyle BorderStyle="Solid" BackColor="Black"></AlternatingItemStyle>
									    <HeaderStyle Font-Bold="true"></HeaderStyle>
									    <Columns>
										<asp:BoundColumn DataField="GRADE" HeaderText="Resin">
										    <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
										    <ItemStyle Wrap="False" CssClass="Content Bold Color4"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="varsize" HeaderText="Total lbs" DataFormatString="{0:#,##}">
										    <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
										    <ItemStyle Wrap="False" CssClass="Content Bold Color4"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Min" HeaderText="Low" DataFormatString="{0:0.##0}">
										    <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
										    <ItemStyle Wrap="False" CssClass="Content Bold Color4"></ItemStyle>
										</asp:BoundColumn>
										<asp:BoundColumn DataField="Max" HeaderText="High" DataFormatString="{0:0.##0}">
										    <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
										    <ItemStyle Wrap="False" CssClass="Content Bold Color4"></ItemStyle>
										</asp:BoundColumn>
										<asp:TemplateColumn Visible="False" HeaderText="Price Range">
										    <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
										    <ItemStyle Wrap="False" CssClass="Content Bold Color4"></ItemStyle>
										</asp:TemplateColumn>
									    </Columns>
									    <ItemStyle BorderColor="Black" BorderWidth="0px" />
									</asp:datagrid>                
								</td>
							 </tr>
							</table>
				</ContentTemplate>                                
		</td>

		<td ><asp:literal id="LiteralImg" runat="server"></asp:literal> </td></tr></tbody></table>
 
		<asp:placeholder id="Placeholder1" runat="server" />
		<table cellspacing="0" cellpadding="0" width="660" align="center" style="background-color:#333333; " border="0">
			<tbody>
				<tr>
					<td class="LinkNormal OrangeLink" valign="middle" align="left" style="background:/pics/home_bar_titles_r1_c1.jpg; width:184; height:10 ">
					<strong class="LinkNormal OrangeLink"><font color="#FD9D00">Resin Markets - Contract</font></strong> </td>
					<td style="background:/pics/home_bar_titles_r1_c1.jpg"></td>
					<td style="background:/pics/home_bar_titles_r1_c1.jpg; height:14px; width:50%;" align="left"  >
					<div id="Div1" class="Header Bold Color3"></div></td>
				</tr>
			</tbody>
		</table>
		<table cellspacing="0" cellpadding="0" width="660" align="center" style="background-color:#000000" border="0">
		 <tbody>
			<tr>
				<td style="width:300">
		
				<ContentTemplate>
				    <table cellspacing="0" cellpadding="3" width="100%" border="0">
							    <tr>
								<td>
								    <asp:datagrid id="dgContract" runat="server" width="100%" bordercolor="Black" 
								    enableviewstate="False" horizontalalign="Center" autogeneratecolumns="False" cellpadding="3" font-size= "Smaller"
								    gridlines="Horizontal" borderstyle="None" backcolor="black" borderwidth="0px" forecolor="White" 
								    cssclass="LinkNormal WhiteLink">
						    <AlternatingItemStyle BorderStyle="Solid" BackColor="Black"></AlternatingItemStyle>
						    <HeaderStyle Font-Bold="true"></HeaderStyle>
						    <Columns>
							<asp:BoundColumn DataField="cont_labl" HeaderText="Resin">
							    <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
							   <ItemStyle Wrap="False" CssClass="Content Bold Color4"></ItemStyle> 
							</asp:BoundColumn>                      
							<asp:BoundColumn  ItemStyle-Width="62px"></asp:BoundColumn>                                                                                                                                           
							<asp:BoundColumn DataField="bid" HeaderText="Bid" DataFormatString="{0:0.##0}">
							    <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
							    <ItemStyle Wrap="False" CssClass="Content Bold Color4"></ItemStyle>
							</asp:BoundColumn>
							
							<asp:BoundColumn DataField="OFFR" HeaderText="Offer" DataFormatString="{0:0.##0}">
							    <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
							    <ItemStyle Wrap="False" CssClass="Content Bold Color4"></ItemStyle>
							</asp:BoundColumn>
						    </Columns>
						    <ItemStyle BorderColor="Black" BorderWidth="0px" />
						</asp:datagrid>  
					                        
					                        					                                                                
					    </td>
							    </tr>
							    						   <tr>
					    <td>
						    <asp:Label id="hplTotalLbs" runat="server" onprerender="hplTotalLbs_PreRender" Font-Size="12" ForeColor="red"  />
						
					    </td>
					</tr>

							</table>
		                    
				</ContentTemplate>                                
		</td>
		<td ><asp:literal id="LiteralImg1" runat="server"></asp:literal> </td></tr></tbody></table>

        <div id="flag" style=" width:300px;visibility: hidden; display: none">
        <asp:Label runat="server" Visible="true" ID="lblContractChartID"/>
        <asp:Label runat="server" Visible="true" ID="lblSpotChartID"/>

        </div>
        <div id="flag2" style="visibility: hidden; display: none">
        1</div>
		 	   
    </form>
</body>
</html>