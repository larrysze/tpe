<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page language="c#" Codebehind="Transactions.aspx.cs" AutoEventWireup="false" Inherits="localhost.Public.WebForm1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Transaction</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript">
		function PreviewEmail(param)
		{
			window.open('../administrator/Email.aspx' + param,'','toolbar=false,resizable=1,scrollbars=1,menubar=false,width=800,height=750,top=20,left=100');
		}
		function Track(param)
		{
			window.open('../hauler/Railcar_Event.aspx?Num='+ param,'','toolbar=false,resizable=1,scrollbars=1,menubar=false,width=550,height=250,top=20,left=100');			
		}
		</script>
</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TPE:TEMPLATE id="Template1" PageTitle="Contact Details" Runat="Server"></TPE:TEMPLATE><BR>
			<TABLE id="Table1" cellSpacing="1" cellPadding="1" border="0">
				<TR>
					<TD class="PageHeader" colSpan="2"><asp:label id="lblTitle" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD style="FONT-SIZE: 11pt" colSpan="2"><asp:label id="lblExplanation" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD style="FONT-SIZE: 11pt" align="right" colSpan="2">&nbsp;</TD>
				</TR>
				<TR>
					<TD style="FONT-SIZE: 11pt; WIDTH: 378px" align="left"><asp:label id="lblTotalBalanceLabel" runat="server" Font-Bold="True">Total Balance:</asp:label>&nbsp;
						<asp:label id="lblTotalBalance" runat="server" Font-Bold="True"></asp:label></TD>
					<TD style="FONT-SIZE: 11pt" align="right"><asp:dropdownlist id="ddlPeriod" runat="server" AutoPostBack="True">
							<asp:ListItem Value="O">All Open Transactions</asp:ListItem>
							<asp:ListItem Value="CM">Current month</asp:ListItem>
							<asp:ListItem Value="60D">Last 60 days</asp:ListItem>
							<asp:ListItem Value="6M">Last 6 months</asp:ListItem>
							<asp:ListItem Value="ALL">All</asp:ListItem>
						</asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD colSpan="2"><asp:label id="lblMessage" runat="server" Font-Bold="True" Font-Size="Small"></asp:label><asp:datagrid id="DataGrid1" runat="server" AutoGenerateColumns="False" BorderColor="Black" Width="192px"
							AlternatingItemStyle-CssClass="DataGridRow_Alternate2" ItemStyle-CssClass="DataGridRow2" CssClass="DataGrid2" CellPadding="4">
							<SelectedItemStyle Wrap="False"></SelectedItemStyle>
							<EditItemStyle Wrap="False"></EditItemStyle>
							<AlternatingItemStyle Wrap="False" CssClass="DataGridRow_Alternate2"></AlternatingItemStyle>
							<ItemStyle Wrap="False" CssClass="DataGridRow2"></ItemStyle>
							<HeaderStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" ForeColor="Black" BackColor="White"></HeaderStyle>
							<FooterStyle Wrap="False"></FooterStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Order">
									<HeaderStyle Font-Bold="True"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server">Order#:</asp:Label>
										<asp:Label id="lblOrder" runat="server" Font-Bold="True"></asp:Label><BR>
										<asp:Label id="LabelPO" runat="server">PO#:</asp:Label>
										<asp:Label id="lblPO" runat="server" Font-Bold="True"></asp:Label><BR>
										<asp:Label id="Label3" runat="server">Date:</asp:Label>
										<asp:Label id="lblDate" runat="server" Font-Bold="True"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Product">
									<HeaderStyle Font-Bold="True"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="lblProduct" runat="server" Font-Bold="True"></asp:Label><BR>
										<asp:Label id="Label5" runat="server">Size:</asp:Label>
										<asp:Label id="lblSize" runat="server" Font-Bold="True"></asp:Label><BR>
										<asp:Label id="Label7" runat="server">Weight:</asp:Label>
										<asp:Label id="lblWeight" runat="server" Font-Bold="True"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Specifications">
									<HeaderStyle Font-Bold="True"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="LabelMelt" runat="server">Melt:</asp:Label>
										<asp:Label id="lblMelt" runat="server" Font-Bold="True"></asp:Label><BR>
										<asp:Label id="LabelDensity" runat="server">Density:</asp:Label>
										<asp:Label id="lblDensity" runat="server" Font-Bold="True"></asp:Label><BR>
										<asp:Label id="LabelAdditives" runat="server">Additives:</asp:Label>
										<asp:Label id="lblAdditives" runat="server" Font-Bold="True"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Price">
									<HeaderStyle Font-Bold="True"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="lblPrice" runat="server"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Shipping Status">
									<HeaderStyle Font-Bold="True" Wrap="False"></HeaderStyle>
									<ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="lblShipDate" runat="server"></asp:Label><BR>
										<asp:ImageButton id="imgTrack" runat="server" Visible="False" ImageUrl="../images/Track.gif"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Font-Bold="True"></HeaderStyle>
									<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
									<ItemTemplate>
										<TABLE id="Table2" cellSpacing="0" cellPadding="0" border="0">
											<TR>
												<TD noWrap>
													<asp:Label id="Label6" runat="server" Font-Bold="True">Amount Due:</asp:Label></TD>
												<TD noWrap align="right">
													<asp:Label id="lblAmountDue" runat="server"></asp:Label></TD>
											</TR>
											<TR>
												<TD noWrap>
													<asp:Label id="Label11" runat="server" Font-Bold="True">Paid:</asp:Label></TD>
												<TD noWrap align="right">
													<asp:Label id="lblPaid" runat="server"></asp:Label><BR>
													<asp:Image id="Image1" runat="server" Width="77px" Height="1px" ImageUrl="../images/line.JPG"></asp:Image></TD>
											</TR>
											<TR>
												<TD noWrap>
													<asp:Label id="Label13" runat="server" Font-Bold="True">Balance:</asp:Label></TD>
												<TD noWrap align="right">
													<asp:Label id="lblBalance" runat="server"></asp:Label></TD>
											</TR>
										</TABLE>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Due Date">
									<HeaderStyle Font-Bold="True" Wrap="False"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="lblDueDate" runat="server"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Font-Bold="True"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemTemplate>
										&nbsp;
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Wrap="False"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
			</TABLE>
			<BR>
			<TPE:TEMPLATE id="Template2" Runat="Server" Footer="true"></TPE:TEMPLATE></form>
	</body>
</HTML>
