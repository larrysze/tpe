<%@ Page Language="c#" CodeBehind="About_Us.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.About_Us" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" Title="The Plastics Exchange: Commodity Plastic Resin, Buy Resin, Sell Resin, Plastic Materials" %>
<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master"%>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphHeading">
    <div class="Header Bold Color1">About Us</div>
</asp:Content>
	
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
	<div style="margin:6px;">
		<p style="text-align: justify" class="Content"><br />
<b>The Plastics Exchange</b> is a real-time marketplace for anonymously buying and selling domestic and
 international commodity grade resin (HDPE, LDPE, LLDPE, GPPS, HIPS, HoPP, CoPP). We offer trading in prime and offgrade 
 resins in railcars, bulk or packaged truckloads and FCL. All orders are for actual delivery. 
        <br />
		<br />
		We have trading relationships with a large network of major producers, distributors, traders and brokers. 
        Through these relationships, we have access to 100+ million pounds of warehoused resin and direct/indirect access to most producers?spot railcar offerings.    
        <br />
		<br />
		
		Resin offers are placed online for 1,7, or 14 days and are subject to prior 
		sale and credit approval. We generally maintain 15-25 million pounds of spot offers 
		listed on our trading floor, sometimes more or less depending on market conditions. 
		If you do not see what you want, submit an <span class="LinkNormal"><a href="/public/Submit_RFQ.aspx">
			RFQ</a></span> or call us and we will find it for you. 		
		<br />
		<br />
    	The Plastics Exchange acts as a clearinghouse to complete trades. We are principals, 
    	not brokers ?we handle the credit and logistics. In order to assure the integrity of the market, 
    	we guarantee product quality, delivery and payment of every transaction. 
		<br />
		<br />
		The Plastics Exchange provides in-depth research and news coverage of the resin marketplace. 
		We display historical data, captured from the asking price of our Contract Market, for 21 commodity resin grades as daily, weekly and monthly charts. 

		<br />
		<br />
		We gather unique market information based on exchange observations along with interactions with producers, brokers, 
		traders, distributors, and processors. This information is compiled and published weekly in as Market Updates, 
		available both on our website and by email, simply <span class="LinkNormal"><a href="/Public/Registration.aspx">
			register</a></span> to join or distribution list.
		<br />
		<br />
		We provide these trading tools as a service to the industry, without charge, to 
		help you make more intelligent buying and selling decisions.
		<br />
		<br />
		Please feel free to browse any of our references and guides and <span class="LinkNormal"><a href="/Public/Contact_Us.aspx">
			contact us</a></span> with any questions you may have. <br />
		<br />
		Check out the <span class="LinkNormal" ><a href="/docs/RulesOfTheExchange.aspx">Rules of the Exchange</a></span>
		and then <span class="LinkNormal"><a href="/Public/Registration.aspx?Referrer=<%=Request.Url.ToString()%>">Register</a></span>
		today!<br />
		<br />
	</p>
	</div>
</asp:Content>
