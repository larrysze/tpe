<%@ Page Language="c#" CodeBehind="HistoricalPrices.aspx.cs" AutoEventWireup="false" Inherits="localhost.Public.HistoricalPrices" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<form runat="server" id="Form">
	<TPE:Template PageTitle="Historical prices" Runat="server" id="Template1" />
	<table border="0" cellpadding="0" cellspacing="2" width="100%">
		<TR>
			<TD class="ListHeadlineBold" align="center" colSpan="2"><STRONG>Historical Prices</STRONG>
				<BR>
				<BR>
			</TD>
		</TR>
		<TR>
			<TD align="center" width="50%" height="50%" vAlign="bottom">
				Start date:
				<asp:DropDownList id="ddlStartMonth" runat="server" Width="60px">
					<asp:ListItem Value="01">Jan</asp:ListItem>
					<asp:ListItem Value="02">Feb</asp:ListItem>
					<asp:ListItem Value="03">Mar</asp:ListItem>
					<asp:ListItem Value="04">Apr</asp:ListItem>
					<asp:ListItem Value="05">May</asp:ListItem>
					<asp:ListItem Value="06">Jun</asp:ListItem>
					<asp:ListItem Value="07">Jul</asp:ListItem>
					<asp:ListItem Value="08">Aug</asp:ListItem>
					<asp:ListItem Value="09">Sep</asp:ListItem>
					<asp:ListItem Value="10">Oct</asp:ListItem>
					<asp:ListItem Value="11">Nov</asp:ListItem>
					<asp:ListItem Value="12">Dec</asp:ListItem>
				</asp:DropDownList>&nbsp;
				<asp:TextBox id="txtStartDay" runat="server" Width="50px"></asp:TextBox>&nbsp;
				<asp:TextBox id="txtStartYear" runat="server" Width="67px"></asp:TextBox></TD>
			<TD align="center" rowSpan="2">
				<asp:RadioButtonList id="rbFrequency" runat="server">
					<asp:ListItem Value="1" Selected="True">Daily</asp:ListItem>
					<asp:ListItem Value="5">Weekly</asp:ListItem>
					<asp:ListItem Value="21">Monthly</asp:ListItem>
				</asp:RadioButtonList>&nbsp;</TD>
		</TR>
		<tr>
			<TD align="center" width="575" vAlign="top">&nbsp; End date:
				<asp:DropDownList id="ddlEndMonth" runat="server" Width="60px">
					<asp:ListItem Value="01">Jan</asp:ListItem>
					<asp:ListItem Value="02">Feb</asp:ListItem>
					<asp:ListItem Value="03">Mar</asp:ListItem>
					<asp:ListItem Value="04">Apr</asp:ListItem>
					<asp:ListItem Value="05">May</asp:ListItem>
					<asp:ListItem Value="06">Jun</asp:ListItem>
					<asp:ListItem Value="07">Jul</asp:ListItem>
					<asp:ListItem Value="08">Aug</asp:ListItem>
					<asp:ListItem Value="09">Sep</asp:ListItem>
					<asp:ListItem Value="10">Oct</asp:ListItem>
					<asp:ListItem Value="11">Nov</asp:ListItem>
					<asp:ListItem Value="12">Dec</asp:ListItem>
				</asp:DropDownList>&nbsp;
				<asp:TextBox id="txtEndDay" runat="server" Width="50px"></asp:TextBox>&nbsp;
				<asp:TextBox id="txtEndYear" runat="server" Width="67px"></asp:TextBox></TD>
		</tr>
		<TR>
			<TD class="ListHeadlineBold" align="center" colSpan="2"><BR>
				Select a contract: &nbsp;
				<asp:DropDownList id="ddlContracts" runat="server" Width="160px"></asp:DropDownList><BR>
				<BR>
			</TD>
		</TR>
		<tr>
			<td colspan="2" align="center"><BR>
				<BR>
				<asp:Button id="btnDownload" runat="server" Text="Download To Spreadsheet"></asp:Button></td>
		</tr>
	</table>
	<TPE:Template Footer="true" Runat="server" id="Template2" /><BR>
</form>
