<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="localhost.Public.ChangePassword" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master"%>
<%@ Register assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="System.Web.UI" tagprefix="cc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphHeading">
    <div class="Header Bold Color1">Change user information </div>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphMain">
    <asp:ScriptManager id="sciptmanager1" EnablePartialRendering="True" runat="server">
    </asp:ScriptManager>
    <br /><br />
    <div align="center">
        <cc1:TabContainer ID="TabContainer1"  runat="server" BackColor="#dbdcd7" Width="400">
            <cc1:TabPanel ID="TabPanel1" HeaderText="Personal information" runat="server" BackColor="#dbdcd7">
            <ContentTemplate>
                 <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                    <table align="center" class="Content" bgcolor="#dbdcd7">
                        <tr>
                            <td><br />
                            <asp:Label ID="lblError1" runat="server" ForeColor="Red" Visible="false"></asp:Label><br /><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblFirstName" runat="server" Text="First Name"></asp:Label>
                            <asp:TextBox ID="txtFirstName" Width="200px" runat="server" CssClass="InputForm"></asp:TextBox><br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblLastName" runat="server" Text="Last Name"></asp:Label>
                            <asp:TextBox ID="txtLastName" Width="200px" runat="server" CssClass="InputForm"></asp:TextBox><br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                            <asp:TextBox ID="txtTitle" Width="200px" runat="server" CssClass="InputForm"></asp:TextBox><br /><br />
                            <asp:Label ID="lblCompany" runat="server" Text="Company Name"></asp:Label>
                            <asp:TextBox ID="txtCompany" Width="200px" runat="server" CssClass="InputForm"></asp:TextBox><br /><br />&nbsp;
                            <asp:Label ID="lblPhone" runat="server" Text="Phone Number"></asp:Label>
                            <asp:TextBox ID="txtPhone" Width="200px" runat="server" CssClass="InputForm"></asp:TextBox><br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblEmail" runat="server" Text="E-mail"></asp:Label>
                            <asp:TextBox ID="txtEmail" Width="200px"  runat="server" CssClass="InputForm"></asp:TextBox><br /><br />
                            <p align="center"><asp:Button ID="btnChange1" runat="server" Text="Change" CssClass="Content Color2" OnClick="btnChange1_Click" /></p>
                            </td>
                        </tr>
                    </table>
                    </ContentTemplate>
                    </asp:UpdatePanel>
            </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel ID="TabPanel2" HeaderText="Resin preferences" runat="server">
            <ContentTemplate>
            <asp:UpdatePanel runat="server">
            <ContentTemplate>
            <table align="center" class="Content" bgcolor="#dbdcd7">
              <tr>
                <td>
                <asp:Label ID="lblError2" runat="server" ForeColor="Red" Visible="false"></asp:Label><br />
				<asp:Table id="tblDynamic" runat="server" BackColor="#dbdcd7" CssClass="Content" BorderStyle="None" GridLines="Horizontal" CellSpacing="0" CellPadding="0" ></asp:Table>
				<p align="center"><asp:Button ID="Button1" runat="server" Text="Change" CssClass="Content Color2" OnClick="btnChange2_Click" /></p>
                </td>
              </tr>
            </table>
            </ContentTemplate>
            </asp:UpdatePanel>
            </ContentTemplate>
            </cc1:TabPanel>
            <cc1:TabPanel ID="TabPanel3" HeaderText="Password" runat="server">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                    <table align="center" class="Content" bgcolor="#dbdcd7">
                        <tr>
                            <td><br />
                                <asp:Label ID="lblError" runat="server" ForeColor="Red" Visible="false"></asp:Label><br /><br/>
                                <asp:Label ID="lblOldPwd" runat="server" Text="Enter your old password"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="txtOldPwd" TextMode="password" CssClass="InputForm" runat="server"></asp:TextBox><br /><br />
                                <asp:Label ID="lblNewPwd" runat="server" Text="Enter your new password"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="txtNewPwd" TextMode="password" runat="server" CssClass="InputForm"></asp:TextBox><br /><br />
                                <asp:Label ID="lblConfirmPwd" runat="server" Text="Confirm your new password"></asp:Label>&nbsp;
                                <asp:TextBox ID="txtConfirmPwd" TextMode="password" runat="server" CssClass="InputForm"></asp:TextBox><br />
                                <p align="center"><asp:Button ID="btnChange" runat="server" Text="Change" CssClass="Content Color2" OnClick="btnChange_Click" /></p>
                            </td>
                        </tr>
                    </table>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </cc1:TabPanel>
        </cc1:TabContainer>
    </div>
    <br /><br />
</asp:Content>