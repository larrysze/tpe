using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Web.Mail;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.IO;
using TPE.Utility;


namespace localhost.Public
{ 
	/// <summary>
	/// Summary description for Forgot_Password.
	/// </summary>
	public partial class Forgot_Password : System.Web.UI.Page
	{

        protected System.Text.StringBuilder sbXmlFileName = null;
        protected System.Text.StringBuilder sbXslFileName = null;
		protected void Page_Load(object sender, System.EventArgs e)
        {
            sbXmlFileName = new System.Text.StringBuilder(Server.MapPath("/email/xml/forgot_passwords.xml"));
            sbXslFileName = new System.Text.StringBuilder(Server.MapPath("/email/xsl/forgot_passwords.xsl")); 
			// Put user code to initialize the page here
			btnSend.Attributes.Add("onMouseOver","document.getElementById('" + btnSend.ClientID + "').src='/images/buttons/my_password_red.jpg';");
			btnSend.Attributes.Add("onMouseOut","document.getElementById('" + btnSend.ClientID + "').src='/images/buttons/my_password_orange.jpg';");
        }
	

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSend.Click += new System.Web.UI.ImageClickEventHandler(this.btnSend_Click);

		}
		#endregion
        //pers_account_disabled
		public void btnSend_Click(object sender, ImageClickEventArgs e)
		{
			//Database connection
			SqlConnection conn;
			SqlCommand cmdContent;
			SqlDataReader dtrContent;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
            //StringBuilder sb;


            cmdContent = new SqlCommand("SELECT pers_frst_name,pers_last_name,pers_logn,pers_pswd,pers_mail,pers_account_disabled from person where pers_mail='" + txtEmail.Text + "'", conn);
            dtrContent = cmdContent.ExecuteReader();
            if (dtrContent.Read())
            {
                //sb = new StringBuilder();
                XmlDocument xmldoc = new XmlDocument();

                //we are sure that file and "unpublished" record exist (see getUnpublishedID())

                using (XmlTextReader xmlreader = new XmlTextReader(sbXmlFileName.ToString()))
                {
                    xmldoc.Load(xmlreader);
                }

                XmlNode domain = xmldoc.SelectSingleNode("//forgot_passwords/domain");
                domain.InnerText = ConfigurationSettings.AppSettings["DomainName"].ToString();

                XmlNode firstname = xmldoc.SelectSingleNode("//forgot_passwords/firstname");
                firstname.InnerText = dtrContent["PERS_FRST_NAME"].ToString();

                XmlNode email = xmldoc.SelectSingleNode("//forgot_passwords/email");
                email.InnerText = dtrContent["PERS_MAIL"].ToString();

                XmlNode password = xmldoc.SelectSingleNode("//forgot_passwords/password");
                password.InnerText = Crypto.Decrypt(dtrContent["PERS_PSWD"].ToString());

                XmlNode ip = xmldoc.SelectSingleNode("//forgot_passwords/ip");
                ip.InnerText = Request.UserHostAddress.ToString();

                XmlNode phone = xmldoc.SelectSingleNode("//forgot_passwords/phone");
                phone.InnerText = ConfigurationSettings.AppSettings["PhoneNumber"].ToString();


                string XMLoutput;
                StringWriter writer = new StringWriter();
                xmldoc.Save(writer);
                XMLoutput = writer.ToString();
                writer.Close();


                if (dtrContent["PERS_ACCOUNT_DISABLED"].ToString() == "True")
                    sbXslFileName = new System.Text.StringBuilder(Server.MapPath("/email/xsl/forgot_passwords_disabled.xsl"));

                MailMessage mail;
                mail = new MailMessage();
                mail.From = ConfigurationSettings.AppSettings["strEmailInfo"].ToString();
                mail.To = txtEmail.Text;
                //mail.Bcc = Application["strEmailOwner"].ToString() + ";" + Application["strEmailAdmin"].ToString();
                mail.Subject = "Your Password";
                mail.Body = TransformXML(XMLoutput);
                mail.BodyFormat = MailFormat.Html;

                TPE.Utility.EmailLibrary.Send(Context, mail);
                lblResults.Text = "<BR><BR>Your password has been sent to " + txtEmail.Text;


                Hashtable htPar = new Hashtable();
                htPar.Add("@IP", Request.UserHostAddress.ToString());
                htPar.Add("@ID_TYPE", "3");
                htPar.Add("@DATE_TIME", DateTime.Now.ToString());
                htPar.Add("@PASSWORD", dtrContent["PERS_PSWD"].ToString());
                htPar.Add("@EMAIL", dtrContent["PERS_MAIL"].ToString());

                string strSql = "INSERT DASHBOARD (ID_TYPE, DATE_TIME, IP, PASSWORD, BODY, EMAIL) VALUES (@ID_TYPE, @DATE_TIME, @IP, @PASSWORD, '" + mail.Body.ToString() + "', @EMAIL)";

                DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSql, htPar);


            }
            else
            {
                lblResults.Text = "<BR><BR>I'm sorry but there were no records in our database that match the email address the you provided (<font color=red>" + txtEmail.Text + "</font>).";
                lblResults.Text += "  If you feel that you have reached this message in error or for additional assistance <a href='mailto:webmaster@theplasticsexchange.com'>contact us</a>";

            }
            pnDirections.Visible = false;
            pnThanks.Visible = true;
            dtrContent.Close();
            conn.Close();
           

		}

        

        private string TransformXML(string XMLoutput)
        {
      
            string HTMLoutput;

            XslTransform xslt = new XslTransform();
            StringReader rdr = new StringReader(XMLoutput);

            xslt.Load(sbXslFileName.ToString());
            //XPathDocument doc = new XPathDocument(XMLfile);
            
            StringWriter writer = new StringWriter();
            xslt.Transform(new XPathDocument(rdr), null, writer, null);
            HTMLoutput = writer.ToString();
            return HTMLoutput;
        }

	}
}
