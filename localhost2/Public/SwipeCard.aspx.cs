using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for SwipeCard.
	/// </summary>
	public class SwipeCard : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Panel pnlSwipe;
		protected System.Web.UI.WebControls.Label lblRegister;
		protected System.Web.UI.WebControls.Panel pnlRegister;
		protected System.Web.UI.WebControls.Label lblCardData;
		protected System.Web.UI.WebControls.Label lblSwipeTitle;
		protected System.Web.UI.WebControls.TextBox card_data;
	
		private void Page_Load(object sender, System.EventArgs e)
		{



			Response.Cookies["Id"].Expires = DateTime.Today.AddDays(-1);
			Session.Abandon();
	

			if((Request.QueryString["card_data"] != null) && (Request.QueryString["card_data"] != ""))
			{
				pnlSwipe.Visible = false;
				pnlRegister.Visible = true;
				lblCardData.Text = "Card Data:" + Request.QueryString["card_data"];
				if(lblCardData.Text.Length <= 20)
				{
					Response.Redirect("/Public/SwipeCard.aspx");
				}
				else
				{
					Response.Redirect("/Public/Registration.aspx?card_data=" + Request.QueryString["card_data"]);
				}
			}
			else
			{
				pnlSwipe.Visible = true;
				pnlRegister.Visible = false;

			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
