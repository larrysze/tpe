using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public class WebForm1 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			DataTable dtSample = new DataTable();
			dtSample.Columns.Add("Order");
			dtSample.Columns.Add("PO");
			dtSample.Columns.Add("Date");
			dtSample.Columns.Add("Product");
			dtSample.Columns.Add("Size");
			dtSample.Columns.Add("Weight");
			dtSample.Columns.Add("Melt");
			dtSample.Columns.Add("Density");
			dtSample.Columns.Add("Additives");
			dtSample.Columns.Add("Price");
			dtSample.Columns.Add("ShipDate");
			dtSample.Columns.Add("AmountDue");
			dtSample.Columns.Add("Paid");
			dtSample.Columns.Add("Balance");
			dtSample.Columns.Add("DueDate");

			for(int i=0;i<5;i++)
			{
				DataRow dtNewLine = dtSample.NewRow();
				dtNewLine["Order"] = i.ToString();
				dtNewLine["PO"] = i.ToString();
				dtNewLine["Date"] = DateTime.Today.ToShortDateString();
				dtNewLine["Product"] = "Product Description " + i.ToString();
				dtNewLine["Size"] = i.ToString() + i.ToString() + i.ToString() + " lbs";
				dtNewLine["Weight"] = i.ToString() + i.ToString() + i.ToString() + i.ToString();
				dtNewLine["Melt"] = i.ToString();
				dtNewLine["Density"] = i.ToString();
				dtNewLine["Additives"] = "Add " + i.ToString();
				dtNewLine["Price"] = "$" + i.ToString() + i.ToString() + i.ToString() + i.ToString();
				dtNewLine["ShipDate"] = DateTime.Today.ToShortDateString();
				dtNewLine["AmountDue"] = "$200,000.00";
				dtNewLine["Paid"] = "$50,000.00";
				dtNewLine["Balance"] = "$200,000.00";
				dtNewLine["DueDate"] = DateTime.Today.AddDays(30).ToShortDateString();
				dtSample.Rows.Add(dtNewLine);
			}

			DataGrid1.DataSource = dtSample;
			DataGrid1.DataBind();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.DataGrid1.PreRender += new System.EventHandler(this.DataGrid1_PreRender);
			this.DataGrid1.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DataGrid1_ItemDataBound);
			this.DataGrid1.SelectedIndexChanged += new System.EventHandler(this.DataGrid1_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void DataGrid1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void DataGrid1_PreRender(object sender, System.EventArgs e)
		{
			
		}

		private void DataGrid1_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblOrder")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Order"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblPO")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"PO"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblDate")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Date"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblProduct")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Product"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblSize")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Size"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblWeight")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Weight"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblMelt")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Melt"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblDensity")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Density"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblAdditives")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Additives"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblPrice")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Price"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblShipDate")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"ShipDate"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblAmountDue")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"AmountDue"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblPaid")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Paid"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblBalance")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Balance"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblDueDate")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"DueDate"));
			}
		}
	}
}
