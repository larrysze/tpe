using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;

namespace localhost.Public
{
    public partial class ScrollingPageTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Add spot offers			
                using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
                {
                    conn.Open();
                    AddSpotOffers(conn);
                    
                }

            }
        }

        private void AddSpotOffers(SqlConnection conn)
        {
            StringBuilder sbSpotOffers = new StringBuilder();
            string strOffer;
            SqlCommand cmdSpotOffers;
            SqlDataReader dtrSpotOffers;

            cmdSpotOffers = new SqlCommand("SELECT OFFR_ID,OFFR_QTY,VARSIZE=CAST(OFFR_QTY AS VARCHAR)+' '+(CASE OFFR_SIZE WHEN 1 THEN 'lbs' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 42000 THEN 'TL Boxes' WHEN 45000 THEN 'BT' WHEN 44092 THEN 'TL Bags' WHEN 190000 THEN 'HC'	END)+(CASE WHEN OFFR_QTY>1 AND OFFR_SIZE <> 44092 THEN ' ' ELSE ' ' END),OFFR_PROD, OFFR_PRCE, OFFR_DETL, OFFR_DENS, OFFR_MELT From BBOFFER WHERE OFFR_HOT='1' AND OFFR_ZIP is not null", conn);
            dtrSpotOffers = cmdSpotOffers.ExecuteReader();
            int iCount;
            iCount = 1;

            // create list of parameters for java applet
            ArrayList offerArray = new ArrayList();
            ArrayList offerIDArray = new ArrayList();
            // read amounts into two parrallel arrays
            while (dtrSpotOffers.Read())
            {
                strOffer = dtrSpotOffers["VARSIZE"].ToString();
                strOffer += dtrSpotOffers["OFFR_PROD"].ToString();
                strOffer += " ";
                strOffer += dtrSpotOffers["OFFR_DETL"].ToString();
                if (HelperFunction.getSafeStringFromDB(dtrSpotOffers["OFFR_MELT"]).Trim() != "")
                {
                    strOffer += " Melt: ";
                    strOffer += HelperFunction.getSafeStringFromDB(dtrSpotOffers["OFFR_MELT"]).Trim();
                }
                if (HelperFunction.getSafeStringFromDB(dtrSpotOffers["OFFR_DENS"]).Trim() != "")
                {
                    strOffer += " Density: ";
                    strOffer += HelperFunction.getSafeStringFromDB(dtrSpotOffers["OFFR_DENS"]).Trim();
                }
                strOffer += " $";
                strOffer += dtrSpotOffers["OFFR_PRCE"].ToString();

                offerArray.Add(strOffer);
                offerIDArray.Add(dtrSpotOffers["OFFR_ID"].ToString());
                iCount++;
            }
            dtrSpotOffers.Close();

            // the parameters need to start random point.  this logic defines that random pnt and displays the records for after
            // the point and then the ones before it
            Random rand = new Random();
            int iRandom = rand.Next(1, (iCount - 1));

            for (int k = iRandom; k <= iCount - 1; k++)
            {
                sbSpotOffers.Append("<param name=text" + (k - iRandom).ToString() + " value='" + offerArray[k - 1].ToString() + "'>");
                sbSpotOffers.Append("<param name=link" + (k - iRandom).ToString() + " value=" + System.Configuration.ConfigurationSettings.AppSettings["UrlPrefix"] + "://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/Spot/Inquire.aspx?Id=" + offerIDArray[k - 1].ToString() + "><param name=target_frame" + k.ToString() + " value=_self>");
            }
            for (int k = 0; k <= (iCount - iRandom - 1); k++)
            {
                sbSpotOffers.Append("<param name=text" + k.ToString() + " value='" + offerArray[k].ToString() + "'>");
                sbSpotOffers.Append("<param name=link" + k.ToString() + " value=" + System.Configuration.ConfigurationSettings.AppSettings["UrlPrefix"] + "://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/Spot/Inquire.aspx?Id=" + offerIDArray[k].ToString() + "><param name=target_frame" + k.ToString() + " value=_self>");
            }
            // add parameters as literal control
            phSpotParameters.Controls.Add(new LiteralControl(sbSpotOffers.ToString()));
        }

    }
}
