using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for ML_Unsubscribe_2.
	/// </summary>
	public partial class ML_Unsubscribe : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
            pnlInput.Visible = true;
            pnlThankYou.Visible = false;
		}

        protected void btnUnsub_Click(object sender, EventArgs e)
        {
            //unsubscribe email and show confirmation message
            if (txtEmail.Text != "")
            {
                Hashtable param = new Hashtable();
                param.Add("@VALUE", HelperFunction.RemoveQuotationMarks(txtEmail.Text));
                string sqlStr = "SELECT PERS_MAIL FROM PERSON WHERE PERS_MAIL=@VALUE";

                DataTable dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), sqlStr, param);
                if (dt.Rows.Count != 0)
                {
                    sqlStr = "UPDATE PERSON SET Email_ENBL=0 WHERE PERS_MAIL=@PARAM1";
                    param.Clear();
                    param.Add("@PARAM1", HelperFunction.RemoveQuotationMarks(txtEmail.Text));
                    DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sqlStr, param);
                    lblThankYouMessage.Text = "<center>Your request has been proceesed.  You will no longer receive emails from the The Plastics Exchange<br>Thank you.</center>";
                    txtEmail.Text = "";
                }
                else
                {
                    lblThankYouMessage.Text = "Email wasn't found in our Database";
                }
            }

            pnlInput.Visible = false;
            pnlThankYou.Visible = true;
        }
	}
}
