using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using localhost.Administrator;
using TPE.Utility;

namespace localhost.Public
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public class WebForm1 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblExplanation;
		protected System.Web.UI.WebControls.DropDownList ddlPeriod;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.Label lblTotalBalanceLabel;
		protected System.Web.UI.WebControls.Label lblTotalBalance;
		string screenMode = "P"; //(P)Purchases or (S)Sells
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["ID"]==null) Response.Redirect("/default.aspx");

			if (Request.QueryString["Mode"]!=null)
			{
				screenMode = Request.QueryString["Mode"].ToString();
			}

			if (!IsPostBack)
			{
				//Session["ID"] = "4456";

				string SQL;
				if (screenMode=="P")
				{
					lblTitle.Text = "Purchases";
					lblExplanation.Text = "Track shipment, payment and get all the paperwork fot transactions you have open with The Plastics Exchange.";
					lblTotalBalanceLabel.Text = "Total amount owed:";
					SQL = "SELECT SUM(SHIPMENT_BUYR_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY)) - ISNULL(SUM(PAY_AMNT),0) BALANCE " +
						"FROM SHIPMENT S, (SELECT PAY_ORDR, PAY_PERS, SUM(PAY_AMNT) PAY_AMNT, PAY_SHIPMENT FROM PAYMENT GROUP BY PAY_ORDR, PAY_PERS, PAY_SHIPMENT) P " +
						"WHERE " +
						"	SHIPMENT_BUYER_CLOSED_DATE is null " +
						"	AND S.SHIPMENT_ID *= P.PAY_SHIPMENT " +
						"	AND S.SHIPMENT_ORDR_ID *= P.PAY_ORDR " +
						"	AND S.SHIPMENT_BUYR *= P.PAY_PERS " +
						"	AND S.SHIPMENT_BUYR = " + DBLibrary.ScrubSQLStringInput(Session["ID"].ToString());
				}
				else
				{
					lblTitle.Text = "Sales";
					lblExplanation.Text = "Track payment and get all the paperwork fot transactions you have open with The Plastics Exchange.";
					lblTotalBalanceLabel.Text = "Total amount due:";
					SQL = "SELECT SUM(SHIPMENT_PRCE*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY)) - ISNULL(SUM(PAY_AMNT),0) BALANCE " +
						"FROM SHIPMENT S, (SELECT PAY_ORDR, PAY_PERS, SUM(PAY_AMNT) PAY_AMNT, PAY_SHIPMENT FROM PAYMENT GROUP BY PAY_ORDR, PAY_PERS, PAY_SHIPMENT) P " +
						"WHERE " +
						"	SHIPMENT_SELLER_CLOSED_DATE1 is null " +
						"	AND S.SHIPMENT_ID *= P.PAY_SHIPMENT " +
						"	AND S.SHIPMENT_ORDR_ID *= P.PAY_ORDR " +
						"	AND S.SHIPMENT_SELR *= P.PAY_PERS " +
						"	AND S.SHIPMENT_SELR = " + DBLibrary.ScrubSQLStringInput(Session["ID"].ToString());
				}
				BindDataGrid();
				
				//Update TotalBalance
				using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
				{
					using (SqlDataReader dtrTotal = DBLibrary.GetDataReaderFromSelect(conn,SQL))
					{
						if (dtrTotal.Read())
						{
							if (dtrTotal["BALANCE"] == DBNull.Value)
								lblTotalBalance.Text = "$0.00";
							else
								lblTotalBalance.Text = string.Format("{0:c}", Math.Round(System.Convert.ToDecimal(dtrTotal["BALANCE"].ToString()),2));
						}
					}
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddlPeriod.SelectedIndexChanged += new System.EventHandler(this.ddlPeriod_SelectedIndexChanged);
			this.DataGrid1.PreRender += new System.EventHandler(this.DataGrid1_PreRender);
			this.DataGrid1.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DataGrid1_ItemDataBound);
			this.DataGrid1.SelectedIndexChanged += new System.EventHandler(this.DataGrid1_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		
		private void BindDataGrid()
		{
			DataSet ds = new DataSet();
			DataTable dtTransaction = new DataTable();
			SqlDataAdapter dataADP = new SqlDataAdapter();
			using (SqlConnection Conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				Conn.Open();				
			
				if (screenMode=="P") dataADP.SelectCommand=new SqlCommand("spPurchases",Conn);
				if (screenMode=="S") dataADP.SelectCommand=new SqlCommand("spSales",Conn);

				dataADP.SelectCommand.CommandType=CommandType.StoredProcedure;
				dataADP.SelectCommand.Parameters.Add("@PersID", DBLibrary.ScrubSQLStringInput(Session["ID"].ToString()));
				dataADP.SelectCommand.Parameters.Add("@RangeTransactions", DBLibrary.ScrubSQLStringInput(ddlPeriod.SelectedItem.Value));

				dataADP.Fill(dtTransaction);
			}

			DataTable dtSample = new DataTable();
			dtSample.Columns.Add("Order");
			dtSample.Columns.Add("PO");
			dtSample.Columns.Add("Date");
			dtSample.Columns.Add("Product");
			dtSample.Columns.Add("Size");
			dtSample.Columns.Add("Weight");
			dtSample.Columns.Add("Melt");
			dtSample.Columns.Add("Density");
			dtSample.Columns.Add("Additives");
			dtSample.Columns.Add("Price");
			dtSample.Columns.Add("ShipDate");
			dtSample.Columns.Add("AmountDue");
			dtSample.Columns.Add("Paid");
			dtSample.Columns.Add("Balance");
			dtSample.Columns.Add("DueDate");
			dtSample.Columns.Add("RC_Number");

			for(int i=0;i<dtTransaction.Rows.Count;i++)
			{
				DataRow dtNewLine = dtSample.NewRow();
				dtNewLine["Order"] = dtTransaction.Rows[i]["ORDER_NUMBER"].ToString();
				dtNewLine["PO"] = dtTransaction.Rows[i]["PO_NUMBER"].ToString();;
				dtNewLine["Date"] = dtTransaction.Rows[i]["ORDER_DATE"].ToString();
				dtNewLine["Product"] = dtTransaction.Rows[i]["CONTRACT"].ToString();
				dtNewLine["Size"] = dtTransaction.Rows[i]["SIZE"].ToString();
				dtNewLine["Weight"] = dtTransaction.Rows[i]["WEIGHT"].ToString();
				dtNewLine["Melt"] = dtTransaction.Rows[i]["MELT"].ToString();
				dtNewLine["Density"] = dtTransaction.Rows[i]["DENSITY"].ToString();
				dtNewLine["Additives"] = dtTransaction.Rows[i]["ADDITIVES"].ToString();
				dtNewLine["Price"] = "$" + System.Convert.ToString(Math.Round(System.Convert.ToDecimal(dtTransaction.Rows[i]["PRICE"].ToString()),4));
				dtNewLine["RC_Number"] = dtTransaction.Rows[i]["RC_NUMBER"].ToString();

				if (dtTransaction.Rows[i]["DELIVERY_DATE"].ToString()=="")
					if (dtTransaction.Rows[i]["SHIPPING_DATE"].ToString()=="")
						dtNewLine["ShipDate"] = "Not shipped";
					else
						dtNewLine["ShipDate"] = "Shipped " + dtTransaction.Rows[i]["SHIPPING_DATE"].ToString();
				else
					dtNewLine["ShipDate"] = "Delivered " + dtTransaction.Rows[i]["DELIVERY_DATE"].ToString();

				if (dtTransaction.Rows[i]["AMOUNT_DUE"] == DBNull.Value) dtTransaction.Rows[i]["AMOUNT_DUE"]="0";
				dtNewLine["AmountDue"] = string.Format("{0:c}", Math.Round(System.Convert.ToDecimal(dtTransaction.Rows[i]["AMOUNT_DUE"].ToString()),2));
				
				if (dtTransaction.Rows[i]["PAID"] == DBNull.Value) dtTransaction.Rows[i]["PAID"]="0";
				dtNewLine["Paid"] = string.Format("{0:c}", Math.Round(System.Convert.ToDecimal(dtTransaction.Rows[i]["PAID"].ToString()),2));

				decimal balance = Math.Round(System.Convert.ToDecimal(dtTransaction.Rows[i]["AMOUNT_DUE"].ToString()),2) - Math.Round(System.Convert.ToDecimal(dtTransaction.Rows[i]["PAID"].ToString()),2);
				dtNewLine["Balance"] = string.Format("{0:c}", balance);

				if ((balance<=0) || (dtTransaction.Rows[i]["PAYMENT_STATUS"].ToString()=="Closed"))
					dtNewLine["DueDate"] = "-";
				else
					dtNewLine["DueDate"] = System.Convert.ToDateTime(dtTransaction.Rows[i]["ORDER_DATE"].ToString()).AddDays(System.Convert.ToDouble(dtTransaction.Rows[i]["PAYMENT_TERMS"].ToString())).ToShortDateString();

				dtSample.Rows.Add(dtNewLine);
			}

			DataGrid1.DataSource = dtSample;
			DataGrid1.DataBind();

			if (dtTransaction.Rows.Count==0)
			{
				DataGrid1.Visible = false;
				lblMessage.Text = "No items were found.";
			}
			else
			{
				DataGrid1.Visible = true;
				if (dtTransaction.Rows.Count==1)
                	lblMessage.Text = "1 transaction was found.";
				else
					lblMessage.Text = dtTransaction.Rows.Count + " transactions were found.";
			}

			if (screenMode=="S")
			{
				DataGrid1.Columns[4].Visible = false;
				DataGrid1.Columns[7].Visible = false;
			}
			else
			{
				DataGrid1.Columns[8].Visible = false;
			}
		}

		private void DataGrid1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void DataGrid1_PreRender(object sender, System.EventArgs e)
		{
			
		}

		private void DataGrid1_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblOrder")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Order"));
				
				if (screenMode=="P") 
				{
					string po = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"PO"));
					if (po.Trim()!="")
						((System.Web.UI.WebControls.Label)e.Item.FindControl("lblPO")).Text = po;
					else
						((System.Web.UI.WebControls.Label)e.Item.FindControl("LabelPO")).Visible = false;
				}
				else
				{
					((System.Web.UI.WebControls.Label)e.Item.FindControl("LabelPO")).Visible = false;
					((System.Web.UI.WebControls.Label)e.Item.FindControl("lblPO")).Visible = false;
				}

				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblDate")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Date"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblProduct")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Product"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblSize")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Size"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblWeight")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Weight"));
				
				//Melt Index
				string melt = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Melt"));
				if (melt.Trim()!="")
					((System.Web.UI.WebControls.Label)e.Item.FindControl("lblMelt")).Text = melt;
				else
					((System.Web.UI.WebControls.Label)e.Item.FindControl("LabelMelt")).Visible = false;
				
				//Density
				string density = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Density"));
				if (density.Trim()!="")
					((System.Web.UI.WebControls.Label)e.Item.FindControl("lblDensity")).Text = density;
				else
					((System.Web.UI.WebControls.Label)e.Item.FindControl("LabelDensity")).Visible = false;
				
				//Additives
				string additives = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Additives"));
				if (additives.Trim()!="")
					((System.Web.UI.WebControls.Label)e.Item.FindControl("lblAdditives")).Text = additives;
				else
					((System.Web.UI.WebControls.Label)e.Item.FindControl("LabelAdditives")).Visible = false;

				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblPrice")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Price"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblShipDate")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"ShipDate"));
				
				//Track Button
				if (String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Size")).IndexOf("Rail Car")>=0)
				{
					if (String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"ShipDate")).StartsWith("Shipped"))
					{
						e.Item.Cells[4].Text +=((System.Web.UI.WebControls.Label)e.Item.FindControl("lblShipDate")).Text+"<BR><img src='/images/Track.gif' onclick=\"Track('"+(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "RC_NUMBER")))+"')\" onmouseover=this.style.cursor='hand'>";
						
					}
                }
				
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblAmountDue")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"AmountDue"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblPaid")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Paid"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblBalance")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Balance"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblDueDate")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"DueDate"));

				if (screenMode=="P")
				{
					e.Item.Cells[7].Text = "<a href=\"javascript:PreviewEmail('?EmailBody_Shipment_Documents_Type=4&amp;EmailBody_Shipment_Documents_TransactionID=" + String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Order")) + "')\"" + ">Sales Confirmation</a>";
					if (String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"ShipDate")).IndexOf("Not shipped")<0)
					{
						e.Item.Cells[7].Text += "<BR><a href=\"javascript:PreviewEmail('?EmailBody_Shipment_Documents_Type=1&amp;EmailBody_Shipment_Documents_TransactionID=" + String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Order")) + "')\"" + ">Invoice</a>";
						e.Item.Cells[7].Text += "<BR><a href=\"javascript:PreviewEmail('?EmailBody_Shipment_Documents_Type=3&amp;EmailBody_Shipment_Documents_TransactionID=" + String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Order")) + "')\"" + ">Certificate</a>";
					}
				}
				else
				{
					e.Item.Cells[8].Text = "<a href=\"javascript:PreviewEmail('?EmailBody_Shipment_Documents_Type=2&amp;EmailBody_Shipment_Documents_TransactionID=" + String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Order")) + "')\"" + ">Purchase Order</a>";
				}					
			}
		}

		private void ddlPeriod_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BindDataGrid();
		}
	}
}
