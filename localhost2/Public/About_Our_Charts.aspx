<%
   '*****************************************************************************
   '*   1. File Name       : Public\About_Our_Charts.aspx                       *
   '*   2. Description     : A short message explains our charts, accessed from *
   '*			     homepage                                           *
   '*						                                *
   '*   3. Modification Log:                                                    *
   '*     Ver No.       Date          Author             Modification           *
   '*   -----------------------------------------------------------------       *
   '*      1.00       2-27-2004       Xiaoda             Comment                *
   '*                                                                           *
   '*****************************************************************************
%>
<%@ Page Language="c#" CodeBehind="About_Our_Charts.aspx.cs" AutoEventWireup="false" Inherits="localhost.Public.About_Our_Charts" %>
<head>
	<title>About Our Charts</title>
</head>
<font face="Verdana, Arial, Helvetica,sans-serif"><img src="/images/1x1.gif" height="5"><center>
		<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td CLASS="S5" colspan="5" background="../images/Bars/BG.gif" align="center"><img src="/images/1x1.gif" width="3">
					<font color="white" size="3"><b>About our Charts</b></font>
				</td>
			</tr>
			<tr>
				<td bgcolor="#000000" width="1"><img src="/images/1x1.gif" width="1">
				</td>
				<td width="3"><img src="/images/1x1.gif" width="1">
				</td>
				<td><font size="2">
						<center>Estimated delivered prices to the end-user; including LTL, truckloads and 
							hoppercars blended average.<br>
						</center>
					</font>
				</td>
				<td width="3"><img src="/images/1x1.gif" width="1">
				</td>
				<td bgcolor="#000000" width="1"><img src="/images/1x1.gif" width="1">
				</td>
			</tr>
			<tr>
				<td bgcolor="#000000" width="1"><img src="/images/1x1.gif" width="1">
				</td>
				<td colspan="3"><img src="/images/1x1.gif" height="6">
				</td>
				<td bgcolor="#000000" width="1"><img src="/images/1x1.gif" width="1">
				</td>
			</tr>
			<tr>
				<td bgcolor="#000000" width="1"><img src="/images/1x1.gif" width="1">
				</td>
				<td colspan="3"><center><input type="button" class="tpebutton" value='OK' onClick="javascript:window.close()">
				</td>
				<td bgcolor="#000000" width="1"><img src="/images/1x1.gif" width="1">
				</td>
			</tr>
			<tr>
				<td bgcolor="#000000" width="1"><img src="/images/1x1.gif" width="1">
				</td>
				<td colspan="3"><img src="/images/1x1.gif" height="4">
				</td>
				<td bgcolor="#000000" width="1"><img src="/images/1x1.gif" width="1">
				</td>
			</tr>
			<tr>
				<td colspan="5" background="../images/Bars/BG.gif"><img src="/images/1x1.gif" height="1">
				</td>
			</tr>
</font></table></center></body>
