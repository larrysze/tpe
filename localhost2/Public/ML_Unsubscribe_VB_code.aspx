<%@ Page Language="VB" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Web.Mail" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>

<%
If Request.Form("Mail")<>"" Then
            Dim cmdAddtoBuffer as SqlCommand
            Dim connAddtoBuffer as SqlConnection
            connAddtoBuffer = new SqlConnection(Application("DBConn"))
            connAddtoBuffer.Open()
            Dim conn as SqlConnection
	    Dim cmdContent as SQLCommand
	    Dim Rec0 as SqlDataReader
	    conn = new SqlConnection(Application("DBConn"))
            conn.Open()

            'Check if this email exist in our database
            cmdContent= new SqlCommand("SELECT PERS_MAIL FROM PERSON WHERE PERS_MAIL='"+Request.Form("Mail")+"'", conn)
	    Rec0= cmdContent.ExecuteReader()
	    Rec0.Read()

            'Exist
            If Rec0.Hasrows Then
            Rec0.Close()

            'Disable this email in PERSON table
            'cmdContent =  new SqlCommand("UPDATE PERSON SET Email_ENBL=0 WHERE PERS_MAIL='"+Request.Form("Mail")+"'",conn)
            'cmdContent.ExecuteNonQuery()
            End IF

            'Insert email to EMAIL_BUFFER talbe, a VB script on GRANT(Server) will be running every night
            'to remove the email(Is_Add is false) from MAILING table in Intranet databse

			'cmdAddtoBuffer =  new SqlCommand("INSERT INTO EMAIL_BUFFER (Email,Is_Add) VALUES ('"+Request.Form("Mail")+"','0')",connAddtoBuffer)
            'cmdAddtoBuffer.ExecuteNonQuery()
            'connAddtoBuffer.Close()
            response.write ("<center><br><br><br>Your request has been proceesed.  You will no longer receive emails from the The Plastics Exchange<br>Thank you.</center>")

            'Alert administrator
            Dim mail1 AS MailMessage
	    mail1 = new MailMessage()
	    mail1.From = "info@ThePlasticsExchange.com"
	    mail1.To = Application("strEmailAdmin")
	    mail1.Cc = Application("strEmailOwner")
	    mail1.Subject = "Unsubscribe"
	    mail1.Body = Request.Form("Mail")
	    mail1.BodyFormat = MailFormat.Html
            TPE.Utility.EmailLibrary.Send(mail1)
            conn.Close()
            connAddtoBuffer.Close()
Else
%>

<form runat="server" id="Form">
    <TPE:Template Runat="Server" />
    <TPE:Web_Box Heading="Unsubscribe" Runat="Server" />
        <BR><BR>Please type your email address in below if you would like to unsubscribe from our mailing list.
        <BR><BR><input type=text name=Mail maxlength=100 size=30>
        <input type=submit class=tpebutton value='Unsubscribe' >
        <BR><BR><BR><BR>
    <TPE:Web_Box Footer="true" Runat="Server" />
    <TPE:Template Footer="true" Runat="Server" />
</form>
<%End IF%>
