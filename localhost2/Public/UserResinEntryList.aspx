<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page language="c#" Codebehind="UserResinEntryList.aspx.cs" AutoEventWireup="false" Inherits="localhost.Public.UserResinEntryList" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>UserResinEntryList</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TPE:TEMPLATE id="Template1" Runat="Server" PageTitle="Contact Details"></TPE:TEMPLATE>
			<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="95%" border="0">
				<TR>
					<TD class="PageHeader">
						<DIV align="right">
							<TABLE id="Table2" align="right" border="0">
								<asp:Panel id="pnDomestic" runat="server" Visible="false">
									<TBODY>
										<TR>
											<TD vAlign="top" rowSpan="2"><IMG src="/Pics/icons/icon_US_Flag.jpg"></TD>
											<TD><B><FONT size="2">U.S. Domestic Market</FONT></B>
											</TD>
										</TR>
										<TR>
											<TD align="right">
												<asp:LinkButton id="lnkInternational" runat="server">Show International</asp:LinkButton></TD>
										</TR>
								</asp:Panel>
								<asp:Panel id="pnInternational" runat="server" Visible="false">
									<TR>
										<TD vAlign="top" rowSpan="2"><IMG src="/Pics/icons/icon_Globe.gif"></TD>
										<TD><B><FONT size="2">International Market</FONT></B>
										</TD>
									</TR>
									<TR>
										<TD align="right">
											<asp:LinkButton id="lnkDomestic" runat="server">Show U.S. Market</asp:LinkButton></TD>
									</TR>
								</asp:Panel></TABLE>
						</DIV>
					</TD>
					<TD class="PageHeader"></TD>
				</TR>
				<TR>
					<TD class="PageHeader">
						<asp:label id="lblTitle" runat="server"></asp:label></TD>
					<TD class="PageHeader"></TD>
				</TR>
				<TR>
					<TD style="FONT-SIZE: 11pt">
						<asp:Label id="lblExplanationRequest" runat="server">You can manage and edit all resin requests that you place with The Plastics Exchange. All requests are good until filled or expire.</asp:Label>
						<asp:Label id="lblExplanationOffer" runat="server">You can manage and edit all resin offes that you place with The Plastics Exchange. All offers are good until filled or expire.</asp:Label></TD>
					<TD style="FONT-SIZE: 11pt"></TD>
				</TR>
				<TR>
					<TD style="FONT-SIZE: 11pt" align="right">
						<asp:Button id="btnNewTransaction" runat="server" Text="Button"></asp:Button></TD>
					<TD style="FONT-SIZE: 11pt" align="right"></TD>
				</TR>
				<TR>
					<TD>
						<asp:datagrid id="dgEntries" runat="server" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
							CellPadding="2" DataKeyField="VARID" HorizontalAlign="Center" ShowFooter="True" CssClass="DataGrid"
							ItemStyle-CssClass="DataGridRow" AlternatingItemStyle-CssClass="DataGridRow_Alternate" HeaderStyle-CssClass="DataGridHeader">
							<AlternatingItemStyle CssClass="DataGridRow_Alternate"></AlternatingItemStyle>
							<ItemStyle CssClass="DataGridRow"></ItemStyle>
							<HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
							<Columns>
								<asp:ButtonColumn Text="Cancel" CommandName="Cancel"></asp:ButtonColumn>
								<asp:ButtonColumn Text="Edit" CommandName="Edit"></asp:ButtonColumn>
								<asp:BoundColumn Visible="False" DataField="VARID" SortExpression="VARID ASC" HeaderText="ID #">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="VARCONTRACT" SortExpression="VARCONTRACT ASC" HeaderText="Product">
									<ItemStyle Wrap="False"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="VARSIZE" SortExpression="VARSIZE ASC" HeaderText="Size">
									<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="VARMELT" SortExpression="VARMELT ASC" HeaderText="Melt">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="VARDENS" SortExpression="VARDENS ASC" HeaderText="Density">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="VARADDS" SortExpression="VARADDS ASC" HeaderText="Adds">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="VARTERM" SortExpression="VARTERM ASC" HeaderText="Delivery Terms">
									<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="VARPRICE" SortExpression="VARPRICE ASC" HeaderText="Price" DataFormatString="{0:c}">
									<ItemStyle Font-Bold="True" HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="VAREXPR" SortExpression="VAREXPR ASC" HeaderText="Expiration"></asp:BoundColumn>
							</Columns>
						</asp:datagrid></TD>
					<TD></TD>
				</TR>
				</TBODY></TABLE>
		</form>
		<TPE:TEMPLATE id="Template2" Runat="Server" Footer="true"></TPE:TEMPLATE>
	</body>
</HTML>
