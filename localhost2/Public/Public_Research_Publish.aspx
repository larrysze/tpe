<%@ Page Language="c#" Codebehind="Public_Research_Publish.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.Public_Research_Publish" Title="Public Research" %>

<html>
<head>
    <link href="/styles/TPENewStyles.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript" language="JavaScript" src="/include/js/Instructions.js">
    </script>

    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
</head>
<body>
    <form runat="server">
        <asp:Table ID="Table1" runat="server" Width="780">
            <asp:TableRow ID="trPrint">
                <asp:TableCell Width="35px">&nbsp;</asp:TableCell>
                <asp:TableCell Width="35px" Text="&lt;IMG src=&quot;../pics/bullet.gif&quot;&gt;
			    "></asp:TableCell>
                <asp:TableCell ColumnSpan="4" CssClass="Content Color2" Style="font-size: 12px; text-align: left;" Width="720px">
                    <div class="LinkNormal Color2">
                        <asp:Label runat="server" ID="lblPrintLink"></asp:Label></div>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="35px">&nbsp;</asp:TableCell>
                <asp:TableCell Width="35px" Text="
				    &lt;IMG src=&quot;../pics/bullet.gif&quot;&gt;
			    "></asp:TableCell>
                <asp:TableCell Width="350px" CssClass="Content Color2" Style="font-size: 12px; text-align: left;" Text="Add an email to join our distribution list:"></asp:TableCell>
                <asp:TableCell Width="150px" Wrap="False">
                    <asp:TextBox runat="server" EnableViewState="False" Width="130px" CssClass="InputForm" ID="txtEmail" MaxLength="255"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEmail" ErrorMessage="*" ID="rfvEmail"></asp:RequiredFieldValidator>
                </asp:TableCell>
                <asp:TableCell VerticalAlign="Middle" Width="30px">
                    <asp:ImageButton runat="server" ImageUrl="/images/buttons/submit_orange.jpg" ID="btnEmail"></asp:ImageButton>
                </asp:TableCell>
                <asp:TableCell VerticalAlign="Middle" Width="220px" HorizontalAlign="left">
                    <asp:RegularExpressionValidator CssClass="Content Color3" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" ControlToValidate="txtEmail" ErrorMessage="Invalid email" ID="revEmail"></asp:RegularExpressionValidator>
                    <asp:CustomValidator CssClass="Content Color3" runat="server" ID="cvEmail" ControlToValidate="txtEmail"></asp:CustomValidator>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="35px">&nbsp;</asp:TableCell>
                <asp:TableCell Width="35px" Text="
				    &lt;IMG src=&quot;../pics/bullet.gif&quot;&gt;"></asp:TableCell>
                <asp:TableCell CssClass="Header Color2" Style="font-size: 12px; text-align: left;" Text="Past Reports"></asp:TableCell>
                <asp:TableCell>
                    <asp:DropDownList runat="server" AutoPostBack="True" CssClass="InputForm" ID="ddlReportDate" DataTextFormatString="{0:d}">
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell Width="350px" ColumnSpan="2">
                    <asp:Label runat="server" ID="lblEmail" CssClass="Content Color3"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <div class="DivTitleBar">
            <div class="Header Bold Color1">
                Market Update</div>
        </div>
        <asp:Panel runat="server" ID="pnlNewMarketUpdate">
            <div style="margin-left: 20px; margin-right: 20px;">
                <asp:Table ID="Table2" runat="server" Height="152px" Width="100%">
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">
                            <span class="LinkNormal"><a href="http://<%#ConfigurationSettings.AppSettings["DomainName"]%>" target="blank" style="float: left">
                                <asp:Image ID="imgLogo" runat="server" ImageAlign="Left" ImageUrl="~/Pics/Market_Update/tpelogo.gif" />
                                <span style="text-align: left; font-family: Arial Black; font-size: 220%; padding-left: 5;"><span style="color: Black">The</span><span style="color: Red">Plastics</span><span style="color: Black">Exchange</span><span style="color: Red">.</span><span style="color: Black">com</span></span> </a></span>
                            <br />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2" CssClass="Header">
                            <span style="font-size: 200%">Market Update</span>
                            <br />
                            <asp:Label runat="server" ID="lblDate" CssClass="Header" Style="font-size: 125%"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" ID="Table3" Width="100%" CellPadding="10" CellSpacing="10">
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2"></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="50%">
                            <span style="float: left; text-decoration: underline; font-weight: bold">Spot Floor Summary</span>&nbsp;<asp:Label runat="server" ID="lblTotalVolume" CssClass="Content Bold Color3 Underline"></asp:Label>
                            <asp:DataGrid ID="dgSpotFloorSummary" runat="server" Width="100%" CssClass="LinkNormal Content" BorderWidth="0" BackColor="Black" CellSpacing="1" CellPadding="3" AutoGenerateColumns="False" HorizontalAlign="Center" EnableViewState="False">
                                <HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
                                <ItemStyle CssClass="LinkNormal LightGray" />
                                <AlternatingItemStyle CssClass="LinkNormal DarkGray" />
                                <Columns>
                                    <asp:HyperLinkColumn DataNavigateUrlField="GRADE" ItemStyle-HorizontalAlign="Left" DataNavigateUrlFormatString="/spot/Spot_Floor.aspx?Filter={0}" DataTextField="GRADE" HeaderText="Resin">
                                        <HeaderStyle CssClass="Content Bold Color2" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:HyperLinkColumn>
                                    <asp:BoundColumn DataField="VARSIZE" HeaderText="Total lbs" DataFormatString="{0:#,##}">
                                        <HeaderStyle CssClass="Content Bold Color2"></HeaderStyle>
                                        <ItemStyle Wrap="False" CssClass="Content Color2"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="PriceLow" HeaderText="Low" DataFormatString="{0:0.##0}">
                                        <HeaderStyle CssClass="Content Bold Color2"></HeaderStyle>
                                        <ItemStyle Wrap="False" CssClass="Content Color2"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="PriceHigh" HeaderText="High" DataFormatString="{0:0.##0}">
                                        <HeaderStyle CssClass="Content Bold Color2"></HeaderStyle>
                                        <ItemStyle Wrap="False" CssClass="Content Color2"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="Price Range">
                                        <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
                                        <ItemStyle Wrap="False" CssClass="Content Color4"></ItemStyle>
                                    </asp:TemplateColumn>
                                </Columns>
                                <ItemStyle BorderColor="Black" BorderWidth="0px" />
                            </asp:DataGrid>
                        </asp:TableCell>
                        <asp:TableCell Width="50%" HorizontalAlign="left">
                            <asp:Label runat="server" ID="lblSummary" CssClass="Content TextJustify"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow CssClass="Content">
                        <asp:TableCell Width="40%" HorizontalAlign="Left">
                All transactions are for actual delivery; they are cleared through The Plastics Exchange and are <span class="Color3">totally anonymous.</span>
                <br />
                <br />
                All offers are subject to prior sale and credit approval.
                        </asp:TableCell>
                        <asp:TableCell Width="50%"></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                <span class="Content Color2 Bold Underline">Polyethylene</span>
                <br />                
                        </asp:TableCell>
                        <asp:TableCell></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                            <asp:Label runat="server" ID="lblPEIntro" CssClass="Content"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <asp:Image runat="server" ID="imgPEMonthChart" />
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Image runat="server" ID="imgPEYearChart" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <asp:Label runat="server" ID="lblPEBody" CssClass="Content TextJustify"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <span class="Content Color2 Bold Underline">Polypropylene</span>
                            <br />
                            <span class="Content Bold">Price: </span>
                            <asp:Label runat="server" ID="lblPPPrice" CssClass="Content"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                            <asp:Label runat="server" ID="lblPPIntro" CssClass="Content TextJustify"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <asp:Image runat="server" ID="imgPPMonthChart" />
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Image runat="server" ID="imgPPYearChart" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <asp:Label runat="server" ID="lblPPBody" CssClass="Content TextJustify"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                <span class="Content Color2 Bold Underline">Polystyrene</span>
                <br />                
                        </asp:TableCell>
                        <asp:TableCell></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                            <asp:Label runat="server" ID="lblPSIntro" CssClass="Content"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <asp:Image runat="server" ID="imgPSMonthChart" />
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Image runat="server" ID="imgPSYearChart" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <asp:Label runat="server" ID="lblPSBody" CssClass="Content"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlOldMarketUpdate" Visible="false">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td align="left">
                        <br />
                        <asp:Label ID="lblOldContent" CssClass="Content Color2" Style="text-align: left" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </form>
</body>
</html>
