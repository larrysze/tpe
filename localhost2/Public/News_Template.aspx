<%@ Page Language="c#" CodeBehind="News_Template.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.News_Template" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" Title="Plastic Resin News"%>
<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master"%>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphCssLink">
	<style type="text/css">
	    br
	    {
		    margin-bottom: 1.2em;
	    }
	    .titletables {
	        font-family: Verdana, Arial, Helvetica, sans-serif;
	        font-size: 12px;
	        font-style: normal;
	        line-height: normal;
	        font-weight: bold;
	        font-variant: normal;
	        color: #FF9900;
	        padding-left: 2px;
        }
        .NewsContent {
            padding-left: 5px;
            padding-right: 5px;
            text-align: left;
        }
        
        .Header {
            color: #FD9D00
        }
        .Header a:link {
	        text-decoration: none;
	        color: #FD9D00;	
        }
        .Header a:visited {
	        text-decoration: none;
	        color: #FD9D00;
        }
        .Header a:hover {
	        text-decoration: underline;
	        color: #FD9D00;
        }      

    </style>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphHeading">
    <table width='100%' border='0' cellspacing="0" cellpadding="0">
    <tr>
        <td align='left'><span class='Header Bold Color1'>News</span></td>
        <td align='right'><span class="Header Bold Color1" style="padding-right:3px"><a href='Public_News.aspx'>Back to Headlines</a></span></td>
    </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphMain">
    <div class='NewsContent'>
        <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <asp:Label CssClass="Content NewsContent LinkNormal" id="lbltest" runat="server"></asp:Label>    
            </td>
        </tr>
        </table>
    </div>
</asp:Content>
