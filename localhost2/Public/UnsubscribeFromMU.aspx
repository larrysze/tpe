<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnsubscribeFromMU.aspx.cs" Inherits="localhost.Public.UnsubscribeFromMU" MasterPageFile="~/MasterPages/Menu.Master" Title="Unsubscribe From Market Update"%>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<asp:Content ContentPlaceHolderID="cphMain" runat="server">
    <br /><br />
    <div class="Content">

    <asp:Label ID="lblEncryptEmail" runat="server" Visible="false"></asp:Label>
    
    <asp:Panel ID="pnlDelete" runat="server" Visible="false">
        <asp:Label ID="lblEmail" runat="server" Font-Bold="true"></asp:Label>
        <br />
        <asp:Label ID="lblMsg" runat="server">Are you sure you want to unsubscribe from the Market Update mailing list?</asp:Label>
        <br />
        <br />
        <asp:Button ID="btnYes" runat="server" OnClick="btnYes_Click" Text="Yes, Unsubscribe" />
        <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="No" />
    </asp:Panel>
    
    <asp:Panel ID="pnlInput" runat="server" Visible="false">
        Enter your email and click Unsubscribe to stop receiving our Market Updates
        <br /><br /><asp:TextBox ID="txtEmail" runat="server" Width="250"></asp:TextBox><br />
        <asp:Button ID="btnDelete" runat="server" OnClick="btnDelete_Click" Text="Unsubscribe" />
    </asp:Panel>
    
    <asp:Panel ID="pnlThankYou" runat="server" Visible="false">
        <asp:Label ID="lblThankYouMessage" runat="server"></asp:Label>
    </asp:Panel>
    
    </div>
    <br /><br />    
</asp:Content>
