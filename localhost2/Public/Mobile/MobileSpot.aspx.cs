using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace localhost.Public.Mobile
{
    public partial class MobileSpot : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Referrer"] != null)
            {
                Session["Referrer"] = Request.QueryString["Referrer"].ToString();
            }

            RandomizeChart();

            if (!Page.IsPostBack)
            {
                BindSpotSummary();                
            }
        }


        private void BindSpotSummary()//bind the data with the grid.
        {
            //select the data from the bboffer table,and group the record by the data stored in the OFFR_PROD field of the BBOFFER table.to calculate the count of records,the maximum price,the minimum price and the count of the weight for each group.
            string seleStr = "SELECT ";
            seleStr += "count=count(*), ";
            seleStr += "prodlink=('/Spot/Spot_Floor.aspx?Filter='), ";
            seleStr += "GRADE= (SELECT GRADE_NAME from GRADE WHERE GRADE_ID = OFFR_GRADE), ";
            seleStr += "offr_grade, ";
            seleStr += "Min=Cast(min(offr_prce) AS money) , ";//Cast(min(offr_prce) AS money)
            seleStr += "Max=Cast(max(offr_prce) AS money), ";
            seleStr += "VARSIZE=sum(OFFR_QTY*OFFR_SIZE) ";
            //seleStr+="FROM bboffer where offr_grade is not null group by offr_grade  ";
            seleStr += "FROM bboffer where offr_grade is not null and offr_port is null and OFFR_SIZE <> '38500' and OFFR_SIZE <> '44500' group by offr_grade  ";
            seleStr += "ORDER BY MAX DESC";

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString()))
            {
                //connect to the database.
                System.Data.DataSet ds = new DataSet();
                System.Data.SqlClient.SqlDataAdapter ad = new System.Data.SqlClient.SqlDataAdapter(seleStr, conn);
                ad.Fill(ds);//set the query data to dataset.

                dgSpot.DataSource = ds;//get data from the dataset.
                dgSpot.DataBind();
            }
        }


        protected void hplTotalLbs_PreRender(object sender, System.EventArgs e)
        {
            //Total lbs.
            decimal totalLbs = totalLbs = HelperFunction.totalPoundsOffers(this.Context);
            hplTotalLbs.Text = "Total Spot: " + String.Format("{0:#,###}", totalLbs) + " lbs";
            //hplTotalLbs.NavigateUrl = "/spot/Spot_Floor.aspx?Filter=-1";
        }


        private void RandomizeChart()
        {
            string seleStr = @"SELECT top 1
                                offr_grade,
                                VARSIZE=sum(OFFR_QTY*OFFR_SIZE) 
                                FROM bboffer 
                                where offr_grade is not null and offr_port is null 
                                and OFFR_SIZE <> '38500' and OFFR_SIZE <> '44500' 
                                group by offr_grade  
                                ORDER BY VARSIZE DESC";

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dr = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn, seleStr);
                dr.Read();

                lblSpotChartID.Text = dr["offr_grade"].ToString();

                LiteralImg.Text = "<IMG border='0' width='300px' name='ChartImg' width='330px' src='/Research/Charts/home/Spot_" + dr["offr_grade"].ToString() + ".png'>";                
            }

            //string contractid = Convert.ToString(TPE.Utility.DBLibrary.ExecuteScalarSQLStatement(Application["DBConn"].ToString(), "select contractid from HomePageSpotContract where spotid=" + lblSpotChartID.Text));
            //lblContractChartID.Text = contractid;
        }


        
		private void dgSummaryGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			string strGrade;

			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				e.Item.Cells[4].Text = e.Item.Cells[2].Text + "-" + e.Item.Cells[3].Text;

				strGrade = (DataBinder.Eval(e.Item.DataItem, "offr_grade")).ToString();

				e.Item.Attributes.Add("class", "hspotcontent");
				//e.Item.Attributes.Add("onclick", "window.location ='/spot/Spot_Floor.aspx?Filter=" + (DataBinder.Eval(e.Item.DataItem, "offr_grade")).ToString() + "';");
				e.Item.Attributes.Add("onmouseover", "ChartImg.src = '/Research/Charts/home/Spot_" + strGrade + ".png';this.style.backgroundColor='#FF8B00';this.style.cursor='hand';");
				e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='black';this.style.cursor='pointer';");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dgSpot.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgSummaryGrid_ItemDataBound);			
		}
		#endregion
	}
    
}
