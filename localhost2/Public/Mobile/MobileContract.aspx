<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileContract.aspx.cs" Inherits="localhost.Public.Mobile.MobileContract" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="cc1" namespace="System.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%--<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>--%>

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Resin Markets - Contract</title>
	<style type="text/css">
	
		.WhiteLink a:link {color:#FFFFFF; font-size: 10px;}
		.WhiteLink a:visited {color:#FFFFFF; font-size: 10px;}
		.WhiteLink a:hover {color:#FFFFFF; font-size: 10px;}
		.OrangeLink a:link {color:#FD9D00; font-size: 12px;}
		.OrangeLink a:visited {color:#FD9D00; font-size: 12px;}
		.OrangeLink a:hover {color:#FD9D00; font-size: 12px;}
		.RedLink a:link {color:red; font-size: 12px;}
		.RedLink a:visited {color:red; font-size: 12px;}
		.RedLink a:hover {color:red; font-size: 12px;}
		.LinkNormal { text-align: left;}
		.Content 	{
					font-family: Verdana, Arial, Helvetica, sans-serif;
					font-size: 10px;
					font-style: normal;
					line-height: normal;
					font-variant: normal;
					padding-left: 2px;
				}		
		.image 	{width:300px;}									
		
	</style> 
</head>
<body>


    <form id="form1" style="width:300px;" runat="server">  
    

		<asp:ScriptManager ID="ScriptManager1" runat="server">
		</asp:ScriptManager>		
 
		<asp:placeholder id="Placeholder1" runat="server" />
		<table cellspacing="0" cellpadding="0" width="300" align="center" style="background-color:#333333; " border="0">
			<tbody>
				<tr>
					<td class="LinkNormal OrangeLink" valign="middle" align="left" style="background:/pics/home_bar_titles_r1_c1.jpg; width:184; height:10 ">
					<strong class="LinkNormal OrangeLink"><font color="#FD9D00">Resin Markets - Contract</font></strong> </td>
					<td style="background:/pics/home_bar_titles_r1_c1.jpg"></td>
					<td style="background:/pics/home_bar_titles_r1_c1.jpg; height:14px; width:10%;" align="left"  >
					<div id="Div1" class="Header Bold Color3"></div></td>
				</tr>
			</tbody>
		</table>
		<table cellspacing="0" cellpadding="0" width="300" align="center" style="background-color:#000000" border="0">
		    <tbody>
			    <tr>
				<td style="width:300">		
				    <ContentTemplate>
				        <table cellspacing="0" cellpadding="0" width="100%" border="0">
						    <tr>
							    <td>
					                <asp:datagrid id="dgContract" runat="server" width="100%" bordercolor="Black" 
					                enableviewstate="False" horizontalalign="Center" autogeneratecolumns="False" cellpadding="0" font-size= "Small"
					                gridlines="Horizontal" borderstyle="None" backcolor="black" borderwidth="0px" forecolor="White" 
					                cssclass="LinkNormal WhiteLink">
					                <AlternatingItemStyle BorderStyle="Solid" BackColor="Black"></AlternatingItemStyle>
					                <HeaderStyle Font-Bold="true"></HeaderStyle>
					                <Columns>
						            <asp:BoundColumn DataField="cont_labl" HeaderText="Resin">
						                <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
						               <ItemStyle Wrap="False" CssClass="Content Bold Color4"></ItemStyle> 
						            </asp:BoundColumn>                      
						            <asp:BoundColumn  ItemStyle-Width="52px"></asp:BoundColumn>                                                                                                                                           
						            <asp:BoundColumn DataField="bid" HeaderText="Bid" DataFormatString="{0:0.##0}">
						                <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
						                <ItemStyle Wrap="False" CssClass="Content Bold Color4"></ItemStyle>
						            </asp:BoundColumn>
        							
						            <asp:BoundColumn DataField="OFFR" HeaderText="Offer" DataFormatString="{0:0.##0}">
						                <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
						                <ItemStyle Wrap="False" CssClass="Content Bold Color4"></ItemStyle>
						            </asp:BoundColumn>
					                </Columns>
					                <ItemStyle BorderColor="Black" BorderWidth="0px" />
					            </asp:datagrid>  
        					                            					                        					                                                                
				                </td>
			                </tr>							    						  
			            </table>		                    
				        </ContentTemplate>                                
		            </td>		        
		        </tr>
		        <tr>
		            <td class="image">
		                <asp:literal id="LiteralImg1" runat="server"></asp:literal> 
		            </td>
		        </tr>
		    </tbody>
		</table>

        <div id="flag" style=" width:300px;visibility: hidden; display: none">
        <asp:Label runat="server" Visible="true" ID="lblContractChartID"/>
                <asp:Label runat="server" Visible="true" ID="lblSpotChartID"/>        

        </div>
        <div id="flag2" style="visibility: hidden; display: none">
        1</div>
		 	   
    </form>
</body>
</html>