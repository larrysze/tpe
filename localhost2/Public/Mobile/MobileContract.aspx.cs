using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;

namespace localhost.Public.Mobile
{
    public partial class MobileContract : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // flags the user if they were refered by a partner
            if (Request.QueryString["Referrer"] != null)
            {
                Session["Referrer"] = Request.QueryString["Referrer"].ToString();
            }

            RandomizeChart();

            if (!Page.IsPostBack)
            {                
                BindContract();
            }
        }

        private void BindContract()
        {
            int fwdMonth = GetTPEForwardMonth(DateTime.Today.Month, DateTime.Today.Year.ToString());

            string seleStr = @"Select 
                    CONT_LABL= (SELECT GRADE_NAME from GRADE WHERE GRADE_ID = (select spotid from homepagespotcontract where contractid=CONT_ID)), 
                    CONT_ID,
                    QTYBID =(SELECT SUM(BID_QTY) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='{0}' AND BID_SIZE='1'),
                    BID = (SELECT MAX(BID_PRCE) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='{1}' AND BID_SIZE='1'),
                    OFFR = (SELECT MAX(OFFR_PRCE) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='{2}' AND OFFR_SIZE='1'), 
                    QTYOFFR = (SELECT SUM(OFFR_QTY) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='{3}' AND OFFR_SIZE='1'),
                    VARSIZE =(SELECT SUM(OFFR_QTY*OFFR_SIZE) FROM bboffer where offr_grade=(select spotid from homepagespotcontract where contractid=CONT_ID)) 
                    From CONTRACT WHERE CONT_CATG <>'Monomer' AND CONT_ENABLED='True'
                    ORDER BY OFFR DESC";

            seleStr = string.Format(seleStr, fwdMonth, fwdMonth, fwdMonth, fwdMonth);

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString()))
            {
                //connect to the database.
                System.Data.DataSet ds = new DataSet();
                System.Data.SqlClient.SqlDataAdapter ad = new System.Data.SqlClient.SqlDataAdapter(seleStr, conn);
                ad.Fill(ds);//set the query data to dataset.

                dgContract.DataSource = ds;//get data from the dataset.
                dgContract.DataBind();
            }
        }

        public int GetTPEForwardMonth(int sMonth, string sYear)
        {
            Hashtable htParam = new Hashtable();
            htParam.Add("@month", GetMonth(sMonth));
            htParam.Add("@year", sYear);

            string sSQL = "SELECT FWD_ID FROM FWDMONTH WHERE FWD_MNTH=@MONTH AND FWD_YEAR=@year";

            int forwardID = int.Parse(DBLibrary.ExecuteScalarSQLStatement(HttpContext.Current.Application["DBconn"].ToString(), sSQL, htParam).ToString());

            return forwardID;
        }

        public string GetMonth(int month)
        {
            string sMonthName = "";

            switch (month)
            {
                case 1:
                    sMonthName = "January";
                    break;
                case 2:
                    sMonthName = "February";
                    break;
                case 3:
                    sMonthName = "March";
                    break;
                case 4:
                    sMonthName = "April";
                    break;
                case 5:
                    sMonthName = "May";
                    break;
                case 6:
                    sMonthName = "June";
                    break;
                case 7:
                    sMonthName = "July";
                    break;
                case 8:
                    sMonthName = "August";
                    break;
                case 9:
                    sMonthName = "September";
                    break;
                case 10:
                    sMonthName = "October";
                    break;
                case 11:
                    sMonthName = "November";
                    break;
                case 12:
                    sMonthName = "December";
                    break;

            }

            return sMonthName;
        }


        private void RandomizeChart()
        {
            string seleStr = @"SELECT top 1
                                offr_grade,
                                VARSIZE=sum(OFFR_QTY*OFFR_SIZE) 
                                FROM bboffer 
                                where offr_grade is not null and offr_port is null 
                                and OFFR_SIZE <> '38500' and OFFR_SIZE <> '44500' 
                                group by offr_grade  
                                ORDER BY VARSIZE DESC";

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dr = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn, seleStr);
                dr.Read();

                lblSpotChartID.Text = dr["offr_grade"].ToString();

                //LiteralImg.Text = "<IMG border='0' name='ChartImg' width='330px' src='/Research/Charts/home/Spot_" + dr["offr_grade"].ToString() + ".png'>";
                LiteralImg1.Text = "<IMG border='0' name='ChartImg1' width='300px' src='/Research/Charts/home/Contract_9.png'>";
            }

            string contractid = Convert.ToString(TPE.Utility.DBLibrary.ExecuteScalarSQLStatement(Application["DBConn"].ToString(), "select contractid from HomePageSpotContract where spotid=" + lblSpotChartID.Text));
            lblContractChartID.Text = contractid;
        }

        protected void dgContract_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            string strGrade;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                strGrade = (DataBinder.Eval(e.Item.DataItem, "cont_id")).ToString();

                e.Item.Attributes.Add("class", "hspotcontent");
                //e.Item.Attributes.Add("onclick", "window.location ='/spot/Spot_Floor.aspx?Filter=" + (DataBinder.Eval(e.Item.DataItem, "cont_id")).ToString() + "';");
                e.Item.Attributes.Add("onmouseover", "ChartImg1.src = '/Research/Charts/home/Contract_" + strGrade + ".png';this.style.backgroundColor='#FE0002';this.style.cursor='hand';");
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='black';this.style.cursor='pointer';");
            }

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {            
            this.dgContract.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgContract_ItemDataBound);
        }
        #endregion


    }
}
