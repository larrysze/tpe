using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Public.Mobile
{
    public partial class MU : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Referrer"] != null)
            {
                Session["Referrer"] = Request.QueryString["Referrer"].ToString();
            }

            if (!Page.IsPostBack)
            {
                LoadMarketUpdatesy();
            }
        }

        private void LoadMarketUpdatesy()
        {
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString()))
            {
                conn.Open();
                string sSQL = "select top 1 comments from weekly_reviews order by review_date desc";

                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, sSQL);

                while (dtr.Read())
                {
                    lblMU.Text = dtr["comments"].ToString();                    
                }

            }
        }
    }
}
