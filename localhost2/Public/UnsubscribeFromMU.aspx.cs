using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Mail;
using TPE.Utility;

namespace localhost.Public
{
    public partial class UnsubscribeFromMU : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)  
            {
                // NOTE: ID is encrypted email address!!
                if (Request["ID"] != null)
                {

                    string crypt_email = Request["ID"];
                    string decrypt_email = Crypto.Decrypt(crypt_email);
                    lblEmail.Text = decrypt_email;

                    if (!isInTheList(decrypt_email))
                    {
                        pnlDelete.Visible = false;
                        pnlInput.Visible = false;
                        pnlThankYou.Visible = true;

                        lblThankYouMessage.Text = "Sorry, we could not find this email in our List.";

                    }
                    else
                    {
                        pnlDelete.Visible = true;
                        pnlInput.Visible = false;
                        pnlThankYou.Visible = false;

                        lblEncryptEmail.Text = crypt_email;

                    }

                }
                else
                {
                    btnDelete.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to unsubscribe?');");
                    pnlDelete.Visible = false;
                    pnlInput.Visible = true;
                    pnlThankYou.Visible = false;
                }
            }
        }

        private bool isInTheList(string email)
        {
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                Hashtable param = new Hashtable();
                param.Add("@EMAIL", email);
                SqlDataReader dtrHave = DBLibrary.GetDataReaderFromSelect(conn, "SELECT * FROM MAILING WHERE MAI_MAIL=@EMAIL", param);
                return dtrHave.Read();

            } 
        }
        protected void insertToDashboard(string email)
        {
            Hashtable htPar = new Hashtable();
            htPar.Add("@IP", Request.UserHostAddress.ToString());
            htPar.Add("@ID_TYPE", "9");
            htPar.Add("@DATE_TIME", DateTime.Now.ToString());
            htPar.Add("@EMAIL", email);
            

            string strSql = "INSERT DASHBOARD (ID_TYPE, DATE_TIME, IP, EMAIL) VALUES (@ID_TYPE, @DATE_TIME, @IP, @EMAIL)";

            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSql, htPar);


        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            pnlDelete.Visible = false;
            pnlInput.Visible = false;
            pnlThankYou.Visible = true;
            //delete email and show confirmation message
            string email = "";
            if (lblEncryptEmail.Text.Length > 0)
            {
                email = Crypto.Decrypt(lblEncryptEmail.Text);
                Hashtable param = new Hashtable();
                param.Add("@EMAIL", email);
                DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), "UPDATE MAILING SET MAI_ENABLE='False' WHERE MAI_MAIL=@EMAIL", param);
                insertToDashboard(email);
            }

            lblThankYouMessage.Text = "Successfully unsubscribed " + email;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlDelete.Visible = false;
            pnlInput.Visible = false;
            pnlThankYou.Visible = true;

            lblThankYouMessage.Text = "Unsubscribe cancelled.";
            //show confirmation message


        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
		pnlDelete.Visible = false;
		pnlInput.Visible = false;

		string unsubscribe_url = "http://" + ConfigurationSettings.AppSettings["DomainName"] + "/Public/UnsubscribeFromMU.aspx";

		MailMessage mail;
		mail = new MailMessage();
		mail.From = "DevelopmentTeam@theplasticexchange.com";
		mail.To = txtEmail.Text;
		mail.Subject = "The Plastics Exchange unsubcribe confirmation message";
		mail.Body = "<br /><b>" + HelperFunction.getEmailHeaderHTML() + "</b><br />";
		mail.Body += "This is a confirmation message for unsubscribing from Plastic Exchange Market Update.";
		mail.Body += "<br />";
		string link = "If you want to unsubscribe from Market Update mailing list, please <a href=\"" + unsubscribe_url + "?ID=" + Crypto.Encrypt(mail.To) + "\">click here</a>";
		mail.Body += link;
		mail.Body += "<br /><br />" + HelperFunction.getEmailFooterHTML();

		mail.BodyFormat = MailFormat.Html;
		try
		{
			TPE.Utility.EmailLibrary.SendWithinThread(mail);
		}
		catch( Exception sendingError )
		{
		}

		pnlThankYou.Visible = true;
		lblThankYouMessage.Text = "\n Unsubscribe Confirmation email is sent to <font color=\"red\">" + txtEmail.Text + " </ font><br/>";
		lblThankYouMessage.Text += "<br/><font color=\"black\">Please check the mail and confirm this request. </ font>";

        }
    }
}
