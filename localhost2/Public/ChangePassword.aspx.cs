using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Text.RegularExpressions; 

namespace localhost.Public
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Typ"] == "")
            {
                Session["strLoginURL"] = Request.RawUrl.ToString();
                Response.Redirect("/common/FUNCLogin.aspx");
            }
            if (!IsPostBack)
            {
                Build_User_Table();
                
            }
            Build_Resin_Table();
        }
        private void Build_User_Table()
        {
            string PersonId = Session["ID"].ToString();
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                Hashtable htParams = new Hashtable();
                htParams.Add("@persID", PersonId);
                SqlDataReader dtr1 = DBLibrary.GetDataReaderFromSelect(conn, "SELECT PERS_FRST_NAME, PERS_LAST_NAME, PERS_TITL, COMP_NAME, PERS_PHON, PERS_MAIL, PERS_PREF  FROM PERSON WHERE PERS_ID=@persID", htParams);
                while (dtr1.Read())
                {
                    txtFirstName.Text = dtr1["PERS_FRST_NAME"].ToString();
                    txtLastName.Text = dtr1["PERS_LAST_NAME"].ToString();
                    txtTitle.Text = dtr1["PERS_TITL"].ToString();
                    txtCompany.Text = dtr1["COMP_NAME"].ToString();
                    txtPhone.Text = dtr1["PERS_PHON"].ToString();
                    txtEmail.Text = dtr1["PERS_MAIL"].ToString();
                    ViewState["Pref"] = dtr1["PERS_PREF"].ToString();
                }
                dtr1.Close();
            }

         //   Build_Resin_Table();
           
        }

        private void Build_Resin_Table()
        {
            ArrayList ALResin = new ArrayList();

            SqlConnection conn;
            conn = new SqlConnection(Application["DBconn"].ToString());
            conn.Open();


            // defins the cmdResin before dtrPerson 
            SqlCommand cmdResin = new SqlCommand("SELECT CONT_LABL,CONT_ID, CONT_GRADE FROM CONTRACT WHERE POWER(2,CONT_ID-1)&(" + ViewState["Pref"].ToString() + ")<>0 ORDER BY CONT_ORDR", conn);

            // binding resin list repeater control
            SqlDataReader dtrResin;

            dtrResin = cmdResin.ExecuteReader();
            while (dtrResin.Read())
            {
                ALResin.Add(dtrResin["CONT_ID"].ToString());

            }

            dtrResin.Close();

            ALResin.TrimToSize();

            TableRow tr;

            TableCell td1;
            TableCell td2;

            cmdResin = new SqlCommand("SELECT CONT_LABL,CONT_ID, CONT_GRADE FROM CONTRACT  ORDER BY CONT_ORDR", conn);
            dtrResin = cmdResin.ExecuteReader();
            while (dtrResin.Read())
            {
                tr = new TableRow();
                td1 = new TableCell();
                td1.Wrap = false;
                CheckBox cb = new CheckBox();
                cb.ID = "cbResin_" + dtrResin["CONT_ID"].ToString().Trim();
                if (ALResin.IndexOf(dtrResin["CONT_ID"].ToString().Trim()) >= 0)
                {
                    cb.Checked = true;
                }
                //cb.
                td1.Controls.Add(cb);
                td2 = new TableCell();
                td2.Text = dtrResin["CONT_LABL"].ToString().Trim();
                td2.Wrap = false;


                tr.Controls.Add(td1);
                tr.Controls.Add(td2);
                tblDynamic.Rows.Add(tr);
            }
            // close datareaders
            dtrResin.Close();
            conn.Close();

        }

        protected void btnChange_Click(object sender, EventArgs e)
        {
            string pwd="";
            
            string PersonId = Session["ID"].ToString();
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                Hashtable htParams = new Hashtable();
                htParams.Add("@persID", PersonId);
                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, "SELECT PERS_PSWD FROM PERSON WHERE PERS_ID=@persID", htParams);
                while (dtr.Read())
                {
                    pwd = dtr["PERS_PSWD"].ToString();
                }
                dtr.Close();

                if (Crypto.Decrypt(pwd) == txtOldPwd.Text)
                {
                    if (txtNewPwd.Text == txtConfirmPwd.Text)
                    {
                        htParams.Add("@password", Crypto.Encrypt(txtNewPwd.Text));
                                               //DBLibrary.ExecuteSQLStatement(conn, "UPDATE PERSON SET PERS_PSWD=@newPSWD WHERE PERS_ID=@persID", htParams);
                        SqlDataReader dtr2 = DBLibrary.GetDataReaderFromSelect(conn, "UPDATE PERSON SET PERS_PSWD=@password WHERE PERS_ID=@persID", htParams);
                        lblError.Text = "<br />Your password has been changed";
                        lblError.ForeColor = System.Drawing.Color.Black;
                        lblError.Visible = true;
                        lblOldPwd.Visible = false;
                        lblNewPwd.Visible = false;
                        lblConfirmPwd.Visible = false;
                        txtConfirmPwd.Visible = false;
                        txtNewPwd.Visible = false;
                        txtOldPwd.Visible = false;
                        btnChange.Visible = false;
                       
                    }
                    else
                    {
                        lblError.Text = "New password and confirmation do not match";
                        lblError.Visible = true;
                    }
                }
                else
                {
                    lblError.Text = "Please type correct old password";
                    lblError.Visible = true;
                }
            }
        }

        protected void btnChange1_Click(object sender, EventArgs e)
        {

            string PersonId = Session["ID"].ToString();
            string sql = "UPDATE Person set PERS_FRST_NAME = @first_name, PERS_LAST_NAME = @last_name, PERS_TITL=@title, COMP_NAME=@company_name, PERS_PHON=@phone, PERS_MAIL=@email where PERS_ID=@persID;";
            Hashtable param = new Hashtable();
            param.Add("@persID", PersonId);
            param.Add("@first_name", txtFirstName.Text);
            param.Add("@last_name", txtLastName.Text);
            param.Add("@title", txtTitle.Text);
            param.Add("@company_name", txtCompany.Text);
            param.Add("@phone", txtPhone.Text);
            param.Add("@email", txtEmail.Text);
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sql, param);

            lblError1.Text = "<br />Your user information has been changed";
            lblError1.ForeColor = System.Drawing.Color.Black;
            lblError1.Visible = true;

            Build_User_Table();
        }

        protected void btnChange2_Click(object sender, EventArgs e)
        {
            string PersonId = Session["ID"].ToString();
            Hashtable param = new Hashtable();
            param.Add("@persID", PersonId);

            InterateControls(this); // iterate through cb controls to determine the resin preferences.  stored as iPref
            // add Person Status
            string strSQL = "UPDATE PERSON set PERS_PREF='" + iPref.ToString() + "' WHERE PERS_ID = @persID";
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSQL, param);

            lblError2.Text = "<br />Your user preferences has been changed <br />";
            lblError2.ForeColor = System.Drawing.Color.Black;
            lblError2.Visible = true;

            //Build_Resin_Table();
        }

        //int iPref = 0;
        int iPref;
        void InterateControls(Control parent)
        {
            
            foreach (Control child in parent.Controls)
            {
                if (child.GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox") && child.ID.IndexOf("cbResin_") == 0)
                {
                    CheckBox checkbox = (CheckBox)child;
                    if (checkbox.Checked)
                    {
                        string[] arrCheckBoxID;
                        Regex r = new Regex("_"); // Split apart id to reveal the needed values.
                        arrCheckBoxID = r.Split(child.ID.ToString());
                        iPref += (int)Math.Pow(2.0, (Convert.ToInt32(arrCheckBoxID[1]) - 1)); // checked box found! add to total

                    }
                }

                if (child.Controls.Count > 0)
                {
                    InterateControls(child);
                }
            }
        }
    }
}
