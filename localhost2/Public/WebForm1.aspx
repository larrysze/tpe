<%@ Page language="c#" Codebehind="WebForm1.aspx.cs" AutoEventWireup="false" Inherits="localhost.Public.WebForm1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>WebForm1</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="/include/master.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:DataGrid id="DataGrid1" runat="server" CellPadding="4" CssClass="DataGrid2" ItemStyle-CssClass="DataGridRow2"
				AlternatingItemStyle-CssClass="DataGridRow_Alternate2" Width="192px" BorderColor="Black" AutoGenerateColumns="False">
				<SelectedItemStyle Wrap="False"></SelectedItemStyle>
				<EditItemStyle Wrap="False"></EditItemStyle>
				<AlternatingItemStyle Wrap="False" CssClass="DataGridRow_Alternate2"></AlternatingItemStyle>
				<ItemStyle Wrap="False" CssClass="DataGridRow2"></ItemStyle>
				<HeaderStyle Font-Bold="True" Wrap="False" HorizontalAlign="Center" ForeColor="Black" BackColor="White"></HeaderStyle>
				<FooterStyle Wrap="False"></FooterStyle>
				<Columns>
					<asp:TemplateColumn HeaderText="Order">
						<HeaderStyle Font-Bold="True"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label id="Label1" runat="server">Order#:</asp:Label>
							<asp:Label id="lblOrder" runat="server" Font-Bold="True"></asp:Label><BR>
							<asp:Label id="Label2" runat="server">PO#:</asp:Label>
							<asp:Label id="lblPO" runat="server" Font-Bold="True"></asp:Label><BR>
							<asp:Label id="Label3" runat="server">Date:</asp:Label>
							<asp:Label id="lblDate" runat="server" Font-Bold="True"></asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Product">
						<HeaderStyle Font-Bold="True"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label id="lblProduct" runat="server" Font-Bold="True"></asp:Label><BR>
							<asp:Label id="Label5" runat="server">Size:</asp:Label>
							<asp:Label id="lblSize" runat="server" Font-Bold="True"></asp:Label><BR>
							<asp:Label id="Label7" runat="server">Weight:</asp:Label>
							<asp:Label id="lblWeight" runat="server" Font-Bold="True"></asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Specifications">
						<HeaderStyle Font-Bold="True"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label id="Label4" runat="server">Melt:</asp:Label>
							<asp:Label id="lblMelt" runat="server" Font-Bold="True"></asp:Label><BR>
							<asp:Label id="Label8" runat="server">Density:</asp:Label>
							<asp:Label id="lblDensity" runat="server" Font-Bold="True"></asp:Label><BR>
							<asp:Label id="Label10" runat="server">Additives:</asp:Label>
							<asp:Label id="lblAdditives" runat="server" Font-Bold="True"></asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Price">
						<HeaderStyle Font-Bold="True"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label id="lblPrice" runat="server"></asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Ship Date">
						<HeaderStyle Font-Bold="True" Wrap="False"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label id="lblShipDate" runat="server"></asp:Label><BR>
							<asp:ImageButton id="ImageButton1" runat="server" Visible="False"></asp:ImageButton>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn>
						<HeaderStyle Font-Bold="True"></HeaderStyle>
						<ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
						<ItemTemplate>
							<asp:Label id="Label6" runat="server" Font-Bold="True">Amount Due:</asp:Label>
							<asp:Label id="lblAmountDue" runat="server"></asp:Label><BR>
							<asp:Label id="Label11" runat="server" Font-Bold="True">Paid:</asp:Label>
							<asp:Label id="lblPaid" runat="server"></asp:Label><BR>
							<asp:Image id="Image1" runat="server" Width="70px" ImageUrl="../images/line.JPG" Height="1px"></asp:Image><BR>
							<asp:Label id="Label13" runat="server" Font-Bold="True">Balance:</asp:Label>
							<asp:Label id="lblBalance" runat="server"></asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Due Date">
						<HeaderStyle Font-Bold="True" Wrap="False"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label id="lblDueDate" runat="server"></asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn>
						<HeaderStyle Font-Bold="True"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Button id="btnDocument" runat="server" Text="Invoice"></asp:Button>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle Wrap="False"></PagerStyle>
			</asp:DataGrid>
		</form>
	</body>
</HTML>
