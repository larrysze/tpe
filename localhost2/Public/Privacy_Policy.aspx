<%@ Page Language="c#" CodeBehind="Privacy_Policy.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.Privacy_Policy" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" Title="The Plastics Exchange: Commodity Plastic Resin, Buy Resin, Sell Resin, Plastic Materials"%>
<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
<style type="text/css">
.Justify {text-align:justify;}
.Left {text-align:left;}
</style>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphHeading"><span class="Header Bold Color1">Privacy and security statement</span></asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">		
	<table width="771" border="0" align="center" cellpadding="0" cellspacing="10">
		<tr>
			<td class="Content Justify">
				Your information privacy is as important to us as it is to you. The Plastics 
				Exchange has implemented this privacy statement to express our commitment to 
				privacy. In this policy, we illustrate specifically how The Plastics Exchange 
				protects and respects your privacy and why you can trust us. In this policy we 
				detail procedures for gathering and disseminating information through our 
				website.<br>
				<br>
				From time to time we may make changes to our privacy policy. These changes will 
				be posted on this page so that you are always aware of the information we 
				collect, how we use it, and under what circumstances we disclose it.
				
			</td>
		</tr>
	</table>
	
	<div class="DivTitleBar"><span class="Header Bold Color1" style="text-align:left; vertical-align:middle;">Our Commitment to Privacy</span></div>
	
	<table width="771" border="0" align="center" cellpadding="0" cellspacing="10">
		<tr>
			<td class="Content Color2 Left">	
	
				<b>Types of information we collect</b>
				
				    <li>
				    Company information
				    <li>
				    Financial information (including banking information)
				    <li>
					Shipping/receiving information
					<br>
					<br>
	
					<span class="Content Bold Color2 Left">Company information</span><br>
					When you register to become a member we must know the business name, address, 
					telephone and fax number(s), email address and other such information such as 
					federal and state tax I.D. number, company organization (i.e. partnership, 
					corporation etc). This information is used to complete your transaction, 
					administer membership, billing and other clerical functions. We do not share 
					this information with outside parties except to the extent necessary to 
					complete your transaction.<br>
					<br>
					Company information can /include personally identifiable information, 
					specifically names and job titles/functions. This information is needed to 
					administer the exchange, as we need to know the users of the plastic exchange 
					system. We do not share personal information with any other third parties 
					except when required by law.<br>
					<br>
					<span class="Content Bold Color2 Left">Financial information /Bank information</span><br>
					Refers to banking and other such financial information such as account numbers 
					and references supplied to the exchange. This information is used solely for 
					the purpose of receiving a line of credit as required by the exchange. No 
					financial information is ever passed on to anyone other than our credit 
					partners as required under warranted confidentiality agreements<br>
					<br>
					<span class="Content Bold Color2 Left">Shipping and receiving information</span><br>
					Refers to shipping and receiving addresses and instructions. The exchange uses 
					the information solely to distribute product bought and sold on the exchange. 
					Under agreement, our shipping partner(s) will not disclose contra-shipping 
					information; this is to keep the exchange anonymous.<br>
					<br>
					The Plastics Exchange will never sell, rent loan or giveaway the information we 
					gather on our members to any third party including marketers unless required by 
					contract or law (see below). We never share any information with other exchange 
					members. We never use information gathered for our own sales and marketing 
					efforts.<br>
					<br>
					<span class="Content Bold Color2 Left">Who we share information with</span><br>
					The Plastics Exchange shares information with companies that work with us. 
					Under confidentiality agreements, The Plastics Exchange has partnered with 
					several companies to integrate functions such as credit and shipping. All of 
					these companies work together to make the exchange a robust and integrated 
					experience. We only share the information necessary. For example our shipping 
					partner would need shipping information but would not be entitle to banking 
					information<br>
					<br>
					Information may be exchanged with credit bureaus and other similar 
					organizations in connection with credit approval.<br>
					<br>
					If The Plastics Exchange receives a subpoena or similar legal process demanding 
					release of any information about an exchange member, we will generally attempt 
					to notify the party concerned (unless we believe we are prohibited from doing 
					so)<br>
					<br>
					Except as required by law or as described above The Plastics Exchange does not 
					share information with other parties including government agencies.<br>
					<br></li>
			</td>
		</tr>
	</table>
	<div class="DivTitleBar"><span class="Header Bold Color1" style="text-align:left">Other Privacy Matters</span></div>

	<table width="771" border="0" align="center" cellpadding="0" cellspacing="10" class="tableContent">
		<tr>
			<td class="Content Color2 Justify">	
				We may use your IP address to help diagnose problems with our server and to 
				administer our site.<br>
				<br>
				A cookie is a small amount of data that is sent to your browser from a web 
				server and stored on your computers hard drive. We do not use cookies.<br>
				<br>
				This site may contain links to other sites and The Plastics Exchange makes all 
				reasonable efforts to only link to sites that share our high standards and 
				respect for privacy. However, we are not responsible for the content or the 
				privacy practices used by other sites.<br>
				<br>
				<span class="Content Bold Color2 Left">Online applications</span><br>
				When applying for exchange membership online it is done in strict confidence in 
				a secure session established with Secure Socket Layer (SSL see below).<br>
				<br>
				After an application has been submitted online, we recommend that a prospective 
				member end his or her browser session before leaving the computer.<br>
				<br>
				We use the information provided in the account application to grant membership 
				to the exchange. Membership to the exchange requires a line of credit as 
				approved by the exchange credit partners.<br>
				<br>
			</td>
		</tr>	
	</table>
	<div class="DivTitleBar"><span class="Header Bold Color1" style="text-align:left">Our Commitment to Security</span></div>
	<table width="771" border="0" align="center" cellpadding="0" cellspacing="10" class="tableContent">		
		<tr>
			<td class="Content Color2 Justify">
				<span class="Content Bold Color2 Left">Security</span><br>
				The Plastics Exchange has implemented security measures to protect its members 
				information from loss, misuse and alteration. We have taken steps to prevent 
				unauthorized access, maintain data accuracy and sure correct use of 
				information. These steps /include certain physical, electronic and managerial 
				procedures to safeguard and secure the information The Plastics Exchange 
				possesses. In order to increase safety from interception or theft over the 
				Internet, our secure server software uses a state of the art 128 secure socket 
				layer (SSL) encryption. This technology encrypts- or scrambles- your 
				information so it is virtually impossible for anyone other than The Plastics 
				Exchange to read it. (See VeriSign certificate) We use industry standard 
				firewall and password protection systems. An internal team composed of both 
				computer professionals as well as senior management periodically reviews our 
				security system and policy. We constantly upgrade our security systems to 
				ensure that we use the best and most current technology to provide a secure 
				environment for the exchange members data.<br>
				<br>
				<span class="Content Bold Color2 Left">Password</span><br>
				The commerce section (Trading Floor) is located in a password protected area of 
				our website. Members are supplied with a password that they have chosen when 
				they apply for membership. We recommend that members never share their password 
				with anyone. If members share a computer we suggest that they sign out of The 
				Plastics Exchange before another person uses their computer.<br>
				<br>
				Plastics Exchange employees will never ask members for their password. We 
				suggest that members periodically change their password. In the event a member 
				forgets their password, members should call Membership Services (<%=ConfigurationSettings.AppSettings["PhoneNumber"].ToString()%>). 
				A systems administrator will generate an email to the email address on file 
				with the password.<br>
				At no point in that process does any Plastics Exchange employee see your 
				password.<br>
				Ultimately, however the exchange members are solely responsible for maintaining 
				the secrecy of passwords and/or account information. Please be careful and 
				responsible whenever you are online.<br>
				<br>
				An exchange member usually is a representative for a company. An exchange 
				member can be one person or several. You should know: ALL transactions 
				initiated by an exchange member using information the member provides are 
				considered authorized by that exchange member, whether or not they were aware 
				of the specific transaction. You may revoke this authority only by notifying 
				the plastic exchange, and we may need to block your account until we reissue 
				new access codes.<br>
				<br>
				<span class="Content Bold Color2 Left">How to contact us</span><br>
				By Mail:<br>
				<br>
				The Plastics Exchange<br>
				16510 N. 92nd Street. #1010<br>
				Scottsdale, AZ 85260<br>
				<br>
				By phone:<br>
				<%=ConfigurationSettings.AppSettings["PhoneNumber"].ToString() %><br>
				<br>
				By Fax:<br>
				<%=ConfigurationSettings.AppSettings["FaxNumber"].ToString() %><br>
				<br>
				<span class="LinkNormal">By Email:<br>
				<a href="mailto:info@ThePlasticsExchange.com">info@ThePlasticsExchange.com</a></span>
			</td>
		</tr>
	</table>
	<br>
</asp:Content>