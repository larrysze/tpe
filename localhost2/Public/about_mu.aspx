<%@ Page Language="C#" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" AutoEventWireup="true" CodeBehind="about_mu.aspx.cs" Inherits="localhost.Public.about_mu" Title="Market Update" %>


	
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphCssLink">
	<style type="text/css">
	    br
	    {
		    margin-bottom: 1.2em;
	    }
	    .titletables {
	        font-family: Verdana, Arial, Helvetica, sans-serif;
	        font-size: 12px;
	        font-style: normal;
	        line-height: normal;
	        font-weight: bold;
	        font-variant: normal;
	        color: #FF9900;
	        padding-left: 2px;
        }
        .NewsContent {
            padding-left: 5px;
            padding-right: 5px;
            text-align: left;
        }
        
        .Header {
            color: #FD9D00
        }
        .Header a:link {
	        text-decoration: none;
	        color: #FD9D00;	
        }
        .Header a:visited {
	        text-decoration: none;
	        color: #FD9D00;
        }
        .Header a:hover {
	        text-decoration: underline;
	        color: #FD9D00;
        }      

    </style>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphHeading">
       <table width='100%' border='0' cellspacing="0" cellpadding="0">
    <tr>
        <td align='left'><span class='Header Bold Color1'>Market Update</span></td>        
    </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphMain">
   
   
	<div style="margin:6px;">
		<p style="text-align: justify" class="Content">
	
	    Dear Market Update Reader,
	    <br />
	    <br />
	    Unprecedented volatility in the energy and petrochemical markets has created a need for better and timelier information. 
	    Rising fuel and monomer costs have become the primary driver of resin price increases that have a great effect on every business within 
	    our plastics industry. Historically, access to timely monomer pricing has been challenging and prohibitively expensive for the industry en masse.
	    <br />
	    <br />
	    After 8 years, hundreds of our Market Updates, and reaching a subscriber base of nearly 20,000, I am excited that 
	    the need for our market information, coupled with the movement to integrate the petrochemical supply chain, has grown beyond the scope of 
	    just our team at The Plastics Exchange.  
	    <br />
	    <br />
	    The Plastics Exchange partnered with Lozier Energy, the pioneer of NGL brokerage and leading US Olefins broker, to create 
	    <a href="http://www.petrochemwire.com">The PetroChem Wire</a> (PCW), a daily publication quoting active spot markets with commentary for NGLs (Ethane and Propane, etc.), 
	    Olefins (Ethylene and Propylene) and Polyolefins (Polyethylene and Polypropylene). <b>As of July, 2008, The Plastics Exchange is 
	    proud to support the PetroChem Wire with our exclusive daily pricing and market commentary.  Click <a target="_blank" href="http://www.theplasticsexchange.com//Research/Market_Update/pdf/PCW-Weekly-8-8-2008.pdf">here</a> to view a recent issue.</b>
	    <br />
	    <br />
	    Written and managed by <a href="mailto:kathy@petrochemwire.com">Kathy Hall</a> (720.480.6288) with <a href="mailto:mark@petrochemwire.com">Mark Quiner</a> (713.331.0464), The PetroChem Wire has quickly become a 
	    trusted source of Petrochemical Market intelligence; every North American producer subscribes to the Wire, and some now utilize 
	    the PCW's indices as mechanisms to settle some of their contracts. All market participants should be comfortable reporting in 
	    confidence, their spot deals and contract negotiations to these editors, to enhance a full market-wide perspective.
	    <br />
	    <br />
	    With energy and feedstock costs now such a major component of resin prices, spot markets, identification of production margins 
	    and subtle swings in sentiment are critical bits of information that can be used to predict market direction, better manage 
	    inventory levels and provide fodder for conversation with suppliers / customers, especially during price negotiations.
	    <br />
	    <br />
	    <b>For just $1800/year, less than $.01/lb on a single railcar of resin, you can receive 250 daily and 52 weekly issues of 
	    <a href="http://www.petrochemwire.com/registration.aspx">The PetroChem Wire (click to subscribe)</a>.</b> Weekly only subscribers pay just $1200/year. Multi-user / corporate subscriptions are available.
	    <br />
	    <br />
	    We will continue to provide the industry with free access to our liquid spot resin markets, historic price charts and daily industry 
	    news on <a href="http://www.theplasticsexchange.com">ThePlasticsExchange.com</a>. As always our trading desk is staffed to scour the market for your spot resin needs! Just let us know 
	    what you need and we will chase it down for you (minimum of 1 truckload of commodity grade resin).
	    <br />
	    <br />
	    We welcome your feedback and thank you for your attention and patronage.
	    <br />
	    <br />
	    Sincerely,
	    <br />
	    <br />
	    Michael Greenberg, CEO<br />
        <a href="http://www.theplasticsexchange.com">The Plastics Exchange</a><br />
        312.202.0002
	</p>
	</div>
   
   
</asp:Content>
