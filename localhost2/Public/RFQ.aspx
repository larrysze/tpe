<%@ Page Language="c#" CodeBehind="RFQ.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.RFQ" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<form runat="server" id="Form">
	<TPE:Template PageTitle="Request for Quote" Runat="Server" />
	<TPE:Web_Box Heading="Thank you " Runat="Server" />
	<b>Thank you for your request!</b>
	<br>
	<br>
	We will respond to you ASAP.
	<BR>
	<BR>
	<a href="/default.aspx"><font size="3">Back to homepage!</font></a>
	<TPE:Web_Box Footer="true" Runat="Server" />
	<TPE:Template Footer="true" Runat="Server" />
</form>
</body>
