<%@ Page Language="c#" CodeBehind="Public_News.aspx.cs" AutoEventWireup="True" Inherits="localhost.Public.Public_News" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" Title="Plastic Industry News"%>

<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="asp" namespace="System.Web.UI" %>


<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
	<style type="text/css">
	#header-left { PADDING-RIGHT: 10px; DISPLAY: inline; PADDING-LEFT: 10px; FLOAT: left; PADDING-BOTTOM: 10px; PADDING-TOP: 10px }
	#header-right { PADDING-RIGHT: 10px; DISPLAY: inline; PADDING-LEFT: 10px; FLOAT: right; PADDING-BOTTOM: 10px; PADDING-TOP: 10px }
	#Answer {    Float: center;
	             z-index: 8;
                 height: 36px;
                 width: 650px;
                 margin: 0px 50px 0px 50px;
                 text-align: center;
                 position: relative;
                 }
	</style>
</asp:Content> 
<asp:Content runat="server" ContentPlaceHolderID="cphJavaScript">
<script type="text/javascript">
    var clientIDs = new Array(<asp:literal id='litClientIDs' runat='server'/>)

    function checkSearch()
    {
	    //return confirm("clientIDs[0].value = " + document.getElementById(clientIDs[0]).value);
	    if ((document.getElementById(clientIDs[0]).value=='') || (document.getElementById(clientIDs[0]).value == 'Start new search'))
	    {
		    return false;
	    }
	    else
	    { 
		    return true;
	    }
    }

    function checkKey()
    {
	    if (window.event.keyCode == 13) // checks whether the enter key is pressed
	    {
		    window.event.cancelBubble = true;
		    window.event.returnValue = false;
		    //__doPostBack('dg:_ctl1:btnSearch');	// doesn't work!
	    }
    }
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphHeading"><span class="Header Bold Color1">Industry News</span></asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">

<style type="text/css">
	
    .progressAjaxPleaseWait
    {
        display: block;
        position: absolute;
        padding: 2px 3px;
    }
    .containerAjaxPleaseWait
    {
        border: solid 1px #808080;
        border-width: 1px 0px;
    }
    .headerAjaxPleaseWait
    {
        background: url(http://www.theplasticexchange.com/images/sprite.png) repeat-x 0px 0px;
        border-color: #808080 #808080 #ccc;
        border-style: solid;
        border-width: 0px 1px 1px;
        padding: 0px 10px;
        color: #000000;
        font-size: 9pt;
        font-weight: bold;
        line-height: 1.9;  
        font-family: arial,helvetica,clean,sans-serif;
    }
    .bodyAjaxPleaseWait
    {
        background-color: #f2f2f2;
        border-color: #808080;
        border-style: solid;
        border-width: 0px 1px;
        padding: 10px;
    }
</style>
        <asp:ScriptManager id="ScriptManager1" runat="Server" />
    
           <script type="text/javascript">
                function onUpdating()
                {
                    // get the update progress div
                    var pnlPopup = $get('<%= this.pnlPopup.ClientID %>'); 

                    //  get the gridview element        
                    var gridView = $get('<%= this.dg.ClientID %>');
                    
                    // make it visible
                    pnlPopup.style.display = '';	    
                    
                    // get the bounds of both the gridview and the progress div
                    var gridViewBounds = Sys.UI.DomElement.getBounds(gridView);
                    var pnlPopupBounds = Sys.UI.DomElement.getBounds(pnlPopup);
                    
                    //  center of gridview
                    
                    var x = gridViewBounds.x + Math.round(gridViewBounds.width / 2) - Math.round(pnlPopupBounds.width / 2);
                    var y = gridViewBounds.y + Math.round(gridViewBounds.height / 2) - Math.round(pnlPopupBounds.height / 2);	    
                    

                    //	set the progress element to this position
                    Sys.UI.DomElement.setLocation(pnlPopup, x, 200);           
                }

                function onUpdated() 
                {
                    // get the update progress div
                    var pnlPopup = $get('<%= this.pnlPopup.ClientID %>'); 
                    // make it invisible
                    pnlPopup.style.display = 'none';
                }            
                               
        </script>
    

<table cellspacing="0" cellpadding="0">
      <tr>
	<td>
	     <cc1:UpdatePanelAnimationExtender ID="UpdatePanelAnimationExtender1" runat="server" TargetControlID="UpdatePanel1">
                <Animations>
                    <OnUpdating>
                        <Parallel duration="0">
                            <%-- place the update progress div over the gridview control --%>
                            <ScriptAction Script="onUpdating();" />  
                            <FadeOut minimumOpacity=".5" />
                         </Parallel>
                    </OnUpdating>
                    <OnUpdated>
                        <Parallel duration="0">
                         <FadeIn minimumOpacity=".5" />
                            <%--find the update progress div and place it over the gridview control--%>
                            <ScriptAction Script="onUpdated();" /> 
                        </Parallel> 
                    </OnUpdated>
                </Animations>
            </cc1:UpdatePanelAnimationExtender>            
            
            <asp:Panel ID="pnlPopup" runat="server" CssClass="progressAjaxPleaseWait" style="display:none;">
                <div class="containerAjaxPleaseWait">
                    <div class="headerAjaxPleaseWait">Loading...</div>
                    <div class="bodyAjaxPleaseWait">
                        <img src="http://www.theplasticexchange.com/images/activity.gif" />
                    </div>
                </div>
            </asp:Panel> 

	   
	        </td>
	        </tr>
	        </table>

	<asp:UpdatePanel id="UpdatePanel1" runat="Server">
	<ContentTemplate>

	<table height="1" cellspacing="0" cellpadding="0" width="780" align="center" bgcolor="#fdb400"
		border="0">
		<tr>
			<td>
				<asp:datagrid BorderWidth="0" BackColor="#000000" CellSpacing="1" id="dg" runat="server" Width="780px" CssClass="DataGrid" AutoGenerateColumns="False" HorizontalAlign="Left" OnPageIndexChanged="dgPageChange" PageSize="30"
				PagerStyle-Mode="NumericPages" ShowFooter="True" >
				<AlternatingItemStyle CssClass="LinkNormal DarkGray" HorizontalAlign="left"></AlternatingItemStyle>
				<ItemStyle CssClass="LinkNormal LightGray" HorizontalAlign="left"></ItemStyle>
				<HeaderStyle HorizontalAlign="Center" CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
				<Columns>
					<asp:ButtonColumn Text="Edit" CommandName="Edit"></asp:ButtonColumn>
					<asp:ButtonColumn Text="Delete" CommandName="Delete"></asp:ButtonColumn>
					<asp:BoundColumn Visible="False" DataField="ID"></asp:BoundColumn>
					<asp:BoundColumn DataField="DATE" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}">
						<ItemStyle Wrap="False"></ItemStyle>
					</asp:BoundColumn>
					<asp:TemplateColumn>
						<HeaderTemplate>
							<div id="header-left">
								Select Month:
								<asp:DropDownList runat="server" id="ddlNewsDate" CssClass="InputForm" DataTextField="MonthAndYear" DataValueField="Sort" DataSource="<%#GetMonths()%>" SelectedValue="<%#GetDateSortId()%>" AutoPostBack="True" OnSelectedIndexChanged="ddlNewsDate_IndexChanged">
								</asp:DropDownList>
							</div>
							<div id="header-right">
								<asp:TextBox runat="server" name="txtSearch" id="txtSearch" CssClass="InputForm" text="<%#searchString%>" OnTextChanged="txtSearch_OnTextChanged">
								</asp:TextBox>
								<asp:Button runat="server" name="btnSearch" CssClass="Content Color2" id="btnSearch" text="Search" CommandName="Search"
									AutoPostBack="True" OnClick="btnSearch_Click"></asp:Button>
							</div>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:LinkButton runat="server" ID="Linkbutton1"></asp:LinkButton>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle Font-Size="Large" Font-Bold="True" ForeColor="#FF8000" CssClass="big" Mode="NumericPages"></PagerStyle>
				</asp:datagrid>
			</td>
		</tr>
	</table>
	<table  align="center">
		<tr align="center">
			<td style="text-align:center">
				<br />
				<asp:Label id="lblNoRecords" runat="server" Visible="False"><span class="Content">No News Stories Found</span><br /><span class="LinkNormal"><a href="Public_News.aspx">Return</a></span></asp:Label>				
				<br />
				<br />
			</td>
		</tr>
	</table>
		
	</ContentTemplate>
	</asp:UpdatePanel>
	
</asp:Content>
