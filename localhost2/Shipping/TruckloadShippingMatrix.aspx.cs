using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Shipping
{
    public partial class TruckloadShippingMatrix : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.Width = "1800px";
            if ((Session["Typ"].ToString() != "A") && (!bool.Parse(Application["IsDebug"].ToString())))
            {
                Response.Redirect("/default.aspx");
            }

            this.sdsTruckloadMatrix.ConnectionString = Application["DBconn"].ToString();

            if (!this.IsPostBack)
            {                
                BindDLL();
            }
        }

        protected void BindDLL()
        {
            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();
                using (SqlDataReader dtrFromState = DBLibrary.GetDataReaderFromSelect(conn, "select distinct STATE_FROM from FREIGHT_MATRIX_TRUCKLOAD order by STATE_FROM"))
                {
                    while (dtrFromState.Read())
                    {
                        ddlFromState.Items.Add(new ListItem(dtrFromState["STATE_FROM"].ToString(), dtrFromState["STATE_FROM"].ToString()));
                    } 
                }
                using (SqlDataReader dtrToState = DBLibrary.GetDataReaderFromSelect(conn, "select distinct STATE_TO from FREIGHT_MATRIX_TRUCKLOAD order by STATE_TO"))
                {
                    while (dtrToState.Read())
                    {
                        ddlToState.Items.Add(new ListItem(dtrToState["STATE_TO"].ToString(), dtrToState["STATE_TO"].ToString()));
                    }                   
                }
            }

          
            
        }

        protected void btnDisplayMatrix_OnClick(object sender, System.EventArgs e)
        {
            string SQLStr = "select PRICE as matrixprice from FREIGHT_MATRIX_TRUCKLOAD where STATE_FROM = '" + ddlFromState.SelectedValue + "' and STATE_TO = '" + ddlToState.SelectedValue + "' ";

            Object Result = DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), SQLStr);
            Double price = Convert.ToDouble(Result);
            txtMatrixValue.Text = price.ToString();

            //DBLibrary.ExecuteStoredProcedure(Application["DBconn"].ToString(), "p_insertmailingsent", param);
            //select PRICE*110/100 as matrixprice from FREIGHT_MATRIX_TRUCKLOAD where STATE_FROM = 'IL' and STATE_TO = 'AL'
        }

        protected void btnUpdateMatrix_OnClick(object sender, System.EventArgs e)
        {
            Double price = Convert.ToDouble(txtMatrixValue.Text.Trim());
            string SQLStr = "update FREIGHT_MATRIX_TRUCKLOAD set PRICE = '" + price + "'where STATE_FROM = '" + ddlFromState.SelectedValue + "' and STATE_TO = '" + ddlToState.SelectedValue + "' ";
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), SQLStr);

        }

        protected void btnMultiUpdate_OnClick(object sender, System.EventArgs e)
        {
            Double multiplier = Convert.ToDouble(txtMultiplier.Text.Trim());
            string SQLStr = "update FREIGHT_MATRIX_TRUCKLOAD set PRICE = PRICE * '" + multiplier + "' ";
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), SQLStr);

        }
    }
}
