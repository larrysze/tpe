<%@ Page Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="spotavg.aspx.cs" Inherits="localhost.Research.spotavg" Title="Untitled Page" %>


<%@ MasterType VirtualPath="~/MasterPages/Menu.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphMain">
   <table>
   <tr>
   <td>
    <br />
    <asp:DropDownList ID="ddlChart" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlChart_SelectedIndexChanged" CssClass="InputForm" />            
    <br />    
    <asp:Button runat="server" ID="cmdExportToExcel" Text="Export to Excel"  CssClass="Content Color2" onclick="btnExportToExcel_Click" />
    <br />
    <asp:DataGrid ID="dgDaily" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="Content LinkNormal" 
        ItemStyle-CssClass="Content LinkNormal"
        AllowPaging="False" PageSize="100" PagerStyle-CssClass = "Content LinkNormal" PagerStyle-Mode="NumericPages"  
        OnPageIndexChanged="myDataGrid_PageChanger" 
        PagerStyle-NextPageText="Next ->" PagerStyle-PrevPageText="<- Previous" PagerStyle-Position="TopAndBottom"
         CellPadding="5" CellSpacing="5" OnItemDataBound="dgDaily_ItemDataBound">
        <Columns>
            <asp:EditCommandColumn ItemStyle-CssClass="Content LinkNormal" UpdateText="Update" CancelText="Cancel" EditText="Edit">              
            </asp:EditCommandColumn>            
            <asp:BoundColumn DataField="weightdate" ReadOnly="True" HeaderText="Date" DataFormatString="{0:d}"  />
            <asp:BoundColumn DataField="ask" HeaderText="ask" />            
        </Columns>     
        <ItemStyle CssClass="LinkNormal" />
        <ItemStyle CssClass="Content" />
        <HeaderStyle CssClass="Header" />        
    </asp:DataGrid>
       
  </td>
  <td valign="top">    
<br />

<br /><br />    
<asp:Image runat="server" ID="imgChart" Visible="true" /><br /><br />
<asp:Image runat="server" ID="imgChart1" Visible="true" /><br /><br />
<asp:Image runat="server" ID="imgChart2" Visible="true" /><br /><br />
    
    


</td>
</tr>
</table>
</asp:Content>
