using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;
using dotnetCHARTING;
using System.Threading;

namespace localhost.Research
{
    public partial class chainchart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CreateEthyleneChainChart();
            //CreatePropyleneChainChart();


            CreateEthyleneChainChart();

            CreateEthaneChart(true);
            CreateEthyleneChart(true);
            createPropaneChart(true);
            createRGPChart(true);
            createHOPPINJChart(true);
            createLLDPEChart(true);

            CreateEthaneChart(false);
            CreateEthyleneChart(false);
            createPropaneChart(false);
            createRGPChart(false);
            createHOPPINJChart(false);
            createLLDPEChart(false);
        }






        private void CreatePropyleneChainChart()
        {
    
            ChartPropylene.ChartArea.ClearColors();

            ChartPropylene.Title = "The Propylene Chain - Data is Daily Ask Price - Priced $/lbs - YTD 2007";
            
            ChartPropylene.Debug = false;
            ChartPropylene.OverlapFooter = false;
            ChartPropylene.Mentor = false;
            ChartPropylene.TempDirectory = "/research/charts/dp/mono";

            ChartPropylene.FileName = "PropyleneChain";
 
            ChartPropylene.Width = 1024;
            ChartPropylene.Height = 768;
            ChartPropylene.Dpi = 300;
            ChartPropylene.FileManager.ImageFormat = ImageFormat.Png;                    
            ChartPropylene.Type = ChartType.Combo;
            ChartPropylene.ChartAreaSpacing = 3;

            ChartPropylene.Background.Color = Color.Black;
            ChartPropylene.DefaultElement.Marker.Visible =false;
            ChartPropylene.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            ChartPropylene.DefaultSeries.DefaultElement.Marker.Size = 8;

            ChartPropylene.XAxis.Label.Color = Color.White;
            ChartPropylene.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartPropylene.XAxis.TimeInterval = TimeInterval.Day;
            ChartPropylene.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartPropylene.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            ChartPropylene.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartPropylene.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartPropylene.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartPropylene.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            ChartPropylene.XAxis.Line.Color = Color.Orange;

            ChartPropylene.YAxis.Interval = 0.01;
            ChartPropylene.YAxis.Label.Text = "Price";
            ChartPropylene.YAxis.FormatString = "$.00#";
            ChartPropylene.YAxis.Scale = Scale.Range;
            ChartPropylene.YAxis.Line.Color = Color.Orange;
            ChartPropylene.YAxis.Label.Color = Color.White;

            ChartPropylene.TitleBox.Position = TitleBoxPosition.Full;
            ChartPropylene.TitleBox.Background.Color = Color.FromArgb(86, 54, 119);
            ChartPropylene.TitleBox.Label.Color = Color.White;
            ChartPropylene.TitleBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Bold);
               
            ChartPropylene.LegendBox.Template = "%Icon%Name";
            //ChartPropylene.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
            ChartPropylene.LegendBox.Position = new Point(210, 160);
            ChartPropylene.LegendBox.LabelStyle.Color = Color.Black;
            ChartPropylene.LegendBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Regular);

            ChartPropylene.ChartArea.Background.Color = Color.Black;
            ChartPropylene.ChartArea.DefaultSeries.Line.Width = 2;
            ChartPropylene.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPropylene.ChartArea.XAxis.DefaultTick.GridLine.Width = 2;
            ChartPropylene.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartPropylene.ChartArea.XAxis.TickLabelAngle = 90;
            ChartPropylene.ChartArea.XAxis.LabelRotate = true;
            ChartPropylene.ChartArea.XAxis.Label.Color = Color.White;
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;            
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Bold);
            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.YAxis.Label.Font.Name, (float)10.0, FontStyle.Bold);
            ChartPropylene.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPropylene.ChartArea.YAxis.DefaultTick.GridLine.Width = 2;
            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartPropylene.ChartArea.YAxis.Label.Color = Color.White;

            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();

            //ChartPropylene.SeriesCollection.Add(getCreateEthyleneChainChartPriceData());
            ChartPropylene.SeriesCollection.Add(getCreatePropyleneChainChartPriceData());

            ChartPropylene.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap bmp1 = ChartPropylene.GetChartBitmap();
            string fileName2 = "";
            fileName2 = ChartPropylene.FileManager.SaveImage(bmp1);

            ChartPropylene.SeriesCollection.Clear();

        }

        private void CreateEthyleneChainChart()
        {
            ChartEthylene.ChartArea.ClearColors();

            ChartEthylene.Title = "The Ethylene Chain - Data is Daily Ask Price - Priced $/lbs - YTD 2007";

            ChartEthylene.Debug = false;
            ChartEthylene.OverlapFooter = false;
            ChartEthylene.Mentor = false;
            ChartEthylene.TempDirectory = "/research/charts/dp/mono";

            ChartEthylene.FileName = "EthyleneChain";

            ChartEthylene.Width = 1024;
            ChartEthylene.Height = 768;
            ChartEthylene.Dpi = 300;
            ChartEthylene.FileManager.ImageFormat = ImageFormat.Png;
            ChartEthylene.Type = ChartType.Combo;
            ChartEthylene.ChartAreaSpacing = 3;

            ChartEthylene.Background.Color = Color.Black;
            ChartEthylene.DefaultElement.Marker.Visible = false;
            ChartEthylene.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            ChartEthylene.DefaultSeries.DefaultElement.Marker.Size = 8;

            ChartEthylene.XAxis.Label.Color = Color.White;
            ChartEthylene.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartEthylene.XAxis.TimeInterval = TimeInterval.Day;
            ChartEthylene.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartEthylene.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            //ChartEthylene.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartEthylene.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartEthylene.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartEthylene.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            ChartEthylene.XAxis.Line.Color = Color.Orange;

            ChartEthylene.YAxis.Interval = 0.01;
            ChartEthylene.YAxis.Label.Text = "Price";
            ChartEthylene.YAxis.FormatString = "$.00#";
            ChartEthylene.YAxis.Scale = Scale.Range;
            ChartEthylene.YAxis.Line.Color = Color.Orange;
            ChartEthylene.YAxis.Label.Color = Color.White;

            ChartEthylene.TitleBox.Position = TitleBoxPosition.Full;
            ChartEthylene.TitleBox.Background.Color = Color.FromArgb(86, 54, 119);
            ChartEthylene.TitleBox.Label.Color = Color.White;
            ChartEthylene.TitleBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Bold);


            ChartEthylene.LegendBox.Template = "%Icon%Name";
            //ChartEthylene.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
            ChartEthylene.LegendBox.Position = new Point(210, 160);
            ChartEthylene.LegendBox.LabelStyle.Color = Color.Black;
            ChartEthylene.LegendBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Regular);

            ChartEthylene.ChartArea.Background.Color = Color.Black;
            ChartEthylene.ChartArea.DefaultSeries.Line.Width = 2;
            ChartEthylene.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartEthylene.ChartArea.XAxis.DefaultTick.GridLine.Width = 3;
            ChartEthylene.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartEthylene.ChartArea.XAxis.TickLabelAngle = 90;
            ChartEthylene.ChartArea.XAxis.LabelRotate = true;
            ChartEthylene.ChartArea.XAxis.Label.Color = Color.White;
            ChartEthylene.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
            ChartEthylene.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, (float)9.0, FontStyle.Bold);
            ChartEthylene.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.YAxis.Label.Font.Name, (float)9.0, FontStyle.Bold);
            ChartEthylene.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartEthylene.ChartArea.YAxis.DefaultTick.GridLine.Width = 3;
            ChartEthylene.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartEthylene.ChartArea.YAxis.Label.Color = Color.White;


            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();

           ChartEthylene.SeriesCollection.Add(getCreateEthyleneChainChartPriceData());
           
            ChartEthylene.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap bmp1 = ChartEthylene.GetChartBitmap();
            string fileName2 = "";
            fileName2 = ChartEthylene.FileManager.SaveImage(bmp1);

            ChartEthylene.SeriesCollection.Clear();

        }

        private SeriesCollection getCreatePropyleneChainChartPriceData()
        {
            string sSqlEthane = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=39 and export_price.date >'01/02/2007' ORDER BY export_price.date ";
            string sSqlEthylene = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=34 and export_price.date >'01/02/2007' ORDER BY export_price.date ";
            string sSqlLDPE = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=9 and export_price.date >'01/02/2007' ORDER BY export_price.date ";

            SeriesCollection SC = new SeriesCollection();
            Series sBidPrice;

            using (SqlConnection conn = new SqlConnection(Application["MonoDB"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "Propane";

                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthane);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Bid Element
                    eBidPrice.ToolTip = dtr["ask"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd");
                    sBidPrice.AddElements(eBidPrice);
                }
            }

            SC.Add(sBidPrice);

            using (SqlConnection conn = new SqlConnection(Application["MonoDB"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "RGP";
                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthylene);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();


                    //Bid Element
                    eBidPrice.ToolTip = dtr["ask"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd");
                    sBidPrice.AddElements(eBidPrice);
                }
            }
            SC.Add(sBidPrice);


            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "HoPP Inj";
                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlLDPE);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Bid Element
                    eBidPrice.ToolTip = dtr["ask"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd");
                    sBidPrice.AddElements(eBidPrice);
                }
            }
            SC.Add(sBidPrice);


            //SC[0].Type = SeriesTypeFinancial.Bar;
            SC[0].Type = SeriesType.Line;  //propane
            SC[0].DefaultElement.Color = Color.Green;
            SC[0].LegendEntry.Marker.Size = 10;

            SC[1].Type = SeriesType.Line;  //rgp
            SC[1].DefaultElement.Color = Color.Orange;
            SC[1].LegendEntry.Marker.Size = 10;

            SC[2].Type = SeriesType.Line;
            SC[2].DefaultElement.Color = Color.Red;
            SC[2].LegendEntry.Marker.Size = 10;

            return (SC);



        }

        private SeriesCollection getCreateEthyleneChainChartPriceData()
        {
            string sSqlEthane = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=38 and export_price.date >'01/02/2007' ORDER BY export_price.date ";
            string sSqlEthylene = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=33 and export_price.date >'01/02/2007' ORDER BY export_price.date ";
            string sSqlLDPE = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=11 and export_price.date >'01/02/2007' ORDER BY export_price.date ";

            SeriesCollection SC = new SeriesCollection();

            //Series sPriceRange = new Series();


            //sPriceRange.Name = "Price Range";
            Series sBidPrice;

            using (SqlConnection conn = new SqlConnection(Application["MonoDB"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "Ethane";

                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthane);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();


                    //Bid Element
                    eBidPrice.ToolTip = dtr["ask"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd");
                    sBidPrice.AddElements(eBidPrice);
                }
            }

            SC.Add(sBidPrice);

            using (SqlConnection conn = new SqlConnection(Application["MonoDB"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "Ethylene";
                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthylene);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();


                    //Bid Element
                    eBidPrice.ToolTip = dtr["ask"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd");
                    sBidPrice.AddElements(eBidPrice);
                }
            }
            SC.Add(sBidPrice);


            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "LLDPE Film";
                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlLDPE);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Bid Element
                    eBidPrice.ToolTip = dtr["ask"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd");
                    sBidPrice.AddElements(eBidPrice);
                }
            }
            SC.Add(sBidPrice);


            //SC[0].Type = SeriesTypeFinancial.Bar;
            SC[0].Type = SeriesType.Line;  //propane
            SC[0].DefaultElement.Color = Color.Green;
            SC[0].LegendEntry.Marker.Size = 10;

            SC[1].Type = SeriesType.Line;  //rgp
            SC[1].DefaultElement.Color = Color.Orange;
            SC[1].LegendEntry.Marker.Size = 10;

            SC[2].Type = SeriesType.Line;
            SC[2].DefaultElement.Color = Color.Red;
            SC[2].LegendEntry.Marker.Size = 10;

            return (SC);
        }




        int ChartWidth = 800;
        int ChartHeight = 600;
        float TitleFontSize = 18;
        float LabelFontSize = 10;



        private void createRGPChart(bool bMonth)
        {
            ChartPropylene.ChartArea.ClearColors();

            ChartPropylene.Debug = false;
            ChartPropylene.OverlapFooter = false;
            ChartPropylene.Mentor = false;
            ChartPropylene.TempDirectory = "/research/charts/dp/mono";

            
            ChartPropylene.Width = ChartWidth;
            ChartPropylene.Height = ChartHeight;
            //ChartPropylene.Dpi = 300;
            ChartPropylene.FileManager.ImageFormat = ImageFormat.Png;
            ChartPropylene.Type = ChartType.Combo;
            ChartPropylene.ChartAreaSpacing = 3;

            ChartPropylene.Background.Color = Color.Black;
            ChartPropylene.DefaultElement.Marker.Visible = true;
            ChartPropylene.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            ChartPropylene.DefaultSeries.DefaultElement.Marker.Size = 8;

            ChartPropylene.XAxis.Label.Color = Color.White;
            ChartPropylene.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartPropylene.XAxis.TimeInterval = TimeInterval.Day;
            ChartPropylene.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartPropylene.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            ChartPropylene.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartPropylene.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartPropylene.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartPropylene.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            ChartPropylene.XAxis.Line.Color = Color.Orange;

            if (bMonth)
            {
                ChartPropylene.YAxis.Interval = 0.005;
                ChartPropylene.YAxis.FormatString = "$.000#";
            }
            else
            {
                ChartPropylene.YAxis.Interval = 0.01;
                ChartPropylene.YAxis.FormatString = "$.00#";
            }


            ChartPropylene.YAxis.Label.Text = "Price";
            ChartPropylene.YAxis.Scale = Scale.Range;
            ChartPropylene.YAxis.Line.Color = Color.Orange;
            ChartPropylene.YAxis.Label.Color = Color.White;

            ChartPropylene.TitleBox.Position = TitleBoxPosition.Full;
            ChartPropylene.TitleBox.Background.Color = Color.FromArgb(86, 54, 119);
            ChartPropylene.TitleBox.Label.Color = Color.White;
            ChartPropylene.TitleBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, TitleFontSize, FontStyle.Bold);

            ChartPropylene.LegendBox.Visible = false;
            ChartPropylene.LegendBox.Template = "%Icon%Name";
            //ChartPropylene.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
            ChartPropylene.LegendBox.Position = new Point(210, 160);
            ChartPropylene.LegendBox.LabelStyle.Color = Color.Black;
            ChartPropylene.LegendBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Regular);

            ChartPropylene.ChartArea.Background.Color = Color.Black;
            ChartPropylene.ChartArea.DefaultSeries.Line.Width = 2;
            ChartPropylene.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPropylene.ChartArea.XAxis.DefaultTick.GridLine.Width = 2;
            ChartPropylene.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartPropylene.ChartArea.XAxis.TickLabelAngle = 90;
            ChartPropylene.ChartArea.XAxis.LabelRotate = true;
            ChartPropylene.ChartArea.XAxis.Label.Color = Color.White;
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, LabelFontSize, FontStyle.Bold);
            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.YAxis.Label.Font.Name, LabelFontSize, FontStyle.Bold);
            ChartPropylene.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPropylene.ChartArea.YAxis.DefaultTick.GridLine.Width = 2;
            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartPropylene.ChartArea.YAxis.Label.Color = Color.White;

            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();

            if (bMonth)
            {
                ChartPropylene.Title = "RGP - Daily Ask Price $/lbs - Month";
                ChartPropylene.FileName = "RGP";
            }
            else
            {
                ChartPropylene.Title = "RGP - Friday Ask Price $/lbs - 52 Week";
                ChartPropylene.FileName = "RGPYTD";
            }

            ChartPropylene.SeriesCollection.Add(getRGPChartPriceData(bMonth));

            ChartPropylene.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap bmp1 = ChartPropylene.GetChartBitmap();
            string fileName2 = "";
            fileName2 = ChartPropylene.FileManager.SaveImage(bmp1);

            ChartPropylene.SeriesCollection.Clear();


        }

        private SeriesCollection getRGPChartPriceData(bool bMonth)
        {
            string sSqlRGPYTD = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=34 and export_price.date > DATEADD(dd,-365,getdate()) and datepart(dw,export_price.date) = 6 ORDER BY export_price.date ";
            string sSqlRGPMonth = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=34 and export_price.date > DATEADD(dd,-30,getdate())  ORDER BY export_price.date ";

            //string sSqlRGPYTD = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=34 and export_price.date >'01/02/2007' and datepart(dw,export_price.date) = 6 ORDER BY export_price.date ";
            //string sSqlRGPMonth = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=34 and export_price.date > DATEADD(dd,-30,getdate())  ORDER BY export_price.date ";

            SeriesCollection SC = new SeriesCollection();

            Series sBidPrice;

            using (SqlConnection conn = new SqlConnection(Application["MonoDB"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "RGP";

                if (bMonth)
                {
                    dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlRGPMonth);
                }
                else
                {
                    dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlRGPYTD);
                }

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();
                    //Bid Element
                    eBidPrice.ToolTip = dtr["ask"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd");
                    sBidPrice.AddElements(eBidPrice);
                }
            }

            SC.Add(sBidPrice);

            SC[0].Type = SeriesType.Line;  //propane
            SC[0].DefaultElement.Color = Color.Red;
            SC[0].LegendEntry.Marker.Size = 8;

            return (SC);
        }


        private void CreateEthyleneChart(bool bMonth)
        {
            ChartPropylene.ChartArea.ClearColors();

            ChartPropylene.Debug = false;
            ChartPropylene.OverlapFooter = false;
            ChartPropylene.Mentor = false;
            ChartPropylene.TempDirectory = "/research/charts/dp/mono";

            ChartPropylene.Width = ChartWidth;
            ChartPropylene.Height = ChartHeight;
            //ChartPropylene.Dpi = 300;
            ChartPropylene.FileManager.ImageFormat = ImageFormat.Png;
            ChartPropylene.Type = ChartType.Combo;
            ChartPropylene.ChartAreaSpacing = 3;

            ChartPropylene.Background.Color = Color.Black;
            ChartPropylene.DefaultElement.Marker.Visible = true;
            ChartPropylene.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            ChartPropylene.DefaultSeries.DefaultElement.Marker.Size = 8;

            ChartPropylene.XAxis.Label.Color = Color.White;
            ChartPropylene.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartPropylene.XAxis.TimeInterval = TimeInterval.Day;
            ChartPropylene.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartPropylene.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            ChartPropylene.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartPropylene.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartPropylene.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartPropylene.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            ChartPropylene.XAxis.Line.Color = Color.Orange;

            if (bMonth)
            {
                ChartPropylene.YAxis.Interval = 0.005;
                ChartPropylene.YAxis.FormatString = "$.000#";
            }
            else
            {
                ChartPropylene.YAxis.Interval = 0.01;
                ChartPropylene.YAxis.FormatString = "$.00#";
            }
            
            ChartPropylene.YAxis.Label.Text = "Price";
            ChartPropylene.YAxis.Scale = Scale.Range;
            ChartPropylene.YAxis.Line.Color = Color.Orange;
            ChartPropylene.YAxis.Label.Color = Color.White;

            ChartPropylene.TitleBox.Position = TitleBoxPosition.Full;
            ChartPropylene.TitleBox.Background.Color = Color.FromArgb(86, 54, 119);
            ChartPropylene.TitleBox.Label.Color = Color.White;
            ChartPropylene.TitleBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, TitleFontSize, FontStyle.Bold);

            ChartPropylene.LegendBox.Visible = false;
            ChartPropylene.LegendBox.Template = "%Icon%Name";
            //ChartPropylene.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
            ChartPropylene.LegendBox.Position = new Point(210, 160);
            ChartPropylene.LegendBox.LabelStyle.Color = Color.Black;
            ChartPropylene.LegendBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Regular);

            ChartPropylene.ChartArea.Background.Color = Color.Black;
            ChartPropylene.ChartArea.DefaultSeries.Line.Width = 2;
            ChartPropylene.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPropylene.ChartArea.XAxis.DefaultTick.GridLine.Width = 2;
            ChartPropylene.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartPropylene.ChartArea.XAxis.TickLabelAngle = 90;
            ChartPropylene.ChartArea.XAxis.LabelRotate = true;
            ChartPropylene.ChartArea.XAxis.Label.Color = Color.White;
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, LabelFontSize, FontStyle.Bold);
            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.YAxis.Label.Font.Name, LabelFontSize, FontStyle.Bold);
            ChartPropylene.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPropylene.ChartArea.YAxis.DefaultTick.GridLine.Width = 2;
            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartPropylene.ChartArea.YAxis.Label.Color = Color.White;

            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();

            if (bMonth)
            {
                ChartPropylene.Title = "Ethylene - Daily Ask Price $/lbs - Month";
                ChartPropylene.FileName = "Ethylene";
            }
            else
            {
                ChartPropylene.Title = "Ethylene - Friday Ask Price $/lbs - 52 Week";
                ChartPropylene.FileName = "EthyleneYTD";
            }
            
            ChartPropylene.SeriesCollection.Add(getEthyleneChartPriceData(bMonth));

            ChartPropylene.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap bmp1 = ChartPropylene.GetChartBitmap();
            string fileName2 = "";
            fileName2 = ChartPropylene.FileManager.SaveImage(bmp1);

            ChartPropylene.SeriesCollection.Clear();

        }

        private SeriesCollection getEthyleneChartPriceData(bool bMonth)
        {
            string sSqlEthylene = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=33 and export_price.date > DATEADD(dd,-365,getdate()) and datepart(dw,export_price.date) = 6 ORDER BY export_price.date ";
            string sSqlEthyleneMonth = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=33 and export_price.date > DATEADD(dd,-30,getdate())  ORDER BY export_price.date ";

            SeriesCollection SC = new SeriesCollection();

            Series sBidPrice;

            using (SqlConnection conn = new SqlConnection(Application["MonoDB"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "Ethylene";

                if (bMonth)
                {
                    dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthyleneMonth);
                }
                else
                {
                    dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthylene);
                }

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Bid Element
                    eBidPrice.ToolTip = dtr["ask"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd");
                    sBidPrice.AddElements(eBidPrice);
                }
            }

            SC.Add(sBidPrice);

            //SC[0].Type = SeriesTypeFinancial.Bar;
            SC[0].Type = SeriesType.Line;  //propane
            SC[0].DefaultElement.Color = Color.Red;
            SC[0].LegendEntry.Marker.Size = 8;

            return (SC);

        }


        private void createPropaneChart(bool bMonth)
        {
            ChartPropylene.ChartArea.ClearColors();

            ChartPropylene.Debug = false;
            ChartPropylene.OverlapFooter = false;
            ChartPropylene.Mentor = false;
            ChartPropylene.TempDirectory = "/research/charts/dp/mono";

            ChartPropylene.Width = ChartWidth;
            ChartPropylene.Height = ChartHeight;
            //ChartPropylene.Dpi = 300;
            ChartPropylene.FileManager.ImageFormat = ImageFormat.Png;
            ChartPropylene.Type = ChartType.Combo;
            ChartPropylene.ChartAreaSpacing = 3;

            ChartPropylene.Background.Color = Color.Black;
            ChartPropylene.DefaultElement.Marker.Visible = true;
            ChartPropylene.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            ChartPropylene.DefaultSeries.DefaultElement.Marker.Size = 8;

            ChartPropylene.XAxis.Label.Color = Color.White;
            ChartPropylene.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartPropylene.XAxis.TimeInterval = TimeInterval.Day;
            ChartPropylene.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartPropylene.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            ChartPropylene.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartPropylene.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartPropylene.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartPropylene.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            ChartPropylene.XAxis.Line.Color = Color.Orange;

            if (bMonth)
            {
                ChartPropylene.YAxis.Interval = 0.005;
                ChartPropylene.YAxis.FormatString = "$.000#";
            }
            else
            {
                ChartPropylene.YAxis.Interval = 0.01;
                ChartPropylene.YAxis.FormatString = "$.00#";
            }

            ChartPropylene.YAxis.Label.Text = "Price";
            ChartPropylene.YAxis.Scale = Scale.Range;
            ChartPropylene.YAxis.Line.Color = Color.Orange;
            ChartPropylene.YAxis.Label.Color = Color.White;

            ChartPropylene.TitleBox.Position = TitleBoxPosition.Full;
            ChartPropylene.TitleBox.Background.Color = Color.FromArgb(86, 54, 119);
            ChartPropylene.TitleBox.Label.Color = Color.White;
            ChartPropylene.TitleBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, TitleFontSize, FontStyle.Bold);

            ChartPropylene.LegendBox.Visible = false;
            ChartPropylene.LegendBox.Template = "%Icon%Name";
            //ChartPropylene.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
            ChartPropylene.LegendBox.Position = new Point(210, 160);
            ChartPropylene.LegendBox.LabelStyle.Color = Color.Black;
            ChartPropylene.LegendBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Regular);

            ChartPropylene.ChartArea.Background.Color = Color.Black;
            ChartPropylene.ChartArea.DefaultSeries.Line.Width = 2;
            ChartPropylene.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPropylene.ChartArea.XAxis.DefaultTick.GridLine.Width = 2;
            ChartPropylene.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartPropylene.ChartArea.XAxis.TickLabelAngle = 90;
            ChartPropylene.ChartArea.XAxis.LabelRotate = true;
            ChartPropylene.ChartArea.XAxis.Label.Color = Color.White;
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, LabelFontSize, FontStyle.Bold);
            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.YAxis.Label.Font.Name, LabelFontSize, FontStyle.Bold);
            ChartPropylene.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPropylene.ChartArea.YAxis.DefaultTick.GridLine.Width = 2;
            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartPropylene.ChartArea.YAxis.Label.Color = Color.White;

            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();

            if (bMonth)
            {
                ChartPropylene.Title = "Propane - Daily Ask Price $/gal - Month";
                ChartPropylene.FileName = "Propane";
            }
            else
            {
                ChartPropylene.Title = "Propane - Friday Ask Price $/gal - 52 Week";
                ChartPropylene.FileName = "PropaneYTD";
            }


            ChartPropylene.SeriesCollection.Add(getPropaneChartPriceData(bMonth));

            ChartPropylene.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap bmp1 = ChartPropylene.GetChartBitmap();
            string fileName2 = "";
            fileName2 = ChartPropylene.FileManager.SaveImage(bmp1);

            ChartPropylene.SeriesCollection.Clear();



        }

        private SeriesCollection getPropaneChartPriceData(bool bMonth)
        {
            string sSqlPropane = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=39 and export_price.date  > DATEADD(dd,-365,getdate()) and datepart(dw,export_price.date) = 6 ORDER BY export_price.date ";
            string sSqlPropaneMonth = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=39 and export_price.date > DATEADD(dd,-30,getdate())  ORDER BY export_price.date ";

            SeriesCollection SC = new SeriesCollection();

            Series sBidPrice;

            using (SqlConnection conn = new SqlConnection(Application["MonoDB"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "Propane";

                if (bMonth)
                {
                    dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlPropaneMonth);
                }
                else
                {
                    dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlPropane);
                }

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Bid Element
                    eBidPrice.ToolTip = dtr["ask"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd");
                    sBidPrice.AddElements(eBidPrice);
                }
            }

            SC.Add(sBidPrice);

            //SC[0].Type = SeriesTypeFinancial.Bar;
            SC[0].Type = SeriesType.Line;  //propane
            SC[0].DefaultElement.Color = Color.Green;
            SC[0].LegendEntry.Marker.Size = 8;

            return (SC);

        }


        private void CreateEthaneChart(bool bMonth)
        {
            ChartPropylene.ChartArea.ClearColors();
            
            ChartPropylene.Debug = false;
            ChartPropylene.OverlapFooter = false;
            ChartPropylene.Mentor = false;
            ChartPropylene.TempDirectory = "/research/charts/dp/mono";
            
            ChartPropylene.Width = ChartWidth;
            ChartPropylene.Height = ChartHeight;
            //ChartPropylene.Dpi = 300;
            ChartPropylene.FileManager.ImageFormat = ImageFormat.Png;
            ChartPropylene.Type = ChartType.Combo;
            ChartPropylene.ChartAreaSpacing = 3;

            ChartPropylene.Background.Color = Color.Black;
            ChartPropylene.DefaultElement.Marker.Visible = true;
            ChartPropylene.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            //ChartPropylene.DefaultSeries.DefaultElement.Marker.Size = 8;

            ChartPropylene.XAxis.Label.Color = Color.White;
            ChartPropylene.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartPropylene.XAxis.TimeInterval = TimeInterval.Days;
            ChartPropylene.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartPropylene.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            ChartPropylene.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartPropylene.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartPropylene.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartPropylene.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            ChartPropylene.XAxis.Line.Color = Color.Orange;

            if (bMonth)
            {
                ChartPropylene.YAxis.Interval = 0.005;
                ChartPropylene.YAxis.FormatString = "$.000#";
            }
            else
            {
                ChartPropylene.YAxis.Interval = 0.01;
                ChartPropylene.YAxis.FormatString = "$.00#";
            }

            ChartPropylene.YAxis.Label.Text = "Price";

            ChartPropylene.YAxis.Scale = Scale.Range;
            ChartPropylene.YAxis.Line.Color = Color.Orange;
            ChartPropylene.YAxis.Label.Color = Color.White;

            ChartPropylene.TitleBox.Position = TitleBoxPosition.Full;
            ChartPropylene.TitleBox.Background.Color = Color.FromArgb(86, 54, 119);
            ChartPropylene.TitleBox.Label.Color = Color.White;
            ChartPropylene.TitleBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, TitleFontSize, FontStyle.Bold);

            ChartPropylene.LegendBox.Visible = false;
            ChartPropylene.LegendBox.Template = "%Icon%Name";
            //ChartPropylene.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
            ChartPropylene.LegendBox.Position = new Point(210, 160);
            ChartPropylene.LegendBox.LabelStyle.Color = Color.Black;
            ChartPropylene.LegendBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Regular);

            ChartPropylene.ChartArea.Background.Color = Color.Black;
            ChartPropylene.ChartArea.DefaultSeries.Line.Width = 2;
            ChartPropylene.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPropylene.ChartArea.XAxis.DefaultTick.GridLine.Width = 2;
            ChartPropylene.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartPropylene.ChartArea.XAxis.TickLabelAngle = 90;
            ChartPropylene.ChartArea.XAxis.LabelRotate = true;
            ChartPropylene.ChartArea.XAxis.Label.Color = Color.White;
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, LabelFontSize, FontStyle.Bold);
            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.YAxis.Label.Font.Name, LabelFontSize, FontStyle.Bold);
            ChartPropylene.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPropylene.ChartArea.YAxis.DefaultTick.GridLine.Width = 2;
            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartPropylene.ChartArea.YAxis.Label.Color = Color.White;

            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();

            if (bMonth)
            {
                ChartPropylene.Title = "Ethane - Daily Ask Price $/gal - Month";
                ChartPropylene.FileName = "Ethane";
            }
            else
            {
                ChartPropylene.Title = "Ethane - Friday Ask Price $/gal - 52 Week";
                ChartPropylene.FileName = "EthaneYTD";
            }

            
            ChartPropylene.SeriesCollection.Add(getEthaneChartPriceData(bMonth));

            ChartPropylene.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap bmp1 = ChartPropylene.GetChartBitmap();
            string fileName2 = "";
            fileName2 = ChartPropylene.FileManager.SaveImage(bmp1);

            ChartPropylene.SeriesCollection.Clear();


        }

        private SeriesCollection getEthaneChartPriceData(bool bMonth)
        {
            string sSqlEthane = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=38 and export_price.date  > DATEADD(dd,-365,getdate()) and datepart(dw,export_price.date) = 6 ORDER BY export_price.date ";
            string sSqlEthaneMonth = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=38 and export_price.date > DATEADD(dd,-30,getdate())  ORDER BY export_price.date ";

            SeriesCollection SC = new SeriesCollection();

            Series sBidPrice;

            using (SqlConnection conn = new SqlConnection(Application["MonoDB"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "Ethane";

                if (bMonth)
                {
                    dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthaneMonth);
                }
                else
                {
                    dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthane);
                }


                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Bid Element
                    eBidPrice.ToolTip = dtr["ask"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd");
                    sBidPrice.AddElements(eBidPrice);
                }
            }

            SC.Add(sBidPrice);

            SC[0].Type = SeriesType.Line;  //propane
            SC[0].DefaultElement.Color = Color.Green;
            SC[0].LegendEntry.Marker.Size = 8;

            return (SC);
        }


        private void createLLDPEChart(bool bMonth)
        {
            ChartPropylene.ChartArea.ClearColors();

            ChartPropylene.Debug = false;
            ChartPropylene.OverlapFooter = false;
            ChartPropylene.Mentor = false;
            ChartPropylene.TempDirectory = "/research/charts/dp/mono";


            ChartPropylene.Width = ChartWidth;
            ChartPropylene.Height = ChartHeight;
            //ChartPropylene.Dpi = 300;
            ChartPropylene.FileManager.ImageFormat = ImageFormat.Png;
            ChartPropylene.Type = ChartType.Combo;
            ChartPropylene.ChartAreaSpacing = 3;

            ChartPropylene.Background.Color = Color.Black;
            ChartPropylene.DefaultElement.Marker.Visible = true;
            ChartPropylene.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            ChartPropylene.DefaultSeries.DefaultElement.Marker.Size = 8;

            ChartPropylene.XAxis.Label.Color = Color.White;
            ChartPropylene.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartPropylene.XAxis.TimeInterval = TimeInterval.Day;
            ChartPropylene.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartPropylene.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            ChartPropylene.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartPropylene.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartPropylene.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartPropylene.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            ChartPropylene.XAxis.Line.Color = Color.Orange;

            if (bMonth)
            {
                ChartPropylene.YAxis.Interval = 0.005;
                ChartPropylene.YAxis.FormatString = "$.000#";
            }
            else
            {
                ChartPropylene.YAxis.Interval = 0.01;
                ChartPropylene.YAxis.FormatString = "$.00#";
            }

            ChartPropylene.YAxis.Label.Text = "Price";
            ChartPropylene.YAxis.Scale = Scale.Range;
            ChartPropylene.YAxis.Line.Color = Color.Orange;
            ChartPropylene.YAxis.Label.Color = Color.White;

            ChartPropylene.TitleBox.Position = TitleBoxPosition.Full;
            ChartPropylene.TitleBox.Background.Color = Color.FromArgb(86, 54, 119);
            ChartPropylene.TitleBox.Label.Color = Color.White;
            ChartPropylene.TitleBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, TitleFontSize, FontStyle.Bold);

            ChartPropylene.LegendBox.Visible = false;
            ChartPropylene.LegendBox.Template = "%Icon%Name";
            //ChartPropylene.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
            ChartPropylene.LegendBox.Position = new Point(210, 160);
            ChartPropylene.LegendBox.LabelStyle.Color = Color.Black;
            ChartPropylene.LegendBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Regular);

            ChartPropylene.ChartArea.Background.Color = Color.Black;
            ChartPropylene.ChartArea.DefaultSeries.Line.Width = 2;
            ChartPropylene.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPropylene.ChartArea.XAxis.DefaultTick.GridLine.Width = 2;
            ChartPropylene.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartPropylene.ChartArea.XAxis.TickLabelAngle = 90;
            ChartPropylene.ChartArea.XAxis.LabelRotate = true;
            ChartPropylene.ChartArea.XAxis.Label.Color = Color.White;
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, LabelFontSize, FontStyle.Bold);
            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.YAxis.Label.Font.Name, LabelFontSize, FontStyle.Bold);
            ChartPropylene.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPropylene.ChartArea.YAxis.DefaultTick.GridLine.Width = 2;
            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartPropylene.ChartArea.YAxis.Label.Color = Color.White;

            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();

            if (bMonth)
            {
                ChartPropylene.Title = "LLDPE Film - Daily Ask Price $/lbs - Month";
                ChartPropylene.FileName = "LLDPEFilm";
            }
            else
            {
                ChartPropylene.Title = "LLDPE Film - Friday Ask Price $/lbs - 52 Week";
                ChartPropylene.FileName = "LLDPEFilmYTD";
            }

            ChartPropylene.SeriesCollection.Add(getLLDPEChartPriceData(bMonth));

            ChartPropylene.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap bmp1 = ChartPropylene.GetChartBitmap();
            string fileName2 = "";
            fileName2 = ChartPropylene.FileManager.SaveImage(bmp1);

            ChartPropylene.SeriesCollection.Clear();



        }

        private SeriesCollection getLLDPEChartPriceData(bool bMonth)
        {
            string sSqlEthane = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=11 and export_price.date  > DATEADD(dd,-365,getdate()) and datepart(dw,export_price.date) = 6 ORDER BY export_price.date ";
            string sSqlEthaneMonth = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=11 and export_price.date > DATEADD(dd,-30,getdate())  ORDER BY export_price.date ";

            SeriesCollection SC = new SeriesCollection();

            Series sBidPrice;

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "LLDPE Film";

                if (bMonth)
                {
                    dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthaneMonth);
                }
                else
                {
                    dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthane);
                }


                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Bid Element
                    eBidPrice.ToolTip = dtr["ask"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd");
                    sBidPrice.AddElements(eBidPrice);
                }
            }

            SC.Add(sBidPrice);

            SC[0].Type = SeriesType.Line;  //propane
            SC[0].DefaultElement.Color = Color.Yellow;
            SC[0].LegendEntry.Marker.Size = 8;

            return (SC);
        }


        private void createHOPPINJChart(bool bMonth)
        {
            ChartPropylene.ChartArea.ClearColors();

            ChartPropylene.Debug = false;
            ChartPropylene.OverlapFooter = false;
            ChartPropylene.Mentor = false;
            ChartPropylene.TempDirectory = "/research/charts/dp/mono";


            ChartPropylene.Width = ChartWidth;
            ChartPropylene.Height = ChartHeight;
            //ChartPropylene.Dpi = 300;
            ChartPropylene.FileManager.ImageFormat = ImageFormat.Png;
            ChartPropylene.Type = ChartType.Combo;
            ChartPropylene.ChartAreaSpacing = 3;

            ChartPropylene.Background.Color = Color.Black;
            ChartPropylene.DefaultElement.Marker.Visible = true;
            ChartPropylene.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            ChartPropylene.DefaultSeries.DefaultElement.Marker.Size = 8;

            ChartPropylene.XAxis.Label.Color = Color.White;
            ChartPropylene.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartPropylene.XAxis.TimeInterval = TimeInterval.Day;
            ChartPropylene.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartPropylene.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            ChartPropylene.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartPropylene.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartPropylene.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartPropylene.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            ChartPropylene.XAxis.Line.Color = Color.Orange;

            if (bMonth)
            {
                ChartPropylene.YAxis.Interval = 0.005;
                ChartPropylene.YAxis.FormatString = "$.000#";
            }
            else
            {
                ChartPropylene.YAxis.Interval = 0.01;
                ChartPropylene.YAxis.FormatString = "$.00#";
            }

            ChartPropylene.YAxis.Label.Text = "Price";
            ChartPropylene.YAxis.Scale = Scale.Range;
            ChartPropylene.YAxis.Line.Color = Color.Orange;
            ChartPropylene.YAxis.Label.Color = Color.White;

            ChartPropylene.TitleBox.Position = TitleBoxPosition.Full;
            ChartPropylene.TitleBox.Background.Color = Color.FromArgb(86, 54, 119);
            ChartPropylene.TitleBox.Label.Color = Color.White;
            ChartPropylene.TitleBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, TitleFontSize, FontStyle.Bold);

            ChartPropylene.LegendBox.Visible = false;
            ChartPropylene.LegendBox.Template = "%Icon%Name";
            //ChartPropylene.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
            ChartPropylene.LegendBox.Position = new Point(210, 160);
            ChartPropylene.LegendBox.LabelStyle.Color = Color.Black;
            ChartPropylene.LegendBox.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Regular);

            ChartPropylene.ChartArea.Background.Color = Color.Black;
            ChartPropylene.ChartArea.DefaultSeries.Line.Width = 2;
            ChartPropylene.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPropylene.ChartArea.XAxis.DefaultTick.GridLine.Width = 2;
            ChartPropylene.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartPropylene.ChartArea.XAxis.TickLabelAngle = 90;
            ChartPropylene.ChartArea.XAxis.LabelRotate = true;
            ChartPropylene.ChartArea.XAxis.Label.Color = Color.White;
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, LabelFontSize, FontStyle.Bold);
            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.YAxis.Label.Font.Name, LabelFontSize, FontStyle.Bold);
            ChartPropylene.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPropylene.ChartArea.YAxis.DefaultTick.GridLine.Width = 2;
            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartPropylene.ChartArea.YAxis.Label.Color = Color.White;

            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();

            if (bMonth)
            {
                ChartPropylene.Title = "HoPP Inj - Daily Ask Price $/lbs - Month";
                ChartPropylene.FileName = "HoPPInj";
            }
            else
            {
                ChartPropylene.Title = "HoPP Inj - Friday Ask Price $/lbs - 52 Week";
                ChartPropylene.FileName = "HoPPInjYTD";
            }

            ChartPropylene.SeriesCollection.Add(getHOPPChartPriceData(bMonth));

            ChartPropylene.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap bmp1 = ChartPropylene.GetChartBitmap();
            string fileName2 = "";
            fileName2 = ChartPropylene.FileManager.SaveImage(bmp1);

            ChartPropylene.SeriesCollection.Clear();



        }

        private SeriesCollection getHOPPChartPriceData(bool bMonth)
        {
            string sSqlEthane = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=9 and export_price.date  > DATEADD(dd,-365,getdate()) and datepart(dw,export_price.date) = 6 ORDER BY export_price.date ";
            string sSqlEthaneMonth = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=9 and export_price.date > DATEADD(dd,-30,getdate())  ORDER BY export_price.date ";

            SeriesCollection SC = new SeriesCollection();

            Series sBidPrice;

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "HoPP Inj";

                if (bMonth)
                {
                    dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthaneMonth);
                }
                else
                {
                    dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthane);
                }


                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Bid Element
                    eBidPrice.ToolTip = dtr["ask"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd");
                    sBidPrice.AddElements(eBidPrice);
                }
            }

            SC.Add(sBidPrice);

            SC[0].Type = SeriesType.Line;  //propane
            SC[0].DefaultElement.Color = Color.Yellow;
            SC[0].LegendEntry.Marker.Size = 8;

            return (SC);
        }


    
    }
}
