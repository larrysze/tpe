<%@ Page Language="c#" Codebehind="WeeklyReview.aspx.cs" AutoEventWireup="True" Inherits="localhost.Research.WeeklyReview" Title="Public Research" MasterPageFile="~/MasterPages/Menu.Master" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="asp" namespace="System.Web.UI" %>

<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
<style type="text/css">
    .progressAjaxPleaseWait
    {
        display: block;
        position: absolute;
        padding: 2px 3px;
    }
    .containerAjaxPleaseWait
    {
        border: solid 1px #808080;
        border-width: 1px 0px;
    }
    .headerAjaxPleaseWait
    {
        background: url(http://www.theplasticexchange.com/images/sprite.png) repeat-x 0px 0px;
        border-color: #808080 #808080 #ccc;
        border-style: solid;
        border-width: 0px 1px 1px;
        padding: 0px 10px;
        color: #000000;
        font-size: 9pt;
        font-weight: bold;
        line-height: 1.9;  
        font-family: arial,helvetica,clean,sans-serif;
    }
    .bodyAjaxPleaseWait
    {
        background-color: #f2f2f2;
        border-color: #808080;
        border-style: solid;
        border-width: 0px 1px;
        padding: 10px;
    }
</style>

   <script type="text/javascript">
                function onUpdating()
                {
                    // get the update progress div
                    var pnlPopup = $get('<%= this.pnlPopup.ClientID %>'); 

                    //  get the gridview element        
                    var gridView = $get('<%= this.pnlRegular.ClientID %>');
                    
                    // make it visible
                    pnlPopup.style.display = '';	    
                    
                    // get the bounds of both the gridview and the progress div
                    var gridViewBounds = Sys.UI.DomElement.getBounds(gridView);
                    var pnlPopupBounds = Sys.UI.DomElement.getBounds(pnlPopup);
                    
                    //  center of gridview
              var x = gridViewBounds.x + Math.round(gridViewBounds.width / 2) - Math.round(pnlPopupBounds.width / 2);
                    var y = 250;
                    //var y = gridViewBounds.y + Math.round(gridViewBounds.height / 2) - Math.round(pnlPopupBounds.height / 2);	    



                    //	set the progress element to this position
                    Sys.UI.DomElement.setLocation(pnlPopup, x, y);           
                }

                function onUpdated() 
                {
                    // get the update progress div
                    var pnlPopup = $get('<%= this.pnlPopup.ClientID %>'); 
                    // make it invisible
                    pnlPopup.style.display = 'none';
                }            
                
               
                 
                
        </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
      <asp:ScriptManager id="ScriptManager1" runat="Server" />
    
    <table cellspacing="0" cellpadding="0">
      <tr>
	        <td>
	        
	     <cc1:UpdatePanelAnimationExtender ID="UpdatePanelAnimationExtender1" runat="server" TargetControlID="UpdatePanel1">
                <Animations>
                    <OnUpdating>
                        <Parallel duration="0">
                            <%-- place the update progress div over the gridview control --%>
                            <ScriptAction Script="onUpdating();" />  
                            <FadeOut minimumOpacity=".5" />
                         </Parallel>
                    </OnUpdating>
                    <OnUpdated>
                        <Parallel duration="0">
                         <FadeIn minimumOpacity=".5" />
                            <%--find the update progress div and place it over the gridview control--%>
                            <ScriptAction Script="onUpdated();" /> 
                        </Parallel> 
                    </OnUpdated>
                </Animations>
            </cc1:UpdatePanelAnimationExtender>            
            
            <asp:Panel ID="pnlPopup" runat="server" CssClass="progressAjaxPleaseWait" style="display:none;">
                <div class="containerAjaxPleaseWait">
                    <div class="headerAjaxPleaseWait">Loading...</div>
                    <div class="bodyAjaxPleaseWait">
                        <img src="http://www.theplasticexchange.com/images/activity.gif" alt=""/>
                    </div>
                </div>
            </asp:Panel> 

	   
	        </td>
	        </tr>
	        </table>
	        
	             <asp:UpdatePanel id="UpdatePanel1" runat="Server">
 <ContentTemplate>
      
    
    <asp:Panel runat="server" ID="pnlRegular">
        <asp:Table ID="Table1" runat="server" Width="100%">
            <asp:TableRow ID="trPrint">
                <asp:TableCell Width="35px">&nbsp;</asp:TableCell>
                <asp:TableCell Width="35px" Text="&lt;IMG src=&quot;../pics/bullet.gif&quot;&gt;"></asp:TableCell>
                <asp:TableCell ColumnSpan="4" CssClass="ContentWR Color2" Style="font-size: 12px; text-align: left;" Width="720px">
                    <div class="LinkNormal Color2">
                        <asp:Label runat="server" ID="lblPrintLink" />
                    </div>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="35px">&nbsp;</asp:TableCell>
                <asp:TableCell Width="35px" Text="
			            &lt;IMG src=&quot;../pics/bullet.gif&quot;&gt;
		            "></asp:TableCell>
                <asp:TableCell Width="350px" CssClass="ContentWR Color2" Style="font-size: 12px; text-align: left;" Text="Add an email to join our distribution list:"></asp:TableCell>
                <asp:TableCell Width="150px" Wrap="False">
                    <asp:TextBox runat="server" EnableViewState="False" Width="130px" CssClass="InputForm" ID="txtEmail" MaxLength="255"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEmail" ErrorMessage="*" ID="rfvEmail"></asp:RequiredFieldValidator>
                </asp:TableCell>
                <asp:TableCell VerticalAlign="Middle" Width="30px">
                    <asp:ImageButton runat="server" ImageUrl="/images/buttons/submit_orange.jpg" ID="btnEmail"></asp:ImageButton>
                </asp:TableCell>
                <asp:TableCell VerticalAlign="Middle" Width="220px" HorizontalAlign="left">
                    <asp:RegularExpressionValidator CssClass="ContentWR Color3" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" ControlToValidate="txtEmail" ErrorMessage="Invalid email" ID="revEmail"></asp:RegularExpressionValidator>
                    <asp:CustomValidator CssClass="ContentWR Color3" runat="server" ID="cvEmail" ControlToValidate="txtEmail"></asp:CustomValidator>
                </asp:TableCell>
            </asp:TableRow>
        
            <asp:TableRow>
                <asp:TableCell Width="35px">&nbsp;</asp:TableCell>
                <asp:TableCell Width="35px" Text="
				        &lt;IMG src=&quot;../pics/bullet.gif&quot;&gt;"></asp:TableCell>
                <asp:TableCell CssClass="Header" HorizontalAlign="Left" Text="Past Reports"></asp:TableCell>
                <asp:TableCell>
                    <asp:DropDownList runat="server" AutoPostBack="True" CssClass="InputForm" ID="ddlIssue" />
                    
                </asp:TableCell>
                <asp:TableCell Width="350px" ColumnSpan="2">
                    <asp:Label runat="server" ID="lblEmail"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>        
        <div class="DivTitleBar">
            <span class="Header Color1 Bold">Weekly Market Update</span>
        </div>    
    </asp:Panel> 
    
    
    
    
    <asp:Panel runat="server" ID="pnlNewMarketUpdate" Visible="false">
        <div style="margin-left: 20px; margin-right: 20px;">
            <asp:Table ID="Table2" runat="server" Height="152px" Width="100%">
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <span class="LinkNormal"><a href="http://www.theplasticsexchange.com" target="blank" style="float: left">
                            <asp:Image ID="imgLogo" runat="server" ImageAlign="Left" ImageUrl="~/Pics/Market_Update/tpelogo.gif" />
                            <span style="text-align: left; font-family: Arial Black; font-size: 220%; padding-left: 5;"><span style="color: Black">The</span><span style="color: Red">Plastics</span><span style="color: Black">Exchange</span><span style="color: Red">.</span><span style="color: Black">com</span></span> </a></span>
                        <br />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" CssClass="Header" HorizontalAlign="center">
                        <span style="font-size: 200%"><asp:Label runat="server" ID="lblUpdateHeader"></asp:Label> </span>
                        <br />
                        <asp:Label runat="server" ID="lblDate" CssClass="Header" Style="font-size: 125%"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <asp:Table runat="server" ID="Table3" Width="100%" CellPadding="10" CellSpacing="10">
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="50%" HorizontalAlign="left">
                        <asp:Label runat="server" ID="lblSummary" CssClass="ContentWR TextJustify"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell Width="50%"  VerticalAlign="Top">
                        <span style="float: left; text-decoration: underline; font-weight: bold">Spot Floor Summary</span>&nbsp;<asp:Label runat="server" ID="lblTotalVolume" CssClass="ContentWR Bold Color3 Underline"></asp:Label>
                        <asp:DataGrid ID="dgSpotFloorSummary" runat="server" Width="100%" CssClass="LinkNormal ContentWR" BorderWidth="0" BackColor="Black" CellSpacing="1" CellPadding="3" AutoGenerateColumns="False" HorizontalAlign="Center" EnableViewState="False">
                            <HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
                            <ItemStyle CssClass="LinkNormal LightGray" />
                            <AlternatingItemStyle CssClass="LinkNormal DarkGray" />
                            <Columns>
                                <asp:HyperLinkColumn DataNavigateUrlField="grade_id" ItemStyle-HorizontalAlign="Left" DataNavigateUrlFormatString="/spot/Spot_Floor.aspx?Filter={0}" DataTextField="GRADE" HeaderText="Resin">
                                    <HeaderStyle CssClass="ContentWR Bold Color2" HorizontalAlign="Left"></HeaderStyle>
                                </asp:HyperLinkColumn>
                                <asp:BoundColumn DataField="varsize" HeaderText="Total lbs" DataFormatString="{0:#,##}">
                                    <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                                    <ItemStyle Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="PriceLow" HeaderText="Low" DataFormatString="{0:0.##0}">
                                    <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                                    <ItemStyle Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="PriceHigh" HeaderText="High" DataFormatString="{0:0.##0}">
                                    <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                                    <ItemStyle Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn Visible="False" HeaderText="Price Range">
                                    <HeaderStyle CssClass="ContentWR Bold Color4"></HeaderStyle>
                                    <ItemStyle Wrap="False" CssClass="ContentWR Color4"></ItemStyle>
                                </asp:TemplateColumn>
                            </Columns>
                            <ItemStyle BorderColor="Black" BorderWidth="0px" />
                        </asp:DataGrid>
                        
                        <br />
                        
                       <font size="1">
                            All transactions are for actual delivery; they are cleared through The Plastics Exchange and are <span class="Color3">totally anonymous.</span>
                                <br />
                                <br />
                                All offers are subject to prior sale and credit approval.
                        </font>                                                    
                        
                    </asp:TableCell>

                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <a name="Polyethylene">
                            <span class="Content Color2 Bold Underline">Polyethylene</span>
                        </a>
                        <br />
                        <br />
                        <asp:Label runat="server" id="lblPEVL" CssClass="ContentWR Bold">Volume: </asp:Label>
                        <asp:Label runat="server" ID="lblPEVolume" CssClass="ContentWR"></asp:Label>
                        <br />
                        <asp:Label runat="server" id="lblPEPR" CssClass="ContentWR Bold">Price: </asp:Label>
                        
                        <asp:Label runat="server" ID="lblPEPrice" CssClass="ContentWR"></asp:Label><br />
                    </asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <asp:Label runat="server" ID="lblPEIntro" CssClass="ContentWR"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                
                
                <asp:TableRow ID="rowPEOld" runat="server" Visible="true">
                    <asp:TableCell HorizontalAlign="Left">
                    <center><asp:Label ID="Label4" CssClass="ContentWR Bold" runat="server" /><br /><br /></center>
                        <asp:Image runat="server" ID="imgPEMonthChart" width="400" height="300" />
                        <center>
                            <asp:Label ID="Label12" CssClass="ContentWR Bold" runat="server"  />
                            <br />
                        </center>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right">
                    <center><asp:Label ID="Label6" CssClass="ContentWR Bold" runat="server" />
                        <br /><br /></center>
                        <asp:Image runat="server" ID="imgPEYearChart" width="400" height="300" />
                        <center>
                            <asp:Label ID="Label7" CssClass="ContentWR Bold" runat="server"  />
                            <br />
                        </center>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow ID="rowPEChain" runat="server" Visible="false">
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <center>
                            <asp:Label ID="Label13" CssClass="ContentWR Bold" runat="server" Text="Polyethylene Chain" />
                            <br /><br />
                            <asp:Image runat="server" ID="imgPolyethyleneChain"  width="600" height="450"  />
                        </center>                                                
                        <br />                    
                    </asp:TableCell>
                </asp:TableRow>                
                
                
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label runat="server" ID="lblPEBody" CssClass="ContentWR TextJustify"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <a name="Polypropylene">
                            <span class="Content Color2 Bold Underline">Polypropylene</span>
                        </a> 
                        <br />
                        <br />
                        <asp:Label runat="server" id="lblPPVL" CssClass="ContentWR Bold">Volume: </asp:Label>
                        <asp:Label runat="server" ID="lblPPVolume" CssClass="ContentWR"></asp:Label>
                        <br />
                        <asp:Label runat="server" id="lblPPPR" CssClass="ContentWR Bold">Price: </asp:Label>
                        <asp:Label runat="server" ID="lblPPPrice" CssClass="ContentWR"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <asp:Label runat="server" ID="lblPPIntro" CssClass="ContentWR TextJustify"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                
                
                <asp:TableRow ID="rowPPOld" runat="Server">
                    <asp:TableCell HorizontalAlign="Left">
                    <center><asp:Label ID="Label3" CssClass="ContentWR Bold" runat="server" /><br /><br /></center>                   
                        <asp:Image runat="server" ID="imgPPMonthChart" width="400" height="300" />
                        <center>
                            <asp:Label ID="Label8" CssClass="ContentWR Bold" runat="server" Text="1 Month" />
                            <br />
                        </center>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right">
                    <center><asp:Label ID="Label1" CssClass="ContentWR Bold"  runat="server" /><br /><br /></center>
                    <asp:Image runat="server" ID="imgPPYearChart" width="400" height="300" />
                        <center>
                            <asp:Label ID="Label9" CssClass="ContentWR Bold" runat="server" Text="1 Year" />
                            <br />
                        </center>
                    </asp:TableCell>
                </asp:TableRow>
                
                
                
                <asp:TableRow ID="rowPPChain" runat="server" Visible="false">
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <center>
                            <asp:Label ID="Label14" CssClass="ContentWR Bold" runat="server" Text="Polypropylene Chain" />
                            <br /><br />
                            <asp:Image runat="server" ID="imgPolypropyleneChain"  width="600" height="450"  />
                        </center>                                                
                        <br />                    
                    </asp:TableCell>
                </asp:TableRow>    
                
                
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label runat="server" ID="lblPPBody" CssClass="ContentWR TextJustify"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                    <a name="Polystyrene">
                        <span class="ContentWR Color2 Bold Underline">Polystyrene</span>
                    </a>                     
                        <br />
                        <br />
                        <asp:Label runat="server" id="lblPSVL" CssClass="ContentWR Bold">Volume: </asp:Label>
                        <asp:Label runat="server" ID="lblPSVolume" CssClass="ContentWR"></asp:Label>
                        <br />
                        <asp:Label runat="server" id="lblPSPR" CssClass="ContentWR Bold">Price: </asp:Label>
                        <asp:Label runat="server" ID="lblPSPrice" CssClass="ContentWR"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                        <asp:Label runat="server" ID="lblPSIntro" CssClass="ContentWR"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                
                <asp:TableRow ID="rowPSOld" runat="server">
                    <asp:TableCell HorizontalAlign="Left">
                    <center> <asp:Label ID="Label2" CssClass="ContentWR Bold" runat="server" Text="HIPS Inj - 8 melt"></asp:Label><br /><br /></center>
                        <asp:Image runat="server" ID="imgPSMonthChart"  width="400" height="300" />
                        <center>
                            <asp:Label ID="Label10" CssClass="ContentWR Bold" runat="server" Text="1 Month" />
                            <br />
                        </center>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right">
                   <center> <asp:Label ID="Label5" CssClass="ContentWR Bold" runat="server" Text="HIPS Inj - 8 melt"></asp:Label><br /><br /></center>
                        <asp:Image runat="server" ID="imgPSYearChart"  width="400" height="300" />
                        <center>
                            <asp:Label ID="Label11" CssClass="ContentWR Bold" runat="server" Text="1 Year" />
                            <br />
                        </center>
                    </asp:TableCell>
                </asp:TableRow>
                
                        
                <asp:TableRow ID="rowPSChain" runat="server" Visible="false">
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <center>
                            <asp:Label ID="Label15" CssClass="ContentWR Bold" runat="server" Text="Polystyrene Chain" />
                            <br /><br />
                            <asp:Image runat="server" ID="imgPolystyreneChain" width="600" height="450" />
                        </center>                                                
                        <br />                    
                    </asp:TableCell>
                </asp:TableRow>   
                
                
                
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label runat="server" ID="lblPSBody" CssClass="ContentWR"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow CssClass="Content">
                    <asp:TableCell> 
                <br />
                Michael Greenberg, CEO
                <br />
                The Plastics Exchange
                <br />
                (312) 202-0002
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
           <hr width=100% />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow HorizontalAlign="Left">
                    <asp:TableCell CssClass="Content" Font-Size="XX-Small" ColumnSpan="2">
    Check out The Plastics Exchange before your buy or sell! We have live markets and prices on prime and widespec commodity grade resin in truckloads and railcars, and quality and delivery are guaranteed by our fully integrated credit and logistics - click here to register. We also have access to a wide range of wide-spec resin as well as foreign prime resin for international trade.<br />
    Call us at (800) 850-2380 or send an email and we'll source the resin for you. <br />
    If you are already a member, many thanks for your continued support. If not, join today! To make purchases, fax us your credit info at (312) 202-0174 or apply online at <a href="http://theplasticexchange.com">www.ThePlasticsExchange.com</a>.<br />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
           <hr width=100% />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow HorizontalAlign="Left">
                    <asp:TableCell CssClass="Content" ColumnSpan="2" Font-Size="XX-Small">
    Disclaimer: The information and data in this report is gathered from exchange observations as well as interactions with producers, distributors, brokers, and processors. These sources are considered reliable. The accuracy and completeness of this information is not guaranteed. Any decision to purchase or sell as a result of the opinions expressed in this report will be the full responsibility of the person authorizing such transaction. Our market updates are compiled with integrity and we hope that you find them of value.<br />
    Chart values reflect our asking prices of generic prime railcars delivered USA.<br />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </asp:Panel> 
    
    
    
   
        <asp:Panel runat="server" ID="pnlOldMarketUpdate" Visible="false">
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td align="left">
                    <br />
                    <asp:Label ID="lblOldContent" CssClass="Content Color2" Style="text-align: left" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    
    <asp:Panel runat="server" ID="pnl2009dMarketUpdate" Visible="false">
        <asp:Table ID="Table6" runat="server" Height="152px" Width="100%">
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2">
                    <span class="LinkNormal"><a href="http://www.theplasticsexchange.com" target="blank" style="float: left">
                        <asp:Image ID="imgLogo2" runat="server" ImageAlign="Left" ImageUrl="~/Pics/Market_Update/tpelogo.gif" />
                        <span style="text-align: left; font-family: Arial Black; font-size: 220%; padding-left: 5;"><span style="color: Black">The</span><span style="color: Red">Plastics</span><span style="color: Black">Exchange</span><span style="color: Red">.</span><span style="color: Black">com</span></span> </a></span>
                    <br />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" CssClass="Header" HorizontalAlign="center">
                    <span style="font-size: 200%"><asp:Label runat="server" ID="lblUpdateHeader2" Text="Market Update"></asp:Label> </span>
                    <br />
                    <asp:Label runat="server" ID="lblDate2" CssClass="Header" Style="font-size: 125%"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <table style="font-family:Verdana;" width="100%">
            <tr>
                <td style="vertical-align:top; text-align:justify" width="500px" >
                    <h3>Summary</h3>
                    <asp:Label  id="lblMarketSummary" runat="server" CssClass="ContentWR TextJustify"></asp:Label>
                </td>
                <td valign="top" align="center" style="padding-top:40px">
                 <%-- <div style="padding:10px;">
                      <b>Market Update - 
                      <asp:Label  id="lblMarketUpdateDate" runat="server" ForeColor="#da251d"></asp:Label>
                      </b>
                  </div>--%>
                  <table style="text-align:center; border-style:solid; border-color:Black; border-width:1px;  background-color:#DADADA"  width="430" >
                        <tr>
                            <td valign="bottom" align="left" style="padding-left:15px; padding-top:5px; font-size:13px;" width="200">
                               <b>Total Offers</b> 
                               <asp:Label ID="lblNewTotalVolume" ForeColor="#da251d" Font-Bold="true" runat="server" Text="Label"></asp:Label>                               
                            </td>
                            <td valign="bottom" align="center" style="padding-top:5px; font-size:13px; ">
                                <b>Spot</b>
                            </td>
                            <td valign="bottom" align="center" style="padding-top:5px; font-size:13px; ">
                                <b>Contract</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:10px" colspan="3">
                                <asp:datagrid BorderWidth="0" BackColor="#000000" CellSpacing="1" id="dg" runat="server" Width="400" CssClass="DataGrid"
                                    CellPadding="2" ShowFooter="False" AllowSorting="True" AutoGenerateColumns="False"  OnItemDataBound="ItemDataBound">
                                    <AlternatingItemStyle CssClass="LinkNormal DarkGray" BackColor="#BFBFBF"></AlternatingItemStyle>
                                    <ItemStyle CssClass="LinkNormal LightGray" BackColor="#DADADA"></ItemStyle>
                                    <HeaderStyle BackColor="#e77817" Font-Size="15px" Height="25"></HeaderStyle>
                                    <Columns>
                                        <asp:HyperLinkColumn DataNavigateUrlField="grade_id" ItemStyle-HorizontalAlign="Left"  DataNavigateUrlFormatString="/spot/Spot_Floor.aspx?Filter={0}" DataTextField="GRADE_NAME" HeaderText="Resin">
                                            <HeaderStyle CssClass="ContentWR Bold Color2" HorizontalAlign="Left"></HeaderStyle>
                                        </asp:HyperLinkColumn>
                                        <asp:BoundColumn DataField="weight" HeaderText="Total lbs" DataFormatString="{0:#,##}">
                                            <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                                            <ItemStyle  HorizontalAlign="Left" Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Price_Low" HeaderText="Low" DataFormatString="{0:$.##0}" ItemStyle-Font-Bold="true" >
                                            <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                                            <ItemStyle Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn  DataField="Price_High" HeaderText="High" DataFormatString="{0:$.##0}" ItemStyle-Font-Bold="true" >
                                            <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                                            <ItemStyle Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Price_Bid" HeaderText="Bid" DataFormatString="{0:$.##0}" ItemStyle-Font-Bold="true" >
                                            <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                                            <ItemStyle Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Price_Offer" HeaderText="Offer"  DataFormatString="{0:$.##0}" ItemStyle-Font-Bold="true" >
                                            <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                                            <ItemStyle Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn Visible="False" HeaderText="Price Range" >
                                            <HeaderStyle CssClass="ContentWR Bold Color4"></HeaderStyle>
                                            <ItemStyle Wrap="False" CssClass="ContentWR Color4" ></ItemStyle>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:datagrid>
                            </td>
                        </tr>
                    </table>                    
                    <div style="margin:10px; padding:5px; border-style:solid; border-width:1px; width:420px;">
                        <asp:Image  runat="server" ID="imgPE"  width="420" height="300"  />
                        <asp:Image  runat="server" ID="imgPP"  width="420" height="300"  />
                        <asp:Image  runat="server" ID="imgCrudeOil"  width="420" height="300" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td valign="top" style="vertical-align:top; text-align:justify" width="500px" >
                    <asp:Label id="lblH3PE" runat="server"><h3>Polyethylene</h3></asp:label>
                    <asp:Label id="lblPeSummary" runat="server" CssClass="ContentWR"></asp:Label>
                </td>
                <td valign="top" align="center" style="padding-top:40px">
                    <asp:Image runat="server" ID="PeMonth"  width="400" height="267" />
                    <asp:Image runat="server" ID="PeYear"  width="400" height="267" />
                    <asp:Image runat="server" ID="PeFiveYear"  width="400" height="267" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td valign="top" style="vertical-align:top; text-align:justify" width="500px" >
                    <asp:Label id="lblH3PP" runat="server"><h3>Polypropylene</h3></asp:label>
                    <asp:Label id="lblPpSummary" runat="server" CssClass="ContentWR"></asp:Label>
                </td>
                <td valign="top" align="center" style="padding-top:40px">
                    <asp:Image runat="server" ID="PpMonth"  width="400" height="267" />
                    <asp:Image runat="server" ID="PpYear"  width="400" height="267" />
                    <asp:Image runat="server" ID="PpFiveYear"  width="400" height="267" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    
    
    </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>
