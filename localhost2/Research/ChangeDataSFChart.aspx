<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="c#" Codebehind="ChangeDataSFChart.aspx.cs" AutoEventWireup="True" Inherits="localhost.Research.ChangeDataSFChart" MasterPageFile="~/MasterPages/menu.master" Title="Edit PV" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.master" %>

<asp:Content runat=server ContentPlaceHolderID="cphMain">
<table>
<tr>
<td>
    <br />
    <asp:DropDownList ID="ddlChart" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlChart_SelectedIndexChanged" CssClass="InputForm">
    </asp:DropDownList>
    <br />
    <br>   
        <asp:Button runat="server" ID="cmdExportToExcel" Text="Export to Excel"  CssClass="Content Color2" onclick="btnExportToExcel_Click" />
    <br />
    <br />
    <asp:DataGrid ID="dg" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="Header" ItemStyle-CssClass="Content" OnItemDataBound="dg_ItemDataBound">
        <Columns>
            <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ItemStyle-CssClass="LinkNormal"></asp:EditCommandColumn>
            <asp:BoundColumn DataField="DATE" ReadOnly="True" HeaderText="Date" DataFormatString="{0:d}"></asp:BoundColumn>
            <asp:BoundColumn DataField="PRICE_LOW" HeaderText="Low Price"></asp:BoundColumn>
            <asp:BoundColumn DataField="PRICE_HIGH" HeaderText="High Price"></asp:BoundColumn>
            <asp:BoundColumn DataField="Weight" DataFormatString="{0:#,###}" HeaderText="Weight"></asp:BoundColumn>
            <asp:BoundColumn DataField="weightavg" HeaderText="Average" ></asp:BoundColumn>
        </Columns>
    </asp:DataGrid>
</td>
<td valign="top">
    <br />
<asp:Image runat="server" ID="imgChart" Visible="true" /><br /><br />
<asp:Image runat="server" ID="imgChart1" Visible="true" /><br /><br />
<asp:Image runat="server" ID="imgChart2" Visible="true" /><br /><br />
    <br /><br />
<dotnet:Chart id="Chart" runat="server" Visible="false">
<TitleBox Position="Left">
</TitleBox>

<DefaultLegendBox Padding="4" Visible="True" CornerBottomRight="Cut">

<HeaderEntry SortOrder="-1" Visible="False" Value="Value" Name="Name">

<DividerLine Color="Gray">
</DividerLine>

<LabelStyle Font="Arial, 8pt, style=Bold">
</LabelStyle>

</HeaderEntry>

</DefaultLegendBox>

<DefaultTitleBox Visible="True">

</DefaultTitleBox>

<DefaultElement>

<DefaultSubValue>

<Line Color="93, 28, 59" Length="4">
</Line>

</DefaultSubValue>

</DefaultElement>
</dotnet:Chart>
    <asp:Button ID="btnEdit" runat="server" OnClick="btnEdit_Click" Text="Edit All Fridays" />&nbsp;

</td>
</tr>
</table>
     
</asp:Content>
