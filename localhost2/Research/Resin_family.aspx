﻿<%@ Page Language="C#" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Data.SqlClient" %>
<script runat="server">

    public void Page_Load(object sender, EventArgs e){

          SqlConnection conn;
          conn = new SqlConnection(Application["DBconn"].ToString());
          conn.Open();
          ArrayList valuesPolyethylene = new ArrayList();
          ArrayList valuesPolypropylene = new ArrayList();
          ArrayList valuesPolystyrene = new ArrayList();
             if (!IsPostBack){

                SqlCommand cmdPolyethylene;
                SqlDataReader dtr;


                cmdPolyethylene = new SqlCommand("select CONT_ID, cont_labl from contract where cont_fmly='Polyethylene'",conn);
                dtr = cmdPolyethylene.ExecuteReader();
                 while (dtr.Read()){
                 valuesPolyethylene.Add(new PositionData("<a href=../Research/Dashboard.aspx?Cont_Id="+dtr["cont_Id"].ToString()+">"+dtr["cont_labl"].ToString()+"</a>"));
                 }

                Repeater1.DataSource = valuesPolyethylene;
                     Repeater1.DataBind();
                dtr.Close();


                cmdPolyethylene = new SqlCommand("select CONT_ID, cont_labl from contract where cont_fmly='Polypropylene'",conn);
                dtr = cmdPolyethylene.ExecuteReader();
                 while (dtr.Read()){
                 valuesPolypropylene.Add(new PositionData("<a href=../Research/Dashboard.aspx?Cont_Id="+dtr["cont_Id"].ToString()+">"+dtr["cont_labl"].ToString()+"</a>"));
                 }

                Repeater2.DataSource = valuesPolypropylene;
                Repeater2.DataBind();
                dtr.Close();


                cmdPolyethylene = new SqlCommand("select CONT_ID, cont_labl from contract where cont_fmly='Polystyrene'",conn);
                dtr = cmdPolyethylene.ExecuteReader();
                 while (dtr.Read()){
                 valuesPolystyrene.Add(new PositionData("<a href=../Research/Dashboard.aspx?Cont_Id="+dtr["cont_Id"].ToString()+">"+dtr["cont_labl"].ToString()+"</a>"));
                 }

                Repeater3.DataSource = valuesPolystyrene;
                Repeater3.DataBind();
                dtr.Close();


             }

                   conn.Close();

        }

        public class PositionData {
                  private string name;
                  public PositionData(string name) {
                     this.name = name;
                  }

                  public string Name {
                     get {
                        return name;
                     }
                  }


         }

</script>
<form runat="server">
<TPE:Template PageTitle="" Runat="Server" />
<TPE:Web_Box Heading="What is Polyethylene?" Runat="Server" />
    <table width=600 align=center>
        <tr>
            <td width=300 valign=top>
            <p align=justify><font size=3>
            A polymerized ethylene resin. Polyethylene’s high molecular weight is a result of pressurization of ethylene gas. Because of its durability, even under extreme cold temperatures, Polyethylene is commonly used in many household containers including cleansers. Some of its more common uses are in packaging, toys and grocery bags. Polyethylene has good flexibility and strength making it the most popular plastic in the world.
            </td>
            <td width=300 valign=top>
		          <table width=300 align=center valign=top><tr><td>
			         <fieldset>
			         <table  border="0">
			         <tbody>
			             <tr>
				            <td align="left" valign=top>
				                <strong>Traded Polyethylene Grades</strong></td>

			             </tr>
			             <tr>
			             <td>
			             <asp:Repeater id=Repeater1 runat="server">
			                         <HeaderTemplate>
			                             <table border=0>

			                         </HeaderTemplate>

			                         <ItemTemplate>
			                             <tr>
			                             <td><font size=2><li> <%# DataBinder.Eval(Container.DataItem, "Name") %> </td>

			                             </tr>
			                         </ItemTemplate>

			                         <FooterTemplate>
			                             </table>
			                         </FooterTemplate>

			                     </asp:Repeater>


			             </td>
			             </tr>
			             </table>

			         </fieldset>
		          </td>
		     </tr>
		</table>

    </td>
  </tr>
</table>
<TPE:Web_Box Footer="true" Runat="Server" />

<BR><BR>

<TPE:Web_Box Heading="What is Polypropylene?" Runat="Server" />
<table width=600 align=center><tr><td width=300 valign=top>
<p align=justify><font size=3>
Polypropylene is a vinyl polymer with very high durability under both hot and cold conditions. This makes Polypropylene ideal for dishwasher-safe containers as well as containers for hot-fill liquids such as coffee and soups. Polypropylene can be used as either a plastic or a fiber. As a fiber it is commonly used in outdoor carpeting because of its durability and minimal absorption properties.
</td>
<td width=300 valign=top>
		<table width=300 align=center valign=top><tr><td>
			<fieldset>
			<table  border="0">
			<tbody>
			    <tr>
				<td align="left" valign=top>
				    <strong>Available Resins</strong></td>

			    </tr>
			    <tr>
			    <td>
			    <asp:Repeater id=Repeater2 runat="server">
			              <HeaderTemplate>
			                 <table border=0>

			              </HeaderTemplate>

			              <ItemTemplate>
			                 <tr>
			                    <td> <font size=2><li><%# DataBinder.Eval(Container.DataItem, "Name") %> </td>

			                 </tr>
			              </ItemTemplate>

			              <FooterTemplate>
			                 </table>
			              </FooterTemplate>

			           </asp:Repeater>


			    </td>
			    </tr>
			  </table>

			</fieldset>
		</td></tr></table>

</td></tr>
</table>

<TPE:Web_Box Footer="true" Runat="Server" />



<BR><BR>
<TPE:Web_Box Heading="What is Polystyrene?" Runat="Server" />
<table width=600 align=center><tr><td width=300 valign=top>
<p align=justify><font size=3>
Polystyrene is a vinyl polymer that can be foamed or rigid. Polystyrene has a very low melting point, 270’C. It is one of the more versatile plastics and can be used for hard plastic housing and pieces found in automobiles and kitchen appliances or foam packaging items and containers.
</td>
<td width=300 valign=top>
		<table width=300 align=center valign=top><tr><td>
			<fieldset>
			<table  border="0">
			<tbody>
			    <tr>
				<td align="left" valign=top>
				    <strong>Available Resins</strong></td>

			    </tr>
			    <tr>
			    <td>
			    <asp:Repeater id=Repeater3 runat="server">
			              <HeaderTemplate>
			                 <table border=0>

			              </HeaderTemplate>

			              <ItemTemplate>
			                 <tr>
			                    <td> <font size=2><li><%# DataBinder.Eval(Container.DataItem, "Name") %> </td>

			                 </tr>
			              </ItemTemplate>

			              <FooterTemplate>
			                 </table>
			              </FooterTemplate>

			           </asp:Repeater>


			    </td>
			    </tr>
			  </table>

			</fieldset>
		</td></tr></table>

</td></tr>
</table>
<TPE:Web_Box Footer="true" Runat="Server" />
<TPE:Template Footer="true" Runat="Server" />

</form>
