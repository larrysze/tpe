using System;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;
using dotnetCHARTING;
using System.Collections.Specialized;
using System.IO;
namespace localhost.Administrator
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private string contractID = "1";
        private string period = "1Y";
        private string size = "1";
        private string market = "D";
        private string connectionStringDB;
        private string chartsFolder;
        private string currentDate;

        public string ContractID
        {
            get
            {
                return contractID;
            }

            set
            {
                contractID = value;
                ViewState["contractID"] = value;
            }
        }
        public string Period
        {
            get
            {
                return period;
            }

            set
            {
                period = value;
                ViewState["period"] = value;
            }
        }
        [DefaultValue("1"),
        Description("Size of the charts. Use 1 to small and 2 to large")]
        public string Size
        {
            get
            {
                return size;
            }

            set
            {
                if ((value.ToString() == "1") || (value.ToString() == "2"))
                {
                    size = value;
                    ViewState["size"] = value;
                }
            }
        }

        [DefaultValue("D"),
        Description("Type of market. Use D to Domestic and I to International")]
        public string Market
        {
            get
            {
                return market;
            }

            set
            {
                if ((value.ToString() == "D") || (value.ToString() == "I"))
                {
                    market = value;
                    ViewState["market"] = value;
                }
            }
        }

        private string PrefixFile
        {
            get
            {
                string prefix = "";
                if (size == "2") prefix = prefix + "B_";
                if (market == "I") prefix = prefix + "I_";
                return prefix;
            }
        }

        private int tdWidth
        {
            get
            {
                if (Size == "1")//small
                    return 352;
                else //large
                    return 509;
            }
        }

        private int tabWidth1m
        {
            get
            {
                if (Size == "1")//small
                    return 62;
                else //large
                    return 82;
            }
        }

        private int tabWidth1y
        {
            get
            {
                if (Size == "1")//small
                    return 62;
                else //large
                    return 71;
            }
        }

        private int tabWidth5y
        {
            get
            {
                if (Size == "1")//small
                    return 53;
                else //large
                    return 69;
            }
        }

        private int tabWidth10y
        {
            get
            {
                if (Size == "1")//small
                    return 72;
                else //large
                    return 72;
            }
        }

        private int backgroundWidth
        {
            get
            {
                if (Size == "1")//small
                    return 100;
                else //large
                    return 206;
            }
        }

 
        protected void Page_Load(object sender, EventArgs e)
        {
            connectionStringDB = Application["DBConn"].ToString();
            chartsFolder = Server.MapPath("/Research/temp");
            
            if (!IsPostBack)
            {
                loadDDL();
                
            }
        }

        private void loadDDL()
        {

            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, "SELECT * FROM CONTRACT where cont_enabled=1 order by cont_ordr ");
                while (dtr.Read())
                {
                    ddlChart.Items.Add(new ListItem(dtr["CONT_LABL"].ToString(), dtr["CONT_ID"].ToString()));
                    ddlChart2.Items.Add(new ListItem(dtr["CONT_LABL"].ToString(), dtr["CONT_ID"].ToString()));
                    ddlChart3.Items.Add(new ListItem(dtr["CONT_LABL"].ToString(), dtr["CONT_ID"].ToString()));
                }

                ddlChart.Items[0].Selected = true;
            }

        }
        #region Methods to Create new Charts

        protected dotnetCHARTING.Chart ChartObj;

        private void CreateChart(int width, int height, DateTime moment)
        {
            ChartObj = new dotnetCHARTING.Chart();
            ChartObj.ChartArea.ClearColors();

            ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            ChartObj.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);

            ChartObj.XAxis.TimeInterval = TimeInterval.Week;

            string fileName2 = "";
            //System.Console.WriteLine("creation bitmap");
            string period1 = "";

            ChartObj.TempDirectory = chartsFolder;

            ChartObj.Width = width;
            ChartObj.Height = height;

            for (int i = 0; i < 4; ++i)
            {
                if (i == 0)
                {
                    period1 = "1M";
                }
                else if (i == 1)
                {
                    period1 = "1Y";
                }
                else if (i == 2)
                {
                    period1 = "5Y";
                }
                else if (i == 3)
                {
                    period1 = "10Y";
                }

                SeriesCollection timSC = new SeriesCollection();
                int nulls=0;
                
                if (!ddlChart.SelectedValue.Equals("None"))
                {
                    timSC.Add(getData(Convert.ToInt32(ddlChart.SelectedValue.ToString()), period1));
                    nulls++;
                }
                if (!ddlChart2.SelectedValue.Equals("None"))
                {
                    timSC.Add(getData(Convert.ToInt32(ddlChart2.SelectedValue.ToString()), period1));
                    nulls++;
                }
                if (!ddlChart3.SelectedValue.Equals("None"))
                {
                    timSC.Add(getData(Convert.ToInt32(ddlChart3.SelectedValue.ToString()), period1));
                    nulls++;
                }

                for (int k = 0; k < nulls; k++)
                {
                    if (k == 0) timSC[k].DefaultElement.Color = Color.FromArgb(255, 99, 49);
                    if (k == 1) timSC[k].DefaultElement.Color = Color.FromArgb(255, 255, 0);
                    if (k == 2) timSC[k].DefaultElement.Color = Color.FromArgb(49, 255, 49);

                    timSC[k].LegendEntry.Name = Convert.ToString(k+1);

                    //SeriesCollection timSC = new SeriesCollection();
                    //timSC.Add(getData(Convert.ToInt32(ddlChart.SelectedValue.ToString()), period1, "1"));
                    //timSC.Add(getData(Convert.ToInt32(ddlChart2.SelectedValue.ToString()), period1, "2"));
                    //timSC.Add(getData(Convert.ToInt32(ddlChart3.SelectedValue.ToString()), period1, "3"));

                    //timSC[0].DefaultElement.Color = Color.FromArgb(49, 255, 49);
                    //timSC[1].DefaultElement.Color = Color.FromArgb(255, 255, 0);
                    //timSC[2].DefaultElement.Color = Color.FromArgb(255, 99, 49);

                    //ChartObj.SeriesCollection.Add(timSC);

                    //timSC[0].LegendEntry.Name = "1";
                    //timSC[1].LegendEntry.Name = "2";
                    //timSC[2].LegendEntry.Name = "3";

                }
                ChartObj.SeriesCollection.Add(timSC);

                ChartObj.FileName = "Chart_" + ddlChart.SelectedValue.ToString() + ddlChart2.SelectedValue.ToString() + ddlChart3.SelectedValue.ToString() + "_" + period1 + "_" + moment.Month.ToString() + moment.Day.ToString();

                ChartObj.FileManager.ImageFormat = ImageFormat.Png;
                //                            ChartObj.OverlapFooter = true;
                Bitmap bmp1 = ChartObj.GetChartBitmap();

                fileName2 = ChartObj.FileManager.SaveImage(bmp1);

                ChartObj.SeriesCollection.Clear();
            }                   
        }

        private SeriesCollection getData(int iContract, string period)
        {
            SqlConnection conn;
            SqlDataReader dtrData;
            SqlCommand cmdData;
            string strSQL;
            conn = new SqlConnection(this.connectionStringDB);
            conn.Open();
            int iPreRead = 0; // the number of preloaded items
            int iInterval = 0; // the space between each items

            int range = 1;
            int count = 0;
            string tmpSQL = "";

            switch (period)
            {

                default:
                    strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE between DATEADD(yy,-5,@CURRENTDATE) and @CURRENTDATE";
                    iPreRead = 2;
                    iInterval = 0;
                    range = 4;
                    tmpSQL = "select COUNT(*) from HISTORICAL_PRICES WHERE CONT_ID=@CONTRACT AND PRICE_DATE between DATEADD(yy,-5,@CURRENTDATE) and @CURRENTDATE";
                    cmdData = new SqlCommand(tmpSQL, conn);
                    cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                    cmdData.Parameters.AddWithValue("@CURRENTDATE", currentDate);
                    count = Convert.ToInt32(cmdData.ExecuteScalar());

                    break;
                //case "1Y":
                //	strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-1,getdate()) ";
                //	iInterval = 0;
                //	iPreRead = 0;
                //	break;
                case "5Y":
                    strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE between DATEADD(yy,-5,@CURRENTDATE) and @CURRENTDATE ";
                    iInterval = 0;
                    iPreRead = 2;
                    range = 4;
                    tmpSQL = "select COUNT(*) from HISTORICAL_PRICES WHERE CONT_ID=@CONTRACT AND PRICE_DATE between DATEADD(yy,-5,@CURRENTDATE) and @CURRENTDATE";
                    cmdData = new SqlCommand(tmpSQL, conn);
                    cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                    cmdData.Parameters.AddWithValue("@CURRENTDATE", currentDate);
                    count = Convert.ToInt32(cmdData.ExecuteScalar());

                    break;
                case "10Y":
                    strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-10,@CURRENTDATE) ";
                    iInterval = 2;
                    iPreRead = 5;
                    range = 10;
                    tmpSQL = "select COUNT(*) from HISTORICAL_PRICES WHERE CONT_ID=@CONTRACT AND PRICE_DATE between DATEADD(yy,-10,@CURRENTDATE) and @CURRENTDATE";
                    cmdData = new SqlCommand(tmpSQL, conn);
                    cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                    cmdData.Parameters.AddWithValue("@CURRENTDATE", currentDate);
                    count = Convert.ToInt32(cmdData.ExecuteScalar());

                    break;
                case "Forward":
                    strSQL = "Select FWD_ID, DATE= LEFT(FWD_MNTH,3)+' '+RIGHT(FWD_YEAR,2), PRICE = (SELECT RIGHT(CONVERT(varchar,MAX(OFFR_PRCE)),4) FROM OFFER WHERE OFFR_CONT='" + iContract.ToString() + "' AND OFFR_MNTH=FWD_ID AND OFFR_SIZE='1') From FWDMONTH WHERE FWD_ACTV ='1' ORDER BY FWD_ID ASC";
                    break;
            }

            strSQL = "select PRICE,PRICE_DATE AS DATE from HISTORICAL_PRICES " + strSQL + " ORDER BY PRICE_DATE";
            if (period.Equals("1M"))
            {
                // needs to overwrite string completely if it is one month
                strSQL = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE between DATEADD(dd,-30,@CURRENTDATE) and @CURRENTDATE  ORDER BY DATE";
                iPreRead = 0;
                iInterval = 0;
                range = 4;
                tmpSQL = "select count(*) from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE between DATEADD(dd,-30,@CURRENTDATE) and @CURRENTDATE";
                cmdData = new SqlCommand(tmpSQL, conn);
                cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                cmdData.Parameters.AddWithValue("@CURRENTDATE", currentDate);
                count = Convert.ToInt32(cmdData.ExecuteScalar());
            }
            //			if (period.Equals("6M"))
            //			{
            //				// needs to overwrite string completely if it is one month
            //				strSQL = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(mm,-6,getdate())  ORDER BY DATE";
            //				iPreRead = 12;
            //				iInterval = 6;
            //			}
            if (period.Equals("1Y"))
            {
                // needs to overwrite string completely if it is one month
                strSQL = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE between DATEADD(mm,-12,@CURRENTDATE) and @CURRENTDATE AND DATEPART(Dw,DATE)=DATEPART(Dw, (select max(date) from export_price WHERE CONT_ID=@CONTRACT)) ORDER BY DATE";
                iPreRead = 0; //24;
                iInterval = 0; //6;
                range = 4;

                tmpSQL = "select COUNT(*) from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE between DATEADD(mm,-12,@CURRENTDATE) and @CURRENTDATE AND DATEPART(Dw,DATE)=DATEPART(Dw, (select max(date) from export_price WHERE CONT_ID=@CONTRACT))";
                cmdData = new SqlCommand(tmpSQL, conn);
                cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                cmdData.Parameters.AddWithValue("@CURRENTDATE", currentDate);
                count = Convert.ToInt32(cmdData.ExecuteScalar());

            }


            cmdData = new SqlCommand(strSQL, conn);
            //cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
            cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
            cmdData.Parameters.AddWithValue("@CURRENTDATE", currentDate);
            dtrData = cmdData.ExecuteReader();
            SeriesCollection SC = new SeriesCollection();

            for (int k = 0; k < iPreRead; k++)
            {
                if (dtrData.Read())
                { }
            }
            int i = 1;
            int add = 0;

            if (count != 0)
            {
                add = count % range;
            }


            Series s = new Series();

            while (dtrData.Read())
            {
                if (dtrData["DATE"] != DBNull.Value && dtrData["PRICE"] != DBNull.Value)
                {
                    Element e = new Element();
                    //					e.ToolTip = dtrData["PRICE"].ToString();
                    //					e.SmartLabel.DynamicDisplay = false;
                    //					e.SmartLabel.DynamicPosition = true;
                    //					e.ShowValue = false;
                    //e.Name = dtrData["DATE"].ToString();

                    if (((i - add) % range) == 0)
                    {
                        e.Name = Convert.ToDateTime(dtrData["DATE"]).ToString("MMM dd\r\nyyy");
                        //						e.SmartLabel.Text = dtrData["PRICE"].ToString();
                        e.SmartLabel.Color = Color.White;
                        e.XDateTime = (DateTime)dtrData["DATE"];
                    }
                    else
                    {
                        //e.Name = "";
                        e.SmartLabel.Text = "";
                    }

                    i++;


                    //e.Color = Color.Blue;//Color.FromArgb(255, 203, 1);
                    //					e.AxisMarker.Label.Color = Color.Red;

                    e.YValue = Convert.ToDouble(dtrData["PRICE"]);
                    //                    e.Hotspot.ToolTip = Convert.ToDouble(dtrData["PRICE"]).ToString();
                    /* wanted to have 'hand-cursor' while over values
                    e.Hotspot.Attributes.Custom.Add("onmouseover", "this.style.cursor='hand'");
                    e.Hotspot.Attributes.Custom.Add("onmouseout", "this.style.cursor='pointer'");
                    */
                    //                    s.Element.ShowValue = true;
                    s.Elements.Add(e);
                }

                if (period.Equals("6M") || period.Equals("1Y"))// || period.Equals("10Y")
                {
                    for (int k = 0; k < iInterval; k++)
                    {
                        if (dtrData.Read())
                        { }
                    }
                }
            }
            SC.Add(s);
      
            //ChartObj.Depth = 15;
            // Set 3D
            ChartObj.Use3D = false;

            ChartObj.ChartArea.DefaultElement.Color = Color.Orange;
            ChartObj.ChartArea.DefaultSeries.Line.Color = Color.Orange;

            ChartObj.ChartArea.DefaultSeries.Line.Width = 2;


            ChartObj.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
            ChartObj.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartObj.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
            ChartObj.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);


            ChartObj.ChartArea.XAxis.DefaultTick.Line.Color = Color.FromArgb(254, 0, 0); //red
            ChartObj.ChartArea.YAxis.DefaultTick.Line.Color = Color.FromArgb(254, 0, 0); //red

            ChartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;

            ChartObj.ChartArea.XAxis.Line.Width = 4;
            ChartObj.ChartArea.YAxis.Line.Width = 2;

            ChartObj.YAxis.Interval = 0.01;

            ChartObj.ChartArea.XAxis.LabelRotate = true;

            ChartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartObj.ChartArea.XAxis.TickLabelAngle = 90;

            ChartObj.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartObj.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);

            // Set a default transparency
            ChartObj.DefaultSeries.DefaultElement.Transparency = 20;

            // Set color of axis lines
            Axis AxisObj = new Axis();
            AxisObj.Line.Color = Color.FromArgb(255, 255, 0, 0);

            ChartObj.DefaultAxis = AxisObj;

            ChartObj.MarginLeft = 0;
            ChartObj.MarginRight = 0;
            ChartObj.MarginTop = 0;
            ChartObj.MarginBottom = 0;

            // Set the Default Series Type
            ChartObj.DefaultSeries.Type = SeriesType.Line;
            ChartObj.LegendBox.Position = LegendBoxPosition.None;

            // Set the y Axis Scale
            ChartObj.ChartArea.YAxis.Scale = Scale.Range;

            ChartObj.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            ChartObj.DefaultSeries.DefaultElement.Marker.Size = 6;
            ChartObj.DefaultSeries.DefaultElement.Marker.Color = Color.White;//Color.FromArgb( 255, 255, 153, 9);
            ChartObj.XAxis.Label.Color = Color.White;

            //box around the chartarea
            ChartObj.ChartArea.Line.Color = Color.FromArgb(255, 255, 0, 0);//(254, 0, 0)
            ChartObj.ChartArea.Line.Width = 2;

            ChartObj.ChartArea.Background.Color = Color.FromArgb(0, 0, 0, 0);
            ChartObj.ChartArea.Background.Mode = BackgroundMode.Color;
            ChartObj.Background.Color = Color.FromArgb(0, 0, 0, 0);

            ChartObj.TitleBox.Position = TitleBoxPosition.FullWithLegend;
            ChartObj.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;

            ChartObj.LegendBox.Template = "%Icon%Name";
            ChartObj.TitleBox.Background.Color = Color.Orange;


            return (SC);
        }

        #endregion


        //private void GetFiles(DirectoryInfo di, string searchPattern, ref ArrayList MyFiles)
        //{
        //    foreach (FileInfo fi in di.GetFiles(searchPattern))
        //    {
        //        MyFiles.Add(fi.FullName);
        //    }
        //}


        protected void button_click(object sender, EventArgs e)
        {
            if (!Directory.Exists(Server.MapPath("/Research/temp")))
            {
                Directory.CreateDirectory(Server.MapPath("/Research/temp"));
            }

            string path = Server.MapPath("/Research/temp");
            string[] files = Directory.GetFiles(path);
            foreach (string file in files)
                File.Delete(file);


            currentDate = GMDatePicker1.Date.ToString();
 
            DateTime moment = Convert.ToDateTime(currentDate);
            

            CreateChart(390, 280, moment);
            imgChart.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/temp/CHART_" + ddlChart.SelectedValue.ToString() + ddlChart2.SelectedValue.ToString() + ddlChart3.SelectedValue.ToString() + "_1M_" + moment.Month.ToString() + moment.Day.ToString() + ".png";
            imgChart1.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/temp/CHART_" + ddlChart.SelectedValue.ToString() + ddlChart2.SelectedValue.ToString() + ddlChart3.SelectedValue.ToString() + "_1Y_" + moment.Month.ToString() + moment.Day.ToString() + ".png";
            imgChart2.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/temp/CHART_" + ddlChart.SelectedValue.ToString() +  ddlChart2.SelectedValue.ToString() + ddlChart3.SelectedValue.ToString() + "_5Y_" + moment.Month.ToString() + moment.Day.ToString() + ".png";
            imgChart3.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/temp/CHART_" + ddlChart.SelectedValue.ToString() + ddlChart2.SelectedValue.ToString() + ddlChart3.SelectedValue.ToString() + "_10Y_" + moment.Month.ToString() + moment.Day.ToString() + ".png";
            imgChart.Visible = true;
            imgChart1.Visible = true;
            imgChart2.Visible = true;
            imgChart3.Visible = true;
        }


        protected void ddlChart_indexChanged(object sender, EventArgs e)
        {
            imgChart.Visible = false;
            imgChart1.Visible = false;
            imgChart2.Visible = false;
            imgChart3.Visible = false;

        }

        protected void ddlChart2_indexChanged(object sender, EventArgs e)
        {
            imgChart.Visible = false;
            imgChart1.Visible = false;
            imgChart2.Visible = false;
            imgChart3.Visible = false;

        }

        protected void ddlChart3_indexChanged(object sender, EventArgs e)
        {
            imgChart.Visible = false;
            imgChart1.Visible = false;
            imgChart2.Visible = false;
            imgChart3.Visible = false;

        }

        protected void Calendar_changed(object sender, EventArgs e)
        {

            imgChart.Visible = false;
            imgChart1.Visible = false;
            imgChart2.Visible = false;
            imgChart3.Visible = false;
        }
    }
}
