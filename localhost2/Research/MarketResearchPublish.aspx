<%@ Register TagPrefix="mbdb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.DefaultButtons" %>
<%@ Page Language="c#" Codebehind="MarketResearchPublish.aspx.cs" AutoEventWireup="True" Inherits="localhost.Research.MarketResearchPublish" Title="Market Research" MasterPageFile="~/MasterPages/Menu.Master" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content ID="content5" runat="server" ContentPlaceHolderID="cphMain">
   
    <table style="font-family:Verdana;" width="100%">
       <tr>
           <td style="padding:10px">
                <asp:datagrid BorderWidth="0" BackColor="#000000" CellSpacing="1" id="dg" runat="server" Width="600" CssClass="DataGrid"
                    CellPadding="2" ShowFooter="False" AllowSorting="True" AutoGenerateColumns="False" OnItemDataBound="OnItemDataBinding">
                    <AlternatingItemStyle CssClass="LinkNormal DarkGray" BackColor="#BFBFBF"></AlternatingItemStyle>
                    <ItemStyle CssClass="LinkNormal LightGray" BackColor="#DADADA"></ItemStyle>
                    <HeaderStyle BackColor="#e77817" Font-Size="15px" Height="25"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn ItemStyle-HorizontalAlign="Left" DataField="grade_id" HeaderText="Id" ReadOnly="True" >
                            <HeaderStyle CssClass="ContentWR Bold Color2" HorizontalAlign="Left"></HeaderStyle>
                        </asp:BoundColumn>
                        
                        <asp:BoundColumn ItemStyle-HorizontalAlign="Left" DataField="grade_name" HeaderText="Resin" ReadOnly="True" >
                            <HeaderStyle CssClass="ContentWR Bold Color2" HorizontalAlign="Left"></HeaderStyle>
                        </asp:BoundColumn>
                        
                        <asp:TemplateColumn HeaderText="Total lbs" >
                         <HeaderStyle CssClass="ContentWR Bold Color2" HorizontalAlign="Left"></HeaderStyle>
                         <ItemTemplate>
                            <asp:TextBox ID="TextBox4" EnableViewState="True" runat="server" Width="100px" 
                             Text='<%# DataBinder.Eval(Container, "DataItem.varsize") %>'> 
                             </asp:TextBox>
                         </ItemTemplate>
                        </asp:TemplateColumn>
                        
                         <asp:TemplateColumn HeaderText="Low" >
                         <HeaderStyle CssClass="ContentWR Bold Color2" HorizontalAlign="Left"></HeaderStyle>
                         <ItemTemplate>
                            <asp:TextBox EnableViewState="True" runat="server" Width="40px"
                             Text='<%# DataBinder.Eval(Container, "DataItem.offer_low") %>'>
                             </asp:TextBox>
                         </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="High" >
                        <HeaderStyle CssClass="ContentWR Bold Color2" HorizontalAlign="Left"></HeaderStyle>
                         <ItemTemplate>
                            <asp:TextBox ID="TextBox1" EnableViewState="True" runat="server" Width="40px"
                             Text='<%# DataBinder.Eval(Container, "DataItem.offer_high") %>'>
                             </asp:TextBox>
                         </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Bid" >
                        <HeaderStyle CssClass="ContentWR Bold Color2" HorizontalAlign="Left"></HeaderStyle>
                         <ItemTemplate>
                            <asp:TextBox ID="TextBox2" EnableViewState="True" runat="server" Width="40px"
                             Text='<%# DataBinder.Eval(Container, "DataItem.bid") %>'>
                             </asp:TextBox>
                         </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Offer" >
                        <HeaderStyle CssClass="ContentWR Bold Color2" HorizontalAlign="Left"></HeaderStyle>
                         <ItemTemplate>
                            <asp:TextBox ID="TextBox3" EnableViewState="True" runat="server"  Width="40px"
                             Text='<%# DataBinder.Eval(Container, "DataItem.offr") %>'>
                             </asp:TextBox>
                         </ItemTemplate>
                        </asp:TemplateColumn>
                        
<%--
                        <asp:BoundColumn DataField="offer_low" HeaderText="Low" 
                            <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                            <ItemStyle Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn  DataField="offer_high" HeaderText="High" DataFormatString="{0:$.##0}" ItemStyle-Font-Bold="true" >
                            <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                            <ItemStyle Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="bid" HeaderText="Bid" DataFormatString="{0:$.##0}" ItemStyle-Font-Bold="true" >
                            <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                            <ItemStyle Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="offr" HeaderText="Offer"  DataFormatString="{0:$.##0}" ItemStyle-Font-Bold="true" >
                            <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                            <ItemStyle Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="Price Range" >
                            <HeaderStyle CssClass="ContentWR Bold Color4"></HeaderStyle>
                            <ItemStyle Wrap="False" CssClass="ContentWR Color4" ></ItemStyle>
                        </asp:TemplateColumn>--%>
                    </Columns>
                </asp:datagrid>
                <asp:Label ID="lblTotalLbs" runat="server" ></asp:Label><br />
                Date:<asp:DropDownList ID="ddlIssue" runat="server" OnSelectedIndexChanged="ddlIssue_SelectedIndexChanged"></asp:DropDownList>
                <asp:Button ID="btnRefresh" runat="server" Text="Refresh Table" OnClick="Click_btnRefresh"  />
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top; padding-top:10px;">
                <h3>Summary</h3>
                <asp:TextBox ID="txtSummary" Width="800" Height="100" runat="server" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top; padding-top:10px;" >
                <h3>Polyethylene</h3>
                <asp:TextBox ID="txtPe" Width="800" Height="100" runat="server" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top; padding-top:10px;">
                <h3>Polypropylene</h3>
                <asp:TextBox ID="txtPp" Width="800" Height="100" runat="server" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Regarding Date: <asp:TextBox ID="txtSaveDate" runat="server"></asp:TextBox>
                <br/><br/><input class="Content Color2" id="Submit1" type="submit" value="Upload" name="Submit1" runat="server" onserverclick="Submit1_ServerClick" /><br /><br />
            </td>
        </tr>
    </table>
    
</asp:Content>
