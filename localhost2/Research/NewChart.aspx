﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewChart.aspx.cs" Inherits="localhost.Administrator.WebForm1" MasterPageFile="~/MasterPages/menu.master" Title="Chart" %>

<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="сс" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="cc1" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ MasterType VirtualPath="~/MasterPages/Menu.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphMain">
    <cc1:ScriptManager id="sm" runat="server">
    </cc1:ScriptManager>
    <cc1:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE border = "0"><TBODY><TR>
<TD vAlign=top>
<table width="250" border="0" align="center"><tr><td>
<BR />
&nbsp;&nbsp;&nbsp;<asp:Label ID = "lblOne" runat="server" CssClass="Content" Visible="false" Text="1:"></asp:Label>
<asp:DropDownList id="ddlChart" runat="server" Width="199px" CssClass="InputForm" OnSelectedIndexChanged="ddlChart_indexChanged" AutoPostBack="True" ><asp:ListItem value="None">None</asp:ListItem></asp:DropDownList><BR /><BR />
&nbsp;&nbsp;&nbsp;<asp:Label ID = "lblTwo" runat="server" CssClass="Content" Visible="false" Text="2:"></asp:Label>
<asp:DropDownList id="ddlChart2" runat="server" Width="199px" CssClass="InputForm" OnSelectedIndexChanged="ddlChart2_indexChanged" AutoPostBack="True"><asp:ListItem value="None">None</asp:ListItem></asp:DropDownList><BR /><BR />
&nbsp;&nbsp;&nbsp;<asp:Label ID = "lblThree" runat="server" CssClass="Content" Visible="false" Text="3:"></asp:Label>
<asp:DropDownList id="ddlChart3" runat="server" Width="199px" CssClass="InputForm" OnSelectedIndexChanged="ddlChart3_indexChanged" AutoPostBack="True"><asp:ListItem value="None">None</asp:ListItem></asp:DropDownList><BR /><BR />
  &nbsp;&nbsp;&nbsp;&nbsp;<сс:GMDatePicker ID="GMDatePicker1" runat="server" CssClass="Content LinkNormal" TextBoxWidth="170">
          <CalendarDayStyle Font-Size="9pt" />
      <CalendarTodayDayStyle BorderWidth="1px"
         BorderColor="DarkRed" Font-Bold="True" />
      <CalendarOtherMonthDayStyle BackColor="WhiteSmoke" />
      <CalendarTitleStyle BackColor="#E0E0E0"
         Font-Names="Arial" Font-Size="9pt" />
    </сс:GMDatePicker>
    <BR /><BR />&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button id="btnCreate" onclick="button_click" runat="server" Text="Create Charts"></asp:Button>
     </td></tr></table>
     </TD>
     <TD align = "right"><BR /><asp:Image id="imgChart" runat="server" Visible="false" ></asp:Image><BR /><BR /><asp:Image id="imgChart1" runat="server" Visible="false" ></asp:Image><BR /><BR /><asp:Image id="imgChart2" runat="server" Visible="false"></asp:Image><BR /><BR /><asp:Image id="imgChart3" runat="server" Visible="false" ></asp:Image><BR /><BR /></TD></TR></TBODY></TABLE>
</contenttemplate>

    </cc1:UpdatePanel>

</asp:Content>