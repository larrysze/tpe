<%@ Register TagPrefix="mbdb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.DefaultButtons" %>
<%@ Page Language="c#" Codebehind="MarketResearch.aspx.cs" AutoEventWireup="True" Inherits="localhost.Research.MarketResearch" Title="Market Research" MasterPageFile="~/MasterPages/Menu.Master" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>


<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphCssLink">
<style type="text/css">
    .progressAjaxPleaseWait
    {
        display: block;
        position: absolute;
        padding: 2px 3px;
    }
    .containerAjaxPleaseWait
    {
        border: solid 1px #808080;
        border-width: 1px 0px;
    }
    .headerAjaxPleaseWait
    {
        background: url(http://www.theplasticexchange.com/images/sprite.png) repeat-x 0px 0px;
        border-color: #808080 #808080 #ccc;
        border-style: solid;
        border-width: 0px 1px 1px;
        padding: 0px 10px;
        color: #000000;
        font-size: 9pt;
        font-weight: bold;
        line-height: 1.9;  
        font-family: arial,helvetica,clean,sans-serif;
    }
    .bodyAjaxPleaseWait
    {
        background-color: #f2f2f2;
        border-color: #808080;
        border-style: solid;
        border-width: 0px 1px;
        padding: 10px;
    }
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphJavaScript" runat="server">

   <script type="text/javascript">
                function onUpdating()
                {
                    // get the update progress div
                    var pnlPopup = $get('<%= this.pnlPopup.ClientID %>'); 

                    //  get the gridview element        
                    var gridView = $get('<%= this.pnlRegular.ClientID %>');
                    
                    // make it visible
                    pnlPopup.style.display = '';	    
                    
                    // get the bounds of both the gridview and the progress div
                    var gridViewBounds = Sys.UI.DomElement.getBounds(gridView);
                    var pnlPopupBounds = Sys.UI.DomElement.getBounds(pnlPopup);
                    
                    //  center of gridview
              var x = gridViewBounds.x + Math.round(gridViewBounds.width / 2) - Math.round(pnlPopupBounds.width / 2);
                    var y = 250;
                    //var y = gridViewBounds.y + Math.round(gridViewBounds.height / 2) - Math.round(pnlPopupBounds.height / 2);	    



                    //	set the progress element to this position
                    Sys.UI.DomElement.setLocation(pnlPopup, x, y);           
                }

                function onUpdated() 
                {
                    // get the update progress div
                    var pnlPopup = $get('<%= this.pnlPopup.ClientID %>'); 
                    // make it invisible
                    pnlPopup.style.display = 'none';
                }            
        </script>
</asp:Content>

<asp:Content ID="MarketResearch" runat="server" ContentPlaceHolderID="cphMain">
    <table id="ResearchFrom2009">
        <tr>
            <td>
               <table cellspacing="0" cellpadding="0">
                  <tr>
	                    <td>
                            <asp:Panel ID="pnlPopup" runat="server" CssClass="progressAjaxPleaseWait" style="display:none;">
                                <div class="containerAjaxPleaseWait">
                                    <div class="headerAjaxPleaseWait">Loading...</div>
                                    <div class="bodyAjaxPleaseWait">
                                        <img alt="activity" src="http://www.theplasticexchange.com/images/activity.gif" />
                                    </div>
                                </div>
                            </asp:Panel> 
	                    </td>
	                </tr>
	            </table>
            	
                <asp:Panel runat="server" ID="pnlRegular">
                    <asp:Table ID="Table1" runat="server" Width="100%">
                        <asp:TableRow ID="trPrint">
                            <asp:TableCell Width="35px">&nbsp;</asp:TableCell>
                            <asp:TableCell Width="35px" Text="&lt;IMG src=&quot;../pics/bullet.gif&quot;&gt;"></asp:TableCell>
                            <asp:TableCell ColumnSpan="4" CssClass="ContentWR Color2" Style="font-size: 12px; text-align: left;" Width="720px">
                                <div class="LinkNormal Color2">
                                    <asp:Label runat="server" ID="lblPrintLink" />
                                </div>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Width="35px">&nbsp;</asp:TableCell>
                            <asp:TableCell Width="35px" Text="
	                                &lt;IMG src=&quot;../pics/bullet.gif&quot;&gt;
                                "></asp:TableCell>
                            <asp:TableCell Width="350px" CssClass="ContentWR Color2" Style="font-size: 12px; text-align: left;" Text="Add an email to join our distribution list:"></asp:TableCell>
                            <asp:TableCell Width="150px" Wrap="False">
                                <asp:TextBox runat="server" EnableViewState="False" Width="130px" CssClass="InputForm" ID="txtEmail" MaxLength="255"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEmail" ErrorMessage="*" ID="rfvEmail"></asp:RequiredFieldValidator>
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Middle" Width="30px">
                                <asp:ImageButton runat="server" ImageUrl="/images/buttons/submit_orange.jpg" ID="btnEmail"></asp:ImageButton>
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Middle" Width="220px" HorizontalAlign="left">
                                <asp:RegularExpressionValidator CssClass="ContentWR Color3" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" ControlToValidate="txtEmail" ErrorMessage="Invalid email" ID="revEmail"></asp:RegularExpressionValidator>
                                <asp:CustomValidator CssClass="ContentWR Color3" runat="server" ID="cvEmail" ControlToValidate="txtEmail"></asp:CustomValidator>
                            </asp:TableCell>
                        </asp:TableRow>
                    
                        <asp:TableRow>
                            <asp:TableCell Width="35px">&nbsp;</asp:TableCell>
                            <asp:TableCell Width="35px" Text="
		                            &lt;IMG src=&quot;../pics/bullet.gif&quot;&gt;"></asp:TableCell>
                            <asp:TableCell CssClass="Header" HorizontalAlign="Left" Text="Past Reports"></asp:TableCell>
                            <asp:TableCell>
                                <asp:DropDownList runat="server" AutoPostBack="True" CssClass="InputForm" ID="ddlIssue" />
                                
                            </asp:TableCell>
                            <asp:TableCell Width="350px" ColumnSpan="2">
                                <asp:Label runat="server" ID="lblEmail"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>        
                    <div class="DivTitleBar">
                        <span class="Header Color1 Bold">Weekly Market Update</span>
                    </div>    
                </asp:Panel> 
                <table style="font-family:Verdana;" width="100%">
                    <tr>
                        <td style="width:430; vertical-align:top;">
                            <h3>Summary</h3>
                            <asp:Label  id="lblMarketSummary" runat="server" CssClass="ContentWR TextJustify"></asp:Label>
                        </td>
                        <td valign="top" align="center" style="padding-top:40px">
                          <div style="padding:10px;">
                              <b>Market Update - 
                              <asp:Label  id="lblMarketUpdateDate" runat="server" ForeColor="#da251d"></asp:Label>
                              </b>
                          </div>
                          <table style="text-align:center; border-style:solid; border-color:Black; border-width:1px;  background-color:#DADADA"  width="430" >
                                <tr>
                                    <td valign="bottom" align="left" style="padding-left:15px; padding-top:5px; font-size:13px; " >
                                       <b>Total Offers</b> 
                                       <asp:Label ID="lblTotalVolume" ForeColor="#da251d" Font-Bold="true" runat="server" Text="Label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:10px">
                                        <asp:datagrid BorderWidth="0" BackColor="#000000" CellSpacing="1" id="dg" runat="server" Width="400" CssClass="DataGrid"
                                            CellPadding="2" ShowFooter="False" AllowSorting="True" AutoGenerateColumns="False"  OnItemDataBound="ItemDataBound">
                                            <AlternatingItemStyle CssClass="LinkNormal DarkGray" BackColor="#BFBFBF"></AlternatingItemStyle>
                                            <ItemStyle CssClass="LinkNormal LightGray" BackColor="#DADADA"></ItemStyle>
                                            <HeaderStyle BackColor="#e77817" Font-Size="15px" Height="25"></HeaderStyle>
                                            <Columns>
                                                <asp:HyperLinkColumn DataNavigateUrlField="grade_id" ItemStyle-HorizontalAlign="Left"  DataNavigateUrlFormatString="/spot/Spot_Floor.aspx?Filter={0}" DataTextField="GRADE" HeaderText="Resin">
                                                    <HeaderStyle CssClass="ContentWR Bold Color2" HorizontalAlign="Left"></HeaderStyle>
                                                </asp:HyperLinkColumn>
                                                <asp:BoundColumn DataField="varsize" HeaderText="Total lbs" DataFormatString="{0:#,##}">
                                                    <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                                                    <ItemStyle  HorizontalAlign="Left" Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="PriceLow" HeaderText="Low" DataFormatString="{0:$.##0}" ItemStyle-Font-Bold="true" >
                                                    <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                                                    <ItemStyle Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn  DataField="PriceHigh" HeaderText="High" DataFormatString="{0:$.##0}" ItemStyle-Font-Bold="true" >
                                                    <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                                                    <ItemStyle Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="PriceBid" HeaderText="Bid" DataFormatString="{0:$.##0}" ItemStyle-Font-Bold="true" >
                                                    <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                                                    <ItemStyle Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="PriceOffer" HeaderText="Offer"  DataFormatString="{0:$.##0}" ItemStyle-Font-Bold="true" >
                                                    <HeaderStyle CssClass="ContentWR Bold Color2"></HeaderStyle>
                                                    <ItemStyle Wrap="False" CssClass="ContentWR Color2"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn Visible="False" HeaderText="Price Range" >
                                                    <HeaderStyle CssClass="ContentWR Bold Color4"></HeaderStyle>
                                                    <ItemStyle Wrap="False" CssClass="ContentWR Color4" ></ItemStyle>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:datagrid>
                                    </td>
                                </tr>
                            </table>
                            <div style="margin:10px; padding:5px; border-style:solid; border-width:1px; width:430px;">
                                <asp:Image  runat="server" ID="imgCrudeOil"  width="420" height="300" />
                            </div>
                        </td>
                    </tr>
                   
                    <tr>
                        <td valign="top" >
                            <h3>Polyethylene</h3>
                            <asp:Label id="lblPeSummary" runat="server" CssClass="ContentWR"></asp:Label>
                        </td>
                        <td valign="top" align="center" style="padding-top:40px">
                            <asp:Image runat="server" ID="PeMonth"  width="300" height="200" />
                            <asp:Image runat="server" ID="PeYear"  width="300" height="200" />
                            <asp:Image runat="server" ID="PeFiveYear"  width="300" height="200" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <h3>Polypropylene</h3>
                            <asp:Label id="lblPpSummary" runat="server" CssClass="ContentWR"></asp:Label>
                        </td>
                        <td valign="top" align="center" style="padding-top:40px">
                            <asp:Image runat="server" ID="PpMonth"  width="300" height="200" />
                            <asp:Image runat="server" ID="PpYear"  width="300" height="200" />
                            <asp:Image runat="server" ID="PpFiveYear"  width="300" height="200" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

