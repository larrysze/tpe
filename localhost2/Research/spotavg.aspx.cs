using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;

namespace localhost.Research
{
    public partial class spotavg : System.Web.UI.Page
    {
        protected void Page_Load(object sender, System.EventArgs e)
        {
            Master.Width = "780px";
            // Put user code to initialize the page here

            if ((string)Session["Typ"] != "A")
            {
                Response.Redirect("../default.aspx");
            }

            if (!IsPostBack)
            {
                loadDDL();
                BindDG();                
            }
        }
        
        private void loadDDL()
        {

            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, "select HomePageSpotContract.spotid, grade.grade_name from  HomePageSpotContract, grade where HomePageSpotContract.spotid=grade.grade_id order by grade_id");

                
                

                while (dtr.Read())
                {
                    ddlChart.Items.Add(new ListItem(dtr["grade_name"].ToString(), dtr["spotid"].ToString()));
                }

                ddlChart.Items[0].Selected = true;
            }

        }

        private void BindDG()
        {

            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                //  Hashtable param = new Hashtable();
                //  param.Add("@cont_id", ddlChart.SelectedValue);
                string sSql;

                sSql = string.Format("SELECT * FROM SpotWeightAvg WHERE CONTID={0}  ORDER BY weightdate DESC", ddlChart.SelectedValue);
       
                DataSet DS;
                SqlDataAdapter MyCommand;
                MyCommand = new SqlDataAdapter(sSql, conn);
                DS = new DataSet();
                MyCommand.Fill(DS, "tblDynamicText");

                
                //dgDaily.CurrentPageIndex = 0;
                dgDaily.DataSource = DS;
                dgDaily.DataBind();
                dgDaily.Visible = true;
                
     

                //dg.DataSource = DS;
                //dg.DataBind();
                conn.Close();
                //Hashtable param = new Hashtable();
                //param.Add("@cont_id", ddlChart.SelectedValue);
                //param.Add("@date", "02-04-2004");

                //DBLibrary.BindDataGrid(Application["DBConn"].ToString(), dg, "SELECT * FROM EXPORT_PRICE WHERE CONT_ID=@CONT_ID AND DATE >= @DATE ORDER BY DATE DESC", param);

            }
        }


        private void dgDaily_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            // We use CommandEventArgs e to get the row which is being clicked 
            // This also changes the DataGrid labels into Textboxes so user can edit them 
            dgDaily.EditItemIndex = e.Item.ItemIndex;
            // Always bind the data so the datagrid can be displayed. 
            BindDG();
        }

 
        protected void dgDaily_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DateTime RowDate = Convert.ToDateTime(e.Item.Cells[1].Text);

                if (RowDate.DayOfWeek.ToString() == "Friday")
                {                    
                    e.Item.BackColor = Color.Orange;

                }
            }
        }

        
        private void dgDaily_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            // All we do in the cancel method is to assign '-1' to the datagrid editItemIndex 
            // Once the edititemindex is set to '-1' the datagrid returns back to its original condition 
            dgDaily.EditItemIndex = -1;
            BindDG();
        }


        
        private void dgDaily_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {

            string strDate = e.Item.Cells[1].Text;
            //e.Item.Controls[2]

            string strAsk = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
            
            string cont_id = ddlChart.SelectedValue;
            
            Hashtable htParams = new Hashtable();
            
            htParams.Add("@ask", strAsk);
            htParams.Add("@contid", cont_id);
            htParams.Add("@weightdate", strDate);


            // updating existing item
            string strSqlUpdate = @"Update SpotWeightAvg Set ask=@ask, WHERE CONTID=@cont_id AND 
                                    YEAR(weightdate)=YEAR(@DATE) AND MONTH(weightdate)=MONTH(@DATE) AND DAY(weightdate)=DAY(@DATE)";

            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlUpdate, htParams);

            dgDaily.EditItemIndex = -1;
            BindDG();

        }
        public void myDataGrid_PageChanger(object Source, DataGridPageChangedEventArgs E)
        {
            dgDaily.CurrentPageIndex = E.NewPageIndex;            
            BindDG();
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            
            this.dgDaily.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgDaily_EditCommand);
            this.dgDaily.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgDaily_UpdateCommand);

        }
        #endregion

        protected void ddlChart_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BindDG();            
        }




        protected void btnExportToExcel_Click(object sender, System.EventArgs e)
        {
            //export to excel

            //using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            //{
            //  DataSet Ds = TPE.Utility.DBLibrary.GetDataSetFromStoredProcedure(conn, "spInventory");

            BindDG();


            dgDaily.Columns[0].Visible = false;
            


            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";


            string filename = ddlChart.SelectedItem.Text;

            Response.AddHeader("Content-Disposition", "attachment; filename='" + filename + "'");

            //Response.AddHeader("Content-Disposition", "attachment; filename=DayPriceEdit.xls");

            Response.BufferOutput = true;
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Charset = "";
            //Response.Charset = "UTF-8";
            EnableViewState = false;

            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

            FormatExportedGrid(dgDaily);
            this.ClearControls(dgDaily);
            dgDaily.RenderControl(hw);
            
            Response.Write(CleanHTML(tw.ToString()));
            Response.End();

            //  ConvertToExcel(Ds, Response);
            //            }



            //TPE.Utility.DBLibrary.BindDataGridWithStoredProcedure(Application["dbConn"].ToString(), this.dg, "spInventory");

            //Response.Clear();
            //Response.Buffer = true;
            //Response.ContentType = "application/vnd.ms-excel";
            //Response.Charset = "";
            //this.EnableViewState = false;
            //System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
            //System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
            //this.ClearControls(dg);
            ////dg.RenderControl(oHtmlTextWriter);
            //Response.Write(oStringWriter.ToString());
            //Response.End();
        }

        public void FormatExportedGrid(DataGrid dg1)
        {
            dg1.BackColor = System.Drawing.Color.White;
        }


        private string CleanHTML(string OutOut)
        {

            //will add some code to fix gridlines

            return OutOut;


        }


        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */

        }

        private void ClearControls(Control control)
        {
            for (int i = control.Controls.Count - 1; i >= 0; i--)
            {
                ClearControls(control.Controls[i]);
            }
            if (!(control is TableCell))
            {
                if (control.GetType().GetProperty("SelectedItem") != null)
                {
                    LiteralControl literal = new LiteralControl();
                    control.Parent.Controls.Add(literal);
                    try
                    {
                        literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
                    }
                    catch
                    {
                    }
                    control.Parent.Controls.Remove(control);
                }
                else
                    if (control.GetType().GetProperty("Text") != null)
                    {
                        LiteralControl literal = new LiteralControl();
                        control.Parent.Controls.Add(literal);
                        literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);
                        control.Parent.Controls.Remove(control);
                    }
            }
            return;
        }

    }
}
