using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;
using System.Web.Mail;
using System.Text;
using System.IO;
using System.Threading;

namespace localhost.Research
{
	/// <summary>
	/// Summary description for WeeklyReview.
	/// </summary>
	public partial class WeeklyReview : System.Web.UI.Page
	{
        public static string current_issue_date="";


		protected System.Web.UI.WebControls.Label lblPrintLink;

        private DateTime dtFirstArchive = DateTime.Parse("August 10, 2005");


        #region OldMUDates array
        private readonly string[] OldMUDates = { 
                "August 10, 2005", 
                "July 5, 2005", 
                "June 1, 2005",
                "April 18, 2005",
                "February 28, 2005",
                "January 6, 2005",
                "November 8, 2004",
                "September 9, 2004",
                "July 27, 2004",
                "June 3, 2004",
                "April 27, 2004",
                "March 10, 2004",
                "January 27, 2004", //30
                "December 19, 2003",
                "November 4, 2003",
                "September 15, 2003",
                "July 31, 2003",
                "June 17, 2003",
                "April 24, 2003",
                "March 25, 2003",
                "February 19, 2003",
                "January 23, 2003",
                "December 11, 2002",
                "October 23, 2002",//20
                "September 12, 2002",
                "August 6, 2002",
                "May 22, 2002",
                "March 27, 2002",
                "February 7, 2002",
                "November 30, 2001",
                "October 23, 2001",
                "September 12, 2001",
                "August 15, 2001",
                "July 9, 2001",
                "June 27, 2001",
                "April 24, 2001" ,
                "March 27, 2001",
                "March 8, 2001",
                "February 15, 2001",
                "January 24, 2001",
                "January 5, 2001",
                "December 18, 2000",
                "December 6, 2000"
            };
        #endregion



        protected void Page_Load(object sender, System.EventArgs e)
		{
            
            btnEmail.Attributes["onMouseOut"] = "document.getElementById('" + btnEmail.ClientID + "').src='/images/buttons/submit_orange.jpg';";
            btnEmail.Attributes["OnMouseOver"] = "document.getElementById('" + btnEmail.ClientID + "').src='/images/buttons/submit_red.jpg';";

			// Put user code to initialize the page here
			if(!Page.IsPostBack)
			{
				initDDL();

                if (Request["selectedDate"] != null)
                {
  
                    DateTime review_date = Convert.ToDateTime(Request["selectedDate"].ToString());

                    for (int i = 0; i < ddlIssue.Items.Count; i++)
                    {
                        DateTime ddlItem = Convert.ToDateTime(ddlIssue.Items[i].ToString());
                        if (ddlItem.Year == review_date.Year)
                            if (ddlItem.Month == review_date.Month)
                                if (ddlItem.Day == review_date.Day)
                                {
                                    ddlIssue.SelectedItem.Selected = false;
                                    ddlIssue.Items[i].Selected = true;
                                }
                    }
            }
            }

            #region*****CEF Reports on 5/28 and before were using diff PE charts
            DateTime selectedDate = Convert.ToDateTime(ddlIssue.SelectedItem.ToString());
            DateTime TesTime = new DateTime(2007, 5, 28);

            if (selectedDate.CompareTo(TesTime) > 0)
            {
                DateTime NewCutOff = new DateTime(2007, 6, 17);

                if (selectedDate.CompareTo(NewCutOff) > 0)
                {
                    Label4.Text = "HDPE Inj - Pail";
                    Label6.Text = "HDPE Inj - Pail";
                }
                else
                {
                    Label4.Text = "LLDPE Film - Butene";
                    Label6.Text = "LLDPE Film - Butene";
                }
            }
            else
            {
                Label4.Text = "HDPE Blow - HIC";
                Label6.Text = "HDPE Blow - HIC";
            }
            #endregion

            string dateFileName = selectedDate.Month + "_" + selectedDate.Day + "_" + selectedDate.Year;

            string strPdfFileName = Server.MapPath("/Research/Market_Update/pdf/" + dateFileName + ".pdf");
            if (System.IO.File.Exists(strPdfFileName))
            {
                lblPrintLink.Text = "Click <a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Market_Update/pdf/" + dateFileName + ".pdf' target='_blank'>here</a> for a printable version";
                //lblLink.Text = "<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Market_Update/pdf/" + dateFileName + ".pdf' target='_blank'>DOWNLOAD</a>";
            }
            else
            {
                //lblLink.Text = "";
                lblPrintLink.Text = "";
            }

            this.trPrint.Visible = (lblPrintLink.Text != "");

            BindGrid(ddlIssue.SelectedItem.ToString());
            loadHTML(ddlIssue.SelectedItem.ToString());			
		}

        private void BindGrid(string strDate)
        {

            DateTime review_date = Convert.ToDateTime(strDate);
            if (review_date >= new DateTime(2009, 1, 1))
            {
                string year = review_date.Year.ToString();
                string month = review_date.Month.ToString();
                string day = review_date.Day.ToString();
                Hashtable htParam = new Hashtable();
                htParam.Add("@date", strDate);

                SqlConnection conn;
                conn = new SqlConnection(Application["DBConn"].ToString());
                conn.Open();

                SqlDataAdapter dadWorking;
                DataSet dstWorking; 
                string sSQLWorking = "select * from WeeklyReviewSpotOffersSummary W, grade G where W.grade_id = G.grade_id and year(date) = '"+year+"' and month(date) = '"+month+"' and day(date) = '"+day+"' order by weight desc ";

                dadWorking = new SqlDataAdapter(sSQLWorking, conn);
                dstWorking = new DataSet();
                dadWorking.Fill(dstWorking);

                dg.DataSource = dstWorking;
                dg.DataBind();

                string seleStr = "SELECT SUM(weight) AS TotalVolume FROM WeeklyReviewSpotOffersSummary WHERE datediff(day,date,@date)=0";
                string total = DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), seleStr, htParam).ToString();
                if (total.Trim().Equals(""))
                {
                    lblNewTotalVolume.Text = "Not available";
                }
                else
                {
                    lblNewTotalVolume.Text = String.Format("{0:#,###}", decimal.Parse(total)) + " lbs";
                }

                pnl2009dMarketUpdate.Visible = true;
                pnlOldMarketUpdate.Visible = false;
                pnlNewMarketUpdate.Visible = false;  

                //string date = Convert.ToString(review_date);
                //string[] dates = date.Split(' ');
                //string new_date = dates[0];
                //new_date = new_date.Replace("/", "_");
                //current_issue_date = new_date;
                //imgMU.ImageUrl = "~/Research/Weekly_Reviews/Market_Update/"+new_date+".png";
                //pnl2009dMarketUpdate.Visible = true;
                //pnlOldMarketUpdate.Visible = false;
                //pnlNewMarketUpdate.Visible = false;
                ////Response.Redirect("/Research/MarketResearch.aspx?selectedDate=" + strDate);
            }
            else
            {
                if (DateTime.Parse(strDate).CompareTo(dtFirstArchive) > 0)
                {
                    Hashtable htParam = new Hashtable();
                    htParam.Add("@date", strDate);
                    TPE.Utility.DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(), dgSpotFloorSummary, "spWeekInReviewSpotOffersSnapshot", htParam);

                    if (dgSpotFloorSummary.Items.Count == 0)
                    {
                        pnlOldMarketUpdate.Visible = false;
                        pnlNewMarketUpdate.Visible = true;
                        pnl2009dMarketUpdate.Visible = false;

                        //Hashtable htParam = new Hashtable();
                        htParam.Clear();
                        htParam.Add("@date", strDate);
                        TPE.Utility.DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(), dgSpotFloorSummary, "spMarketUpdateSpotOffersSnapshot", htParam);

                        string seleStr = "SELECT SUM(weight) AS TotalVolume FROM MarketUpdateSpotOffersSummary WHERE datediff(day,date,@date)=0";
                        string total = DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), seleStr, htParam).ToString();
                        if (total != "")
                        {
                            lblTotalVolume.Text = "(" + String.Format("{0:#,###}", decimal.Parse(total)) + " lbs)";
                        }
                    }
                    else
                    {
                        pnlOldMarketUpdate.Visible = false;
                        pnlNewMarketUpdate.Visible = true;
                        pnl2009dMarketUpdate.Visible = false;
                        string seleStr = "SELECT SUM(weight) AS TotalVolume FROM WeeklyReviewSpotOffersSummary WHERE datediff(day,date,@date)=0";
                        string total = DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), seleStr, htParam).ToString();
                        if (total.Trim().Equals(""))
                        {
                            lblTotalVolume.Text = "(Not available)";
                        }
                        else
                        {
                            lblTotalVolume.Text = "(" + String.Format("{0:#,###}", decimal.Parse(total)) + " lbs)";
                        }
                    }

                }
                else
                {
                    // show archived MU (from .txt file)
                    pnlOldMarketUpdate.Visible = true;                    
                    pnlNewMarketUpdate.Visible = false;
                    pnl2009dMarketUpdate.Visible = false;
                }
            }
        }


		private void initDDL()
		{
           // ArrayList ReviewDates =  new ArrayList();
            
			using(SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();

                string sSQL = "SELECT DATENAME(MONTH,date) +  ' ' + DATENAME(DAY,date) + ', ' + DATENAME(YEAR,date) as MUDATE,date FROM MARKET_UPDATES WHERE published = 1 UNION select DATENAME(MONTH,review_date) +  ' ' + DATENAME(DAY,review_date) + ', ' + DATENAME(YEAR,review_date) as MUDATE,review_date from WEEKLY_REVIEWS WHERE published = 1  ORDER BY DATE DESC";

				SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn,sSQL);

				while(dtr.Read())
				{
                    ddlIssue.Items.Add(dtr["MUDATE"].ToString());					
				}                
			}

            ListItem li = null;
            for (int i = 0; i < OldMUDates.Length - 1; i++)
            {   // add old dates with negative values ie, -44..0
                li = new ListItem(OldMUDates[i], (i - OldMUDates.Length).ToString());
                ddlIssue.Items.Add(li);
            }
		}

		private void loadHTML(string review_date)
		{

            DateTime reviewdate = Convert.ToDateTime(review_date);
            if (reviewdate >= new DateTime(2009, 1, 1))
            {
                imgPE.Visible = false;
                imgPP.Visible = false;
                lblPeSummary.Visible = true;
                PeMonth.Visible = true;
                PeYear.Visible = true;
                PeFiveYear.Visible = true;

                lblPpSummary.Visible = true;
                PpMonth.Visible = true;
                PpYear.Visible = true;
                PpFiveYear.Visible = true;


                if (reviewdate == new DateTime(2010, 1, 1) || reviewdate >= new DateTime(2010, 7, 2) )
                {
                    //do something here to show special issue.
                    using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
                    {
                        conn.Open();
                        Hashtable htParam = new Hashtable();
                        htParam.Add("@date", review_date);
                        string sSQL = "SELECT comments FROM weekly_reviews WHERE published = 1 AND datediff(day,review_date,@date)=0";

                        SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, sSQL, htParam);

                        while (dtr.Read())
                        {
                            lblMarketSummary.Text = dtr["comments"].ToString();
                        }
                    }
                    string FolderName = reviewdate.Month + "_" + reviewdate.Day + "_" + reviewdate.Year;
                    this.lblDate2.Text = review_date;

                    imgCrudeOil.ImageUrl = @"Weekly_Reviews\pics\" + FolderName + @"\crudeoil.png";
                    imgPE.ImageUrl = @"Weekly_Reviews\pics\" + FolderName + @"\Chart_3_1Y.png";
                    imgPP.ImageUrl = @"Weekly_Reviews\pics\" + FolderName + @"\Chart_9_1Y.png";
                    imgPE.Visible = true;
                    imgPP.Visible = true;
                    imgCrudeOil.Visible = true;

                    if (reviewdate >= new DateTime(2010, 7, 2) )
                    {
                        imgCrudeOil.Visible = false;
                    }

                    lblPeSummary.Visible = false;
                    PeMonth.Visible = false;
                    PeYear.Visible = false;
                    PeFiveYear.Visible = false;

                    lblPpSummary.Visible = false;
                    PpMonth.Visible = false;
                    PpYear.Visible = false;
                    PpFiveYear.Visible = false;

                    lblH3PE.Visible = false;
                    lblH3PP.Visible = false;

                    pnl2009dMarketUpdate.Visible = true;
                    pnlOldMarketUpdate.Visible = false;
                    pnlNewMarketUpdate.Visible = false;
                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
                    {
                        conn.Open();
                        Hashtable htParam = new Hashtable();
                        htParam.Add("@date", review_date);
                        string sSQL = "SELECT comments, polyethylene, polypropylene FROM weekly_reviews WHERE published = 1 AND datediff(day,review_date,@date)=0";

                        SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, sSQL, htParam);

                        while (dtr.Read())
                        {
                            lblMarketSummary.Text = dtr["comments"].ToString();
                            lblPeSummary.Text = dtr["polyethylene"].ToString();
                            lblPpSummary.Text = dtr["polypropylene"].ToString();
                        }
                    }
                    string FolderName = reviewdate.Month + "_" + reviewdate.Day + "_" + reviewdate.Year;
                    this.lblDate2.Text = review_date;

                    imgCrudeOil.ImageUrl = @"Weekly_Reviews\pics\" + FolderName + @"\crudeoil.png";
                    imgCrudeOil.Visible = true;
                    lblH3PE.Visible = true;
                    lblH3PP.Visible = true;
                    imgPE.Visible = false;
                    imgPP.Visible = false;

                    PeMonth.ImageUrl = @"Weekly_Reviews\pics\" + FolderName + @"\Chart_3_1M.png";
                    PeYear.ImageUrl = @"Weekly_Reviews\pics\" + FolderName + @"\Chart_3_1Y.png";
                    PeFiveYear.ImageUrl = @"Weekly_Reviews\pics\" + FolderName + @"\Chart_3_5Y.png";

                    PpMonth.ImageUrl = @"Weekly_Reviews\pics\" + FolderName + @"\Chart_9_1M.png";
                    PpYear.ImageUrl = @"Weekly_Reviews\pics\" + FolderName + @"\Chart_9_1Y.png";
                    PpFiveYear.ImageUrl = @"Weekly_Reviews\pics\" + FolderName + @"\Chart_9_5Y.png";

                    lblPeSummary.Visible = true;
                    PeMonth.Visible = true;
                    PeYear.Visible = true;
                    PeFiveYear.Visible = true;

                    lblPpSummary.Visible = true;
                    PpMonth.Visible = true;
                    PpYear.Visible = true;
                    PpFiveYear.Visible = true;

                    pnl2009dMarketUpdate.Visible = true;
                    pnlOldMarketUpdate.Visible = false;
                    pnlNewMarketUpdate.Visible = false;
                    //Response.Redirect("/Research/MarketResearch.aspx?selectedDate=" + strDate);
                }
                
            }
            else
            {
                if (DateTime.Parse(review_date).CompareTo(dtFirstArchive) > 0)
                {

                    // show new market update format
                    pnlOldMarketUpdate.Visible = false;
                    pnlNewMarketUpdate.Visible = true;
                    pnl2009dMarketUpdate.Visible = false;

                    if (review_date != "")
                    {
                        review_date = " AND Year(review_date)=" + reviewdate.Year + " AND Month(review_date)=" + reviewdate.Month + " AND Day(review_date)=" + reviewdate.Day;
                    }
                    else
                    {
                        review_date = " AND review_date = (select max(review_date) from weekly_reviews where published=1)";
                    }

                    using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
                    {
                        conn.Open();
                        SqlDataReader dtr = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn, "SELECT * FROM WEEKLY_REVIEWS WHERE published=1 " + review_date);

                        if (dtr.HasRows)
                        {
                            lblUpdateHeader.Text = "Market Update";
                            //lblUpdateHeader.Text = "Week in Review";
                            if (dtr.Read())
                            {
                                string total = dtr["TOTAL"].ToString();
                                string issue_date = dtr["REVIEW_DATE"].ToString();

                                string snapshot = dtr["SPOT_FLOOR"].ToString();
                                string strVolumePP = dtr["volumePP"].ToString();
                                string strVolumePS = dtr["volumePS"].ToString();
                                string strVolumePE = dtr["volumePE"].ToString();
                                string strPricePP = dtr["pricePP"].ToString();
                                string strPricePS = dtr["pricePS"].ToString();
                                string strPricePE = dtr["pricePE"].ToString();

                                string strSummary = HelperFunction.ConvertCarriageReturnToBR(dtr["COMMENTS"].ToString());

                                string strPreTextPP = HelperFunction.ConvertCarriageReturnToBR(dtr["pretext_pp"].ToString());
                                string strPreTextPS = HelperFunction.ConvertCarriageReturnToBR(dtr["pretext_ps"].ToString());
                                string strPreTextPE = HelperFunction.ConvertCarriageReturnToBR(dtr["pretext_pe"].ToString());

                                string strPP = HelperFunction.ConvertCarriageReturnToBR(dtr["POLYPROPYLENE"].ToString());
                                string strPS = HelperFunction.ConvertCarriageReturnToBR(dtr["POLYSTYRENE"].ToString());
                                string strPE = HelperFunction.ConvertCarriageReturnToBR(dtr["POLYETHYLENE"].ToString());

                                //**** PE Charts
                                Label4.Text = dtr["PEChart1M"].ToString();
                                Label4.Visible = true;
                                Label6.Text = dtr["PEChart1Y"].ToString();
                                Label6.Visible = true;
                                Label12.Text = dtr["PEChart1MTime"].ToString();
                                Label12.Visible = true;
                                Label7.Text = dtr["PEChart1YTime"].ToString();
                                Label7.Visible = true;

                                //PP Charts
                                Label3.Text = dtr["PPChart1M"].ToString();
                                Label3.Visible = true;
                                Label1.Text = dtr["PPChart1Y"].ToString();
                                Label1.Visible = true;
                                Label8.Text = dtr["PPChart1MTime"].ToString();
                                Label8.Visible = true;
                                Label9.Text = dtr["PPChart1YTime"].ToString();
                                Label9.Visible = true;

                                //PS Charts
                                Label2.Text = dtr["PSChart1M"].ToString();
                                Label2.Visible = true;
                                Label5.Text = dtr["PSChart1Y"].ToString();
                                Label5.Visible = true;
                                Label10.Text = dtr["PSChart1MTime"].ToString();
                                Label10.Visible = true;
                                Label11.Text = dtr["PSChart1YTime"].ToString();
                                Label11.Visible = true;

                                DateTime date_obj = Convert.ToDateTime(issue_date);
                                issue_date = date_obj.ToString("MMMM") + " " + date_obj.Day + ", " + date_obj.Year;

                                this.lblDate.Text = issue_date;

                                DateTime selectedDate = Convert.ToDateTime(ddlIssue.SelectedItem.ToString());
                                DateTime TesTime = new DateTime(2007, 11, 13);

                                rowPEOld.Visible = true;
                                rowPPOld.Visible = true;
                                rowPSOld.Visible = true;

                                imgPEMonthChart.ImageUrl = dtr["PATH1"].ToString();
                                imgPEYearChart.ImageUrl = dtr["PATH2"].ToString();
                                imgPPMonthChart.ImageUrl = dtr["PATH3"].ToString();
                                imgPPYearChart.ImageUrl = dtr["PATH4"].ToString();
                                imgPSMonthChart.ImageUrl = dtr["PATH5"].ToString();
                                imgPSYearChart.ImageUrl = dtr["PATH6"].ToString();

                                // Summary text
                                lblSummary.Text = Highlight(strSummary);

                                // Polyethylene:
                                lblPEVolume.Text = strVolumePE;
                                lblPEPrice.Text = strPricePE;
                                lblPEVolume.Visible = true;
                                lblPEPrice.Visible = true;
                                lblPEVL.Visible = true;
                                lblPEPR.Visible = true;
                                lblPEIntro.Text = Highlight(strPreTextPE);
                                lblPEBody.Text = Highlight(strPE);

                                // Polypropylene:
                                lblPPVolume.Text = strVolumePP;
                                lblPPPrice.Text = strPricePP;
                                lblPPVolume.Visible = true;
                                lblPPPrice.Visible = true;
                                lblPPVL.Visible = true;
                                lblPPPR.Visible = true;
                                lblPPIntro.Text = Highlight(strPreTextPP);
                                lblPPBody.Text = Highlight(strPP);

                                // Polystyrene:
                                lblPSVolume.Text = strVolumePS;
                                lblPSPrice.Text = strPricePS;
                                lblPSVolume.Visible = true;
                                lblPSPrice.Visible = true;
                                lblPSVL.Visible = true;
                                lblPSPR.Visible = true;
                                lblPSIntro.Text = Highlight(strPreTextPS);
                                lblPSBody.Text = Highlight(strPS);


                                //Chart Names



                            }
                        }
                        else
                        {
                            loadMonthlyHTML(reviewdate.ToString());
                        }
                    }
                }
                else
                {
                    pnlOldMarketUpdate.Visible = true;
                    pnlNewMarketUpdate.Visible = false;
                    pnl2009dMarketUpdate.Visible = false;
                    lblOldContent.Text = loadTxt(review_date);
                }
            }
		}

        private void loadMonthlyHTML(string review)
        {            
            // show new market update format
            pnlOldMarketUpdate.Visible = false;
            pnlNewMarketUpdate.Visible = true;
            pnl2009dMarketUpdate.Visible = false;

            lblUpdateHeader.Text = "Market Update";

            string review_date;

            if (review != "")       
            {
                DateTime GoodDate = Convert.ToDateTime(review);
                review_date = " AND Year(date)=" + GoodDate.Year + " AND Month(date)=" + GoodDate.Month + " AND Day(date)=" + GoodDate.Day;
            }
            else
            {
                review_date = " AND date = (select max(date) from weekly_reviews where published=1)";
            }

            DataTable dt = getMarketUpdateData(review_date);

            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                string total = dr["TOTAL"].ToString();
                string issue_date = dr["date"].ToString();

                lblSummary.Text = HelperFunction.ConvertCarriageReturnToBR(Highlight(dr["COMMENTS"].ToString()));

                string strPreTextPP = HelperFunction.ConvertCarriageReturnToBR(dr["pretext_pp"].ToString());
                string strPreTextPS = HelperFunction.ConvertCarriageReturnToBR(dr["pretext_ps"].ToString());
                string strPreTextPE = HelperFunction.ConvertCarriageReturnToBR(dr["pretext_pe"].ToString());

                string strPP = HelperFunction.ConvertCarriageReturnToBR(dr["POLYPROPYLENE"].ToString());
                string strPS = HelperFunction.ConvertCarriageReturnToBR(dr["POLYSTYRENE"].ToString());
                string strPE = HelperFunction.ConvertCarriageReturnToBR(dr["POLYETHYLENE"].ToString());

                DateTime date_obj = Convert.ToDateTime(issue_date);
                issue_date = date_obj.ToString("MMMM") + " " + date_obj.Day + ", " + date_obj.Year;

                lblDate.Text = issue_date;

                //charts:
                imgPEMonthChart.ImageUrl = dr["PEMonthChartPath"].ToString();
                imgPEYearChart.ImageUrl = dr["PEYearChartPath"].ToString();
                imgPPMonthChart.ImageUrl = dr["PPMonthChartPath"].ToString();
                imgPPYearChart.ImageUrl = dr["PPYearChartPath"].ToString();
                imgPSMonthChart.ImageUrl = dr["PSMonthChartPath"].ToString();
                imgPSYearChart.ImageUrl = dr["PSYearChartPath"].ToString();

                // Polyethylene:
                lblPEVolume.Visible = false;
                lblPEPrice.Visible = false;
                lblPEVL.Visible=false;
                lblPEPR.Visible = false;
                lblPEIntro.Text = Highlight(strPreTextPE);
                lblPEBody.Text = Highlight(strPE);

                // Polypropylene:
                lblPPVolume.Visible = false;
                lblPPPrice.Visible = false;
                lblPPVL.Visible=false;
                lblPPPR.Visible = false; 
                lblPPIntro.Text = Highlight(strPreTextPP);
                lblPPBody.Text = Highlight(strPP);

                // Polystyrene:
                lblPSVolume.Visible = false;
                lblPSPrice.Visible = false;
                lblPSVL.Visible=false;
                lblPSPR.Visible = false;
                lblPSIntro.Text = Highlight(strPreTextPS);
                lblPSBody.Text = Highlight(strPS);
            }                                
        }

        protected void ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Footer && e.Item.ItemType != ListItemType.Header)
            {

                if (Convert.IsDBNull(DataBinder.Eval(e.Item.DataItem, "Price_Bid")) && Convert.IsDBNull(DataBinder.Eval(e.Item.DataItem, "Price_Offer")))
                {
                    dg.Columns[4].Visible = false;
                    dg.Columns[5].Visible = false;
                }
                else
                {
                    dg.Columns[4].Visible = true;
                    dg.Columns[5].Visible = true;
                }

                string gradeName = DataBinder.Eval(e.Item.DataItem, "GRADE_NAME").ToString();
                if (gradeName == "PP Homopolymer - Inj")
                    e.Item.Cells[0].Text = "<b>PP Homo</b>";
                if (gradeName == "PP Copolymer - Inj")
                    e.Item.Cells[0].Text = "<b>PP Copo</b>";
            }
        }

        private DataTable getMarketUpdateData(string review_date)
        {
            DataTable dt = null;
   
                using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
                {
                    conn.Open();
                    dt = TPE.Utility.DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), "SELECT * FROM MARKET_UPDATES WHERE published=1 " + review_date);
                }
            
            return dt;
        }

        private string getSessionVariable(string name)
        {
            if (Session[" + name + "] != null)
                return Session[" + name + "].ToString();
            else
                return "";
        }

        private string loadTxt(string strDate)
        {
            string strFileName;
            StringBuilder sbContent = new StringBuilder();

            int index_value = Math.Abs(Convert.ToInt32(ddlIssue.SelectedItem.Value));
            strFileName = Server.MapPath("/Research/Market_Update/" + index_value + ".txt");

            FileStream fsIn = File.OpenRead(strFileName);
            StreamReader Reader;
            Reader = new StreamReader(fsIn);
            while (Reader.Peek() > -1)
            {
                string s = Reader.ReadLine();
                sbContent.Append(s);  
            }

            return sbContent.ToString();
        }

        private string Highlight(string text)
        {
            if ((Request.QueryString["SearchTerm"] != null) && (Request.QueryString["SearchTerm"].ToString().Length > 2))	// don't highlight short words
            {
                text = HelperFunction.Highlight(Request.QueryString["SearchTerm"].ToString(), text);
            }
            return text;
        }

		#region Web Form Designer generated code
		    override protected void OnInit(EventArgs e)
		    {
			    //
			    // CODEGEN: This call is required by the ASP.NET Web Form Designer.
			    //
			    InitializeComponent();
			    base.OnInit(e);
		    }

            private void InitializeComponent()
            {
                btnEmail.Click += new ImageClickEventHandler(this.btnEmail_Click);
                cvEmail.ServerValidate += new ServerValidateEventHandler(this.cvEmail_Validate);
            }        
		#endregion

        private void cvEmail_Validate(object source, ServerValidateEventArgs args)
        {
            lblEmail.Text = "";
            //Check if the email is in the DB
            Hashtable param = new Hashtable();
            string email = txtEmail.Text;

            string strSql = "select MAI_MAIL FROM mailing WHERE MAI_MAIL = @Email";
            param.Add("@Email", email);
            DataTable dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), strSql, param);

            if (dt.Rows.Count == 0)
                args.IsValid = true;
            else
            {
                args.IsValid = false;
                cvEmail.ErrorMessage = email + " is already in our database";
            }
        }

        private void btnEmail_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (this.IsValid)
            {
                lblEmail.Text = "";
                Hashtable param = new Hashtable();
                string email = txtEmail.Text;
                param.Add("@Email", email);

                string strSqlInsert = "INSERT INTO mailing (MAI_MAIL, MAI_CODE) VALUES (@Email, '0')";
                DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlInsert, param);

                lblEmail.Text = "Thank you! " + email + " has been added to our distribution list.";
                txtEmail.Text = "";

                //Mike wants to receive an email when somebody subscribed to MU
                string txtMessage = "This email: " + email + " was added to the subscription list of Market Update.";
                string txtSubject = email + " in MU list";
                MailMessage mailToMike = new MailMessage();
                mailToMike.From = "research@theplasticsexchange.com";
                mailToMike.To = "michael@theplasticexchange.com";
                mailToMike.Subject = txtSubject;
                mailToMike.Body = txtMessage;
                EmailLibrary.Send(mailToMike);
            }
        }

        
	}
}
