using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;
using dotnetCHARTING;

namespace localhost.Research
{
	/// <summary>
	/// Summary description for ChangeDataSFChart.
	/// </summary>
	public partial class ChangeDataSFChart : System.Web.UI.Page
	{
		protected void Page_Load(object sender, System.EventArgs e)
		{
            
			Master.Width = "760px";
			// Put user code to initialize the page here

			if ((string)Session["Typ"] != "A")
			{
				Response.Redirect("../default.aspx");
			}

			if(!IsPostBack)
			{
				loadDDL();
				BindDG();
				createGradeChart();
			}
		}

		private void loadDDL()
		{
			using(SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, "SELECT * FROM GRADE where grade_id <> 12 and grade_id <> 11 and grade_id <> 10");
				while(dtr.Read())
				{
					ddlChart.Items.Add(new ListItem(dtr["GRADE_NAME"].ToString(), dtr["GRADE_ID"].ToString()));
				}

				ddlChart.Items[0].Selected = true;
			}
		}

		private void BindDG()
		{
			using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				Hashtable param = new Hashtable();
				param.Add("@resin_grade_id", ddlChart.SelectedValue);
				param.Add("@date", "01-01-2004");

                string sqlStr = "select Date, grade_id, isnull(weight,0) as weight, isnull(price_low,0) as price_low, isnull(price_high,0) as price_high , weightavg = (select distinct ask from spotweightavg where contid = grade_id and date = CAST(FLOOR(CAST( weightdate AS float)) AS datetime)) from ( select C.Date, S.weight, S.grade_id, S.price_low, S.price_high from calendar C left outer join spot_offers_summary S on S.date = C.Date where C.Date >= @date and C.date <= getdate()-1 and datename(dw, C.Date) <> 'Saturday' and datename(dw,C.Date) <> 'Sunday') Q where grade_id = @resin_grade_id or grade_id is NULL order by date DESC";

                DBLibrary.BindDataGrid(Application["DBConn"].ToString(), dg, sqlStr, param);
				//DBLibrary.BindDataGrid(Application["DBConn"].ToString(), dg, "SELECT * FROM Spot_Offers_Summary WHERE GRADE_ID=@RESIN_GRADE_ID AND DATE >= @DATE ORDER BY DATE DESC", param);
			}
		}


		private void dg_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			// We use CommandEventArgs e to get the row which is being clicked 
			// This also changes the DataGrid labels into Textboxes so user can edit them 
			dg.EditItemIndex = e.Item.ItemIndex; 
			// Always bind the data so the datagrid can be displayed. 
			createGradeChart();
			BindDG(); 
		}


		private void dg_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			// All we do in the cancel method is to assign '-1' to the datagrid editItemIndex 
			// Once the edititemindex is set to '-1' the datagrid returns back to its original condition 
			dg.EditItemIndex = -1; 
			BindDG();
		}

		private void dg_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			
			string strDate = e.Item.Cells[1].Text;
			//e.Item.Controls[2]
            string strSql = "";
            string strSql2 = "";
			string strPrice_Low = ((TextBox) e.Item.Cells[2].Controls[0]).Text;
			string strPrice_High = ((TextBox) e.Item.Cells[3].Controls[0]).Text;
			string strVolume = ((TextBox) e.Item.Cells[4].Controls[0]).Text;
            string strAsk = ((TextBox)e.Item.Cells[5].Controls[0]).Text;

			string grade_id = ddlChart.SelectedValue;
			Hashtable htParams = new Hashtable();
			htParams.Add("@price_high", strPrice_High);
			htParams.Add("@price_low", strPrice_Low);
			htParams.Add("@grade_id", grade_id);
			htParams.Add("@DATE", strDate);
			htParams.Add("@volume", RemoveComma(strVolume));

            int count = 0;
            string SqlStr = "select count(*) from Spot_Offers_Summary where date = '"+strDate+"' and grade_id = '"+grade_id+"' ";
            count = Convert.ToInt32(DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), SqlStr).ToString());
            if (count==0)
            {
                //insert new item
                strSql = "INSERT INTO Spot_Offers_Summary VALUES (@DATE, @grade_id, @volume, @price_low, @price_high)";
            }
            else
            {
                // updating existing item
                strSql = "Update Spot_Offers_Summary Set price_low=@price_low, price_high=@price_high, weight=@volume WHERE GRADE_ID=@GRADE_ID AND YEAR(DATE)=YEAR(@DATE) AND MONTH(DATE)=MONTH(@DATE) AND DAY(DATE)=DAY(@DATE)";
            }
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSql, htParams);

            Hashtable htParams2 = new Hashtable();
            htParams2.Add("@ContID", grade_id);
            htParams2.Add("@weightdate", strDate);
            htParams2.Add("@ask", strAsk);
            count = -1;
            string SqlStr2 = "select count(*) from spotweightavg where contid = '" + grade_id + "' and CAST(FLOOR(CAST( weightdate AS float)) AS datetime) ='" + strDate + "'  ";
            count = Convert.ToInt32(DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), SqlStr2).ToString());
            if (count == 0)
            {
                //insert new item
                strSql2 = "INSERT INTO spotweightavg VALUES ( @ContID, @weightdate, @ask)";
            }
            else
            {
                // updating existing item
                strSql2 = "Update spotweightavg Set ask=@ask WHERE ContID=@ContID AND CAST(FLOOR(CAST( weightdate AS float)) AS datetime) =@weightdate";
            }
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSql2, htParams2);


			dg.EditItemIndex = -1; 
			BindDG();
			createGradeChart();
		}

		private string RemoveComma(string weight)
		{
            while (weight.Contains(","))
            {
                int commaPlace = weight.IndexOf(",");
                weight = weight.Remove(commaPlace, 1);
            }
            return weight;
		}
        
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_EditCommand);
			this.dg.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_UpdateCommand);

		}
		#endregion

		protected void ddlChart_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BindDG();
			createGradeChart();
		}

		private SeriesCollection getPriceData(int grade_id, string period)
		{
		    DateTime mydate = new DateTime();
		    mydate = DateTime.Now;

		    string Date;

		    int day = mydate.Day;
		    int month = mydate.Month;
		    int year = mydate.Year;
	                        
		    SeriesCollection SC = new SeriesCollection();

		    Series s = new Series();
            Series a = new Series();

		    s.Name = "Price Range";

		    if (period == "1M")
			if (month == 1)
			{
			    month = 12;
                year = year - 1;
			}
			else
			{
			    month = month - 1;                
			}
		    if (period == "1Y")
			year = year - 1;
		    if (period == "5Y")
			year = year - 5;

		    Date = month.ToString() + '-' + day.ToString() + '-' + year.ToString();
	           
		    using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
		    {
			SqlDataReader dtr = null;
			Hashtable param = new Hashtable();
			param.Add("@resin_grade_id", grade_id);
			param.Add("@date", Date);

			dtr = DBLibrary.GetDataReaderStoredProcedure(conn, "spSpot_Offers_SummaryByResinGrade", param);
			while (dtr.Read())
			{
                DateTime date = Convert.ToDateTime(dtr["friday_date"].ToString());
			    Element e = new Element();
			    e.XDateTime = date;


			    //e.SmartLabel.Color = Color.White;



			    double price_low = Convert.ToDouble(dtr["price_low"].ToString());
			    double price_high = Convert.ToDouble(dtr["price_high"].ToString());
                //e.XDateTime = Convert.ToDateTime(dtr["friday_date"]).ToString("mmm dd yyyy");

			    e.Close = price_high;
			    e.Open = price_low;

			    e.High = price_high;
			    e.Low = price_low;

			    s.Elements.Add(e);

			}
		    }

            SC.Add(s);      

            string sSpotMarket = "";
            if (period == "1M")
            {
                sSpotMarket = @"select ask, weightdate from SpotWeightAvg where contid={0} and 
                                weightdate > DATEADD(wk,-4,getdate()) and datepart(dw,weightdate)=6 
                                UNION
                                select ask, weightdate from SpotWeightAvg where contid={1} and 
                                weightdate > DATEADD(dd,-1,getdate())
                                order by weightdate";
            }
            if (period == "1Y")
            {
                sSpotMarket = @"select ask, weightdate from SpotWeightAvg where contid={0} and 
                                weightdate > DATEADD(wk,-51,getdate()) and datepart(dw,weightdate)=6 
                                UNION
                                select ask, weightdate from SpotWeightAvg where contid={1} and 
                                weightdate > DATEADD(dd,-1,getdate())
                                order by weightdate";
            }
            if (period == "5Y")
            {
                sSpotMarket = @"select ask, weightdate from SpotWeightAvg where contid={0} and 
                                weightdate > DATEADD(yy,-5,getdate()) and datepart(dw,weightdate)=6 
                                UNION
                                select ask, weightdate from SpotWeightAvg where contid={1} and 
                                weightdate > DATEADD(dd,-1,getdate())
                                order by weightdate";
            }

            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = null;
                conn.Open();
                SqlCommand oCommand = new SqlCommand(string.Format(sSpotMarket, grade_id, grade_id), conn);
                dtr = oCommand.ExecuteReader();

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["weightdate"].ToString());
                    //Element ePriceRange = new Element();
                    Element ea = new Element();
                    ea.XDateTime = date; 

                    //Bid Element
                    //eBidPrice.ToolTip = dtr["ask"].ToString();
                    //eBidPrice.SmartLabel.DynamicDisplay = false;
                    //eBidPrice.SmartLabel.DynamicPosition = true;
                    //eBidPrice.ShowValue = false;
                    //eBidPrice.SmartLabel.Color = Color.White;
                    //eBidPrice.XDateTime = Convert.ToDateTime(date.ToShortDateString());
                    //eBidPrice.SmartLabel.Text = "";
                    ea.YValue = Convert.ToDouble(dtr["ask"]);
                    //ea.XDateTime = Convert.ToDateTime(dtr["weightdate"]).ToString("mmm dd yyyy");
                    a.AddElements(ea);
                }

            }

            SC.Add(a);
            
		          
		    return (SC);
		}

		private void createGradeChart()
		{

		    string[] period1 = new string[3];
		    period1[0] = "1M";
		    period1[1] = "1Y";
		    period1[2] = "5Y";
		    string nameFile = "Chart";
		    int grade_id = Convert.ToInt32(ddlChart.SelectedValue);

		    for (int k = 0; k < 3; k++)
		    {
	      
			Chart.FileName = nameFile + "_" + ddlChart.SelectedValue + "_" + period1[k];

			Chart.ChartArea.ClearColors();
			Chart.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
			Chart.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);


			// Set the chart type
			Chart.Type = ChartType.Combo;

			Chart.OverlapFooter = true;
			Chart.Mentor = false;
			// Set the size
			Chart.Width = 390;
			Chart.Height = 225;
			// Set the temp directory
			Chart.TempDirectory = "temp";
			// Debug mode. ( Will show generated errors if any )
			Chart.Debug = true;
			//Chart.Title = "Historical Price and Volume: " + ddlResinType.SelectedItem;
			//Chart.Title = "Historical Spot Resin Offers";

			Chart.DefaultSeries.Type = SeriesType.AreaLine;

			Chart.DefaultElement.Marker.Visible = true;

			Color[] MyColorObjectArray = new Color[3] { Color.Red, Color.FromArgb(255, 202, 0), Color.Yellow };
			Chart.Palette = MyColorObjectArray;

			Chart.LegendBox.Visible = false;
			Chart.ChartAreaSpacing = 8;

			Chart.DefaultSeries.DefaultElement.Transparency = 20;

      

			Chart.ChartArea.XAxis.DefaultTick.Line.Color = Color.FromArgb(255, 202, 0);
			Chart.ChartArea.YAxis.DefaultTick.Line.Color = Color.FromArgb(255, 202, 0);
			Chart.XAxis.TimeScaleLabels.MonthFormatString = "MMM dd \n yyyy";
			Chart.XAxis.DefaultTick.Label.Text = "<%Name,MMM dd> \n <%Name,yyyy>";
			Chart.ChartArea.YAxis.DefaultTick.Label.Font = new Font(Chart.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
			Chart.ChartArea.XAxis.DefaultTick.Label.Font = new Font(Chart.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);
			Chart.XAxis.LabelRotate = true;

			Chart.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
			Chart.ChartArea.XAxis.TickLabelAngle = 90;

            Chart.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            Chart.DefaultSeries.DefaultElement.Marker.Size = 4;
            Chart.DefaultElement.Marker.Visible = true;
            Chart.DefaultSeries.DefaultElement.Marker.Color = Color.White;

			// Setup the axes.
			//Chart.YAxis.Label.Text = "Price";
			//          Chart.YAxis.FormatString = "Currency";
			Chart.YAxis.Scale = Scale.Range;
			Chart.XAxis.Scale = Scale.Range;

			SeriesCollection mySC = getPriceData(grade_id, period1[k]);			
            mySC[0].Type = SeriesTypeFinancial.Bar;
            mySC[1].Type = SeriesType.Line;
			// SeriesCollection mySCV = getVolumeData(grade_id);

			Chart.ChartArea.Line.Color = Color.Orange;

			// Add the price data.
			Chart.SeriesCollection.Add(mySC);


			//Chart
			//Chart.TitleBox.Background.Color = Color.Orange;
			Chart.Background.Color = Color.Black;
			Chart.ChartArea.Background.Color = Color.Black;
			Chart.ChartArea.DefaultSeries.Line.Color = Color.Red;
			Chart.ChartArea.DefaultSeries.Line.Width = 2;
            

			Chart.ChartArea.XAxis.Line.Width = 4;
			Chart.ChartArea.YAxis.Line.Width = 2;

			//XAxis
			Chart.XAxis.Label.Color = Color.White;
			Chart.ChartArea.XAxis.Label.Color = Color.White;
			Chart.XAxis.Line.Color = Color.Orange;
			Chart.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;

			//YAxis
			Chart.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
			Chart.ChartArea.YAxis.Label.Color = Color.White;
			Chart.YAxis.Line.Color = Color.Orange;
			Chart.YAxis.Label.Color = Color.White;

			Chart.ChartArea.Line.Width = 2;

			Chart.TempDirectory = Server.MapPath("/Research/charts/pv");

			Chart.FileManager.ImageFormat = ImageFormat.Png;
			//                            ChartObj.OverlapFooter = true;

			Bitmap bmp1 = Chart.GetChartBitmap();

			string fileName2 = "";
			fileName2 = Chart.FileManager.SaveImage(bmp1);

			Chart.SeriesCollection.Clear();
	                
		    }
		    imgChart.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/charts/pv/" + nameFile + "_" + ddlChart.SelectedValue + "_1M.png";
		    imgChart1.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/charts/pv/" + nameFile + "_" + ddlChart.SelectedValue + "_1Y.png";
		    imgChart2.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/charts/pv/" + nameFile + "_" + ddlChart.SelectedValue + "_5Y.png";
		}

		protected void dg_ItemDataBound(object sender, DataGridItemEventArgs e)
		{

		    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
		    {
			    DateTime RowDate = Convert.ToDateTime(e.Item.Cells[1].Text);

			    if (RowDate.DayOfWeek.ToString() == "Friday")
			    {

			        e.Item.BackColor = Color.Orange;

			    }

                if (Convert.ToDecimal(e.Item.Cells[2].Text) == 0)
                {
                    e.Item.BackColor = Color.Red;
                }
            

		    }
	          
		}

		protected void btnEdit_Click(object sender, EventArgs e)
		{      
		    BindDG();
		    createGradeChart();

		}




		protected void btnExportToExcel_Click(object sender, System.EventArgs e)
		{
		    //export to excel

		    BindDG();

		    dg.Columns[0].Visible = false;

		    Response.Clear();
		    Response.Buffer = true;
		    Response.ContentType = "application/vnd.ms-excel";

		    string filename = ddlChart.SelectedItem.Text;
	            
		    Response.AddHeader("Content-Disposition", "attachment; filename='" + filename  + "'");


		    Response.BufferOutput = true;
		    Response.ContentEncoding = System.Text.Encoding.UTF8;
		    Response.Charset = "";
		    EnableViewState = false;

		    System.IO.StringWriter tw = new System.IO.StringWriter();
		    System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);


		    FormatExportedGrid(dg);
		    this.ClearControls(dg);
		    dg.RenderControl(hw);

		    Response.Write(CleanHTML(tw.ToString()));
		    Response.End();

		
		}

		public void FormatExportedGrid(DataGrid dg1)
		{
		    dg1.BackColor = System.Drawing.Color.White;
		}


		private string CleanHTML(string OutOut)
		{

		    //will add some code to fix gridlines

		    return OutOut;


		}


		public override void VerifyRenderingInServerForm(Control control)
		{
		    /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
		       server control at run time. */
		}

		private void ClearControls(Control control)
		{
		    for (int i = control.Controls.Count - 1; i >= 0; i--)
		    {
			ClearControls(control.Controls[i]);
		    }
		    if (!(control is TableCell))
		    {
			if (control.GetType().GetProperty("SelectedItem") != null)
			{
			    LiteralControl literal = new LiteralControl();
			    control.Parent.Controls.Add(literal);
			    try
			    {
				literal.Text = (string)control.GetType().GetProperty("SelectedItem").GetValue(control, null);
			    }
			    catch
			    {
			    }
			    control.Parent.Controls.Remove(control);
			}
			else
			    if (control.GetType().GetProperty("Text") != null)
			    {
				LiteralControl literal = new LiteralControl();
				control.Parent.Controls.Add(literal);
				literal.Text = (string)control.GetType().GetProperty("Text").GetValue(control, null);
				control.Parent.Controls.Remove(control);
			    }
		    }
		    return;
		}

        
	}
}
