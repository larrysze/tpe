using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;
using System.Web.Mail;
using System.Text;
using System.IO;
using System.Threading;

namespace localhost.Research
{
    /// <summary>
    /// Summary description for WeeklyReview.
    /// </summary>
    public partial class MarketResearch : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.Label lblPrintLink;


        #region OldMUDates array
        private readonly string[] OldMUDates = { 
                "August 10, 2005", 
                "July 5, 2005", 
                "June 1, 2005",
                "April 18, 2005",
                "February 28, 2005",
                "January 6, 2005",
                "November 8, 2004",
                "September 9, 2004",
                "July 27, 2004",
                "June 3, 2004",
                "April 27, 2004",
                "March 10, 2004",
                "January 27, 2004", //30
                "December 19, 2003",
                "November 4, 2003",
                "September 15, 2003",
                "July 31, 2003",
                "June 17, 2003",
                "April 24, 2003",
                "March 25, 2003",
                "February 19, 2003",
                "January 23, 2003",
                "December 11, 2002",
                "October 23, 2002",//20
                "September 12, 2002",
                "August 6, 2002",
                "May 22, 2002",
                "March 27, 2002",
                "February 7, 2002",
                "November 30, 2001",
                "October 23, 2001",
                "September 12, 2001",
                "August 15, 2001",
                "July 9, 2001",
                "June 27, 2001",
                "April 24, 2001" ,
                "March 27, 2001",
                "March 8, 2001",
                "February 15, 2001",
                "January 24, 2001",
                "January 5, 2001",
                "December 18, 2000",
                "December 6, 2000"
            };
        #endregion


        protected void Page_Load(object sender, System.EventArgs e)
        {


            btnEmail.Attributes["onMouseOut"] = "document.getElementById('" + btnEmail.ClientID + "').src='/images/buttons/submit_orange.jpg';";
            btnEmail.Attributes["OnMouseOver"] = "document.getElementById('" + btnEmail.ClientID + "').src='/images/buttons/submit_red.jpg';";


            // Put user code to initialize the page here
            if (!Page.IsPostBack)
            {
                initDDL();
             

                if (Request["selectedDate"] != null)
                {
            
                    DateTime review_date = Convert.ToDateTime(Request["selectedDate"].ToString());

                    for (int i = 0; i < ddlIssue.Items.Count; i++)
                    {
                        DateTime ddlItem = Convert.ToDateTime(ddlIssue.Items[i].ToString());
                        if (ddlItem.Year == review_date.Year)
                            if (ddlItem.Month == review_date.Month)
                                if (ddlItem.Day == review_date.Day)
                                {
                                    ddlIssue.SelectedItem.Selected = false;
                                    ddlIssue.Items[i].Selected = true;
                                }
                    }
            
                }
         
            }

            lblMarketUpdateDate.Text = ddlIssue.SelectedValue;

            DateTime selectedDate = Convert.ToDateTime(ddlIssue.SelectedItem.ToString());

            string dateFileName = selectedDate.Month + "_" + selectedDate.Day + "_" + selectedDate.Year;
            

            string strPdfFileName = Server.MapPath("/Research/Market_Update/pdf/" + dateFileName + ".pdf");
            if (System.IO.File.Exists(strPdfFileName))
            {
                lblPrintLink.Text = "Click <a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Market_Update/pdf/" + dateFileName + ".pdf'>here</a> for a printable version";
            }
            else
            {
                lblPrintLink.Text = "";
            }

            this.trPrint.Visible = (lblPrintLink.Text != "");

            BindGrid(ddlIssue.SelectedItem.ToString());

            string dateChart = Convert.ToString(selectedDate.Month) + "_" + Convert.ToString(selectedDate.Day) + "_" + Convert.ToString(selectedDate.Year);
            PeMonth.ImageUrl = @"\Research\Weekly_Reviews\pics\" + dateChart + @"\Chart_3_1M.png";
            PeYear.ImageUrl = @"\Research\Weekly_Reviews\pics\" + dateChart + @"\Chart_3_1Y.png";
            PeFiveYear.ImageUrl = @"\Research\Weekly_Reviews\pics\" + dateChart + @"\Chart_3_5Y.png";

            PpMonth.ImageUrl = @"\Research\Weekly_Reviews\pics\" + dateChart + @"\Chart_9_1M.png";
            PpYear.ImageUrl = @"\Research\Weekly_Reviews\pics\" + dateChart + @"\Chart_9_1Y.png";
            PpFiveYear.ImageUrl = @"\Research\Weekly_Reviews\pics\" + dateChart + @"\Chart_9_5Y.png";
            
            imgCrudeOil.ImageUrl = @"\Research\Weekly_Reviews\pics\" + dateChart + @"\crude_oil.png";


        }

        private void BindGrid(string strDate)
        {
            DateTime review_date = Convert.ToDateTime(strDate);
            if (review_date < new DateTime(2009, 1, 1))
            {
                Response.Redirect("/Research/WeeklyReview.aspx?selectedDate=" + strDate);
            }

            Hashtable htParam = new Hashtable();
            htParam.Add("@date", strDate);
            TPE.Utility.DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(), dg, "spWeekInReviewSpotOffersSnapshot", htParam);


            string seleStr = "SELECT SUM(weight) AS TotalVolume FROM WeeklyReviewSpotOffersSummary WHERE datediff(day,date,@date)=0";
            string total = DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), seleStr, htParam).ToString();
            if (total.Trim().Equals(""))
            {
                lblTotalVolume.Text = "Not available";
            }
            else
            {
                lblTotalVolume.Text = String.Format("{0:#,###}", decimal.Parse(total)) + " lbs";
            }


            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();

                string sSQL = "SELECT comments, polyethylene, polypropylene FROM weekly_reviews WHERE published = 1 AND datediff(day,review_date,@date)=0";

                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, sSQL, htParam);

                while (dtr.Read())
                {
                    lblMarketSummary.Text = dtr["comments"].ToString();
                    lblPeSummary.Text = dtr["polyethylene"].ToString();
                    lblPpSummary.Text = dtr["polypropylene"].ToString();
                }
            }


        }

        private void initDDL()
        {
            // ArrayList ReviewDates =  new ArrayList();

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();

                string sSQL = "SELECT DATENAME(MONTH,date) +  ' ' + DATENAME(DAY,date) + ', ' + DATENAME(YEAR,date) as MUDATE,date FROM MARKET_UPDATES WHERE published = 1 UNION select DATENAME(MONTH,review_date) +  ' ' + DATENAME(DAY,review_date) + ', ' + DATENAME(YEAR,review_date) as MUDATE,review_date from WEEKLY_REVIEWS WHERE published = 1  ORDER BY DATE DESC";

                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, sSQL);

                while (dtr.Read())
                {
                    ddlIssue.Items.Add(dtr["MUDATE"].ToString());
                }
            }

            ListItem li = null;
            for (int i = 0; i < OldMUDates.Length - 1; i++)
            {   // add old dates with negative values ie, -44..0
                li = new ListItem(OldMUDates[i], (i - OldMUDates.Length).ToString());
                ddlIssue.Items.Add(li);
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            btnEmail.Click += new ImageClickEventHandler(this.btnEmail_Click);
            cvEmail.ServerValidate += new ServerValidateEventHandler(this.cvEmail_Validate);
        }
        #endregion

        private void cvEmail_Validate(object source, ServerValidateEventArgs args)
        {
            lblEmail.Text = "";
            //Check if the email is in the DB
            Hashtable param = new Hashtable();
            string email = txtEmail.Text;

            string strSql = "select MAI_MAIL FROM mailing WHERE MAI_MAIL = @Email";
            param.Add("@Email", email);
            DataTable dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), strSql, param);

            if (dt.Rows.Count == 0)
                args.IsValid = true;
            else
            {
                args.IsValid = false;
                cvEmail.ErrorMessage = email + " is already in our database";
            }

        }

        private void btnEmail_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (this.IsValid)
            {
                lblEmail.Text = "";
                Hashtable param = new Hashtable();
                string email = txtEmail.Text;
                param.Add("@Email", email);

                string strSqlInsert = "INSERT INTO mailing (MAI_MAIL, MAI_CODE) VALUES (@Email, '0')";
                DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlInsert, param);

                lblEmail.Text = "Thank you! " + email + " has been added to our distribution list.";
                txtEmail.Text = "";

                //Mike wants to receive an email when somebody subscribed to MU
                string txtMessage = "This email: " + email + " was added to the subscription list of Market Update.";
                string txtSubject = email + " in MU list";
                MailMessage mailToMike = new MailMessage();
                mailToMike.From = "research@theplasticsexchange.com";
                mailToMike.To = "michael@theplasticexchange.com";
                mailToMike.Subject = txtSubject;
                mailToMike.Body = txtMessage;
                EmailLibrary.Send(mailToMike);
            }
        }

        protected void ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Footer && e.Item.ItemType != ListItemType.Header)
            {

                if (Convert.IsDBNull(DataBinder.Eval(e.Item.DataItem, "PriceBid")) && Convert.IsDBNull(DataBinder.Eval(e.Item.DataItem, "PriceOffer")))
                {
                    dg.Columns[4].Visible = false;
                    dg.Columns[5].Visible = false;
                }
                else
                {
                    dg.Columns[4].Visible = true;
                    dg.Columns[5].Visible = true;
                }

                string gradeName = DataBinder.Eval(e.Item.DataItem, "GRADE").ToString();
                if (gradeName == "PP Homopolymer - Inj")
                    e.Item.Cells[0].Text = "<b>PP Homo</b>";
                if (gradeName == "PP Copolymer - Inj")
                    e.Item.Cells[0].Text = "<b>PP Copo</b>";
            }
        }

  
    }
}
