using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;
using dotnetCHARTING;

namespace localhost.Research
{
    public partial class MonomerList : System.Web.UI.Page
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {
            Master.Width = "780px";
            // Put user code to initialize the page here

            if ((string)Session["Typ"] != "A")
            {
                Response.Redirect("../default.aspx");
            }

            if (!IsPostBack)
            {
                loadDDL();
                BindDG();
                //CreateChart();

                CreateEthyleneChainChart();
                CreatePropyleneChainChart();

            }
        }
        private void CreateChart()
        {
            dotnetCHARTING.Chart ChartObj = new dotnetCHARTING.Chart();

            ChartObj.ChartArea.ClearColors();

            ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            ChartObj.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);

            ChartObj.TempDirectory = Server.MapPath("/Research/charts/dp");

            ChartObj.Width = 390;
            ChartObj.Height = 225;

            string[] period1 = new string[3];
            period1[0] = "1M";
            period1[1] = "1Y";
            period1[2] = "5Y";
            string nameFile = "Chart";
            //ChartObj.SeriesCollection.Add(getData(Convert.ToInt32(myReader2.GetValue(0)), period1, nameFile));

            for (int k = 0; k < 3; k++)
            {
                ChartObj.FileName = nameFile + "_" + ddlChart.SelectedValue + "_" + period1[k];

                ChartObj.SeriesCollection.Add(getData(Convert.ToInt32(ddlChart.SelectedValue), period1[k], nameFile));

                ChartObj.Use3D = false;

                ChartObj.Width = 390;
                ChartObj.Height = 225;
                ChartObj.ChartArea.ClearColors();

                ChartObj.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
                ChartObj.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
                ChartObj.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
                ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);

                ChartObj.XAxis.TimeInterval = TimeInterval.Week;

                ChartObj.ChartArea.DefaultElement.Color = Color.Orange;
                ChartObj.ChartArea.DefaultSeries.Line.Color = Color.Orange;
                ChartObj.ChartArea.DefaultSeries.Line.Width = 2;

                ChartObj.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
                ChartObj.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
                ChartObj.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
                ChartObj.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartObj.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);

                ChartObj.ChartArea.XAxis.DefaultTick.Line.Color = Color.FromArgb(254, 0, 0); //red
                ChartObj.ChartArea.YAxis.DefaultTick.Line.Color = Color.FromArgb(254, 0, 0); //red

                ChartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;

                ChartObj.ChartArea.XAxis.Line.Width = 4;
                ChartObj.ChartArea.YAxis.Line.Width = 2;

                ChartObj.YAxis.Interval = 0.01;

                ChartObj.ChartArea.XAxis.LabelRotate = true;

                ChartObj.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
                ChartObj.ChartArea.XAxis.TickLabelAngle = 90;

                ChartObj.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
                ChartObj.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);

                // Set a default transparency
                ChartObj.DefaultSeries.DefaultElement.Transparency = 20;

                // Set color of axis lines
                Axis AxisObj = new Axis();
                AxisObj.Line.Color = Color.FromArgb(255, 255, 0, 0);

                ChartObj.DefaultAxis = AxisObj;

                ChartObj.MarginLeft = 0;
                ChartObj.MarginRight = 0;
                ChartObj.MarginTop = 0;
                ChartObj.MarginBottom = 0;

                // Set the Default Series Type
                ChartObj.DefaultSeries.Type = SeriesType.Line;
                ChartObj.LegendBox.Position = LegendBoxPosition.None;

                // Set the y Axis Scale
                ChartObj.ChartArea.YAxis.Scale = Scale.Range;

                ChartObj.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
                ChartObj.DefaultSeries.DefaultElement.Marker.Size = 6;
                ChartObj.DefaultSeries.DefaultElement.Marker.Color = Color.White;//Color.FromArgb( 255, 255, 153, 9);
                ChartObj.XAxis.Label.Color = Color.White;

                ChartObj.ChartArea.Line.Color = Color.FromArgb(255, 255, 0, 0);//(254, 0, 0)
                ChartObj.ChartArea.Line.Width = 2;

                ChartObj.Background.Color = Color.Black;
                ChartObj.ChartArea.Background.Color = Color.Black;

                ChartObj.FileManager.ImageFormat = ImageFormat.Png;
                //                            ChartObj.OverlapFooter = true;

                Bitmap bmp1 = ChartObj.GetChartBitmap();

                string fileName2 = "";
                fileName2 = ChartObj.FileManager.SaveImage(bmp1);

                ChartObj.SeriesCollection.Clear();

            }
            imgChart.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/charts/dp/" + nameFile + "_" + ddlChart.SelectedValue + "_1M.png";
            imgChart1.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/charts/dp/" + nameFile + "_" + ddlChart.SelectedValue + "_1Y.png";
            imgChart2.ImageUrl = "http://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/research/charts/dp/" + nameFile + "_" + ddlChart.SelectedValue + "_5Y.png";


            //       ChartObj.SeriesCollection.Clear();0

        }


        private SeriesCollection getData(int iContract, string period, string nameFile)
        {
            SqlConnection conn;
            SqlDataReader dtrData;
            SqlCommand cmdData;
            string strSQL;
            conn = new SqlConnection(Application["MonoDB"].ToString());
            conn.Open();
            int iPreRead = 0; // the number of preloaded items
            int iInterval = 0; // the space between each items

            int range = 1;
            int count = 0;
            string tmpSQL = "";


            switch (period)
            {

                default:
                    strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate()) ";
                    iPreRead = 2;
                    iInterval = 0;
                    range = 4;
                    tmpSQL = "select COUNT(*) from HISTORICAL_PRICES WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate())";
                    cmdData = new SqlCommand(tmpSQL, conn);
                    cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                    count = Convert.ToInt32(cmdData.ExecuteScalar());

                    break;
                //case "1Y":
                //	strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-1,getdate()) ";
                //	iInterval = 0;
                //	iPreRead = 0;
                //	break;
                case "5Y":
                    strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate()) ";
                    iInterval = 0;
                    iPreRead = 2;
                    range = 4;
                    tmpSQL = "select COUNT(*) from HISTORICAL_PRICES WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-5,getdate())";
                    cmdData = new SqlCommand(tmpSQL, conn);
                    cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                    count = Convert.ToInt32(cmdData.ExecuteScalar());

                    break;
                case "10Y":
                    strSQL = "WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-10,getdate()) ";
                    iInterval = 2;
                    iPreRead = 5;
                    range = 10;
                    tmpSQL = "select COUNT(*) from HISTORICAL_PRICES WHERE CONT_ID=@CONTRACT AND PRICE_DATE > DATEADD(yy,-10,getdate())";
                    cmdData = new SqlCommand(tmpSQL, conn);
                    cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                    count = Convert.ToInt32(cmdData.ExecuteScalar());

                    break;
                case "Forward":
                    strSQL = "Select FWD_ID, DATE= LEFT(FWD_MNTH,3)+' '+RIGHT(FWD_YEAR,2), PRICE = (SELECT RIGHT(CONVERT(varchar,MAX(OFFR_PRCE)),4) FROM OFFER WHERE OFFR_CONT='" + iContract.ToString() + "' AND OFFR_MNTH=FWD_ID AND OFFR_SIZE='1') From FWDMONTH WHERE FWD_ACTV ='1' ORDER BY FWD_ID ASC";
                    break;
            }

            strSQL = "select PRICE,PRICE_DATE AS DATE from HISTORICAL_PRICES " + strSQL + " ORDER BY PRICE_DATE";
            if (period.Equals("1M"))
            {
                // needs to overwrite string completely if it is one month
                strSQL = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(dd,-30,getdate())  ORDER BY DATE";
                iPreRead = 0;
                iInterval = 0;
                range = 4;
                tmpSQL = "select count(*) from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(dd,-30,getdate())";
                cmdData = new SqlCommand(tmpSQL, conn);
                cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                count = Convert.ToInt32(cmdData.ExecuteScalar());
            }
            //			if (period.Equals("6M"))
            //			{
            //				// needs to overwrite string completely if it is one month
            //				strSQL = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(mm,-6,getdate())  ORDER BY DATE";
            //				iPreRead = 12;
            //				iInterval = 6;
            //			}
            if (period.Equals("1Y"))
            {
                // needs to overwrite string completely if it is one month
                strSQL = "select ASK as PRICE,DATE from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(mm,-12,getdate()) AND DATEPART(Dw,DATE)=DATEPART(Dw, (select max(date) from export_price WHERE CONT_ID=@CONTRACT)) ORDER BY DATE";
                iPreRead = 0; //24;
                iInterval = 0; //6;
                range = 4;

                tmpSQL = "select COUNT(*) from EXPORT_PRICE WHERE CONT_ID=@CONTRACT AND DATE > DATEADD(mm,-12,getdate()) AND DATEPART(Dw,DATE)=DATEPART(Dw, (select max(date) from export_price WHERE CONT_ID=@CONTRACT))";
                cmdData = new SqlCommand(tmpSQL, conn);
                cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
                count = Convert.ToInt32(cmdData.ExecuteScalar());

            }


            cmdData = new SqlCommand(strSQL, conn);
            cmdData.Parameters.AddWithValue("@CONTRACT", iContract);
            dtrData = cmdData.ExecuteReader();

            SeriesCollection SC = new SeriesCollection();

            for (int k = 0; k < iPreRead; k++)
            {
                if (dtrData.Read())
                { }
            }
            int i = 1;
            int add = 0;

            if (count != 0)
            {
                add = count % range;
            }


            Series s = new Series();

            while (dtrData.Read())
            {
                if (dtrData["DATE"] != DBNull.Value && dtrData["PRICE"] != DBNull.Value)
                {
                    Element e = new Element();
                    //					e.ToolTip = dtrData["PRICE"].ToString();
                    //					e.SmartLabel.DynamicDisplay = false;
                    //					e.SmartLabel.DynamicPosition = true;
                    //					e.ShowValue = false;
                    //e.Name = dtrData["DATE"].ToString();

                    if (((i - add) % range) == 0)
                    {
                        e.Name = Convert.ToDateTime(dtrData["DATE"]).ToString("MMM dd\r\nyyy");
                        //						e.SmartLabel.Text = dtrData["PRICE"].ToString();
                        e.SmartLabel.Color = Color.White;
                        e.XDateTime = (DateTime)dtrData["DATE"];
                    }
                    else
                    {
                        //e.Name = "";
                        e.SmartLabel.Text = "";
                    }

                    i++;


                    //e.Color = Color.Blue;//Color.FromArgb(255, 203, 1);
                    //					e.AxisMarker.Label.Color = Color.Red;

                    e.YValue = Convert.ToDouble(dtrData["PRICE"]);
                    //                    e.Hotspot.ToolTip = Convert.ToDouble(dtrData["PRICE"]).ToString();
                    /* wanted to have 'hand-cursor' while over values
                    e.Hotspot.Attributes.Custom.Add("onmouseover", "this.style.cursor='hand'");
                    e.Hotspot.Attributes.Custom.Add("onmouseout", "this.style.cursor='pointer'");
                    */
                    //                    s.Element.ShowValue = true;
                    s.Elements.Add(e);
                }

                if (period.Equals("6M") || period.Equals("1Y"))// || period.Equals("10Y")
                {
                    for (int k = 0; k < iInterval; k++)
                    {
                        if (dtrData.Read())
                        { }
                    }
                }
            }
            SC.Add(s);

            //ChartObj.Depth = 15;
            // Set 3D



            return (SC);
        }

        private void loadDDL()
        {

            using (SqlConnection conn = new SqlConnection(Application["MonoDB"].ToString()))
            {
                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, "SELECT * FROM CONTRACT order by cont_ordr ");
                while (dtr.Read())
                {
                    ddlChart.Items.Add(new ListItem(dtr["CONT_LABL"].ToString(), dtr["CONT_ID"].ToString()));
                }

                ddlChart.Items[0].Selected = true;
            }

        }

        private void BindDG()
        {

            using (SqlConnection conn = new SqlConnection(Application["MonoDB"].ToString()))
            {
                //  Hashtable param = new Hashtable();
                //  param.Add("@cont_id", ddlChart.SelectedValue);
                string sSql;

                sSql = string.Format("SELECT * FROM EXPORT_PRICE WHERE CONT_ID={0} AND DATE >= '02-04-2004' ORDER BY DATE DESC", ddlChart.SelectedValue);

                if (cbMonth.Checked)
                {

                    sSql = string.Format("select PRICE_DATE as Date, Price as ASK from HISTORICAL_PRICES WHERE CONT_ID={0} ORDER BY DATE DESC", ddlChart.SelectedValue);
                }

                if (cbWeek.Checked)
                {

                    sSql = string.Format("SELECT * FROM EXPORT_PRICE WHERE CONT_ID={0} AND DATE >= '02-04-2004' and  datepart(dw,date) = 6 ORDER BY DATE DESC", ddlChart.SelectedValue);
                }

                DataSet DS;
                SqlDataAdapter MyCommand;
                MyCommand = new SqlDataAdapter(sSql, conn);
                DS = new DataSet();
                MyCommand.Fill(DS, "tblDynamicText");

                if (!cbMonth.Checked)
                {
                    if (!dgDaily.Visible)
                    {
                        dgDaily.CurrentPageIndex = 0;
                    }
                    //dgDaily.CurrentPageIndex = 0;
                    dgDaily.DataSource = DS;
                    dgDaily.DataBind();
                    dgDaily.Visible = true;
                    dgMonthly.Visible = false;
                }
                else
                {
                    if (!dgMonthly.Visible)
                    {
                        dgMonthly.CurrentPageIndex = 0;
                    }
                    //dgMonthly.CurrentPageIndex = 0;
                    dgMonthly.DataSource = DS;
                    dgMonthly.DataBind();
                    dgMonthly.Visible = true;
                    dgDaily.Visible = false;
                }

                //dg.DataSource = DS;
                //dg.DataBind();
                conn.Close();
                //Hashtable param = new Hashtable();
                //param.Add("@cont_id", ddlChart.SelectedValue);
                //param.Add("@date", "02-04-2004");

                //DBLibrary.BindDataGrid(Application["DBConn"].ToString(), dg, "SELECT * FROM EXPORT_PRICE WHERE CONT_ID=@CONT_ID AND DATE >= @DATE ORDER BY DATE DESC", param);

            }
        }

        private void dgMonthly_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            // We use CommandEventArgs e to get the row which is being clicked 
            // This also changes the DataGrid labels into Textboxes so user can edit them 
            dgMonthly.EditItemIndex = e.Item.ItemIndex;
            // Always bind the data so the datagrid can be displayed. 
            BindDG();
        }

        private void dgDaily_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            // We use CommandEventArgs e to get the row which is being clicked 
            // This also changes the DataGrid labels into Textboxes so user can edit them 
            dgDaily.EditItemIndex = e.Item.ItemIndex;
            // Always bind the data so the datagrid can be displayed. 
            BindDG();
        }

        public void Check_Clicked(Object sender, EventArgs e)
        {

            cbWeek.Checked = false;
            BindDG();
        }
        
        public void Check_Clicked2(Object sender, EventArgs e)
        {

            cbMonth.Checked = false;
            dgDaily.CurrentPageIndex = 0;
            BindDG();
        }
        
        
        protected void dgDaily_ItemDataBound(object sender, DataGridItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DateTime RowDate = Convert.ToDateTime(e.Item.Cells[1].Text);

                if (RowDate.DayOfWeek.ToString() == "Friday")
                {
                    if (!cbWeek.Checked)
                        e.Item.BackColor = Color.Orange;

                }

            }

        }

        private void dgMonthly_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            // All we do in the cancel method is to assign '-1' to the datagrid editItemIndex 
            // Once the edititemindex is set to '-1' the datagrid returns back to its original condition 
            dgMonthly.EditItemIndex = -1;
            BindDG();
        }

        private void dgDaily_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            // All we do in the cancel method is to assign '-1' to the datagrid editItemIndex 
            // Once the edititemindex is set to '-1' the datagrid returns back to its original condition 
            dgDaily.EditItemIndex = -1;
            BindDG();
        }


        private void dgMonthly_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {

            string strDate = e.Item.Cells[1].Text;
            //e.Item.Controls[2]


            string strAsk = ((TextBox)e.Item.Cells[2].Controls[0]).Text;

            string cont_id = ddlChart.SelectedValue;
            Hashtable htParams = new Hashtable();
            htParams.Add("@ask", strAsk);
            htParams.Add("@cont_id", cont_id);
            htParams.Add("@DATE", strDate);


            // updating existing item
            string strSqlUpdate = "Update HISTORICAL_PRICES Set price=@ask WHERE CONT_ID=@cont_id AND YEAR(PRICE_DATE)=YEAR(@DATE) AND MONTH(PRICE_DATE)=MONTH(@DATE) AND DAY(PRICE_DATE)=DAY(@DATE)";
            DBLibrary.ExecuteSQLStatement(Application["MonoDB"].ToString(), strSqlUpdate, htParams);

            dgMonthly.EditItemIndex = -1;
            BindDG();

        }
        
        private void dgDaily_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {

            string strDate = e.Item.Cells[1].Text;
            //e.Item.Controls[2]

            string strBid = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
            string strAsk = ((TextBox)e.Item.Cells[3].Controls[0]).Text;

            string cont_id = ddlChart.SelectedValue;
            Hashtable htParams = new Hashtable();
            htParams.Add("@ask", strAsk);
            htParams.Add("@bid", strBid);
            htParams.Add("@cont_id", cont_id);
            htParams.Add("@DATE", strDate);


            // updating existing item
            string strSqlUpdate = "Update Export_Price Set BID=@bid, ASK=@ask WHERE CONT_ID=@cont_id AND YEAR(DATE)=YEAR(@DATE) AND MONTH(DATE)=MONTH(@DATE) AND DAY(DATE)=DAY(@DATE)";
            DBLibrary.ExecuteSQLStatement(Application["MonoDB"].ToString(), strSqlUpdate, htParams);

            dgDaily.EditItemIndex = -1;
            BindDG();

        }
        
        public void myDataGrid_PageChanger(object Source, DataGridPageChangedEventArgs E)
        {
            dgDaily.CurrentPageIndex = E.NewPageIndex;
            dgMonthly.CurrentPageIndex = E.NewPageIndex;
            BindDG();
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgMonthly.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgMonthly_EditCommand);
            this.dgMonthly.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgMonthly_UpdateCommand);

            this.dgDaily.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgDaily_EditCommand);
            this.dgDaily.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgDaily_UpdateCommand);

        }
        #endregion

        protected void ddlChart_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BindDG();
            CreateChart();
        }



        private void CreateEthyleneChainChart()
        {
            ChartEthylene.ChartArea.ClearColors();
            ChartEthylene.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartEthylene.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);

            ChartEthylene.OverlapFooter = true;
            ChartEthylene.Mentor = false;
            ChartEthylene.TempDirectory = "/research/charts/dp/mono/";
            ChartEthylene.FileName = " EthyleneChain";
            ChartEthylene.Width = 800;
            ChartEthylene.Height = 600;
            ChartEthylene.FileManager.ImageFormat = ImageFormat.Png;
            // Set the chart type                        
            ChartEthylene.Type = ChartType.Combo;

            ChartEthylene.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            ChartEthylene.DefaultSeries.DefaultElement.Marker.Size = 6;
            ChartEthylene.DefaultSeries.DefaultElement.Marker.Color = Color.White;//Color.FromArgb( 255, 255, 153, 9);
            ChartEthylene.XAxis.Label.Color = Color.White;

            // Debug mode. ( Will show generated errors if any )
            ChartEthylene.Debug = false;
            ChartEthylene.Title = "The Ethylene Chain";

            ChartEthylene.TitleBox.Position = TitleBoxPosition.FullWithLegend;
            ChartEthylene.DefaultElement.Marker.Visible = false;

            Color[] MyColorObjectArray = new Color[3] { Color.Red, Color.FromArgb(255, 202, 0), Color.Yellow };
            ChartEthylene.Palette = MyColorObjectArray;

            ChartEthylene.ChartAreaSpacing = 3;
            ChartEthylene.LegendBox.Template = "%Icon%Name";
            ChartEthylene.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;

//            // Modify the x axis labels.
            ChartEthylene.XAxis.TimeInterval = TimeInterval.Day;
            ChartEthylene.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartEthylene.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            ChartEthylene.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            //ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);
            ChartEthylene.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartEthylene.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartEthylene.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartEthylene.ChartArea.XAxis.TickLabelAngle = 90;

            ChartEthylene.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartEthylene.ChartArea.XAxis.LabelRotate = true;
            ChartEthylene.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            //ChartObj.XAxis.TimeInterval = TimeInterval.Month;


            //XAxis
            ChartEthylene.XAxis.Label.Color = Color.White;
            ChartEthylene.ChartArea.XAxis.Label.Color = Color.White;
            ChartEthylene.XAxis.Line.Color = Color.Orange;
            ChartEthylene.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;

            ChartEthylene.YAxis.Interval = 0.01;
            // ChartEthylene the axes.
            ChartEthylene.YAxis.Label.Text = "Price";
            ChartEthylene.YAxis.FormatString = "Currency";
            ChartEthylene.YAxis.Scale = Scale.Range;

            ChartEthylene.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartEthylene.ChartArea.YAxis.Label.Color = Color.White;
            ChartEthylene.YAxis.Line.Color = Color.Orange;
            ChartEthylene.YAxis.Label.Color = Color.White;



            //Chart
            ChartEthylene.TitleBox.Background.Color = Color.Orange;
            ChartEthylene.Background.Color = Color.Black;
            ChartEthylene.ChartArea.Background.Color = Color.Black;
            ChartEthylene.ChartArea.DefaultSeries.Line.Color = Color.Red;
            //CurrChart.ChartArea.DefaultSeries.Line.StartCap = System.Drawing.Drawing2D.LineCap.Triangle;
            //CurrChart.ChartArea.DefaultSeries.Line.EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
            ChartEthylene.ChartArea.DefaultSeries.Line.Width = 2;

            ChartEthylene.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartEthylene.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
            ChartEthylene.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartEthylene.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);

            //CurrChart.ChartArea.Line.Color = Color.FromArgb(255, 255, 0, 0);//(254, 0, 0)
            ChartEthylene.ChartArea.Line.Color = Color.Orange;
            //CurrChart.ChartArea.Line.Width = 2;


            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();
            ChartEthylene.SeriesCollection.Add(getCreateEthyleneChainChartPriceData());

                           
            }
       
        private SeriesCollection getCreateEthyleneChainChartPriceData()
        {
            string sSqlEthane = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=38 and export_price.date >'12/31/2006' and datepart(dw,export_price.date) = 6 ";
            string sSqlEthylene = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=33 and export_price.date >'12/31/2006'  and datepart(dw,export_price.date) = 6";
            string sSqlLDPE = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=11 and export_price.date >'12/31/2006'  and datepart(dw,export_price.date) = 6";

            SeriesCollection SC = new SeriesCollection();

            //Series sPriceRange = new Series();
 

            //sPriceRange.Name = "Price Range";
            Series sBidPrice;

            using (SqlConnection conn = new SqlConnection(Application["MonoDB"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "Ethane";

                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthane);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

 
                    //Bid Element
                    eBidPrice.ToolTip = dtr["BID"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["bid"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd\r\nyyy");
                    sBidPrice.AddElements(eBidPrice);
                }
            }

            SC.Add(sBidPrice);

            using (SqlConnection conn = new SqlConnection(Application["MonoDB"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "Ethylene";
                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthylene);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();


                    //Bid Element
                    eBidPrice.ToolTip = dtr["BID"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["bid"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd\r\nyyy");
                    sBidPrice.AddElements(eBidPrice);
                }
            }
            SC.Add(sBidPrice);


            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "LLDPE Film";
                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlLDPE);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Bid Element
                    eBidPrice.ToolTip = dtr["BID"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["bid"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd\r\nyyy");
                    sBidPrice.AddElements(eBidPrice);
                }
            }
            SC.Add(sBidPrice);


            //SC[0].Type = SeriesTypeFinancial.Bar;
            SC[0].Type = SeriesType.Line;
            SC[1].Type = SeriesType.Line;
            SC[2].Type = SeriesType.Line;

            return (SC);
        }





        private void CreatePropyleneChainChart()
        {
            ChartPropylene.ChartArea.ClearColors();
            ChartPropylene.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPropylene.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);

            ChartPropylene.OverlapFooter = true;
            ChartPropylene.Mentor = false;
            ChartPropylene.TempDirectory = "/research/charts/dp/mono/";
            ChartPropylene.FileName = " PropyleneChain";
            ChartPropylene.Width = 800;
            ChartPropylene.Height = 600;
            ChartPropylene.FileManager.ImageFormat = ImageFormat.Png;
            // Set the chart type                        
            ChartPropylene.Type = ChartType.Combo;


            ChartPropylene.XAxis.Label.Color = Color.White;

            // Debug mode. ( Will show generated errors if any )
            ChartPropylene.Debug = false;
            ChartPropylene.Title = "The Propylene Chain";

            ChartPropylene.TitleBox.Position = TitleBoxPosition.FullWithLegend;
            ChartPropylene.DefaultElement.Marker.Visible = false;

            Color[] MyColorObjectArray = new Color[3] { Color.Red, Color.FromArgb(255, 202, 0), Color.Yellow };
            ChartPropylene.Palette = MyColorObjectArray;

            ChartPropylene.ChartAreaSpacing = 3;
            ChartPropylene.LegendBox.Template = "%Icon%Name";
            ChartPropylene.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;

            //            // Modify the x axis labels.
            ChartPropylene.XAxis.TimeInterval = TimeInterval.Day;
            ChartPropylene.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartPropylene.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            ChartPropylene.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            //ChartObj.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.FromArgb(255, 202, 0);
            ChartPropylene.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartPropylene.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartPropylene.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartPropylene.ChartArea.XAxis.TickLabelAngle = 90;

            ChartPropylene.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartPropylene.ChartArea.XAxis.LabelRotate = true;
            ChartPropylene.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            //ChartObj.XAxis.TimeInterval = TimeInterval.Month;


            //XAxis
            ChartPropylene.XAxis.Label.Color = Color.White;
            ChartPropylene.ChartArea.XAxis.Label.Color = Color.White;
            ChartPropylene.XAxis.Line.Color = Color.Orange;
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;

            ChartPropylene.YAxis.Interval = 0.01;
            // Setup the axes.
            ChartPropylene.YAxis.Label.Text = "Price";
            ChartPropylene.YAxis.FormatString = "Currency";
            ChartPropylene.YAxis.Scale = Scale.Range;

            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartPropylene.ChartArea.YAxis.Label.Color = Color.White;
            ChartPropylene.YAxis.Line.Color = Color.Orange;
            ChartPropylene.YAxis.Label.Color = Color.White;



            //Chart
            ChartPropylene.TitleBox.Background.Color = Color.Orange;
            ChartPropylene.Background.Color = Color.Black;
            ChartPropylene.ChartArea.Background.Color = Color.Black;
            ChartPropylene.ChartArea.DefaultSeries.Line.Color = Color.Red;
            //CurrChart.ChartArea.DefaultSeries.Line.StartCap = System.Drawing.Drawing2D.LineCap.Triangle;
            //CurrChart.ChartArea.DefaultSeries.Line.EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
            ChartPropylene.ChartArea.DefaultSeries.Line.Width = 2;

            ChartPropylene.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
            ChartPropylene.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartPropylene.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);

            //CurrChart.ChartArea.Line.Color = Color.FromArgb(255, 255, 0, 0);//(254, 0, 0)
            ChartPropylene.ChartArea.Line.Color = Color.Orange;
            //CurrChart.ChartArea.Line.Width = 2;

            ChartPropylene.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            ChartPropylene.DefaultSeries.DefaultElement.Marker.Size = 6;
            ChartPropylene.DefaultSeries.DefaultElement.Marker.Color = Color.White;//Color.FromArgb( 255, 255, 153, 9);


            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();
            ChartPropylene.SeriesCollection.Add(getCreatePropyleneChainChartPriceData());


        }


        private SeriesCollection getCreatePropyleneChainChartPriceData()
        {
            string sSqlEthane = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=39 and export_price.date >'12/31/2006' and datepart(dw,export_price.date) = 6 ";
            string sSqlEthylene = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=34 and export_price.date >'12/31/2006'  and datepart(dw,export_price.date) = 6";
            string sSqlLDPE = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id=9 and export_price.date >'12/31/2006'  and datepart(dw,export_price.date) = 6";

            SeriesCollection SC = new SeriesCollection();

            //Series sPriceRange = new Series();


            //sPriceRange.Name = "Price Range";
            Series sBidPrice;

            using (SqlConnection conn = new SqlConnection(Application["MonoDB"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "Propane";

                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthane);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();


                    //Bid Element
                    eBidPrice.ToolTip = dtr["BID"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["bid"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd\r\nyyy");
                    sBidPrice.AddElements(eBidPrice);
                }
            }

            SC.Add(sBidPrice);

            using (SqlConnection conn = new SqlConnection(Application["MonoDB"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "RGP";
                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlEthylene);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();


                    //Bid Element
                    eBidPrice.ToolTip = dtr["BID"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["bid"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd\r\nyyy");
                    sBidPrice.AddElements(eBidPrice);
                }
            }
            SC.Add(sBidPrice);


            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = "HoPP Inj";
                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSqlLDPE);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Bid Element
                    eBidPrice.ToolTip = dtr["BID"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = true;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["bid"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd\r\nyyy");
                    sBidPrice.AddElements(eBidPrice);
                }
            }
            SC.Add(sBidPrice);


            //SC[0].Type = SeriesTypeFinancial.Bar;
            SC[0].Type = SeriesType.Line;
            SC[1].Type = SeriesType.Line;
            SC[2].Type = SeriesType.Line;

            return (SC);
        }





    }
}
