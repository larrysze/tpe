using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;
using System.IO;

namespace localhost.Research
{
    public partial class MarketResearchPublish : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C"))
            {
                Response.Redirect("../default.aspx");
            }

            if (!Page.IsPostBack)
            {
                initDDL();
                Bind();
                
            }
        }

        private void initDDL()
        {
            DateTime today = DateTime.Today.AddDays(1);
            today = today.AddHours(17);

            for (int i = -20; i < 0; i++)
            {
                DateTime timeLine = today.AddDays(i);
                ddlIssue.Items.Insert(0, timeLine.ToString());
            }
            
        }


        protected void Click_btnRefresh(object sender, System.EventArgs e)
        {
            Bind();
            
        }

        private void Bind()
        {
            DateTime date;
            date = Convert.ToDateTime(ddlIssue.SelectedItem.ToString());

            int fwdMonth = GetTPEForwardMonth(date.Month, date.Year.ToString());

            Hashtable htParam = new Hashtable();

            htParam.Add("@date", date.ToString() );

            htParam.Add("@fwdMonth", fwdMonth.ToString());
            TPE.Utility.DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(), dg, "spSnapshotWeeklySummary", htParam);
            txtSaveDate.Text = ddlIssue.SelectedItem.ToString();
        }

        private void DeleteSameDateReview()
        {
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                conn.Open();
                DateTime review_date;
                if (txtSaveDate.Text == "")
                    review_date = Convert.ToDateTime(ddlIssue.SelectedItem.ToString());
                else
                {
                    review_date = Convert.ToDateTime(txtSaveDate.Text);
                }

                string sqlString = "delete weekly_reviews where Year(review_date)=Year(@date) and Month(review_date)=Month(@date) and Day(review_date)=Day(@date);";

                Hashtable param = new Hashtable();

                param.Add("@date", review_date.ToString());

                DBLibrary.ExecuteSqlWithoutScrub(Application["DBConn"].ToString(), sqlString, param);
            }

            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                conn.Open();
                DateTime review_date;
                if (txtSaveDate.Text == "")
                    review_date = Convert.ToDateTime(ddlIssue.SelectedItem.ToString());
                else
                {
                    review_date = Convert.ToDateTime(txtSaveDate.Text);
                }

                string sqlString = "delete WeeklyReviewSpotOffersSummary where Year(date)=Year(@date) and Month(date)=Month(@date) and Day(date)=Day(@date);";

                Hashtable param = new Hashtable();

                param.Add("@date", review_date.ToString());

                DBLibrary.ExecuteSqlWithoutScrub(Application["DBConn"].ToString(), sqlString, param);
            }

        }

        protected void Submit1_ServerClick(object sender, System.EventArgs e)
        {
            this.DeleteSameDateReview();
            DateTime review_date;
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                conn.Open();                
                if(txtSaveDate.Text == "")
                    review_date = Convert.ToDateTime(ddlIssue.SelectedItem.ToString());
                else
                {
                    review_date = Convert.ToDateTime(txtSaveDate.Text);
                }

                string sqlString = "insert into weekly_reviews (published, review_date, comments, polyethylene, polypropylene) values('True',@date, @comments,                                         @polyethylene ,@polypropylene);";

                Hashtable param = new Hashtable();

                param.Add("@comments", this.Trimmer(txtSummary.Text));
                param.Add("@polypropylene", this.Trimmer(txtPp.Text));
                param.Add("@polyethylene", this.Trimmer(txtPe.Text));
                param.Add("@date", review_date.ToString());

                DBLibrary.ExecuteSqlWithoutScrub(Application["DBConn"].ToString(), sqlString, param);
            }

            this.SaveSpotSummary();
            this.saveSpotPictures();            
        }

        private string Trimmer(string text)
        {
            string[] records = text.Split('\n');

            string retText = null;
            for (int i = 0; i < records.Length; i++)
                if (records[i].Length > 3)
                    retText += records[i] + "<br><br>";
            return retText;
        }

        private void saveSpotPictures()
        {

            string[] charts = new string[6];
            charts[0] = Server.MapPath("/Research/Charts/DP/Chart_3_1M.png");
            charts[1] = Server.MapPath("/Research/Charts/DP/Chart_3_1Y.png");
            charts[2] = Server.MapPath("/Research/Charts/DP/Chart_3_5Y.png");
            charts[3] = Server.MapPath("/Research/Charts/DP/Chart_9_1M.png");
            charts[4] = Server.MapPath("/Research/Charts/DP/Chart_9_1Y.png");
            charts[5] = Server.MapPath("/Research/Charts/DP/Chart_9_5Y.png");

            DateTime currentDate = Convert.ToDateTime(txtSaveDate.Text);

            string path = "/Research/Weekly_Reviews/pics/" + currentDate.Month.ToString() + "_" + currentDate.Day + "_" + currentDate.Year.ToString() + "/";
            string file1 = "Chart_3_1M.png";
            string file2 = "Chart_3_1Y.png";
            string file3 = "Chart_3_5Y.png";
            string file4 = "Chart_9_1M.png";
            string file5 = "Chart_9_1Y.png";
            string file6 = "Chart_9_5Y.png";

            System.IO.Directory.CreateDirectory(Server.MapPath(path));

            System.IO.File.Copy(charts[0], Server.MapPath(path + file1), true);
            System.IO.File.Copy(charts[1], Server.MapPath(path + file2), true);
            System.IO.File.Copy(charts[2], Server.MapPath(path + file3), true);
            System.IO.File.Copy(charts[3], Server.MapPath(path + file4), true);
            System.IO.File.Copy(charts[4], Server.MapPath(path + file5), true);
            System.IO.File.Copy(charts[5], Server.MapPath(path + file6), true);
        }

        private void SaveSpotSummary()
        {

            int fwdMonth = GetTPEForwardMonth(DateTime.Today.Month, DateTime.Today.Year.ToString());
            
            DateTime review_date;
            if (txtSaveDate.Text == "")
                review_date = Convert.ToDateTime(ddlIssue.SelectedItem.ToString());
            else
            {
                review_date = Convert.ToDateTime(txtSaveDate.Text);
            }


            for (int i = 0; i < dg.Items.Count; i++)
            {

                Hashtable htParam = new Hashtable();
                htParam.Add("@date", review_date.ToString());
                htParam.Add("@gradeId", dg.Items[i].Cells[0].Text );
                htParam.Add("@totalLbs", ((TextBox)dg.Items[i].Cells[2].Controls[1]).Text);
                htParam.Add("@low", ((TextBox)dg.Items[i].Cells[3].Controls[1]).Text);
                htParam.Add("@high", ((TextBox)dg.Items[i].Cells[4].Controls[1]).Text);
                htParam.Add("@bid", ((TextBox)dg.Items[i].Cells[5].Controls[1]).Text);
                htParam.Add("@offr", ((TextBox)dg.Items[i].Cells[6].Controls[1]).Text);

                TPE.Utility.DBLibrary.ExecuteScalarStoredProcedure(Application["DBconn"].ToString(), "spSaveWeeklySummary", htParam);    
            }
            
        }
    
        private int GetTPEForwardMonth(int sMonth, string sYear)
        {
            Hashtable htParam = new Hashtable();
            htParam.Add("@month", GetMonth(sMonth));
            htParam.Add("@year", sYear);

            string sSQL = "SELECT FWD_ID FROM FWDMONTH WHERE FWD_MNTH=@MONTH AND FWD_YEAR=@year";

            int forwardID = int.Parse(DBLibrary.ExecuteScalarSQLStatement(HttpContext.Current.Application["DBconn"].ToString(), sSQL, htParam).ToString());

            return forwardID;
        }

        private string GetMonth(int month)
        {
            string sMonthName = "";

            switch (month)
            {
                case 1:
                    sMonthName = "January";
                    break;
                case 2:
                    sMonthName = "February";
                    break;
                case 3:
                    sMonthName = "March";
                    break;
                case 4:
                    sMonthName = "April";
                    break;
                case 5:
                    sMonthName = "May";
                    break;
                case 6:
                    sMonthName = "June";
                    break;
                case 7:
                    sMonthName = "July";
                    break;
                case 8:
                    sMonthName = "August";
                    break;
                case 9:
                    sMonthName = "September";
                    break;
                case 10:
                    sMonthName = "October";
                    break;
                case 11:
                    sMonthName = "November";
                    break;
                case 12:
                    sMonthName = "December";
                    break;
            }
            return sMonthName;

        }

        protected void ddlIssue_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtSaveDate.Text = ddlIssue.SelectedItem.ToString();
        }

        int lbsTotal = 0;
        protected void OnItemDataBinding(object sender, DataGridItemEventArgs e)
		{
            if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                lbsTotal += HelperFunction.ConvertToInt32(DataBinder.Eval(e.Item.DataItem, "VARSIZE"));
            }

            else if (e.Item.ItemType == ListItemType.Footer)
            {
                lblTotalLbs.Text = "Total LBS: " + lbsTotal.ToString("0,0"); 
            }
		}
    }
}