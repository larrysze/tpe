<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="chainchart.aspx.cs" Inherits="localhost.Research.chainchart" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
           <table width="100%" cellpadding="5" cellspacing="0">
        <tr>
            <td>
           
            <asp:HyperLink runat="server" ID="hlEthylene" Width="500" Height="350" Visible="true" >
                <asp:Image runat="server" ID="imgEthylene" Width="500" Height="350" Visible="false" />                            
            </asp:HyperLink>
            <br />
            <asp:HyperLink runat="server" ID="hlPropylene" Width="500" Height="350" Visible="true" >
                <asp:Image runat="server" ID="imgPropylene" Width="500" Height="350" Visible="false" />                            
            </asp:HyperLink>            
            
            <dotnet:Chart id="ChartEthylene" runat="server" Visible="false">
                <TitleBox Position="Left" />                
                <DefaultLegendBox Padding="4" Visible="True" CornerBottomRight="Cut">
                    <HeaderEntry SortOrder="-1" Visible="False" Value="Value" Name="Name">
                        <DividerLine Color="Gray" />                   
                        <LabelStyle Font="Arial, 8pt, style=Bold" />
                    </HeaderEntry>
                </DefaultLegendBox>
                <DefaultTitleBox Visible="True" />
                <DefaultElement>
                    <DefaultSubValue>
                        <Line Color="93, 28, 59" Length="4" />
                    </DefaultSubValue>
                </DefaultElement>
            </dotnet:Chart>


<dotnet:Chart id="ChartPropylene" runat="server" Visible="false" >
<TitleBox Position="Left">
</TitleBox>

<DefaultLegendBox Padding="4" Visible="True" CornerBottomRight="Cut">

<HeaderEntry SortOrder="-1" Visible="False" Value="Value" Name="Name">

<DividerLine Color="Gray">
</DividerLine>

<LabelStyle Font="Arial, 8pt, style=Bold">
</LabelStyle>

</HeaderEntry>

</DefaultLegendBox>

<DefaultTitleBox Visible="True">

</DefaultTitleBox>

<DefaultElement>

<DefaultSubValue>

<Line Color="93, 28, 59" Length="4">
</Line>

</DefaultSubValue>

</DefaultElement>
</dotnet:Chart>
            
            </td>                
        </tr>        
    </table>

    
    
    
    
    </div>
    
    
    <a href="chartlist.html">Go to Charts!>>></a>
    
    </form>
</body>
</html>
