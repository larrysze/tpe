<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page language="c#" Codebehind="SpotFloorChart.aspx.cs" AutoEventWireup="True" Inherits="localhost.SpotFloorChart" MasterPageFile="~/MasterPages/Menu.Master" Title="SpotSummaryChart" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">

			<div style="WIDTH: 780px; BACKGROUND-COLOR: black">
				<div class="DivTitleBarMenu">
				<table width="100%" id="Table4" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td valign="middle" style="width: 92px"><span class="Header Bold Color1">Resin Charts</span></td>
						<td align="right" style="width: 577px"><img alt="" src="/Pics/select_resin.jpg" /></td><td align="right">
							<asp:DropDownList CssClass="InputForm" id="ddlResinType" runat="server" AutoPostBack="True" onselectedindexchanged="ddlResinType_SelectedIndexChanged">
							</asp:DropDownList>
							</td>
					</tr>
				</table>
				</div>
				<br />
				<div style="TEXT-ALIGN: center">
					<dotnet:Chart id="Chart" runat="server">
<TitleBox Position="Left">
</TitleBox>

<DefaultLegendBox Padding="4" Visible="True" CornerBottomRight="Cut">

<HeaderEntry SortOrder="-1" Visible="False" Value="Value" Name="Name">

<DividerLine Color="Gray">
</DividerLine>

<LabelStyle Font="Arial, 8pt, style=Bold">
</LabelStyle>

</HeaderEntry>

</DefaultLegendBox>

<DefaultTitleBox Visible="True">
</DefaultTitleBox>

<DefaultElement>

<DefaultSubValue>

<Line Color="93, 28, 59" Length="4">
</Line>

</DefaultSubValue>

</DefaultElement>
					</dotnet:Chart></div>
					<br /><br />
			</div>

</asp:Content>