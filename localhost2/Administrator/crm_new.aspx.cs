using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;
using System.Text;
using System.Text.RegularExpressions;


namespace localhost.Administrator
{
    public partial class crm_new : System.Web.UI.Page
    {       

        protected void Page_Load(object sender, EventArgs e)
        {            
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "T") && ((string)Session["Typ"] != "L"))
            {
                Response.Redirect("/default.aspx");
            }
            if (!Page.IsPostBack)
            {
                ViewState["ID"] = Session["ID"].ToString();
                ViewState["Sort"] = "PERS_ID DESC";
                this.ViewState["Broker"]=ViewState["ID"].ToString();				
				this.ViewState["State"]="Broker";
                string id = ViewState["ID"].ToString();
                 
                if (((Session["ID"].ToString() == "2") || (Session["ID"].ToString() == "13564") || (Session["ID"].ToString() == "13642") ))
                {
                    btnAllocate.Visible = true;                    
                }


                using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
                {
                    ddlFolder.Items.Add(new ListItem("ALL Leads", "All Leads"));
                    ddlFolder.Items.Add(new ListItem("Your Leads", "1"));
                    conn.Open();
                    
                    string strSQL = "SELECT (SELECT COUNT(*) FROM PERSON WHERE PERS_ENBL='1' AND PERS_TYPE ='D' AND PERS_ACES='" + id + "' and pers_id not in (select group_member_contact_id from group_member)) AS ShowAll,(SELECT COUNT(*) FROM PERSON WHERE PERS_ENBL='0' AND PERS_TYPE='D' AND PERS_ACES=" + id + ") AS Recycled FROM [PERSON]";
                    using (SqlDataReader dtrGroup = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
                    {
                        if (dtrGroup.Read())
                        {
                            ddlFolder.Items.Add(new ListItem("Recycle Bin (" + dtrGroup["Recycled"].ToString() + ")", "0"));
                        }
                    }

                    string strSQL2 = "SELECT GROUP_NAME,(SELECT COUNT(*) FROM GROUP_MEMBER WHERE GROUP_MEMBER_GROUP_ID = GROUP_ID AND GROUP_MEMBER_BROKER_ID=" + id + ") AS COUNT,GROUP_ID FROM [GROUP] WHERE GROUP_BROKER=" + id + " ORDER BY GROUP_NAME";
                    using (SqlDataReader dtrGroup = DBLibrary.GetDataReaderFromSelect(conn, strSQL2))
                    {
                        while (dtrGroup.Read())
                        {
                            ddlFolder.Items.Add(new ListItem(dtrGroup["GROUP_NAME"].ToString() + " (" + dtrGroup["COUNT"].ToString() + ")", dtrGroup["GROUP_ID"].ToString()));
                        }
                    }

                    ddlFolder.Items[1].Selected = true;
                }
                BindDG();
            }

            
        }

        protected void BindDG()
        {
            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();
                System.Text.StringBuilder sbSQL = new System.Text.StringBuilder();
                DataSet dstContent;

                Hashtable htParams = new Hashtable();
                if (this.ViewState["Broker"] != null)
                {
                    htParams.Add("@ID", ViewState["Broker"].ToString());
                }                

                if (this.ViewState["Sort"] != null)
                {
                    htParams.Add("@Sort", ViewState["Sort"].ToString());
                }

                if (!ddlColorType.SelectedValue.ToString().Equals("*"))
                {
                    htParams.Add("@Color", ddlColorType.SelectedValue.ToString());
                }

                if (ddlFolder.SelectedValue.ToString().Equals("All Leads"))
                {
                    htParams.Remove("@ID");
                    htParams.Add("@Folder", "1");
                }
                else
                {
                    htParams.Add("@Folder", ddlFolder.SelectedValue.ToString());                    
                }

                if (txtSearch.Text != "")
                {
                    htParams.Add("@SearchText", txtSearch.Text);
                }

                dstContent = DBLibrary.GetDataSetFromStoredProcedure(conn, "spCRM_new", htParams);

                dg.DataSource = dstContent;

                try
                {
                    dg.DataBind();
                }
                catch
                {
                    //Response.Write(ex.Message + " " + ex.InnerException);
                }

            }

            Session["Folder"] = ddlFolder.SelectedValue;
        }

        protected void dg_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dg.PageIndex = e.NewPageIndex;
            BindDG();
        }
        
        protected void dg_Sorting(object sender, GridViewSortEventArgs e)
        {
            string[] SortExprs;
            string[] ViewStateExprs;
            string NewSearchMode = "";
            string NewSortExpr;

            Regex r = new Regex(" ");
            SortExprs = r.Split(e.SortExpression);

            Regex v = new Regex(" ");
            ViewStateExprs = v.Split(ViewState["Sort"].ToString());

            if (ViewStateExprs[0] == SortExprs[0])
            {
                if (ViewStateExprs[1].ToString() == "ASC") NewSearchMode = "DESC";
                else NewSearchMode = "ASC";
            }
            else NewSearchMode = "ASC";

            NewSortExpr = SortExprs[0] + " " + NewSearchMode;

            ViewState["Sort"] = NewSortExpr;

            BindDG();

        }

        protected void ddlColor_SelectdIndexChanged(object sender, EventArgs e)
        {
            BindDG();
        }
        
        protected void ddlFolder_SelectdIndexChanged(object sender, EventArgs e)
        {           
            BindDG();
        }

        protected void ddlPageSize_SelectdIndexChanged(object sender, EventArgs e)
        {
            dg.PageSize = System.Convert.ToInt32(ddlPageSize.SelectedValue.ToString());            
            BindDG();
        }

        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i <= dg.Rows.Count - 1; i++)
            {
                (((CheckBox)dg.Rows[i].FindControl("CheckBox1"))).Checked = true;                
            }
            //BindDG();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {                          
                BindDG();           
        }

        protected void btnEmailOffer_Click(object sender, EventArgs e)
        {
            if (getCheckPerson("Email"))
            {                
                Response.Redirect("/Administrator/SendEmail.aspx");
            }            
        }

        protected void btnEmailLetter_Click(object sender, EventArgs e)
        {
            if (getCheckPerson("Email"))
            {
                Response.Redirect("/Administrator/SendEmail.aspx?Letter=1");
            }  
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            StringBuilder sbPerson = new StringBuilder();
            StringBuilder sbSQL = new StringBuilder();
            bool bFound = false;
            for (int i = 0; i <= dg.Rows.Count - 1; i++)
            {
                if ((((CheckBox)dg.Rows[i].FindControl("CheckBox1")).Checked) == true)
                {
                    sbPerson.Append(Convert.ToString(dg.DataKeys[i][0]) + ',');
                    bFound = true;
                }
            }
            sbPerson = sbPerson.Remove(sbPerson.Length - 1, 1);
            sbSQL.Append("UPDATE PERSON SET PERS_ENBL='0'  WHERE PERS_ID IN ("+sbPerson.ToString()+");");

            if (ddlFolder.SelectedValue.ToString() != "1" && ddlFolder.SelectedValue.ToString() != "0" && ddlFolder.SelectedValue.ToString() != "All Leads")
            {
                sbSQL.Append("DELETE FROM GROUP_MEMBER WHERE GROUP_MEMBER_BROKER_ID='" + DBLibrary.ScrubSQLStringInput(this.ViewState["Broker"].ToString()) + "' AND GROUP_MEMBER_CONTACT_ID IN (" + sbPerson.ToString() + ");");
            }

            if (bFound) 
            {
                DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sbSQL.ToString());
                BindDG();
            }            
        }

        protected void btnAllocate_Click(object sender, EventArgs e)
        {
            if (getCheckPerson("Allocate"))
            {
                Response.Redirect("/administrator/Allocate_Lead.aspx?User=" + Session["strAllocateLead"].ToString());
            }
        }

        protected void btnManageLead_Click(object sender, EventArgs e)
        {
            if (getCheckPerson("ManageLead"))
            {
                Response.Redirect("/administrator/Manage_Lead.aspx?User=" + Session["strManageLead"].ToString());
            }
        }

        protected bool getCheckPerson(String type)
        {
            StringBuilder sbPerson = new StringBuilder();
            bool bFound = false;
            for (int i = 0; i <= dg.Rows.Count - 1; i++)
            {
                if ((((CheckBox)dg.Rows[i].FindControl("CheckBox1")).Checked) == true)
                {
                    sbPerson.Append(Convert.ToString(dg.DataKeys[i][0]) + ',');
                    bFound = true;
                }
            }
            if (bFound)
            {
                if (type == "Email") Session["strEmailOffer_Address"] = sbPerson.ToString();
                if (type == "Allocate") Session["strAllocateLead"] = sbPerson.ToString();
                if (type == "ManageLead") Session["strManageLead"] = sbPerson.ToString();
                return true;
            }
            else
            {
                return false;
            }
            
        }
    } 
}
