using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Text.RegularExpressions;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Users_Management.
	/// </summary>
	public class Users_Management : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid dg;
	
		/************************************************************************
	 *   1. File Name       :Administrator\Users_Management.aspx             *
	 *   2. Description     :Exchange user management                        *
	 *   3. Modification Log:                                                *
	 *     Ver No.       Date          Author             Modification       *
	 *   -----------------------------------------------------------------   *
	 *      1.00      2-25-2004      Zach                Comment             *
	 *                                                                       *
	 ************************************************************************/
		public void Page_Load(object sender, EventArgs e)
		{
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "S") && ((string)Session["Typ"] != "P")&& ((string)Session["Typ"] != "OP")&& ((string)Session["Typ"] != "S") && ((string)Session["Typ"] != "OS"))
			{
				Response.Redirect("../default.aspx");
			}
			if (!IsPostBack)
			{
				// sets the default sort
				ViewState["Sort"] = "COMP_NAME ASC";
				Data_Bind();
			}
			SqlCommand cmdContent;
			if (Request.QueryString["Switch"] != null)
			{
				// enables or disables the company
				SqlConnection conn;
				conn = new SqlConnection(Application["DBconn"].ToString());
				conn.Open();
				// COMP_ENBL=1-COMP_ENBL is the key.  makes sure that it works for both directions of the switch
				cmdContent= new SqlCommand("UPDATE PERSON SET PERS_ENBL=1-PERS_ENBL WHERE PERS_ID="+Request.QueryString["Switch"], conn);
				cmdContent.ExecuteNonQuery();
				conn.Close();
				Response.Redirect("Users_Management.aspx");
			}
		}
		private void Data_Bind()
		{
			string strSQL;
			SqlDataAdapter dadContent;
			DataSet dstContent;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();

			strSQL =" SELECT * FROM ";
			strSQL+=" (SELECT ";
			strSQL+="	PERS_ID, ";
			strSQL+="	PERS_PSWD, ";
			strSQL+="	COMP_NAME=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=PERS_COMP), ";
			strSQL+="	PERS_LOGN, ";
			strSQL+="	PERS_TYPE=(CASE PERS_TYPE WHEN 'P' THEN 'Purchaser' WHEN 'S' THEN 'Supplier' ELSE 'Officer' END), ";
			strSQL+="	PERS_ENBL ";
			/*
			strSQL+="	PERS_WO=ISNULL(( ";
			strSQL+="				CASE PERS_TYPE WHEN 'P' THEN ";
			strSQL+="					 (SELECT SUM((CASE BID_SIZE WHEN 0 THEN 42000 ELSE 190000 END)*BID_QTY*BID_PRCE) FROM BID WHERE BID_BUYR=PERS_ID) ";
			strSQL+="				WHEN 'S' THEN ";
			strSQL+="					 (SELECT SUM((CASE OFFR_SIZE WHEN 0 THEN 42000 ELSE 190000 END)*OFFR_QTY*OFFR_PRCE) FROM OFFER WHERE OFFR_SELR=PERS_ID) END) ";
			strSQL+="				,0), ";
			strSQL+="	PERS_WO_NB=ISNULL(( ";
			strSQL+="				CASE PERS_TYPE ";
			strSQL+="					 WHEN 'P' THEN (SELECT COUNT(*) FROM BID WHERE BID_BUYR=PERS_ID) ";
			strSQL+="				 	 WHEN 'S' THEN (SELECT COUNT(*) FROM OFFER WHERE OFFR_SELR=PERS_ID) END),0), ";
			strSQL+="	PERS_FO=ISNULL(( ";
			strSQL+="				CASE PERS_TYPE WHEN 'P' THEN ";
			strSQL+="					(SELECT SUM((ORDR_PRCE+ORDR_SHIP_PRCE+ORDR_MARK)*(CASE WHEN ORDR_WGHT IS NOT NULL THEN ORDR_WGHT ELSE ORDR_SIZE*ORDR_QTY*(CASE WHEN ORDR_BUYR_RATE IS NOT NULL THEN ORDR_BUYR_RATE ELSE 1 END) END)) FROM ORDERS WHERE ORDR_BUYR=PERS_ID) ";
			strSQL+="				WHEN 'S' THEN ";
			strSQL+="					 (SELECT SUM(ORDR_PRCE*(CASE WHEN ORDR_WGHT IS NOT NULL THEN ORDR_WGHT ELSE ORDR_SIZE*ORDR_QTY*(CASE WHEN ORDR_SELR_RATE IS NOT NULL THEN ORDR_SELR_RATE ELSE 1 END) END)) FROM ORDERS WHERE ORDR_SELR=PERS_ID) END) ";
			strSQL+="				,0), ";
			strSQL+="	PERS_FO_NB=ISNULL(( ";
			strSQL+="				CASE PERS_TYPE WHEN 'P' ";
			strSQL+="					THEN (SELECT COUNT(*) FROM ORDERS WHERE ORDR_BUYR=PERS_ID) ";
			strSQL+="				WHEN 'S' THEN ";
			strSQL+="					(SELECT COUNT(*) FROM ORDERS WHERE ORDR_SELR=PERS_ID) END),0) ";
			*/
			strSQL+=" FROM PERSON ";
			strSQL+=" WHERE PERS_REG=1  ";
			strSQL+=" AND (SELECT COMP_ENBL FROM COMPANY WHERE COMP_ID=PERS_COMP) =1  ";
			strSQL+=" AND (SELECT COMP_REG FROM COMPANY WHERE COMP_ID=PERS_COMP)=1 ";
			strSQL+=" AND PERS_TYPE IN ('P','S','O'))Q0 ORDER BY " + ViewState["Sort"].ToString();
          
			dadContent = new SqlDataAdapter(strSQL,conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dg.DataSource = dstContent;
			dg.DataBind();
			conn.Close();

		}

		// <summary>
		// Sorting function
		// </summary>
		public void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{

			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;

			//  Parse the sort expression - delimiter space
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
			ColumnToSort = SortExprs[0];
			// If a sort order is specified get it, else default is descending
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DeSC";
			}

			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;

			// Figure out the column index
			int iIndex;
			iIndex = 1;
			switch(ColumnToSort.ToUpper())
			{
				case "COMP_NAME":
					iIndex = 0;
					break;
				case "PERS_LOGN":
					iIndex = 1;
					break;
				case "PERS_PSWD":
					iIndex = 2;
					break;
				case "PERS_TYPE":
					iIndex = 3;
					break;
				//case "PERS_WO":
				//	iIndex = 4;
				//	break;
				//case "PERS_FO":
				//	iIndex = 5;
				//	break;
			}
			// alter the column's sort expression
			dg.Columns[iIndex].SortExpression = NewSortExpr;
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
			// store the new SortExpr in a labl for use by page function
			ViewState["Sort"] =NewSortExpr;
			// Sort the data in new order
			Data_Bind();
		}
		// <summary>
		//  Determines if the row is a 'thumbs up' or a 'thumbs down'
		protected void UporDown(object sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				if  ((DataBinder.Eval(e.Item.DataItem, "PERS_ENBL")).ToString() != null)
				{
					if  ((DataBinder.Eval(e.Item.DataItem, "PERS_ENBL")).ToString() == "True")
					{  
						e.Item.Cells[4].Text = "<a href=\"Users_Management.aspx?switch="+ (DataBinder.Eval(e.Item.DataItem, "PERS_ID")).ToString() +"\"><img src=/Images/Icons/Enabled.gif border=0></a>";
					}
					else
					{
						e.Item.Cells[4].Text = "<a href=\"Users_Management.aspx?switch="+ (DataBinder.Eval(e.Item.DataItem, "PERS_ID")).ToString() +"\"><img src=/Images/Icons/Disabled.gif border=0></a>";
					}
				}

			}

		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
