using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NineRays.Web.UI.WebControls.FlyTreeView;
using System.Data.SqlClient;
using TPE.Utility;
namespace localhost.Administrator
{
	/// <summary>
	/// subnote 的摘要说明。
	/// </summary>
	/// 

	public partial class subnote : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// 在此处放置用户代码以初始化页面
			//if(!Page.IsPostBack)
			//{
				if(Request["persId"]!=null)
				{
					this.ViewState["ID"]=Session["ID"].ToString();
					string persId=Request["persId"].ToString();
					CreatSubNotes(persId);
				}
			//}

		}	
		private void CreatSubNotes(string PersonId)
		{
		/*	SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			NineRays.Web.UI.WebControls.FlyTreeView.TreeNode tn;
          //  string inbox="";
			string becycle="";
			SqlCommand cmdGroup;
			SqlDataReader dtrGroup;
			cmdGroup= new SqlCommand("SELECT (SELECT COUNT(*) FROM PERSON WHERE PERS_ENBL='1' AND PERS_TYPE ='D' AND PERS_ACES='"+PersonId+"' and pers_id not in (select group_member_contact_id from group_member)) AS ShowAll,(SELECT COUNT(*) FROM PERSON WHERE PERS_ENBL='0' AND PERS_TYPE='D' AND PERS_ACES='"+PersonId+"') AS Recycled FROM [PERSON]",conn);
			dtrGroup = cmdGroup.ExecuteReader();
			if (dtrGroup.Read())
			{
				//inbox = "InBox("+dtrGroup["ShowAll"].ToString()+")";
				becycle= "Recycle Bin ("+dtrGroup["Recycled"].ToString()+")";				
			}
			dtrGroup.Close();
			*/
			NineRays.Web.UI.WebControls.FlyTreeView.TreeNode tn;
            string becycle="";
			using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				conn.Open();
				string strSQL = "SELECT (SELECT COUNT(*) FROM PERSON WHERE PERS_ENBL='1' AND PERS_TYPE ='D' AND PERS_ACES='"+PersonId+"' and pers_id not in (select group_member_contact_id from group_member)) AS ShowAll,(SELECT COUNT(*) FROM PERSON WHERE PERS_ENBL='0' AND PERS_TYPE='D' AND PERS_ACES=@pers_aces) AS Recycled FROM [PERSON]";
				Hashtable htParams = new Hashtable();
				htParams.Add("@pers_aces",PersonId);
				using (SqlDataReader dtrGroup = DBLibrary.GetDataReaderFromSelect(conn,strSQL,htParams))
				{
					if (dtrGroup.Read())
					{
						becycle= "Recycle Bin ("+dtrGroup["Recycled"].ToString()+")";				
					}
				}

			
				//NineRays.Web.UI.WebControls.FlyTreeView.TreeNode tn = new NineRays.Web.UI.WebControls.FlyTreeView.TreeNode();			
				//tn.Key="Group_user_"+PersonId;
				//tn.Text=inbox;
				//tn.ImageUrl="office2003_search.gif";
				//tn.DragDropName="d";
				//tn.DragDropJavascript="return true;";
				//tn.DragDropAcceptNames="c";
				//mn.Nodes.Add(tn);


				tn = new NineRays.Web.UI.WebControls.FlyTreeView.TreeNode();
				tn.Key="Group_recycle_"+PersonId;
				tn.Text="<a href='#' id=recycle ondragenter='handleDragEnter()' ondrop='drop()' ondragover='cancelEvent()' ondragleave='handleDragLeave()'>"+becycle+"</a>";//becycle;
                tn.ContextMenuID = "";
                tn.IsCheckBox = NBool.False;
				tn.ImageUrl="office2003_deleted.gif";
				tn.DragDropName="b";
				tn.DragDropJavascript="return true;";
				tn.DragDropAcceptNames="a";
				//tn.NodeSrc="leafnote.aspx?groupId=null&exchangeUser=null&recycle=someone&userId="+PersonId;
				mn.Nodes.Add(tn);	
				//cmdGroup= new SqlCommand("SELECT GROUP_NAME,(SELECT COUNT(*) FROM GROUP_MEMBER WHERE GROUP_MEMBER_GROUP_ID = GROUP_ID AND GROUP_MEMBER_BROKER_ID='"+PersonId+"') AS COUNT,GROUP_ID FROM [GROUP] WHERE GROUP_BROKER='"+PersonId+"' ORDER BY GROUP_NAME",conn);
				//dtrGroup = cmdGroup.ExecuteReader();			
				string strSQL2="SELECT GROUP_NAME,(SELECT COUNT(*) FROM GROUP_MEMBER WHERE GROUP_MEMBER_GROUP_ID = GROUP_ID AND GROUP_MEMBER_BROKER_ID=@group_member_broker) AS COUNT,GROUP_ID FROM [GROUP] WHERE GROUP_BROKER=@group_broker ORDER BY GROUP_NAME";
				Hashtable htParams2 = new Hashtable();
				htParams2.Add("@group_member_broker",PersonId);
				htParams2.Add("@group_broker",PersonId);
				using (SqlDataReader dtrGroup = DBLibrary.GetDataReaderFromSelect(conn,strSQL2,htParams2))
				{
						while (dtrGroup.Read())
						{
							tn = new NineRays.Web.UI.WebControls.FlyTreeView.TreeNode();
							tn.Key="Group_"+dtrGroup["group_id"].ToString().Trim()+"_"+PersonId;
							tn.Text="<a href='#' id="+dtrGroup["group_id"].ToString()+" ondragenter='handleDragEnter()' ondrop='drop()' ondragover='cancelEvent()' ondragleave='handleDragLeave()'>"+dtrGroup["group_name"].ToString()+"("+dtrGroup["COUNT"].ToString()+")</a>";
							tn.DragDropJavascript="return true;";
							tn.ImageUrl="office2003_folder.gif";
							tn.DragDropName="d";
							tn.DragDropJavascript="return true;";
							tn.DragDropAcceptNames="c";	
							tn.ContextMenuID="FlyContextMenu1";
							mn.Nodes.Add(tn);
						}
					//	dtrGroup.Close();
					//	conn.Close();
				}
			}
		}
		
		#region Web 窗体设计器生成的代码
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: 该调用是 ASP.NET Web 窗体设计器所必需的。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// 设计器支持所需的方法 - 不要使用代码编辑器修改
		/// 此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
