using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;

namespace localhost.Administrator
{
    public partial class tarnellreport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.Width = "1200px";

            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C"))
            {
                Response.Redirect("/default.aspx");
            }

            BindDataGrid();
        }

        protected void BtnExport_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=tarnellreport.xls");
            Response.Charset = "";

            // If you want the option to open the Excel file without saving then
            // comment out the line below
            // Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            gvReport.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }


        protected void BindDataGrid()
        {
            Hashtable htParams = new Hashtable();
            DataSet dstContent;

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {

                DateTime dt = DateTime.Now;
                DateTime firstDayOfThisMonth = dt.Subtract(TimeSpan.FromDays(dt.Day - 1));
                DateTime lastDayOfThisMonth = firstDayOfThisMonth.Subtract(TimeSpan.FromDays(1)) ;

                lblDate.Text = "Report for Date: " + lastDayOfThisMonth.ToShortDateString();

                htParams.Add("@MonthDate", lastDayOfThisMonth.ToShortDateString());
                dstContent = DBLibrary.GetDataSetFromStoredProcedure(conn, "spTarnellReportWWW",htParams);

                gvReport.DataSource = dstContent;
                gvReport.DataBind();     
                }
            }     
        

        

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */

        }
    }
}
