<%@ Page Language="c#" CodeBehind="Person_Details.aspx.cs" AutoEventWireup="false" Inherits="localhost.Administrator.Person_Details" %>
<HTML>
	<HEAD>
		
		<link href="../include/style.css" type="text/css" rel="STYLESHEET">
			<asp:PlaceHolder id="phBody" runat="server"></asp:PlaceHolder></FORM>
	</HEAD>
	<body>
		<p align="center"><br>
			Please refresh the page after save to see the changes.</p>
		<P></P>
		<form runat="server">
			<fieldset width="200">
				<legend>
					<strong>
						<asp:Label id="lblType" runat="server"></asp:Label></strong>
				</legend>
				<asp:ValidationSummary runat="server" HeaderText="There are problems with the following fields. Please correct the errors below." id="ValidationSummary1" />

				<table width="200">
					<tbody>
						<tr>
							<td colspan="2">
							</td>
						</tr>
						<tr>
							<td>
								First Name:
							</td>
							<td>
								<asp:TextBox id="txtFname" runat="server"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td>
								Last Name:
							</td>
							<td>
								<asp:TextBox id="txtLname" runat="server"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td>
								Title:
							</td>
							<td>
								<asp:TextBox id="txtTitle" runat="server"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td>
								Phone:
							</td>
							<td>
								<asp:TextBox id="txtPhone" runat="server"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td>
								Fax:
							</td>
							<td>
								<asp:TextBox id="txtFAX" runat="server"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td>
								Email:
							</td>
							<td>
								<asp:CustomValidator runat="server" controltovalidate="txtMAIL" display="none" ErrorMessage="Email already exists."
								OnServerValidate="IsDuplicate" id="CustomValidator1" />
								<asp:RequiredFieldValidator runat="server" ControlToValidate="txtMAIL" Display="none" ErrorMessage="Email can not be blank."
								id="Requiredfieldvalidator3" />
								<asp:TextBox id="txtMAIL" runat="server"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td>
								Login:
							</td>
							<td><asp:Label id="txtLOGN" runat="server"></asp:Label>
							</td>
						</tr>
						<!--<tr>
							<td>
								Password:
							</td>
							<td>
								<asp:TextBox TextMode="Password" id="txtPSWD" runat="server"></asp:TextBox>
							</td>
						</tr>-->
						<tr>
							<td align="middle" colspan="2">
								<asp:Button id="Button1" onclick="Click_Save" runat="server" Text="Save"></asp:Button>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input onclick="javascript:window.close();" type="button" value="Close">
							</td>
						</tr>
					</tbody>
				</table>
			</fieldset>
			<!-- Insert content here --></form>
	</body>
</HTML>
