<%@ Page Language="c#" CodeBehind="Select_Front_Month.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.Select_Front_Month" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
	<asp:Panel Width="780px" id="pnMain" runat="server">
		<TABLE border="0" cellpadding="0" cellspacing="0" align="center" width="400px" style="text-align:left" class="Content Color2">
			<TR>
				<TD><br />
					Note that by changing the front month all of the working<br />
					orders associated with this contract will be deleted.<br />
				</TD>
			</TR>
			<TR>
				<TD><br />
					The current front month is
					<asp:Label id="lblMonth" runat="server"></asp:Label>.<br />
					Click submit to disable this month and make the next month the front month.
				</TD>
			</TR>
			<TR>
				<TD align="center"><br />
					<asp:Button CssClass="Content Color2" id="btnSubmit" onclick="Click_Submit" runat="server" Text="Click Submit"></asp:Button><br /><br />
					</TD>
			</TR>
		</TABLE>
	</asp:Panel>
	<asp:Panel Width="780px" id="pnConfirm" Visible="False" runat="server">
		<TABLE border="0">
			<TR>
				<TD colSpan="2" class="Content Color2"><br />Are you sure you want to update the front month?<br />
				</TD>
			</TR>
			<TR>
				<TD align="center">
					<asp:Button CssClass="Content Color2" id="btnYes" onclick="btnYes_Click" runat="server" Text="Yes"></asp:Button></TD>
				<TD align="center">
					<asp:Button CssClass="Content Color2" id="btnNo" onclick="btnNo_Click" runat="server" Text="No"></asp:Button></TD>
			</TR>
		</TABLE><br />
	</asp:Panel>	
</asp:Content>