using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Data.SqlClient;
using System.Text;
using TPE.Utility;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Allocate_Lead.
	/// </summary>
	public partial class Allocate_Lead : System.Web.UI.Page
	{
		
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "L"))
			{
				Response.Redirect("/default.aspx");
			}
			
			if (!IsPostBack) 
			{
				using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
				{
					conn.Open();
				 
					// binding allocation
                    string strSql = "select NAME = CAST(PERS_FRST_NAME as varchar) +' ' +CAST(PERS_LAST_NAME as varchar) ,PERS_ID, PERS_LOGN, NUMLEAD = (SELECT COUNT(*) FROM GROUP_MEMBER WHERE GROUP_MEMBER_BROKER_ID = P.pers_id) + (SELECT COUNT(*) FROM PERSON WHERE PERS_ENBL='1' AND PERS_TYPE in ('S','O','P','D','B') AND PERS_ACES = P.pers_id AND pers_id not in (select group_member_contact_id from group_member))from person P where ( PERS_TYPE='B' or PERS_TYPE='T' or PERS_TYPE='A' or PERS_TYPE='L') AND PERS_ENBL='1' ORDER BY NAME";
                    //string strSql = "SELECT PERS_FRST_NAME,PERS_LAST_NAME, PERS_LOGN , PERS_ID FROM PERSON WHERE (PERS_TYPE='B' OR PERS_TYPE='A' OR PERS_TYPE='L' OR PERS_TYPE='T') AND PERS_ENBL='1' ORDER BY PERS_FRST_NAME ASC";
                    using (SqlDataReader dtrAllocate = DBLibrary.GetDataReaderFromSelect(conn, strSql))
                    {
                        //binding the allocate dd list
                        while (dtrAllocate.Read())
                        {
                            if (Convert.ToInt32(dtrAllocate["NUMLEAD"].ToString()) > 0 || dtrAllocate["PERS_ID"].ToString().Equals(System.Configuration.ConfigurationSettings.AppSettings["newBrokerId"].ToString()))                            
                            {
                                string name = Convert.ToString(dtrAllocate["NAME"]) + " (" + Convert.ToString(dtrAllocate["PERS_LOGN"]) + ")";
                                ddlAllocate.Items.Add(new ListItem(name, dtrAllocate["PERS_ID"].ToString()));
                            }
							
						}
					}
					string strSQL;
					string strOutput;
					
					strSQL= "Select PERS_ID,PERS_FRST_NAME,PERS_LAST_NAME, PERS_CATEGORY, ISNULL(PERS_PRMLY_INTRST, 1) AS MARKET FROM PERSON WHERE PERS_ID IN (";
					strSQL += Request.QueryString["User"].ToString().Substring(0,Request.QueryString["User"].ToString().Length-1);
					strSQL +=")";
			
					strOutput ="";

					string status = "";
					bool internationalMarket = false;
					using (SqlDataReader dtrAllocateList = DBLibrary.GetDataReaderFromSelect(conn,strSQL))
					{
						while (dtrAllocateList.Read())
						{
							string name = Convert.ToString(dtrAllocateList["PERS_FRST_NAME"]) + " " + Convert.ToString(dtrAllocateList["PERS_LAST_NAME"]);
							strOutput += "<tr><td class=\"Content\">"+name+"<td align=\"center\"><input type=\"hidden\" name=\"Id_"+dtrAllocateList["PERS_ID"].ToString()+"\"> <input name=\"To_"+dtrAllocateList["PERS_ID"].ToString()+"\" type=\"checkbox\" checked=\"yes\"></td><td align=\"center\"><input name=\"CC_"+dtrAllocateList["PERS_ID"].ToString()+"\" type=\"checkbox\" checked></td></td></tr>";
							string tmpStatus = dtrAllocateList["PERS_CATEGORY"].ToString();
							string tmpMarket = dtrAllocateList["MARKET"].ToString();
							if(tmpStatus == "Analyst/Market Observer")
							{
								status = "Analyst";
							}
							else if(tmpStatus == "Resin Producer")
							{
								status = "Producer";
							}

							if (tmpMarket == "2")
							{
								internationalMarket = true;
							}
						}
						phAllocate.Controls.Add (new LiteralControl(strOutput));
					}
			

					// Put user code to initialize the page here
					StringBuilder sbEmailText = new StringBuilder();
					// create text for mail to be sent to user
					sbEmailText.Append("Dear <PERSON FIRST NAME>, "+((char)(13)).ToString()+((char)(13)).ToString());
					sbEmailText.Append("Thank you for registering with <a href=\"http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "\">The Plastics Exchange</a>. We are all about resin and we know...price matters!  " +((char)(13)).ToString()+((char)(13)).ToString()); 
 
					if(status == "Producer")
					{
						sbEmailText.Append("We are interested in working with <COMPANY NAME> and hope you will find The Plastics Exchange to be an important source of market intelligence and the place you sell your surplus commodity grade resin. We have excellent markets for a full range of domestic prime and wide spec resin as well as strong export network to move material offshore. ");
						sbEmailText.Append("If you have any immediate resin offers or if you ever have any questions please call <BROKER NAME> directly at 312.202.0002 or reply to this email.  " + ((char)(13)).ToString() + ((char)(13)).ToString());                        
					}
					else if(status == "Analyst")
					{
						sbEmailText.Append("We are interested in working with <COMPANY NAME> and hope you will find The Plastics Exchange to be an important source of market intelligence. We work with many industry analysts, explaining the dynamics of this volatile resin market, so if you ever have any questions please call the Trading Desk (or me personally) at 312.202.0002 or reply to this email. " + ((char)(13)).ToString() + ((char)(13)).ToString());
						sbEmailText.Append("Should any resin needs arise, we also have excellent markets for a full range of domestic prime and wide spec as well as international prime resin for import/export.  Given your current system status as an Analyst / Market Observer, you will not receive emailed offers, please let us know if you wish this status changed. " + ((char)(13)).ToString() + ((char)(13)).ToString());
					}
					else if (internationalMarket)
					{
						sbEmailText.Append("We are interested in working with <COMPANY NAME> and hope you will find The Plastics Exchange to be an important source of market intelligence and the place you procure spot commodity grade resin. If you have any immediate resin needs or if you ever have any questions please call <BROKER NAME>, Director of International Trading at +5515.9772.2020 or reply to this email.  " + ((char)(13)).ToString() + ((char)(13)).ToString());
						sbEmailText.Append("We have excellent markets for a full range of domestic prime and wide spec resin as well as international prime resin for import/export.  Just let us know what you need and we will chase it down for you. " + ((char)(13)).ToString() + ((char)(13)).ToString());
					}
					else
					{
						sbEmailText.Append("We are interested in working with <COMPANY NAME> and hope you will find The Plastics Exchange to be an important source of market intelligence and the place you procure spot commodity grade resin. If you have any immediate resin needs or if you ever have any questions please call <BROKER NAME> at 312.202.0002 or reply to this email.  " + ((char)(13)).ToString() + ((char)(13)).ToString());
						sbEmailText.Append("We have excellent markets for a full range of domestic prime and wide spec resin as well as international prime resin for import/export.  Just let us know what you need and we will chase it down for you. " + ((char)(13)).ToString() + ((char)(13)).ToString());
					}
				
					//last section of welcome letter
					//sbEmailText.Append("Crazy resin market, huh?!?  Feel free to visit the website to view our historic price charts, daily industry news and login to see our prime and offgrade markets as they move around constantly. "+((char)(13)).ToString()+((char)(13)).ToString());

					sbEmailText.Append("Crazy resin market, huh?!?  Feel free to visit the website to view our historic price charts, daily industry news and login to see our prime and offgrade markets as they move around constantly." + ((char)(13)).ToString() + ((char)(13)).ToString());
                    sbEmailText.Append("You will receive our weekly Market Update by email. " + ((char)(13)).ToString() + ((char)(13)).ToString());

					//sbEmailText.Append("We also contribute to <a href='http://www.petrochemwire.com'>The PetroChem Wire</a>, a daily report providing exclusive market commentary and daily quotes for NGLs (Ethane, Propane, etc), Olefins (Ethylene and Propylene) and Polyolefins (Polyethylene and Polypropylene).  Please let me know if you would like a free 2-week trial." + ((char)(13)).ToString() + ((char)(13)).ToString());
					//sbEmailText.Append("You will receive our Market Update every week by email." + ((char)(13)).ToString() + ((char)(13)).ToString());

					//sbEmailText.Append("<B>Email: <PERSON EMAIL></B>"+((char)(13)).ToString());
					//sbEmailText.Append("<B>Password: <PERSON PASSWORD></B>"+((char)(13)).ToString()+((char)(13)).ToString());
					sbEmailText.Append("Sincerely,"+((char)(13)).ToString()+((char)(13)).ToString());
					sbEmailText.Append("Michael A. Greenberg, CEO"+((char)(13)).ToString());
					sbEmailText.Append("The Plastics Exchange"+((char)(13)).ToString());
					sbEmailText.Append("Tel: " + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + ((char)(13)).ToString());
					sbEmailText.Append("Fax: " + ConfigurationSettings.AppSettings["FaxNumber"].ToString() + ((char)(13)).ToString());
	 
					txtBody.Text=sbEmailText.ToString();
					txtSubject.Text = "Welcome to the Plastics Exchange";
				}
			}
		}
		public void Cancel_Allocate(object sender, EventArgs e)
		{
			Response.Redirect("/administrator/CRM.aspx");
		}
		// <summary>
		//  Updates then allocated broker in the DB.
		//  It then email the user and/or the broker with a welcome message
		// </summary>

		public void Submit_Allocate(object sender, EventArgs e)
		{
			
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
		
				using (SqlDataReader dtrBrokerEmail = DBLibrary.GetDataReaderFromSelect(conn,"Select PERS_FRST_NAME+ ' ' + PERS_LAST_NAME AS NAME, PERS_MAIL from PERSON WHERE PERS_ID="+ddlAllocate.SelectedItem.Value))
				{
					dtrBrokerEmail.Read();
					// flags that determine whether to email users
					bool bToUser;
					bool bCCBroker;
					//string MailStr; // text of email body
					// loop through all objects on the form looking for the hidden id
					StringBuilder sbHTML;
					foreach (string item in Request.Form)
					{
						if (item.Substring(0,3) == "Id_")
						{ 
							// then it found a user that needs to be updated
							// update the users allocation
							DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"UPDATE PERSON SET PERS_ACES="+ddlAllocate.SelectedItem.Value+" WHERE PERS_ID="+item.Substring(3,item.Length -3));
							// gets broker info needed for the emails
							using (SqlConnection conn2 = new SqlConnection(Application["DBconn"].ToString()))
							{
								using (SqlDataReader dtrEmail = DBLibrary.GetDataReaderFromSelect(conn2,"Select *,(SELECT TOP 1 CMNT_TEXT FROM COMMENT WHERE CMNT_PERS=PERS_ID) AS NOTE From PERSON WHERE PERS_ID="+item.Substring(3,item.Length -3)))
								{
									dtrEmail.Read();
									// defaults boolean flags to no
									bToUser = false;
									bCCBroker = false;
									string strMail ="";

									// determines if the user has checked the boxes to indicate that the user/brokers should recieve mail
									foreach (string Email in Request.Form)
									{
										if (Email.Substring(0,3) == "To_")
										{
											if (item.Substring(3,item.Length -3).Equals(Email.Substring(3,Email.Length -3)))
											{
												bToUser = true;
											}
										}
										if (Email.Substring(0,3) == "CC_")
										{
											if (item.Substring(3,item.Length -3).Equals(Email.Substring(3,Email.Length -3)))
											{
												bCCBroker = true;
											}
										}
									}
									// begin building mail body
									if (bToUser)
									{
										sbHTML = new StringBuilder();
																
										strMail = txtBody.Text;
										strMail = strMail.Replace(((char)(13)).ToString(), "<BR>");
										strMail = strMail.Replace("<PERSON FIRST NAME>",dtrEmail["PERS_FRST_NAME"].ToString());
										strMail = strMail.Replace("<COMPANY NAME>",dtrEmail["COMP_NAME"].ToString());
										strMail = strMail.Replace("<BROKER NAME>",dtrBrokerEmail["Name"].ToString());
										//strMail = strMail.Replace("<PERSON EMAIL>",dtrEmail["PERS_MAIL"].ToString());
										//strMail = strMail.Replace("<PERSON PASSWORD>",Crypto.Decrypt(dtrEmail["PERS_PSWD"].ToString()));
						
						
										sbHTML.Append("<table border=0 cellspacing=0 cellpadding=0>");
										sbHTML.Append("<tr><td><img src=http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "//images/email/tpelogo.gif></td>");
										sbHTML.Append("<td>");
										sbHTML.Append("<font face=arial black size=4 color=Black>The</font><font face=arial black size=4 color='red'>Plastics</font><font face=arial black size=4 color=Black>Exchange</font><font face=arial black size=4 color='#4A4AFE'>.</font><font face=arial black size=4 color=Black>com</font>");
										sbHTML.Append("</td>");
										sbHTML.Append("</tr></table><BR><BR>");
										// add body 
										sbHTML.Append(strMail);

										sbHTML.Append("<BR><BR></BODY></HTML>");

										// now to create mail message
										MailMessage mail = new MailMessage();
										mail.From = Application["strEmailOwner"].ToString();
										// if the flags have been set to true the users have elected to recieve the mails
										mail.To = dtrEmail["PERS_MAIL"].ToString();
										
										if (bCCBroker)
										{
											mail.Cc = dtrBrokerEmail["PERS_MAIL"].ToString();
										}
										mail.Subject = txtSubject.Text;
						
										mail.Body = sbHTML.ToString();
										mail.BodyFormat = MailFormat.Html;
										//SmtpMail.SmtpServer = "caesar";
										TPE.Utility.EmailLibrary.Send(Context,mail);

									}

									// now send email to mike and the broker

									StringBuilder sbBrokerMail = new StringBuilder();

									sbBrokerMail.Append("The following lead has been allocated to "+dtrBrokerEmail["Name"].ToString());
									sbBrokerMail.Append(HelperFunction.getUserInformationHTML(dtrEmail["PERS_MAIL"].ToString(), dtrEmail["PERS_PSWD"].ToString(),dtrEmail["PERS_MAIL"].ToString(),this.Context));
								
									MailMessage b_mail = new MailMessage();
									b_mail.From = Application["strEmailOwner"].ToString();
									// mike always gets the mail
					
									//					b_mail.To = "amre@theplasticexchange.com";
									b_mail.To=Application["strEmailOwner"].ToString();

									// broker only gets cced when they are selected
									if (bCCBroker)
									{
										b_mail.Cc = dtrBrokerEmail["PERS_MAIL"].ToString();

									}
									b_mail.Subject = "Lead Allocated";
									b_mail.Body = sbBrokerMail.ToString();
									b_mail.BodyFormat = MailFormat.Html;
									//SmtpMail.SmtpServer="caesar";
									TPE.Utility.EmailLibrary.Send(Context,b_mail);

									Hashtable htPar = new Hashtable();
									htPar.Add("@PERSON_ID", item.Substring(3,item.Length -3));
									htPar.Add("@ID_TYPE", "8");
									htPar.Add("@DATE_TIME", DateTime.Now.ToString());
									htPar.Add("ALLOCATED_TO", dtrBrokerEmail["Name"].ToString());

									string strSql2 = "INSERT DASHBOARD (ID_TYPE, DATE_TIME, PERSON_ID, BODY, ALLOCATED_TO) VALUES (@ID_TYPE, @DATE_TIME, @PERSON_ID, '" + sbBrokerMail.ToString() + "',@ALLOCATED_TO)";
									DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSql2, htPar);

								}
							}
						}

					}
				}			
				Response.Redirect("/administrator/CRM.aspx");
			}
		}
	

		// <summary>
		//  function returns a HTML list of the resin as specified by the passed PREF ID
		// </summary>
		private string CreateResinList(string iPref)
		{
			string strOutput;

			//Database connection
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();

				//string Pref; // The users preference
				//'get the value of preference from querystring passed by Demo_Lead.aspx
				//Pref = Request.QueryString("Pref")

				string strSQL;
				
				//Use "POWER" method to find preference
				strSQL="SELECT CONT_LABL FROM CONTRACT WHERE POWER(2,CONT_ID-1)&("+iPref+")<>0";
				using (SqlDataReader dtrResin = DBLibrary.GetDataReaderFromSelect(conn,strSQL))
				{
					strOutput ="<table>";
					while (dtrResin.Read())
					{
						strOutput +="<tr><td>"+dtrResin["CONT_LABL"].ToString()+"</td></tr>";
					}
					strOutput +="</table>";
				}
			}
			return strOutput;
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
