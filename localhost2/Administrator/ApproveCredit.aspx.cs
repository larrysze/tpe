using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

          /******************************************************************************
          '*   1. File Name       : ApproveCredit.aspx                                  * 
          '*   2. Description     : This page shows all the details that is filled by   * 
		  '*                        the user in credit application page.if user goes    * 
		  '*						through the online credit app option.if user chosse *
		  '*						the fax crdit app then this page is blank and admin *
		  '*						fills all the entries with reference the fax report *
		  '*	    				and in this page admin can approve or deny the      * 
		  '*					    credit.if admin deny the credit then that particular*
		  '*    					order is removed from the table and user get the    *
		  '*						information through mail.if approve the user also   *
		  '*					    get the approve credit information mail and data    *
		  '*						stores in the database and user type is also changed*                                 
          '*						                                                    *
          '*   3. Modification Log:                                                     *
          '*     Ver No.       Date          Author             Modification            *
          '*   -----------------------------------------------------------------        *
          '*      1.00                     Hnau Software         Comment                *
          '*                                                                            *
          '******************************************************************************/
namespace pes1.Administrator 
{
	/// <summary>
	/// Summary description for ApproveCredit.
	/// </summary>
	public class ApproveCredit : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox txtPers_Fname;
		protected System.Web.UI.WebControls.TextBox txtPers_Lname;
		protected System.Web.UI.WebControls.TextBox txtPers_Title;
		protected System.Web.UI.WebControls.TextBox txtPers_Phone;
		protected System.Web.UI.WebControls.TextBox txtPers_Fax;
		protected System.Web.UI.WebControls.TextBox txtPers_Email;
		protected System.Web.UI.WebControls.TextBox txtComp_Cname;
		protected System.Web.UI.WebControls.TextBox txtComp_Address;
		protected System.Web.UI.WebControls.TextBox txtComp_City;
		protected System.Web.UI.WebControls.TextBox txtComp_Zip;
		protected System.Web.UI.WebControls.DropDownList ddlComp_State;
		protected System.Web.UI.WebControls.DropDownList ddlComp_Country;
		protected System.Web.UI.WebControls.TextBox txtBank_Bname;
		protected System.Web.UI.WebControls.TextBox txtTrad1_Cname;
		protected System.Web.UI.WebControls.TextBox txtBank_Contact;
		protected System.Web.UI.WebControls.TextBox txtBank_Phone;
		protected System.Web.UI.WebControls.TextBox txtBank_Fax;
		protected System.Web.UI.WebControls.TextBox txtTrad1_Phone;
		protected System.Web.UI.WebControls.TextBox txtTrad1_Fax;
		protected System.Web.UI.WebControls.TextBox txtTrad2_Cname;
		protected System.Web.UI.WebControls.TextBox txtTrad3_Cname;
		protected System.Web.UI.WebControls.TextBox txtTrad2_Contact;
		protected System.Web.UI.WebControls.TextBox txtTrad3_Contact;
		protected System.Web.UI.WebControls.TextBox txtTrad2_Phone;
		protected System.Web.UI.WebControls.TextBox txtTrad2_Fax;
		protected System.Web.UI.WebControls.TextBox txtTrad3_Phone;
		protected System.Web.UI.WebControls.TextBox txtTrad3_Fax;
		protected System.Web.UI.WebControls.Button btnDeny;
		protected System.Web.UI.WebControls.TextBox txtTrad1_Contact;
		protected int CREDIT_ID;
		protected int ORD_SRNO;
		protected int USR_ID;
		protected System.Web.UI.WebControls.TextBox txtConfrm;
		protected System.Web.UI.HtmlControls.HtmlInputButton btnApprove1;
		protected int BIDOFFER_ID;

	
		private void Page_Load(object sender, System.EventArgs e)
		{
			
			if (Session["ID"] == null)
			{
				Response.Redirect("/default.aspx");
			}
			///**********Storing INFORMATION in corresponding variables ***********//
			
				CREDIT_ID =Convert.ToInt32(Request.QueryString["CREDIT_ID"].ToString());
				ViewState["CR_ID"]=Convert.ToInt32(Request.QueryString["CREDIT_ID"].ToString());
				ORD_SRNO =Convert.ToInt32(Request.QueryString["ORD_SRNO"].ToString());
				ViewState["ORD_SRNO"]=Convert.ToInt32(Request.QueryString["ORD_SRNO"].ToString());
				USR_ID =Convert.ToInt32(Request.QueryString["USER_ID"].ToString());
				ViewState["USR_ID"] =Convert.ToInt32(Request.QueryString["USER_ID"].ToString());
				BIDOFFER_ID =Convert.ToInt32(Request.QueryString["BIDOFFR_ID"].ToString());
				ViewState["BIDOFFER_ID"] =Convert.ToInt32(Request.QueryString["BIDOFFR_ID"].ToString());
	
			if (!IsPostBack )
			{
		
				
				//Adding States into dropdown combobox
				FillCombos(ddlComp_State,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE order by STAT_NAME ASC");
				//Adding countries to drop down list box
				FillCombos(ddlComp_Country,"CTRY_NAME","CTRY_CODE","SELECT CTRY_NAME,CTRY_CODE From COUNTRY ORDER BY CTRY_NAME ASC");
				//Showing Information filled up in form			
			
				
				ShowCredit(CREDIT_ID);
		
			}

		// Put user code to initialize the page here
	}


		private void ShowCredit(int ParamCredit)
		{
			string TmpStr=string.Empty;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			SqlCommand Cmd = new SqlCommand();
			Cmd.CommandType=CommandType.StoredProcedure;
			Cmd.CommandText="spShowSaveCredit";

			foreach (Control c in Page.Controls)
			{
				foreach (Control childc in c.Controls)
				{
					if (childc is TextBox)
					{   
						TmpStr = ((TextBox)childc).ID.ToString(); //((TextBox)childc).ID.ToString()
						if (((TextBox)childc).ID.ToString()!="txtConfrm")
						{
							Cmd.Parameters.Add("@"+ TmpStr.Remove(0,3) ,Convert.DBNull );
						}
					}
					if (childc is DropDownList)					
					{
						TmpStr = ((DropDownList)childc).ID.ToString();
						Cmd.Parameters.Add("@"+ TmpStr.Remove(0,3) ,Convert.DBNull);//((DropDownList)childc).SelectedValue.ToString());
					}
				}
			}
			Cmd.Parameters.Add("@ID",SqlDbType.Int);
			Cmd.Parameters["@ID"].Value =ParamCredit; 
			//Cmd.Parameters["@ID"].Value =3725;//ParamCredit
			
			Cmd.Parameters.Add("@Flag",SqlDbType.Char);
			Cmd.Parameters["@Flag"].Value="S"; 

			Cmd.Parameters.Add("@APPROVED",SqlDbType.Char);
			Cmd.Parameters["@APPROVED"].Value = Convert.DBNull;// "A";
			SqlDataReader DR;
			conn.Open();
			Cmd.Connection=conn;
			DR=Cmd.ExecuteReader();


			while (DR.Read())
			{
				txtPers_Fname.Text= DR.GetValue(1).ToString();
				txtPers_Lname.Text= DR.GetValue(2).ToString();
				txtPers_Title.Text= DR.GetValue(3).ToString();
				txtPers_Phone.Text= DR.GetValue(4).ToString();
				txtPers_Fax.Text= DR.GetValue(5).ToString();
				txtPers_Email.Text= DR.GetValue(6).ToString();
				txtComp_Cname.Text = DR.GetValue(7).ToString();
				txtComp_Address.Text= DR.GetValue(8).ToString();
				txtComp_City.Text= DR.GetValue(9).ToString();
				txtComp_Zip.Text= DR.GetValue(10).ToString();

				ddlComp_State.SelectedValue= DR.GetValue(11).ToString();
				ddlComp_Country.SelectedValue= DR.GetValue(12).ToString();

				txtBank_Bname.Text=DR.GetValue(12).ToString();
				txtBank_Contact.Text=DR.GetValue(13).ToString();
				txtBank_Phone.Text=DR.GetValue(14).ToString();
				txtBank_Fax.Text=DR.GetValue(15).ToString();
				txtTrad1_Cname.Text=DR.GetValue(16).ToString();
				txtTrad1_Contact.Text=DR.GetValue(17).ToString();
				txtTrad1_Phone.Text=DR.GetValue(18).ToString();
				txtTrad1_Fax.Text=DR.GetValue(19).ToString();
				txtTrad2_Cname.Text=DR.GetValue(20).ToString();
				txtTrad2_Contact.Text=DR.GetValue(21).ToString();
				txtTrad2_Phone.Text=DR.GetValue(22).ToString();
				txtBank_Fax.Text=DR.GetValue(23).ToString();
				txtTrad3_Cname.Text=DR.GetValue(24).ToString();
				txtTrad3_Contact.Text=DR.GetValue(25).ToString();
				txtTrad3_Phone.Text=DR.GetValue(26).ToString();
				txtTrad3_Fax.Text=DR.GetValue(27).ToString();
			}
			//Cmd.ExecuteNonQuery();
			
			//Cache["FrmName"]="Credit";


			//Response.Redirect("OrderConfirmation.aspx?ID="+ Session.Contents["ProductId"].ToString());
			conn.Close();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnDeny.Click += new System.EventHandler(this.btnDeny_Click);
			this.btnApprove1.ServerClick += new System.EventHandler(this.btnApprove1_ServerClick);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Submit1_ServerClick(object sender, System.EventArgs e)
		{
		
		}
		private void FillCombos(DropDownList DDL,string TFld,string ValFld,string Query)
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			SqlDataReader DTR;
			SqlCommand Cmd = new SqlCommand();
			Cmd.CommandType=CommandType.Text;
			Cmd.CommandText=Query;
			
			conn.Open();
			Cmd.Connection=conn;
			DTR=Cmd.ExecuteReader();
			DDL.DataSource=DTR;
			DDL.DataTextField=TFld;
			DDL.DataValueField=ValFld;
			DDL.DataBind();
			DTR.Close();
			conn.Close();
			if (DDL.Items.Count>=1)
			{
				DDL.SelectedIndex=0;
			}
			else
			{
				DDL.SelectedIndex=-1;
			}
			
		}

		private void btnDeny_Click(object sender, System.EventArgs e)
		{
			
			//Subroutine for denying the order approval
			SqlConnection conn;
			//////string strCmd ;
			conn = new SqlConnection(Application["DBConn"].ToString());
			SqlCommand Cmd = new SqlCommand();
			Cmd.CommandType=CommandType.Text;

			conn.Open();
			Cmd.Connection=conn;
			Cmd.CommandText = "Delete from Credit_App where Id=" + Convert.ToInt32(ViewState["CR_ID"].ToString());   //+ TmpCrId ;
			Cmd.ExecuteNonQuery();

//			strCmd = "Update  Pending_Orders set ORDR_STAT='D'" ;
//			strCmd +=  " where ordr_srno=" + ORD_SRNO + " and PERSON_ID = " + USR_ID + " AND ";
//			strCmd +=  " (ORDR_BID = " + BIDOFFER_ID + " OR ORDR_OFFR = " + BIDOFFER_ID + ")";
			//Cmd.CommandText = strCmd;
			//conn.Open();
			Cmd.CommandText = "Delete from Pending_Orders where Credit_Id=" + Convert.ToInt32(ViewState["CR_ID"].ToString());   //+ TmpCrId ;
			Cmd.ExecuteNonQuery();

			Cmd.Connection=conn;
			Cmd.ExecuteNonQuery();

			Response.Redirect("PendingTransactions.aspx");
				//*******************************/
				//		Add Mailing Code  Below //
				//*******************************/
		}

		private void btnApprove_Click(object sender, System.EventArgs e)
		{
		

		}

		private void btnApprove1_ServerClick(object sender, System.EventArgs e)
		{
			//CHANGE CODE FOR UPDATING ORDER STATUS AND ADDING TO ORDER TABLE
			if  (txtConfrm.Text.ToString().Trim()=="0")		///Checking whether value of textbox =1 for checking if bill/ship address filled or not
			{
				return;
			}
			string TmpStr=string.Empty;
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			SqlConnection connGet = new SqlConnection(Application["DBConn"].ToString());
			SqlCommand Cmd = new SqlCommand();
			Cmd.CommandType=CommandType.StoredProcedure;
			Cmd.CommandText="spShowSaveCredit";

			foreach (Control c in Page.Controls)
			{
				foreach (Control childc in c.Controls)
				{
					if (childc is TextBox)
					{   
						if (((TextBox)childc).Text.ToString()!="")
						{
							TmpStr = ((TextBox)childc).ID.ToString(); //((TextBox)childc).ID.ToString()
							if (((TextBox)childc).ID.ToString()!="txtConfrm")
							{
								Cmd.Parameters.Add("@"+ TmpStr.Remove(0,3) ,((TextBox)childc).Text.ToString() );
							}
						}
						else
						{
							TmpStr = ((TextBox)childc).ID.ToString();
							Cmd.Parameters.Add("@"+ TmpStr.Remove(0,3) ,Convert.DBNull );
						}
					}
					if (childc is DropDownList)					
					{
						TmpStr = ((DropDownList)childc).ID.ToString();
						TmpStr="@"+TmpStr.Remove(0,3);
						Cmd.Parameters.Add(TmpStr,SqlDbType.Char); //   ((DropDownList)childc).SelectedValue.ToString());
						Cmd.Parameters[TmpStr].Value=((DropDownList)childc).SelectedValue.ToString();
					}
				}
			}
			Cmd.Parameters.Add("@ID",SqlDbType.Int);
			Cmd.Parameters["@ID"].Value = Convert.ToInt32(ViewState["CR_ID"].ToString()); 
			//Cmd.Parameters["@ID"].Value =3725;//ParamCredit
			
			Cmd.Parameters.Add("@Flag",SqlDbType.Char);
			Cmd.Parameters["@Flag"].Value="E"; // "E" for editing by admin

			Cmd.Parameters.Add("@APPROVED",SqlDbType.Char);
			Cmd.Parameters["@APPROVED"].Value = "A";			//Approved Credit application	
			conn.Open();
			Cmd.Connection= conn;
			int TmpCreditID = Convert.ToInt32(Cmd.ExecuteScalar());
			//Cmd.ExecuteNonQuery();  //Changed 8th Jan

			SqlCommand cmdSql = new SqlCommand();
			cmdSql.CommandType=CommandType.Text;
			cmdSql.Connection=conn;
			
			if (Request.QueryString["CREDIT_ID"].ToString()=="0")
			{
				SqlCommand cmdUp = new SqlCommand();
				cmdUp.CommandType=CommandType.Text;
				cmdUp.Connection=conn;
				cmdUp.CommandText="Update Pending_Orders Set Credit_Id=" + TmpCreditID.ToString() + " where ORDR_SRNO =" + Convert.ToInt32(Request.QueryString["ORD_SRNO"].ToString());
				cmdUp.ExecuteNonQuery();
			}

			// Inserting and getting Id of newly inserted company after getting type of company P-Purchaser,S-Seller
			string TmpCmpType = String.Empty;
			SqlCommand cmdGetOffr = new SqlCommand();
			connGet.Open();
			cmdGetOffr.Connection=connGet;
			cmdGetOffr.CommandText="Select ORDR_OFFR from Pending_Orders where ORDR_SRNO =" + Convert.ToInt32(Request.QueryString["ORD_SRNO"].ToString());
			SqlDataReader DROffer = cmdGetOffr.ExecuteReader();
			while (DROffer.Read())
			{
				if (DROffer.IsDBNull(0))
				{
					TmpCmpType ="S";
				}
				else
				{
					TmpCmpType ="P";
				}
			}
			DROffer.Close();
			connGet.Close();

			cmdSql.CommandText="Insert into Company(COMP_TYPE,COMP_NAME,COMP_DATE,COMP_REG,COMP_ENBL) values ('"+ TmpCmpType +"','" + txtComp_Cname.Text.ToString() + "',getdate(),1,1)";
			cmdSql.ExecuteNonQuery();

			cmdSql.CommandText="Select @@Identity from Company";
			int TmpCompId =Convert.ToInt32(cmdSql.ExecuteScalar());
			//Updating Company field in PLace is null and user id = "D"
			cmdSql.CommandText="update place set plac_comp=" + TmpCompId + " where plac_id in ( " ;
			cmdSql.CommandText+="select ordr_shipto as PLACE from pending_orders where ordr_srno=" + Convert.ToInt32(Request.QueryString["ORD_SRNO"].ToString());
			cmdSql.CommandText+="union ";
			cmdSql.CommandText+="select ordr_bilto as PLACE from pending_orders where ordr_srno=" + Convert.ToInt32(Request.QueryString["ORD_SRNO"].ToString()) + ") and plac_comp is null" ;
			cmdSql.ExecuteNonQuery();	
	
			//Update Person changing to Officer from a demo user
			SqlCommand cmdGetPType = new SqlCommand("Select PERS_TYPE from PERSON where Pers_Id=" + Convert.ToInt32(ViewState["USR_ID"].ToString()),conn);
			string PersType = cmdGetPType.ExecuteScalar().ToString();
			if (PersType=="D")
			{
				cmdSql.CommandText="Update Person Set PERS_COMP=" + TmpCompId + ",Pers_Type='O' where Pers_Id=" + Convert.ToInt32(ViewState["USR_ID"].ToString());
				cmdSql.ExecuteNonQuery();	
			}			

			
			conn.Close();

			Response.Redirect("PendingTransactions.aspx");
			//*******************************/
			//		Add Mailing Code  Below //
			//*******************************/

		}
	}
}
