<%
   '*****************************************************************************
   '*   1. File Name       : Administrator\MB_Specs_Brand.aspx                  *
   '*   2. Description     : Display the brand info, accessed from "brand approval" *
   '*			     click on "Specifications" button.                  *
   '*						                                *
   '*   3. Modification Log:                                                    *
   '*     Ver No.       Date          Author             Modification           *
   '*   -----------------------------------------------------------------       *
   '*      1.00       2-24-2004       Xiaoda             Commented              *
   '*                                                                           *
   '*****************************************************************************
%>
<%@ Page Language="VB" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Web.Mail" %>
<!--#INCLUDE FILE="../include/Head.inc"-->

<%
'Database connection'
 Dim conn as SqlConnection
 Dim cmdContent as SQLCommand
 Dim Rec0 as SqlDataReader
 conn = new SqlConnection(Application("DBconn"))
 conn.Open()
	 
'Query brand related info
cmdContent= new SqlCommand("SELECT  BRAN_Name, (SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=BRAN_COMP), (SELECT CONT_LABL FROM CONTRACT WHERE BRAN_CONT = CONT_ID),  (SELECT CONT_CATG FROM CONTRACT WHERE BRAN_CONT = CONT_ID), BRAN_NAME FROM BRAND WHERE BRAN_ID="&Request.Querystring("Id"), conn)
Rec0= cmdContent.ExecuteReader()
Rec0.Read()

Dim temp 'build a stupid array, we will get rid of this sonn.
temp="<td nowrap width=100 CLASS=S8>,<td nowrap WIDTH=164 CLASS=S9>"
Dim Str() As String=Split(temp,",")
IF Rec0(0)<>"" THEN
	Str(0)=Str(0)&"Brand:<br>"
	Str(1)=Str(1)&Rec0(0)&"<br>"
END IF

IF Rec0(1)<>"" THEN
	Str(0)=Str(0)&"Company Name:<br>"
	Str(1)=Str(1)&Rec0(1)&"<br>"
END IF
IF Rec0(2)<>"" THEN
	Str(0)=Str(0)&"Contract name:<br>"
	Str(1)=Str(1)&Rec0(2)&"<br>"
END IF
IF Rec0(3)<>"" THEN
	Str(0)=Str(0)&"Category:<br>"
	Str(1)=Str(1)&Rec0(3)&"<br>"
END IF
%>
<table cellpadding=0 cellspacing=0 width=100% height=100% border=0>
	<tr valign=middle>
		<td>
			<table border=0 cellpadding=0 cellspacing=0 width=350>
				<tr bgcolor=black>
					<td CLASS=S5 colspan=6><img src=/Images/1x1.gif width=3>Brand Specifications
					</td>
				</tr>
				<tr>
					<td bgcolor=black width=1 rowspan=3>
					</td>
					<td width=348 height=5 colspan=4>
					</td>
					<td bgcolor=black width=1 rowspan=3>
					</td>
				</tr>
				<tr>
					<td width=5>
					</td>
<%=Str(0)&"</td>"&Str(1)%>
					</td>
					<td width=5>
					</td>
				</tr>
				<tr>
					<td colspan=4><center>
<input type=button class=tpebutton value='OK' onClick="javascript:Path.hide(window,'specs')">
					</td>
				</tr>
				<tr>
					<td bgcolor=black width=1 height=1 colspan=6>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!--#INCLUDE FILE="../include/Bottom.inc"-->
<%Rec0.Close()
conn.Close()
%>
</body></html>
