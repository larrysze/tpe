<%@ Page language="c#" Codebehind="Email.aspx.cs" AutoEventWireup="True" Inherits="localhost.Email" ValidateRequest="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Email Preview</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<BODY style="FONT: 10pt ARIAL" bottomMargin="0" bgColor="#ffffff" leftMargin="0" topMargin="0"
		rightMargin="0">
		<FORM id="Form1" method="post" runat="server">
			<asp:panel Visible="False" id="pnlEmail" runat="server" Width="100%" Height="80px" BackColor="#E0E0E0">
				<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%" border="0">
					<TR>
						<TD style="WIDTH: 33px">&nbsp;</TD>
						<TD style="WIDTH: 47px">
							<asp:Label id="From" runat="server" Font-Size="10pt">From:</asp:Label></TD>
						<TD>
							<asp:TextBox id="txtFrom" runat="server" Width="536px" Font-Size="10pt" Enabled="False"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 33px">&nbsp;</TD>
						<TD style="WIDTH: 47px">
							<asp:Label id="Label2" runat="server" Font-Size="10pt">To:</asp:Label></TD>
						<TD>
							<asp:TextBox id="txtTo" runat="server" Width="536px" Font-Size="10pt" Enabled="False"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 33px">&nbsp;</TD>
						<TD style="WIDTH: 47px">
							<asp:Label id="Label3" runat="server" Font-Size="10pt">Cc:</asp:Label></TD>
						<TD>
							<asp:TextBox id="txtCC" runat="server" Width="536px" Font-Size="10pt" Enabled="False"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 33px">&nbsp;</TD>
						<TD style="WIDTH: 47px">
							<asp:Label id="Label4" runat="server" Font-Size="10pt">Subject:</asp:Label></TD>
						<TD>
							<asp:TextBox id="TxtSubject" runat="server" Width="536px" Font-Size="10pt" Enabled="False"></asp:TextBox></TD>
					</TR>
				</TABLE>
			</asp:panel>
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD>
						<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD width="5%"></TD>
								<TD><BR>
									<asp:literal id="EmailBody" runat="server"></asp:literal><BR>
								</TD>
								<TD width="5%"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			</TD></TR>
			<TR>
				<TD></TD>
			</TR>
			</TABLE>
		</FORM>
	</BODY>
</HTML>
