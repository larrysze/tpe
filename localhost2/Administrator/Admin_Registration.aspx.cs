using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using TPE.Utility;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Admin_Registration.
	/// </summary>
	public partial class Admin_Registration : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.CustomValidator CustomValidator1;
		protected System.Web.UI.WebControls.CustomValidator CustomValidator2;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator6;
		protected System.Web.UI.WebControls.TextBox txtConfirmPassword;
	/************************************************************************
	*   1. File Name       :Administrator\Admin_Registration.aspx           *
	*   2. Description     :Exchange user registration step 1               *
	*                       Step 2 is Exchange_Registration.aspx	        *
	*   3. Modification Log:                                                *
	*     Ver No.       Date          Author             Modification       *
	*   -----------------------------------------------------------------   *
	*      1.00      2-25-2004      Zach                Comment             *
	*      1.01		 8-24-2004		Matthieu			added comp_name     *
	************************************************************************/

		protected void Page_Load(object sender, EventArgs e)
		{
            if (Session["Typ"].ToString() != "A" && Session["Typ"].ToString() != "L")
            {
                Response.Redirect("/default.aspx");
            }
            Master.Width = "760px";
		}


		protected void Submit(object sender, EventArgs e)
		{
			
			if (IsDuplicate()) // promote person
			{
				pnlMain.Visible=false;
				pnlPromote.Visible= true;
			
			}
			else // insert person 
			{
				CreateCompany();

				//PERS_TYPE is O(officer), PERS_ENBL is 1(enabled)
				string strSql = "INSERT INTO PERSON (PERS_COMP,COMP_NAME,PERS_MAIL,PERS_PSWD, PERS_TYPE, PERS_FRST_NAME, PERS_LAST_NAME, PERS_ENBL, PERS_ACCOUNT_DISABLED) VALUES(@COMP_ID,@COMP_NAME,@Email,@Password,'O',@FirstName,@LastName,1,'False')";
				//VALUES(@COMP_ID,@COMP_NAME,@Email, @Password,'O',@FirstName,@LastName,1)";
				Hashtable htParams = new Hashtable();
				htParams.Add("@COMP_ID",ViewState["COMP_ID"].ToString());
				htParams.Add("@COMP_NAME",txtCompName.Text);
				htParams.Add("@Email",txtEmail.Text);
				htParams.Add("@Password",Crypto.Encrypt(txtPassword.Text));
				htParams.Add("@FirstName",txtfname.Text);
				htParams.Add("@LastName",txtlname.Text);
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),strSql,htParams);

				Response.Redirect("Company_Info.aspx?Id="+ViewState["COMP_ID"].ToString());
			}
			
		}
		// <summary>
		//  Checks to see if the Username is available, no duplicate Username is allowed!
		//  Have to check both PERSON tables in Genius and DEMO database
		// </summary>
		protected bool IsDuplicate()
		{
		
			string strSql = "SELECT PERS_LOGN FROM PERSON WHERE PERS_MAIL=@Email";
			Hashtable htParam = new Hashtable();
			htParam.Add("@Email",txtEmail.Text);
			if (DBLibrary.HasRows(Application["DBconn"].ToString(),strSql,htParam))
			{
				// found same Username exist already!
				return true;
			}
			return false;
		}
		protected void CreateCompany()
		{
			string strSql = "INSERT INTO COMPANY (COMP_TYPE,COMP_REG,COMP_DATE,COMP_FELN,COMP_BKRP,COMP_ENBL, COMP_NAME) VALUES('"+ddlType.SelectedValue.ToString()+"',1,GETDATE(),0,0,1,'"+txtCompName.Text+"')";
			//VALUES('"+ddlType.SelectedValue.ToString()+"',1,GETDATE(),0,0,1,'"+txtCompName.Text+"')";
			Hashtable htParams = new Hashtable();
			htParams.Add("@CompanyType",ddlType.SelectedValue.ToString());
			htParams.Add("@CompanyName",txtCompName.Text);
			
			string strCOMP_ID = DBLibrary.InsertAndReturnIdentity(Application["DBconn"].ToString(),strSql,htParams).ToString();

			StringBuilder sbSQL = new StringBuilder();
			//insert to BRAND table
			sbSQL.Append("INSERT INTO BRAND (BRAN_COMP, BRAN_ENBL) Values("+strCOMP_ID+",0);");
			//insert to Associate
			sbSQL.Append("INSERT INTO ASSOCIATE (ASSO_COMP) Values("+strCOMP_ID+");");
			//insert to BANK
			sbSQL.Append("INSERT INTO BANK (BANK_COMP) Values("+strCOMP_ID+");");
			//insert to CREDIT
			sbSQL.Append("INSERT INTO CREDIT (CRED_COMP,CRED_AMNT,CRED_AVLB,CRED_RATE) Values("+strCOMP_ID+",0,0,0);");
			
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),sbSQL.ToString());
			
			ViewState["COMP_ID"] =  strCOMP_ID;			
		}
	

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnPromote_Click(object sender, System.EventArgs e)
		{		
			CreateCompany();
			Hashtable htParams = new Hashtable();
			htParams.Add("@Email",txtEmail.Text);
			DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(),"UPDATE PERSON SET PERS_COMP='"+ViewState["COMP_ID"].ToString()+"',PERS_TYPE='O' WHERE PERS_MAIL=@Email",htParams);
			
			Response.Redirect("Company_Info.aspx?Id="+ViewState["COMP_ID"].ToString());
		}

		protected void btnBack_Click(object sender, System.EventArgs e)
		{
			pnlMain.Visible=true;
			pnlPromote.Visible= false;
		
		}
	}
}
