<%@ Page language="c#" Codebehind="User_Details.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.User_Details" %>
<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<TITLE>Customer Details</TITLE>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="/include/master.css" type="text/css" rel="stylesheet">		
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form runat="server">
			<table height="460" cellSpacing="0" cellPadding="0" width="500">
				<tr>
					<td align="left"><asp:label id="lblCustomerName" runat="server" Font-Size="Medium"></asp:label></td>
					<td align="right"><asp:button id="btnUpdate" onclick="btnUpdate_Click" runat="server" Text="Update"></asp:button><asp:button id="btnCancel" runat="server" Text="Close" CausesValidation="False"></asp:button><br>
						<br>
					</td>
				</tr>
				<tr>
					<td colSpan="2">
					<iewc:tabstrip id="TabStrip1" style="FONT-WEIGHT: bold" runat="server" TabSelectedStyle="border:solid 1px black;border-bottom:none;background:white;padding-left:5px;padding-right:5px;"
							TabHoverStyle="color:red" TabDefaultStyle="border:solid 1px black;background:#dddddd;padding-left:5px;padding-right:5px;"
							TargetID="mp" SepDefaultStyle="border-bottom:solid 1px #000000;" Width="500px" Height="32px">
							<iewc:Tab Text="Lead History"></iewc:Tab>
							<iewc:TabSeparator></iewc:TabSeparator>
							<iewc:Tab Text="User Information"></iewc:Tab>
							<iewc:TabSeparator></iewc:TabSeparator>
							<iewc:Tab Text="Preferences"></iewc:Tab>
							<iewc:TabSeparator></iewc:TabSeparator>
							<iewc:Tab Text="Schedule Next Contact"></iewc:Tab>
							<iewc:TabSeparator DefaultStyle="width:100%;"></iewc:TabSeparator>
					</iewc:tabstrip>
					<iewc:multipage id="mp" style="BORDER-RIGHT: #000000 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: medium none; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: #000000 1px solid; PADDING-TOP: 5px; BORDER-BOTTOM: #000000 1px solid"
							runat="server" Width="500px" Height="460px">
							<iewc:PageView>
								<table cellSpacing="0" cellPadding="3" width="100%" border="0">
									<tr>
										<TD class="ListHeadlineBold" align="center">New Note</TD>
									</tr>
									<TR>
										<TD vAlign="top" align="center" colSpan="2">
											<asp:textbox id="txtComments" runat="server" Rows="2" TextMode="MultiLine" MaxLength="10" Width="400px"></asp:textbox></TD>
									</TR>
									<TR>
										<TD colspan="2" align="right">
											<asp:button id="btnAddNote" runat="server" Text="Attach Note To Account" CausesValidation="False" onclick="btnAddNote_Click"></asp:button></TD>
									</TR>
									<tr>
										<td colSpan="4"><br>
										</td>
									</tr>
									<tr>
										<TD class="ListHeadlineBold" align="center">Customer History</TD>
									</tr>
									<tr>
										<td>
										    <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
												<tr>
													<td>Registered:
														<asp:label id="lblDateRegistered" runat="server"></asp:label>
													</td>
													<td>Last Login:
														<asp:label id="lblLastLogin" runat="server"></asp:label>
										            </td>
									            </tr>
									            <tr>
										            <td>Total Logins:
											            <asp:label id="lblTotalLogin" runat="server"></asp:label>
										            </td>
									            </tr>
								            </TABLE>
					                    </td>
				                    </tr>
				                    <tr>
					                    <TD colSpan="2" align="left">
						                    <asp:datagrid id="dg" runat="server" CssClass="DataGrid" ItemStyle-CssClass="DataGridRow" ShowHeader="False"
							                    AlternatingItemStyle-CssClass="DataGridRow_Alternate" HeaderStyle-CssClass="DataGridRow_Alternate"
							                    AutoGenerateColumns="False" CellPadding="2" HorizontalAlign="left" OnItemDataBound="db_ItemDataBound" onprerender="dg_PreRender">
							                    <AlternatingItemStyle CssClass="DataGridRow_Alternate"></AlternatingItemStyle>
							                    <ItemStyle CssClass="DataGridRow"></ItemStyle>
							                    <HeaderStyle CssClass="DataGridRow_Alternate"></HeaderStyle>
							                    <Columns>
								                    <asp:ButtonColumn Text="Delete" ButtonType="LinkButton" CommandName="Delete"></asp:ButtonColumn>
								                    <asp:BoundColumn DataField="CMNT_DATE" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}">
									                    <ItemStyle Wrap="False"></ItemStyle>
								                    </asp:BoundColumn>
								                    <asp:BoundColumn DataField="CMNT_TEXT" HeaderText="Comment"></asp:BoundColumn>
								                    <asp:BoundColumn Visible="False" DataField="CMNT_EMAIL_SYSTEM_ID"></asp:BoundColumn>
								                    <asp:BoundColumn Visible="False" DataField="CMNT_ID"></asp:BoundColumn>
							                    </Columns>
						                    </asp:datagrid>
						                 </TD>
				                    </tr>
			                    </table>
			                </iewc:PageView>
			                <iewc:PageView>
				            <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
					            <TR>
						            <TD>First Name:
						            </TD>
						            <TD>
							            <asp:textbox id="txtFirstName" runat="server"></asp:textbox></TD>
						            <TD>Last Name:
						            </TD>
						            <TD>
							            <asp:textbox id="txtLastName" runat="server"></asp:textbox></TD>
					            </TR>
					            <TR>
						            <TD>Title:
						            </TD>
						            <TD>
							            <asp:textbox id="txtTitle" runat="server"></asp:textbox></TD>
						            <TD>
							            <asp:label id="lblCompanyName" runat="server">Company:</asp:label></TD>
						            <TD>
							            <asp:textbox id="txtCompanyName" runat="server"></asp:textbox></TD>
					            </TR>
					            <TR>
						            <TD>Phone:
						            </TD>
						            <TD>
							            <asp:textbox id="txtPhone" runat="server"></asp:textbox></TD>
						            <TD>Password:
						            </TD>
						            <TD>
							            <asp:textbox id="txtPassword" runat="server"></asp:textbox></TD>
					            </TR>
					            <TR>
						            <TD>
							            <asp:label id="lblUsernameLable" runat="server" Visible="False">Username:</asp:label>
							            <asp:label id="lblUsername" runat="server" Visible="False"></asp:label>Email:
						            </TD>
						            <TD colSpan="3">
							            <asp:textbox id="txtEmail" runat="server" Width="267px"></asp:textbox>
							            <asp:label id="lblOldEmail" runat="server" Visible="False"></asp:label>
							            <asp:label id="lblMessage" runat="server" Visible="False" ForeColor="Red"></asp:label>
							            <asp:regularexpressionvalidator id="EmailValidator" runat="server" ErrorMessage="Invalid E-mail!" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
								            ControlToValidate="txtEmail"></asp:regularexpressionvalidator></TD>
					            </TR>
					            <TR>
						            <TD colSpan="4">
							            <table>
								            <tr>
									            <td><B>Broker: </B>
									            </td>
									            <td>
										            <asp:label id="lblBroker" runat="server"></asp:label></td>
								            </tr>
								            <tr>
									            <td><B>Corporate Identity: </B>
									            </td>
									            <td>
										            <asp:dropdownlist id="ddlLeadCategory" runat="server">
											            <asp:ListItem>Please Select</asp:ListItem>
											            <asp:ListItem>Resin Processor</asp:ListItem>
											            <asp:ListItem>Broker/Distributor/Trader</asp:ListItem>
											            <asp:ListItem>Resin Producer</asp:ListItem>
											            <asp:ListItem>Analyst/Market Observer</asp:ListItem>
											            <asp:ListItem>Other</asp:ListItem>
										            </asp:dropdownlist>
									            </td>
								            </tr>
								            <tr>
									            <td><b>Rate User: </b>
									            </td>
									            <td>
										            <asp:dropdownlist id="ddlGrade" runat="server"></asp:dropdownlist></td>
								            </tr>
							            </table>
						            </TD>
					            </TR>
					            <tr>
						            <TD align="center" colSpan="4">
							            <table width="100%">
								            <tr>
									            <td colSpan="2"><B>Delivery:</B>
									            </td>
								            </tr>
								            <tr>
									            <td>
										            <asp:radiobutton id="rdbInsideUS" runat="server" Text="Domestic (Zip Code)" Checked="True" GroupName="Interest"></asp:radiobutton></td>
									            <td>
										            <asp:textbox id="txtZip" Columns="5" runat="Server"></asp:textbox>
										            <asp:label id="lblCity" runat="server"></asp:label>
									            </td>
								            </tr>
								            <tr>
									            <td>
										            <asp:radiobutton id="rdbOutsideUS" runat="server" Text="International (Port)" GroupName="Interest"></asp:radiobutton></td>
									            <td>
										            <asp:dropdownlist id="ddlPort" runat="server"></asp:dropdownlist></td>
								            </tr>
							            </table>
						            </TD>
					            </tr>
				            </TABLE>
			                </iewc:PageView>
			                <iewc:PageView>
				                <table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
					                <tr>
						                <td valign="top">
							                <asp:Table id="tblDynamic" runat="server" BorderStyle="None" GridLines="Horizontal" CellSpacing="0"
								                CellPadding="0" Font-Size="1"></asp:Table>
						                </td>
						                <td valign="top">
							                <table>
								                <tr>
									                <td>
										                <b>Sizes:</b>
										                <ul>
											                <li>
												                <asp:checkbox id="chkSize1" runat="server" Text="Rail Cars"></asp:checkbox></li>
											                <li>
												                <asp:checkbox id="chkSize2" runat="server" Text="Bulk Trucks"></asp:checkbox></li>
											                <li>
												                <asp:checkbox id="chkSize3" runat="server" Text="Truckload Boxes\Bags"></asp:checkbox></li>
										                </ul>
									                </td>
								                </tr>
								                <tr>
									                <td>
										                <b>Quality:</b>
										                <ul>
											                <li>
												                <asp:checkbox id="chkQuality1" runat="server" Text="Prime"></asp:checkbox></li>
											                <li>
												                <asp:checkbox id="chkQuality2" runat="server" Text="Good Offgrade"></asp:checkbox></li>
											                <li>
												                <asp:checkbox id="chkQuality3" runat="server" Text="Regrind/Repro"></asp:checkbox></li>
										                </ul>
									                </td>
								                </tr>
								                <tr>
									                <td>
										                <b>Email Frequency: </b>
										                <asp:RadioButtonList id="rblEmailFrequency" runat="server" RepeatDirection="Vertical" Width="80%">
											                <asp:ListItem Value="2">Daily</asp:ListItem>
											                <asp:ListItem Value="1">Weekly</asp:ListItem>
											                <asp:ListItem Value="0">Never</asp:ListItem>
										                </asp:RadioButtonList><br>
									                </td>
								                </tr>
							                </table>
						                </td>
					                </tr>
				                </table>
			                </iewc:PageView>
			                <iewc:PageView>
				                <table cellSpacing="0" cellPadding="0" width="100%" align="left" border="0">
					                <tr>
						                <td align="center">
							                <asp:calendar id="Calendar1" runat="server" onDayRender="MyCalendar_DayRender"></asp:calendar></td>
					                </tr>
					                <tr>
						                <td align="center" vAlign="top"><B>Regarding:</B><BR>
							                <asp:textbox id="txtBody" runat="server" Columns="30" Rows="2" TextMode="MultiLine"></asp:textbox></td>
					                </tr>
					                <tr>
						                <td vAlign="top" align="center">
							                <asp:button id="btnSubmit" runat="server" Text="Schedule" CausesValidation="False" onclick="btnSubmit_Click"></asp:button></td>
					                </tr>
					                <tr>
						                <td align="center">
							                <asp:Label id="Label1" runat="server" Width="200px" ForeColor="#ff0033"></asp:Label></td>
					                </tr>
				                </table>
			                </iewc:PageView>
			        </iewc:multipage>
			    </td>
			</tr>
			</table>
			</form>
	</body>
</HTML>
