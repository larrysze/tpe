<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditMarketUpdate.aspx.cs" Inherits="localhost.Administrator.EditMarketUpdate" MasterPageFile="~/MasterPages/Menu.Master" Title="Edit Market Update Notepad" %>

<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
<br>
<center><span class="Content Color2"><b>TPE Market Update</B></SPAN></CENTER><br><span class="Content">
<asp:placeholder id=htmlPlace Runat="server"></asp:placeholder></span><
asp:panel id=mainPanel Runat="server">
<asp:datagrid id=dgSummaryGrid runat="server" BorderWidth="0" BackColor="#000000" CellSpacing="1" CellPadding="3"  Width="390" AutoGenerateColumns="False" HorizontalAlign="Center">

	<AlternatingItemStyle BorderStyle="Solid" CssClass="LinkNormal Color2 DarkGray">
	</AlternatingItemStyle>
   
    <ItemStyle CssClass="LinkNormal Color2 LightGray">
    </ItemStyle>

	<HeaderStyle CssClass="LinkNormal Color2 OrangeColor">
	</HeaderStyle>

	<Columns>
	<asp:HyperLinkColumn ItemStyle-HorizontalAlign="Left" DataNavigateUrlField="offr_grade" DataNavigateUrlFormatString="/spot/Spot_Floor.aspx?Filter={0}" DataTextField="GRADE" HeaderText="Resin">
	<HeaderStyle CssClass="LinkNormal" HorizontalAlign="Left">
	</HeaderStyle>


	</asp:HyperLinkColumn>
	<asp:BoundColumn DataField="varsize" HeaderText="Total lbs" DataFormatString="{0:#,##}">
	<HeaderStyle CssClass="hspottitles">
	</HeaderStyle>

	<ItemStyle Wrap="False" CssClass="Content LinkNormal">
	</ItemStyle>
	</asp:BoundColumn>
	<asp:BoundColumn DataField="Min" HeaderText="Low" DataFormatString="{0:c}">
	<HeaderStyle CssClass="hspottitles">
	</HeaderStyle>

	<ItemStyle Wrap="False" CssClass="Content LinkNormal">
	</ItemStyle>
	</asp:BoundColumn>
	<asp:BoundColumn DataField="Max" HeaderText="High" DataFormatString="{0:c}">
	<HeaderStyle CssClass="hspottitles">
	</HeaderStyle>

	<ItemStyle Wrap="False" CssClass="Content LinkNormal">
	</ItemStyle>
	</asp:BoundColumn>
	<asp:TemplateColumn Visible="False" HeaderText="Price Range">
	<HeaderStyle CssClass="hspottitles">
	</HeaderStyle>

	<ItemStyle Wrap="False" CssClass="Content LinkNormal">
	</ItemStyle>
	</asp:TemplateColumn>
	</Columns>
			</asp:datagrid><BR><FONT class="Content"><B>Feedstock cost</B></FONT> 
<TABLE id=Table1 width="100%" border=0>
  <TR width="100%"></TR></TABLE>
<asp:textbox id=txtComments runat="server" CssClass="InputForm" Width="100%" TextMode="MultiLine" Height="170px"></asp:textbox><BR><BR>
<HR SIZE=2>
<BR>
<TABLE id=Table2 width="100%">
  <TR>
    <TD style="WIDTH: 183px; HEIGHT: 20px">
      <P><FONT class="Content"><B>Polyethylene</B></FONT></P></TD></TR>
  <TR>
    <TD colSpan=2>
<asp:TextBox id=txtPreTextPE Runat="server" CssClass="InputForm" Width="100%" TextMode="MultiLine" Height="130px"></asp:TextBox></TD></TR>
  <TR>
    <TD width="50%">
<asp:image id=imgPolyethyleneYear runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_3_1M.png" height="225"></asp:image></TD>
    <TD width="50%">
<asp:image id=imgPolyethyleneMonth runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_3_1Y.png" height="225"></asp:image></TD></TR>
  <TR>
    <TD style="HEIGHT: 215px" colSpan=2>
<asp:textbox id=txtPolyethylene runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="230px"></asp:textbox></TD>
  <TR></TR>
  <TR>
    <TD style="WIDTH: 183px">
      <P>&nbsp;</P>
      <P><FONT class="Content"><B>Polypropylene</B></FONT></P></TD></TR>
  <TR>
    <TD colSpan=2>
<asp:TextBox id=txtPreTextPP Runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="130px"></asp:TextBox></TD></TR>
  <TR>
    <TD width="50%">
<asp:image id=Image1 runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_26_1M.png" height="225"></asp:image></TD>
    <TD width="50%">
<asp:image id=Image2 runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_26_1Y.png" height="225"></asp:image></TD>
  <TR>
    <TD style="HEIGHT: 215px" colSpan=2>
<asp:textbox id=txtPolypropylene runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="230px"></asp:textbox></TD></TR>
  <TR>
    <TD style="WIDTH: 183px">
      <P>&nbsp;</P>
      <P><FONT class="Content"><B>Polystyrene</B></FONT></P></TD></TR>
  <TR>
    <TD colSpan=2>
<asp:TextBox id=txtPreTextPS Runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="130px"></asp:TextBox></TD></TR>
  <TR>
    <TD width="50%">
<asp:image id=Image3 runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_20_1M.png" height="225"></asp:image></TD>
    <TD width="50%">
<asp:image id=Image4 runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_20_1Y.png" height="225"></asp:image>
  <TR>
    <TD style="HEIGHT: 215px" colSpan=2>
<asp:textbox id=txtPolystyrene CssClass="InputForm" runat="server" Width="100%" TextMode="MultiLine" Height="230px"></asp:textbox></TD></TR></TABLE><BR><BR>
<BR><BR><BR>
<TABLE id=Table3 width="100%" runat="server">
  <TR>
    <TD align=center>
<asp:button id=btnSave CssClass="Content Color2" runat="server" Text="Save" onclick="btnSave_Click"></asp:button>&nbsp; 
<asp:button id=btnPreview CssClass="Content Color2" runat="server" Text="Preview" onclick="btnPreview_Click"></asp:button></TD>
    <TD align=center>
<asp:button id=btnPublish runat="server" CssClass="Content Color2" Text="Publish" DESIGNTIMEDRAGDROP="156" onclick="btnPublish_Click"></asp:button>
<BR></TD></TR></TABLE></asp:panel>
<CENTER><asp:button id=btnClose CssClass="Content Color2" runat="server" Text="OK" Visible="False" onclick="btnClose_Click"></asp:button></CENTER>
<CENTER><asp:button id=btnSure CssClass="Content Color2" runat="server" Text="Yes, I am sure" Visible="False" onclick="btnSure_Click"></asp:button><asp:button id=btnCancel runat="server" CssClass="Content Color2" Text="No, later" Visible="False" onclick="btnCancel_Click"></asp:button></CENTER><br>

</asp:Content>
