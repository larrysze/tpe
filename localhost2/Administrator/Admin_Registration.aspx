<%@ Page Language="c#" Codebehind="Admin_Registration.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.Admin_Registration" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" Title="Add new Account" %>
<%@ MasterType VirtualPath="~/MasterPages/TemplateWithoutInstructions.Master" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphHeading"><div class="Header Bold Color1">Add New Account</div></asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
   <br> <asp:Panel runat="server" ID="pnlMain">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="Content Color3" HeaderText="There are problems with the following fields. Please correct the errors below."></asp:ValidationSummary>
        <table align="center" class=Content>
            <tr align=left>
                <td>
                    First Name:
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="First name can not be blank." Display="none" ControlToValidate="txtfname"></asp:RequiredFieldValidator>
                    <asp:TextBox ID="txtfname" CssClass="InputForm" runat="server" MaxLength="50"></asp:TextBox></td>
                <td>
                    Last Name:
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Last name can not be blank." Display="none" ControlToValidate="txtlname"></asp:RequiredFieldValidator>
                    <asp:TextBox ID="txtlname" CssClass="InputForm" runat="server" MaxLength="15"></asp:TextBox></td>
            </tr>
            <tr align=left>
                <td>
                    Email:
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Email can not be blank." Display="none" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                    <asp:TextBox ID="txtEmail" CssClass="InputForm" runat="server" MaxLength="50"></asp:TextBox></td>
                <td>
                    Company name:
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Company name can not be blank." Display="none" ControlToValidate="txtCompname"></asp:RequiredFieldValidator>
                    <asp:TextBox ID="txtCompName" CssClass="InputForm" runat="server" MaxLength="15"></asp:TextBox></td>
            </tr>
            <tr align=left>
                <td>
                    Password:
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Password can not be blank." Display="none" ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
                    <asp:TextBox ID="txtPassword" CssClass="InputForm" runat="server" MaxLength="15" TextMode="Password"></asp:TextBox></td>
                <td colspan="2">
                    &nbsp;</td>
            </tr>
            <tr align=left>
                <td colspan="4">
                    Type of Account:
                    <asp:DropDownList ID="ddlType" CssClass="InputForm" runat="server">
                        <asp:ListItem Text="Purchaser" Value="P" />
                        <asp:ListItem Text="Supplier" Value="S" />
                        <asp:ListItem Text="Distributor" Value="D" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr align=left>
                <td colspan="4">
                    <center>
                        <br>
                        <br>
                        <asp:Button ID="Button1" CssClass="Content Color2" OnClick="Submit" runat="server" Text="Add Account"></asp:Button></center>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlPromote" Visible="false">
        <table align="center">
            <tr>
                <td align="center">
                    <br>
                    <br>
                    This is account an account tied to this email address. Do you want to promote this lead to an exchange account?
                    <br>
                    <br>
                    <b>Note:</b> Promoting an account maintains the users original password<br>
                    <br>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnBack" runat="server" CssClass="Content Color2" Text="Back " OnClick="btnBack_Click"></asp:Button>&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnPromote" runat="server" CssClass="Content Color2" Text="Promote" OnClick="btnPromote_Click"></asp:Button></td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
