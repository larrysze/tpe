<%@ Page Language="c#" CodeBehind="Location_Details.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.Location_Details" %>
<HTML>
	<HEAD>
		<link href="../include/master.css" type="text/css" rel="STYLESHEET">
			<asp:PlaceHolder id="phBody" runat="server"></asp:PlaceHolder></FORM>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form runat="server">
			<table width="200">
				<tbody>
					<tr>
						<TD class="ListHeadlineBold" colSpan="2" align="center">
							<asp:Label id="lbllocation" runat="server"></asp:Label>
						</TD>
					</tr>
					<tr>
						<td colspan="2">
							<asp:ValidationSummary id="ValidationSummary1" runat="server" HeaderText=""></asp:ValidationSummary>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<asp:Label id="lblText" runat="server"></asp:Label></td>
					</tr>
					<tr>
						<td width="62">
							Type:
						</td>
						<td>
							<asp:Label id="lblType" runat="server"></asp:Label></td>
					</tr>
					<asp:Panel id="pnlInst" Visible="False" Runat="Server">
						<TR>
							<TD width="62">Location Name:
							</TD>
							<TD>
								<asp:TextBox id="txtComments" runat="server"></asp:TextBox></TD>
						</TR>
					</asp:Panel>
					<asp:Panel id="pnlLabl" Visible="False" Runat="Server"></asp:Panel>
					<tr>
						<td width="62">
							Contact:
						</td>
						<td>
							<asp:TextBox id="txtContact" runat="server"></asp:TextBox>
						</td>
					</tr>
					<TR>
						<TD width="62">Email:
						</TD>
						<TD>
							<asp:TextBox id="txtEmail" runat="server"></asp:TextBox></TD>
					</TR>
					<tr>
						<td width="62">
							Phone:
						</td>
						<td>
							<asp:TextBox id="txtPhone" runat="server"></asp:TextBox>
						</td>
					</tr>
					<TR>
						<TD width="62">Fax:</TD>
						<TD>
							<asp:TextBox id="txtFax" runat="server"></asp:TextBox></TD>
					</TR>
					<tr>
						<td width="62">
							Address:
						</td>
						<td>
							<asp:TextBox id="txtAddress1" runat="server"></asp:TextBox>
						</td>
					</tr>
					<TR>
						<TD width="62"></TD>
						<TD>
							<asp:TextBox id="txtAddress2" runat="server"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD width="62"></TD>
						<TD>
							<asp:TextBox id="txtAddress3" runat="server"></asp:TextBox></TD>
					</TR>
					
					<TR>
						<TD width="62">Zip Code:
						</TD>
						<TD>
							<asp:TextBox id="txtZip" runat="server"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD width="62">Rail #:
						</TD>
						<TD>
							<asp:TextBox id="txtRail" runat="server"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2">
							<asp:Button id="Button1" onclick="Click_Save" runat="server" Text="Save"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT onclick="javascript:window.close();" type="button" value="Close">
						</TD>
					</TR>
				</tbody>
			</table>
			<!-- Insert content here --></form>
	</body>
</HTML>
