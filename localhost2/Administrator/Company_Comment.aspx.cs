using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using localhost;
using TPE.Utility;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Company_Comment.
	/// </summary>
	public partial class Company_Comment : System.Web.UI.Page
	{
		//protected System.Web.UI.WebControls.TextLengthValidator ValidLength;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			
			//only adminstrator and borker can access this page.
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "L"))
			{
				Response.Redirect("../default.aspx");
			}
			
			if (!IsPostBack)
			{
				try
				{
                    if (Request.UrlReferrer != null)
                    {
                        ViewState["Referrer"] = Request.UrlReferrer.AbsoluteUri.ToString();
                    }
                    else if (Request.QueryString["Referrer"].ToString() != null)
                    {
                        ViewState["Referrer"] = Request.QueryString["Referrer"].ToString();
                    }
				}
				catch
				{
					// do nothing
				}
				ViewState["COMPANY_ID"] = Request.QueryString["ID"];
				Bind();
			}
		}

		private void Bind()
		{
			Hashtable htParams = new Hashtable();
			htParams.Add("@CompanyID",ViewState["COMPANY_ID"].ToString());
			DBLibrary.BindDataGrid(Application["DBconn"].ToString(),dg,"SELECT CMNT_TEXT,CMNT_DATE,CMNT_COMP,CMNT_USER= (SELECT PERS_FRST_NAME FROM PERSON WHERE PERS_ID=CMNT_USER_ID ) FROM COMPANY_COMMENT WHERE CMNT_COMP = @CompanyID ORDER BY CMNT_DATE DESC",htParams);	
		}

		protected void btnAddNew_Click(object sender, System.EventArgs e)
		{
			if(txtComment.Text.Length > 8000)
			{
				lblCommentTooLong.Text = "The comment is too long, its length is " + (txtComment.Text.Length - 8000) + " over the limit.";
                lblCommentTooLong.Visible = true;
				return;
			}
			
			string strSQL = "INSERT INTO COMPANY_COMMENT (CMNT_USER_ID,CMNT_TEXT,CMNT_COMP) VALUES (@ID,@Comment,@CompanyID)";
			Hashtable htParams = new Hashtable();
			htParams.Add("@ID",Session["ID"].ToString());
			htParams.Add("@Comment",txtComment.Text);
			htParams.Add("@CompanyID",ViewState["COMPANY_ID"].ToString());
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),strSQL,htParams);
			
			Bind();
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnConclude_Click(object sender, System.EventArgs e)
		{
			Response.Redirect(ViewState["Referrer"].ToString());
		}
	}
}