using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using NineRays.Web.UI.WebControls.FlyTreeView;
using System.IO;
using TPE.Utility;

namespace localhost.Administrator
{
	/// <summary>
	/// notes 的摘要说明。
	/// </summary>
	public partial class notes : System.Web.UI.Page
	{
			
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// 在此处放置用户代码以初始化页面
			//if(!Page.IsPostBack)
			//{
			this.ViewState["ID"]=Session["ID"].ToString();				
			LoadTreeViewNotes();	
			//}
		
		}		
		private void LoadTreeViewNotes()
		{			
			// Put user code to initialize the page here
			
			// adding nodes			
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
				if ((string)Session["Typ"] == "A" || (string)Session["Typ"] == "L")
				{
                    //using (SqlDataReader dtrBroker = DBLibrary.GetDataReaderFromSelect(conn, "select NAME = CAST(PERS_FRST_NAME as varchar) +' ' +CAST(PERS_LAST_NAME as varchar) ,PERS_ID, NUMLEAD = (SELECT COUNT(*) FROM GROUP_MEMBER WHERE GROUP_MEMBER_BROKER_ID = P.pers_id) + (SELECT COUNT(*) FROM PERSON WHERE PERS_ENBL='1' AND PERS_TYPE in ('S','O','P','D','B') AND PERS_ACES = P.pers_id AND pers_id not in (select group_member_contact_id from group_member))from person P where ( PERS_TYPE='B' or PERS_TYPE='T' or PERS_TYPE='A' or PERS_TYPE='L') AND PERS_ENBL='1' AND PERS_ID = 26403 ORDER BY NAME"))
                    Hashtable param = new Hashtable();
                    param.Add("@dummy","dummy");
                    using (SqlDataReader dtrBroker = DBLibrary.GetDataReaderStoredProcedure(conn,"p_getBroker",param))
					{				
						NineRays.Web.UI.WebControls.FlyTreeView.TreeNode mytn = new NineRays.Web.UI.WebControls.FlyTreeView.TreeNode();
						mytn.Key="Broker_Unallocated";
						mytn.Text="Unallocated Leads";
						mytn.ImageUrl="office2003_contacts.gif";				    
						myNotes.Nodes.Add(mytn);
						mytn = new NineRays.Web.UI.WebControls.FlyTreeView.TreeNode();
						mytn.Key="Broker_All";
						mytn.Text=" All Leads";
						mytn.ImageUrl="office2003_contacts.gif";				    
						myNotes.Nodes.Add(mytn);	
						//myNotes.Nodes.Add(mytn);
					
						int nLeads;
						while (dtrBroker.Read())
						{		
							//Hashtable param = new Hashtable();
							//param.Add("@BrokerId", dtrBroker["pers_id"]);
							//nLeads = (int)DBLibrary.ExecuteScalarStoredProcedure(Application["DBConn"].ToString(),"spCalculateTotalLeads", param);

                            if (Convert.ToInt32(dtrBroker["NUMLEAD"].ToString()) > 50 || dtrBroker["pers_id"].ToString().Equals(System.Configuration.ConfigurationSettings.AppSettings["newBrokerId"].ToString())) //Sherie has 14 leads under her, so I use 15
						    {
							    NineRays.Web.UI.WebControls.FlyTreeView.TreeNode tn = new NineRays.Web.UI.WebControls.FlyTreeView.TreeNode();
							    tn.Key = "Broker_" + dtrBroker["pers_id"].ToString();

							    if (this.ViewState["ID"].ToString().Equals(dtrBroker["pers_id"].ToString()))
							    {
							        tn.Text = "<a href='#' id=default_" + dtrBroker["pers_id"].ToString() + " ondragenter='handleDragEnter()' ondrop='drop()' ondragover='cancelEvent()' ondragleave='handleDragLeave()' class='red'>" + dtrBroker["Name"].ToString() + " (" + dtrBroker["NUMLEAD"].ToString() + ")" + "</a>";
							        //tn.Text="<a href='#' id=default_"+dtrBroker["pers_id"].ToString()+" ondragenter='handleDragEnter()' ondrop='drop()' ondragover='cancelEvent()' ondragleave='handleDragLeave()'><FONT color=#0000ff><b>"+dtrBroker["Name"].ToString()+"</b></font></a>";
							    }
							    else
							    {
							        tn.Text = "<a href='#' id=default_" + dtrBroker["pers_id"].ToString() + " ondragenter='handleDragEnter()' ondrop='drop()' ondragover='cancelEvent()' ondragleave='handleDragLeave()'>" + dtrBroker["Name"].ToString() + " (" + dtrBroker["NUMLEAD"].ToString() + ")" + "</a>";
							    }

							    tn.ImageUrl = "office2003_contacts.gif";
							    //CreatSubNotes(tn,dtrBroker["pers_id"].ToString(),dtrBroker["pers_id"].ToString());
							    tn.NodeSrc = "subnote.aspx?persId=" + dtrBroker["pers_id"].ToString();
							    tn.ContextMenuID = "FlyContextMenu2";
							    tn.DragDropAcceptNames = "b";
							    tn.DragDropName = "a";
							    tn.DragDropJavascript = "return true;";
							    myNotes.Nodes.Add(tn);
						    }
						}
					}
				}
				else
				{
					using (SqlDataReader dtrGroup = DBLibrary.GetDataReaderFromSelect(conn,"SELECT NAME = CAST(PERS_FRST_NAME as varchar) +' ' +CAST(PERS_LAST_NAME as varchar) ,PERS_ID from person where pers_id="+this.ViewState["ID"].ToString()))
					{
						//binding dd list
						if (dtrGroup.Read())
						{					
							NineRays.Web.UI.WebControls.FlyTreeView.TreeNode tn = new NineRays.Web.UI.WebControls.FlyTreeView.TreeNode();
							tn.Key="Broker_"+dtrGroup["pers_id"].ToString();
							tn.Text="<a href='#' id=default_"+dtrGroup["pers_id"].ToString()+" ondragenter='handleDragEnter()' ondrop='drop()' ondragover='cancelEvent()' ondragleave='handleDragLeave()'>"+dtrGroup["Name"].ToString()+"</a>";
							tn.ImageUrl="office2003_contacts.gif";					
							//CreatSubNotes(tn,dtrBroker["pers_id"].ToString(),dtrBroker["pers_id"].ToString());
							tn.NodeSrc="subnote.aspx?persId="+dtrGroup["pers_id"].ToString();
							tn.ContextMenuID="FlyContextMenu2";
							myNotes.Nodes.Add(tn);	
						}	
					}				
				}
			
			}
		}

		#region Web 窗体设计器生成的代码
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: 该调用是 ASP.NET Web 窗体设计器所必需的。
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// 设计器支持所需的方法 - 不要使用代码编辑器修改
		/// 此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
