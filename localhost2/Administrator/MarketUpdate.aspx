<%@ Page language="c#" Codebehind="MarketUpdate.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.MarketUpdate" MasterPageFile="~/MasterPages/Menu.Master" Title="Market Update Notepad" %>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">
   <script language= "javascript">
             
        function aspSpellCheck(strName) 
      {
	    /* Default Values - May be ammended */
	    /*var setupPath = "/aspspellcheck/";*/
        var setupPath = "~/Tools/EditMU.aspx";
	    var languages = "English (International)";
	    var windowLanguage = "EN";
	    var useServerSession = true;
	    var hideSummary = false;
	    var ignoreAllCaps = true;
	    var ignoreWebAddresses = true;
	    var ignoreNumbers = true;
	    var newSentanceOnEachNewLine = false;
	    var checkGramar = true;
	    var caseSensitive = true;
	    var externalCSS = "";
	    /* End of Default Values*/
	    /* Do Not Edit Below This Line*/
	    this.uid = uid;
	    this.setupPath = setupPath;
	    this.languages = languages;
	    this.useServerSession = useServerSession;
	    this.hideSummary = hideSummary;
	    this.ignoreAllCaps = ignoreAllCaps;
	    this.ignoreWebAddresses = ignoreWebAddresses;
	    this.ignoreNumbers = ignoreNumbers;
	    this.newSentanceOnEachNewLine = newSentanceOnEachNewLine;
	    this.checkGramar = checkGramar;
	    this.caseSensitive = caseSensitive;
	    this.externalCSS = externalCSS;
	    this.windowLanguage = windowLanguage;   
      	    var uid = UID();
	        function ajaxTest() 
           {
		        var xmlhttp = false;
		        
		        if (!xmlhttp && typeof XMLHttpRequest != 'undefined') 
              {
			    
                try {
				      xmlhttp = new XMLHttpRequest();
			        } catch (e) 
              {
				xmlhttp = false;
			  }
	        }
		    if (xmlhttp) 
            {
			return true;
		    }
		    return false;
       }
	    function ajaxCallBack(result) 
        {
		//
	    }
	    function spellCheckWindowTest() 
        {
		if (!ajaxEnabled) 
           {
			return null;
		   }
		    var strword = '';
		
		    for (var i=0; i<arguments.length; i++) 
        {
			var testArg = arguments[i].replace('window.', '');
			strword += window.JavaScriptSpellCheck__PostForward_API(testArg)+' ';
		}
	    	
		var ajaxURL = spellURL('', this)+"&ajax=true&word="+escape(strword);
		var result = getXMLFromURL(ajaxURL, false);
		return result;
	   }
	        getXMLFromURL;
	        function spellCheckWindow() 
            {
		    var strFields = '';
		      for (i=0; i<arguments.length; i++) 
            {
			    if (strFields) {
				strFields = strFields+",";
		    }
			arguments[i] = arguments[i].replace('window.', '');
			strFields = strFields+arguments[i];
		}
		    window['FN'+this.uid] = this.callBack;
		    window.open(spellURL(strFields, this), 'JS_SpellWin', 'width=460, height=290, scrollbars=no');
		    return true;
	    }
	        function callBack() 
    {
	    	/*empty function object*/
	}
	        function UID() 
    {
		    return uid=new Date().getTime()+"X"+(Math.random()+'').replace('.', '');
	}
	        function getXMLFromURL(strURL, binAsync) 
              {
		
		        binAsync = (binAsync == true);
		        var xmlhttp = false;
		        /*@cc_on @*/
		        /*@if (@_jscript_version >= 5)
		         try {
		          xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		         } catch (e) {
		          try {
		           xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		          } catch (E) {
		           xmlhttp = false;
		          }
		         }
		        @end @*/
		    if (!xmlhttp && typeof XMLHttpRequest != 'undefined') 
      {
			xmlhttp = new XMLHttpRequest();
		}
		    arrURL = strURL.split("&word=");
		    strURL = arrURL[0];
		    arrURL[1]=arrURL[1].replace(/\+/g,"%2B")
		    var strPOST = 'word='+arrURL[1];
		
		    xmlhttp.open("POST", strURL, binAsync);
		    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		    var response_text;
		    xmlhttp.onreadystatechange = function() 
            {
			if (xmlhttp.readyState == 4) 
            {
				responseXML = (xmlhttp.responseXML);
				if (binAsync) 
                  {
					if (!responseXML.getElementsByTagName('id')[0]) 
                     {
						if (xmlhttp.responseText.indexOf('<script') != -1) 
                        {
						var op=xmlhttp.responseText.split('<?x')
						var opp=op[0].replace(/(<([^>]+)>)/ig,""); 
						eval(opp) ; 
						op[0]="" ;	responseXML.loadXML(op.join("<?x"))
					
						} else {							
							if (document.location.href.toLowerCase().indexOf('http')==-1)
                                {
								strMessage='SpellChecker -  must be used on a Microsoft Web-Server.'
								}else{
								var strMessage='SpellChecker - Ajax Instalation Error'	
								}
							alert(strMessage);
							return false;
						}
						
					}
					ajaxuid = responseXML.getElementsByTagName('id')[0].firstChild.nodeValue;
					window['FN_JavaScriptSpellCheck__AJAX_API'+ajaxuid](parseXMLDoc(responseXML));
					window['FN_JavaScriptSpellCheck__AJAX_API'+ajaxuid] = null;
			    	}
			     }
		    };
		        try{xmlhttp.send(strPOST);}catch(e){ return null}
		            if (!binAsync) 
                  {
			
		        var responseXML = (xmlhttp.responseXML);
			
					if (!responseXML.getElementsByTagName('ajaxspell')[0]) 
                    {
					
						if (xmlhttp.responseText.indexOf('<script') != -1 ) 
                        {
							
							if(arrURL[1].length<1){return true}
							
								var op=xmlhttp.responseText.split('<?x')
								var opp=op[0].replace(/(<([^>]+)>)/ig,""); 
								alert(opp)
								eval(opp)
								op[0]="" ;	responseXML.loadXML(op.join("<?x"))
						
						} else {
							alert('Javascript SpellCheck Ajax Instalation Error');
							return false;
						}
				
				}
			
		            	return parseXMLDoc(responseXML);
		        }
	        }
	            function spellURL(strFields, objCaller) 
            {
		        if (!strFields) 
            {
		        	strFields = '';
		    }
		        var arrfields=strFields.split(",")
		        for (var strItt in arrfields)
                {
			    arrfields[strItt]="js:"+arrfields[strItt].replace(" ","").replace(" ","")
		        }
		        strFields = 	arrfields.join(",")
		        languages= objCaller.languages
		        var url = setupPath+'/aspspellcheck.asp?fields='+strFields;
		
		        if ((languages+'').length>0) 
              {
			    url += "&Langs="+languages;
			    if (languages.indexOf(",")>-1) 
                {
				url += "&multiLang=True";
			    }
		      }
		    arrCharictaristics = new Array("useServerSession|Session", "windowLanguage|dialogLanguage", "hideSummary|AutoClose", "checkGramar|Grammar", "caseSensitive|CaseSensitive", "externalCSS|CSS", "windowLanguage|UILang", "ignoreAllCaps|AllCaps", "ignoreWebAddresses|Web", "ignoreNumbers|Numeric", "newSentanceOnEachNewLine|NewLine", "uid|uid");
		    for (strItterand in arrCharictaristics) 
            {
		    	var arrSplit = arrCharictaristics[strItterand].split("|");
			url += "&"+arrSplit[1]+"="+eval('objCaller.'+arrSplit[0]);
		    }
		    return url;
	     }
	      function spellCheck(strInput) 
          {
	    	if (!ajaxEnabled) 
            {
			return null;
		    }
		var ajaxURL = spellURL('', this)+"&ajax=true&suggest=true&word="+escape(strInput);
		return getXMLFromURL(ajaxURL, false);
	     }
	    function ajaxSpellCheck(strInput) 
        {
		if (!ajaxEnabled) 
        {
			return null;
		}
		var ajaxuid = UID();
		window['FN_JavaScriptSpellCheck__AJAX_API'+ajaxuid] = this.ajaxCallBack;
		var ajaxURL = spellURL('', this)+"&ajax=true&id="+ajaxuid+"&suggest=true&word="+escape(strInput);
		getXMLFromURL(ajaxURL, true);
		return true;
	}
	    function parseXMLDoc(mydoc) {
		if (mydoc.getElementsByTagName('word').length == 0) {
			return null;
		}
		if (mydoc.getElementsByTagName('spellcheck')[0].firstChild.nodeValue.toUpperCase() == "TRUE") {
			return true;
		}
		if (mydoc.getElementsByTagName('suggestion').length<1) {
			return false;
		}
		var suggestions = new Array();
		for (i=0; i<mydoc.getElementsByTagName('suggestion').length; i++) {
			suggestions[i] = unescape(mydoc.getElementsByTagName('suggestion')[i].firstChild.nodeValue);
		}
		return suggestions;
	    }
	    ajaxEnabled = ajaxTest();
	    // Public Functions
	    this.ajaxEnabled = ajaxEnabled;
	    this.spellCheckWindow = spellCheckWindow;
	    this.spellCheckWindowTest = spellCheckWindowTest;
	    this.spellCheck = spellCheck;
	    this.ajaxSpellCheck = ajaxSpellCheck;
	    this.callBack = callBack;
	    this.ajaxCallBack = ajaxCallBack;
    }
    function JavaScriptSpellCheck__Postback_API(strObj, strValue) {
	strObj = JavaScriptSpellCheck__DOM_API(strObj);
	eval(strObj+' = unescape("'+escape(strValue)+'")');
	return true;
  }
    function JavaScriptSpellCheck__DOM_API(strObj) {
	var arrObj = strObj.split('.');
	strObj = 'window';
	for (i=0; i<arrObj.length; i++) {
		if (i == 0) {
			if (!eval(strObj+'.'+arrObj[i])) {
				if (document.getElementById(arrObj[i])) {
					arrObj[i] = "document.getElementById('"+arrObj[i]+"')";
				}
			}
		}
		if (eval(strObj+'.'+arrObj[i])) {
			strObj += '.'+arrObj[i];
		} else if (arrObj[i] == 'document' && eval(strObj+'.'+'contentDocument')) {
			strObj += '.'+'contentDocument';
		} else {
			strObj = 'false';
			break;
		}
	}
	    var objObj = eval(strObj);
	    if (objObj.value!=undefined) {
		strObj += '.value';
	    } else if (objObj.innerHTML) {
		strObj += '.innerHTML';
	}

	     return strObj;
   }
        function JavaScriptSpellCheck__PostForward_API(strObj) {
	    var binGood = false;
	    var strObjMem = strObj;
	    strObj = JavaScriptSpellCheck__DOM_API(strObj);
	    eval("if("+strObj+"){binGood=true }");
	    if (!binGood) {
		//	alert('Object: '+strObjMem+"\n Does not exist")
		return null;
	}
	    var strOut = eval(strObj);

	    if (strOut.replace  ){
	    strOut = strOut.replace(/^\s*|\s*$/g,"");}
	    return strOut;
   }
   </script>
</asp:Content>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
<br>
<center><span class="Content Color2"><b>TPE Market Update</B></SPAN></CENTER><br><span class="Content"><asp:placeholder id=htmlPlace 
Runat="server"></asp:placeholder></span><asp:panel id=mainPanel 
Runat="server">
<asp:datagrid id=dgSummaryGrid runat="server" BorderWidth="0" BackColor="#000000" CellSpacing="1" CellPadding="3"  Width="390" AutoGenerateColumns="False" HorizontalAlign="Center">

	<AlternatingItemStyle BorderStyle="Solid" CssClass="LinkNormal Color2 DarkGray">
	</AlternatingItemStyle>
   
    <ItemStyle CssClass="LinkNormal Color2 LightGray"> 
    </ItemStyle>

	<HeaderStyle CssClass="LinkNormal Color2 OrangeColor">
	</HeaderStyle>

	<Columns>
	<asp:HyperLinkColumn ItemStyle-HorizontalAlign="Left" DataNavigateUrlField="offr_grade" DataNavigateUrlFormatString="/spot/Spot_Floor.aspx?Filter={0}" DataTextField="GRADE" HeaderText="Resin">
	<HeaderStyle CssClass="LinkNormal" HorizontalAlign="Left">
	</HeaderStyle>


	</asp:HyperLinkColumn>
	<asp:BoundColumn DataField="varsize" HeaderText="Total lbs" DataFormatString="{0:#,##}">
	<HeaderStyle CssClass="hspottitles">
	</HeaderStyle>

	<ItemStyle Wrap="False" CssClass="Content LinkNormal">
	</ItemStyle>
	</asp:BoundColumn>
	<asp:BoundColumn DataField="Min" HeaderText="Low" DataFormatString="{0:c}">
	<HeaderStyle CssClass="hspottitles">
	</HeaderStyle>

	<ItemStyle Wrap="False" CssClass="Content LinkNormal">
	</ItemStyle>
	</asp:BoundColumn>
	<asp:BoundColumn DataField="Max" HeaderText="High" DataFormatString="{0:c}">
	<HeaderStyle CssClass="hspottitles">
	</HeaderStyle>

	<ItemStyle Wrap="False" CssClass="Content LinkNormal">
	</ItemStyle>
	</asp:BoundColumn>
	<asp:TemplateColumn Visible="False" HeaderText="Price Range">
	<HeaderStyle CssClass="hspottitles">
	</HeaderStyle>

	<ItemStyle Wrap="False" CssClass="Content LinkNormal">
	</ItemStyle>
	</asp:TemplateColumn>
	</Columns>
			</asp:datagrid><BR><FONT class="Content"><B>Feedstock cost</B></FONT> 
<TABLE id=Table1 width="100%" border=0>
  <TR width="100%"></TR></TABLE>
<asp:textbox id=txtComments runat="server" CssClass="InputForm" Width="100%" TextMode="MultiLine" Height="170px"></asp:textbox><BR><BR>
<HR SIZE=2>
<BR>
<TABLE id=Table2 width="100%">
  <TR>
    <TD style="WIDTH: 183px; HEIGHT: 20px">
      <P><FONT class="Content"><B>Polyethylene</B></FONT></P></TD></TR>
  <TR>
    <TD colSpan=2>
<asp:TextBox id=txtPreTextPE Runat="server" CssClass="InputForm" Width="100%" TextMode="MultiLine" Height="130px"></asp:TextBox></TD></TR>
  <TR>
    <TD width="50%">
    <center><asp:Label ID="Label4" CssClass="Content Bold" runat="server" Text="HDPE Blow - HIC"></asp:Label><br><br></center>
<asp:image id=imgPolyethyleneYear runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_3_1M.png" height="225"></asp:image></TD>
    <TD width="50%">
    <center><asp:Label ID="Label2" CssClass="Content Bold" runat="server" Text="HDPE Blow - HIC"></asp:Label><br><br></center>
<asp:image id=imgPolyethyleneMonth runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_3_1Y.png" height="225"></asp:image></TD></TR>
  <TR>
    <TD style="HEIGHT: 215px" colSpan=2>
<asp:textbox id=txtPolyethylene runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="230px"></asp:textbox></TD>
  <TR></TR>
  <TR>
    <TD style="WIDTH: 183px">
      <P>&nbsp;</P>
      <P><FONT class="Content"><B>Polypropylene</B></FONT></P></TD></TR>
  <TR>
    <TD colSpan=2>
<asp:TextBox id=txtPreTextPP Runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="130px"></asp:TextBox></TD></TR>
  <TR>
    <TD width="50%">
     <center><asp:Label ID="Label1" CssClass="Content Bold" runat="server" Text="HoPP Inj - 35 melt"></asp:Label><br><br></center>                   
<asp:image id=Image1 runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_26_1M.png" height="225"></asp:image></TD>
    <TD width="50%">
     <center><asp:Label ID="Label3" CssClass="Content Bold" runat="server" Text="HoPP Inj - 35 melt"></asp:Label><br><br></center>                   
<asp:image id=Image2 runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_26_1Y.png" height="225"></asp:image></TD>
  <TR>
    <TD style="HEIGHT: 215px" colSpan=2>
<asp:textbox id=txtPolypropylene runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="230px"></asp:textbox></TD></TR>
  <TR>
    <TD style="WIDTH: 183px">
      <P>&nbsp;</P>
      <P><FONT class="Content"><B>Polystyrene</B></FONT></P></TD></TR>
  <TR>
    <TD colSpan=2>
<asp:TextBox id=txtPreTextPS Runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="130px"></asp:TextBox></TD></TR>
  <TR>
    <TD width="50%">
    <center> <asp:Label ID="Label5" CssClass="Content Bold" runat="server" Text="HIPS Inj - 8 melt"></asp:Label><br><br></center>
<asp:image id=Image3 runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_20_1M.png" height="225"></asp:image></TD>
    <TD width="50%">
    <center> <asp:Label ID="Label6" CssClass="Content Bold" runat="server" Text="HIPS Inj - 8 melt"></asp:Label><br><br></center>
<asp:image id=Image4 runat="server" width="315" ImageUrl="/Research/Charts/S_Chart_20_1Y.png" height="225"></asp:image>
  <TR>
    <TD style="HEIGHT: 215px" colSpan=2>
<asp:textbox id=txtPolystyrene CssClass="InputForm" runat="server" Width="100%" TextMode="MultiLine" Height="230px"></asp:textbox></TD></TR></TABLE><BR><BR>
<BR><BR><BR>
<TABLE id=Table3 width="100%" runat="server">
  <TR>
    <TD align=center>
<asp:button id=btnSave CssClass="Content Color2" runat="server" Text="Save" onclick="btnSave_Click"></asp:button>&nbsp; 
<asp:button id=btnPreview CssClass="Content Color2" runat="server" Text="Preview" onclick="btnPreview_Click"></asp:button></TD>
    <TD align=center>
<asp:button id=btnPublish runat="server" CssClass="Content Color2" Text="Publish" DESIGNTIMEDRAGDROP="156" onclick="btnPublish_Click"></asp:button>
<BR></TD></TR></TABLE></asp:panel>
<CENTER><asp:button id=btnClose CssClass="Content Color2" runat="server" Text="OK" Visible="False" onclick="btnClose_Click"></asp:button></CENTER>
<CENTER><asp:button id=btnSure CssClass="Content Color2" runat="server" Text="Yes, I am sure" Visible="False" onclick="btnSure_Click"></asp:button><asp:button id=btnCancel runat="server" CssClass="Content Color2" Text="No, later" Visible="False" onclick="btnCancel_Click"></asp:button></CENTER><br>

</asp:Content>
