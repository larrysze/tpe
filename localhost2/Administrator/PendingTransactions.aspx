<%@ Page language="c#" Codebehind="PendingTransactions.aspx.cs" AutoEventWireup="True" Inherits="PlasticsExchange.PendingTransactions" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<%@ Import namespace="System" %>
<%@ Import namespace="System.Data.SqlClient" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
<div class="DivTitleBarMenu"><span class="Header Color1 Bold">Pending Transactions</span></div>

	<asp:datagrid BackColor="#000000" CellSpacing="1" BorderWidth="0" id="dgPO" runat="server" OnItemDataBound="Bind_Data_ToGrid" AutoGenerateColumns="False" Width="780px" CssClass="DataGrid" ShowFooter="False" PageSize="5">
							<EditItemStyle BackColor="Silver"></EditItemStyle>
							<AlternatingItemStyle CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
							<ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
							<HeaderStyle CssClass="Header LinkNormal OrangeColor"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Credit Application"></asp:TemplateColumn>
								<asp:BoundColumn ItemStyle-HorizontalAlign="Left" DataField="ORDR_PROD_SPOT" HeaderText="Bid/Offer"></asp:BoundColumn>
								<asp:BoundColumn DataField="Product_ID" HeaderText="Bid/Offer Id"></asp:BoundColumn>
								<asp:BoundColumn ItemStyle-HorizontalAlign="Left" DataField="NAME" HeaderText="Seller/Buyer"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Approve Order"></asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
		
</asp:Content>