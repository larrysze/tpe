<%@ Page Language="VB" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>

<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Web.Mail" %>
<%@ import Namespace="system.io" %>
<form runat="server" id="Form">
    <TPE:Template PageTitle="Update Research" Runat="Server" width=780/>

<%
'only adminstrator can access this page.
'IF Session("Typ")<>"A"  THEN Response.Redirect ("../default.aspx")
%>

<%IF Request.Form("PE")&Request.Form("PS")&Request.Form("PP")<>"" THEN


        'the Market Update section at homepages is from Research/date.txt, PS.txt, PP.txt, PE.txt



        'Open a file for writing
        Dim FILENAMEdate as String = Server.MapPath("/Research/date.txt")

        'StreamWriter that can be used to write the file
        Dim objStreamWriter as StreamWriter
        objStreamWriter = File.CreateText(FILENAMEdate)

        'Replace the text inside txt file.
        objStreamWriter.WriteLine(request.form("date"))
        objStreamWriter.Close()

        Dim objStreamWriterPS as StreamWriter
        Dim FILENAMEPS as String = Server.MapPath("/Research/PS.txt")
        objStreamWriterPS = File.CreateText(FILENAMEPS)
        objStreamWriterPS.WriteLine(request.form("PS"))
        objStreamWriterPS.Close()

        Dim objStreamWriterPP as StreamWriter
        Dim FILENAMEPP as String = Server.MapPath("/Research/PP.txt")
        objStreamWriterPP = File.CreateText(FILENAMEPP)
        objStreamWriterPP.WriteLine(request.form("PP"))
        objStreamWriterPP.Close()


        Dim objStreamWriterPE as StreamWriter
        Dim FILENAMEPE as String = Server.MapPath("/Research/PE.txt")
        objStreamWriterPE = File.CreateText(FILENAMEPE)
        objStreamWriterPE.WriteLine(request.form("PE"))
        objStreamWriterPE.Close()
%>


<TPE:Web_Box Heading="Update Successful" Runat="Server" width=780/>
    <!-- Show message if update is successful. -->
    <table border=0 cellspacing=0 cellpadding=0 height=100% width=100% align=center>
	   <tr>
		  <td CLASS=S1><center>Your research has been successfully updated.  Nice work on the research, now SELL SOME MORE PLASTICS!!!</center>
		  </td></tr>
    </table>


    <%ELSE%>

        <!-- three textbox to display the content of PS, PP, PE.txt. -->
        <TPE:Web_Box  Heading="Date"  Runat="Server" width=780/>
            <input type="textbox" name=date value="<!--#INCLUDE FILE="../research/date.txt"-->" >
        
        <BR><BR>
        <TPE:Web_Box Heading="Polyethylene:"  Runat="Server" width=780/>
            <textarea cols=80 rows=15 name=PE ><!--#INCLUDE FILE="../research/PE.txt"--></textarea>
        
        <BR><BR>
        <TPE:Web_Box  Heading="Polypropylene: "  Runat="Server" width=780/>
            <textarea cols=80 rows=15 name=PP ><!--#INCLUDE FILE="../research/PP.txt"--></textarea>
        
        <BR><BR>
        <TPE:Web_Box Heading="Polystyrene:"  Runat="Server" width=780/>
            <textarea cols=80 rows=15 name=PS ><!--#INCLUDE FILE="../research/PS.txt"--></textarea>
        


        <center><input type=submit class=tpebutton value='Update' ><br><br>

<%END IF%>
<TPE:Template Footer="true" Runat="Server" />

</form>
