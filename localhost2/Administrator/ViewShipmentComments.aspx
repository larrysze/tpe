<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewShipmentComments.aspx.cs" Inherits="localhost.Administrator.ViewShipmentComments" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>View Comments</title>
 
    
    
    
    
    <style type="text/css">
    .tablestyle 
{
	font-family: arial;
	font-size: small;
	border: solid 1px #7f7f7f;
}

.altrowstyle 
{
    background-color: #edf5ff;
}

.headerstyle th 
{
    background: url(img/sprite.png) repeat-x 0px 0px;
    border-color: #989898 #cbcbcb #989898 #989898;
    border-style: solid solid solid none;
    border-width: 1px 1px 1px medium;
    color: #000;
    padding: 4px 5px 4px 10px;
    text-align: center;
    vertical-align: bottom;
}  

.headerstyle th a
{
    font-weight: normal;
	text-decoration: none;
	text-align: center;
    color: #000;
	display: block;
    padding-right: 10px;
}    

.rowstyle .sortaltrow, .altrowstyle .sortaltrow 
{
    background-color: #edf5ff;
}

.rowstyle .sortrow, .altrowstyle .sortrow 
{
    background-color: #dbeaff;
}

.rowstyle td, .altrowstyle td 
{
    padding: 4px 10px 4px 10px;
    border-right: solid 1px #cbcbcb;
}

.headerstyle .sortascheader 
{
    background: url(img/sprite.png) repeat-x 0px -100px;
}

.headerstyle .sortascheader a 
{
    background: url(img/dt-arrow-up.png) no-repeat right 50%;
} 

.headerstyle .sortdescheader 
{
    background: url(img/sprite.png) repeat-x 0px -100px;
}   

.headerstyle .sortdescheader a 
{
    background: url(img/dt-arrow-dn.png) no-repeat right 50%;
} 

td.sortedeven
{
    background-color:#EDF5FF;        
}
td.sortedodd
{
    background-color:#DBEAFF;            
}
    
    
    </style>
    
</head>
<body onload="resizeTo(550,600)";>
    <form id="form1" runat="server">
    
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td>
            <input type= button value="Close" onClick="javascript:window.close();">
            <input type="button" value="Print" onClick="window.print()">

        </td>
        </tr>
        
        <tr>
            <td>
            <asp:GridView runat="server" ID="gvComments" AutoGenerateColumns="false" Font-Size="Small" Width="500" CssClass="tablesorter"  >
            <AlternatingRowStyle CssClass="altrowstyle" />
            <HeaderStyle CssClass="headerstyle" />
            <RowStyle CssClass="rowstyle" />
            <Columns>                  
                <asp:BoundField HeaderText="Date" DataField="Comment_Date2" />
                <asp:BoundField HeaderText="Comment" DataField="Comment_Text" />
                <asp:BoundField HeaderText="Person" DataField="Person" />                            
            </Columns>        
        </asp:GridView>
            
            </td>
        
        </tr>

    </table>
    
    </form>
</body>
</html>
