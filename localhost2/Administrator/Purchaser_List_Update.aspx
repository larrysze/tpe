<%
   '*****************************************************************************
   '*   1. File Name       : Administrator\Purchaser_List_Update.aspx           *
   '*   2. Description     : Change the  offer from "Working Orders", it will delete*
   '*			     the old offer and place new offer into database.   *
   '*						                                *
   '*			     IT WON"T UPDATE RECORD, will insert new record     *
   '*		                        	                                *
   '*						                                *
   '*						                                *
   '*   3. Modification Log:                                                    *
   '*     Ver No.       Date          Author             Modification           *
   '*   -----------------------------------------------------------------       *
   '*      1.00       2-24-2004       Xiaoda             Comment                *
   '*                                                                           *
   '*****************************************************************************
%>

<%@ Page Language="VB" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Web.Mail" %>
<!--#INCLUDE FILE="../include/Head.inc"-->
<!--#INCLUDE FILE="../include/BeginMargin.aspx"-->

<!--#INCLUDE FILE="../include/Loading.inc"-->
<DIV id=specs style='position:absolute; width:100%; height:50%; visibility: hidden; overflow: visible;'>

<table border=0 cellspacing=0 cellpading=0 width=100% height=100%><tr><td width=100% height=100%  align=left><left>
<iframe TopMargin=0 RightMargin=0 marginheight=0 border=0 scrolling=no frameborder=0 WIDTH=1020 HEIGHT=530 name=description ></iframe>
</left></td></tr></table>
</DIV>

<%

Dim i, j, Locl, Str0, Str1, k, l, Str2, IdOrder, IdComp, Valid, Pers_Id, Cont_Id, Quantity, Price, Size, Mess, Locl0, Locl1, errMsg, strSql,y
Dim c,Ci,o,Str3,Str,Stra,Strb,ordArray,a,Prce, Mont
Dim StrL1,StrL2,StrL3,StrL4,StrL5,StrL6,StrL7,StrL8,StrL9,StrL10,StrL11,StrL12,StrL13,StrL14,StrL15,StrL16,StrL17,StrL18,StrL19,StrL20,StrL21,StrL22,StrL23,StrL24,StrL25,StrL26,StrL27,StrL28,supp
'Database connection'
Dim conn as SqlConnection
Dim cmdContent as SQLCommand
Dim Rec0 as SqlDataReader
conn = new SqlConnection(Application("DBconn"))
conn.Open()

Dim Rec2 as SqlDataReader
Dim Rec1 as SqlDataReader
Dim RecM as SqlDataReader
Dim RecB as SqlDataReader
Dim RecC as SqlDataReader
Dim connM as SqlConnection
connM = new SqlConnection(Application("DBconn"))
connM.Open()
Dim conn1 as SqlConnection
conn1 = new SqlConnection(Application("DBconn"))
conn1.Open()
Dim conn2 as SqlConnection
conn2 = new SqlConnection(Application("DBconn"))
conn2.Open()

' This part of code is converted from Include/month.inc, 
Dim cmonth,StM0,StM1
cmdContent= new SqlCommand("SELECT FWD_ID,FWDLABL=(FWD_MNTH+' '+RIGHT(CONVERT(varchar,FWD_YEAR),2)), FWD_MNTH, FWD_YEAR FROM FWDMONTH WHERE FWD_ACTV=1 ORDER BY FWD_ID", connM)
RecM= cmdContent.ExecuteReader()
RecM.Read()
IF Request.Form("month")<>"" THEN
	cmonth=Request.Form("month")
ELSEIF Request.Querystring("month")<>"" THEN
	cmonth=Request.Querystring("month")
ELSE
	cmonth=RecM(0)
END IF

StM0=""
StM1=""
RecM.Close()
cmdContent= new SqlCommand("SELECT FWD_ID,FWDLABL=(FWD_MNTH+' '+RIGHT(CONVERT(varchar,FWD_YEAR),2)), FWD_MNTH, FWD_YEAR FROM FWDMONTH WHERE FWD_ACTV=1 ORDER BY FWD_ID", connM)
RecM= cmdContent.ExecuteReader()
While RecM.Read()
        StM0=StM0+"'"&RecM("FWD_ID")&"',"
	StM1=StM1+"'"&RecM("FWDLABL")&"',"

End While
StM0=Left(StM0,Len(StM0)-1)&"),new Array("&Left(StM1,Len(StM1)-1)
RecM.Close

'end of Month.inc
%>


<!--#INCLUDE FILE="../include/Body.inc"-->
<%
IF Session("Typ")<>"A" AND Session("Typ")<>"B" THEN Response.Redirect ("../default.aspx") 
IF Request.Form("Valid")<>"" THEN
	Dim myArray() As String= split(Request.Form("Valid"),",")
	Response.Clear
	
	'Works with multiple offers
	
	FOR c=1 TO UBound(myArray)
		strSql=""
		IdOrder = Request.Form("IdOrder"&(c-1)&"")
		If IdOrder<>"" Then IdOrder = "@Order_Id="+IdOrder.ToString()+", "
		IdComp = Request.Form("Comp"&(c-1)&"")
		If IdComp.ToString()<>"" Then IdComp = "@Comp_Id="+IdComp.ToString()+", "
		Valid = Request.Form("Valid")
		If Valid<>"" Then Valid = "@Valid="+myArray(c).ToString()+", "
		Pers_Id = Request.Form("Pers"&(c-1)&"")
		If Pers_Id.ToString()<>"" Then Pers_Id = "@Pers_Id="+Pers_Id.ToString()+", "
		Cont_Id = Request.Form("Contract"&(c-1)&"")
		If Cont_Id<>"" Then Cont_Id = "@Cont_Id="+Cont_Id.ToString()+", "
		Quantity=Request.Form("Quantity"&(c-1)&"")
		If Quantity<>"" Then Quantity = "@Qty="+Quantity.ToString()+", "
		Price = Request.Form("Price"&(c-1)&"")
		If Price<>"" Then Price = "@Price=0."+Price.ToString()+", "
		Size = Request.Form("Size"&(c-1)&"")
		If Size<>"" Then Size = "@Size="+Size.ToString()+", "
		Mont = Request.Form("month"&(c-1)&"")
		If Mont<>"" Then Mont = "@Month="+Mont.ToString()+", "
		'Locl(0):Locality
		'Locl(1):Place
		Locl=Split(Request.Form("Place"&(c-1)&""),",")
		If Locl(0)<>"" Then Locl0 = "@Locl0="+Locl(0).ToString()+", "
		If Locl(1)<>"" Then Locl1 = "@Locl1="+Locl(1).ToString()+", "
		'Mess = Request.Form("Mess"&(c-1)&"")
		'If Mess<>"" Then Mess = "@Mess='"&Mess&"', "
		errMsg = "@ErrMsg=NULL  "
                strSql=" Exec spTransactionEngine_P "+IdOrder+" "+IdComp+" "+Valid+" "+Pers_Id+" "+Cont_Id+" "+Quantity+" "+Price+" "+Size+" "+Mont+" "+Locl0+" "+Locl1+" "&errMsg
		
		Application.Lock()
		Application("Flag")=1
		cmdContent= new SqlCommand(strSql, conn)
		cmdContent.ExecuteNonQuery()
		Application("Flag")=0
		Application.UnLock()

	NEXT

Response.Redirect("/Common/Common_Working_Orders.aspx?type=*Bid")
END IF

IF Request.QueryString("ORDS")<>"" THEN%>
<script>
cp=0
function L(F,G,H,I,J,K,L,M,N,O,P,Q,B)
{
	StrForm='document.Form'
	F.write('<tr',(cp%2==0)?'>':' bgcolor=#E8E8E8>','<td><img src=/images/1x1.gif width=1 height=21></td><td>&nbsp;&nbsp;<input type=hidden name=IDOrder',cp,' value=',G,'><select class=txtInput name=Quantity',cp,'>',Path.SOStrg(0,20,I),'</select></td><td><select class=txtInput name=Size',cp,'><option value=0>Truckloads<option value=1', (K=='True')?' selected':'','>Rail Cars</select></td><td><input type=hidden name=Contract',cp,' value=',H,' title=',M,'><font size=1>',M,'</font></td><td><select class=txtInput name=month',cp,'>',Path.SOStrg(new Array(<%=StM0%>),B),'</td><td><input type=hidden name=Place',cp,' value=',L,'><font size=1>',P,'</font></td><td>$&nbsp;<b>.</b><input type=text name=Price',cp,' maxlength=3 size=3 value=',J,'></td><td><img src=/images/1x1.gif><input type=hidden name=Comp',cp,' value=',O,'><input type=hidden name=Pers',cp,' value=',N,'></td><td>Bid #',G,' Buyer: ',Q,'</td></tr>')
	cp=cp+1
}
</script>
<SPAN ID=Main name=Main style='position:absolute; x:0px; y:0px; visibility: hidden'>
<form method=post name=Form>
<input type=hidden name=Valid value=''>
<input type=hidden name=Submit value=''>
<table border=0 cellpadding=0 cellspacing=0><br>
	<tr>
		<td colspan=9>
			<table border=0 cellpadding=0 cellspacing=0>
<script>
Path.Menu(new Array('Quantity','Type','Contract','Month','Location','Price'),new Array('','','','',''),new Array('70','100','240','90','140','60'),'<%IF Request.Form("Search")<>"" AND Request.Form("ShowAll")="" THEN Response.write (Request.Form("Search"))%>',document);
</script>
<script>
<%
    ordArray = split(Request.QueryString("ORDS"),",")
    Dim intTempStore
         
         For i = 0 To UBound(ordArray) - 1
         For j = i To UBound(ordArray)
         if ordArray(i) > ordArray(j) Then
         intTempStore = ordArray(i)
         ordArray(i) = ordArray(j)
         ordArray(j) = intTempStore
         End if 
        Next
        Next
Ci=0

  For a=1 TO UBound(ordArray)
  	cmdContent= new SqlCommand("SELECT BID_CONT,BID_QTY,BID_PRCE,BID_SIZE, (CONVERT(varchar,BID_TO_LOCL) + ',' + CONVERT(varchar,BID_TO)),(SELECT CONT_LABL FROM CONTRACT WHERE CONT_ID=BID_CONT),BID_BUYR,(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=BID_BUYR),(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=BID_TO_LOCL),(SELECT PERS_FRST_NAME+' '+PERS_LAST_NAME FROM PERSON WHERE PERS_ID=BID_BUYR),BID_MNTH FROM BID WHERE BID_ID="+ordArray(a).ToString()+"", connM)
	Rec0= cmdContent.ExecuteReader()
  	Rec0.Read()
  	prce=""
	prce=Right(FormatNumber(Rec0(2),3,0,-2,-2),3)
	response.write ("L(document,"+ordArray(a).ToString()+",'"+Rec0(0).ToString()+"',"+Rec0(1).ToString()+","+prce.ToString()+",'"+Rec0(3).ToString()+"','"+Rec0(4).ToString()+"','"+Rec0(5).ToString()+"','"+Rec0(6).ToString()+"','"+Rec0(7).ToString()+"','"+Rec0(8).ToString()+"','"+Rec0(9).ToString()+"','"+Rec0(10).ToString()+"');")
	Rec0.Close()
NEXT%>
</script>
			</table>
		</td>
	</tr>
</table>
<div align=center><input type=button value=OK class=tpebutton onclick="Javascript:Check()"></div>
<%
ELSE
	response.redirect("/Common/Common_Working_Orders.aspx")
END IF %>

<script>
function Check()
{
	mess='Place the following buy orders:<br>'
	with(<%response.write (N1)%>Form)
	{
	document.Form.Valid.value=''
		for(q=0;q<cp;q++) {
		Price=eval('document.Form.Price'+q+'.value')
		Qty=eval('Quantity'+q+'.options[Quantity'+q+'.options.selectedIndex].text')
		if (( Qty!='0' && Price=='') || (Qty=='0' && Price!=''))
		return Path.Mess(window,'For each valid entry row, please enter a quantity and a price.','0')
		for(j=0;j<Price.length;j++)
				if (Price.charAt(j)<"0"||Price.charAt(j)>"9")
					return Path.Mess(window,'Please, enter a valid price.','0')
		Val=eval("parseFloat(Price)/(Math.pow(10,Price.length-3))*Qty")
		Sze=eval('document.Form.Size'+q+'.value')
		Prce=new
 String(Math.round(Sze=='0'?eval("42*100*Val"):eval("190*100*Val"))/100)
		if (Prce!='NaN' && Price!='' && Price!='0' && Qty!='0')
		document.Form.Valid.value=document.Form.Valid.value+','+Prce
		Cont=eval('document.Form.Contract'+q+'.title')

		if (Prce!='NaN' && Price!='' && Price!='0' && Qty!='0')
		mess=mess+Qty+(Sze=='0'?' T':' R')+(eval("Qty>1")?'s ':' ')+' of '+Cont+' for $.'+Price+'/lb<br>'

		}
		return Path.Mess(window,mess,'1')
	}
}
function Answer(Flag)
{
	with(<%response.write (N1)%>Form)
		if(Flag)
		{
			Submit.value='Submit'
			submit()
		}
		else
			Valid.value=''
}
</script>
</form>
</SPAN>
</body></html>
<script>Path.Start(window)</script>