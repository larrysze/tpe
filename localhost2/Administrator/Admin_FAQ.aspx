<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="Admin_FAQ.aspx.cs" Inherits="localhost.Administrator.Admin_FAQ" Title="FAQ Admin" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master"%>
<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master"%>
     <asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphHeading"><div class="Header Bold Color1">FAQ Admin</div></asp:Content>
     <asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
        <br>
        <asp:datagrid id="dg" DataKeyField="id" BorderWidth="0px" BackColor="Black" CellSpacing="1" CssClass="Content LinkNormal" runat="server" Width="100%" AutoGenerateColumns="False">
		<AlternatingItemStyle CssClass="LinkNormal Color2 DarkGray" />
        <ItemStyle CssClass="LinkNormal Color2 LightGray">
        </ItemStyle>

        <HeaderStyle CssClass="LinkNormal Color2 OrangeColor">
        </HeaderStyle>

		<Columns>
			<asp:EditCommandColumn UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
			<asp:ButtonColumn Text="Delete" CommandName="Delete"></asp:ButtonColumn>
            <asp:TemplateColumn HeaderText="Id">
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.id") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox runat="server" CssClass="InputForm" Columns="10" Text='<%# DataBinder.Eval(Container, "DataItem.id") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateColumn>
			<asp:TemplateColumn ItemStyle-HorizontalAlign="left" HeaderText="Question">
				<ItemTemplate>
					<asp:Label ID="lblQuestion" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.question") %>'>
					</asp:Label>
				</ItemTemplate>
				<EditItemTemplate>
					<asp:TextBox ID="txtQuestion" runat="server" CssClass="InputForm" MaxLength="500" Rows="5" TextMode="MultiLine" Columns="50" Text='<%# DataBinder.Eval(Container, "DataItem.question") %>'>
					</asp:TextBox>
				</EditItemTemplate>
			</asp:TemplateColumn>	
						<asp:TemplateColumn ItemStyle-HorizontalAlign="left"  HeaderText="Answer">
				<ItemTemplate>
					<asp:Label ID="lblAnswer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.answer") %>'>
					</asp:Label>
				</ItemTemplate>
				<EditItemTemplate>
					<asp:TextBox ID="txtAnswer" runat="server" CssClass="InputForm" MaxLength="500" Rows="5" TextMode="MultiLine" Columns="50" Text='<%# DataBinder.Eval(Container, "DataItem.answer") %>'>
					</asp:TextBox>
				</EditItemTemplate>
			</asp:TemplateColumn>	
		</Columns>
	    </asp:datagrid>
	    <asp:Button ID="Button1" runat="server" CssClass="Content Color2"
            Text="Add" Height="23px" Width="79px" OnClick="Button1_Click" /><asp:TextBox ID="id" runat="server" CssClass="InputForm" Height="16px" Width="50px"></asp:TextBox><asp:TextBox ID="question" runat="server" CssClass="InputForm" Height="55px" Width="245px"  Rows="5" TextMode="MultiLine" Columns="50"></asp:TextBox><asp:TextBox ID="answer" runat="server" CssClass="InputForm" Height="55px" Width="303px" Rows="5" TextMode="MultiLine" Columns="50"></asp:TextBox>
        <br>
        <asp:Label ID="lblError" ForeColor="Red" runat="server" Text=""></asp:Label>
        <br>
     </asp:Content>