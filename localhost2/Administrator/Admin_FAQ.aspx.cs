using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Data.SqlClient;

namespace localhost.Administrator
{
    public partial class Admin_FAQ : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Master.Width = "780px";
            if (Session["Typ"].ToString() != "A")
            {
                Response.Redirect("/default.aspx");
            }
            if (!IsPostBack)
            {
                Binddg();
            }
        }

        private void Binddg()
        {

            DBLibrary.BindDataGrid(Application["DBconn"].ToString(), dg, "SELECT * from FAQ");

        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.dg.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_CancelCommand_1);
            this.dg.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_EditCommand_1);
            this.dg.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_UpdateCommand_1);
            this.dg.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_DeleteCommand_1);
            this.dg.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_ItemDataBound);
        }
        #endregion

        private void dg_CancelCommand_1(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            dg.EditItemIndex = -1;
            Binddg();
        }

        private void dg_DeleteCommand_1(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            string id = dg.DataKeys[e.Item.ItemIndex].ToString();
           
            string sql = "delete from FAQ where id = @id";

            Hashtable param = new Hashtable();
            param.Add("@id", id);
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sql, param);
            Binddg();
        }

        private void dg_EditCommand_1(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            dg.EditItemIndex = e.Item.ItemIndex;
            Binddg();

        }

        private void dg_UpdateCommand_1(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            //string key = dg.DataKeys[e.Item.ItemIndex].ToString();


            string id = ((TextBox)e.Item.Cells[2].Controls[1]).Text;
            string question = ((TextBox)e.Item.Cells[3].Controls[1]).Text;
            string answer = ((TextBox)e.Item.Cells[4].Controls[1]).Text;

            string key = dg.DataKeys[e.Item.ItemIndex].ToString();

            string sql = "UPDATE FAQ  set id = @id, question = @question, answer = @answer where id = @KEY;";
            Hashtable param = new Hashtable();
            param.Add("@id", id);
            param.Add("@question", question);
            param.Add("@answer", answer);
            param.Add("@KEY", key);
            DBLibrary.ExecuteSqlWithoutScrub(Application["DBconn"].ToString(), sql, param);

            dg.EditItemIndex = -1;
            Binddg();
        }
        
        protected void Button1_Click(object sender, EventArgs e)
        {
            if ((id.Text != "") & (question.Text != "") & (answer.Text != ""))
            {
                Hashtable param = new Hashtable();

                string strSql = "select id FROM FAQ WHERE id = @id";
                param.Add("@id", id.Text);
                param.Add("@question", question.Text);
                param.Add("@answer", answer.Text);
                DataTable dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), strSql, param);

                if (dt.Rows.Count == 0)
                {
                    string strSqlInsert = "INSERT INTO FAQ VALUES (@id, @question, @answer)";
                    DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlInsert, param);
                    Binddg();
                    lblError.Text = "";
                    id.Text = "";
                    question.Text = "";
                    answer.Text = "";
                }
                else lblError.Text = "This index already used"; 
            }
            else lblError.Text = "Please complete all fields"; 

        }
        

        protected void dg_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[1] != null)
                    ((LinkButton)e.Item.Cells[1].Controls[0]).Attributes.Add("onClick", "return confirm('Are you sure you want to delete this definition?');");
            }
        }

    }
}
