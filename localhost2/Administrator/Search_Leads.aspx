<%@ Register TagPrefix="mbdb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.DefaultButtons" %>
<%@ Page Language="C#" CodeBehind="Search_Leads.aspx.cs" AutoEventWireup="true" Inherits="localhost.Administrator.Search_Leads" MasterPageFile="~/MasterPages/Menu.Master" EnableViewState="True" EnableEventValidation="false" Title="Search Leads" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content ID="ContentSearchLeads" runat="server" ContentPlaceHolderID="cphCssLink">
	<style type="text/css">
	table{ align:center }
	.menuskin { BORDER-RIGHT: black 2px solid; BORDER-TOP: black 2px solid; Z-INDEX: 100; VISIBILITY: hidden; FONT: 12px/18px Verdana; BORDER-LEFT: black 2px solid; WIDTH: 165px; BORDER-BOTTOM: black 2px solid; POSITION: absolute; BACKGROUND-COLOR: menu }
	.menuskin A { PADDING-RIGHT: 10px; PADDING-LEFT: 10px; COLOR: black; TEXT-DECORATION: none }
	#mouseoverstyle { BACKGROUND-COLOR: highlight }
	#mouseoverstyle A { COLOR: white }
	</style>
</asp:Content>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
	<table cellspacing="0" cellpadding="0" align="center" border="0" class="Content LinkNormal" width="1200">
		<tr>
			<td>
			<div class="DivTitleBarMenu"><span class="Header Color1 Bold">Search Leads</span></div>
			</td>
		</tr>
		<tr>
		    <td align="center">
			         <table width="100%" align="left">
			<tbody>
                		<tr valign="bottom">
					<td valign="top" align="left"><b> Search within leads:</b>
						<asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
						<asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" /></td>
				</tr>
			</tbody>
	</table>
		    </td>
		</tr>
		<tr>
			<td align="center"><asp:datagrid BorderWidth="0" BackColor="#000000" CellSpacing="1" id="dg" runat="server" Width="85%" CssClass="DataGrid" HorizontalAlign="Center"
					CellPadding="2" ShowFooter="True" AllowSorting="True" DataKeyField="PERS_NAME" OnSortCommand="dg_SortCommand"
					AutoGenerateColumns="False" AllowPaging="false"  OnItemDataBound="dg_ItemDataBound" ItemStyle-HorizontalAlign="Left">
					<AlternatingItemStyle CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
					<ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
					<HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
					<FooterStyle CssClass="Content Color4 Bold FooterColor" />
					<Columns>						
						<asp:BoundColumn SortExpression="PERS_NAME" HeaderText="Name"
						    DataField="PERS_NAME" ItemStyle-CssClass="LinkNormal">
						</asp:BoundColumn>
						<asp:BoundColumn SortExpression="PERS_TITL" HeaderText="Title"
						    DataField="PERS_TITL">
						</asp:BoundColumn>
						<asp:BoundColumn SortExpression="PERS_COMPANY" HeaderText="Company"
						    DataField="PERS_COMPANY" ItemStyle-CssClass="LinkNormal">
						</asp:BoundColumn>
						<asp:BoundColumn SortExpression="PERS_CATEGORY" HeaderText="Corporate Identity"
						    DataField="PERS_CATEGORY">
						</asp:BoundColumn>
						<asp:BoundColumn SortExpression="PERS_PHON" HeaderText="Phone"
						    DataField="PERS_PHON">
						</asp:BoundColumn>
						<asp:BoundColumn SortExpression="PERS_ALLOCATED" HeaderText="Broker"
						    DataField="PERS_ALLOCATED">
						    <HeaderStyle Width="30px"></HeaderStyle>
						</asp:BoundColumn>
					       <asp:BoundColumn HeaderText="CMT" DataField="UserCMT" >                                        
						</asp:BoundColumn> 
					       <asp:BoundColumn HeaderText="Leader" DataField="PERS_ALLOCATED" 
						    SortExpression="PERS_ALLOCATED">
						</asp:BoundColumn> 
					       <asp:BoundColumn HeaderText="Group" DataField="GROUP_NAME" 
						    SortExpression="GROUP_NAME">
						</asp:BoundColumn> 
						<asp:BoundColumn HeaderText="Email" DataField="PERS_MAIL"  
						     SortExpression="PERS_MAIL">
						</asp:BoundColumn>
					</Columns>
				</asp:datagrid><asp:label id="lblMessage" runat="server" Visible="false">There are no transactions listed that match this request!</asp:label>
			</td>
		</tr>
	</table>
</asp:Content>