﻿<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page Language="c#" CodeBehind="Demo_Leads.aspx.cs" AutoEventWireup="false" Inherits="localhost.Administrator.Demo_Leads" %>
<form runat="server" id="Form">
	<TPE:Template PageTitle="Demo Leads" Runat="server" id="Template1" />
	<script language="JavaScript">
<!--
function Resin_Popup(ID)
// This function opens up a pop up window and passes the appropiate ID number for the window
{


if (document.all)
    var xMax = screen.width, yMax = screen.height;
else
    if (document.layers)
        var xMax = window.outerWidth, yMax = window.outerHeight;
    else
        var xMax = 640, yMax=480;

var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
var str = "../common/MB_Resins.aspx?Pref=" + ID;
window.open(str,'Resins','location=true,toolbar=true,width=275,height=500,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');

}


//-->
	</script>
	<asp:Label runat="server" visible="false" id="lblPage" />
	<asp:Label runat="server" visible="false" id="lblSort" />
	<asp:Panel id="pnMain" Visible="true" runat="server">
		<TABLE>
			<asp:Panel id="pnEmail_Offer" runat="server" Visible="false">
				<TBODY>
					<TR>
						<TD>
							<TABLE width="75%" align="center" border="0">
								<TR>
									<TD><IMG src="/pics/icons/alert.gif">
									</TD>
									<TD><STRONG>Items have been selected to email out. Please select which leads should 
											recieve these offers/requests</STRONG>
									</TD>
									<TD>
										<asp:Button id="Button1" onclick="Cancel_Sending_Offers" runat="server" Text="Cancel Sending Items"></asp:Button></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
			</asp:Panel>
			<TR>
				<TD align="left">Filter By Resin:
					<asp:DropDownList id="ddlResinFilter" runat="server" OnSelectedIndexChanged="Resin_Filter" AutoPostBack="True"></asp:DropDownList>By 
					Type:
					<asp:DropDownList id="ddlTypeFilter" runat="server" OnSelectedIndexChanged="Type_Filter" AutoPostBack="True">
						<asp:ListItem text="Show All" Value="A" />
						<asp:ListItem text="Purchasers" Value="P" />
						<asp:ListItem text="Suppliers" Value="S" />
					</asp:DropDownList>
					<asp:Label id="lblAccess" runat="server" Visible="false">By Access</asp:Label>
					<asp:DropDownList id="ddlAccess" runat="server" Visible="false" OnSelectedIndexChanged="Access_Filter"
						AutoPostBack="True"></asp:DropDownList></TD>
				<TD align="right">
					<asp:TextBox id="txtSearch" runat="server" maxsize="40" size="15"></asp:TextBox>
					<asp:Button class="tpebutton" id="Button2" onclick="Click_Search" runat="server" Text="Search"></asp:Button></TD>
			</TR>
			<TR>
			</TR>
			</TBODY></TABLE>
		<FIELDSET style="WIDTH: 980px; HEIGHT: 300px">
			<TABLE cellSpacing="0" cellPadding="0" width="960" border="0">
				<TR>
					<TD align="left">
						<asp:Button class="tpebutton" id="Button3" onclick="Click_Email_Offers" runat="server" Text="Email Offers"></asp:Button>
						<asp:Button class="tpebutton" id="Button4" onclick="Click_Delete" runat="server" Text="Delete"></asp:Button>
						<asp:Button class="tpebutton" id="btnAllocate" onclick="Click_Allocate" runat="server" Text="Allocate"></asp:Button></TD>
					<TD align="right">
						<asp:Button class="tpebutton" id="btnLast_Page" onclick="Last_Page" runat="server" visible="false"
							Text="<< Last Page"></asp:Button>
						<asp:Button class="tpebutton" id="btnNext_Page" onclick="Next_Page" runat="server" visible="false"
							Text="Next Page >>"></asp:Button></TD>
				</TR>
			</TABLE>
			<TABLE cellSpacing="0" cellPadding="0" width="960" border="0">
				<TR>
					<TD>&nbsp;</TD>
					<TD>&nbsp;</TD>
					<TD><STRONG>Prefs</STRONG></TD>
					<TD><STRONG>
							<asp:Linkbutton id="Linkbutton1" onclick="SortName" Runat="server" Text="Name"></asp:Linkbutton></STRONG></TD>
					<TD><STRONG>
							<asp:Linkbutton id="Linkbutton2" onclick="SortTitle" Runat="server" Text="Title"></asp:Linkbutton></STRONG></TD>
					<TD><STRONG>
							<asp:Linkbutton id="Linkbutton3" onclick="SortCompany" Runat="server" Text="Company"></asp:Linkbutton></STRONG></TD>
					<TD><STRONG>Phone</STRONG></TD>
					<TD><STRONG>Email</STRONG></TD>
					<TD><STRONG>
							<asp:Linkbutton id="Linkbutton4" onclick="SortDate" Runat="server" Text="Date"></asp:Linkbutton></STRONG></TD>
					<%
                    // only daboss account see the access column
                     if (Session["Typ"].ToString() == "A") {%>
					<TD><STRONG>
							<asp:Linkbutton id="Linkbutton5" onclick="SortAccess" Runat="server" Text="Access"></asp:Linkbutton></STRONG></TD>
					<% }%>
				</TR>
				<asp:PlaceHolder id="phContent" runat="server"></asp:PlaceHolder></TABLE>
			<TABLE cellSpacing="0" cellPadding="0" width="960" border="0">
				<TR>
					<TD align="left">
						<asp:Button class="tpebutton" id="Button5" onclick="Click_Email_Offers" runat="server" Text="Email Offers"></asp:Button>
						<asp:Button class="tpebutton" id="Button6" onclick="Click_Delete" runat="server" Text="Delete"></asp:Button>
						<asp:Button class="tpebutton" id="Button7" onclick="Click_Allocate" runat="server" Text="Allocate"></asp:Button></TD>
					<TD align="right">
						<asp:Button class="tpebutton" id="btnLast_Page2" onclick="Last_Page" runat="server" visible="false"
							Text="<< Last Page"></asp:Button>
						<asp:Button class="tpebutton" id="btnNext_Page2" onclick="Next_Page" runat="server" visible="false"
							Text="Next Page >>"></asp:Button></TD>
				</TR>
			</TABLE>
		</FIELDSET>
	</asp:Panel>
	<asp:Panel id="pnAllocate" runat="server" visible="false">
<FIELDSET style="WIDTH: 300px">
			<STRONG>1) Allocate To:</STRONG>
			<asp:DropDownList id="ddlAllocate" runat="server"></asp:DropDownList>
			<BR>
			<BR>
			<STRONG>2) Select Users to send email:</STRONG>
			<TABLE>
				<TR>
					<TD>Name</TD>
					<TD align="middle">Email User</TD>
					<TD align="middle">BCC Broker</TD>
				</TR>
				<asp:PlaceHolder id="phAllocate" runat="server"></asp:PlaceHolder>
				<TR>
					<TD colSpan="3">
						<asp:Button class="tpebutton" id="Button8" onclick="Submit_Allocate" runat="server" Text="Allocate"></asp:Button>
						<asp:Button class="tpebutton" id="Button9" onclick="Cancel_Allocate" runat="server" Text="Cancel"></asp:Button></TD>
				</TR>
			</TABLE>
		</FIELDSET> 
		
<BR><BR><BR><BR>Thank you for registering with The Plastics Exchange. We are all 
about resin and we know…price matters! <BR><BR>We are interested in working with 
you and hope you will find The Plastics Exchange to be your one stop for 
research, trading tools and your resin needs. <BR><BR>If you have any questions, 
please feel free to contact _______ at 1.800.851.7303 or reply to this email. 
<BR><BR>Our services include: 
<UL>
			<LI>
			Custom forward pricing analysis.
			<LI>
			Spot market research and news.
			<LI>
			Access to prime and wide spec resin.
			<LI>
				Domestic and international resin.
			</LI>
		</UL><BR><BR>You will be receiving 
our market research on a regular basis, but if you would also like to receive 
information regarding our other tools, including forward pricing and spot 
prices, please click here for our full registration and access to the exchange. 
<BR><BR>Sincerely, <BR><BR>Michael A. Greenberg, CEO <BR>The Plastics Exchange 
<BR>Tel: 312.202.0002 <BR>Fax: 312.202.0174
        </asp:Panel>
	<TPE:Template Footer="true" Runat="server" id="Template2" />
</form>
