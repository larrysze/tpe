using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using localhost;
using TPE.Utility;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Contact_Comment.
	/// </summary>
	public partial class Contact_Details : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.RadioButton rbtnGreat;
		protected System.Web.UI.WebControls.RadioButton rbtnFair;
		protected System.Web.UI.WebControls.RadioButton rbtnPoor;
		protected System.Web.UI.WebControls.RadioButton rbtnMessage;
		protected System.Web.UI.WebControls.RadioButton rbtnEmail;
		protected System.Web.UI.WebControls.Label lblGradeID;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			HelperFunction.InsertConfirmationCode(this);
			if (Request.UserHostAddress.ToString() !=ConfigurationSettings.AppSettings["OfficeIP"].ToString())
			{
                if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "T") && ((string)Session["Typ"] != "L"))
				{
					Response.Redirect("/default.aspx");
				}
			}
			ViewState["ID"] = Request.QueryString["ID"].ToString();
			hlEditResins.NavigateUrl = "/administrator/Update_Resins.aspx?ID="+ViewState["ID"].ToString();
			if (!IsPostBack)
			{
				// defaults to the logged in user
				if (Session["ID"] != null)
				{
					ViewState["BrokerID"] = Session["ID"].ToString();
				}
				else
				{
					ViewState["BrokerID"] = "-1";
				}
				Bind();
			}
		}
		
		private void Bind()
		{
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
		
				BindNotes();

				using (SqlDataReader dtrPerson = DBLibrary.GetDataReaderFromSelect(conn,"SELECT *,(SELECT PERS_FRST_NAME FROM PERSON B WHERE B.PERS_ID = P.PERS_ACES ) AS BROKER FROM PERSON P WHERE PERS_ID = '"+ViewState["ID"]+"'"))
				{
					if (dtrPerson.Read())
					{
						if (dtrPerson["PERS_ACES"].ToString() !="-1") // overwrite the BrokerID with a allocated broker
						{
							ViewState["BrokerID"] = dtrPerson["PERS_ACES"].ToString() ;
						}
						// adds ports 
						using (SqlConnection conn2 = new SqlConnection(Application["DBconn"].ToString()))
						{
							conn2.Open();
							using (SqlDataReader dtrPort = DBLibrary.GetDataReaderFromSelect(conn2,"select PORT_ID, PORT= PORT_CITY +', ' + (SELECT CTRY_NAME FROM COUNTRY WHERE CTRY_CODE=PORT_COUNTRY)from INTERNATIONAL_PORT ORDER BY PORT"))
							{
							
								// build the port drop down
								ddlPort.Items.Clear();
								ddlPort.Items.Add(new ListItem ("Select Port","*"));		
								ListItem lstPort;
								while (dtrPort.Read())
								{
									lstPort = new ListItem(dtrPort["PORT"].ToString(),dtrPort["PORT_ID"].ToString());
									if (dtrPort["PORT_ID"].ToString() ==dtrPerson["PERS_PORT"].ToString() )
									{
										lstPort.Selected = true;
									}
									ddlPort.Items.Add(lstPort);
								}														
							}
						}						
						// finish adding ports

						// add the broker lable 
						lblBroker.Text = getSafeString(dtrPerson["BROKER"]);
						txtFirstName.Text = getSafeString(dtrPerson["PERS_FRST_NAME"]);
						txtLastName.Text = getSafeString(dtrPerson["PERS_LAST_NAME"]);
						txtTitle.Text = getSafeString(dtrPerson["PERS_TITL"]);
						if (!(getSafeString(dtrPerson["PERS_TYPE"])=="D"))
						{
							// exchange users (defined by PERS_TYPE ="D")
							txtCompanyName.Visible = false;
							lblCompanyName.Visible = false;
						}
						else
						{
							txtCompanyName.Text = getSafeString(dtrPerson["COMP_NAME"]);
						}
				
						txtPhone.Text = getSafeString(dtrPerson["PERS_PHON"]);
						txtEmail.Text = getSafeString(dtrPerson["PERS_MAIL"]);
						lblOldEmail.Text = getSafeString(dtrPerson["PERS_MAIL"]);
						if (dtrPerson["PERS_LOGN"] == null)
						{
							// user are based on emails addresses and not usernames
							// this is only displayed for legacy users 
							lblUsername.Visible = false;
							lblUsernameLable.Visible = false;
						}
						else
						{
							lblUsername.Text = getSafeString(dtrPerson["PERS_LOGN"]).ToString();
						}
				
						txtPassword.Text = Crypto.Decrypt(getSafeString(dtrPerson["PERS_PSWD"]));
						lblLastLogin.Text = getSafeString(dtrPerson["PERS_LAST_CHKD"]);
						lblDateRegistered.Text = getSafeString(dtrPerson["PERS_DATE"]);
						lblTotalLogin.Text = getSafeString(dtrPerson["PERS_NUMBER_LOGINS"]);

						if (dtrPerson["EMAIL_ENBL"]==null)
							cbEmailSubscriber.Checked = false;
						else
							if (Convert.ToBoolean(dtrPerson["EMAIL_ENBL"]))
							cbEmailSubscriber.Checked = true;
				
						if (getSafeString(dtrPerson["PERS_PRMLY_INTRST"]) == "2")
							rdbOutsideUS.Checked = true;
						else
							rdbInsideUS.Checked = true;
				
						string strSize = getSafeString(dtrPerson["PERS_INTRST_SIZE"]).Trim();
						string strQuality = getSafeString(dtrPerson["PERS_INTRST_QUALT"]).Trim();
				
						if (strSize.Length==3)
						{
							chkSize1.Checked = strSize[0].Equals('1')? true:false;
							chkSize2.Checked = strSize[1].Equals('1')? true:false;
							chkSize3.Checked = strSize[2].Equals('1')? true:false;
						}
						else
						{
							chkSize1.Checked = false;
							chkSize2.Checked = false;
							chkSize3.Checked = false;
						}

						if (strQuality.Length==3)
						{
							chkQuality1.Checked = strQuality[0].Equals('1')? true:false;
							chkQuality2.Checked = strQuality[1].Equals('1')? true:false;
							chkQuality3.Checked = strQuality[2].Equals('1')? true:false;
						}
						else
						{
							chkQuality1.Checked = false;
							chkQuality2.Checked = false;
							chkQuality3.Checked = false;
						}
					}

					// sets up the ddl the the persons 'status'.  this individualy adds the list items and set them selected 
					// when they are = to the already stored value
					ddlGrade.Items.Clear();
					ListItem lst;
					lst = new ListItem ("Great","1" );
					if (getSafeString(dtrPerson["PERS_STAT"])=="1")
					{
						lst.Selected = true;
					}
					ddlGrade.Items.Add (lst );

					lst = new ListItem ("Fair","2" );
					if (getSafeString(dtrPerson["PERS_STAT"])=="2")
					{
						lst.Selected = true;
					}
					ddlGrade.Items.Add (lst );

					lst = new ListItem ("Unlikely","3" );
					if (getSafeString(dtrPerson["PERS_STAT"])=="3")
					{
						lst.Selected = true;
					}
					ddlGrade.Items.Add (lst );

					lst = new ListItem ("Message","4" );
					if (getSafeString(dtrPerson["PERS_STAT"])=="4")
					{
						lst.Selected = true;
					}
					ddlGrade.Items.Add (lst );
					// List count of Resins
					lblResinCount.Text = HelperFunction.CountSelectedResins(this.Context,Convert.ToInt32(dtrPerson["PERS_PREF"]));

					// defins the cmdResin before dtrPerson 
					using (SqlConnection conn3 = new SqlConnection(Application["DBconn"].ToString()))
					{
						using (SqlDataReader dtrResin = DBLibrary.GetDataReaderFromSelect(conn3,"SELECT CONT_LABL,CONT_ID, CONT_GRADE FROM CONTRACT WHERE POWER(2,CONT_ID-1)&("+getSafeString(dtrPerson["PERS_PREF"])+")<>0 ORDER BY CONT_ORDR"))
						{					
							rptResins.DataSource = dtrResin;
							rptResins.DataBind();
						}
					}
				}				

				//Calendar1.SelectedDate.Month ="11";
				//Calendar1.SelectedDate.Year ="2005";
				//Calendar1.SelectedDate.Day ="20";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_ItemCommand);
			this.rptResins.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptResins_ItemDataBound);

		}
		#endregion
		/// <summary>
		///	Event that fires when submit is clicked.
		/// Saves task info in db
		/// </summary>
		private void btnSubmit_Click(object sender, System.EventArgs e)
		{
			string strSQL;
			string test = ViewState["BrokerID"].ToString();
			
			//strSQL = "INSERT INTO TASK_LIST (TASK_PRIORITY,TASK_DESCRIPTION,TASK_PERSON,TASK_DUE_DATE,TASK_LEAD_ID) 
			//VALUES ('2','"+HelperFunction.RemoveSqlEscapeChars(txtBody.Text)+"','"+ViewState["BrokerID"].ToString()+"','"+Calendar1.SelectedDate.ToString()+"','"+ViewState["ID"].ToString()+"');";
			strSQL = "INSERT INTO TASK_LIST (TASK_PRIORITY,TASK_DESCRIPTION,TASK_PERSON,TASK_DUE_DATE,TASK_LEAD_ID) VALUES ('2',@Body,@BrokerID,@Date,@ID);";
			Hashtable htParams = new Hashtable();
			htParams.Add("@Body",HelperFunction.RemoveSqlEscapeChars(txtBody.Text));
			htParams.Add("@BrokerID",ViewState["BrokerID"].ToString());
			htParams.Add("@Date",Calendar1.SelectedDate.ToString());
			htParams.Add("@ID",ViewState["ID"].ToString());
			
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),strSQL,htParams);				
			
			Bind();
		}

		protected void btnAddNote_Click(object sender, System.EventArgs e)
		{
			string strSQL="";

			if (txtComments.Text !="")
			{
				strSQL = "INSERT INTO COMMENT (CMNT_PERS,CMNT_TEXT,CMNT_DATE) VALUES (@ID,@Comments,getDate());";
			}
			strSQL += "UPDATE PERSON set PERS_STAT=@PERS_STAT WHERE PERS_ID=@ID";
			Hashtable htParams = new Hashtable();
			htParams.Add("@ID",ViewState["ID"]);
			htParams.Add("@Comments",HelperFunction.RemoveSqlEscapeChars(txtComments.Text));
			htParams.Add("@PERS_STAT",ddlGrade.SelectedValue.ToString());
			
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSQL, htParams);
						
			Bind();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("/administrator/CRM.aspx");
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			// insert updated code 
			string strSQL;
			string strAllowEmail;
			bool bValidEmail=true;
			
			//Checking if the e-mail address was changed
			if (lblOldEmail.Text.ToLower()!=txtEmail.Text.ToLower())
			{
				//Checking if the e-mail already exists in the Database
				if(HelperFunction.ExistEmail(this.Context, txtEmail.Text))
				{
					bValidEmail = false;
					lblMessage.Text = "E-mail already in use!";
					lblMessage.Visible = true;
				}
			}

			if (bValidEmail)
			{
				lblMessage.Text = "";
				lblMessage.Visible = false;

				if (cbEmailSubscriber.Checked)
				{
					strAllowEmail ="1";
				}
				else
				{
					strAllowEmail ="0";
				}
				
				string strSQL2 = "";
				strSQL2 += ", PERS_PRMLY_INTRST = " + (rdbInsideUS.Checked ? "'1'": "'2'");
				strSQL2 += ", PERS_INTRST_SIZE = " + "'" + Bit(chkSize1.Checked)+ Bit(chkSize2.Checked)+ Bit(chkSize3.Checked) + "'";
				strSQL2 += ", PERS_INTRST_QUALT = " + "'" + Bit(chkQuality1.Checked)+ Bit(chkQuality2.Checked)+ Bit(chkQuality3.Checked) + "'";
				if (txtZip.Text != "")
				{
					strSQL2 += ", PERS_ZIP='"+txtZip.Text+"' ";
					
				}
				else
				{
					strSQL2 += ", PERS_ZIP=null ";
				}
				if (ddlPort.SelectedValue.ToString() !="*")
				{
					strSQL2 += ", PERS_PORT='"+ddlPort.SelectedValue.ToString()+"' ";
				}
				else
				{
					strSQL2 += ", PERS_PORT=null ";
				}

				strSQL = "UPDATE PERSON set PERS_FRST_NAME=@FirstName, PERS_LAST_NAME=@LastName, PERS_TITL=@Title,COMP_NAME=@CompanyName,PERS_PHON=@Phone,PERS_PSWD=@Password,EMAIL_ENBL=@AllowEmail, PERS_MAIL = @Email" + strSQL2 + " WHERE PERS_ID =@ID";
				Hashtable htParams = new Hashtable();
				htParams.Add("@FirstName",HelperFunction.RemoveSqlEscapeChars(txtFirstName.Text.ToString()));
				htParams.Add("@LastName",HelperFunction.RemoveSqlEscapeChars(txtLastName.Text.ToString()));
				htParams.Add("@Title",HelperFunction.RemoveSqlEscapeChars(txtTitle.Text.ToString()));
				htParams.Add("@CompanyName",HelperFunction.RemoveSqlEscapeChars(txtCompanyName.Text.ToString()));
				htParams.Add("@Phone",HelperFunction.RemoveSqlEscapeChars(txtPhone.Text.ToString()));
                htParams.Add("@Password",HelperFunction.RemoveSqlEscapeChars(Crypto.Encrypt(txtPassword.Text)));
				htParams.Add("@AllowEmail",strAllowEmail);
				htParams.Add("@ID",ViewState["ID"].ToString());
				htParams.Add("@Email",HelperFunction.RemoveSqlEscapeChars(txtEmail.Text));

				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),strSQL, htParams);
				Bind();
			}
		}
		
		protected void db_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				if ((e.Item.Cells[3].Text.ToString()!="&nbsp;") && (e.Item.Cells[3].Text.ToString()!=""))
				{
					e.Item.Cells[2].Text = "<a href=javascript:sealWin=window.open('./EmailSystem.aspx?ID="+e.Item.Cells[3].Text.ToString()+ "&DATE=" + e.Item.Cells[1].Text.ToString() + "','win','toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=1000,height=800');void(0)>" + 
						e.Item.Cells[2].Text+"</a>";
				}
			}
		}
		

		protected void MyCalendar_DayRender (object sender, System.Web.UI.WebControls.DayRenderEventArgs e) 
		{

			/*
			// define a new Style for the current date
			Style todayStyle = new Style();
			todayStyle.BackColor = System.Drawing.Color.Gray;
			todayStyle.ForeColor = System.Drawing.Color.White;
		

			if (e.Day.IsToday) 
			{
				// apply the todayStyle style to the current style dynamically
				e.Cell.ApplyStyle(todayStyle);
			} 
			
			*/
		} 

		private string getSafeString(object objString)
		{
			string ret = "";
			try
			{	
				if (objString!=null)
				{
					ret = objString.ToString();
				}
			}
			finally
			{
				//do nothing
			}
			
			return ret;
		}

		private string Bit(bool condition)
		{
			if (condition)
				return "1";
			else
				return "0";
		}

		private void BindNotes()
		{
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();

				using (SqlDataAdapter dadContent = new SqlDataAdapter("SELECT CMNT_ID, CMNT_TEXT,CMNT_DATE, CMNT_EMAIL_SYSTEM_ID FROM COMMENT WHERE CMNT_PERS ='"+ViewState["ID"].ToString()+"' ORDER BY CMNT_DATE DESC",conn))
				{
					DataSet dstContent = new DataSet();
					dadContent.Fill(dstContent);
					dg.DataSource = dstContent;
					dg.DataBind();
				}
			}			
		}

		private void dg_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.CommandName == "Delete")
			{
				Hashtable htParams = new Hashtable();
				htParams.Add("@ID",e.Item.Cells[4].Text);
				TPE.Utility.DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), "DELETE FROM COMMENT WHERE CMNT_ID = @ID",htParams);
				BindNotes();
			}
		}

		protected void dg_PreRender(object sender, System.EventArgs e)
		{
			foreach(DataGridItem item in dg.Items)
			{
				HelperFunction.InsertControlConfirmationCode((WebControl)item.Cells[0].Controls[0],"Are you sure you want to delete this note?");
			}
		}

		private void rptResins_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			((CheckBox)e.Item.FindControl("CheckBoxResin")).Text = DataBinder.Eval(e.Item.DataItem, "CONT_LABL").ToString() + "<br>";
			((Label)e.Item.FindControl("lblGradeID")).Text = System.Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CONT_GRADE"));
		}

		protected void btnSendEmail_Click(object sender, System.EventArgs e)
		{
			string strEmailGrades = "";
			for(int i=0;i<rptResins.Items.Count;i++)
			{
				if (((CheckBox)rptResins.Items[i].FindControl("CheckBoxResin")).Checked)
				{
					strEmailGrades += ((Label)rptResins.Items[i].FindControl("lblGradeID")).Text + ",";
				}
			}

			if (strEmailGrades != "")
			{
				strEmailGrades = strEmailGrades.Substring(0, strEmailGrades.Length-1);
				Session["strEmailOffer_Address"] = ViewState["ID"].ToString();
				Session["strEmailGrades"] = strEmailGrades;
//				Session["bEmailToAllMyLeads"] = "false";
				Response.Redirect ("/Administrator/SendEmail.aspx");
			}
		}
	}
}