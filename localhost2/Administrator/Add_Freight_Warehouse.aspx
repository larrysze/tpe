<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add_Freight_Warehouse.aspx.cs" Inherits="localhost.Administrator.WebForm3" 
MaintainScrollPositionOnPostback="true" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/TemplateWithoutInstructions.Master" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphHeading">
    <span class="Header Bold Color1">Add Freight/Warehouse Company</span></asp:Content>
   <asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphMain">
    <table cellpadding="2" width="100%" border="0">
        <tr align="center">
            <td style="height: 28px">
                <asp:Label CssClass="Content Color2" ID="LabelCompany" runat="server" Text="Shipment Company Name:"></asp:Label>&nbsp;
                <asp:TextBox ID="TextBoxCompany" runat="server"></asp:TextBox></td>
            <td style="height: 28px">
                <asp:Label CssClass="Content Color2" ID="LabelShipmentType" runat="server" Text="Shipment Type:"></asp:Label>
                <asp:DropDownList CssClass="InputForm" ID="ListShipmentType" runat="server" AutoPostBack="True" >
                    <asp:ListItem>Freight</asp:ListItem> 
                    <asp:ListItem>Warehouse</asp:ListItem>
                    <asp:ListItem>Freight and WareHouse</asp:ListItem>  
                </asp:DropDownList>
            </td>
            <td width="10" style="height: 28px">
                &nbsp;
            </td>
            <td style="height: 28px">
                <asp:Button CssClass="Content Color2" ID="btnAddNew" runat="server" Text="Add New Item" OnClick="btnAddNew_Click"
                    ></asp:Button></td>
        </tr>
    </table> 
       <asp:Label ID="Label1" runat="server" Height="15px" Text="" Width="700px" Font-Size="Small"></asp:Label>
       
       <asp:DataGrid BackColor="#000000" HorizontalAlign="Center" CellSpacing="1" CellPadding="2" BorderWidth="0"
         ID="dg" runat="server" PageSize="50" CssClass="normalContent" AutoGenerateColumns="False"
         DataKeyField="SHIPMENT_COMP_ID" AllowSorting="True" AllowPaging="True" ShowFooter="false">
            <PagerStyle CssClass="LinkNormal LightGray" Mode="NumericPages"></PagerStyle>
            <AlternatingItemStyle HorizontalAlign="Justify" CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
            <ItemStyle HorizontalAlign="Justify" CssClass="LinkNormal LightGray"></ItemStyle>
            <HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
            <Columns>
                <asp:EditCommandColumn CancelText="Cancel" UpdateText="Update" EditText="Edit"><HeaderStyle Width="40px"></HeaderStyle></asp:EditCommandColumn> 
                <asp:BoundColumn DataField="SHIPMENT_COMP_ID" HeaderText="ID" ReadOnly="True" SortExpression="SHIPMENT_COMP_ID">
                     <HeaderStyle Width="40px"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="SHIPMENT_COMP_NAME" HeaderText="Shipment Company Name" SortExpression="SHIPMENT_COMP_NAME" >
                    <HeaderStyle Width="240px"></HeaderStyle>
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Shipment Type" SortExpression="SHIPMENT_COMP_TYPE">
                    <ItemTemplate>
                        <asp:Label CssClass="Content Color2" ID="ddlType" runat="server" Text='<%# ConvertStatus(DataBinder.Eval(Container, "DataItem.SHIPMENT_COMP_TYPE")) %>'>
                        </asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="50px"></HeaderStyle>
                    <EditItemTemplate>
                        <asp:DropDownList CssClass="InputForm" ID="ddlType" runat="server" SelectedValue='<%# ConvertStatus(DataBinder.Eval(Container, "DataItem.SHIPMENT_COMP_TYPE")) %>'>
                            <asp:ListItem Value="Freight">Freight</asp:ListItem>
                            <asp:ListItem Value="Warehouse">Warehouse</asp:ListItem>
                            <asp:ListItem Value="Freight and Warehouse">Freight and Warehouse</asp:ListItem>
                        </asp:DropDownList>
                    </EditItemTemplate>
                </asp:TemplateColumn>      
                          
              <%--  <asp:ButtonColumn ButtonType="PushButton" HeaderText="Delete" CommandName="Del"  DataTextField="SHIPMENT_COMP_ID"  DataTextFormatString="{0:D4}">          
                </asp:ButtonColumn>--%>
            </Columns>
      </asp:DataGrid>
   
   </asp:Content> 