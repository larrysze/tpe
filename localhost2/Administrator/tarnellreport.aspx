
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tarnellreport.aspx.cs" Inherits="localhost.Administrator.tarnellreport"  MasterPageFile="~/MasterPages/menu.master" Title="Tarnell Report" %>

<%@ MasterType VirtualPath="~/MasterPages/Menu.master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphMain">
   <br />
   <asp:Label runat="server" CssClass="Content" ID="lblDate" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="BtnExport" CssClass="Content" runat="server" OnClick="BtnExport_Click"   Text="Export to Excel" />
    <br />
   
   <asp:GridView ID="gvReport" Font-Size="Small"  runat="server" HeaderStyle-CssClass="Content LinkNormal" RowStyle-CssClass="Content LinkNormal" AutoGenerateColumns="False">  
    <columns>
        <asp:boundfield datafield="ID" HeaderText="Cust#" />
        <asp:boundfield datafield="COMP_NAME" HeaderText="Company name" />
        <asp:boundfield datafield="Address" HeaderText="Address" />
        <asp:boundfield datafield="City" HeaderText="City" />
        <asp:boundfield datafield="State" HeaderText="State" />
        <asp:boundfield datafield="ZIP" HeaderText="ZIP" />
        <asp:boundfield datafield="CreditLimit" HeaderText="CreditLimit" HtmlEncode="false" DataFormatString="{0:##,###}" />
        <asp:boundfield datafield="Outstanding" HeaderText="Outstanding" HtmlEncode="false" DataFormatString="{0:##,###}" />
        <asp:boundfield datafield="maxCurrentShipment" HeaderText="Current" HtmlEncode="false"  DataFormatString="{0:##,###}" />
        <asp:boundfield datafield="maxInventoryShipment" HeaderText="Commited" HtmlEncode="false"  DataFormatString="{0:##,###}" />
        <asp:boundfield datafield="from0to30Days" HeaderText="0-30" HtmlEncode="false"  DataFormatString="{0:##,###}" />
        <asp:boundfield datafield="from30to60Days" HeaderText="30-60" HtmlEncode="false" DataFormatString="{0:##,###}" />
        <asp:boundfield datafield="from60to90Days" HeaderText="60-90" HtmlEncode="false" DataFormatString="{0:##,###}" />
        <asp:boundfield datafield="moreThen90Days" HeaderText=">90" HtmlEncode="false" DataFormatString="{0:##,###}" />
        <asp:boundfield datafield="LastSale" HeaderText="Date of Last Sale"   />
        <asp:boundfield datafield="Term" HeaderText="Term" HtmlEncode="false" DataFormatString="{0:##,###}" />
        <asp:boundfield datafield="DaysToPayment" HeaderText="Pay Days"  />
    </columns>
       <RowStyle CssClass="Content LinkNormal" />
       <HeaderStyle CssClass="Content LinkNormal" />
    </asp:GridView>
              
    <br />
</asp:Content>