using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Edit_Transaction.
	/// </summary>
	public class Edit_Transaction : System.Web.UI.Page
	{
		/************************************************************************
	 *   1. File Name       :   Edit_Transaction.aspx                       *
	 *   2. Description     :   Allows Admins to edit details of the        *
	 *   4. Modification Log:                                               *
	 *                                                                      *
	 *     Ver No.       Date          Author             Modification      *
	 *    ----------------------------------------------------------------  *
	 *      1.00      10-24-2003       Zach               First Baseline    *
	 ************************************************************************/
		string location;
		SqlConnection conn;
		string strMarket;// contains the table name of the market being accessed
		string strId;// the id that is initially passed in the querystring
		string isspot;
		string prodname;
		protected System.Web.UI.WebControls.ListBox lbComment;
		protected System.Web.UI.WebControls.Label lbltest;
		protected System.Web.UI.WebControls.Label lbltest2;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.RequiredFieldValidator rfvComment;
		protected System.Web.UI.WebControls.DropDownList ddlBuyerComp;
		protected System.Web.UI.WebControls.DropDownList ddlBuyerName;
		protected System.Web.UI.WebControls.DropDownList ddlBuyerLocation;
		protected System.Web.UI.WebControls.RangeValidator RangeValidator1;
		protected System.Web.UI.WebControls.CompareValidator CompareValidator1;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
		protected System.Web.UI.WebControls.TextBox txtBuyerTerms;
		protected System.Web.UI.WebControls.TextBox txtComment;
		protected System.Web.UI.WebControls.RangeValidator RangeValidator2;
		protected System.Web.UI.WebControls.CompareValidator CompareValidator2;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;
		protected System.Web.UI.WebControls.TextBox txtPrice;
		protected System.Web.UI.WebControls.DropDownList ddlSellerComp;
		protected System.Web.UI.WebControls.DropDownList ddlSellerName;
		protected System.Web.UI.WebControls.DropDownList ddlSellerLocation;
		protected System.Web.UI.WebControls.RangeValidator RangeValidator3;
		protected System.Web.UI.WebControls.CompareValidator CompareValidator3;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator3;
		protected System.Web.UI.WebControls.TextBox txtSellerTerms;
		protected System.Web.UI.WebControls.RangeValidator RangeValidator4;
		protected System.Web.UI.WebControls.CompareValidator CompareValidator4;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator4;
		protected System.Web.UI.WebControls.TextBox txtSPrice;
		protected System.Web.UI.WebControls.Label lblUpdated;
		protected System.Web.UI.WebControls.CompareValidator CompareValidator5;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator5;
		protected System.Web.UI.WebControls.TextBox txtQty;
		protected System.Web.UI.WebControls.DropDownList ddlSize;
		protected System.Web.UI.WebControls.TextBox txtProduct;
		protected System.Web.UI.WebControls.Label lblOrderID;
		protected System.Web.UI.WebControls.TextBox txtMELT;
		protected System.Web.UI.WebControls.TextBox txtDENS;
		protected System.Web.UI.WebControls.TextBox txtADDS;
		protected System.Web.UI.WebControls.TextBox txtPO;
		protected System.Web.UI.WebControls.DropDownList ddlShipTerms;
		protected System.Web.UI.WebControls.TextBox txtDate;
		protected System.Web.UI.WebControls.TextBox txtCommission;
		protected System.Web.UI.WebControls.Label LblMarkup;
		protected System.Web.UI.WebControls.CompareValidator CompareValidator6;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator6;
		protected System.Web.UI.WebControls.TextBox txtFreight;
		protected System.Web.UI.WebControls.TextBox txtWeight;
		protected System.Web.UI.WebControls.Panel pnlWGHT;
		protected System.Web.UI.WebControls.Button btnUpdate;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label LblRailCarNbr;
		protected System.Web.UI.WebControls.Label LblLoadStatus;
		protected System.Web.UI.WebControls.DropDownList ddlBrokers;
		protected System.Web.UI.WebControls.DataGrid dgComments;
		protected System.Web.UI.WebControls.Label Label1;
		string prodid;

		public void Page_Load(object sender, EventArgs e)
		{
			// kick user out if they aren't allowed
			if (((string)Session["Typ"] != "A") )
			{
				Response.Redirect("/default.aspx");
			}
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			
		//begin - binding broker`s commission drop-down list
		//ddlBrokers.DataSource = dtrBrok;
		//ddlBrokers.DataTextField= "PERS_LAST_NAME"; 
		//ddlBrokers.DataValueField= "PERS_FRST_NAME";
		//ddlBrokers.DataBind();
		//ddlBrokers.Items.Add("Other");
		//btrBrok.Closed();
		//conn.Close();
		//end - binding broker`s commission drop-down list

			// Get SHIPMENT ID
			string[] arrID;
			Regex r = new Regex("-"); // Split on .
			arrID = r.Split(Request.QueryString["Id"].ToString()) ;
			SqlCommand cmdShipmentID = new SqlCommand("Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID = '"+arrID[0].ToString()+"' and SHIPMENT_SKU ='"+arrID[1].ToString()+"'",conn);
			ViewState["ShipmentID"]= cmdShipmentID.ExecuteScalar(); 
			strId = arrID[0].ToString();
			strMarket="ORDERS";
    
			//strId = (string)Request.QueryString["Id"];
			//if (strId.LastIndexOf("-") != -1)
			//{
			//	// if the ORDR_SKU number is passed, trim the passed ORDR_SKU number from the ORDR_ID
			//	strId = strId.Substring(0,strId.LastIndexOf("-"));
			//}
			
    
			// check if ORDR_WGHT is NULL, only allow update ORDR_WGHT when it has value
			//If is NULL, the panel "pnlWGHT" will be hidden
			SqlCommand cmdCheckWGHT;
			SqlDataReader dtrCheckWGHT;
    
			cmdCheckWGHT = new SqlCommand("Select SHIPMENT_WEIGHT FROM SHIPMENT WHERE SHIPMENT_ORDR_ID=@Id",conn);
			cmdCheckWGHT.Parameters.Add("@Id",strId);
			dtrCheckWGHT = cmdCheckWGHT.ExecuteReader();
			dtrCheckWGHT.Read();
    
			if (dtrCheckWGHT["SHIPMENT_WEIGHT"]!= DBNull.Value)
			{
				pnlWGHT.Visible = true;
			}
			else
			{
				pnlWGHT.Visible = false;
			}
			dtrCheckWGHT.Close();
			// check if ORDR_SIZE is 190000(RailCar), the panel "pnlFreight" will be hidden
			//because the freight(ship price) for rail car is always '0'
			//not any more!!!!!!!!!!!!!!!!!!!!!
			//if input freight for Railcar, then seller price and markup will be messed since we don't save seller price in database.
			SqlCommand cmdCheckFreight;
			SqlDataReader dtrCheckFreight;
    
			cmdCheckFreight = new SqlCommand("Select ORDR_ISSPOT, ORDR_MELT, ORDR_DENS, ORDR_ADDS, ORDR_PROD_SPOT FROM "+strMarket+" WHERE ORDR_ID=@Id",conn);
			cmdCheckFreight.Parameters.Add("@Id",strId);
			dtrCheckFreight = cmdCheckFreight.ExecuteReader();
			dtrCheckFreight.Read();
			if (dtrCheckFreight["ORDR_ISSPOT"].ToString()=="True" )
			{
				prodname = dtrCheckFreight["ORDR_PROD_SPOT"].ToString();
    
				isspot="true";
				//pnlSPOT.Visible = true;
				//txtMELT.Text = dtrCheckFreight["ORDR_MELT"].ToString();
				//txtDENS.Text = dtrCheckFreight["ORDR_DENS"].ToString();
				//txtADDS.Text = dtrCheckFreight["ORDR_ADDS"].ToString();
			}
			else 
			{
    
				isspot="false";
				//pnlSPOT.Visible = false;
			}
			dtrCheckFreight.Close();
			// first add the already selected one
			SqlCommand cmdShipTerms;
			cmdShipTerms = new SqlCommand("Select SHIPMENT_FOB FROM SHIPMENT WHERE SHIPMENT_ID=@Id",conn);
			cmdShipTerms.Parameters.Add("@Id",ViewState["ShipmentID"].ToString());
			ddlShipTerms.Items.Add(cmdShipTerms.ExecuteScalar().ToString());
			ddlShipTerms.Items.Add(new ListItem ("FOB/Delivered","FOB/Delivered"));
			ddlShipTerms.Items.Add(new ListItem ("FOB/Shipping","FOB/Shipping"));
			ddlShipTerms.Items.Add(new ListItem ("Delivered by TPE","Delivered Tpe"));
    
    
		/*
			SqlCommand cmdUpdated;
			
			Not useed anymore.
					cmdUpdated = new SqlCommand("select ORDR_UPDATED from "+strMarket+" where ORDR_ID =@Id",conn);
					cmdUpdated.Parameters.Add("@Id",strId);
					// display a message if someone has already edited this transaction
					if ((bool)cmdUpdated.ExecuteScalar()){
						lblUpdated.Text = "Updated since the order took place!";
					}else{
						lblUpdated.Text = "";
					}
			*/    
			lblUpdated.Text = "";
			if (!IsPostBack)
			{
				cmdCheckFreight = new SqlCommand("Select ORDR_ISSPOT, ORDR_MELT, ORDR_DENS, ORDR_ADDS FROM "+strMarket+" WHERE ORDR_ID=@Id",conn);
				cmdCheckFreight.Parameters.Add("@Id",strId);
				dtrCheckFreight = cmdCheckFreight.ExecuteReader();
				dtrCheckFreight.Read();
				if (dtrCheckFreight["ORDR_ISSPOT"].ToString()=="True" )
				{
					//pnlFreight.Visible = false;
					isspot="true";
					//pnlSPOT.Visible = true;
					txtMELT.Text = dtrCheckFreight["ORDR_MELT"].ToString();
					txtDENS.Text = dtrCheckFreight["ORDR_DENS"].ToString();
					txtADDS.Text = dtrCheckFreight["ORDR_ADDS"].ToString();
				}
				else 
				{
    
					isspot="false";
					//pnlSPOT.Visible = false;
				}
				dtrCheckFreight.Close();
    
				// Begin Seller Setup
				SqlCommand cmdSeller;
				SqlDataReader dtrSeller;
    
				cmdSeller = new SqlCommand("Select SHIPMENT_SELR,(Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME FROM PERSON WHERE PERS_ID =SHIPMENT_SELR) AS PERS_NAME,(Select PERS_ID FROM PERSON WHERE PERS_ID =SHIPMENT_SELR) AS PERS_ID,(Select COMP_NAME FROM COMPANY WHERE COMP_ID =(Select PERS_COMP FROM PERSON WHERE PERS_ID =SHIPMENT_SELR)) AS COMP_NAME,(Select COMP_ID FROM COMPANY WHERE COMP_ID =(Select PERS_COMP FROM PERSON WHERE PERS_ID =SHIPMENT_SELR)) AS COMP_ID,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=SHIPMENT_FROM)) AS PLAC_NAME, SHIPMENT_FROM AS PLAC_ID FROM SHIPMENT WHERE SHIPMENT_ID=@Id",conn);
				cmdSeller.Parameters.Add("@Id",ViewState["ShipmentID"].ToString());
				dtrSeller = cmdSeller.ExecuteReader();
				dtrSeller.Read();
				// add currently selected items into Seller boxes
    
				if (dtrSeller["COMP_NAME"].ToString().Length > 25)
				{ // truncates the name just so that it doesn't grow too long
					ddlSellerComp.Items.Add ( new ListItem (dtrSeller["COMP_NAME"].ToString().Substring(0,25)+"...",dtrSeller["COMP_ID"].ToString() ) );
				} 
				else 
				{
					ddlSellerComp.Items.Add ( new ListItem (dtrSeller["COMP_NAME"].ToString(),dtrSeller["COMP_ID"].ToString() ) );
    
				}
				ddlSellerName.Items.Add ( new ListItem ((string) dtrSeller["PERS_NAME"],dtrSeller["PERS_ID"].ToString() ) );
				if (dtrSeller["PLAC_NAME"]!= DBNull.Value)
				{
					//add location tied to this transaction to drop-down first
					ddlSellerLocation.Items.Add ( new ListItem ((string) dtrSeller["PLAC_NAME"],dtrSeller["PLAC_ID"].ToString() ) );
				}
    
				dtrSeller.Close();
    
    
				// add all other options underneath
				// this way the currently selected ones are listed automatically on the top
				SqlCommand cmdCompanies;
				SqlDataReader dtrCompanies;
				// query returns all companies except TPE.  We should not be listed on the screen
				cmdCompanies = new SqlCommand("Select COMP_NAME,COMP_ID FROM COMPANY WHERE COMP_ID<>'148' ORDER BY COMP_NAME ASC",conn);
				dtrCompanies = cmdCompanies.ExecuteReader();
    
				while (dtrCompanies.Read())
				{
					if (dtrCompanies["COMP_NAME"].ToString().Length > 25)
					{
						ddlSellerComp.Items.Add ( new ListItem (dtrCompanies["COMP_NAME"].ToString().Substring(0,25)+"...",dtrCompanies["COMP_ID"].ToString() ) );
					} 
					else 
					{
						ddlSellerComp.Items.Add ( new ListItem (dtrCompanies["COMP_NAME"].ToString(),dtrCompanies["COMP_ID"].ToString() ) );
					}
    
				}
				dtrCompanies.Close();
				//Begin Buyer Setup
    
				SqlCommand cmdBuyer;
				SqlDataReader dtrBuyer;

				cmdBuyer = new SqlCommand("Select SHIPMENT_BUYR,(Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME FROM PERSON WHERE PERS_ID =SHIPMENT_BUYR) AS PERS_NAME,(Select PERS_ID FROM PERSON WHERE PERS_ID =SHIPMENT_BUYR) AS PERS_ID,(Select COMP_NAME FROM COMPANY WHERE COMP_ID =(Select PERS_COMP FROM PERSON WHERE PERS_ID =SHIPMENT_BUYR)) AS COMP_NAME,(Select COMP_ID FROM COMPANY WHERE COMP_ID =(Select PERS_COMP FROM PERSON WHERE PERS_ID =SHIPMENT_BUYR)) AS COMP_ID,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=SHIPMENT_TO)) AS PLAC_NAME, SHIPMENT_TO AS PLAC_ID FROM SHIPMENT WHERE SHIPMENT_ID=@Id",conn);
				cmdBuyer.Parameters.Add("@Id",ViewState["ShipmentID"].ToString());
				dtrBuyer = cmdBuyer.ExecuteReader();
				dtrBuyer.Read();
				// add currently selected items into Seller boxes
    
				if (dtrBuyer["COMP_NAME"].ToString().Length > 25)
				{ // truncates the name just so that it doesn't grow too long
					ddlBuyerComp.Items.Add ( new ListItem (dtrBuyer["COMP_NAME"].ToString().Substring(0,25)+"...",dtrBuyer["COMP_ID"].ToString() ) );
				} 
				else 
				{
					ddlBuyerComp.Items.Add ( new ListItem (dtrBuyer["COMP_NAME"].ToString(),dtrBuyer["COMP_ID"].ToString() ) );
    
				}
				ddlBuyerName.Items.Add ( new ListItem ((string) dtrBuyer["PERS_NAME"],dtrBuyer["PERS_ID"].ToString() ) );
				//add location tied to this transaction to drop-down first
				ddlBuyerLocation.Items.Add ( new ListItem (dtrBuyer["PLAC_NAME"].ToString(),dtrBuyer["PLAC_ID"].ToString() ) );
    
				dtrBuyer.Close();
    
    
				// add all other options underneath
				// this way the currently selected ones are listed automatically on the top
				SqlCommand cmdBuyerCompanies;
				SqlDataReader dtrBuyerCompanies;
    
				cmdBuyerCompanies = new SqlCommand("Select COMP_NAME,COMP_ID FROM COMPANY ORDER BY COMP_NAME ASC",conn);
				dtrBuyerCompanies = cmdBuyerCompanies.ExecuteReader();
    
				while (dtrBuyerCompanies.Read())
				{
					if (dtrBuyerCompanies["COMP_NAME"].ToString().Length > 25)
					{
						ddlBuyerComp.Items.Add ( new ListItem (dtrBuyerCompanies["COMP_NAME"].ToString().Substring(0,25)+"...",dtrBuyerCompanies["COMP_ID"].ToString() ) );
					} 
					else 
					{
						ddlBuyerComp.Items.Add ( new ListItem (dtrBuyerCompanies["COMP_NAME"].ToString(),dtrBuyerCompanies["COMP_ID"].ToString() ) );
					}
    
				}
				dtrBuyerCompanies.Close();
    
				Bind_Seller();
				Bind_Buyer();

				// bind datagrid for past  comments
				string strSQL ="SELECT CONVERT(varchar,COMMENT_DATE,101) AS COMMENT_DATE, COMMENT_TEXT,(SELECT PERS_FRST_NAME FROM PERSON WHERE PERS_ID=COMMENT_PERSID) AS PERSON FROM SHIPMENT_COMMENT WHERE COMMENT_SHIPMENT='"+ViewState["ShipmentID"].ToString()+"' ORDER BY COMMENT_DATE";
				SqlDataAdapter dadContent;
				DataSet dstContent;
				SqlConnection connComments;
				connComments = new SqlConnection(Application["DBconn"].ToString());
				connComments.Open();

				
				dadContent = new SqlDataAdapter(strSQL,connComments);
				dstContent = new DataSet();
				dadContent.Fill(dstContent);
				dgComments.DataSource = dstContent;
				dgComments.DataBind();
				connComments.Close();
    
				// populating other information
    
    
				SqlCommand cmdTransaction;
				SqlDataReader dtrTransaction;
    
				cmdTransaction = new SqlCommand("select CAST(ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR) AS ID,(SELECT PERS_FRST_NAME FROM PERSON WHERE PERS_ID=SHIPMENT_BROKER_ID) AS BROKER,SHIPMENT_BROKER_ID,SHIPMENT_COMMENT,(CASE SHIPMENT_SHIP_STATUS WHEN 1 THEN 'Inventory' WHEN 2 THEN 'En Route' WHEN 3 THEN 'Delivered'  END) AS SHIPMENT_STATUS,SHIPMENT_BUYER_TERMS,SHIPMENT_SELLER_TERMS, SHIPMENT_WEIGHT,SHIPMENT_PRCE,SHIPMENT_MARK=(SHIPMENT_BUYR_PRCE-SHIPMENT_PRCE-SHIPMENT_COMM)*ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY)-SHIPMENT_SHIP_PRCE, CONVERT(varchar(14),ORDR_DATE,101) AS ORDR_Date, SPRICE=SHIPMENT_BUYR_PRCE,SHIPMENT_SHIP_PRCE, SHIPMENT_PO_NUM, SHIPMENT_SIZE, SHIPMENT_QTY, SHIPMENT_COMM from "+strMarket+" , SHIPMENT where  SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ID =@Id",conn);
				cmdTransaction.Parameters.Add("@Id",ViewState["ShipmentID"].ToString());
				dtrTransaction = cmdTransaction.ExecuteReader();
				dtrTransaction.Read();
				txtPO.Text = dtrTransaction["SHIPMENT_PO_NUM"].ToString();
				txtSellerTerms.Text = dtrTransaction["SHIPMENT_SELLER_TERMS"].ToString();
				txtBuyerTerms.Text=dtrTransaction["SHIPMENT_BUYER_TERMS"].ToString();
				lblOrderID.Text = dtrTransaction["ID"].ToString();
				txtDate.Text =dtrTransaction["ORDR_Date"].ToString();
				txtSPrice.Text = dtrTransaction["SHIPMENT_PRCE"].ToString();
				txtQty.Text = dtrTransaction["SHIPMENT_QTY"].ToString();
				txtCommission.Text = dtrTransaction["SHIPMENT_COMM"].ToString();
				txtPrice.Text = dtrTransaction["SPRICE"].ToString();
				LblMarkup.Text = String.Format("{0:c}", dtrTransaction["SHIPMENT_MARK"].ToString());
				txtWeight.Text = dtrTransaction["SHIPMENT_WEIGHT"].ToString();
				txtFreight.Text = dtrTransaction["SHIPMENT_SHIP_PRCE"].ToString();
				
				if ((dtrTransaction["SHIPMENT_SIZE"].ToString().Equals("190000")) && (dtrTransaction["SHIPMENT_COMMENT"] != null))
				LblRailCarNbr.Text = "Rail Car #: <a href=\"https://www.steelroads.com/servlet/SAServlet?CONTROLLER=ETScriptedAccessCtlr&ScriptedAccessType=Dynamic&ACTION=&LANGUAGE=en&ImportList&EquipmentList="+ dtrTransaction["SHIPMENT_COMMENT"].ToString()+"\" >" + dtrTransaction["SHIPMENT_COMMENT"].ToString()+"</a>";
				LblLoadStatus.Text = dtrTransaction["SHIPMENT_STATUS"].ToString();
				txtProduct.Text=prodname;
    
				//display the order size from database in the drop-down first
				switch(dtrTransaction["SHIPMENT_SIZE"].ToString())
				{
					case "190000":
						ddlSize.Items.Add(new ListItem ("Rail Car","190000"));
						break;
					case "2205":
						ddlSize.Items.Add(new ListItem ("Metric Ton","2205"));
						break;
					case "42000":
						ddlSize.Items.Add(new ListItem ("Truckload Boxes","42000"));
						break;
					case "44092":
						ddlSize.Items.Add(new ListItem ("Truckload Bags","44092"));
						break;
					case "45000":
						ddlSize.Items.Add(new ListItem ("Bulk Truck","45000"));
						break;
					case "1":
						ddlSize.Items.Add(new ListItem ("Pounds (lbs)","1"));
						break;
					case "38500":
						ddlSize.Items.Add(new ListItem ("20' Container","38500"));
						break;
					case "44500":
						ddlSize.Items.Add(new ListItem ("30' Container","44500"));
						break;
    
				}
				//order size drop-down
				ddlSize.Items.Add(new ListItem ("Rail Car","190000"));
				ddlSize.Items.Add(new ListItem ("Metric Ton","2205"));
				ddlSize.Items.Add(new ListItem ("Truckload Boxes","42000"));
				ddlSize.Items.Add(new ListItem ("Truckload Bags","44092"));
				ddlSize.Items.Add(new ListItem ("Bulk Truck","45000"));
				ddlSize.Items.Add(new ListItem ("Pounds (lbs)","1"));
				ddlSize.Items.Add(new ListItem ("20' Container","38500"));
				ddlSize.Items.Add(new ListItem ("30' Container","44500"));
				if (dtrTransaction["BROKER"] !=  DBNull.Value)
				{
					ddlBrokers.Items.Add ( new ListItem (dtrTransaction["BROKER"].ToString(),dtrTransaction["SHIPMENT_BROKER_ID"].ToString() ) );
				}
				ddlBrokers.Items.Add ( new ListItem ("None","null" ) );
                dtrTransaction.Close();

				SqlCommand cmdBroker;
				SqlDataReader dtrBroker;
    
				cmdBroker = new SqlCommand("select PERS_FRST_NAME,PERS_ID FROM PERSON WHERE PERS_TYPE='A' or PERS_TYPE='B'",conn);
				dtrBroker = cmdBroker.ExecuteReader();
				while (dtrBroker.Read())
				{	
					ddlBrokers.Items.Add ( new ListItem (dtrBroker["PERS_FRST_NAME"].ToString(),dtrBroker["PERS_ID"].ToString() ) );
				}
				dtrBroker.Close();
				conn.Close();
  
			}
			//txtComment.Multiline=true;
			//txtComment.ScrollBars = ScrollBars.Vertical;
			//extComment.AcceptsReturn = true;

    
		}
		//public void Click_Done(object sender, EventArgs e){
		// redirect the user once the save takes place
		//    Save();
		//    Response.Redirect(Request.QueryString["Referer"]);
		//}
		public void Click_Update(object sender, EventArgs e)
		{
			Save();
			Response.Redirect(Request.QueryString["Referer"]);
			// Cancel shouln't be visilbe anymore. it would imply that a user could undo the
			// changes that have already been made
			//btnCancel.Visible = false;
    
		}
    
		public void Save()
		{
    
			//string product;
			//if (radio_Other.Checked){
    
			//         product= txtProduct.Text;
    
			//      }else{
			//     product=ddlProduct.SelectedItem.ToString();
			// }
    
    
    
    
    
			string strSQLOrder="";
			//double dbTempMarkup;
			SqlCommand cmdUpdateOrder;    
			// determines markup
			//dbTempMarkup = (Convert.ToDouble(txtPrice.Text)-Convert.ToDouble(txtSPrice.Text)-Convert.ToDouble(txtCommission.Text));
			//LblMarkup.Text = Math.Round(dbTempMarkup,5).ToString();
    
			strSQLOrder += "UPDATE SHIPMENT set SHIPMENT_FROM='"+ddlSellerLocation.SelectedItem.Value+"',SHIPMENT_FOB='"+ddlShipTerms.SelectedItem.Value.ToString()+"',SHIPMENT_TO='"+ddlBuyerLocation.SelectedItem.Value+"',SHIPMENT_BUYR='"+ddlBuyerName.SelectedItem.Value+"',SHIPMENT_SELR='"+ddlSellerName.SelectedItem.Value+"',SHIPMENT_PRCE='"+txtSPrice.Text+"',SHIPMENT_BUYR_PRCE='"+txtPrice.Text+"',SHIPMENT_COMM='"+txtCommission.Text+"'";
			
			if(pnlWGHT.Visible == true) 
			{
				strSQLOrder +=",SHIPMENT_WEIGHT='"+txtWeight.Text+"'";
			}
			if (ddlBrokers.SelectedItem.Value != "null")
			{
				strSQLOrder +=",SHIPMENT_BROKER_ID='"+ddlBrokers.SelectedItem.Value+"'";
			}
			else
			{
				strSQLOrder +=",SHIPMENT_BROKER_ID = null";
			}
			strSQLOrder +=",SHIPMENT_BUYER_TERMS='"+txtBuyerTerms.Text+"',SHIPMENT_PO_NUM='"+txtPO.Text+"',SHIPMENT_SELLER_TERMS='"+txtSellerTerms.Text+"',SHIPMENT_SHIP_PRCE='"+txtFreight.Text+"',SHIPMENT_QTY='"+txtQty.Text+"', SHIPMENT_SIZE="+ddlSize.SelectedItem.Value+", SHIPMENT_UPDATED ='1' where SHIPMENT_ORDR_ID='"+strId+"' AND SHIPMENT_ID='"+ViewState["ShipmentID"].ToString()+"';";
			strSQLOrder += "UPDATE ORDERS set ORDR_DATE='"+txtDate.Text+"', ORDR_MELT='"+txtMELT.Text+"', ORDR_DENS='"+txtDENS.Text+"', ORDR_ADDS='"+txtADDS.Text+"', ORDR_PROD_SPOT='"+txtProduct.Text+"' where ORDR_ID='"+strId+"';";
			strSQLOrder += "INSERT into Shipment_Comment (Comment_Shipment, Comment_PersID, Comment_Date, Comment_Text) values ('"+ViewState["ShipmentID"].ToString()+"','"+ Session["Id"].ToString()+"','"+System.DateTime.Today.Date+"','"+txtComment.Text+"' )";
					
			cmdUpdateOrder = new SqlCommand(strSQLOrder,conn);
			cmdUpdateOrder.ExecuteNonQuery();
			Response.Write(strSQLOrder);
			

		}
		public void Cancel(object sender, EventArgs e)
		{
			// redirect the user once the save takes place
			
			Response.Redirect(Request.QueryString["Referer"]);
		}
		public void Change_Company(object sender, EventArgs e)
		{
			Bind_Seller();
			Bind_Buyer();
		}
		public void Bind_Buyer()
		{
			if (IsPostBack)
			{
				ddlBuyerName.Items.Clear();
				ddlBuyerLocation.Items.Clear();
			}
    
			//
			// adding all the possible users within the company
			SqlCommand cmdUsers;
			SqlDataReader dtrUsers;
    
			cmdUsers = new SqlCommand("Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME as PERS_NAME,PERS_ID FROM PERSON WHERE PERS_COMP="+ddlBuyerComp.SelectedItem.Value+" ORDER By PERS_TYPE DESC",conn);
			dtrUsers = cmdUsers.ExecuteReader();
    
			while(dtrUsers.Read())
			{
				ddlBuyerName.Items.Add ( new ListItem ((string) dtrUsers["PERS_NAME"],dtrUsers["PERS_ID"].ToString() ) );
    
			}
			dtrUsers.Close();
    
			//
			// adding all the possible locations within the company
			SqlCommand cmdLocations;
			SqlDataReader dtrLocations;


			// add locations 

			cmdLocations = new SqlCommand("select  PLAC_ID, LABEL = (select LOCL_CITY +', '+ LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) from place where PLAC_COMP="+ddlBuyerComp.SelectedItem.Value+" and plac_type<>'H'  ORDER BY LABEL",conn);
			dtrLocations = cmdLocations.ExecuteReader();
			while(dtrLocations.Read())
			{
				ddlBuyerLocation.Items.Add ( new ListItem (dtrLocations["LABEL"].ToString(),dtrLocations["PLAC_ID"].ToString() ) );
			}
			dtrLocations.Close();
    
			
			//add the warehouses!!
			cmdLocations = new SqlCommand("select  PLAC_ID, LABEL = (select LOCL_CITY +', '+ LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) + ' ('+PLAC_LABL+')'  from place where plac_type='W'  ORDER BY LABEL",conn);
			dtrLocations = cmdLocations.ExecuteReader();
			while(dtrLocations.Read())
			{
				ddlBuyerLocation.Items.Add ( new ListItem (dtrLocations["LABEL"].ToString(),dtrLocations["PLAC_ID"].ToString() ) );
			}
			dtrLocations.Close();

			
    
		}
    
		public void Bind_Seller()
		{
    
			if (IsPostBack)
			{
				ddlSellerName.Items.Clear();
				ddlSellerLocation.Items.Clear();
			}
			//
			// adding all the possible users within the company
			SqlCommand cmdUsers;
			SqlDataReader dtrUsers;
    
			cmdUsers = new SqlCommand("Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME as PERS_NAME,PERS_ID FROM PERSON WHERE PERS_COMP="+ddlSellerComp.SelectedItem.Value+" ORDER By PERS_TYPE DESC" ,conn);
			dtrUsers = cmdUsers.ExecuteReader();
    
			while(dtrUsers.Read())
			{
				ddlSellerName.Items.Add ( new ListItem ((string) dtrUsers["PERS_NAME"],dtrUsers["PERS_ID"].ToString() ) );
    
			}
			dtrUsers.Close();
    
			//
			// adding all the possible locations within the company
			SqlCommand cmdLocations;
			SqlDataReader dtrLocations;


			// add locations 

			cmdLocations = new SqlCommand("select  PLAC_ID, LABEL = (select LOCL_CITY +', '+ LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) from place where PLAC_COMP="+ddlSellerComp.SelectedItem.Value+" and plac_type<>'H'  ORDER BY LABEL",conn);
			dtrLocations = cmdLocations.ExecuteReader();
			while(dtrLocations.Read())
			{
				ddlSellerLocation.Items.Add ( new ListItem (dtrLocations["LABEL"].ToString(),dtrLocations["PLAC_ID"].ToString() ) );
			}
			dtrLocations.Close();
    

			//add the warehouses!!
			cmdLocations = new SqlCommand("select  PLAC_ID, LABEL = (select LOCL_CITY +', '+ LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) + ' ('+PLAC_LABL+')'  from place where plac_type='W'  ORDER BY LABEL",conn);
			dtrLocations = cmdLocations.ExecuteReader();
			while(dtrLocations.Read())
			{
				ddlSellerLocation.Items.Add ( new ListItem (dtrLocations["LABEL"].ToString(),dtrLocations["PLAC_ID"].ToString() ) );
			}
			dtrLocations.Close();
    
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
