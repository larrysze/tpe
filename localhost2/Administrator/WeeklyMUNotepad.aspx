<%@ Page language="c#" Codebehind="WeeklyMUNotepad.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.WeeklyMUNotepad" MasterPageFile="~/MasterPages/Menu.Master" Title="Week in Review Notepad" %>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="asp" namespace="System.Web.UI" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">

</asp:Content>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">

  <asp:ScriptManager id="ScriptManager1" runat="Server" />
    
                 <asp:UpdatePanel id="UpdatePanel1" runat="Server">
 <ContentTemplate>
      

    <br />    
    <center>
        <span class="Content Color2"><b>TPE Week in Review</B></span>
    </center>
    <br />
    <span class="Content">
        <asp:placeholder id="htmlPlace" Runat="server" />
    </span>

    <asp:panel id="mainPanel" Runat="server">
       
    <asp:datagrid id="dgSummaryGrid" runat="server" BorderWidth="0" BackColor="#000000" CellSpacing="1" CellPadding="3"  
        Width="390" AutoGenerateColumns="False" HorizontalAlign="Center">
	    <AlternatingItemStyle BorderStyle="Solid" CssClass="LinkNormal Color2 DarkGray" />	   
        <ItemStyle CssClass="LinkNormal Color2 LightGray" />    
	    <HeaderStyle CssClass="LinkNormal Color2 OrangeColor" />
	
	    <Columns>
	        <asp:HyperLinkColumn ItemStyle-HorizontalAlign="Left" DataNavigateUrlField="offr_grade" 
	            DataNavigateUrlFormatString="/spot/Spot_Floor.aspx?Filter={0}" DataTextField="GRADE" HeaderText="Resin">
	            <HeaderStyle CssClass="LinkNormal" HorizontalAlign="Left" />	        
	        </asp:HyperLinkColumn>
	        
	        <asp:BoundColumn DataField="varsize" HeaderText="Total lbs" DataFormatString="{0:#,##}">
	            <HeaderStyle CssClass="hspottitles" />	
	            <ItemStyle Wrap="False" CssClass="Content LinkNormal" />	        
	        </asp:BoundColumn>
        		
	        <asp:BoundColumn DataField="Min" HeaderText="Low" DataFormatString="{0:c}">
    	        <HeaderStyle CssClass="hspottitles" />
	            <ItemStyle Wrap="False" CssClass="Content LinkNormal" />	        
	        </asp:BoundColumn>
        	
	        <asp:BoundColumn DataField="Max" HeaderText="High" DataFormatString="{0:c}">
	            <HeaderStyle CssClass="hspottitles" />
	            <ItemStyle Wrap="False" CssClass="Content LinkNormal" />	        
	        </asp:BoundColumn>	
        	
            <asp:TemplateColumn Visible="False" HeaderText="Price Range">
                <HeaderStyle CssClass="hspottitles" />
                <ItemStyle Wrap="False" CssClass="Content LinkNormal" />
            </asp:TemplateColumn>
        </Columns>
	</asp:datagrid>
						
	<BR>
	<FONT class="Content"><B>Summary</B></FONT> 
<TABLE id=Table1 width="100%" border=0>
  <TR width="100%"></TR></TABLE>
<asp:textbox id=txtComments runat="server" CssClass="InputForm" Width="100%" TextMode="MultiLine" Height="170px"></asp:textbox><BR><BR>
<HR SIZE=2>
<BR>

    <TABLE id=Table2 width="100%">
    <TR>
    <TD style="WIDTH: 183px; HEIGHT: 20px">
      <P><FONT class="Content"><B>Polyethylene</B></FONT></P></TD></TR>
  <TR>
    <TD align="center">
<asp:Label id=lblVolumePE Runat="server" CssClass="Content" Font-Bold="True">Volume:</asp:Label>
<asp:TextBox id=txtVolumePE CssClass="InputForm" Runat="server"></asp:TextBox></TD></TR>
  <TR>
    <TD align="center">
<asp:Label id=lblPricePE Runat="server" CssClass="Content" Font-Bold="True">Price:</asp:Label>&nbsp;&nbsp;&nbsp; 
<asp:TextBox id=txtPricePE Runat="server" CssClass="InputForm"></asp:TextBox></TD></TR>
    <tr>
        <td colSpan=2>
            <asp:TextBox id=txtPreTextPE Runat="server" CssClass="InputForm" Width="100%" TextMode="MultiLine" Height="130px" />
        </td>
    </tr>
    
    <tr>
        <td width="50%">
             <center>
                <asp:Label id=Label3 Runat="server" CssClass="Content" Font-Bold="True" Text="Chart: " />                
                <asp:DropDownList runat="server" ID="ddlPEChart" CssClass="InputForm" AutoPostBack="True" OnSelectedIndexChanged="ddlPEChart_SelectedIndexChanged">
                    <asp:ListItem Text="Polyethylene Chain 30 Day" Value="0" Selected="True" />
                    <asp:ListItem Text="Spot LLDPE Butene - Spot Ethylene - Spread" Value="1" />
                </asp:DropDownList>
            </center>
        </td>
        <td width="50%">
            &nbsp;
        </td>
    </tr>   
    
    <tr>
        <td width="50%">
             <center>
                <asp:Label ID="lblPE30Day" CssClass="Content Bold" runat="server" Text="Polyethylene 30 Day" />
                <br /><br />
            </center>
            <asp:image id="imgPolyethyleneMonth" runat="server" width="315" height="225" ImageUrl="/Research/Charts/PolyethyleneChain30Day.png"  />
        </td>
        <td width="50%">
             <center>
                <asp:Label ID="Label4" CssClass="Content Bold" runat="server" Text="Polyethylene 1 Year" />
                <br /><br />
            </center>
            <asp:image id="imgPolyethyleneYear" runat="server" width="315" height="225"  ImageUrl="/Research/Charts/PolyethyleneChain.png" />
        </td>
    </tr>


  <TR>
    <TD style="HEIGHT: 215px" colSpan=2>
<asp:textbox id=txtPolyethylene runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="230px"></asp:textbox></TD>
  <TR></TR>
  <TR>
    <TD style="WIDTH: 183px">
      <P>&nbsp;</P>
      <P><FONT class="Content"><B>Polypropylene</B></FONT></P></TD></TR>
  <TR>
    <TD>
<asp:Label id=lblVolumePP CssClass="Content" Runat="server" Font-Bold="True">Volume:</asp:Label>
<asp:TextBox id=txtVolumePP CssClass="InputForm" Runat="server"></asp:TextBox></TD></TR>
  <TR>
    <TD>
<asp:Label id=lblPricePP Runat="server" CssClass="Content" Font-Bold="True">Price:</asp:Label>&nbsp;&nbsp;&nbsp; 
<asp:TextBox id=txtPricePP Runat="server" CssClass="InputForm"></asp:TextBox></TD></TR>
  <TR>
    <TD colSpan=2>
<asp:TextBox id=txtPreTextPP Runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="130px"></asp:TextBox></TD></TR>
  

    <tr>
        <td width="50%">
             <center>
                <asp:Label id=Label2 Runat="server" CssClass="Content" Font-Bold="True" Text="Chart: " />                
                <asp:DropDownList runat="server" ID="ddlPPChart" CssClass="InputForm" AutoPostBack="True" OnSelectedIndexChanged="ddlPPChart_SelectedIndexChanged">
                    <asp:ListItem Text="Polypropylene Chain 30 Day" Value="0" Selected="True" />
                    <asp:ListItem Text="Spot HoPP - Spot RGP - Spread" Value="1" />
                </asp:DropDownList>
            </center>
        </td>
        <td width="50%">
            &nbsp;
        </td>
    </tr>  


    <TR>
        <TD width="50%">
             <center>
                <asp:Label ID="lblPP30Day" CssClass="Content Bold" runat="server" />
                <br /><br />
            </center>
            <asp:image id="imgPolypropyleneMonth" runat="server" width="315"  height="225" ImageUrl = "/Research/Charts/PolypropyleneChain30Day.png" />
        </TD>
        <TD width="50%">
             <center>
                <asp:Label ID="Label6" CssClass="Content Bold" runat="server" Text="Polypropylene 1 Year" />
                <br /><br />
            </center>
            <asp:image id="Image4" runat="server" width="315" ImageUrl="/Research/Charts/PolypropyleneChain.png" height="225" />
        </TD>
    </TR>


  <TR>
    <TD style="HEIGHT: 215px" colSpan=2>
<asp:textbox id=txtPolypropylene runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="230px"></asp:textbox></TD></TR>
  <TR>
    <TD style="WIDTH: 183px">
      <P>&nbsp;</P>
      <P><FONT class="Content"><B>Polystyrene</B></FONT></P></TD></TR>
  <TR>
    <TD>
<asp:Label id=lblVolumePS Runat="server" CssClass="Content" Font-Bold="True">Volume:</asp:Label>
<asp:TextBox id=txtVolumePS Runat="server" CssClass="InputForm"></asp:TextBox></TD></TR>
  <TR>
    <TD>
<asp:Label id=lblPricePS Runat="server" CssClass="Content" Font-Bold="True">Price:</asp:Label>&nbsp;&nbsp;&nbsp; 
<asp:TextBox id=txtPricePS Runat="server" CssClass="InputForm"></asp:TextBox></TD></TR>
  <TR>
    <TD colSpan=2>
<asp:TextBox id=txtPreTextPS Runat="server" Width="100%" CssClass="InputForm" TextMode="MultiLine" Height="130px"></asp:TextBox></TD></TR>



    <TR>
        <TD width="50%">
             <center>
                <asp:Label ID="Label7" CssClass="Content Bold" runat="server" Text="Polystyrene 30 Day"></asp:Label>
                <br /><br />
            </center>
            <asp:image id="Image5" runat="server" width="315" ImageUrl="/Research/Charts/PolystyreneChain30Day.png" height="225" />
        </TD>
        <TD width="50%">
             <center>
                <asp:Label ID="Label8" CssClass="Content Bold" runat="server" Text="Polystyrene 1 Year" />
                <br /><br />
            </center>
            <asp:image id="Image6" runat="server" width="315" ImageUrl="/Research/Charts/PolystyreneChain.png" height="225" />
        </TD>
    </TR>



  <TR>
    <TD style="HEIGHT: 215px" colSpan=2>
<asp:textbox id=txtPolystyrene CssClass="InputForm" runat="server" Width="100%" TextMode="MultiLine" Height="230px"></asp:textbox></TD></TR></TABLE><BR><BR>
<BR><BR><BR>
<TABLE id=Table3 width="100%" runat="server">
  <TR>
    <TD align=center>
<asp:button id=btnSave CssClass="Content Color2" runat="server" Text="Save" onclick="btnSave_Click"></asp:button>&nbsp; 
<asp:button id=btnPreview CssClass="Content Color2" runat="server" Text="Preview" onclick="btnPreview_Click"></asp:button></TD>
    <TD align=center>
<asp:button id=btnPublish runat="server" CssClass="Content Color2" Text="Publish" DESIGNTIMEDRAGDROP="156" onclick="btnPublish_Click"></asp:button>
<asp:button id=btnUpdate runat="server" CssClass="Content Color2" Text="Update WR (no confirmation)" Visible="False" onclick="btnUpdate_Click"></asp:button><BR></TD></TR></TABLE></asp:panel>
<CENTER><asp:button id=btnClose CssClass="Content Color2" runat="server" Text="OK" Visible="False" onclick="btnClose_Click"></asp:button></CENTER>
<CENTER><asp:button id=btnSure CssClass="Content Color2" runat="server" Text="Yes, I am sure" Visible="False" onclick="btnSure_Click"></asp:button><asp:button id=btnCancel runat="server" CssClass="Content Color2" Text="No, later" Visible="False" onclick="btnCancel_Click"></asp:button></CENTER><br>

</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>

