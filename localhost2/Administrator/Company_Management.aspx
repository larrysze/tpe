<%@ Page Language="c#" CodeBehind="Company_Management.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.Company_Management" MasterPageFile="~/MasterPages/Menu.Master" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="cc" %>

<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">

    <style type="text/css">        
        .progressAjaxPleaseWait
        {
            display: block;
            position: absolute;
            padding: 2px 3px;
        }
        .containerAjaxPleaseWait
        {
            border: solid 1px #808080;
            border-width: 1px 0px;
        }
        .headerAjaxPleaseWait
        {
            background: url(http://www.theplasticexchange.com/images/sprite.png) repeat-x 0px 0px;
            border-color: #808080 #808080 #ccc;
            border-style: solid;
            border-width: 0px 1px 1px;
            padding: 0px 10px;
            color: #000000;
            font-size: 9pt;
            font-weight: bold;
            line-height: 1.9;  
            font-family: arial,helvetica,clean,sans-serif;
        }
        .bodyAjaxPleaseWait
        {
            background-color: #f2f2f2;
            border-color: #808080;
            border-style: solid;
            border-width: 0px 1px;
            padding: 10px;
        }                
        /* ajax__tab_yuitabview-theme theme (img/yui/sprite.png) */
        .ajax__tab_yuitabview-theme .ajax__tab_header 
        {
            font-family:arial,helvetica,clean,sans-serif;
            font-size:small;
            border-bottom:solid 5px #2647a0;
        }
        .ajax__tab_yuitabview-theme .ajax__tab_header .ajax__tab_outer 
        {
            background:url(http://www.theplasticexchange.com/images/yui/sprite.png) #d8d8d8 repeat-x;
            margin:0px 0.16em 0px 0px;
            padding:1px 0px 1px 0px;
            vertical-align:bottom;
            border:solid 1px #a3a3a3;
            border-bottom-width:0px;
        }
        .ajax__tab_yuitabview-theme .ajax__tab_header .ajax__tab_tab
        {    
            color:#000;
            padding:0.35em 0.75em;    
            margin-right:0.01em;
        }
        .ajax__tab_yuitabview-theme .ajax__tab_hover .ajax__tab_outer 
        {
            background: url(http://www.theplasticexchange.com/images/yui/sprite.png) #bfdaff repeat-x left -1300px;
        }
        .ajax__tab_yuitabview-theme .ajax__tab_active .ajax__tab_tab 
        {
            color:#fff;
        }
        .ajax__tab_yuitabview-theme .ajax__tab_active .ajax__tab_outer
        {
            background:url(http://www.theplasticexchange.com/images/yui/sprite.png) #2647a0 repeat-x left -1400px;
        }
        .ajax__tab_yuitabview-theme .ajax__tab_body 
        {
            font-family:verdana,tahoma,helvetica;
            font-size:10pt;
            padding:0.25em 0.5em;
            background-color:#666666;    
            border:solid 1px #808080;
            border-top-width:0px;
        }
    </style>
     
    <cc:ScriptManager ID="dsf" runat="server" />
 
 
    <script type="text/javascript">

            function onUpdating(){
                // get the update progress div
                var pnlPopup = $get('<%= this.pnlPopup.ClientID %>'); 

                //  get the gridview element        
                var gridView = $get('<%= this.dg.ClientID %>');
                
                // make it visible
                pnlPopup.style.display = '';	    
                
                // get the bounds of both the gridview and the progress div
                var gridViewBounds = Sys.UI.DomElement.getBounds(gridView);
                var pnlPopupBounds = Sys.UI.DomElement.getBounds(pnlPopup);
                
                //  center of gridview
                var x = gridViewBounds.x + Math.round(gridViewBounds.width / 2) - Math.round(pnlPopupBounds.width / 2);
                var y = gridViewBounds.y + Math.round(pnlPopupBounds.height / 2);	    

                //	set the progress element to this position
                Sys.UI.DomElement.setLocation(pnlPopup, x, y);           
            }

            function onUpdated() {
                // get the update progress div
                var pnlPopup = $get('<%= this.pnlPopup.ClientID %>'); 
                // make it invisible
                pnlPopup.style.display = 'none';
            }            
            
        </script>
 
   <div style="text-align:center; margin:10px, 0, 10px, 0">
   <b>Accounts Summary</b><br /><asp:Button ID="btnExcel" runat="server" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
   </div>
  
       <cc:UpdatePanel ID="updatepn" runat="server">
       
    <ContentTemplate>
        <div style="text-align:center; margin:10px, 0, 10px, 0">
        <span class="Content"><br>Show</span>
            <asp:dropdownlist id="ddlCompanyType" CssClass="InputForm" runat="server" AutoPostBack="true" onselectedindexchanged="ddlCompanyType_SelectedIndexChanged">
            <asp:ListItem Value="Buyers">Buyers</asp:ListItem>
            <asp:ListItem Value="Sellers">Sellers</asp:ListItem>
            </asp:dropdownlist> 
            <span class="Content">Years</span>
            <asp:dropdownlist id="ddlYears" CssClass="InputForm" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged">
            </asp:dropdownlist> 
        </div>
           
        <asp:datagrid id="dg" runat="server" BorderWidth="0" BackColor="#000000" CellSpacing="1" 
        ShowFooter="True" Width="780px" HorizontalAlign="Center" AllowSorting="True" 
        onSortCommand="SortDG" AutoGenerateColumns="False" OnItemCommand="dg_ItemCommand">
    
        <AlternatingItemStyle CssClass="LinkNormal Color2 DarkGray" />    
        <ItemStyle CssClass="LinkNormal Color2 LightGray" />
        <HeaderStyle CssClass="LinkNormal Color2 OrangeColor" />
        <FooterStyle CssClass="Content Bold Color4 FooterColor" />
    
        <Columns>
            <asp:BoundColumn DataField="COMP_ID" SortExpression="COMP_ID ASC" HeaderText="Company ID" />
                        
            <asp:HyperLinkColumn  DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/administrator/Company_Info.aspx?Id={0}" 
                DataTextField="COMP_NAME" SortExpression="COMP_NAME DESC" HeaderText="Company Name" ItemStyle-Wrap="False" ItemStyle-HorizontalAlign="Left" />
                       
            <asp:BoundColumn Visible="False" DataField="COMP_TYPE" SortExpression="COMP_TYPE DESC" HeaderText="Type" ItemStyle-Wrap="false" />
            
            <asp:TemplateColumn>
                <ItemTemplate>
                    <asp:ImageButton id="Image1" runat="server" ImageUrl="/Pics/icons/comment.gif" CommandName="NoComments" ToolTip="Company Comments" />	
                </ItemTemplate>
            </asp:TemplateColumn>

            <asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/spot/Spot_Floor.aspx?Filter=-1&amp;COMP_ID={0}" 
                DataTextField="WO_TOTAL_LBS" SortExpression="WO_TOTAL_LBS ASC" HeaderText="Total Open Orders" ItemStyle-HorizontalAlign="Right" 
                DataTextFormatString="{0:#,###}" />
            
            
            <asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/spot/Spot_Floor.aspx?Filter=-1&amp;COMP_ID={0}" 
                DataTextField="WO_NUM" SortExpression="WO_NUM ASC" HeaderText="#" ItemStyle-Wrap="false" />                
            
            <asp:TemplateColumn>
                <HeaderTemplate>
				    &nbsp;&nbsp;&nbsp;&nbsp;            				
                </HeaderTemplate>
            </asp:TemplateColumn>
                        
            <asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/Creditor/AccountSummary.aspx?Id={0}&amp;fct=all_history" 
                DataTextField="FO_TOTAL_LBS" ItemStyle-HorizontalAlign="Right" SortExpression="FO_TOTAL_LBS ASC" HeaderText="Total Filled Orders"  
                DataTextFormatString="{0:#,###}" />
            
            <asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/Creditor/AccountSummary.aspx?Id={0}&amp;fct=all_history" 
                DataTextField="FO_NUM" SortExpression="FO_NUM ASC" HeaderText="#" ItemStyle-Wrap="false" />
            
            
            <asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/Creditor/AccountSummary.aspx?Id={0}&amp;fct=all_history" 
                DataTextField="FO_TOTAL_DOLLARS" ItemStyle-HorizontalAlign="Right" SortExpression="FO_TOTAL_DOLLARS ASC" HeaderText="Total $" 
                DataTextFormatString="{0:c}" ItemStyle-Wrap="false" />

            <asp:HyperLinkColumn DataNavigateUrlField="COMP_ID" DataNavigateUrlFormatString="/Creditor/Transaction_Summary.aspx?COMP_ID={0}" 
                DataTextField="FO_TOTAL_PROFIT" SortExpression="FO_TOTAL_PROFIT ASC" HeaderText="Profit Margin" ItemStyle-HorizontalAlign="Right" 
                DataTextFormatString="{0:c}" />

            <asp:BoundColumn DataField="Balance" SortExpression="BALANCE ASC" HeaderText="Balance" ItemStyle-HorizontalAlign="Right" 
            DataFormatString="{0:c}" ItemStyle-Wrap="false" />
            
            <asp:TemplateColumn Visible="False">
                <HeaderTemplate>
                    Enabled 
                </HeaderTemplate>
            </asp:TemplateColumn>
        </Columns>    
    </asp:datagrid>


    </ContentTemplate>
</cc:UpdatePanel>



 
  <table cellspacing="0" cellpadding="0">
      <tr>
	        <td>
	     <cc1:UpdatePanelAnimationExtender ID="UpdatePanelAnimationExtender1" runat="server" TargetControlID="updatepn">
                <Animations>
                    <OnUpdating>
                        <Parallel duration="0">
                            <%-- place the update progress div over the gridview control --%>
                            <ScriptAction Script="onUpdating();" />                              
                         </Parallel>
                    </OnUpdating>
                    <OnUpdated>
                        <Parallel duration="0">
                         
                            <%--find the update progress div and place it over the gridview control--%>
                            <ScriptAction Script="onUpdated();" /> 
                        </Parallel> 
                    </OnUpdated>
                </Animations>
            </cc1:UpdatePanelAnimationExtender>            
            
            <asp:Panel ID="pnlPopup" runat="server" CssClass="progressAjaxPleaseWait" style="display:none;">
                <div class="containerAjaxPleaseWait">
                    <div class="headerAjaxPleaseWait">Loading...</div>
                    <div class="bodyAjaxPleaseWait">
                        <img src="http://www.theplasticexchange.com/images/activity.gif" />
                    </div>
                </div>
            </asp:Panel> 

	   
	        </td>
	        </tr>
	        </table>


</asp:Content>
