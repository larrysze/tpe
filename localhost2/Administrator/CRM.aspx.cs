using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using NineRays.Web.UI.WebControls.FlyTreeView;
using TPE.Utility;
using System.Text;
using MetaBuilders.WebControls;
using System.Text.RegularExpressions;
namespace localhost.Administrator
{
	/// <summary>
	/// ListViewCRM 的摘要说明。
	/// </summary>
	public partial class CRM2 : ServerSideViewstate.BasePage
	{
		protected System.Web.UI.WebControls.PlaceHolder phActionButton;
		protected System.Web.UI.WebControls.Label Label2;
		int iLinkSetCount = 1;
		protected System.Web.UI.WebControls.Label Label4;
		protected string operation="";

		//la colonna 12 contiene lo stesso campo della 14 (M)
		private const int PERS_ALLOCATED = 13; //Broker
		private const int PERS_PRMLY_INTRST = 14; //Unvisible
		private const int PERS_INTRST_SIZE = 15; //Unvisible
        
		private static bool isCorp_IDCheck;
		private static bool isHDPECheck, isLDPECheck, isPSPPCheck;




		protected void Page_Load(object sender, System.EventArgs e)
		{
			Master.Width = "1200px";
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "T") && ((string)Session["Typ"] != "L"))
			{
				Response.Redirect("/default.aspx");
			}
			string event_target = Request.Params.Get("__EVENTTARGET");
			if (event_target != null)
			{
				if (event_target.EndsWith("CreateFile"))
			{
			    CreateFile_Click(null, null);
			}
			else if (event_target.EndsWith("EditFile"))
			{
			    EditFile_Click(null, null);
			}
			else if (event_target.EndsWith("AddFile"))
			{
			    AddFile_Click(null, null);
			}
			}
			// 在此处放置用户代码以初始化页面
			if(!Page.IsPostBack)
			{		
				dg.MasterTableView.DataSourcePersistenceMode = Telerik.WebControls.GridDataSourcePersistenceMode.ViewState;

				if (((string)Session["Typ"] == "A") || ((string)Session["Typ"] == "L"))
				{
					this.ddlPageSize.ClearSelection();
				    this.ddlPageSize.Items.FindByText("50").Selected = true;
				}
				else if (((string)Session["Typ"] == "B"))
				{
					this.ddlPageSize.ClearSelection();
				    this.ddlPageSize.Items.FindByText("100").Selected = true;
				}
				else
				{
					this.ddlPageSize.ClearSelection();
				    this.ddlPageSize.Items.FindByText("200").Selected = true;
				}

				dg.PageSize = System.Convert.ToInt32(ddlPageSize.SelectedValue.ToString());
				dg.MasterTableView.PageSize = System.Convert.ToInt32(ddlPageSize.SelectedValue.ToString());

				ViewState["ID"]= Session["ID"].ToString();//get the user id

				Hashtable htParams = new Hashtable();
				htParams.Add("@ID",Session["ID"].ToString());
				using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
				{
					conn.Open();
					using (SqlDataReader rd = DBLibrary.GetDataReaderFromSelect(conn,"select pers_logn from person where pers_id=@ID",htParams))
					{
						if(rd.Read())
						{
							this.ViewState["login"]=rd["pers_logn"].ToString();
						}					
					}
				}			

				//this.Label1.Text= Session["ID"].ToString();
				if ((int)Session["ID"] == 2 || (int)Session["ID"] == 13564)				
				{
					this.ViewState["State"]="All";
					this.ViewState["Broker"]=null;
					ViewState["GroupID"]=null;
					ViewState["GroupName"]=null;
					ViewState["Recycle"]=null;
					ViewState["ExchangeUser"]=null;
					ViewState["Unexchangeuser"]="";
					ViewState["Unallocated"]=null;
				}
				else
				{
					ViewState["GroupID"]=null;
					ViewState["GroupName"]=null;
					ViewState["Recycle"]=null;
					ViewState["ExchangeUser"]=null;
					ViewState["Unexchangeuser"]="someone";
					ViewState["Unallocated"]=null;
					this.ViewState["Broker"]=ViewState["ID"].ToString();				
					this.ViewState["State"]="Broker";
				}
				this.btnAllocate.Visible=false;
				this.btnRemoveGroupMember.Visible=false;
				this.btnRestore.Visible=false;
				this.btnEmailOffer.Visible=true;
                
				this.btnDelete.Visible=true;

				//dg.ClientSettings.Selecting.EnableDragToSelectRows = true;
				InitializeCRM();
			}			
		}

		private void initUserTitleDDL()
		{
		    Hashtable htParams = new Hashtable();
		    if (this.ViewState["Sort"] != null)
		    {
			htParams.Add("@Sort", ViewState["Sort"].ToString());
		    }

		    if (this.ViewState["Broker"] != null)
		    {
			if (this.ViewState["State"] == null || this.ViewState["State"].ToString() != "Group")
			{
			    htParams.Add("@ID", ViewState["Broker"].ToString());
			}
		    }
		    if (this.ViewState["Unallocated"] != null)
		    {
			htParams.Add("@Unallocated", "someone");
		    }
		    // add selected market filter
		    if (rblMarketFilter.SelectedValue != "*")
		    {
			htParams.Add("@MarketFilter", rblMarketFilter.SelectedValue.ToString());
		    }            

		    // add select user flag
		    //if (!cbAllResin.Checked)
		    //{
		    //    // passes a value (hard coded) for the user preference that means all resin
		    //    htParams.Add("@AllResin", "113061887");
		    //}
		    // add search parameter
		    if (txtSearch.Text != "")
		    {
			htParams.Add("@ShowAll", "'True'");
			htParams.Add("@Search", txtSearch.Text);
		    }
		    else
		    {
			htParams.Add("@ShowAll", "'True'");
		    }

		    // filter by state if at varible is passed
		    if (ddlState.SelectedValue.ToString() != "All")
		    {
			htParams.Add("@State", ddlState.SelectedValue.ToString());
		    }
		    if (ViewState["Unexchangeuser"] != null)
		    {
			htParams.Add("@Unexchangeuser", "'someone'");
		    }
		    if (ViewState["GroupID"] != null)
		    {
			//this.Label1.Text = "--> Group:" + ViewState["GroupName"].ToString();
			//if (txtSearch.Text == "" )  // this is set so that it doesn't pass the parameter twice during a search
			//{
			//	sbSQL.Append(",@ShowAll='True'");
			//}
			// add a group number if one is available
			// @ShowAll parameter is passed because group should note differentiate between users and leads

			htParams.Add("@Group", ViewState["GroupID"].ToString());
		    }
		    // add a group number if one is avaialbe 
		    if (ViewState["Recycle"] != null)
		    {
			//this.Label1.Text = "--> Recycle Bin";
			htParams.Add("@Recycle", ViewState["Recycle"].ToString());
		    }
		    // add a group number if one is avaialbe 
		    if (ViewState["ExchangeUser"] != null)
		    {
			//this.Label1.Text = "--> Users";
			htParams.Add("@ExchangeUser", ViewState["ExchangeUser"].ToString());
			//btnDelete.Visible = false;
		    }

		    // filter by Type of leads whether they are suppliers of producers
		    //if (ddlResinFilter.SelectedItem.Value != "All")
		    //{
		    //    // be sure to subtract one from the contract number or the thing returns the wrong amount
		    //    htParams.Add("@resin_filter", (Convert.ToInt16(ddlResinFilter.SelectedValue) - 1).ToString());
		    //}

		    ListItem old_selectedValue = ddlUserTitle.SelectedItem;
		    ddlUserTitle.Items.Clear();
		    ddlUserTitle.Items.Add("All");

		    using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
		    {
			SqlDataReader dtr = DBLibrary.GetDataReaderStoredProcedure(conn, "spInitializeCRM_DDL_CATEGORY", htParams);

			while (dtr.Read())
			{
			    if ((dtr["PERS_CATEGORY"] != DBNull.Value) && (dtr["PERS_CATEGORY"].ToString().Trim() != ""))
			    {
				ddlUserTitle.Items.Add(new ListItem(dtr["PERS_CATEGORY"].ToString() + "(" + dtr["NUM_LEADS"].ToString() + ")", dtr["PERS_CATEGORY"].ToString()));
			    }
			}
		    }
		    if (ddlUserTitle.Items.Contains(old_selectedValue))
		    {
			ddlUserTitle.SelectedValue = old_selectedValue.Value;
		    }
		    else
		    {
			ddlUserTitle.SelectedIndex = 0;
		    }
		}


		private void RefreshTreeView()
		{			
			LoadTreeViewNotes();
		}
		private void LoadTreeViewNotes()
		{			
			// Put user code to initialize the page here			
			// adding nodes		
			string brokerId="";
			foreach (NineRays.Web.UI.WebControls.FlyTreeView.TreeNode tn in this.FTV.Nodes)
			{
				if(tn.Expanded==NBool.True)
				{
					string[] str=tn.Key.Split('_');
					brokerId+=str[1]+",";					
				}
			}	
			if(brokerId.Length>0)
			{
				brokerId=brokerId.Substring(0,brokerId.Length-1);
			}
			//this.Label2.Text=brokerId;
			string[] broker=brokerId.Split(',');
			this.FTV.Nodes.Clear();
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
			
				// opening contracts list
				this.FTV.Nodes.Clear();
				if ((string)Session["Typ"] == "A" || (string)Session["Typ"] == "L")
				{
					NineRays.Web.UI.WebControls.FlyTreeView.TreeNode mytn = new NineRays.Web.UI.WebControls.FlyTreeView.TreeNode();
					mytn.Key="Broker_Unallocated";
					mytn.Text="Unallocated Leads";
					mytn.ImageUrl="office2003_contacts.gif";				    
					this.FTV.Nodes.Add(mytn);
					mytn = new NineRays.Web.UI.WebControls.FlyTreeView.TreeNode();
					mytn.Key="Broker_All";
					mytn.Text=" All Leads";
					mytn.ImageUrl="office2003_contacts.gif";				    
					this.FTV.Nodes.Add(mytn);				
					int nLeads;
					using (SqlDataReader dtrBroker = DBLibrary.GetDataReaderFromSelect(conn, "select NAME = CAST(PERS_FRST_NAME as varchar) +' ' +CAST(PERS_LAST_NAME as varchar) ,PERS_ID from person where ( PERS_TYPE='B' or PERS_TYPE='T' OR PERS_TYPE='L' or PERS_TYPE='A') AND PERS_ENBL='1' ORDER BY NAME"))
					{
						while (dtrBroker.Read())
						{	
							Hashtable param = new Hashtable();
							param.Add("@BrokerId", dtrBroker["pers_id"]);
							nLeads = (int)DBLibrary.ExecuteScalarStoredProcedure(Application["DBconn"].ToString(),"spCalculateTotalLeads", param);

							NineRays.Web.UI.WebControls.FlyTreeView.TreeNode tn = new NineRays.Web.UI.WebControls.FlyTreeView.TreeNode();
							tn.Key="Broker_"+dtrBroker["pers_id"].ToString();
							if(this.ViewState["ID"].ToString().Equals(dtrBroker["pers_id"].ToString()))
							{
								//tn.Text="<a href='#' id=default_"+dtrBroker["pers_id"].ToString()+" ondragenter='handleDragEnter()' ondrop='drop()' ondragover='cancelEvent()' ondragleave='handleDragLeave()'><FONT color=#0000ff><b>"+dtrBroker["Name"].ToString()+"</b></font></a>";
								tn.Text="<a href='#' id=default_"+dtrBroker["pers_id"].ToString()+" ondragenter='handleDragEnter()' ondrop='drop()' ondragover='cancelEvent()' ondragleave='handleDragLeave()' class='red'>"+dtrBroker["Name"].ToString()+ " (" + nLeads + ")" +"</a>";
							}
							else
							{
								tn.Text="<a href='#' id=default_"+dtrBroker["pers_id"].ToString()+" ondragenter='handleDragEnter()' ondrop='drop()' ondragover='cancelEvent()' ondragleave='handleDragLeave()'>"+dtrBroker["Name"].ToString()+ " (" + nLeads + ")" +"</a>";
							}
							tn.ImageUrl="office2003_contacts.gif";					
							//CreatSubNotes(tn,dtrBroker["pers_id"].ToString(),dtrBroker["pers_id"].ToString());
							tn.NodeSrc="subnote.aspx?persId="+dtrBroker["pers_id"].ToString();
							tn.ContextMenuID="FlyContextMenu2";
							tn.DragDropAcceptNames="b";
							tn.DragDropName="a";
							tn.DragDropJavascript="return true;";	
							bool flag=false;
							for(int i=0;i<broker.Length;i++)
							{
								if(broker[i].Equals(dtrBroker["pers_id"].ToString()))
								{
									flag=true;
								}
								//this.Label2.Text+=broker[i]+"_";
							}
							if(flag)
							{
								tn.Expanded=NBool.True;		
							}
							this.FTV.Nodes.Add(tn);	
						}
					}	// end using dtrBroker					
				}
				else
				{
					// opening 
					Hashtable htParam2 = new Hashtable();
					htParam2.Add("@ID",this.ViewState["ID"].ToString());
					using (SqlDataReader dtrGroup = DBLibrary.GetDataReaderFromSelect(conn,"SELECT NAME = CAST(PERS_FRST_NAME as varchar) +' ' +CAST(PERS_LAST_NAME as varchar) ,PERS_ID from person where pers_id=@ID",htParam2))
					{
						//binding dd list
						if (dtrGroup.Read())
						{
							NineRays.Web.UI.WebControls.FlyTreeView.TreeNode tn = new NineRays.Web.UI.WebControls.FlyTreeView.TreeNode();
							tn.Key="Broker_"+dtrGroup["pers_id"].ToString();
							tn.Text="<a href='#' id=default_"+dtrGroup["pers_id"].ToString()+" ondragenter='handleDragEnter()' ondrop='drop()' ondragover='cancelEvent()' ondragleave='handleDragLeave()'>"+dtrGroup["Name"].ToString()+"</a>";
							tn.ImageUrl="office2003_contacts.gif";					
							//CreatSubNotes(tn,dtrBroker["pers_id"].ToString(),dtrBroker["pers_id"].ToString());
							tn.NodeSrc="subnote.aspx?persId="+dtrGroup["pers_id"].ToString();
							tn.ContextMenuID="FlyContextMenu2";
							//bool flag=false;
							//for(int i=0;i>broker.Length;i++)
							//{
							//if(broker[i].Equals(dtrBroker["pers_id"].ToString()))
							//{
							//flag=true;
							//}
							//this.Label2.Text+=broker[i]+"_";
							//}
							//if(flag)
							//{
							tn.Expanded=NBool.True;		
							//}
							this.FTV.Nodes.Add(tn);	
						}	
					}				
				}
			}	// end using conn
		}
		
		private void MoveToGroup(string id)
		{
			Hashtable htParams = new Hashtable();

			if(!id.Equals("recycle"))
			{
				string[] myid=id.Split('_');
				if(myid[0].Equals("default"))
				{
					
					if(this.ViewState["State"]!=null&&this.ViewState["State"].ToString()!="recycle")
					{
						this.RemoveGroupMember(myid[1].ToString());
					}
					else
					{
						this.Restore(myid[1].ToString());
					}
				}
				else
				{
					string GroupId=id;
					string message="The following leads: ";

					string GroupName="";
					//string PersonId=strid[1];
					string brokerId="";			
					using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
					{
						conn.Open();
						htParams.Add("@ID",GroupId);
						using (SqlDataReader rd=DBLibrary.GetDataReaderFromSelect(conn,"select * from [group] where group_id=@ID",htParams))
						{
							if(rd.Read())
							{
								brokerId=rd["group_broker"].ToString();
								GroupName=rd["GROUP_NAME"].ToString();
							}
						}
						
						StringBuilder sbSQL = new StringBuilder();	
						//this.dg.SelectedItems[0]
						bool bFound =false; 
						
						for (int selectedIndex=0; selectedIndex< this.dg.SelectedIndexes.Count;selectedIndex++)
						{
							int selIndex = Convert.ToInt32(dg.SelectedIndexes[selectedIndex]);
							htParams.Clear();
							htParams.Add("@GroupID",GroupId);
							htParams.Add("@BrokerId",brokerId);
							htParams.Add("@GroupMemberContactID",dg.DataKeys[selIndex].ToString());
							
							using (SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn,"SELECT * FROM GROUP_MEMBER WHERE (GROUP_MEMBER_GROUP_ID=@GroupID AND GROUP_MEMBER_BROKER_ID = @BrokerId AND GROUP_MEMBER_CONTACT_ID=@GroupMemberContactID)",htParams))
							{
								if (!dtr.Read())
								{
									sbSQL.Append("delete from GROUP_MEMBER where group_member_contact_id='" + dg.DataKeys[selIndex].ToString() + "';");
									sbSQL.Append("INSERT INTO GROUP_MEMBER( GROUP_MEMBER_GROUP_ID,GROUP_MEMBER_BROKER_ID,GROUP_MEMBER_CONTACT_ID) VALUES ('"+GroupId+"','"+brokerId+"','"+dg.DataKeys[selIndex].ToString()+"') ;");
									sbSQL.Append("UPDATE PERSON SET PERS_ACES="+brokerId+" WHERE PERS_ID='"+dg.DataKeys[selIndex].ToString()+"';");
									
									if(ViewState["Recycle"]!=null)
									{
										sbSQL.Append("UPDATE PERSON SET PERS_ENBL='1' WHERE PERS_ID='"+dg.DataKeys[selIndex].ToString()+"';");
									}
									
									bFound = true;									
								}
							}							
						}
						
						if (bFound) // only execute if at least one instance has been selected.
						{				
							SqlCommand cmd = new SqlCommand(sbSQL.ToString(),conn);
							cmd.ExecuteNonQuery();
							cmd.Dispose();	
							message=message.Substring(0,message.Length-1);
							message+=" have moved to Folder: "+GroupName + ".";

							//***** 08-14-2008 CF: Changed SQL above to delete person from all groups.  No need to run this.
							//if(this.ViewState["State"]!=null && this.ViewState["State"].ToString().Equals("Group"))
							//{
							//    this.RemoveGroupMember();
							//}

						}
						else
						{
							message="Nothing was moved.";
						}
					}	// end using conn


					this.Label1.Text=message;													
				}
			}
			else
			{
				DeleteGroupMember();				
			}
            			
		}
		
		public void EditFile_Click(object sender, System.EventArgs e)
		{
			string strEdit = editnote.Value;
			if(strEdit != null)
			{
				string nodeId=DBLibrary.ScrubSQLStringInput(strEdit);
				string[] groupId=nodeId.Split('_');
				string groupName=groupId[3].ToString();
				using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
				{
					conn.Open();
					System.Text.StringBuilder sbSel=new StringBuilder("");
					sbSel.Append("select con=count(*) from [group] where group_name='"+groupName+"' and group_broker="+groupId[2]);
					System.Text.StringBuilder sbSql=new StringBuilder("");
					sbSql.Append("update [group] set group_name='"+groupName+"' where group_id="+groupId[1]+";select mycount=count(*) from GROUP_MEMBER WHERE GROUP_MEMBER_GROUP_ID ="+groupId[1]+" AND GROUP_MEMBER_BROKER_ID='"+groupId[2]+"'");
					SqlCommand cmd=new SqlCommand(sbSel.ToString(),conn);
					SqlDataReader count=cmd.ExecuteReader();
					int n=1;
					try
					{
						if(count.Read())
						{
							n=Convert.ToInt32(count["con"].ToString());
						}
					}
					finally
					{
						count.Close();
					}

					if(n==0)
					{
						cmd=new SqlCommand(sbSql.ToString(),conn);
						SqlDataReader rd=cmd.ExecuteReader();
						try
						{
							if(rd.Read())
							{
								groupName=groupName+"("+rd["mycount"].ToString()+")";
							}	
						}
						finally
						{
							rd.Close();	
						}
						this.Label1.Text="";
					}
					else
					{
						this.Label1.Text="The Name is repeat!";
					}
				}				
				this.BindDG();
				this.RefreshTreeView();
			}
		}
		
		public void CreateFile_Click(object sender, System.EventArgs e)
		{
			string strFuncParam = funcParam.Value;
			if(strFuncParam != null)
			{
				//this.Label1.Text=Request["funcParam"].ToString();	
				this.message.Text="";
                
				MoveToGroup(strFuncParam);	
				
				this.RefreshTreeView();
				
				if(this.ViewState["Recycle"]!=null)
				{
					this.ViewState["State"]=null;
					ViewState["Recycle"]="someone";
					ViewState["ExchangeUser"]=null;
					ViewState["Unexchangeuser"]=null;
				}
				else
				{
					if(this.ViewState["State"]!=null&&ViewState["State"].ToString().Equals("Broker"))
					{				
						
						ViewState["Recycle"]=null;
						ViewState["ExchangeUser"]=null;
						ViewState["Unexchangeuser"]="someone";
					}
					else
					{
						ViewState["Recycle"]=null;
						ViewState["ExchangeUser"]=null;
						ViewState["Unexchangeuser"]=null;
					}
				}
				//this.ViewState["State"]="Group";
				this.BindDG();
			}				
		}
		

		public void AddFile_Click(object sender,System.EventArgs e)
		{
		        string strFuncParam = funcParam.Value;
		        if (strFuncParam != null)
			{
				string nodeId=DBLibrary.ScrubSQLStringInput(addnote.Value);
				string[] groupId=nodeId.Split('_');
				string tName="";
				tName=groupId[2];
				SqlConnection conn;
				conn = new SqlConnection(Application["DBconn"].ToString());
				try
				{
					conn.Open();
					System.Text.StringBuilder sbSql=new StringBuilder("");
					System.Text.StringBuilder sbSel=new StringBuilder("");
					sbSel.Append("select con=count(*) from [GROUP] where  GROUP_NAME='"+tName+"' and GROUP_BROKER="+groupId[1]);
					SqlCommand cmdsel=new SqlCommand(sbSel.ToString(),conn);
					System.Data.SqlClient.SqlDataReader selrd=cmdsel.ExecuteReader();
					int n=1;
					try
					{
						if(selrd.Read())
						{
							n=(int)selrd["con"];
						}
					}
					finally
					{
						selrd.Close();
					}
					if(n==0)
					{
						sbSql.Append("INSERT INTO [GROUP] ( GROUP_NAME,GROUP_BROKER) VALUES ('"+tName+"','"+groupId[1]+"')");
						SqlCommand cmd=new SqlCommand(sbSql.ToString(),conn);
						cmd.ExecuteNonQuery();
						this.RefreshTreeView();
						this.Label1.Text="Add New Group;"+tName+"  successfully";
					}
					else
					{
						this.Label1.Text="Add New Group fails the name is repeat";
					}				
					this.BindDG();
				}
				finally
				{
					conn.Close();
				}
			}
			else
			{
				this.Label1.Text="Add New Group fails";
			}
			
		}
		private string GetLastNotes(string persID)
		{
			string lastNotes = "";
			//DBLibrary db = new DBLibrary(this.Context);
			//db.OpenConnection();
			DataTable dtR = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(),"select TOP 1 cmnt_text LASTNOTE, cmnt_date from comment where cmnt_pers = "+DBLibrary.ScrubSQLStringInput(persID)+" order by cmnt_date desc");
			if (dtR.Rows.Count > 0)
			{
				lastNotes = HelperFunction.getSafeStringFromDB(dtR.Rows[0]["LASTNOTE"]);
			}
			
			return lastNotes;
		}
		
		protected void OnDataBound(object sender, Telerik.WebControls.GridItemEventArgs  e)
		{	                                            
			if (e.Item.ItemType == Telerik.WebControls.GridItemType.Item || e.Item.ItemType == Telerik.WebControls.GridItemType.AlternatingItem)
			{            
				e.Item.Attributes["onmousemove"]="handleMouseMove()";
				e.Item.Attributes["ondragstart"]="handleDragStart()";
									
				//Market
				int market_index = dg.Columns.FindByDataField("PERS_PRMLY_INTRST").OrderIndex;
				
				if(e.Item.Cells[market_index].Text.Trim().Equals("2"))
				{
					e.Item.Cells[market_index].Text="I";			
				}
				else
				{
					e.Item.Cells[market_index].Text="D";
				}
												
				if(cbDetails.Checked)
				{
					LoadDetails(e);
				}

				int comp_index = dg.Columns.FindByDataField("PERS_COMPANY").OrderIndex;

				string sDetailBox = "<a style='color:{0}; text-decoration:underline;cursor:pointer' onClick=window.open('/administrator/user_Details.aspx?ID=" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "PERS_ID")) + "','win','scrollbars=yes,resizable=yes,width=550,height=565')>" + e.Item.Cells[comp_index].Text + "</a>";

				// modal window example.  not used because post back caused UserDetails.aspx to open in yet another window							
				//Resin Processor" Value="2"
				//Broker/Distributor/Trader" Value="3"
				//Resin Producer" Value="4"
				//Analyst/Market Observer" Value="5" 

				switch (Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "PERS_STAT")))
				{							
					case 1:												
			                        e.Item.Cells[comp_index].Text = string.Format(sDetailBox, "red");
						break;
					case 2:
						//green
						e.Item.Cells[comp_index].Text = string.Format(sDetailBox, "green");
						break;
					case 3:
						e.Item.Cells[comp_index].Text = string.Format(sDetailBox, "orange");						
						break;
					case 4:
						e.Item.Cells[comp_index].Text = string.Format(sDetailBox, "blue");						
						break;					
					default:
						e.Item.Cells[comp_index].Text = string.Format(sDetailBox, "black");
						break;
				}
             
			}
			
		}

		private void LoadDetails(Telerik.WebControls.GridItemEventArgs e)
		{
		    //***** CEF Code to load the details from checkbox

		    SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());
		    SqlCommand cmdResin;
		    SqlDataReader dtrResin;
		    string strActionBoxHTML = "";
		    string market = "Domestic";

		    string strPref = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "PERS_PREF"));

		    //PERS_INTRS_SIZE
		    string size = "";
		    int size_index = dg.Columns.FindByDataField("PERS_INTRST_SIZE").OrderIndex;
		    if (e.Item.Cells[size_index].Text.Substring(0, 1) == "1") size += ", HC";
		    if (e.Item.Cells[size_index].Text.Substring(1, 1) == "1") size += ", BT";
		    if (e.Item.Cells[size_index].Text.Substring(2, 1) == "1") size += ", TL";
		    if (size.Trim().Length > 0) size = size.Substring(2); else size = " - ";


		    //PERS_INTRST_QUALT
		    string quality = "";
		    int qualit_index = dg.Columns.FindByDataField("PERS_INTRST_QUALT").OrderIndex;
		    if (e.Item.Cells[qualit_index].Text.Substring(0, 1) == "1") quality += ", Prime";
		    if (e.Item.Cells[qualit_index].Text.Substring(0, 1) == "1") quality += ", Offgrade";
		    if (e.Item.Cells[qualit_index].Text.Substring(0, 1) == "1") quality += ", Regrind";
		    if (quality.Trim().Length > 0) quality = quality.Substring(2); else quality = " - ";


		    int comp_index = dg.Columns.FindByDataField("PERS_COMPANY").OrderIndex;
					
		    string strSQL;

		    conn.Open();

		    //Details
		    strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]='<div class=\"menuitems\"></div>'" + ((char)13).ToString();
		    //Market...
		    strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<div class=\"menuitems\">Delivery: " + market + "</div>'" + ((char)13).ToString();
		    strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<hr>' //Optional Separator" + ((char)13).ToString();
		    //Size
		    strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<div class=\"menuitems\">Size: " + size + "</div>'" + ((char)13).ToString();
		    strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<hr>' //Optional Separator" + ((char)13).ToString();
		    //Quality
		    strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<div class=\"menuitems\">Quality: " + quality + "</div>'" + ((char)13).ToString();
		    strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<hr>' //Optional Separator" + ((char)13).ToString();
		    //Notes
		    string lastNotes = GetLastNotes(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "PERS_ID")));
		    if (lastNotes.Length > 100) lastNotes = lastNotes.Substring(0, 100) + "...";
	            
		    lastNotes = lastNotes.Replace("\r\n", "");
		    lastNotes = lastNotes.Replace("#", "number");
		    lastNotes = lastNotes.Replace("'", "");
	            
	            
		    strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<div class=\"menuitems\">Last Note: " + lastNotes + "</div>'" + ((char)13).ToString();
		    strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<hr>' //Optional Separator" + ((char)13).ToString();
		    //Contracts
		    string count = HelperFunction.CountSelectedResins(this.Context, Convert.ToInt32(strPref));
	            
		    strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<div class=\"menuitems\">Selected Resins:" + count + "</div>'" + ((char)13).ToString();
		    strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<hr>' //Optional Separator" + ((char)13).ToString();

	            
	            
		    //Use "POWER" method to find preference
		    strSQL = "SELECT CONT_LABL,CONT_ID FROM CONTRACT WHERE POWER(2,CONT_ID-1)&(" + strPref + ")<>0 ORDER BY CONT_ORDR";
		    cmdResin = new SqlCommand(strSQL, conn);
		    dtrResin = cmdResin.ExecuteReader();            
		    try
		    {
			while (dtrResin.Read())
			{
			    strActionBoxHTML += "linkset[" + iLinkSetCount.ToString() + "]+='<div class=\"menuitems\">" + dtrResin["CONT_LABL"].ToString() + "</div>'" + ((char)13).ToString();
			}
		    }
		    finally
		    {
			dtrResin.Close();
		    }

		    this.phActionButton.Controls.Add(new LiteralControl(strActionBoxHTML));

		    // setting the mouseover color
		    e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");

		    if (e.Item.ItemType == Telerik.WebControls.GridItemType.Item)
		    {
			e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee'");
		    }
		    else
		    {
			e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3'");
		    }
		    // add scripts for 'action boxes'

		    //PERS_COMPANY

		    e.Item.Cells[comp_index].Attributes.Add("onmouseover", "showmenu(event,linkset[" + iLinkSetCount.ToString() + "])");
		    e.Item.Cells[comp_index].Attributes.Add("onMouseout", "delayhidemenu()");
		    iLinkSetCount++;

		    conn.Close();

		}

	        
		//private void BindResin(System.Data.SqlClient.SqlConnection conn)
		//{
		//    SqlCommand cmdContract;
		//    SqlDataReader dtrContract;
		//    // opening contracts list
		//    cmdContract = new SqlCommand("SELECT CONT_LABL,CONT_ID FROM CONTRACT ORDER BY CONT_ORDR",conn);
		//    dtrContract = cmdContract.ExecuteReader();
		//    //binding company dd list
		//    ListItem lst = new ListItem ("All","All" );
		//    ddlResinFilter.Items.Add ( lst );
		//    try
		//    {
		//        while (dtrContract.Read())
		//        {
		//            lst = new ListItem ((string) dtrContract["CONT_LABL"],dtrContract["CONT_ID"].ToString() );
		//            ddlResinFilter.Items.Add ( lst);
		//        }
		//    }
		//    finally
		//    {
		//        dtrContract.Close();
		//    }
		//}
		private void BindState(SqlConnection conn)
		{
			SqlCommand cmdState;
			SqlDataReader dtrState;
			// opening 
			cmdState = new SqlCommand("SELECT STAT_CODE FROM STATE WHERE STAT_CTRY='US' ORDER BY STAT_CODE",conn);
			dtrState = cmdState.ExecuteReader();
			//binding dd list
			ddlState.Items.Add ( new ListItem ("All","All" ) );
			try
			{
				while (dtrState.Read())
				{

					ListItem lst = new ListItem ((string) dtrState["STAT_CODE"],dtrState["STAT_CODE"].ToString() );
					// US map will pass the state parameter.  this should default the ddl
					if (Request.QueryString["State"] != null)
					{
						if (Request.QueryString["State"].ToString().ToUpper().Equals(lst.Text))
						{
							lst.Selected = true;
						}
					}
					ddlState.Items.Add (lst );
				}
			}
			finally
			{
				dtrState.Close();		
			}

		}
		private void ClearSearchSession()
		{
			//this.ddlMarketFilter.SelectedItem.Selected=false;
			//this.ddlMarketFilter.Items.FindByValue("*").Selected=true;
		        this.rblMarketFilter.SelectedValue = "*";
			this.ddlState.SelectedItem.Selected=false;
			this.ddlState.Items.FindByValue("All").Selected=true;
			//this.ddlPageSize.SelectedItem.Selected=false;
			//this.ddlPageSize.Items.FindByText("200").Selected=true;
			//this.ddlResinFilter.SelectedItem.Selected=false;
			//this.ddlResinFilter.Items.FindByText("All").Selected=true;
			//this.cbAllResin.Checked=true;
			this.txtSearch.Text="";
		}
		private void InitializeCRM()
		{
			// sets the default sorting for the screen
			ViewState["Sort"] ="PERS_ID DESC";  
			
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
				// default the ViewState to the users ID
				// this can be manipualated later
				// Bindign Contract Filter
						
				//BindResin(conn);
						
				// binding state Filter ddl		
						
				BindState(conn);
			}
			
			//	PutSearchFieldsBackSession();

			if (((Session["ID"].ToString() == "2") || (Session["ID"].ToString() == "13564")) && (Request.QueryString["EmailOffers"] == "true"))
			{
				OnLinkClickTradingPartners(); // Send Mike directly to "Trading parters" folder

				this.ddlPageSize.Items.FindByText("50").Selected = false;
				this.ddlPageSize.Items.FindByText("100").Selected = false;
				this.ddlPageSize.Items.FindByText("1000").Selected = false;
				this.ddlPageSize.Items.FindByText("5000").Selected = false;
				this.ddlPageSize.Items.FindByText("200").Selected = true;

				dg.PageSize = System.Convert.ToInt32(ddlPageSize.SelectedValue.ToString());
				dg.MasterTableView.PageSize = System.Convert.ToInt32(ddlPageSize.SelectedValue.ToString());
			}
			//lblMessageError.Text = "";	
			// must be called every load or LinkButton Handler didn't seem to work
			// build group drop down list
			// we have offers to be emailed then display dialog box on the top
			if (Session["strEmailOffer_IDS"]!=null)
			{
				pnEmail_Offer.Visible = true;
			}

			// only Mike should be allowed to see who is allocated to whom
		        if (!Session["ID"].ToString().Equals("2") && !Session["ID"].ToString().Equals("13564"))
			{
				dg.Columns.FindByDataField("PERS_ALLOCATED").Visible = false;
			}
			DisplayAllocateButton();

			isCorp_IDCheck = false;
			isHDPECheck = false;
			isLDPECheck = false;
			isPSPPCheck = false;

			BindDG();

		}

		private void OnLinkClickTradingPartners()
		{
			// expand to Michael's Trading Partners node:
			//
			string[] TradingPartnersNodeList = {"Broker_2","Group_32_2"};
			FTV.ExpandToNodeKeyList = TradingPartnersNodeList;
			FTV.SelectedNodeKeyList = TradingPartnersNodeList;
			
			// snip taken from FTV_SelectedNodeChanged:
			this.operation="folder";
			this.ViewState["Broker"]="2";
			this.ViewState["State"]="Group";
			ViewState["Recycle"]=null;
			ViewState["ExchangeUser"]=null;
			ViewState["Unexchangeuser"]=null;
			ViewState["Unallocated"]=null;
			ViewState["GroupID"]="32";

			ViewState["GroupName"]="Trading Partners";
			this.Label1.Text="Group: " + "<a href='#' id=32 ondragenter='handleDragEnter()' ondrop='drop()' ondragover='cancelEvent()' ondragleave='handleDragLeave()'>Trading partners</a>";
			DisplayAllocateButton();
			this.btnRemoveGroupMember.Visible=true;
			this.btnRestore.Visible=false;
			this.btnEmailOffer.Visible=true;            
			this.btnEmailLetter.Visible=true;	
			this.btnDelete.Visible=true;

			ClearSearchSession();
			this.dg.CurrentPageIndex=0;
			this.BindDG();
			// end snip
		}
	
		#region Web
		override protected void OnInit(EventArgs e)
		{
			
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
	
		/// </summary>
		private void InitializeComponent()
		{    
			this.FTV.SelectedNodeChanged += new NineRays.Web.UI.WebControls.FlyTreeView.SelectedNodeChangedEventHandler(this.FTV_SelectedNodeChanged);
			this.FlyContextMenu1.MenuItemClick += new NineRays.Web.UI.WebControls.FlyTreeView.MenuItemClickEventHandler(this.FlyContextMenu1_MenuItemClick);
			this.FlyContextMenu2.MenuItemClick += new NineRays.Web.UI.WebControls.FlyTreeView.MenuItemClickEventHandler(this.FlyContextMenu1_MenuItemClick);

            this.ddlUserTitle.SelectedIndexChanged += new EventHandler(ddlUserTitle_SelectedIndexChanged);

            this.dg.PageIndexChanged += new Telerik.WebControls.GridPageChangedEventHandler(this.dg_PageIndexChanged);                        			
            this.dg.ItemDataBound += new Telerik.WebControls.GridItemEventHandler(this.OnDataBound);
			this.dg.SortCommand += new Telerik.WebControls.GridSortCommandEventHandler(this.dg_SortCommand);

		}
		
		#endregion

		private void FlyContextMenu1_MenuItemClick(object sender, NineRays.Web.UI.WebControls.FlyTreeView.MenuItemClickEventArgs e)
		{
			
			if(e.MenuItemKey.Equals("delete"))
			{
				NineRays.Web.UI.WebControls.FlyTreeView.TreeNode tn=FTV.Nodes.FindNode(e.NodeKeyList);	
				string[] groupId=tn.Key.Split('_');
				FTV.Nodes.FindNode(e.NodeKeyList).Parent.Nodes.Remove(tn);
				using (SqlConnection conn  = new SqlConnection(Application["DBconn"].ToString()))
				{
					conn.Open();
					System.Text.StringBuilder sbSql=new StringBuilder("");
					sbSql.Append("delete group_member where group_member_group_id="+groupId[1]+"; delete [group] where group_id="+groupId[1]);
					SqlCommand cmd=new SqlCommand(sbSql.ToString(),conn);
					cmd.ExecuteNonQuery();
					this.Label1.Text="";
					this.BindDG();
				}
			}

            //Larry -- user right click the group under trader and click email
            //Updating: Right-Click on the Trader and click Email 
            if (e.MenuItemKey.Equals("email"))
            {
                using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
                {
                    NineRays.Web.UI.WebControls.FlyTreeView.TreeNode tn = FTV.Nodes.FindNode(e.NodeKeyList);
                    StringBuilder sbEmails = new StringBuilder();
                    bool bFound = false;

                    conn.Open();
                    SqlCommand cmd;
                    SqlDataReader dtr;
                    string BrokerId = e.NodeKeyList[0].Split('_')[1];
                    string sql = "SELECT GROUP_ID FROM [GROUP] WHERE GROUP_BROKER = " + BrokerId;

                    cmd = new SqlCommand(sql, conn);
                    dtr = cmd.ExecuteReader();

                    while (dtr.Read())
                    {
                        string GroupId = dtr["GROUP_ID"].ToString();
                        string[] keyGroup = { "Broker_" + BrokerId, "Group_" + GroupId + "_" + BrokerId };

                        NineRays.Web.UI.WebControls.FlyTreeView.TreeNode tn1 = FTV.Nodes.FindNode(keyGroup);

                        if (tn.IsChecked || (tn1 != null && tn1.IsChecked))
                        {
                            using (SqlConnection conn1 = new SqlConnection(Application["DBconn"].ToString()))
                            {
                                conn1.Open();
                                SqlCommand cmd1;
                                SqlDataReader dtr1;
                                string sql1 = "select pers_id from person where pers_id in (select group_member_contact_id from group_member where group_member_group_id = " + GroupId + ")";

                                cmd1 = new SqlCommand(sql1, conn1);
                                dtr1 = cmd1.ExecuteReader();

                                while (dtr1.Read())
                                {
                                    sbEmails.Append(dtr1["PERS_ID"] + ",");
                                    bFound = true;
                                }
                            }
                        }
                    }
                    if (bFound) // only execute if at least one instance has been selected.
                    {
                        Session["strEmailOffer_Address"] = sbEmails.ToString();
                        Response.Redirect("/Administrator/SendEmail.aspx?Letter=1");
                    }
                }
            }


		}

		protected void dgPageChange(object sender, DataGridPageChangedEventArgs e)
		{

			dg.CurrentPageIndex = e.NewPageIndex;
			BindDG();

		}
		
		private void BindDG()
		{
            //initUserTitleDDL();

			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()+";Connect Timeout=30;"))
            //using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{                
				conn.Open();
                string temp = conn.ConnectionTimeout.ToString();
				System.Text.StringBuilder sbSQL = new System.Text.StringBuilder();
				DataSet dstContent;
				this.Button1.Visible=true;

				Hashtable htParams = new Hashtable();
				htParams.Add("@Sort",ViewState["Sort"].ToString());

				if (this.ViewState["Broker"] != null)
				{
					if(this.ViewState["State"]==null||this.ViewState["State"].ToString()!="Group")
					{
						htParams.Add("@ID", ViewState["Broker"].ToString());
					}
				}
				if(this.ViewState["Unallocated"]!=null)
				{
					htParams.Add("@Unallocated","someone");
				}
				// add selected market filter
				if (rblMarketFilter.SelectedValue !="*")
				{				
					htParams.Add("@MarketFilter",rblMarketFilter.SelectedValue.ToString());				
				}
			
			
				// add select user flag
                //if (!cbAllResin.Checked )
                //{
                //    // passes a value (hard coded) for the user preference that means all resin
                //    htParams.Add("@AllResin","113061887");
                //}
				// add search parameter
				if (txtSearch.Text != "" )
				{
					htParams.Add("@ShowAll","'True'");
					htParams.Add("@Search", txtSearch.Text);
				}
				else
				{
					htParams.Add("@ShowAll","'True'");
				}
			
				// filter by state if at varible is passed
				if (ddlState.SelectedValue.ToString() != "All" )
				{
					htParams.Add("@State", ddlState.SelectedValue.ToString());
				}
				if(ViewState["Unexchangeuser"]!=null)
				{
					htParams.Add("@Unexchangeuser","'someone'");
				}
				if (ViewState["GroupID"] != null )
				{
					//this.Label1.Text = "--> Group:" + ViewState["GroupName"].ToString();
					//if (txtSearch.Text == "" )  // this is set so that it doesn't pass the parameter twice during a search
					//{
					//	sbSQL.Append(",@ShowAll='True'");
					//}
					// add a group number if one is available
					// @ShowAll parameter is passed because group should note differentiate between users and leads
				
					htParams.Add("@Group",ViewState["GroupID"].ToString());				
				}
				// add a group number if one is avaialbe 
				if (ViewState["Recycle"] != null )
				{
					//this.Label1.Text = "--> Recycle Bin";
					htParams.Add("@Recycle", ViewState["Recycle"].ToString());				
				}
				// add a group number if one is avaialbe 
				if (ViewState["ExchangeUser"] != null )
				{
					//this.Label1.Text = "--> Users";
					htParams.Add("@ExchangeUser",ViewState["ExchangeUser"].ToString());
					//btnDelete.Visible = false;
				}
                
				// add selected corp ID
				if (isCorp_IDCheck)
				{
				    string strCorpID = "";
				    for (int i = 0; i < cblCorpID.Items.Count-1; i++)
				    {
					if (cblCorpID.Items[i].Selected)
					{
					    strCorpID = strCorpID + "'" + cblCorpID.Items[i].Value + "',";
					}
				    }

				    if (strCorpID.Length > 1)
				    {
					strCorpID = strCorpID.Substring(0, strCorpID.Length - 1);
					htParams.Add("@Corp_ID", strCorpID);
				    }else {
					isCorp_IDCheck = false;
				    }

				}
		                
				if (isHDPECheck || isLDPECheck || isPSPPCheck)
				{
					if( ddlPrefOption.Text == "Exclusive" )
					{
						htParams.Add("@PrefOption", "True");
					}

				    double ResinIndex = 0;

				    for (int i = 0; i < cblHDPE.Items.Count; i++)
				    {
					if (cblHDPE.Items[i].Selected)
					{
					    ResinIndex = ResinIndex + Math.Pow(2.0, (Convert.ToDouble(cblHDPE.Items[i].Value)-1));
					}
				    }
				    if (ResinIndex <= 0) { isHDPECheck = false; }
				    double temp1 = ResinIndex;
				    for (int i = 0; i < cblLDPE.Items.Count; i++)
				    {
					if (cblLDPE.Items[i].Selected)
					{
					    ResinIndex = ResinIndex + Math.Pow(2.0, (Convert.ToDouble(cblLDPE.Items[i].Value)-1));
					}
				    }
				    if (temp1 == ResinIndex) { isLDPECheck = false; }
				    temp1 = ResinIndex;
				    for (int i = 0; i < cblPSPP.Items.Count; i++)
				    {
					if (cblPSPP.Items[i].Selected)
					{
					    ResinIndex = ResinIndex + Math.Pow(2.0, (Convert.ToDouble(cblPSPP.Items[i].Value)-1));
					}
				    }
				    if (temp1 == ResinIndex) { isPSPPCheck = false; }

				    if (ResinIndex >0)
				    htParams.Add("@resin_filter", Convert.ToString(ResinIndex));                    
				}
                


				// filter by Type of leads whether they are suppliers of producers
				//if (ddlResinFilter.SelectedItem.Value != "All")
				//{
					// be sure to subtract one from the contract number or the thing returns the wrong amount
				//	htParams.Add("@resin_filter",(Convert.ToInt16(ddlResinFilter.SelectedValue)-1).ToString());
				//}

				// filter by Titles of Usert
				if (ddlUserTitle.SelectedItem.Value != "All")
				{
				    // be sure to subtract one from the contract number or the thing returns the wrong amount
				    htParams.Add("@user_title", ddlUserTitle.SelectedItem.Value);
				}

				dstContent = DBLibrary.GetDataSetFromStoredProcedure(conn, "spCRM", htParams);

				count.Text="Number of Leads in the grid: " + dstContent.Tables[0].Rows.Count;

				dg.DataSource = dstContent;
				
				try
				{
					dg.DataBind();	
				}
				catch
				{
					//Response.Write(ex.Message + " " + ex.InnerException);
				}

				this.count.Visible=true;
				if (dg.Items.Count==0) // only display the grid if there is data behind it
				{
					dg.Visible = false;
					this.btnEmailOffer.Visible=false;                    
					this.btnEmailLetter.Visible=false;	
					this.btnDelete.Visible=false;
					this.btnAllocate.Visible=false;
					this.btnEmailAllMyLeads.Visible=false;
					this.btnRemoveGroupMember.Visible=false;
					this.btnRestore.Visible=false;
					this.Button1.Visible=false;
				
					this.count.Visible=false;
					if(this.operation.Equals("search"))
					{
						this.Label3.Text="There are no customers that match the parameters requested";
					}
					else
					{
						this.Label3.Text="This folder is empty";
					}
					//this.Label1.Visible=false;
					//pnNoData.Visible = true;
					//lblLeadCount.Text = "";
				}				
				else
				{
					this.Label3.Text="";
					dg.Visible = true;
					//this.btnEmailOffer.Visible=true;
					//this.btnDelete.Visible=true;
					//this.btnAllocate.Visible=true;
					//this.btnEmailAllMyLeads.Visible=true;
					//this.btnRemoveGroupMember.Visible=true;
					//this.btnRestore.Visible=true;
					////pnNoData.Visible = false;
					////lblLeadCount.Text = dstContent.Tables["pubs"].Rows.Count.ToString();
					///
					//                this.count.Text="Number of Leads: "+ dg.
				}
			}
			
			KeepSearchFieldsSession();
			message.Visible = true;

		}
		
		private void KeepSearchFieldsSession()
		{
			Session["CRM.ddlSelectBroker.SelectedItem.Value"] =this.ViewState["Broker"];
			//Session["CRM.cbAllResin.Checked"] = cbAllResin.Checked;
			Session["CRM.ddlPageSize.SelectedItem.Value"] = ddlPageSize.SelectedItem.Value;
			//Session["CRM.ddlResinFilter.SelectedItem.Value"] = ddlResinFilter.SelectedItem.Value;
			Session["CRM.ddlState.SelectedItem.Value"] = ddlState.SelectedItem.Value;

			//if (ddlGroup.Items.Count>0) Session["CRM.ddlGroup.SelectedItem.Value"] = ddlGroup.SelectedItem.Value;
			Session["CRM.txtSearch.Text"] = txtSearch.Text;
			//Keeping the Group
			Session["CRM.GroupID"] = ViewState["GroupID"];
			Session["CRM.GroupName"] = ViewState["GroupName"];
		}
		private void FTV_SelectedNodeChanged(object sender, NineRays.Web.UI.WebControls.FlyTreeView.SelectedNodeChangedEventArgs e)
		{
			NineRays.Web.UI.WebControls.FlyTreeView.TreeNode tn=FTV.Nodes.FindNode(e.NewNodeKeyList);	
			string[] personId=tn.Key.Split('_');
			this.operation="folder";
			
			if(personId[0].Equals("Group"))
			{				
				if(personId[1].Equals("recycle"))
				{							
					ViewState["Recycle"]="someone";
					ViewState["ExchangeUser"]=null;
					ViewState["GroupID"]=null;
					ViewState["Unexchangeuser"]=null;
					ViewState["Unallocated"]=null;
					this.ViewState["State"]="recycle";
					this.ViewState["Broker"]=personId[2];
					this.Label1.Text="Recycle";
					this.btnAllocate.Visible=false;
					this.btnRemoveGroupMember.Visible=false;
					this.btnRestore.Visible=true;
					this.btnEmailOffer.Visible=false;                    
					this.btnDelete.Visible=false;					
				}
				else
				{										
					this.ViewState["Broker"]=personId[2];
					this.ViewState["State"]="Group";
					ViewState["Recycle"]=null;
					ViewState["ExchangeUser"]=null;
					ViewState["Unexchangeuser"]=null;
					ViewState["Unallocated"]=null;
					ViewState["GroupID"]=personId[1];
					ViewState["GroupName"]=tn.Text;	
					this.Label1.Text="Group:"+tn.Text;					
					DisplayAllocateButton();
					this.btnRemoveGroupMember.Visible=true;
					this.btnRestore.Visible=false;
					this.btnEmailOffer.Visible=true;
					this.btnEmailLetter.Visible=true;	
					this.btnDelete.Visible=true;
				}
				//}				
			}
			else
			{	
				
				if(personId[1].Equals("All"))
				{
					this.ViewState["State"]="All";
					this.ViewState["Broker"]=null;
					ViewState["GroupID"]=null;
					ViewState["GroupName"]=null;
					ViewState["Recycle"]=null;
					ViewState["ExchangeUser"]=null;
					ViewState["Unexchangeuser"]="";
					ViewState["Unallocated"]=null;
				}
				else
				{
					if(personId[1].Equals("Unallocated"))
					{
						this.ViewState["State"]="All";
						this.ViewState["Broker"]=null;
						ViewState["GroupID"]=null;
						ViewState["GroupName"]=null;
						ViewState["Recycle"]=null;
						ViewState["ExchangeUser"]=null;
						ViewState["Unexchangeuser"]="";
						ViewState["Unallocated"]="someone";
					}
					else
					{
						this.ViewState["State"]="Broker";
						this.ViewState["Broker"]=personId[1];
						ViewState["GroupID"]=null;					
						ViewState["ExchangeUser"]=null;
						ViewState["GroupName"]=null;
						ViewState["Unexchangeuser"]="someone";
						ViewState["Unallocated"]=null;
						ViewState["Recycle"]=null;
					}
				}
				if((tn.Text.Trim() != "All Leads") && (tn.Text.Trim() != "Unallocated Leads"))
				{
					this.Label1.Text="Broker:"+tn.Text;				
				}
				else
				{
					Label1.Text = tn.Text;
				}

				DisplayAllocateButton();
				this.btnRemoveGroupMember.Visible=false;
				this.btnRestore.Visible=false;
				this.btnEmailOffer.Visible=true;
				this.btnEmailLetter.Visible=true;	
				this.btnDelete.Visible=true;
				this.message.Text="";
			}			
			ClearSearchSession();
			this.dg.CurrentPageIndex=0;
			this.BindDG();
		}

		private void FTV_NodeEvent(object sender, NodeEventArgs e)
		{	
			
		}

		protected void btnGo_Click(object sender, System.EventArgs e)
		{
			dg.CurrentPageIndex = 0;
			this.operation="search";
			BindDG();
		}

		protected void btnCancelSendingOffers_Click(object sender, System.EventArgs e)
		{
			pnEmail_Offer.Visible = false;			
			Session["strEmailOffer_IDS"]=null;
			Session["strCompId"]=null;
			BindDG();
			this.Label1.Text = "";
		}

		protected void btnEmailAllMyLeads_Click(object sender, System.EventArgs e)
		{
			StringBuilder sbSQL = new StringBuilder();
			bool bFound =false;			
			//DBLibrary db = new DBLibrary(this.Context);			
			//db.OpenConnection();
			DataTable dtr = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(),"SELECT PERS_ID FROM PERSON WHERE PERS_ACES = " + Session["ID"].ToString());
			foreach (DataRow dr in dtr.Rows)
			{
				sbSQL.Append(dr["PERS_ID"].ToString()+",");
				bFound = true;
			}
//			dtr.Close();
//			db.CloseConnection();

			if (bFound) // only execute if at least one instance has been selected.
			{
				
				Session["strEmailOffer_Address"]= sbSQL.ToString();
//				Session["bEmailToAllMyLeads"]= "true";
				Response.Redirect ("/Administrator/SendEmail.aspx");
			}
		}

		protected void ddlPageSize_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dg.PageSize = System.Convert.ToInt32(ddlPageSize.SelectedValue.ToString());
			dg.MasterTableView.PageSize=System.Convert.ToInt32(ddlPageSize.SelectedValue.ToString());
			BindDG();
		}

		protected void ddlUserTitle_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		    BindDG();
		}
	        
		protected void cblMarketFilter_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dg.CurrentPageIndex = 0;
			BindDG();
		}

		protected void ddlState_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dg.CurrentPageIndex = 0;
			BindDG();
		}

		protected void btnEmailOffer_Click(object sender, System.EventArgs e)
		{
			
			StringBuilder sbSQL = new StringBuilder();			
			bool bFound =false;
			//			if (rsc.SelectedIndexes.Length<=200)
			//			{
			for (int selectedIndex=0; selectedIndex< this.dg.SelectedIndexes.Count;selectedIndex++)
			{
				int selIndex =Convert.ToInt32(dg.SelectedIndexes[selectedIndex]);
				sbSQL.Append(dg.DataKeys[selIndex].ToString()+",");
				bFound = true;
			}
			if (bFound) // only execute if at least one instance has been selected.
			{
				
				Session["strEmailOffer_Address"]= sbSQL.ToString();
//				Session["bEmailToAllMyLeads"] = "false";
				Response.Redirect ("/Administrator/SendEmail.aspx");
			}
			//			}			
		}


		private void DeleteGroupMember()
		{
			StringBuilder sbSQL = new StringBuilder();				
			bool bFound =false; 
//			bool mess=true;
			for (int selectedIndex=0; selectedIndex<this.dg.SelectedIndexes.Count;selectedIndex++)
			{
				int selIndex =Convert.ToInt32(this.dg.SelectedIndexes[selectedIndex]);				

				//condition that checks if lead is allocated
//				if(this.dg.SelectedItems[selectedIndex].Cells[15].Text.Trim()!="&nbsp;"&&this.dg.SelectedItems[selectedIndex].Cells[15].Text.Trim()!="")
//				{
					sbSQL.Append("UPDATE PERSON SET PERS_ENBL='0'  WHERE PERS_ID='"+dg.DataKeys[selIndex].ToString()+"';");
					if(this.ViewState["Broker"]!=null)
					{
						sbSQL.Append("DELETE FROM GROUP_MEMBER WHERE GROUP_MEMBER_BROKER_ID='"+DBLibrary.ScrubSQLStringInput(this.ViewState["Broker"].ToString())+"' AND GROUP_MEMBER_CONTACT_ID='"+dg.DataKeys[selIndex].ToString()+"';");
					}
					else
					{
						sbSQL.Append("DELETE FROM GROUP_MEMBER WHERE GROUP_MEMBER_BROKER_ID='"+DBLibrary.ScrubSQLStringInput(this.ViewState["ID"].ToString())+"' AND GROUP_MEMBER_CONTACT_ID='"+dg.DataKeys[selIndex].ToString()+"';");
					}
					bFound = true;
/*
			}
				else
				{					
					mess=false;
				}	
*/				
			}
			if (bFound) // only execute if at least one instance has been selected.
			{
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),sbSQL.ToString());
				//this.Label1.Text="Leaders you selected have moved to recycle";
				//this.Label1.Text=sbSQL.ToString();	
			}
/*
			if(!mess)
			{
				this.message.Text="Error:You can not delete leads that is not allocated to anyone ";
				//this.Label3.Text=sbSQL.ToString();
			}
			else
			{
				this.message.Text="";
			}
			*/
			BindDG();	
			RefreshTreeView();
			// build group drop down list
			//BindGroups();
		}
		protected void btnDelete_Click(object sender, System.EventArgs e)
		{			
			DeleteGroupMember();			
		}
		private void Restore()
		{
			StringBuilder sbSQL = new StringBuilder();			
			bool bFound =false; 
			for (int selectedIndex=0; selectedIndex< dg.SelectedIndexes.Count;selectedIndex++)
			{
				int selIndex = Convert.ToInt32(dg.SelectedIndexes[selectedIndex]);
				sbSQL.Append("UPDATE PERSON SET PERS_ENBL='1' WHERE PERS_ID='"+dg.DataKeys[selIndex].ToString()+"';");
				// Is this about Removing the item from all groups???
				//sbSQL.Append("DELETE FROM GROUP_MEMBER WHERE GROUP_MEMBER_BROKER_ID='"+Session["ID"].ToString()+"' AND GROUP_MEMBER_CONTACT_ID='"+dg.DataKeys[selIndex].ToString()+"';");
				bFound = true;
			}
			if (bFound) // only execute if at least one instance has been selected.
			{
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),sbSQL.ToString());
			}	
				
		}
		private void Restore(string id)
		{
			string strSQL = string.Empty;
			bool bFound =false; 
			for (int selectedIndex=0; selectedIndex< dg.SelectedIndexes.Count;selectedIndex++)
			{
				int selIndex = Convert.ToInt32(dg.SelectedIndexes[selectedIndex]);
				strSQL = "UPDATE PERSON SET PERS_ENBL='1',PERS_ACES="+id+"  WHERE PERS_ID='"+dg.DataKeys[selIndex].ToString()+"';";
				//
				// Is this about Removing the item from all groups???
				//sbSQL.Append("DELETE FROM GROUP_MEMBER WHERE GROUP_MEMBER_BROKER_ID='"+Session["ID"].ToString()+"' AND GROUP_MEMBER_CONTACT_ID='"+dg.DataKeys[selIndex].ToString()+"';");
				bFound = true;
			}
			if (bFound) // only execute if at least one instance has been selected.
			{
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),strSQL);				
			}	
				
		}
		protected void btnRestore_Click(object sender, System.EventArgs e)
		{
			this.message.Text="";
			Restore();
			RefreshTreeView();
			BindDG();		
		}

		protected void btnAllocate_Click(object sender, System.EventArgs e)
		{
			this.message.Text="";
			string strID="";			
			bool bFound =false; 
			for (int selectedIndex=0; selectedIndex< dg.SelectedIndexes.Count;selectedIndex++)
			{
				int selIndex = Convert.ToInt32(dg.SelectedIndexes[selectedIndex]);
				strID += dg.DataKeys[selIndex].ToString()+",";
				//sbSQL.Append("DELETE FROM GROUP_MEMBER WHERE GROUP_MEMBER_BROKER_ID='"+Session["ID"].ToString()+"' AND GROUP_MEMBER_CONTACT_ID='"+dg.DataKeys[selIndex].ToString()+"';");
				bFound = true;
			}
			if (bFound) // only execute if at least one instance has been selected.
			{
				Response.Redirect("/administrator/Allocate_Lead.aspx?User="+strID);
			}
			else  // nothing selected.  resetup the grid
			{
				BindDG();
				RefreshTreeView();
			}						
		}

		protected void ddlResinFilter_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dg.CurrentPageIndex = 0;
			BindDG();
		}
		
		private void RemoveGroupMember()
		{
			string strSQL = string.Empty;
			bool bFound =false; 
			for (int selectedIndex=0; selectedIndex< dg.SelectedIndexes.Count;selectedIndex++)
			{				
				int selIndex =Convert.ToInt32(dg.SelectedIndexes[selectedIndex]);
				strSQL = "DELETE FROM GROUP_MEMBER  WHERE GROUP_MEMBER_GROUP_ID ='"+DBLibrary.ScrubSQLStringInput(ViewState["GroupID"].ToString())+"' AND GROUP_MEMBER_BROKER_ID='"+DBLibrary.ScrubSQLStringInput(this.ViewState["Broker"].ToString())+"' AND GROUP_MEMBER_CONTACT_ID='"+dg.DataKeys[selIndex].ToString()+"' ;";				
				bFound = true;
			}
			if (bFound) // only execute if at least one instance has been selected.
			{
			
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),strSQL);
				this.Label1.Text="Leads you selected have moved to default folder";	
                		
			}
			else
			{
				this.Label1.Text="you move nothing";
			}
			// build group drop down list
			dg.CurrentPageIndex = 0;
			RefreshTreeView();
			BindDG();
		}
		
		private void RemoveGroupMember(string id)
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			try
			{
				conn.Open();
				StringBuilder sbSQL = new StringBuilder();				
				bool bFound =false; 
				for (int selectedIndex=0; selectedIndex< dg.SelectedIndexes.Count;selectedIndex++)
				{				
					int selIndex =Convert.ToInt32(dg.SelectedIndexes[selectedIndex]);
					if(this.ViewState["State"].ToString()=="Group")
					{
						sbSQL.Append("DELETE FROM GROUP_MEMBER  WHERE GROUP_MEMBER_GROUP_ID ='"+DBLibrary.ScrubSQLStringInput(ViewState["GroupID"].ToString())+"' AND GROUP_MEMBER_BROKER_ID='"+DBLibrary.ScrubSQLStringInput(this.ViewState["Broker"].ToString())+"' AND GROUP_MEMBER_CONTACT_ID='"+dg.DataKeys[selIndex].ToString()+"' ;");
					}				
					sbSQL.Append("UPDATE PERSON SET PERS_ACES="+DBLibrary.ScrubSQLStringInput(id)+" WHERE PERS_ID="+dg.DataKeys[selIndex].ToString()+" ;");
					bFound = true;
				
				}
				if (bFound) // only execute if at least one instance has been selected.
				{
				
					SqlCommand cmd = new SqlCommand(sbSQL.ToString(),conn);
					cmd.ExecuteNonQuery();
					cmd.Dispose();
					this.Label1.Text="Leads you selected have moved to default folder";	
                			
				}
				else
				{
					this.Label1.Text="you move nothing";
				}
			}
			finally
			{
				// build group drop down list
				conn.Close();
			}
			conn.Dispose();
			dg.CurrentPageIndex = 0;
//			int indexx = dg.Columns.FindByDataField("").;

			RefreshTreeView();
			BindDG();
		}

		protected void btnRemoveGroupMember_Click(object sender, System.EventArgs e)
		{
			this.message.Text="";
			RemoveGroupMember();		
		}

		private void dg_PageIndexChanged(object source, Telerik.WebControls.GridPageChangedEventArgs e)
		{
			dg.CurrentPageIndex = e.NewPageIndex;
			BindDG();
		}
		private void dg_SortCommand(object source, Telerik.WebControls.GridSortCommandEventArgs e)
		{
			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;    
			//  Parse the sort expression - delimiter space
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
			ColumnToSort = SortExprs[0];
			// If a sort order is specified get it, else default is descending
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DESC";
			}
    
			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;
    
			/*
			// Figure out the column index
			int iIndex;
			iIndex = 0;
			switch(ColumnToSort.ToUpper())
			{
				case "PERS_NAME":
					iIndex = 2;
					break;
				case "PERS_TITL":
					iIndex = 3;
					break;
				case "PERS_COMPANY":
					iIndex = 4;
					break;
				case "PERS_LAST_CHKD":
					iIndex = 6;
					break;
				case "STATE":
					iIndex = 8;
					break;
				case "PERS_PHON":
					iIndex = 9;
					break;
				case "PERS_DATE":
					iIndex = 10;
					break;
				case "PERS_ALLOCATED":
					iIndex = 12;
					break;	
				case "EMAIL_ENBL":
					iIndex = 10;
					break;
			}
			// alter the column's sort expression
			dg.Columns[iIndex].SortExpression = NewSortExpr;    
			*/
			dg.Columns.FindByDataField(ColumnToSort.ToUpper()).SortExpression = NewSortExpr;
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;    
			// store the new SortExpr in a labl for use by page function
			ViewState["Sort"] = NewSortExpr;
			//lblSort.Text =NewSortExpr;
			// Sort the data in new order
			
			BindDG();	
		}

		private void dg_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
		{
            
		}

		private void DisplayAllocateButton()
		{
			// only mike see the allocate button
            if (Session["ID"].ToString().Equals("2") || Session["ID"].ToString().Equals("13564"))
			{
				btnAllocate.Visible = true;
			}
			else
			{
				btnAllocate.Visible = false;
			}
		}


		protected void Button1_Click(object sender, System.EventArgs e)
		{
			foreach( Telerik.WebControls.GridItem dr in dg.Items)
			{
				dr.Selected=true;
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			string strSQL;

			foreach( Telerik.WebControls.GridItem dr in dg.Items)
			{				
				DropDownList dl = (DropDownList)dr.FindControl("ddl");
				if(dl.SelectedValue != null)
				{
					strSQL = "UPDATE PERSON SET PERS_NPE_MARKETING = " + dl.SelectedValue + 					
						" WHERE PERS_ID = " + dg.DataKeys[dr.ItemIndex].ToString()+
						" AND PERS_NPE_MARKETING <> " + dl.SelectedValue;

					DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),strSQL);
				}
			}
		}

		private void ddlNPEFilter_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dg.CurrentPageIndex = 0;
			BindDG();
		}

		protected void cbDetails_CheckedChanged(object sender, System.EventArgs e)
		{
			BindDG();
			Session["cbDetailState"] = ((CheckBox)sender).Checked;
		}

		protected void btnEmailLetter_Click(object sender, System.EventArgs e)
		{					
			StringBuilder sbSQL = new StringBuilder();			
			bool bFound =false;
			//			if (rsc.SelectedIndexes.Length<=200)
			//			{
			for (int selectedIndex=0; selectedIndex< this.dg.SelectedIndexes.Count;selectedIndex++)
			{
				int selIndex =Convert.ToInt32(dg.SelectedIndexes[selectedIndex]);
				sbSQL.Append(dg.DataKeys[selIndex].ToString()+",");
				bFound = true;
			}
			if (bFound) // only execute if at least one instance has been selected.
			{
				
				Session["strEmailOffer_Address"]= sbSQL.ToString();
				//				Session["bEmailToAllMyLeads"] = "false";
				Response.Redirect ("/Administrator/SendEmail.aspx?Letter=1");
			}
		}


		protected void dgAdmin_ItemDataBound2(object sender, Telerik.WebControls.GridItemEventArgs e)
		{
		    try
		    {

			if ((e.Item.ItemType != Telerik.WebControls.GridItemType.Header) && (e.Item.ItemType != Telerik.WebControls.GridItemType.Footer))
			{
			    if (e.Item.Cells.Count > 1)
			    {
				if (e.Item.Cells[19].Text != "&nbsp;")
				{
				    //e.Item.Cells[18].Text = dg.Columns.FindByDataField("USerCMT").ToString();
				    e.Item.Cells[19].Text = "<img src='/pics/Icons/icon_quotation_bubble.gif'>";
				    //e.Item.Cells[18].Text = "<img onclick='PreviewEmail(" + e.Item.Cells[2].Text + ")' src='/pics/Icons/icon_quotation_bubble.gif'>";
				}
				else
				{
				    //e.Item.Cells[18].Text =  dg.Columns.FindByDataField("USerCMT").ToString();
				    e.Item.Cells[19].Text = "<img src='/pics/Icons/comment.gif'>";
				}
			    }
			}

		    }
		    catch (Exception ex)
		    {


		    }
	              

		}

		protected void btnUpdate_Click(object sender, EventArgs e)
		{
		    BindDG();
		}

		protected void cblCorpIDIndex_Changed(object sender, System.EventArgs e)
		{
		    isCorp_IDCheck = true;
		}

		protected void cblHDPEIndex_Changed(object sender, System.EventArgs e)
		{
		    isHDPECheck = true;
		}
	        
		protected void cblLDPEIndex_Changed(object sender, System.EventArgs e)
		{
		    isLDPECheck = true;
		}
	        
		protected void cblPSPPIndex_Changed(object sender, System.EventArgs e)
		{
		    isPSPPCheck = true;
		}
        
    }
}
