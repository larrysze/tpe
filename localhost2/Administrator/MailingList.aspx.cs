using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;
using System.Text;

namespace localhost.Administrator
{
    public partial class MailingList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C") && ((string)Session["Typ"] != "L"))
            {
                Response.Redirect("../default.aspx");
            }
            if (!IsPostBack)
            {                
                BindGV();                
            }
        }

        protected void BindGV()
        {
            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                DataSet ds = DBLibrary.GetDataSetFromStoredProcedure(conn, "p_getnewemails");
                gvMailingList.DataSource = ds;
                gvMailingList.DataBind();
            }
        }

        protected void cbSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk;
            foreach (GridViewRow rowItem in gvMailingList.Rows)
            {
                chk = (CheckBox)(rowItem.Cells[0].FindControl("cbSelect"));
                chk.Checked = ((CheckBox)sender).Checked;
            }
        }

        protected void btnAdd_OnClick(object sender, EventArgs e)
        {            
            string mailid = "";

            for (int i = 0; i < gvMailingList.Rows.Count; i++)
            {
                GridViewRow row = gvMailingList.Rows[i];
                bool isChecked = ((CheckBox)row.FindControl("cbSelect")).Checked;

                if (isChecked)
                {
                    // Column 2 is the name column
                    mailid = Convert.ToString(this.gvMailingList.DataKeys[gvMailingList.Rows[i].RowIndex].Value);
                   
                    Hashtable param = new Hashtable();
                    param.Add("@mailid", mailid);
                    DBLibrary.ExecuteStoredProcedure(Application["DBconn"].ToString(), "p_insertmailingsent", param);
                    param.Clear();

                }
            }

            BindGV();  
        }



    }
}
