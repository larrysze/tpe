<%@ Register TagPrefix="mbdb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.DefaultButtons" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Page language="c#" Codebehind="Task_List.aspx.cs" AutoEventWireup="True" Inherits="localhost.task_list" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
<div class="DivTitleBarMenu" style="width:780px"><span class="Header Color1 Bold">Task List:</span></div>
	<asp:panel id="pnMain" runat="server">
		<TABLE cellSpacing="0" cellPadding="0" width="90%" border="0">
			<TR>
				<TD align="left">
					<asp:Button CssClass="Content Color2" id="btnDelete" runat="server" Text="Delete Selected" align="left" onclick="btnDelete_Click"></asp:Button></TD>
				<TD align="right" height="33"><span class="Header Colo2 Bold"><br />Select Due Date:</span>
					<asp:DropDownList CssClass="InputForm" id="ddlDueDate" runat="server" AutoPostBack="True" onselectedindexchanged="ddlDueDate_SelectedIndexChanged">
						<asp:ListItem Value="All" Selected="True">All</asp:ListItem>
						<asp:ListItem Value="Overdue">Overdue</asp:ListItem>
						<asp:ListItem Value="DueSoon">Due Soon</asp:ListItem>
					</asp:DropDownList><br /><br /></TD>
			</TR>
			<TR>
				<TD align="center" colSpan="2">
					<asp:DataGrid BackColor="#000000" CellSpacing="1" BorderWidth="0" id="dgTask" runat="server" width="770px" CssClass="DataGrid" AutoGenerateColumns="false" CellPadding="2" 
					HorizontalAlign="Center" ShowFooter="false" DataKeyField="TASK_ID">
						<AlternatingItemStyle CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
						<ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
						<HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
						<Columns>
							<mbrsc:RowSelectorColumn AllowSelectAll="True"></mbrsc:RowSelectorColumn>
							<asp:BoundColumn DataField="TASK_DUE_DATE" HeaderText="Due Date" DataFormatString="{0:MM/dd/yyyy}">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:HyperLinkColumn DataNavigateUrlField="TASK_LEAD_ID" DataNavigateUrlFormatString="../administrator/Contact_Details.aspx?ID={0}"
								DataTextField="TASK_LEAD" HeaderText="Contact">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:HyperLinkColumn>
							<asp:BoundColumn DataField="TASK_PRIORITY" HeaderText="Priority">
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="TASK_DESCRIPTION" HeaderText="Brief Description"></asp:BoundColumn>
						</Columns>
					</asp:DataGrid><br /></TD>
			</TR>
		</TABLE>
	</asp:panel>
</asp:Content>

