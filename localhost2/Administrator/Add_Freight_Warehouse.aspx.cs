
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using TPE.Utility;
using System.Data.SqlClient;


namespace localhost.Administrator
{
    public partial class WebForm3 : System.Web.UI.Page
    {

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dg.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_ItemCommand);
            this.dg.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_PageIndexChanged);
            this.dg.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_CancelCommand);
            this.dg.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_EditCommand);
            this.dg.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dg_SortCommand);
            this.dg.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_UpdateCommand);
        //    this.dg.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_ItemDataBound);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "T") && ((string)Session["Typ"] != "L"))
            {
                Response.Redirect("/default.aspx");
            }

            if (!IsPostBack)
            {
                BindData();
            }
            Label1.Text = "";
        }

        protected ICollection CreateDataSource()
        {
            DataTable dt = new DataTable();

            string Sql =
                "SELECT SHIPMENT_COMP_ID, SHIPMENT_COMP_NAME, SHIPMENT_COMP_TYPE" +
                " FROM SHIPMENT_COMP;";

            dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), Sql);
            DataView dv = new DataView(dt);

            if (ViewState["SortExpression"] != null)
            {
                dv.Sort = ViewState["SortExpression"].ToString();
                if (ViewState["SortDirection"] != null)
                    dv.Sort += " " + ViewState["SortDirection"].ToString();
            }

            return dv;
        }

        private void BindData()
        {
            dg.DataSource = CreateDataSource();
            dg.DataBind();
        }


        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            string shipmentType=null;
            switch (ListShipmentType.SelectedIndex)
            {
                case 0: shipmentType = "F"; break;
                case 1: shipmentType = "W"; break;
                case 2: shipmentType = "B"; break;
            }
            
      
            string strSqlInsert = "Insert into SHIPMENT_COMP (SHIPMENT_COMP_NAME,SHIPMENT_COMP_TYPE) Values (@COMPANY_NAME,@SHIPMENT_TYPE);"; 

            Hashtable ht = new Hashtable();
            ht.Add("@COMPANY_NAME", TextBoxCompany.Text);
            ht.Add("@SHIPMENT_TYPE", shipmentType);
            
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlInsert,ht);

            Label1.Text = "   You have just added: " + TextBoxCompany.Text + " into the database, Type-" + ListShipmentType.SelectedItem.Text;
            
            BindData();
   
            TextBoxCompany.Text = "";
            ListShipmentType.SelectedIndex = 0;

        }


        private void dg_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            dg.EditItemIndex = e.Item.ItemIndex;
            BindData();
        }

        private void dg_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            dg.EditItemIndex = -1;
            BindData();
        }

        private void dg_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {

            string strCompanyName = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
            int shipmentType = ((DropDownList)e.Item.Cells[3].FindControl("ddlType")).SelectedIndex;
            int companyID = Convert.ToInt32(e.Item.Cells[1].Text);
            
            string strShipmentType = null;
            switch (shipmentType)
            {
                case 0: strShipmentType = "F"; break;
                case 1: strShipmentType = "W"; break;
                case 2: strShipmentType = "B"; break;
            }

            string strSqlInsert = "update SHIPMENT_COMP set " +
                                  "SHIPMENT_COMP_NAME=@COMPANY_NAME, " +
                                  "SHIPMENT_COMP_TYPE=@SHIPMENT_TYPE " +
                                  "where " +
                                  "SHIPMENT_COMP_ID=@COMPANY_ID" +
                                  ";";

            Hashtable ht = new Hashtable();
            ht.Add("@COMPANY_NAME", strCompanyName);
            ht.Add("@SHIPMENT_TYPE", strShipmentType);
            ht.Add("@COMPANY_ID", companyID);

            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlInsert,ht);

            dg.EditItemIndex = -1;
            BindData();
        }

        private void dg_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            int prevPageCount = dg.PageCount;

            if (e.CommandName == "Del")
            {
                int id = Convert.ToInt32(dg.DataKeys[e.Item.ItemIndex]);

                string strSqlInsert = "delete from SHIPMENT_COMP" +
                                      " where" +
                                      " SHIPMENT_COMP_ID=" + id +
                                      ";";

                DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlInsert);

                //CurrentPageIndex error occurs when the last item of the page is deleted
                //Set the new page index, before binding the data.
                dg.CurrentPageIndex = 0;

                BindData();

            }
            
        }

        private void dg_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            if (ViewState["SortDirection"] == null)
                ViewState.Add("SortDirection", "ASC");
            else
            {
                if (ViewState["SortDirection"].ToString() == "ASC")
                {
                    ViewState["SortDirection"] = "DESC";
                }
                else
                {
                    ViewState["SortDirection"] = "ASC";
                }
            }
            ViewState["SortExpression"] = e.SortExpression.ToString();
            dg.CurrentPageIndex = 0;
            BindData();
            
        }

        private void dg_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            dg.CurrentPageIndex = e.NewPageIndex;
            BindData();
        }

        public string ConvertStatus(object status)
        {
            if (status.ToString() == "F")
                return "Freight";
            else if (status.ToString() == "W")
                return "Warehouse";
            else if (status.ToString() == "B")
                return "Freight and Warehouse";
            else
                return "NULL";
        }

    }

}