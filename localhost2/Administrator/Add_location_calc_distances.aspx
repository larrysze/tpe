using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Data.SqlClient;
using System.IO;
using System.Threading;

namespace localhost.Administrator
{
	public class Add_location_calc_distances : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblEstimationTime;
		protected System.Web.UI.WebControls.Label lblCalculating;
		public string idLocality = "";
		public string idQuery = "";
		public string pathServer = ""; // used for thread to store wwwroot path

		private void Page_Load(object sender, System.EventArgs e)
		{
			idLocality = insertLocality();
			idQuery = Request.QueryString["id"];
			
			// get the country of the new locality
			string countryTempPlace = "";
			SqlConnection conn2 = new SqlConnection(Application["DBconn"].ToString());
			conn2.Open();
			string strSQL2="SELECT TMP_PLAC_CTRY FROM TMP_PLACE WHERE TMP_PLAC_ID='"+idQuery+"'";
			SqlCommand cmdSelect;
			cmdSelect= new SqlCommand(strSQL2, conn2);
			SqlDataReader dtrSelect= cmdSelect.ExecuteReader();
			while(dtrSelect.Read())
			{
				countryTempPlace = dtrSelect["TMP_PLAC_CTRY"].ToString();
			}
			conn2.Close();
			
			if(countryTempPlace == "US")
			{// calculate distances if the country is US because geoplace does not support other country
				calcEstimateTime();
				
				//set current path for the thread, because server.MapPath in a thread is windows root directory.
				pathServer = @Server.MapPath("./").ToString() + @"/sqlDistances.sql"; 

				Thread threadDistances = new Thread(new ThreadStart(calcAllDistances));
				threadDistances.Start();
			}
			else
			{// insert place for other country
				insertPlace();
				lblEstimationTime.Text = "The new location has been added to the database. Distance cannot be calculated. There will be no estimation freight price.";
				lblCalculating.Text = "You can continue to navigate through the website.";
			}
		}
		private void insertPlace()
		{// inserting the new place to the company

			// get information from tmp_place which has been set on add_location.aspx.cs page.
			SqlConnection conn2 = new SqlConnection(Application["DBconn"].ToString());
			conn2.Open();
			string strSQL2="SELECT * FROM TMP_PLACE WHERE TMP_PLAC_ID='"+idQuery+"'";
			SqlCommand cmdSelect;
			cmdSelect= new SqlCommand(strSQL2, conn2);
			SqlDataReader dtrSelect= cmdSelect.ExecuteReader();
			while(dtrSelect.Read())
			{   
				// inserting values in PLACE table
				SqlConnection conn3 = new SqlConnection(Application["DBconn"].ToString());
				conn3.Open();
				string strSQL3="INSERT INTO PLACE (PLAC_COMP, PLAC_LOCL, PLAC_PERS, PLAC_PHON, PLAC_ADDR_ONE, PLAC_ADDR_TWO, PLAC_ZIP, PLAC_TYPE, PLAC_ENBL,PLAC_INST, PLAC_LABL, PLAC_RAIL_NUM) VALUES ('"+dtrSelect["TMP_PLAC_COMP"]+"', '"+idLocality+"','"+dtrSelect["TMP_PLAC_PERS"]+"' ,'"+dtrSelect["TMP_PLAC_PHON"]+"', '"+dtrSelect["TMP_PLAC_ADDR_ONE"]+"', '"+dtrSelect["TMP_PLAC_ADDR_TWO"]+"', '"+dtrSelect["TMP_PLAC_ZIP"]+"', '"+dtrSelect["TMP_PLAC_TYPE"]+"', '1', '"+dtrSelect["TMP_PLAC_INST"]+"', '"+dtrSelect["TMP_PLAC_LABL"]+"','"+dtrSelect["TMP_PLAC_RAIL_NUM"]+"')";
				SqlCommand cmdInsert;
				cmdInsert= new SqlCommand(strSQL3, conn3);
				cmdInsert.ExecuteNonQuery();			
				conn3.Close();
			}
			conn2.Close();
	
			// delete the entry from TMP_PLACE table
			SqlConnection conn4 = new SqlConnection(Application["DBconn"].ToString());
			conn4.Open();
			string strSQL4="DELETE TMP_PLACE";
			SqlCommand cmdInsert4;
			cmdInsert4= new SqlCommand(strSQL4, conn4);
			cmdInsert4.ExecuteNonQuery();			
			conn4.Close();

		}
		private string insertLocality()
		{// insert the new locality in LOCALITY table
			string id = "";
			
			// get values about the new place from TMP_PLACE table
			string cityFrom = "";
			string stateFrom = "";	
			string countryFrom = "";	

			SqlConnection conn2 = new SqlConnection(Application["DBconn"].ToString());
			conn2.Open();
			string strSQL2="SELECT TMP_PLAC_ID,TMP_PLAC_CITY,TMP_PLAC_STAT,TMP_PLAC_CTRY FROM TMP_PLACE WHERE TMP_PLAC_ID='"+Request.QueryString["id"]+"'";
			SqlCommand cmdSelect;
			cmdSelect= new SqlCommand(strSQL2, conn2);
			SqlDataReader dtrSelect= cmdSelect.ExecuteReader();
			while(dtrSelect.Read())
			{
				cityFrom = dtrSelect["TMP_PLAC_CITY"].ToString();
				stateFrom = dtrSelect["TMP_PLAC_STAT"].ToString();	
				countryFrom = dtrSelect["TMP_PLAC_CTRY"].ToString();	
			}
			conn2.Close();

			// set the new place in LOCALITY
			SqlConnection conn3 = new SqlConnection(Application["DBconn"].ToString());
			conn3.Open();
			string strSQL3="INSERT INTO LOCALITY (LOCL_CITY,LOCL_STAT,LOCL_CTRY) VALUES ('"+cityFrom+"','"+stateFrom+"','"+countryFrom+"')";
			SqlCommand cmdInsert;
			cmdInsert= new SqlCommand(strSQL3, conn3);
			cmdInsert.ExecuteNonQuery();			
			conn3.Close();
			  
			// get the id of the new locality
			SqlConnection conn4 = new SqlConnection(Application["DBconn"].ToString());
			conn4.Open();
			string strSQL4="SELECT MAX(LOCL_ID) AS MAXI FROM LOCALITY";
			SqlCommand cmdSelectMax;
			cmdSelectMax= new SqlCommand(strSQL4, conn4);
			SqlDataReader dtrSelectMax= cmdSelectMax.ExecuteReader();
			while(dtrSelectMax.Read())
			{
				id = dtrSelectMax["MAXI"].ToString();
			}
			conn4.Close();
			return id;
		}
		private void calcAllDistances()
		{// calculate the distances between the new locality and the the others that already exist.

			// get values about the new place from TMP_PLACE table
			string cityFrom = "";
			string stateFrom = "";	
			string countryFrom = "";	

			SqlConnection conn2 = new SqlConnection(Application["DBconn"].ToString());
			conn2.Open();
			string strSQL2="SELECT TMP_PLAC_ID,TMP_PLAC_CITY,TMP_PLAC_STAT,TMP_PLAC_CTRY FROM TMP_PLACE WHERE TMP_PLAC_ID='"+idQuery+"'";
			SqlCommand cmdSelect;
			cmdSelect= new SqlCommand(strSQL2, conn2);
			SqlDataReader dtrSelect= cmdSelect.ExecuteReader();
			while(dtrSelect.Read())
			{
				cityFrom = dtrSelect["TMP_PLAC_CITY"].ToString();
				stateFrom = dtrSelect["TMP_PLAC_STAT"].ToString();	
				countryFrom = dtrSelect["TMP_PLAC_CTRY"].ToString();	
			}
			conn2.Close();	

			float sDistance = 0;

			// initialize geoplaces webservice
			GeoPlaces myGeoPlaces = new GeoPlaces();
			AuthenticationHeader AuthenticationHeaderValue = new AuthenticationHeader();
			AuthenticationHeaderValue.SessionID="Wn7BufIemFWK6F6J5s2aS+UJlA7WPHA44bregtf8HqQtA/RH4b8VNa4JOhZmrPAOFqGvDqbCMwvPUZVjkkriBf/vxKxNHLxi";
			myGeoPlaces.AuthenticationHeaderValue = AuthenticationHeaderValue;

			// get all cities that arre different from the new locality
			string strSQL = "SELECT LOCL_ID,LOCL_CITY,LOCL_STAT,LOCL_CTRY FROM LOCALITY WHERE (NOT(LOCL_CITY = '"+cityFrom+"' AND LOCL_STAT = '"+stateFrom+"')) AND LOCL_CTRY ='US'"; 
			SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			SqlCommand cmdLocality= new SqlCommand(strSQL, conn);
			SqlDataReader dtrLocality= cmdLocality.ExecuteReader();
			while(dtrLocality.Read())			
			{
				// get the distance between the new locality and all other cities
				sDistance = myGeoPlaces.GetDistanceBetweenPlaces(cityFrom,stateFrom,dtrLocality["LOCL_CITY"].ToString(),dtrLocality["LOCL_STAT"].ToString());
				
				// insert each distance in DISTANCES table
				SqlConnection conn3 = new SqlConnection(Application["DBconn"].ToString());
				conn3.Open();
				string strSQL3="INSERT INTO DISTANCES (CITY_FROM_ID,CITY_FROM_NAME,CITY_TO_ID,CITY_TO_NAME,DISTANCE) VALUES ('"+idLocality+"','"+cityFrom+"','"+dtrLocality["LOCL_ID"].ToString()+"','"+dtrLocality["LOCL_CITY"].ToString()+"','"+sDistance.ToString()+"')";
				SqlCommand cmdInsert3;
				cmdInsert3= new SqlCommand(strSQL3, conn3);
				cmdInsert3.ExecuteNonQuery();			
				conn3.Close();

				// insert each distance in DISTANCES table (city_from and city_to are reversed)
				SqlConnection conn4 = new SqlConnection(Application["DBconn"].ToString());
				conn4.Open();
				string strSQL4="INSERT INTO DISTANCES (CITY_FROM_ID,CITY_FROM_NAME,CITY_TO_ID,CITY_TO_NAME,DISTANCE) VALUES ('"+dtrLocality["LOCL_ID"].ToString()+"','"+dtrLocality["LOCL_CITY"].ToString()+"','"+idLocality+"','"+cityFrom+"','"+sDistance.ToString()+"')";
				SqlCommand cmdInsert4;
				cmdInsert4= new SqlCommand(strSQL4, conn4);
				cmdInsert4.ExecuteNonQuery();			
				conn4.Close();
			}
			conn.Close();
			insertPlace();

		}
		private void calcEstimateTime()
		{// estimate the maximum time to calculate all distances
			float sDistance = 0;
			
			// initialize geoplaces webservice
			GeoPlaces myGeoPlaces = new GeoPlaces();
			AuthenticationHeader AuthenticationHeaderValue = new AuthenticationHeader();
			AuthenticationHeaderValue.SessionID="Wn7BufIemFWK6F6J5s2aS+UJlA7WPHA44bregtf8HqQtA/RH4b8VNa4JOhZmrPAOFqGvDqbCMwvPUZVjkkriBf/vxKxNHLxi";
			myGeoPlaces.AuthenticationHeaderValue = AuthenticationHeaderValue;
			
			// calculate time to get the 5 distances
			int sec1 = DateTime.Now.Second;
			int min1 = DateTime.Now.Minute;
			for(int i=0;i<5;++i)
			{
				sDistance = myGeoPlaces.GetDistanceBetweenPlaces("chicago","il","new york","ny");
			}
			int sec2 = DateTime.Now.Second;
			int min2 = DateTime.Now.Minute;
			
			// time to get 5 request from the geoplaces web service
			float total = (min2*60 + sec2) - (min1*60 + sec1); 


			string strSQL = "SELECT count(*) AS MAXI FROM LOCALITY"; 
			SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			SqlCommand cmdNb= new SqlCommand(strSQL, conn);
			SqlDataReader dtrNb= cmdNb.ExecuteReader();
			float total2 = 0;
			int nbCities = 0;
			while(dtrNb.Read())			
			{// build the maximum estimated time to get all the distances.
				total2 = (total / 5) * Convert.ToInt32(dtrNb["MAXI"].ToString());
				nbCities = Convert.ToInt32(dtrNb["MAXI"].ToString());
			}
			conn.Close();
			
			string delimStr = ".";
			char [] delimiter = delimStr.ToCharArray();
			string[] tabTotal2 = ((total2/60).ToString()).Split(delimiter);

			lblEstimationTime.Text = "The estimated time to calculate distance with the other cities is " + (tabTotal2[0]).ToString() + " min " +Convert.ToInt32(Convert.ToDouble(tabTotal2[1])* 0.6).ToString() + " sec.";
			lblCalculating.Text = "You can continue to navigate through the website and the new location will be added at the end of the time.";
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
