<%@ Page language="c#" Codebehind="Ports.aspx.cs" AutoEventWireup="True" Inherits="localhost.Ports" MasterPageFile="~/MasterPages/Menu.Master" Title="Manage International Ports" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">

<style type="text/css">

.Left {text-align:left;}

</style>
 
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
<div class="DivTitleBarMenu"><span class="Header Color1 Bold"><asp:label id="lblTitle" runat="server">Manage International Ports</asp:label></span></div>

	<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
		<TR>
			<TD colSpan="2">
				<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<TD>
							<TABLE id="Table2" style="WIDTH: 100%" cellSpacing="1" cellPadding="1" width="726" align="left"
								border="0">
								<TR>
									<TD width="15"></TD>
									<TD vAlign="top" align="center" colSpan="3">
										<asp:Label CssClass="Content Color3 Bold" id="lblMessage" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD width="15">&nbsp;&nbsp;&nbsp;
									</TD>
									<TD vAlign="top" align="center">
										<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="254" border="0">
											<TR>
												<TD align="right" width="80" height="16" noWrap><span class="Content Color2"><asp:label id="Label1" runat="server">New Group:</asp:label></span></TD>
												<TD height="16" align="left"><asp:textbox CssClass="InputForm" id="txtNewGroup" runat="server"></asp:textbox></TD>
											</TR>
											<TR>
												<TD width="80"></TD>
												<TD align="left"><asp:button CssClass="Content Color2" Height="20" id="btnAddGroup" runat="server" Text="Add Group" onclick="btnAddGroup_Click"></asp:button></TD>
											</TR>
										</TABLE>
										<BR>
										<P><asp:datagrid Width="250" BackColor="#000000" CellSpacing="1" BorderWidth="0" id="dgGroups" runat="server" CssClass="DataGrid"
												AutoGenerateColumns="False">
												<AlternatingItemStyle CssClass="LinkNormal Left LightGray"></AlternatingItemStyle>
												<ItemStyle CssClass="LinkNormal Left DarkGray"></ItemStyle>
												<HeaderStyle CssClass="LinkNormal Left Bold OrangeColor"></HeaderStyle>
												<Columns>
													<asp:BoundColumn ItemStyle-Horizontalalign="left" Visible="False" DataField="Port_Group_ID" HeaderText="PortGroupID"></asp:BoundColumn>
													<asp:ButtonColumn DataTextField="Port_Group_Name"  HeaderText="List of Port Groups" CommandName="List"></asp:ButtonColumn>	
													<asp:ButtonColumn ItemStyle-HorizontalAlign="Center" Text="Delete" ButtonType="PushButton" CommandName="Delete"></asp:ButtonColumn>
												</Columns>
											</asp:datagrid></P>
									</TD>
									<TD vAlign="top" align="center">&nbsp;&nbsp;&nbsp;
									</TD>
									<TD vAlign="top" align="center">
										<TABLE id="Table5" height="121" cellSpacing="1" cellPadding="1" width="302" border="0">
											<TR>
												<TD width="86" align="left"><asp:label id="Label2" runat="server"><span class="Content Color2">Port City:</span></asp:label></TD>
												<TD align="left"><asp:textbox CssClass="InputForm" id="txtPortCity" runat="server"></asp:textbox></TD>
											</TR>
											<TR>
												<TD width="86" noWrap align="left"><span class="Content Color2"><asp:label id="Label3" runat="server">Port Country:</asp:label></span></TD>
												<TD align="left"><asp:dropdownlist CssClass="InputForm" id="ddlPortCoutry" runat="server"></asp:dropdownlist></TD>
											</TR>
											<TR>
												<TD width="86" align="left"><span class="Content Color2"><asp:label id="Label4" runat="server">Port Group:</asp:label></span></TD>
												<TD align="left"><asp:dropdownlist CssClass="InputForm" id="ddlPortGroup" runat="server"></asp:dropdownlist></TD>
											</TR>
											<TR>
												<TD width="86" align="left"><span class="Content Color2"><asp:label id="Label5" runat="server">Origin Port:</asp:label></span></TD>
												<TD align="left"><asp:checkbox Width="50" id="chkOriginPort" CssClass="Content Color2" runat="server" Text="Yes"></asp:checkbox></TD>
											</TR>
											<TR>
												<TD width="86" align="left"></TD>
												<TD align="left"><asp:button Height="20" id="btnAddPort" CssClass="Content Color2" runat="server" Text="Add Port" onclick="btnAddPort_Click"></asp:button></TD>
											</TR>
										</TABLE>
										<br />
										<asp:label CssClass="Header Bold Color2" id="lblSelectedGroup" runat="server" Visible="False"></asp:label><asp:label CssClass="Header Bold Color2" id="lblSelectedPort" runat="server" Font-Bold="True">Click on a Port Group to see its Ports.</asp:label>
										<asp:datagrid BorderWidth="0" CellSpacing="1" BackColor="#000000" id="dgPorts" runat="server" Width="520px" CssClass="DataGrid" AutoGenerateColumns="False">
											<AlternatingItemStyle CssClass="LinkNormal LightGray"></AlternatingItemStyle>
											<ItemStyle CssClass="LinkNormal DarkGray"></ItemStyle>
											<HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
											<Columns>
												<asp:BoundColumn Visible="False" DataField="Port_ID" HeaderText="PortID"></asp:BoundColumn>
												<asp:BoundColumn DataField="Port_City" HeaderText="Port">
													<ItemStyle HorizontalAlign="Left"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Port_Country" SortExpression="FILE_OWNER ASC" HeaderText="Country">
													<ItemStyle HorizontalAlign="Left"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="Port_Group" HeaderText="GroupID">
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Group">
													<ItemTemplate>
														<asp:DropDownList CssClass="InputForm" id="ddlGroup" runat="server"></asp:DropDownList>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="Port_Enbl" HeaderText="Origin Port">
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Origin Port">
													<ItemTemplate>
														<asp:CheckBox id="chkYes" runat="server" Text="Yes"></asp:CheckBox>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:ButtonColumn Text="Save" ButtonType="PushButton" CommandName="Save"></asp:ButtonColumn>
												<asp:ButtonColumn Text="Delete" ButtonType="PushButton" CommandName="Delete"></asp:ButtonColumn>
											</Columns>
										</asp:datagrid>
										<P>&nbsp;</P>
									</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
	</TABLE>

</asp:Content>