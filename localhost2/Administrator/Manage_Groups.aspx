<%@ Page language="c#" Codebehind="Manage_Groups.aspx.cs" AutoEventWireup="false" Inherits="localhost.Administrator.Manage_Groups" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<form id="Manage_Groups" runat="server">
	<TPE:TEMPLATE id="Template1" Runat="Server" PageTitle="Manage Groups"></TPE:TEMPLATE>
	<table width="80%">
		<tr>
			<td>
			<TABLE cellSpacing="0" cellPadding="0" border="0" width="50%" align="center">
				<tr>
					<td colspan="2" class="ListHeadlineBold">
						Manage Groups
					</td>
				</tr>
				<tr>
					<td>
						<br>
					</td>
				</tr>
				<TR>
					<TD colSpan="2">
						<asp:DataGrid id="dg" runat="server" ShowHeader="False" HorizontalAlign="Center" CellPadding="2"
							AutoGenerateColumns="False" HeaderStyle-CssClass="DataGridRow_Alternate" AlternatingItemStyle-CssClass="DataGridRow_Alternate"
							ItemStyle-CssClass="DataGridRow" CssClass="DataGrid" OnEditCommand="DG_Edit" OnCancelCommand="DG_Cancel"
							OnUpdateCommand="DG_Update" OnDeleteCommand="DG_Delete" width="100%">
							<AlternatingItemStyle CssClass="DataGridRow_Alternate"></AlternatingItemStyle>
							<ItemStyle CssClass="DataGridRow"></ItemStyle>
							<HeaderStyle CssClass="DataGridRow_Alternate"></HeaderStyle>
							<Columns>
								<asp:ButtonColumn Text="Delete" CommandName="Delete"></asp:ButtonColumn>
								<asp:BoundColumn DataField="GROUP_NAME" HeaderText="Groups">
									<ItemStyle Wrap="False"></ItemStyle>
								</asp:BoundColumn>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Rename"></asp:EditCommandColumn>
								<asp:BoundColumn DataField="GROUP_ID" ReadOnly="True" Visible="false" />
							</Columns>
						</asp:DataGrid>
						<br>
					</TD>
				</TR>
				<tr>
					<td colspan="2" class="ListHeadlineBold">
						Create New Group
					</td>
				</tr>
				<tr>
					<td colspan="2"><br>
						<br>
						<asp:TextBox id="txtNewGroup" runat="server"></asp:TextBox>
						<asp:Button id="btnCreate" runat="server" Text="Create"></asp:Button>
<asp:Button id=btnCancel runat="server" Text="Done"></asp:Button>
					</td>
				</tr>
			</TABLE>
			
			</td>
		</tr>
		<tr>
			<td align="right">
				
			</td>
		</tr>
	
	</table>
	
	
	<center>&nbsp;</center>
	<TPE:TEMPLATE id="Template2" footer="True" Runat="Server"></TPE:TEMPLATE>
</form>
