<%@ Page Language="VB" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Web.Mail" %>
<form runat="server" id="Form">
    <TPE:Template PageTitle="New Lead" Runat="Server" />
<!--#include FILE="../include/VBLegacy_Code.aspx"-->

<%
'only adminstrator and borker can access this page.
IF Session("Typ")<>"A" AND Session("Typ")<>"B" THEN Response.Redirect("../default.aspx")

Dim Id,Str,Mail
Dim conn as SqlConnection
Dim cmdContent as SQLCommand
conn = new SqlConnection(Application("DBconn"))
conn.Open()

'Execute store procedure spUpdLeads to create a new lead
IF Request.Form("Submit")<>"" THEN
Str = "Exec spUpdLeads @compname='"&Request.Form("company")&"'"
IF Request.Form("resin")<>"" THEN
	Str=Str+", @resin='"&Request.Form("resin")&"'"
END IF
IF Request.Form("city")<>"" THEN
	Str=Str+", @city='"&Request.Form("city")&"'"
END IF
IF Request.Form("state")<>"" THEN
	Str=Str+", @state='"&Request.Form("state")&"'"
END IF
IF Request.Form("zip")<>"" THEN
	Str=Str+", @zip='"&Request.Form("zip")&"'"
END IF
IF Request.Form("phone")<>"" THEN
	Str=Str+", @phone='"&Request.Form("phone")&"'"
END IF
IF Request.Form("lbs")<>"" THEN
	Str=Str+", @lbs='"&Request.Form("lbs")&"'"
END IF
IF Request.Form("email")<>"" THEN
Str=Str+", @email='"&Request.Form("email")&"'"

'Send email to "sqlserver@ThePlasticsExchange.com", email address will be added to
'mailing list database on Intranet.
Dim mail1 AS MailMessage
mail1 = new MailMessage()
mail1.From = "WebSite"
mail1.To = "sqlserver@ThePlasticsExchange.com"
mail1.Subject = "ML"
mail1.Body = Request.Form("email")
mail1.BodyFormat = MailFormat.Html
TPE.Utility.EmailLibrary.Send(Context,mail1)

END IF
IF Request.Form("name")<>"" THEN
	Str=Str+", @name='"&Request.Form("name")&"'"
END IF
IF Request.Form("title")<>"" THEN
	Str=Str+", @title='"&Request.Form("title")&"'"
END IF
IF Request.Form("pack")<>"" THEN
	Str=Str+", @pack='"&Request.Form("pack")&"'"
END IF
IF Request.Form("enduser")<>"" THEN
	Str=Str+", @enduser='"&Request.Form("enduser")&"'"
END IF
IF Request.Form("item")<>"" THEN
	Str=Str+", @item='"&Request.Form("item")&"'"
END IF
IF Request.Form("ccont")<>"" THEN
	Str=Str+", @ccont='"&Request.Form("ccont")&"'"
END IF
IF Request.Form("cmail")<>"" THEN
Str=Str+", @cmail='"&Request.Form("cmail")&"'"

'Send email to "sqlserver@ThePlasticsExchange.com", email address will be added to
'mailing list database on Intranet.
Dim mail2 AS MailMessage
mail2 = new MailMessage()
mail2.From = "WebSite"
mail2.To = "sqlserver@ThePlasticsExchange.com"
mail2.Subject = "ML"
mail2.Body = Request.Form("cmail")
mail2.BodyFormat = MailFormat.Html
TPE.Utility.EmailLibrary.Send(Context,mail2)

END IF
IF Request.Form("ctitle")<>"" THEN
	Str=Str+", @ctitle='"&Request.Form("ctitle")&"'"
END IF
IF Request.Form("cphone")<>"" THEN
	Str=Str+", @cphone='"&Request.Form("cphone")&"'"
END IF

'Create a new lead
cmdContent= new SqlCommand(Str, conn)
cmdContent.ExecuteNonQuery()

'back to lead_list.aspx
response.redirect ("/Administrator/Leads_List.aspx?nav=Customers")
END IF
%>
<!--#INCLUDE FILE="../include/Loading.inc"-->
<!--#INCLUDE FILE="../include/PopUp.inc"-->
<SPAN ID=Main name=Main style=' x:0px; y:0px; visibility: hidden'><center>

<input type=hidden name=Submit value=''>

<TPE:Web_Box Heading="Create a New Lead" Runat="Server" />
<table border=0 cellpadding=0 cellspacing=0  align=middle>


	<tr>
		<td align=left>
			<table cellpadding=3 cellspacing=2>
				<tr>
					<td CLASS=S1 colspan=6>&nbsp;&nbsp;&nbsp;&nbsp;Company Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name=company type=text maxlength=50 size=40>
					</td>
				</tr>
				<tr>
					<td colspan=6><center><b><u>Location Information</u></b></center>
					</td>
				</tr>
				<tr>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Resin
					</td>
					<td><input name=resin type=text maxlength=40 size=25>
					</td>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Email
					</td>
					<td colspan=3><input name=email type=text maxlength=75 size=40>
					</td>
				</tr>
				<tr>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;City
					</td>
					<td><input name=city type=text maxlength=40 size=20>
					</td>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;State
					</td>
					<td><input name=state type=text maxlength=20 size=5>
					</td>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Zip
					</td>
					<td><input name=zip type=text maxlength=10 size=8>
					</td>
				</tr>
				<tr>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Name
					</td>
					<td><input name=name type=text maxlength=40 size=20>
					</td>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Title
					</td>
					<td><input name=title type=text maxlength=40 size=8>
					</td>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Phone
					</td>
					<td><input name=phone type=text maxlength=25 size=15>
					</td>
				</tr>
				<tr>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Package
					</td>
					<td><input name=pack type=text maxlength=25 size=25>
					</td>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Users
					</td>
					<td colspan=3><input name=enduser type=text maxlength=75 size=40>
					</td>
				</tr>
				<tr>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Lbs/Year
					</td>
					<td><input name=lbs type=text maxlength=12 size=15>
					</td>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Items
					</td>
					<td colspan=3><input name=item type=text maxlength=150 size=40>
					</td>
				</tr>
				<tr>
					<td colspan=6><center><b><u>Company Information</u></b></center>
					</td>
				</tr>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Contact
					</td>
					<td><input name=ccont type=text maxlength=40 size=25>
					</td>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Email
					</td>
					<td colspan=3><input name=cmail type=text maxlength=75 size=40>
					</td>
				</tr>
				</tr>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Title
					</td>
					<td><input name=ctitle type=text maxlength=40 size=25>
					</td>
					<td CLASS=S1>&nbsp;&nbsp;&nbsp;&nbsp;Phone
					</td>
					<td colspan=3><input name=cphone type=text maxlength=25 size=20>
					</td>
				</tr>
			</table>
		</td>
		<td><br>
		</td>
	</tr>
	<tr>
		<td colspan=2><br><center><input type=button class=tpebutton value="SUBMIT" onClick="Javascript:if(document.Form.company.value!=''){document.Form.Submit.value='submit';document.Form.submit()}else{alert('A company name must be entered.');}">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type=button class=tpebutton value="CANCEL" onclick="Javascript:history.back()">
		<BR><BR></td>
	</tr>

</table>
<TPE:Web_Box Footer="true" Runat="Server" />



<TPE:Template Footer="true" Runat="Server" />

</form>
</SPAN><script>Path.Start(window)</script>
