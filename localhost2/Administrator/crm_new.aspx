<%@ Page Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="crm_new.aspx.cs" Inherits="localhost.Administrator.crm_new" Title="Beta CRM page" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCssLink" runat="server">
</asp:Content>
    
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphMain">

 <table cellspacing="0" cellpadding="0" width="780" border="0">
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="height: 21px">
            <asp:Button CssClass="Content Color2" ID="btnSelectAll" runat="server" Text="Select All"
                OnClick="btnSelectAll_Click"></asp:Button>
            <asp:Button CssClass="Content Color2" ID="btnEmailOffer" runat="server" Width="80px"
                Text="Email" OnClick="btnEmailOffer_Click"></asp:Button>  
            <asp:Button CssClass="Content Color2" ID="btnEmailLetter" runat="server" Text="Email Letter"
                OnClick="btnEmailLetter_Click"></asp:Button>
            <asp:Button CssClass="Content Color2" ID="btnDelete" runat="server" Width="80px"
                Text="Delete" OnClick="btnDelete_Click"></asp:Button>               
             <asp:Button CssClass="Content Color2" ID="btnManageLead" runat="server"
                Text="Manage Leads" OnClick="btnManageLead_Click"></asp:Button>   
            <asp:Button CssClass="Content Color2" ID="btnAllocate" runat="server" Width="80px" Visible="False"
                Text="Allocate" OnClick="btnAllocate_Click"></asp:Button>   
            
        </td>              
    </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="780" border="0">
    <tr>
        <td style="height: 21px; width: 100px;">
            <asp:DropDownList ID="ddlFolder" runat="server" OnSelectedIndexChanged="ddlFolder_SelectdIndexChanged"  AutoPostBack="True">            
            </asp:DropDownList></td>
        <td align="left" style="height: 21px">
            <div style="font-size:smaller">Display:<asp:DropDownList ID="ddlPageSize" runat="server" OnSelectedIndexChanged="ddlPageSize_SelectdIndexChanged" AutoPostBack="True">                       
            <asp:ListItem Value="25" Selected="true">25</asp:ListItem>
            <asp:ListItem Value="50">50</asp:ListItem>
            <asp:ListItem Value="100">100</asp:ListItem>
            <asp:ListItem Value="200">200</asp:ListItem>
            <asp:ListItem Value="500">500</asp:ListItem>            
            </asp:DropDownList></div>
        </td>
        <td align="left" style="height: 21px">
            <div style="font-size:smaller">Color Type:<asp:DropDownList ID="ddlColorType" runat="server" OnSelectedIndexChanged="ddlColor_SelectdIndexChanged" AutoPostBack="True">                       
            <asp:ListItem Value="*" Selected="true">All</asp:ListItem>
            <asp:ListItem Value="0">Black</asp:ListItem>
            <asp:ListItem Value="1">Red</asp:ListItem>
            <asp:ListItem Value="2">Green</asp:ListItem>
            <asp:ListItem Value="3">Orange</asp:ListItem>            
            <asp:ListItem Value="4">Blue</asp:ListItem>   
            </asp:DropDownList></div>
        </td>
          
        <td>
            <asp:TextBox CssClass="InputForm" ID="txtSearch" runat="server" Width="200px"></asp:TextBox>
            <asp:Button CssClass="Content Color2" ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click"></asp:Button>
        </td> 
    </tr>    
    <tr>
     <td style="width: 190px">
            <asp:Label runat="server" ID="lbTEMP" Text="" Visible="true" ></asp:Label><br />
        </td>
    </tr>
    </table>
    
    <table cellspacing="0" cellpadding="0" width="780" border="0">
    <tr>
        <td valign="top" align="left"  style="width:auto">
            <asp:GridView ID="dg" runat="server" OnSorting="dg_Sorting" OnPageIndexChanging="dg_PageIndexChanging" CssClass="Content" 
                PageSize="200" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="PERS_ID" AllowSorting="True" >
                <HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
				<FooterStyle CssClass="Content Color4 Bold FooterColor" />
                <Columns>
                    <asp:BoundField DataField="PERS_ID" ReadOnly="true"  Visible="false" />
                    <asp:TemplateField>
                        <HeaderStyle Width="20px"></HeaderStyle>
                        <ItemTemplate>
                               <asp:CheckBox ID="CheckBox1" EnableViewState="false" runat="server" />                               
                        </ItemTemplate>                        
                    </asp:TemplateField>                                        
                    <asp:TemplateField HeaderText="Name" SortExpression="PERS_NAME">
                        <HeaderStyle Width="20px"></HeaderStyle>
                            <ItemTemplate>
                                <a href='mailto:<%#Eval("PERS_MAIL")%>'><%#Eval(" PERS_NAME")%></a>
                            </ItemTemplate>
                    </asp:TemplateField>                    
                    <asp:BoundField SortExpression="PERS_TITL" HeaderText="Title" DataField="PERS_TITL" />                                     
                    <asp:TemplateField HeaderText="Company" SortExpression="PERS_STAT ASC">                        
                        <ItemTemplate>
                            <a href="#"  style="color:<%#Eval("PERS_STAT")%>" onclick="javascript:window.open('/administrator/user_Details.aspx?ID=<%#Eval("PERS_ID")%>','win','scrollbars=yes,resizable=yes,width=550,height=565');">
                            <%#Eval("PERS_COMPANY")%></a>
                        </ItemTemplate>
                    </asp:TemplateField>                    
                    <asp:BoundField SortExpression="PERS_CATEGORY" HeaderText="Corporate Identity" DataField="PERS_CATEGORY" />
                    <asp:BoundField SortExpression="PERS_LAST_CHKD" HeaderText="Last Login" DataField="PERS_LAST_CHKD" DataFormatString="{0:d}"/>
                    <asp:BoundField SortExpression="PERS_NUMBER_LOGINS" HeaderText="Logins" DataField="PERS_NUMBER_LOGINS" />
                    <asp:BoundField SortExpression="STATE" HeaderText="State" DataField="STATE" />
                    <asp:BoundField SortExpression="PERS_PHON" HeaderText="Phone" DataField="PERS_PHON" />
                    <asp:BoundField SortExpression="PERS_DATE" HeaderText="Date Registered" DataField="PERS_DATE" DataFormatString="{0:MM/dd/yyyy}" />
                    <asp:BoundField SortExpression="PERS_PRMLY_INTRST" HeaderText="M" DataField="PERS_PRMLY_INTRST" />
                    <asp:BoundField SortExpression="EMAIL_ENBL" HeaderText="Email Freq" DataField="EMAIL_ENBL" />                    
                    <asp:ImageField SortExpression="UserCMT" HeaderText="CMT" DataImageUrlField="UserCMT" DataImageUrlFormatString="../pics/Icons/{0}.gif"/>                                         
                    
                </Columns>
            </asp:GridView>
            
        </td>
    </tr>
 </table>

           
           
    
</asp:Content>


