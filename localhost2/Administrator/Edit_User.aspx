<%@ Page language="c#" Codebehind="Edit_User.aspx.cs" AutoEventWireup="True" Inherits="localhost.Edit_User" MasterPageFile="~/MasterPages/Menu.Master"  %>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">		
		<DIV id="pnlStart"><br>
			<center><asp:label id="lblCompanyName" runat="server" Width="100%" CssClass="Content"></asp:label></center>
					<BR>
					<TABLE id="Table2" cellSpacing="0" cellPadding="1" width="100%" border="0">
						<TR>
							<TD align="center" bgColor="gainsboro" height="25"><asp:label id="lblAction" CssClass="Content Bold Color2" runat="server" Font-Bold="True"></asp:label></TD>
							<TD bgColor="gainsboro" height="25"><asp:label id="lblCompanyID" runat="server" Visible="False"></asp:label><asp:label id="lblOriginalEmail" runat="server" Visible="False"></asp:label>
							<TD bgColor="gainsboro" height="25">
								<P align="right"><asp:label id="lblUserID" runat="server" Visible="False"></asp:label>&nbsp;&nbsp;&nbsp;</P>
							</TD>
						</TR>
					</TABLE>

						<asp:validationsummary id="ValidationSummary1" runat="server" HeaderText="There are problems with the following fields. Please correct the errors below."></asp:validationsummary>
						<TABLE id="Table1" height="344" width="433" border="0">
							<TR>
								<TD colSpan="2"></TD>
							</TR>
							<TR>
								<TD align="left" width="120"><asp:label id="lblType" CssClass="Content Bold Color2" runat="server" Font-Bold="True">Type:</asp:label></TD>
								<TD align="left"><asp:dropdownlist id="ddlType" CssClass="InputForm" runat="server" Width="180px"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD align="left" width="90"><asp:label id="lblFirstName" CssClass="Content Bold Color2" runat="server" Font-Bold="True">First Name:</asp:label></TD>
								<TD align="left"><asp:textbox id="txtFirstName" CssClass="InputForm" runat="server" Width="180px" MaxLength="50"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="left" width="90" height="26"><asp:label id="lblLastName" CssClass="Content Bold Color2" runat="server" Font-Bold="True">Last Name:</asp:label></TD>
								<TD align="left" height="26"><asp:textbox id="txtLastName" CssClass="InputForm" runat="server" Width="180px" MaxLength="50"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="left" width="90"><asp:label id="lblTitle" CssClass="Content Bold Color2" runat="server" Font-Bold="True">Title:</asp:label></TD>
								<TD align="left"><asp:textbox id="txtTitle" CssClass="InputForm" runat="server" Width="180px" MaxLength="45"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="left" width="90"><asp:label id="lblPhone" CssClass="Content Bold Color2" runat="server" Font-Bold="True">Phone:</asp:label></TD>
								<TD align="left"><asp:textbox id="txtPhone" CssClass="InputForm" runat="server" Width="180px" MaxLength="45"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="left" width="90"><asp:label id="lblFax" CssClass="Content Bold Color2" runat="server" Font-Bold="True">Fax:</asp:label></TD>
								<TD align="left"><asp:textbox id="txtFax" CssClass="InputForm" runat="server" Width="180px" MaxLength="45"></asp:textbox></TD>
							</TR>
							<TR>
								<TD align="left" width="90"><asp:label id="lblEmail" CssClass="Content Bold Color2" runat="server" Font-Bold="True">Email:</asp:label></TD>
								<TD align="left"><asp:customvalidator id="CustomValidator1" runat="server" controltovalidate="txtEmail" display="none"
										ErrorMessage="Email already exists." OnServerValidate="IsDuplicate"></asp:customvalidator><asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" ErrorMessage="Email can not be blank."
										ControlToValidate="txtEmail" Display="none"></asp:requiredfieldvalidator><asp:textbox id="txtEmail" CssClass="InputForm" runat="server" Width="180px" MaxLength="90"></asp:textbox>
									<asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid e-mail!" ControlToValidate="txtEmail"
										ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></TD>
							</TR>
							<TR>
								<TD align="left" width="90"><asp:label id="lblLogin" CssClass="Content Bold Color2" runat="server" Font-Bold="True">Login:</asp:label></TD>
								<TD align="left"><asp:textbox id="txtLOGN" CssClass="InputForm" runat="server" Width="180px" MaxLength="50"></asp:textbox><asp:label id="lblMessage" runat="server" ForeColor="Red"></asp:label></TD>
							</TR>
							<TR>
								<TD align="left" width="90"><asp:label id="lblPassword" CssClass="Content Bold Color2" runat="server" Font-Bold="True">Password:</asp:label></TD>
								<TD align="left"><asp:textbox id="txtPSWD" CssClass="InputForm" runat="server" Width="180px" MaxLength="15"></asp:textbox><asp:requiredfieldvalidator id="Requiredfieldvalidator4" runat="server" ErrorMessage="Password can not be blank."
										ControlToValidate="txtPSWD" Display="none"></asp:requiredfieldvalidator></TD>
							</TR>
							<TR>
								<TD align="center" colSpan="2"><asp:button id="cmdSave" CssClass="Content Color2" runat="server" Width="70px" Text="Save" onclick="cmdSave_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<asp:button id="cmdClose" CssClass="Content Color2" runat="server" Width="70px" Text="Close" CausesValidation="False" onclick="cmdClose_Click"></asp:button></TD>
							</TR>
						</TABLE>
					<BR>
		</DIV>
</asp:Content>