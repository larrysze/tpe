<%@ Page language="c#" Codebehind="Company_Comment.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.Company_Comment" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
<div class="DivTitleBarMenu"><span class="Header Bold Color1">Company Comment</span></div>
			<table cellspacing="0" cellpadding="0" border="0" style="width:780px">
	
				<tr>
					<TD colSpan="3"><span class="Header Color2 Bold"><br />Comment History:<br /><br /></span>
					</TD>
				</tr>
				<TR>
					<TD colSpan="3"><asp:datagrid BorderWidth="0" CellSpacing="1" BackColor="#000000" id="dg" runat="server" ShowFooter="false" HorizontalAlign="Center" CellPadding="2"
							AutoGenerateColumns="false" HeaderStyle-CssClass="LinkNormal Bold OrangeColor" AlternatingItemStyle-CssClass="LinkNormal DarkGray"
							ShowHeader="False" ItemStyle-CssClass="LinkNormal LightGray" CssClass="DataGrid" width="100%">
							<Columns>
								<asp:BoundColumn DataField="CMNT_DATE" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}" ItemStyle-Wrap="false" />
								<asp:BoundColumn DataField="CMNT_TEXT" HeaderText="Comment" ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="left" />
								<asp:BoundColumn DataField="CMNT_USER" HeaderText="User" ItemStyle-Wrap="false" />
							</Columns>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD colSpan="3"><span class="Header Bold Color2"><br />New Comment:<br /><br /></span>
						<asp:Label id="lblCommentTooLong" runat="server" CssClass="Content Bold Color3" Visible="false">The comment is too long</asp:Label>
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" align="center" colSpan="3" height="83">
						<asp:TextBox CssClass="InputForm" id="txtComment" runat="server" Columns="30" Rows="2" TextMode="MultiLine" align="center"
							Width="688px" Height="72px"></asp:TextBox>
					</TD>
				</TR>
				<TR>
					<td align="center">
						<asp:Button CssClass="Content Color2" id="btnAddNew" runat="server" Text="Submit" CausesValidation="False" onclick="btnAddNew_Click"></asp:Button>&nbsp;&nbsp;
						<asp:Button CssClass="Content Color2" id="btnConclude" runat="server" Text="Done" CausesValidation="False" onclick="btnConclude_Click"></asp:Button>
					</td>
				</TR>
			</table>
</asp:Content>