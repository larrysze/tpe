using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Update_Company_Info.
	/// </summary>
	public class Update_Company_Info : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblcompanyname2;
		protected System.Web.UI.WebControls.Label lbltest;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.Label lblcompanyname;
		protected System.Web.UI.WebControls.Label lblcompanyURL;
		protected System.Web.UI.WebControls.Label lblCreditLimit;
		protected System.Web.UI.WebControls.Label lblCreditAvail;
		protected System.Web.UI.WebControls.Label lblOrigCreditLimit;
		protected System.Web.UI.WebControls.Button Button14;
		protected System.Web.UI.WebControls.PlaceHolder phOfficer;
		protected System.Web.UI.WebControls.PlaceHolder phPurchaser;
		protected System.Web.UI.WebControls.PlaceHolder phSupplier;
		protected System.Web.UI.WebControls.Button Button3;
		protected System.Web.UI.WebControls.Label lblbankname;
		protected System.Web.UI.WebControls.Label lblbankpers;
		protected System.Web.UI.WebControls.Label lblbankphon;
		protected System.Web.UI.WebControls.Label lblbankfax;
		protected System.Web.UI.WebControls.Label lblbankacnt;
		protected System.Web.UI.WebControls.Label lblbankaddr1;
		protected System.Web.UI.WebControls.Label lblbankaddr2;
		protected System.Web.UI.WebControls.Label lblbankcity;
		protected System.Web.UI.WebControls.Label lblbankzip;
		protected System.Web.UI.WebControls.Label lblbankstat;
		protected System.Web.UI.WebControls.Button Button4;
		protected System.Web.UI.WebControls.PlaceHolder phHeadquarters;
		protected System.Web.UI.WebControls.PlaceHolder phShippingPoints;
		protected System.Web.UI.WebControls.Panel pnlStart;
		protected System.Web.UI.WebControls.TextBox txtCompanyName;
		protected System.Web.UI.WebControls.TextBox txtCompanyURL;
		protected System.Web.UI.WebControls.TextBox txtCreditLimit;
		protected System.Web.UI.WebControls.Button Button5;
		protected System.Web.UI.WebControls.Button Button2;
		protected System.Web.UI.WebControls.Panel pnlCompany;
		protected System.Web.UI.WebControls.TextBox txtbankname;
		protected System.Web.UI.WebControls.TextBox txtbankpers;
		protected System.Web.UI.WebControls.TextBox txtbankphon;
		protected System.Web.UI.WebControls.TextBox txtbankfax;
		protected System.Web.UI.WebControls.TextBox txtbankaddr1;
		protected System.Web.UI.WebControls.TextBox txtbankaddr2;
		protected System.Web.UI.WebControls.TextBox txtbankcity;
		protected System.Web.UI.WebControls.TextBox txtbankstat;
		protected System.Web.UI.WebControls.TextBox txtbankzip;
		protected System.Web.UI.WebControls.TextBox txtbankacnt;
		protected System.Web.UI.WebControls.TextBox txtbankrep;
		protected System.Web.UI.WebControls.TextBox txtbankaba;
		protected System.Web.UI.WebControls.Button Button7;
		protected System.Web.UI.WebControls.Button Button8;
		protected System.Web.UI.WebControls.Panel pnlBank;
		protected System.Web.UI.WebControls.Panel pnlLocations;
	
		/************************************************************************
		 *   1. File Name       :Administrator\Update_Company_Info.aspx          *
		 *   2. Description     :update all info of company                      *
		 *   3. Modification Log:                                                *
		 *     Ver No.       Date          Author             Modification       *
		 *   -----------------------------------------------------------------   *
		 *      1.00      2-24-2004      Xiaoda               Comment            *
		 *                                                                       *
		 ************************************************************************/
		SqlConnection conn;
		double dCredit_Available;
    
    
		public void Page_Load(object sender, EventArgs e)
		{
			//only adminstrator and borker can access this page.
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B")){
				   Response.Redirect("../default.aspx");
			}
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			if (!IsPostBack)
			{
    
				Bind_Controls();
			}
		}
    
    
    
    
		//The reason of having Id2 is because this page also links to exchange registration,
		//if someone registered as a Distributor then two companies(distributor, purchaser)
		//will be generated, so when we perform an update, it needs to update two compaies.
    
		//Please look administrator/Exchange_Registration.aspx to understand how registration works!
    
		//Id2 is the ID of Purchasre company.
		//if Id2 is not empty, which means it's from exchange user registration
    
    
		public void Bind_Controls()
		{
			String Id;
			Id = Request.QueryString["Id"];
			//Response.Write("Update, ID: "+Id+"end<br><br>");
			String Str;
			//Query the company based the Id
			Str="Select COMP_NAME, COMP_URL,COMP_TYPE, BANK_NAME, BANK_ACNT_NB, BANK_PERS, BANK_ADDR_ONE, BANK_ADDR_TWO, BANK_CITY, BANK_ZIP, BANK_STAT, BANK_CTRY, BANK_PHON, BANK_FAX, BANK_ABA, BANK_REP_NUMBER,(SELECT CAST(ROUND( CRED_AMNT, 0) AS int) FROM CREDIT WHERE CRED_COMP=COMP_ID) As CRED_AMNT,(SELECT CAST(ROUND( CRED_AVLB, 0) AS int) FROM CREDIT WHERE CRED_COMP=COMP_ID) As CRED_AVLB from COMPANY, BANK Where COMP_ID=@Cid And BANK_COMP=COMP_ID";
			SqlDataReader dtr;
			SqlCommand cmd;
			cmd= new SqlCommand(Str, conn);
			cmd.Parameters.Add("@Cid", Id);
			dtr= cmd.ExecuteReader();
			dtr.Read();
			if (dtr["COMP_NAME"] != DBNull.Value )
			{
				txtCompanyName.Text = (string)dtr["COMP_NAME"];
			}
			else
			{
				txtCompanyName.Text = "";
			}
    
			if (dtr["COMP_URL"] != DBNull.Value )
			{
				txtCompanyURL.Text = (string)dtr["COMP_URL"];
			}
			else
			{
				txtCompanyURL.Text = "";
			}
    
			if (dtr["BANK_NAME"] != DBNull.Value )
			{
				txtbankname.Text=(string)dtr["BANK_NAME"];
			}
			else
			{
				txtbankname.Text="";
			}
    
    
			if (dtr["BANK_ABA"] != DBNull.Value )
			{
				txtbankaba.Text=dtr["BANK_ABA"].ToString();
			}
			else
			{
				txtbankaba.Text="";
			}
    
    
			if (dtr["BANK_REP_NUMBER"] != DBNull.Value )
			{
				txtbankrep.Text=dtr["BANK_REP_NUMBER"].ToString();
			}
			else
			{
				txtbankrep.Text="";
			}
    
    
			if (dtr["CRED_AMNT"] != DBNull.Value )
			{
				lblCreditLimit.Text=String.Format("{0:c}",dtr["CRED_AMNT"]);
				txtCreditLimit.Text=dtr["CRED_AMNT"].ToString();
				lblOrigCreditLimit.Text=dtr["CRED_AMNT"].ToString();
			}
			if (dtr["CRED_AVLB"] != DBNull.Value )
			{
				lblCreditAvail.Text=String.Format("{0:c}",dtr["CRED_AVLB"]);
			}
    
    
			if (dtr["BANK_PERS"] != DBNull.Value )
			{
				txtbankpers.Text=(string)dtr["BANK_PERS"];
			}
			else
			{
				txtbankpers.Text="";
			}
    
			if (dtr["BANK_ACNT_NB"] != DBNull.Value )
			{
				txtbankacnt.Text=(string)dtr["BANK_ACNT_NB"];
			}
			else
			{
				txtbankacnt.Text="";
			}
    
			if (dtr["BANK_ADDR_ONE"] != DBNull.Value )
			{
				txtbankaddr1.Text=(string)dtr["BANK_ADDR_ONE"];
			}
			else
			{
				txtbankaddr1.Text="";
			}
    
			if (dtr["BANK_ADDR_TWO"] != DBNull.Value )
			{
				txtbankaddr2.Text=(string)dtr["BANK_ADDR_TWO"];
			}
			else
			{
				txtbankaddr2.Text="";
			}
    
    
			if (dtr["BANK_CITY"] != DBNull.Value )
			{
				txtbankcity.Text=(string)dtr["BANK_CITY"];
			}
			else
			{
				txtbankcity.Text="";
			}
    
    
			if (dtr["BANK_ZIP"] != DBNull.Value )
			{
				txtbankzip.Text=(string)dtr["BANK_ZIP"];
			}
			else
			{
				txtbankzip.Text="";
			}
    
			if (dtr["BANK_STAT"] != DBNull.Value )
			{
				txtbankstat.Text=(string)dtr["BANK_STAT"];
			}
			else
			{
				txtbankstat.Text="";
			}
    
			if (dtr["BANK_PHON"] != DBNull.Value )
			{
				txtbankphon.Text=(string)dtr["BANK_PHON"];
			}
			else
			{
				txtbankphon.Text="";
			}
    
			if (dtr["BANK_FAX"] != DBNull.Value )
			{
				txtbankfax.Text=(string)dtr["BANK_FAX"];
			}
			else
			{
				txtbankfax.Text="";
			}
    
			if (dtr["COMP_URL"] != DBNull.Value )
			{
				lblcompanyURL.Text="<A target=\"parent\" href=\"http://"+dtr["COMP_URL"].ToString()+"\">"+dtr["COMP_URL"].ToString()+"</a>";
			}
			else
			{
				lblcompanyURL.Text="";
			}
    
			if (dtr["COMP_NAME"] != DBNull.Value )
			{
				lblcompanyname.Text=(string)dtr["COMP_NAME"];
			}
			else
			{
				lblcompanyname.Text="";
			}
    
			if (dtr["COMP_NAME"] != DBNull.Value )
			{
				lblcompanyname2.Text=dtr["COMP_NAME"].ToString() ;
				if (dtr["COMP_TYPE"] != DBNull.Value)
				{
					lblcompanyname2.Text+= "(" +dtr["COMP_TYPE"].ToString() + ")";
				}
			}
			else
			{
				lblcompanyname2.Text="";
			}
    
    
			if (dtr["BANK_NAME"] != DBNull.Value )
			{
				lblbankname.Text=(string)dtr["BANK_NAME"];
			}
			else
			{
				lblbankname.Text="";
			}
    
			if (dtr["BANK_PERS"] != DBNull.Value )
			{
				lblbankpers.Text=(string)dtr["BANK_PERS"];
			}
			else
			{
				lblbankpers.Text="";
			}
    
			if (dtr["BANK_ACNT_NB"] != DBNull.Value )
			{
				lblbankacnt.Text=(string)dtr["BANK_ACNT_NB"];
			}
			else
			{
				lblbankacnt.Text="";
			}
    
			/*if (dtr["BANK_REP_NUMBER"] != DBNull.Value )
								{
								lblbankrep.Text=(string)dtr["BANK_REP_NUMBER"];
								}
								else
								{
								lblbankrep.Text="";
			   }*/
    
    
			if (dtr["BANK_ADDR_ONE"] != DBNull.Value )
			{
				lblbankaddr1.Text=(string)dtr["BANK_ADDR_ONE"];
			}
			else
			{
				lblbankaddr1.Text="";
			}
    
			if (dtr["BANK_ADDR_TWO"] != DBNull.Value )
			{
				lblbankaddr2.Text=(string)dtr["BANK_ADDR_TWO"];
			}
			else
			{
				lblbankaddr2.Text="";
			}
    
			if (dtr["BANK_CITY"] != DBNull.Value )
			{
				lblbankcity.Text=(string)dtr["BANK_CITY"];
			}
			else
			{
				lblbankcity.Text="";
			}
    
			if (dtr["BANK_ZIP"] != DBNull.Value )
			{
				lblbankzip.Text=(string)dtr["BANK_ZIP"];
			}
			else
			{
				lblbankzip.Text="";
			}
    
			if (dtr["BANK_STAT"] != DBNull.Value )
			{
				lblbankstat.Text=(string)dtr["BANK_STAT"];
			}
			else
			{
				lblbankstat.Text="";
			}
    
    
			/*if (dtr["BANK_CTRY"] != DBNull.Value )
									  {
									  lblbankctry.Text=(string)dtr["BANK_CTRY"];
									  }
									  else
									  {
									  lblbankctry.Text="";
			   } */
    
			if (dtr["BANK_PHON"] != DBNull.Value )
			{
				lblbankphon.Text=(string)dtr["BANK_PHON"];
			}
			else
			{
				lblbankphon.Text="";
			}
    
			if (dtr["BANK_FAX"] != DBNull.Value )
			{
				lblbankfax.Text=(string)dtr["BANK_FAX"];
			}
			else
			{
				lblbankfax.Text="";
			}
			dtr.Close();
    
			// binding location info
			Str ="Select PLAC_TYPE,PLAC_ID,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) AS PLAC_LOCATION,PLAC_COMP from PLACE Where PLAC_COMP=@Cid";
			SqlCommand cmdloca;
			SqlDataReader dtrloca;
			cmdloca= new SqlCommand(Str, conn);
			cmdloca.Parameters.Add("@Cid", Id);
			dtrloca= cmdloca.ExecuteReader();
    
    
			while (dtrloca.Read())
			{
				if (dtrloca["PLAC_TYPE"] != DBNull.Value )
				{
					// headquarters and shipping point go to different locations
					if ((string)dtrloca["PLAC_TYPE"] == "H") 
					{
						phHeadquarters.Controls.Add (new LiteralControl("<tr><td>Headquarters:</td><td><a onmouseover=this.style.cursor='hand' onclick=javascript:Address_PopUp("+dtrloca["PLAC_ID"]+")><u>"+dtrloca["PLAC_LOCATION"]+"</u></a></td></tr>"));
					}
					else if ((string)dtrloca["PLAC_TYPE"] == "S") 
					{
						phShippingPoints.Controls.Add (new LiteralControl("<tr><td>Shipping Point:</td><td><a onmouseover=this.style.cursor='hand' onclick=javascript:Address_PopUp("+dtrloca["PLAC_ID"]+")><u>"+dtrloca["PLAC_LOCATION"]+"</u></a></td></tr>"));
					}
					else
					{
						phShippingPoints.Controls.Add (new LiteralControl("<tr><td>Delivery Point:</td><td><a onmouseover=this.style.cursor='hand' onclick=javascript:Address_PopUp("+dtrloca["PLAC_ID"]+")><u>"+dtrloca["PLAC_LOCATION"]+"</u></a></td></tr>"));
					}
				}
    
			}
			dtrloca.Close();
    
    
			// binding User info
    
			Str ="Select PERS_COMP,PERS_TYPE,PERS_TITL,PERS_ID,PERS_FRST_NAME, PERS_LAST_NAME  FROM PERSON WHERE PERS_COMP=@Cid";
			SqlCommand cmdUsers;
			SqlDataReader dtrUsers;
			cmdUsers= new SqlCommand(Str, conn);
			cmdUsers.Parameters.Add("@Cid", Id);
			dtrUsers= cmdUsers.ExecuteReader();
    
			while (dtrUsers.Read())
			{
				// users and officers are listed seperatly point go to different locations
				if ((string)dtrUsers["PERS_TYPE"] == "O") 
				{
					//phOfficer.Controls.Add (new LiteralControl("<tr><td>Officer:</td><td><a href='../administrator/Update_User_Info.aspx?nav=Customers&Id="+dtrUsers["PERS_ID"]+"'><u>"+dtrUsers["PERS_NAME"]+"</u></a></td></tr>"));
    
    
					//phOfficer.Controls.Add (new LiteralControl("<tr><td>Officer:</td><td>"+dtrUsers["PERS_NAME"]+"</td></tr>"));
    
					phOfficer.Controls.Add (new LiteralControl("<tr><td>Officer:</td><td><a onmouseover=this.style.cursor='hand' onclick=javascript:Person_PopUp("+dtrUsers["PERS_ID"]+")><u>"+dtrUsers["PERS_FRST_NAME"]+"&nbsp"+dtrUsers["PERS_LAST_NAME"]+"</u></a></td></tr>"));
    
				}
				if ((string)dtrUsers["PERS_TYPE"] == "P")
				{
    
					phPurchaser.Controls.Add (new LiteralControl("<tr><td>Purchaser:</td><td><a onmouseover=this.style.cursor='hand' onclick=javascript:Person_PopUp("+dtrUsers["PERS_ID"]+")><u>"+dtrUsers["PERS_FRST_NAME"]+"&nbsp"+dtrUsers["PERS_LAST_NAME"]+"</u></a></td></tr>"));
				}
    
				if ((string)dtrUsers["PERS_TYPE"] == "S")
				{
    
					phSupplier.Controls.Add (new LiteralControl("<tr><td>Supplier:</td><td><a onmouseover=this.style.cursor='hand' onclick=javascript:Person_PopUp("+dtrUsers["PERS_ID"]+")><u>"+dtrUsers["PERS_FRST_NAME"]+"&nbsp"+dtrUsers["PERS_LAST_NAME"]+"</u></a></td></tr>"));
				}
				if ((string)dtrUsers["PERS_TYPE"] == "A")
				{

					phSupplier.Controls.Add (new LiteralControl("<tr><td>Admin:</td><td><a onmouseover=this.style.cursor='hand' onclick=javascript:Person_PopUp("+dtrUsers["PERS_ID"]+")><u>"+dtrUsers["PERS_FRST_NAME"]+"&nbsp"+dtrUsers["PERS_LAST_NAME"]+"</u></a></td></tr>"));
				}
				if ((string)dtrUsers["PERS_TYPE"] == "B")
				{

					phSupplier.Controls.Add (new LiteralControl("<tr><td>Broker:</td><td><a onmouseover=this.style.cursor='hand' onclick=javascript:Person_PopUp("+dtrUsers["PERS_ID"]+")><u>"+dtrUsers["PERS_FRST_NAME"]+"&nbsp"+dtrUsers["PERS_LAST_NAME"]+"</u></a></td></tr>"));
				}
    
    
    
			}
			dtrUsers.Close();
			conn.Close();
		}
    
		public void Update_Company(object sender, EventArgs e)
		{
			//show the company info, hide rest
			pnlStart.Visible = false;
			pnlCompany.Visible = true;
    
		}
    
		public void Save_Company_Info(object sender, EventArgs e)
		{
    
			//if it's from exchange registration(Id2 is not empty), then update two companies!
			string strSQL;
			string Id;
			Id = Request.QueryString["Id"];
			strSQL="UPDATE COMPANY SET COMP_NAME= '"+(string)txtCompanyName.Text+"',COMP_URL='"+(string)txtCompanyURL.Text+"' Where COMP_ID=@Cid";
			SqlCommand cmdCom;
			cmdCom= new SqlCommand(strSQL, conn);
			cmdCom.Parameters.Add("@Cid", Id);
			cmdCom.ExecuteNonQuery();              
			// update credit table.
			// Yes, I know this should be done above,but I'm lazy!
    
    
			if( txtCreditLimit.Text == "")
			{
				strSQL="UPDATE CREDIT SET CRED_AVLB=CRED_AVLB - "+(Convert.ToInt32(lblOrigCreditLimit.Text))+", CRED_AMNT= '0' Where CRED_COMP=@Cid";
			}
			else 
			{
				strSQL="UPDATE CREDIT SET CRED_AVLB=CRED_AVLB + " + (Convert.ToInt32(txtCreditLimit.Text)-Convert.ToInt32(lblOrigCreditLimit.Text))+", CRED_AMNT= '"+(string)txtCreditLimit.Text+"' Where CRED_COMP=@Cid";
			}
    
			SqlCommand cmdCredit;
			cmdCredit= new SqlCommand(strSQL, conn);
			cmdCredit.Parameters.Add("@Cid", Id);
			cmdCredit.ExecuteNonQuery();
    
			//reset controls
			Bind_Controls();
			pnlStart.Visible = true;
			pnlCompany.Visible = false;
			conn.Close();
		}
		public void CompanyInfoBack(object sender, EventArgs e)
		{
			//hides the company info
			pnlStart.Visible = true;
			pnlCompany.Visible = false;
			Bind_Controls();
		}
    
		public void Save_Bank(object sender, EventArgs e)
		{
			//if it's from exchange registration(Id2 is not empty), then update two companies credit!
			string strSQL;
			string Id;
			Id = Request.QueryString["Id"];
			strSQL="Update BANK set BANK_NAME='"+txtbankname.Text+"', BANK_ACNT_NB='"+txtbankacnt.Text+"', BANK_REP_NUMBER='"+txtbankrep.Text+"', BANK_PERS='"+txtbankpers.Text+"', BANK_ADDR_ONE='"+txtbankaddr1.Text+"', BANK_ADDR_TWO='"+txtbankaddr2.Text+"', BANK_CITY='"+txtbankcity.Text+"', BANK_ZIP='"+txtbankzip.Text+"', BANK_STAT='"+txtbankstat.Text+"', BANK_PHON='"+txtbankphon.Text+"', BANK_FAX='"+txtbankfax.Text+"', BANK_ABA='"+txtbankaba.Text+"' Where BANK_COMP=@Cid";
			SqlCommand cmdCom;
			cmdCom= new SqlCommand(strSQL, conn);
			cmdCom.Parameters.Add("@Cid", Id);
			cmdCom.ExecuteNonQuery();
			pnlStart.Visible = true;
			pnlBank.Visible = false;
    
			//reset controls
			Bind_Controls();
		}
		public void Update_Bank(object sender, EventArgs e)
		{
			//show deatiled bank info, hides rest
			pnlStart.Visible = false;
			pnlBank.Visible = true;
			Bind_Controls();
		}
    
		public void BankBack(object sender, EventArgs e)
		{
			//user clicks on "cancel", then hides bank info
			pnlStart.Visible = true;
			pnlBank.Visible = false;
			Bind_Controls();
		}
		public void Add_Location(object sender, EventArgs e)
		{
			//add location at different page
			conn.Close();
			Server.Transfer("../administrator/Add_Location.aspx");
		}
		public void Add_User(object sender, EventArgs e)
		{
			//add new user at different page
			string Id;
			Id = Request.QueryString["Id"];
			conn.Close();
			Server.Transfer("../administrator/Add_User.aspx?Id="+Id+"");
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
