using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;
using System.Text.RegularExpressions;
using System.Text;

namespace localhost.Administrator
{
    public partial class Manage_Lead : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {          
            if (!IsPostBack)
            {
                using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
                {
                    conn.Open();

                    // binding allocation
                    string id = Session["ID"].ToString();                                     
                    ddlMoveTo.Items.Add(new ListItem("Your Leads", "1"));                    

                    string strSQL = "SELECT (SELECT COUNT(*) FROM PERSON WHERE PERS_ENBL='1' AND PERS_TYPE ='D' AND PERS_ACES='" + id + "' and pers_id not in (select group_member_contact_id from group_member)) AS ShowAll,(SELECT COUNT(*) FROM PERSON WHERE PERS_ENBL='0' AND PERS_TYPE='D' AND PERS_ACES=" + id + ") AS Recycled FROM [PERSON]";
                    using (SqlDataReader dtrGroup = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
                    {
                        if (dtrGroup.Read())
                        {
                            ddlMoveTo.Items.Add(new ListItem("Recycle Bin (" + dtrGroup["Recycled"].ToString() + ")", "0"));
                        }
                    }

                    string strSQL2 = "SELECT GROUP_NAME,(SELECT COUNT(*) FROM GROUP_MEMBER WHERE GROUP_MEMBER_GROUP_ID = GROUP_ID AND GROUP_MEMBER_BROKER_ID=" + id + ") AS COUNT,GROUP_ID FROM [GROUP] WHERE GROUP_BROKER=" + id + " ORDER BY GROUP_NAME";
                    using (SqlDataReader dtrGroup = DBLibrary.GetDataReaderFromSelect(conn, strSQL2))
                    {
                        while (dtrGroup.Read())
                        {
                            ddlMoveTo.Items.Add(new ListItem(dtrGroup["GROUP_NAME"].ToString() + " (" + dtrGroup["COUNT"].ToString() + ")", dtrGroup["GROUP_ID"].ToString()));
                        }
                    }

                    ddlMoveTo.Items[0].Selected = true;
               
                    string strSQL1;
                    string strOutput;

                    strSQL1 = "Select PERS_ID,PERS_FRST_NAME+ ' ' + PERS_LAST_NAME AS Name, PERS_CATEGORY, ISNULL(PERS_PRMLY_INTRST, 1) AS MARKET FROM PERSON WHERE PERS_ID IN (";
                    strSQL1 += Request.QueryString["User"].ToString().Substring(0, Request.QueryString["User"].ToString().Length - 1);
                    strSQL1 += ")";

                    strOutput = "";
                    using (SqlDataReader dtrAllocateList = DBLibrary.GetDataReaderFromSelect(conn, strSQL1))
                    {
                        while (dtrAllocateList.Read())
                        {
                            strOutput += "<tr><td class=\"Content\">" + dtrAllocateList["Name"].ToString() + "<td align=\"center\"><input type=\"hidden\" name=\"Id_" + dtrAllocateList["PERS_ID"].ToString() + "\"> </td></td></tr>";                            
                        }
                        phAllocate.Controls.Add(new LiteralControl(strOutput));
                    }                    
                }
            }
        }

        public void Cancel_Allocate(object sender, EventArgs e)
        {
            Response.Redirect("/administrator/crm_new.aspx");
        }

        public void Submit_Allocate(object sender, EventArgs e)
        {
            string group_member_contact_id = Request.QueryString["User"].ToString().Substring(0, Request.QueryString["User"].ToString().Length - 1);
            Regex r = new Regex(",");
            string[] array_contact_id = r.Split(group_member_contact_id);
            string broker_id = Session["ID"].ToString();
            string folder_to = ddlMoveTo.SelectedValue;
            string folder_from = Session["Folder"].ToString();

            if (folder_from != folder_to)
            {
                if (folder_from == "1")
                {
                    MoveFromAllLeads(array_contact_id, folder_to, broker_id);
                }

                if (folder_from == "0")
                {
                    MoveFromRecycled(array_contact_id, folder_to, broker_id);
                }

                if (folder_from != "1" && folder_from != "0")
                {
                    MoveFromFolders(array_contact_id, folder_to, broker_id);
                }
            }
            Response.Redirect("/administrator/crm_new.aspx");            
        }

        private void MoveFromAllLeads(string[] array_contact_id, string folder_to, string broker_id)
        {
            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();
                StringBuilder sbSQL = new StringBuilder();
                for (int i = 0; i < array_contact_id.Length; i++)
                {
                    if (folder_to == "0")
                    {
                        sbSQL.Append("UPDATE PERSON SET PERS_ENBL='0' WHERE PERS_ID='" + array_contact_id[i] + "';");
                    }
                    else 
                    {
                        sbSQL.Append("INSERT INTO GROUP_MEMBER( GROUP_MEMBER_GROUP_ID,GROUP_MEMBER_BROKER_ID,GROUP_MEMBER_CONTACT_ID) VALUES ('" + folder_to + "','" + broker_id + "','" + array_contact_id[i] + "') ;");
                    }                                                            
                    SqlCommand cmd = new SqlCommand(sbSQL.ToString(), conn);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
            }
        }

        private void MoveFromRecycled(string[] array_contact_id, string folder_to, string broker_id)
        {
            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();
                StringBuilder sbSQL = new StringBuilder();
                for (int i = 0; i < array_contact_id.Length; i++)
                {                    
                    sbSQL.Append("UPDATE PERSON SET PERS_ENBL='1' WHERE PERS_ID='" + array_contact_id[i] + "';");
                    if (folder_to != "1") sbSQL.Append("INSERT INTO GROUP_MEMBER( GROUP_MEMBER_GROUP_ID,GROUP_MEMBER_BROKER_ID,GROUP_MEMBER_CONTACT_ID) VALUES ('" + folder_to + "','" + broker_id + "','" + array_contact_id[i] + "') ;");               
         
                    SqlCommand cmd = new SqlCommand(sbSQL.ToString(), conn);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
            }            
        }

        private void MoveFromFolders(string[] array_contact_id, string folder_to, string broker_id)
        {
            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();
                StringBuilder sbSQL = new StringBuilder();
                for (int i = 0; i < array_contact_id.Length; i++)
                {
                    if (folder_to == "0")
                    {
                        sbSQL.Append("UPDATE PERSON SET PERS_ENBL='0' WHERE PERS_ID='" + array_contact_id[i] + "';");
                        sbSQL.Append("DELETE FROM GROUP_MEMBER WHERE GROUP_MEMBER_BROKER_ID = '" + broker_id + "' AND GROUP_MEMBER_CONTACT_ID = '" + array_contact_id[i] + "' ;");
                    }
                    else if (folder_to == "1")
                    {
                        sbSQL.Append("DELETE FROM GROUP_MEMBER WHERE GROUP_MEMBER_BROKER_ID = '" + broker_id + "' AND GROUP_MEMBER_CONTACT_ID = '" + array_contact_id[i] + "' ;");
                    }
                    else
                    {
                        sbSQL.Append("UPDATE GROUP_MEMBER SET GROUP_MEMBER_GROUP_ID = '"+folder_to+"' WHERE GROUP_MEMBER_BROKER_ID = '"+broker_id+"' AND GROUP_MEMBER_CONTACT_ID = '"+array_contact_id[i]+"' ;");
                    }
                    SqlCommand cmd = new SqlCommand(sbSQL.ToString(), conn);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
            }
        }
    }
}
