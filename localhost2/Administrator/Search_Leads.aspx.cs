using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Administrator
{
	public partial class Search_Leads : System.Web.UI.Page
	{
		protected void Page_Load( object sender, EventArgs e )
		{

			string sessionType = (string)Session["Typ"];	//Authority Checking
			if( ( sessionType != "A" ) && ( sessionType != "C" ) )
				Response.Redirect("../default.aspx");

			Session["CompID"] = "";
			Master.Width = "1200px";

			if( !IsPostBack )
			{
				Bind();
			}
		}

		protected void Bind()
		{
			using( SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()) )
			{
				conn.Open();
				System.Text.StringBuilder sbSQL = new System.Text.StringBuilder();
				DataSet dstContent;
				Hashtable htParams = new Hashtable();
									
				if( txtSearch.Text != "" )
				{
					htParams.Add("@Search", txtSearch.Text);
				}

				dstContent = DBLibrary.GetDataSetFromStoredProcedure(conn, "spSearchLeads", htParams);

				DataView dv = new DataView(dstContent.Tables[0]);

				if( ViewState["SortExpression"] != null )
				{
					dv.Sort = ViewState["SortExpression"].ToString();
					if( ViewState["SortDirection"] != null )
						dv.Sort += " " + ViewState["SortDirection"].ToString();
				}

				dg.DataSource = dv;
				try
				{
					dg.DataBind();
				}
				catch
				{
					//Response.Write(ex.Message + " " + ex.InnerException);
				}

				if( dg.Items.Count == 0 )
					lblMessage.Visible = true;
				else
					lblMessage.Visible = false;

			}
		}

		protected void btnSearch_Click( object sender, EventArgs e )
		{
			Bind();
		}

		protected void dg_ItemDataBound( object sender, DataGridItemEventArgs e )
		{
			try
			{
				if( e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer )
				{
					if( e.Item.Cells.Count > 1 )
					{
						if( e.Item.Cells[6].Text != "&nbsp;" )
						{
							e.Item.Cells[6].Text = "<img src='/pics/Icons/icon_quotation_bubble.gif'>";
						}
						else
						{
							e.Item.Cells[6].Text = "<img src='/pics/Icons/comment.gif'>";
						}
					}
				}
			}
			catch( Exception ex )
			{
			}
		}

		protected void dg_SortCommand( object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e )
		{
			if( ViewState["SortDirection"] == null )
				ViewState.Add("SortDirection", "ASC");
			else
			{
				if( ViewState["SortDirection"].ToString() == "ASC" )
				{
					ViewState["SortDirection"] = "DESC";
				}
				else
				{
					ViewState["SortDirection"] = "ASC";
				}
			}
			ViewState["SortExpression"] = e.SortExpression.ToString();
			dg.CurrentPageIndex = 0;
			Bind();
		}
	}
}
