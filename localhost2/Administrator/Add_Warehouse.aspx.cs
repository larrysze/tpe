using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;
using System.Web.Mail;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for AddWarehouse.
	/// </summary>
	public partial class AddWarehouse : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			Master.Width = "765px";
			if (Session["Typ"].ToString() != "A" )
			{
				Response.Redirect("/default.aspx");
			}

            Master.Width = "1000px";

			if (IsPostBack)
			{
				if(Request.Params["Edit"] != null)
				{
					edit_Warehouse(Request.Params["Edit"]);
				}

			}				
			else
			{
                ViewState["ID"] = Session["ID"].ToString();//get the user id

                Hashtable htParams = new Hashtable();
                htParams.Add("@ID", Session["ID"].ToString());
                using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
                {
                    conn.Open();
                    using (SqlDataReader rd = DBLibrary.GetDataReaderFromSelect(conn, "select pers_mail from person where pers_id=@ID", htParams))
                    {
                        if (rd.Read())
                        {
                            this.ViewState["login_mail"] = rd["pers_mail"].ToString();
                        }
                    }
                }			
                
				Bind("PLAC_ID DESC");
			}
            btnConfirmDelete.OnClientClick = String.Format("fnClickUpdate('{0}','{1}')", btnConfirmDelete.UniqueID, "");

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_ItemCommand);
			this.dg.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dg_PageIndexChanged);
			this.dg.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dg_SortCommand);
            this.dg.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_ItemDataBound);
            
            
		}
		#endregion

		
		private void createWarehouse(int local_id)
		{
			string sqlInsert = "INSERT INTO PLACE(PLAC_TYPE, PLAC_EMAIL,  PLAC_ADDR_ONE, PLAC_ADDR_TWO, PLAC_ADDR_THREE, PLAC_PERS, PLAC_LABL, PLAC_PHON, PLAC_RAIL_NUM, PLAC_INST, PLAC_LOCL, PLAC_ZIP,PLAC_FAX) VALUES( ";
				sqlInsert += " 'W',";
				sqlInsert += "@PLAC_EMAIL,";
				sqlInsert += "@PLAC_ADDR_ONE,";
				sqlInsert += "@PLAC_ADDR_TWO,";
			sqlInsert += "@PLAC_ADDR_THREE,";
				sqlInsert += "@PLAC_PERS,";
				sqlInsert += "@PLAC_LABL,";
				sqlInsert += "@PLAC_PHON,";
				sqlInsert += "@PLAC_RAIL_NUM,";
				sqlInsert += "@PLAC_INST,";
				sqlInsert += "@PLAC_LOCL,";
				sqlInsert += "@PLAC_ZIP,";
			sqlInsert += "@PLAC_FAX)";

			Hashtable ht = new Hashtable();
			ht.Add("@PLAC_EMAIL", txtEmail.Text);
			ht.Add("@PLAC_ADDR_ONE", txtAddr.Text);
			ht.Add("@PLAC_ADDR_TWO", txtAddr2.Text);
			ht.Add("@PLAC_ADDR_THREE", txtAddr3.Text);
			ht.Add("@PLAC_PERS", txtContact.Text);
			ht.Add("@PLAC_LABL", txtLabel.Text);
			ht.Add("@PLAC_PHON", txtPhone.Text);
			ht.Add("@PLAC_FAX", txtFax.Text);
			ht.Add("@PLAC_RAIL_NUM", txtRail.Text);
			ht.Add("@PLAC_INST", txtWName.Text);
			ht.Add("@PLAC_LOCL", local_id);
			ht.Add("@PLAC_ZIP", txtZip.Text);
			
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sqlInsert, ht);
		}

		private void editWarehouse(int plac_id, int local_id)
		{			
			Hashtable ht = new Hashtable();
			
			string sqlUpdate = "UPDATE PLACE SET ";
			sqlUpdate += " PLAC_LOCL=@PLAC_LOCL,";
			ht.Add("@PLAC_LOCL", local_id);
			if(txtAddr.Text != ""){
				sqlUpdate += "PLAC_ADDR_ONE=@PLAC_ADDR_ONE,";
				ht.Add("@PLAC_ADDR_ONE", txtAddr.Text);
			}
//			if(txtAddr2.Text != "")
//			{
				sqlUpdate += "PLAC_ADDR_TWO=@PLAC_ADDR_TWO,";
				ht.Add("@PLAC_ADDR_TWO", txtAddr2.Text);
//			}
			sqlUpdate += "PLAC_ADDR_THREE=@PLAC_ADDR_THREE,";
			ht.Add("@PLAC_ADDR_THREE", txtAddr3.Text);

			if( txtEmail.Text != "" )
			{
				sqlUpdate += "PLAC_EMAIL=@PLAC_EMAIL,";
				ht.Add("@PLAC_EMAIL", txtEmail.Text);
			}

			if(txtContact.Text != "")
			{
				sqlUpdate += "PLAC_PERS=@PLAC_PERS,";
				ht.Add("@PLAC_PERS", txtContact.Text);
			}
//			if(txtLabel.Text != "")
//			{
			sqlUpdate += "PLAC_LABL=@PLAC_LABL,";
			ht.Add("@PLAC_LABL", txtLabel.Text);
//			}
			if(txtPhone.Text != "")
			{
				sqlUpdate += "PLAC_PHON=@PLAC_PHON,";
				ht.Add("@PLAC_PHON", txtPhone.Text);
			}
			if (txtFax.Text != "")
			{
				sqlUpdate += "PLAC_FAX=@PLAC_FAX,";
				ht.Add("@PLAC_FAX", txtFax.Text);
			}
			if(txtWName.Text != "")
			{
				sqlUpdate += "PLAC_INST=@PLAC_INST,";
				ht.Add("@PLAC_INST", txtWName.Text);
			}
//			if(txtRail.Text != "")
//			{
			sqlUpdate += "PLAC_RAIL_NUM=@PLAC_RAIL_NUM,";
			ht.Add("@PLAC_RAIL_NUM", txtRail.Text );
//			}

			if(txtZip.Text != "")
			{
				sqlUpdate += "PLAC_ZIP=@PLAC_ZIP";
				ht.Add("@PLAC_ZIP", txtZip.Text);
			}
			else
			{
				//we don't need ","
				sqlUpdate = sqlUpdate.Remove(sqlUpdate.Length - 1, 1);
			}

			sqlUpdate += " WHERE plac_id=" + plac_id;

			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sqlUpdate, ht);

			Bind("country");
		}

		private int getLocality()
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			int ret = -1;

			Hashtable ht = new Hashtable();
			ht.Add("@LOCL_CITY", txtCity.Text);

			string strState = ddlState.SelectedItem.Text;
			string strSql = "SELECT LOCL_ID FROM locality WHERE LOCL_CITY=@LOCL_CITY";

			if(strState != "Not Applicable")
			{
				strSql += " and LOCL_STAT = (SELECT STAT_CODE FROM STATE WHERE STAT_NAME='" + strState + "')";				
			}

			strSql += " and LOCL_CTRY = (SELECT CTRY_CODE FROM COUNTRY WHERE CTRY_NAME='" + ddlCountry.SelectedItem.Text + "')";
						
			SqlDataReader dtrLocality = DBLibrary.GetDataReaderFromSelect(conn, strSql, ht);
            
			if(dtrLocality.Read())
			{
				ret = Convert.ToInt32(dtrLocality["LOCL_ID"].ToString());
			}

			return ret;
		}

		private void createLocality()
		{
			string strCTRY_CODE = "";
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
			
				//retrieving CTRY_CODE
				Hashtable ht = new Hashtable();
				ht.Add("@Country",ddlCountry.SelectedItem.Text);
				using (SqlDataReader dtrCountry = DBLibrary.GetDataReaderFromSelect(conn,"SELECT CTRY_CODE FROM COUNTRY WHERE CTRY_NAME=@Country",ht))
				{
					if(dtrCountry.Read())
					{
						strCTRY_CODE = dtrCountry["CTRY_CODE"].ToString();
					}
				}				
				string strSTAT_CODE = "";

				if(ddlState.SelectedItem.Text == "Not Applicable")
				{
					strSTAT_CODE = "";
				}
				else
				{
					string strSql = "SELECT STAT_CODE FROM STATE WHERE STAT_NAME=@State AND STAT_CTRY=@Country";// + ddlState.SelectedItem.Text + "' AND STAT_CTRY='" + strCTRY_CODE + "'";
					ht.Clear();
					ht.Add("@State", ddlState.SelectedItem.Text);
					ht.Add("@Country", strCTRY_CODE);
					using (SqlDataReader dtrState = DBLibrary.GetDataReaderFromSelect(conn,strSql,ht))
					{
						if(dtrState.Read())
						{
							strSTAT_CODE = dtrState["STAT_CODE"].ToString();
						}
					}
				}

				ht.Clear();
				ht.Add("@LOCL_CITY", txtCity.Text);
			
				string strSqlInsert = "INSERT INTO locality(LOCL_CITY, LOCL_STAT, LOCL_CTRY) VALUES(@LOCL_CITY,'" + strSTAT_CODE + "','" +  strCTRY_CODE + "')";
						
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlInsert,ht);
			}
		}


		protected void btncreate_Click(object sender, System.EventArgs e)
		{
			lblTitle.Text = "Creating Warehouse";
			pnlDefault.Visible = false;
			pnlSave.Visible = true;
			addAllCountries();
			updateListofStates(ddlCountry.SelectedValue);
			Session["warehouse"] = "-1";
		}

		public void Bind(string SQLOrder)
		{			
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
				string strSQL;
				strSQL ="";
		
				strSQL="select company=(select COMP_NAME from Company where comp_ID=Plac_comp), *, city=(Select locl_city from locality where LOCL_ID=PLAC_LOCL), state=(Select locl_STAT from locality where LOCL_ID=PLAC_LOCL), country=(Select locl_ctry from locality where LOCL_ID=PLAC_LOCL) from place WHERE PLAC_TYPE='W' ORDER BY "+ SQLOrder;
				SqlDataAdapter dadContent;
				DataSet dstContent;
				dadContent = new SqlDataAdapter(strSQL ,conn);
				dstContent = new DataSet();
				dadContent.Fill(dstContent);
				dg.DataSource = dstContent;
				dg.DataBind();
			}
		}

		private void addAllCountries()
		{

			ddlCountry.Items.Clear();
			
			ddlCountry.Items.Add("United States");
			ddlCountry.Items.Add("Mexico");
			ddlCountry.Items.Add("Canada");
			ddlCountry.Items.Add("Brazil");
			ddlCountry.Items.Add("Chile");
			ddlCountry.Items.Add("Singapore");
		}

		private void edit_Warehouse(string plac_id)
		{
			Session["warehouse"] = plac_id;
			lblTitle.Text = "Editing Warehouse";
			
			pnlDefault.Visible = false;
			pnlSave.Visible = true;

			txtContact.Text = plac_id;

			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				conn.Open();
								
				Hashtable ht = new Hashtable();
				ht.Add("@PLAC_ID", plac_id);
			
				SqlDataReader dtrWarehouse = DBLibrary.GetDataReaderFromSelect(conn, "SELECT * FROM place WHERE PLAC_ID=@PLAC_ID",ht);

				if (dtrWarehouse.Read())
				{
					txtAddr.Text = dtrWarehouse["PLAC_ADDR_ONE"].ToString();
					txtAddr2.Text = dtrWarehouse["PLAC_ADDR_TWO"].ToString();
					txtAddr3.Text = dtrWarehouse["PLAC_ADDR_THREE"].ToString();
					txtContact.Text = dtrWarehouse["PLAC_PERS"].ToString();
					txtLabel.Text = dtrWarehouse["PLAC_LABL"].ToString();
					txtPhone.Text = dtrWarehouse["PLAC_PHON"].ToString();
					txtFax.Text = dtrWarehouse["PLAC_FAX"].ToString();
					txtRail.Text = dtrWarehouse["PLAC_RAIL_NUM"].ToString();
					txtWName.Text = dtrWarehouse["PLAC_INST"].ToString();
					txtZip.Text = dtrWarehouse["PLAC_ZIP"].ToString();
					txtEmail.Text = dtrWarehouse["PLAC_EMAIL"].ToString();


					addAllCountries();

					//have to get Country/State/City from Location|record
					string sql= "select city=" + 
						"(Select locl_city from locality where LOCL_ID=PLAC_LOCL), " + 
					
						" state=" + 
						"(select stat_name from state " + 
						"where stat_code = " + 
						"(Select locl_STAT from locality where LOCL_ID=PLAC_LOCL) " + 
						" and STAT_CTRY=" + 
						"(SELECT LOCL_CTRY from locality WHERE LOCL_ID=PLAC_LOCL))," + 

						" country=" + 
						"(select ctry_name from country where " + 
						"ctry_code = " + 
						"(Select locl_ctry from locality where LOCL_ID=PLAC_LOCL))" + 
					
						" from place WHERE PLAC_ID=@PLAC_ID";
				
					string strCurrentCountry = "";
					string strCurrentState = "";
					string strCurrentCity = "";
					using (SqlConnection conn2 = new SqlConnection(Application["DBconn"].ToString()))
					{
						conn2.Open();
						using (SqlDataReader dtrGetLocation = DBLibrary.GetDataReaderFromSelect(conn2, sql,ht))
						{				
							if(dtrGetLocation.Read())
							{
								strCurrentCountry = dtrGetLocation["country"].ToString();
								strCurrentState = dtrGetLocation["state"].ToString();
								strCurrentCity = dtrGetLocation["city"].ToString();
							}
						}					
					}
					updateListofStates(strCurrentCountry);

					ListItem find;
					if((find = ddlCountry.Items.FindByText(strCurrentCountry)) == null)
					{
						//hope that this never can happen
						ddlCountry.Items.Insert(0, strCurrentCountry);
						ddlCountry.SelectedIndex = 0;
					}
					else
					{
						ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(find);
					}
				
					if((ddlState.Items.FindByText("Not Applicable") == null) &&(find = ddlState.Items.FindByText(strCurrentState)) == null)
					{
						ddlState.Items.Insert(0, strCurrentState);
						ddlState.SelectedIndex = 0;
					}
					else
					{
						ddlState.SelectedIndex = ddlState.Items.IndexOf(find);
					}
					
					txtCity.Text = strCurrentCity;
				}
			}
		}

		private void updateListofStates(string strCountry)
		{
			ddlState.Items.Clear();
			using (SqlConnection addConn = new SqlConnection(Application["DBconn"].ToString()))
			{
				addConn.Open();
				Hashtable htParams = new Hashtable();
				htParams.Add("@Country",strCountry);				
				using (SqlDataReader dtrState = DBLibrary.GetDataReaderFromSelect(addConn,"SELECT * FROM state WHERE stat_ctry = (SELECT ctry_code FROM country WHERE ctry_name = @Country)",htParams))
				{
					if(!dtrState.HasRows)
					{
						ddlState.Items.Add("Not Applicable");
					}
					else
					{

						while (dtrState.Read())
						{
							ddlState.Items.Add(dtrState["STAT_NAME"].ToString());
						}
					}
				}
			}	
		}

		protected void ddlCountry_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string country = ddlCountry.SelectedItem.Text ;

			//we have to change list of available states!
			updateListofStates(country);
		}

		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			pnlDefault.Visible = true;
			pnlSave.Visible = false;
			Session["warehouse"] = null;
			//we have to clear all fields!!!
			pnlSave.Controls.Clear();
			
			
		}

        protected void btnConfirmDelete_Click(object sender, System.EventArgs e)
        {
            string edit_id = Session["place_id"].ToString();

            Hashtable htParams = new Hashtable();
            htParams.Add("@PLAC_ID", edit_id);

            string mailBody = "";

            string transactionsSql = "SELECT trans_number=(Convert(varchar,shipment_ordr_id)+'-'+Convert(varchar,shipment_sku)),shipment_transaction_date FROM shipment WHERE shipment_from=@PLAC_ID or shipment_to=@PLAC_ID;";
            using (SqlDataReader dtrState = DBLibrary.GetDataReaderFromSelect(new SqlConnection(Application["DBconn"].ToString()), transactionsSql, htParams))
            {
                mailBody += "<u>Related Transactions</u><br/><br/>";
                while (dtrState.Read())
                {
                        mailBody +=  "Transaction Number [" +dtrState["trans_number"].ToString() + "],  Shipment Date [" + dtrState["shipment_transaction_date"].ToString() + "]<br/>";
                }
                mailBody += "<br/><br/><br/>";
            }

            string infoSql = "SELECT * FROM place WHERE plac_id=@PLAC_ID";
            using (SqlDataReader dtrState = DBLibrary.GetDataReaderFromSelect(new SqlConnection(Application["DBconn"].ToString()), infoSql, htParams))
            {
                mailBody += "<u>Deleted Warehouse</u><br/><br/>";
                while (dtrState.Read())
                {
                    Object[] values = new Object[dtrState.FieldCount];
                    int fieldCount = dtrState.GetValues(values);

                    for (int i = 0; i < fieldCount; i++)
                    {
                        
                        mailBody +=  dtrState.GetName(i) + " : " + values[i].ToString() + "<br/>";
                    }
                    this.SendAwareEmail("DevelopmentTeam@theplasticexchange.com", mailBody);
                }
            }

            

            string sqlInsert = "Delete place WHERE plac_id=@PLAC_ID;";

            Hashtable ht = new Hashtable();
            ht.Add("@PLAC_ID", edit_id);

            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sqlInsert, ht);

            this.ModalPopupExtender1.Hide();
            Bind("PLAC_ID DESC");
        }

		private string[] parseSortCommand(string command)
		{
			string[] ret = new string[2];
			ret[0] = "";
			ret[1] = "";

			int i = command.IndexOf(" ");
			if(i != -1){
				ret[0] = command.Substring(0, i);
				ret[1] = command.Substring(i + 1);
					
			}

			return ret;
		}

		private void dg_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			string sort_command = e.SortExpression;
			string old_sort = "";
			
			if(Session["sort_command"] != null)
			{
				old_sort = Session["sort_command"].ToString();
			}

			string [] new_str = parseSortCommand(sort_command);
			string [] old_str = parseSortCommand(old_sort);

			if(new_str[0] == old_str[0])
			{
				if(old_str[1] == "ASC")
				{
					new_str[1] = "DESC";
				}
				else
				{
					new_str[1] = "ASC";
				}
			}
			
			sort_command = new_str[0] + " " + new_str[1];
			Session["sort_command"] = sort_command;
			Bind(sort_command);

		}

		private void dg_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			string s = e.Item.ItemType.ToString();

            string edit_id = e.Item.Cells[0].Text;

            if (e.CommandName == "Select")
            {
                if (s != "Header")
                {
                    
                    if (edit_id != "")
                    {
                        edit_Warehouse(edit_id);
                    }
                }
            }
            else if (e.CommandName == "Delete")
            {
                
                string sqlInsert = "SELECT trans_number=(Convert(varchar,shipment_ordr_id)+'-'+Convert(varchar,shipment_sku)),shipment_transaction_date FROM shipment WHERE shipment_from=@PLAC_ID or shipment_to=@PLAC_ID;";

                Hashtable ht = new Hashtable();
                ht.Add("@PLAC_ID", edit_id);

                DataSet dstContent = DBLibrary.GetDataSetFromSelect(new SqlConnection(Application["DBconn"].ToString()), sqlInsert, ht);
                
                dg_transaction.DataSource = dstContent;
                dg_transaction.DataBind();

                this.ModalPopupExtender1.Show();
                Session["place_id"] = edit_id;
            }
		
		}

		private void dg_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dg.CurrentPageIndex = e.NewPageIndex;
			if(Session["sort_command"] != null)
			{
				Bind(Session["sort_command"].ToString());
			}
			else
			{
				Bind("PLAC_ID DESC");
			}
//			dg. = e.NewPageIndex;
		}

		protected void btnSave_Click(object sender, System.EventArgs e)
		{
			if(Session["warehouse"] != null)
			{
				string str_plac_id = Session["warehouse"].ToString();
				int plac_id = Convert.ToInt32(str_plac_id); 
				int local_id = getLocality();
				if(local_id == -1)
				{
					createLocality();
					local_id = getLocality();
				}
					
				if(plac_id != -1)
				{
					editWarehouse(plac_id, local_id);
				}
				else
				{
					createWarehouse(local_id);
				}
				Bind("PLAC_ID DESC");
			}

			//need to clear all fields!
			pnlSave.Visible = false;
			pnlDefault.Visible = true;	
			pnlSave.Controls.Clear();
			Session["warehouse"] = null;

		}

        protected void dg_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            // setting the mouseover color
            if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
            {
                e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");
                if (e.Item.ItemType == ListItemType.Item)
                {
                    e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee'");
                }
                else
                {
                    e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3'");
                }
            }
        }

        private void SendAwareEmail(string emails, string mail_body)
        {
            // send email to Brokers that need to be aware about new offers/bids
            MailMessage mail;
            mail = new MailMessage();

            mail.From = "system@theplasticexchange.com";
            mail.Subject = "The follwing warehouse has been deleted";
            mail.To = this.ViewState["login_mail"].ToString();
            mail.Cc = emails;
            mail.Body += "</u><br><b>Information Detail-</b><BR><BR><BR>" + mail_body + "<BR>";
            mail.BodyFormat = MailFormat.Html;
            //SmtpMail.SmtpServer="caesar";
            TPE.Utility.EmailLibrary.Send(Context, mail);
        }

  	}
}
