<%@ Page Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="MailingList.aspx.cs" Inherits="localhost.Administrator.MailingList" Title="Mailing List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphCssLink" runat="server">
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="cphMain" runat="server">

<table id="Table2" class="Content LinkNormal" align="center" width="800px" cellspacing="0" cellpadding="0" runat="server">
    <tr align="center">
        <td align="center">
            <p><b>Mailing List</b></p>
            <p>How to: Copy and paste the email list below to a text file, click check all. <br /> And then Click Update Mailing List. It will update the Mailing List.</p>
            <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_OnClick" Text="Update Mailing List" />
        </td>
    </tr>
</table>

<asp:ScriptManager id="ScriptManager1" runat="Server" />
<asp:UpdatePanel id="UpdatePanel1" runat="Server" UpdateMode="Conditional">
        <ContentTemplate>

        <table id="Table1" class="Content LinkNormal" align="center" width="800px" cellspacing="0" cellpadding="0" runat="server">    
            <tr align="center">
                <td align="center">           
                    <asp:GridView ID="gvMailingList" CssClass="DataGrid"  runat="server" DataKeyNames="MAI_ID" 
                        AutoGenerateColumns="False"  >                 				
				        <HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
				        <FooterStyle CssClass="Content Color4 Bold FooterColor" />
                        <Columns>
                          <asp:BoundField ReadOnly="true" DataField="MAI_ID" SortExpression="MAI_ID" Visible="false"/>
                          <asp:TemplateField Visible="true">
                                <HeaderTemplate>
                                    <asp:CheckBox runat="server" ID="cbSelectAll" Text="Check All" AutoPostBack="true" OnCheckedChanged="cbSelectAll_CheckedChanged" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="cbSelect" />                                            
                                </ItemTemplate>
                            </asp:TemplateField>   
                          <asp:BoundField HeaderText="Email" DataField="MAI_MAIL" ReadOnly="true"/>                         
                        </Columns>
                    </asp:GridView>          
                </td>
            </tr>
        </table>
    </ContentTemplate>   
</asp:UpdatePanel> 


</asp:Content>
