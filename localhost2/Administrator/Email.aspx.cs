using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace localhost
{
	/// <summary>
	/// Summary description for Email.
	/// </summary>
	public partial class Email : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			Administrator.Shipment_Documents sd = new Administrator.Shipment_Documents();
			Button btn = new Button();
			btn.Text = "Preview Email";
			
			if (Request.QueryString["EmailBody_Shipment_Documents_Type"]!=null)
				Session["EmailBody_Shipment_Documents_Type"] = System.Convert.ToInt32(Request.QueryString["EmailBody_Shipment_Documents_Type"]);

			if (Request.QueryString["EmailBody_Shipment_Documents_TransactionID"]!=null)
				Session["EmailBody_Shipment_Documents_TransactionID"] = Request.QueryString["EmailBody_Shipment_Documents_TransactionID"].ToString();
			
			int DocumentType = Convert.ToInt32(Session["EmailBody_Shipment_Documents_Type"]);
			string TransactionID = (string)Session["EmailBody_Shipment_Documents_TransactionID"];
			string customField = Request.QueryString["CustomField"];
            //Larry - get the info about include metric
            bool metric = (bool)Session["EmailBody_Shipment_Documents_Metric"]; 

            if (customField==null) customField = "";
            //Larry - send the info to the preview email
			sd.btnEmailPreview_Click(btn, metric, DocumentType,TransactionID, customField, Application["DBconn"].ToString(), Context.Session["Name"].ToString());

			// Put user code to initialize the page here
			txtFrom.Text = (string)Session["EmailBody_Shipment_Documents_emailFrom"];
			TxtSubject.Text = (string)Session["EmailBody_Shipment_Documents_emailSubject"];
			EmailBody.Text = (string)Session["EmailBody_Shipment_Documents_emailBody"];
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
