using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using TPE.Utility;
using System.Reflection;
using System.Text;
using System.IO;
using System.Configuration;
using System.Threading;

namespace localhost.Administrator
{
    public partial class ViewShipmentComments : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.gvComments.Rows.Count > 0)
            {
                gvComments.UseAccessibleHeader = true;
                gvComments.HeaderRow.TableSection = TableRowSection.TableHeader;
                gvComments.FooterRow.TableSection = TableRowSection.TableFooter;
            }

            string shipmentID = Request.QueryString[0].ToString();

            SetUpComments(shipmentID);
        }


        private void SetUpComments(string shipID)
        {
            string strComment = "";
            string strSQL = "SELECT CONVERT(varchar,COMMENT_DATE,101) AS COMMENT_DATE2, COMMENT_TEXT,(SELECT PERS_FRST_NAME FROM PERSON WHERE PERS_ID=COMMENT_PERSID) AS PERSON FROM SHIPMENT_COMMENT WHERE COMMENT_SHIPMENT=@ShipID ORDER BY COMMENT_DATE DESC";
            
            Hashtable param = new Hashtable();

            param.Add("@ShipID", shipID);
                        
            DataTable dtComments = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), strSQL, param);

            gvComments.DataSource = dtComments;
            gvComments.DataBind();            
        }


    }
}
