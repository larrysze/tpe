<%@ Page language="c#" Codebehind="Edit_News.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.Edit_News" MasterPageFile="~/MasterPages/Menu.Master" Title="Edit News" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">

	
			<table height="30" cellspacing="0" cellpadding="0" width="780" border="0">
				<tr>
					<td align="center" width="641"><span class="Content Bold Color2"><asp:label id="Message" runat="server"></asp:label></span>&nbsp;&nbsp;</td>
				</tr>
			</table>
			<table height="500" cellspacing="0" cellpadding="0" width="780" border="0">
				<tr>
					<td width="10" height="30">&nbsp;</td>
					<td width="79" height="30" align="left" valign="middle">Headline</td>
					<td width="513" height="30" align="left" valign="middle">
						<asp:TextBox id="txtHeadline" runat="server" CssClass="InputForm" Width="400px"></asp:TextBox></td>
				</tr>
                <tr>
					<td width="10" height="30">&nbsp;</td>
					<td width="140" height="30" align="left" valign="middle">Headline Font Size</td>
					<td width="513" height="30" align="left" valign="middle">
						<asp:TextBox id="txtHeadlineFont" runat="server" CssClass="InputForm" Width="30px"></asp:TextBox></td>
				</tr>
				<tr>
                    <td width="10" height="30">&nbsp;</td>
					<td align="left" valign="top">Date</td>
					<td height="30" align="left" valign="middle">
						<asp:TextBox id="txtDate" runat="server" CssClass="InputForm" Width="80px"></asp:TextBox><FONT class="Content Color2">(mm-dd-yyyy)</FONT></td>
				</tr>
                <tr>
					<td width="10" height="30">&nbsp;</td>
					<td width="140" height="30" align="left" valign="middle">Story Font Size</td>
					<td width="513" height="30" align="left" valign="middle">
						<asp:TextBox id="txtStoryFont" runat="server" CssClass="InputForm" Width="30px"></asp:TextBox></td>
				</tr>
				<tr>
                    <td width="10" height="30">&nbsp;</td>
					<td align="left" valign="top">Story</td>
					<td height="260" align="left" valign="top">
						<asp:TextBox id="txtStory" runat="server" CssClass="InputForm" Width="400px" TextMode="MultiLine" Height="400px"></asp:TextBox></td>
				</tr>
			</table>
			<table cellspacing="0" cellpadding="0" width="780" border="0">
				<tr>
					<td align="center"><br /><asp:button Height="20" id="UpdateNews" runat="server" Width="80px" Text="Update" onclick="UpdateCustomNews_Click"></asp:button>
						<asp:Button Height="20" id="CancelNews" runat="server" Width="80px" Text="Cancel" onclick="CancelNews_Click"></asp:Button><br /><br /></td>
				</tr>
			</table>

</asp:Content>