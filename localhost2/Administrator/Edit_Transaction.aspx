<%@ Page Language="c#" CodeBehind="Edit_Transaction.aspx.cs" AutoEventWireup="false" Inherits="localhost.Administrator.Edit_Transaction" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>



<script runat="server">

   
</script>

<script language=JavaScript>

    function Address_PopUp_Seller()
    // This function opens up a pop up window and passes the appropiate ID number for the window
    {


    if (document.all)
        var xMax = screen.width, yMax = screen.height;
    else
        if (document.layers)
            var xMax = window.outerWidth, yMax = window.outerHeight;
        else
            var xMax = 640, yMax=480;

    var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
    var str = "../administrator/Location_Details.aspx?ID=" + document.Form.ddlSellerLocation.options[document.Form.ddlSellerLocation.selectedIndex].value;
    window.open(str,'Address','location=true,toolbar=true,width=275,height=370,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');

    }
        function Address_PopUp_Buyer()
    // This function opens up a pop up window and passes the appropiate ID number for the window
    {


    if (document.all)
        var xMax = screen.width, yMax = screen.height;
    else
        if (document.layers)
            var xMax = window.outerWidth, yMax = window.outerHeight;
        else
            var xMax = 640, yMax=480;

    var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
    var str = "../administrator/Location_Details.aspx?ID=" + document.Form.ddlBuyerLocation.options[document.Form.ddlBuyerLocation.selectedIndex].value;
    window.open(str,'Address','location=true,toolbar=true,width=275,height=370,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');

    }


    function Swap_Other(Flag)
        {
    	   if(Flag)
    	   {
    		  document.Form.ddlProduct.disabled=true;
    		  document.Form.txtProduct.disabled = false;
    		   	   }
    	   else
    	   {
    		  document.Form.ddlProduct.disabled = false;
    		  document.Form.txtProduct.disabled = true;

    	   }
    }


</script>

<form id=Form runat="server"><TPE:TEMPLATE id=Template1 title="" Runat="server"></TPE:TEMPLATE><asp:label id=lbltest runat="server"></asp:label><asp:label id=lbltest2 runat="server"></asp:label><asp:validationsummary id=ValidationSummary1 runat="server" HeaderText="There are problems with the following fields. Please correct the errors below."></asp:validationsummary>
	<!-- Insert content here -->
	
<table border=0>
  <tr>
    <td vAlign=top>
      <table border=0>
        <tr>
          <td>
            <fieldset>
            <table>
              <tr>
                <td><strong>Sold To:</STRONG></TD></TR>
              <tr>
                <td><asp:dropdownlist id=ddlBuyerComp runat="server" AutoPostBack="True" OnSelectedIndexChanged="Change_Company"></asp:dropdownlist></TD></TR>
              <tr>
                <td><strong>User:</STRONG></TD></TR>
              <tr>
                <td><asp:dropdownlist id=ddlBuyerName runat="server"></asp:dropdownlist></TD></TR>
              <tr>
                <td><strong>Ship To:</STRONG></TD></TR>
              <tr>
                <td><asp:dropdownlist id=ddlBuyerLocation runat="server"></asp:dropdownlist><a 
                  onmouseover="this.style.cursor='hand'" 
                  onclick=javascript:Address_PopUp_Buyer() 
                  ><u>Details:</U></A> 
                </TD></TR>
              <tr>
                <td>
                  <asp:rangevalidator id=RangeValidator1 runat="server" ControlToValidate="txtBuyerTerms" ErrorMessage="Buyer Term must be greater than zero." Display="none" MinimumValue="0" MaximumValue="999"></asp:rangevalidator><asp:comparevalidator id=CompareValidator1 Runat="server" ControlToValidate="txtBuyerTerms" ErrorMessage="Buyer Term must be a integer." Display="none" Operator="DataTypeCheck" Type="Integer"></asp:comparevalidator><asp:requiredfieldvalidator id=RequiredFieldValidator1 runat="server" ControlToValidate="txtBuyerTerms" ErrorMessage="Buyer Term cannot be blank" Display="none"></asp:requiredfieldvalidator>
                  <strong>Buyer Terms: </STRONG>(in days)
                 </TD>
              </TR>
              <tr>
                <td><asp:textbox id=txtBuyerTerms runat="server" size="4"></asp:textbox></TD></TR>
              <tr>
                <td>
					<asp:rangevalidator id=RangeValidator2 runat="server" ControlToValidate="txtPrice" ErrorMessage="Buy price must be less than one dollar per pound." Display="none" MinimumValue=".001" MaximumValue=".999" type="Double"></asp:rangevalidator>
					<asp:comparevalidator id=CompareValidator2 Runat="server" ControlToValidate="txtPrice" ErrorMessage="Buy Price must be a number." Display="none" Operator="DataTypeCheck" Type="Double"></asp:comparevalidator>
					<asp:requiredfieldvalidator id=RequiredFieldValidator2 runat="server" ControlToValidate="txtPrice" ErrorMessage="Buy Price cannot be blank" Display="none"></asp:requiredfieldvalidator>
					<strong>Buyer Price:</STRONG>
				 </TD>
			  </TR>
              <tr>
                <td>$<asp:textbox id=txtPrice runat="server" size="4"></asp:textbox> 
                </TD></TR></TABLE></FIELDSET> </TD></TR></TABLE>
    <td vAlign=top>
      <table border=0>
        <tr>
          <td>
            <fieldset>
            <table>
              <TBODY>
              <tr>
                <td><strong 
                  >Purchased From:</STRONG></TD></TR>
              <tr>
                <td><asp:dropdownlist id=ddlSellerComp runat="server" AutoPostBack="True" OnSelectedIndexChanged="Change_Company"></asp:dropdownlist></TD></TR>
              <tr>
                <td><strong 
                  >User:</STRONG></TD></TR>
              <tr>
                <td><asp:dropdownlist id=ddlSellerName runat="server"></asp:dropdownlist></TD></TR>
              <tr>
                <td><strong 
                  >Pickup:</STRONG></TD></TR>
              <tr>
                <td><asp:dropdownlist id=ddlSellerLocation runat="server"></asp:dropdownlist><a 
                  onmouseover="this.style.cursor='hand'" 
                  onclick=javascript:Address_PopUp_Seller() 
                  ><u>Details:</U></A> 
                </TD></TR>
              <tr>
                <td><strong>Seller 
                  Terms: </STRONG>(in days) </TD></TR>
              <tr>
                <td><asp:rangevalidator id=RangeValidator3 runat="server" ControlToValidate="txtSellerTerms" ErrorMessage="Sell Term must be greater than zero." Display="none" MinimumValue="0" MaximumValue="999"></asp:rangevalidator><asp:comparevalidator id=CompareValidator3 Runat="server" ControlToValidate="txtSellerTerms" ErrorMessage="Sell Term must be a integer." Display="none" Operator="DataTypeCheck" Type="Integer"></asp:comparevalidator><asp:requiredfieldvalidator id=RequiredFieldValidator3 runat="server" ControlToValidate="txtSellerTerms" ErrorMessage="Sell Term cannot be blank" Display="none"></asp:requiredfieldvalidator><asp:textbox id=txtSellerTerms runat="server" size="4"></asp:textbox></TD></TR>
              <tr>
                <td><asp:rangevalidator id=RangeValidator4 runat="server" ControlToValidate="txtSPrice" ErrorMessage="Buy price must be less than one dollar per pound." Display="none" MinimumValue=".001" MaximumValue=".999" type="Double"></asp:rangevalidator><asp:comparevalidator id=CompareValidator4 Runat="server" ControlToValidate="txtSPrice" ErrorMessage="Buy Price must be a number." Display="none" Operator="DataTypeCheck" Type="Double"></asp:comparevalidator><asp:requiredfieldvalidator id=RequiredFieldValidator4 runat="server" ControlToValidate="txtSPrice" ErrorMessage="Buy Price cannot be blank" Display="none"></asp:requiredfieldvalidator><strong 
                  >Seller Price:</STRONG> </TD></TR>
              <tr>
                <td>$<asp:textbox id=txtSPrice runat="server" size="4"></asp:textbox> 
                </TD></TR></TBODY></TABLE></FIELDSET> </TD></TR></TABLE></TD>
  <tr>
    <td align=left colSpan=2 rowSpan=3>
      <table cellSpacing=5 cellPadding=5 border=0 100%??>
        <tr>
          <td vAlign=top><b 
            >Comment:</B><br><asp:textbox id=txtComment runat="server" size="500" TextMode="Multiline" lines="2"></asp:textbox><br 
            ><asp:requiredfieldvalidator id=rfvComment runat="server" ControlToValidate="txtComment" ErrorMessage="Comment field cannot be blank" Display="none"></asp:requiredfieldvalidator></TD>
          <td vAlign=top><b>Comments 
            History:</B><br><asp:datagrid id=dgComments runat="server" CssClass="DataGrid" ItemStyle-CssClass="DataGridRow" AlternatingItemStyle-CssClass="DataGridRow_Alternate" HeaderStyle-CssClass="DataGridHeader" AutoGenerateColumns="false">
						<Columns>
							
							<asp:BoundColumn DataField="COMMENT_TEXT" HeaderText="Comment" />
							<asp:BoundColumn DataField="PERSON" HeaderText="User" ItemStyle-Wrap="false" />
							<asp:BoundColumn DataField="COMMENT_DATE" HeaderText="Date" ItemStyle-Wrap="false" />
							</Columns>
					</asp:datagrid></TD></TR></TABLE></TD></TR></TABLE></TD><td valign="top">
<table border=0>
  <TBODY>
  <tr>
    <td colSpan=2><font color=red><asp:label id=lblUpdated runat="server"></asp:label></FONT></TD></TR>
  <tr><asp:comparevalidator id=CompareValidator5 Runat="server" ControlToValidate="txtQty" ErrorMessage="Quantity must be a number." Display="none" Operator="DataTypeCheck" Type="Integer" Read-Only="true"></asp:comparevalidator><asp:requiredfieldvalidator id=RequiredFieldValidator5 runat="server" ControlToValidate="txtQty" ErrorMessage="Quanity cannot be blank" Display="none"></asp:requiredfieldvalidator>
    <td><strong>Quantity: <asp:textbox id=txtQty runat="server" size="1" ReadOnly="True"></asp:textbox><asp:dropdownlist id=ddlSize runat="server" AutoPostBack="True"></asp:dropdownlist></STRONG></TD></TR>
  <tr>
    <td><strong>Products:<asp:textbox id=txtProduct runat="server" MaxLength="50"></asp:textbox></STRONG> 
    </TD></TR>
  <tr>
    <td><strong>Order 
      Number:</STRONG>&nbsp;&nbsp;<asp:label id=lblOrderID runat="server"></asp:label> 
    </TD></TR>
  <tr>
    <td><strong>Load Status: 
      </STRONG><asp:label id=LblLoadStatus runat="server"></asp:label></TD></TR>
  <TR>
    <td><strong><asp:label id=LblRailCarNbr runat="server" BorderStyle="None"></asp:label></STRONG></TD></TR>
  <TR>
    <TD><STRONG 
      >Melt:&nbsp;&nbsp;&nbsp;</STRONG> <asp:textbox id=txtMELT runat="server" size="3" MaxLength="5"></asp:TextBox></TD></TR>
  <TR>
    <TD><STRONG 
      >Dens:&nbsp;&nbsp;</STRONG> <asp:textbox id=txtDENS runat="server" size="3" MaxLength="5"></asp:TextBox></TD></TR>
  <TR>
    <TD><STRONG 
      >Adds:&nbsp;&nbsp;</STRONG> <asp:textbox id=txtADDS runat="server" size="3" MaxLength="8"></asp:TextBox></TD></TR>
  <tr>
    <td><strong 
      >PO#:&nbsp;&nbsp;&nbsp;</STRONG><asp:textbox id=txtPO runat="server" size="8" maxlength="8"></asp:textbox></TD></TR>
  <tr>
    <td><strong 
      >Terms:&nbsp;&nbsp;&nbsp;</STRONG><asp:dropdownlist id=ddlShipTerms runat="server"></asp:dropdownlist></TD></TR>
  <tr>
    <td><strong>Transaction 
      Date:</STRONG> &nbsp;&nbsp; <asp:textbox id=txtDate runat="server"></asp:textbox></TD></TR>
  <tr>
    <td><strong>Commission:</STRONG> 
      &nbsp;&nbsp; <asp:textbox id=txtCommission runat="server" size="4"></asp:textbox><asp:dropdownlist id=ddlBrokers runat="server"></asp:DropDownList>
  <tr>
    <td><strong 
      >Profit:</STRONG>&nbsp; $ <asp:label id=LblMarkup runat="server"></asp:label></TD></TR>
  <tr>
    <td><strong>Freight:</STRONG> 
      &nbsp;&nbsp; <asp:comparevalidator id=CompareValidator6 Runat="server" ControlToValidate="txtFreight" ErrorMessage="Freight price must be a number." Display="none" Operator="DataTypeCheck" Type="Double"></asp:comparevalidator><asp:requiredfieldvalidator id=RequiredFieldValidator6 runat="server" ControlToValidate="txtFreight" ErrorMessage="Freight price cannot be blank" Display="none"></asp:requiredfieldvalidator>$<asp:textbox id=txtFreight runat="server" size="4"></asp:textbox> 
    </TD></TR><asp:panel id=pnlWGHT runat="server" 
  visible="true">
  <TR>
    <TD><STRONG>Weight:</STRONG> 
<asp:TextBox id=txtWeight runat="server"></asp:TextBox></TD></TR></asp:panel>
  <tr>
    <td align=center colSpan=2><asp:button class=tpebutton id=btnUpdate onclick=Click_Update runat="server" Text="Update"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;<asp:button class=tpebutton id=btnCancel onclick=Cancel runat="server" Text="Cancel" CausesValidation="false"></asp:button> 
    </TD></TR></TBODY></TABLE></TD></TR></TABLE>
    
    
    <TPE:TEMPLATE id=Template2 Runat="server" Footer="true"></TPE:TEMPLATE></FORM>
