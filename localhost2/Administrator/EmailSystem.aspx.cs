using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;

namespace localhost
{
	/// <summary>
	/// Summary description for Email.
	/// </summary>
	public partial class EmailSystem : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			string emailID = Request.QueryString["ID"].ToString();
			string emailDate = Request.QueryString["DATE"].ToString();
			string emailBody = "";

			DataTable dtEmail = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), "SELECT EMAIL_BODY FROM EMAIL_SYSTEM WHERE EMAIL_ID=" + emailID);
			if (dtEmail.Rows.Count > 0) emailBody = dtEmail.Rows[0]["EMAIL_BODY"].ToString();
						
			emailDate = "********** Email Sent in " + emailDate + " **********";
			EmailBody.Text = emailDate + "<BR>" + emailBody;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
