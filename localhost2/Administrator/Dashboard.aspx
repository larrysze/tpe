﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="localhost.Administrator.Dashboard" MasterPageFile="~/MasterPages/Menu.master" Title="Dashboard" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="asp" namespace="System.Web.UI" %>
<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="сс" %>
<%@ import Namespace="TPE.Utility" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphJavaScript" runat="server">
	<script language="JavaScript">
		function PreviewEmail(id)
		{
		    window.open('../administrator/DashboardEmail.aspx?id='+id,'','toolbar=false,resizable=1,scrollbars=1,menubar=false,width=800,height=800,top=20,left=100');			
		}
	</script> 
</asp:Content>

<asp:Content ID="Content1" runat=server ContentPlaceHolderID="cphMain">
<table>
<tr>
<td>
    <br />
    

    <asp:Label runat="server" ID="Date1" />
    <сс:GMDatePicker id="GMDatePickerDashboard" runat="server" CssClass="Content LinkNormal" InitialText="" CalendarFont-Names="Arial" TextBoxWidth="120" >
        <CalendarDayStyle Font-Size="9pt" />
        <CalendarTodayDayStyle BorderWidth="1px" BorderColor="DarkRed" Font-Bold="True" />
        <CalendarOtherMonthDayStyle BackColor="WhiteSmoke" />
        <CalendarTitleStyle BackColor="#E0E0E0" Font-Names="Arial" Font-Size="9pt" />
    </сс:GMDatePicker> 
    <br />
        <asp:DropDownList ID="ddlSummary" runat="server" CssClass="InputForm">    
            <asp:ListItem value="Yearly">Yearly</asp:ListItem>
            <asp:ListItem value="Monthly" Selected="true">Monthly</asp:ListItem>
            <asp:ListItem value="Weekly">Weekly</asp:ListItem>
        </asp:DropDownList>        
    <asp:Button runat="server" ID="btnRefresh" Text="Refresh" OnClick="OnClick_btnRefresh" />
    
    <asp:HyperLink ID="lnkBack" runat="server" CssClass="LinkNormal" NavigateUrl="dashboard.aspx?param=0" Text="Back" /><br /><br />
    
    <asp:DataGrid ID="dg" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="DataGridHeader" CssClass="DataGrid" ItemStyle-CssClass="DataGridRow" AlternatingItemStyle-CssClass="DataGridRow_Alternate"
     ShowFooter="True" OnItemDataBound="OnDataGridBind" HorizontalAlign="Center" BorderColor="Black" >
            <AlternatingItemStyle CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
            <ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
            <HeaderStyle CssClass="LinkNormal OrangeColor"></HeaderStyle>
            <FooterStyle CssClass="Content Bold Color4 FooterColor"></FooterStyle>
          <Columns>   
            <asp:BoundColumn DataField="DATE" ItemStyle-Font-Bold="true"  ReadOnly="True" HeaderText="Date" DataFormatString="{0:d}"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Ret. Demo">
            <ItemTemplate>
            <asp:HyperLink ID="t1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"t1")%>' NavigateUrl='<%# "dashboard.aspx?param=1&date=" + DataBinder.Eval(Container, "DataItem.date") %>'></asp:HyperLink>
            </ItemTemplate>  
            </asp:TemplateColumn> 
            <asp:TemplateColumn HeaderText="Demo Login">
            <ItemTemplate>
            <asp:HyperLink ID="t2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"t2")%>' NavigateUrl='<%# "dashboard.aspx?param=2&date=" + DataBinder.Eval(Container, "DataItem.date") %>'></asp:HyperLink>
            </ItemTemplate>  
            </asp:TemplateColumn> 
            <asp:TemplateColumn HeaderText="Your Password">
            <ItemTemplate>
            <asp:HyperLink ID="t3" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"t3")%>' NavigateUrl='<%# "dashboard.aspx?param=3&date=" + DataBinder.Eval(Container, "DataItem.date") %>'></asp:HyperLink>
            </ItemTemplate>  
            </asp:TemplateColumn> 
            <asp:TemplateColumn HeaderText="Demo Registration">
            <ItemTemplate>
            <asp:HyperLink ID="t4" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"t4")%>' NavigateUrl='<%# "dashboard.aspx?param=4&date=" + DataBinder.Eval(Container, "DataItem.date") %>'></asp:HyperLink>
            </ItemTemplate>  
            </asp:TemplateColumn> 
            <asp:TemplateColumn HeaderText="Broker Login">
            <ItemTemplate>
            <asp:HyperLink ID="t5" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"t5")%>' NavigateUrl='<%# "dashboard.aspx?param=5&date=" + DataBinder.Eval(Container, "DataItem.date") %>'></asp:HyperLink>
            </ItemTemplate>  
            </asp:TemplateColumn> 
            <asp:TemplateColumn HeaderText="Invalid Login">
            <ItemTemplate>
            <asp:HyperLink ID="t6" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"t6")%>' NavigateUrl='<%# "dashboard.aspx?param=6&date=" + DataBinder.Eval(Container, "DataItem.date") %>'></asp:HyperLink>
            </ItemTemplate>  
            </asp:TemplateColumn> 
            <asp:TemplateColumn HeaderText="Exchange Login">
            <ItemTemplate>
            <asp:HyperLink ID="t7" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"t7")%>' NavigateUrl='<%# "dashboard.aspx?param=7&date=" + DataBinder.Eval(Container, "DataItem.date") %>'></asp:HyperLink>
            </ItemTemplate>  
            </asp:TemplateColumn> 
            <asp:TemplateColumn HeaderText="Allocated">
            <ItemTemplate>
            <asp:HyperLink ID="t8" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"t8")%>' NavigateUrl='<%# "dashboard.aspx?param=8&date=" + DataBinder.Eval(Container, "DataItem.date") %>'></asp:HyperLink>
            </ItemTemplate>  
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Unsubscribed">
            <ItemTemplate>
            <asp:HyperLink ID="t9" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"t9")%>' NavigateUrl='<%# "dashboard.aspx?param=9&date=" + DataBinder.Eval(Container, "DataItem.date") %>'></asp:HyperLink>
            </ItemTemplate>  
            </asp:TemplateColumn> 

            <asp:TemplateColumn HeaderText="Admin Login">
            <ItemTemplate>
            <asp:HyperLink ID="t11" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"t11")%>' NavigateUrl='<%# "dashboard.aspx?param=11&date=" + DataBinder.Eval(Container, "DataItem.date") %>'></asp:HyperLink>
            </ItemTemplate>  
            </asp:TemplateColumn> 
            </Columns>
    </asp:DataGrid>
        
        <asp:DataGrid ID="dgList" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="DataGridHeader" CssClass="DataGrid" ItemStyle-CssClass="DataGridRow" AlternatingItemStyle-CssClass="DataGridRow_Alternate"
     ShowFooter="True" HorizontalAlign="Center" BorderColor="Black" OnItemDataBound="dgList_ItemDataBound" >
            <AlternatingItemStyle CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
            <ItemStyle CssClass="LinkNormal LightGray"></ItemStyle>
            <HeaderStyle CssClass="LinkNormal OrangeColor"></HeaderStyle>
            <FooterStyle CssClass="Content Bold Color4 FooterColor"></FooterStyle>
          <Columns>   
            <asp:BoundColumn DataField="DATE_TIME" ReadOnly="True" HeaderText="Date Time" ></asp:BoundColumn>
            <asp:BoundColumn DataField="FIRST_NAME" ReadOnly="True" HeaderText="First Name"></asp:BoundColumn>
            <asp:BoundColumn DataField="LAST_NAME" ReadOnly="True" HeaderText="Last Name"></asp:BoundColumn>
            <asp:BoundColumn DataField="COMP_NAME" ReadOnly="True" HeaderText="Comp Name"></asp:BoundColumn>
            <asp:BoundColumn DataField="EMAIL" ReadOnly="True" HeaderText="Email"></asp:BoundColumn>
            <asp:BoundColumn DataField="PERS_PHON" ReadOnly="True" HeaderText="Phone"></asp:BoundColumn>
            <asp:BoundColumn DataField="IP" ReadOnly="True" HeaderText="IP"></asp:BoundColumn>
<%--            <asp:BoundColumn DataField="password" ReadOnly="True" HeaderText="password"></asp:BoundColumn>--%>
             <asp:TemplateColumn HeaderText="Password">
	          <ItemTemplate>
	          <asp:Label ID="Label1" runat="server" Text='<%#Crypto.Decrypt((DataBinder.Eval(Container.DataItem,"password")).ToString())%>' />
	          </ItemTemplate>
	         </asp:TemplateColumn>
            <asp:BoundColumn DataField="Allocated_to" ReadOnly="True" HeaderText="Allocated_to"></asp:BoundColumn>
             <asp:TemplateColumn HeaderText="Preview">
	          <ItemTemplate>
	             <INPUT class="Content Color2" id="btnPreview" onclick=PreviewEmail(<%# DataBinder.Eval(Container, "DataItem.id") %>) type="button"  value="View Mail">
	          </ItemTemplate>
	        </asp:TemplateColumn>
	       <asp:BoundColumn DataField="PERSON_ID" ReadOnly="True" HeaderText="IP"></asp:BoundColumn>

        </Columns>
    </asp:DataGrid>


</td>
</tr>
</table>
<br /><br />
     
</asp:Content>
