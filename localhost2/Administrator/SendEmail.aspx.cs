using System;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.Mail;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPE.Utility;
using System.Threading;
using System.Configuration;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class SendEmail : System.Web.UI.Page 
	{
		private DataSet dsEmails;
		public string EmailTitle;
		public string EmailBody;
		public string EmailSalutation;
		private int totalUsers = 0;
		private int MinLeadsToEmailInThread;
		private int MaxLeadsToDisplay;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{			
			if (bool.Parse(Application["IsDebug"].ToString()))
			{
				MinLeadsToEmailInThread = 5;
			}
			else
			{
				MinLeadsToEmailInThread = Int32.Parse(Application["MinLeadsToEmailInThread"].ToString());
			}
			MaxLeadsToDisplay= Int32.Parse(Application["MaxLeadsToDisplayOnSendEmailScreen"].ToString());
				
			if(!Page.IsPostBack)
			{
				string strEmailType = "";
				if(Request.QueryString["Letter"] != "1")
					strEmailType = (Session["strCompId"] == null ? "Offers": "Bids");
				else
					strEmailType = "Letters";

				SetEmailFields(strEmailType);
				//Displaying info on the page
				txtSubject.Text = EmailTitle;
				txtBody.Text = EmailBody;
				txtSalutation.Text = EmailSalutation;				
				
				string strUserIds= "";
				if (Session["strEmailOffer_Address"]!= null)
				{
					strUserIds = Session["strEmailOffer_Address"].ToString();
					if (strUserIds.Length > 0)
					{
                        if (strUserIds.Substring(strUserIds.Length - 1, 1) == ",")
                        {
                            strUserIds = strUserIds.Substring(0, strUserIds.Length - 1);
                            string[] users = strUserIds.Split(',');
                            totalUsers = users.Length;
                        }
                        else 
                        {
                            totalUsers = 1;

                        }
                        Session["totalUsers"] = totalUsers;
					}
				}

//				Session["UserIds"] = strUserIds;
				if(Request.QueryString["Letter"] == "1")
				{
					lblItens.Visible = false;
					Session["dsEmails"] = GetUsers(strUserIds);
					// rg changed:
					Session["UserIds"] = strUserIds;
					Session["EmailType"] = strEmailType;					

				}
				else
				{					
					ViewState["DisplayPreferences"]=true; //user's preferences

					string BidsOffersCondition = "";
					if (Session["strEmailOffer_IDS"] != null)
					{
						//ViewState["DisplayPreferences"]=false;
						BidsOffersCondition = Session["strEmailOffer_IDS"].ToString();
						if (BidsOffersCondition.Length > 0)
						{
							if (BidsOffersCondition.Substring(BidsOffersCondition.Length-1,1)==",")
							{
								BidsOffersCondition = BidsOffersCondition.Substring(0,BidsOffersCondition.Length-1);
							}
						}
					}
					

					string GradesCondition = "";
					if (Session["strEmailGrades"] != null)
					{
						GradesCondition = Session["strEmailGrades"].ToString();
						if (GradesCondition.Length > 0)
						{
							if (GradesCondition.Substring(GradesCondition.Length-1,1)==",")
							{
								GradesCondition = GradesCondition.Substring(0,GradesCondition.Length-1);
							}
						}
					}

					bool useUserPreferences = (bool)ViewState["DisplayPreferences"];
					//if (BidsOffersCondition.Length>0) useUserPreferences = false;

					Session["EmailType"] = strEmailType;					
					if(totalUsers >= MinLeadsToEmailInThread)
					{
						Session["EmailDataReady"] = 0;
						Session["useUserPreferences"] = useUserPreferences;
						Session["UserIds"] = strUserIds;
						Session["BidsOffersCondition"] = BidsOffersCondition;
						Session["GradesCondition"] = GradesCondition;
						lblItens.Visible = true;
						lblItens.Text = totalUsers + " users selected. <BR>" +
							"Since number of leads you selected is more than " + (MinLeadsToEmailInThread - 1) + " you can only change the text of the email." ;
					}
					else
					{
						ContainerClassForEmails emails = new ContainerClassForEmails(Application["DBConn"].ToString(), bool.Parse(Application["IsDebug"].ToString()));

                        if (chkOutlookStyle.Checked)
                        {
                            emails.OutlookLikeEmail = true;
                        }
                        else
                        {
                            emails.OutlookLikeEmail = false;
                        }

						dsEmails = emails.GetOffersBids(useUserPreferences,strUserIds,BidsOffersCondition,GradesCondition,strEmailType);				
						dsEmails = emails.PrepareTransactions(dsEmails, strEmailType);
						
						dgUsers.DataSource = dsEmails.Tables[0];
						dgUsers.DataBind();

						//Keep info about Items to send
						Session["dsEmails"] = dsEmails;
					}	
				}
			}
		}
	


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgUsers.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgUsers_ItemDataBound);

		}
		#endregion

		public DataSet GetUsers(string usersCondition)
		{
			DataSet ds = new DataSet();
			using (SqlConnection Conn = new SqlConnection(Application["DBConn"].ToString()))
			{
				SqlDataAdapter dataADP = new SqlDataAdapter();
				Conn.Open();

				//Getting User's info as Name, Email, etc.
				string sqlStatement = "select pers_id, pers_frst_name, pers_last_name, pers_mail, pers_pswd, pers_pref, pers_intrst_size, pers_intrst_qualt, pers_prmly_intrst, Email_ENBL, distance_city + ', ' + distance_state + '(' + distance_zipcode + ')' pers_zip, international_port.port_city + ', ' + international_port.port_country pers_port ";
				sqlStatement = sqlStatement + "from person, international_port, distance_zipcode ";
				sqlStatement = sqlStatement + "where person.pers_port *= international_port.port_id ";
				sqlStatement = sqlStatement + "and person.pers_zip *= distance_zipcode.distance_zipcode";
				if (usersCondition!="") sqlStatement += " and pers_id in (" + usersCondition + ")";
				sqlStatement += " order by pers_frst_name, pers_last_name";
				dataADP = new SqlDataAdapter();
				dataADP.SelectCommand=new SqlCommand(sqlStatement,Conn);
				dataADP.SelectCommand.CommandType=CommandType.Text;
				dataADP.Fill(ds,"Users");
			}
			return ds;
		}

		private void dgUsers_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				Label title = ((System.Web.UI.WebControls.Label)e.Item.FindControl("lblTitle"));
				title.Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"pers_frst_name"))+" "+String.Format("{0}",DataBinder.Eval(e.Item.DataItem, "pers_last_name"));
				
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblPersID")).Text = String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"pers_id"));
				
				if (String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"Email_ENBL"))=="False")
				{
					title.Text +=":";
					((System.Web.UI.WebControls.Label)e.Item.FindControl("lblMessage")).Text = "This user has chosen do not receive emails.";
				}
				else
				{
					string pers_id = String.Format("{0}",DataBinder.Eval(e.Item.DataItem, "pers_id"));
					System.Web.UI.WebControls.DataGrid currentGrid=(System.Web.UI.WebControls.DataGrid)e.Item.FindControl("dgEmail");
					DataTable dtBind = dsEmails.Tables[1].Clone();
					DataRow[] dtRows = dsEmails.Tables[1].Select("pers_id = '" + String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"pers_id")) + "'","Grade, Price");
					CopyRowsToDataTable(dtRows, dtBind);

					if (dtRows.Length<=0)
					{
						title.Text +=":";
						((System.Web.UI.WebControls.Label)e.Item.FindControl("lblMessage")).Text = "Resins not found using this user's preferences.";
					}
					else
					{
						if (totalUsers < MinLeadsToEmailInThread)
						{
							if (MaxLeadsToDisplay>0)
							{
								currentGrid.DataSource = dtBind;
								currentGrid.DataBind();
								//if (currentGrid.Items.Count==0)
								MaxLeadsToDisplay--;
							}
						}
						else
						{
							currentGrid.DataSource = dtBind;
							currentGrid.DataBind();
							//if (currentGrid.Items.Count==0)
						}
					}
				}
			}
		}

		public void dgEmail_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				((System.Web.UI.WebControls.TextBox)e.Item.FindControl("txtPrice")).Text= String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"price"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblPrice")).Text= String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"price"));
				
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblTransactionID")).Text= String.Format("{0}",DataBinder.Eval(e.Item.DataItem,"TransactionID"));

				((System.Web.UI.HtmlControls.HtmlInputCheckBox)e.Item.Cells[1].Controls[0]).Checked= System.Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem,"selected"));
				((System.Web.UI.WebControls.Label)e.Item.FindControl("lblSelected")).Text= System.Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem,"selected")).ToString();
			}
		}

		public DataSet updateItems(DataSet dsEmail)
		{
			DataTable dtTransaction = dsEmail.Tables[1];

			foreach(DataGridItem userEmail in dgUsers.Items)
			{
				string persID = ((System.Web.UI.WebControls.Label)userEmail.FindControl("lblPersID")).Text;

				System.Web.UI.WebControls.DataGrid emailGrid =(System.Web.UI.WebControls.DataGrid)userEmail.FindControl("dgEmail");
				foreach(DataGridItem emailItem in emailGrid.Items)
				{
					string transactionID = ((System.Web.UI.WebControls.Label)emailItem.FindControl("lblTransactionID")).Text;
					bool selected = ((System.Web.UI.HtmlControls.HtmlInputCheckBox)emailItem.Cells[1].Controls[0]).Checked;
					string oldSelected = ((System.Web.UI.WebControls.Label)emailItem.FindControl("lblSelected")).Text;
					if(selected.ToString().ToLower() != oldSelected.ToLower())
					{
						//update item into DataTable
						DataRow dtRow = (DataRow)dtTransaction.Select("pers_id = '" + persID + "' and TransactionID = '" + transactionID + "'").GetValue(0);
						dtRow["selected"] = System.Convert.ToBoolean(selected);
						dtRow.AcceptChanges();
					}
					
					string price=((System.Web.UI.WebControls.TextBox)emailItem.FindControl("txtPrice")).Text;
					string oldPrice=((System.Web.UI.WebControls.Label)emailItem.FindControl("lblPrice")).Text;
					if (price!=oldPrice)
					{
						//update item into DataTable
						DataRow dtRow = (DataRow)dtTransaction.Select("pers_id = '" + persID + "' and TransactionID = '" + transactionID + "'").GetValue(0);
						dtRow["Price"] = price;
						dtRow.AcceptChanges();
					}
				}
			}
			return dsEmail;
		}

		protected void btnSendMail_Click(object sender, System.EventArgs e)
		{
			int totalUsers = (int)Session["totalUsers"];

			ContainerClassForEmails emails = new ContainerClassForEmails(Application["DBConn"].ToString(),bool.Parse(Application["IsDebug"].ToString()));

			if (chkOutlookStyle.Checked)
			{
				emails.OutlookLikeEmail = true;
			}
			else
			{
				emails.OutlookLikeEmail = false;
			}

			string DBString = Application["DBconn"].ToString();
			string strEmailOwner = Application["strEmailOwner"].ToString();
			string strID = Session["id"].ToString();
			
			if(totalUsers >= MinLeadsToEmailInThread)
			{

				// threaded version

				if(Request.QueryString["Letter"] != "1")
				{
					emails.Init(Session["EmailType"].ToString(), Convert.ToBoolean(Session["useUserPreferences"]), Session["UserIds"].ToString(), Session["BidsOffersCondition"].ToString(), Session["GradesCondition"].ToString(), Session["Email"].ToString(), Application["strEmailOwner"].ToString(), txtSubject.Text, txtBody.Text, txtSalutation.Text, strID);
					Thread threadSendEmails = new Thread(new ThreadStart(emails.SendEmailsFromThread));
					threadSendEmails.Priority = ThreadPriority.Lowest;
					threadSendEmails.Start();					
				}
				else
				{
					emails.InitNoOffers((DataSet)Session["dsEmails"], Session["Email"].ToString(), Application["strEmailOwner"].ToString(), txtSubject.Text, txtBody.Text, txtSalutation.Text, strID, UploadFile());
					Thread threadSendEmails = new Thread(new ThreadStart(emails.SendEmailsNoOffersFromThread));
					threadSendEmails.Priority = ThreadPriority.Lowest;
					threadSendEmails.Start();										
				}
			}
			else
			{
				//non-threaded version
				emails.InitWithoutThread(Session["EmailType"].ToString(), Session["Email"].ToString(), Application["strEmailOwner"].ToString(), txtSubject.Text, txtBody.Text, txtSalutation.Text, strID,UploadFile());
				
				if(Request.QueryString["Letter"] != "1")
				{
					updateItems((DataSet)Session["dsEmails"]);
			
					EmailTitle = txtSubject.Text;
					EmailBody = txtBody.Text;
					EmailSalutation = txtSalutation.Text;

					emails.SendEmails((DataSet)Session["dsEmails"], false);

				}
				else
				{
					EmailTitle = txtSubject.Text;
					EmailBody = txtBody.Text;
					EmailSalutation = txtSalutation.Text;

					emails.SendEmailsNoOffers((DataSet)Session["dsEmails"], false);
				}
			}

			Session["strCompId"] = null;
			Session["strEmailOffer_Address"] = null;
			Session["strEmailOffer_IDS"]= null;
			Session["strEmailGrades"] = null;
			Response.Redirect("/administrator/SendEmailDisclaimer.aspx");
		}


		private string UploadFile()
		{
		    if (txtFileUpload.HasFile)
		    {
			string dir = "/upload";
			string fileName = txtFileUpload.FileName;
			string SaveLocation = Server.MapPath(dir) + "\\" + fileName;

			txtFileUpload.SaveAs(SaveLocation);

			return SaveLocation;

		    }
		    else
		    {
			return "";
		    }
		}
			
		private void SetEmailFields(string transactionType)
			{
				StringBuilder strBody = new StringBuilder("");
				StringBuilder strSalutation = new StringBuilder("");
				
				//			if (dsEmail.Tables["Users"].Rows.Count == 1)
				//			{
				//				strBody.Append("Dear "+dsEmail.Tables["Users"].Rows[0]["pers_frst_name"].ToString().Trim()+","+((char)(13)).ToString()+((char)(13)).ToString());
				//			}
				//			else
				//			{
				strBody.Append("Dear <PERSON FIRST NAME>,"+((char)(13)).ToString()+((char)(13)).ToString());
				//			}

				if (transactionType == "Offers")
				{
					EmailTitle = "Resin Offers";
					//strBody.Append("We found <TOTAL_OFFERS> offers that are currently available which matched your requirements:"+((char)(13)).ToString()+((char)(13)).ToString());
					//strBody.Append("Hurry, these offers are subject to prior sale!");
					strSalutation.Append("These and all offers are subject to prior sale and possible fuel surcharge."+((char)(13)).ToString()+((char)(13)).ToString());
				}
				else if(transactionType == "Letters")
				{
					EmailTitle = "The Plastic Exchange";
					strBody.Append("");
				}
				else // Bids
				{
					EmailTitle = "Resin Requests";
					strBody.Append("We are looking for the following resin today.  Please let us know if you have something close."+((char)(13)).ToString()+((char)(13)).ToString());
				}
				EmailBody = strBody.ToString();
				
				strSalutation.Append("Sincerely,"+((char)(13)).ToString()+((char)(13)).ToString());
				strSalutation.Append(Session["Name"].ToString()+((char)(13)).ToString());
				strSalutation.Append("The Plastics Exchange"+((char)(13)).ToString());
				strSalutation.Append(ConfigurationSettings.AppSettings["PhoneNumber"].ToString());
				EmailSalutation = strSalutation.ToString();
			}
	    
		private void CopyRowsToDataTable(DataRow[] dtRows, DataTable dtTable)
		{
			foreach(DataRow dtRow in dtRows)
			{
				DataRow newRow = dtTable.NewRow();
				foreach(DataColumn column in dtTable.Columns)
				{
					newRow[column.ColumnName] = dtRow[column.ColumnName].ToString();
				}
				dtTable.Rows.Add(newRow);
			}
		}

        

	}	


}