using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using TPE.Utility;
using System.Collections.Generic;


namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Setup_News.
	/// </summary>
	public partial class Setup_News : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button Button1;
	
	/*****************************************************************************
	'*   1. File Name       : Administrator\Setup_News.aspx                      *
	'*   2. Description     : Set up news                       	             *
	'*						                                                     *
	'*						                                                     *
	'*   3. Modification Log:                                                    *
	'*     Ver No.       Date          Author             Modification           *
	'*   -----------------------------------------------------------------       *
	'*      1.00      3-11-2004       Xiaoda                                     *
	'*                                                                           *
	'*****************************************************************************/

		public void add(object sender, EventArgs e)
		{
			//Request.Form["CheckTPE"] will contain news Id, like 53453445, 56889349
			string CheckTPE = string.Empty;
			CheckTPE = Request.Form["CheckTPE"];

            //Inserting in TPE DataBase
            if ((CheckTPE != null) && (CheckTPE != string.Empty))
			{
				//split the string, get the News Id separetely
				string[] IdsTPE;
				Regex r = new Regex(","); // Split on .
				IdsTPE = r.Split(CheckTPE);

				//add news has 3 steps: 
				// determine if file is in /buffer or /newsChecked dir
				// insert record into db and get [new identity]
				// move xml file to /Current dir, renaming it to [new identity].xml

				for (int i=0; i<IdsTPE.Length; i++)
				{
					string headline;
					string path = string.Empty; 
					if (File.Exists(Server.MapPath("/News/Buffer/" + IdsTPE[i] + ".xml")))
						path = Server.MapPath("/News/Buffer/");
					else if (File.Exists(Server.MapPath("/News/newsChecked/" + IdsTPE[i] + ".xml")))
						path = Server.MapPath("/News/newsChecked/");
						
					if (path.Length > 0)
					{
						//Call ReadHead fucntion to get the value of headline
						headline=ReadHead(path + IdsTPE[i] + ".xml");

						//have to get rid of single quote, otherwise SQL will generat error
						headline = headline.Replace("'", "''");

						//insert into NEWS table
						string Str;
						Str = "INSERT INTO NEWS (headline,date) VALUES('"+headline+"',getDate())";
						string sNewId = DBLibrary.InsertAndReturnIdentity(Application["DBconn"].ToString(),Str,null).ToString();
						if (!File.Exists(Server.MapPath("/News/Current/" + sNewId + ".xml")))
							File.Move(path + IdsTPE[i] + ".xml", Server.MapPath("/News/Current/" + sNewId + ".xml"));												
					}
				}

                if ((CheckTPE != null) && (CheckTPE != string.Empty))
                {
                    BuildFilterTable();
                    btnFind_Click(this, null);
                }
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
            Master.Width = "760px";
            if (Session["Typ"].ToString() != "A" && Session["Typ"].ToString() != "L")
			{
				Response.Redirect("/default.aspx");
			}
			//read the XML files in "buffer" folder
			BuildFilterTable();
		}

  		private void BuildFilterTable()
		{
			string news;
			news="";
			string strDirTPE = Server.MapPath("/News/Buffer/");

			DirectoryInfo dirTPE = new DirectoryInfo(strDirTPE);
			
			//get all files in buffer folder of TPE
			FileInfo[] files = dirTPE.GetFiles();

            //Sorting the 'files' array by date and so by name; 'files' index will be into 'sl' array
            List<string> sl = new List<string>();
            for (int i = 0; i < files.Length; i++)
            {
                sl.Add(files[i].LastWriteTime.Year.ToString()
                     + files[i].LastWriteTime.Month.ToString().PadLeft(2, '0')
                     + files[i].LastWriteTime.Day.ToString().PadLeft(2, '0')
                     + files[i].Name
                     + i.ToString().PadLeft(7, '0'));
            }
            sl.Sort();

            news += "<table cellSpacing='1' cellPadding='2' width='575' bgColor='#9a9b96' border='0'><TBODY>";
            news += "<tr bgcolor=white> <td onclick=SelectAll('CheckTPE')><a href='#'>TPE</a></td><td align=center>Date</td><td width=500 align=center>HeadLine</td>";

            //Bring the 175 more recent files to the page
            int j; //Index to the most recent files
            for (int i = files.Length - 1; i >= 0 && files.Length - i <= 175; i--)
            {
                j = Convert.ToInt32(sl[i].Substring(sl[i].Length - 7));

                news = news + ReadXML(files[j].FullName, "buffer");
            }

			// close table
			news += "</tbody></table>";

			//put the content into a label
			lbltest.Text=news;

            dirTPE = null;
		}

		bool color = true;

		public string ReadXML(string UrlToXmlFile,string strPath)
		{
			// this string will concatenate all the xml values
			System.Text.StringBuilder sbXML = new System.Text.StringBuilder();
			XmlTextReader reader = null;
			// load the file
			reader = new XmlTextReader(UrlToXmlFile);

			//read XML files based on node
			object oName = reader.NameTable.Add("HeadLine");
			object HeadId = reader.NameTable.Add("TransmissionId");
			object DateAndTime = reader.NameTable.Add("DateAndTime");
			object TransmissionId = reader.NameTable.Add("TransmissionId");

			//build the content. save within a string

			string NewsId;
			NewsId="";
			string TempDate;
			TempDate="";

			while (reader.Read())
			{
				if (reader.NodeType == XmlNodeType.Element) 
				{
					if (reader.Name.Equals(HeadId))
					{
						NewsId=reader.ReadString(); 
						// REMOVE--local testing only
						//string s = UrlToXmlFile.Substring(UrlToXmlFile.LastIndexOf("\\") + 1);
						//NewsId = s.Remove(s.IndexOf(".xml"),4);

					}
					if (reader.Name.Equals(oName))
					{
						sbXML.Append("<td><font size=1><a href='/Public/News_Template.aspx?DIR="+strPath+"&Id="+NewsId+"&Highlight=True' target=blank>");
                        sbXML.Append(reader.ReadString()).Append("</a></td></tr>");
					}
					
					if (reader.Name.Equals(TransmissionId))
					{
						if (color)
						{
                            sbXML.Append("<tr bgcolor=#E8E8E8><td><input type=checkbox name=CheckTPE value=" + NewsId + " ></input></td>");
						}
						else
						{
                            sbXML.Append("<tr bgcolor=white><td><input type=checkbox name=CheckTPE value=" + NewsId + " ></input></td>");
						}

						if (color)
						{
							color = false;
						}
						else
						{
							color=true;
						}
					}

					if (reader.Name.Equals(DateAndTime))
					{
						TempDate=reader.ReadString();
						sbXML.Append("<td><font size=1>"+TempDate.Substring(4,2)+"/"+TempDate.Substring(6,2)+"/"+TempDate.Substring(0,4)+"</td>");
					}
				}
			}
			reader.Close();
			return sbXML.ToString();
		}

		public string ReadHead(string UrlToXmlFile)
		{
			//only read headline node
			System.Text.StringBuilder sbXML = new System.Text.StringBuilder();
			XmlTextReader reader = null;
			reader = new XmlTextReader(UrlToXmlFile);
			object oName = reader.NameTable.Add("HeadLine");
			while (reader.Read())
			{
				if (reader.NodeType == XmlNodeType.Element)
				{
					if (reader.Name.Equals(oName))
					{
						sbXML.Append(reader.ReadString()).Append("");
					}
				}
			}
			reader.Close();
			return sbXML.ToString();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		public void btnFind_Click(object sender, System.EventArgs e)
		{
            System.Text.StringBuilder sbSearchResults = new System.Text.StringBuilder();

			if (txtSearch.Text !="")
			{
                //read the XML files in "newChecked" folder
                string strDirTPE = Server.MapPath("/News/newsChecked/");

                DirectoryInfo dirTPE = new DirectoryInfo(strDirTPE);

                //get all files in buffer folder of TPE
                FileInfo[] files = dirTPE.GetFiles();

                //Sorting the 'files' array by date and so by name; 'files' index will be into 'sl' array
                List<string> sl = new List<string>();
                for (int i = 0; i < files.Length; i++)
                {
                    sl.Add(files[i].LastWriteTime.Year.ToString()
                         + files[i].LastWriteTime.Month.ToString().PadLeft(2, '0')
                         + files[i].LastWriteTime.Day.ToString().PadLeft(2, '0')
                         + files[i].Name
                         + i.ToString().PadLeft(7, '0'));
                }
                sl.Sort();

				sbSearchResults.Append("<table cellSpacing='1' cellPadding='2' width='575' bgColor='#9a9b96' border='0'><TBODY>");
			
                //Bring the more recent files to the page
                int j; //Index to the most recent files
                for (int i = files.Length - 1; i >= 0; i--)
                {
                    j = Convert.ToInt32(sl[i].Substring(sl[i].Length - 7));

                    // add row
                    if (SearchXML(files[j].FullName, txtSearch.Text))
                    {
                        sbSearchResults.Append(ReadXML(files[j].FullName, "newsChecked"));
                    }
                }

				// close table
				sbSearchResults.Append("</tbody></table>");

                dirTPE = null;
			}
            //put the content into a label
            lblSearchResults.Text = sbSearchResults.ToString();
		}

		private bool SearchXML(string UrlToXmlFile,string strSearch)
		{
			XmlTextReader reader = null;
			// load the file
			reader = new XmlTextReader(UrlToXmlFile);
			try
			{
				//read XML files based on node
				while (reader.Read())
				{
					if (reader.ReadString().ToUpper().IndexOf(strSearch.ToUpper())>0)
					{
						reader.Close();
						return true;
					}
				}
				// can't find search string. return false 
				return false;
			}
			finally
			{
				reader.Close();
			}
		}
	}
}
