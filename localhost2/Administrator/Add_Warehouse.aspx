<%@ Page Language="c#" Codebehind="Add_Warehouse.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.AddWarehouse" MasterPageFile="~/MasterPages/Menu.Master" Title="Add Warehouse" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolKit" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="cc" %>
    
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphCssLink">
    <style type="text/css">
    .modalBackground {
	    background-color:Gray;
	    filter:alpha(opacity=70);
	    opacity:0.7;
    }
    .modalPopup {
	    background-color:#ffffdd;
	    border-width:3px;
	    border-style:solid;
	    border-color:Gray;
	    padding:3px;
	    width:250px;
    }
    </style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphMain" >

   <script type="text/javascript">
     function HideModalPopup() 
    { 
      var modal = $find('ModalPopupExtender1'); 
      modal.hide(); 
    }
    function fnClickUpdate(sender, e)
    {
      __doPostBack(sender,e);
    }
   </script>
   
    <cc:ScriptManager ID="ScriptManager1" runat="server"> 
    </cc:ScriptManager> 

    <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center"  BackColor="LightGray" Width="428px" style="display:none; max-height:500px;" BorderStyle="Solid" BorderColor="Black" BorderWidth="2px"> 
      <ajaxToolKit:ModalPopupExtender BehaviorID="ModalPopupExtender1" ID="ModalPopupExtender1" runat="server" 
        PopupControlID="Panel1" TargetControlID="Panel1" BackgroundCssClass="modalBackground" CancelControlID="btnCancelDelete" OnCancelScript="HideModalPopup()" > 
      </ajaxToolKit:ModalPopupExtender>  
        <div style="margin:10px; ">
            <em>Transactions related to this warehouse</em><br />
            <div style="overflow:auto; max-height:350px;">
            <asp:DataGrid ID="dg_transaction" runat="server" CssClass="DataGrid" Width="85%" BorderWidth="0" BackColor="#000000" CellSpacing="1" AllowSorting="True" ItemStyle-CssClass="Content" AlternatingItemStyle-CssClass="Content" HeaderStyle-CssClass="LinkNormal" HorizontalAlign="Center" CellPadding="2" AutoGenerateColumns="False" AllowPaging="False">
                <AlternatingItemStyle CssClass="LinkNormal Color2 DarkGray" />
                <ItemStyle CssClass="LinkNormal Color2 LightGray"></ItemStyle>
                <HeaderStyle CssClass="LinkNormal Color2 OrangeColor"></HeaderStyle>
                <FooterStyle CssClass="Content Color4 Bold FooterColor" />
                <Columns>
                    <asp:BoundColumn DataField="trans_number" ItemStyle-Width="150" HeaderText="Transaction #"></asp:BoundColumn>
                    <asp:BoundColumn DataField="shipment_transaction_date" ItemStyle-Width="250" HeaderText="Date"></asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
            </div>
            <small><br />These transactions will be affected. Do you want to Delete?<br /><br /></small>
            <asp:Button ID="btnConfirmDelete" runat="server" Text="Confirm" OnClick="btnConfirmDelete_Click"  /> 
            <asp:Button ID="btnCancelDelete" runat="server" Text="Cancel" /> 
        </div>
    </asp:Panel> 


  

    <asp:Panel ID="pnlDefault" runat="server" Visible="true" style="margin:10px;">
        <table class="Content LinkNormal" cellspacing="0" cellpadding="0" width="100%" border="0" style="text-align:center;">
            <tr>
                <td align="center" class="Header">
                    <h3>List of Warehouses</h3>
                </td>
            </tr> 
            <tr>
                <td align="center">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dg" runat="server"  
                    Width="85%" BorderWidth="0" BackColor="#000000" CellSpacing="1" AllowSorting="True" AllowPaging="True" PageSize="50" 
                    ItemStyle-CssClass="Content" AlternatingItemStyle-CssClass="Content" HeaderStyle-CssClass="LinkNormal" HorizontalAlign="Center" CellPadding="2" 
                    AutoGenerateColumns="False" OnItemDataBound="dg_ItemDataBound" CssClass="DataGrid">
                        <AlternatingItemStyle CssClass="LinkNormal Color2 DarkGray" />
                        <ItemStyle CssClass="LinkNormal Color2 LightGray">
                        </ItemStyle>
                        <HeaderStyle CssClass="LinkNormal Color2 OrangeColor">
                        </HeaderStyle>
                        <FooterStyle CssClass="Content Bold Color4 FooterColor">
                        </FooterStyle>
                        <Columns>
                            <asp:BoundColumn Visible="False" DataField="PLAC_ID" HeaderText="ID"></asp:BoundColumn>
                            <asp:ButtonColumn Text="Edit"   CommandName="Select" ItemStyle-CssClass="LinkNormal"></asp:ButtonColumn>
                            <asp:BoundColumn DataField="PLAC_PERS" SortExpression="PLAC_PERS  DESC" HeaderText="Person "></asp:BoundColumn>
                            <asp:BoundColumn DataField="PLAC_EMAIL" SortExpression="PLAC_EMAIL  DESC" HeaderText="Email "></asp:BoundColumn> 
                            <asp:BoundColumn DataField="PLAC_PHON" SortExpression="PLAC_PHON  DESC" HeaderText="Phone"></asp:BoundColumn>
                            <asp:BoundColumn DataField="PLAC_FAX" SortExpression="PLAC_FAX  DESC" HeaderText="Fax"></asp:BoundColumn>
                            <asp:BoundColumn DataField="PLAC_ADDR_ONE" SortExpression="PLAC_ADDR_ONE  DESC" HeaderText="Address"></asp:BoundColumn>
                            <asp:BoundColumn DataField="PLAC_ZIP" SortExpression="PLAC_ZIP  ASC" HeaderText="Zip"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="PLAC_TYPE" SortExpression="PLAC_TYPE  DESC" HeaderText="Type"></asp:BoundColumn>
                            <asp:BoundColumn DataField="PLAC_INST" SortExpression="PLAC_INST  DESC" HeaderText="Place Name"></asp:BoundColumn>
                            <asp:BoundColumn DataField="City" SortExpression="city  DESC" HeaderText="City"></asp:BoundColumn>
                            <asp:BoundColumn DataField="State" SortExpression="state  DESC" HeaderText="State"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Country" SortExpression="country  DESC" HeaderText="Country"></asp:BoundColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDelete" runat="server" Text="Delete" CommandName="Delete" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle NextPageText="&amp;vt" Position="TopAndBottom" Mode="NumericPages" CssClass="LinkNormal LightGray"></PagerStyle>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <center>
            <asp:Button ID="btncreate" runat="server" Text="Create a New WareHouse" CssClass="Content Color2" OnClick="btncreate_Click"></asp:Button><br /><br /></center>
    </asp:Panel>
    <asp:Panel ID="pnlSave" runat="server" Visible="false">
        <center>
            <h3>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h3>
        </center>
        <table style="width: 464px; height: 332px" align="center" border="0" class="Content">
            <tr>
                <td colspan="2">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="487px" Height="92px" HeaderText="There are problems with the following fields. Please correct the errors below."></asp:ValidationSummary>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr align=left>
                <td style="height: 25px">
                    Contact:</td>
                <td style="height: 25px">
                    <asp:TextBox ID="txtContact" CssClass="InputForm" runat="server" Width="184px" MaxLength="30" size="20" maxsize="40"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="txtContact" Display="None" ErrorMessage="Contact is required"></asp:RequiredFieldValidator></td>
            </tr>
            <tr align=left>
                <td>
                    Email:</td>
                <td>
                    <asp:TextBox ID="txtEmail" CssClass="InputForm" runat="server" MaxLength="50"  Width="300px" size="20" maxsize="50"></asp:TextBox>
                </td>
            </tr>
            <tr align=left>
                <td>
                    Phone:</td>
                <td>
                    <asp:TextBox ID="txtPhone" CssClass="InputForm" runat="server" MaxLength="25" size="15" maxsize="40"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" ControlToValidate="txtPhone" Display="None" ErrorMessage="Phone is required"></asp:RequiredFieldValidator></td>
            </tr>
            <tr align=left>
                <td>
                    Fax:</td>
                <td>
                    <asp:TextBox ID="txtFax" CssClass="InputForm" runat="server" MaxLength="15" size="15" maxsize="40"></asp:TextBox>
                </td>
            </tr>            
            <tr align=left>            
                <td>
                    Address:</td>
                <td>
                    <asp:TextBox ID="txtAddr" CssClass="InputForm" runat="server" MaxLength="30" size="25" maxsize="40"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ControlToValidate="txtAddr" Display="None" ErrorMessage="Address is required"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
            <tr align=left>
                <td>
                </td>
                <td>
                    <asp:TextBox ID="txtAddr2" CssClass="InputForm" runat="server" MaxLength="30" size="25" maxsize="40"></asp:TextBox></td>
            </tr>
            <tr align=left>
                <td>
                </td>
                <td>
                    <asp:TextBox ID="txtAddr3" CssClass="InputForm" runat="server" MaxLength="30" size="25" maxsize="40"></asp:TextBox></td>
            </tr>
            <tr align=left>
                <td>
                    Zip:</td>
                <td>
                    <asp:TextBox ID="txtZip" CssClass="InputForm" runat="server" Width="120px" MaxLength="10" size="20" maxsize="30"></asp:TextBox></td>
            </tr>
            <tr align=left>
                <td style="height: 45px">
                    Warehouse Name:</td>
                <td style="height: 45px">
                    <asp:TextBox ID="txtWName" runat="server" CssClass="InputForm" MaxLength="30" size="25" maxsize="40" row="2"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtWName" Display="None" ErrorMessage="Warehouse Name is required"></asp:RequiredFieldValidator></td>
            </tr>
            <tr align=left>
                <td>
                    Warehouse Label:</td>
                <td>
                    <asp:TextBox ID="txtLabel" CssClass="InputForm" runat="server" Width="184px" MaxLength="30" size="20" maxsize="30"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLabel" Display="None" ErrorMessage="Warehouse Label is required"></asp:RequiredFieldValidator></td>
            </tr>
            <tr align=left>
                <td>
                    Rail #:</td>
                <td>
                    <asp:TextBox ID="txtRail" runat="server" CssClass="InputForm" Width="184px" MaxLength="30" size="20" maxsize="30"></asp:TextBox></td>
            </tr>
            <tr align=left>
                <td>
                    Country:</td>
                <td>
                    <asp:DropDownList ID="ddlCountry" CssClass="InputForm" runat="server" Visible="True" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr align=left>
                <td style="height: 15px">
                    State:</td>
                <td style="height: 15px">
                    <asp:DropDownList ID="ddlState" CssClass="InputForm" runat="server" Visible="True">
                    </asp:DropDownList></td>
            </tr>
            <tr align=left>
                <td>
                    City:</td>
                <td>
                    <asp:TextBox ID="txtCity" runat="server" CssClass="InputForm" MaxLength="25" size="20" maxsize="30" Visible="True"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ControlToValidate="txtCity" Display="None" ErrorMessage="City is required"></asp:RequiredFieldValidator></td>
            </tr>
        </table>
        <p align="center">
            <asp:Button ID="btnSave" runat="server" CssClass="Content Color2" Text="Save" OnClick="btnSave_Click"></asp:Button>&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnCancel" runat="server" CssClass="Content Color2" Text="Cancel" CausesValidation="False" OnClick="btnCancel_Click"></asp:Button>&nbsp;&nbsp;&nbsp;
    </asp:Panel>
</asp:Content>
