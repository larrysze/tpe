﻿<%@ Page Language="c#" Codebehind="Transaction_Details_Deleted.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.Transaction_Details_Deleted" MasterPageFile="~/MasterPages/Menu.Master" Title="Transaction Details" EnableEventValidation="false"  ValidateRequest="false" %> 

<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>
<%@ Register TagPrefix="uc1" TagName="Shipment_Documents" Src="/include/Shipment_Documents.ascx" %>

<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="сс" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="asp" namespace="System.Web.UI" %>

<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
    <style type="text/css"> 
        .menuskin { BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 100; VISIBILITY: hidden; FONT: 9px/10px Verdana; BORDER-LEFT: black 1px solid; WIDTH: 360px; BORDER-BOTTOM: black 1px solid; POSITION: absolute; BACKGROUND-COLOR: menu }
        .menuskin A { PADDING-RIGHT: 10px; PADDING-LEFT: 10px; COLOR: black; TEXT-DECORATION: none }
        #mouseoverstyle { BACKGROUND-COLOR: highlight }
        #mouseoverstyle A { COLOR: white }
    </style>
</asp:Content>
<asp:Content runat="Server" ContentPlaceHolderID="cphJavaScript">

    <script type="text/javascript" language="JavaScript">
    function Address_PopUp_Seller()
    // This function opens up a pop up window and passes the appropiate ID number for the window
    {
    if (document.all)
        var xMax = screen.width, yMax = screen.height;
    else
        if (document.layers) 
            var xMax = window.outerWidth, yMax = window.outerHeight;
        else
            var xMax = 640, yMax=480;

    var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
    var str = "../administrator/Location_Details.aspx?ID=" + document.getElementById('<%=ddlPickupFrom.ClientID%>').options[document.getElementById('<%=ddlPickupFrom.ClientID%>').selectedIndex].value;    
    
    window.open(str,'Address','location=true,toolbar=true,width=275,height=450,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');

    } 
        function Address_PopUp_Buyer()
    // This function opens up a pop up window and passes the appropiate ID number for the window
    {
    if (document.all)
        var xMax = screen.width, yMax = screen.height;
    else
        if (document.layers)
            var xMax = window.outerWidth, yMax = window.outerHeight;
        else
            var xMax = 640, yMax=480;

    var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
    var str = "../administrator/Location_Details.aspx?ID=" + document.getElementById('<%=ddlDeliveryTo.ClientID%>').options[document.getElementById('<%=ddlDeliveryTo.ClientID%>').selectedIndex].value;
    window.open(str,'Address','location=true,toolbar=true,width=275,height=450,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');
    }
    

function showMe(id) { // This gets executed when the user clicks on the checkbox
	var obj = document.getElementById(id);
    
    if (obj.style.visibility=="hidden") 
    { // if it is checked, make it visible, if not, hide it
	    obj.style.visibility = "visible";
    } 
    else 
    {
	    obj.style.visibility = "hidden";
    }
}


    </script>

    <script type="text/javascript" language="JavaScript1.2">

        //Pop-it menu- By Dynamic Drive
        //For full source code and more DHTML scripts, visit http://www.dynamicdrive.com
        //This credit MUST stay intact for use

        var linkset=new Array()
        //SPECIFY MENU SETS AND THEIR LINKS. FOLLOW SYNTAX LAID OUT

        <asp:PlaceHolder id="phActionButton"  runat="server" />
        <asp:PlaceHolder id="phActionButton2"  runat="server" />
        ////No need to edit beyond here

        var ie4=document.all&&navigator.userAgent.indexOf("Opera")==-1
        var ns6=document.getElementById&&!document.all
        var ns4=document.layers

        function showmenu(e,which){

        if (!document.all&&!document.getElementById&&!document.layers)
        return

        clearhidemenu()

        menuobj=ie4? document.all.popmenu : ns6? document.getElementById("popmenu") : ns4? document.popmenu : ""
        menuobj.thestyle=(ie4||ns6)? menuobj.style : menuobj

        if (ie4||ns6)
        menuobj.innerHTML=which
        else{
        menuobj.document.write('<layer name=gui bgColor=#E6E6E6 width=165 onmouseover="clearhidemenu()" onmouseout="hidemenu()">'+which+'</layer>')
        menuobj.document.close()
        }

        menuobj.contentwidth=(ie4||ns6)? menuobj.offsetWidth : menuobj.document.gui.document.width
        menuobj.contentheight=(ie4||ns6)? menuobj.offsetHeight : menuobj.document.gui.document.height
        eventX=ie4? event.clientX : ns6? e.clientX : e.x
        eventY=ie4? event.clientY : ns6? e.clientY : e.y

        //Find out how close the mouse is to the corner of the window
        var rightedge=ie4? document.body.clientWidth-eventX : window.innerWidth-eventX
        var bottomedge=ie4? document.body.clientHeight-eventY : window.innerHeight-eventY

        //if the horizontal distance isn't enough to accomodate the width of the context menu
        if (rightedge<menuobj.contentwidth)
        //move the horizontal position of the menu to the left by it's width
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX-menuobj.contentwidth : ns6? window.pageXOffset+eventX-menuobj.contentwidth : eventX-menuobj.contentwidth
        else
        //position the horizontal position of the menu where the mouse was clicked
        menuobj.thestyle.left=ie4? document.body.scrollLeft+eventX : ns6? window.pageXOffset+eventX : eventX

        //same concept with the vertical position
        if (bottomedge<menuobj.contentheight)
        menuobj.thestyle.top=ie4? document.body.scrollTop+eventY-menuobj.contentheight : ns6? window.pageYOffset+eventY-menuobj.contentheight : eventY-menuobj.contentheight
        else
        menuobj.thestyle.top=ie4? document.body.scrollTop+event.clientY : ns6? window.pageYOffset+eventY : eventY
        menuobj.thestyle.visibility="visible"
        return false
        }

        function contains_ns6(a, b) {
        //Determines if 1 element in contained in another- by Brainjar.com
        while (b.parentNode)
        if ((b = b.parentNode) == a)
        return true;
        return false;
        }

        function hidemenu(){
        if (window.menuobj)
        menuobj.thestyle.visibility=(ie4||ns6)? "hidden" : "hide"
        }

        function dynamichide(e){
        if (ie4&&!menuobj.contains(e.toElement))
        hidemenu()
        else if (ns6&&e.currentTarget!= e.relatedTarget&& !contains_ns6(e.currentTarget, e.relatedTarget))
        hidemenu()
        }

        function delayhidemenu(){
        if (ie4||ns6||ns4)
        delayhide=setTimeout("hidemenu()",500)
        }

        function clearhidemenu(){
        if (window.delayhide)
        clearTimeout(delayhide)
        }

        function highlightmenu(e,state){
        if (document.all)
        source_el=event.srcElement
        else if (document.getElementById)
        source_el=e.target
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        else{
        while(source_el.id!="popmenu"){
        source_el=document.getElementById? source_el.parentNode : source_el.parentElement
        if (source_el.className=="menuitems"){
        source_el.id=(state=="on")? "mouseoverstyle" : ""
        }
        }
        }
        }

        if (ie4||ns6)
        document.onclick=hidemenu

    </script>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">

<asp:ScriptManager id="ScriptManager1" runat="Server" />

     <asp:UpdatePanel id="UpdatePanel1" runat="Server">
        <ContentTemplate>
<asp:Panel id="pnlMainPanel" runat="server">
    <asp:ValidationSummary id="ValidationSummary1" runat="server" HeaderText="There are problems with the following fields. Please correct the errors below.">
    </asp:ValidationSummary>
    <br />
    <div id="popmenu" onmouseover="clearhidemenu();highlightmenu(event,'on')" class="menuskin" onmouseout="highlightmenu(event,'off');dynamichide(event)"></div>
    <FIELDSET width="200"><LEGEND><SPAN class="Header Color2 Bold">Order&nbsp;Details</SPAN></LEGEND>
        <table id="Table2" cellSpacing=1 cellPadding=1 width="100%" border=0>
            <tbody>
                <tr>
                    <td align=left colSpan=4>
                        <asp:Label style="TEXT-ALIGN: right" id="lblOrderNumber" runat="server" CssClass="Content Bold Color2">Order # 3565</asp:Label>
                        <asp:Label id="lblShipmentID" runat="server" Visible="False" CssClass="Content Bold Color2"></asp:Label>
                        <asp:Label id="lblOrderID" runat="server" Visible="False" CssClass="Content Bold Color2"></asp:Label>
                    </td>
                    <td>
                        <asp:Label id="lblLegacyShipment" runat="server" Visible="False" CssClass="Content Bold Color2" text="Legacy # "></asp:Label>
                        <asp:Label id="lblLegacyShipmentID" runat="server" Visible="False" CssClass="Content Bold Color2"></asp:Label>
                    </td>
                    <td align=right colSpan=4>
                        <asp:Button Visible="false" id="btnEditOrder" onclick="btnEditOrder_Click" runat="server" Width="120px" CssClass="Content Color2" Text="Edit Order">
                        </asp:Button>&nbsp;&nbsp;
                        <asp:Button Visible="false" id="btnDeleteOrder" onclick="btnDeleteOrder_Click" runat="server" Width="100px" CssClass="Content Color2" Text="Delete Order">
                        </asp:Button>&nbsp; 
                    </td>
                </tr>
                <tr>
                    <td colSpan=2>
                        <asp:Label id="lblProductDesc" runat="server" CssClass="Content Color2">Product</asp:Label>&nbsp; 
                        <asp:TextBox id="txtProductDesc" runat="server" Width="180px" CssClass="InputForm"></asp:TextBox>
                    </td>
                    <td colSpan=2>
                        <asp:Label id="lblWarehouse" runat="server" CssClass="Content Color2">Warehouse</asp:Label>&nbsp; 
                        <asp:TextBox id="txtWarehouseName" runat="server" Width="70px" CssClass="InputForm" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td align=right colSpan=2>
                        <asp:Label id="lblTransactionDate" runat="server" CssClass="Content Color2">Transaction Date:</asp:Label>&nbsp; 
                    </td>
                    <td>
                        <asp:TextBox id="txtTransactionDate" runat="server" Width="101px" CssClass="InputForm"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label id="lblMelt" runat="server" CssClass="Content Color2">Melt</asp:Label>&nbsp; 
                        <asp:TextBox id="txtMelt" runat="server" Width="30px" CssClass="InputForm"></asp:TextBox>
                    </td>
                    <td colSpan=2>
                        <p>
                            <asp:Label id="lblDensity" runat="server" CssClass="Content Color2">Density</asp:Label>&nbsp; 
                            <asp:TextBox id="txtDensity" runat="server" Width="56px" CssClass="InputForm"></asp:TextBox> 
                        </p>
                    </td>
                    <td>
                        <asp:Label id="lblAdds" runat="server" CssClass="Content Color2">Adds</asp:Label>&nbsp; 
                    </td>
                    <td>
                        <asp:TextBox id="txtAdds" runat="server" Width="73px" CssClass="InputForm"></asp:TextBox> 
                    </td>
                    <td>
                        <asp:CheckBox id="cbPrime" runat="server" CssClass="Content Color2" Text="Prime"></asp:CheckBox> 
                    </td>
                    <td>
                    </td>
                </tr>
            </tbody>
        </table>
    </FIELDSET>
     <p></p>
     <!-- Insert content here -->
     <FIELDSET width="200">
        <LEGEND>
            <asp:Label id="lblTabShipTitle" runat="server" CssClass="Header Color2 Bold">Shipment Details</asp:Label>
        </LEGEND>
        <TABLE id="Table1" height="0" cellSpacing=1 cellPadding=1 width="100%" border=0>
            <TBODY>
                <tr>
                    <td height=20>
                        <asp:Label id="lblQuantity" runat="server" CssClass="Content Color2">Quantity</asp:Label>
                    </td>
                    <td colSpan=2 height=20>
                        <asp:TextBox id="txtQuatity" runat="server" Width="48px" CssClass="InputForm"></asp:TextBox>&nbsp; 
                        <asp:DropDownList id="ddlSize" runat="server" Width="155px" CssClass="InputForm" OnSelectedIndexChanged="ddlSize_SelectedIndexChanged" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td height=20>
                        <asp:Label id="lblStatus" runat="server" CssClass="Content Color2">Status</asp:Label>&nbsp; 
                        <asp:DropDownList id="ddlStatus" runat="server" CssClass="InputForm" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="True">
                            <asp:ListItem Value="1">Inventory</asp:ListItem>
                            <asp:ListItem Value="2">Enroute</asp:ListItem>
                            <asp:ListItem Value="3">Delivered</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    
                    <td height=20>
                        <asp:Label id="lblReleaseNumber" runat="server" CssClass="Content Color2">Release #</asp:Label>&nbsp;                         
                        <asp:TextBox id="txtReleaseNumber" runat="server" CssClass="InputForm" Width="80px"></asp:TextBox>
                    </td>
                    <td height=20>
                        <asp:Label id="lblLotNumber" runat="server" CssClass="Content Color2">Lot #</asp:Label>&nbsp;                         
                        <asp:TextBox id="txtLotNumber" runat="server" CssClass="InputForm" Width="80px"></asp:TextBox>&nbsp;
                        <asp:Label id="lblRailCarNumber" runat="server" BorderStyle="None" CssClass="Content Color2"></asp:Label>&nbsp;
                    </td>
                    <td colSpan=3 height=20>
                        <p align=right>                            
                            <asp:Button Visible="false" id="btnEditShipment" onclick="btnEditShipment_Click" runat="server" Width="130px" CssClass="Content Color2" Text="Edit Shipment"></asp:Button>&nbsp;&nbsp; 
                            <asp:Button Visible="false" id="btnDeleteShipment" onclick="btnDeleteShipment_Click" runat="server" Width="120px" CssClass="Content Color2" Text="Delete Shipment"></asp:Button>&nbsp; 
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colSpan=5 height=137>
                        <FIELDSET width="200">
                            <LEGEND>
                                <SPAN class="Header Color2 Bold">Sold To</SPAN> 
                            </LEGEND>
                            <TABLE id="Table3" cellSpacing=1 cellPadding=1 width="100%" border=0>
                                <TBODY>
                                    <tr>
                                        <td>
                                            <asp:Label id="lblCompanySoldTo" runat="server" CssClass="Content Color2">Company</asp:Label>
                                        </td>
                                        <td noWrap>
                                            <asp:DropDownList id="ddlCompanySoldTo" runat="server" CssClass="InputForm" OnSelectedIndexChanged="ddlCompanySoldTo_SelectedIndexChanged" AutoPostBack="True">
                                            </asp:DropDownList>&nbsp; 
                                            <asp:Label id="lblUserSoldTo" runat="server" CssClass="Content Color2">User</asp:Label>&nbsp; 
                                            <asp:DropDownList id="ddlUserSoldTo" runat="server" CssClass="InputForm" AutoPostBack="True"></asp:DropDownList> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td noWrap>
                                            <SPAN class="LinkNormal">
                                                <a onmouseover="this.style.cursor='hand'" onclick="javascript:Address_PopUp_Buyer()"><u>Delivery to</u></a> 
                                            </SPAN>
                                        </td>
                                        <td>
                                            <asp:DropDownList id="ddlDeliveryTo" runat="server" CssClass="InputForm" OnSelectedIndexChanged="ddlDeliveryTo_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList><br/>
                                            <asp:Label id="lblBuyerEditUser" runat="server" CssClass="LinkNormal"></asp:Label>&nbsp;&nbsp;&nbsp; 
                                            <asp:Label id="lblBuyerAddUser" runat="server" CssClass="LinkNormal"></asp:Label> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label id="lblSellPrice" runat="server" CssClass="Content Color2">Sell Price</asp:Label> 
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtSellPrice" runat="server" Width="70px" CssClass="InputForm"></asp:TextBox>&nbsp; 
                                            <asp:Label id="lblTermsSoldTo" runat="server" CssClass="Content Color2">Terms</asp:Label>&nbsp; 
                                            <asp:TextBox id="txtTermsSoldTo" runat="server" Width="48px" CssClass="InputForm"></asp:TextBox>&nbsp; 
                                            <asp:Label id="Label16" runat="server" CssClass="Content Color2">Due: </asp:Label>&nbsp; 
                                            <asp:Label id="lblDueSoldTo" runat="server" CssClass="Content Color2">-</asp:Label> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label id="lblDeliveryDate" runat="server" CssClass="Content Color2">Shipping Date To Customer </asp:Label>
                                            <asp:TextBox id="txtDeliveryDate" runat="server" Width="160px" CssClass="InputForm"></asp:TextBox> 
                                        </td>
                                    </tr>                                
                                    <tr>
                                        <td colSpan=2>
                                            <asp:Label id="Label17" runat="server" CssClass="Content Color2">Total Outstanding ($):</asp:Label>&nbsp; 
                                            <asp:Label id="lblTotalOutstandingsSoldTo" runat="server" CssClass="Content Color2"></asp:Label>&nbsp; &nbsp; 
                                            <asp:LinkButton id="lnkRecordPaymentSoldTo" onclick="lnkRecordPaymentSoldTo_Click" runat="server" CssClass="LinkNormal">Record Payment</asp:LinkButton>
                                        </td>
                                    </tr>
                                </TBODY>
                            </TABLE>
                        </FIELDSET>
                        <asp:RangeValidator id="TermValidator1" runat="server" CssClass="Content Color3 Bold" MaximumValue="999" MinimumValue="0" Display="none" ErrorMessage="Buyer Term must be greater than zero." ControlToValidate="txtTermsSoldTo"></asp:RangeValidator> 
                        <asp:CompareValidator id="TermValidator2" runat="server" CssClass="Content Color3 Bold" Display="none" ErrorMessage="Buyer Term must be a integer." ControlToValidate="txtTermsSoldTo" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator> 
                        <asp:RequiredFieldValidator id="TermValidator3" runat="server" CssClass="Content Color3 Bold" Display="none" ErrorMessage="Buyer Term cannot be blank" ControlToValidate="txtTermsSoldTo"></asp:RequiredFieldValidator> 
                        <asp:RangeValidator id="SellPriceValidator1" runat="server" CssClass="Content Color3 Bold" MaximumValue="2.999" MinimumValue=".001" Display="none" ErrorMessage="Buy price must be less than one dollar per pound." ControlToValidate="txtSellPrice" Type="Double"></asp:RangeValidator> 
                        <asp:CompareValidator id="SellPriceValidator2" runat="server" CssClass="Content Color3 Bold" Display="none" ErrorMessage="Buy Price must be a number." ControlToValidate="txtSellPrice" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator> 
                        <asp:RequiredFieldValidator id="SellPriceValidator3" runat="server" CssClass="Content Color3 Bold" Display="none" ErrorMessage="Buy Price cannot be blank" ControlToValidate="txtSellPrice"></asp:RequiredFieldValidator> 
                        <asp:RequiredFieldValidator id="POValidator1" runat="server" CssClass="Content Color3 Bold" Display="none" ErrorMessage="PO# cannot be blank" ControlToValidate="txtPONumber"></asp:RequiredFieldValidator> 
                    </td>
                    <td colSpan=5 height=137>
                        <FIELDSET width="200">
                            <LEGEND>
                                <SPAN class="Header Color2 Bold">Purchased From</SPAN> 
                            </LEGEND>
                            <TABLE id="Table4" cellSpacing=1 cellPadding=1 width="100%" border=0>
                                <TBODY>
                                    <tr>
                                        <td width=93>
                                            <asp:Label id="lblCompanyFrom" runat="server" CssClass="Content Color2">Company</asp:Label> 
                                        </td>
                                        <td noWrap colSpan=5>
                                            <asp:DropDownList id="ddlCompanyFrom" runat="server" CssClass="InputForm" OnSelectedIndexChanged="ddlCompanyFrom_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>&nbsp; 
                                            <asp:Label id="lblUserFrom" runat="server" CssClass="Content Color2">User</asp:Label>&nbsp; 
                                            <asp:DropDownList id="ddlUserFrom" runat="server" CssClass="InputForm" AutoPostBack="True"></asp:DropDownList> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td noWrap width=93>
                                            <SPAN class="LinkNormal">
                                                <a onmouseover="this.style.cursor='hand'" onclick="javascript:Address_PopUp_Seller()"><u>Pick up From</u> </a>
                                            </SPAN>
                                        </td>
                                        <td>
                                            <asp:DropDownList id="ddlPickupFrom" runat="server" CssClass="InputForm" AutoPostBack="True"></asp:DropDownList><br />
                                            <asp:Label id="lblSellerEditUser" runat="server" CssClass="LinkNormal"></asp:Label>&nbsp;&nbsp;&nbsp; 
                                            <asp:Label id="lblSellerAddUser" runat="server" CssClass="LinkNormal"></asp:Label> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width=93>
                                            <asp:Label id="lblBuyPrice" runat="server" CssClass="Content Color2">Buy Price</asp:Label> 
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtBuyPrice" runat="server" Width="70px" CssClass="InputForm"></asp:TextBox>&nbsp; 
                                            <asp:Label id="lblTermsFrom" runat="server" CssClass="Content Color2">Terms</asp:Label>&nbsp; 
                                            <asp:TextBox id="txtTermsFrom" runat="server" Width="48px" CssClass="InputForm"></asp:TextBox>&nbsp; 
                                            <asp:Label id="Label19" runat="server" CssClass="Content Color2">Due:</asp:Label>&nbsp; 
                                            <asp:Label id="lblDueFrom" runat="server" CssClass="Content Color2">-</asp:Label> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label id="lblShippingDate" runat="server" CssClass="Content Color2">Shipping Date From Vender</asp:Label> 
                                            <asp:TextBox id="txtShippingDate" runat="server" Width="160px" CssClass="InputForm"></asp:TextBox> 
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td colSpan=2>
                                            <asp:Label id="Label18" runat="server" CssClass="Content Color2">Total Outstanding ($):</asp:Label>&nbsp;&nbsp; 
                                            <asp:Label id="lblTotalOustandingFrom" runat="server" CssClass="Content Color2"></asp:Label>&nbsp;&nbsp;&nbsp; 
                                            <asp:LinkButton id="lnkRecordPaymentFrom" onclick="lnkRecordPaymentFrom_Click" runat="server" CssClass="LinkNormal">Record Payment</asp:LinkButton> 
                                        </td>
                                    </tr>
                                </TBODY>
                            </TABLE>
                        </FIELDSET>
                        <asp:RangeValidator id="TermFromValidator1" runat="server" CssClass="Content Color3 Bold" MaximumValue="999" MinimumValue="0" Display="none" ErrorMessage="Sell Term must be greater than zero." ControlToValidate="txtTermsFrom"></asp:RangeValidator> 
                        <asp:CompareValidator id="TermFromValidator2" runat="server" CssClass="Content Color3 Bold" Display="none" ErrorMessage="Sell Term must be a integer." ControlToValidate="txtTermsFrom" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator> 
                        <asp:RequiredFieldValidator id="TermFromValidator3" runat="server" CssClass="Content Color3 Bold" Display="none" ErrorMessage="Sell Term cannot be blank" ControlToValidate="txtTermsFrom" Enabled="False"></asp:RequiredFieldValidator> 
                        <asp:RangeValidator id="BuyPriceValidator1" runat="server" CssClass="Content Color3 Bold" MaximumValue="2.999" MinimumValue=".001" Display="none" ErrorMessage="Buy price must be less than one dollar per pound." ControlToValidate="txtBuyPrice" Type="Double"></asp:RangeValidator> 
                        <asp:CompareValidator id="BuyPriceValidator2" runat="server" CssClass="Content Color3 Bold" Display="none" ErrorMessage="Buy Price must be a number." ControlToValidate="txtBuyPrice" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator> 
                        <asp:RequiredFieldValidator id="BuyPriceValidator3" runat="server" CssClass="Content Color3 Bold" Display="none" ErrorMessage="Buy Price cannot be blank" ControlToValidate="txtBuyPrice"></asp:RequiredFieldValidator> 
                    </td>
                </tr>
            </TBODY>
        </TABLE>
        <TABLE id="Table5" cellSpacing=1 cellPadding=1 width="100%" border=0>
            <TBODY>
                <tr>
                    <td height=11>
                        <asp:Label id="lblPONumber" runat="server" CssClass="Content Color2">PO#</asp:Label> 
                    </td>
                    <td height=11>
                        <asp:TextBox id="txtPONumber" runat="server" Width="100%" CssClass="InputForm"></asp:TextBox> 
                    </td>
                    <td>
                        <asp:Label id="lblTerm" runat="server" CssClass="Content Color2">Term</asp:Label> 
                    </td>
                    <td>
                        <asp:DropDownList id="ddlTerm" runat="server" CssClass="InputForm" AutoPostBack="True">
                        <asp:ListItem Value="FOB/Shipping">FOB/Shipping</asp:ListItem>
                        </asp:DropDownList>                         
                    </td>
                    <td height=11>
                        <asp:Label id="lblWeight" runat="server" CssClass="Content Color2">Weight</asp:Label> 
                    </td>
                    <td height=11>
                        <asp:TextBox id="txtWeight" runat="server" Width="100%" CssClass="InputForm"></asp:TextBox> 
                    </td>
                    <td colSpan=2 rowSpan=4>
                        <FIELDSET width="200">
                            <LEGEND>
                                <SPAN class="Header Color2 Bold">Shipment Notes</SPAN>
                            </LEGEND>
                            <TABLE id="Table6" cellSpacing=1 cellPadding=1 width="100%" border=0>
                                <TBODY>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtCommentNotes" runat="server" Width="140px" CssClass="InputForm" TextMode="Multiline" size="500" lines="2"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:HyperLink id="imgComment" runat="server" />
                                            
                                        </td>
                                        <td>
                                            <SPAN class="Content">Comment</SPAN>&nbsp;&nbsp; 
                                            <asp:Button Visible="false" id="btnAddNotes" onclick="btnAddNotes_Click" runat="server" Width="40px" CssClass="Content Color2" Text="Add"></asp:Button> 
                                        </td>
                                    </tr>
                                </TBODY>
                            </TABLE>
                        </FIELDSET>
                        <br />
                        <asp:Image id="imgInternalNotes" runat="server"></asp:Image>
                        <asp:Label runat="server" CssClass="Content Color2">  Internal Notes</asp:Label> 
                    </td>
                </tr>
                <tr>
                    <td height=11>
                        <asp:Label id="lblBroker" runat="server" CssClass="Content Color2">Broker</asp:Label> 
                    </td>
                    <td height=11>
                        <asp:DropDownList id="ddlBroker" runat="server" CssClass="InputForm" OnSelectedIndexChanged="ddlBroker_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                    </td>
                    <td height=11>
                        <asp:Label id="lblComission" runat="server" CssClass="Content Color2">Commission (%)</asp:Label> 
                    </td>
                    <td height=11>
                        <asp:TextBox id="txtComission" runat="server" Width="70%" CssClass="InputForm"></asp:TextBox> 
                    </td>
                    <td>
                        <asp:Label id="Label32" runat="server" CssClass="Content Color2">Profit ($)</asp:Label> 
                    </td>
                    <td>
                        <asp:Label id="lblProfit" runat="server" CssClass="Content Color2" Font-Bold="True"></asp:Label> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label id="lblGst" runat="server" CssClass="Content Color2">GST: </asp:Label> 
                        <asp:TextBox id="txtGST" runat="server" Width="50px" CssClass="InputForm"></asp:TextBox> 
                    </td>
                    <td>
                        <asp:Label id="lblCustoms" runat="server" CssClass="Content Color2">Customs: </asp:Label> 
                        <asp:TextBox id="txtCustoms" runat="server" Width="50px" CssClass="InputForm"></asp:TextBox> 
                    </td>
                    <td>
                        <asp:Label id="lblCommPaid" runat="server" CssClass="Content Color2">Commission is not Paid</asp:Label>                        
                    </td>
                    <td>
                        <asp:Label id="lblProblem" runat="server" CssClass="Content Color3 Bold">Problem: </asp:Label> 
                        <asp:CheckBox id="chkProblem" onclick="showMe('divProblem');" runat="server"></asp:CheckBox> 
                    </td>
                    <td>
                        <div style="VISIBILITY: hidden" id="divProblem">
                            <asp:TextBox id="txtProblem" runat="server" Width="100px" CssClass="InputForm"></asp:TextBox> 
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colSpan=5 height=180>
                        <FIELDSET width="600">
                            <LEGEND>
                                <SPAN class="Header Color2 Bold">Inventory</SPAN> 
                            </LEGEND>
                            <TABLE id="Table9" height="0" cellSpacing=1 cellPadding=1 width="100%" border=0>
                                <TBODY>
                                    <tr>
                                        <td width=125>
                                            <asp:Label id="lblFreight" runat="server" CssClass="Content Color2">Freight ($) </asp:Label> 
                                        </td>
                                        <td align=right width=75>
                                            <asp:Label id="lblFreightEst" runat="server" CssClass="Content Color2">Estimate </asp:Label> 
                                        </td>
                                        <td width=125>
                                            <asp:TextBox id="txtFreightEst" runat="server" Width="90%" CssClass="InputForm"></asp:TextBox> 
                                        </td>
                                        <td width=150>
                                            <asp:DropDownList id="ddlFreightComp" runat="server" CssClass="InputForm" name="ddlFreightComp">
								            </asp:DropDownList> 
								        </td>
							            <td width=225>
							            </td>
							        </tr>
							        <tr>
							            <td>
							            </td>
							            <td align=right>
							                <asp:Label id="lblFreightInv" runat="server" CssClass="Content Color2">Invoice </asp:Label> 
							            </td>
							            <td>
								            <asp:TextBox id="txtFreightInv" runat="server" Width="90%" CssClass="InputForm"></asp:TextBox> 
								        </td>
								        <td>
								            <asp:Label id="lblFreightInvNumber" runat="server" CssClass="Content Color2">Inv #</asp:Label> 
								            <asp:TextBox id="txtFreightInvNumber" runat="server" Width="80%" CssClass="InputForm"></asp:TextBox> 
								        </td>
								        <td>
								            <asp:Label id="lblFreightInvDate" runat="server" CssClass="Content Color2">Date</asp:Label> 
								            <сс:GMDatePicker id="GMDatePickerFreight" runat="server" CssClass="Content LinkNormal" InitialText=" " CalendarFont-Names="Arial" TextBoxWidth="120">
                                                <CalendarDayStyle Font-Size="9pt" /><CalendarTodayDayStyle BorderWidth="1px" BorderColor="DarkRed" Font-Bold="True" />
                                                <CalendarOtherMonthDayStyle BackColor="WhiteSmoke" /><CalendarTitleStyle BackColor="#E0E0E0" Font-Names="Arial" Font-Size="9pt" />
                                            </сс:GMDatePicker> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label id="lblWharehouse" runat="server" CssClass="Content Color2">Warehouse ($) </asp:Label> 
                                        </td>
                                        <td align="right">
                                            <asp:Label id="lblWharehouseEst" runat="server" CssClass="Content Color2" align="right">Estimate </asp:Label> 
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtWharehouseEst" runat="server" Width="90%" CssClass="InputForm"></asp:TextBox> 
                                        </td>
                                        <td>
                                            <asp:DropDownList id="ddlWarehouseComp" runat="server" CssClass="InputForm" name="ddlWarehouseComp"></asp:DropDownList> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td align="right">
                                            <asp:Label id="lblWharehouseInv" runat="server" CssClass="Content Color2">Invoice </asp:Label> 
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtWharehouseInv" runat="server" Width="90%" CssClass="InputForm"></asp:TextBox> 
                                        </td>
                                        <td>
                                            <asp:Label id="lblWharehouseInvNumber" runat="server" CssClass="Content Color2">Inv #</asp:Label> 
                                            <asp:TextBox id="txtWharehouseInvNumber" runat="server" Width="80%" CssClass="InputForm"></asp:TextBox> 
                                        </td>
                                        <td>
                                            <asp:Label id="lblWharehouseInvDate" runat="server" CssClass="Content Color2">Date</asp:Label> 
                                            <сс:GMDatePicker id="GMDatePickerWharehouse" runat="server" CssClass="Content LinkNormal" InitialText=" " CalendarFont-Names="Arial" TextBoxWidth="120">
                                                <CalendarDayStyle Font-Size="9pt" /><CalendarTodayDayStyle BorderWidth="1px" BorderColor="DarkRed" Font-Bold="True" />
                                                <CalendarOtherMonthDayStyle BackColor="WhiteSmoke" /><CalendarTitleStyle BackColor="#E0E0E0" Font-Names="Arial" Font-Size="9pt" />
                                            </сс:GMDatePicker> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label id="lblMisc" runat="server" CssClass="Content Color2">Misc ($) </asp:Label> 
                                        </td>
                                        <td align=right>
                                            <asp:Label id="lblMiscEst" runat="server" CssClass="Content Color2" align="right">Estimate </asp:Label> 
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtMiscEst" runat="server" Width="90%" CssClass="InputForm"></asp:TextBox> 
                                        </td>
                                        <td align=left>
                                            <asp:Label id="lblMiscDescription" runat="server" CssClass="Content Color2" align="right">Description </asp:Label> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td align=right>
                                            <asp:Label id="lblMiscInv" runat="server" CssClass="Content Color2">Invoice </asp:Label> 
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtMiscInv" runat="server" Width="90%" CssClass="InputForm"></asp:TextBox> 
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtMiscDescription" runat="server" Width="80%" CssClass="InputForm"></asp:TextBox> 
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </TBODY>
                            </TABLE>                            
                        </FIELDSET> 
                    </td>
                </tr>
            </TBODY>
        </TABLE>
    </FIELDSET>
    <asp:RangeValidator id="CommissionValidator1" runat="server" CssClass="Content Color3 Bold" MaximumValue="100" MinimumValue="0" Display="none" ErrorMessage="Commission must be between 0% and 100%." ControlToValidate="txtComission" Type="Double"></asp:RangeValidator> 
    <asp:CompareValidator id="CommissionValidator2" runat="server" CssClass="Content Color3 Bold" Display="none" ErrorMessage="Commission must be a number." ControlToValidate="txtComission" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator> 
    <br />
    <TABLE id="Table7" cellSpacing=1 cellPadding=1 width="100%" border=0>
        <TBODY>
            <tr>
                <td vAlign=top>
                    <uc1:shipment_documents id="Shipment_Documents1" runat="server"></uc1:shipment_documents> 
                </td>
                <td vAlign=top>
                    <asp:Panel id="pnlFiles" runat="server">
                        <FIELDSET width="200">
                            <LEGEND>
                                <SPAN class="Header Color2 Bold">Shipment Files</SPAN> 
                            </LEGEND>
                            <br />
                            <asp:DataGrid id="dg" runat="server" Width="500" CssClass="DataGrid" AutoGenerateColumns="false" ItemStyle-CssClass="LinkNormal LightGray" AlternatingItemStyle-CssClass="LinkNormal DarkGray" HeaderStyle-CssClass="LinkNormal Bold OrangeColor">
                                <Columns>
                                    <asp:ButtonColumn Text="Delete" CommandName="Delete" />
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:ButtonColumn Text="FILE_NAME" DataTextField="FILE_NAME" HeaderText="File" CommandName="Edit" />
                                    <asp:BoundColumn DataField="FILE_DESCRIPT" HeaderText="Description" SortExpression="FILE_DESCRIPT ASC" ItemStyle-HorizontalAlign="left" />
                                    <asp:BoundColumn DataField="FILE_OWNER" HeaderText="Owner" SortExpression="FILE_OWNER ASC" ItemStyle-HorizontalAlign="left" />
                                    <asp:BoundColumn DataField="FILE_DATE" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}" />
                                    <asp:BoundColumn DataField="FILE_BUYER" HeaderText="Buyer" ItemStyle-HorizontalAlign="center" />
                                    <asp:BoundColumn DataField="FILE_SELLER" HeaderText="Seller" ItemStyle-HorizontalAlign="center" />
                                    <asp:BoundColumn DataField="ID" Visible="false" HeaderText="File_Id" SortExpression="ID ASC" ItemStyle-HorizontalAlign="right" />
                                </Columns>
                            </asp:DataGrid>
                            <br />
                            <TABLE id="Table8" width="100%" border=0>
                                <TBODY>
                                    <tr>
                                        <td>
                                            <asp:Label id="Label22" runat="server" CssClass="Content Color2">File to Add</asp:Label>&nbsp; 
                                        </td>
                                        <td>
                                            <input id="File_to_add" class="InputForm" type=file size=22 name="MyFile" runat="Server" /> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label id="Label13" runat="server" CssClass="Content Color2">Description</asp:Label> 
                                        </td>
                                        <td>
                                            <asp:TextBox id="Description" runat="server" Width="188px" CssClass="InputForm"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label id="Label36" runat="server" CssClass="Content Color2">Access Rights</asp:Label> 
                                        </td>
                                        <td>
                                            <asp:CheckBox id="CheckBuyer" runat="server" CssClass="Content Color2"></asp:CheckBox> 
                                            <asp:Label id="lblBuyer" runat="server" CssClass="Content Color2" Font-Bold="True">JLM Engineered Resins</asp:Label> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:CheckBox id="CheckSeller" runat="server" CssClass="Content Color2"></asp:CheckBox> 
                                            <b>
                                                <asp:Label id="lblSeller" runat="server" CssClass="Content Color2">Cal Thermo</asp:Label> 
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align=center colSpan=2 height=15>
                                            <asp:Button Visible="false" id="btnUpload" onclick="btnUpload_Click" runat="server" CssClass="Content Color2" Text="Upload"></asp:Button> 
                                        </td>
                                    </tr>
                                </TBODY>
                            </TABLE>
                        </FIELDSET>
                    </asp:Panel>
                </td>
            </tr>
        </TBODY>
    </TABLE>
</asp:Panel>
<asp:Panel id="pnlConfirmationPanel" runat="server" Visible="false">
    <%--<td width="100%" valign="top" align="left" />--%>
    <FIELDSET width="200">
        <LEGEND>
            <SPAN class="Header Color3 Bold">Approvement</SPAN> 
        </LEGEND>
        <br />
        <TABLE width="100%">
            <TBODY>
                <tr>
                    <td align=left>
                        <asp:Label id="lblConfirmMessage" runat="server" Visible="false" CssClass="Content Color3 Bold">Sorry, incorrect password! Please, try again.</asp:Label> 
                    </td>
                </tr>
            </TBODY>
        </TABLE>
        <asp:Panel id="pnlSubPanel" runat="server" Visible="true">
            <TABLE width="100%">
                <TBODY>
                    <tr>
                        <td align=left>
                            <asp:Label id="lblConfirmationMessage" runat="server" CssClass="Content Color3 Bold"></asp:Label> 
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align=center>
                            <asp:TextBox id="txtPassword" runat="server" CssClass="InputForm" TextMode="Password"></asp:TextBox> 
                            <asp:Button Visible="false" id="btnSubmitPassword" onclick="btnSubmitPassword_Click" runat="server" CssClass="Content Color2" Text="Confirm"></asp:Button> 
                            <asp:Button Visible="false" id="btnCancel" onclick="btnCancel_Click" runat="server" CssClass="Content Color2" Text="Cancel"></asp:Button> 
                        </td>
                    </tr>
                </TBODY>
            </TABLE>
        </asp:Panel>
    </FIELDSET> 
</asp:Panel> 
<asp:Panel id="pnlCanNotEdit" runat="server" Visible="false">
        <%--<td width="100%" align="left" valign="top">--%>
    <fieldset width="200">
        <legend>
            <span class="Header Color3 Bold">Already received Payment</span>
        </legend>
        <br />
        <table width="100%">
            <tr>
                <td align="left">
                    <asp:Label CssClass="Content Color3 Bold" ID="lblCanNotEdit" runat="server">This can not be edited/deleted since we have payments associated with this transaction!!!</asp:Label><br />
                    <br />
                    <asp:Button Visible="false" CssClass="Content Color2" ID="btnOK" runat="server" Text="Return To The Screen"
                        OnClick="btnOK_Click"></asp:Button>
                </td>
            </tr>
        </table>
    </fieldset>
        <%--</td>--%>
</asp:Panel> 
</ContentTemplate>
    </asp:UpdatePanel>  



<asp:UpdateProgress ID="UpdateProgress1" runat="server">
    <ProgressTemplate>  
            <asp:Panel runat="server" ID="pnUpdateProgress" BackColor="Gainsboro" Width="400px" Height="200px">
                <div style="text-align:center;width:400px;height:200px">
                    <br /><br /><br /><br />
                        <asp:Image ID="Image1" runat="server" ImageUrl="http://www.theplasticexchange.com/images/progressbar_green.gif" />
                    <br />
                    <asp:Label ID="Label1" runat="server" Font-Names="Arial" Font-Size="12px" Text="Communicating With Server..." />
                </div>
            </asp:Panel>        
    </ProgressTemplate>        
</asp:UpdateProgress>
<cc1:RoundedCornersExtender ID="rceUpdateProgress" BorderColor="Black" Corners="All" Radius="10" TargetControlID="pnUpdateProgress" runat="server" />
<cc1:AlwaysVisibleControlExtender ID="avceUpdateProgress" VerticalSide="top" HorizontalSide="left" HorizontalOffset="450" VerticalOffset="100" TargetControlID="pnUpdateProgress" runat="server" />
        
    
</asp:Content>
