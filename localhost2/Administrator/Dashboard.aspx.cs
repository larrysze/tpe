using System;
using System.Data;
using System.Configuration;using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using TPE.Utility;



namespace localhost.Administrator
{

    public partial class Dashboard : System.Web.UI.Page
    {

        double ReturningSum = 0.0;
        double DemoSum = 0.0;
        double PasswordSum = 0.0;
        double RegistraitionSum = 0.0;
        double BrokerSum = 0.0;
        double InvalidSum = 0.0;
        double ExchangeSum = 0.0;
        double AllocatedSum = 0.0;
        double UnsubscribeSum = 0.0;
        double AdminLogin = 0.0;


        public bool WeekTrigger = false;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

            GMDatePickerDashboard.InitialText = DateTime.Today.ToShortDateString();

                if ((Request.QueryString["param"].ToString() == "0") || (Request.QueryString["param"].ToString() == null))
                {
                    dgList.Visible = false;
                    dg.Visible = true;
                    lnkBack.Visible = false;
                    BindDG();
                }
                else
                {
                    dgList.Visible = true;
                    dg.Visible = false;
                    lnkBack.Visible = true;
                    BindDG2();
                }            
 
            }
        }



        public void BindDG()
        {
            Hashtable htParams = new Hashtable();
            DataSet dstContent;

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
	
		        DateTime week = GMDatePickerDashboard.Date;
                htParams.Add("@ID", Session["ID"].ToString());

                if (ddlSummary.SelectedValue.Equals("Yearly"))
                {
                    this.BindYearly();
                    return;
                }

                else if (ddlSummary.SelectedValue.Equals("Weekly"))
                {

                    while (week.DayOfWeek != DayOfWeek.Sunday)
                        week = week.AddDays(-1);

                    htParams.Add("@StartDate", week.ToShortDateString() + " 12:00:00AM");
                    htParams.Add("@EndDate", week.AddDays(6).ToShortDateString() + " 11:59:59PM");

                    dstContent = DBLibrary.GetDataSetFromStoredProcedure(conn, "spDashboardWeek", htParams);
                }
                else
                {
                    htParams.Add("@Month", week.Month);
                    htParams.Add("@Year", week.Year);
                    dstContent = DBLibrary.GetDataSetFromStoredProcedure(conn, "spDashboard", htParams);
                }

                dg.DataSource = dstContent;
                dg.DataBind();
            }

        }



        public void BindDG2()
        {
            string CurrentDate = Request.QueryString["date"].ToString();
            SqlConnection conn;

            conn = new SqlConnection(Application["DBconn"].ToString());
            conn.Open();

            string strSQL = "";

            if ((Session["ID"].ToString() == "2") || (Session["ID"].ToString() == "13564") || (Session["ID"].ToString() == "9445"))
            {
                if (Request.QueryString["param"].ToString() == "1")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, (Select PERS_MAIL from PERSON WHERE PERS_ID=PERSON_ID)as EMAIL, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=1 and convert(varchar,DATE_TIME,101)='" + CurrentDate + "'order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                }

                if (Request.QueryString["param"].ToString() == "2")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, (Select PERS_MAIL from PERSON WHERE PERS_ID=PERSON_ID)as EMAIL, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=2 and convert(varchar,DATE_TIME,101)='" + CurrentDate + "'order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                }


                if (Request.QueryString["param"].ToString() == "3")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_MAIL=EMAIL) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_MAIL=EMAIL) as LAST_NAME, EMAIL, (Select COMP_NAME from PERSON WHERE PERS_MAIL=EMAIL)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_MAIL=EMAIL) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=3 and convert(varchar,DATE_TIME,101)='" + CurrentDate + "' order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                }

                if (Request.QueryString["param"].ToString() == "4")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, (Select PERS_MAIL from PERSON WHERE PERS_ID=PERSON_ID)as EMAIL, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=4 and convert(varchar,DATE_TIME,101)='" + CurrentDate + "'order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                }

                if (Request.QueryString["param"].ToString() == "5")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, (Select PERS_MAIL from PERSON WHERE PERS_ID=PERSON_ID)as EMAIL, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=5 and convert(varchar,DATE_TIME,101)='" + CurrentDate + "'order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                }

                if (Request.QueryString["param"].ToString() == "6")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, EMAIL, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=6 and convert(varchar,DATE_TIME,101)='" + CurrentDate + "' order by date_time desc";
                    dgList.Columns[1].Visible = false;
                    dgList.Columns[2].Visible = false;
                    dgList.Columns[3].Visible = false;
                    dgList.Columns[5].Visible = false;
                    dgList.Columns[7].Visible = true;
                    dgList.Columns[8].Visible = false;
                }

                if (Request.QueryString["param"].ToString() == "7")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, (Select PERS_MAIL from PERSON WHERE PERS_ID=PERSON_ID)as EMAIL, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=7 and convert(varchar,DATE_TIME,101)='" + CurrentDate + "' order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                }

                if (Request.QueryString["param"].ToString() == "8")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, (Select PERS_MAIL from PERSON WHERE PERS_ID=PERSON_ID)as EMAIL, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=8 and convert(varchar,DATE_TIME,101)='" + CurrentDate + "'order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                }
                if (Request.QueryString["param"].ToString() == "9")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_MAIL=EMAIL) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_MAIL=EMAIL) as LAST_NAME, EMAIL, (Select COMP_NAME from PERSON WHERE PERS_MAIL=EMAIL)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_MAIL=EMAIL) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=9 and convert(varchar,DATE_TIME,101)='" + CurrentDate + "' order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                    dgList.Columns[9].Visible = false;
                }
                if (Request.QueryString["param"].ToString() == "11")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, (Select PERS_MAIL from PERSON WHERE PERS_ID=PERSON_ID)as EMAIL, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=11 and convert(varchar,DATE_TIME,101)='" + CurrentDate + "'order by date_time desc";
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                }
            }

            else
            {
                if (Request.QueryString["param"].ToString() == "1")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, (Select PERS_MAIL from PERSON WHERE PERS_ID=PERSON_ID)as EMAIL, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=1 and (Select PERS_ACES from PERSON WHERE PERS_ID=PERSON_ID)='" + Session["ID"].ToString() + "' and  convert(varchar,DATE_TIME,101)='" + CurrentDate + "'order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                }

                if (Request.QueryString["param"].ToString() == "2")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, (Select PERS_MAIL from PERSON WHERE PERS_ID=PERSON_ID)as EMAIL, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=2 and (Select PERS_ACES from PERSON WHERE PERS_ID=PERSON_ID)='" + Session["ID"].ToString() + "' and convert(varchar,DATE_TIME,101)='" + CurrentDate + "'order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                }

                if (Request.QueryString["param"].ToString() == "3")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_MAIL=EMAIL) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_MAIL=EMAIL) as LAST_NAME, EMAIL, (Select COMP_NAME from PERSON WHERE PERS_MAIL=EMAIL)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_MAIL=EMAIL) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=3 and (Select PERS_ACES from PERSON WHERE PERS_MAIL=EMAIL)='" + Session["ID"].ToString() + "' and convert(varchar,DATE_TIME,101)='" + CurrentDate + "' order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                }

                if (Request.QueryString["param"].ToString() == "4")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, (Select PERS_MAIL from PERSON WHERE PERS_ID=PERSON_ID)as EMAIL, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=4 and (Select PERS_ACES from PERSON WHERE PERS_ID=PERSON_ID)='" + Session["ID"].ToString() + "' and convert(varchar,DATE_TIME,101)='" + CurrentDate + "'order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                }
                if (Request.QueryString["param"].ToString() == "5")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, (Select PERS_MAIL from PERSON WHERE PERS_ID=PERSON_ID)as EMAIL, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=5 and (Select PERS_ACES from PERSON WHERE PERS_ID=PERSON_ID)='" + Session["ID"].ToString() + "' and convert(varchar,DATE_TIME,101)='" + CurrentDate + "'order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                }

                if (Request.QueryString["param"].ToString() == "6")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, EMAIL, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=6 and (Select PERS_ACES from PERSON WHERE PERS_ID=PERSON_ID)='" + Session["ID"].ToString() + "' and convert(varchar,DATE_TIME,101)='" + CurrentDate + "' order by date_time desc";
                    dgList.Columns[1].Visible = false;
                    dgList.Columns[2].Visible = false;
                    dgList.Columns[3].Visible = false;
                    dgList.Columns[5].Visible = false;
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                }

                if (Request.QueryString["param"].ToString() == "7")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, (Select PERS_MAIL from PERSON WHERE PERS_ID=PERSON_ID)as EMAIL, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=7 and (Select PERS_ACES from PERSON WHERE PERS_ID=PERSON_ID)='" + Session["ID"].ToString() + "' and convert(varchar,DATE_TIME,101)='" + CurrentDate + "' order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                }

                if (Request.QueryString["param"].ToString() == "8")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, (Select PERS_MAIL from PERSON WHERE PERS_ID=PERSON_ID)as EMAIL, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=8 and (Select PERS_ACES from PERSON WHERE PERS_ID=PERSON_ID)='" + Session["ID"].ToString() + "' and convert(varchar,DATE_TIME,101)='" + CurrentDate + "'order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                }

                if (Request.QueryString["param"].ToString() == "9")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_MAIL=EMAIL) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_MAIL=EMAIL) as LAST_NAME, EMAIL, (Select COMP_NAME from PERSON WHERE PERS_MAIL=EMAIL)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_MAIL=EMAIL) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=3 and (Select PERS_ACES from PERSON WHERE PERS_MAIL=EMAIL)='" + Session["ID"].ToString() + "' and convert(varchar,DATE_TIME,101)='" + CurrentDate + "' order by date_time desc";
                    dgList.Columns[6].Visible = false;
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                    dgList.Columns[9].Visible = false;
                }

                if (Request.QueryString["param"].ToString() == "11")
                {
                    strSQL = "SELECT ID, DATE_TIME, (Select PERS_FRST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as FIRST_NAME, (Select PERS_LAST_NAME from PERSON WHERE PERS_ID=PERSON_ID) as LAST_NAME, (Select PERS_MAIL from PERSON WHERE PERS_ID=PERSON_ID)as EMAIL, (Select COMP_NAME from PERSON WHERE PERS_ID=PERSON_ID)as COMP_NAME, (Select PERS_PHON from PERSON WHERE PERS_ID=PERSON_ID) as PERS_PHON, IP, Password, Allocated_to, PERSON_ID FROM DASHBOARD where ID_TYPE=11 and convert(varchar,DATE_TIME,101)='" + CurrentDate + "'order by date_time desc";
                    dgList.Columns[7].Visible = false;
                    dgList.Columns[8].Visible = false;
                }
            }


            SqlDataAdapter dadContent;
            DataSet dstContent;
            dadContent = new SqlDataAdapter(strSQL, conn);
            dstContent = new DataSet();
            dadContent.Fill(dstContent);
            dgList.DataSource = dstContent;
            dgList.DataBind();

            conn.Close();

        }

        protected void OnDataGridBind(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ReturningSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "t1"));
                DemoSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "t2"));
                PasswordSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "t3"));
                RegistraitionSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "t4"));
                BrokerSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "t5"));
                InvalidSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "t6"));
                ExchangeSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "t7"));
                AllocatedSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "t8"));
                UnsubscribeSum += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "t9"));
                AdminLogin += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "t11"));
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                e.Item.Cells[0].Text = "<b>Total:</b> ";
                e.Item.Cells[1].Text = "<b>" + ReturningSum + "</b>";
                e.Item.Cells[2].Text = "<b>" + DemoSum + "</b>";
                e.Item.Cells[3].Text = "<b>" + PasswordSum + "</b>";
                e.Item.Cells[4].Text = "<b>" + RegistraitionSum + "</b>";
                e.Item.Cells[5].Text = "<b>" + BrokerSum + "</b>";
                e.Item.Cells[6].Text = "<b>" + InvalidSum + "</b>";
                e.Item.Cells[7].Text = "<b>" + ExchangeSum + "</b>";
                e.Item.Cells[8].Text = "<b>" + AllocatedSum + "</b>";
                e.Item.Cells[9].Text = "<b>" + UnsubscribeSum + "</b>";
                e.Item.Cells[10].Text = "<b>" + AdminLogin + "</b>";
            }
        } 


        protected void dgList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if ((Request.QueryString["param"].ToString() != "6") && (Request.QueryString["param"].ToString() != "3") && (Request.QueryString["param"].ToString() != "9"))
            {
                if (e.Item.ItemType != ListItemType.Header)
                    e.Item.Cells[4].Text = "<a style='color:black; text-decoration:underline; cursor:pointer' onClick=window.open('/administrator/user_Details.aspx?ID=" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "PERSON_ID")) + "','win','scrollbars=yes,resizable=yes,width=550,height=565')>" + e.Item.Cells[4].Text + "</a>";
            }

            dgList.Columns[10].Visible = false;
        }

        protected void OnClick_btnRefresh(object sender, EventArgs e)
        {
            ReturningSum = 0.0;
            DemoSum = 0.0;
            PasswordSum = 0.0;
            RegistraitionSum = 0.0;
            BrokerSum = 0.0;
            InvalidSum = 0.0;
            ExchangeSum = 0.0;
            AllocatedSum = 0.0;
            UnsubscribeSum = 0.0;
            AdminLogin = 0;

            BindDG();
        }

        private void BindYearly()
        {
            DateTime baseDate = new DateTime(GMDatePickerDashboard.Date.Year, 1, 1);

            DateTime endDate = DateTime.Now;
            if (baseDate.Year != endDate.Year) //if it's not this year selected, set it as the end of the year.
            {
                endDate = new DateTime(baseDate.Year, 12, 31);
            }
            
            DataTable dtSummary = new DataTable("tblSummary");
            dtSummary.Columns.Add("date", typeof(string));
            dtSummary.Columns.Add("t1", typeof(string));
            dtSummary.Columns.Add("t2", typeof(string));
            dtSummary.Columns.Add("t3", typeof(string));
            dtSummary.Columns.Add("t4", typeof(string));
            dtSummary.Columns.Add("t5", typeof(string));
            dtSummary.Columns.Add("t6", typeof(string));
            dtSummary.Columns.Add("t7", typeof(string));
            dtSummary.Columns.Add("t8", typeof(string));
            dtSummary.Columns.Add("t9", typeof(string));
            dtSummary.Columns.Add("t11", typeof(string));

            double ReturningSum = 0.0;
            double DemoSum = 0.0;
            double PasswordSum = 0.0;
            double RegistraitionSum = 0.0;
            double BrokerSum = 0.0;
            double InvalidSum = 0.0;
            double ExchangeSum = 0.0;
            double AllocatedSum = 0.0;
            double UnsubscribeSum = 0.0;
            double AdminLogin = 0.0;

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                while ( baseDate <= endDate )
                {
                    Hashtable htParams = new Hashtable();
                    htParams.Add("@ID", Session["ID"].ToString());
                    htParams.Add("@Month", baseDate.Month);
                    htParams.Add("@Year", baseDate.Year);

                    SqlDataReader drMonth;
                    drMonth = DBLibrary.GetDataReaderStoredProcedure(conn,"spDashBoard",htParams);
                   
                        ReturningSum = 0;
                        DemoSum = 0;
                        PasswordSum = 0;
                        RegistraitionSum = 0;
                        BrokerSum = 0;
                        InvalidSum = 0;
                        ExchangeSum = 0;
                        AllocatedSum = 0;
                        UnsubscribeSum = 0;
                        AdminLogin = 0;

                        if (drMonth.HasRows)
                        while (drMonth.Read())
                        {
                            ReturningSum += Convert.ToDouble(drMonth[1]);
                            DemoSum += Convert.ToDouble(drMonth[2]);
                            PasswordSum += Convert.ToDouble(drMonth[3]);
                            RegistraitionSum  += Convert.ToDouble(drMonth[4]);
                            BrokerSum +=  Convert.ToDouble(drMonth[5]);
                            InvalidSum  += Convert.ToDouble(drMonth[6]);
                            ExchangeSum += Convert.ToDouble(drMonth[7]);
                            AllocatedSum  += Convert.ToDouble(drMonth[8]);
                            UnsubscribeSum += Convert.ToDouble(drMonth[9]);
                            AdminLogin += Convert.ToDouble(drMonth[10]);
                        }
                        
                        DataRow newRow;
                        newRow= dtSummary.NewRow();

                        newRow["date"] =  baseDate.Year + "/" + baseDate.Month;
                        newRow["t1"] = ReturningSum;
                        newRow["t2"] = DemoSum;
                        newRow["t3"] = PasswordSum;
                        newRow["t4"] = RegistraitionSum;
                        newRow["t5"] = BrokerSum;
                        newRow["t6"] = InvalidSum;
                        newRow["t7"] = ExchangeSum;
                        newRow["t8"] = AllocatedSum;
                        newRow["t9"] = UnsubscribeSum;
                        newRow["t11"] = AdminLogin;

                        dtSummary.Rows.Add(newRow);
                    
                    drMonth.Close();
                    baseDate = baseDate.AddMonths(1);
                }
                    dg.DataSource = dtSummary;
                    dg.DataBind();
             }
        }
    }

}


