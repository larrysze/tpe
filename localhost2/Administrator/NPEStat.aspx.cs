using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using dotnetCHARTING;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for NPEStat.
	/// </summary>
	public class NPEStat : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblGarbage;
		protected System.Web.UI.WebControls.Button chart1;
		protected System.Web.UI.WebControls.Button Button1;
		protected dotnetCHARTING.Chart Chart;
	


		private SeriesCollection getData(string chart_num)
		{

			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
	
			string sql_day =  "select DAY(npe_card_time) as NPE_X, COUNT(DAY(npe_card_time)) as NPE_Y from npe_cards GROUP BY DAY(npe_card_time)";
			string sql_hour = "select datepart(hour, npe_card_time) as NPE_X, COUNT(datepart(hour, npe_card_time)) as NPE_Y from npe_cards GROUP BY datepart(hour, npe_card_time)";

			string sql_str = sql_day;
			string xName = "";
			string mode = "";
			bool hours = false;
			bool isURL = true;

			if(Request.QueryString["param"] != null)
			{
				isURL = false;
				int int_param = Convert.ToInt32(Request.QueryString["param"]);
				if( (Request.QueryString["mode"] != null) && (Request.QueryString["mode"] == "1") )
				{
					sql_str = "select DAY(npe_card_time), datepart(hour, npe_card_time) as NPE_X, COUNT(datepart(hour, npe_card_time)) as NPE_Y from npe_cards GROUP BY datepart(hour, npe_card_time), DAY(npe_card_time) HAVING DAY(npe_card_time) = " + Request.QueryString["param"];
					xName = "Hours: ";
					
					Chart.ChartArea.XAxis.Label.Text = "June: " + int_param + " (by hour)";

					// Set the title.
					Chart.Title="Number of registered cards on June " + int_param;
					hours = true;


				}
				else
				{
					sql_str = "select DAY(npe_card_time) as NPE_X, datepart(hour, npe_card_time), COUNT(DAY(npe_card_time)) as NPE_Y from npe_cards GROUP BY DAY(npe_card_time), datepart(hour, npe_card_time) HAVING datepart(hour, npe_card_time)  = " + Request.QueryString["param"];
					xName = "June ";
					
					Chart.ChartArea.XAxis.Label.Text = "Hour: " + int_param + "-" + (int_param + 1) + " (by date)";
					Chart.Title="Number of registered at " + int_param;
				}

			}else if(chart_num == "2")
			{
				sql_str = sql_hour;
				xName = "Hour ";
				mode = "2";
			}
			else 
			{
				sql_str = sql_day;
				xName = "June ";
				mode = "1";
			}


			SqlDataReader dr = DBLibrary.GetDataReaderFromSelect(conn, sql_str);
	

			SeriesCollection SC = new SeriesCollection();

			Series s = new Series();
	
			s.Name = "Num of cards";
	
			while(dr.Read())
			{
				Element e = new Element();
				if(hours)
				{
					int hour = Convert.ToInt32(dr["NPE_X"].ToString());

					e.Name = xName +  hour + "-" + (hour + 1);
				}
				else
				{
					e.Name = xName + dr["NPE_X"].ToString();
				}
				e.YValue = Convert.ToDouble(dr["NPE_Y"].ToString());
				e.Hotspot.ToolTip = dr["NPE_Y"].ToString();
				if(isURL)
				{
					e.Hotspot.URL = "/Administrator/NPEStat.aspx?param=" + dr["NPE_X"].ToString() + "&mode=" + mode;
				}
				
				s.Elements.Add(e);

			}
	
			dr.Close();
			conn.Close();
	
			SC.Add(s);

			// Set Different Colors for our Series
			//	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
			return SC;
		}


		void Page_Load(Object sender,EventArgs e)
		{

			if (Session["Typ"].ToString() != "A" )
			{
				Response.Redirect("/default.aspx");
			}
			
			// Set the title.
			Chart.Title="NPE 2006 Report";
			Chart.DefaultSeries.Type = SeriesType.Line;
			Chart.Mentor = false;

			// Set the x axis label
			Chart.ChartArea.XAxis.Label.Text = "Date";

			// Set the y axis label
			Chart.ChartArea.YAxis.Label.Text = "Number of registered cards";

			// Set the directory where the images will be stored.
			Chart.TempDirectory="temp";

			// Set the bar shading effect
			Chart.ShadingEffect = true;

			// Set he chart size.
			Chart.Width = 600;
			Chart.Height = 350;

			// Add the random data.
			Chart.SeriesCollection.Add(getData(Request.QueryString["chart"]));

			//use only if we want to copy all data from "rawcarddata table" to "npe_cards table"
			//in application we use only "npe_cards table" since in "rawcarddata table" "card_date column" is varchar
//			convertTable();
	
    
		}

		void convertTable()
		{
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			SqlDataReader dr = DBLibrary.GetDataReaderFromSelect(conn, "SELECT * FROM RAWCARDDATA");
			Hashtable param = new Hashtable();
	
			while(dr.Read())
			{
				param.Clear();
				try
				{
					DateTime date = (Convert.ToDateTime(dr["card_time"].ToString()));
					string card_date = date.Day + "/" + date.Month + "/" + Convert.ToString(date.Year).Substring(2) + " " +  (Convert.ToDateTime(dr["card_time"].ToString())).ToLongTimeString();
					lblGarbage.Text += card_date + "<B><BR>";
					param.Add("@data", dr["card_data"].ToString());
					param.Add("@time", date.ToShortDateString() + " " + date.ToShortTimeString());

					DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), "INSERT INTO NPE_CARDS(npe_card_data, npe_card_time) VALUES(@data, @time)", param);

				}
				catch(Exception e)
				{
					lblGarbage.Text += e.Message + "<BR>";
				}
		
			}
			dr.Close();
			conn.Close();
	
		}



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.chart1.Click += new System.EventHandler(this.chart1_Click);
			this.Button1.Click += new System.EventHandler(this.Button1_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void chart1_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("/Administrator/NPEStat.aspx?chart=1");

		}

		private void Button1_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("/Administrator/NPEStat.aspx?chart=2");
		
		}

	}
}
