<%@ Page Language="c#" Codebehind="IT_ToDoList.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.IT_ToDoList"
    MaintainScrollPositionOnPostback="true" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" %>

<%@ MasterType VirtualPath="~/MasterPages/TemplateWithoutInstructions.Master" %>
<asp:Content runat="server" ContentPlaceHolderID="cphHeading">
    <span class="Header Bold Color1">To do List</span></asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">
    <table cellpadding="2" width="100%" border="0">
        <tr align="center">
            <td>
                <span class="Content Bold Color2">Filter:&nbsp;&nbsp;</span>
                <asp:Label CssClass="Content Color2" ID="Label5" runat="server" Text="Status:"></asp:Label>
                <asp:DropDownList CssClass="InputForm" ID="ddlStatusFilter" runat="server" AutoPostBack="True"
                    OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                    <asp:ListItem Value="All">All</asp:ListItem>
                    <asp:ListItem Value="Open" Selected="True">Open</asp:ListItem>
                    <asp:ListItem Value="Closed">Closed</asp:ListItem>
                    <asp:ListItem Value="Pending">Pending</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label CssClass="Content Color2" ID="Label6" runat="server" Text="Assigned To:"></asp:Label>
                <asp:DropDownList CssClass="InputForm" ID="ddlDeveloperFilter" runat="server" AutoPostBack="True"
                    DataTextField="PERSON_NAME" DataValueField="PERSON_ID" OnSelectedIndexChanged="ddlDeveloperFilter_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td width="10">
                &nbsp;
            </td>
            <td>
                <asp:Button CssClass="Content Color2" ID="btnAddNew" runat="server" Text="Add New Item"
                    OnClick="btnAddNewItem_Click"></asp:Button></td>
        </tr>
    </table>
          <asp:Panel ID="pnlAddNew" runat="server" HorizontalAlign="Center" Visible="False">
            <table width="100%" border="0">
                <tbody>
                    <tr>
                        <td style="width: 141px">
                            <asp:Label ID="Label2" runat="server" CssClass="Content Color2">Screen Name:</asp:Label>
                        </td>
                        <td style="width: 371px">
                            <asp:Label ID="Label3" runat="server" CssClass="Content Color2">Suggestion/Bug:</asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblNewAssignedTo" runat="server" CssClass="Content Color2">Assigned To:</asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label4" runat="server" CssClass="Content Color2">Priority:</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 141px">
                            <asp:TextBox CssClass="InputForm" ID="NewScreenName" runat="server" MaxLength="100"></asp:TextBox></td>
                        <td style="width: 371px">
                            <asp:TextBox CssClass="InputForm" ID="NewDescription" runat="server" Width="353px"
                                MaxLength="500" Columns="50" TextMode="MultiLine" Rows="5"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList CssClass="InputForm" ID="ddlNewAssignedTo" runat="server" Width="177px"
                                DataTextField="ASSIGNED_TO" DataValueField="ASSIGNED_TO_ID">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList CssClass="InputForm" ID="ddlNewPriority" runat="server">
                                <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="3">
                            <asp:Button CssClass="Content Color2" ID="btnInsert" OnClick="btnInsert_Click" runat="server"
                                Text="Insert"></asp:Button>&nbsp;
                            <asp:Button CssClass="Content Color2" ID="btnCancel" OnClick="btnCancel_Click" runat="server"
                                Text="Cancel"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel> 
        <asp:DataGrid BackColor="#000000" HorizontalAlign="Center" CellSpacing="1" CellPadding="2" BorderWidth="0"
            ID="dg" runat="server" PageSize="20" CssClass="normalContent" AutoGenerateColumns="False"
            DataKeyField="DO_ID" AllowSorting="True" AllowPaging="True" ShowFooter="false">
            <PagerStyle CssClass="LinkNormal LightGray" Mode="NumericPages"></PagerStyle>
            <AlternatingItemStyle HorizontalAlign="Justify" CssClass="LinkNormal DarkGray"></AlternatingItemStyle>
            <ItemStyle HorizontalAlign="Justify" CssClass="LinkNormal LightGray"></ItemStyle>
            <HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
            <Columns>
                <asp:BoundColumn DataField="DO_ID" Visible="True"></asp:BoundColumn>
                <asp:BoundColumn DataField="DO_SCREEN" HeaderText="Screen Name" SortExpression="DO_SCREEN">
                    <HeaderStyle Width="120px"></HeaderStyle>
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Suggestion/Bug" ItemStyle-Width="250px" SortExpression="DO_DESC" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label CssClass="Content Color2" ID="lblDescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DO_DESC") %>'>
                        </asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox CssClass="InputForm" ID="txtDescription" runat="server" MaxLength="500"
                            Rows="5" TextMode="MultiLine" Columns="50" Text='<%# DataBinder.Eval(Container, "DataItem.DO_DESC") %>'>
                        </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Priority" SortExpression="DO_PRIORITY">
                    <ItemTemplate>
                        <asp:Label CssClass="Content Color2" ID="lblPriority" runat="server" align="middle"
                            Text='<%# DataBinder.Eval(Container, "DataItem.DO_PRIORITY") %>'>
                        </asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList CssClass="InputForm" ID="ddlPriority" runat="server" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.DO_PRIORITY") %>'>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                        </asp:DropDownList>
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Open Date" ItemStyle-Width="70px" SortExpression="OPEN_DATE">
                    <ItemTemplate>
                        <asp:Label CssClass="Content Color2" ID="Label1" runat="server" Width="60px" Text='<%# DataBinder.Eval(Container, "DataItem.OPEN_DATE", "{0:M/dd/yy}") %>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Close Date" ItemStyle-Width="70px" SortExpression="CLOSE_DATE">
                    <ItemTemplate>
                        <asp:Label CssClass="Content Color2" ID="lblCloseDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CLOSE_DATE", "{0:M/dd/yy}") %>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Status" SortExpression="DO_STATUS">
                    <ItemTemplate>
                        <asp:Label CssClass="Content Color2" ID="lblStatus" runat="server" Text='<%# ConvertStatus(DataBinder.Eval(Container, "DataItem.DO_STATUS")) %>'>
                        </asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="40px"></HeaderStyle>
                    <EditItemTemplate>
                        <asp:DropDownList CssClass="InputForm" ID="ddlStatus" runat="server" SelectedValue='<%# ConvertStatus(DataBinder.Eval(Container, "DataItem.DO_STATUS")) %>'>
                            <asp:ListItem Value="Open">Open</asp:ListItem>
                            <asp:ListItem Value="Closed">Closed</asp:ListItem>
                            <asp:ListItem Value="Pending">Pending</asp:ListItem>
                        </asp:DropDownList>
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="Opened_By" ItemStyle-Width="120px" HeaderText="Opened By" ReadOnly="True">
                    <HeaderStyle Width="100px"></HeaderStyle>
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Assigned To" ItemStyle-Width="120px" SortExpression="ASSIGNED_TO">
                    <ItemTemplate>
                        <asp:Label CssClass="Content Color2" ID="lblAssignedTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ASSIGNED_TO") %>'>
                        </asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="100px"></HeaderStyle>
                    <EditItemTemplate>
                        <asp:DropDownList CssClass="InputForm" ID="ddlAssignedTo" runat="server" DataTextField="ASSIGNED_TO"
                            DataValueField="ASSIGNED_TO_ID" DataSource="<%# DeveloperList() %>">
                        </asp:DropDownList>
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="DO_FILES" ItemStyle-Width="120px" HeaderText="Files Changed">
                    <HeaderStyle Width="100px"></HeaderStyle>
                </asp:BoundColumn>
                <asp:EditCommandColumn CancelText="Cancel" UpdateText="Update" EditText="Edit"></asp:EditCommandColumn>
                <asp:ButtonColumn CommandName="Close" Text="Close"></asp:ButtonColumn>
            </Columns>
        </asp:DataGrid>
</asp:Content>
