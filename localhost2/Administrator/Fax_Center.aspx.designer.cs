//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.42
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace localhost.Common {
    
    public partial class Fax_Center {
        protected System.Web.UI.WebControls.Label lblPageHeader;
        protected System.Web.UI.WebControls.Panel pnlCentralFax;
        protected System.Web.UI.WebControls.Label Label1;
        protected System.Web.UI.WebControls.Label lblMyFaxes;
        protected System.Web.UI.WebControls.Label lblSelectEmployee;
        protected System.Web.UI.WebControls.DropDownList ddlEmployee;
        protected System.Web.UI.WebControls.Button cmdPreviewFaxInbox;
        protected System.Web.UI.WebControls.Button btnDelete;
        protected System.Web.UI.WebControls.ListBox lstInbox;
        protected System.Web.UI.WebControls.Button btnAttachFaxUser;
        protected System.Web.UI.WebControls.Button btnDetachFaxUser;
        protected System.Web.UI.WebControls.Button cmdPreviewFaxEmployeeBox;
        protected System.Web.UI.WebControls.Button btnDeleteFaxMyBox;
        protected System.Web.UI.WebControls.ListBox lstEmployeebox;
        protected System.Web.UI.WebControls.Label Label3;
        protected System.Web.UI.WebControls.Label lblMessagePreview;
        protected System.Web.UI.WebControls.Label Label4;
        protected System.Web.UI.WebControls.Label Label5;
        protected System.Web.UI.WebControls.Label Label6;
        protected System.Web.UI.WebControls.DropDownList ddlOrders;
        protected System.Web.UI.WebControls.TextBox txtFaxDescription;
        protected System.Web.UI.WebControls.Button cmdMove;
        protected System.Web.UI.WebControls.Button cmdMoveShowFolder;
        public new localhost.MasterPages.Menu Master {
            get {
                return ((localhost.MasterPages.Menu)(base.Master));
            }
        }
    }
}
