<%@ Page Language="c#" CodeBehind="Demo_Promote.aspx.cs" AutoEventWireup="true" Inherits="localhost.Administrator.Demo_Promote" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<form runat="server" id="form1">
	<TPE:TEMPLATE id="Template1" Runat="server"></TPE:TEMPLATE><asp:panel id="pnlstep1" Runat="Server" Visible="True">
<TABLE align="center" border="0">
			<TR>
				<TD vAlign="top"><IMG src="../pics/icons/demostep1.gif" border="0">
					<TABLE width="250" border="0">
						<TR>
							<TD align="left"><STRONG>Person Info</STRONG></TD>
							<TD align="right"></TD>
						</TR>
						<asp:panel id="pnlMiss" Runat="Server" Visible="False">
							<TR>
								<TD>There are problems with the following fields. Please correct the errors below.<BR>
									<CENTER><FONT color="red">- First name can not be blank.<BR>
											- Last name can not be blank.<BR>
										</FONT>
									</CENTER>
								</TD>
							</TR>
						</asp:panel>
						<asp:panel id="pnlMissComp" Runat="Server" Visible="False">
							<CENTER><FONT color="red">- Company name can not be blank.<BR>
									<BR>
								</FONT>
								<CENTER>
						</asp:panel>
						<TR>
							<TD colSpan="2">
								<P>
									<TABLE>
										<TR>
											<TD>First Name:<BR>
												<asp:TextBox id="txtf" runat="server" BackColor="#FFFF99" size="10"></asp:TextBox></TD>
											<TD>Last Name:<BR>
												<asp:TextBox id="txtl" runat="server" BackColor="#FFFF99" size="10"></asp:TextBox></TD>
											<TD>Title:<BR>
												<asp:TextBox id="txttitle" runat="server" size="10"></asp:TextBox></TD>
										</TR>
									</TABLE>
									<BR>
									<TABLE>
										<TR>
											<TD>Phone:<BR>
												<asp:TextBox id="txtp" runat="server" size="10"></asp:TextBox></TD>
											<TD>Email:<BR>
												<asp:TextBox id="txte" runat="server" size="10"></asp:TextBox></TD>
											<TD>Fax:<BR>
												<asp:TextBox id="txtfax" runat="server" size="10"></asp:TextBox></TD>
										</TR>
									</TABLE>
									<BR>
								</P>
						<TR>
							<TD align="left"><STRONG>Company Info</STRONG></TD>
							<TD align="right"></TD>
						</TR>
						<BR>
						<TR>
							<TD colSpan="2">
								<P>
									<TABLE>
										<TR>
											<TD>Company Name:<BR>
											</TD>
											<TD>
												<asp:DropDownList id="ddlListComp" runat="server" AutoPostBack="True" OnSelectedIndexChanged="checkDropDown"></asp:DropDownList></TD>
											<TD>&nbsp;&nbsp;&nbsp;<B>
													<asp:Button id="Button1" onclick="changeToOther" runat="server" Text="Other.." CommandName="OtherCompagny"></asp:Button></B></TD>
											</B></TR>
									</TABLE>
									<BR>
									<asp:Panel id="panel1" Runat="server" Visible="False">
										<TABLE>
											<TR>
												<TD>I am interested in
												</TD>
												<TD>
													<asp:CheckBox id="txtbuying" runat="server"></asp:CheckBox><B>buying</B> resin
												</TD>
												<TD>
													<asp:CheckBox id="txtselling" runat="server"></asp:CheckBox><B>selling</B> resin
												</TD>
											</TR>
										</TABLE>
										<BR>
										<TABLE>
											<TR>
												<TD>Company Name:<BR>
													<asp:TextBox id="txtcname" runat="server" size="10" BackColor="#FFFF99"></asp:TextBox></TD>
												<TD>Website:<BR>
													<asp:TextBox id="txtsite" runat="server" size="10"></asp:TextBox></TD>
												<TD>Credit Request:<BR>
													<asp:TextBox id="txtcredit" runat="server" size="10"></asp:TextBox></TD>
											</TR>
										</TABLE>
										<BR>
										<TABLE>
											<TR>
												<TD colSpan="2">Address:<BR>
													<asp:TextBox id="txta1" runat="server" size="20" BackColor="#FFFF99"></asp:TextBox></TD>
												<TD>City:<BR>
													<asp:TextBox id="txtci" runat="server" size="10" BackColor="#FFFF99"></asp:TextBox></TD>
											</TR>
										</TABLE>
										<BR>
										<TABLE>
											<TR>
												<TD>Zip:<BR>
													<asp:TextBox id="txtzip" runat="server" size="10" BackColor="#FFFF99"></asp:TextBox></TD>
												<TD>State:<BR>
													<asp:DropDownList id="ddlstate" runat="server"></asp:DropDownList></TD>
												<TD>Country:<BR>
													<asp:DropDownList id="ddlCountry" runat="server"></asp:DropDownList></TD>
											</TR>
										</TABLE>
									</asp:Panel><BR>
								</P>
							</TD>
						</TR>
						<P></P>
				</TD>
			</TR>
		</TABLE>
      <P align="center">
			<asp:ImageButton id="imagebutton1" onclick="Cancel" runat="server" ImageUrl="../images/buttons/cancel.gif"
				ImageAlign="left" AlternateText="ImageButton 1"></asp:ImageButton>
			<asp:ImageButton id="imagebutton2" onclick="Continue" runat="server" ImageUrl="../images/buttons/continue.gif"
				ImageAlign="right" AlternateText="ImageButton 1"></asp:ImageButton></P></CENTER></CENTER></TD></TR></TABLE></asp:panel><asp:panel id="pnlstep2" Runat="Server" Visible="False">
		<TABLE align="center" border="0">
			<TBODY>
				<TR>
					<TD vAlign="top"><IMG src="/pics/icons/demostep2.gif" border="0">
						<TABLE width="250" border="0">
							<TR>
								<TD align="left"><STRONG>Bank Info</STRONG></TD>
								<TD align="right"></TD>
							</TR>
							<asp:panel id="panelBankErr" Runat="Server" Visible="False">
								<TR>
									<TD>There are problems with some of the following fields. Please correct the errors 
										below.<BR>
										<CENTER><FONT color="red">- Bank Name can not be blank.<BR>
												- Account can not be blank.<BR>
												- Repetitive can not be blank.<BR>
												- ABA can not be blank.<BR>
											</FONT>
											<CENTER></CENTER>
										</CENTER>
									</TD>
								</TR>
							</asp:panel>
							<TR>
								<TD colSpan="2">
									<P>
										<TABLE>
											<TR>
												<TD>Bank Name:<BR>
													<asp:TextBox id="txtbanme" runat="server" size="10" BackColor="#FFFF99"></asp:TextBox></TD>
												<TD>Contact
													<BR>
													<asp:TextBox id="txtbcontact" runat="server" size="10"></asp:TextBox></TD>
												<TD>Phone:<BR>
													<asp:TextBox id="txtbphone" runat="server" size="10"></asp:TextBox></TD>
											</TR>
										</TABLE>
										<BR>
										<TABLE>
											<TR>
												<TD>Fax:<BR>
													<asp:TextBox id="txtBfax" runat="server" size="10"></asp:TextBox></TD>
												<TD colSpan="2">Address:<BR>
													<asp:TextBox id="txtBadd" runat="server" size="20"></asp:TextBox></TD>
											</TR>
										</TABLE>
										<BR>
										<TABLE>
											<TR>
												<TD>City:<BR>
													<asp:TextBox id="txtBcity" runat="server" size="10"></asp:TextBox></TD>
												<TD>State:<BR>
													<asp:DropDownList id="ddlstate2" runat="server"></asp:DropDownList></TD>
												<TD>Country:<BR>
													<asp:DropDownList id="ddlCountry2" runat="server"></asp:DropDownList></TD>
												<TD>Zip<BR>
													<asp:TextBox id="txtzip2" runat="server" size="10"></asp:TextBox></TD>
											</TR>
										</TABLE>
										<BR>
										<TABLE>
											<TR>
												<TD>Account #:<BR>
													<asp:TextBox id="txtBacc" runat="server" size="10" BackColor="#FFFF99"></asp:TextBox></TD>
												<TD>Repetitive #:<BR>
													<asp:TextBox id="txtbre" runat="server" size="10"></asp:TextBox></TD>
												<TD>ABA #<BR>
													<asp:TextBox id="txtaba" runat="server" size="10" BackColor="#FFFF99"></asp:TextBox></TD>
											</TR>
										</TABLE>
									</P>
								</TD>
							</TR>
						</TABLE>
						<P align="center">
							<asp:ImageButton id="imagebutton3" onclick="Continue2" runat="server" ImageUrl="../images/buttons/back.gif"
								ImageAlign="left" AlternateText="ImageButton 1"></asp:ImageButton>
							<asp:panel id="pnFinish" Runat="Server" Visible="True">
								<asp:ImageButton id="imagebutton4" onclick="Finish" runat="server" ImageUrl="../images/buttons/finish.gif"
									ImageAlign="right" AlternateText="ImageButton 1"></asp:ImageButton></P>
	</asp:panel>
	<asp:panel id="pnFinish_Bank" Runat="Server" Visible="False">
		<asp:ImageButton id="Imagebutton5" onclick="FinishBis_Bank" runat="server" ImageUrl="../images/buttons/finish.gif"
			ImageAlign="right" AlternateText="ImageButton 1"></asp:ImageButton>
		<P></P>
	</asp:panel></TD></TR></TBODY></TABLE></asp:panel>
	<TPE:TEMPLATE id="Template2" Runat="server" Footer="true"></TPE:TEMPLATE></form>
