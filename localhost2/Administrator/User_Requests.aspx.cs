
using System;
using System.Data.SqlClient;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for News_Template.
	/// </summary>
	public partial class User_Requests : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.PlaceHolder phActionButton;
	
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
//			Session["Id"] = 6075;
//			Session["Name"] = "Ander Andreani";

            Master.Width = "1300px";
            //Administrator and Creditor have access
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C") && ((string)Session["Typ"] != "L"))
			{
				Response.Redirect("/default.aspx");
			}


			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			if (!IsPostBack)
			{			
				LoadDdlDate();
				
				ViewState["Sort"] = "REQ_DATE_TIME ASC";
				Bind(ViewState["Sort"].ToString());			
			}
			
			lblDataChanged.Visible = false;
			pnlConfirm.Visible = false;
			
			dg.Enabled = true;
		}

		private void LoadDdlDate()
		{
			
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();

			string sSql = "EXEC spRequestGropuedByDate";
            
			SqlDataAdapter dadContent = new SqlDataAdapter(sSql,conn);			
			DataTable dtRequest = new DataTable();			
			dadContent.Fill(dtRequest);

			ddlDate.Items.Add("All dates");

			string month, day, year, nReq;
			for(int i=0; i < dtRequest.Rows.Count;i++)
			{
				day = dtRequest.Rows[i]["Day"].ToString();
				month = dtRequest.Rows[i]["Month"].ToString();
				year = dtRequest.Rows[i]["Year"].ToString();
				nReq = dtRequest.Rows[i]["nReq"].ToString();

				if(day.Length == 1)
					day = "0" + day;
				if(month.Length == 1)
					month = "0" + month;
				
				ddlDate.Items.Add(month + "/"+ day + "/" + year + " (" + nReq + ")");
			}

			ddlDate.SelectedIndex = 1;
		}
		
		private void Bind(string sortExpression)
		{
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();

			string sSql;
			if(ddlDate.SelectedValue == "All dates")
			{
				sSql = "Exec spUserRequests";
			}
			else
			{
				sSql = "Exec spUserRequests @RequestFromDate = '" + ddlDate.SelectedValue.Split(' ')[0] + "', "
										+" @RequestToDate = '" + Convert.ToDateTime(ddlDate.SelectedValue.Split(' ')[0]).AddDays(1).ToShortDateString() + "'";
			}
			

			SqlDataAdapter dadContent = new SqlDataAdapter(sSql,conn);			
			DataTable dtContent = new DataTable();
			dadContent.Fill(dtContent);
			DataView dv = dtContent.DefaultView;
			dv.Sort = sortExpression;
			dg.DataSource = dv;
			dg.DataBind();
			conn.Close();
		}

		int iLinkSetCount = 1;

		private void dg_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.AlternatingItem ||
				e.Item.ItemType == ListItemType.Item)
			{			
				FillBrokerDropDownList(e);

				//Puts the whole message of the request into a Tooltip
				BuildMessageToolTip(e);				
				
			}
			
		}

		// <summary>
		// Sorting function
		// </summary>
		public void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{
			string[] SortExprs;
			string[] oldSortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;
			//  Parse the sort expression - delimiter space
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
			
			oldSortExprs = r.Split(ViewState["Sort"].ToString()) ;
			
			ColumnToSort = SortExprs[0];
			
			// If a sort order is specified get it, else default is descending
			if (SortExprs[0] == oldSortExprs[0])
			{
				CurrentSearchMode = oldSortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, set Default
				NewSearchMode = SortExprs[1];
			}

			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;

			//Response.Write(" ------- newsort = " + NewSortExpr.ToString());

			// Figure out the column index
			int iIndex;
			iIndex = 0;

			switch(ColumnToSort.ToUpper())
			{
				case "REQ_NAME":
					iIndex = 0;
					break;

				case "REQ_COMPANY":
					iIndex = 3;
					break;

				case "REQ_LOCATION":
					iIndex = 4;
					break;

				case "OFFR_DENS":
					iIndex = 5;
					break;

				case "REQ_OFFER_ID":
					iIndex = 6;
					break;

				case "OFFR_PROD":
					iIndex = 7;
					break;

				case "OFFR_PRCE":
					iIndex = 8;
					break;

				case "OFFR_SIZE":
					iIndex = 9;
					break;

				case "OFFR_MELT":
					iIndex = 10;
					break;

			}

			dg.Columns[iIndex].SortExpression = NewSortExpr;			
			dg.CurrentPageIndex = 0;

			// Sort the data in new order
			ViewState["Sort"] = NewSortExpr;
			// reset dbase page to the first one whether the sorting changes
					
			Bind(NewSortExpr);
			dg.DataBind();
			
		}

		private void FillBrokerDropDownList(System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			string broker = (string)((DataRowView)e.Item.DataItem).Row["BROKER_ALLOCATED"];

			string idBroker = (string)((DataRowView)e.Item.DataItem).Row["REQ_PERSON_ID_ALLOCATED"].ToString();
				
			DropDownList list = (DropDownList)e.Item.FindControl("ddlBrokers");

			//				string[] options;
			if(broker == "")
			{
				list.Items.Add("Unclaimed");
				list.Items[0].Value = "NULL";

				list.Items.Add(Session["Name"].ToString().Split(' ')[0]);
				list.Items[1].Value = Session["Id"].ToString();

				//options = new string[]{ "Unclaimed", Session["Name"].ToString().Split(' ')[0]};
			}
			else if(broker == Session["Name"].ToString().Split(' ')[0])
			{
				//					options = new string[]{Session["Name"].ToString(), "Unclaimed"};
				//					list.DataSource = options;			
				//					list.DataBind();				

				list.Items.Add(Session["Name"].ToString().Split(' ')[0]);
				list.Items[0].Value = Session["Id"].ToString();

				list.Items.Add("Unclaimed");
				list.Items[1].Value = "NULL";
			}
			else 
			{
				//					options = new string[]{broker, "Unclaimed", Session["Name"].ToString().Split(' ')[0]};
				//					list.DataSource = options;			
				//					list.DataBind();	
			
				list.Items.Add(broker);
				list.Items[0].Value = idBroker;

				list.Items.Add("Unclaimed");
				list.Items[1].Value = "NULL";

				list.Items.Add(Session["Name"].ToString().Split(' ')[0]);
				list.Items[2].Value = Session["Id"].ToString();

			}
		}

		private void BuildMessageToolTip(System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			string strMessageActionBox = "";
			string strCompanyActionBox = "";
			
			//Creats the the content of the Message tooltip
			string strMessage = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "REQ_MESSAGE")).Replace("'"," ");
			strMessage = strMessage.Replace("\r\n", "<BR>");
			strMessageActionBox += "linkset["+iLinkSetCount.ToString()+"]='<div class=\"menuitems\">"+ strMessage +"</div>'" + ((char)13).ToString();
			phActionButton.Controls.Add (new LiteralControl(strMessageActionBox));


//			string strEmail = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "REQ_EMAIL"));
//			string strPhone = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "REQ_PHONE"));
//			
//			strCompanyActionBox += "companylinkset["+iLinkSetCount.ToString()+"]='<div class=\"menuitems\">"+ strPhone +"</div>'" + ((char)13).ToString();
//			strCompanyActionBox += "companylinkset["+iLinkSetCount.ToString()+"]+='<hr>' //Optional Separator" + ((char)13).ToString();
//			strCompanyActionBox += "companylinkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=mailto:" + strEmail + ">" + strEmail +"</a></div>'" + ((char)13).ToString();
//			phActionButton.Controls.Add (new LiteralControl(strCompanyActionBox));
			
//			strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<hr>' //Optional Separator" + ((char)13).ToString();
//			strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=1\">Invoice</a></div>'" + ((char)13).ToString();
//			strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=2\">Purchase Order</a></div>'" + ((char)13).ToString();
//			strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<hr>' //Optional Separator" + ((char)13).ToString();
//			strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=3\">Certificate</a></div>'" + ((char)13).ToString();
//			strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=4\">Sale Confirmation</a></div>'" + ((char)13).ToString();
//			strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/hauler/Freight_Company.aspx?ID="+strID+"\">Freight R.F.Q</a></div>'" + ((char)13).ToString();
//			//strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/administrator/Send_Documents.aspx?ID="+strID+"&DocumentType=6\">Title Transfer</a></div>'" + ((char)13).ToString();
				
			//if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "VARWGHT")).ToString() == "190000")
			//{
//			strActionBoxHTML += "linkset["+iLinkSetCount.ToString()+"]+='<div class=\"menuitems\"><a href=\"/common/Convert_Order.aspx?ID="+strID+"\">Convert to 4 BT</a></div>'" + ((char)13).ToString();
			//}

			

			
			// setting the mouseover color
			e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");
			if (e.Item.ItemType == ListItemType.Item)
			{
				e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee'");
			}
			else
			{
				e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3'");
			}
			
			//Binds the tooltip to the column containing the truncated message
			e.Item.Cells[9].Attributes.Add("onmouseover","showmenu(event,linkset["+iLinkSetCount.ToString()+"])");
			e.Item.Cells[9].Attributes.Add("onMouseout", "delayhidemenu()");
//
//			e.Item.Cells[4].Attributes.Add("onmouseover","showmenu(event,companylinkset["+iLinkSetCount.ToString()+"])");
//			e.Item.Cells[4].Attributes.Add("onMouseout", "delayhidemenu()");
//			e.Item.Cells[5].Attributes.Add("onmouseover","showmenu(event,companylinkset["+iLinkSetCount.ToString()+"])");
//			e.Item.Cells[5].Attributes.Add("onMouseout", "delayhidemenu()");
			
			iLinkSetCount ++;
		}

		protected void DropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			DropDownList list = (DropDownList)sender;
			
			TableCell cell = list.Parent as TableCell;
			DataGridItem item = cell.Parent as DataGridItem;

			//TextBoxTitle = CType(item.FindControl("REQ_ID"), TextBox)

			string idReq = item.Cells[0].Text;
//			string broker = ((DataRowView)dg.Items[item.ItemIndex].DataItem.Cells.[0].DataItem).Row["BROKER_ALLOCATED"].ToString();
//	
			Session["idReq"] = idReq;
			Session["ddlSelectedText"] = list.SelectedItem.Text;
			Session["ddlIdOriginalBroker"] = list.Items[0].Value;
			if(list.Items.Count == 3)
			{
				pnlConfirm.Visible = true;						
			}
			else if(list.Items.Count == 2)
			{
				if(list.SelectedItem.Text == "Unclaimed" && list.Items[0].Text != Session["Name"].ToString().Split(' ')[0])
				{
					pnlConfirm.Visible = true;
					
				}
				else
				{
					string idBroker;
					if(Session["ddlSelectedText"].ToString() == "Unclaimed")
						idBroker = "NULL";
					else
						idBroker = Session["Id"].ToString();
					UpdateBrokerAllocated(idReq, idBroker);
					Bind(ViewState["Sort"].ToString());
					dg.DataBind();
				}
			}
			else throw new Exception("Something wrong");		
			
//
//			int index = item.ItemIndex;
//			string content = item.Cells[0].Text;
//
//			Response.Write(
//				String.Format("Row {0} contains {1}", index, content)
//				);
   
		}

		protected void DropDownDate_SelectedIndexChanged(object sender, EventArgs e)
		{
			Bind(ViewState["Sort"].ToString());		
			dg.DataBind();
		}

		private void UpdateBrokerAllocated(string idRequest, string idBroker)
		{
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			
			
			string strSQL;

			if(Session["ddlIdOriginalBroker"].ToString() == "NULL")
				strSQL = "SELECT REQ_ID FROM USER_REQUEST WHERE REQ_PERSON_ID_ALLOCATED IS NULL" +
					" AND REQ_ID = " + idRequest;
			else
				strSQL = "SELECT REQ_ID FROM USER_REQUEST WHERE REQ_PERSON_ID_ALLOCATED = " + Session["ddlIdOriginalBroker"].ToString() + 
					" AND REQ_ID = " + idRequest;

			SqlCommand cmd_UpdateBroker = new SqlCommand(strSQL, conn);
			if(cmd_UpdateBroker.ExecuteScalar() == null)
			{
				pnlDataChanged.Visible = true;
				lblDataChanged.Visible = true;						
				Bind(ViewState["Sort"].ToString());
				return;
				//throw new Exception("Data have changed please refresh");
			}

			strSQL = "UPDATE USER_REQUEST SET REQ_PERSON_ID_ALLOCATED = " + idBroker + 
							" ,REQ_DATE_TIME_ALLOCATED = GETDATE()" +
							" WHERE REQ_ID = " + idRequest;
				
			cmd_UpdateBroker = new SqlCommand(strSQL, conn);
			cmd_UpdateBroker.ExecuteNonQuery();
			conn.Close();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{   
			this.dg.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.SortDG);
			this.dg.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dg_ItemDataBound);

		}
		#endregion

	

		private void btnBack_Click(object sender, System.EventArgs e)
		{
			pnlConfirm.Visible = false;
			
			Bind(ViewState["Sort"].ToString());		
			dg.DataBind();
		}

		private void pnlDataChanged_Load(object sender, System.EventArgs e)
		{
		
		}

		protected void btnSubmit_Click_1(object sender, System.EventArgs e)
		{
			pnlConfirm.Visible = false;
												
			string idBroker;
			if(Session["ddlSelectedText"].ToString() == "Unclaimed")
				idBroker = "NULL";
			else
				idBroker = Session["Id"].ToString();

			UpdateBrokerAllocated(Session["idReq"].ToString(), idBroker);
			Bind(ViewState["Sort"].ToString());
			dg.DataBind();
		}
						
	}
}
