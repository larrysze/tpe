using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Select_Front_Month.
	/// </summary>
	public partial class Select_Front_Month : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			
			//only adminstrator can access this page.
            if (Session["Typ"].ToString() != "A" && Session["Typ"].ToString() != "L")
				Response.Redirect("/default.aspx");

			//Database connection'
			if (!IsPostBack)
			{
			
				using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
				{
					conn.Open();

                    //Currently months are added to FWDMONTH table manually with FWD_ACTIV=1
					string strSQL = "SELECT TOP 2 FWD_ID, FWD_MNTH+' '+CONVERT(varchar,FWD_YEAR) AS MONTH FROM FWDMONTH WHERE FWD_ACTV=1 ORDER BY FWD_ID ASC";
					using (SqlDataReader dtrMonth = DBLibrary.GetDataReaderFromSelect(conn,strSQL))
					{
						if (dtrMonth.Read())
						{
							lblMonth.Text = dtrMonth["MONTH"].ToString();
							ViewState["Date_Id"] = dtrMonth["FWD_ID"].ToString();
							//dtrMonth.Close();
							//conn.Close();
						}
						else  
						{
                            //May be we need to INSERT
							Response.Write("Error: Out of front months.  Please <a href\"webmaster@theplasticsexchange.com\" email tech</a>  to fix. ");
							Response.End();
						}
					}
				}
			}
		}
		
		protected void btnYes_Click(object sender, System.EventArgs e)
		{
			Hashtable htParam = new Hashtable();
			htParam.Add("@Month",ViewState["Date_Id"].ToString());
			DBLibrary.ExecuteStoredProcedure(Application["DBconn"].ToString(),"spUpdFront",htParam);
                			
			Response.Redirect("/Forward/Admin_Forward_Floor.aspx");
		}
		protected void Click_Submit(object sender, System.EventArgs e)
		{
			pnMain.Visible = false;
			pnConfirm.Visible = true;
		}

		protected void btnNo_Click(object sender, System.EventArgs e)
		{	
			pnMain.Visible = true;
			pnConfirm.Visible = false;
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		

	

		
	}
}
