using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Text;
using TPE.Utility;

namespace localhost
{
	/// <summary>
	/// Summary description for Order_Documents.
	/// </summary>
	public partial class Edit_User : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.HyperLink Hyperlink1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl FileDetails;
		protected System.Web.UI.HtmlControls.HtmlGenericControl FileName;
		protected System.Web.UI.HtmlControls.HtmlGenericControl MyContentType;
		protected System.Web.UI.HtmlControls.HtmlGenericControl ContentLength;
		protected System.Web.UI.WebControls.ListBox ListBox1;
		protected System.Web.UI.WebControls.ListBox ListBox2;
		protected System.Web.UI.WebControls.ListBox Inbox;
		protected System.Web.UI.WebControls.ListBox Employeebox;
		protected System.Web.UI.WebControls.ListBox ddlInbox;
		protected System.Web.UI.WebControls.ListBox ddlEmployeebox;
		protected System.Web.UI.WebControls.HyperLink del;

		protected void Page_Load(object sender, System.EventArgs e)
		{
            Master.Width = "750px";
			if (!IsPostBack)
			{
				//only adminstrator and borker can access this page.
//				if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B"))
//				{
//					Response.Redirect("../default.aspx");
//				}
				
				if (!IsPostBack)
				{
					Bind_Controls();
				}
			}
		}

		public void Bind_Controls()
		{
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			Hashtable param = new Hashtable();
			SqlDataReader dtr;
			// Binding Company Info
			param.Add("@CompID",Int32.Parse(Request.QueryString["CompanyID"].ToString()));
			try
			{
				conn.Open();
				dtr = DBLibrary.GetDataReaderFromSelect(conn, "Select COMP_NAME from COMPANY Where COMP_ID=@CompID", param);
				dtr.Read();
				lblCompanyID.Text = Request.QueryString["CompanyID"].ToString();
				lblCompanyName.Text = HelperFunction.getSafeStringFromDB(dtr["COMP_NAME"]);
				dtr.Close();
			}
			finally 
			{
				conn.Close();
			}
				
			lblUserID.Text = Request.QueryString["UserID"].ToString();
			if (lblUserID.Text=="0")
			{
				//Action: AddNew
				lblAction.Text = "Add New User";
				BindType(lblCompanyID.Text);
			}
			else
			{
				//Action: Update
				lblAction.Text = "Update User";
				//Binding User Details
				param.Clear();
				param.Add("@PersID",lblUserID.Text);
				try
				{
					conn.Open();

					dtr = DBLibrary.GetDataReaderFromSelect(conn,"select * from PERSON where PERS_ID=@PersID", param);
					dtr.Read();
					if ((string)dtr["PERS_TYPE"] == "O") ddlType.Items.Add("Officer");
					if ((string)dtr["PERS_TYPE"] == "P") ddlType.Items.Add("Purchaser");
					if ((string)dtr["PERS_TYPE"] == "S") ddlType.Items.Add("Supplier");
					ddlType.Enabled = false;
					

					txtFirstName.Text = HelperFunction.getSafeStringFromDB(dtr["PERS_FRST_NAME"]);;
					txtLastName.Text = HelperFunction.getSafeStringFromDB(dtr["PERS_LAST_NAME"]);
					txtTitle.Text = HelperFunction.getSafeStringFromDB(dtr["PERS_TITL"]);
					txtPhone.Text = HelperFunction.getSafeStringFromDB(dtr["PERS_PHON"]);
					txtFax.Text = HelperFunction.getSafeStringFromDB(dtr["PERS_FAX"]);
					txtEmail.Text = HelperFunction.getSafeStringFromDB(dtr["PERS_MAIL"]);
					lblOriginalEmail.Text = HelperFunction.getSafeStringFromDB(dtr["PERS_MAIL"]);
					txtLOGN.Text = HelperFunction.getSafeStringFromDB(dtr["PERS_LOGN"]);
					txtPSWD.Text = Crypto.Decrypt(HelperFunction.getSafeStringFromDB(dtr["PERS_PSWD"]));

					if (HelperFunction.getSafeStringFromDB(dtr["PERS_COMP"])=="148")
					{
						txtPSWD.Enabled = false;
						lblPassword.Visible = false;
						txtPSWD.Visible = false;
						txtLOGN.Enabled = false;	
					}
				}
				finally
				{
					conn.Close();
				}
			}
		}

		public void BindType(string companyID)
		{
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			Hashtable param = new Hashtable();
			param.Add("@CompanyID",companyID);
			//Get type of company.
			string compType="";
			SqlDataReader dtReader;
			try
			{
				conn.Open();;
				dtReader = DBLibrary.GetDataReaderFromSelect(conn,"SELECT COMP_TYPE FROM COMPANY WHERE COMP_ID=@CompanyID", param);
				param.Clear();
				param.Add("@CompanyID",companyID);
				while(dtReader.Read())
				{
					compType = dtReader["COMP_TYPE"].ToString();
				}
			}
			finally
			{
				conn.Close();	
			}
		
			//check if there is already an officer.
			int foundOfficer=0;
			try
			{
				conn.Open();
				param.Clear();
				param.Add("@CompanyID",companyID);
				dtReader = DBLibrary.GetDataReaderFromSelect(conn,"SELECT PERS_ID FROM PERSON WHERE PERS_COMP=@CompanyID AND PERS_TYPE='O'", param);
				while(dtReader.Read())
				{
					foundOfficer = 1;
				}
			}
			finally
			{
				conn.Close();
			}

			if(foundOfficer == 0)
			{
				ddlType.Items.Add(new ListItem ("Officer","O"));
			}

			if(companyID == "148")
			{
				ddlType.Items.Add(new ListItem ("Purchaser","P"));
				ddlType.Items.Add(new ListItem ("Supplier","S"));
				ddlType.Items.Add(new ListItem ("Admin","A"));
				ddlType.Items.Add(new ListItem ("Broker","B"));
			}
			else if(compType=="D")
			{
				ddlType.Items.Add(new ListItem ("Purchaser","P"));
				ddlType.Items.Add(new ListItem ("Supplier","S"));
			}
			else if(compType=="P")
			{
				ddlType.Items.Add(new ListItem ("Purchaser","P"));
			}
			else if(compType=="S")
			{
				ddlType.Items.Add(new ListItem ("Supplier","S"));
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.CustomValidator1.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(this.IsDuplicate);

		}
		#endregion

		protected void IsDuplicate(object source, ServerValidateEventArgs args)
		{
			if (txtEmail.Text.Trim()!=lblOriginalEmail.Text.Trim())
			{
				args.IsValid = !HelperFunction.ExistEmail(this.Context, txtEmail.Text.Trim(), lblUserID.Text=="0" ? "": lblUserID.Text);
			}
		}

		protected void cmdSave_Click(object sender, System.EventArgs e)
		{
			if (Page.IsValid)
			{
				Hashtable param = new Hashtable();
					
				if (lblUserID.Text!="0")
				{	//Update User Information
					//string strSQL = "UPDATE PERSON SET PERS_FRST_NAME='"+txtFirstName.Text+"', PERS_LAST_NAME='"+.Text+"', PERS_TITL='"+.Text+"', PERS_PHON='"++"', PERS_FAX='"+txtFax.Text+"', PERS_MAIL='"+txtEmail.Text.Trim()+"',PERS_LOGN='"+ txtLOGN.Text + "',PERS_PSWD='"+Crypto.Encrypt(txtPSWD.Text)+"' Where PERS_ID=" + lblUserID.Text;
					string strSQL = "UPDATE PERSON SET PERS_FRST_NAME=@FirstName, PERS_LAST_NAME=@LastName, PERS_TITL=@Title, PERS_PHON=@Phone, PERS_FAX=@Fax, PERS_MAIL=@Email,PERS_LOGN=@LOGN,PERS_PSWD=@Password Where PERS_ID=@UserID";
					param.Add("@FirstName",txtFirstName.Text.ToString());
					param.Add("@LastName",txtLastName.Text.ToString());
					param.Add("@Title",txtTitle.Text.ToString());
					param.Add("@Phone",txtPhone.Text.ToString());
					param.Add("@Fax",txtFax.Text.ToString());
					param.Add("@Email",txtEmail.Text.Trim());
					param.Add("@LOGN",txtLOGN.Text.ToString());
					param.Add("@Password",Crypto.Encrypt(txtPSWD.Text));
					param.Add("@UserID",lblUserID.Text);
					
					DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), strSQL, param);
					cmdClose_Click(sender, e);
				}
				else
				{	//Insert a new user
					//Check if this user name already in database.
					if (!ExistUser(txtLOGN.Text))
					{	
						//string strSQL = "INSERT INTO PERSON (PERS_COMP,PERS_TYPE,PERS_LAST_NAME,PERS_FRST_NAME,PERS_TITL,PERS_PHON,PERS_FAX,PERS_MAIL, PERS_PSWD, PERS_DATE,PERS_ENBL,PERS_REG, PERS_LOGN, PERS_LAST_CHKD, PERS_ALIA, PERS_PREF) VALUES("+lblCompanyID.Text+",'"+ddlType.SelectedItem.Value+"','"+txtLastName.Text+"','"+txtFirstName.Text+"','"+txtTitle.Text+"','"+txtPhone.Text+"','"+txtFax.Text+"','"+txtEmail.Text+"','"+ Crypto.Encrypt(txtPSWD.Text) +"',getdate(),1,1,'"+txtLOGN.Text+"',getdate(), Null,268435455)";
						string strSQL = "INSERT INTO PERSON (PERS_COMP,PERS_TYPE,PERS_LAST_NAME,PERS_FRST_NAME,PERS_TITL,PERS_PHON,PERS_FAX,PERS_MAIL, PERS_PSWD, PERS_DATE,PERS_ENBL,PERS_REG, PERS_LOGN, PERS_LAST_CHKD, PERS_ALIA, PERS_PREF, PERS_ACCOUNT_DISABLED) VALUES(@CompanyID,@Type,@LastName,@FirstName,@Title,@Phone,@Fax,@Email,@Password,getdate(),1,1,@LOGN,getdate(), Null,268435455,'False')";
						param.Clear();
						param.Add("@CompanyID",lblCompanyID.Text);
						param.Add("@Type",ddlType.SelectedItem.Value);
						param.Add("@LastName",txtLastName.Text);
						param.Add("@FirstName",txtFirstName.Text);
						param.Add("@Title",txtTitle.Text);
						param.Add("@Phone",txtPhone.Text);
						param.Add("@Fax",txtFax.Text);
						param.Add("@Email",txtEmail.Text);
						param.Add("@Password",Crypto.Encrypt(txtPSWD.Text));
						param.Add("@LOGN",txtLOGN.Text);
						
						DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), strSQL, param);
						cmdClose_Click(sender, e);
					}
				}
			}
		}

		private bool ExistUser(string strLogin)
		{
			bool bExist = false;
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());

			//Check if this user name already in database.
			//System doesn't allow duplicate user name!
			try
			{
				conn.Open();
				Hashtable param = new Hashtable();
				param.Add("@Login",strLogin);
				string sql = "Select PERS_LOGN from Person where PERS_LOGN=@Login";
				if (lblUserID.Text!="0") 
				{
					sql += " and PERS_ID <> @UserID";
					param.Add("@UserID",lblUserID.Text);
				}
				SqlDataReader dtReader = DBLibrary.GetDataReaderFromSelect(conn,sql,param);
				if (dtReader.Read())
				{
					lblMessage.Text="This username is already exist in our database!";
					bExist = true;
				}
				dtReader.Close();
			}
			finally
			{
				conn.Close();
			}
			return bExist;
		}

		protected void cmdClose_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("../administrator/Company_Info.aspx?Id="+lblCompanyID.Text+"");
		}
	}
}