using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Configuration;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Email_Offers.
	/// </summary>
	public class Email_Offers : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblContent;
		protected System.Web.UI.WebControls.Button btnSend;
		protected System.Web.UI.WebControls.Label lblBidOffers;
		protected System.Web.UI.WebControls.TextBox txtBody;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			Session["Name"] = "Zach Simmons";


			if (!IsPostBack)
			{
				StringBuilder sbText = new StringBuilder();
				sbText.Append("Dear <PERSON FIRST NAME>, "+((char)(13)).ToString()+((char)(13)).ToString());
				if (Session["strCompId"] != null)
				{
					lblBidOffers.Text = "Bids";
					sbText.Append("We are looking for the following resin today.  Please let us know if you have something close.");
				}
				else
				{
					lblBidOffers.Text = "Offers";
					sbText.Append("Based on the resin preferences that you provided upon registration, we are sending you the following resin offers."+((char)(13)).ToString()+((char)(13)).ToString());
					sbText.Append("Please call or reply to this email if we can serve you.  Hurry, they are subject to prior sale!"+((char)(13)).ToString()+((char)(13)).ToString());
				}
				sbText.Append("Sincerely,"+((char)(13)).ToString()+((char)(13)).ToString());
				sbText.Append(Session["Name"].ToString()+((char)(13)).ToString());
				sbText.Append("<a href='http://" + ConfigurationSettings.AppSettings["DomainName"] + "'>The Plastics Exchange</a>"+((char)(13)).ToString());
				sbText.Append("800.850.2380");
				txtBody.Text = sbText.ToString();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
