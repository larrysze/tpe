<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="Admin_Glossary.aspx.cs" Inherits="localhost.Administrator.Admin_Glossary" Title = "Glossary Admin" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master"%>
<%@ MasterType virtualPath="~/MasterPages/TemplateWithoutInstructions.Master"%>
    <asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphHeading">
        <span class="Header Bold Color1">Glossary Admin</span>
    </asp:Content>
    
    <asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">
	<asp:datagrid id="dg" DataKeyField="word" BorderWidth="0px" BackColor="Black" 
	    CellSpacing="1" CssClass="Content LinkNormal" runat="server" 
	    Width="100%" CellPadding="4" AutoGenerateColumns="False">
		<AlternatingItemStyle CssClass="LinkNormal Color2 DarkGray">
		</AlternatingItemStyle>
        <ItemStyle CssClass="LinkNormal Color2 LightGray">
        </ItemStyle>
        <HeaderStyle CssClass="LinkNormal Color2 OrangeColor">
        </HeaderStyle>
		<Columns>
			<asp:EditCommandColumn UpdateText="Update"  CancelText="Cancel" EditText="Edit">
			</asp:EditCommandColumn>
			<asp:ButtonColumn Text="Delete" CommandName="Delete">
			</asp:ButtonColumn>
            <asp:TemplateColumn HeaderText="Word">
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.word") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox CssClass="InputForm" runat="server" 
                        Text='<%# DataBinder.Eval(Container, "DataItem.word") %>'>
                    </asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateColumn>
			<asp:TemplateColumn HeaderText="Definition">
				<ItemTemplate>
					<asp:Label ID="lblDefinition" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.definition") %>'>
					</asp:Label>
				</ItemTemplate>
				<EditItemTemplate>
					<asp:TextBox ID="txtDefinition" CssClass="InputForm" runat="server" 
					    MaxLength="500" Rows="5" TextMode="MultiLine" Columns="70" 
					    Text='<%# DataBinder.Eval(Container, "DataItem.definition")%>'>
					</asp:TextBox>
				</EditItemTemplate>
                <ItemStyle HorizontalAlign="Left" />
			</asp:TemplateColumn>	
		</Columns>
	</asp:datagrid>
	<asp:Button ID="Button1" runat="server" CssClass = "Content Color2"
            Text="Add" Height="23px" Width="79px" OnClick="Button1_Click">
    </asp:Button>
    <asp:TextBox ID="word" CssClass="InputForm" runat="server" Height="16px" Width="151px">
    </asp:TextBox>
    <asp:TextBox ID="definition" CssClass="InputForm" runat="server" Height="16px" Width="379px">
    </asp:TextBox>
    <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red">
    </asp:Label>
</asp:Content>
        

