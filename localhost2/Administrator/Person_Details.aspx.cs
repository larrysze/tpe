using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace localhost.Administrator
{
	/// <summary>
	/// Summary description for Person_Details.
	/// </summary>
	public class Person_Details : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.PlaceHolder phBody;
		protected System.Web.UI.WebControls.Label lblType;
		protected System.Web.UI.WebControls.TextBox txtFname;
		protected System.Web.UI.WebControls.TextBox txtLname;
		protected System.Web.UI.WebControls.TextBox txtTitle;
		protected System.Web.UI.WebControls.TextBox txtPhone;
		protected System.Web.UI.WebControls.TextBox txtFAX;
		protected System.Web.UI.WebControls.TextBox txtMAIL;
		protected System.Web.UI.WebControls.Label txtLOGN;
		protected System.Web.UI.WebControls.TextBox txtPSWD;
		protected System.Web.UI.WebControls.Button Button1;
	
		/************************************************************************
	*   1. File Name       :Administrator\Person_Details.aspx               *
	*   2. Description     :View and update user info, access from "Update_Company_Info.aspx"*
	*   3. Modification Log:                                                *
	*     Ver No.       Date          Author             Modification       *
	*   -----------------------------------------------------------------   *
	*      1.00      2-22-2004      Zach                First Baseline      *
	*                                                                       *
	************************************************************************/
		SqlConnection conn;
		public void Page_Load(object sender, EventArgs e)
		{
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
    
			if (!IsPostBack)
			{
				SqlDataReader dtrPerson;
				SqlCommand cmdPerson;
				//Check the current person, display type of person based on PERS_TYPE
				//O:Officer, P: Purchaser, S: Supplier
             
				cmdPerson = new SqlCommand("select * from PERSON where PERS_ID='"+(string)Request.QueryString["ID"]+"'",conn);
				dtrPerson = cmdPerson.ExecuteReader();
    
             
				dtrPerson.Read();
    
				if ( (string)dtrPerson["PERS_TYPE"] == "O")
				{
					lblType.Text="Officer";
    
				}
				if ( (string)dtrPerson["PERS_TYPE"] == "P")
				{
					lblType.Text="Purchaser";
    
				}
				if ( (string)dtrPerson["PERS_TYPE"] == "S")
				{
					lblType.Text="Supplier";
				}
				if ( dtrPerson["PERS_FRST_NAME"] != DBNull.Value)
				{
					txtFname.Text = (string)dtrPerson["PERS_FRST_NAME"];
				}
    
				if ( dtrPerson["PERS_LAST_NAME"] != DBNull.Value)
				{
					txtLname.Text = (string)dtrPerson["PERS_LAST_NAME"];
				}
				if ( dtrPerson["PERS_TITL"] != DBNull.Value)
				{
					txtTitle.Text = (string)dtrPerson["PERS_TITL"];
				}
       
				if ( dtrPerson["PERS_PHON"] != DBNull.Value)
				{
					txtPhone.Text = (string)dtrPerson["PERS_PHON"];
				}
				if ( dtrPerson["PERS_FAX"] != DBNull.Value)
				{
					txtFAX.Text = (string)dtrPerson["PERS_FAX"];
				}
    
				if ( dtrPerson["PERS_MAIL"] != DBNull.Value)
				{
					txtMAIL.Text = (string)dtrPerson["PERS_MAIL"];
				}
    
    
				if ( dtrPerson["PERS_LOGN"] != DBNull.Value)
				{
					txtLOGN.Text = (string)dtrPerson["PERS_LOGN"];
				}
    
    
				if ( dtrPerson["PERS_PSWD"] != DBNull.Value)
				{
					txtPSWD.Text = (string)Crypto.Decrypt(dtrPerson["PERS_PSWD"].ToString());
				}
      
    
				// window should regain focus if users clicks off
				//phBody.Controls.Add (new LiteralControl("<body onblur=\"window.focus()\">"));
				phBody.Controls.Add (new LiteralControl("<body>"));
			}
		}
		protected void IsDuplicate(object source, ServerValidateEventArgs args)
		{
			SqlConnection conn;
			SqlCommand cmd;
			SqlDataReader dtr;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			cmd= new SqlCommand("SELECT PERS_LOGN FROM PERSON WHERE PERS_MAIL='"+txtMAIL.Text+"' AND PERS_ID<>'"+Request.QueryString["ID"]+"' ", conn);
			dtr = cmd.ExecuteReader();
			if (dtr.HasRows)
			{
				// found same Username exist already!
				args.IsValid = false;
			}
			conn.Close();
		}
		public void Click_Save(object sender, EventArgs e)
		{
			if (Page.IsValid)
			{
				// window should close after saving
				string strSQL;
				string Id;
				Id = Request.QueryString["Id"];
         
				//perform update
				strSQL="UPDATE PERSON SET PERS_FRST_NAME='"+txtFname.Text+"', PERS_LAST_NAME='"+txtLname.Text+"', PERS_TITL='"+txtTitle.Text+"', PERS_PHON='"+txtPhone.Text+"', PERS_FAX='"+txtFAX.Text+"', PERS_MAIL='"+txtMAIL.Text+"',PERS_LOGN='"+txtLOGN.Text+"' Where PERS_ID=@Cid";
				SqlCommand cmdUpdate;
				cmdUpdate= new SqlCommand(strSQL, conn);
				cmdUpdate.Parameters.Add("@Cid", Id);
				cmdUpdate.ExecuteNonQuery();
				phBody.Controls.Add (new LiteralControl("<body onload=\"window.close()\">"));
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
