<%@ Page language="c#" Codebehind="Contact_Details.aspx.cs" AutoEventWireup="True" Inherits="localhost.Administrator.Contact_Details" MasterPageFile="~/MasterPages/Menu.Master" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">

<TABLE cellSpacing="2" cellPadding="2" width="100%" border="0">
		<TR>
			<TD vAlign="top">
				<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<TD align="left"><span class="Header Color2 Bold"><br />Lead History<br /><br /></span></TD>
					</TR>
					<TR>
						<TD colSpan="2">
						<asp:datagrid BorderWidth="0" CellSpacing="1" BackColor="#000000" id="dg" runat="server" OnItemDataBound="db_ItemDataBound" HorizontalAlign="Center" CellPadding="2" AutoGenerateColumns="False" ShowHeader="False" onprerender="dg_PreRender">
								<AlternatingItemStyle CssClass="LinkNormal Bold LightGray"></AlternatingItemStyle>
								<ItemStyle CssClass="LinkNormal DarkGray" HorizontalAlign="Left"></ItemStyle>
								<HeaderStyle CssClass="LinkNormal OrangeColor"></HeaderStyle>
								<Columns>
									<asp:ButtonColumn Text="Delete" ButtonType="PushButton" CommandName="Delete"></asp:ButtonColumn>
									<asp:BoundColumn DataField="CMNT_DATE" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}">
										<ItemStyle Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="CMNT_TEXT" HeaderText="Comment"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="CMNT_EMAIL_SYSTEM_ID"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="CMNT_ID"></asp:BoundColumn>
								</Columns>
							</asp:datagrid></TD>
					</TR>
					<TR>
						<TD colSpan="2" class="Header Color2 Bold"><B>New Note:</B>
						</TD>
					</TR>
					<TR>
						<TD vAlign="top" align="left" colSpan="2">
						<asp:textbox CssClass="InputForm" Width="200px" id="txtComments" runat="server" align="center" TextMode="MultiLine" Rows="2" Columns="30"></asp:textbox></TD>
					</TR>
					<TR>
						<TD align="left"><span class="Content">Rate Lead:</span>
							<asp:dropdownlist CssClass="InputForm" id="ddlGrade" runat="server"></asp:dropdownlist></TD>
						<td align="right"><asp:button CssClass="Content Color2" id="btnAddNote" runat="server" CausesValidation="False" Text="Add Note" onclick="btnAddNote_Click"></asp:button></td>
					</TR>
					<TR>
						<TD class="Header Bold Color2" align="center" colSpan="2" height="23">Schedule Next 
							Contact</TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2">
							<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0" class="LinkNormal">
								<TR>
									<TD align="center"><asp:calendar id="Calendar1" runat="server" onDayRender="MyCalendar_DayRender"></asp:calendar></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="left"><span class="Header Bold Color2"><br />Regarding:<br /><br /></span>
										<asp:textbox Width="200px" CssClass="InputForm" id="txtBody" runat="server" TextMode="MultiLine" Rows="2" Columns="30"></asp:textbox></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="right"><asp:button CssClass="Content Color2" id="btnSubmit" runat="server" CausesValidation="False" Text="Schedule"></asp:button></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
			</TD>
			<TD vAlign="top">
				<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR> <!-- primera tabela -->
						<TD>
							<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD class="Header Color2 Bold" align="left" colSpan="4"><br />User Information<br />
									</TD>
								</TR>
								<tr>
									<td colSpan="4"><br>
									</td>
								</tr>
								<TR>
									<TD class="Content" style="text-align:left">First Name:
									</TD>
									<TD><asp:textbox CssClass="InputForm" id="txtFirstName" runat="server"></asp:textbox></TD>
									<TD class="Content" style="text-align:left">Last Name:
									</TD>
									<TD><asp:textbox CssClass="InputForm" id="txtLastName" runat="server"></asp:textbox></TD>
								</TR>
								<TR>
									<TD class="Content" style="text-align:left">Title:
									</TD>
									<TD><asp:textbox CssClass="InputForm" id="txtTitle" runat="server"></asp:textbox></TD>
									<TD><asp:label CssClass="Content" id="lblCompanyName" runat="server">Company:</asp:label></TD>
									<TD><asp:textbox CssClass="InputForm" id="txtCompanyName" runat="server"></asp:textbox></TD>
								</TR>
								<TR>
									<TD class="Content" style="text-align:left">Phone:
									</TD>
									<TD><asp:textbox CssClass="InputForm" id="txtPhone" runat="server"></asp:textbox></TD>
									<TD class="Content" style="text-align:left">Password:
									</TD>
									<TD><asp:textbox CssClass="InputForm" id="txtPassword" runat="server"></asp:textbox></TD>
								</TR>
								<TR>
									<TD class="Content" valign="middle"><asp:label CssClass="Content" id="lblUsernameLable" runat="server" Visible="False">Username:</asp:label>
									</TD>
									</tr>
									<tr>
									<td class="Content" valign="middle">Email:<asp:label id="lblUsername" runat="server" Visible="False"></asp:label></td>
									<TD colSpan="2" class="Content" valign="middle" style="text-align:left">
									<asp:textbox CssClass="InputForm" id="txtEmail" runat="server" Width="230px"></asp:textbox>
									<asp:label CssClass="Content" id="lblOldEmail" runat="server" Visible="False"></asp:label><asp:label CssClass="Content" id="lblMessage" runat="server" Visible="False" ForeColor="Red"></asp:label><asp:regularexpressionvalidator id="EmailValidator" runat="server" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
											ErrorMessage="Invalid E-mail!"></asp:regularexpressionvalidator></TD>
								</tr>
								<TR>
									<TD colSpan="4" class="Content" style="text-align:left"><span class="Content Bold">Accepts Emails:</span>
										<asp:checkbox CssClass="Content" id="cbEmailSubscriber" runat="server"></asp:checkbox><br>
										<span class="Content Color2 Bold">Broker:</span>
										<asp:label CssClass="Content" id="lblBroker" runat="server"></asp:label></TD>
								</TR>
								<tr>
									<TD align="center" colSpan="4">
										<table width="100%">
											<tr>
												<td colSpan="2" class="Header Color2 Bold" style="text-align:left"><br />Delivery:<br />
												</td>
											</tr>
											<tr>
												<td><asp:radiobutton CssClass="Content" id="rdbInsideUS" runat="server" Text="Domestic (zip)" GroupName="Interest" Checked="True"></asp:radiobutton></td>
												<td><asp:radiobutton CssClass="Content" id="rdbOutsideUS" runat="server" Text="International (port)" GroupName="Interest"></asp:radiobutton></td>
											</tr>
											<tr>
												<td><asp:textbox CssClass="InputForm" id="txtZip" runat="Server"></asp:textbox></td>
												<td><asp:dropdownlist CssClass="InputForm" id="ddlPort" runat="server"></asp:dropdownlist></td>
											</tr>
										</table>
									</TD>
								</tr>
								<TR>
									<TD align="center" colSpan="4"><BR>
										<P align="left"><span class="Header Color2 Bold">Primarily interested in:<br /></span>&nbsp;&nbsp;&nbsp;</P>
									</TD>
								</TR>
							</TABLE>
							<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0" class="Content">
								<TR>
									<TD width="15">&nbsp;
									</TD>
									<TD width="55" height="1">
										<P align="left"><b>Sizes:</b></P>
									</TD>
									<TD>
										<P align="left"><asp:checkbox id="chkSize1" runat="server" Text="Rail Cars"></asp:checkbox></P>
									</TD>
									<TD>
										<P align="left"><asp:checkbox id="chkSize2" runat="server" Text="Bulk Trucks"></asp:checkbox></P>
									</TD>
									<TD>
										<P align="left"><asp:checkbox id="chkSize3" runat="server" Text="Truckload Boxes\Bags"></asp:checkbox></P>
									</TD>
								</TR>
								<TR>
									<TD width="15"></TD>
									<TD width="55"></TD>
									<TD></TD>
									<TD>
										<P align="left">&nbsp;</P>
									</TD>
									<TD>
										<P align="left">&nbsp;</P>
									</TD>
								</TR>
								<TR>
									<TD width="15">&nbsp;
									</TD>
									<TD width="55">
										<P align="left"><b>Quality:</b></P>
									</TD>
									<TD>
										<P align="left"><asp:checkbox id="chkQuality1" runat="server" Text="Prime"></asp:checkbox></P>
									</TD>
									<TD>
										<P align="left"><asp:checkbox id="chkQuality2" runat="server" Text="Good Offgrade"></asp:checkbox></P>
									</TD>
									<TD>
										<P align="left"><asp:checkbox id="chkQuality3" runat="server" Text="Regrind/Repro"></asp:checkbox></P>
									</TD>
								</TR>
							</TABLE>
						</TD>
					</TR> <!-- segunda tabela -->
					<TR>
						<TD><BR>
							<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td vAlign="top">
										<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" class="LinkNormal" style="text-align:left">
											<TR>
												<TD><B>List of Resins&nbsp;
														<asp:label id="lblResinCount" runat="server"></asp:label></B>&nbsp;
													<asp:hyperlink id="hlEditResins" runat="server">(Edit)</asp:hyperlink></TD>
											</TR>
											<TR>
												<TD><asp:repeater id="rptResins" runat="server">
														<ItemTemplate>
															<asp:CheckBox id="CheckBoxResin" runat="server"></asp:CheckBox>
															<asp:Label id="lblGradeID" runat="server" Visible="False"></asp:Label>
														</ItemTemplate>
													</asp:repeater></TD>
											</TR>
										</TABLE>
										&nbsp;
										<asp:Button CssClass="Content" id="btnSendEmail" runat="server" Text="Email Offers" onclick="btnSendEmail_Click"></asp:Button>
									</td>
									<td vAlign="top">
										<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" class="LinkNormal" style="text-align:left">
											<tr>
												<td><b>User History</b></td>
											</tr>
											<tr>
												<td>Registered:
													<asp:label id="lblDateRegistered" runat="server"></asp:label></td>
											</tr>
											<tr>
												<td>Last Login:
													<asp:label id="lblLastLogin" runat="server"></asp:label></td>
											</tr>
											<tr>
												<td>Total Logins:
													<asp:label id="lblTotalLogin" runat="server"></asp:label></td>
											</tr>
										</TABLE>
									</td>
								</tr>
							</TABLE>
							<br>
							<br>
						</TD>
					</TR>
					<tr>
						<td align="right"><asp:button CssClass="Content" id="btnUpdate" runat="server" Text="Update" onclick="btnUpdate_Click"></asp:button><asp:button CssClass="Content" id="btnCancel" runat="server" CausesValidation="False" Text="Cancel"></asp:button></td>
					</tr>
					<!-- fechando tudo --></TABLE>
			</TD>
		</TR>
	</TABLE>
</asp:Content>