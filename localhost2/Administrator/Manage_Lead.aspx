<%@ Page Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="Manage_Lead.aspx.cs" Inherits="localhost.Administrator.Manage_Lead" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCssLink" runat="server">
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="cphMain" runat="server">
<table align="center" width="780" cellpadding="0">
	<tr>
	    <td align="left" style="width: 531px; height: 19px;">
		    <span class="Content Bold Color2">Move To:</span>
			<asp:DropDownList CssClass="InputForm" id="ddlMoveTo" runat="server"></asp:DropDownList>
		</td>		    
	</tr>
	<tr>
		<td align="left" style="width: 531px; height: 37px">
			<table>
				<tr align="left">
					<td><span class="Content Color2 Bold">Name</span></td>
				</tr>
				<tr>
				    <td align="center"><asp:PlaceHolder id="phAllocate" runat="server"></asp:PlaceHolder></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" style="width: 770px">
			<asp:Button class="tpebutton" id="Button8" onclick="Submit_Allocate" runat="server" Text="Allocate"></asp:Button>
			<asp:Button class="tpebutton" id="Button9" onclick="Cancel_Allocate" runat="server" Text="Cancel"></asp:Button>
		</td>
	</tr>
</table>

</asp:Content>
