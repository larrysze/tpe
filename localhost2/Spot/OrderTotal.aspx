<%@ Page language="c#" Codebehind="OrderTotal.aspx.cs" AutoEventWireup="True" Inherits="localhost.Spot.OrderTotal" MasterPageFile="~/MasterPages/Menu.Master" Title="Order Total" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">

<style type="text/css">
.table { BACKGROUND-COLOR: #333333 }
</style>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphJavaScript">

<script language="javascript">
	function OpenPdf()
	{
		window.open("/docs/Credit_App.pdf");
	}
</script>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">

<table class="InstructionTable">
				<tr>
					<td>
						<TABLE id="Table1" borderColor="black" cellSpacing="0" cellPadding="0" width="747" align="center"
							border="0">
							<TR> <!--<td align="left"><span class="PageHeader">Total / Method of Payment</span>
					</td>-->
								<TD class="Header Color2 Bold" align="center"><br /><br />
									<P><asp:label id="lblTitle1" runat="server">1. Shipping Information</asp:label></P><br />
								</TD>
								<TD class="Header Color1 Bold" align="center"><br /><br />
									<P><asp:label id="lblTitle2" runat="server">2. Credit Application</asp:label></P><br />
								</TD>
								<TD class="Header Color2 Bold" align="center"><br /><br />
									<P><asp:label id="lblTitle3" runat="server">3. Order Confirmation</asp:label></P><br />
								</TD>
							</TR>
						</TABLE>
					</td>
				</tr>
			</table>
			<TABLE id="Table22" borderColor="black" cellSpacing="0" cellPadding="0" width="780" align="center"
				border="0">
				<tbody>
					<TR>
						<TD vAlign="top" align="left">
							<TABLE bgcolor="#333333" id="Table121" width="100%" align="center" border="0" cellSpacing=0 cellPadding=0>
								<TR>
									<TD vAlign="top" align="left" colSpan="3">
										<TABLE id="Table42" cellSpacing="0" cellPadding="0" width="100%" border="0" class="table">
											<TBODY>
												<TR>
													<TD>
														<p align="left"><br />
															<table cellSpacing="0" cellPadding="0" width="760" align="center" border="0">
																<tr vAlign="top">
																	<td style="HEIGHT: 82px" bgColor="#999999"><SPAN class="Content Color2"><b>Shipping adress:</b><br />
																			<P style="padding-left:4px"><asp:label id="LblShip" runat="server" Width="121px"></asp:label></P>
																		</SPAN>
																	</td>
																	<td style="HEIGHT: 82px" bgColor="#999999"><SPAN class="Content Color2"><b>Billing address:</b><br />
																			<br>
																			<P><asp:label id="LblBill" runat="server" Width="121px"></asp:label></P>
																		</SPAN>
																	</td>
																	<td style="HEIGHT: 82px" align="center" valign="middle" bgColor="#999999">
																		<table cellSpacing="1" border="0" style="background-color:Black">
																			<tr>
																				<td style="FONT: 8pt ARIAL" width="100" bgColor="#cccccc">P.O. #</td>
																				<td style="FONT: 8pt ARIAL" align="left" bgColor="#999999"><asp:textbox CssClass="InputForm" id="txtPO" Runat="server" Width="100px"></asp:textbox></td>
																			</tr>
																			<tr>
																				<td style="FONT: 8pt ARIAL" width="100" bgColor="#cccccc">Transaction #</td>
																				<td style="FONT: 8pt ARIAL" align="left" bgColor="#999999"><asp:label id="lblID" runat="server" ForeColor="#C00000"></asp:label></td>
																			</tr>
																			<tr>
																				<td style="FONT: 8pt ARIAL" bgColor="#cccccc">Transaction Date</td>
																				<td style="FONT: 8pt ARIAL" align="left" bgColor="#999999"><% Response.Write(DateTime.Today.ToShortDateString()); %></td>
																			</tr>
																		</table>
																	</td>
																<tr vAlign="top">
																	<td align="center" colSpan="3"><br>
																		<table cellSpacing="1" cellPadding="1" width="100%" border="0">
																			<TR>
																				<td class="Content Color2 Bold" bgColor="#dbdcd7">Size</td>
																				<td class="Content Color2 Bold" bgColor="#dbdcd7">Product</td>
																				<td class="Content Color2 Bold" bgColor="#dbdcd7">Melt</td>
																				<td class="Content Color2 Bold" bgColor="#dbdcd7">Dens</td>
																				<td class="Content Color2 Bold" bgColor="#dbdcd7">Adds</td>
																				<td class="Content Color2 Bold" bgColor="#dbdcd7">Terms</td>
																				<td class="Content Color2 Bold" bgColor="#dbdcd7">Price</td>
																				<td class="Content Color2 Bold" bgColor="#dbdcd7">Frieght</td>
																			</TR>
																			<tr>
																				<td  bgColor="#999999"><asp:label CssClass="Content Color2" id="lblSize" runat="server"></asp:label></td>
																				<td  bgColor="#999999"><asp:label CssClass="Content Color2" id="lblProduct" runat="server" Width="111px" ></asp:label></td>
																				<TD  bgColor="#999999"><asp:label CssClass="Content Color2" id="lblMelt" runat="server" Width="111px" ></asp:label></TD>
																				<td  bgColor="#999999"><asp:label CssClass="Content Color2" id="lblDens" runat="server" Width="111px" ></asp:label></td>
																				<TD  bgColor="#999999"><asp:label CssClass="Content Color2" id="lblAdds" runat="server" ></asp:label></TD>
																				<td  bgColor="#999999"><asp:label CssClass="Content Color2" id="lblTerms" runat="server" ></asp:label></td>
																				<td  bgColor="#999999"><asp:label CssClass="Content Color2" id="lblPrice" runat="server" ></asp:label></td>
																				<td  bgColor="#999999"><asp:label CssClass="Content Color2" id="lblFreight" runat="server"></asp:label></td>
																			</tr>
																		</table>
																		<asp:label id="lblTotal" runat="server" Visible="False">&nbsp;</asp:label><asp:label id="TotPrice" runat="server" Visible="False">&nbsp;</asp:label><asp:label id="lblTax" Runat="server"></asp:label></td>
																</tr>
																<TR>
																	<TD align="center" colSpan="3"><asp:label id="lblPOMessage" runat="server" Font-Bold="True" ForeColor="Red" Visible="False"
																			CssClass="blackbold">P.O. # needs to be entered in order to proceed</asp:label></TD>
																</TR>
															</table>
															<br>
														</p>
													</TD>
												</TR>
												<TR>
													<TD align="center" colSpan="3" class="LinkNormal Color4">
													<asp:checkbox CssClass="Content Color4" id="cbAgreement" runat="server" visible="true" Text="I read and agree with the "></asp:checkbox>
													<a href="javascript:void window.open('Agreement.aspx', 'Agreement', 'toolbar=false, scrollbars=1')"><span class="Color4">Conditions of Sales Agreement</span></a>
													</TD>
												</TR>
												<tr>
													<td align="center" colSpan="5"><asp:label id="lblConfirmError" Runat="server" Visible="False" CssClass="Content Color2 Bold">
													<span class="ErrorText">You have to read and agree with Conditions of Sales in order to proceed</span></asp:label></td>
												</tr>
								</TR>
							</TABLE>
							<br />
							<asp:panel id="pnlCreditOn" runat="server" Height="144px" BackColor="#333333">
							<div class="DivTitleBarMenu"><span class="Header Color1 Bold">Method of Payment</span></div><br />
            <TABLE class=table id=Table3 borderColor=black height=108 cellSpacing=1 cellPadding=1 width=760 align=center border=0>
              <TR>
                <TD bgColor=#999999 align="center"><span class="Content Bold Color2">Submit Credit Application online.</span></TD>
                <TD bgColor=#999999 align="center"><span class="Content Bold Color2">I prefer to fax my info.</span></TD></TR>
              <TR>
                <TD align="center" width=400 bgColor=#999999>
<asp:Button id=btnFaxOnline runat="server" CssClass="Content Color2" Text="Use Online App" onclick="btnFaxOnline_Click"></asp:Button></TD>
                <TD align="center" bgColor=#999999>
<asp:Button id=btnFaxCredit runat="server" CssClass="Content Color2" Text="Fax Credit App(PDF)" onclick="btnFaxCredit_Click"></asp:Button></TD></TR></TABLE>
							</asp:panel>
							<asp:panel id="pnlCreditOff" runat="server" Width="744px" Height="40px">
							
            <TABLE id=Table4 borderColor=black height=32 align=center border=0>
              <TR>
                <TD align=center>&nbsp; 
<asp:Button id=btnGoBack runat="server" CssClass="Content Color2" Text="    Back   " onclick="btnGoBack_Click"></asp:Button>
<asp:Button id=btnContinue runat="server" CssClass="Content Color2" Width="80px" Text=" Confirm" onclick="btnContinue_Click"></asp:Button></TD></TR></TABLE>
							</asp:panel></TD>
					</TR>
				</tbody>
			</TABLE>

</asp:Content>