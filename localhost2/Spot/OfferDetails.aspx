<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page language="c#" Codebehind="OfferDetails.aspx.cs" AutoEventWireup="false" Inherits="localhost.Spot.OfferDetails" %>
<form id="Inquire2" runat="server">
	<TPE:TEMPLATE id="Template3" Runat="Server" PageTitle="Offer Details"></TPE:TEMPLATE><asp:panel id="pnMain" runat="server">
		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
			<TR>
				<TD align="left"><SPAN class="PageHeader">Offer Details</SPAN>
					<BR>
					<BR>
					<BR>
					<BR>
					<FONT color="red"></FONT>
				</TD>
			</TR>
			<TR>
				<TD>
					<TABLE height="71" width="752" border="0">
						<TR>
							<TD width="342"><FONT color="black" size="2" align="left"><B>Product: </B></FONT><STRONG>
									<asp:label id="lblProduct" runat="server" ForeColor="#C00000"></asp:label>
									<asp:Label id="lblID" runat="server"></asp:Label></STRONG></TD>
							<TD width="233"><FONT color="black" size="2" align="left"><B>Size: </B></FONT><STRONG>
									<asp:label id="lblSize" runat="server" ForeColor="#C00000" width="184px"></asp:label></STRONG></TD>
						</TR>
						<TR>
							<TD width="342"><FONT color="black" size="2" align="left"><B>Melt: </B></FONT><STRONG>
									<asp:label id="lblMelt" runat="server" ForeColor="#C00000" width="40"></asp:label></STRONG></TD>
							<TD width="233"><FONT color="black" size="2" align="left"><B>Density: </B></FONT><STRONG>
									<asp:label id="lblDens" runat="server" ForeColor="#C00000" width="40"></asp:label></STRONG></TD>
							<TD><FONT color="black" size="2" align="left"><B>Adds: </B></FONT><STRONG>
									<asp:label id="lblAdds" runat="server" ForeColor="#C00000" width="112px"></asp:label></STRONG></TD>
						</TR>
						<TR>
							<TD width="342"><FONT color="black" size="2" align="left"><B>Order Terms: </B></FONT>
								<STRONG>
									<asp:label id="lblTerms" runat="server" ForeColor="#C00000"></asp:label></STRONG></TD>
							<TD width="233"><FONT color="black" size="2" align="left"><B>Price: </B></FONT><STRONG>
									<asp:label id="lblPrice" runat="server" ForeColor="#C00000" width="40"></asp:label></STRONG></TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
			<TR>
				<TD>
					<TABLE width="600" align="center" bgColor="#eeeeee" border="0">
						<TR>
							<TD width="100%">
								<CENTER><FONT color="black" size="2" align="left"><B>If you are interested on this offer 
											please contact us at 1-800-850-2380 or ask a question below and somebody will 
											get back to you quickly. </B></FONT>
								</CENTER>
							</TD>
						<TR>
							<TD align="center">&nbsp;
								<asp:TextBox id="txtComments" runat="server" TextMode="MultiLine" Rows="5" Columns="55" Width="395px"></asp:TextBox>
								<asp:Button id="btnSendQuery" onclick="Click_Submit" runat="server" Text="Send Question"></asp:Button></TD>
						</TR>
						<BR>
						<BR>
						<TR>
							<TD>
								<CENTER><INPUT id="btnBack" onclick="history.back()" type="button" value="    Back    ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<asp:Button id="btnSubmit" runat="server" Width="122px" Text="Buy Online"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
								</CENTER>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
	</asp:panel><asp:panel id="pnThanks" runat="server" Visible="False">
		<CENTER><BR>
			<BR>
			<BR>
			Thank you. We will be contacting you shortly.<BR>
			<BR>
			<BR>
		</CENTER>
	</asp:panel><TPE:TEMPLATE id="Template2" Runat="Server" Footer="true"></TPE:TEMPLATE></form>
