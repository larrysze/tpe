<%@ Page language="c#" Codebehind="ThankYou.aspx.cs" AutoEventWireup="True" Inherits="PlasticsExchange.ThankYou" MasterPageFile="~/MasterPages/Menu.Master" Title="Thank You"%>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>

<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
<style type="text/css">
.Center {text-align:center}
</style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
<div class="Content Color2 Center">
<br />
<p><b>Thank You.</b></p>
<p>Your order has completed.<br /></p>
<p>You will receive a confirmation email soon.</p>
<br />
</div>
</asp:Content>