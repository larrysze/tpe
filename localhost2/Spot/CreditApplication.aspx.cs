using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Web.Mail;
using System.Configuration;
using TPE.Utility;

namespace PlasticsExchange
{
	/// <summary>
	/// Summary description for CreditApplication.
	/// </summary>
	public partial class CreditApplication : System.Web.UI.Page
	{
		protected int CREDIT_ID;
		protected int ORD_SRNO;
		protected int USR_ID;
		protected int BIDOFFER_ID;
		private void FillPreTypedInfo(string TmpSId)
		{
			//Function for getting the shippingto address from ShippingDetails.aspx displayed in this page
			SqlConnection conn;
			string ShipAdd = String.Empty;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			SqlDataReader DR;
			
			StringBuilder SbSql = new StringBuilder();
			SbSql.Append("SELECT DISTINCT PLACE.PLAC_ID, PLACE.PLAC_PERS,");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ADDR_ONE is not Null or ltrim(rtrim(PLACE.PLAC_ADDR_ONE))<>'' ");
			SbSql.Append("then  PLACE.PLAC_ADDR_ONE else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CITY is not Null or ltrim(rtrim( PLACE.PLAC_CITY))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CITY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_STAT is not Null or ltrim(rtrim( PLACE.PLAC_STAT))<>'' ");
			SbSql.Append("then + PLACE.PLAC_STAT else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CTRY  is not Null or ltrim(rtrim( PLACE.PLAC_CTRY ))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CTRY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ZIP is not Null or ltrim(rtrim(PLACE.PLAC_ZIP))<>'' ");
			SbSql.Append("then PLACE.PLAC_ZIP else '' end ");
			SbSql.Append("FROM PLACE ");
			SbSql.Append("WHERE PLACE.PLAC_ID = '" + TmpSId + "'");
			
			SqlCommand Cmd = new SqlCommand(SbSql.ToString(),conn);
			DR = Cmd.ExecuteReader();
			if (DR.Read())
			{
				txtComp_Address.Text=DR.GetValue(2).ToString()  ;
				txtComp_City.Text =DR.GetValue(3).ToString()  ;
				
				ListItem li =ddlComp_Country.Items.FindByText(DR.GetValue(5).ToString())  ;
				ddlComp_Country.SelectedItem.Selected=false;
				if (li != null)
				{
					li.Selected=true;
				}


				FillCombos(ddlComp_State ,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE where STAT_CTRY='" + ddlComp_Country.SelectedValue.ToString() + "' order by STAT_NAME ASC");
				if (ddlComp_State.Items.Count>0)
				{
					txtState.Visible=false;
					ddlComp_State.Visible=true;
					ListItem li2 = ddlComp_State.Items.FindByText(DR.GetValue(4).ToString());
					ddlComp_State.SelectedItem.Selected=false;
					li2.Selected=true;
				}
				else
				{
					ddlComp_State.Visible=false;
					txtState.Visible=true;
					txtState.Text=DR.GetValue(4).ToString();
				}

				txtComp_Zip.Text=DR.GetValue(6).ToString()  ;
				
			}
			DR.Close();
			conn.Close();
			//shows the shipto address in label
			//Session["ID"].ToString()
			SqlConnection conn1;
			
			conn1 = new SqlConnection(Application["DBconn"].ToString());
			conn1.Open();
			SqlDataReader DRPerson;
			
			StringBuilder SbSqlPers = new StringBuilder();
			SbSqlPers.Append("SELECT DISTINCT  PERS_FRST_NAME, Person.PERS_LAST_NAME,");
			SbSqlPers.Append("CASE ");
			SbSqlPers.Append("when Person.PERS_TITL is not Null or ltrim(rtrim(Person.PERS_TITL))<>'' ");
			SbSqlPers.Append("then  Person.PERS_TITL else '' end, ");
			SbSqlPers.Append("CASE ");
			SbSqlPers.Append("when Person.PERS_PHON is not Null or ltrim(rtrim( Person.PERS_PHON))<>'' ");
			SbSqlPers.Append("then  Person.PERS_PHON else '' end, ");
			SbSqlPers.Append("CASE ");
			SbSqlPers.Append("when Person.PERS_FAX is not Null or ltrim(rtrim( Person.PERS_FAX))<>'' ");
			SbSqlPers.Append("then + Person.PERS_FAX else '' end, ");
			SbSqlPers.Append("CASE ");
			SbSqlPers.Append("when Person.PERS_MAIL  is not Null or ltrim(rtrim( Person.PERS_MAIL ))<>'' ");
			SbSqlPers.Append("then  Person.PERS_MAIL else '' end ");
			SbSqlPers.Append("FROM Person ");
			SbSqlPers.Append("WHERE Person.PERS_ID = '" + Session["ID"].ToString() + "'");
			
			SqlCommand CmdPers = new SqlCommand(SbSqlPers.ToString(),conn1);
			DRPerson = CmdPers.ExecuteReader();
			if (DRPerson.Read())
			{
				txtPers_Fname.Text=DRPerson.GetValue(0).ToString();
				txtPers_Lname.Text=DRPerson.GetValue(1).ToString();
				txtPers_Title.Text=DRPerson.GetValue(2).ToString();
				txtPers_Phone.Text=DRPerson.GetValue(3).ToString();
				txtPers_Fax.Text=DRPerson.GetValue(4).ToString();
				txtPers_Email.Text=DRPerson.GetValue(5).ToString();
			}
			DRPerson.Close();
			conn1.Close();

			
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//////////////////////amit
			if (Session["ID"] == null)
			{
				Response.Redirect("/default.aspx");
			}



			if (Session["Typ"].ToString() != "A") 
				//			if  (Session["UType"].ToString().Equals("D")|| Session["UType"].ToString().Equals("O")) 
			{
				pnlCredit.Visible=true;
				pnlapproveCredit.Visible=false;
				
				if (!IsPostBack )
				{
					
					// Adding Countries to the combo
					FillCombos(ddlComp_Country,"CTRY_NAME","CTRY_CODE","SELECT CTRY_NAME,CTRY_CODE From COUNTRY ORDER BY CTRY_NAME ASC");
					ListItem li1 = ddlComp_Country.Items.FindByValue("US");
					ddlComp_Country.SelectedItem.Selected=false;
					li1.Selected = true;

					// Adding States to dropdown list box
					//FillCombos(ddlComp_State ,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE where STAT_CTRY='US' order by STAT_NAME ASC");
					//ddlComp_State.SelectedIndex = 0;
					//li2.Selected = true;

					//Filling PreTyped Info
					FillPreTypedInfo(Session.Contents["ShipId"].ToString() );
					
				}
			}
			else
			{
				pnlapproveCredit.Visible=true;
				pnlCredit.Visible=false;
				
				CREDIT_ID =Convert.ToInt32(Request.QueryString["CREDIT_ID"].ToString());
				ViewState["CR_ID"]=Convert.ToInt32(Request.QueryString["CREDIT_ID"].ToString());
				ORD_SRNO =Convert.ToInt32(Request.QueryString["ORD_SRNO"].ToString());
				ViewState["ORD_SRNO"]=Convert.ToInt32(Request.QueryString["ORD_SRNO"].ToString());
				USR_ID =Convert.ToInt32(Request.QueryString["USER_ID"].ToString());
				ViewState["USR_ID"] =Convert.ToInt32(Request.QueryString["USER_ID"].ToString());
				BIDOFFER_ID =Convert.ToInt32(Request.QueryString["BIDOFFR_ID"].ToString());
				ViewState["BIDOFFER_ID"] =Convert.ToInt32(Request.QueryString["BIDOFFR_ID"].ToString());
	
				if (!IsPostBack )
				{
					//Adding countries to drop down list box
					FillCombos1(ddlComp_Country,"CTRY_NAME","CTRY_CODE","SELECT CTRY_NAME,CTRY_CODE From COUNTRY ORDER BY CTRY_NAME ASC");
					ListItem Li =  ddlComp_Country.Items.FindByValue("US")	;
					ddlComp_Country.SelectedItem.Selected=false;

					Li.Selected=true;
					//Adding States into dropdown combobox
					FillCombos1(ddlComp_State,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE where STAT_CTRY='US' order by STAT_NAME ASC");
					if (ddlComp_State.Items.Count>0)
					{
						ddlComp_State.SelectedIndex=0;
					}
					//Showing Information filled up in form				
					ShowCredit(CREDIT_ID);
				}
			}		
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

	
		
		private void FillCombos(DropDownList DDL,string TFld,string ValFld,string Query)
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			SqlDataReader DTR;
			SqlCommand Cmd = new SqlCommand();
			Cmd.CommandType=CommandType.Text;
			Cmd.CommandText=Query;
			
			conn.Open();
			Cmd.Connection=conn;
			DTR=Cmd.ExecuteReader();
			//DDL.SelectedItem.Selected=false;

			DDL.DataSource=DTR;
			DDL.DataTextField=TFld;
			DDL.DataValueField=ValFld;
			DDL.DataBind();
			DTR.Close();
			conn.Close();
			//			if (DDL.Items.Count>=1)
			//			{
			//				DDL.SelectedIndex=0;
			//			}
			//			else
			//			{
			//				DDL.SelectedIndex=-1;
			//			}
			
		}

		protected void ddlComp_Country_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillCombos(ddlComp_State ,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE where STAT_CTRY='" + ddlComp_Country.SelectedValue.ToString() + "' order by STAT_NAME ASC");
			if (ddlComp_State.Items.Count>0)
			{
				ddlComp_State.Visible=true;
				txtState.Visible=false;
			}
			else
			{
				ddlComp_State.Visible=false;
				txtState.Visible=true;
			}
		}

        private Hashtable SetParams(int ID,char Flag, char Approved)
        {
            Hashtable param = new Hashtable();
            param.Add("@Flag", Flag);
            param.Add("@ID", ID);
            param.Add("@Pers_Fname", txtPers_Fname.Text.ToString());
            param.Add("@Pers_Lname", txtPers_Lname.Text.ToString());
            param.Add("@Pers_Title", txtPers_Title.Text.ToString());
            param.Add("@Pers_Phone", txtPers_Phone.Text.ToString());
            param.Add("@Pers_Fax", txtPers_Fax.Text.ToString());
            param.Add("@Pers_Email", txtPers_Email.Text.ToString());
            param.Add("@Comp_Cname", txtComp_Cname.Text.ToString());
            param.Add("@Comp_Address", txtComp_Address.Text.ToString());
            param.Add("@Comp_City", txtComp_City.Text.ToString());
            param.Add("@Comp_Zip", txtComp_Zip.Text.ToString());

            //sbQuery.Append("spShowSaveCredit 'A',0,@Pers_Fname,@Pers_Lname,@Pers_Title,@Pers_Phone,");			
            //sbQuery.Append(  "@Pers_Fax,@Pers_Email,@Comp_Cname,@Comp_Address,@Comp_City,@Comp_Zip,@Comp_State");
            if (ddlComp_State.Visible)
            {
                param.Add("@Comp_State", ddlComp_State.SelectedItem.Text.ToString());
            }
            else
            {
                param.Add("@Comp_State", txtState.Text.ToString());
            }

            param.Add("@Comp_Country", ddlComp_Country.SelectedItem.Text.ToString());
            param.Add("@Bank_Bname", txtBank_Bname.Text.ToString());
            param.Add("@Bank_Contact", txtBank_Contact.Text.ToString());
            param.Add("@Bank_Phone", txtBank_Phone.Text.ToString());
            param.Add("@Bank_Fax", txtBank_Fax.Text.ToString());
            param.Add("@Trad1_Cname", txtTrad1_Cname.Text.ToString());
            param.Add("@Trad1_Contact", txtTrad1_Contact.Text.ToString());
            param.Add("@Trad1_Phone", txtTrad1_Phone.Text.ToString());
            param.Add("@Trad1_Fax", txtTrad1_Fax.Text.ToString());
            param.Add("@Trad2_Cname", txtTrad2_Cname.Text.ToString());
            param.Add("@Trad2_Contact", txtTrad2_Contact.Text.ToString());
            param.Add("@Trad2_Phone", txtTrad2_Phone.Text.ToString());
            param.Add("@Trad2_Fax", txtTrad2_Fax.Text.ToString());
            param.Add("@Trad3_Cname", txtTrad3_Cname.Text.ToString());
            param.Add("@Trad3_Contact", txtTrad3_Contact.Text.ToString());
            param.Add("@Trad3_Phone", txtTrad3_Phone.Text.ToString());
            param.Add("@Trad3_Fax", txtTrad3_Fax.Text.ToString());
            param.Add("@APPROVED", Approved);
            return param;
        }
		//Approved Credit application	
		protected void btnConfirm_Click(object sender, System.EventArgs e)
		{
			string TmpStr = string.Empty;
			//StringBuilder sbQuery = new StringBuilder();
            Hashtable param = SetParams(0,'A','P');
						
			Session["FrmName"]="Credit";
			Session["ParamQuery"] = param;

			//Response.Redirect("OrderConfirmation.aspx?PassVal="+ sbQuery.ToString() );
//Emails to Admin and Client

			//DoMail();

			Response.Redirect("OrderTotal.aspx");
			//Response.Redirect("OrderTotal.aspx?PassVal="+ sbQuery.ToString() );
		}
		private void DoMail()
		{
			StringBuilder sbHTML =  new StringBuilder() ;
			sbHTML.Append("<Table><TR><TD><h3>Online Credit Application form </h3></TD></TR>");
			sbHTML.Append("<tr><td align='left' colspan='2'><font size='3'><b>Online Credit Application Form has been filled by the customer. </b></font></td></tr></Table>");
			
			SqlConnection Conn =  new SqlConnection(Application["DBConn"].ToString());
			Conn.Open();
			SqlCommand SqlChkType = new SqlCommand("Select PERS_MAIL from person where PERS_ID=" + Convert.ToUInt16(Session["ID"]),Conn);
			string senderId = Convert.ToString(SqlChkType.ExecuteScalar());

			MailMessage EmailOrdr = new MailMessage();
			EmailOrdr.From = senderId;
			EmailOrdr.To = Application["strEmailAdmin"].ToString();
			EmailOrdr.Cc = Application["strEmailOwner"].ToString();
			EmailOrdr.Subject = "Customer Credit Application for the Order";
			EmailOrdr.Body = sbHTML.ToString();
			EmailOrdr.BodyFormat = MailFormat.Html;
			//SmtpMail.SmtpServer="caesar";
			//TPE.Utility.EmailLibrary.Send(EmailOrdr);
		}
		protected void btnBack_Click(object sender, System.EventArgs e)
		{
			//Response.Redirect("Total.aspx");
			Response.Redirect("OrderTotal.aspx");
		}
		
		///////////amit
		private void ShowCredit(int ParamCredit)
		{
			string TmpStr=string.Empty;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			SqlCommand Cmd = new SqlCommand();
			Cmd.CommandType=CommandType.StoredProcedure;
			Cmd.CommandText="spShowSaveCredit";     

            Cmd.Parameters.Add("@Pers_Fname", Convert.DBNull);
            Cmd.Parameters.Add("@Pers_Lname", Convert.DBNull);
            Cmd.Parameters.Add("@Pers_Title", Convert.DBNull);
            Cmd.Parameters.Add("@Pers_Phone", Convert.DBNull);
            Cmd.Parameters.Add("@Pers_Fax", Convert.DBNull);
            Cmd.Parameters.Add("@Pers_Email", Convert.DBNull);
            Cmd.Parameters.Add("@Comp_Cname", Convert.DBNull);
            Cmd.Parameters.Add("@Comp_Address", Convert.DBNull);
            Cmd.Parameters.Add("@Comp_City", Convert.DBNull);
            Cmd.Parameters.Add("@Comp_Zip", Convert.DBNull);
            Cmd.Parameters.Add("@Comp_State", Convert.DBNull);
            Cmd.Parameters.Add("@Comp_Country", Convert.DBNull);
            Cmd.Parameters.Add("@Bank_Bname", Convert.DBNull);
            Cmd.Parameters.Add("@Bank_Contact", Convert.DBNull);
            Cmd.Parameters.Add("@Bank_Phone", Convert.DBNull);
            Cmd.Parameters.Add("@Bank_Fax", Convert.DBNull);
            Cmd.Parameters.Add("@Trad1_Cname", Convert.DBNull);
            Cmd.Parameters.Add("@Trad1_Contact", Convert.DBNull);
            Cmd.Parameters.Add("@Trad1_Phone", Convert.DBNull);
            Cmd.Parameters.Add("@Trad1_Fax", Convert.DBNull);
            Cmd.Parameters.Add("@Trad2_Cname", Convert.DBNull);
            Cmd.Parameters.Add("@Trad2_Contact", Convert.DBNull);
            Cmd.Parameters.Add("@Trad2_Phone", Convert.DBNull);
            Cmd.Parameters.Add("@Trad2_Fax", Convert.DBNull);
            Cmd.Parameters.Add("@Trad3_Cname", Convert.DBNull);
            Cmd.Parameters.Add("@Trad3_Contact", Convert.DBNull);
            Cmd.Parameters.Add("@Trad3_Phone", Convert.DBNull);
            Cmd.Parameters.Add("@Trad3_Fax", Convert.DBNull);
            
			Cmd.Parameters.Add("@ID",SqlDbType.Int);
			Cmd.Parameters["@ID"].Value =ParamCredit; 
			//Cmd.Parameters["@ID"].Value =3725;//ParamCredit
			
			Cmd.Parameters.Add("@Flag",SqlDbType.Char);
			Cmd.Parameters["@Flag"].Value="S"; 

			Cmd.Parameters.Add("@APPROVED",SqlDbType.Char);
			Cmd.Parameters["@APPROVED"].Value = Convert.DBNull;// "A";
			SqlDataReader DR;
			conn.Open();
			Cmd.Connection=conn;
			DR=Cmd.ExecuteReader();


			while (DR.Read())
			{
				txtPers_Fname.Text= DR.GetValue(1).ToString();
				txtPers_Lname.Text= DR.GetValue(2).ToString();
				txtPers_Title.Text= DR.GetValue(3).ToString();
				txtPers_Phone.Text= DR.GetValue(4).ToString();
				txtPers_Fax.Text= DR.GetValue(5).ToString();
				txtPers_Email.Text= DR.GetValue(6).ToString();
				txtComp_Cname.Text = DR.GetValue(7).ToString();
				txtComp_Address.Text= DR.GetValue(8).ToString();
				txtComp_City.Text= DR.GetValue(9).ToString();
				txtComp_Zip.Text= DR.GetValue(10).ToString();

				//NEW CHANGES
				
				//ddlComp_Country.SelectedValue= DR.GetValue(12).ToString();
				ListItem li = ddlComp_Country.Items.FindByText(DR.GetValue(12).ToString());
				ddlComp_Country.SelectedItem.Selected=false;
				if ( !li.Selected)
				{
					li.Selected=true;
				}
				//FillCombos(ddlComp_State,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE where STAT_CTRY ='" + DR.GetValue(12).ToString() + "' order by STAT_NAME ASC");
				FillCombos(ddlComp_State,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE where STAT_CTRY ='" + li.Value.ToString() + "' order by STAT_NAME ASC");
				if (ddlComp_State.Items.Count>0 )
				{
					ddlComp_State.Visible=true;
					txtState.Visible=false;
					ListItem li1 = ddlComp_State.Items.FindByText(DR.GetValue(11).ToString());	
					ddlComp_State.SelectedItem.Selected=false;
					
					if ( !li1.Selected)
					{
						li1.Selected=true;
					}
				}
				else
				{
					//ddlComp_State.SelectedItem.Selected=false;
					ddlComp_State.Visible=false;
					txtState.Visible=true;
					txtState.Text= DR.GetValue(11).ToString();
				}
				//ddlComp_State.SelectedValue= DR.GetValue(11).ToString();

				txtBank_Bname.Text=DR.GetValue(13).ToString();
				txtBank_Contact.Text=DR.GetValue(14).ToString();
				txtBank_Phone.Text=DR.GetValue(15).ToString();
				txtBank_Fax.Text=DR.GetValue(16).ToString();

				txtTrad1_Cname.Text=DR.GetValue(17).ToString();
				txtTrad1_Contact.Text=DR.GetValue(18).ToString();
				txtTrad1_Phone.Text=DR.GetValue(19).ToString();
				txtTrad1_Fax.Text=DR.GetValue(20).ToString();

				txtTrad2_Cname.Text=DR.GetValue(21).ToString();
				txtTrad2_Contact.Text=DR.GetValue(22).ToString();
				txtTrad2_Phone.Text=DR.GetValue(23).ToString();
				txtTrad2_Fax.Text=DR.GetValue(24).ToString();

				txtTrad3_Cname.Text=DR.GetValue(25).ToString();
				txtTrad3_Contact.Text=DR.GetValue(26).ToString();
				txtTrad3_Phone.Text=DR.GetValue(27).ToString();
				txtTrad3_Fax.Text=DR.GetValue(28).ToString();
			}
			//Cmd.ExecuteNonQuery();
			
			//Cache["FrmName"]="Credit";
			DR.Close();

			//Response.Redirect("OrderConfirmation.aspx?ID="+ Session.Contents["ProductId"].ToString());
			conn.Close();
		}
		private void Submit1_ServerClick(object sender, System.EventArgs e)
		{
		
		}
		private void FillCombos1(DropDownList DDL,string TFld,string ValFld,string Query)
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			SqlDataReader DTR;
			SqlCommand Cmd = new SqlCommand();
			Cmd.CommandType=CommandType.Text;
			Cmd.CommandText=Query;
			
			conn.Open();
			Cmd.Connection=conn;
			DTR=Cmd.ExecuteReader();
			DDL.DataSource=DTR;
			DDL.DataTextField=TFld;
			DDL.DataValueField=ValFld;
			DDL.DataBind();
			DTR.Close();
			conn.Close();
//			if (DDL.Items.Count>=1)
//			{
//				DDL.SelectedIndex=0;
//			}
//			else
//			{
//				DDL.SelectedIndex=-1;
//			}
			
		}

		protected void btnDeny_Click(object sender, System.EventArgs e)
		{
			
			//Subroutine for denying the order approval
			SqlConnection conn;
			//////string strCmd ;
			conn = new SqlConnection(Application["DBConn"].ToString());
			SqlCommand Cmd = new SqlCommand();
			Cmd.CommandType=CommandType.Text;

			conn.Open();
			Cmd.Connection=conn;
			Cmd.CommandText = "Delete from Credit_App where Id=" + Convert.ToInt32(ViewState["CR_ID"].ToString());   //+ TmpCrId ;
			Cmd.ExecuteNonQuery();

			//			strCmd = "Update  Pending_Orders set ORDR_STAT='D'" ;
			//			strCmd +=  " where ordr_srno=" + ORD_SRNO + " and PERSON_ID = " + USR_ID + " AND ";
			//			strCmd +=  " (ORDR_BID = " + BIDOFFER_ID + " OR ORDR_OFFR = " + BIDOFFER_ID + ")";
			//Cmd.CommandText = strCmd;
			//conn.Open();
			Cmd.CommandText = "Delete from Pending_Orders where Credit_Id=" + Convert.ToInt32(ViewState["CR_ID"].ToString());   //+ TmpCrId ;
			Cmd.ExecuteNonQuery();

			Cmd.Connection=conn;
			Cmd.ExecuteNonQuery();
			DoApproveDeny(0);
			Response.Redirect("/Administrator/PendingTransactions.aspx");
			//*******************************/
			//		Add Mailing Code  Below //
			//*******************************/
			
		}

		protected void btnApprove_Click(object sender, System.EventArgs e)
		{
			string TmpStr=string.Empty;
            Hashtable param = SetParams(Convert.ToInt32(ViewState["CR_ID"].ToString()), 'E', 'A');

            int TmpCreditID = Convert.ToInt32(TPE.Utility.DBLibrary.ExecuteScalarStoredProcedure(Application["DBconn"].ToString(), "spShowSaveCredit", param));
            		
			if (Request.QueryString["CREDIT_ID"].ToString()=="0")
			{
                string sqlUpdate = "Update Pending_Orders Set Credit_Id=" + TmpCreditID.ToString() + " where ORDR_SRNO =" + Convert.ToInt32(Request.QueryString["ORD_SRNO"].ToString());
                TPE.Utility.DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sqlUpdate);
			}

	        string sqlGetOffr = "Select ORDR_OFFR from Pending_Orders where ORDR_SRNO =" + Convert.ToInt32(Request.QueryString["ORD_SRNO"].ToString());
            string TmpCmpType = String.Empty;

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();
                using (SqlDataReader DROffer = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn, sqlGetOffr))
                {
                    while (DROffer.Read())
                    {
                        if (DROffer.IsDBNull(0))
                        {
                            TmpCmpType = "S";
                        }
                        else
                        {
                            TmpCmpType = "P";
                        }
                    }
                }
            }

            int TmpCompId = Convert.ToInt32(TPE.Utility.DBLibrary.InsertAndReturnIdentity(Application["DBconn"].ToString(), "Insert into Company(COMP_TYPE,COMP_NAME,COMP_DATE,COMP_REG,COMP_ENBL) values ('" + TmpCmpType + "','" + txtComp_Cname.Text.ToString() + "',getdate(),1,1)"));
			
			//Updating Company field in PLace is null and user id = "D"
			string sqlUpdatePlace = "update place set plac_comp=" + TmpCompId + " where plac_id in ( " ;
			sqlUpdatePlace+="select ordr_shipto as PLACE from pending_orders where ordr_srno=" + Convert.ToInt32(Request.QueryString["ORD_SRNO"].ToString());
            sqlUpdatePlace += "union ";
            sqlUpdatePlace += "select ordr_bilto as PLACE from pending_orders where ordr_srno=" + Convert.ToInt32(Request.QueryString["ORD_SRNO"].ToString()) + ") and plac_comp is null";
            TPE.Utility.DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sqlUpdatePlace);
	
			//Update Person changing to Officer from a demo user
            string sqlGetPType = "Select PERS_TYPE from PERSON where Pers_Id=" + Convert.ToInt32(ViewState["USR_ID"].ToString());
            string PersType = DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), sqlGetPType).ToString();
			if (PersType=="D")
			{
                string sqlUpdatePerson = "Update Person Set PERS_COMP=" + TmpCompId + ",Pers_Type='O',PERS_LAST_NAME='" + txtPers_Lname.Text + "',PERS_FRST_NAME='" + txtPers_Fname.Text + "',";
                sqlUpdatePerson +="PERS_TITL='" + txtPers_Title.Text + "',PERS_PHON='" + txtPers_Phone.Text + "',PERS_FAX='" + txtPers_Fax.Text + "',PERS_MAIL='" + txtPers_Email.Text + "' ";
                sqlUpdatePerson += " where Pers_Id=" + Convert.ToInt32(ViewState["USR_ID"].ToString());
                DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sqlUpdatePerson);
			}			
			//////////////////////amit  
			
			
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"Insert into Bank(BANK_COMP,BANK_NAME,BANK_PERS,BANK_PHON,BANK_FAX) values ('"+ TmpCompId +"','" + txtBank_Bname.Text.ToString() + "','" + txtBank_Contact.Text.ToString() + "','" + txtBank_Phone.Text.ToString() + "','" + txtBank_Fax.Text.ToString() + "')");
			
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"Insert into associate(ASSO_COMP,ASSO_NAME,ASSO_PERS,ASSO_PHON,ASSO_FAX) values ('"+ TmpCompId +"','" + txtTrad1_Cname.Text.ToString() + "','" + txtTrad1_Contact.Text.ToString() + "','" + txtTrad1_Phone.Text.ToString() + "','" + txtTrad1_Fax.Text.ToString() + "')");
			
            if  (txtTrad2_Cname.Text.Trim()!= "")
			{
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"Insert into associate(ASSO_COMP,ASSO_NAME,ASSO_PERS,ASSO_PHON,ASSO_FAX) values ('"+ TmpCompId +"','" + txtTrad2_Cname.Text.ToString() + "','" + txtTrad2_Contact.Text.ToString() + "','" + txtTrad2_Phone.Text.ToString() + "','" + txtTrad2_Fax.Text.ToString() + "')");
			}
			
			if  (txtTrad3_Cname.Text.Trim()!= "" )
			{
				DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),"Insert into associate(ASSO_COMP,ASSO_NAME,ASSO_PERS,ASSO_PHON,ASSO_FAX) values ('"+ TmpCompId +"','" + txtTrad3_Cname.Text.ToString() + "','" + txtTrad3_Contact.Text.ToString() + "','" + txtTrad3_Phone.Text.ToString() + "','" + txtTrad3_Fax.Text.ToString() + "')");	
			}

			/////////////////////amit			
			DoApproveDeny(1);
			Response.Redirect("/Administrator/PendingTransactions.aspx");
			//*******************************/
			//		Add Mailing Code  Below //
			//*******************************/
			

		}

		private void DoApproveDeny(int DenyApprove)
		{
			StringBuilder sbHTML =  new StringBuilder() ;
			
			SqlConnection Conn =  new SqlConnection(Application["DBConn"].ToString());
			Conn.Open();
			SqlCommand SqlChkType = new SqlCommand("Select PERS_MAIL from person where PERS_ID=" + USR_ID ,Conn);
			string RecId = Convert.ToString(SqlChkType.ExecuteScalar());

			MailMessage EmailOrdr = new MailMessage();
			
			if (DenyApprove==1)
			{
				EmailOrdr.Subject = "Credit Application Approved for the Order.";

				sbHTML.Append("<Table><TR><TD><h3>Congratulations...! </h3></TD></TR>");
				sbHTML.Append("<TR><TD>Dear Customer...</TD></TR>");
				sbHTML.Append("<tr><td align='left' colspan='2'><font size='3'>Your Credit Application Form has been accepted.  </font></td></tr>");
				sbHTML.Append("<tr><td align='left' colspan='2'><font size='3'>You will be Contacted soon. </font></td></tr><tr></tr><tr></tr>");
				sbHTML.Append("<tr><td align='left' colspan='2'><font size='3'><b>Regards </b></font></td></tr></Table>");
				sbHTML.Append("<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "'>The Plastics Exchange</a>");
		
			}
			else
			{
				EmailOrdr.Subject = "Credit Application denied for the Order";

				sbHTML.Append("<Table><TR><TD>Dear Customer...</TD></TR>");
				sbHTML.Append("<tr><td align='left' colspan='2'><font size='3'>We regret to inform that your Credit Application form has been denied.  </font></td></tr><tr></tr><tr></tr>");
				sbHTML.Append("<tr><td align='left' colspan='2'><font size='3'><b>Regards </b></font></td></tr></Table>");
				sbHTML.Append("<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "'>The Plastics Exchange</a>");


			}
			EmailOrdr.From = Application["strEmailAdmin"].ToString();
			EmailOrdr.To = RecId;
			EmailOrdr.Cc = Application["strEmailOwner"].ToString();
			
			EmailOrdr.Body = sbHTML.ToString();
			EmailOrdr.BodyFormat = MailFormat.Html;
			//SmtpMail.SmtpServer="caesar";
			//TPE.Utility.EmailLibrary.Send(EmailOrdr);
		}
		private void btnApprove1_ServerClick(object sender, System.EventArgs e)
		{

		}
	}




///////////amit



}


