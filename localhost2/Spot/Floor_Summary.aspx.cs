using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPECharts;
using System.Threading;
using dotnetCHARTING;
using TPE.Utility;
using System.Data.SqlClient;
using System.IO;
////////////////////////////////////////////////////////////////////////////////////////
//according different data stored in the OFFR_PROD field of the BBOFFER table,show the//
//count of record,maximum price and the minimum price in the grid.                    //
//                                                                                    //
//                                                                                    //
////////////////////////////////////////////////////////////////////////////////////////

namespace localhost.Spot
{
	/// <summary>
	/// 
	/// </summary>
	public partial class summary : System.Web.UI.Page
	{

		private SeriesCollection getRandomData()
		{
			SeriesCollection SC = new SeriesCollection();
			Random myR = new Random();
			for(int a = 0; a < 4; a++)
			{
				Series s = new Series();
				s.Name = "Series " + a;
				for(int b = 0; b < 5; b++)
				{
					Element e = new Element();
					e.Name = "E " + b;
					e.YValue = myR.Next(50);
					s.Elements.Add(e);
				}
				SC.Add(s);
			}

			// Set Different Colors for our Series
			SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
			SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
			SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
			SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);
			return SC;
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{						
            if(!this.Page.IsPostBack)
			{
                //debug locally please comment this out for production, uncomment it
                //loadDDL();

                //Bottom of this creates CONTRACT market charts 1 to 5yr
                dotnetCHARTING.Chart ChartObj = new dotnetCHARTING.Chart();

                // Add the random data.
                ChartObj.SeriesCollection.Add(getRandomData());

                ChartObj.Width = 10;
                ChartObj.Height = 10;

                ChartObj.TempDirectory = "Temp";

                // Set the name of the file
                ChartObj.FileName = "Temp";

                // Set the format of the file
                ChartObj.FileManager.ImageFormat = ImageFormat.Png;
                Bitmap bmp1 = ChartObj.GetChartBitmap();
                //ChartObj.FileManager.SaveImage(bmp1);

                //Set the DB Connection to the Chart Control. It makes it to be able to re-create all the charts, if necessary.
                TPEChartsControl chartsControl = new TPEChartsControl();
                chartsControl.ConnectionStringDB = Application["DBConn"].ToString();
                chartsControl.setFolder(Server.MapPath("/Research/charts"));


                Thread threadCharts = new Thread(new ThreadStart(chartsControl.check_images));
                threadCharts.Start();
           
                

                //createWIRCharts();

				DataBindGrid();//bind the data with the grid.
			
			}

            if (Session["Typ"].ToString() != "A" && Session["Typ"].ToString() != "B" && Session["Typ"].ToString() != "L" && Session["Typ"].ToString() != "T" && Session["Typ"].ToString() != "S" && Session["Typ"].ToString() != "D" && Session["Typ"].ToString() != "X") 
			{
				summaryGrid.Columns[0].Visible = false;
				summaryGrid.Columns[4].Visible = false;
				summaryGrid.Columns[3].Visible = false;
				summaryGrid.Columns[2].Visible = false;
				summaryGrid.Columns[1].Visible = false;
			}
		}
		        
        double dbTotalOffers = 0.0;
		double dbTotalOffersLBS = 0.0;
		double dbTotalBid = 0.0;
		double dbTotalBidLBS = 0.0;
	
		protected void dg_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				dbTotalOffers += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "offr_count")==DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "offr_count"));
				dbTotalOffersLBS += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "offr_size")==DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "offr_size"));
				dbTotalBid += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "bid_count")==DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "bid_count"));
				dbTotalBidLBS += Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "bid_size")==DBNull.Value ? 0 : DataBinder.Eval(e.Item.DataItem, "bid_size"));

                if (Session["Typ"].ToString() != "A" && Session["Typ"].ToString() != "B" && Session["Typ"].ToString() != "L" && Session["Typ"].ToString() != "T" && Session["Typ"].ToString() != "S" && Session["Typ"].ToString() != "D" && Session["Typ"].ToString() != "X") 
				{
					e.Item.Cells[5].Text = (e.Item.Cells[0].Text=="&nbsp;" ? e.Item.Cells[6].Text :e.Item.Cells[0].Text);
					e.Item.Cells[5].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "'>" + e.Item.Cells[5].Text + "</a>";
                    e.Item.Cells[8].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&Sort=VARBIDOFFER_DESC'>" + e.Item.Cells[8].Text + "</a>"; //lbs
                    e.Item.Cells[9].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&Sort=VARPRICE2_ASC'>" + e.Item.Cells[9].Text + "</a>"; //low
                    e.Item.Cells[10].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&Sort=VARPRICE2_DESC'>" + e.Item.Cells[10].Text + "</a>"; //high
				}
				else
				{
					e.Item.Cells[4].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&StartOn=bid'>" + e.Item.Cells[4].Text + "</a>"; //qtd
					e.Item.Cells[3].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&StartOn=bid&Sort=VARBIDOFFER_DESC'>" + e.Item.Cells[3].Text + "</a>"; //lbs
					e.Item.Cells[2].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&StartOn=bid&Sort=VARPRICE_ASC'>" + e.Item.Cells[2].Text + "</a>"; //low
					e.Item.Cells[1].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&StartOn=bid&Sort=VARPRICE_DESC'>" + e.Item.Cells[1].Text + "</a>"; //high
					e.Item.Cells[5].Text = (e.Item.Cells[0].Text=="&nbsp;" ? e.Item.Cells[6].Text :e.Item.Cells[0].Text);
					e.Item.Cells[7].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "'>" + e.Item.Cells[7].Text + "</a>"; //qtd
					e.Item.Cells[8].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&Sort=VARBIDOFFER_DESC'>" + e.Item.Cells[8].Text + "</a>"; //lbs
					e.Item.Cells[9].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&Sort=VARPRICE2_ASC'>" + e.Item.Cells[9].Text + "</a>"; //low
					e.Item.Cells[10].Text = "<a href='./Spot_Floor.aspx?Filter=" + e.Item.Cells[11].Text + "&Sort=VARPRICE2_DESC'>" + e.Item.Cells[10].Text + "</a>"; //high
				}
			}
			else if (e.Item.ItemType == ListItemType.Footer)
			{
				e.Item.Cells[5].Text = "<b>Totals:</b> " ;
				
				e.Item.Cells[4].Text = "<b>"+String.Format("{0:#,###}", dbTotalBid)+"</b>";
				e.Item.Cells[3].Text = "<b>"+String.Format("{0:#,###}", dbTotalBidLBS)+"</b>";

				e.Item.Cells[7].Text = "<b>"+String.Format("{0:#,###}", dbTotalOffers)+"</b>";
				e.Item.Cells[8].Text = "<b>"+String.Format("{0:#,###}", dbTotalOffersLBS)+"</b>";
			}
		}

		private void DataBindGrid()//bind the data with the grid.
		{			
	
			DBLibrary.BindDataGridWithStoredProcedure(Application["DBconn"].ToString(),summaryGrid,"spSpotFloorSummary");			
		}
		#region Web 
		override protected void OnInit(EventArgs e)
		{
			//

			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// 
		/// 
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


        //*****CEF 04022007 Export to Excel
        protected void btnExportToExcel_Click(object sender, System.EventArgs e)
        {
            //export to excel

            //using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            //{
            //  DataSet Ds = TPE.Utility.DBLibrary.GetDataSetFromStoredProcedure(conn, "spInventory");

            btnExportToExcel.Enabled = false;

            DataBindGrid();

            summaryGrid.Columns[0].Visible = false;
            summaryGrid.Columns[1].Visible = false;

            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition","attachment;filename=SpotFloorSummary.xls");

            Response.BufferOutput = true;
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Charset = "UTF-8";
            EnableViewState = false;

            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

            ClearControls(summaryGrid);
            FormatExportedGrid(summaryGrid);
            summaryGrid.RenderControl(hw);

            Response.Write(tw.ToString());
            Response.End();

            btnExportToExcel.Enabled = true;

        }

        public void FormatExportedGrid(DataGrid dg1)
        {
            dg1.BackColor = System.Drawing.Color.White;     
        }

        private void ClearControls(Control ctrl)
        {
            int i;
            //for (int x = FileLen; x >= 1; x--) 
            //for (i = (ctrl.Controls.Count - 1); (i <= 0); i = (i + (i - 1)))

            for (i = (ctrl.Controls.Count - 1); (i >= 0); i--)
            {
                ClearControls(ctrl.Controls[i]);
            }

            Type ctrlType = ctrl.GetType();

            if (!(ctrlType.Name == "TableCell"))
            {
                if (!(ctrl.GetType().GetProperty("SelectedItem") == null))
                {
                    LiteralControl literal = new LiteralControl();
                    ctrl.Parent.Controls.Add(literal);
                    try
                    {
                        literal.Text = ((string)(ctrl.GetType().GetProperty("SelectedItem").GetValue(ctrl, null)));
                    }
                    catch
                    {
                    }
                    ctrl.Parent.Controls.Remove(ctrl);
                }
                else if (!(ctrl.GetType().GetProperty("Text") == null))
                {
                    LiteralControl literal = new LiteralControl();
                    ctrl.Parent.Controls.Add(literal);
                    literal.Text = ((string)(ctrl.GetType().GetProperty("Text").GetValue(ctrl, null)));
                    ctrl.Parent.Controls.Remove(ctrl);
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */

        }

        private void loadDDL()
        {
            int grade_id;
            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, "SELECT * FROM GRADE");
                while (dtr.Read())
                {

  //                  if (Convert.ToInt32(dtr["GRADE_ID"]) < 11)
  //                  {
                        grade_id = Convert.ToInt32(dtr["GRADE_ID"]);
                        createGradeChart(grade_id);

  //                  }
                }
            }

        }

        private void createGradeChart(int grade_id)
        {
            string fileName2 = "";
            dotnetCHARTING.Chart Chart = new dotnetCHARTING.Chart();
            Chart.ChartArea.ClearColors();
            Chart.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            Chart.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);


            // Set the chart type
            Chart.Type = ChartType.Combo;

            Chart.OverlapFooter = true;
            Chart.Mentor = false;
            // Set the size
            Chart.Width = 390;
            Chart.Height = 225;
            // Set the temp directory
            Chart.TempDirectory = "temp";
            // Debug mode. ( Will show generated errors if any )
            Chart.Debug = true;
            //Chart.Title = "Historical Price and Volume: " + ddlResinType.SelectedItem;
            //Chart.Title = "Historical Spot Resin Offers";

            Chart.DefaultSeries.Type = SeriesType.AreaLine;

            Chart.DefaultElement.Marker.Visible = false;

            Color[] MyColorObjectArray = new Color[3] { Color.Red, Color.FromArgb(255, 202, 0), Color.Yellow };
            Chart.Palette = MyColorObjectArray;

            Chart.LegendBox.Visible = false;
            Chart.ChartAreaSpacing = 8;

            Chart.DefaultSeries.DefaultElement.Transparency = 20;

            Chart.ChartArea.XAxis.DefaultTick.Line.Color = Color.FromArgb(255, 202, 0);
            Chart.ChartArea.YAxis.DefaultTick.Line.Color = Color.FromArgb(255, 202, 0);
            Chart.XAxis.TimeScaleLabels.MonthFormatString = "MMM dd \n yyyy";
            Chart.XAxis.DefaultTick.Label.Text = "<%Value,MMM dd> \n <%Value,yyyy>";
            Chart.ChartArea.YAxis.DefaultTick.Label.Font = new Font(Chart.ChartArea.YAxis.Label.Font.Name, (float)7.0, FontStyle.Bold);
            Chart.ChartArea.XAxis.DefaultTick.Label.Font = new Font(Chart.ChartArea.XAxis.Label.Font.Name, (float)6.0, FontStyle.Bold);
            Chart.XAxis.LabelRotate = true;

            Chart.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            Chart.ChartArea.XAxis.TickLabelAngle = 90;

            // Setup the axes.
            //Chart.YAxis.Label.Text = "Price";
            //          Chart.YAxis.FormatString = "Currency";
            Chart.YAxis.Scale = Scale.Range;

            SeriesCollection mySC = getPriceData(grade_id);
            mySC[0].Type = SeriesTypeFinancial.Bar;
            // SeriesCollection mySCV = getVolumeData(grade_id);

            Chart.ChartArea.Line.Color = Color.Orange;

            // Add the price data.
            Chart.SeriesCollection.Add(mySC);


            //Chart
            //Chart.TitleBox.Background.Color = Color.Orange;
            Chart.Background.Color = Color.Black;
            Chart.ChartArea.Background.Color = Color.Black;
            Chart.ChartArea.DefaultSeries.Line.Color = Color.Red;
            Chart.ChartArea.DefaultSeries.Line.Width = 2;

            Chart.ChartArea.XAxis.Line.Width = 4;
            Chart.ChartArea.YAxis.Line.Width = 2;

            //XAxis
            Chart.XAxis.Label.Color = Color.White;
            Chart.ChartArea.XAxis.Label.Color = Color.White;
            Chart.XAxis.Line.Color = Color.Orange;
            Chart.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;

            //YAxis
            Chart.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            Chart.ChartArea.YAxis.Label.Color = Color.White;
            Chart.YAxis.Line.Color = Color.Orange;
            Chart.YAxis.Label.Color = Color.White;

            Chart.ChartArea.Line.Width = 2;

            Chart.TempDirectory = System.Configuration.ConfigurationSettings.AppSettings["ChartPath"];

           //Chart.TempDirectory = "C:\\Data\\TPE\\localhost2\\Research\\ChartsSpot";
           //Chart.TempDirectory = "C:\\Data\\TPE\\localhost\\Research\\ChartsSpot";
           //Chart.TempDirectory = "E:\\Data\\TPE\\localhost\\Research\\ChartsSpot";
            Chart.FileName = "Chart_" + grade_id + "_1Y";
            Chart.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap png1 = Chart.GetChartBitmap();
            fileName2 = Chart.FileManager.SaveImage(png1);

            Chart.SeriesCollection.Clear();

        }

        private SeriesCollection getPriceData(int grade_id)
        {


            SeriesCollection SC = new SeriesCollection();

            Series s = new Series();
            //s.Name = "Price Range";


            using (SqlConnection conn = new SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = null;
                Hashtable param = new Hashtable();
                param.Add("@resin_grade_id", grade_id);
                param.Add("@date", "04-02-2006");

                dtr = DBLibrary.GetDataReaderStoredProcedure(conn, "spSpot_Offers_SummaryByResinGrade", param);
                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["friday_date"].ToString());
                    Element e = new Element();
                    e.XDateTime = date;


                    //e.SmartLabel.Color = Color.White;



                    double price_low = Convert.ToDouble(dtr["price_low"].ToString());
                    double price_high = Convert.ToDouble(dtr["price_high"].ToString());

                    e.Close = price_high;
                    e.Open = price_low;

                    e.High = price_high;
                    e.Low = price_low;

                    s.Elements.Add(e);

                }
            }


            SC.Add(s);
            return (SC);
        }



        private void createWIRCharts()
        {        
            dotnetCHARTING.Chart ChartCreate = new dotnetCHARTING.Chart();
       
            CreatePEChainChart(ChartCreate, false);
            CreatePPChainChart(ChartCreate, false);
            CreatePSChainChart(ChartCreate, false);

            CreatePEChainChart(ChartCreate,true);
            CreatePPChainChart(ChartCreate, true);
            CreatePSChainChart(ChartCreate, true);
     
            CreateLLDPEEthyleneChart(ChartCreate);
            CreateHOPPRGPChart(ChartCreate);
        }

        int chartWidth= 700;
        int chartHeight = 600;

        float TitleFontSize = 12;
        float AxisFontSize = 11;
        


        private void CreatePEChainChart(dotnetCHARTING.Chart ChartPE, Boolean MonthlyChart)
        {                       
            
            ChartPE.ChartArea.ClearColors();

            if (MonthlyChart == false)
            {
                ChartPE.Title = "Polyethylene - Priced $/lbs - 1 Year                     �The Plastics Exchange 2008";
                ChartPE.FileName = "PolyethyleneChain";
            }
            else
            {
                ChartPE.Title = "Polyethylene - Priced $/lbs - 30 Day                     �The Plastics Exchange 2008";
                ChartPE.FileName = "PolyethyleneChain30Day";
            }

            ChartPE.Debug = false;
            ChartPE.OverlapFooter = false;
            ChartPE.Mentor = false;
            ChartPE.TempDirectory = "/research/charts";

            ChartPE.Width = chartWidth;
            ChartPE.Height = chartHeight;

            //ChartPE.Dpi = 300;
            ChartPE.FileManager.ImageFormat = ImageFormat.Png;
            ChartPE.Type = ChartType.Combo;
            ChartPE.ChartAreaSpacing = 3;

            ChartPE.Background.Color = Color.Black;
            ChartPE.DefaultElement.Marker.Visible = true;
            ChartPE.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            //ChartPE.DefaultSeries.DefaultElement.Marker.Size = 8;

            ChartPE.XAxis.Label.Color = Color.White;
            ChartPE.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartPE.XAxis.TimeInterval = TimeInterval.Day;
            ChartPE.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartPE.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            //ChartPE.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartPE.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartPE.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartPE.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            ChartPE.XAxis.Line.Color = Color.Orange;

            ChartPE.YAxis.Interval = 0.01;
            ChartPE.YAxis.Label.Text = "Price";
            ChartPE.YAxis.FormatString = "$.00#";
            ChartPE.YAxis.Scale = Scale.Range;
            ChartPE.YAxis.Line.Color = Color.Orange;
            ChartPE.YAxis.Label.Color = Color.White;

            ChartPE.TitleBox.Position = TitleBoxPosition.Full;
            //ChartPE.TitleBox.Background.Color = Color.FromArgb(206, 136, 7);  //TITLE BOX
            ChartPE.TitleBox.Background.Color = Color.Orange;  //TITLE BOX
            ChartPE.TitleBox.Label.Color = Color.Black;


            ChartPE.TitleBox.Label.Font = new Font(ChartPE.ChartArea.XAxis.Label.Font.Name, TitleFontSize, FontStyle.Bold);


            ChartPE.LegendBox.Template = "%Icon%Name";
            //ChartPE.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
            ChartPE.LegendBox.Position = new Point(70, 70);
            ChartPE.LegendBox.LabelStyle.Color = Color.Black;
            ChartPE.LegendBox.Label.Font = new Font(ChartPE.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Regular);

            ChartPE.ChartArea.Background.Color = Color.Black;
            ChartPE.ChartArea.DefaultSeries.Line.Width = 2;
            ChartPE.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPE.ChartArea.XAxis.DefaultTick.GridLine.Width = 3;
            ChartPE.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartPE.ChartArea.XAxis.TickLabelAngle = 90;
            ChartPE.ChartArea.XAxis.LabelRotate = true;
            ChartPE.ChartArea.XAxis.Label.Color = Color.White;
            ChartPE.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
            
            
            ChartPE.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartPE.ChartArea.XAxis.Label.Font.Name, AxisFontSize, FontStyle.Bold);
            ChartPE.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartPE.ChartArea.YAxis.Label.Font.Name, AxisFontSize, FontStyle.Bold);
            
            
            ChartPE.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPE.ChartArea.YAxis.DefaultTick.GridLine.Width = 3;
            ChartPE.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartPE.ChartArea.YAxis.Label.Color = Color.White;


            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();

            ChartPE.SeriesCollection.Add(getCreatePropyleneChainChartPriceData(MonthlyChart));

            ChartPE.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap bmp1 = ChartPE.GetChartBitmap();
            string fileName2 = "";
            fileName2 = ChartPE.FileManager.SaveImage(bmp1);

            ChartPE.SeriesCollection.Clear();

        }

        private SeriesCollection getCreatePropyleneChainChartPriceData(Boolean MonthlyChart)
        {
            string sSQL;
            if (MonthlyChart == false)
            {
                sSQL = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id={0} AND export_price.date between DATEADD(mm,-12,'{1}') and '{2}' and datepart(dw,export_price.date) = 6 ORDER BY export_price.date";
            }
            else
            {
                sSQL = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id={0} AND export_price.date between DATEADD(dd,-30,'{1}') and '{2}'  ORDER BY export_price.date";
            }
            

            Hashtable htItem = new Hashtable();
            htItem.Add(1,"HDPE Film - HMW");
            htItem.Add(3,"HDPE Blow - HIC");
            htItem.Add(5,"HDPE Inj - Pail");
            htItem.Add(6,"LDPE Film - Clarity");
            htItem.Add(11,"LLDPE Film - Butene");
            
            SeriesCollection SC = new SeriesCollection();
            int i =0;

            foreach( DictionaryEntry de in htItem )
            {
                string s = string.Format(sSQL,de.Key,DateTime.Now,DateTime.Now);
                SC.Add(BuildData(s, de.Value.ToString()));
                                    
                SC[i].Type = SeriesType.Line;  
                SC[i].DefaultElement.Color = GetColor(i);
                SC[i].LegendEntry.Marker.Size = 6;

                i= i + 1;
            }

            return (SC);

        }


        private void CreatePPChainChart(dotnetCHARTING.Chart ChartPE, Boolean MonthlyChart)
        {
            ChartPE.ChartArea.ClearColors();


            if (MonthlyChart == false)
            {
                ChartPE.Title = "Polypropylene - Priced $/lbs - 1 Year                �The Plastics Exchange 2008";
                ChartPE.FileName = "PolypropyleneChain";
            }
            else
            {
                ChartPE.Title = "Polypropylene - Priced $/lbs - 30 Day                �The Plastics Exchange 2008";
                ChartPE.FileName = "PolypropyleneChain30Day";
            }


            ChartPE.Debug = false;
            ChartPE.OverlapFooter = false;
            ChartPE.Mentor = false;
            ChartPE.TempDirectory = "/research/charts";



            ChartPE.Width = chartWidth;
            ChartPE.Height = chartHeight;
            //ChartPE.Dpi = 300;
            ChartPE.FileManager.ImageFormat = ImageFormat.Png;
            ChartPE.Type = ChartType.Combo;
            ChartPE.ChartAreaSpacing = 3;

            ChartPE.Background.Color = Color.Black;
            ChartPE.DefaultElement.Marker.Visible = true;
            ChartPE.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            //ChartPE.DefaultSeries.DefaultElement.Marker.Size = 8;

            ChartPE.XAxis.Label.Color = Color.White;
            ChartPE.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartPE.XAxis.TimeInterval = TimeInterval.Day;
            ChartPE.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartPE.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            //ChartPE.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartPE.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartPE.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartPE.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            ChartPE.XAxis.Line.Color = Color.Orange;

            ChartPE.YAxis.Interval = 0.01;
            ChartPE.YAxis.Label.Text = "Price";
            ChartPE.YAxis.FormatString = "$.00#";
            ChartPE.YAxis.Scale = Scale.Range;
            ChartPE.YAxis.Line.Color = Color.Orange;
            ChartPE.YAxis.Label.Color = Color.White;

            ChartPE.TitleBox.Position = TitleBoxPosition.Full;
            //ChartPE.TitleBox.Background.Color = Color.FromArgb(206, 136, 7);  //TITLE BOX
            ChartPE.TitleBox.Background.Color = Color.Orange;  //TITLE BOX
            ChartPE.TitleBox.Label.Color = Color.Black;


            ChartPE.TitleBox.Label.Font = new Font(ChartPE.ChartArea.XAxis.Label.Font.Name, TitleFontSize, FontStyle.Bold);


            ChartPE.LegendBox.Template = "%Icon%Name";
            //ChartPE.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
            ChartPE.LegendBox.Position = new Point(70, 70);
            ChartPE.LegendBox.LabelStyle.Color = Color.Black;
            ChartPE.LegendBox.Label.Font = new Font(ChartPE.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Regular);

            ChartPE.ChartArea.Background.Color = Color.Black;
            ChartPE.ChartArea.DefaultSeries.Line.Width = 2;
            ChartPE.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPE.ChartArea.XAxis.DefaultTick.GridLine.Width = 3;
            ChartPE.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartPE.ChartArea.XAxis.TickLabelAngle = 90;
            ChartPE.ChartArea.XAxis.LabelRotate = true;
            ChartPE.ChartArea.XAxis.Label.Color = Color.White;
            ChartPE.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;

            ChartPE.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartPE.ChartArea.XAxis.Label.Font.Name, AxisFontSize, FontStyle.Bold);
            ChartPE.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartPE.ChartArea.YAxis.Label.Font.Name, AxisFontSize, FontStyle.Bold);
            
            ChartPE.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPE.ChartArea.YAxis.DefaultTick.GridLine.Width = 3;
            ChartPE.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartPE.ChartArea.YAxis.Label.Color = Color.White;




            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();

            ChartPE.SeriesCollection.Add(getCreatePolypropyleneChainChartPriceData(MonthlyChart));

            ChartPE.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap bmp1 = ChartPE.GetChartBitmap();
            string fileName2 = "";
            fileName2 = ChartPE.FileManager.SaveImage(bmp1);

            ChartPE.SeriesCollection.Clear();

        }

        private SeriesCollection getCreatePolypropyleneChainChartPriceData(Boolean MonthlyChart)
        {

            string sSQL;
            
            if (MonthlyChart == false)
            {
               sSQL = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id={0} AND export_price.date between DATEADD(mm,-12,'{1}') and '{2}' and datepart(dw,export_price.date) = 6 ORDER BY export_price.date";
            }
            else
            {
                sSQL = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id={0} AND export_price.date between DATEADD(dd,-30,'{1}') and '{2}'  ORDER BY export_price.date";
            }


            Hashtable htItem = new Hashtable();
            htItem.Add(9, "HoPP Inj - 20 melt");
            htItem.Add(22, "CoPP Inj - 20 melt");
            

            SeriesCollection SC = new SeriesCollection();
            int i = 0;

            foreach (DictionaryEntry de in htItem)
            {
                string s = string.Format(sSQL, de.Key, DateTime.Now, DateTime.Now);
                SC.Add(BuildData(s, de.Value.ToString()));

                SC[i].Type = SeriesType.Line;
                SC[i].DefaultElement.Color = GetColor(i);
                SC[i].LegendEntry.Marker.Size = 6;

                i = i + 1;
            }

            return (SC);

        }


        private void CreatePSChainChart(dotnetCHARTING.Chart ChartPE, Boolean MonthlyChart)
        {
            ChartPE.ChartArea.ClearColors();


            if (MonthlyChart == false)
            {
                ChartPE.Title = "Polystyrene - Priced $/lbs - 1 Year                     �The Plastics Exchange 2008";
                ChartPE.FileName = "PolystyreneChain";
            }
            else
            {
                ChartPE.Title = "Polystyrene - Priced $/lbs - 30 Day                     �The Plastics Exchange 2008";
                ChartPE.FileName = "PolystyreneChain30Day";
            }

            
            ChartPE.Debug = false;
            ChartPE.OverlapFooter = false;
            ChartPE.Mentor = false;
            ChartPE.TempDirectory = "/research/charts";


            ChartPE.Width = chartWidth;
            ChartPE.Height = chartHeight;
            //ChartPE.Dpi = 300;
            ChartPE.FileManager.ImageFormat = ImageFormat.Png;
            ChartPE.Type = ChartType.Combo;
            ChartPE.ChartAreaSpacing = 3;

            ChartPE.Background.Color = Color.Black;
            ChartPE.DefaultElement.Marker.Visible = true;
            ChartPE.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            //ChartPE.DefaultSeries.DefaultElement.Marker.Size = 8;

            ChartPE.XAxis.Label.Color = Color.White;
            ChartPE.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartPE.XAxis.TimeInterval = TimeInterval.Day;
            ChartPE.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartPE.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            //ChartPE.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartPE.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartPE.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartPE.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            ChartPE.XAxis.Line.Color = Color.Orange;

            ChartPE.YAxis.Interval = 0.01;
            ChartPE.YAxis.Label.Text = "Price";
            ChartPE.YAxis.FormatString = "$.00#";
            ChartPE.YAxis.Scale = Scale.Range;
            ChartPE.YAxis.Line.Color = Color.Orange;
            ChartPE.YAxis.Label.Color = Color.White;

            ChartPE.TitleBox.Position = TitleBoxPosition.Full;
            //ChartPE.TitleBox.Background.Color = Color.FromArgb(206, 136, 7);  //TITLE BOX
            ChartPE.TitleBox.Background.Color = Color.Orange;  //TITLE BOX
            ChartPE.TitleBox.Label.Color = Color.Black;
            ChartPE.TitleBox.Label.Font = new Font(ChartPE.ChartArea.XAxis.Label.Font.Name, TitleFontSize, FontStyle.Bold);


            ChartPE.LegendBox.Template = "%Icon%Name";
            //ChartPE.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
            ChartPE.LegendBox.Position = new Point(70, 70);
            ChartPE.LegendBox.LabelStyle.Color = Color.Black;
            
            ChartPE.LegendBox.Label.Font = new Font(ChartPE.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Regular);

            ChartPE.ChartArea.Background.Color = Color.Black;
            ChartPE.ChartArea.DefaultSeries.Line.Width = 2;
            ChartPE.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPE.ChartArea.XAxis.DefaultTick.GridLine.Width = 3;
            ChartPE.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartPE.ChartArea.XAxis.TickLabelAngle = 90;
            ChartPE.ChartArea.XAxis.LabelRotate = true;
            ChartPE.ChartArea.XAxis.Label.Color = Color.White;
            ChartPE.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
            ChartPE.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartPE.ChartArea.XAxis.Label.Font.Name, AxisFontSize, FontStyle.Bold);
            ChartPE.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartPE.ChartArea.YAxis.Label.Font.Name, AxisFontSize, FontStyle.Bold);
            ChartPE.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPE.ChartArea.YAxis.DefaultTick.GridLine.Width = 3;
            ChartPE.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartPE.ChartArea.YAxis.Label.Color = Color.White;




            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();

            ChartPE.SeriesCollection.Add(getCreatePolystyreneChainChartPriceData(MonthlyChart));

            ChartPE.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap bmp1 = ChartPE.GetChartBitmap();
            string fileName2 = "";
            fileName2 = ChartPE.FileManager.SaveImage(bmp1);

            ChartPE.SeriesCollection.Clear();

        }

        private SeriesCollection getCreatePolystyreneChainChartPriceData(Boolean MonthlyChart)
        {

            string sSQL;
            if (MonthlyChart == false)
            {
                sSQL = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id={0} AND export_price.date between DATEADD(mm,-12,'{1}') and '{2}' and datepart(dw,export_price.date) = 6 ORDER BY export_price.date";
            }
            else
            {
                sSQL = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id={0} AND export_price.date between DATEADD(dd,-30,'{1}') and '{2}'  ORDER BY export_price.date";
            }

            Hashtable htItem = new Hashtable();
            htItem.Add(19, "GPPS Inj - 8 melt");
            htItem.Add(20, "HIPS Inj - 8 melt");


            SeriesCollection SC = new SeriesCollection();
            int i = 0;

            foreach (DictionaryEntry de in htItem)
            {
                string s = string.Format(sSQL, de.Key, DateTime.Now, DateTime.Now);
                SC.Add(BuildData(s, de.Value.ToString()));

                SC[i].Type = SeriesType.Line;
                SC[i].DefaultElement.Color = GetColor(i);
                SC[i].LegendEntry.Marker.Size = 6;

                i = i + 1;
            }

            return (SC);

        }


        private void CreateLLDPEEthyleneChart(dotnetCHARTING.Chart ChartPE)
        {
            ChartPE.ChartArea.ClearColors();

            ChartPE.Title = "Spot LLDPE - Spot Ethylene (Spread $/lbs) -  1 Year   �PetroChemWire 2008";
            ChartPE.FileName = "LLDPEEthyleneSpread";            

            ChartPE.Debug = false;
            ChartPE.OverlapFooter = false;
            ChartPE.Mentor = false;
            ChartPE.TempDirectory = "/research/charts";

            //ChartPE.Width = 600;
            //ChartPE.Height = 450;

            ChartPE.Width = chartWidth;
            ChartPE.Height = chartHeight;
            //ChartPE.Dpi = 300;
            ChartPE.FileManager.ImageFormat = ImageFormat.Png;
            ChartPE.Type = ChartType.Combo;
            ChartPE.ChartAreaSpacing = 3;

            ChartPE.Background.Color = Color.Black;
            ChartPE.DefaultElement.Marker.Visible = true;
            ChartPE.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            //ChartPE.DefaultSeries.DefaultElement.Marker.Size = 8;

            ChartPE.XAxis.Label.Color = Color.White;
            ChartPE.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            
            //ChartPE.XAxis.TimeInterval = TimeInterval.Week;
            ChartPE.XAxis.TimeInterval = TimeInterval.Weeks;
            ChartPE.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartPE.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            //ChartPE.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartPE.XAxis.TimeScaleLabels.DayFormatString = "p";
            //ChartPE.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartPE.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            ChartPE.XAxis.Line.Color = Color.Orange;



            ChartPE.YAxis.Interval = 0.01;
            ChartPE.YAxis.Label.Text = "Price";
            ChartPE.YAxis.FormatString = "$.00#";
            ChartPE.YAxis.Scale = Scale.Range;
            ChartPE.YAxis.Line.Color = Color.Orange;
            ChartPE.YAxis.Label.Color = Color.White;

            ChartPE.TitleBox.Position = TitleBoxPosition.Full;
            //ChartPE.TitleBox.Background.Color = Color.FromArgb(206, 136, 7);  //TITLE BOX
            ChartPE.TitleBox.Background.Color = Color.Orange;  //TITLE BOX
            ChartPE.TitleBox.Label.Color = Color.Black;
            ChartPE.TitleBox.Label.Font = new Font(ChartPE.ChartArea.XAxis.Label.Font.Name,TitleFontSize, FontStyle.Bold);


            //ChartPE.LegendBox.Template = "%Icon%Name";
            ////ChartPE.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
            //ChartPE.LegendBox.Position = new Point(70, 70);
            //ChartPE.LegendBox.LabelStyle.Color = Color.Black;
            //ChartPE.LegendBox.Label.Font = new Font(ChartPE.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Regular);

            ChartPE.LegendBox.Visible = false;

            ChartPE.ChartArea.Background.Color = Color.Black;
            ChartPE.ChartArea.DefaultSeries.Line.Width = 2;
            ChartPE.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPE.ChartArea.XAxis.DefaultTick.GridLine.Width = 3;
            ChartPE.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartPE.ChartArea.XAxis.TickLabelAngle = 90;
            ChartPE.ChartArea.XAxis.LabelRotate = true;
            ChartPE.ChartArea.XAxis.Label.Color = Color.White;
            ChartPE.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
            ChartPE.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartPE.ChartArea.XAxis.Label.Font.Name, AxisFontSize, FontStyle.Bold);
            ChartPE.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartPE.ChartArea.YAxis.Label.Font.Name, AxisFontSize, FontStyle.Bold);
            ChartPE.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPE.ChartArea.YAxis.DefaultTick.GridLine.Width = 3;
            ChartPE.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartPE.ChartArea.YAxis.Label.Color = Color.White;


            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();

            ChartPE.SeriesCollection.Add(getLLDPEEthyleneData());

            ChartPE.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap bmp1 = ChartPE.GetChartBitmap();
            string fileName2 = "";
            fileName2 = ChartPE.FileManager.SaveImage(bmp1);

            ChartPE.SeriesCollection.Clear();

        }


        private void CreateHOPPRGPChart(dotnetCHARTING.Chart ChartPE)
        {
            ChartPE.ChartArea.ClearColors();

            ChartPE.Title = "Spot HoPP - Spot RGP (Spread $/lbs) -  1 Year            �PetroChemWire 2008";
            ChartPE.FileName = "HOPPRGPSpread";

            ChartPE.Debug = false;
            ChartPE.OverlapFooter = false;
            ChartPE.Mentor = false;
            ChartPE.TempDirectory = "/research/charts";

            ChartPE.Width = chartWidth;
            ChartPE.Height = chartHeight;
            //ChartPE.Dpi = 300;
            ChartPE.FileManager.ImageFormat = ImageFormat.Png;
            ChartPE.Type = ChartType.Combo;
            ChartPE.ChartAreaSpacing = 3;

            ChartPE.Background.Color = Color.Black;
            ChartPE.DefaultElement.Marker.Visible = true;
            ChartPE.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.Diamond;
            //ChartPE.DefaultSeries.DefaultElement.Marker.Size = 8;

            ChartPE.XAxis.Label.Color = Color.White;
            ChartPE.XAxis.DefaultTick.Label.Text = "<%Name,mmm>";
            ChartPE.XAxis.TimeInterval = TimeInterval.Day;
            ChartPE.XAxis.TimeScaleLabels.WeekFormatString = "MMM\r\ndd";
            ChartPE.XAxis.TimeScaleLabels.MonthFormatString = "MMM<BR>ndd";
            //ChartPE.XAxis.TimeScaleLabels.YearFormatString = "yyyy";
            ChartPE.XAxis.TimeScaleLabels.DayFormatString = "p";
            ChartPE.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day);
            ChartPE.XAxis.TimeScaleLabels.YearTick.Line.Color = Color.Orange;
            ChartPE.XAxis.Line.Color = Color.Orange;

            ChartPE.YAxis.Interval = 0.01;
            ChartPE.YAxis.Label.Text = "Price";
            ChartPE.YAxis.FormatString = "$.00#";
            ChartPE.YAxis.Scale = Scale.Range;
            ChartPE.YAxis.Line.Color = Color.Orange;
            ChartPE.YAxis.Label.Color = Color.White;

            ChartPE.TitleBox.Position = TitleBoxPosition.Full;
            //ChartPE.TitleBox.Background.Color = Color.FromArgb(206, 136, 7);  //TITLE BOX
            ChartPE.TitleBox.Background.Color = Color.Orange;  //TITLE BOX
            ChartPE.TitleBox.Label.Color = Color.Black;
            ChartPE.TitleBox.Label.Font = new Font(ChartPE.ChartArea.XAxis.Label.Font.Name, TitleFontSize, FontStyle.Bold);


            //ChartPE.LegendBox.Template = "%Icon%Name";
            ////ChartPE.LegendBox.Orientation = dotnetCHARTING.Orientation.TopRight;
            //ChartPE.LegendBox.Position = new Point(70, 70);
            //ChartPE.LegendBox.LabelStyle.Color = Color.Black;
            //ChartPE.LegendBox.Label.Font = new Font(ChartPE.ChartArea.XAxis.Label.Font.Name, (float)10.0, FontStyle.Regular);

            ChartPE.LegendBox.Visible = false;

            ChartPE.ChartArea.Background.Color = Color.Black;
            ChartPE.ChartArea.DefaultSeries.Line.Width = 2;
            ChartPE.ChartArea.XAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPE.ChartArea.XAxis.DefaultTick.GridLine.Width = 3;
            ChartPE.ChartArea.XAxis.TickLabelMode = TickLabelMode.Angled;
            ChartPE.ChartArea.XAxis.TickLabelAngle = 90;
            ChartPE.ChartArea.XAxis.LabelRotate = true;
            ChartPE.ChartArea.XAxis.Label.Color = Color.White;
            ChartPE.ChartArea.XAxis.DefaultTick.Label.Color = Color.White;
            ChartPE.ChartArea.XAxis.DefaultTick.Label.Font = new Font(ChartPE.ChartArea.XAxis.Label.Font.Name, AxisFontSize, FontStyle.Bold);
            ChartPE.ChartArea.YAxis.DefaultTick.Label.Font = new Font(ChartPE.ChartArea.YAxis.Label.Font.Name,AxisFontSize, FontStyle.Bold);
            ChartPE.ChartArea.YAxis.DefaultTick.GridLine.Color = Color.FromArgb(70, 118, 118, 118);
            ChartPE.ChartArea.YAxis.DefaultTick.GridLine.Width = 3;
            ChartPE.ChartArea.YAxis.DefaultTick.Label.Color = Color.White;
            ChartPE.ChartArea.YAxis.Label.Color = Color.White;


            //Load data as chart series

            SeriesCollection mySC = new SeriesCollection();

            ChartPE.SeriesCollection.Add(getHOPPRGPData());

            ChartPE.FileManager.ImageFormat = ImageFormat.Png;
            Bitmap bmp1 = ChartPE.GetChartBitmap();
            string fileName2 = "";
            fileName2 = ChartPE.FileManager.SaveImage(bmp1);

            ChartPE.SeriesCollection.Clear();

        }



        private SeriesCollection getLLDPEEthyleneData()
        {            
            //LLDPE


            //sSQL = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id={0} AND export_price.date between DATEADD(mm,-12,'{1}') and '{2}' and datepart(dw,export_price.date) = 6 ORDER BY export_price.date";

            string sSQL1 = "select convert(char,date,110) as date2, convert(numeric(10,5),((bid + ask) /2)) as ask from export_price where cont_id=11 AND export_price.date between DATEADD(mm,-12,'{0}') and '{1}' and datepart(dw,export_price.date) = 6 order by date ASC";            
            //Ethylene
            string sSQL2 = "select convert(char,date,110) as date2, convert(numeric(10,5),((bid + ask) /2)) as ask from export_price where cont_id=33 AND export_price.date between DATEADD(mm,-12,'{0}') and '{1}' and datepart(dw,export_price.date) = 6 order by date ASC";            
            
            SeriesCollection SC = new SeriesCollection();
            Series sBidPrice;

            sBidPrice = new Series();
            sBidPrice.Name = "Spread";

            double dSpread = 0;

            using (SqlConnection conn1 = new SqlConnection(Application["DBconn"].ToString()))
            {
                SqlDataReader dtr1 = null;
                dtr1 = DBLibrary.GetDataReaderFromSelect(conn1, string.Format(sSQL1,DateTime.Now,DateTime.Now));

                using (SqlConnection conn2 = new SqlConnection(Application["MonoDB"].ToString()))
                {
                    SqlDataReader dtr2 = null;
                    dtr2 = DBLibrary.GetDataReaderFromSelect(conn2, string.Format(sSQL2, DateTime.Now, DateTime.Now));

                    while (dtr1.Read())
                    {
                        while (dtr2.Read())
                        {
                            if (dtr1["date2"].ToString() == dtr2["date2"].ToString())
                            {

                                DateTime date = Convert.ToDateTime(dtr1["date2"].ToString());
                                Element eBidPrice = new Element();
                            
                                dSpread = Convert.ToDouble(dtr1["ask"].ToString()) - (Convert.ToDouble(dtr2["ask"].ToString()));                               

                                eBidPrice.ToolTip = dSpread.ToString("N5");
                                eBidPrice.SmartLabel.DynamicDisplay = false;
                                eBidPrice.SmartLabel.DynamicPosition = true;
                                eBidPrice.ShowValue = false;
                                eBidPrice.SmartLabel.Color = Color.White;
                                eBidPrice.XDateTime = date;
                                eBidPrice.SmartLabel.Text = "";
                                eBidPrice.YValue = double.Parse(dSpread.ToString("N5"));
                                eBidPrice.Name = Convert.ToDateTime(dtr1["DATE2"]).ToString("MMM dd");
                                //eBidPrice.XValue = date.ToOADate();
                                sBidPrice.AddElements(eBidPrice);

                                break;
                            }

                        }


                    }


                }

                SC.Add(sBidPrice);

                //SC[0].Type = SeriesTypeFinancial.Bar;
                SC[0].Type = SeriesType.Line;  //propane
                SC[0].DefaultElement.Color = Color.Green;
                SC[0].LegendEntry.Marker.Size = 10;

                return (SC);

            }


        }


        private SeriesCollection getHOPPRGPData()
        {
            //LLDPE


            //sSQL = "select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id={0} AND export_price.date between DATEADD(mm,-12,'{1}') and '{2}' and datepart(dw,export_price.date) = 6 ORDER BY export_price.date";

            string sSQL1 = "select convert(char,date,110) as date2, convert(numeric(10,5),((bid + ask) /2)) as ask from export_price where cont_id=9 AND export_price.date between DATEADD(mm,-12,'{0}') and '{1}' and datepart(dw,export_price.date) = 6 order by date ASC";
            //Ethylene
            string sSQL2 = "select convert(char,date,110) as date2, convert(numeric(10,5),((bid + ask) /2)) as ask from export_price where cont_id=34 AND export_price.date between DATEADD(mm,-12,'{0}') and '{1}' and datepart(dw,export_price.date) = 6 order by date ASC";

            SeriesCollection SC = new SeriesCollection();
            Series sBidPrice;

            sBidPrice = new Series();
            sBidPrice.Name = "Spread";

            double dSpread = 0;

            using (SqlConnection conn1 = new SqlConnection(Application["DBconn"].ToString()))
            {
                SqlDataReader dtr1 = null;
                dtr1 = DBLibrary.GetDataReaderFromSelect(conn1, string.Format(sSQL1, DateTime.Now, DateTime.Now));

                using (SqlConnection conn2 = new SqlConnection(Application["MonoDB"].ToString()))
                {
                    SqlDataReader dtr2 = null;
                    dtr2 = DBLibrary.GetDataReaderFromSelect(conn2, string.Format(sSQL2, DateTime.Now, DateTime.Now));

                    while (dtr1.Read())
                    {
                        while (dtr2.Read())
                        {
                            if (dtr1["date2"].ToString() == dtr2["date2"].ToString())
                            {

                                DateTime date = Convert.ToDateTime(dtr1["date2"].ToString());
                                Element eBidPrice = new Element();

                                dSpread = Convert.ToDouble(dtr1["ask"].ToString()) - (Convert.ToDouble(dtr2["ask"].ToString()));

                                eBidPrice.ToolTip = dSpread.ToString("N5");
                                eBidPrice.SmartLabel.DynamicDisplay = false;
                                eBidPrice.SmartLabel.DynamicPosition = true;
                                eBidPrice.ShowValue = false;
                                eBidPrice.SmartLabel.Color = Color.White;
                                eBidPrice.XDateTime = date;
                                eBidPrice.SmartLabel.Text = "";
                                eBidPrice.YValue = double.Parse(dSpread.ToString("N5"));
                                eBidPrice.Name = Convert.ToDateTime(dtr1["DATE2"]).ToString("MMM dd");
                                //eBidPrice.XValue = date.ToOADate();
                                sBidPrice.AddElements(eBidPrice);

                                break;
                            }

                        }


                    }


                }

                SC.Add(sBidPrice);

                //SC[0].Type = SeriesTypeFinancial.Bar;
                SC[0].Type = SeriesType.Line;  //propane
                SC[0].DefaultElement.Color = Color.Green;
                SC[0].LegendEntry.Marker.Size = 10;

                return (SC);

            }


        }




        private Color GetColor(int id)
        {
            switch (id)
            {            
                case 0:
                    return Color.Red;
                    break;
                
                case 1:
                    return Color.Gold;
                    break;

                case 2:
                    return Color.Green;
                    break;

                case 3:
                    return Color.Silver;
                    break;
                
                case 4:
                    return Color.SandyBrown;
                    break;

                default:
                    return Color.Red;
                    break;
            }


        }
       
        private Series BuildData(string sSQL, string itemName)
        {

            SeriesCollection SC = new SeriesCollection();
            Series sBidPrice;

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                SqlDataReader dtr = null;
                sBidPrice = new Series();
                sBidPrice.Name = itemName;

                dtr = DBLibrary.GetDataReaderFromSelect(conn, sSQL);

                while (dtr.Read())
                {
                    DateTime date = Convert.ToDateTime(dtr["date"].ToString());
                    //Element ePriceRange = new Element();
                    Element eBidPrice = new Element();

                    //Bid Element
                    eBidPrice.ToolTip = dtr["ask"].ToString();
                    eBidPrice.SmartLabel.DynamicDisplay = false;
                    eBidPrice.SmartLabel.DynamicPosition = false;
                    eBidPrice.ShowValue = false;
                    eBidPrice.SmartLabel.Color = Color.White;
                    eBidPrice.XDateTime = date;                    
                    eBidPrice.SmartLabel.Text = "";
                    eBidPrice.YValue = Convert.ToDouble(dtr["ask"]);
                    eBidPrice.Name = Convert.ToDateTime(dtr["DATE"]).ToString("MMM dd");
                    sBidPrice.AddElements(eBidPrice);
                }
            }

            return sBidPrice;

        }

        
	}
}
