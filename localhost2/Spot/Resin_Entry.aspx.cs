using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using TPE.Utility;
using System.Configuration;

namespace localhost.Spot
{
	/// <summary>
	/// Summary description for Resin_Entry.
	/// </summary>
	public partial class Resin_Entry : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblDensity;
		protected System.Web.UI.WebControls.Label lblAdd;
		//protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.Literal Literal1;
		
		string ScreenMode;
		string State = "";
		string City = "";
		string Details = ""; 
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			//must logged in to access this page, out Demo users that cannot access that page
			//if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "S") && ((string)Session["Typ"] != "P")&& ((string)Session["Typ"] != "D")&& ((string)Session["Typ"] != "Demo"))
			//{
			//	Response.Redirect("/default.aspx");
			//}
			
			//Register the block to ignore Enter Key            

			HelperFunction.RegisterBlockNoEnterKeyPress(this);

			cmdSubmit.Attributes["OnMouseOver"] = "document.getElementById('" + cmdSubmit.ClientID +"').src='/images/buttons/submit_pressed.jpg';";
			cmdSubmit.Attributes["onMouseOut"] = "document.getElementById('" + cmdSubmit.ClientID +"').src='/images/buttons/submit_unpressed.jpg';";

     

			if (!IsPostBack)
			{
				if ((Request.QueryString["Market"]!=null) && ((string)Request.QueryString["Market"]=="International"))
					MarketScreenMode(false);
				else
					MarketScreenMode(true);

				LoadCompanyList();

				string compID = HelperFunction.getSafeStringFromDB(Session["Comp_Id"]);
				string comp = HelperFunction.getSafeStringFromDB(Session["Comp"]);
				string id = HelperFunction.getSafeStringFromDB(Session["Id"]);
				string typ = HelperFunction.getSafeStringFromDB(Session["Typ"]);
				

				if (InternalUser())
				{
					SetScreenModeUsers();
					pnlMarket.Visible = true;
					pnlNotes.Visible = true;
				}
				else if ((typ == "S") || (typ == "P") || (typ == "D")|| (typ == "Demo"))
					SetScreenModeCustomers(compID, comp, id, typ);
				else //Session["ID"] == null
					SetScreenModeCustomers("", "", "", "");



				HelperFunction.fillResin(ddlProduct, this.Context);
				//LoadProductList();
				LoadQualityList();
				LoadQuantityList();
				LoadShippingPoint();
				LoadTermsList();
				LoadExperireList();

				ShowConfirmation(false);

				// read querystring variables if Edit
				string strID = (string)Request.QueryString["Change"];
				string orderType = (string)Request.QueryString["type"];
				pnlSubmitAction.Visible = false;
				if (HelperFunction.getSafeStringFromDB(strID)!="")
				{
					lblID.Text = strID;
					QueryEditInformation(orderType, strID);
					
					//When editing users are not able to change the market
					lnkInternationalMarket.Visible = false;

					//When user is not internal, always use the option to Edit the item
					if (!InternalUser())
					{
						rdbEdit.Checked = true;
						pnlSubmitAction.Visible = false;
					}
					else
					{
						pnlSubmitAction.Visible = true;
					}

				}
				
				if (!pnlNonLoggedContactInfo.Visible && !pnlSelectCompany.Visible) // contact details shouldn't be visible if the sub groups aren't
					pnlContactDetails.Visible = false;


				//SetMonomerScreen();

				//somewhere else, not here!
				//detail.Attributes["OnClick"]="javascript:Address_PopUp()";//use the Attributes to set the javascript to the link control.
				//detail.Attributes["onmouseover"]="this.style.cursor='hand'";
			}
			else
			{

			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.cmvProduct.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(this.cmvProduct_ServerValidate);
			this.cmvProduct2.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(this.cmvProduct2_ServerValidate);
			this.cmvQuality.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(this.cmvQuality_ServerValidate);
			this.cvQuantity.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(this.cvQuantity_ServerValidate);
			this.cmvShipingPoint.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(this.cmvShipingPoint_ServerValidate);
			this.cmdSubmit.Click += new System.Web.UI.ImageClickEventHandler(this.cmdSubmit_Click);

		}
		#endregion

		private void LoadUsersLocationsList()
		{
			ddlUser.Items.Clear();
			ddlUser.Visible = false;

			if (ddlCompanies.SelectedItem.Value != "0")
			{
				// adding all the possible users within the company
				DataTable dtUsers = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(),"Select PERS_FRST_NAME + ' ' + PERS_LAST_NAME as PERS_NAME,PERS_ID FROM PERSON WHERE PERS_COMP="+ddlCompanies.SelectedItem.Value);
				foreach(DataRow dr in dtUsers.Rows)
				{
					ddlUser.Items.Add ( new ListItem ((string) dr["PERS_NAME"],dr["PERS_ID"].ToString() ) );
				}
				
				ddlUser.Visible = ddlCompanies.Visible;

				lblUserEditUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=" + ddlUser.SelectedValue + "&CompanyID=" + ddlCompanies.SelectedValue + "\">Edit User</a>";
				lblUserAddUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=0&CompanyID=0" + ddlCompanies.SelectedValue + "\">Add User</a>";

			}
		}

		private void SetMonomerScreen()
		{
		    if (ddlProduct.SelectedValue == "12" || ddlProduct.SelectedValue == "13" || ddlProduct.SelectedValue == "14" || ddlProduct.SelectedValue == "15" || ddlProduct.SelectedValue == "16")
		    {
			pnlQualitySpecs.Visible = false;
		    }
		    else
		    {
			pnlQualitySpecs.Visible = true;
		    }  
		}


		protected void ddlProduct_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		    if (ddlProduct.Text.Equals("Regrind/Repro"))
			txtProduct.Text = "Regrind/Repro";
		    //SetMonomerScreen();
		}


		protected void ddlTypeEntry_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (ddlTypeEntry.SelectedValue == "O")
			{
				SetLabelsSellResin();
			}
			else
			{
				SetLabelsRequestResin();
			}
		}

		protected void ddlCompanies_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			LoadUsersLocationsList();
		}

		protected void ddlUser_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		    lblUserEditUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=" + ddlUser.SelectedValue + "&CompanyID=" + ddlCompanies.SelectedValue + "\">Edit User</a>";
		    lblUserAddUser.Text = "<a target=new href=\"http://" + ConfigurationSettings.AppSettings["DomainName"] + "/administrator/Edit_User.aspx?UserID=0&CompanyID=0" + ddlCompanies.SelectedValue + "\">Add User</a>";
		}

		private void SetScreenModeCustomers(string companyID, string companyName, string userID, string userType)
		{
			this.ScreenMode = userType; //HelperFunction.CompanyType(this.Context, companyID)

			//change the lables depending on the type of user
			switch(this.ScreenMode.ToUpper())
			{
				case "S": //Suppliers
					SetScreenSellResin();
					break;
				case "D": //Distributor
					SetScreenSellRequestResin();
					break;
				default: //Purchaser, Demo or any other type
					SetScreenRequestResin();
					break;
			}
			
				SetCompanySelection(false);

			if (companyID!="") //Select the company of the user logged on
			{
				ddlCompanies.SelectedItem.Selected = false;
				ddlCompanies.Items.FindByValue(companyID).Selected = true;
			}
			else
			{
				if (userID!="") //If the user is logged on
				{
					string contact = Convert.ToString((Session["Name"].ToString() + "("+companyName +")"));
					if (contact.Length > 45) 
						contact = contact.Substring(0,41);
//					txtCompany.Text =  "<a href='/administrator/Contact_Details.aspx?ID="+userID+"'>" + contact +"</a>";
					txtCompany.Text = "<a href='/administrator/Contact_Details.aspx?ID=" + userID + "'>" + contact + "</a>";

				}
			}
			
			//Load all users and point
			ddlCompanies_SelectedIndexChanged(this, null);

			if (companyID!="")
			{
				//Select the user logged on
				ddlUser.SelectedItem.Selected = false;
				ddlUser.Items.FindByValue(userID).Selected = true;
			}

			if (userID=="") //If the user isn't logged on, show the contact fields
			{
				ShowContactInformation(true);
			}
			else
			{
				ShowContactInformation(false);
			}
		}

		private void SetScreenModeUsers()
		{
			SetScreenSellRequestResin();
			SetCompanySelection(true);
			
			ddlCompanies_SelectedIndexChanged(this, null);

			ShowContactInformation(false);
		}

		private void LoadCompanyList()
		{			
			//Top 10 looking at BBOFFERARCH
			string sqlStatement = "SELECT     TOP 10 C.COMP_ID, C.COMP_NAME, C.COMP_TYPE, QTT.SUM_QTD " +
				"FROM         COMPANY C INNER JOIN " +
				"(SELECT     ID, SUM(QTTF) AS SUM_QTD " +
				"FROM          (SELECT     COUNT(OFFR_ID) AS QTTF, OFFR_COMP_ID AS ID " +
				"FROM          BBOFFERARCH " +
				"WHERE      (OFFR_DATE BETWEEN DATEADD(day, - 90, GETDATE()) AND GETDATE()) AND OFFR_COMP_ID IS NOT NULL AND  " +
				"OFFR_COMP_ID <> 0 " +
				"GROUP BY OFFR_COMP_ID " +
				"UNION " +
				"SELECT     COUNT(BID_ID) AS QTTF, BID_COMP_ID AS ID " +
				"FROM         BBIDARCH " +
				"WHERE     (BID_DATE BETWEEN DATEADD(day, - 90, GETDATE()) AND GETDATE()) AND BID_COMP_ID IS NOT NULL AND  " +
				"BID_COMP_ID <> 0 " +
				"GROUP BY BID_COMP_ID) DERIVEDTBL " +
				"GROUP BY ID) QTT ON C.COMP_ID = QTT.ID " +
				"WHERE     (C.COMP_REG = 1) AND (C.COMP_TYPE IN ('P', 'S', 'D')) AND EXISTS " +
				"(SELECT PERS_ID FROM PERSON WHERE PERS_COMP = COMP_ID) " +
				"ORDER BY QTT.SUM_QTD DESC, C.COMP_NAME";
			DataTable dtCompanies = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), sqlStatement);
			ddlCompanies.Items.Add ( new ListItem ("Select Company","0") );
			foreach(DataRow dr in dtCompanies.Rows)
			{
				ddlCompanies.Items.Add ( new ListItem ((string) dr["COMP_NAME"],dr["COMP_ID"].ToString() ) );
			}
			            
			//Rest of them, excluding top 10
			sqlStatement = "SELECT     COMP_ID, COMP_NAME, COMP_TYPE " +
				"FROM         COMPANY C " +
				"WHERE     (COMP_REG = 1) AND (COMP_TYPE IN ('P', 'S', 'D')) AND EXISTS " +
				"(SELECT     PERS_ID " +
				"FROM          PERSON " +
				"WHERE      PERS_COMP = COMP_ID) AND (COMP_ID NOT IN " +
				"(SELECT     TOP 10 C.COMP_ID " +
				"FROM          COMPANY C INNER JOIN " +
				"(SELECT     ID, SUM(QTTF) AS SUM_QTD " +
				"FROM          (SELECT     COUNT(OFFR_ID) AS QTTF, OFFR_COMP_ID AS ID " +
				"FROM          BBOFFERARCH " +
				"WHERE      (OFFR_DATE BETWEEN DATEADD(day, - 90, GETDATE()) AND GETDATE()) AND OFFR_COMP_ID IS NOT NULL " +
				"AND OFFR_COMP_ID <> 0 " +
				"GROUP BY OFFR_COMP_ID " +
				"UNION " +
				"SELECT     COUNT(BID_ID) AS QTTF, BID_COMP_ID AS ID " +
				"FROM         BBIDARCH " +
				"WHERE     (BID_DATE BETWEEN DATEADD(day, - 90, GETDATE()) AND GETDATE()) AND BID_COMP_ID IS NOT NULL AND " +
				"BID_COMP_ID <> 0 " +
				"GROUP BY BID_COMP_ID) AS TABLE1 " +
				"GROUP BY ID) QTT ON C.COMP_ID = QTT.ID " +
				"WHERE      (C.COMP_REG = 1) AND (C.COMP_TYPE IN ('P', 'S', 'D')) AND EXISTS " +
				"(SELECT PERS_ID FROM PERSON WHERE PERS_COMP = COMP_ID) " +
				"ORDER BY QTT.SUM_QTD DESC, C.COMP_NAME)) " +
				"ORDER BY COMP_NAME";
			dtCompanies = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), sqlStatement);
			ddlCompanies.Items.Add ( new ListItem (" --------------------","0") );
			foreach(DataRow dr in dtCompanies.Rows)
			{
				ddlCompanies.Items.Add ( new ListItem ((string) dr["COMP_NAME"],dr["COMP_ID"].ToString() ) );
			}			
		}

		//private void LoadProductList()
		//{
		//        // creating product drop-down list manually
		//        //ddlProduct.Items.Add(new ListItem("Select Product Grade","0"));
		//        //HelperFunction.LoadGrades(this.Context, ddlProduct, "Grade_ID");
		//        //ddlProduct.Items.Add(new ListItem("Others", "0"));

		//        HelperFunction.fillResin(ddlProduct, this.Context);
		//}

		private void LoadShippingPoint()
		{
            ddlDeliveryPoint.Items.Clear();
			if (lblMarketMode.Text=="International")
			{
				txtZipCode.Visible = false;
				lblShippingPointDomestic.Visible = false;
				lblShippingPointZip.Visible = false;
				lblShippingPointOr.Visible = false;
				//Load Ports
				ddlDeliveryPoint.Items.Clear();
			
				ddlDeliveryPoint.Items.Add(new ListItem("Select Port...",""));
				DataTable dtPorts = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), "SELECT IP.PORT_ID, C.CTRY_NAME  + ISNULL(' - '+NULLIF(IP.PORT_CITY,'') ,'')  PORT FROM INTERNATIONAL_PORT IP, COUNTRY C WHERE IP.PORT_COUNTRY = C.CTRY_CODE ORDER BY C.CTRY_NAME");
				foreach(DataRow dr in dtPorts.Rows)
				{
					ddlDeliveryPoint.Items.Add(new ListItem(dr["PORT"].ToString(),dr["PORT_ID"].ToString()));
				}
			}
			else
			{
				txtZipCode.Visible = true;
				lblShippingPointZip.Visible = true;
				lblShippingPointOr.Visible = true;
				lblShippingPointDomestic.Visible = true;
				//Load the Most Popular Cities
				HelperFunctionDistances hfd = new HelperFunctionDistances();
				hfd.loadMostPopularCities(ddlDeliveryPoint, Session["Typ"].ToString());
				
				if (HelperFunction.getSafeStringFromDB(Session["Typ"]) == "A")
				{
					ddlDeliveryPoint.SelectedIndex = 3;
				}
			}
		}

		private void LoadQualityList()
		{	
//			if (lblMarketMode.Text=="International")
//			{
//				lblQuality.Visible = false;
//				ddlQuality.Visible = false;
//				
//			}
//			else
//			{
				//Quantity drop-down list
				ddlQuality.Visible = true;
				ddlQuality.Items.Clear();
				ddlQuality.Items.Add(new ListItem ("Select Quality",""));
				ddlQuality.Items.Add(new ListItem ("Prime","P"));
				ddlQuality.Items.Add(new ListItem ("Offgrade","O"));
				ddlQuality.Items.Add(new ListItem ("Scrap/Regrind/Repro","R"));

				string typ = HelperFunction.getSafeStringFromDB(Session["Typ"]);
				if (InternalUser())
				{
					ddlQuality.SelectedItem.Selected = false;
					ddlQuality.Items[1].Selected = true;
				}
//			}
		}

		private void LoadQuantityList()
		{
            ddlQty.Items.Clear();
			if (lblMarketMode.Text=="International")
			{
				//Container is available for Export
				ddlQty.Items.Add(new ListItem ("20' Container","38500"));
				ddlQty.Items.Add(new ListItem ("40' Container","44500"));
			}
			else
			{
				//Quantity drop-down list
				ddlQty.Items.Add(new ListItem ("Railcar","190000"));
				ddlQty.Items.Add(new ListItem ("Bulk Truck","45000"));
				ddlQty.Items.Add(new ListItem("Barrel", "5000"));
				ddlQty.Items.Add(new ListItem ("Truckload Boxes","42000"));
				ddlQty.Items.Add(new ListItem ("Truckload Bags","44092"));				
				ddlQty.Items.Add(new ListItem ("Pounds (lbs)","1"));
				ddlQty.Items.Add(new ListItem ("Metric Tonnes","2205"));
			}
		}

		private void LoadTermsList()
		{
            ddlTerms.Items.Clear();
			if (lblMarketMode.Text=="International")
			{
				//Extra terms for Export
				ddlTerms.Items.Add(new ListItem("CFR", "CFR"));
				ddlTerms.Items.Add(new ListItem("CIF", "CIF"));
				ddlTerms.Items.Add(new ListItem("FAS", "FAS"));
				ddlTerms.Items.Add(new ListItem("FCA", "FCA"));
				ddlTerms.Items.Add(new ListItem("FOB", "FOB"));
				ddlTerms.Items.Add(new ListItem("DDU", "DDU"));
				ddlTerms.Items.Add(new ListItem("DDP", "DDP"));
				ddlTerms.Items.Add(new ListItem("DAF", "DAF"));
			}
			else
			{
				//Payment Terms List
				ddlTerms.Items.Add(new ListItem ("FOB Delivered","FOB/Delivered"));
				ddlTerms.Items.Add(new ListItem ("FOB Shipping","FOB/Shipping"));
			}
		}

		private void LoadExperireList()
		{

			//Expire Drop-down List
			ddlExpires.Items.Add(new ListItem ("End of Day","0"));
			ddlExpires.Items.Add(new ListItem ("One week","7"));
			ddlExpires.Items.Add(new ListItem ("Two weeks","14"));
			ddlExpires.Items.Add(new ListItem ("One month","30"));

			int DaysInCurrentMonth = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
			int CurrentDay = DateTime.Now.Day;


			ddlExpires.Items.Add(new ListItem("EOM", Convert.ToString(DaysInCurrentMonth- CurrentDay)));

			ddlExpires.Items[1].Selected = true;
		}

		private void SetScreenSellResin()
		{
			SetLabelsSellResin();
			ddlTypeEntry.Items.Clear();
			ListItem item = new ListItem("Sell Resin","O");
			ddlTypeEntry.Items.Add(item);
			
			pnlTypeofEntry.Visible = false;
			pnlDeliveryTerms.Visible = true;
			pnlExpiration.Visible = true;
			
		}

		private void SetLabelsSellResin()
		{
			string typ = HelperFunction.getSafeStringFromDB(Session["Typ"]);
			if (InternalUser())
				lblPrice.Text = "TPE Buy Price:"; 
			else
				lblPrice.Text = "Price:";
			
			lblMarkup.Text = "Markup";

			if (lblMarketMode.Text == "Domestic")
			{
				//lblPrice.Text += "0.";
				cpvPrice.Type = ValidationDataType.Integer;
				
				//lblMarkup.Text += "0.";
				cpvMarkup.Type = ValidationDataType.Integer;
				txtMarkup.Text = "02";
			}
			else
			{
				lblPriceMeasure.Text = "per metric tons.";
				lblPricePrefix.Visible = false;
				txtPrice.MaxLength = 9;
				cpvPrice.Type = ValidationDataType.Currency;

				lblMarkupMeasure.Text = "per metric tons.";
				lblMarkupPrefix.Visible = false;
				txtMarkup.MaxLength = 9;
				cpvMarkup.Type = ValidationDataType.Currency;
				txtMarkup.Text = "40.00";
			}
				
			//lblTitle.Text = "Sell Resin";
			lblExpiration.Text = "Sell Expiration:";
			lblPoint.Text="Shipping Point:";
		}

		private void SetScreenRequestResin()
		{
			SetLabelsRequestResin();
			ddlTypeEntry.Items.Clear();
			ListItem item = new ListItem("Request Resin","B");
			ddlTypeEntry.Items.Add(item);
			
			pnlTypeofEntry.Visible = false;
			
			string typ = HelperFunction.getSafeStringFromDB(Session["Typ"]);
			if (!InternalUser())
			{
				pnlDeliveryTerms.Visible = false;
			}
			
			if ((typ == "P") || (typ == "Demo") || (Session["ID"] == null))
			{
				pnlExpiration.Visible =false;
				//lblExpiration.Visible = false;
				//ddlExpires.Visible = false;
			}
			else
			{
				pnlExpiration.Visible = true;
				//blExpiration.Visible = true;
				//ddlExpires.Visible = true;
			}

			if (ddlTerms.Items.Count>0)
			{
				ddlTerms.SelectedItem.Selected = false;
				ddlTerms.Items[0].Selected = true;
			}
		}

		private void SetLabelsRequestResin()
		{
			string typ = HelperFunction.getSafeStringFromDB(Session["Typ"]);
			if (InternalUser())
				lblPrice.Text = "TPE Buy:";
			else
				lblPrice.Text = "Price:";
			
			if (lblMarketMode.Text == "Domestic") 
			{
				//lblPrice.Text += "0.";
				cpvPrice.Type = ValidationDataType.Integer;
			}
			else
			{
				lblPriceMeasure.Text = "per metric tons.";
				lblPricePrefix.Visible = false;
				txtPrice.MaxLength = 9;
				cpvPrice.Type = ValidationDataType.Currency;
			}
			
			//lblTitle.Text = "Request Resin";
			lblExpiration.Text = "Request Expiration:";
			lblPoint.Text="Delivery Point:";
		}

		private void SetScreenSellRequestResin()
		{
			ddlTypeEntry.Items.Clear();
			//Checking if there is a parameter setting Request as default
			if ((Request.QueryString["RequestResin"] != null) && (Request.QueryString["RequestResin"].ToString() == "true"))
			{
				ListItem item = new ListItem("Request Resin","B");
				ddlTypeEntry.Items.Add(item);
				item = new ListItem("Sell Resin","O");
				ddlTypeEntry.Items.Add(item);
			}
			else
			{
				ListItem item = new ListItem("Sell Resin","O");
				ddlTypeEntry.Items.Add(item);
				item = new ListItem("Request Resin","B");
				ddlTypeEntry.Items.Add(item);
			}

			ddlTypeEntry_SelectedIndexChanged(this,null);
		}

		private void SetCompanySelection(bool permitCompanySelection)
		{
			pnlSelectCompany.Visible  = permitCompanySelection;
			lblCompany.Visible = permitCompanySelection;
			ddlCompanies.Visible = permitCompanySelection;
			
			txtCompany.Visible = permitCompanySelection;
			
			txtMarkup.Visible = permitCompanySelection;
			lblMarkup.Visible = permitCompanySelection;
			lblMarkupMeasure.Visible = permitCompanySelection;
			cpvMarkup.Visible = permitCompanySelection;
			rfvMarkup.Visible = permitCompanySelection;
			pnlMarkup.Visible = permitCompanySelection;
			
		}

		private void ShowConfirmation(bool Show)
		{
			pnlMain.Visible = !Show;
			pnlConfirmation.Visible = Show;
			lblMessage.Visible = Show;
			cmdYes.Visible = Show;
			cmdNo.Visible = Show;
		}
		
		private string CreateConfirmationTable(ArrayList arItems)
		{
			string htmlTable = "<table align='center' cellSpacing=0 border=0>"; //width='100%'
			//htmlTable +="<TR><TD colspan = 2 align='center'><B><font size='2pt'>"+arItems[0].ToString()+"</font></B></TD></TR>";
			htmlTable +="<TR><TD colspan = 2>&nbsp;</TD></TR>";
			
			for(int i=0;i<arItems.Count;i++)
			{
				htmlTable += "<TR><TD nowrap=true colspan = 2>"+arItems[i].ToString()+"</TD>";
//				if (i+1 < arItems.Count) //Add the second value
//					htmlTable += "<TD colspan = 2>"+arItems[i+1].ToString()+"</TD>";
//				else
//					htmlTable += "<TD>&nbsp;</TD>";
				htmlTable +="</TR>";
			}
			htmlTable +="</table>";

			return htmlTable;
		}

		protected void cmdYes_Click(object sender, System.EventArgs e)
		{			
			lblMessage.Text = "";
			string procedureName = "";
			string idParameter = "";
			string emailUser= "";
            string commentParameter = "";
			if(ddlTypeEntry.SelectedValue == "O")
			{
				procedureName = "spAddOffer";
				idParameter = "@OfferID";
                commentParameter = "@OFFR_COMMENT";
			}
			else
			{
				procedureName = "spAddBid";
				idParameter = "@BidID";
                commentParameter = "@BID_COMMENT";
			}

			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
						
			Hashtable AddNew_parameters = new Hashtable();
			bool bNew = false; //if the user selected that the record must be new
            if (lblID.Text != "")
            {
                string orderType = (string)Request.QueryString["type"];
                Hashtable param = new Hashtable();

                if (pnlSubmitAction.Visible)
                {
                    if (rdbNew.Checked)
                    {
                        string SQL = "";
                        if (orderType == "offer")
                        {
                            SQL = "spSendToArchive";
                            param.Add("@ID", lblID.Text);
                        }
                        else
                        {
                            SQL = "spSendToArchive";
                            param.Add("@ID", lblID.Text);
                            param.Add("@Is_Bid", "1");
                        }

                        DBLibrary.ExecuteStoredProcedureWithoutScrub(Application["DBConn"].ToString(), SQL, param);
                        bNew = true;
                    }
                }

                if (((orderType == "offer") && (ddlTypeEntry.SelectedValue == "B")) || ((orderType == "bid") && (ddlTypeEntry.SelectedValue == "O")))
                {
                    Hashtable par = new Hashtable();

                    if (orderType == "offer")
                    {
                        param.Add("@OFFR_ID", lblID.Text);
                        DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), "DELETE FROM BBOFFER WHERE OFFR_ID = @OFFR_ID", par);
                    }
                    else
                    {
                        param.Add("@BID_ID", lblID.Text);
                        DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), "DELETE FROM BBID WHERE BID_ID = @BID_ID", par);
                    }
                }
                else
                {
                    if (bNew == false)
                    {
                        AddNew_parameters.Add(idParameter, lblID.Text);
                        //AddNew_parameters.Add(commentParameter, txtNotes.Text);
                    }
                }
                AddNew_parameters.Add(commentParameter, txtNotes.Text);
            }

            else
            {
                AddNew_parameters.Add(idParameter, null);
                AddNew_parameters.Add(commentParameter, txtNotes.Text);
            }

            string typUser = HelperFunction.getSafeStringFromDB(Session["Typ"]);
            
            if (ddlCompanies.SelectedItem.Value == "0") //if (ddlCompanies.SelectedItem.Value=="0")
			{
				if (Session["ID"]==null) //the user is not logged on
				{
					AddNew_parameters.Add("@CompanyDescription", DBLibrary.ScrubSQLStringInput(txtName.Text) + "<BR>" + DBLibrary.ScrubSQLStringInput(txtPhone.Text) + "<BR>" + DBLibrary.ScrubSQLStringInput(txtEmail.Text));
					emailUser = txtEmail.Text;
					lblMessage.Text += "<font color=red>"+txtName.Text + "</font> ";
				}
				else
				{
                    if (typUser == "Demo") // for Demo user just store his company name or his name
                    {
                        
                        string comp_desc = "";
                        if ((Session["COMP"] != null) && (Session["COMP"].ToString().Trim().Length > 0))
                        {
                            comp_desc = Session["COMP"].ToString();
                        }
                        else
                        {
                            comp_desc = Session["NAME"].ToString();
                        }

                        AddNew_parameters.Add("@CompanyDescription", comp_desc);
                        lblMessage.Text += "<font color=red>" + DBLibrary.ScrubSQLStringInput(txtCompany.Text) + "</font> ";

                    }
                    else if ((typUser == "A") || (typUser == "B") || (typUser == "T") || (typUser == "L") || (typUser == ""))
                    {
                        AddNew_parameters.Add("@CompanyDescription", DBLibrary.ScrubSQLStringInput(txtCompany.Text));
                    }
				}
				
				AddNew_parameters.Add("@Comp_Id",null);

                if ((typUser == "A") || (typUser == "B") || (typUser == "T") || (typUser == "L") || (typUser == ""))
					AddNew_parameters.Add("@Pers_Id",null);
				else
					AddNew_parameters.Add("@Pers_Id",Session["Id"].ToString());
			}
			else
			{
				// use the company of the dropDownList
				AddNew_parameters.Add("@CompanyDescription",null);
				AddNew_parameters.Add("@Comp_Id",ddlCompanies.SelectedItem.Value);
				AddNew_parameters.Add("@Pers_Id",ddlUser.SelectedItem.Value);
				lblMessage.Text +="<font color='red'>"+ddlCompanies.SelectedItem.ToString() +"</font> ";
			}
			
			if(ddlTypeEntry.SelectedValue == "O")
				lblMessage.Text += "offers ";
			else
				lblMessage.Text += "requests ";

			// # of items
			AddNew_parameters.Add("@Quantity",txtQty.Text);
			lblMessage.Text += " "+"<font color='red'>"+txtQty.Text+"</font> ";
			// of size
			AddNew_parameters.Add("@Size",Int32.Parse(ddlQty.SelectedItem.Value));
			lblMessage.Text += "<font color='red'>" + ddlQty.SelectedItem.ToString() +"</font> of ";
			
			string quality="";
			//if ((lblMarketMode.Text!="International")&&(ddlQuality.SelectedIndex>0))
			//{
				quality = "(" + ddlQuality.SelectedItem.Text + ")";
			//}

			// other product overwrite the dropdown list
			if (txtProduct.Text.Trim()!="")	//if (ddlProduct.SelectedValue=="0")
			{
				AddNew_parameters.Add("@Grade",null);
				AddNew_parameters.Add("@ProductDescription",txtProduct.Text);
				lblMessage.Text += "<font color='red'>"+txtProduct.Text+quality+"</font>";
			}
			else
			{
				AddNew_parameters.Add("@Grade",ddlProduct.SelectedValue);
				AddNew_parameters.Add("@ProductDescription",ddlProduct.SelectedItem.ToString());
				lblMessage.Text += "<font color='red'>"+ddlProduct.SelectedItem.ToString()+quality+"</font>";
			}

			string markup;
			string typ = HelperFunction.getSafeStringFromDB(Session["Typ"]);
			if (lblMarketMode.Text =="International")
			{
				double price = System.Convert.ToDouble(txtPrice.Text);
				lblMessage.Text += " at <font color='red'>$" + txtPrice.Text +"</font>" ;

				if (!InternalUser())
				{
					if(ddlTypeEntry.SelectedValue != "O")
					{
						decimal TPEBuyPrice = System.Convert.ToDecimal(txtPrice.Text) - System.Convert.ToDecimal(txtMarkup.Text);
						AddNew_parameters.Add("@BuyPrice",System.Convert.ToString(System.Convert.ToDouble(TPEBuyPrice) * 0.00045359237));
					}
					else
					{
						AddNew_parameters.Add("@BuyPrice",System.Convert.ToString(price * 0.00045359237));
					}
				}
				else
				{
					AddNew_parameters.Add("@BuyPrice",System.Convert.ToString(price * 0.00045359237));
				}

				markup = txtMarkup.Text;
				double markupValue = System.Convert.ToDouble(txtMarkup.Text);
				AddNew_parameters.Add("@Markup",System.Convert.ToString(markupValue * 0.00045359237));
			}
			else
			{
				lblMessage.Text += " at <font color='red'>$0." + txtPrice.Text +"</font>";
				if (!InternalUser())
				{
					if(ddlTypeEntry.SelectedValue != "O")
					{
						decimal TPEBuyPrice = System.Convert.ToDecimal("0." + txtPrice.Text) - System.Convert.ToDecimal("0." + txtMarkup.Text);
						AddNew_parameters.Add("@BuyPrice",TPEBuyPrice);
					}
					else
					{
						AddNew_parameters.Add("@BuyPrice","0." + txtPrice.Text);
					}
				}
				else
				{
					AddNew_parameters.Add("@BuyPrice","0." + txtPrice.Text);
				}
				markup = "0." + txtMarkup.Text;
				AddNew_parameters.Add("@Markup",markup);
			}
			
			if (InternalUser())
				lblMessage.Text += " and markup of <font color='red'>$" + markup +"</font>. </BR> " ;
			else
				lblMessage.Text += ". </BR> " ;

			if (!txtMelt.Text.Equals(""))
			{
				AddNew_parameters.Add("@Melt",txtMelt.Text);
				lblMessage.Text += " Melt: " +"<font color='red'>"+txtMelt.Text+"</font>";
			}
			if (!txtDensity.Text.Equals(""))
			{
				AddNew_parameters.Add("@Dens","0."+txtDensity.Text);
				lblMessage.Text += " Density: " +"<font color='red'>0."+txtDensity.Text+"</font>";
			}
			if (!txtAdds.Text.Equals(""))
			{
				AddNew_parameters.Add("@Adds",txtAdds.Text);
				lblMessage.Text += " Adds: " +"<font color='red'>"+txtAdds.Text+"</font>";
			}
						
			AddNew_parameters.Add("@PlaceID",null);
			AddNew_parameters.Add("@LoclID",null);
			AddNew_parameters.Add("@LocalDescription",null);
			if (lblMarketMode.Text !="International")
			{
				if (txtZipCode.Text != "")
				{
					AddNew_parameters.Add("@ZipCode",txtZipCode.Text);
					
					if (ddlTerms.SelectedItem.Value=="FOB/Delivered")
						lblMessage.Text += " delivered to <font color='red'> " + City + "(" + State +")</font> ";
					else
						lblMessage.Text += " out of <font color='red'> " + City + "(" + State +")</font> ";
				}
				else
				{
					AddNew_parameters.Add("@ZipCode",ddlDeliveryPoint.SelectedItem.Value);
					if (ddlTerms.SelectedItem.Value=="FOB/Delivered")
						lblMessage.Text += " delivered to <font color='red'> " +ddlDeliveryPoint.SelectedItem.Text.ToString()+"</font> ";
					else
						lblMessage.Text += " out of <font color='red'> " +ddlDeliveryPoint.SelectedItem.Text.ToString()+"</font> ";
				}
			}
			else
			{
				AddNew_parameters.Add("@PortID",ddlDeliveryPoint.SelectedItem.Value);
				lblMessage.Text += " out of <font color='red'> " +ddlDeliveryPoint.SelectedItem.Text.ToString()+"</font> ";
			}
				
			AddNew_parameters.Add("@ResinDetails",txtDetail.Text);

			AddNew_parameters.Add("@Terms",ddlTerms.SelectedItem.Value);
			if (ddlTerms.SelectedItem.Value!="FOB/Delivered")
			{
				lblMessage.Text += "<font color='red'>"+ddlTerms.SelectedItem.ToString()+"</font>";
			}
			
			AddNew_parameters.Add("@DaysExpiration",ddlExpires.SelectedItem.Value);

			//if ((lblMarketMode.Text!="International")&&(ddlQuality.SelectedIndex>0))
			//{
				AddNew_parameters.Add("@Quality",ddlQuality.SelectedItem.Value);
			//}

			AddNew_parameters.Add("@Market",rblMarket.SelectedValue);

			//Store the PERS_ID only if the user is a Broker
			if ((typ == "B") || (typ == "A") || (typ == "T") || (typ == "L"))
			{
				AddNew_parameters.Add("@Broker",Session["ID"].ToString());
			}
			
			DBLibrary.ExecuteStoredProcedureWithoutScrub(Application["DBConn"].ToString(), procedureName, AddNew_parameters);
						
			string sqlID = "SELECT @@IDENTITY AS New";			
			SqlDataReader dtOrder = DBLibrary.GetDataReaderFromSelect(conn, sqlID);
			string strOrderNum = String.Empty;
			try
			{
				dtOrder.Read();
				strOrderNum = dtOrder[0].ToString();
			}
			finally
			{
				conn.Close();
			}

			if ((typ != "A") && (typ != "B") && (typ != "L"))
			{
				SendEmail(strOrderNum, emailUser);

				lblMessage.Text = "<BR><BR><BR><BR><BR>";
				if (ddlTypeEntry.SelectedValue == "O")
				{
					lblMessage.Text +="<font color='black' size='2pt'>Congratulations, resin offer <B>" + strOrderNum + "</B> has been successfully posted online.  There are no seller fees associated with trading on our system. Please note that The Plastics Exchange added a $." + txtMarkup.Text +" markup on this offer when it was posted on our Spot Floor.</font>";
					cmdNewEntry.Text = "Post another offer online";
				}
				else
				{
					lblMessage.Text +="<font color='white' size='3pt'>Your request has been successfully processed. We will be searching for your resin and be in contact with you soon.</font>";
					cmdNewEntry.Text = "Make another Request.";
				}
				lblMessage.Text += "<BR><BR><BR><BR>" + Details + "<BR><BR><BR>";
				cmdNewEntry.Visible = true;
				cmdSpotFloor.Visible = true;
				
				if (Session["Id"]==null) cmdSpotFloor.Visible = false;
			}
			
			//Send email letting people aware about new emails
			string[] emails = {"larry@theplasticsexchange.com"};
			SendAwareEmail(strOrderNum,emails);

			pnlConfirmation.Visible = true;
			cmdYes.Visible = false;
			cmdNo.Visible = false;

			//if (lblID.Text != "") //Editing
			//{
			//	cmdCancel_Click(this,null);
			//}
		}

		private void SendEmail(string number, string email)
		{
			// send email to alert Zach and Mike after reopen the task
			MailMessage mail;
			mail = new MailMessage();

			//Getting the lead's email
			string emailBroker = "";
			if (Session["ID"]!=null) 
				emailBroker = HelperFunction.EmailBroker(this.Context,Session["ID"].ToString());

			// get users email address
			string emailUser=email;
			if (emailUser=="")
			{
				SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
				try
				{
					conn.Open();
			
					Hashtable param = new Hashtable();
					param.Add("@PERS_ID",Session["ID"].ToString());
					emailUser = DBLibrary.ExecuteScalarSQLStatement(Application["DBConn"].ToString(),"Select PERS_MAIL FROM PERSON WHERE PERS_ID=@PERS_ID",param).ToString();
				}
				finally
				{
					conn.Close();
				}
			}
//			if (Session["Email"]==null)
//			{
//				mail.From = txtEmail.Text;
//			}
//			else
//			{
//				mail.From = Session["Email"].ToString();
//			}
			mail.From = "system@theplasticsexchange.com";

			mail.Subject = emailUser + " placed a " + ddlTypeEntry.SelectedItem.Text;

			if ((emailBroker != "") && (emailBroker.ToUpper()!="MIKE@THEPLASTICEXCHANGE.COM") && (emailBroker.ToUpper()!="MICHAEL@THEPLASTICEXCHANGE.COM")) 
			{
				mail.To = emailBroker;
			}
			
			if (mail.To==null)
			{
				mail.To = Application["strEmailOwner"].ToString();
                mail.Cc = Application["strEmailAdmin"].ToString() + "; dominick@theplasticexchange.com";
			}
			else
			{
				mail.Cc = Application["strEmailAdmin"].ToString() + ";" + Application["strEmailOwner"].ToString()+"; dominick@theplasticexchange.com";
			}

			mail.Body = HelperFunction.getEmailHeaderHTML() + "<BR><u>" + ddlTypeEntry.SelectedItem.Text + " number: " + number;
			mail.Body += "</u><BR><br><b>Confirmation Dialog:</b><BR>"+ lblMessage.Text + "<BR>";
			if ((Session["UserName"]!=null) && (Session["Password"]!=null))
			{
				mail.Body += HelperFunction.getUserInformationHTML(Session["UserName"].ToString(), Session["Password"].ToString(),mail.To,this.Context);
			}
			else
			{
				mail.Body += "<br><b>Name</b>:" + txtName.Text;
				mail.Body += "<br><b>E-mail</b>:" + txtEmail.Text;
				mail.Body += "<br><b>Phone</b>:" + txtPhone.Text;
			}
			mail.Body += "<BR><BR>" + HelperFunction.getEmailFooterHTML();
			mail.BodyFormat = MailFormat.Html;
			//SmtpMail.SmtpServer="caesar";
			
			TPE.Utility.EmailLibrary.Send(Context,mail);			
		}

		private void SendAwareEmail(string number, string[] emails)
		{
			// send email to Brokers that need to be aware about new offers/bids
			MailMessage mail;
			mail = new MailMessage();

			mail.From = Application["strEmailOwner"].ToString();
			mail.Subject = "New " + ddlTypeEntry.SelectedItem.Text + " was placed.";

			for(int i=0;i<emails.Length;i++)
			{
				mail.To = emails[i].ToString();
				mail.Body = HelperFunction.getEmailHeaderHTML() + "<BR><u>" + ddlTypeEntry.SelectedItem.Text + " number: " + number;
				mail.Body += "</u><BR><br><b>Confirmation Dialog:</b><BR>"+ lblMessage.Text + "<BR>";
				mail.Body += "<BR><BR>" + HelperFunction.getEmailFooterHTML();
				mail.BodyFormat = MailFormat.Html;
				//SmtpMail.SmtpServer="caesar";
				TPE.Utility.EmailLibrary.Send(Context,mail);
			}
		}

		private void QueryEditInformation(string typeEntry, string entryNumber)
		{
			//query the bid or offer info
			string sqlStatement = "";
			if (typeEntry=="bid")
			{
				sqlStatement = "Select ISNULL(BID_PRCE_SELL-BID_PRCE,0) MARKUP,*, EXPR=convert(varchar, BID_EXPR, 101), BID_MARKET, BID_COMMENT FROM BBID WHERE BID_ID=" + entryNumber;
				SetScreenRequestResin();

				ddlTypeEntry.Items.Clear();
				ListItem item = new ListItem("Request Resin","B");
				ddlTypeEntry.Items.Add(item);
				item = new ListItem("Sell Resin","O");
				ddlTypeEntry.Items.Add(item);
			}
			else
			{
				sqlStatement = "Select ISNULL(OFFR_PRCE-OFFR_PRCE_BUY,0.02) MARKUP,ISNULL(OFFR_PRCE_BUY, OFFR_PRCE-ISNULL(OFFR_PRCE-OFFR_PRCE_BUY,0.02)) OFFR_PRCE_BUY_REAL,*, EXPR=convert(varchar, OFFR_EXPR, 101), OFFR_MARKET, OFFR_COMMENT FROM BBOFFER WHERE OFFR_ID=" + entryNumber;
				SetScreenSellResin();

				ddlTypeEntry.Items.Clear();
				ListItem item = new ListItem("Sell Resin","O");
				ddlTypeEntry.Items.Add(item);
				item = new ListItem("Request Resin","B");
				ddlTypeEntry.Items.Add(item);
			}
			ddlTypeEntry.Visible = true;
			lblTypeEntry.Visible = true;

			DataTable dtEntry = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), sqlStatement);
			DataRow rdEntry = dtEntry.Rows[0];
			
			string size;
			string product;
			string grade;
			string CompID = "";
			string CompanyDescription = "";
			string PersID = "";
			string PlaceID = "";
			string PortID = "";
			string ZipCode = "";
			string Terms = "";
			string Quality = "";
			string market = "";
			DateTime Expiration;
			if (typeEntry=="bid")
			{
				txtDetail.Text = HelperFunction.getSafeStringFromDB(rdEntry["BID_DETL"]).Trim();
				txtQty.Text = HelperFunction.getSafeStringFromDB(rdEntry["BID_QTY"]).Trim();
				txtMelt.Text = HelperFunction.getSafeStringFromDB(rdEntry["BID_MELT"]).Trim();
				txtNotes.Text = HelperFunction.getSafeStringFromDB(rdEntry["BID_COMMENT"]).Trim();
				
				string density = HelperFunction.getSafeStringFromDB(rdEntry["BID_DENS"]).Trim();
				if (density.Length>2) 
				{
					int positionDot = density.IndexOf('.');
					txtDensity.Text = density.Substring(positionDot+1);
					//txtDensity.Text = HelperFunction.getSafeStringFromDB(rdEntry["BID_DENS"]).Trim().Substring(2);
				}
				else
					txtDensity.Text = HelperFunction.getSafeStringFromDB(rdEntry["BID_DENS"]).Trim();
				txtAdds.Text = HelperFunction.getSafeStringFromDB(rdEntry["BID_ADDS"]).Trim();

				CompID = HelperFunction.getSafeStringFromDB(rdEntry["BID_COMP_ID"]);
				PersID = HelperFunction.getSafeStringFromDB(rdEntry["BID_PERS_ID"]);
				CompanyDescription = HelperFunction.getSafeStringFromDB(rdEntry["BID_COMP"]).Trim();
				
				Terms = HelperFunction.getSafeStringFromDB(rdEntry["BID_TERM"]);
				
				if (lblMarketMode.Text == "International")
				{
					double price = System.Convert.ToDouble(InternalUser() ? rdEntry["BID_PRCE"].ToString() : rdEntry["BID_PRCE_SELL"].ToString());
					double markup = System.Convert.ToDouble(rdEntry["MARKUP"].ToString());
					txtPrice.Text = System.Convert.ToString(Math.Round(price / 0.00045359237));
					txtMarkup.Text = System.Convert.ToString(Math.Round(markup / 0.00045359237,2));
				}
				else
				{
					if (HelperFunction.getSafeStringFromDB(InternalUser() ? rdEntry["BID_PRCE"] : rdEntry["BID_PRCE_SELL"]).Trim().Length>2)
						txtPrice.Text = HelperFunction.getSafeStringFromDB(InternalUser() ? rdEntry["BID_PRCE"] : rdEntry["BID_PRCE_SELL"]).ToString().Trim().Substring(2);
					else
						txtPrice.Text = HelperFunction.getSafeStringFromDB(InternalUser() ? rdEntry["BID_PRCE"] : rdEntry["BID_PRCE_SELL"]).Trim();
					if (rdEntry["MARKUP"].ToString() != "0") txtMarkup.Text = rdEntry["MARKUP"].ToString().Trim().Substring(2);
				}
				Expiration = System.Convert.ToDateTime(rdEntry["EXPR"].ToString());
				size = rdEntry["BID_SIZE"].ToString();
				product = rdEntry["BID_PROD"].ToString();
				grade = rdEntry["BID_GRADE"].ToString();

				//txtDeliveryPoint.Text = HelperFunction.getSafeStringFromDB(rdEntry["BID_TO_LOCL_NAME"]);
				//PlaceID = HelperFunction.getSafeStringFromDB(rdEntry["BID_TO"]);
				PortID = HelperFunction.getSafeStringFromDB(rdEntry["BID_PORT"]);
				ZipCode = HelperFunction.getSafeStringFromDB(rdEntry["BID_ZIP"]);
				Quality = HelperFunction.getSafeStringFromDB(rdEntry["BID_QUALT"]);
				market = HelperFunction.getSafeStringFromDB(rdEntry["BID_MARKET"]);

			}
			else
			{   //offer
				txtDetail.Text=HelperFunction.getSafeStringFromDB(rdEntry["OFFR_DETL"]).Trim();
				txtQty.Text=HelperFunction.getSafeStringFromDB(rdEntry["OFFR_QTY"]).Trim();
				txtMelt.Text = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_MELT"]).Trim();
				txtNotes.Text = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_COMMENT"]).Trim();

				string density = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_DENS"]).Trim();
				if (density.Length>2) 
				{
					int positionDot = density.IndexOf('.');
					txtDensity.Text = density.Substring(positionDot+1);
					//HelperFunction.getSafeStringFromDB(rdEntry["OFFR_DENS"]).Trim().Substring(2);
				}
				else
					txtDensity.Text = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_DENS"]).Trim();
				txtAdds.Text = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_ADDS"]).Trim();

				CompID = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_COMP_ID"]);
				PersID = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_PERS_ID"]);
				CompanyDescription = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_COMP"]).Trim();

				Terms = rdEntry["OFFR_TERM"].ToString();
				if (lblMarketMode.Text == "International")
				{
					double price = System.Convert.ToDouble(rdEntry["OFFR_PRCE_BUY_REAL"].ToString());
					double markup = System.Convert.ToDouble(rdEntry["MARKUP"].ToString());
					txtPrice.Text = System.Convert.ToString(Math.Round(price / 0.00045359237,2));
					txtMarkup.Text = System.Convert.ToString(Math.Round(markup / 0.00045359237,2));
				}
				else
				{
					if (HelperFunction.getSafeStringFromDB(rdEntry["OFFR_PRCE_BUY_REAL"]).Trim().Length>2)
						txtPrice.Text = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_PRCE_BUY_REAL"]).Trim().Substring(2);
					else
						txtPrice.Text = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_PRCE_BUY_REAL"]).Trim();
					if (rdEntry["MARKUP"].ToString() != "0") txtMarkup.Text = rdEntry["MARKUP"].ToString().Trim().Substring(2);
				}
				ViewState["payMent"]=rdEntry["OFFR_PAY"].ToString();	
				Expiration = System.Convert.ToDateTime(rdEntry["EXPR"].ToString());
				size=rdEntry["OFFR_SIZE"].ToString();
				product=rdEntry["OFFR_PROD"].ToString();
				grade = rdEntry["OFFR_GRADE"].ToString();
				
				//txtDeliveryPoint.Text = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_FROM_LOCL_NAME"]);
				//PlaceID = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_FROM"]);
				PortID = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_PORT"]);
				ZipCode = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_ZIP"]);
				Quality = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_QUALT"]);
				market = HelperFunction.getSafeStringFromDB(rdEntry["OFFR_MARKET"]);
			}
			
			if(market == "")
			{
				market = "0";
			}

			rblMarket.SelectedValue = market;

			if(CompID!="")
			{
				ddlCompanies.SelectedItem.Selected = false;
				ddlCompanies.Items.FindByValue(CompID).Selected=true;
				ddlCompanies_SelectedIndexChanged(this,null);

				ddlUser.SelectedItem.Selected = false;
				ddlUser.Items.FindByValue(PersID).Selected=true;
			}
			else
			{
				ddlCompanies_SelectedIndexChanged(this,null);
				txtCompany.Text = CompanyDescription;
			}
			
//			if (PlaceID!="")
//			{
//				ddlDeliveryPoint.SelectedItem.Selected = false;
//				if (ddlDeliveryPoint.Items.FindByValue(PlaceID)!=null)
//					ddlDeliveryPoint.Items.FindByValue(PlaceID).Selected = true;
//			}

			if (PortID != "")
			{
				ddlDeliveryPoint.SelectedItem.Selected = false;
				if (ddlDeliveryPoint.Items.FindByValue(PortID)!=null)
					ddlDeliveryPoint.Items.FindByValue(PortID).Selected = true;
			}
			else //by ZipCode
			{
				ddlDeliveryPoint.SelectedItem.Selected = false;
				if (ddlDeliveryPoint.Items.FindByValue(ZipCode)!=null && ddlDeliveryPoint.Items.FindByValue(ZipCode).Value==ZipCode)
					ddlDeliveryPoint.Items.FindByValue(ZipCode).Selected = true;
				else
					txtZipCode.Text = ZipCode.Trim();
			}
			
			//Selecting or Add a new item with the current expiration like: 3 days, 3
			DateTime today = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
			TimeSpan time = Expiration.Subtract(today);
			string selectedValue = "";
			if (time.Days < 0)
			{
				ddlExpires.Items.Add(new ListItem ("Expired",time.Days.ToString()));
				selectedValue = time.Days.ToString();
			}
			else
			{
				//Checking if the value is already on the list
				for(int y=0;y<ddlExpires.Items.Count;y++)
				{
					if (time.Days.ToString() == ddlExpires.Items[y].Value)
					{
						selectedValue = time.Days.ToString();
						break;
					}
				}
			}
			if (selectedValue=="")
			{
				//Include the value manually
				string days = " day";
				if (time.Days > 1) days += "s";
				ddlExpires.Items.Add(new ListItem (time.Days.ToString() + days,time.Days.ToString()));
				selectedValue = time.Days.ToString();
			}
			ddlExpires.SelectedItem.Selected = false;
			ddlExpires.Items.FindByValue(selectedValue).Selected = true;

			ddlTerms.SelectedItem.Selected = false;
			if (ddlTerms.Items.FindByValue(Terms)!=null)
			{
				ddlTerms.Items.FindByValue(Terms).Selected = true;
			}
			else
			{
				if (lblMarketMode.Text == "International")
				{
					Response.Redirect("/spot/Resin_Entry.aspx?type=" + typeEntry + "&Change=" + entryNumber + "&Market=Domestic");
				}
				ListItem li = new ListItem(Terms,Terms);
				ddlTerms.Items.Add(li);
				ddlTerms.Items.FindByValue(Terms).Selected = true;
			}

			ddlQty.SelectedItem.Selected = false;
			if (ddlQty.Items.FindByValue(size)!=null)
			{
				ddlQty.Items.FindByValue(size).Selected = true;
			}
			else
			{
				if (lblMarketMode.Text == "International")
				{
					Response.Redirect("/spot/Resin_Entry.aspx?type=" + typeEntry + "&Change=" + entryNumber + "&Market=Domestic");
				}
				ListItem li = new ListItem(size,size);
				ddlQty.Items.Add(li);
				ddlQty.Items.FindByValue(size).Selected = true;
			}
			
			//bool customProduct = true;
			ddlProduct.SelectedItem.Selected = false;
			if (grade == "")
			{
				txtProduct.Text = product;
				ddlProduct.Items[ddlProduct.Items.Count-1].Selected = true;
			}
			else
			{
				ddlProduct.Items.FindByValue(grade).Selected = true;
			}

			//if ((lblMarketMode.Text!="International")&&(Quality!=""))
			//{
				ddlQuality.SelectedItem.Selected = false;
				ddlQuality.Items.FindByValue(Quality).Selected = true;
			//}
			//else if (lblMarketMode.Text!="International")
			//{
				//ddlQuality.SelectedItem.Selected = false;
				//ddlQuality.Items[0].Selected = true;
			//}
			
		}

		protected void cmdNo_Click(object sender, System.EventArgs e)
		{
			ShowConfirmation(false);
		}

		private void cmvShipingPoint_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
		{
			if(ddlTypeEntry.SelectedValue == "O")
				cmvShipingPoint.ErrorMessage = "Please enter a valid shipping point.";
			else
				cmvShipingPoint.ErrorMessage = "Please enter a valid delivery point.";

			if (lblMarketMode.Text=="International")
				if (ddlDeliveryPoint.SelectedValue=="")
					args.IsValid = false;
				else
					args.IsValid = true;
			else
			{
				if ((ddlDeliveryPoint.SelectedValue=="") && (txtZipCode.Text==""))
				{
					args.IsValid = false;
				}
				else
				{
					if (txtZipCode.Text!="") //Checking if the zip code is valid
					{
						args.IsValid = HelperFunctionDistances.IsValidZipCode(txtZipCode.Text, ref City, ref State, Application["DBconn"].ToString());
					}
					else
					{
						args.IsValid = true;
					}
				}
			}
		}

		private void cmvProduct_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
		{
            if (txtProduct.Text.Length > 0 && txtProduct.Text.Contains("<a href="))
            {
                args.IsValid = false;
            }
            else
            {
                if ((ddlProduct.SelectedValue == "0") && (txtProduct.Text.Trim() == ""))
                    args.IsValid = false;
                else
                    args.IsValid = true;
            }
			
		}

//		private void cmdCancel_Click(object sender, System.EventArgs e)
//		{
//			string page = "/spot/Spot_Floor.aspx";
//			if (Request.QueryString["Screen"]=="Dashboard") page = "/research/Dashboard.aspx";
//
//			if (lblMarketMode.Text == "International")
//				Response.Redirect(page + "?Export=true");
//			else
//				Response.Redirect(page);
//		}

		protected void LinkButton1_Click(object sender, System.EventArgs e)
		{
 
            if (lblMarketMode.Text != "International")
                MarketScreenMode(false);
            else
                MarketScreenMode(true);


            string compID = HelperFunction.getSafeStringFromDB(Session["Comp_Id"]);
            string comp = HelperFunction.getSafeStringFromDB(Session["Comp"]);
            string id = HelperFunction.getSafeStringFromDB(Session["Id"]);
            string typ = HelperFunction.getSafeStringFromDB(Session["Typ"]);

            if (InternalUser())
            {
                SetScreenModeUsers();
                pnlMarket.Visible = true;
                pnlNotes.Visible = true;
            }
            else if ((typ == "S") || (typ == "P") || (typ == "D") || (typ == "Demo"))
                SetScreenModeCustomers(compID, comp, id, typ);
            else //Session["ID"] == null
                SetScreenModeCustomers("", "", "", "");


            LoadQuantityList();
            LoadShippingPoint();
            LoadTermsList();
            SetLabelsSellResin();


            string strID = (string)Request.QueryString["Change"];
            string orderType = (string)Request.QueryString["type"];
            pnlSubmitAction.Visible = false;
            if (HelperFunction.getSafeStringFromDB(strID) != "")
            {
                lblID.Text = strID;
                QueryEditInformation(orderType, strID);

                //When editing users are not able to change the market
                lnkInternationalMarket.Visible = false;

                //When user is not internal, always use the option to Edit the item
                if (!InternalUser())
                {
                    rdbEdit.Checked = true;
                    pnlSubmitAction.Visible = false;
                }
                else
                {
                    pnlSubmitAction.Visible = true;
                }

            }

            if (!pnlNonLoggedContactInfo.Visible && !pnlSelectCompany.Visible) // contact details shouldn't be visible if the sub groups aren't
                pnlContactDetails.Visible = false;

		}

		private void MarketScreenMode(bool marketDomesticMode)
		{
			imgDomestic.Visible = marketDomesticMode;
			imgInternational.Visible = !marketDomesticMode;
			
			if (marketDomesticMode)
			{
				lnkInternationalMarket.Text = "Change to International Market";
				lblMarketMode.Text = "Domestic";
				lblMarket.Text = "U.S. Market";
			}
			else
			{
				lnkInternationalMarket.Text = "Change to U.S. Domestic Market";
				lblMarketMode.Text = "International";
				lblMarket.Text = "International Market";
			}
		}

		private void cmvProduct2_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
		{
            if (txtProduct.Text.Length > 0 && txtProduct.Text.Contains("<a href="))
            {
                args.IsValid = false;
            }
            else
            {
                if ((ddlProduct.SelectedValue != "0") && (txtProduct.Text.Trim() != ""))
                    args.IsValid = false;
                else
                    args.IsValid = true;
            }

					
		}

		protected void cmdSpotFloor_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("/spot/Spot_Floor.aspx");
		}

		protected void cmdNewEntry_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("/spot/Resin_Entry.aspx?Maket=" + lblMarketMode.Text);
		}

		private void ShowContactInformation(bool show)
		{
			pnlNonLoggedContactInfo.Visible = show;
			lblName.Visible = show;
			lblEmail.Visible = show;
			lblPhone.Visible = show;
			txtName.Visible = show;
			txtEmail.Visible = show;
			txtPhone.Visible = show;
			txtName.Enabled = show;
			txtEmail.Enabled = show;
			txtPhone.Enabled = show;
			rfvName.Visible = show;
			rfvEmail.Visible = show;
			rfvPhone.Visible = show;
		}

//		private void cvQuantity_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
//		{
//			args.IsValid = false;
//			if ((txtQty.Text != String.Empty))
//			{
//				long total = (System.Convert.ToInt64(txtQty.Text) * System.Convert.ToInt64(ddlQty.SelectedItem.Value));
//				if (total<20000000)
//				{
//					args.IsValid = true;
//				}
//			}
//		}

		//Check if the current user is an Internal User (type = A, B or T
		private bool InternalUser()
		{
			bool ret = false;
			string typ = HelperFunction.getSafeStringFromDB(Session["Typ"]);
			
			//Added L for LeadSeller 03212007 CF
			if ((typ == "A") || (typ == "B") || (typ == "T") || (typ == "L"))
				ret = true;	

			return ret;
		}

		private void cmvQuality_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
		{
			if (ddlQuality.SelectedValue=="")
				args.IsValid = false;
			else
				args.IsValid = true;
		}

		protected void cmvPrice_ServerValidate(object source, ServerValidateEventArgs args)
		{
            //international use metric tons, so the value is greater than 0.25.
           
        
            if (ddlProduct.SelectedValue.ToString().Equals("13"))
            {
                args.IsValid = true;
            }
            else
            {
                if (((string)Request.QueryString["Market"] == "International"))
                {
                    double price = Convert.ToDouble(txtPrice.Text);
                    if (price < 550)
                        args.IsValid = false;
                    else
                        args.IsValid = true;
                }
                else
                {
                    string price = "0." + txtPrice.Text;
                    double decimalPrice = Convert.ToDouble(price);
                    if (decimalPrice < 0.25)
                        args.IsValid = false;
                    else
                        args.IsValid = true;
                }
                
                
            }
        
		    
	            
		}

		protected void ddlQuality_SelectedIndexChanged(object sender, System.EventArgs e)
		{
/*
			if ( (ddlQuality.SelectedValue == "P") && (txtDetail.Text.IndexOf("Prime") == -1))
			{
				txtDetail.Text = "Prime " + txtDetail.Text;
			}
		*/
		}

		private void cmdSubmit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{            

			if (IsValid)
			{
				string typ = HelperFunction.getSafeStringFromDB(Session["Typ"]);
                if ((typ == "A") || (typ == "B") || (typ == "L"))
				{
					lblMessage.Visible = true;
					cmdYes_Click(this, null);
					// edited transactions should take users back to the spot floor
					if (Request.QueryString["Change"] != null)
						Response.Redirect("Spot_Floor.aspx");
				}
				else
				{ 
					//					ArrayList arConfirmationItens = new ArrayList();
					//
					//					//Creating the confirmation message
					//					//arConfirmationItens.Add("You are going to place an order to <font size='2pt' color=red>" + ddlTypeEntry.SelectedItem.Text + "</font></font>");
					//					
					//					string quality="";
					//					//if ((lblMarketMode.Text!="International")&&(ddlQuality.SelectedIndex>0))
					//					//{
					//						quality = "(" + ddlQuality.SelectedItem.Text + ")";
					//					//}
					//			
					//					if (ddlProduct.SelectedValue=="0")
					//						arConfirmationItens.Add("<font size='2pt'>" + lblProduct.Text + " <font color=red>" + txtProduct.Text + quality + "</font></font>");
					//					else
					//						arConfirmationItens.Add("<font size='2pt'>" + lblProduct.Text + " <font size='2pt' color=red>" + ddlProduct.SelectedItem.ToString()+ quality + "</font></font>");
					//
					//					if (!txtDetail.Text.Equals("")) arConfirmationItens.Add("<font size='2pt'>" + lblDetails.Text +" <font size='2pt' color=red>" + txtDetail.Text+ "</font></font>");
					//					if (!txtMelt.Text.Equals("")) arConfirmationItens.Add("<font size='2pt'>" + lblMelt.Text +" <font size='2pt' color=red>" + txtMelt.Text+ "</font></font>");
					//					if (!txtDensity.Text.Equals("")) arConfirmationItens.Add("<font size='2pt'>" + lblDensity.Text +" <font size='2pt' color=red>" + txtDensity.Text+ "</font></font>");
					//					if (!txtAdds.Text.Equals("")) arConfirmationItens.Add("<font size='2pt'>" + lblAdd.Text +" <font size='2pt' color=red>" + txtAdds.Text+ "</font></font>");
					//
					//					arConfirmationItens.Add("<font size='2pt'>" + lblQuantity.Text +" <font size='2pt' color=red>" + txtQty.Text + " " + ddlQty.SelectedItem.Text + "</font></font>");
					//					
					//					if (txtZipCode.Text.Equals("")) 
					//						arConfirmationItens.Add("<font size='2pt'>" + lblPoint.Text +" <font size='2pt' color=red>" + ddlDeliveryPoint.SelectedItem.Text+ "</font></font>");
					//					else
					//						arConfirmationItens.Add("<font size='2pt'>" + lblPoint.Text +" <font size='2pt' color=red>" + City + "(" + State + ")</font></font>");
					//
					//					arConfirmationItens.Add("<font size='2pt'>" + lblPrice.Text +" <font size='2pt' color=red>" + txtPrice.Text+ "</font> " + lblPriceMeasure.Text + "</font></font>");
					//					if ((typ == "A") || (typ == "B"))
					//						arConfirmationItens.Add("<font size='2pt'>" + lblMarkup.Text +" <font size='2pt' color=red>" + txtMarkup.Text+ "</font> " + lblMarkupMeasure.Text + "</font></font>");
					//
					//					if (ddlExpires.Visible)
					//						arConfirmationItens.Add("<font size='2pt'>" + lblExpiration.Text +" <font size='2pt' color=red>" + ddlExpires.SelectedItem.ToString()+ "</font></font>");
					//
					//					if (ddlTerms.Visible) arConfirmationItens.Add("<font size='2pt'>" + lblTerms.Text +" <font size='2pt' color=red>" + ddlTerms.SelectedItem.ToString()+ "</font></font>");
					
					//lblMessage.Text = "<BR><BR><BR><BR><BR><BR><BR>" + CreateConfirmationTable(arConfirmationItens) + "<BR><BR><BR><BR>";
					//					Details = CreateConfirmationTable(arConfirmationItens);
					//					ShowConfirmation(true);
					cmdYes_Click(this, null);

					Response.Redirect("Spot_Floor.aspx?Product=" + ddlProduct.SelectedValue + "&Size=" + ddlQty.SelectedValue + "&Export=" + imgInternational.Visible.ToString().ToLower());

				}
			}	
		}

		private void cvQuantity_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
		{
			string Message = string.Empty;
			try 
			{
				int Quantity = Int32.Parse(txtQty.Text);
				if (Quantity <= 0)
				{
					Message = "Quantity must be a positive number";
					args.IsValid = false;
				}	
			}
			catch(Exception e)
			{
				Message = "Quantity must be a positive number";
				args.IsValid = false;
			}

			if (args.IsValid)
			{
				Message = "Quantity must be less than ";
				switch (ddlQty.SelectedItem.ToString())
				{
					case "Railcar" :
						args.IsValid = (Int32.Parse(txtQty.Text) < 100);
						Message = Message + "100 for Railcars";
						break;

					case "Bulk Truck" : 
						args.IsValid = (Int32.Parse(txtQty.Text) < 100);
						Message = Message + "100 for Bulk Truck";
						break;

					case "Truckload Boxes" :
						args.IsValid = (Int32.Parse(txtQty.Text) < 100);
						Message = Message + "100 for Truckload Boxes";
						break;

					case "Truckload Bags" :
						args.IsValid = (Int32.Parse(txtQty.Text) < 100);
						Message = Message + "100 for Truckload Bags";
						break;

					case "Pounds (lbs)" :
						args.IsValid = (Int32.Parse(txtQty.Text) < 11000000);
						Message = Message + "11000000 for Pounds";
						break;

					case "Metric Tonnes":
						args.IsValid = (Int32.Parse(txtQty.Text) < 5000);
						Message = Message + "5000 for Metric Tonnes";
						break;
                         
					default :
						args.IsValid = (Int32.Parse(txtQty.Text) < 100);
						Message = Message + "100 for Railcars";
						break;

				}
			}
			if (!args.IsValid)
			{
				this.cvQuantity.ErrorMessage = Message;
			}
		}	

	}
}