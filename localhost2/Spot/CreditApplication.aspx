
<%@ Page language="c#" Codebehind="CreditApplication.aspx.cs" AutoEventWireup="True" Inherits="PlasticsExchange.CreditApplication" enableViewStateMac="False" MasterPageFile="~/MasterPages/Menu.Master" Title="CreditApplication" %>
<%@ MasterType virtualPath="~/MasterPages/Menu.Master"%>
 
<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">

<style type="text/css">

.Header {padding-top:3;}

</style>
 
</asp:Content> 
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="cphMain">

			<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 731px" borderColor="black" 
				cellSpacing="0" cellPadding="0" align="center" class="Content" border="0">
				<TR>
					<TD vAlign="top" align="left" bgColor=#999999>
						<TABLE id="Table11" height="155" width="100%" border="0" cellSpacing=0 cellPadding=0 bgColor=#999999>
							<TR>
								<TD class="Header Bold" align="center">
									<P><b>1. Shipping Information</b></P>
								</TD>
								<TD class="Header Bold" align="center">
									<P><b><font color='orange'>2. Credit Application</font></b></P>
								</TD>
								<TD class="Header Bold" align="center">
									<P><b>3. Order Confirmation</b></P>
								</TD>
							</TR>
						</TABLE>						 
						<TABLE  id="Table12" width="100%" height="630" border="0" cellSpacing=0 cellPadding=0 align="center" bgColor=#999999 >
							<tr>
							    <td colspan="5">
							        <div class="DivTitleBar"><span class="Header Color1 Bold">Personal Information</span></div>
							    </td>
							</tr>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 353px" align="left" colSpan="2"><B>First Name&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtPers_Fname" Display="Dynamic"
											ErrorMessage="<span class='ErrorText'>(Required)</span>"></asp:requiredfieldvalidator></B></TD>
								<TD style="WIDTH: 153px" align="left" colSpan="1"><B>Last Name&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator13" runat="server" ErrorMessage="<span class='ErrorText'>(Required)</span>" Display="Dynamic"
											ControlToValidate="txtPers_Lname"></asp:requiredfieldvalidator></B></TD>
								<TD align="left" colSpan="1"><B>Title </B>
								</TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>								
								<TD align="left" style="WIDTH: 353px" colSpan="2">
								    <asp:textbox CssClass="InputForm" id="txtPers_Fname" runat="server" Width="346px" MaxLength="40" tabIndex="1"></asp:textbox>
								</TD>
								<TD align="left" style="WIDTH: 153px" colSpan="1">
								    <asp:textbox CssClass="InputForm" id="txtPers_Lname" runat="server" Width="152px" MaxLength="40" tabIndex="1"></asp:textbox>
								</TD>
								<TD align="left" style="HEIGHT: 27px" colSpan="1">
								    <asp:textbox CssClass="InputForm" id="txtPers_Title" runat="server" Width="152px" MaxLength="20" tabIndex="2"></asp:textbox>
								</TD>
							<TR>
								<td width="35">&nbsp;</td>								
								<TD style="WIDTH: 353px" align="left" colSpan="2"><B>Phone&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" ErrorMessage="<span class='ErrorText'>(Required)</span>" Display="Dynamic"
											ControlToValidate="txtPers_Phone"></asp:requiredfieldvalidator></B></TD>
								<TD style="WIDTH: 153px" align="left" colSpan="1"><B>Fax</B></TD>
								<TD  align="left" colSpan="1"><B>Email&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator3" runat="server" ErrorMessage="<span class='ErrorText'>(Required)</span>" Display="Dynamic"
											ControlToValidate="txtPers_Email"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" ErrorMessage="<span class='ErrorText'>(Invalid email)</span>"
											Display="Dynamic" ControlToValidate="txtPers_Email" ValidationExpression="\S+@\S+\.\S{2,3}"></asp:regularexpressionvalidator></B></TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>								
								<TD style="WIDTH: 353px; HEIGHT: 4px" colSpan="2"><asp:textbox CssClass="InputForm" id="txtPers_Phone" runat="server" Width="346px" MaxLength="15" tabIndex="3"></asp:textbox></TD>
								<TD style="WIDTH: 153px" colSpan="1"><asp:textbox CssClass="InputForm" id="txtPers_Fax" runat="server" Width="152px" MaxLength="15" tabIndex="4"></asp:textbox></TD>
								<TD colSpan="1"><asp:textbox CssClass="InputForm" id="txtPers_Email" runat="server" Width="152px" MaxLength="30" tabIndex="5"></asp:textbox></TD>
							</TR>
							<TR>
								<TD colSpan="5">
									<div class="DivTitleBar"><span class="Header Bold Color1">Company Information</span></div>
								</TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 353px" align="left" colSpan="2"><B>Company Name&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator4" runat="server" ErrorMessage="<span class='ErrorText'>(Required)</span>" Display="Dynamic"
											ControlToValidate="txtComp_Cname"></asp:requiredfieldvalidator></B></TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 353px; HEIGHT: 5px" colSpan="2"><asp:textbox CssClass="InputForm" id="txtComp_Cname" runat="server" Width="346px" MaxLength="50" tabIndex="6"></asp:textbox></TD>
							<TR>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 353px" align="left" colSpan="2"><B>Address&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator5" runat="server" ErrorMessage="<span class='ErrorText'>(Required)</span>" Display="Dynamic"
											ControlToValidate="txtComp_Address"></asp:requiredfieldvalidator></B></TD>
								<TD style="WIDTH: 289px" align="left" colSpan="2"><B>City&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator6" runat="server" ErrorMessage="<span class='ErrorText'>(Required)</span>" Display="Dynamic"
											ControlToValidate="txtComp_City"></asp:requiredfieldvalidator></B></TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 353px; HEIGHT: 3px" colSpan="2"><asp:textbox CssClass="InputForm" id="txtComp_Address" runat="server"  Width="346px" MaxLength="70" tabIndex="7"></asp:textbox></TD>
								<TD style="WIDTH: 289px; HEIGHT: 3px" colSpan="2"><asp:textbox CssClass="InputForm" id="txtComp_City" runat="server" Width="346px" MaxLength="50" tabIndex="8"></asp:textbox></TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 353px" align="left" colSpan="2"><B>Zip Code</B></TD>
								<TD style="WIDTH: 153px" align="left" colSpan="1"><B>Country</B></TD>
								<TD style="WIDTH: 158px" align="left" colSpan="1"><B>State</B></TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 353px; HEIGHT: 19px" colSpan="2"><asp:textbox CssClass="InputForm" id="txtComp_Zip" runat="server" Width="346px" MaxLength="10" tabIndex="9"></asp:textbox></TD>
								<TD style="WIDTH: 153px"><asp:dropdownlist CssClass="InputForm" id="ddlComp_Country" runat="server" Width="152px" AutoPostBack="True" tabIndex="10" onselectedindexchanged="ddlComp_Country_SelectedIndexChanged"></asp:dropdownlist></TD>
								<TD style="WIDTH: 91px; HEIGHT: 19px"><asp:dropdownlist CssClass="InputForm" id="ddlComp_State" runat="server" Width="152px" tabIndex="11"></asp:dropdownlist><asp:textbox CssClass="InputForm" id="txtState" runat="server" Width="152px" MaxLength="15" Visible="False" ToolTip="State"></asp:textbox></TD>
							</TR>
							<TR>
								<TD colSpan="3" style="WIDTH: 360px">
									<div style="width:100%" class="DivTitleBar"><span class="Header Bold Color1">Bank Reference</span></div>
								</TD>
								<TD colSpan="2">
									<div style="width:100%" class="DivTitleBar"><span class="Header Bold Color1">Trade Reference</span></div>
								</TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 353px" align="left" colSpan="2"><B>Bank Name&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator7" runat="server" ErrorMessage="<span class='ErrorText'>(Required)</span>" Display="Dynamic"
											ControlToValidate="txtBank_Bname"></asp:requiredfieldvalidator></B></TD>
								<TD style="WIDTH: 289px" align="left" colSpan="2"><B>Company Name&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator8" runat="server" ErrorMessage="<span class='ErrorText'>(Required)</span>" Display="Dynamic"
											ControlToValidate="txtTrad1_Cname"></asp:requiredfieldvalidator></B></TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 353px" colSpan="2"><asp:textbox CssClass="InputForm" id="txtBank_Bname" runat="server"  Width="346px" MaxLength="40" tabIndex="12"></asp:textbox></TD>
								<TD style="WIDTH: 289px" colSpan="2"><asp:textbox CssClass="InputForm" id="txtTrad1_Cname" runat="server" Width="346px" MaxLength="40" tabIndex="16"></asp:textbox></TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 353px; HEIGHT: 19px" align="left" colSpan="2"><B>Contact&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator9" runat="server" ErrorMessage="<span class='ErrorText'>(Required)</span>" Display="Dynamic"
											ControlToValidate="txtBank_Contact"></asp:requiredfieldvalidator></B></TD>
								<TD style="WIDTH: 289px; HEIGHT: 19px" align="left" colSpan="2"><B>Contact&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator10" runat="server" ErrorMessage="<span class='ErrorText'>(Required)</span>" Display="Dynamic"
											ControlToValidate="txtTrad1_Contact"></asp:requiredfieldvalidator></B></TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 353px; HEIGHT: 1px" colSpan="2"><asp:textbox CssClass="InputForm" id="txtBank_Contact" runat="server" Width="346px" MaxLength="15" tabIndex="13"></asp:textbox></TD>
								<TD style="WIDTH: 289px; HEIGHT: 1px" colSpan="2"><asp:textbox CssClass="InputForm" id="txtTrad1_Contact" runat="server"  Width="346px" MaxLength="15"
										tabIndex="17"></asp:textbox></TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 135px" align="left" colSpan="1"><B>Phone&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator11" runat="server" ControlToValidate="txtBank_Phone" Display="Dynamic"
											ErrorMessage="<span class='ErrorText'>(Required)</span>"></asp:requiredfieldvalidator></B></TD>
								<TD style="WIDTH: 167px; HEIGHT: 14px" align="left" colSpan="1"><B>Fax&nbsp;<asp:requiredfieldvalidator id="Requiredfieldvalidator14" runat="server" ControlToValidate="txtBank_Fax" Display="Dynamic"
											ErrorMessage="<span class='ErrorText'>(Required)</span>"></asp:requiredfieldvalidator></B></TD>
								<TD style="WIDTH: 153px; HEIGHT: 14px" align="left" colSpan="1"><B>Phone&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator12" runat="server" ErrorMessage="<span class='ErrorText'>(Required)</span>" Display="Dynamic"
											ControlToValidate="txtTrad1_Phone"></asp:requiredfieldvalidator></B></TD>
								<TD style="WIDTH: 117px; HEIGHT: 14px" align="left" colSpan="1"><B>Fax&nbsp;<asp:requiredfieldvalidator id="Requiredfieldvalidator15" runat="server" ControlToValidate="txtTrad1_Fax" Display="Dynamic"
											ErrorMessage="<span class='ErrorText'>(Required)</span>"></asp:requiredfieldvalidator></B></TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 135px" colSpan="1" width=135><asp:textbox CssClass="InputForm" id="txtBank_Phone" runat="server" Width="152px" MaxLength="15" tabIndex="14"></asp:textbox></TD>
								<TD style="WIDTH: 167px" colSpan="1"><asp:textbox CssClass="InputForm" id="txtBank_Fax" runat="server" Width="160px" MaxLength="15" tabIndex="15"></asp:textbox></TD>
								<TD style="WIDTH: 153px" colSpan="1"><asp:textbox CssClass="InputForm" id="txtTrad1_Phone" runat="server" Width="152px" MaxLength="15" tabIndex="18"></asp:textbox></TD>
								<TD style="WIDTH: 117px; HEIGHT: 3px" colSpan="1"><asp:textbox CssClass="InputForm" id="txtTrad1_Fax" runat="server" Width="152px" MaxLength="15" tabIndex="19"></asp:textbox></TD>
							</TR>
							<TR>
								<TD colSpan="3" style="WIDTH: 360px">
									<div style="width:100%" class="DivTitleBar"><span class="Header Bold Color1">Reference 2</span></div>
								</TD>
								<TD colSpan="2">
									<div style="width:100%" class="DivTitleBar"><span class="Header Bold Color1">Trade Reference 3</span></div>
								</TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 353px" align="left" colSpan="2"><B>Company Name&nbsp;<asp:requiredfieldvalidator id="Requiredfieldvalidator16" runat="server" ControlToValidate="txtTrad2_Cname"
											Display="Dynamic" ErrorMessage="<span class='ErrorText'>(Required)</span>"></asp:requiredfieldvalidator></B></TD>
								<TD style="WIDTH: 289px" align="left" colSpan="2"><B>Company Name</B></TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 353px" colSpan="2"><asp:textbox CssClass="InputForm" id="txtTrad2_Cname" runat="server"  Width="346px" MaxLength="40" tabIndex="20"></asp:textbox></TD>
								<TD style="WIDTH: 289px" colSpan="2"><asp:textbox CssClass="InputForm" id="txtTrad3_Cname" runat="server"  Width="346px" MaxLength="40" tabIndex="24"></asp:textbox></TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 353px" align="left" colSpan="2"><B>Contact&nbsp;<asp:requiredfieldvalidator id="Requiredfieldvalidator17" runat="server" ControlToValidate="txtTrad2_Contact"
											Display="Dynamic" ErrorMessage="<span class='ErrorText'>(Required)</span>"></asp:requiredfieldvalidator></B></TD>
								<TD style="WIDTH: 289px" align="left" colSpan="2"><B>Contact</B></TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 353px" colSpan="2"><asp:textbox CssClass="InputForm" id="txtTrad2_Contact" runat="server"  Width="346px" MaxLength="15"
										tabIndex="21"></asp:textbox></TD>
								<TD style="WIDTH: 289px" colSpan="2"><asp:textbox CssClass="InputForm" id="txtTrad3_Contact" runat="server" Width="346px" MaxLength="15"
										tabIndex="25"></asp:textbox></TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 135px" align="left" colSpan="1"><B>Phone&nbsp;<asp:requiredfieldvalidator id="Requiredfieldvalidator18" runat="server" ControlToValidate="txtTrad2_Phone"
											Display="Dynamic" ErrorMessage="<span class='ErrorText'>(Required)</span>"></asp:requiredfieldvalidator></B></TD>
								<TD style="WIDTH: 167px" align="left" colSpan="1"><B>Fax&nbsp;<asp:requiredfieldvalidator id="Requiredfieldvalidator19" runat="server" ControlToValidate="txtTrad2_Fax" Display="Dynamic"
											ErrorMessage="<span class='ErrorText'>(Required)</span>"></asp:requiredfieldvalidator></B></TD>
								<TD style="WIDTH: 153px" align="left" colSpan="1"><B>Phone</B></TD>
								<TD style="WIDTH: 117px" align="left" colSpan="1"><B>Fax</B></TD>
							</TR>
							<TR>
								<td width="35">&nbsp;</td>
								<TD style="WIDTH: 135px" colSpan="1"><asp:textbox CssClass="InputForm" id="txtTrad2_Phone" runat="server" Width="152px" MaxLength="15" tabIndex="22"></asp:textbox></TD>
								<TD style="WIDTH: 167px" colSpan="1"><asp:textbox CssClass="InputForm" id="txtTrad2_Fax" runat="server" Width="160px" MaxLength="15" tabIndex="23"></asp:textbox></TD>
								<TD style="WIDTH: 153px" colSpan="1"><asp:textbox CssClass="InputForm" id="txtTrad3_Phone" runat="server" Width="152px" MaxLength="15" tabIndex="26"></asp:textbox></TD>
								<TD style="WIDTH: 117px" colSpan="1"><asp:textbox CssClass="InputForm" id="txtTrad3_Fax" runat="server" Width="152px" MaxLength="15" tabIndex="27"></asp:textbox></TD>
							</TR>
							<tr>
								<td colspan="5">&nbsp;</td>
							</tr>
						</TABLE>
						<asp:panel id="pnlCredit" runat="server" Height="35px" Width="688px">
      <TABLE id=Table2 style="WIDTH: 711px; HEIGHT: 24px" borderColor=black 
      height=24 width=711 border=0>
        <TR>
          <TD align=center>
<asp:Button id=btnBack CssClass="Content Color2" runat="server"  Text="  Back  " CausesValidation="False" onclick="btnBack_Click"></asp:Button>&nbsp;&nbsp;&nbsp; 
<asp:Button id=btnConfirm tabIndex=28 CssClass="Content Color2" runat="server"  Text="Confirm" onclick="btnConfirm_Click"></asp:Button></TD></TR></TABLE>
						</asp:panel><asp:panel id="pnlapproveCredit" runat="server" Height="40px" Width="711px">
      <TABLE id=Table4 style="WIDTH: 711px; HEIGHT: 32px" borderColor=black 
      height=32 width=711 border=0>
        <TR>
          <TD align=center>
<asp:button id=btnDeny runat="server" CssClass="Content Color2" Text="Deny" CausesValidation="False" onclick="btnDeny_Click"></asp:button>&nbsp;&nbsp;&nbsp; 
<asp:button id=btnApprove runat="server" CssClass="Content Color2" Text="Approve" onclick="btnApprove_Click"></asp:button></TD></TR></TABLE>
						</asp:panel></TD>
				</TR>
			</TABLE>
</asp:Content>