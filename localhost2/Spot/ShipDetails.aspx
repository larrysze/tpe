<%@ Page language="c#" Codebehind="ShipDetails.aspx.cs" AutoEventWireup="True" Inherits="localhost.Spot.ShipDetails" MasterPageFile="~/MasterPages/Menu.Master" Title="Ship Details" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
<style type=text/css>BODY { MARGIN: 0px; BACKGROUND-COLOR: #ffffff }
	#question { FONT-WEIGHT: bold; FONT-SIZE: 10px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif }
	.TABLE { BACKGROUND-COLOR: #333333 }
	</style>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphJavaScript">

<script type="text/javascript" language="javascript">
			
		function sameAddress()
		{
			if(document.getElementById('<%=chkSame.ClientID %>').checked)
			{
				document.getElementById('<%=txtBCname.ClientID %>').value = document.getElementById('<%=txtSCname.ClientID%>').value;
				document.getElementById('<%=txtBAddress1.ClientID %>').value = document.getElementById('<%=txtSAddress1.ClientID %>').value;
				document.getElementById('<%=txtBCity.ClientID %>').value = document.getElementById('<%=txtSCity.ClientID %>').value;
				document.getElementById('<%=txtBZip.ClientID %>').value = document.getElementById('<%=txtSZip.ClientID %>').value;
//				document.Form1.txtBState.value = document.Form1.txtSState.value;
				
				if(document.getElementById('<%=txtSState.ClientID %>') != null)
				{
					if(document.getElementById('<%=txtBState.ClientID %>') != null){
						document.getElementById('<%=txtBState.ClientID %>').value = document.getElementById('<%=txtSState.ClientID %>').value;
					}
				}
				else
				{
					if(document.getElementById('<%=ddlBState.ClientID %>') != null)
					{
						document.getElementById('<%=ddlBState.ClientID %>').value = document.getElementById('<%=ddlSState.ClientID %>').value;
					}
					else
					{
						document.getElementById('<%=txtBState.ClientID %>').value = document.getElementById('<%=ddlSState.ClientID %>').value;
					}
				
				}
				
				document.getElementById('<%=ddlBCountry.ClientID %>').value = document.getElementById('<%=ddlSCountry.ClientID %>').value;
			}
			else
			{
				document.getElementById('<%=txtBCname.ClientID %>').value = "";
				document.getElementById('<%=txtBAddress1.ClientID %>').value = "";
				document.getElementById('<%=txtBCity.ClientID %>').value = "";
				document.getElementById('<%=txtBZip.ClientID %>').value = "";
//				document.Form1.txtBState.value = document.Form1.txtSState.value;
				if(document.getElementById('<%=txtBState.ClientID %>') != null)
				{
					document.getElementById('<%=txtBState.ClientID %>').value = "";
				}
				else
				{
					document.getElementById('<%=ddlBState.ClientID %>').value = document.getElementById('<%=ddlBState.ClientID %>').item[0];
					
				}
				document.getElementById('<%=ddlBCountry.ClientID %>').value = document.getElementById('<%=ddlBCountry.ClientID %>').getElementsByTagName("United States");
			}
		}
		
	</script>

</asp:Content>


<asp:Content runat="server" ContentPlaceHolderID="cphMain">

<table id="Table1" borderColor="black" cellSpacing="0" cellPadding="0" width="369" align="center" border="0" style="background-color:#333333">
  <tr>
    <td vAlign="top" align="center">
      <table height="155" cellSpacing="0" cellPadding="0" width="780" align="center" bgColor="#CCCCCC" border="0">
        <tr>
          <td align="right">
            <table width="736" border="0">
              <tr>
                <td vAlign="top" align="center">
                  <table id="Table11" width="736">
                    <tr>
                      <td class="Content Bold Color1" align="center">
                        <p><asp:label id=lblTitle1 runat="server">1. Shipping Information</asp:label></P></td>
                      <td class="Content Bold Color2" noWrap align="center">
                        <p><asp:label id="lblTitle2" runat="server">2. Credit Application</asp:label></P></td>
                      <td class="Content Bold Color2" align="center">
                        <p><asp:label id="lblTitle3" runat="server" Font-Bold="True">3. Order Confirmation</asp:label></P></td> <!--<td align="left"><span class="PageHeader">Shipping Details</span></td>--></tr></table> <!--<td align="left"><span class="PageHeader">Shipping Details</span></td>--></td>
                    </tr>                        
                    <tr>
                      <td><asp:validationsummary CssClass="Content Color1 Bold" id="ValidSummary" runat="server" Width="544px" Height="72px" HeaderText="There are problems with the following fields."></asp:validationsummary></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
                <div class="DivTitleBarMenu"><span class="Header Bold Color1">Product details</span></div>
      <table class="TABLE" id="Table16" width="780" align="center" border="0">
        <tr>
          <td align="center">
            <table id="SpotGrid1_dgAdmin" cellSpacing=0 cellPadding=1 rules="all" 
                align="center" border="0">
              <tr>
                <td class="Content Color2 Bold" bgColor="#dbdcd7">#</td>
                <td class="Content Color2 Bold" style="WIDTH: 234px" bgColor="#dbdcd7" 
                >Product</td>
                <td class="Content Color2 Bold" bgColor="#dbdcd7" 
                >Size</td>
                <td class="Content Color2 Bold" bgColor="#dbdcd7" 
                >Melt</td>
                <td class="Content Color2 Bold" bgColor="#dbdcd7" 
                  >Density</td>
                <td class="Content Color2 Bold" bgColor="#dbdcd7" 
                >Adds</td>
                <td class="Content Color2 Bold" bgColor="#dbdcd7" 
                  >Delivery Terms</td>
                <td class="Content Color2 Bold" bgColor="#dbdcd7" 
                >Price</td>
              </tr>
              <tr>
                <td align="center" bgColor="#999999">
                <asp:label id=lblID runat="server" Width="44px" CssClass="Content Color7 Bold"></asp:label></td>
                <td align="center" bgColor="#999999">
                <asp:label id=lblProduct runat="server" CssClass="Content Color7 Bold"></asp:label></td>
                <td style="WIDTH: 79px; HEIGHT: 10px" align="center" bgColor="#999999" 
                ><asp:label id=lblSize runat="server" Height="16px" width="100px" CssClass="Content Color7 Bold"></asp:label></td>
                <td style="WIDTH: 72px; HEIGHT: 10px" align="center" bgColor="#999999" 
                ><asp:label id=lblMelt runat="server" width="40px" CssClass="Content Color7 Bold"></asp:label></td>
                <td style="WIDTH: 70px; HEIGHT: 10px" align="center" bgColor="#999999" 
                ><asp:label id=lblDens runat="server" width="40px" CssClass="Content Color7 Bold"></asp:label></td>
                <td style="WIDTH: 137px; HEIGHT: 10px" align="center" 
                bgColor="#999999"><asp:label id=lblAdds runat="server" width="40" CssClass="Content Color7 Bold"></asp:label>
                </td>
                <td style="HEIGHT: 13px" align="center" width=400 bgColor="#999999" 
                ><asp:label id=lblTerms runat="server" CssClass="Content Color7 Bold"></asp:label></td>
                <td align="center" width=400 bgColor="#999999">
                <asp:label id=lblPrice runat="server" CssClass="Content Color7 Bold"></asp:label></td></tr></TABLE>
               </td>
             </tr>
            <tr>
              <td align="center" class="Content Color4 Bold"><br />Click <span class="LinkNormal Color7"><a style="COLOR: #f3c300" href="javascript:void window.open('Agreement.aspx', 'Agreement', 'toolbar=false, scrollbars=1')" >here</a></span> 
                to see Conditions of Sales Agreement </td>
            </tr>
          </table>
        <asp:textbox id="txtEnDisable" runat="server" CssClass="Content Color2 Bold" Height="0px">0</asp:textbox>
      </td>
    </tr>
  <tr>
        <td align="center">
         <div class="DivTitleBarMenu"><span class="Header Bold Color1">Shipping Adress</span></div>
          <TABLE class=TABLE id=Table4 width=778 align="center" border=0>
            <tr>
              <td vAlign=middle width=433></td>
            </tr>
            <tr>
              <td align="center">
                <TABLE id=Table8 width=300 border=0>
                  <tr>
                    <td>
                      <TABLE id=Table6 width=602 border=0>
                        <tr>
                          <td align="center" bgColor="#999999"><br /><asp:dropdownlist CssClass="InputForm" id=ddlAddress runat="server" Width="505px" AutoPostBack="True" onselectedindexchanged="ddlAddress_SelectedIndexChanged"></asp:dropdownlist><br /><br /></td></tr>
                        <tr>
                          <td vAlign=middle align="left" bgColor="#999999"><asp:panel id=Panel3 runat="server" Width="333px"> 
    <asp:Label id=Label4 runat="server" Font-Bold="True" CssClass="Content Color2 Bold" Visible="True">&nbsp;New Address</asp:Label></asp:panel> 
                            <asp:panel id=pnlNewAdd 
                            runat="server" Width="600px" DESIGNTIMEDRAGDROP="114">
                            <TABLE id=Table9 style="background:black; WIDTH: 437px; HEIGHT: 182px" 
                            cellSpacing=1 cellPadding=0 width=437 align="center" 
                            border=0>
                              <tr>
                                <td class="Content Color2 Bold" style="WIDTH: 171px" align="right" 
                                width=171 bgColor="#dbdcd7" height="20">Contact Person 
                                  Name: </td>
                                <td class="Content Color2" align="left" width=260 
                                bgColor="#999999">
    <asp:requiredfieldvalidator id=rfvSContact runat="server" ErrorMessage="Contact Person  Name can not be blank." Font-Names="Times New Roman" Display="None" ControlToValidate="txtSCname"></asp:requiredfieldvalidator>
    <asp:textbox CssClass="InputForm" id=txtSCname Width="240px" Runat="server" MaxLength="40"></asp:textbox></td></tr>
                              <tr>
                                <td class="Content Color2 Bold" style="WIDTH: 171px" align="right" 
                                bgColor="#dbdcd7" height="20">Address:</td>
                                <td class="Content Color2" align="left" bgColor="#999999">
    <asp:requiredfieldvalidator id=rfvSAdd runat="server" ErrorMessage="Shipping Address can not be blank." Font-Names="Times New Roman" Display="None" ControlToValidate="txtSAddress1"></asp:requiredfieldvalidator>
    <asp:textbox CssClass="InputForm" id=txtSAddress1 runat="server" Width="240px"></asp:textbox></td></tr>
                              <tr>
                                <td class="Content Color2 Bold" style="WIDTH: 171px" align="right" 
                                bgColor="#dbdcd7" height="20">City:</td>
                                <td class="Content Color2" align="left" bgColor="#999999">
    <asp:requiredfieldvalidator id=rfvSCity runat="server" ErrorMessage="Ship City can not be blank." Font-Names="Times New Roman" Display="None" ControlToValidate="txtSCity"></asp:requiredfieldvalidator>
    <asp:textbox CssClass="InputForm" id=txtSCity runat="server" Width="240px"></asp:textbox></td></tr>
                              <tr>
                                <td class="Content Color2 Bold" style="WIDTH: 171px" align="right" 
                                bgColor="#dbdcd7" height="20">Zip code:</td>
                                <td class="Content Color2" align="left" bgColor="#999999">
    <asp:requiredfieldvalidator id=rfvSZip runat="server" ErrorMessage="Shipping Address Zip can not be blank." Font-Names="Times New Roman" Display="None" ControlToValidate="txtSZip"></asp:requiredfieldvalidator>
    <asp:textbox CssClass="InputForm" id=txtSZip runat="server" Width="69px" MaxLength="10"></asp:textbox></td></tr>
                              <tr>
                                <td class="Content Color2 Bold" 
                                style="WIDTH: 171px; HEIGHT: 15px" align="right" 
                                bgColor="#dbdcd7" height="15">State:</td>
                                <td class="Content Color2" style="HEIGHT: 15px" align="left" 
                                bgColor="#999999">
    <asp:textbox CssClass="InputForm" id=txtSState runat="server" Width="240px"></asp:textbox>
    <asp:dropdownlist CssClass="InputForm" id=ddlSState runat="server" Width="240px"></asp:dropdownlist></td></tr>
                              <tr>
                                <td class="Content Color2 Bold" style="WIDTH: 171px" align="right" 
                                bgColor="#dbdcd7" height="20">Country:</td>
                                <td class="Content Color2" align="left" bgColor="#999999">
    <asp:dropdownlist CssClass="InputForm" id=ddlSCountry runat="server" Width="240px" Height="22px" AutoPostBack="True" onselectedindexchanged="ddlSCountry_SelectedIndexChanged"></asp:dropdownlist></td></tr></TABLE>
    <asp:panel id=Panel4 runat="server" Width="560px">
    <asp:TextBox BackColor="#999999" BorderWidth="0" CssClass="Content Color2" id=TextBox4 runat="server" Width="24px" ReadOnly="True"></asp:TextBox>
    <asp:Label id=Label5 runat="server"><span class="Content Bold Color2">Delivering Rail Carrier / Track Number:</span></asp:Label>
    <asp:TextBox BackColor="#999999" BorderWidth="0" CssClass="Content Color2" id=TextBox3 runat="server" Width="24px" ReadOnly="True"></asp:TextBox>
    <asp:textbox CssClass="InputForm" id=txtRailNum runat="server" Width="180px" Font-Size="X-Small"></asp:textbox></asp:panel><BR></asp:panel>
  </td>
</tr>
</table></td></tr>
              <tr>
                <td></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td class="Content Color4 Bold" align="center">Click <span class="LinkNormal Color7"><A style="COLOR: #f3c300" href="javascript:void window.open('Agreement.aspx', 'Agreement', 'toolbar=false, scrollbars=1')" >here</A></span> 
            to see Conditions of Sales Agreement<br /></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="center">
    <div class="DivTitleBarMenu"><span class="Header Bold Color1">Billing Address</span></div>
      <table class="TABLE" id="Table224" cellSpacing="1" cellPadding="1" width="778" align="center" border="0">
        <tr>
          <td valign="middle" width="433"></td></tr>
        <tr>
          <td align="center">
            <table id=Table106 cellSpacing="0" cellPadding="0" width="602" border="0">
              <tr>
                <td align="left" bgColor="#999999"><asp:checkbox id="chkSame" onclick="sameAddress()" runat="server" CssClass="Content Color2" Text="Same as Shipping Address:"></asp:checkbox></td></tr>
              <tr>
                <td align="left" bgColor="#999999">
                  <table id="Table2" style="WIDTH: 437px; HEIGHT: 182px; background-color:Black" 
                    cellSpacing="1" cellPadding="0" width="437" align="center" border=0>
                    <tr>
                      <td class="Content Color2 Bold" style="WIDTH: 171px" align="right" 
                      width=171 bgColor="#dbdcd7" height="20">Contact Person Name: </td>
                      <td class="Content Color2" align="left" width=260 bgColor="#999999" 
                      ><asp:requiredfieldvalidator CssClass="Content Color2" id=rfvBContact runat="server" ControlToValidate="txtBCname" Display="None"  ErrorMessage="Bill Contact Name can not be blank."></asp:requiredfieldvalidator><asp:textbox CssClass="InputForm" id="txtBCname" runat="server" Width="240px" MaxLength="40"></asp:textbox></td></tr>
                    <tr>
                      <td class="Content Color2 Bold" style="WIDTH: 171px" align="right" 
                      bgColor="#dbdcd7" height="20">Address:</td>
                      <td class="Content Color2" align="left" bgColor="#999999" 
                      ><asp:requiredfieldvalidator id=rfvBillAdd runat="server" ControlToValidate="txtBAddress1" Display="None" ErrorMessage="Bill Address can not be blank."></asp:requiredfieldvalidator>
                      <asp:textbox CssClass="InputForm" id="txtBAddress1" runat="server" Width="240px" MaxLength="50"></asp:textbox></td></tr>
                    <tr>
                      <td class="Content Color2 Bold" style="WIDTH: 171px" align="right" 
                      bgColor="#dbdcd7" height="20">City:</td>
                      <td class="Content Color2" align="left" bgColor="#999999" 
                      ><asp:requiredfieldvalidator id="rfvBCity" runat="server" ControlToValidate="txtBCity" Display="None" Font-Names="Times New Roman" ErrorMessage="Bill City can not be blank."></asp:requiredfieldvalidator>
                      <asp:textbox CssClass="InputForm" id="txtBCity" runat="server" Width="240px" MaxLength="20"></asp:textbox></td></tr>
                    <tr>
                      <td class="Content Color2 Bold" style="WIDTH: 171px" align="right" 
                      bgColor="#dbdcd7" height="20">Zip 
                      code:</td>
                      <td class="Content Color2" align="left" bgColor="#999999"><asp:requiredfieldvalidator id="rfvBZip" runat="server" ControlToValidate="txtBZip" Display="None" Font-Names="Times New Roman" ErrorMessage="Billing Address Zip can not be blank."></asp:requiredfieldvalidator>
                      <asp:textbox CssClass="InputForm" id="txtBZip" runat="server" Width="69px" MaxLength="10"></asp:textbox></td></tr>
                    <tr>
                      <td class="Content Color2 Bold" style="WIDTH: 171px; HEIGHT: 15px" 
                      align="right" bgColor="#dbdcd7" height="15" 
                        >State:</td>
                      <td class="Content Color2" style="HEIGHT: 15px" align="left" bgColor="#999999">
                      <asp:dropdownlist CssClass="InputForm" id="ddlBState" runat="server" Width="241"></asp:dropdownlist>
                      <asp:textbox id="txtBState" CssClass="InputForm" runat="server" Width="241px" Visible="False" ></asp:textbox>
                      </td></tr>
                    <tr>
                      <td class="Content Color2 Bold" style="WIDTH: 171px" align="right" 
                      bgColor="#dbdcd7" height="20" 
                      >Country:</td>
                      <td class="Content Color2" align="left" bgColor="#999999" 
                      ><asp:dropdownlist CssClass="InputForm" id="ddlBCountry" runat="server" Width="240px" Height="22" AutoPostBack="True" onselectedindexchanged="ddlBCountry_SelectedIndexChanged"></asp:dropdownlist><br />
                      <asp:textbox CssClass="InputForm" id="txtBCountry" runat="server" Visible="False"></asp:textbox>
                      </td>
                    </tr>
                  </table>
                  <br />
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td align="center"></td>
        </tr>
      </table>
      <br />
    </td>
  </tr>
  <tr>
    <td align="center"><asp:button id="btnBack" runat="server" CssClass="Content Color2" Width="92px" Text="Back" CausesValidation="False" onclick="btnBack_Click"></asp:button>&nbsp;&nbsp;&nbsp; 
      <asp:button id="btnContinue" runat="server" Width="95px" CssClass="Content Color2" Text="Continue" CausesValidation="False" onclick="btnContinue_Click"></asp:button>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>

</asp:Content>