<%@ Page Language="c#" Codebehind="Resin_Entry.aspx.cs" AutoEventWireup="True" Inherits="localhost.Spot.Resin_Entry" MasterPageFile="~/MasterPages/Template.Master" Title="Resin Entry" MaintainScrollPositionOnPostback="true"  EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/MasterPages/Template.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="asp" namespace="System.Web.UI" %>

<asp:Content runat="server" ContentPlaceHolderID="cphCssLink">
    <style type="text/css">
        .LeftAlign {text-align:left;}
    </style>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphJavaScript">

    <script type="text/javascript" language="JavaScript">
		if(document.all && !document.getElementById) 
		{
			document.getElementById = function(id) { 
				return document.all[id]; 
			}
		}

		function Address_PopUp_Details()
		// This function opens up a pop up window and passes the appropiate ID number for the window
		{
			if (document.all)
				var xMax = screen.width, yMax = screen.height;
			else
				if (document.layers)
					var xMax = window.outerWidth, yMax = window.outerHeight;
				else
					var xMax = 640, yMax=480;

			var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
			if (document.Form1.ddlDeliveryPoint.options[document.Form1.ddlDeliveryPoint.selectedIndex].value!="0")
			{
				var str = "../administrator/Location_Details.aspx?ID=" + document.Form1.ddlDeliveryPoint.options[document.Form1.ddlDeliveryPoint.selectedIndex].value;
				window.open(str,'Address','location=true,toolbar=true,width=275,height=370,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');
			}
		}
    </script>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphInstructions">
    <table>
        <tr>
            <td width="35">
                <img src='../pics/bullet.gif' /></td>
            <td>
                Please fill the form below and provide the most complete and accurate details as
                possible.</td>
        </tr>
        <tr>
            <td width="35">
                <img src='../pics/bullet.gif' /></td>
            <td>
                Upon submitting, our system will display current offers that most closely match
                your request.</td>
        </tr>
        <tr>
            <td width="35">
                <img src='../pics/bullet.gif' /></td>
            <td>
                Purchase one of the offers or contact our trading desk (312.202.0002) to discuss
                your requirements.</td>
        </tr>
        <tr>
            <td width="35">
                <img src='../pics/bullet.gif' /></td>
            <td>
                Our trading desk will also scour the market to fill your need.</td>
        </tr>
        <tr>
            <td width="35">
                <img src='../pics/bullet.gif' /></td>
            <td>
                Expect our further follow up by email or phone.</td>
        </tr>
    </table>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphHeading">
    <span class="Header Bold Color1">Request Resin</span></asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphMain">

    <asp:ScriptManager id="ScriptManager1" runat="Server">
    </asp:ScriptManager>
    
     <asp:UpdatePanel id="UpdatePanel1" runat="Server">
 <ContentTemplate> 

    <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
        <tr>
            <td valign="top">
                <table height="1" cellspacing="0" cellpadding="0" width="100%" bgcolor="#fed400"
                    border="0">
                    <tr>
                        <td id="td2">
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlMain" runat="server">
                    <asp:Label ID="lblMarketMode" runat="server" Visible="False"></asp:Label><asp:Label
                        ID="lblID" runat="server" Visible="False"></asp:Label></asp:Panel>
                <table id="Table1" width="100%" cellspacing="10" border="0" bgcolor="#000000">
                    <tr>
                        <td valign="top" align="center" rowspan="2">
                            <asp:Image ID="imgDomestic" runat="server" ImageUrl="/Pics/icons/icon_US_Flag.jpg"></asp:Image><asp:Image
                                ID="imgInternational" runat="server" ImageUrl="/Pics/icons/icon_Globe.gif" Visible="False">
                            </asp:Image></td>
                        <td nowrap>
                            <asp:Label ID="lblMarket" runat="server" Font-Bold="True" CssClass="Header Bold Color4">U.S. Domestic Market</asp:Label></td>
                        <td class="LinkNormal Color4">
                            <asp:LinkButton ID="lnkInternationalMarket" runat="server" CausesValidation="False"
                                ForeColor="White" OnClick="LinkButton1_Click">Change to International Market</asp:LinkButton>
                        </td>
                        <td width="300">
                        </td>
                    </tr>
                </table>
                <table height="25" cellspacing="0" cellpadding="0" width="780" border="0">
                    <tr>
                        <td class="Content Color2 Bold">
                            Resin Details</td>
                    </tr>
                </table>
                <table cellspacing="1" cellpadding="0" width="575" bgcolor="000000" border="0" align="center">
                    <asp:Panel ID="pnlTypeofEntry" runat="server">
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                <asp:Label ID="lblTypeEntry" runat="server">Type of Entry:</asp:Label></td>
                            <td class="Content Color2" align="left" bgcolor="#9a9b96">
                                <asp:DropDownList ID="ddlTypeEntry" runat="server" AutoPostBack="true" CssClass="InputForm"
                                    OnSelectedIndexChanged="ddlTypeEntry_SelectedIndexChanged">
                                </asp:DropDownList></td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                            <asp:Label ID="lblProduct" runat="server">Product Grade:</asp:Label>
                        </td>
                        <td class="Content Color2" height="29" nowrap bgcolor="#9a9b96" align="left">
                            <asp:DropDownList CssClass="InputForm" ID="ddlProduct" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:Label ID="Label2" runat="server" CssClass="Content Color2 Bold">OR <span class="Content Color2">enter other</SPAN></asp:Label>
                            <asp:TextBox CssClass="InputForm" onkeypress="return noenter()" ID="txtProduct" runat="server"></asp:TextBox>
                            <asp:CustomValidator ID="cmvProduct" runat="server" ErrorMessage="*"></asp:CustomValidator>
                            <asp:CustomValidator ID="cmvProduct2" runat="server" ErrorMessage="*" Display="None"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                            <asp:Label ID="lblDetails" runat="server" Font-Bold="True">Additional Details:</asp:Label>
                        </td>
                        <td class="Content Color2" align="left" width="200" height="29" valign="middle" bgcolor="#9a9b96">
                            <asp:TextBox onkeypress="return noenter()" ID="txtDetail" runat="server" MaxLength="75"
                                size="62" CssClass="InputForm"></asp:TextBox>
                        </td>
                    </tr>
                    
                   <asp:Panel ID="pnlQualitySpecs" runat="server" >                                          
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                Quality:</td>
                            <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                <asp:DropDownList ID="ddlQuality" runat="server" AutoPostBack="False" CssClass="InputForm"
                                    OnSelectedIndexChanged="ddlQuality_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:CustomValidator ID="cmvQuality" runat="server" ErrorMessage="*"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                <asp:Label ID="lblMelt" runat="server">Specifications:</asp:Label>
                            </td>
                            <td bgcolor="#9a9b96">
                                <table>
                                    <tr>
                                        <td class="Content Color2" width="80">
                                            Melt:</td>
                                        <td class="Content Color2" width="80">
                                            <asp:Label ID="Label1" runat="server">Density:</asp:Label>
                                        </td>
                                        <td class="Content Color2" width="120" align="left">
                                            <asp:Label ID="Label3" runat="server">Adds or Izod:</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="middle" class="Content Color2">
                                            <asp:TextBox onkeypress="return noenter()" ID="txtMelt" runat="server" CssClass="InputForm"
                                                size="4" MaxLength="16" type="text"></asp:TextBox></td>
                                        <td valign="middle" class="Content Color2">
                                            0.<asp:TextBox onkeypress="return noenter()" ID="txtDensity" runat="server" CssClass="InputForm"
                                                size="4" MaxLength="16" type="text"></asp:TextBox></td>
                                        <td valign="middle" class="Content Color2" align="center">
                                            <br />
                                            &nbsp;&nbsp;&nbsp;<asp:TextBox onkeypress="return noenter()" ID="txtAdds" runat="server"
                                                CssClass="InputForm" size="4" MaxLength="20" type="text"></asp:TextBox><asp:CompareValidator
                                                    ID="cpvDensity" runat="server" ErrorMessage="*" ControlToValidate="txtDensity"
                                                    Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator><asp:RangeValidator
                                                        ID="rgvDensity" runat="server" ErrorMessage="Density must be between 0.001 and 0.999."
                                                        Type="Integer" ControlToValidate="txtDensity" MinimumValue="1" MaximumValue="999"></asp:RangeValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </asp:Panel> 
                   <asp:Panel ID="pnlNotes" runat="server" visible="false" >                                          
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                Internal Notes:</td>
                            <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                <asp:TextBox id="txtNotes" TextMode="multiline" width="375"  size="162" CssClass="InputForm" runat="server" ></asp:TextBox>
                            </td>
                        </tr>
                    
                    </asp:Panel> 
                    <tr>
                        <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                            <asp:Label ID="lblQuantity" runat="server">Amount:</asp:Label></td>
                        <td bgcolor="#9a9b96">
                            <table border="0" width="400">
                                <tr align="left">
                                    <td align="left" class="Content Color2" width="30">
                                        Qty:</td>
                                    <td align="left" width="170">
                                    </td>
                                    <td align="left" class="Content Color2" width="150">
                                        Units:</td>
                                </tr>
                                <tr>
                                    <td align="left" class="Content Color2" width="30">
                                        <asp:TextBox onkeypress="return noenter()" ID="txtQty" runat="server" MaxLength="8"
                                            size="8" CssClass="InputForm"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="rfvQuantity"
                                                runat="server" ErrorMessage="*" ControlToValidate="txtQty"></asp:RequiredFieldValidator></td>
                                    <td align="left" width="170" nowrap>
                                        <asp:CustomValidator ID="cvQuantity" runat="server" ErrorMessage="Invalid quantity"
                                            ControlToValidate="txtQty" Width="170"></asp:CustomValidator></td>
                                    <td align="left" class="Content Color2" width="150">
                                        <asp:DropDownList ID="ddlQty" runat="server" CssClass="InputForm">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <asp:Panel ID="pnlDeliveryTerms" runat="server" Visible="True">
                        <tr>
                            <td class="Content Color2 Bold" align="right" bgcolor="#dbdcd7" height="29">
                                <asp:Label ID="lblTerms" runat="server">Delivery Terms:</asp:Label>
                            </td>
                            <td align="left" class="Content Color2" bgcolor="#9a9b96">
                                <asp:DropDownList ID="ddlTerms" runat="server" CssClass="InputForm">
                                </asp:DropDownList></td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                            <asp:Label ID="lblPoint" runat="server">Shipping Point:</asp:Label></td>
                        <td bgcolor="#9a9b96">
                            <table border="0">
                                <tr>
                                    <td class="Content Color2" width="80">
                                        <asp:Label ID="lblShippingPointZip" runat="server">Zip Code</asp:Label></td>
                                    <td class="Content Color2" width="20">
                                        <asp:Label ID="lblShippingPointOr" runat="server"><b>OR</b></asp:Label></td>
                                    <td class="Content Color2" width="140">
                                        <asp:Label ID="lblShippingPointDomestic" runat="server">Select a City:</asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="Content Color2">
                                        <asp:TextBox onkeypress="return noenter()" ID="txtZipCode" runat="server" MaxLength="8"
                                            Columns="5" CssClass="InputForm"></asp:TextBox>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td align="left" class="Content Color2">
                                        <br />
                                        <asp:DropDownList ID="ddlDeliveryPoint" runat="server" CssClass="InputForm">
                                        </asp:DropDownList><asp:CustomValidator ID="cmvShipingPoint" runat="server" ErrorMessage="Please enter a valid shipping point."></asp:CustomValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <asp:Panel ID="pnlBuyPrice" runat="server" Visible="true">
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                <asp:Label ID="lblPrice" runat="server">TPE Buy Price:</asp:Label></td>
                            <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                <asp:Label ID="lblPricePrefix" runat="server">$0. </asp:Label>
                                <asp:TextBox onkeypress="return noenter()" ID="txtPrice" runat="server" MaxLength="4"
                                    size="4" CssClass="InputForm"></asp:TextBox>                                    
                                <asp:Label ID="lblPriceMeasure" runat="server">per lb.</asp:Label>                                
                                <asp:RequiredFieldValidator ID="rfvPrice" runat="server" ErrorMessage="*" ControlToValidate="txtPrice"></asp:RequiredFieldValidator>                                
                                <asp:CompareValidator ID="cpvPrice" runat="server" ErrorMessage="<br/>Price must be a number." ControlToValidate="txtPrice" Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                                <asp:CustomValidator ID="cmvPrice" runat="server" onservervalidate="cmvPrice_ServerValidate" Font-Size="11pt" ErrorMessage="<br><b>Please be realistic and place a bid at a fair market price!  Our system does not accept requests less than $.25/lb or $550/Metric Tons.<b>"></asp:CustomValidator>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="pnlMarkup" runat="server" Visible="false">
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                <asp:Label ID="lblMarkup" runat="server">Markup</asp:Label></td>
                            <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                <asp:Label ID="lblMarkupPrefix" runat="server">$0. </asp:Label>
                                <asp:TextBox onkeypress="return noenter()" ID="txtMarkup" runat="server" MaxLength="4"
                                    size="4" CssClass="InputForm">02</asp:TextBox>
                                <asp:Label ID="lblMarkupMeasure" runat="server">per lb.</asp:Label>
                                <asp:CompareValidator ID="cpvMarkup" runat="server" ErrorMessage="Markup must be a number."
                                    ControlToValidate="txtMarkup" Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                                <asp:RequiredFieldValidator ID="rfvMarkup" runat="server" ErrorMessage="*" ControlToValidate="txtMarkup"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="pnlExpiration" runat="server">
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                <asp:Label ID="lblExpiration" runat="server">Offer Expiration:</asp:Label></td>
                            <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                <asp:DropDownList ID="ddlExpires" runat="server" CssClass="InputForm">
                                </asp:DropDownList></td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="pnlMarket" runat="server" Visible="false">
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                <asp:Label ID="lblMarketLabel" runat="server"><u>Do NOT</u> display for: </asp:Label></td>
                            <td class="Content Color2" style="background-color: #9a9b96" align="left">
                                <asp:RadioButtonList ID="rblMarket" runat="server" CssClass="InputForm LeftAlign"
                                    BackColor="#9a9b96" BorderWidth="0">
                                    <asp:ListItem Value="0" Selected="True">Not Applicable</asp:ListItem>
                                    <asp:ListItem Value="1">International Market</asp:ListItem>
                                    <asp:ListItem Value="2">Domestic Market</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </asp:Panel>
                    
                    <asp:Panel ID="pnlPleaseWait" runat="server">
                        <tr>
                            <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                    &nbsp;
                            </td>
                            <td class="Content Color2" style="background-color: #dbdcd7" align="left">
                                  <asp:UpdateProgress id="UpdateProgress1" runat="server">
                                        <ProgressTemplate>  
                                            <asp:Image ID="imgPleaseWait" runat="server" ImageUrl="http://www.theplasticexchange.com/images/progressbar_green.gif" />
                                                <br />
                                                Loading...                                                                                            
                                        </ProgressTemplate>        
                                </asp:UpdateProgress> 
                            </td>
                        </tr>
                    </asp:Panel>
                    
                    
                    <asp:Panel ID="pnlSubmitAction" runat="server" Visible="false">
                        <tr>
                            <td class="Content Color2 Bold" nowrap align="right" width="135" bgcolor="#dbdcd7"
                                height="29">
                                <asp:Label ID="lblSubmitAction" runat="server">Submit Action</asp:Label></td>
                            <td class="Content Color2" align="left" bgcolor="#9a9b96">
                                <asp:RadioButton ID="rdbNew" runat="server" Text="Save as a new Entry, archiving this one.  "
                                    Checked="True" GroupName="Save"></asp:RadioButton><br />
                                <asp:RadioButton ID="rdbEdit" runat="server" Text="Save as the same Entry." GroupName="Save">
                                </asp:RadioButton></td>
                        </tr>
                    </asp:Panel>
                </table>
                <br>
                <asp:Panel ID="pnlContactDetails" runat="server">
                    <table height="25" cellspacing="0" cellpadding="0" width="780" border="0">
                        <tr>
                            <td class="Content Color2 Bold">
                                Contact Details</td>
                        </tr>
                    </table>
                    <table cellspacing="1" cellpadding="5" width="575" bgcolor="#000000" border="0" align="center">
                        <asp:Panel ID="pnlSelectCompany" runat="server" Visible="false">
                            <tr>
                                <td class="Content Color2 Bold" align="right" width="135" bgcolor="#dbdcd7" height="29">
                                    <asp:Label ID="lblCompany" runat="server" Font-Bold="True">Company:</asp:Label>
                                </td>
                                <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                    <asp:DropDownList ID="ddlCompanies" runat="server" AutoPostBack="true" CssClass="InputForm"
                                        OnSelectedIndexChanged="ddlCompanies_SelectedIndexChanged">
                                    </asp:DropDownList>&nbsp;
                                    <asp:DropDownList ID="ddlUser" runat="server" Visible="False" CssClass="InputForm"
                                        OnSelectedIndexChanged="ddlUser_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <div align="center">
                                        <asp:Label ID="lblUserEditUser" CssClass="LinkNormal" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;<asp:Label
                                            ID="lblUserAddUser" CssClass="LinkNormal" runat="server"></asp:Label></div>
                                    <b>OR</b> enter company
                                    <asp:TextBox onkeypress="return noenter()" ID="txtCompany" runat="server" Width="190px"
                                        CssClass="InputForm"></asp:TextBox>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlNonLoggedContactInfo" runat="server" Visible="false">
                            <tr>
                                <td class="Content Color2 Bold" valign="middle" width="135" align="right" bgcolor="#dbdcd7">
                                    <asp:Label ID="lblName" runat="server">Name:</asp:Label></td>
                                <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                    <asp:TextBox ID="txtName" runat="server" Width="200px" CssClass="InputForm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="*" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="Content Color2 Bold" valign="middle" align="right" bgcolor="#dbdcd7">
                                    <asp:Label ID="lblEmail" runat="server">E-mail:</asp:Label></td>
                                <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                    <asp:TextBox ID="txtEmail" runat="server" Width="200px" CssClass="InputForm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="*" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="Content Color2 Bold" valign="middle" align="right" bgcolor="#dbdcd7">
                                    <asp:Label ID="lblPhone" runat="server" Font-Bold="True">Phone:</asp:Label></td>
                                <td class="Content Color2" bgcolor="#9a9b96" align="left">
                                    <asp:TextBox ID="txtPhone" runat="server" Width="150px" CssClass="InputForm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ErrorMessage="*" ControlToValidate="txtPhone"></asp:RequiredFieldValidator></td>
                            </tr>
                        </asp:Panel>
                    </table>
                </asp:Panel>
                &nbsp;&nbsp;
                <p align="center">
                    <br>
                    <asp:ImageButton ID="cmdSubmit" ImageUrl="../images/buttons/submit_unpressed.jpg"
                        runat="server"></asp:ImageButton></p>                        
                <br>
                <asp:Panel ID="pnlConfirmation" runat="server">
                    <table>
                        <tr>
                            <td align="center" colspan="43">
                                <asp:Label ID="lblMessage" runat="server" Width="550px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="cmdNewEntry" runat="server" Visible="False" Text="Enter a new one"
                                    OnClick="cmdNewEntry_Click"></asp:Button>
                                <asp:Button ID="cmdNo" runat="server" CausesValidation="False" Text="Back" Width="125px"
                                    OnClick="cmdNo_Click"></asp:Button>&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="cmdYes" runat="server" Text="Continue" Width="125px" OnClick="cmdYes_Click">
                                </asp:Button>
                                <asp:Button ID="cmdSpotFloor" runat="server" Visible="False" Text="Take me to the Spot Floor"
                                    OnClick="cmdSpotFloor_Click"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>

    </table>
    
        
    </ContentTemplate>
 </asp:UpdatePanel>   
    
</asp:Content>
