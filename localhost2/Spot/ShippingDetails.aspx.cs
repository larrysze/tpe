using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Web.Mail;
using System.Threading;

          /*****************************************************************************
          '*   1. File Name       : Spot/ShippingDetails.aspx                          *
          '*   2. Description     : This page shown the  selected offer detail/        *
          '*						bid details that is passed from OfferDetails.aspx  *
		  '*                        or Biddetails.aspx and in this page we can take    *
		  '* 						the shipto and billto address from the user where  *
		  '*  						user wants ship and bill the offer/bid .           *
		  '*                                                                           *
          '*   3. Modification Log:                                                    *
          '*     Ver No.       Date          Author             Modification           *
          '*   -----------------------------------------------------------------       *
          '*      1.00                      Hanu software        Comment               *
          '*                                                                           *
          '*****************************************************************************/
namespace localhost.Spot
{
	/// <summary>
	/// Summary description for ShippinDetails.
	/// </summary>
	public class ShippingDetails : System.Web.UI.Page
	{

		private System.Data.SqlClient.SqlCommand cmdInsert;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Label lblEmail;
		protected System.Web.UI.WebControls.Label lblType;
		protected System.Web.UI.WebControls.Label lblComment;
		protected System.Web.UI.WebControls.Label lblPrice;
		protected System.Web.UI.WebControls.Label lblOrigin;
		protected System.Web.UI.WebControls.Label lblExpiration;
		protected System.Web.UI.WebControls.Label lblCompany;
		protected System.Web.UI.WebControls.Label lblPayment;
		protected System.Web.UI.WebControls.Label lblFirm;
		protected System.Web.UI.WebControls.Label lblDescription;
		protected System.Web.UI.WebControls.TextBox txtComments;
		protected System.Web.UI.WebControls.Panel pnMain;
		protected System.Web.UI.WebControls.Panel pnThanks;
		protected System.Web.UI.WebControls.Button btnSubmit;
		protected System.Web.UI.WebControls.TextBox txtBCompName;
		protected System.Web.UI.WebControls.TextBox txtBAdds;
		protected System.Web.UI.WebControls.TextBox txtCache;
		protected System.Web.UI.WebControls.Label lblID;
		protected System.Web.UI.WebControls.Label lblProduct;
		protected System.Web.UI.WebControls.Label lblSize;
		protected System.Web.UI.WebControls.Label lblMelt;
		protected System.Web.UI.WebControls.Label lblDens;
		protected System.Web.UI.WebControls.Label lblAdds;
		protected System.Web.UI.WebControls.Label lblTerms;
		protected System.Web.UI.WebControls.TextBox txtSCname;
		protected System.Web.UI.WebControls.TextBox txtSAddress1;
		protected System.Web.UI.WebControls.TextBox txtSCity;
		protected System.Web.UI.WebControls.TextBox txtSZip;
		protected System.Web.UI.WebControls.DropDownList ddlSState;
		protected System.Web.UI.WebControls.DropDownList ddlSCountry;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox TxtChk;
		protected System.Web.UI.WebControls.TextBox txtBCname;
		protected System.Web.UI.WebControls.TextBox txtBAddress1;
		protected System.Web.UI.WebControls.TextBox txtBCity;
		protected System.Web.UI.WebControls.TextBox txtBZip;
		protected System.Web.UI.WebControls.DropDownList ddlBState;
		protected System.Web.UI.WebControls.DropDownList ddlBCountry;
		protected string TmpSId;
		protected System.Web.UI.WebControls.ImageButton btnShipAddress;
		protected System.Web.UI.WebControls.ImageButton btnBillAddress;
		protected System.Web.UI.HtmlControls.HtmlInputButton btnContinue;
		protected System.Web.UI.HtmlControls.HtmlInputButton BackButton;
		protected string TmpBId; 
		public string pathServer = "";
		protected System.Web.UI.WebControls.TextBox txtHShip;
		protected System.Web.UI.WebControls.TextBox txtHBill; 
		public int PARAM_LOCL_ID;
		private void InitializeComponent()
		{
			this.btnShipAddress.Click += new System.Web.UI.ImageClickEventHandler(this.btnShipAddress_Click);
			this.btnBillAddress.Click += new System.Web.UI.ImageClickEventHandler(this.btnBillAddress_Click);
			this.BackButton.ServerClick += new System.EventHandler(this.BackButton_ServerClick);
			this.btnContinue.ServerClick += new System.EventHandler(this.btnContinue_ServerClick);
			this.Load += new System.EventHandler(this.Page_Load);

		}
	
		private void Page_Load(object sender, System.EventArgs e)
		{							
			/// without login nobody can access this page always open default page
			
			if (Session["ID"] == null)
			{
				Response.Redirect("/default.aspx");
			}
			try
			{
				string [] ArrProd = Session.Contents["ProductID"].ToString().Split(new Char []{'^'}) ;		
				ViewState["ID"]=ArrProd[1].ToString();
			}
			catch 
			{
				//if session value expires User is redirected to spot floor
				Response.Redirect("Spot_Floor.aspx");
			}
			if (!IsPostBack)
			{
				CreateCountryDDL();   /// Filling shipping Country combo
				CreateStateDDL();     /// Filling shipping State combo	

				CreateCountryDDL1();  /// Filling billing Country combo
				CreateStateDDL1();    /// Filling billing Country combo
				
				SqlConnection conn;
				conn = new SqlConnection(Application["DBConn"].ToString());
				conn.Open();
				/// <summary>
				/// query offers if offer is selected in the spot_floor.aspx and user came here from offerdetails.aspx
				/// </summary>
				SqlCommand cmdOffer;
				SqlDataReader dtrOffer;
				StringBuilder sbSQL = new StringBuilder();			
				sbSQL.Append("	SELECT *, ");
				sbSQL.Append("	VARTERM=(CASE WHEN OFFR_TERM= 'FOB/Delivered' THEN 'Delivered' WHEN  ");
				sbSQL.Append("	OFFR_TERM= 'FOB/Shipping' THEN 'FOB ' + CAST(ISNULL(OFFR_FROM_LOCL_NAME,(SELECT  ");
				sbSQL.Append("	LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL  ");
				sbSQL.Append("	FROM PLACE WHERE PLAC_ID=OFFR_FROM))) AS VARCHAR) END ), ");
				//New Aditions  on 14th Jan
				sbSQL.Append("		VARSIZE=CAST(OFFR_QTY AS VARCHAR)+' '+(CASE OFFR_SIZE WHEN 1 THEN ");
				sbSQL.Append("		'lb' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 42000 ");
				sbSQL.Append("		THEN 'T/L - Boxes' WHEN 45000 THEN 'Bulk Truck' WHEN 44092 THEN 'T/L Bags' ");
				sbSQL.Append("		WHEN 190000 THEN 'Rail Car'	END)+(CASE WHEN (OFFR_QTY>1 AND OFFR_SIZE <> 42000 AND OFFR_SIZE <> 44092 ) THEN 's' ELSE ' ' END) ");

				sbSQL.Append("	FROM BBOFFER ");
				sbSQL.Append("	WHERE  ");
				sbSQL.Append("	OFFR_ID= '"+ViewState["ID"].ToString()+"' ");
				
				cmdOffer = new SqlCommand(sbSQL.ToString(),conn );
				dtrOffer = cmdOffer.ExecuteReader();

				if (dtrOffer.Read())
				{
					/// offer details are displayed here in different label that parameter recd. from spot_floor.aspx page 
					lblID.Text = dtrOffer["OFFR_ID"].ToString();
					lblProduct.Text = dtrOffer["OFFR_PROD"].ToString();
					//lblSize.Text = String.Format("{0:#,###}", dtrOffer["OFFR_SIZE"].ToString());
					lblSize.Text = dtrOffer["VARSIZE"].ToString();					
					lblMelt.Text = dtrOffer["OFFR_MELT"].ToString();
					lblDens.Text = dtrOffer["OFFR_DENS"].ToString();
					lblAdds.Text = dtrOffer["OFFR_ADDS"].ToString();
					lblTerms.Text = dtrOffer["VARTERM"].ToString();
					
					ViewState["Firm"] = dtrOffer["OFFR_FIRM"].ToString();
					ViewState["Origin"] = dtrOffer["OFFR_FROM_LOCL"].ToString();
					ViewState["Expiration"] = dtrOffer["OFFR_EXPR"].ToString();
					ViewState["TERMS"] = dtrOffer["OFFR_PAY"].ToString();
					ViewState["TYPE"] = "Offer";
					dtrOffer.Close();
				}
				else
				{
					dtrOffer.Close();

					/// <summary>
					/// query bids if bid is selected in the spot_floor.aspx and user came here from biddetails.aspx page
					/// </summary>
					
					
					SqlCommand cmdBid;
					SqlDataReader dtrBid;
					sbSQL.Remove(0,sbSQL.Length);
					sbSQL.Append("		SELECT *, ");
					sbSQL.Append("		VARTERM=(CASE WHEN BID_TERM= 'FOB/Delivered' THEN 'Delivered' WHEN  ");
					sbSQL.Append("						BID_TERM= 'FOB/Shipping' THEN 'FOB ' + CAST(ISNULL(BID_TO_LOCL_NAME,(SELECT  ");
					sbSQL.Append("						LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL  ");
					sbSQL.Append("						FROM PLACE WHERE PLAC_ID=BID_TO))) AS VARCHAR) END ), ");
					//New Aditions  on 14th Jan
					sbSQL.Append("		VARSIZE=CAST(BID_QTY AS VARCHAR)+' '+(CASE BID_SIZE WHEN 1 THEN ");
					sbSQL.Append("		'lb' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 42000 ");
					sbSQL.Append("		THEN 'T/L - Boxes' WHEN 45000 THEN 'Bulk Truck' WHEN 44092 THEN 'T/L Bags' ");
					sbSQL.Append("		WHEN 190000 THEN 'Rail Car'	END)+(CASE WHEN (BID_QTY>1 AND BID_SIZE <> 42000 AND BID_SIZE <> 44092 ) THEN 's' ELSE ' ' END) ");

					sbSQL.Append("		FROM BBID ");
					sbSQL.Append("		WHERE  ");
					sbSQL.Append("			BID_ID= '"+ViewState["ID"].ToString()+"' ");
					
					cmdBid = new SqlCommand(sbSQL.ToString(),conn );
					dtrBid = cmdBid.ExecuteReader();
  
					if (dtrBid.Read())
					{
						/// Bid details are displayed here in different label that parameter recd. from spot_floor.aspx page 
						lblID.Text = dtrBid["BID_ID"].ToString();
						lblProduct.Text = dtrBid["BID_PROD"].ToString();
						//lblSize.Text = String.Format("{0:#,###}", dtrBid["BID_SIZE"].ToString());
						lblSize.Text = dtrBid["VARSIZE"].ToString();					
						lblMelt.Text = dtrBid["BID_MELT"].ToString();
						lblDens.Text = dtrBid["BID_DENS"].ToString();
						lblAdds.Text = dtrBid["BID_ADDS"].ToString();
						lblTerms.Text = dtrBid["VARTERM"].ToString();
											
						ViewState["Firm"] = dtrBid["BID_FIRM"].ToString();
						ViewState["Origin"] = dtrBid["BID_TO_LOCL"].ToString();
						ViewState["Expiration"] = dtrBid["BID_EXPR"].ToString();
						ViewState["TERMS"] = dtrBid["BID_PAY"].ToString();
						ViewState["TYPE"] = "Bid";
						dtrBid.Close();
					
					}
					else
					{
						// output error
					}
					/// connection close
					dtrBid.Close();
		
				}
				conn.Close();
			}
			// Checking whether the user is Demo
			SqlConnection Conn =  new SqlConnection(Application["DBConn"].ToString());
			SqlCommand SqlChkType = new SqlCommand();
			SqlChkType.CommandText = "Select Pers_Type from person where PERS_ID=" + Convert.ToUInt16(Session["ID"]);
			Conn.Open();
			SqlChkType.Connection=Conn;
			Session["UType"] = Convert.ToString(SqlChkType.ExecuteScalar());
			if (Session["UType"].ToString().Equals("D"))
			{
				btnShipAddress.Visible=false;
				btnBillAddress.Visible=false;
			}
			else
			{
				btnShipAddress.Visible=true;
				btnBillAddress.Visible=true;
				
			}
			//EnableDisable();	
			///**************************************************/
			///Checking whether the session "ShipId" or "BillId" exist when it comes back from change shipping screen
			///if it exists then the address remains unchanged in billing or shipping address or vice versa 
			///**************************************************/

			if (Session.Contents["ShipId"]!=null)
			{
				
				TmpSId= Session.Contents["ShipId"].ToString();
				//Session.Contents["ShipId"]=TmpSId;
				if (!IsPostBack)
				{
					FillShipAddress(TmpSId);	
				}
			}
			
			if (Request.QueryString["SId"]!= null )
			{
				TmpSId= Request.QueryString["SId"];
				Session.Contents["ShipId"]=TmpSId;
				if (!IsPostBack)
				{
					FillShipAddress(TmpSId);
					txtHShip.Text=TmpSId.ToString();
				}
			}



			if (Session.Contents["BillId"]!=null )
			{
				TmpBId= Session.Contents["BillId"].ToString();
//				Session.Contents["BillId"]=TmpBId;
				if (!IsPostBack)
				{
					FillBillAddress(TmpBId);	
				}
			}
			
			if (Request.QueryString["BId"]!= null )
			{
				TmpBId= Request.QueryString["BId"];
				Session.Contents["BillId"]=TmpBId;
				if (!IsPostBack)
				{
					FillBillAddress(TmpBId);	

					txtHBill.Text=TmpBId.ToString();
				}
			}
			//this array is used to the productid with seperator that is differentiate that it's for bid or offer
			//string [] ArrProd = Session.Contents["ProductID"].ToString().Split(new Char []{'^'}) ;		
			//ViewState["ID"]=ArrProd[1].ToString();

			
			//ViewState["ID"]=Session.Contents["ProductID"].ToString();
			
			/// check here that page is post back or not if not than country and state combo box are 
			/// displayed with data so we call the  CreateCountryDDL(),CreateStateDDL()
			/// CreateCountryDDL1(),CreateStateDDL1() functions
			
		
		}
		private void EnableDisable()
		{
			if (Session["UType"].ToString().Equals("O"))
			{	
				foreach (Control c in Page.Controls)
				{
					foreach (Control childc in c.Controls)
					{
						if (childc is TextBox)
						{   
							((TextBox)childc).ReadOnly =true;
						}
					
					}
				}
			}
		}
	
		protected void ChangeCountry(object sender, EventArgs e)
		{
			CreateStateDDL();
		}
		// function call for fill the  shipto address state combo box			
		private void CreateStateDDL()
		{
			SqlCommand cmdStates;
			SqlDataReader dtrStates;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();

			// opening state drop-down list
			cmdStates = new SqlCommand("SELECT STAT_NAME,STAT_CODE From STATE order by STAT_NAME ASC",conn);
			dtrStates = cmdStates.ExecuteReader();			
			
			// binding state drop-down list
			ddlSState.DataSource = dtrStates;
			ddlSState.DataTextField= "STAT_NAME";
			ddlSState.DataValueField= "STAT_CODE";
			ddlSState.DataBind();
			dtrStates.Close();
			conn.Close();
			if(ddlSState.SelectedValue.ToString() == "")
				ddlSState.Items.Add(new ListItem ("No State available",""));

		}
		// function call for fill the  shipto address country combo box		
		private void CreateCountryDDL()
		{
			SqlCommand cmdCountry;
			SqlDataReader dtrCountry;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			// opening state dorp-down list
			cmdCountry = new SqlCommand("SELECT CTRY_NAME,CTRY_CODE From COUNTRY ORDER BY CTRY_NAME ASC",conn);
			dtrCountry = cmdCountry.ExecuteReader();
			// binding country dorp-down list
			ddlSCountry.DataSource = dtrCountry;
			ddlSCountry.DataTextField= "CTRY_NAME";
			ddlSCountry.DataValueField= "CTRY_CODE";
			ddlSCountry.DataBind();
			dtrCountry.Close();
			conn.Close();
	
			// select 'us' in the ddl
			ListItem li1 = ddlSCountry.Items.FindByValue("US");
			ddlSCountry.SelectedIndex = -1;
			li1.Selected = true;
		}
		//for billing  ddls
		protected void ChangeCountry1(object sender, EventArgs e)
		{
			CreateStateDDL1();
		}
		private void CreateStateDDL1()
		{
			// function call for fill the  billto address state combo box	
			SqlCommand cmdStates;
			SqlDataReader dtrStates;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();

			// opening state drop-down list
			cmdStates = new SqlCommand("SELECT STAT_NAME,STAT_CODE From STATE order by STAT_NAME ASC",conn);
			dtrStates = cmdStates.ExecuteReader();			
			
			// binding state drop-down list
			ddlBState.DataSource = dtrStates;
			ddlBState.DataTextField= "STAT_NAME";
			ddlBState.DataValueField= "STAT_CODE";
			ddlBState.DataBind();
			dtrStates.Close();
			conn.Close();
			if(ddlBState.SelectedValue.ToString() == "")
				ddlBState.Items.Add(new ListItem ("No State available",""));
				
		}
		private void CreateCountryDDL1()
		{
			// function call for fill the  billto address country combo box
			SqlCommand cmdCountry;
			SqlDataReader dtrCountry;
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			// opening country dorp-down list
			cmdCountry = new SqlCommand("SELECT CTRY_NAME,CTRY_CODE From COUNTRY ORDER BY CTRY_NAME ASC",conn);
			dtrCountry = cmdCountry.ExecuteReader();
			// binding country dorp-down list
			ddlBCountry.DataSource = dtrCountry;
			ddlBCountry.DataTextField= "CTRY_NAME";
			ddlBCountry.DataValueField= "CTRY_CODE";
			ddlBCountry.DataBind();
			dtrCountry.Close();
			conn.Close();
	
			// select 'us' in the ddl
			ListItem li2 = ddlBCountry.Items.FindByValue("US");
			ddlBCountry.SelectedIndex = -1;
			li2.Selected = true;
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion

	
		
		private void BackButton_ServerClick(object sender, System.EventArgs e)
		{
			// back button click and go to offerdetails.aspx/biddetails.aspx as screen refer
			Session.Contents.Remove("ShipId");
			Session.Contents.Remove("BillId");
			
			string[] ArrProd= new string[0] ;
			try
			{
				ArrProd = Session.Contents["ProductID"].ToString().Split(new Char []{'^'}) ;		
				ViewState["ID"]=ArrProd[1].ToString();
			}
			catch 
			{
				//if session value expires User is redirected to spot floor
				Response.Redirect("Spot_Floor.aspx");
			}


			//string [] ArrProd = Session.Contents["ProductID"].ToString().Split(new Char []{'^'}) ;		
			if(ArrProd[0].ToString()=="O")
				//if user came from offerdetails.aspx then he will back to offerdetails.apx otherwise biddetails.aspx	
			{
				Response.Redirect("OfferDetails.aspx?ID="+ArrProd[1].ToString());
			}
			else
			{
				Response.Redirect("BidDetails.aspx?ID="+ArrProd[1].ToString());
			}
												  
													  
		}


			
		private void btnContinue_ServerClick(object sender, System.EventArgs e)
		{
			if  (txtCache.Text.ToString().Trim()=="0")		///Checking whether value of textbox =1 for checking if bill/ship address filled or not
			{
				return;
			}
			//if (Session["UType"].ToString().Equals("D"))
			//{	
				SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());
				SqlConnection connAdd  = new SqlConnection(Application["DBconn"].ToString());
			
				conn.Open();
				connAdd.Open();

				string strSQL;
				int intIdLocality=0;
			if (txtHShip.Text.Length.Equals(0))
			{

				// Adding Shipping Address
				// Check if the location already exist in shipping matrix
				strSQL="SELECT LOCL_ID FROM LOCALITY WHERE upper(LOCL_CITY)='"+txtSCity.Text.ToUpper() +"' AND ";
				strSQL+="LOCL_STAT='"+ddlSState.SelectedItem.Value+"' AND LOCL_CTRY='"+ddlSCountry.SelectedItem.Value+"'";
				SqlDataReader dtrCheck;
			
				SqlCommand CmdCheck= new SqlCommand(strSQL, conn);
				dtrCheck= CmdCheck.ExecuteReader();
				////New Code 15th Jan
				if (dtrCheck.HasRows)
				{
					while (dtrCheck.Read())
					{
						intIdLocality= Convert.ToInt32(dtrCheck.GetValue(0));
					}

					
				}
				else
				{
					strSQL = "INSERT INTO LOCALITY (LOCL_CITY,LOCL_STAT,LOCL_CTRY) VALUES ('" + txtSCity.Text.ToString() + "' ,'"+ddlSState.SelectedItem.Value.ToString()+"','"+ddlSCountry.SelectedItem.Value.ToString()+"')";
					cmdInsert= new System.Data.SqlClient.SqlCommand(strSQL, connAdd);
					cmdInsert.ExecuteNonQuery();
					
					SqlCommand cmdIdentity = new SqlCommand("SELECT @@IDENTITY AS NewID",connAdd);
					intIdLocality = Convert.ToInt32(cmdIdentity.ExecuteScalar());
					//Initiating a New thread
					PARAM_LOCL_ID=intIdLocality;
					pathServer = @Server.MapPath("./").ToString() + @"/sqlDistances.sql"; 

					Thread threadDistances = new Thread(new ThreadStart(calcAllDistances));
					threadDistances.Start();

				}
				
				dtrCheck.Close();

				strSQL = "INSERT INTO PLACE (PLAC_LOCL,PLAC_PERS,PLAC_ADDR_ONE,PLAC_ZIP,PLAC_ENBL,PLAC_TYPE)  VALUES("+ intIdLocality + ",'"+ txtSCname.Text + "','" + txtSAddress1.Text+ "','"+txtSZip.Text+"',1,'D')"; 
				cmdInsert= new System.Data.SqlClient.SqlCommand(strSQL,connAdd);
				cmdInsert.ExecuteNonQuery();

				SqlCommand cmdShip = new SqlCommand("SELECT @@IDENTITY AS NewID",connAdd);
				Session.Contents["ShipId"]= cmdShip.ExecuteScalar();
				
				SqlCommand CmdUpdate = new SqlCommand("Update Place set PLAC_COMP=(Select Comp_ID from company where comp_id=(select pers_comp from person where pers_id=" + Convert.ToInt32(Session["ID"]) + "))where PLAC_TYPE='D' and PLAC_ID="+Convert.ToInt32(Session["ShipId"]),connAdd);
				CmdUpdate.ExecuteNonQuery();
				}
			else
			{
				Session.Contents["ShipId"]= txtHShip.Text.ToString();// cmdShip.ExecuteScalar();
			}
			
				////Adding Billing Address
				///
			if (txtHBill.Text.Length.Equals(0))
			{
				if (TxtChk.Text.ToString().Trim() =="0")
				{
					strSQL="SELECT LOCL_ID FROM LOCALITY WHERE upper(LOCL_CITY)='"+txtBCity.Text.ToUpper() +"' AND ";
					strSQL+="LOCL_STAT='"+ddlBState.SelectedItem.Value+"' AND LOCL_CTRY='"+ddlBCountry.SelectedItem.Value+"'";
					SqlDataReader dtrBCheck;
			
					SqlCommand cmdBCheck= new SqlCommand(strSQL, conn);
					//cmdBCheck.CommandText=strSQL.ToString();
					cmdBCheck.Connection=conn;
					dtrBCheck= cmdBCheck.ExecuteReader();
					////New Code 15th Jan
					if (dtrBCheck.HasRows)
					{
						while (dtrBCheck.Read())
						{
							intIdLocality= Convert.ToInt32(dtrBCheck.GetValue(0));
						}

						
					}
					else
					{
						strSQL = "INSERT INTO LOCALITY (LOCL_CITY,LOCL_STAT,LOCL_CTRY) VALUES ('" + txtBCity.Text.ToString() + "' ,'"+ddlBState.SelectedItem.Value.ToString()+"','"+ddlBCountry.SelectedItem.Value.ToString()+"')";
						cmdInsert= new System.Data.SqlClient.SqlCommand(strSQL, connAdd);
						cmdInsert.ExecuteNonQuery();
						SqlCommand cmdIdentity = new SqlCommand("SELECT @@IDENTITY AS NewID",connAdd);
						intIdLocality = Convert.ToInt32(cmdIdentity.ExecuteScalar());
						PARAM_LOCL_ID=intIdLocality;
						//Following code to be added for initiating a thread
						pathServer = @Server.MapPath("./").ToString() + @"/sqlDistances.sql"; 

						Thread threadDistances = new Thread(new ThreadStart(calcAllDistances));
						threadDistances.Start();
					}
					dtrBCheck.Close();
					strSQL = "INSERT INTO PLACE (PLAC_LOCL,PLAC_PERS,PLAC_ADDR_ONE,PLAC_ZIP,PLAC_ENBL,PLAC_TYPE)  VALUES("+ intIdLocality+",'"+ txtBCname.Text + "','" + txtBAddress1.Text+ "','"+txtBZip.Text+"',1,'H')"; 
					cmdInsert= new System.Data.SqlClient.SqlCommand(strSQL,connAdd);
					cmdInsert.ExecuteNonQuery();
					SqlCommand cmdBill = new SqlCommand("SELECT @@IDENTITY AS NewID",connAdd);
					Session.Contents["BillId"] =  cmdBill.ExecuteScalar();
					SqlCommand CmdUpdate = new SqlCommand("Update Place set PLAC_COMP=(Select Comp_ID from company where comp_id=(select pers_comp from person where pers_id=" + Convert.ToInt32(Session["ID"]) + "))where PLAC_TYPE='H' and PLAC_ID="+Convert.ToInt32(Session["BillId"]),connAdd);
					CmdUpdate.ExecuteNonQuery();
				}
				else
				{
					//New additions
					strSQL="SELECT LOCL_ID FROM LOCALITY WHERE upper(LOCL_CITY)='"+txtSCity.Text.ToUpper() +"' AND ";
					strSQL+="LOCL_STAT='"+ddlSState.SelectedItem.Value+"' AND LOCL_CTRY='"+ddlSCountry.SelectedItem.Value+"'";
					SqlDataReader dtrBCheck;
			
					SqlCommand cmdBCheck= new SqlCommand(strSQL, conn);
					//cmdBCheck.CommandText=strSQL.ToString();
					cmdBCheck.Connection=conn;
					dtrBCheck= cmdBCheck.ExecuteReader();
					////New Code 15th Jan
					if (dtrBCheck.HasRows)
					{
						while (dtrBCheck.Read())
						{
							intIdLocality= Convert.ToInt32(dtrBCheck.GetValue(0));
						}

						
					}
					else
					{
						strSQL = "INSERT INTO LOCALITY (LOCL_CITY,LOCL_STAT,LOCL_CTRY) VALUES ('" + txtBCity.Text.ToString() + "' ,'"+ddlBState.SelectedItem.Value.ToString()+"','"+ddlBCountry.SelectedItem.Value.ToString()+"')";
						cmdInsert= new System.Data.SqlClient.SqlCommand(strSQL, connAdd);
						cmdInsert.ExecuteNonQuery();
						SqlCommand cmdIdentity = new SqlCommand("SELECT @@IDENTITY AS NewID",connAdd);
						intIdLocality = Convert.ToInt32(cmdIdentity.ExecuteScalar());
						PARAM_LOCL_ID=intIdLocality;
						//Following code to be added for initiating a thread
						pathServer = @Server.MapPath("./").ToString() + @"/sqlDistances.sql"; 

						Thread threadDistances = new Thread(new ThreadStart(calcAllDistances));
						threadDistances.Start();
					}
					dtrBCheck.Close();
					//########
					strSQL = "INSERT INTO PLACE (PLAC_LOCL,PLAC_PERS,PLAC_ADDR_ONE,PLAC_ZIP,PLAC_ENBL,PLAC_TYPE)  VALUES("+ intIdLocality + ",'"+ txtSCname.Text + "','" + txtSAddress1.Text+ "','"+txtSZip.Text+"',1,'H')"; 
					cmdInsert= new System.Data.SqlClient.SqlCommand(strSQL,connAdd);
					cmdInsert.ExecuteNonQuery();

					SqlCommand cmdBill = new SqlCommand("SELECT @@IDENTITY AS NewID",connAdd);
					Session.Contents["BillId"]= cmdBill.ExecuteScalar();
					SqlCommand CmdUpdate = new SqlCommand("Update Place set PLAC_COMP=(Select Comp_ID from company where comp_id=(select pers_comp from person where pers_id=" + Convert.ToInt32(Session["ID"]) + "))where PLAC_TYPE='H' and PLAC_ID="+Convert.ToInt32(Session["BillId"]),connAdd);
					CmdUpdate.ExecuteNonQuery();

				}
				
				conn.Close();
				connAdd.Close();
			}
			else
			{
					Session.Contents["BillId"]=txtHBill.Text.ToString();
			}
			Response.Redirect("Total.aspx");
			
			/*
				strSQL="";
	
					//strSQL = "INSERT INTO PLACE (PLAC_COMP,PLAC_LOCL,PLAC_ADDR_ONE,PLAC_ZIP,PLAC_ENBL)  VALUES("+ intIdCompany+","+ intIdLocality+",'"+txtSAddress1.Text+"','"+txtSZip.Text+"',1," + userid  +  ")"; 
					strSQL = "INSERT INTO PLACE (PLAC_LOCL,PLAC_PERS,PLAC_ADDR_ONE,PLAC_ZIP,PLAC_ENBL,PLAC_TYPE)  VALUES("+ intIdLocality+",'"+ txtSCname.Text + "','" + txtSAddress1.Text+ "','"+txtSZip.Text+"',1,'D')"; 
					cmdInsert= new System.Data.SqlClient.SqlCommand(strSQL,connAdd);
					cmdInsert.ExecuteNonQuery();

		//check  box check if the shipping and billing address are same 	
					if (TxtChk.Text.ToString().Trim() =="0")
					{
						if (Session.Contents["BillId"]!=null)
						{

						}
						else
						{
							// LOCALITY
							strSQL = "INSERT INTO LOCALITY (LOCL_CITY,LOCL_STAT,LOCL_CTRY) VALUES ('"+txtSCity.Text+"' ,'"+ddlSState.SelectedItem.Value.ToString()+"','"+ddlSCountry.SelectedItem.Value.ToString()+"')";
							cmdInsert= new System.Data.SqlClient.SqlCommand(strSQL, connAdd);
							cmdInsert.ExecuteNonQuery();
               
							//COMPANY
							strSQL = "INSERT INTO COMPANY (COMP_NAME) VALUES('"+txtSCname.Text+"')";
							cmdInsert= new System.Data.SqlClient.SqlCommand(strSQL, connAdd);
							cmdInsert.ExecuteNonQuery();
							//place
							strSQL = "INSERT INTO PLACE (PLAC_COMP,PLAC_LOCL,PLAC_ADDR_ONE,PLAC_ZIP,PLAC_ENBL,Pers_ID)  VALUES("+ intIdCompany+","+ intIdLocality+",'"+txtSAddress1.Text+"','"+txtSZip.Text+"',1," + userid  +  ")"; 
							cmdInsert= new System.Data.SqlClient.SqlCommand(strSQL,connAdd);
							cmdInsert.ExecuteNonQuery();

							strSQL= "SELECT max(plac_id) from place  "; 
							cmdInsert= new SqlCommand(strSQL,connAdd);
							intIdPlace= Convert.ToInt32(cmdInsert.ExecuteScalar());
							Session.Contents["BillId"]=intIdPlace.ToString();
						}
					}
			}
				// connection close
				conn.Close();
				connAdd.Close();
				//user lead to total.aspx page							  
				//Changes 8th Jan Checking
				//Response.Redirect("Total.aspx?id=" +Session.Contents["ProductId"]);
				Response.Redirect("Total.aspx");
				//*******************************/
			//		 Mailing Code  Below //
			//*******************************/

			
			// email admin to let him know that which user select the offer and 
			// user fill the shipping /billing  addrses 
			/* MailMessage mail = new MailMessage();
						mail.From = "Locations@theplasticsexchange.com";
						mail.To = Application["strEmailAdmin"].ToString();
						mail.Subject = "New Location to be added ";
						mail.Body = txtCity.Text+" "+ddlState.SelectedItem.Value;
						mail.BodyFormat = MailFormat.Html;
						SmtpMail.Send(mail);*/
			
			
	
		
	}

		private void btnShipAddress_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			//click on chossen from address book button lead to changeshipping.aspx page
			Response.Redirect("ChangeShipping.aspx?ShipAdd=S" + "&BillAdd=");
		}
	
		private void FillShipAddress(string TmpSId)
		{
			//function for filling the ship to address choosing from address book	
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			SqlDataReader DR;
			string StrCmd;
			string TmpStr1;
			string TmpStr2;
			StrCmd="SELECT DISTINCT PLACE.PLAC_PERS, PLACE.PLAC_ADDR_ONE, PLACE.PLAC_ADDR_TWO, LOCALITY.LOCL_CITY, PLACE.PLAC_ZIP, STATE.STAT_NAME,";
			StrCmd+="COUNTRY.CTRY_NAME,PLACE.PLAC_ID, COMPANY.COMP_NAME , LOCALITY.LOCL_STAT, LOCALITY.LOCL_CTRY FROM PLACE INNER JOIN LOCALITY ON PLACE.PLAC_LOCL = LOCALITY.LOCL_ID AND PLACE.PLAC_LOCL = LOCALITY.LOCL_ID INNER JOIN ";
			StrCmd+="STATE ON LOCALITY.LOCL_STAT = STATE.STAT_CODE INNER JOIN COUNTRY ON LOCALITY.LOCL_CTRY = COUNTRY.CTRY_CODE INNER JOIN COMPANY ON PLACE.PLAC_COMP = COMPANY.COMP_ID";
			StrCmd+=" Where PLACE.PLAC_ID='"+TmpSId+ "' and PLACE.PLAC_TYPE='D'" ;
			conn.Open();
			SqlCommand Cmd = new SqlCommand(StrCmd,conn);
			DR = Cmd.ExecuteReader();
			
			if (DR.Read())
			{
				//  for fill the  shipto address in textboxes
				txtSCname.Text=DR.GetValue(0).ToString();
				txtSAddress1.Text=DR.GetValue(1).ToString();
				txtSCity.Text=DR.GetValue(3).ToString();
				txtSZip.Text=DR.GetValue(4).ToString();
				TmpStr1=DR.GetValue(9).ToString();
				TmpStr2=DR.GetValue(10).ToString();
				ddlSState.SelectedValue=TmpStr1;
				ddlSCountry.SelectedValue=TmpStr2;

			}
			DR.Close();
			conn.Close();

		}

		private void FillBillAddress(string TmpSId)
		{
			//function for filling the bill to address choosing from address book	
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			SqlDataReader DR;
			string StrCmd;
			string TmpStr1;
			string TmpStr2;
			StrCmd="SELECT DISTINCT PLACE.PLAC_PERS, PLACE.PLAC_ADDR_ONE, PLACE.PLAC_ADDR_TWO, LOCALITY.LOCL_CITY, PLACE.PLAC_ZIP, STATE.STAT_NAME,";
			StrCmd+="COUNTRY.CTRY_NAME,PLACE.PLAC_ID, COMPANY.COMP_NAME , LOCALITY.LOCL_STAT, LOCALITY.LOCL_CTRY FROM PLACE INNER JOIN LOCALITY ON PLACE.PLAC_LOCL = LOCALITY.LOCL_ID AND PLACE.PLAC_LOCL = LOCALITY.LOCL_ID INNER JOIN ";
			StrCmd+="STATE ON LOCALITY.LOCL_STAT = STATE.STAT_CODE INNER JOIN COUNTRY ON LOCALITY.LOCL_CTRY = COUNTRY.CTRY_CODE INNER JOIN COMPANY ON PLACE.PLAC_COMP = COMPANY.COMP_ID";
			StrCmd+=" Where PLACE.PLAC_ID='"+TmpSId+ "' and PLACE.PLAC_TYPE='H'" ;
			conn.Open();
			SqlCommand Cmd = new SqlCommand(StrCmd,conn);
			DR = Cmd.ExecuteReader();
			
			if (DR.Read())
			{
				//  for fill the  billto address in textboxes		
				txtBCname.Text=DR.GetValue(0).ToString();
				txtBAddress1.Text=DR.GetValue(1).ToString();
				txtBCity.Text=DR.GetValue(3).ToString();
				txtBZip.Text=DR.GetValue(4).ToString();
				TmpStr1=DR.GetValue(9).ToString();
				TmpStr2=DR.GetValue(10).ToString();
				ddlBState.SelectedValue=TmpStr1;
				ddlBCountry.SelectedValue=TmpStr2;

			}
			DR.Close();
			conn.Close();

		}

		private void btnBillAddress_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			//click on choose from address book iamge button lead to changeshipping.aspx page
			//
			 Response.Redirect("ChangeShipping.aspx?ShipAdd=''" + "&BillAdd=B");
		}
		private void calcAllDistances()
		{// calculate the distances between the new locality and the the others that already exist.

			// get values about the new place from TMP_PLACE table
			string cityFrom = "";
			string stateFrom = "";	
			string countryFrom = "";	

			SqlConnection conn2 = new SqlConnection(Application["DBconn"].ToString());
			conn2.Open();
			string strSQL2 = "SELECT LOCL_ID,LOCL_CITY,LOCL_STAT,LOCL_CTRY FROM LOCALITY WHERE LOCL_ID = "+PARAM_LOCL_ID ; 
			SqlCommand cmdSelect;
			cmdSelect= new SqlCommand(strSQL2, conn2);
			SqlDataReader dtrSelect= cmdSelect.ExecuteReader();
			while(dtrSelect.Read())
			{
				cityFrom = dtrSelect["LOCL_CITY"].ToString();
				stateFrom = dtrSelect["LOCL_STAT"].ToString();	
				countryFrom = dtrSelect["LOCL_CTRY"].ToString();	
			}
			conn2.Close();	

			float sDistance = 0;

			// initialize geoplaces webservice
			GeoPlaces myGeoPlaces = new GeoPlaces();
			AuthenticationHeader AuthenticationHeaderValue = new AuthenticationHeader();
			AuthenticationHeaderValue.SessionID="Wn7BufIemFWK6F6J5s2aS+UJlA7WPHA44bregtf8HqQtA/RH4b8VNa4JOhZmrPAOFqGvDqbCMwvPUZVjkkriBf/vxKxNHLxi";
			myGeoPlaces.AuthenticationHeaderValue = AuthenticationHeaderValue;

			// get all cities that arre different from the new locality
			string strSQL = "SELECT LOCL_ID,LOCL_CITY,LOCL_STAT,LOCL_CTRY FROM LOCALITY WHERE LOCL_ID <> "+PARAM_LOCL_ID; 
			SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			SqlCommand cmdLocality= new SqlCommand(strSQL, conn);
			SqlDataReader dtrLocality= cmdLocality.ExecuteReader();
			while(dtrLocality.Read())			
			{
				// get the distance between the new locality and all other cities
				sDistance = myGeoPlaces.GetDistanceBetweenPlaces( cityFrom,stateFrom,dtrLocality["LOCL_CITY"].ToString(),dtrLocality["LOCL_STAT"].ToString());
				
				// insert each distance in DISTANCES table
				SqlConnection conn3 = new SqlConnection(Application["DBconn"].ToString());
				conn3.Open();
				string strSQL3="INSERT INTO DISTANCES (CITY_FROM_ID,CITY_FROM_NAME,CITY_TO_ID,CITY_TO_NAME,DISTANCE) VALUES ('"+PARAM_LOCL_ID+"','"+cityFrom+"','"+dtrLocality["LOCL_ID"].ToString()+"','"+dtrLocality["LOCL_CITY"].ToString()+"','"+sDistance.ToString()+"')";
				SqlCommand cmdInsert3;
				cmdInsert3= new SqlCommand(strSQL3, conn3);
				cmdInsert3.ExecuteNonQuery();			
				conn3.Close();

				// insert each distance in DISTANCES table (city_from and city_to are reversed)
				SqlConnection conn4 = new SqlConnection(Application["DBconn"].ToString());
				conn4.Open();
				string strSQL4="INSERT INTO DISTANCES (CITY_FROM_ID,CITY_FROM_NAME,CITY_TO_ID,CITY_TO_NAME,DISTANCE) VALUES ('"+dtrLocality["LOCL_ID"].ToString()+"','"+dtrLocality["LOCL_CITY"].ToString()+"','"+PARAM_LOCL_ID+"','"+cityFrom+"','"+sDistance.ToString()+"')";
				SqlCommand cmdInsert4;
				cmdInsert4= new SqlCommand(strSQL4, conn4);
				cmdInsert4.ExecuteNonQuery();			
				conn4.Close();
			}
			conn.Close();
			//insertPlace();
			
		}
		
	}
} 
