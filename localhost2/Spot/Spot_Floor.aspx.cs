using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.Mail;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;


namespace localhost.Spot
{
    public partial class _Spot_Floor : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["Typ"].ToString() == "A" || Session["Typ"].ToString() == "B" || Session["Typ"].ToString() == "L" || Session["Typ"].ToString() == "X")
              Master.Width = "1220px";
            else
              Master.Width = "780px"; 

            //Instructions1.Content = "";			

            if (Request.QueryString["Product"] != null && Request.QueryString["Size"] != null)
            {
                SpotGrid1.Filter = Request.QueryString["Product"].ToString();
                SpotGrid1.Size = Request.QueryString["Size"].ToString();
                //Instructions1.Content += RESINREQUESTEDINSTRUCTIONS;
                ResinRequested.Visible = true;
                International.Visible = false;
                Domestic.Visible = false;
            }

            if (Request.QueryString["Export"] != null)
            {
                if (Request.QueryString["Export"].ToString() == "true")
                {
                    MarketScreenMode(false);
                    SpotGrid1.International = true;
                    //Instructions1.Content += INTERNATIONALINSTRUCTIONS;
                    ResinRequested.Visible = false;
                    International.Visible = true;
                    Domestic.Visible = false;


                }
                else
                {
                    MarketScreenMode(true);
                    //Instructions1.Content += DOMESTICINSTRUCTIONS;
                    ResinRequested.Visible = false;
                    International.Visible = false;
                    Domestic.Visible = true;
                }
            }
            else
            {
                if (DomesticUser())
                {
                    MarketScreenMode(true);
                    //Instructions1.Content += DOMESTICINSTRUCTIONS;
                    ResinRequested.Visible = false;
                    International.Visible = false;
                    Domestic.Visible = true;
                }
                else
                {
                    MarketScreenMode(false);
                    SpotGrid1.International = true;
                    //Instructions1.Content += INTERNATIONALINSTRUCTIONS;
                    ResinRequested.Visible = false;
                    International.Visible = true;
                    Domestic.Visible = false;
                    //lblIntoText.Text ="Welcome to our International Floor. From here you can see some of our latest offers and details. Simply click Resin Request in the top navigation bar to enter your bids. You can also request additional information by clicking the Inquire option to the left of the offers.";
                }
            }
        }

        private bool DomesticUser()
        {
            bool bDomestic = true;

            if (Session["ID"] != null)
            {
                Hashtable ht = new Hashtable(2);
                ht.Add("@persID", Session["ID"].ToString());

                if (DBLibrary.HasRows(Application["DBConn"].ToString(), "SELECT 1 FROM PERSON WHERE PERS_ID = @persID AND PERS_PRMLY_INTRST = 2", ht)) bDomestic = false;
            }
            return bDomestic;
        }

        protected void LinkButton1_Click(object sender, System.EventArgs e)
        {
            if (lblMarketMode.Text == "International")
                Response.Redirect("/Spot/Spot_Floor.aspx?Export=false");
            else
                Response.Redirect("/Spot/Spot_Floor.aspx?Export=true");
        }

        private void MarketScreenMode(bool marketDomesticMode)
        {
            imgDomestic.Visible = marketDomesticMode;
            imgInternational.Visible = !marketDomesticMode;

            if (marketDomesticMode)
            {
                lnkInternationalMarket.Text = "Change to International Market";
                lblMarketMode.Text = "Domestic";
                lblMarket.Text = "U.S. Market";
            }
            else
            {
                lnkInternationalMarket.Text = "Change to U.S. Domestic Market";
                lblMarketMode.Text = "International";
                lblMarket.Text = "International Market";
            }
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
         

        }
        #endregion
    }
}
