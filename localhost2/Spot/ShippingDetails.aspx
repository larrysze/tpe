<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ Page language="c#" Codebehind="ShippingDetails.aspx.cs" AutoEventWireup="false" Inherits="localhost.Spot.ShippingDetails" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Shipping Details</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript">
		function ValidateCtls()
	{

		if (document.Form1.txtSCname.value=="")
		{
			alert ("Fill Contact Person Name.");
			document.Form1.txtCache.value=0;
			document.Form1.txtSCname.focus();

			return;
		}
	
		if (document.Form1.txtSAddress1.value=="")
		{
			alert ("Fill Address .");
			document.Form1.txtCache.value=0;
			document.Form1.txtSAddress1.focus();
			
			return;
		}

		if (document.Form1.txtSCity.value=="")
		{
			alert ("Fill City Name.");
			document.Form1.txtCache.value=0;			
			document.Form1.txtSCity.focus();
			
			return;
		}
		if (document.Form1.txtSZip.value=="")
		{
			alert ("Fill Zip Code.");
			document.Form1.txtCache.value=0;			
			document.Form1.txtSZip.focus();
			
			return;

		}
		if (document.Form1.txtBCname.value=="")
		{
			alert ("Fill Contact Person Name.");
			document.Form1.txtCache.value=0;			
			document.Form1.txtBCname.focus();
			
			return;

		}
		if (document.Form1.txtBAddress1.value=="")
		{
			alert ("Fill Address .");
			document.Form1.txtCache.value=0;			
			document.Form1.txtBAddress1.focus();
			
			return;

		}

		if (document.Form1.txtBCity.value=="")
		{
			alert ("Fill City Name.");
			document.Form1.txtCache.value=0;			
			document.Form1.txtBCity.focus();
			
			return;

		}
		if (document.Form1.txtBZip.value=="")
		{
			alert ("Fill Zip Code.");
			document.Form1.txtCache.value=0;			
			document.Form1.txtBZip.focus();
			
			return;

		}
			document.Form1.txtCache.value=1;
	}
	
	
		function CheckClick()
	{
	 
		if (document.Form1.ChkShip.checked==true)
		{
			document.Form1.txtBCname.value=document.Form1.txtSCname.value;
			document.Form1.txtBAddress1.value=document.Form1.txtSAddress1.value;
			document.Form1.txtBCity.value=document.Form1.txtSCity.value;
			document.Form1.txtBZip.value=document.Form1.txtSZip.value;
			document.Form1.ddlBState.selectedIndex=document.Form1.ddlSState.selectedIndex;
			document.Form1.ddlBCountry.selectedIndex=document.Form1.ddlSCountry.selectedIndex;
			
			document.Form1.txtBCname.disabled=true;
			document.Form1.txtBAddress1.disabled=true;
			document.Form1.txtBCity.disabled=true;
			document.Form1.txtBZip.disabled=true;
			document.Form1.ddlBState.disabled=true;
			document.Form1.ddlBCountry.disabled=true;
			
			document.Form1.TxtChk.value=1;
		}
		else
		{		
			document.Form1.txtBCname.value="";
			document.Form1.txtBAddress1.value="";
			document.Form1.txtBCity.value="";
			document.Form1.txtBZip.value="";
			document.Form1.ddlBState.selectedIndex=0;
			document.Form1.ddlBCountry.selectedIndex=0;
			document.Form1.TxtChk.value=0;
		}
	}
	function EnableDisable()
	{
//
	}
		</script>
	</HEAD>
	<body onload="EnableDisable()" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TPE:TEMPLATE id="Template3" NAME="Template3" Runat="Server" PageTitle="Shipping Details"></TPE:TEMPLATE>
			<TABLE id="Table1" style="WIDTH: 651px; HEIGHT: 638px" borderColor="black" height="638"
				cellSpacing="0" cellPadding="0" width="651" align="center" border="0">
				<tr>
					<td style="WIDTH: 590px" align="left"><span class="PageHeader">Shipping Details</span>
						<BR>
						<br>
						<br>
						<font color="red"></font>
					</td>
				</tr>
				<TR>
					<TD style="WIDTH: 590px" vAlign="top" align="left">
						<TABLE id="Table12" style="WIDTH: 640px; HEIGHT: 502px" height="502" cellSpacing="1" cellPadding="1"
							width="640" border="0">
							<TR>
								<TD align="left" width="566" colSpan="7"></TD>
							</TR>
							<TR>
								<TD align="left" width="566" colSpan="7"><FONT face="Century gothic" color="blue" size="3"><B>Order 
											Summary</B></FONT></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 347px; HEIGHT: 22px" align="left"><FONT face="Century gothic" size="2"><B>ID#</B></FONT>
								</TD>
								<TD style="WIDTH: 179px; HEIGHT: 22px" align="left"><FONT face="Century gothic" size="2"><B>Product</B></FONT></TD>
								<TD style="WIDTH: 79px; HEIGHT: 22px" align="left"><FONT face="Century gothic" size="2"><B>Size</B></FONT></TD>
								<TD style="WIDTH: 72px; HEIGHT: 22px" align="left"><FONT face="Century gothic" size="2"><B>Melt</B></FONT></TD>
								<TD style="WIDTH: 70px; HEIGHT: 22px" align="left"><FONT face="Century gothic" size="2"><B>Density</B></FONT></TD>
								<TD style="WIDTH: 137px; HEIGHT: 22px" align="left"><FONT face="Century gothic" size="2"><B>Adds</B></FONT></TD>
								<TD style="HEIGHT: 22px" align="left" width="600"><FONT face="Century gothic" size="2"><B>Terms</B></FONT></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 347px" align="left"><FONT face="Times New Roman" size="2"><asp:label id="lblID" runat="server" ForeColor="#C00000" Width="48px"></asp:label></FONT></TD>
								<TD style="WIDTH: 179px; HEIGHT: 13px" align="left"><FONT face="Times New Roman" size="2"><asp:label id="lblProduct" runat="server" ForeColor="#C00000" Width="160px"></asp:label></FONT></TD>
								<TD style="WIDTH: 79px; HEIGHT: 13px" align="left"><FONT face="Times New Roman" size="2"><asp:label id="lblSize" runat="server" ForeColor="#C00000" width="109px" Height="16px"></asp:label></FONT></TD>
								<TD style="WIDTH: 72px; HEIGHT: 13px" align="left"><FONT face="Times New Roman" size="2"><asp:label id="lblMelt" runat="server" ForeColor="#C00000" width="41px"></asp:label></FONT></TD>
								<TD style="WIDTH: 70px; HEIGHT: 13px" align="left"><FONT face="Times New Roman" size="2"><asp:label id="lblDens" runat="server" ForeColor="#C00000" width="48px"></asp:label></FONT></TD>
								<TD style="WIDTH: 137px; HEIGHT: 13px" align="left"><FONT face="Times New Roman" size="2"><asp:label id="lblAdds" runat="server" ForeColor="#C00000" width="40"></asp:label></FONT></TD>
								<TD style="HEIGHT: 13px" align="left" width="600"><FONT face="Times New Roman" size="2"><asp:label id="lblTerms" runat="server" ForeColor="#C00000"></asp:label></FONT></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 347px" align="left" width="343"><FONT face="Century gothic" color="black" size="2"><B>Ship 
											to:</B></FONT></TD>
								<td><asp:imagebutton id="btnShipAddress" runat="server" Width="185px" Height="11" ImageUrl="../images/ChooseFromAddress.gif"></asp:imagebutton></td>
							</TR>
							<TR>
								<TD style="HEIGHT: 155px" noWrap align="left" width="600" colSpan="7" rowSpan="1"><FONT face="Century gothic" size="2"><B>
											<TABLE id="Table3" style="WIDTH: 592px; HEIGHT: 141px" cellSpacing="0" cellPadding="0"
												width="592" border="0">
												<TR>
													<TD style="WIDTH: 448px"><STRONG><FONT size="2">Contact Person Name:</FONT></STRONG></TD>
													<TD style="WIDTH: 445px"><STRONG><FONT size="2">Address:</FONT></STRONG></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 448px"><asp:textbox id="txtSCname" Runat="server" Width="201" Height="28" MaxLength="40"></asp:textbox></TD>
													<TD style="WIDTH: 445px"><asp:textbox id="txtSAddress1" runat="server" Width="200" Height="26" MaxLength="50"></asp:textbox></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 448px"><STRONG><FONT size="2">City:</FONT></STRONG></TD>
													<TD style="WIDTH: 445px"><STRONG><FONT size="2">Zip:</FONT></STRONG></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 448px"><asp:textbox id="txtSCity" runat="server" Width="201px" Height="28px" MaxLength="20"></asp:textbox></TD>
													<TD style="WIDTH: 445px"><asp:textbox id="txtSZip" runat="server" Width="200px" Height="26px" MaxLength="10"></asp:textbox></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 448px; HEIGHT: 16px"><STRONG><FONT size="2">State:</FONT></STRONG></TD>
													<TD style="WIDTH: 445px; HEIGHT: 16px"><STRONG><FONT size="2">Country:</FONT></STRONG></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 448px"><asp:dropdownlist id="ddlSState" runat="server" Width="201px" Height="28px"></asp:dropdownlist></TD>
													<TD style="WIDTH: 445px"><asp:dropdownlist id="ddlSCountry" runat="server" Width="201px" Height="22px"></asp:dropdownlist></TD>
												</TR>
											</TABLE>
										</B></FONT>
								</TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 9px" align="left" width="347"><FONT face="Century gothic" size="2"><B>Bill 
											to:</B></FONT></TD>
								<TD style="HEIGHT: 17px"><asp:imagebutton id="btnBillAddress" runat="server" Width="185" Height="11px" ImageUrl="../images/ChooseFromAddress.gif"></asp:imagebutton></TD>
							</TR>
							<TR>
								<TD align="left" width="600" colSpan="7">
									<P><FONT face="Century gothic" size="2">
											<TABLE id="Table4" style="WIDTH: 591px; HEIGHT: 160px" cellSpacing="1" cellPadding="1"
												width="591" border="0">
												<TR>
													<TD style="WIDTH: 296px; HEIGHT: 17px"><INPUT id="ChkShip" style="WIDTH: 16px; HEIGHT: 19px" onclick="CheckClick()" type="checkbox"
															value="on" name="ChkShip">
														<asp:label id="Label1" runat="server" Width="184px"> Same Shipping Address</asp:label><asp:textbox id="TxtChk" runat="server" Width="1px" Height="1px">0</asp:textbox></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 296px; HEIGHT: 17px">
														<P><FONT face="Century gothic" size="2"><B>Contact Person Name:</B></FONT></P>
													</TD>
													<TD style="HEIGHT: 17px">
														<P><FONT face="Century gothic" size="2"><B><FONT face="Century gothic" size="2"><B>Address:</B></FONT></B></FONT></P>
													</TD>
												</TR>
												<TR>
													<TD style="WIDTH: 296px; HEIGHT: 26px"><asp:textbox id="txtBCname" runat="server" Width="201px" Height="28px" MaxLength="40"></asp:textbox></TD>
													<TD style="HEIGHT: 26px"><asp:textbox id="txtBAddress1" runat="server" Width="200" Height="26" MaxLength="50"></asp:textbox></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 296px; HEIGHT: 11px"><FONT face="Century gothic" size="2"><B>City:</B></FONT></TD>
													<TD style="HEIGHT: 11px"><FONT face="Century gothic" size="2"><B>Zip:</B></FONT></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 296px"><asp:textbox id="txtBCity" runat="server" Width="201px" Height="28px" MaxLength="20"></asp:textbox></TD>
													<TD><asp:textbox id="txtBZip" runat="server" Width="199" Height="24" MaxLength="10"></asp:textbox></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 296px"><FONT face="Century gothic" size="2"><B>State:</B></FONT></TD>
													<TD><FONT face="Century gothic" size="2"><B>Country:</B></FONT></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 296px"><asp:dropdownlist id="ddlBState" runat="server" Width="201px" Height="28px"></asp:dropdownlist></TD>
													<TD><asp:dropdownlist id="ddlBCountry" runat="server" Width="201" Height="22"></asp:dropdownlist></TD>
												</TR>
											</TABLE>
											</B></FONT></P>
								</TD>
							</TR>
						</TABLE>
						<TABLE id="Table2" style="WIDTH: 640px; HEIGHT: 41px" borderColor="black" height="41" width="640"
							bgColor="activeborder" border="0">
							<TR>
								<TD align="center"><INPUT id="BackButton" style="WIDTH: 88px; HEIGHT: 24px" type="submit" value="Back" name="BackButton"
										runat="server"> <INPUT id="btnContinue" style="WIDTH: 88px; HEIGHT: 24px" onclick="ValidateCtls()" type="submit"
										value="Continue" name="Buy" runat="server">
									<asp:TextBox id="txtHShip" runat="server" Width="1px" Height="1px"></asp:TextBox>
									<asp:TextBox id="txtHBill" runat="server" Width="1px" Height="1px"></asp:TextBox>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			<asp:textbox id="txtCache" style="Z-INDEX: 101; LEFT: 688px; POSITION: absolute; TOP: 640px"
				runat="server" Width="8px" Height="1px">0</asp:textbox>
			<TPE:TEMPLATE id="Template2" Runat="Server" Footer="true"></TPE:TEMPLATE></form>
	</body>
</HTML>
