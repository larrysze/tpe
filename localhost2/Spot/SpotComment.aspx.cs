using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost
{
	/// <summary>
	/// Summary description for Email.
	/// </summary>
	public partial class SpotComment : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
            string IdNum = Request.QueryString["id"];
            if (Request.QueryString["archive"] == "1")
            {
                if (Session["bid"].ToString() == "0")
                {
                    GetDataArchiveOffer(IdNum);
                }
                else
                {
                    GetDataArchiveBid(IdNum);
                }
            }
            else
            {

                if (Session["bid"].ToString() == "0")
                {
                    GetDataOffer(IdNum);
                }
                else
                {
                    GetDataBid(IdNum);
                }
            }
		}

        public void GetDataOffer(string IdNum)
        {
            Hashtable htParam = new Hashtable();
            htParam.Add("@ID", IdNum);
            string Body;
            string strSQL;
            strSQL = "SELECT OFFR_COMMENT FROM BBOFFER WHERE OFFR_ID=@ID";
            Body = DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), strSQL, htParam).ToString();
            EmailBody.Text = Body;
        }

        public void GetDataBid(string IdNum)
        {
            Hashtable htParam = new Hashtable();
            htParam.Add("@ID", IdNum);
            string Body;
            string strSQL;
            strSQL = "SELECT BID_COMMENT FROM BBID WHERE BID_ID=@ID";
            Body = DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), strSQL, htParam).ToString();
            EmailBody.Text = Body;
        }

        public void GetDataArchiveOffer(string IdNum)
        {
            Hashtable htParam = new Hashtable();
            htParam.Add("@ID", IdNum);
            string Body;
            string strSQL;
            strSQL = "SELECT OFFR_COMMENT FROM BBOFFERARCH WHERE OFFR_ID=@ID";
            Body = DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), strSQL, htParam).ToString();
            EmailBody.Text = Body;
        }

        public void GetDataArchiveBid(string IdNum)
        {
            Hashtable htParam = new Hashtable();
            htParam.Add("@ID", IdNum);
            string Body;
            string strSQL;
            strSQL = "SELECT BID_COMMENT FROM BBIDARCH WHERE BID_ID=@ID";
            Body = DBLibrary.ExecuteScalarSQLStatement(Application["DBconn"].ToString(), strSQL, htParam).ToString();
            EmailBody.Text = Body;
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


	}
}
