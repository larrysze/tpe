<%@ Page language="c#" Codebehind="Agreement.aspx.cs" AutoEventWireup="True" Inherits="localhost.Spot.Agreement" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>Agreement</title>
</HEAD>
<BODY style="FONT: 10pt ARIAL" bgColor=#ffffff>
<table width="100%">
  <tr>
    <td width=46><IMG src="/images/email/tpelogo.gif" ></TD>
    <td align=left><font face=arial color=black size=4 
       black>The</FONT><font face=arial color=red size=4 
       black>Plastics</FONT><font face=arial color=black 
      size=4 black>Exchange</FONT><font face=arial 
      color=red size=4 black>.</FONT><font face=arial 
      color=black size=4 black>com</FONT> 
</TD></TR></TABLE><BR><BR><BR><b>CONDITIONS OF SALES AGREEMENT</b>
<div class="Content Color2" style="text-align:justify">
<p>All orders for goods placed by Purchaser with The 
Plastics Exchange (TPE), whether written or verbal and whether made 
simultaneously with the submission of this credit application or any time 
thereafter, shall be subject to the terms and conditions set forth below. These 
terms and conditions shall take precedence over any differing terms in any other 
documentation of Purchaser including, but not limited to, any other clauses or 
terms which appear on any correspondence, purchase orders, or order slips of 
Purchaser. </P>
<p>Rejection of nonconforming goods and materials received 
by Purchaser shall be made by sending written notification of such rejection to 
TPE within ten (10) days of Purchaser�s receipt of goods. Such notification 
shall state the basis of the nonconformity of the goods and a detailed 
description of that portion of the shipment being rejected. Purchaser�s failure 
to give notice in writing to TPE within ten (10) days of Purchaser�s receipt of 
goods shall constitute an absolute and unconditional acceptance of such 
materials and a waiver by Purchaser of all claims with respect thereto. </P>
<p>Upon receipt of notification of rejection, TPE shall 
have a reasonable period of time under the circumstances to undertake an 
inspection of any rejected goods at the point of delivery. </P>
<p>Goods determined to be nonconforming by TPE will be 
replaced or credit will be issued to Purchaser, at TPE�s option. At no time will 
TPE�s liability exceed the amount invoiced on the subject purchase order for the 
nonconforming goods. No credit for incidental or consequential damages will be 
issued by TPE. </P>
<p>TPE shall not be liable for delay in performance or 
nonperformance caused by circumstances beyond its control, including, but not 
limited to: acts of God, fire, explosion, flood, natural catastrophe, war, civil 
disturbance, governmental regulation, direction or request, accident, strike, 
labor trouble, shortage of or inability to obtain material, equipment or 
transportation. </P>
<p>Purchaser agrees to indemnify and hold harmless TPE 
against any and all claims and liability arising out of any use of the material 
or of products made from the material purchased from TPE. </P>
<p>DISCLAIMER OF WARRANTIES. PURCHASER AND TPE AGREE THAT 
TPE DOES NOT MAKE OR INTEND AND TPE DOES NOT AUTHORIZE ANY AGENT OR 
REPRESENTATIVE TO MAKE ANY REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, OF 
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE (WHETHER OR NOT THAT PURPOSE 
IS KNOWN TO TPE), OR OTHERWISE WITH RESPECT TO ITS PRODUCTS. ANY SUGGESTIONS BY 
TPE CONCERNING USES OR APPLICATIONS OF ITS PRODUCTS REFLECT TPE�S OPINION ONLY 
AND TPE MAKES NO WARRANTY OF RESULTS TO BE OBTAINED. TPE SHALL NOT BE LIABLE 
FOR, AND PURCHASER ASSUMES RESPONSIBILITY FOR, ALL PERSONAL AND BODILY INJURY 
AND PROPERTY DAMAGES RESULTING FROM THE HANDLING, POSSESSION, USE OR RESALE OF 
TPE�S PRODUCTS. TPE SHALL NOT BE LIABLE FOR CONSEQUENTIAL DAMAGES, PUNITIVE 
DAMAGES, OR LOSS OF PROFITS. IN ANY EVENT, TPE�S LIABILITY SHALL NOT EXCEED THE 
VALUE OF THE ORDER AT ISSUE. </P>
<p>This agreement shall be governed, construed and enforced 
in accordance with the substantive laws of the State of Illinois without regard 
to principles of conflict of laws. </P>
<p>Failure to pay amount due on account will accrue 
interest at the rate of 0.75% per 15 days until paid, or the maximum interest 
rate allowable by law, and Purchaser agrees to pay any and all costs associated 
with the recovery of such amounts due on account including principal, interest, 
and attorney costs. Payments are due per the terms printed on the face page of 
this invoice. </P>
<p>The invalidity of any one or more of the clauses or 
words contained in this Agreement shall not affect the enforceability of the 
remaining portions of this Agreement, all of which are inserted conditionally on 
being valid in law; and in the event any part or portions of this Agreement 
shall be determined to be invalid or illegal or unenforceable in whole or in 
part, neither the validity of the remaining part of such term nor the validity 
of any other term of this Agreement shall in any way be affected thereby. </P>
<p>The waiver by any party of any provision of this 
Agreement shall not operate as, or be construed to be, a waiver of any 
subsequent breach hereof. </P>
<p>The terms of this Conditions of Sales Agreement, 
combined with any invoice or other similar document issued by TPE with respect 
to orders for goods placed by Purchaser, are intended by the parties as the 
complete expression of their agreement, with respect to each order for goods 
placed by Purchaser with TPE. Any and all changes, amendments, or modifications 
of this Agreement shall not be effective unless made in writing and signed by 
the parties hereto. </P>TPE/SGH 1205 <br><br>
</div>
 <hr>
<table width="100%">
  <tr>
    <td vAlign=top align=center colSpan=3><font 
      face=arial color=black size=1>16510 N. 92nd Street. #1010</FONT>&nbsp;&nbsp;<font face=arial color=red size=1 
      >Scottsdale, AZ 85260</FONT>&nbsp;&nbsp;<font face=arial 
      color=black size=1><b>tel 
      <%=ConfigurationSettings.AppSettings["PhoneNumber"].ToString() %></B></FONT>&nbsp;&nbsp;<font face=arial color=red size=1 
      >fax <%=ConfigurationSettings.AppSettings["FaxNumber"].ToString() %></FONT></TD></TR></TABLE>
<table width="100%" align=center>
  <tr>
    <td vAlign=top align=center colSpan=3>
    </TD></TR></TABLE>
  </BODY>
</HTML>
