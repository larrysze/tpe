using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Mail;
using System.Text;
using TPE.Utility;

          /*****************************************************************************
          '*   1. File Name       : Spot/OfferDetails.aspx                             *
          '*   2. Description     : This page shown the  selected offer detail      *
          '*						that is selected from spot_floor.aspx or           *
		  '*                        dashboard.aspx                                     *
          '*   3. Modification Log:                                                    *
          '*     Ver No.       Date          Author             Modification           *
          '*   -----------------------------------------------------------------       *
          '*      1.00                      Hanu software        Comment               *
          '*                                                                           *
          '*****************************************************************************/
namespace localhost.Spot
{
	/// <summary>
	/// Summary description for OfferDetails.
	/// </summary>
	public class OfferDetails : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblProduct;
		protected System.Web.UI.WebControls.Label lblSize;
		protected System.Web.UI.WebControls.Label lblMelt;
		protected System.Web.UI.WebControls.Label lblDens;
		protected System.Web.UI.WebControls.Label lblAdds;
		protected System.Web.UI.WebControls.Label lblTerms;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Label lblEmail;
		protected System.Web.UI.WebControls.Label lblType;
		protected System.Web.UI.WebControls.Label lblComment;
		protected System.Web.UI.WebControls.Label lblPrice;
		protected System.Web.UI.WebControls.Label lblOrigin;
		protected System.Web.UI.WebControls.Label lblExpiration;
		protected System.Web.UI.WebControls.Label lblCompany;
		protected System.Web.UI.WebControls.Label lblPayment;
		protected System.Web.UI.WebControls.Label lblFirm;
		protected System.Web.UI.WebControls.Label lblDescription;
		protected System.Web.UI.WebControls.TextBox txtComments;
		protected System.Web.UI.WebControls.Panel pnMain;
		protected System.Web.UI.WebControls.Panel pnThanks;
		protected System.Web.UI.WebControls.Button btnSendQuery;
		protected System.Web.UI.WebControls.Button btnSubmit;
		protected System.Web.UI.WebControls.Label lblID;
	
		
		private void Page_Load(object sender, System.EventArgs e)
		{
		/// without login nobody can access this page allways open default page
			if (Session["ID"].Equals(null))
			{
				Response.Redirect("/default.aspx");
			}
			ViewState["ID"] = Request.QueryString["ID"];
			
        /// check here that page is post back or not if not than make the connection and execute the command
			if (!IsPostBack)
			{
				SqlConnection conn;
				conn = new SqlConnection(Application["DBConn"].ToString());
				conn.Open();
		
		/// offer read that passed value/parameter from spot_floor.aspx page/dashboard.aspx page
			
				Hashtable param = new Hashtable();
				param.Add("@OFFR_ID", ViewState["ID"].ToString());

				SqlDataReader dtrOffer;
				StringBuilder sbSQL = new StringBuilder();			
				sbSQL.Append("		SELECT *, ");
				sbSQL.Append("		VARTERM=(CASE WHEN OFFR_TERM= 'FOB/Delivered' THEN 'Delivered' WHEN  ");
				sbSQL.Append("		OFFR_TERM= 'FOB/Shipping' THEN 'FOB ' + CAST(ISNULL(OFFR_FROM_LOCL_NAME,(SELECT  ");
				sbSQL.Append("		LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL  ");
				sbSQL.Append("		FROM PLACE WHERE PLAC_ID=OFFR_FROM))) AS VARCHAR) END ), ");
//New Aditions  on 14th Jan
				sbSQL.Append("		VARSIZE=CAST(OFFR_QTY AS VARCHAR)+' '+(CASE OFFR_SIZE WHEN 1 THEN ");
				sbSQL.Append("		'lb' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 42000 ");
				sbSQL.Append("		THEN 'T/L - Boxes' WHEN 45000 THEN 'Bulk Truck' WHEN 44092 THEN 'T/L Bags' ");
				sbSQL.Append("		WHEN 190000 THEN 'Rail Car'	END)+(CASE WHEN (OFFR_QTY>1 AND OFFR_SIZE <> 42000 AND OFFR_SIZE <> 44092 ) THEN 's' ELSE ' ' END) ");

				sbSQL.Append("		FROM BBOFFER ");
				sbSQL.Append("		WHERE  ");
				sbSQL.Append("		OFFR_ID = @OFFR_ID");				

				dtrOffer = DBLibrary.GetDataReaderFromSelect(conn, sbSQL.ToString(), param);

				if  (dtrOffer.Read())
				{
		/// offer details are displayed here in different label that parameter recd. from spot_floor.aspx page/dashboard.aspx page 
					lblID.Text = dtrOffer["OFFR_ID"].ToString();
					lblProduct.Text = dtrOffer["OFFR_PROD"].ToString();
					//lblSize.Text = String.Format("{0:#,###}", dtrOffer["OFFR_SIZE"].ToString()); changes 14th Jan
					lblSize.Text = dtrOffer["VARSIZE"].ToString();
					lblMelt.Text = dtrOffer["OFFR_MELT"].ToString();
					lblDens.Text = dtrOffer["OFFR_DENS"].ToString();

					lblAdds.Text = dtrOffer["OFFR_ADDS"].ToString();
					lblTerms.Text = dtrOffer["VARTERM"].ToString();
					lblPrice.Text = dtrOffer["OFFR_PRCE"].ToString();

					ViewState["Firm"] = dtrOffer["OFFR_FIRM"].ToString();
					ViewState["Origin"] = dtrOffer["OFFR_FROM_LOCL"].ToString();
					ViewState["Expiration"] = dtrOffer["OFFR_EXPR"].ToString();
					ViewState["TERMS"] = dtrOffer["OFFR_PAY"].ToString();
					ViewState["TYPE"] = "Offer";
					
				}

		///connection  close
				dtrOffer.Close();

				conn.Close();
			}
		
		}
 	
		
		
		protected void Click_Submit(object sender, EventArgs e)
		{
		
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSendQuery.Click += new System.EventHandler(this.btnSendQuery_Click);
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		private void btnSubmit_Click(object sender, System.EventArgs e)
		{
	// click on buy online button to go shipping detail page here we also get the offer id in the session[ProductId] variable with seperator ^O  
			//try
			//{
				Session.Contents["ProductId"] = "O^"+ViewState["ID"].ToString() ; //lblID.Text.Trim() ;
				Response.Redirect("ShipDetails.aspx");
			/*}
			catch (Exception ex)
			{
//////				throw (ex);
				Response.Redirect("Spot_Floor.aspx");
			}
			finally
			{
				Response.Redirect("ShippingDetails.aspx");
			}*/

	}
		private void SetFocus(Control ctrl)
		{
			// Define the JavaScript function for the specified control.
			string focusScript = "<script language='javascript'>" +
				"document.getElementById('" + ctrl.ClientID +
				"').focus();</script>";

			// Add the JavaScript code to the page.
			Page.RegisterStartupScript("FocusScript", focusScript);
		}

		private void btnSendQuery_Click(object sender, System.EventArgs e)
		{
	/* click on send question button the user send the query about the offer to the admin
	   if he have any query written in the textbox. and get a soon response message */  		
			if(txtComments.Text.Length<=0)
			{

				Response.Write ("<HTML><HEAD><SCRIPT>");
				Response.Write("alert('If you have query about this offer then please write query in query box.');");
				
      			Response.Write ("</SCRIPT></HEAD></HTML>");
				SetFocus(txtComments);
				return;

			}
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
 // make the connection and get the user detail and offer detail to know that which user send the query mail about which offer .						
			SqlDataReader dtrPerson;

			Hashtable param = new Hashtable();
			param.Add("@PERS_ID", Session["ID"].ToString());

			StringBuilder sbSQL= new StringBuilder();
			sbSQL.Append(" SELECT ");
			sbSQL.Append(" 	P.PERS_FRST_NAME +' '+ P.PERS_LAST_NAME AS NAME, ");
			sbSQL.Append(" 	COMP_NAME = ISNULL((SELECT COMP_NAME FROM COMPANY WHERE COMP_ID= PERS_COMP ),P.COMP_NAME), ");
			sbSQL.Append("	P.PERS_MAIL ");
			sbSQL.Append(" FROM ");
			sbSQL.Append("	PERSON P  ");
			sbSQL.Append(" WHERE ");
			sbSQL.Append("	P.PERS_ID = @PERS_ID");
			
			dtrPerson = DBLibrary.GetDataReaderFromSelect(conn, sbSQL.ToString(), param);
			dtrPerson.Read();

			//*******************************/
			//		 Mailing Code  Below     /
			// mail is sent to the admin  for 
			// an  alert that any offer is 
			// selected by an user
			//*******************************/	
			StringBuilder sbHTML= new StringBuilder();
			sbHTML.Append("<h3>Offer Inquiry Regarding - "+ViewState["TYPE"].ToString()+" #"+ViewState["ID"].ToString()+ "</h3>");
			sbHTML.Append("<table><tr><td align='left'><font size='3'><b>Name: </b></font></td><td>"+dtrPerson["NAME"]+" </td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Company: </b></font></td><td>"+dtrPerson["COMP_NAME"]+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Email: </b></font></td><td><a href='mailto:"+dtrPerson["PERS_MAIL"].ToString()+"'>"+dtrPerson["PERS_MAIL"].ToString()+"</a></td></tr>");
			
			// close connection
			dtrPerson.Close();
			conn.Close();
			
			sbHTML.Append("<tr><td colspan='2'><font align='left' size='4'><b><U>Product Specifications</U></b></font></td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Product: </b></font></td><td>"+lblProduct.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Size: </b></font></td><td>"+lblSize.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Price: </b></font></td><td>"+lblPrice.Text+"</td></tr>");
			//sbHTML.Append("<tr><td align='left'><font size='3'><b>Location: </b></font></td><td>"+ViewState["Origin"].ToString()+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Expiration: </b></font></td><td>"+ViewState["Expiration"].ToString()+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Adds: </b></font></td><td>"+lblAdds.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Melt: </b></fontsize></td><td>"+lblMelt.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Density: </b></font></td><td>"+lblDens.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Terms: </b></font></td><td>"+lblTerms.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Payment: </b></font></td><td>"+ViewState["TERMS"].ToString()+" days</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Firm: </b></font></td><td>"+ViewState["Firm"].ToString()+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Comments: </b></font></td><td>"+txtComments.Text+"</td></tr></table>");
			


			System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
			mail.From=Application["strEmailOwner"].ToString();
			mail.To ="Resin@theplasticsexchange.com";//////shold be change after given new emailid of query
			mail.Cc= Application["strEmailAdmin"].ToString();
			mail.To ="agupta@hanusoftware.com";//////shold be change after given new emailid of query
			mail.Body=sbHTML.ToString();
			mail.Subject="Offer Inquiry!";
			mail.BodyFormat = MailFormat.Html;
			//SmtpMail.SmtpServer="localhost";//mail.hanusoftware.com
			//SmtpMail.SmtpServer="caesar";
			//TPE.Utility.EmailLibrary.Send(mail);

  ///   get the response message if mail is send
			pnMain.Visible = false;
			pnThanks.Visible = true;
		}
	}
}
