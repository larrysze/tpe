using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mail;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Spot
{
	/// <summary>
	/// Summary description for ShipDetails.
	/// </summary>
	public partial class ShipDetails : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Panel Panel1;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			txtEnDisable.Attributes.Add("Style","Visibility:Hidden");
			// Put user code to initialize the page here
//			if (Session["ID"] == null)
//			{
//				Response.Redirect("/default.aspx");
//			}

			if(Session["backShipDetails"] != null)
			{
				loadAllFields();
			}

			if ((Session["Typ"]==null) || (Session["Typ"].ToString()=="")) 
			{
				Session["strLoginURL"] =Request.RawUrl.ToString(); 
				Response.Redirect("/common/FUNCLogin.aspx");
			}
			Hashtable param = new Hashtable();
			param.Add("@PERS_ID",Session["ID"].ToString());
			//Session["UType"] = Convert.ToString(SqlChkType.ExecuteScalar());
			Session["UType"] = DBLibrary.ExecuteScalarSQLStatement(Application["DBConn"].ToString(),"Select Pers_Type from person where PERS_ID=@PERS_ID",param).ToString();
			if  (Session["UType"].ToString().Equals("O")) 
			{
				chkSame.Visible=false;
			}
			else
			{
				chkSame.Visible=true;
			}
			//Panel1.Visible=false;
			ShowProdDetails();
			if (lblSize.Text.EndsWith("Rail Cars") || lblSize.Text.EndsWith("Rail Car"))
			{
				Panel4.Visible=true;
			}
			else
			{
				Panel4.Visible=false;
			}
			if (!IsPostBack)
			{
				FillCountryState();
				if  (Session["UType"].ToString().Equals("O")) 
				{
					FillAddress();
					ShowBillAddress();
					txtEnDisable.Text="0";
					//SetRadio("0");
					EnableDisableOnLoad(true,false);
					//Panel1.Visible=true;
					Panel3.Visible=false;
				}
				else
				{
					FillAddress();	// added rg
					//Panel1.Visible=false;
					Panel3.Visible=true;
					txtEnDisable.Text="1";
					//SetRadio("1");
					EnableDisableOnLoad(false,true);
				}
			}
			if (IsPostBack)
			{
				if (txtEnDisable.Text=="1")
				{
					//SetRadioOnPostBack("1");
					EnableDisableOnLoad(false,true);
				}
				else
				{
					//SetRadioOnPostBack("0");
					EnableDisableOnLoad(true,false);
				}

			}

			if(ddlAddress.Items.Count != 0)
			{
				ddlAddress.Enabled = true;
			}

		}
		private void FillCountryState()
		{
			// Filling Shipping Address Dropdown lists
			FillCombos(ddlSCountry,"CTRY_NAME","CTRY_CODE","SELECT CTRY_NAME,CTRY_CODE From COUNTRY ORDER BY CTRY_NAME ASC");
			ListItem li1 = ddlSCountry.Items.FindByValue("US");
			//ddlSCountry.SelectedIndex = -1;
			li1.Selected = true;
			// Adding States to dropdown list box
			FillCombos(ddlSState  ,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE where STAT_CTRY='US' order by STAT_NAME ASC");
			
			// Filling Billing Address Dropdown lists

			FillCombos(ddlBCountry,"CTRY_NAME","CTRY_CODE","SELECT CTRY_NAME,CTRY_CODE From COUNTRY ORDER BY CTRY_NAME ASC");
			ListItem li2 = ddlBCountry.Items.FindByValue("US");
			//ddlBCountry.SelectedIndex = -1;
			li2.Selected = true;
			// Adding States to dropdown list box
			FillCombos(ddlBState ,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE where STAT_CTRY='US' order by STAT_NAME ASC");
			
		}
		private void FillCombos(DropDownList DDL,string TFld,string ValFld,string Query)
		{
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			SqlDataReader DTR;
			SqlCommand Cmd = new SqlCommand();
			Cmd.CommandType=CommandType.Text;
			Cmd.CommandText=Query;
			
			try
			{
				conn.Open();
				Cmd.Connection=conn;
				DTR=Cmd.ExecuteReader();
				DDL.DataSource=DTR;
				DDL.DataTextField=TFld;
				DDL.DataValueField=ValFld;
				DDL.DataBind();
				DTR.Close();
			}
			finally
			{
				conn.Close();
			}
			if ( DDL.ID=="ddlBState")
			{
				if (DDL.Items.Count<=0 )
				{
					txtBState.Visible=true;
					ddlBState.Visible=false;
				}
				else
				{
					txtBState.Visible=false;
					ddlBState.Visible=true;
				}
			}
			if ( DDL.ID=="ddlSState")
			{
				if (DDL.Items.Count<=0)
				{
					txtSState.Visible=true;
					ddlSState.Visible=false;
					
					txtBState.Visible=true;
					ddlBState.Visible=false;

				}
				else
				{
					txtSState.Visible=false;
					ddlSState.Visible=true;

					txtBState.Visible=false;
					ddlBState.Visible=true;
				}
				ddlBCountry.SelectedIndex = ddlSCountry.SelectedIndex;
			}
		}
		private void ShowBillAddress()
		{
			StringBuilder SbSql = new StringBuilder();
			SbSql.Append("SELECT DISTINCT PLACE.PLAC_ID, PLACE.PLAC_PERS,");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ADDR_ONE is not Null or ltrim(rtrim(PLACE.PLAC_ADDR_ONE))<>'' ");
			SbSql.Append("then  PLACE.PLAC_ADDR_ONE else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CITY is not Null or ltrim(rtrim( PLACE.PLAC_CITY))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CITY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_STAT is not Null or ltrim(rtrim( PLACE.PLAC_STAT))<>'' ");
			SbSql.Append("then + PLACE.PLAC_STAT else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CTRY  is not Null or ltrim(rtrim( PLACE.PLAC_CTRY ))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CTRY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ZIP is not Null or ltrim(rtrim(PLACE.PLAC_ZIP))<>'' ");
			SbSql.Append("then PLACE.PLAC_ZIP else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_RAIL_NUM is not Null or ltrim(rtrim(PLACE.PLAC_RAIL_NUM))<>'' ");
			SbSql.Append("then  PLACE.PLAC_RAIL_NUM else '' end  ");
			SbSql.Append("FROM PLACE INNER JOIN COMPANY ON PLACE.PLAC_COMP = COMPANY.COMP_ID ");
			SbSql.Append("INNER JOIN PERSON ON COMPANY.COMP_ID = PERSON.PERS_COMP ");
			SbSql.Append("WHERE (PERSON.PERS_ID = " + Session["ID"].ToString() + ") AND (PLACE.PLAC_TYPE = 'H')");
			
			
			SqlConnection Conn = new SqlConnection(Application["DBConn"].ToString());
			try
			{
				Conn.Open();
				SqlDataReader sdrAddress;		
				sdrAddress = DBLibrary.GetDataReaderFromSelect(Conn,SbSql.ToString());
				while (sdrAddress.Read())
				{
					ViewState["BContName"]  = sdrAddress.GetValue(1).ToString();
					ViewState["BAddress"] = sdrAddress.GetValue(2).ToString();
					ViewState["BCity"] = sdrAddress.GetValue(3).ToString();
					ViewState["BZip"] = sdrAddress.GetValue(6).ToString();
					ViewState["BState"] =sdrAddress.GetValue(4).ToString();
					ViewState["BCountry"] =sdrAddress.GetValue(5).ToString();
					//ViewState["BRail"] = sdrAddress.GetValue(7).ToString();
				}
				sdrAddress.Close();
			}
			finally
			{
				Conn.Close();
			}
			//Calling procdure to fill corresponding Values
			FillBillAddtoCtrls();

			ddlBCountry.SelectedIndex = ddlBCountry.Items.IndexOf(ddlBCountry.Items.FindByText(txtBCountry.Text.Trim()));
			FillCombos(ddlBState ,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE where STAT_CTRY='" + ddlBCountry.SelectedValue.ToString() + "' order by STAT_NAME ASC");
			
		}
		private void FillBillAddtoCtrls()
		{
			try
			{
				txtBCname.Text = ViewState["BContName"].ToString();
				txtBAddress1.Text = ViewState["BAddress"].ToString();
				txtBCity.Text = ViewState["BCity"].ToString();
				txtBZip.Text = ViewState["BZip"].ToString();
				txtBCountry.Text=ViewState["BCountry"].ToString();

				ListItem li = ddlBCountry.Items.FindByText(ViewState["BCountry"].ToString());
				ddlBCountry.SelectedItem.Selected=false;
				li.Selected=true;
				FillCombos(ddlBState ,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE where STAT_CTRY='" + ddlBCountry.SelectedValue.ToString() + "' order by STAT_NAME ASC");
				if (txtBState.Visible && ddlBState.Items.Count>0)
				{
					txtBState.Visible=false;
					ddlBState.Visible=true;
					ListItem li2 = ddlBState.Items.FindByText(ViewState["BCountry"].ToString());
					ddlBState.SelectedItem.Selected=false;
					li2.Selected=true;
				}
				else
				{
					ddlBState.Visible=false;
					txtBState.Visible=true;
					txtBState.Text=ViewState["BState"].ToString();
				}
			
			
				//txtBRailNum.Text= ViewState["BRail"].ToString();
			}
			catch
			{
				txtBCname.Text ="";
				txtBAddress1.Text = "";
				txtBCity.Text = "";
				txtBZip.Text = "";
				txtBState.Text="";
				//txtBRailNum.Text="";
				txtBState.Visible=false;
				ListItem li2 = ddlBCountry.Items.FindByValue("US");
				ddlBCountry.SelectedItem.Selected=false;
				//ddlBCountry.SelectedIndex = -1;
				li2.Selected = true;
				// Adding States to dropdown list box
				FillCombos(ddlBState ,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE where STAT_CTRY='US' order by STAT_NAME ASC");
				ddlBState.SelectedItem.Selected=false;
				ddlBState.SelectedIndex=0;
			}
		}

		private void EnableDisableOnLoad(Boolean ParamVal1,Boolean ParamVal2)
		{
			ddlAddress.Enabled=ParamVal1;
			txtSCname.Enabled=ParamVal2;
			txtSAddress1.Enabled=ParamVal2;
			txtSCity.Enabled=ParamVal2;
			txtSZip.Enabled=ParamVal2;
			txtSState.Enabled=ParamVal2;
			ddlSCountry.Enabled=ParamVal2;
			ddlSState.Enabled=ParamVal2;
			txtRailNum.Enabled=ParamVal2;
		}
		private void SetRadio(string NewOld)
		{
			string TryCheck=string.Empty;
			if  (Session["UType"].ToString().Equals("O")) 
			{
				if (NewOld=="0")
				{
					// Define the JavaScript function for the specified control.
					TryCheck = "<script language='javascript'>document.Form1.rdo1.checked=true;</script>";
				}
				else
				{
					TryCheck = "<script language='javascript'>document.Form1.rdo1.disabled=true;document.Form1.rdo2.checked=true;</script>";
				}
			}
			else
			{
				if (NewOld=="0")
				{
					// Define the JavaScript function for the specified control.
					TryCheck = "<script language='javascript'>document.Form1.rdo1.checked=true;</script>";
				}

			}
			// Add the JavaScript code to the page.
			Page.RegisterStartupScript("TryCheck", TryCheck);
		}
		private void SetRadioOnPostBack(string NewOld)
		{
			string TryCheck=string.Empty;
			if  (Session["UType"].ToString().Equals("O")) 
			{
				if (NewOld=="0")
				{
					// Define the JavaScript function for the specified control.
					TryCheck = "<script language='javascript'>document.Form1.rdo1.checked=true;</script>";
				}
				else
				{
					TryCheck = "<script language='javascript'>document.Form1.rdo2.checked=true;</script>";
				}
			}
			else
			{
				if (NewOld=="0")
				{
					// Define the JavaScript function for the specified control.
					TryCheck = "<script language='javascript'>document.Form1.rdo1.checked=true;</script>";
				}
			}
			// Add the JavaScript code to the page.
			Page.RegisterStartupScript("TryCheck", TryCheck);
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		private void ShowProdDetails()
		{
				
			try
			{
				string [] ArrProd = Session.Contents["ProductID"].ToString().Split(new Char []{'^'}) ;		
				ViewState["ID"]=ArrProd[1].ToString();
			}
			catch 
			{
				//if session value expires User is redirected to spot floor
				Response.Redirect("Spot_Floor.aspx");
			}
			///<summary>
			// query offers if offer is selected in the spot_floor.aspx and user came here from offerdetails.aspx
			/// </summary>
			SqlCommand cmdOffer;
			SqlDataReader dtrOffer;
			StringBuilder sbSQL = new StringBuilder();			
			sbSQL.Append("	SELECT *, ");
			sbSQL.Append("	VARTERM=(CASE WHEN OFFR_TERM= 'FOB/Delivered' THEN 'FOB Delivered' WHEN  ");
			sbSQL.Append("	OFFR_TERM= 'FOB/Shipping' THEN 'FOB ' END ), ");
			//New Aditions  on 14th Jan
			sbSQL.Append("		VARSIZE=CAST(OFFR_QTY AS VARCHAR)+' '+(CASE OFFR_SIZE WHEN 1 THEN ");
			sbSQL.Append("		'lb' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 42000 ");
			sbSQL.Append("		THEN 'T/L - Boxes' WHEN 45000 THEN 'Bulk Truck' WHEN 44092 THEN 'T/L Bags' ");
			sbSQL.Append("		WHEN 190000 THEN 'Rail Car'	END)+(CASE WHEN (OFFR_QTY>1 AND OFFR_SIZE <> 42000 AND OFFR_SIZE <> 44092 ) THEN 's' ELSE ' ' END) ");

			sbSQL.Append("	FROM BBOFFER ");
			sbSQL.Append("	WHERE  ");
			sbSQL.Append("	OFFR_ID= '"+ViewState["ID"].ToString()+"' ");
			
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			try
			{
				conn.Open();
			
				cmdOffer = new SqlCommand(sbSQL.ToString(),conn );
				dtrOffer = cmdOffer.ExecuteReader();

				try
				{
					if (dtrOffer.Read())
					{
						/// offer details are displayed here in different label that parameter recd. from spot_floor.aspx page 
						lblID.Text = dtrOffer["OFFR_ID"].ToString();
						lblProduct.Text = dtrOffer["OFFR_PROD"].ToString();
						//lblSize.Text = String.Format("{0:#,###}", dtrOffer["OFFR_SIZE"].ToString());
						lblMelt.Text = dtrOffer["OFFR_MELT"].ToString();
						lblDens.Text = dtrOffer["OFFR_DENS"].ToString();
						lblAdds.Text = dtrOffer["OFFR_ADDS"].ToString();
						lblTerms.Text = dtrOffer["OFFR_TERM"].ToString();
						if((Session["RC_changed"] != null) && (Session["RC_changed"].ToString() == "true"))
						{
							//					string sizeFromOffer = dtrOffer["VARSIZE"].ToString();					
							//					sizeFromOffer = sizeFromOffer.Remove(0, sizeFromOffer.IndexOf(" ", 0) + 1);
							//					lblSize.Text = Session["qty"].ToString() + " " + sizeFromOffer;
							lblSize.Text = Session["qty"].ToString() + " " + "Rail Car(s)";
							lblPrice.Text = Session["price"].ToString();

						}
						else
						{
							lblSize.Text = dtrOffer["VARSIZE"].ToString();					
							lblPrice.Text = String.Format("{0:c4}",Convert.ToDecimal(dtrOffer["OFFR_PRCE"]));
						}
					
						ViewState["Firm"] = dtrOffer["OFFR_FIRM"].ToString();
						ViewState["Origin"] = dtrOffer["OFFR_FROM_LOCL"].ToString();
						ViewState["Expiration"] = dtrOffer["OFFR_EXPR"].ToString();
						ViewState["TERMS"] = dtrOffer["OFFR_PAY"].ToString();
						ViewState["TYPE"] = "Offer";
					}							
					else
					{
						/// <summary>
						/// query bids if bid is selected in the spot_floor.aspx and user came here from biddetails.aspx page
						/// </summary>
												
						SqlCommand cmdBid;
						SqlDataReader dtrBid;
						sbSQL.Remove(0,sbSQL.Length);
						sbSQL.Append("		SELECT *, ");
						sbSQL.Append("		VARTERM=(CASE WHEN BID_TERM= 'FOB/Delivered' THEN 'FOB Delivered' WHEN  ");
						sbSQL.Append("		BID_TERM= 'FOB/Shipping' THEN 'FOB '  END ), ");
						//New Aditions  on 14th Jan
						sbSQL.Append("		VARSIZE=CAST(BID_QTY AS VARCHAR)+' '+(CASE BID_SIZE WHEN 1 THEN ");
						sbSQL.Append("		'lb' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 42000 ");
						sbSQL.Append("		THEN 'T/L - Boxes' WHEN 45000 THEN 'Bulk Truck' WHEN 44092 THEN 'T/L Bags' ");
						sbSQL.Append("		WHEN 190000 THEN 'Rail Car'	END)+(CASE WHEN (BID_QTY>1 AND BID_SIZE <> 42000 AND BID_SIZE <> 44092 ) THEN 's' ELSE ' ' END) ");

						sbSQL.Append("		FROM BBID ");
						sbSQL.Append("		WHERE  ");
						sbSQL.Append("			BID_ID= '"+ViewState["ID"].ToString()+"' ");
							
						SqlConnection conn2;
						conn2 = new SqlConnection(Application["DBConn"].ToString());
						try
						{
							conn2.Open();
							cmdBid = new SqlCommand(sbSQL.ToString(),conn2);
							dtrBid = cmdBid.ExecuteReader();
							try
							{
								if (dtrBid.Read())
								{
									/// Bid details are displayed here in different label that parameter recd. from spot_floor.aspx page 
									lblID.Text = dtrBid["BID_ID"].ToString();
									lblProduct.Text = dtrBid["BID_PROD"].ToString();
									//lblSize.Text = String.Format("{0:#,###}", dtrBid["BID_SIZE"].ToString());
									lblMelt.Text = dtrBid["BID_MELT"].ToString();
									lblDens.Text = dtrBid["BID_DENS"].ToString();
									lblAdds.Text = dtrBid["BID_ADDS"].ToString();
									lblTerms.Text = dtrBid["BID_TERM"].ToString();

									if((Session["RC_changed"] != null) && (Session["RC_changed"].ToString() == "true"))
									{
										//						string sizeFromOffer = dtrBid["VARSIZE"].ToString();
										//						sizeFromOffer = sizeFromOffer.Remove(0, sizeFromOffer.IndexOf(" ", 0) + 1);
										//						lblSize.Text = Session["qty"].ToString() + " " + sizeFromOffer;
										lblSize.Text = Session["qty"].ToString() + " " + "Rail Car(s)";
										lblPrice.Text = Session["price"].ToString();

									}
									else
									{
										lblSize.Text = dtrBid["VARSIZE"].ToString();					
										lblPrice.Text = String.Format("{0:c4}",Convert.ToDecimal(dtrBid["BID_PRCE"]));
									}
																		
									ViewState["Firm"] = dtrBid["BID_FIRM"].ToString();
									ViewState["Origin"] = dtrBid["BID_TO_LOCL"].ToString();
									ViewState["Expiration"] = dtrBid["BID_EXPR"].ToString();
									ViewState["TERMS"] = dtrBid["BID_PAY"].ToString();
									ViewState["TYPE"] = "Bid";
							
								}
								else
								{
									// output error
								}
							}
							finally
							{
								dtrBid.Close();
							}
						}
						finally
						{
							conn2.Close();
						}
					}
				}
				finally
				{
					dtrOffer.Close();
				}				
			}		
			finally
			{
				conn.Close();
			}
		}

		private void FillAddress()
		{

			StringBuilder SbSql = new StringBuilder();
			SbSql.Append("SELECT DISTINCT PLACE.PLAC_ID, PLACE.PLAC_PERS +");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ADDR_ONE is not Null or ltrim(rtrim(PLACE.PLAC_ADDR_ONE))<>'' ");
			SbSql.Append("then + ' - '  + PLACE.PLAC_ADDR_ONE else '' end ");
			SbSql.Append("+CASE ");
			SbSql.Append("when PLACE.PLAC_CITY is not Null or ltrim(rtrim( PLACE.PLAC_CITY))<>'' ");
			SbSql.Append("then + ' - '  +  PLACE.PLAC_CITY else '' end ");
			SbSql.Append("+CASE ");
			SbSql.Append("when PLACE.PLAC_STAT is not Null or ltrim(rtrim( PLACE.PLAC_STAT))<>'' ");
			SbSql.Append("then + ' - '  +  PLACE.PLAC_STAT else '' end ");
			SbSql.Append("+CASE ");
			SbSql.Append("when PLACE.PLAC_CTRY  is not Null or ltrim(rtrim( PLACE.PLAC_CTRY ))<>'' ");
			SbSql.Append("then + ' - '  +  PLACE.PLAC_CTRY else '' end ");
			SbSql.Append("+CASE ");
			SbSql.Append("when PLACE.PLAC_ZIP is not Null or ltrim(rtrim(PLACE.PLAC_ZIP))<>'' ");
			SbSql.Append("then + ' - '  + PLACE.PLAC_ZIP else '' end ");
			SbSql.Append("+CASE ");
			SbSql.Append("when PLACE.PLAC_RAIL_NUM is not Null or ltrim(rtrim(PLACE.PLAC_RAIL_NUM))<>'' ");
			SbSql.Append("then + ' - '  + PLACE.PLAC_RAIL_NUM else '' end AS ADDRESS ");
			SbSql.Append("FROM PLACE INNER JOIN COMPANY ON PLACE.PLAC_COMP = COMPANY.COMP_ID ");
			SbSql.Append("INNER JOIN PERSON ON COMPANY.COMP_ID = PERSON.PERS_COMP ");
			SbSql.Append("WHERE (PERSON.PERS_ID = " + Session["ID"].ToString() + ") AND (PLACE.PLAC_TYPE = 'D')");
			
			DBLibrary.BindDropDownList(Application["DBConn"].ToString(),ddlAddress,SbSql.ToString(),"ADDRESS","PLAC_ID");
			if (ddlAddress.Items.Count >= 1)
			{
				// select radiobutton rdo1 (select address below)
				ddlAddress.Items.Insert(0, new ListItem("Select an address:", "-1"));

			}
			else
			{	// select radiobutton rdo2 (new address)
				// question: why aren't rdo1,2 server controls??	
			}
		}

		private void RdoBtn1_CheckedChanged(object sender, System.EventArgs e)
		{
		}

		private void saveAllFields()
		{
			Session["SCname"] = txtSCname.Text;
			Session["SCity"] = txtSCity.Text;
			Session["SAddress1"] = txtSAddress1.Text;
			Session["SZip"] = txtSZip.Text;
			Session["SState"] = txtSState.Text;
			Session["RailNum"] = txtRailNum.Text;
			
			
			Session["SCountry"] = ddlSCountry.SelectedValue;
			Session["SStateList"] = ddlSState.SelectedValue;
			Session["chkbox"] = chkSame.Checked.ToString();
			
			if(!chkSame.Checked)
			{
				Session["BCname"] = txtBCname.Text;
				Session["BCity"] = txtBCity.Text;
				Session["BAddress1"] = txtBAddress1.Text;
				Session["BZip"] = txtBZip.Text;
				Session["BState"] = txtBState.Text;
				Session["BCountry"] = ddlBCountry.SelectedValue;
				Session["BStateList"] = ddlBState.SelectedValue;
			}

		}

		private void loadAllFields()
		{
			txtSCname.Text = Session["SCname"].ToString();
			txtSCity.Text = Session["SCity"].ToString();
			txtSAddress1.Text = Session["SAddress1"].ToString();
			txtSZip.Text = Session["SZip"].ToString();
			txtSState.Text = Session["SState"].ToString();
			txtRailNum.Text = Session["RailNum"].ToString();
			
			
			ddlSCountry.SelectedValue = Session["SCountry"].ToString();
			ddlSState.SelectedValue = Session["SStateList"].ToString();
			chkSame.Checked = Convert.ToBoolean(Session["chkbox"].ToString());
			
			if(!chkSame.Checked)
			{
				txtBCname.Text = Session["BCname"].ToString();
				txtBCity.Text = Session["BCity"].ToString();
				txtBAddress1.Text = Session["BAddress1"].ToString();
				txtBZip.Text = Session["BZip"].ToString();
				txtBState.Text = Session["BState"].ToString();
				ddlBCountry.SelectedValue = Session["BCountry"].ToString();
				ddlBState.SelectedValue = Session["BStateList"].ToString();
			}
			else
			{
				txtBCname.Text = Session["SCname"].ToString();
				txtBCity.Text = Session["SCity"].ToString();
				txtBAddress1.Text = Session["SAddress1"].ToString();
				txtBZip.Text = Session["SZip"].ToString();
				txtBState.Text = Session["SState"].ToString();
				ddlBCountry.SelectedValue = Session["SCountry"].ToString();
				ddlBState.SelectedValue = Session["SStateList"].ToString();
			}
			
			Session.Remove("BackShipDetails");


		}

		protected void btnContinue_Click(object sender, System.EventArgs e)
		{
			
			saveAllFields();
			
			if(Session["confirmation"] != null)
			{
				Session.Remove("confirmation");
			}
			
			EnableValidators(pnlNewAdd,false);
			if (txtEnDisable.Text=="1")
			{
				EnableValidators(pnlNewAdd,true);
			}
			Page.Validate();
			if (!Page.IsValid)
			{
				return;
			}
			int BillCounter=0;
			string strSQL=string.Empty;
			
			if (txtEnDisable.Text=="1")
			{
				
				strSQL = "INSERT INTO PLACE (PLAC_PERS,PLAC_ADDR_ONE,PLAC_ZIP,PLAC_ENBL,PLAC_TYPE,PLAC_RAIL_NUM, PLAC_CITY,PLAC_STAT,PLAC_CTRY)  VALUES('";
				strSQL += txtSCname.Text + "','" + txtSAddress1.Text+ "','"+txtSZip.Text+"',1,'D','" + txtRailNum.Text + "','" + txtSCity.Text + "','";
				if (txtSState.Visible)
				{
					strSQL += txtSState.Text + "','";
				}
				else
				{
					strSQL += ddlSState.SelectedItem.Text.ToString() + "','";
				}

				strSQL += ddlSCountry.SelectedItem.Text.ToString() + "')"; //  txtSCountry.Text
				
				SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());
				try
				{
					conn.Open();
					SqlCommand cmdInsert= new SqlCommand();			
					cmdInsert.Connection=conn;
					cmdInsert.CommandText=strSQL.ToString();				
					cmdInsert.ExecuteNonQuery();

					SqlCommand cmdShip = new SqlCommand("SELECT @@IDENTITY AS NewID",conn);
					Session.Contents["ShipId"]= cmdShip.ExecuteScalar();
				}
				finally
				{
					conn.Close();
				}
			}
			else
			{
				Session.Contents["ShipId"]= ddlAddress.SelectedValue.ToString();
			}

			strSQL="SELECT COUNT(*) FROM PERSON INNER JOIN COMPANY ON PERSON.PERS_COMP = COMPANY.COMP_ID INNER JOIN " ;
			strSQL+=" PLACE ON COMPANY.COMP_ID = PLACE.PLAC_COMP WHERE (PERSON.PERS_ID =" +  Session["ID"].ToString() + ") AND (PLACE.PLAC_TYPE = 'H') ";
			SqlConnection conn2 = new SqlConnection(Application["DBconn"].ToString());
			try
			{
				conn2.Open();
				SqlCommand Cmd2 = new SqlCommand(strSQL,conn2);
				BillCounter = Convert.ToInt32(Cmd2.ExecuteScalar());
			}
			finally
			{
				conn2.Close();
			}
			if (BillCounter==0)
			{
				strSQL = "INSERT INTO PLACE (PLAC_PERS,PLAC_ADDR_ONE,PLAC_ZIP,PLAC_ENBL,PLAC_TYPE,PLAC_CITY,PLAC_STAT,PLAC_CTRY)  VALUES('";
				strSQL += txtBCname.Text + "','" + txtBAddress1.Text+ "','"+txtBZip.Text+"',1,'H','" +  txtBCity.Text + "','";
				if (txtBState.Visible)
				{
					strSQL += txtBState.Text + "','" ;
				}
				else
				{
					strSQL += ddlBState.SelectedItem.Text + "','";
				}
					
				strSQL += ddlBCountry.SelectedItem.Text + "')"; 

				SqlConnection conn3 = new SqlConnection(Application["DBconn"].ToString());
				try
				{
					conn3.Open();			
					SqlCommand Cmd3 = new SqlCommand(strSQL,conn3);
					Cmd3.CommandText=strSQL.ToString();				
					Cmd3.ExecuteNonQuery();

					SqlCommand cmdBill = new SqlCommand("SELECT @@IDENTITY AS NewID",conn3);
					Session.Contents["BillId"]= Convert.ToString(cmdBill.ExecuteScalar());
				}
				finally
				{
					conn3.Close();
				}
			}
			else
			{
				strSQL="SELECT max(PLACE.PLAC_ID) FROM PERSON INNER JOIN COMPANY ON PERSON.PERS_COMP = COMPANY.COMP_ID INNER JOIN " ;
				strSQL+=" PLACE ON COMPANY.COMP_ID = PLACE.PLAC_COMP WHERE (PERSON.PERS_ID =" +  Session["ID"].ToString() + ") AND (PLACE.PLAC_TYPE = 'H') ";
				SqlConnection conn4 = new SqlConnection(Application["DBconn"].ToString());
				try
				{
					conn4.Open();			
				
					SqlCommand GetCmd = new SqlCommand(strSQL,conn4);
					Session.Contents["BillId"]= Convert.ToString(GetCmd.ExecuteScalar());
				}
				finally
				{
					conn4.Close();
				}

				strSQL = "UPDATE PLACE set PLAC_PERS='" + txtBCname.Text + "',PLAC_ADDR_ONE='" + txtBAddress1.Text + "',PLAC_ZIP='" + txtBZip.Text + "',PLAC_CITY='";
				strSQL += txtBCity.Text + "',PLAC_STAT='" + txtBState.Text+ "',PLAC_CTRY='"+txtBCountry.Text+"' where PLAC_ID=" + Session.Contents["BillId"].ToString() ;
				SqlConnection conn5 = new SqlConnection(Application["DBconn"].ToString());
				try
				{
					conn5.Open();			
					SqlCommand cmdUpdate= new System.Data.SqlClient.SqlCommand(strSQL,conn5);
					cmdUpdate.ExecuteNonQuery();
				}
				finally
				{
					conn5.Close();
				}
			}
		
			if (txtEnDisable.Text=="1")
			{
				DoMail();
			}
			Response.Redirect("OrderTotal.aspx");
		}

		private void DoMail()
		{
			StringBuilder sbHTML =  new StringBuilder() ;
			sbHTML.Append("<Table>");
			sbHTML.Append("<TR><TD><h3>Resin Billing/Shipping Information for the following Product </h3></TD></TR>");
			
			sbHTML.Append("<tr><td colspan='2'><font align='left' size='4'><b><U>Customer Information</U></b></font></td></tr>");
			if(Session["UserName"] != null)
			{
				sbHTML.Append("<tr><td align='left'><font size='3'><b>ID: </b></font></td><td>"+Session["UserName"].ToString()+"</td></tr>");
			}
			else
			{
				sbHTML.Append("<tr><td align='left'><font size='3'><b>ID: </b></font></td><td>"+ Session["Email"].ToString() +"</td></tr>");
			}
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Name: </b></font></td><td>"+Session["Name"].ToString()+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Email: </b></font></td><td>"+Session["Email"].ToString()+"</td></tr>");
			
			sbHTML.Append("<tr></tr>");
			sbHTML.Append("<tr><td colspan='2'><font align='left' size='4'><b><U>Product Specifications</U></b></font></td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>ID#: </b></font></td><td>"+lblID.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Product: </b></font></td><td>"+lblProduct.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Size: </b></font></td><td>"+lblSize.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Adds: </b></font></td><td>"+lblAdds.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Melt: </b></fontsize></td><td>"+lblMelt.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Density: </b></font></td><td>"+lblDens.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Terms: </b></font></td><td>"+lblTerms.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Price: </b></font></td><td>"+lblPrice.Text+"</td></tr>");
            			
			sbHTML.Append("<tr></tr>");
			sbHTML.Append("<tr><td><h3>Shipping Address </h3></td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Contact: </b></font></td><td>"+txtSCname.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Address: </b></font></td><td>"+txtSAddress1.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>City: </b></font></td><td>"+txtSCity.Text+"</td></tr>");
			if (txtSState.Visible)
			{
				sbHTML.Append("<tr><td align='left'><font size='3'><b>State: </b></fontsize></td><td>"+txtSState.Text+"</td></tr>");
			}
			else
			{
				sbHTML.Append("<tr><td align='left'><font size='3'><b>State: </b></fontsize></td><td>"+ddlSState.SelectedItem.Text+"</td></tr>");
			}
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Country: </b></font></td><td>"+ddlSCountry.SelectedItem.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Zip: </b></font></td><td>"+txtSZip.Text+"</td></tr>");
			
			if (lblSize.Text.EndsWith("Rail Cars") || lblSize.Text.EndsWith("Rail Car"))
			{
				sbHTML.Append("<tr><td align='left'><font size='3'><b>Delivering Rail Carrier/Track No: </b></font></td><td>"+txtRailNum.Text+"</td></tr>");

			}
			sbHTML.Append("<tr></tr>");
			sbHTML.Append("<tr><td><h3>Billing Address </h3></td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Contact: </b></font></td><td>"+txtBCname.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Address: </b></font></td><td>"+txtBAddress1.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>City: </b></font></td><td>"+txtBCity.Text+"</td></tr>");
			if (txtSState.Visible)
			{
				sbHTML.Append("<tr><td align='left'><font size='3'><b>State: </b></fontsize></td><td>"+txtBState.Text+"</td></tr>");
			}
			else
			{
				if(ddlBState.SelectedIndex != -1)
				{
					sbHTML.Append("<tr><td align='left'><font size='3'><b>State: </b></fontsize></td><td>"+ddlBState.SelectedItem.Text+"</td></tr>");
				}
				else
				{
					sbHTML.Append("<tr><td align='left'><font size='3'><b>State: </b></fontsize></td><td>"+txtBState.Text+"</td></tr>");
				}
			}
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Country: </b></font></td><td>"+ddlBCountry.SelectedItem.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Zip: </b></font></td><td>"+txtBZip.Text+"</td></tr>");
			
			MailMessage EmailShip;
			EmailShip = new MailMessage();
			EmailShip.From = "info@theplasticsexchange.com";
			EmailShip.To = Application["strEmailAdmin"].ToString();
			EmailShip.Cc = Application["strEmailOwner"].ToString();
			EmailShip.Subject = "Customer entered the Shipping Details";
			EmailShip.Body = sbHTML.ToString();
			EmailShip.BodyFormat = MailFormat.Html;
			//SmtpMail.SmtpServer="caesar";
			TPE.Utility.EmailLibrary.Send(Context,EmailShip);
		}
		private void txtEnDisable_TextChanged(object sender, System.EventArgs e)
		{
			
			//if(txtEnDisable.Text=="0")
			//{
			//	btnContinue.CausesValidation=false;
			//	}

		}

		protected void btnBack_Click(object sender, System.EventArgs e)
		{
			// back button click and go to offerdetails.aspx/biddetails.aspx as screen refer
			Session.Contents.Remove("ShipId");
			Session.Contents.Remove("BillId");

			string TmpProd = Convert.ToString(Session.Contents["ProductID"]);
			string[] ArrProd = TmpProd.Split(new Char []{'^'}) ;
			//ArrProd = Session.Contents["ProductID"].Split(new Char []{'^'}) ;		
			ViewState["ID"]=ArrProd[1].ToString();
			Response.Redirect("Inquire.aspx?ID="+ArrProd[1]);
			
		}

		private void EnableValidators(Control container,Boolean EnDis)
		{						
			foreach (Control c in container.Controls)
			{				
				if(c is IValidator)
				{
					((BaseValidator)c).Enabled = EnDis;	//Assumption every control that implements IValidator is derived from BaseValidator class
				}
			}
		}

		protected void ddlSCountry_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillCombos(ddlSState ,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE where STAT_CTRY='" + ddlSCountry.SelectedValue.ToString() + "' order by STAT_NAME ASC");
//			FillCombos(ddlBState ,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE where STAT_CTRY='" + ddlBCountry.SelectedValue.ToString() + "' order by STAT_NAME ASC");
		}

		protected void ddlBCountry_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillCombos(ddlBState ,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE where STAT_CTRY='" + ddlBCountry.SelectedValue.ToString() + "' order by STAT_NAME ASC");
		}

		private void chbSame_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkSame.Checked)
			{
				
				txtBCname.Text = txtSCname.Text;
				txtBAddress1.Text = txtSAddress1.Text;
				txtBCity.Text = txtSCity.Text;
				txtBZip.Text = txtSZip.Text;
				ddlBCountry.SelectedIndex=ddlSCountry.SelectedIndex;
				FillCombos(ddlBState ,"STAT_NAME","STAT_CODE","SELECT STAT_NAME,STAT_CODE From STATE where STAT_CTRY='" + ddlBCountry.SelectedValue.ToString() + "' order by STAT_NAME ASC");
				if (ddlSState.Visible)
				{
					txtBState.Visible=false;
					ddlBState.Visible=true;
					ddlBState.SelectedIndex=ddlSState.SelectedIndex;
				}
				else
				{
					ddlBState.Visible=false;
					txtBState.Visible=true;
					txtBState.Text=txtSState.Text;
				}
				
				//txtBRailNum.Text= txtRailNum.Text;			
			}
			else
			{
				FillBillAddtoCtrls();
			}
			
		}

		protected void ddlAddress_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strSQL = "SELECT PLAC_PERS, PLAC_ADDR_ONE + ' ' + PLAC_ADDR_TWO as PLAC_ADDR, LOCL_CITY, PLAC_ZIP, LOCL_STAT, LOCL_CTRY FROM PLACE, LOCALITY where plac_locl=locl_id and plac_id=@param_plac_id";
			Hashtable param = new Hashtable();
			param.Add("@param_plac_id", ddlAddress.SelectedValue);
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			SqlDataReader address = DBLibrary.GetDataReaderFromSelect(conn, strSQL, param);
			try
			{
				if(address.Read())
				{
					txtSCname.Text = address["PLAC_PERS"].ToString();
					txtSAddress1.Text = address["PLAC_ADDR"].ToString();
					txtSCity.Text = address["LOCL_CITY"].ToString();
					ddlSState.SelectedValue = address["LOCL_STAT"].ToString();
					ddlSCountry.SelectedValue = address["LOCL_CTRY"].ToString();
					txtSZip.Text = address["PLAC_ZIP"].ToString();
				}
			}
			finally
			{
				address.Close();
				conn.Close();
			}
		}

	}
}
