<%@ Page language="c#" Codebehind="SpotComment.aspx.cs" AutoEventWireup="True" Inherits="localhost.SpotComment" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Comments</title>
        <style type="text/css"> 
            .LinkNormal {
		        font-size:12px;
		        color: #000000
        }

        .LinkNormal a { 
		        font-weight: bold; 
		        color: #000000 
        }

        .LinkNormal a:link {
	        text-decoration:none;
	        color: #000000
        }

        .LinkNormal a:visited {
	        text-decoration:none;
	        color: #000000
        }

        .LinkNormal a:hover {
	        text-decoration:underline;
	        color: #000000
        }
        </style>
    </HEAD>        
	<BODY style="FONT: 10pt ARIAL" bottomMargin="0" bgColor="#dbdcd7" leftMargin="0" topMargin="0"
		rightMargin="0">
		<FORM id="Form1" method="post" runat="server">
			<asp:panel Visible="False" id="pnlEmail" runat="server" Width="100%" Height="80px" BackColor="#E0E0E0">
				<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%" border="0">
					<TR>
						<TD style="WIDTH: 33px">&nbsp;</TD>
						<TD style="WIDTH: 47px">
							<asp:Label id="From" runat="server" Font-Size="10pt">From:</asp:Label></TD>
						<TD>
							<asp:TextBox id="txtFrom" runat="server" Width="536px" Font-Size="10pt" Enabled="False"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 33px">&nbsp;</TD>
						<TD style="WIDTH: 47px">
							<asp:Label id="Label2" runat="server" Font-Size="10pt">To:</asp:Label></TD>
						<TD>
							<asp:TextBox id="txtTo" runat="server" Width="536px" Font-Size="10pt" Enabled="False"></asp:TextBox></TD>
					</TR>`
					<TR>
						<TD style="WIDTH: 33px">&nbsp;</TD>
						<TD style="WIDTH: 47px">
							<asp:Label id="Label3" runat="server" Font-Size="10pt">Cc:</asp:Label></TD>
						<TD>
							<asp:TextBox id="txtCC" runat="server" Width="536px" Font-Size="10pt" Enabled="False"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 33px">&nbsp;</TD>
						<TD style="WIDTH: 47px">
							<asp:Label id="Label4" runat="server" Font-Size="10pt">Subject:</asp:Label></TD>
						<TD>
							<asp:TextBox id="TxtSubject" runat="server" Width="536px" Font-Size="10pt" Enabled="False"></asp:TextBox></TD>
					</TR>
				</TABLE>
			</asp:panel>
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD>
						<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD width="5%"></TD>
								<TD><BR>
									<asp:literal id="EmailBody" runat="server"></asp:literal><BR>
								</TD>
								<TD width="5%"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
            <a href="JavaScript:window.close()" class="Linknormal">close window</a>

		</FORM>
	</BODY>
</HTML>
