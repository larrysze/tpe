<%@ import Namespace="System.Web.Mail" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Instructions" Src="/include/ScreenInstructions.ascx" %>
<%@ Page Language="C#" %>
<script runat="server">

    /*****************************************************************************
    '*   1. File Name       : spot/Order_Entry.asp	                         *
    '*   2. Description     : spot offers/ bids	entry screen fro buyer and seller *
    '*						                                 *
    '*   3. Modification Log:                                                    *
    '*     Ver No.       Date          Author             Modification           *
    '*   -----------------------------------------------------------------       *
    '*      1.00      11-26-2003       Zach                  First Baseline      *
    '*                                                                           *
    '*****************************************************************************/
    string location;
    SqlConnection conn;
    string UserType;
    bool IsDemo;
    public void Page_Load(object sender, EventArgs e){
		Response.Redirect("resin_entry.aspx");
        //only for buyer and seller
        if (((string)Session["Typ"] != "S") && ((string)Session["Typ"] != "P")&& ((string)Session["Typ"] != "O")&& ((string)Session["Typ"] != "Demo")){
            Response.Redirect("../default.aspx");
        }
        if (Session["Typ"].ToString().Equals("D")){
            // must be a demo user
            IsDemo = true;
            UserType = "S";
            //disable delivery location drop-down list fro demo user
            pnDelivery_Point.Visible =false;
        }else{
            // user is a normal exchange user
            IsDemo = false;
            UserType = (string)Session["Typ"];
        }
        // opening the connections
        conn = new SqlConnection(Application["DBconn"].ToString());
        conn.Open();


        if (!IsPostBack){
           if (Request.QueryString["Export"] !=null){
                ViewState["Export"] =true;
            }else{
                ViewState["Export"] =false;

            }
            //opening contract list, disabled!
             /*
             SqlCommand cmdContract;
            SqlDataReader dtrContract;

            cmdContract = new SqlCommand("SELECT CONT_ID,CONT_LABL FROM Contract ORDER BY CONT_ORDR",conn);
            dtrContract = cmdContract.ExecuteReader();

            //binding company dd list
            ddlProduct.DataSource = dtrContract;
            ddlProduct.DataTextField= "CONT_LABL";
            //ddlProduct.DataValueField= "CONT_ID";
            ddlProduct.DataBind();
            dtrContract.Close();

            */
            //building the contract list
            ddlProduct.Items.Add("HMWPE - Film Grade");
            ddlProduct.Items.Add("HDPE - Inj");
            ddlProduct.Items.Add("HDPE - Blow Mold");
            ddlProduct.Items.Add("LDPE - Film");
            ddlProduct.Items.Add("LDPE - Inj");
            ddlProduct.Items.Add("LLDPE - Film");
            ddlProduct.Items.Add("LLDPE - Inj");
            ddlProduct.Items.Add("PP Homopolymer - Inj");
            ddlProduct.Items.Add("PP Copolymer - Inj");
            ddlProduct.Items.Add("GPPS");
            ddlProduct.Items.Add("HIPS");
            //ddlProduct.Items.Add("PET");
            //ddlProduct.Items.Add("PVC");

            // populate other info

            if ((bool)ViewState["Export"]){
                //make container available for export transaction
                ddlQty.Items.Add(new ListItem ("20' Container","38500"));
                ddlQty.Items.Add(new ListItem ("40' Container","44500"));

                //extra payment terms for export transaction
                ddlTerms.Items.Add(new ListItem ("CFR","CFR"));
                ddlTerms.Items.Add(new ListItem ("CIF","CIF"));
                ddlTerms.Items.Add(new ListItem ("FAS","FAS"));

                //ddlPayment.Items.Add(new ListItem ("LC Sight","7"));
                //ddlPayment.Items.Add(new ListItem ("CIA","0"));
                //ddlPayment.Items.Add(new ListItem ("LC 30","30"));
                //ddlPayment.Items.Add(new ListItem ("LC 60","60"));
                //ddlPayment.Items.Add(new ListItem ("LC 90","90"));


            }else{

                //not an export transaction! different order size, terms
                ddlQty.Items.Add(new ListItem ("Rail Car","190000"));
                ddlQty.Items.Add(new ListItem ("Metric Ton","2205"));
                ddlQty.Items.Add(new ListItem ("Truckload Boxes","42000"));
                ddlQty.Items.Add(new ListItem ("Truckload Bags","44092"));
                ddlQty.Items.Add(new ListItem ("Bulk Truck","45000"));
                ddlQty.Items.Add(new ListItem ("Pounds (lbs)","1"));

                ddlTerms.Items.Add(new ListItem ("FOB Delivered","FOB/Delivered"));
                ddlTerms.Items.Add(new ListItem ("FOB Shipping","FOB/Shipping"));

                //ddlPayment.Items.Add(new ListItem ("30","30"));
                //ddlPayment.Items.Add(new ListItem ("Cash","0"));
                //ddlPayment.Items.Add(new ListItem ("45","45"));
                //ddlPayment.Items.Add(new ListItem ("60","60"));
                //ddlPayment.Items.Add(new ListItem ("90","90"));
            }

            //expire terms drop-down, for both export and non-export
            ddlexpires.Items.Add(new ListItem ("One week","7"));
            ddlexpires.Items.Add(new ListItem ("End of Day","0"));
            ddlexpires.Items.Add(new ListItem ("Two weeks","14"));
            ddlexpires.Items.Add(new ListItem ("One month","30"));
            //ddlexpires.Items.Add(new ListItem ("Two months","60"));
            //ddlexpires.Items.Add(new ListItem ("Three months","90"));
            // binds the additional fields
            Bind_Info();

        }


    }
    // <summary>
    //  This function is a wrapper for calling the Bind Function
    //  All the controls that auto post back call this function
    // </summary>
    public void PostBack(object sender, EventArgs e){

        Bind_Info();
    }
    // <summary>
    //  Function populates the melt density and adds fields depending on the currently selected contract
    // </summary>
    /* not implemented at the moment
    public void Change_Contract(object sender, EventArgs e){
        
        SqlDataReader dtrContract;
        Hashtable param = new Hashtable();
		param.Add("@CONT_ID", ddlProduct.SelectedItem.Value);
		        
        dtrContract = DBLibrary.GetDataReaderFromSelect(conn, "select * from CONTRACT WHERE CONT_ID=@CONT_ID", param);

        dtrContract.Read();
        txtMelt.Text = dtrContract["Cont_Melt_TGT"].ToString();
        txtDensity.Text = dtrContract["Cont_Dens_TGT"].ToString();
        txtAdds.Text = dtrContract["Cont_IZOD_TGT"].ToString();
        dtrContract.Close();
    }
    */
    // <summary>
    // Primary function for binding content to the controls
    // </summary>
    public void Bind_Info(){


        ddlDelivery_Point.Items.Clear();
        // change the lables depending on the type of user
        switch(UserType){
            //purchaser
            case "P":
                WebBox1.Heading = "Buy Order Entry";
                lblPrice.Text = "Buying Price";
                lblExpires.Text = "Bid Expiration";
                lblPoint.Text="Delivery Point";
                break;
            //supplier
            case "S":
                WebBox1.Heading = "Sell Order Entry";
                lblPrice.Text = "Selling Price";
                lblExpires.Text = "Offer Expiration";
                lblPoint.Text="Shipping Point";
                break;
            //purchaser
            case "D":
                WebBox1.Heading = "Sell Order Entry";
                lblPrice.Text = "Selling Price";
                lblExpires.Text = "Offer Expiration";
                lblPoint.Text="Shipping Point";
                break;
        }


        // adding all the possible locations within the company
        if (!IsDemo){
            // only for exchange users. Demo users don't have this
            SqlCommand cmdLocations;
            SqlDataReader dtrLocations;

            cmdLocations = new SqlCommand("select PLAC_ID ,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) AS Location from place where PLAC_TYPE<>'H' AND plac_comp='"+Session["Comp"]+"'",conn);
            dtrLocations = cmdLocations.ExecuteReader();
            while(dtrLocations.Read()){
                ddlDelivery_Point.Items.Add ( new ListItem ((string) dtrLocations["LOCATION"],dtrLocations["PLAC_ID"].ToString() ) );
            }
            dtrLocations.Close();
            //add the warehouses!!
           cmdLocations = new SqlCommand("select  PLAC_ID, LABEL = (select LOCL_CITY +', '+ LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) + ' ('+PLAC_LABL+')'  from place where plac_type='W'  ORDER BY LABEL",conn);
           dtrLocations = cmdLocations.ExecuteReader();
           while(dtrLocations.Read()){
                   ddlDelivery_Point.Items.Add ( new ListItem (dtrLocations["LABEL"].ToString(),dtrLocations["PLAC_ID"].ToString() ) );
           }
               dtrLocations.Close();
        }

    }
    // <summary>
    //  Function enters the order into the system
    // </summary>
    public void Click_Cancel(object sender, EventArgs e){
        pnMain.Visible= true;
        pnConfirm.Visible = false;
    }
    // <summary>
    //  Creates and displays the confirmation screen
    // </summary>
    public void Click_Submit(object sender, EventArgs e){
        pnMain.Visible= false;
        string strText;

        // builds the confirmation string
        strText = " You are going to place an order to ";


        // selects the user type

        if (UserType.Equals("P")){
           strText += "buy ";
        }else{
           strText += "sell ";
        }


        strText += txtQty.Text + " " + ddlQty.SelectedItem.ToString() + " of ";

        if (radio_Other.Checked){
                strText += txtProduct.Text;
        }else{
                strText +=ddlProduct.SelectedItem.ToString();
        }
        strText +=" for $ 0." + txtPrice.Text +" per pound ";
        strText +=" and markup of $ 0." + txtMarkup.Text +" per pound. ";
        if (!txtMelt.Text.Equals("")){
            strText +="Melt: " + txtMelt.Text +". ";
        }
        if (!txtDensity.Text.Equals("")){
            strText +="Density: " + txtDensity.Text +". ";
        }
        if (!txtAdds.Text.Equals("")){
            strText +="Density: " +txtAdds.Text +". ";
        }
        strText += "Shipped from ";

        if (txtDelivery_Point.Text.Equals("")){
            strText += ddlDelivery_Point.SelectedItem + ". ";
        }else{
            strText += txtDelivery_Point.Text +". ";
        }
        strText +=" Expiration: In "+ddlexpires.SelectedItem.ToString()+". Terms: "+ddlTerms.SelectedItem.ToString()+".";
        // Payment: "+ddlPayment.SelectedItem+" days.
        
        strText +="<BR><BR>Price on the Spot Floor will be increased by $ 0."+txtMarkup.Text+" per pound due to TPE markup.";
        
        lblConfirm.Text = strText;
        ViewState["ConfirmationText"] = strText;
        
        pnConfirm.Visible = true;
    }
    // <summary>
    //  Function enters the order into the system
    // </summary>


    // <summary>
    //  Function enters the order into the system
    // </summary>
    public void Save(object sender, EventArgs e){
            SqlCommand cmdAddNew;  // stored proc adding record
            cmdAddNew= new SqlCommand("spAddtoBB", conn);
            cmdAddNew.CommandType = CommandType.StoredProcedure;

            if (IsDemo){
                // use the alternate company
                cmdAddNew.Parameters.Add("@Comp",Session["Comp"].ToString());
                cmdAddNew.Parameters.Add("@Comp_Id",null);
                cmdAddNew.Parameters.Add("@Pers_id",null);

            }else{
                cmdAddNew.Parameters.Add("@Comp",null);
                cmdAddNew.Parameters.Add("@Comp_Id",Session["Comp_Id"].ToString());
                cmdAddNew.Parameters.Add("@Pers_id",Session["Id"].ToString());
            }

            if (UserType.Equals("S")){
                cmdAddNew.Parameters.Add("@Type","S");
            }else{
                cmdAddNew.Parameters.Add("@Type","P");
            }
            if (txtDelivery_Point.Text.Equals("")){
                // use the drop down location
                SqlCommand cmdLocality;
                // select the locality
                cmdLocality = new SqlCommand("Select PLAC_LOCL FROM Place WHERE PLAC_ID='"+ddlDelivery_Point.SelectedItem.Value+"'",conn);
                // add locality id
                cmdAddNew.Parameters.Add("@Locl0",cmdLocality.ExecuteScalar().ToString());
                // add place id
                cmdAddNew.Parameters.Add("@Locl1",ddlDelivery_Point.SelectedItem.Value);
                cmdAddNew.Parameters.Add("@Local",null);

            }else{
               // use the text box to manually enter the location
               cmdAddNew.Parameters.Add("@Locl0",null);
               cmdAddNew.Parameters.Add("@Local",txtDelivery_Point.Text.ToString());
            }

            // other product overwrite the dropdown list
            if (radio_Other.Checked){
                cmdAddNew.Parameters.Add("@Prod",txtProduct.Text);
            }else{
                cmdAddNew.Parameters.Add("@Prod",ddlProduct.SelectedItem.ToString());
            }

            cmdAddNew.Parameters.Add("@Detail",txtDetail.Text);
            cmdAddNew.Parameters.Add("@Qty",txtQty.Text);
            cmdAddNew.Parameters.Add("@Size",Int32.Parse(ddlQty.SelectedItem.Value));
            cmdAddNew.Parameters.Add("@Terms",ddlTerms.SelectedItem.Value);
            // TODO
            if (txtDelivery_Point.Text.Equals("") && (!IsDemo)){
                cmdAddNew.Parameters.Add("@Firm","1"); // firm
            }else{
                cmdAddNew.Parameters.Add("@Firm","0"); // not firm
            }
            cmdAddNew.Parameters.Add("@Payment","0");
            cmdAddNew.Parameters.Add("@Expr",ddlexpires.SelectedItem.Value);
            cmdAddNew.Parameters.Add("@Price","0." + txtPrice.Text);
            cmdAddNew.Parameters.Add("@Markup",txtMarkup.Text);

            if (!txtMelt.Text.Equals("")){
                cmdAddNew.Parameters.Add("@Melt",txtMelt.Text);
            }
            if (!txtDensity.Text.Equals("")){
                cmdAddNew.Parameters.Add("@Dens",txtDensity.Text);
            }
            if (!txtAdds.Text.Equals("")){
                cmdAddNew.Parameters.Add("@Adds",txtAdds.Text);
            }

           cmdAddNew.ExecuteNonQuery();

            SqlCommand cmdIdentity;
            cmdIdentity = new SqlCommand("SELECT @@IDENTITY AS NewID",conn);
            string strOrderNum = cmdIdentity.ExecuteScalar().ToString();

            lblMessage.Text ="<font color=red>Order "+strOrderNum+" entered!</font>";

            // send email to alert Zach and Mike after reopen the task
           MailMessage mail;
           mail = new MailMessage();

           // get users email address
           SqlCommand cmdGetEmail;
           cmdGetEmail = new SqlCommand("Select PERS_MAIL FROM PERSON WHERE PERS_ID="+Session["Id"].ToString(),conn);  // reuse object

           mail.From = "Website";
           //mail.Subject ="User placed a Spot Bid/Offer";
           mail.Subject = cmdGetEmail.ExecuteScalar().ToString() + " placed a Spot Bid/Offer";

           mail.To = Application["strEmailAdmin"].ToString();
           mail.Cc = Application["strEmailOwner"].ToString();
           mail.Body = "<u>Order Number: " +strOrderNum+"</U><BR><br><b>Confirmation Dialog:</b><BR>"+ ViewState["ConfirmationText"].ToString();
           mail.BodyFormat = MailFormat.Html;
           //SmtpMail.SmtpServer="caesar";
           TPE.Utility.EmailLibrary.Send(mail);





        pnMain.Visible= true;
        pnConfirm.Visible = false;
        Response.Redirect("/Spot/Spot_Floor.aspx");

    }

</script>
<script language="javascript">
        <!--
        function Swap_Other(Flag)
        {
	       if(Flag)
	       {
		      document.form.ddlProduct.disabled=true;
		      document.form.txtProduct.disabled = false;
		      document.form.txtMelt.Value ='';
		      document.form.txtDensity.Value ='';
		      document.form.txtAdds.Value ='';

	       }
	       else
	       {
		      document.form.ddlProduct.disabled = false;
		      document.form.txtProduct.disabled = true;

	       }
        }

        function Address_PopUp()
        // This function opens up a pop up window and passes the appropiate ID number for the window
        {


        if (document.all)
            var xMax = screen.width, yMax = screen.height;
        else
            if (document.layers)
                var xMax = window.outerWidth, yMax = window.outerHeight;
            else
                var xMax = 640, yMax=480;

        var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
        var str = "../administrator/Location_Details.aspx?ID=" + document.form.ddlDelivery_Point.options[document.form.ddlDelivery_Point.selectedIndex].value;
        window.open(str,'Address','location=true,toolbar=true,width=275,height=370,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');

        }


    //-->
</script>
<form id="form" runat="server">
	<TPE:TEMPLATE id="Template1" Runat="server" PageTitle="Order Entry"></TPE:TEMPLATE>
	<!-- flag for the radio selection --><TPE:WEB_BOX id="WebBox1" Runat="Server" Heading="Order Entry"></TPE:WEB_BOX><asp:panel id="pnMain" runat="server">
		<asp:Label id="lblMessage" runat="server"></asp:Label>
		<asp:ValidationSummary id="ValidationSummary1" runat="server" HeaderText="There are problems with the following fields. Please correct the errors below."></asp:ValidationSummary>
		<TPE:Instructions id="Instructions1" Runat="server" Text="To place an order, be sure to fill out all of the fields including any details. Once an order has been placed, it will be posted on the Exchange Floor for the time period you specify."></TPE:Instructions>
		<TABLE cellSpacing="0" cellPadding="0" align="center">
			<TR>
				<TD>
					<TABLE width="350">
						<TR>
							<TD colSpan="2">
								<TABLE cellSpacing="0" cellPadding="0" border="0">
									<TR>
										<TD class="S1">Product
										</TD>
										<TD>
											<asp:RadioButton id="radio_Product" onclick="JavaScript:Swap_Other(0)" runat="server" GroupName="RadioGroup1"
												checked="true"></asp:RadioButton></TD>
										<TD>
											<asp:DropDownList id="ddlProduct" runat="server"></asp:DropDownList></TD>
									</TR>
									<TR>
										<TD class="S1">Other
										</TD>
										<TD class="S1">
											<asp:RadioButton id="radio_Other" onclick="JavaScript:Swap_Other(1)" runat="server" GroupName="RadioGroup1"></asp:RadioButton></TD>
										<TD class="S1">
											<asp:TextBox id="txtProduct" runat="server"></asp:TextBox></TD>
									</TR>
								</TABLE>
							</TD>
						</TR>
						<TR>
							<TD class="S1">Details
							</TD>
							<TD>
								<asp:TextBox id="txtDetail" runat="server" size="30" maxlength="75"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="S1" colSpan="2">Melt
								<asp:TextBox id="txtMelt" runat="server" size="4" maxlength="16" type="text"></asp:TextBox>&nbsp;&nbsp;&nbsp;Density
								<asp:CompareValidator id="CompareValidator1" Runat="server" Type="Double" Operator="DataTypeCheck" Display="none"
									ErrorMessage="Density must be a number." ControlToValidate="txtDensity"></asp:CompareValidator>
								<asp:RangeValidator id="RangeValidator1" runat="server" type="Double" Display="none" ErrorMessage="Density must be less than one."
									ControlToValidate="txtDensity" MaximumValue=".9999" MinimumValue=".0001"></asp:RangeValidator>
								<asp:TextBox id="txtDensity" runat="server" size="4" maxlength="16" type="text"></asp:TextBox>&nbsp;&nbsp;&nbsp;Adds
								<asp:TextBox id="txtAdds" runat="server" size="4" maxlength="20" type="text"></asp:TextBox></TD>
						</TR>
						<TR>
							<TD class="S1">Quantity
							</TD>
							<TD>
								<asp:CompareValidator id="CompareValidator2" Runat="server" Type="Integer" Operator="DataTypeCheck" Display="none"
									ErrorMessage="Quantity must be a number." ControlToValidate="txtQty"></asp:CompareValidator>
								<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" Display="none" ErrorMessage="Quanity cannot be blank"
									ControlToValidate="txtQty"></asp:RequiredFieldValidator>
								<asp:TextBox id="txtQty" runat="server" size="3" maxlength="5"></asp:TextBox>&nbsp;&nbsp;&nbsp;
								<asp:DropDownList id="ddlQty" runat="server"></asp:DropDownList></TD>
						</TR>
					</TABLE>
				</TD>
				<TD>
					<TABLE>
						<TR>
							<TD class="S1" vAlign="top">
								<asp:Label id="lblPoint" runat="server"></asp:Label></TD>
							<TD><%if (Session["Typ"].ToString().Equals("P")  || Session["Typ"].ToString().Equals("S")){%>
								<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" Display="none" ErrorMessage="Delivery Point cannot be blank"
									ControlToValidate="txtDelivery_Point"></asp:RequiredFieldValidator><%} %>
								<asp:TextBox id="txtDelivery_Point" runat="server"></asp:TextBox><BR>
								<asp:Panel id="pnDelivery_Point" runat="server">
<asp:DropDownList id="ddlDelivery_Point" runat="server"></asp:DropDownList>&nbsp;&nbsp;<A onmouseover="this.style.cursor='hand'" onclick="javascript:Address_PopUp()">
										<U>Details</U></A> 
          </asp:Panel></TD>
						</TR>
						<TR>
							<TD class="S1">Terms
							</TD>
							<TD>
								<asp:DropDownList id="ddlTerms" runat="server"></asp:DropDownList></TD>
						</TR>
						<TR>
							<TD class="S1">
								<asp:Label id="lblPrice" runat="server"></asp:Label></TD>
							<TD>
								<asp:CompareValidator id="CompareValidator3" Runat="server" Type="Integer" Operator="DataTypeCheck" Display="none"
									ErrorMessage="Price must be a number." ControlToValidate="txtPrice"></asp:CompareValidator>
								<asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" Display="none" ErrorMessage="Price cannot be blank"
									ControlToValidate="txtPrice"></asp:RequiredFieldValidator>$<B>.</B>
								<asp:TextBox id="txtPrice" runat="server" size="4" maxlength="4"></asp:TextBox>per 
								lb.
							</TD>
						</TR>
						<TR>
							<TD class="S1">Markup</TD>
							<TD>
								<asp:CompareValidator id="CompareValidator4" Runat="server" Type="Integer" Operator="DataTypeCheck" Display="none"
									ErrorMessage="Markup must be a number." ControlToValidate="txtMarkup"></asp:CompareValidator>
								<asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server" Display="none" ErrorMessage="Markup cannot be blank"
									ControlToValidate="txtMarkup"></asp:RequiredFieldValidator>$<B>.</B>
								<asp:TextBox onkeypress="return noenter()" id="txtMarkup" runat="server" size="4" maxlength="4"
									Enabled="False">02</asp:TextBox>per lb.</TD>
						<TR>
							<TD class="S1">
								<asp:Label id="lblExpires" runat="server"></asp:Label></TD>
							<TD>
								<asp:DropDownList id="ddlexpires" runat="server"></asp:DropDownList></TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
			<TR vAlign="middle" height="25">
				<TD colSpan="3">
					<CENTER>
						<asp:Button class="tpebutton" id="Button1" onclick="Click_Submit" runat="server" Text="Submit"></asp:Button></CENTER>
				</TD>
			</TR>
		</TABLE>
	</asp:panel><asp:panel id="pnConfirm" runat="server" Visible="false">
		<CENTER><BR>
			<TABLE cellSpacing="0" cellPadding="0" width="425" border="0">
				<TR vAlign="top">
					<TD>
						<asp:Label id="lblConfirm" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD colSpan="7">
						<CENTER><BR>
							<BR>
							<asp:Button class="tpebutton" id="btnSubmit" onclick="Save" runat="server" Text="Submit"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:Button class="tpebutton" id="btnCancel" onclick="Click_Cancel" runat="server" Text="Cancel"></asp:Button></CENTER>
					</TD>
				</TR>
			</TABLE>
		</CENTER>
		<BR>
		<BR>
		<BR>
	</asp:panel><TPE:WEB_BOX id="Web_Box1" Runat="server" Footer="true"></TPE:WEB_BOX><TPE:TEMPLATE id="Template2" Runat="server" Footer="True"></TPE:TEMPLATE></form>
