<%@ Page Language="c#" Codebehind="Matched_Order.aspx.cs" AutoEventWireup="True" Inherits="localhost.Spot.Matched_Order" MasterPageFile="~/MasterPages/TemplateWithoutInstructions.Master" Title="Matched Order"  EnableEventValidation="false"  %>

<%@ MasterType VirtualPath="~/MasterPages/TemplateWithoutInstructions.Master" %>

<asp:Content ContentPlaceHolderID="cphJavaScript" runat="server">

    <script language="javascript">
     <!--
    function Swap_Other(Flag)
    {
	   if(Flag)
	   {
		  document.getElementById('<%=ddlProduct.ClientID %>').disabled=true;
		  document.getElementById('<%=txtProduct.ClientID %>').disabled = false;
		  document.getElementById('<%=txtMelt.ClientID %>').Value ='';
		  document.getElementById('<%=txtDensity.ClientID %>').Value ='';
		  document.getElementById('<%=txtAdds.ClientID %>').Value ='';
		  document.getElementById('Other_Product').value ='1';
	   }
	   else
	   {
		  document.getElementById('<%=ddlProduct.ClientID %>').disabled = false;
		  document.getElementById('<%=txtProduct.ClientID %>').disabled = true;
		  document.getElementById('Other_Product').value = '';
	   }
    }
    function Address_PopUp()
    // This function opens up a pop up window and passes the appropiate ID number for the window
    {


    if (document.all)
        var xMax = screen.width, yMax = screen.height;
    else
        if (document.layers)
            var xMax = window.outerWidth, yMax = window.outerHeight;
        else
            var xMax = 640, yMax=480;

    var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
    var str = "../administrator/Location_Details.aspx?ID=" + document.getElementById('<%=ddlDelivery_Point.ClientID %>').options[document.getElementById('<%=ddlDelivery_Point.ClientID %>').selectedIndex].value;
    window.open(str,'Address','location=true,toolbar=true,width=275,height=390,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');


    }
    function Address_PopUp2()
    // This function opens up a pop up window and passes the appropiate ID number for the window
    {


    if (document.all)
        var xMax = screen.width, yMax = screen.height;
    else
        if (document.layers)
            var xMax = window.outerWidth, yMax = window.outerHeight;
        else
            var xMax = 640, yMax=480;

    var xOffset = (xMax - 200)/2, yOffset = (yMax - 200)/2;
    var str = "../administrator/Location_Details.aspx?ID=" + document.getElementById('<%=ddlShipping_Point.ClientID %>').options[document.getElementById('<%=ddlShipping_Point.ClientID %>').selectedIndex].value;
    window.open(str,'Address','location=true,toolbar=true,width=275,height=390,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+'');

    }


    //-->
    </script>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphHeading"><span class="Header Color1 Bold">Matched Orders</span></asp:Content>

<asp:Content ContentPlaceHolderID="cphMain" runat="server">


    <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="There are problems with the following fields. Please correct the errors below." CssClass="Header Color3" ForeColor=" "></asp:ValidationSummary>
    <asp:Label ID="test" runat="server"></asp:Label>
    <!-- flag for the radio selection -->
    <input type="hidden" class="Content" name="Other_Product" id="Other_Product">
    <table bordercolor="#212121" cellspacing="0" cellpadding="0" border="1" width="927" class="Content">
        <tr>
            <td>
                <asp:Panel ID="pnMain" runat="server">
                    <table cellspacing="0" cellpadding="0" border="0" class="Content">
                        <tr valign="middle" bgcolor="#cccccc" height="20">
                            <td class="Header" align="center" width="180">
                                Buyer</td>
                            <td bgcolor="#212121" rowspan="6">
                            </td>
                            <td class="Header" align="center" width="325">
                                Contract</td>
                            <td bgcolor="#212121" rowspan="6">
                            </td>
                            <td class="Header" align="center" width="180">
                                Seller</td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <table cellspacing="0" cellpadding="2" border="0">
                                    <tr>
                                        <td>
                                            Company:<br>
                                            <asp:DropDownList ID="ddlBuyer_Comp" CssClass="InputForm" runat="server" OnSelectedIndexChanged="Change_Buyer" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <br>
                                            User:<br>
                                            <asp:DropDownList ID="ddlBuyer" runat="server" CssClass="InputForm">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblBuyerEditUser" runat="server" CssClass="LinkNormal">Edit User</asp:Label>&nbsp;&nbsp;&nbsp;<asp:Label ID="lblBuyerAddUser" runat="server" CssClass="LinkNormal">Add User</asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RangeValidator ID="RangeValidator1" runat="server" MaximumValue="2.9999" MinimumValue=".0001" Type="Double" Display="none" ErrorMessage="Buy price must be less than one dollar per pound." ControlToValidate="txtBuyPrice"></asp:RangeValidator>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" Display="none" ErrorMessage="Buy Price must be a number." ControlToValidate="txtBuyPrice" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="none" ErrorMessage="Buy Price cannot be blank" ControlToValidate="txtBuyPrice"></asp:RequiredFieldValidator><br>
                                            Buy Price $
                                            <asp:TextBox ID="txtBuyPrice" runat="server" CssClass="InputForm" MaxLength="5" size="5"></asp:TextBox>per pound</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        Payment Terms:</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlBuyerTerms" runat="server" CssClass="InputForm">
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        PO#
                                                        <asp:TextBox ID="txtPO" runat="server" MaxLength="20" CssClass="InputForm"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="none" ErrorMessage="PO# cannot be blank" ControlToValidate="txtPO"></asp:RequiredFieldValidator></td>
                                    </tr>
                                </table>
                            </td>
                            <td align="left">
                                <table cellspacing="0" cellpadding="2" border="0">
                                    <tr>
                                        <td colspan="2">
                                            Product:
                                            <table width="150" border="0">
                                                <tr>
                                                    <td>
                                                        <asp:RadioButton ID="radio_Product" onclick="JavaScript:Swap_Other(0)" runat="server" GroupName="RadioGroup1" border="0"></asp:RadioButton></td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlProduct" CssClass="InputForm" runat="server">
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td class="S1">
                                                        <asp:RadioButton ID="radio_Other" onclick="JavaScript:Swap_Other(1)" runat="server" GroupName="RadioGroup1"></asp:RadioButton></td>
                                                    <td class="S1">
                                                        <asp:TextBox ID="txtProduct" CssClass="InputForm" runat="server"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            Details:
                                            <asp:TextBox ID="txtDetail" runat="server" CssClass="InputForm"></asp:TextBox>
                                            <asp:CheckBox ID="cbPrime" runat="server" Text="Prime"></asp:CheckBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table>
                                                <tr>
                                                    <td>
                                                        Melt</td>
                                                    <td>
                                                        <asp:TextBox ID="txtMelt" Width="35px" runat="server" CssClass="InputForm"></asp:TextBox></td>
                                                    <td>
                                                        Density</td>
                                                    <td>
                                                        <asp:TextBox ID="txtDensity" Width="35px" runat="server" CssClass="InputForm"></asp:TextBox></td>
                                                    <td>
                                                        Adds:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtAdds" Width="35px" runat="server" CssClass="InputForm"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table>
                                                <tr>
                                                    <asp:CompareValidator ID="CompareValidator2" runat="server" Display="none" ErrorMessage="Quantity must be a number." ControlToValidate="txtQty" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator><td>
                                                        Quantity</td>
                                                    <td>
                                                        <asp:TextBox ID="txtQty" Width="35px" runat="server" CssClass="InputForm" ReadOnly="false" value="1"></asp:TextBox></td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSize" runat="server" CssClass="InputForm" OnSelectedIndexChanged="Change_Size" AutoPostBack="True">
                                                        </asp:DropDownList></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Broker
                                        </td>
                                        <td>
                                            Commission</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlBroker" runat="server" CssClass="InputForm" AutoPostBack="True" OnSelectedIndexChanged="ddlBroker_SelectedIndexChanged">
                                            </asp:DropDownList></td>
                                        <td>
                                            <asp:RangeValidator ID="Rangevalidator4" runat="server" CssClass="InputForm" MaximumValue="100" MinimumValue="0" Type="Integer" Display="none" ErrorMessage="Broker commission must be between 0% and 100%" ControlToValidate="txtCommission"></asp:RangeValidator>
                                            <asp:TextBox ID="txtCommission" Width="35px" runat="server" CssClass="InputForm"></asp:TextBox>%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblWarehouse" runat="server">Storage Warehouse:</asp:Label><br>
                                            <asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="InputForm" OnSelectedIndexChanged="Change_Warehouse" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <table cellspacing="0" cellpadding="2" border="0" align=left>
                                    <tr>
                                        <td style="width: 206px">
                                            Company:
                                            <asp:Label ID="lblSellerNotAvail" runat="server"></asp:Label><br>
                                            <asp:DropDownList ID="ddlSeller_Comp" runat="server" CssClass="InputForm" OnSelectedIndexChanged="Change_Seller" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr  align=left>
                                        <td style="width: 206px">
                                            <br>
                                            User:<br>
                                            <asp:DropDownList ID="ddlSeller" runat="server" CssClass="InputForm">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblSellerEditUser" runat="server" CssClass="LinkNormal">Edit User</asp:Label>&nbsp;&nbsp;&nbsp;<asp:Label ID="lblSellerAddUser" runat="server" CssClass="LinkNormal">Add User</asp:Label></td>
                                    </tr>
                                    <tr align=left>
                                        <td style="width: 206px">
                                            <asp:RangeValidator ID="RangeValidator2" runat="server" MaximumValue="2.9999" MinimumValue=".0001" Type="Double" Display="none" ErrorMessage="Sell price must be less than one dollar per pound." ControlToValidate="txtSellPrice"></asp:RangeValidator>
                                            <asp:CompareValidator ID="CompareValidator3" runat="server" Display="none" ErrorMessage="Sell Price must be a number." ControlToValidate="txtSellPrice" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="none" ErrorMessage="Sell Price cannot be blank" ControlToValidate="txtSellPrice"></asp:RequiredFieldValidator><br>
                                            Sell Price $
                                            <asp:TextBox ID="txtSellPrice" runat="server" Width="55px" MaxLength="5" CssClass="InputForm"></asp:TextBox> per pound</td>
                                    </tr>
                                    <tr align=left>
                                        <td style="width: 206px">
                                            Shipping Terms:
                                            <asp:DropDownList ID="ddlSellerTerms" runat="server" CssClass="InputForm">
                                            </asp:DropDownList></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr valign="middle" bgcolor="#cccccc" height="20">
                            <td class="Header" align="center">
                                Delivery Point</td>
                            <td class="Header" align="center">
                            </td>
                            <td class="Header" align="center">
                                Shipping Point</td>
                        </tr>
                        <tr>
                            <td>
                                <br>
                                <br>
                                &nbsp;&nbsp;
                                <asp:DropDownList ID="ddlDelivery_Point" runat="server" CssClass="InputForm" OnSelectedIndexChanged="ddlDelivery_Point_SelectedIndexChanged" AutoPostBack="True">
                                </asp:DropDownList>&nbsp;&nbsp;<a onmouseover="this.style.cursor='pointer'" onclick="javascript:Address_PopUp()" class="LinkNormal"><u>Details</u></a><br>
                                <asp:DropDownList id="ddlFreightComp" name="ddlFreightComp" runat="server" CssClass="InputForm" >
								</asp:DropDownList>&nbsp;
                            </td>
                            <td colspan="2">
                                <table border="0">
                                    <tr>
                                        <td>
                                            <asp:RangeValidator ID="RangeValidator3" runat="server" MaximumValue="99999" MinimumValue="0" Type="Double" Display="none" ErrorMessage="Enter Total Freight, it can NOT be cents per pound" ControlToValidate="txtFreight"></asp:RangeValidator>
                                            <asp:CompareValidator ID="CompareValidator4" runat="server" Display="none" ErrorMessage="Freight Price must be a number." ControlToValidate="txtFreight" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="none" ErrorMessage="Freight Price cannot be blank" ControlToValidate="txtFreight"></asp:RequiredFieldValidator>Total Freight/shipment
                                            <asp:TextBox ID="txtFreight" Width="75px" runat="server" CssClass="InputForm">0</asp:TextBox></td>

                                        <td valign ="bottom">
                                            Terms:<br />
                                            <asp:DropDownList ID="ddlShippingTerms" runat="server" CssClass="InputForm">
                                            </asp:DropDownList></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <br>
                                <br>
                                <asp:DropDownList ID="ddlShipping_Point" runat="server" CssClass="InputForm" OnSelectedIndexChanged="Change_City" AutoPostBack="True">
                                </asp:DropDownList>&nbsp;<a onmouseover="this.style.cursor='pointer'" onclick="javascript:Address_PopUp2()" class="LinkNormal"><u>Details</u></a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" height="30">
                            </td>
                            <td>
                                <center>
                                    <asp:Button ID="Button1" OnClick="Click_Continue" CssClass="Content Color2" runat="server" Text="Continue"></asp:Button></center>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnConfirm" runat="server" Visible="false">
                    <center><br>
                        <p class="Content Bold Color2">
                            Order Confirmation</p>
                    </center>
                    <asp:DropDownList ID="ddlAllocate" runat="server" Visible="false" CssClass="InputForm">
                    </asp:DropDownList>
                    <table cellspacing="4" cellpadding="4" align="center" border="0">
                        <tr valign="middle" bgcolor="#cccccc">
                            <td align="center" colspan="5">
                                <strong>
                                    <asp:Label ID="lblProduct" runat="server"></asp:Label></strong></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <strong>User</strong></td>
                            <td width="309">
                                <strong>Company</strong></td>
                            <td>
                                <strong>Location</strong></td>
                            <td>
                                <strong>Price</strong></td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Buyer</strong></td>
                            <td>
                                <asp:Label ID="lblBuyer" runat="server"></asp:Label>
                            </td>
                            <td width="309">
                                <asp:Label ID="lblBuyerComp" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblBuyerLocal" runat="server"></asp:Label>
                            </td>
                            <td>
                                $<asp:Label ID="lblBuyPrice" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Seller</strong></td>
                            <td>
                                <asp:Label ID="lblSeller" runat="server"></asp:Label>
                            </td>
                            <td width="309">
                                <asp:Label ID="lblSellerComp" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblSellerLocal" runat="server"></asp:Label>
                            </td>
                            <td>
                                <font color="red">$(<asp:Label ID="lblSellPrice" runat="server"></asp:Label>)</font></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTaxName" runat="server" Font-Bold="True"></asp:Label>
                            </td>
                            <td align="center" colspan="3">
                            </td>
                            <td>
                                <asp:Label ID="lblTax" runat="server">0.0</asp:Label>
                                %</td>
                        </tr>
                        <tr>
                            <td height="26">
                                <strong>Commission</strong></td>
                            <td align="center" colspan="3" height="26">
                            </td>
                            <td height="26">
                                <font color="red">&nbsp;(<asp:Label ID="lblComm" runat="server"></asp:Label>)%</font></td>
                        </tr>                        
                        <tr>
                            <td>
                                <strong>Total Freight</strong></td>
                            <td align="center">
                                <font color="red">
                                    <asp:Label ID="lblFreightMessage" runat="server"></asp:Label>
                                </font>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblFreightCompany" runat="server"></asp:Label>
                            </td>
                            <td></td>
                            <td>
                                <font color="red">$(<asp:Label ID="lblFreight" runat="server"></asp:Label>)</font></td>
                        </tr>                        
                        <tr>
                            <td>
                                <strong>Profit</strong></td>
                            <td>
                            </td>
                            <td width="309">
                            </td>
                            <td>
                            </td>
                            <td>
                                <strong>&nbsp;$<asp:Label ID="lblMarkup" runat="server"></asp:Label></strong></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="5">
                                <br>
                                <br>
                                &nbsp;
                                <asp:Label ID="lblCreditInformation" runat="server" Font-Bold="True"></asp:Label>
                                <br>
                                <br>
                                <asp:DataGrid ID="dg" runat="server" Visible="False" AutoGenerateColumns="False" CellPadding="2" ShowFooter="True" HorizontalAlign="Center" Width="800px">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                    <HeaderStyle BackColor="Silver"></HeaderStyle>
                                    <Columns>
                                        <asp:HyperLinkColumn Visible="False" DataTextField="COMP_ID" SortExpression="COMP_ID ASC" HeaderText="Company ID"></asp:HyperLinkColumn>
                                        <asp:BoundColumn DataField="COMP_NAME" SortExpression="COMP_NAME ASC" HeaderText="Company Name">
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FO" SortExpression="FO ASC" HeaderText="Outstanding" DataFormatString="{0:c}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="COMP_CREDIT_LIMIT" SortExpression="COMP_CREDIT_LIMIT ASC" HeaderText="Credit Limit" DataFormatString="{0:c}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="COMMINVTRY" SortExpression="COMMINVTRY ASC" HeaderText="Committed Inventory" DataFormatString="{0:c}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="OCURRENT" SortExpression="OCURRENT ASC" HeaderText="Current" DataFormatString="{0:c}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="O30" SortExpression="O30 ASC" HeaderText="1-30" DataFormatString="{0:c}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="O3160" SortExpression="O3160 ASC" HeaderText="30-60" DataFormatString="{0:c}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="O6190" SortExpression="O6190 ASC" HeaderText="60-90" DataFormatString="{0:c}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="O90ORMORE" SortExpression="O90ORMORE ASC" HeaderText="&gt;90" DataFormatString="{0:c}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="5">
                                <asp:Button CssClass="Content Color2" ID="Button2" OnClick="Click_Back" runat="server" Text="Back"></asp:Button>
                                <asp:Button CssClass="Content Color2" ID="Button3" OnClick="Click_Submit" runat="server" Text="Submit"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                
  
                
            </td>
        </tr>
        

</asp:Content>
