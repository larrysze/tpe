

using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace localhost.Spot
{
	/// <summary>
	/// Summary description for Archive.
	/// </summary>
	public partial class Archive : System.Web.UI.Page
	{
	
	/*****************************************************************************
	'*   1. File Name       : spot/Archive.aspx	                                 *
	'*   2. Description     : Lists past spot offers/bids                        *
	'*						                                                     *
	'*   3. Modification Log:                                                    *
	'*     Ver No.       Date          Author             Modification           *
	'*   -----------------------------------------------------------------       *
	'*      1.00      6-6-2004       Zach                  First Baseline        *
	'*                                                                           *
	'*****************************************************************************/

		public void Page_Load(object sender, EventArgs e)
		{
            Master.Width = "1024px";
			//must logged in to access this page
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "B") && ((string)Session["Typ"] != "L") && ((string)Session["Typ"] != "S") && ((string)Session["Typ"] != "P") && ((string)Session["Typ"] != "OP") && ((string)Session["Typ"] != "S") && ((string)Session["Typ"] != "OS") && ((string)Session["Typ"] != "X"))
			{
				Response.Redirect("/default.aspx");
			}
			// 1st thing a post back should do is blank the search value
			ViewState["Search"] = "";

			if (!IsPostBack)
			{
				// db connection for a list of companies
				SqlConnection conn2;
				conn2 = new SqlConnection(Application["DBConn"].ToString());
				conn2.Open();

				
				SqlDataReader dtrGrade;
				SqlCommand cmdGrade = new SqlCommand("select * from grade",conn2);
				dtrGrade = cmdGrade.ExecuteReader();
				ddlResinType.Items.Add(new ListItem ("All","*"));

				while (dtrGrade.Read())
				{
					ddlResinType.Items.Add(new ListItem (dtrGrade["GRADE_NAME"].ToString(),dtrGrade["GRADE_ID"].ToString()));
				}
				dtrGrade.Close();


				HelperFunction.fillCompany(ddlCompanies, this.Context);
				//SqlCommand cmdCompanies;
				//SqlDataReader dtrCompanies;
				//cmdCompanies= new SqlCommand("SELECT * FROM (SELECT COMP_ID,COMP_NAME,COMP_TYPE FROM COMPANY WHERE COMP_REG=1 AND COMP_TYPE IN ('P','S','D'))Q ORDER BY COMP_NAME", conn2);
				//dtrCompanies= cmdCompanies.ExecuteReader();

				//// add the default value(display all companies)
				//ddlCompanies.Items.Add(new ListItem ("Show all Companies","*"));
				//while (dtrCompanies.Read())
				//{
				//        ddlCompanies.Items.Add(new ListItem (dtrCompanies["COMP_NAME"].ToString(),dtrCompanies["COMP_ID"].ToString()));
				//}
				//dtrCompanies.Close();

				ViewState["Sort"] = "VAREXPR DESC";


				CreateDateList();
				BindDataGrid();
				CreateSessionBid();
			}
			HelperFunction.renderDisabledCompanyList(ddlCompanies, this.Context);
		}

		// <summary>
		//  Primary function that binds the grid
		// </summary>
        public void CreateSessionBid()
        {
            if (ddlSelector.SelectedItem.Value.Equals("-1"))
            {
                Session["bid"] = "1";
            }
            else
            {
                Session["bid"] = "0";
            }
        }
		public void BindDataGrid()
		{

			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			SqlDataAdapter dadContent;
			DataSet dstContent;
			string strSQL="";
			strSQL = "Exec spSpot_Archive @BidOfferSelector="+ddlSelector.SelectedItem.Value.ToString()+", @Sort='"+ ViewState["Sort"].ToString() +"' " ;
			// add resin filter
			if (!ddlResinType.SelectedItem.Value.Equals("*") )
			{
				strSQL +=" ,@Resin_Filter='"+ddlResinType.SelectedItem.Value.ToString()+"'";
			}
			// add company filter
			if (!ddlCompanies.SelectedItem.Value.Equals("*") )
			{
				strSQL +=" ,@CompID='"+ddlCompanies.SelectedItem.Value.ToString()+"'";
			}
			// add search parameter
			if (txtSearch.Text !="")
			{
				strSQL +=" ,@Search='"+txtSearch.Text+"'";
			}

			
			if (!ddlMonth.SelectedItem.Value.Equals("null"))
			{
				string strDate = ddlMonth.SelectedItem.Value.ToString();
				strSQL +=",@MM='"+strDate.Substring(4,2)+"'";
				strSQL +=",@YY='"+strDate.Substring(0,4)+"'";
			}
			
			if (ddlSelector.SelectedItem.Value.Equals("-1"))
			{
				//BID
				dg.Columns[9].HeaderText = "TPE Buy";
				dg.Columns[10].HeaderText = "TPE Sell";
			}
			else
			{
				//OFFER
				dg.Columns[9].HeaderText = "TPE Buy Price";
				dg.Columns[10].HeaderText = "TPE Sell Price";
			}
			//Hide the column if the user is not Admin
			if ((string)Session["Typ"] != "A") dg.Columns[10].Visible = false;

			//Response.Write(strSQL);
			dadContent = new SqlDataAdapter(strSQL,conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dg.DataSource = dstContent;
			dg.DataBind();
			conn.Close();

		}

		// <summary>
		// Sorting function
		// </summary>
		public void SortDG(Object sender, DataGridSortCommandEventArgs e )
		{


			string[] SortExprs;
			string CurrentSearchMode;
			string NewSearchMode;
			string ColumnToSort;
			string NewSortExpr;

			//  Parse the sort expression - delimiter space
			Regex r = new Regex(" "); // Split on spaces.
			SortExprs = r.Split(e.SortExpression) ;
			ColumnToSort = SortExprs[0];
			// If a sort order is specified get it, else default is descending
			if (SortExprs.Length > 1)
			{
				CurrentSearchMode = SortExprs[1].ToUpper();
				if (CurrentSearchMode == "ASC")
				{
					NewSearchMode = "DESC";
				}
				else
				{
					NewSearchMode = "ASC";
				}
			}
			else
			{   // If no mode specified, Default is descending
				NewSearchMode = "DeSC";
			}

			//  Derive the new sort expression.
			NewSortExpr = ColumnToSort + " " + NewSearchMode;

			// Figure out the column index
			int iIndex;
			iIndex = 1;
			switch(ColumnToSort.ToUpper())
			{
				case "VARID":
					iIndex = 1;
					break;
				case "VARSIZE":
					iIndex = 2;
					break;
				case "VARCONTRACT":
					iIndex = 3;
					break;
				case "VARMELT":
					iIndex = 4;
					break;
				case "VARDENS":
					iIndex = 5;
					break;
				case "VARADDS":
					iIndex = 6;
					break;
				case "VARTERM":
					iIndex = 7;
					break;
				case "VARPAY":
					iIndex = 8;
					break;
				case "VARPRICE":
					iIndex = 9;
					break;
				case "VARPRICE2":
					iIndex = 10;
					break;
				case "VARVALUE":
					iIndex = 11;
					break;
				case "VARPLACE":
					iIndex = 12;
					break;
				case "VARUSR":
					iIndex = 13;
					break;
				case "VAREXPR":
					iIndex = 14;
					break;
			}

			// alter the column's sort expression
			dg.Columns[iIndex].SortExpression = NewSortExpr;
			ViewState["Sort"] = NewSortExpr;
			// reset dbase page to the first one whether the sorting changes
			dg.CurrentPageIndex = 0;
			// Sort the data in new order
			BindDataGrid();

		}

		protected void AddOnDataBind(object sender, DataGridItemEventArgs e)
		{

			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				
				// reinstate link shouldn't be availalbe if both the zip and the port are unavailable
				if (((DataBinder.Eval(e.Item.DataItem, "VARPORT")).ToString() == "") && ((DataBinder.Eval(e.Item.DataItem, "VARZIP")).ToString() == "")  )
			
				{
					e.Item.Cells[0].Text ="";
				}
				// setting the mouseover color
				e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Silver'");

				if (e.Item.ItemType == ListItemType.Item)
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#eeeeee'");
				}
				else
				{
					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#D9D7D3'");
				}

				// adding Reinstate link

				if (!ddlSelector.SelectedItem.Value.Equals("-1"))
				{
					//Change this point for a call to Reinstate method (using code behind)
					//e.Item.Cells[0].Text = "<a href=\"/Spot/Reinstate.aspx?ID=S" +(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString() + "\">Reinstate</a>";
				}
				else
				{
					//Change this point for a call to Reinstate method (using code behind)
					//e.Item.Cells[0].Text = "<a href=\"/Spot/Reinstate.aspx?ID=" +(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString() + "\">Reinstate</a>";
				}

                if ((e.Item.ItemType != ListItemType.Header) && (e.Item.ItemType != ListItemType.Footer))
                {
                    if (e.Item.Cells[16].Text != "&nbsp;")
                    {
                        e.Item.Cells[16].Text = "<img onclick='PreviewEmail(" + e.Item.Cells[1].Text + ")' src='/pics/Icons/icon_quotation_bubble.gif'>";

                    }
                    else
                    {
                        e.Item.Cells[16].Text = "<img src='/pics/Icons/comment.gif'>";
                    }
                }


			}
		}
		// <summary>
		//  searches past months to see which months have transactions pending
		// </summary>
		protected void CreateDateList()
		{
			ddlMonth.Items.Clear(); // starts the list out fresh
			HelperFunction.fillDatesDesc(01, 2000, ddlMonth);
			ListItem lstItem = new ListItem("Show All Dates", "*", true);
			ddlMonth.Items.Add(lstItem);
			ddlMonth.SelectedItem.Selected = false;
			ddlMonth.Items[0].Selected = true;

			//ddlMonth.Items.Clear(); // starts the list out fresh
			//DateTime moment = DateTime.Now;
			////start out with today's dates
			//int iYY = moment.Year;
			//int iMM = moment.Month;
			//string strCompSQL; // holds the syntax of the company should it be needed
			//if (!ddlCompanies.SelectedItem.Value.Equals("*"))
			//{
			//        if (!ddlSelector.SelectedItem.Value.Equals("-1"))
			//        {
			//                strCompSQL =" AND BID_COMP='"+ddlCompanies.SelectedItem.Value.ToString()+"'";
			//        }
			//        else
			//        {
			//                strCompSQL =" AND OFFR_COMP='"+ddlCompanies.SelectedItem.Value.ToString()+"'";
			//        }
				
			//}
			//else
			//{
			//        strCompSQL ="";
			//}
			//SqlConnection conn2;
			//conn2 = new SqlConnection(Application["DBConn"].ToString());
			//conn2.Open();
			//SqlCommand cmdMonth;
			//SqlDataReader dtrMonth;

			//while (iYY >= 2000)
			//{
				
			//        if (!ddlSelector.SelectedItem.Value.Equals("-1"))
			//        {
			//                cmdMonth= new SqlCommand("SELECT TOP 1 LEFT(CONVERT(varchar,BID_EXPR,107),3) + ' ' + Right(CONVERT(varchar,BID_EXPR,107),4) AS Date FROM BBIDARCH WHERE Month(BID_EXPR) =@Month AND Year(BID_EXPR) = @Year " + strCompSQL, conn2);
			//        }
			//        else
			//        {
			//                cmdMonth= new SqlCommand("SELECT TOP 1 LEFT(CONVERT(varchar,OFFR_EXPR,107),3) + ' ' + Right(CONVERT(varchar,OFFR_EXPR,107),4) AS Date FROM BBOFFERARCH WHERE Month(OFFR_EXPR) =@Month AND Year(OFFR_EXPR) = @Year " + strCompSQL, conn2);
			//        }
			//        cmdMonth.Parameters.Add("@Month",iMM);
			//        cmdMonth.Parameters.Add("@Year",iYY);
			//        dtrMonth= cmdMonth.ExecuteReader();
			//        if (dtrMonth.HasRows)
			//        {
			//                dtrMonth.Read();
			//                ddlMonth.Items.Add(new ListItem (dtrMonth["Date"].ToString(),iMM.ToString() +"-"+iYY.ToString()));
			//        }
			//        dtrMonth.Close();
			//        iMM--;
			//        if (iMM == 0)
			//        {
			//                iMM = 12;
			//                iYY--;
			//        }
			//}
			//ddlMonth.Items.Add(new ListItem ("Show All Dates","null"));
			//conn2.Close();
		}
		protected void dgPageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dg.CurrentPageIndex = e.NewPageIndex;
			BindDataGrid();
		}
		protected void ChangeType(object sender, EventArgs e)
		{
			dg.CurrentPageIndex = 0;
			txtSearch.Text = "";
			CreateDateList();
			BindDataGrid();
		}
		protected void Click_Search(object sender, EventArgs e)
		{
			//ViewState["Search"] = ",@Search='"+txtSearch.Text+"' , @SearchCol='"+ddlSeachCol.SelectedItem.Value.ToString()+"' ";
			//ViewState["Search"] = ",@Search='"+txtSearch.Text+"'";
			dg.CurrentPageIndex = 0;
			BindDataGrid();
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dg.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dg_ItemCommand);

		}
		#endregion

		private void Reinstate(string ID)
		{
			String strSql = "";
			String Flag = "";
			String OId = "";
			
			SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			
			Flag=ID.Substring(0,1); // Left 1
			OId=ID.Substring(1,ID.Length-1); //Right remaining
			
			strSql = "Exec spReinstate @OldId=" + OId.ToString();
			strSql = strSql+ ", @Type='" + Flag.ToString() + "'";

			SqlCommand cmd = new SqlCommand(strSql, conn);
			cmd.ExecuteNonQuery();
			conn.Close();
			BindDataGrid();

		}

		private void dg_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.Item.ItemType.ToString().IndexOf("Item")>=0)
			{
				if (!ddlSelector.SelectedItem.Value.Equals("-1"))
				{
					Reinstate("S" + e.Item.Cells[1].Text.ToString());
					//e.Item.Cells[0].Text = "<a href=\"/Spot/Reinstate.aspx?ID=S" +(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString() + "\">Reinstate</a>";
				}
				else
				{
					//e.Item.Cells[0].Text = "<a href=\"/Spot/Reinstate.aspx?ID=" +(DataBinder.Eval(e.Item.DataItem, "VARID")).ToString() + "\">Reinstate</a>";
					Reinstate("P" + e.Item.Cells[1].Text.ToString());
				}
			}
		}
		//<summary>
		// Handler for when the user changes the resin filter
		//</summary>
		protected void ddlResinType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dg.CurrentPageIndex = 0;
			BindDataGrid();
		}

		protected void dg_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		protected void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dg.CurrentPageIndex = 0;
			BindDataGrid();
			
		}

		protected void ddlSelector_SelectedIndexChanged(object sender, System.EventArgs e)
		{
            //CreateDateList();
            BindDataGrid();
            CreateSessionBid();
		
		}
	}
}
