using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Mail;
using System.Text;
using TPE.Utility;


          /*****************************************************************************
          '*   1. File Name       : Spot/OrderConfirmation.aspx                        *
          '*   2. Description     : This page shows the  selected offer/bid details    *
          '*						that is passed from OfferDetails/Biddetails page   *
		  '*                        and in this page we can get the total price from   *
		  '*						total page and the shipto and billto address from  *
		  '*						Shipping Details page where user wants shipto and  *
		  '*						billto the offer/bid.                              *
		  '*                                                                           *
          '*   3. Modification Log:                                                    *
          '*     Ver No.       Date          Author             Modification           *
          '*   -----------------------------------------------------------------       *
          '*      1.00                      Hanu software        Comment               *
          '*                                                                           *
          '*****************************************************************************/
namespace PlasticsExchange
{
	/// <summary>
	/// Summary description for OrderConfirmation.
	/// </summary>
	public class OrderConfirmation : System.Web.UI.Page
		
	{
		protected System.Web.UI.WebControls.LinkButton LinkButton1;
		protected System.Web.UI.WebControls.Label lblID;
		protected System.Web.UI.WebControls.Label lblProduct;
		protected System.Web.UI.WebControls.Label lblSize;
		protected System.Web.UI.WebControls.Label lblMelt;
		protected System.Web.UI.WebControls.Label lblAdds;
		protected System.Web.UI.WebControls.Label lblTerms;
		protected System.Web.UI.WebControls.Label lblTotal;
		protected System.Web.UI.WebControls.Label lblDens;
		protected System.Web.UI.WebControls.Label LblShip;
		protected System.Web.UI.WebControls.Label LblBill;
		protected string TmpSId;
		protected string TmpBId;
		protected int TmpCreditID;
		protected System.Web.UI.HtmlControls.HtmlInputButton btnBack;
		protected string TmpCache;
		protected System.Web.UI.WebControls.Button btnBackTo;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.CheckBox cbAgreement;
		private  double total;
		private void Page_Load(object sender, System.EventArgs e)
		{
			/// without login nobody can access this page allways open default page			
			if (Session["ID"] == null)
			{
				Response.Redirect("/default.aspx");
			}
			TmpCache=Session["FrmName"].ToString();
			
			total=Math.Round( Convert.ToDouble(Session["PayTotal"].ToString()),3);
			lblTotal.Text=String.Format("{0:c}", total);

			//check that user chosee fax/online option if user chose fax credit app 
			//then he get the pdf link on this page otherwise he will not see the link.
			if (!Session["Typ"].ToString().Equals("Demo"))
			{
				LinkButton1.Visible=false;
			}
			else
			{
				if (TmpCache=="Credit")
				{
					LinkButton1.Visible=false;
				}
				else
				{
					LinkButton1.Visible=true;
				}
				
			}


			try
			{
				string [] ArrProd = Session.Contents["ProductID"].ToString().Split(new Char []{'^'}) ;		
				ViewState["ID"]=ArrProd[1].ToString();
			}
			catch //(Exception Ex)
			{
				Response.Redirect("Spot_Floor.aspx");
				//throw (Ex);
			}
			
			
			/// check here that page is post back or not if not than make the connection and execute the command
			if (!IsPostBack)
			{
	
				SqlConnection conn;
				conn = new SqlConnection(Application["DBConn"].ToString());
				conn.Open();

				/// offer read that passed value/parameter from spot_floor.aspx page/dashboard.aspx page				
				Hashtable param = new Hashtable();
				param.Add("@OFFR_ID", ViewState["ID"].ToString());

				StringBuilder sbSQL = new StringBuilder();			
				sbSQL.Append("		SELECT *, ");
				sbSQL.Append("		VARTERM=(CASE WHEN OFFR_TERM= 'FOB/Delivered' THEN 'Delivered' WHEN  ");
				sbSQL.Append("						OFFR_TERM= 'FOB/Shipping' THEN 'FOB '  END ), ");
				//New Aditions  on 14th Jan
				sbSQL.Append("		VARSIZE=CAST(OFFR_QTY AS VARCHAR)+' '+(CASE OFFR_SIZE WHEN 1 THEN ");
				sbSQL.Append("		'lb' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 42000 ");
				sbSQL.Append("		THEN 'T/L - Boxes' WHEN 45000 THEN 'Bulk Truck' WHEN 44092 THEN 'T/L Bags' ");
				sbSQL.Append("		WHEN 190000 THEN 'Rail Car'	END)+(CASE WHEN (OFFR_QTY>1 AND OFFR_SIZE <> 42000 AND OFFR_SIZE <> 44092 ) THEN 's' ELSE ' ' END) ");

				sbSQL.Append("		FROM BBOFFER ");
				sbSQL.Append("		WHERE  ");
				sbSQL.Append("		OFFR_ID= @OFFR_ID");
								
				SqlDataReader dtrOffer = DBLibrary.GetDataReaderFromSelect(conn,sbSQL.ToString(),param);

				if (dtrOffer.Read())
				{
					/// offer details are displayed here in different label that parameter recd. from spot_floor.aspx page/dashboard.aspx page 
					lblID.Text = dtrOffer["OFFR_ID"].ToString();
					lblProduct.Text = dtrOffer["OFFR_PROD"].ToString();
					//lblSize.Text = String.Format("{0:#,###}", dtrOffer["OFFR_SIZE"].ToString());
					lblSize.Text = dtrOffer["VARSIZE"].ToString();
					lblMelt.Text = dtrOffer["OFFR_MELT"].ToString();
					lblDens.Text = dtrOffer["OFFR_DENS"].ToString();
					lblAdds.Text = dtrOffer["OFFR_ADDS"].ToString();
					lblTerms.Text = dtrOffer["VARTERM"].ToString();
					
					ViewState["Firm"] = dtrOffer["OFFR_FIRM"].ToString();
					ViewState["Origin"] = dtrOffer["OFFR_FROM_LOCL"].ToString();
					ViewState["Expiration"] = dtrOffer["OFFR_EXPR"].ToString();
					ViewState["TERMS"] = dtrOffer["OFFR_PAY"].ToString();
					ViewState["TYPE"] = "Offer";
					dtrOffer.Close();
				}
				else
				{
					dtrOffer.Close();
					/// Bid read that passed value/parameter from spot_floor.aspx page
					
					Hashtable p = new Hashtable();
					param.Add("@BID_ID", ViewState["ID"].ToString());

					SqlDataReader dtrBid;
					sbSQL.Remove(0,sbSQL.Length);
					sbSQL.Append("		SELECT *, ");
					sbSQL.Append("		VARTERM=(CASE WHEN BID_TERM= 'FOB/Delivered' THEN 'Delivered' WHEN  ");
					sbSQL.Append("						BID_TERM= 'FOB/Shipping' THEN 'FOB '  END ), ");
					//New Aditions  on 14th Jan
					sbSQL.Append("		VARSIZE=CAST(BID_QTY AS VARCHAR)+' '+(CASE BID_SIZE WHEN 1 THEN ");
					sbSQL.Append("		'lb' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 42000 ");
					sbSQL.Append("		THEN 'T/L - Boxes' WHEN 45000 THEN 'Bulk Truck' WHEN 44092 THEN 'T/L Bags' ");
					sbSQL.Append("		WHEN 190000 THEN 'Rail Car'	END)+(CASE WHEN (BID_QTY>1 AND BID_SIZE <> 42000 AND BID_SIZE <> 44092 ) THEN 's' ELSE ' ' END) ");
					

					sbSQL.Append("		FROM BBID ");
					sbSQL.Append("		WHERE  ");
					sbSQL.Append("		BID_ID = @BID_ID");
					
					dtrBid = DBLibrary.GetDataReaderFromSelect(conn,sbSQL.ToString(),p);

					if (dtrBid.Read())
					{
						/// Bid details are displayed here in different label that parameter recd. from spot_floor.aspx page 
						lblID.Text = dtrBid["BID_ID"].ToString();
						lblProduct.Text = dtrBid["BID_PROD"].ToString();
						lblSize.Text = dtrBid["VARSIZE"].ToString();
						lblMelt.Text = dtrBid["BID_MELT"].ToString();
						lblDens.Text = dtrBid["BID_DENS"].ToString();
						lblAdds.Text = dtrBid["BID_ADDS"].ToString();
						lblTerms.Text = dtrBid["VARTERM"].ToString();

						ViewState["Firm"] = dtrBid["BID_FIRM"].ToString();
						ViewState["Origin"] = dtrBid["BID_TO_LOCL"].ToString();
						ViewState["Expiration"] = dtrBid["BID_EXPR"].ToString();
						ViewState["TERMS"] = dtrBid["BID_PAY"].ToString();
						ViewState["TYPE"] = "Bid";
						dtrBid.Close();
					
					}
					else
					{
						// output error
					}
					dtrBid.Close();
		
				}
				conn.Close();

				FillShipAddress(Session.Contents["ShipId"].ToString());

				if (Session.Contents["BillId"]== null) 
				{
					FillBillAddress(Session.Contents["ShipId"].ToString());
					Session.Contents["BillId"]=Session.Contents["ShipId"].ToString();
				}
				else
				{
					FillBillAddress(Session.Contents["BillId"].ToString());
				}
			}
			
		}
 
		private void FillShipAddress(string TmpSId)
		{
			//Function for getting the shippingto address from ShippingDetails.aspx displayed in this page
			SqlConnection conn;
			string ShipAdd = String.Empty;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			SqlDataReader DR;
			
			Hashtable param = new Hashtable();
			param.Add("@PLAC_ID", TmpSId);

			StringBuilder SbSql = new StringBuilder();
			SbSql.Append("SELECT DISTINCT PLACE.PLAC_ID, PLACE.PLAC_PERS,");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ADDR_ONE is not Null or ltrim(rtrim(PLACE.PLAC_ADDR_ONE))<>'' ");
			SbSql.Append("then  PLACE.PLAC_ADDR_ONE else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CITY is not Null or ltrim(rtrim( PLACE.PLAC_CITY))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CITY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_STAT is not Null or ltrim(rtrim( PLACE.PLAC_STAT))<>'' ");
			SbSql.Append("then + PLACE.PLAC_STAT else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CTRY  is not Null or ltrim(rtrim( PLACE.PLAC_CTRY ))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CTRY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ZIP is not Null or ltrim(rtrim(PLACE.PLAC_ZIP))<>'' ");
			SbSql.Append("then PLACE.PLAC_ZIP else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_RAIL_NUM is not Null or ltrim(rtrim(PLACE.PLAC_RAIL_NUM))<>'' ");
			SbSql.Append("then  PLACE.PLAC_RAIL_NUM else '' end  ");
			SbSql.Append("FROM PLACE ");
			SbSql.Append("WHERE PLACE.PLAC_ID = @PLAC_ID");
						
			DR = DBLibrary.GetDataReaderFromSelect(conn, SbSql.ToString(), param);
			
			if (DR.Read())
			{
				ShipAdd+=DR.GetValue(1).ToString()+ "<br>" ;
				ShipAdd+=DR.GetValue(2).ToString()+ "<br>" ;
				ShipAdd+=DR.GetValue(3).ToString()+ "<br>" ;
				ShipAdd+=DR.GetValue(4).ToString()+ "<br>" ;
				ShipAdd+=DR.GetValue(5).ToString()+ "<br>" ;
				ShipAdd+=DR.GetValue(6).ToString()+ "<br>" ;
				ShipAdd+=DR.GetValue(7).ToString()  ;
			}
			DR.Close();
			conn.Close();
			//shows the shipto address in label
			LblShip.Text=ShipAdd;
		}

		private void FillBillAddress(string TmpBId)
		{
			//Function for get the Billto address from ShippingDetails.aspx displayed in this page
			SqlConnection conn;
			string BillAdd = String.Empty;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			SqlDataReader DR;

			Hashtable param = new Hashtable();
			param.Add("@PLAC_ID", TmpBId);

			StringBuilder SbSql = new StringBuilder();
			SbSql.Append("SELECT DISTINCT PLACE.PLAC_ID, PLACE.PLAC_PERS,");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ADDR_ONE is not Null or ltrim(rtrim(PLACE.PLAC_ADDR_ONE))<>'' ");
			SbSql.Append("then  PLACE.PLAC_ADDR_ONE else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CITY is not Null or ltrim(rtrim( PLACE.PLAC_CITY))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CITY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_STAT is not Null or ltrim(rtrim( PLACE.PLAC_STAT))<>'' ");
			SbSql.Append("then + PLACE.PLAC_STAT else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CTRY  is not Null or ltrim(rtrim( PLACE.PLAC_CTRY ))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CTRY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ZIP is not Null or ltrim(rtrim(PLACE.PLAC_ZIP))<>'' ");
			SbSql.Append("then PLACE.PLAC_ZIP else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_RAIL_NUM is not Null or ltrim(rtrim(PLACE.PLAC_RAIL_NUM))<>'' ");
			SbSql.Append("then  PLACE.PLAC_RAIL_NUM else '' end  ");
			SbSql.Append("FROM PLACE ");
			SbSql.Append("WHERE PLACE.PLAC_ID = '" + TmpBId + "'");

			DR = DBLibrary.GetDataReaderFromSelect(conn, SbSql.ToString(), param);
			if (DR.Read())
			{
				BillAdd+=DR.GetValue(1).ToString()+"<br>" ;
				BillAdd+=DR.GetValue(2).ToString()+"<br>" ;
				BillAdd+=DR.GetValue(3).ToString()+"<br>" ;
				BillAdd+=DR.GetValue(4).ToString()+"<br>" ;
				BillAdd+=DR.GetValue(5).ToString()+"<br>" ;
				BillAdd+=DR.GetValue(6).ToString()+"<br>" ;
				BillAdd+=DR.GetValue(7).ToString()  ;
			}
			DR.Close();
			conn.Close();
			//shows the billto address in label
			LblBill.Text=BillAdd;			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.LinkButton1.Click += new System.EventHandler(this.LinkButton1_Click);
			this.btnBackTo.Click += new System.EventHandler(this.btnBackTo_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		private void InsertData()
		{
			string strCredit=string.Empty;
			string BID_OFFR=string.Empty;	
			SqlConnection conn;
			conn = new SqlConnection(Application["DBconn"].ToString());
			SqlCommand Cmd = new SqlCommand();
			//here we check that we are coming after filling the credit application or not. 
			if (TmpCache=="Credit")
			{
				SqlConnection conn2;
				conn2 = new SqlConnection(Application["DBconn"].ToString());
				SqlCommand CmdCA = new SqlCommand(); //Command for CreditApp
				CmdCA.CommandText = Request.QueryString["PassVal"].ToString();
				conn2.Open();
				CmdCA.Connection=conn2;
				strCredit = CmdCA.ExecuteScalar().ToString() ;
				conn2.Close();

			}
			else
			{
				strCredit="0";
			}
			
			string [] ArrProd = Session.Contents["ProductID"].ToString().Split(new Char []{'^'}) ;		
			ViewState["ID"]=ArrProd[1].ToString();
			BID_OFFR =ArrProd[0].ToString();

			//here we check that in table entry is for bid or offer 
			if (BID_OFFR=="B")
			{
				Cmd.CommandText="Insert into Pending_orders(PERSON_ID,ORDR_BID,ORDR_DATE,ORDR_PROD_SPOT,ORDR_SIZE,ORDR_MELT,ORDR_DENS,ORDR_ADDS,ORDR_Terms,ORDR_ISSPOT,CREDIT_ID,ORDR_STAT,ORDR_FREIGHT,ORDR_TOTAL,ORDR_SHIPFROM,ORDR_SHIPTO,ORDR_BILTO )" ;
				Cmd.CommandText+="values ('"+ Session["ID"].ToString() +"','" + ViewState["ID"].ToString()  +"','" + DateTime.Today + "','" + lblProduct.Text.ToString() + "','" ;
				Cmd.CommandText+=lblSize.Text.ToString() + "','" + lblMelt.Text.ToString() + "','" + lblDens.Text.ToString() +  "','" + lblAdds.Text.ToString() + "','" + lblTerms.Text.ToString() + "',0," + strCredit.ToString() + ",'P','";
				Cmd.CommandText+=Session["freight"].ToString() +"','" + Session["PayTotal"].ToString()  +  "','" + Session.Contents["ShipFrom"]  +  "','" + Session.Contents["ShipId"]  +  "','" + Session.Contents["BillId"]  +  "')" ;

			}
			else
			{
				Cmd.CommandText="Insert into Pending_orders(PERSON_ID,ORDR_OFFR,ORDR_DATE,ORDR_PROD_SPOT,ORDR_SIZE,ORDR_MELT,ORDR_DENS,ORDR_ADDS,ORDR_TERMS,ORDR_ISSPOT,CREDIT_ID,ORDR_STAT,ORDR_FREIGHT,ORDR_TOTAL,ORDR_SHIPFROM,ORDR_SHIPTO,ORDR_BILTO   )" ;
				Cmd.CommandText+="values ('" + Session["ID"].ToString() +"','" + ViewState["ID"].ToString()  +"','" + DateTime.Today + "','" + lblProduct.Text.ToString() + "','" ;
				Cmd.CommandText+=lblSize.Text.ToString() + "','" + lblMelt.Text.ToString() + "','" + lblDens.Text.ToString() +  "','" + lblAdds.Text.ToString() + "','" + lblTerms.Text.ToString() + "',0," + strCredit.ToString() + ",'P','" + Session["freight"].ToString() +"','";
				Cmd.CommandText+= Session["PayTotal"].ToString() + "','" + Session.Contents["ShipFrom"] +  "','" + Session.Contents["ShipId"]  +  "','" + Session.Contents["BillId"]  +  "')" ;

			}
			conn.Open(); 
			Cmd.Connection=conn;
			Cmd.ExecuteNonQuery();

		}

		

		private void LinkButton1_Click(object sender, System.EventArgs e)
		{
			//click on pdf file link opens the pdf file	
			Response.Redirect("/docs/Credit_App.pdf");
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			InsertData();


			Session.Remove("ProductID");
			Session.Remove("ShipFrom");
			Session.Remove("ShipId");
			Session.Remove("BillId");
			Response.Redirect("ThankYou.aspx"); 
			// UNCHECK THE COMMENTED CODE FOR MAILING INFORMATION

			//**************************************/
			// Mailing Code  Below					/
			// mail is sent to the admin that		/
			// an order is placed in the			/
			// database in the temparary table		/
			//**************************************/
			
			//string from = "agupta@hanusoftware.com";
			//string to = "agupta@hanusoftware.com";
			//string subject = "Checking";
			//string body = "<html><body>Check - success</body></html>";
			//SmtpMail.SmtpServer = "mail.hanusoftware.com";
			//TPE.Utility.EmailLibrary.Send(from, to, subject, body);
			//
			//MailMessage mail = new MailMessage();
			//send email 
			 
			//Get users email address
			 
			//SqlCommand cmdGetEmail;
			//cmdGetEmail = new SqlCommand("Select PERS_MAIL FROM PERSON WHERE PERS_ID="+Session["Id"].ToString(),conn);  // reuse object
			 
			//mail.From = "Website";
			//mail.Subject ="User placed a Spot Bid/Offer";
			//mail.Subject = cmdGetEmail.ExecuteScalar().ToString() + " placed a Spot Bid/Offer";
			 
			//mail.To = Application["strEmailAdmin"].ToString();
			//mail.Cc = Application["strEmailOwner"].ToString();
			//mail.Body = "Check Your mailbox.";
			//mail.BodyFormat = MailFormat.Html;
			//SmtpMail.SmtpServer="localhost";
			//TPE.Utility.EmailLibrary.Send(mail);
			
		}

		private void btnBackTo_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Total.aspx");
		}
	}
}
