using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Mail;
using System.Text;
using TPE.Utility;

          /*****************************************************************************
          '*   1. File Name       : OrderTotal.aspx                                    *
          '*   2. Description     : This page shows the details of offer selected  by  *
		  '*						the user. In addition it also displays calculated  *
          							freight along with the shipping  details and also  *
		                            show total price that user have topay upon         *
									confirmation of that order				           * 
          '*   3. Modification Log:                                                    *
          '*     Ver No.       Date          Author             Modification           *
          '*   -----------------------------------------------------------------       *
          '*      1.00                      Hanu software        Comment               *
          '*                                                                           *
          '*****************************************************************************/
namespace localhost.Spot
{
	/// <summary>
	/// Summary description for Total.
	/// </summary>
	public partial class OrderTotal : System.Web.UI.Page
	{
		private double total;
		private double price;
		//		private double TmpFreight;
		 
		
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Session["ID"] == null)
			{
				Response.Redirect("/default.aspx");
			}

			if  (Session["UType"].ToString().Equals("D") && (Session["FrmName"]==null)) 
			{
				lblTitle2.ForeColor = System.Drawing.Color.Orange;
				lblTitle3.ForeColor = System.Drawing.Color.Black;
				pnlCreditOn.Visible=true;
				pnlCreditOff.Visible=false;

				cbAgreement.Visible = false;
				txtPO.Enabled = true;
			}
			else
			{
				lblTitle2.ForeColor = System.Drawing.Color.Black;
				lblTitle3.ForeColor = System.Drawing.Color.Orange;
				pnlCreditOff.Visible=true;
				pnlCreditOn.Visible=false;

				cbAgreement.Visible = true;
				txtPO.Enabled = true;
			}

			string strSql=string.Empty ;
			double SizeVal=0;
			string[] ArrProd= new string[0] ;
			try
			{
				ArrProd = Session.Contents["ProductID"].ToString().Split(new Char []{'^'}) ;
				ViewState["ID"]=ArrProd[1].ToString();
			}
			catch  
			{
				Response.Redirect("Spot_Floor.aspx");
			}
				
			if( ((Session["confirmation"] != null) && (Session["confirmation"].ToString() == "yes")) || (!Session["UType"].ToString().Equals("D")) )
			{
				lblTitle2.ForeColor = System.Drawing.Color.Black;
				lblTitle3.ForeColor = System.Drawing.Color.Orange;
				cbAgreement.Visible = true;
				txtPO.Enabled = true;
			}
			else
			{
				lblTitle2.ForeColor = System.Drawing.Color.Orange;
				lblTitle3.ForeColor = System.Drawing.Color.Black;
				cbAgreement.Visible = false;
				txtPO.Enabled = false;
			}
			
			/// check here that page is post back or not if not than make the connection and execute the command
			if (!IsPostBack)
			{

				if(txtPO.Text != "")
				{
					lblPOMessage.Visible = false;
				}
				

				btnFaxCredit.Attributes.Add("onclick","OpenPdf()");
				SqlConnection conn= new SqlConnection(Application["DBConn"].ToString());
				conn.Open();

				/// offer read that passed value/parameter from spot_floor.aspx page/dashboard.aspx page				
				SqlDataReader dtrOffer;

				Hashtable param = new Hashtable();
				param.Add("@OFFR_ID", ViewState["ID"].ToString());

				StringBuilder sbSQL = new StringBuilder();			
				sbSQL.Append("		SELECT *, ");
				sbSQL.Append("		VARTERM=(CASE WHEN OFFR_TERM= 'FOB/Delivered' THEN 'FOB Delivered' WHEN  ");
				sbSQL.Append("		OFFR_TERM= 'FOB/Shipping' THEN 'FOB '  END ), ");
				sbSQL.Append("		VARSIZE=CAST(OFFR_QTY AS VARCHAR)+' '+(CASE OFFR_SIZE WHEN 1 THEN ");
				sbSQL.Append("		'lb' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 42000 ");
				sbSQL.Append("		THEN 'T/L - Boxes' WHEN 45000 THEN 'Bulk Truck' WHEN 44092 THEN 'T/L Bags' ");
				sbSQL.Append("		WHEN 190000 THEN 'Rail Car'	END)+(CASE WHEN (OFFR_QTY>1 AND OFFR_SIZE <> 42000 AND OFFR_SIZE <> 44092 ) THEN 's' ELSE ' ' END), ");
				sbSQL.Append("		VARVALUE=(OFFR_PRCE*OFFR_SIZE*OFFR_QTY) ");
				
				sbSQL.Append("		FROM BBOFFER ");
				sbSQL.Append("		WHERE  ");
				sbSQL.Append("		OFFR_ID= @OFFR_ID");
				
				
				dtrOffer = DBLibrary.GetDataReaderFromSelect(conn,sbSQL.ToString(),param);

				string Terms = "";
				string Size = "";
				string zipCodeOrigin = "";
				string zipCodeDestination = "";
				int	quantity = 0;
				if (dtrOffer.Read())
				{
					// offer details are displayed here in different label that parameter recd. from spot_floor.aspx page/dashboard.aspx page 
					lblID.Text = dtrOffer["OFFR_ID"].ToString();
					lblProduct.Text = dtrOffer["OFFR_PROD"].ToString();
					SizeVal = Convert.ToDouble(dtrOffer["OFFR_SIZE"]);
					lblMelt.Text= dtrOffer["OFFR_MELT"].ToString();
					lblDens.Text= dtrOffer["OFFR_DENS"].ToString();
					lblAdds.Text= dtrOffer["OFFR_ADDS"].ToString();
					if(lblAdds.Text == "")
					{
						lblAdds.Text = "N/A";
					}
					Terms = dtrOffer["OFFR_TERM"].ToString();
					Size = dtrOffer["OFFR_SIZE"].ToString();
					zipCodeOrigin = dtrOffer["OFFR_ZIP"].ToString();

					
					if((Session["RC_changed"] != null) && (Session["RC_changed"].ToString() == "true"))
					{
						lblPrice.Text="$" + Session["price"].ToString();
						quantity = Convert.ToInt32(Session["qty"].ToString());
						lblSize.Text = quantity + " " + "Rail Car(s)";
						price = Math.Round( Convert.ToDouble(Convert.ToDouble(Session["price"].ToString()) * quantity * 190000.0),3) ;
					}
					else
					{
						lblPrice.Text="$" + Convert.ToString(Math.Round(Convert.ToDecimal(dtrOffer["OFFR_PRCE"].ToString()),4));
						quantity = Convert.ToInt32(dtrOffer["OFFR_QTY"].ToString());
						lblSize.Text = dtrOffer["VARSIZE"].ToString();
						price = Math.Round( Convert.ToDouble(dtrOffer["VARVALUE"].ToString()),3) ;
					}

					if(HelperFunction.returnTaxByPlace(Session.Contents["ShipId"].ToString(), this.Context) != 0)
					{
						lblTax.Text = "This transaction is subject to " + HelperFunction.returnTaxName(Session.Contents["ShipId"].ToString(), this.Context);
					}

					// Show Shipping Address
					zipCodeDestination = FillShipAddress(Session.Contents["ShipId"].ToString());
					
					TotPrice.Text=String.Format("{0:c}", price );
					ViewState["Firm"] = dtrOffer["OFFR_FIRM"].ToString();
					ViewState["Origin"] = dtrOffer["OFFR_FROM_LOCL"].ToString();   
					ViewState["Expiration"] = dtrOffer["OFFR_EXPR"].ToString();
					ViewState["TERMS"] = dtrOffer["OFFR_PAY"].ToString();
					ViewState["TYPE"] = "Offer";
					dtrOffer.Close();
				}
				else
				{
					dtrOffer.Close();
					/// Bid read that passed value/parameter from spot_floor.aspx page

					Hashtable p = new Hashtable();
					param.Add("@BID_ID", ViewState["ID"].ToString());

					SqlDataReader dtrBid;
					sbSQL.Remove(0,sbSQL.Length);
					sbSQL.Append("		SELECT *, ");
					sbSQL.Append("		VARTERM=(CASE WHEN BID_TERM= 'FOB/Delivered' THEN 'FOB Delivered' WHEN  ");
					sbSQL.Append("		BID_TERM= 'FOB/Shipping' THEN 'FOB '  END ), ");
					sbSQL.Append("		VARSIZE=CAST(BID_QTY AS VARCHAR)+' '+(CASE BID_SIZE WHEN 1 THEN ");
					sbSQL.Append("		'lb' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 42000 ");
					sbSQL.Append("		THEN 'T/L - Boxes' WHEN 45000 THEN 'Bulk Truck' WHEN 44092 THEN 'T/L Bags' ");
					sbSQL.Append("		WHEN 190000 THEN 'Rail Car'	END)+(CASE WHEN (BID_QTY>1 AND BID_SIZE <> 42000 AND BID_SIZE <> 44092 ) THEN 's' ELSE ' ' END), ");
					sbSQL.Append("		VARVALUE=(BID_PRCE*BID_SIZE*BID_QTY) ");
					sbSQL.Append("		FROM BBID ");
					sbSQL.Append("		WHERE  ");
					sbSQL.Append("		BID_ID = @BID_ID");
					
					dtrBid = DBLibrary.GetDataReaderFromSelect(conn,sbSQL.ToString(),p);

					if (dtrBid.Read())
					{
						/// Bid details are displayed here in different label that parameter recd. from spot_floor.aspx page 
						lblID.Text = dtrBid["BID_ID"].ToString();
						lblProduct.Text = dtrBid["BID_PROD"].ToString();
						lblMelt.Text = dtrBid["BID_MELT"].ToString();
						lblDens.Text= dtrBid["BID_DENS"].ToString();
						lblAdds.Text= dtrBid["BID_ADDS"].ToString();
						Terms = dtrBid["BID_TERM"].ToString();
						Size = dtrBid["BID_SIZE"].ToString();
						zipCodeDestination = dtrOffer["BID_ZIP"].ToString();

						if((Session["RC_changed"] != null) && (Session["RC_changed"].ToString() == "true"))
						{
							lblPrice.Text="$" + Session["price"].ToString();
							quantity = Convert.ToInt32(Session["qty"].ToString());
							lblSize.Text = quantity + " " + "Rail Car(s)";
							price = Math.Round( Convert.ToDouble(Convert.ToDouble(Session["price"].ToString()) * quantity * 190000.0),3) ;
						}
						else
						{
							quantity = Convert.ToInt32(dtrOffer["BID_QTY"].ToString());
							lblPrice.Text= "$" + Convert.ToString(Math.Round(Convert.ToDecimal(dtrBid["BID_PRCE"].ToString()),4));
							lblSize.Text = dtrBid["VARSIZE"].ToString();
							price = Math.Round( Convert.ToDouble(dtrBid["VARVALUE"].ToString()),3) ;
						}
							// Show Shipping Address
						zipCodeOrigin = FillShipAddress(Session.Contents["ShipId"].ToString());

						TotPrice.Text=String.Format("{0:c}", price );
						ViewState["Firm"] = dtrBid["BID_FIRM"].ToString();
						ViewState["Origin"] = dtrBid["BID_TO_LOCL"].ToString();
						ViewState["Expiration"] = dtrBid["BID_EXPR"].ToString();
						ViewState["TERMS"] = dtrBid["BID_PAY"].ToString();
						ViewState["TYPE"] = "Bid";
						dtrBid.Close();
					}
					else
					{
						// output error
					}
					dtrBid.Close();
				}
				conn.Close();

				bool freightAvailable = false;
				string placeFrom = "";
				string placeTo = "";
				decimal freight = 0;
				HelperFunctionDistances hfd = new HelperFunctionDistances();
				if (Size=="45000")
				{	
					freightAvailable = hfd.calculateFreightByZipCode(zipCodeOrigin,zipCodeDestination, ref placeFrom, ref placeTo, ref freight, TruckType.BulkTruck, Application["DBconn"].ToString());
				}
				else if ((Size=="42000") || (Size=="44092"))
				{
					freightAvailable = hfd.calculateFreightByZipCode(zipCodeOrigin,zipCodeDestination, ref placeFrom, ref placeTo, ref freight, TruckType.TruckLoad, Application["DBconn"].ToString());
				}
				else if (Size=="190000")
				{
					lblFreight.Text = "included";
				}
				else if (Size=="1")
				{
					freightAvailable = hfd.calculateFreightByZipCode(zipCodeOrigin,zipCodeDestination, ref placeFrom, ref placeTo, ref freight, TruckType.TruckLoad, Application["DBconn"].ToString());
				}
				else
				{
					lblFreight.Text = "Not Applicable";
				}
						
				if (freightAvailable)
				{
					if (Size=="1")
					{
						int lbs = quantity;
						double truck =  42000;
						int num_of_trucks = (int)Math.Ceiling (lbs / truck); 
						lblFreight.Text = string.Format("{0:c}", num_of_trucks * Math.Round(freight / lbs ,4));
					}
					else
					{
						lblFreight.Text = string.Format("{0:c}",Math.Round(freight * quantity,2));
					}

					Terms = "Delivered " + placeTo;
				}
				else
				{
					lblFreight.Text = "Not Applicable";
				}
				lblTerms.Text = Terms;
				Session["freight"] = freight;

				//lblFreight.Text= string.Format("{0:c}",freight);
				total = Math.Round((price + Convert.ToDouble(freight * quantity)),2) ;
				Session["PayTotal"]= total.ToString();
				lblTotal.Text=String.Format("{0:c}", total);

				// Show Billing Address
				if (Session.Contents["BillId"]== null)
				{
					FillBillAddress(Session.Contents["ShipId"].ToString());
					Session.Contents["BillId"]=Session.Contents["ShipId"].ToString();
				}
				else
				{
					FillBillAddress(Session.Contents["BillId"].ToString());
				}
			}			
		}

		//Return ZipCode
		private string FillShipAddress(string TmpSId)
		{
			string zipCode = "";
			
			//Function for getting the shippingto address from ShippingDetails.aspx displayed in this page
			SqlConnection conn;
			string ShipAdd = String.Empty;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();

			Hashtable param = new Hashtable();
			param.Add("@PLAC_ID", TmpSId);

			SqlDataReader DR;
			
			StringBuilder SbSql = new StringBuilder();
			SbSql.Append("SELECT DISTINCT PLACE.PLAC_ID, PLACE.PLAC_PERS,");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ADDR_ONE is not Null or ltrim(rtrim(PLACE.PLAC_ADDR_ONE))<>'' ");
			SbSql.Append("then  PLACE.PLAC_ADDR_ONE else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CITY is not Null or ltrim(rtrim( PLACE.PLAC_CITY))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CITY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_STAT is not Null or ltrim(rtrim( PLACE.PLAC_STAT))<>'' ");
			SbSql.Append("then + PLACE.PLAC_STAT else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CTRY  is not Null or ltrim(rtrim( PLACE.PLAC_CTRY ))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CTRY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ZIP is not Null or ltrim(rtrim(PLACE.PLAC_ZIP))<>'' ");
			SbSql.Append("then PLACE.PLAC_ZIP else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_RAIL_NUM is not Null or ltrim(rtrim(PLACE.PLAC_RAIL_NUM))<>'' ");
			SbSql.Append("then  PLACE.PLAC_RAIL_NUM else '' end  ");
			SbSql.Append("FROM PLACE ");
			SbSql.Append("WHERE PLACE.PLAC_ID = @PLAC_ID");
						
			DR = DBLibrary.GetDataReaderFromSelect(conn, SbSql.ToString(), param);

			if (DR.Read())
			{
				ShipAdd+=DR.GetValue(1).ToString()+ "<br>" ;
				ShipAdd+=DR.GetValue(2).ToString()+ "<br>" ;
				ShipAdd+=DR.GetValue(3).ToString()+ "<br>" ;
				ShipAdd+=DR.GetValue(4).ToString()+ "<br>" ;
				ShipAdd+=DR.GetValue(5).ToString()+ "<br>" ;
				ShipAdd+=DR.GetValue(6).ToString()+ "<br>" ;
				ShipAdd+=DR.GetValue(7).ToString()  ;
				zipCode = HelperFunction.getSafeStringFromDB(DR.GetValue(6));
			}
			DR.Close();
			conn.Close();
			//shows the shipto address in label
			LblShip.Text=ShipAdd;

			return zipCode;
		}



		private void FillBillAddress(string TmpBId)
		{
			//Function for get the Billto address from ShippingDetails.aspx displayed in this page
			SqlConnection conn;
			string BillAdd = String.Empty;
			conn = new SqlConnection(Application["DBconn"].ToString());
			conn.Open();
			SqlDataReader DR;

			Hashtable param = new Hashtable();
			param.Add("@PLAC_ID", TmpBId);

			StringBuilder SbSql = new StringBuilder();
			SbSql.Append("SELECT DISTINCT PLACE.PLAC_ID, PLACE.PLAC_PERS,");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ADDR_ONE is not Null or ltrim(rtrim(PLACE.PLAC_ADDR_ONE))<>'' ");
			SbSql.Append("then  PLACE.PLAC_ADDR_ONE else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CITY is not Null or ltrim(rtrim( PLACE.PLAC_CITY))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CITY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_STAT is not Null or ltrim(rtrim( PLACE.PLAC_STAT))<>'' ");
			SbSql.Append("then + PLACE.PLAC_STAT else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_CTRY  is not Null or ltrim(rtrim( PLACE.PLAC_CTRY ))<>'' ");
			SbSql.Append("then  PLACE.PLAC_CTRY else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_ZIP is not Null or ltrim(rtrim(PLACE.PLAC_ZIP))<>'' ");
			SbSql.Append("then PLACE.PLAC_ZIP else '' end, ");
			SbSql.Append("CASE ");
			SbSql.Append("when PLACE.PLAC_RAIL_NUM is not Null or ltrim(rtrim(PLACE.PLAC_RAIL_NUM))<>'' ");
			SbSql.Append("then  PLACE.PLAC_RAIL_NUM else '' end  ");
			SbSql.Append("FROM PLACE ");
			SbSql.Append("WHERE PLACE.PLAC_ID = @PLAC_ID");

			DR = DBLibrary.GetDataReaderFromSelect(conn, SbSql.ToString(), param);

			if (DR.Read())
			{
				BillAdd+=DR.GetValue(1).ToString()+"<br>" ;
				BillAdd+=DR.GetValue(2).ToString()+"<br>" ;
				BillAdd+=DR.GetValue(3).ToString()+"<br>" ;
				BillAdd+=DR.GetValue(4).ToString()+"<br>" ;
				BillAdd+=DR.GetValue(5).ToString()+"<br>" ;
				BillAdd+=DR.GetValue(6).ToString()+"<br>" ;
				BillAdd+=DR.GetValue(7).ToString()  ;
			}
			DR.Close();
			conn.Close();
			//shows the billto address in label
			LblBill.Text=BillAdd;

			
		}

		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	
		protected void btnGoBack_Click(object sender, System.EventArgs e)
		{
			Session["BackShipDetails"] = "yes";
			Response.Redirect("ShipDetails.aspx");
		}

		protected void btnContinue_Click(object sender, System.EventArgs e)
		{
			bool isOK = true;
			if(txtPO.Text.Trim() == "")
			{
				lblPOMessage.Visible = true;
				pnlCreditOff.Visible=true;
				pnlCreditOn.Visible=false;
				isOK = false;
			}
			else
			{
				lblPOMessage.Visible = false;
			}
	
			if(!cbAgreement.Checked )
			{
				lblConfirmError.Visible = true;
				pnlCreditOff.Visible=true;
				pnlCreditOn.Visible=false;
				isOK = false;
			}
			else
			{
				lblConfirmError.Visible = false;
			}

			if(isOK)
			{
				lblConfirmError.Visible = false;

				SendData();	
			}
		}
		private void SendData()
		{
			InsertData();
			DoMail();
			
			Session.Remove("ProductID");
			Session.Remove("ShipFrom");
			Session.Remove("ShipId");
			Session.Remove("BillId");
			Session.Remove("FrmName");
			Session.Remove("BackShipDetails");
			Session.Remove("confirmation");

			Response.Redirect("ThankYou.aspx"); 
			//Below commented code is previously working  code

			//Session["FrmName"]="Total";
			//Response.Redirect("OrderConfirmation.aspx");
		}
		private void InsertData()
		{
			string strCredit=string.Empty;
			string BID_OFFR=string.Empty;	
						
			//here we check that we are coming after filling the credit application or not. 
			//if (TmpCache=="Credit")
			if (Session["FrmName"]!=null)
			{
				Hashtable ht = (Hashtable)Session["ParamQuery"];
				strCredit = DBLibrary.ExecuteScalarStoredProcedure(Application["DBconn"].ToString(), "spShowSaveCredit", ht).ToString();
			}
			else
			{
				strCredit="0";
			}
			
			string [] ArrProd = Session.Contents["ProductID"].ToString().Split(new Char []{'^'}) ;		
			ViewState["ID"]=ArrProd[1].ToString();
			BID_OFFR =ArrProd[0].ToString();

			Hashtable param = new Hashtable();
			param.Add("@PERSON_ID", Session["ID"].ToString() );
			param.Add("@ORDR_DATE", DateTime.Today);
			param.Add("@ORDR_PROD_SPOT", lblProduct.Text.ToString());
			param.Add("@ORDR_SIZE", lblSize.Text.ToString());
			param.Add("@ORDR_MELT", lblMelt.Text.ToString());
			param.Add("@ORDR_DENS", lblDens.Text.ToString());
			param.Add("@ORDR_ADDS", lblAdds.Text.ToString());
			param.Add("@ORDR_Terms", lblTerms.Text.ToString());				
			param.Add("@CREDIT_ID", strCredit.ToString());				
			param.Add("@ORDR_FREIGHT", Session["freight"].ToString());
			param.Add("@ORDR_TOTAL", Session["PayTotal"].ToString());
			param.Add("@ORDR_SHIPTO", Session.Contents["ShipId"]);
			param.Add("@ORDR_BILTO", Session.Contents["BillId"]);
			param.Add("@ORDR_PO_NUM", HelperFunction.RemoveSqlEscapeChars(txtPO.Text));

			string sqlStatement;
			//here we check that in table entry is for bid or offer 
			if (BID_OFFR=="B")
			{				
				param.Add("@ORDR_BID", ViewState["ID"].ToString());
							
				sqlStatement = "Insert into Pending_orders(PERSON_ID,ORDR_BID,ORDR_DATE,ORDR_PROD_SPOT,ORDR_SIZE,ORDR_MELT,ORDR_DENS,ORDR_ADDS,ORDR_Terms,ORDR_ISSPOT,CREDIT_ID,ORDR_STAT,ORDR_FREIGHT,ORDR_TOTAL,ORDR_SHIPTO,ORDR_BILTO, ORDR_PO_NUM )" ;
				sqlStatement +="values (@PERSON_ID, @ORDR_BID, @ORDR_DATE,@ORDR_PROD_SPOT,@ORDR_SIZE,@ORDR_MELT,@ORDR_DENS,@ORDR_ADDS,@ORDR_Terms,0,@CREDIT_ID,'P',@ORDR_FREIGHT,@ORDR_TOTAL,@ORDR_SHIPTO,@ORDR_BILTO,@ORDR_PO_NUM)" ; //Session.Contents["ShipFrom"] exchanged with System.DbNull
			}
			else
			{				
				param.Add("@ORDR_OFFR", ViewState["ID"].ToString());
							
				sqlStatement = "Insert into Pending_orders(PERSON_ID,ORDR_OFFR,ORDR_DATE,ORDR_PROD_SPOT,ORDR_SIZE,ORDR_MELT,ORDR_DENS,ORDR_ADDS,ORDR_TERMS,ORDR_ISSPOT,CREDIT_ID,ORDR_STAT,ORDR_FREIGHT,ORDR_TOTAL,ORDR_SHIPTO,ORDR_BILTO, ORDR_PO_NUM  )" ;
				sqlStatement +="values (@PERSON_ID, @ORDR_OFFR, @ORDR_DATE,@ORDR_PROD_SPOT,@ORDR_SIZE,@ORDR_MELT,@ORDR_DENS,@ORDR_ADDS,@ORDR_Terms,0,@CREDIT_ID,'P',@ORDR_FREIGHT,@ORDR_TOTAL,@ORDR_SHIPTO,@ORDR_BILTO,@ORDR_PO_NUM)" ; //Session.Contents["ShipFrom"] exchanged with System.DbNull

			}
			
			DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), sqlStatement, param);
			
		}
		private void DoMail()
		{
			StringBuilder sbHTML =  new StringBuilder() ;
			sbHTML.Append("<Table><TR><TD><h3>Order Confirmation for following Product </h3></TD></TR>");
			sbHTML.Append("<tr><td colspan='2'><font align='left' size='4'><b><U>Customer Information</U></b></font></td></tr>");
			
			string userID = "";
			//some users doesn't have logins instead we should use their emails
			if(Session["UserName"] != null)
			{
				userID = Session["UserName"].ToString();
			}
			else
			{
				userID = Session["Email"].ToString();
			}
			sbHTML.Append("<tr><td align='left'><font size='3'><b>ID: </b></font></td><td>"+ userID +"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Name: </b></font></td><td>"+Session["Name"].ToString()+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Email: </b></font></td><td>"+Session["Email"].ToString()+"</td></tr>");

			sbHTML.Append("<tr></tr>");
			sbHTML.Append("<tr><td colspan='2'><font align='left' size='4'><b><U>Product Specifications</U></b></font></td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>ID#: </b></font></td><td>"+lblID.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Product: </b></font></td><td>"+lblProduct.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Size: </b></font></td><td>"+lblSize.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Adds: </b></font></td><td>"+lblAdds.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Melt: </b></fontsize></td><td>"+lblMelt.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Density: </b></font></td><td>"+lblDens.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Terms: </b></font></td><td>"+lblTerms.Text+"</td></tr>");
			sbHTML.Append("<tr><td align='left'><font size='3'><b>Price: </b></font></td><td>"+lblPrice.Text+"</td></tr>");
			sbHTML.Append("<tr></tr>");
			sbHTML.Append("<tr></tr>");
			
			sbHTML.Append("<tr><td align='left' colspan='2'><font size='3'><b>An Order for the above product has been placed by the customer. </b></font></td></tr></Table>");

			MailMessage EmailOrdr = new MailMessage();
			EmailOrdr.From = "Info@theplasticsexchange.com";
			EmailOrdr.To = Application["strEmailAdmin"].ToString();
			EmailOrdr.Cc = Application["strEmailOwner"].ToString();
			EmailOrdr.Subject = "Customer confirmed the Order";
			EmailOrdr.Body = sbHTML.ToString();
			EmailOrdr.BodyFormat = MailFormat.Html;
			//SmtpMail.SmtpServer="caesar";
			TPE.Utility.EmailLibrary.Send(Context,EmailOrdr);
		}
		protected void btnFaxOnline_Click(object sender, System.EventArgs e)
		{
			
			Session["confirmation"] = "yes";

			//Session["FrmName"]="Total";
			Response.Redirect("CreditApplication.aspx");

			//**********************************************/
			//			Mailing Code  Below					/
			//			mail is sent to the admin/User that /
			//			user filled							/
			//			the  credit application through		/
			//			online credit app.					/
			//**********************************************/

			StringBuilder sbHTML= new StringBuilder();
			sbHTML.Append("<align='left'><font size='3'><b>Credit Application:</b></font><br>");
			sbHTML.Append("<align='left'>Thank you for your credit application which we are currently processing.<br>");
			System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
			mail.From=Application["strEmailOwner"].ToString();
			mail.To = Session["Email"].ToString();//"user@theplasticsexchange.com";//////shold be change after given new emailid of query
			mail.Cc= Application["strEmailAdmin"].ToString();
			mail.Body=sbHTML.ToString();
			mail.Subject="Credit Application ";
			mail.BodyFormat = MailFormat.Html;
			//SmtpMail.SmtpServer="localhost";
			TPE.Utility.EmailLibrary.Send(Context,mail);

			cbAgreement.Visible = true;

		}

		protected void btnFaxCredit_Click(object sender, System.EventArgs e)
		{
			Session["confirmation"] = "yes";
			//***********************************/
			//		 Mailing Code  Below         /
			// mail is sent to the admin that    /
			// what is the total price of that   /
			// particular user's selected offer  /
			// admin is also know that user sends/ 
			// the  credit application through   /
			// fax app                           /
			// and one mail is also send to the  /
			// user with the pdf attachment file /
			//***********************************/
									
			StringBuilder sbHTML= new StringBuilder();
			sbHTML.Append("<align='left'><font size='3'><b>Credit Application:</b></font><br>");
			sbHTML.Append("<align='left'>A user filing credit Application by fax.<br>");
			System.Web.Mail.MailMessage mail = new System.Web.Mail.MailMessage();
			mail.From=Application["strEmailOwner"].ToString();
			//mail.To ="bbhushan@hanusoftware.com";
			mail.To =Session["Email"].ToString();	//"user@theplasticsexchange.com";//////shold be change after given new emailid of query
			mail.Cc= Application["strEmailAdmin"].ToString();
			mail.Body=sbHTML.ToString();
			mail.Subject="Credit Application.pdf file is attached with this mail. You must fill out and fax in order to proceed with the transaction.";
			mail.BodyFormat = MailFormat.Html;
			
			MailAttachment mAttch = new MailAttachment(Server.MapPath("/docs/credit_App.pdf"),MailEncoding.UUEncode)  ;
			mail.Attachments.Add(mAttch); 

			TPE.Utility.EmailLibrary.Send(Context,mail);
			
			cbAgreement.Visible = true;
			txtPO.Enabled = true;

			pnlCreditOff.Visible=true;
			pnlCreditOn.Visible=false;


			
			lblTitle2.ForeColor = System.Drawing.Color.Black;
			lblTitle3.ForeColor = System.Drawing.Color.Orange;


			// WORKING

			/* string popupScript = "<script language='javascript'>" +
				"window.open('credit-app_pdf2-lasalle.pdf', 'CustomPopUp') " +
				"</script>";

			Page.RegisterStartupScript("PopupScript", popupScript);
 */
			
			//Session["FrmName"]="Total";	
			//Response.Redirect("OrderConfirmation.aspx");

			
		}

		

	

		
	}
}
