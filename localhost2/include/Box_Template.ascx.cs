namespace localhost.include
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for newBox_Template.
	/// </summary>
	public partial class newBox_Template : System.Web.UI.UserControl
	{

		public bool Footer = false;
		public string Heading ="";
		public string Align = "center";
		public string Width = "100%";
		protected System.Web.UI.WebControls.Panel pnHeader;
		public string bgColor = "#000000"; //"#F1F1F1";
				
	protected void Page_Load(object sender, System.EventArgs e)
	{

	}

	// Insert user control code here
	//

		
	#region Web Form Designer generated code
	override protected void OnInit(EventArgs e)
{
	//
	// CODEGEN: This call is required by the ASP.NET Web Form Designer.
	//
	InitializeComponent();
	base.OnInit(e);
}
		
	/// <summary>
	///		Required method for Designer support - do not modify
	///		the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent()
{			

	}
	#endregion


	}
}
