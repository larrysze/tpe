<%@ Control Language="c#" %>
<%@ Register TagPrefix="mbdb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.DefaultButtons" %>
<%@ import Namespace="System.Web.Mail" %>
<%@ import Namespace="System.Web" %>
<%@ import Namespace="localhost" %>
<script runat="server">

    public string PageTitle = "The Plastics Exchange: Commodity Plastic Resin, Buy Resin, Sell Resin, Plastic Materials";
    public bool Footer = false;
    public bool Margins = true;
    public bool HomePage = false;
    public bool ShowLeftMargin = true;
    private void Page_Load(object sender, EventArgs e){
		if (Request.UserHostAddress.ToString() =="216.70.155.52")
		//if (Session["Typ"].ToString() =="A" || Session["Id"] =="B" || Session["Id"] =="T" )
		{
			LivePerson.Visible = false;
			
			
		}
		lbLivePerson.Text = HelperFunction.getLivePersonButton();
		LivePerson.Text ="<script language=\"JavaScript1.2\"> "; 
		LivePerson.Text +="if (typeof(tagVars) == \"undefined\"){ tagVars = \"\"; ";
		LivePerson.Text +="tagVars += \"&VISITORVAR!User%20Name%20=Not%20Logged%20On&VISITORVAR!Account=1"+"\"; }"  ;
		//LivePerson.Text +="tagVars += \"&VISITORVAR!User%20Name%20=Not%20Logged%20On\"; }"  ;
		// workaround for M$ bug 
		// more info at -- http://support.microsoft.com/?kbid=827420
		LivePerson.Text += "<" + "/SCRIPT>";
		LivePerson.Text +="<script language='javascript' src='https://server.iad.liveperson.net/hc/92199096/x.js?cmd=file&file=chatScript3&site=92199096&&category=en;woman;5'>";
		LivePerson.Text += "<" + "/SCRIPT>";

    
        if (Session["Id"] == null || HomePage ){
        
            pnDefault.Visible = true;

            pnDemoNav.Visible = false;
            pnBrokerNav.Visible = false;
            pnPurchaserNav.Visible = false;
            pnDistributorNav.Visible = false;
        }else{
            pnTopNavRegister.Visible = false;
            pnLoginSubmitEmail.Visible = false;
            pnDistributorNav.Visible = false;
            pnDefault.Visible = false;
            pnSupportCenter.Visible = false;
            pnRFQ.Visible = false;
            
            switch (Session["Typ"].ToString()){
                case "A":
                    pnAdminMenu.Visible = true;
                    // add in right click menu
                    pnContextMenu.Visible = true;
                    break;
                case "B":		
                    wbContextMenu.Visible = true;
                    pnBrokerNav.Visible = true;
                    break;
                case "T":
				    // pnAdminMenu.Visible = true;
                    // add in right click menu
                    wbContextMenu.Visible = true;
                    pnBrokerNav.Visible = true;
                    break;
                case "P":
                    // add in right click menu
                    wbContextMenu.Visible = true;
                    pnPurchaserNav.Visible = true;
                    break;
                case "S":
                   wbContextMenu.Visible = true;
					pnSellerNav.Visible = false;
                    break;
                case "D":
				    // pnAdminMenu.Visible = true;
                    // add in right click menu
                    wbContextMenu.Visible = true;
                    pnDistributorNav.Visible = true;
                    break;
               
                case "Demo":
				    //  pnAdminMenu.Visible = true;
                    // add in right click menu
                    wbContextMenu.Visible = true;
					pnPurchaserNav.Visible = true;
                     break;
    
                default:
				  //   pnAdminMenu.Visible = true;
                    // add in right click menu
                    wbContextMenu.Visible = true;          
                    pnDemoNav.Visible = true;
                   
                    break;
            }
    
    
    
        }
       
        // home page has its own unigue set of panels
        if (HomePage && Session["Id"] != null){
            pnLoginSubmitEmail.Visible = false;
            pnRFQ.Visible = false;
    
            switch (Session["Typ"].ToString()){
                case "A":
                    //pnDefault.Visible = true;
                    pnAdminMenu.Visible = true;
					pnContextMenu.Visible = true;  
                    break;
                case "B":
                    pnBrokerNav.Visible = true;
					wbContextMenu.Visible = true;  
                    break;
                case "T":
                    pnBrokerNav.Visible = true;
                    break;
                case "P":
				    wbContextMenu.Visible = true;  
                    pnPurchaserNav.Visible = true;
                    break;
                case "S":
                    pnSellerNav.Visible = true;
                    break;
                case "D":
				    wbContextMenu.Visible = true;  
                    pnDistributorNav.Visible = true;
                    break;
                default:
				    wbContextMenu.Visible = true;  
                    pnPurchaserNav.Visible = true;
                    break;
    
    
            }
            
    
        }
    
    
        // swap the files in the event that it is a header or a footer
        if (Footer){
            pnHeader.Visible = false;
            pnFooter.Visible = true;
        }else{
            pnHeader.Visible = true;
            pnFooter.Visible = false;
        }
        if (Session["Id"] != null){
            lblUser.Text = Session["Name"].ToString();
            //
            LivePerson.Text ="<script language=\"JavaScript1.2\"> "; 
			LivePerson.Text +="if (typeof(tagVars) == \"undefined\"){ tagVars = \"\"; ";
			LivePerson.Text +="tagVars += \"&VISITORVAR!User%20Name%20="+Session["Name"].ToString()+ "&VISITORVAR!Account="+Session["ID"].ToString()+"\"; }"  ;
			// workaround for M$ bug 
			// more info at -- http://support.microsoft.com/?kbid=827420
			LivePerson.Text += "<" + "/SCRIPT>";
			LivePerson.Text +="<script language='javascript' src='https://server.iad.liveperson.net/hc/92199096/x.js?cmd=file&file=chatScript3&site=92199096&&category=en;woman;5'>";
			LivePerson.Text += "<" + "/SCRIPT>";
            
            if(Session["Typ"].ToString() == "A"){
                // admins should see the number of users online
                lblUser.Text  += "<BR><a href='/Administrator/WebTrends.aspx'>Current Users: " + Application["iUsersOnline"].ToString()+"</a>";
            }
            pnSignInSignUp.Visible = false;
            hlLogout.Visible = true;
    
               if ((Session["Typ"].ToString() == "A") || (Session["Typ"].ToString() == "B") && (Session["Typ"].ToString() == "T"))
               {
                imgFax.Visible = HelperFunction.ExistFax(Application["DBConn"].ToString(), Convert.ToInt16(Session["ID"]));
            }
            else
            {
                   imgFax.Visible = false;
            }
    
        }
        else
        {
               imgFax.Visible = false;
        }
    
        if(Margins){
            pnHeaderMargin.Visible = true;
            pnFooterMargin.Visible = true;
        }else{
            pnHeaderMargin.Visible = false;
            pnNoMargin.Visible = true;
            pnFooterMargin.Visible = false;
        }
    
        //f (!ShowLeftMargin){
        //    pnAdminNav.Visible = true;
        //    pnDefault.Visible = false;
        //}
    
    
    
    }
    
    private void ClickRFQ(object sender, ImageClickEventArgs e ){
        // Saving information into session object to be passed on to the login function
        Response.Redirect("/Public/Submit_RFQ.aspx?Resin="+txtNavResin.Text+"&Qty="+txtNavQty.Text+"&ShipTo="+ txtNavShipTo.Text+"&Price="+ txtNavPrice.Text  +"&Name="+txtNavName.Text +"&Email="+ txtNavEmail.Text + "&Phone="+txtNavPhone.Text);
    }
    private void ClickLogin(object sender, ImageClickEventArgs e ){
        // Saving information into session object to be passed on to the login function
        Session["UserName"] = UserName.Text;
        Session["Password"] = Password.Text;
        Response.Redirect("/Common/FUNCLogin.aspx");
    }
    
    private void ClickSubmitEmail(object sender, ImageClickEventArgs e ){
         if (txtEmail.Text !=""){
            Response.Redirect("/Public/Registration.aspx?Email="+ txtEmail.Text );
         }else{
            Response.Redirect("/Public/Registration.aspx" );
         }
    
    }

</script>
<!-- Insert content here -->
<asp:Panel id="pnHeader" runat="server">
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"  "http://www.w3.org/TR/html4/strict.dtd">
	<html>
		<head>
		<!-- START OF SmartSource Data Collector TAG -->
<!-- � 1999-2003 NetIQ Corporation. All rights reserved. -->
<!-- V6.1 -->
<!-- $DateTime: 2003/09/30 11:23:29 $ -->
<SCRIPT LANGUAGE="Javascript"><!--
gVersion="1.0";
//-->
</SCRIPT>
<SCRIPT LANGUAGE="Javascript1.1"><!--
gVersion="1.1";
//-->
</SCRIPT>
<SCRIPT LANGUAGE="Javascript1.2"><!--
gVersion="1.2";
var RE={"%09":/\t/g, "%20":/ /g, "%23":/\#/g,"%26":/\&/g,"%2B":/\+/g,"%3F":/\?/g,"%5C":/\\/g};
//-->
</SCRIPT>
<SCRIPT LANGUAGE="Javascript1.3"><!--
gVersion="1.3";
//-->
</SCRIPT>
<SCRIPT LANGUAGE="Javascript1.4"><!--
gVersion="1.4";
//-->
</SCRIPT>
<SCRIPT LANGUAGE="Javascript1.5"><!--
gVersion="1.5";
//-->
</SCRIPT>
<SCRIPT LANGUAGE="JavaScript"><!--
var gImages=new Array;
var gIndex=0;
var DCS=new Object();
var WT=new Object();
var DCSext=new Object();

var gDomain="statse.webtrendslive.com";
var gDcsId="dcs6fcuwwhr53duyc6hw598cb_3h8c";

function dcsVar(){
	var dCurrent=new Date();
	WT.tz=dCurrent.getTimezoneOffset()/60*-1;
	if (WT.tz==0){
		WT.tz="0";
	}
	WT.bh=dCurrent.getHours();
	WT.ul=navigator.appName=="Netscape"?navigator.language:navigator.userLanguage;
	if (typeof(screen)=="object"){
		WT.cd=navigator.appName=="Netscape"?screen.pixelDepth:screen.colorDepth;
		WT.sr=screen.width+"x"+screen.height;
	}
	if (typeof(navigator.javaEnabled())=="boolean"){
		WT.jo=navigator.javaEnabled()?"Yes":"No";
	}
	if (document.title){
		WT.ti=document.title;
	}
	WT.js="Yes";
	if (typeof(gVersion)!="undefined"){
		WT.jv=gVersion;
	}
	WT.sp="49437";
	DCS.dcsdat=dCurrent.getTime();
	DCS.dcssip=window.location.hostname;
	DCS.dcsuri=window.location.pathname;
	if (window.location.search){
		DCS.dcsqry=window.location.search;
	}
	if ((window.document.referrer!="")&&(window.document.referrer!="-")){
		if (!(navigator.appName=="Microsoft Internet Explorer"&&parseInt(navigator.appVersion)<4)){
			DCS.dcsref=window.document.referrer;
		}
	}
}

function A(N,V){
	return "&"+N+"="+dcsEscape(V);
}

function dcsEscape(S){
	if (typeof(RE)!="undefined"){
		var retStr = new String(S);
		for (R in RE){
			retStr = retStr.replace(RE[R],R);
		}
		return retStr;
	}
	else{
		return escape(S);
	}
}

function dcsCreateImage(dcsSrc){
	if (document.images){
		gImages[gIndex]=new Image;
		gImages[gIndex].src=dcsSrc;
		gIndex++;
	}
	else{
		document.write('<IMG BORDER="0" NAME="DCSIMG" WIDTH="1" HEIGHT="1" SRC="'+dcsSrc+'">');
	}
}

function dcsMeta(){
	var myDocumentElements;
	if (document.all){
		myDocumentElements=document.all.tags("meta");
	}
	else if (document.documentElement){
		myDocumentElements=document.getElementsByTagName("meta");
	}
	if (typeof(myDocumentElements)!="undefined"){
		for (var i=1;i<=myDocumentElements.length;i++){
			myMeta=myDocumentElements.item(i-1);
			if (myMeta.name){
				if (myMeta.name.indexOf('WT.')==0){
					WT[myMeta.name.substring(3)]=myMeta.content;
				}
				else if (myMeta.name.indexOf('DCSext.')==0){
					DCSext[myMeta.name.substring(7)]=myMeta.content;
				}
				else if (myMeta.name.indexOf('DCS.')==0){
					DCS[myMeta.name.substring(4)]=myMeta.content;
				}
			}
		}
	}
}

function dcsTag(){
	var P="http"+(window.location.protocol.indexOf('https:')==0?'s':'')+"://"+gDomain+(gDcsId==""?'':'/'+gDcsId)+"/dcs.gif?";
	for (N in DCS){
		if (DCS[N]) {
			P+=A(N,DCS[N]);
		}
	}
	for (N in WT){
		if (WT[N]) {
			P+=A("WT."+N,WT[N]);
		}
	}
	for (N in DCSext){
		if (DCSext[N]) {
			P+=A(N,DCSext[N]);
		}
	}
	if (P.length>2048&&navigator.userAgent.indexOf('MSIE')>=0){
		P=P.substring(0,2040)+"&WT.tu=1";
	}
	dcsCreateImage(P);
}

dcsVar();
dcsMeta();
dcsTag();
//-->
</SCRIPT>

<NOSCRIPT>
<IMG BORDER="0" NAME="DCSIMG" WIDTH="1" HEIGHT="1" SRC="https://statse.webtrendslive.com/dcs6fcuwwhr53duyc6hw598cb_3h8c/njs.gif?dcsuri=/nojavascript&WT.js=No">
</NOSCRIPT>
<!-- END OF Data Collection Server TAG -->	

			<meta name="author" content="The Exchange Corp.">
			<meta name="publisher" content="The Exchange Corp.">
			<meta name="copyright" content="The Exchange Corp.">
			<meta name="description" content="Realtime marketplace for buying and selling plastic resin. We carry commodity grade Polyethylene, Polypropylene and Polystyrene">
			<meta name="keywords" content="resin, plastic resin, buy resin, Basell, copolymer, polymer, plastic, resins, polypropylene, polystyrene,online plastics, polyethylene, buy plastic, sell plastic,Chematch, ChemConnect, Omnexus, HDPE, LDPE, LLDPE, PP, copolymer, PS, HIPS, GPPS, HMWPE, plastics exchange, online plastics, resin trading, plastics broker, resin distributor">
			<title>
				<%=PageTitle%>
			</title>
			<script language="javascript" type="text/javascript" src="/include/menu.js"></script>
			<LINK href="/include/master.css" type="text/css" rel="stylesheet">
				<LINK href="/include/menu.css" type="text/css" rel="stylesheet">
					<asp:Panel id="pnAdminMenu" Visible="false" runat="server">
						<!-- DHTML Menu Builder Loader Code START -->
						<div id="DMBRI" style="position:absolute;">
							<img src="/Pics/Header/dmb_i.gif" name="DMBImgFiles" width="1" height="1" border="0"
								alt=""> <img src="/menu/dmb_m.gif" name="DMBJSCode" width="1" height="1" border="0" alt="">
						</div>
						<script language="JavaScript" type="text/javascript">
        var rimPath=null;var rjsPath=null;var rPath2Root=null;function InitRelCode(){var iImg;var jImg;var tObj;if(!document.layers){iImg=document.images['DMBImgFiles'];jImg=document.images['DMBJSCode'];tObj=jImg;}else{tObj=document.layers['DMBRI'];if(tObj){iImg=tObj.document.images['DMBImgFiles'];jImg=tObj.document.images['DMBJSCode'];}}if(!tObj){window.setTimeout("InitRelCode()",700);return false;}rimPath=_gp(iImg.src);rjsPath=_gp(jImg.src);rPath2Root=rjsPath+"../";return true;}function _purl(u){return xrep(xrep(u,"%%REP%%",rPath2Root),"\\","/");}function _fip(img){if(img.src.indexOf("%%REL%%")!=-1) img.src=rimPath+img.src.split("%%REL%%")[1];return img.src;}function _gp(p){return p.substr(0,p.lastIndexOf("/")+1);}function xrep(s,f,n){if(s) s=s.split(f).join(n);return s;}InitRelCode();
						</script>
						<script language="JavaScript" type="text/javascript">
        function LoadMenus() {if(!rjsPath){window.setTimeout("LoadMenus()", 10);return false;}var navVer = navigator.appVersion;
        if(navVer.substr(0,3) >= 4)
        if((navigator.appName=="Netscape") && (parseInt(navigator.appVersion)==4)) {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/nsAdminDDmenu.js"><\/script\>');
        } else {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/ieAdminDDmenu.js"><\/script\>');
        }return true;}LoadMenus();</script>
						<!-- DHTML Menu Builder Loader Code END -->
					</asp:Panel>
					<asp:Panel id="pnBrokerNav" Visible="false" runat="server">
						<!-- DHTML Menu Builder Loader Code START -->
						<div id="DMBRI" style="position:absolute;">
							<img src="/Pics/Header/dmb_i.gif" name="DMBImgFiles" width="1" height="1" border="0"
								alt=""> <img src="/menu/dmb_m.gif" name="DMBJSCode" width="1" height="1" border="0" alt="">
						</div>
						<script language="JavaScript" type="text/javascript">
        var rimPath=null;var rjsPath=null;var rPath2Root=null;function InitRelCode(){var iImg;var jImg;var tObj;if(!document.layers){iImg=document.images['DMBImgFiles'];jImg=document.images['DMBJSCode'];tObj=jImg;}else{tObj=document.layers['DMBRI'];if(tObj){iImg=tObj.document.images['DMBImgFiles'];jImg=tObj.document.images['DMBJSCode'];}}if(!tObj){window.setTimeout("InitRelCode()",700);return false;}rimPath=_gp(iImg.src);rjsPath=_gp(jImg.src);rPath2Root=rjsPath+"../";return true;}function _purl(u){return xrep(xrep(u,"%%REP%%",rPath2Root),"\\","/");}function _fip(img){if(img.src.indexOf("%%REL%%")!=-1) img.src=rimPath+img.src.split("%%REL%%")[1];return img.src;}function _gp(p){return p.substr(0,p.lastIndexOf("/")+1);}function xrep(s,f,n){if(s) s=s.split(f).join(n);return s;}InitRelCode();
						</script>
						<script language="JavaScript" type="text/javascript">
        function LoadMenus() {if(!rjsPath){window.setTimeout("LoadMenus()", 10);return false;}var navVer = navigator.appVersion;
        if(navVer.substr(0,3) >= 4)
        if((navigator.appName=="Netscape") && (parseInt(navigator.appVersion)==4)) {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/nsBrokerDDmenu.js"><\/script\>');
        } else {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/ieBrokerDDmenu.js"><\/script\>');
        }return true;}LoadMenus();</script>
						<!-- DHTML Menu Builder Loader Code END -->
					</asp:Panel>
					<asp:Panel id="pnDistributorNav" Visible="false" runat="server">
						<!-- DHTML Menu Builder Loader Code START -->
						<div id="DMBRI" style="position:absolute;">
							<img src="/Pics/Header/dmb_i.gif" name="DMBImgFiles" width="1" height="1" border="0"
								alt=""> <img src="/menu/dmb_m.gif" name="DMBJSCode" width="1" height="1" border="0" alt="">
						</div>
						<script language="JavaScript" type="text/javascript">
        var rimPath=null;var rjsPath=null;var rPath2Root=null;function InitRelCode(){var iImg;var jImg;var tObj;if(!document.layers){iImg=document.images['DMBImgFiles'];jImg=document.images['DMBJSCode'];tObj=jImg;}else{tObj=document.layers['DMBRI'];if(tObj){iImg=tObj.document.images['DMBImgFiles'];jImg=tObj.document.images['DMBJSCode'];}}if(!tObj){window.setTimeout("InitRelCode()",700);return false;}rimPath=_gp(iImg.src);rjsPath=_gp(jImg.src);rPath2Root=rjsPath+"../";return true;}function _purl(u){return xrep(xrep(u,"%%REP%%",rPath2Root),"\\","/");}function _fip(img){if(img.src.indexOf("%%REL%%")!=-1) img.src=rimPath+img.src.split("%%REL%%")[1];return img.src;}function _gp(p){return p.substr(0,p.lastIndexOf("/")+1);}function xrep(s,f,n){if(s) s=s.split(f).join(n);return s;}InitRelCode();
						</script>
						<script language="JavaScript" type="text/javascript">
        function LoadMenus() {if(!rjsPath){window.setTimeout("LoadMenus()", 10);return false;}var navVer = navigator.appVersion;
        if(navVer.substr(0,3) >= 4)
        if((navigator.appName=="Netscape") && (parseInt(navigator.appVersion)==4)) {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/nsDistributorDDmenu.js"><\/script\>');
        } else {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/ieDistributorDDmenu.js"><\/script\>');
        }return true;}LoadMenus();</script>
						<!-- DHTML Menu Builder Loader Code END -->
					</asp:Panel>
					<asp:Panel id="pnPurchaserNav" Visible="false" runat="server">
						<!-- DHTML Menu Builder Loader Code START -->
						<div id="DMBRI" style="position:absolute;">
							<img src="/Pics/Header/dmb_i.gif" name="DMBImgFiles" width="1" height="1" border="0"
								alt=""> <img src="/menu/dmb_m.gif" name="DMBJSCode" width="1" height="1" border="0" alt="">
						</div>
						<script language="JavaScript" type="text/javascript">
        var rimPath=null;var rjsPath=null;var rPath2Root=null;function InitRelCode(){var iImg;var jImg;var tObj;if(!document.layers){iImg=document.images['DMBImgFiles'];jImg=document.images['DMBJSCode'];tObj=jImg;}else{tObj=document.layers['DMBRI'];if(tObj){iImg=tObj.document.images['DMBImgFiles'];jImg=tObj.document.images['DMBJSCode'];}}if(!tObj){window.setTimeout("InitRelCode()",700);return false;}rimPath=_gp(iImg.src);rjsPath=_gp(jImg.src);rPath2Root=rjsPath+"../";return true;}function _purl(u){return xrep(xrep(u,"%%REP%%",rPath2Root),"\\","/");}function _fip(img){if(img.src.indexOf("%%REL%%")!=-1) img.src=rimPath+img.src.split("%%REL%%")[1];return img.src;}function _gp(p){return p.substr(0,p.lastIndexOf("/")+1);}function xrep(s,f,n){if(s) s=s.split(f).join(n);return s;}InitRelCode();
						</script>
						<script language="JavaScript" type="text/javascript">
        function LoadMenus() {if(!rjsPath){window.setTimeout("LoadMenus()", 10);return false;}var navVer = navigator.appVersion;
        if(navVer.substr(0,3) >= 4)
        if((navigator.appName=="Netscape") && (parseInt(navigator.appVersion)==4)) {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/nsPurchaserDDmenu.js"><\/script\>');
        } else {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/iePurchaserDDmenu.js"><\/script\>');
        }return true;}LoadMenus();</script>
						<!-- DHTML Menu Builder Loader Code END -->
					</asp:Panel>
					<asp:Panel id="pnSellerNav" Visible="false" runat="server">
						<!-- DHTML Menu Builder Loader Code START -->
						<div id="DMBRI" style="position:absolute;">
							<img src="/Pics/Header/dmb_i.gif" name="DMBImgFiles" width="1" height="1" border="0"
								alt=""> <img src="/menu/dmb_m.gif" name="DMBJSCode" width="1" height="1" border="0" alt="">
						</div>
						<script language="JavaScript" type="text/javascript">
        var rimPath=null;var rjsPath=null;var rPath2Root=null;function InitRelCode(){var iImg;var jImg;var tObj;if(!document.layers){iImg=document.images['DMBImgFiles'];jImg=document.images['DMBJSCode'];tObj=jImg;}else{tObj=document.layers['DMBRI'];if(tObj){iImg=tObj.document.images['DMBImgFiles'];jImg=tObj.document.images['DMBJSCode'];}}if(!tObj){window.setTimeout("InitRelCode()",700);return false;}rimPath=_gp(iImg.src);rjsPath=_gp(jImg.src);rPath2Root=rjsPath+"../";return true;}function _purl(u){return xrep(xrep(u,"%%REP%%",rPath2Root),"\\","/");}function _fip(img){if(img.src.indexOf("%%REL%%")!=-1) img.src=rimPath+img.src.split("%%REL%%")[1];return img.src;}function _gp(p){return p.substr(0,p.lastIndexOf("/")+1);}function xrep(s,f,n){if(s) s=s.split(f).join(n);return s;}InitRelCode();
						</script>
						<script language="JavaScript" type="text/javascript">
        function LoadMenus() {if(!rjsPath){window.setTimeout("LoadMenus()", 10);return false;}var navVer = navigator.appVersion;
        if(navVer.substr(0,3) >= 4)
        if((navigator.appName=="Netscape") && (parseInt(navigator.appVersion)==4)) {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/nsSellerDDmenu.js"><\/script\>');
        } else {
        document.write('<' + 'script language="JavaScript" type="text/javascript" src="/menu/ieSellerDDmenu.js"><\/script\>');
        }return true;}LoadMenus();</script>
						<!-- DHTML Menu Builder Loader Code END -->
					</asp:Panel>
		</head>
		<body onload="document.expando=true;" onmousedown="c1c_doc_onmousedown(event);" bottomMargin="0"
			leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
			<asp:Panel id="pnContextMenu" Visible="false" runat="server">
				<style>
					<!--

        /* Context menu Script-  Dynamic Drive (www.dynamicdrive.com) Last updated: 01/08/22
        For full source code and Terms Of Use, visit http://www.dynamicdrive.com */

        .skin0{
        position:absolute;
        width:165px;
        border:2px solid black;
        background-color:menu;
        font-family:Verdana;
        line-height:20px;
        cursor:default;
        font-size:12px;
        z-index:100;
        visibility:hidden;
        }

        .menuitems{
        padding-left:10px;
        padding-right:10px;
        }
        --></style>
				<div id="ie5menu" class="skin0" onMouseover="highlightie5(event)" onMouseout="lowlightie5(event)"
					onClick="jumptoie5(event)" display:none>
					<div class="menuitems" url="/research/Dashboard.aspx">Dashboard</div>
					<div class="menuitems" url="/Administrator/CRM.aspx">Demo Leads</div>
					<!-- only mike will see this link -->
					<%
        if (Session["ID"].ToString() == "2") Response.Write("<div class='menuitems' url='/Administrator/CRM.aspx?EmailOffers=true'>Trading Partners</div>");
        
        %>
					<hr>
					<div class="menuitems" url="/Spot/Floor_Summary.aspx">Floor Summary</div>
					<div class="menuitems" url="/Spot/Spot_Floor.aspx">Spot Floor</div>
					<div class="menuitems" url="/Spot/Resin_Entry.aspx">Order Entry</div>
					<div class="menuitems" url="/Spot/Matched_Order.aspx">Matched Order</div>
					<div class="menuitems" url="/Creditor/Inventory.aspx">Inventory</div>
					<hr>
					<div class="menuitems" url="/Research/Charts.aspx">Charts</div>
					<div class="menuitems" url="/Forward/Admin_Forward_Floor.aspx">Contract Prices</div>
					<hr>
					<div class="menuitems" url="/Creditor/CurrARAPAging.aspx">Credit Management</div>
					<div class="menuitems" url="/Creditor/Transaction_Summary.aspx">Transaction Summary</div>
				</div>
				<script language="JavaScript1.2">

        //set this variable to 1 if you wish the URLs of the highlighted menu to be displayed in the status bar
        var display_url=0

        var ie5=document.all&&document.getElementById
        var ns6=document.getElementById&&!document.all
        if (ie5||ns6)
        var menuobj=document.getElementById("ie5menu")

        function showmenuie5(e){
        //Find out how close the mouse is to the corner of the window
        var rightedge=ie5? document.body.clientWidth-event.clientX : window.innerWidth-e.clientX
        var bottomedge=ie5? document.body.clientHeight-event.clientY : window.innerHeight-e.clientY

        //if the horizontal distance isn't enough to accomodate the width of the context menu
        if (rightedge<menuobj.offsetWidth)
        //move the horizontal position of the menu to the left by it's width
        menuobj.style.left=ie5? document.body.scrollLeft+event.clientX-menuobj.offsetWidth : window.pageXOffset+e.clientX-menuobj.offsetWidth
        else
        //position the horizontal position of the menu where the mouse was clicked
        menuobj.style.left=ie5? document.body.scrollLeft+event.clientX : window.pageXOffset+e.clientX

        //same concept with the vertical position
        if (bottomedge<menuobj.offsetHeight)
        menuobj.style.top=ie5? document.body.scrollTop+event.clientY-menuobj.offsetHeight : window.pageYOffset+e.clientY-menuobj.offsetHeight
        else
        menuobj.style.top=ie5? document.body.scrollTop+event.clientY : window.pageYOffset+e.clientY

        menuobj.style.visibility="visible"
        return false
        }

        function hidemenuie5(e){
        menuobj.style.visibility="hidden"
        }

        function highlightie5(e){
        var firingobj=ie5? event.srcElement : e.target
        if (firingobj.className=="menuitems"||ns6&&firingobj.parentNode.className=="menuitems"){
        if (ns6&&firingobj.parentNode.className=="menuitems") firingobj=firingobj.parentNode //up one node
        firingobj.style.backgroundColor="highlight"
        firingobj.style.color="white"
        if (display_url==1)
        window.status=event.srcElement.url
        }
        }

        function lowlightie5(e){
        var firingobj=ie5? event.srcElement : e.target
        if (firingobj.className=="menuitems"||ns6&&firingobj.parentNode.className=="menuitems"){
        if (ns6&&firingobj.parentNode.className=="menuitems") firingobj=firingobj.parentNode //up one node
        firingobj.style.backgroundColor=""
        firingobj.style.color="black"
        window.status=''
        }
        }

        function jumptoie5(e){
        var firingobj=ie5? event.srcElement : e.target
        if (firingobj.className=="menuitems"||ns6&&firingobj.parentNode.className=="menuitems"){
        if (ns6&&firingobj.parentNode.className=="menuitems") firingobj=firingobj.parentNode
        if (firingobj.getAttribute("target"))
        window.open(firingobj.getAttribute("url"),firingobj.getAttribute("target"))
        else
        window.location=firingobj.getAttribute("url")
        }
        }

        if (ie5||ns6){
        menuobj.style.display=''
        document.oncontextmenu=showmenuie5
        document.onclick=hidemenuie5
        }

				</script>
			</asp:Panel>
			<asp:Panel id="wbContextMenu" Visible="false" runat="server">
				<style>
					<!--

        /* Context menu Script-  Dynamic Drive (www.dynamicdrive.com) Last updated: 01/08/22
        For full source code and Terms Of Use, visit http://www.dynamicdrive.com */

        .skin0{
        position:absolute;
        width:165px;
        border:0px ;       
        font-family:Verdana;
        cursor:default;
        font-size:12px;
        z-index:100;
        visibility:hidden;
        }     
        --></style>
				<div id="ie5menu" class="skin0" onMouseover="highlightie5(event)" display:none >
					
					<!-- only mike will see this link -->
					
					
					
					
				</div>
				<script language="JavaScript1.2">

        //set this variable to 1 if you wish the URLs of the highlighted menu to be displayed in the status bar
        var display_url=0

        var ie5=document.all&&document.getElementById
        var ns6=document.getElementById&&!document.all
        if (ie5||ns6)
        var menuobj=document.getElementById("ie5menu")

        function showmenuie5(e){
        //Find out how close the mouse is to the corner of the window
        var rightedge=ie5? document.body.clientWidth-event.clientX : window.innerWidth-e.clientX
        var bottomedge=ie5? document.body.clientHeight-event.clientY : window.innerHeight-e.clientY

        //if the horizontal distance isn't enough to accomodate the width of the context menu
        if (rightedge<menuobj.offsetWidth)
        //move the horizontal position of the menu to the left by it's width
        menuobj.style.left=ie5? document.body.scrollLeft+event.clientX-menuobj.offsetWidth : window.pageXOffset+e.clientX-menuobj.offsetWidth
        else
        //position the horizontal position of the menu where the mouse was clicked
        menuobj.style.left=ie5? document.body.scrollLeft+event.clientX : window.pageXOffset+e.clientX

        //same concept with the vertical position
        if (bottomedge<menuobj.offsetHeight)
        menuobj.style.top=ie5? document.body.scrollTop+event.clientY-menuobj.offsetHeight : window.pageYOffset+e.clientY-menuobj.offsetHeight
        else
        menuobj.style.top=ie5? document.body.scrollTop+event.clientY : window.pageYOffset+e.clientY

        menuobj.style.visibility="visible"
        return false
        }

        function hidemenuie5(e){
        menuobj.style.visibility="hidden"
        }

        function highlightie5(e){
        var firingobj=ie5? event.srcElement : e.target
        if (firingobj.className=="menuitems"||ns6&&firingobj.parentNode.className=="menuitems"){
        if (ns6&&firingobj.parentNode.className=="menuitems") firingobj=firingobj.parentNode //up one node
        firingobj.style.backgroundColor="highlight"
        firingobj.style.color="white"
        if (display_url==1)
        window.status=event.srcElement.url
        }
        }

        function lowlightie5(e){
        var firingobj=ie5? event.srcElement : e.target
        if (firingobj.className=="menuitems"||ns6&&firingobj.parentNode.className=="menuitems"){
        if (ns6&&firingobj.parentNode.className=="menuitems") firingobj=firingobj.parentNode //up one node
        firingobj.style.backgroundColor=""
        firingobj.style.color="black"
        window.status=''
        }
        }

        function jumptoie5(e){
        var firingobj=ie5? event.srcElement : e.target
        if (firingobj.className=="menuitems"||ns6&&firingobj.parentNode.className=="menuitems"){
        if (ns6&&firingobj.parentNode.className=="menuitems") firingobj=firingobj.parentNode
        if (firingobj.getAttribute("target"))
        window.open(firingobj.getAttribute("url"),firingobj.getAttribute("target"))
        else
        window.location=firingobj.getAttribute("url")
        }
        }

        if (ie5||ns6){
        menuobj.style.display=''
        document.oncontextmenu=showmenuie5
        document.onclick=hidemenuie5
        }

				</script>
			</asp:Panel>
			<!-- begin container table -->
			<table id="table1" cellSpacing="0" cellPadding="0" width="765" border="0">
				<tr>
					<td width="100%" valign="top">
						<!-- nested table that includes logo & sign in -->
						<table id="AutoNumber1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td width="350" height="50" bgcolor="black">
									<a href="/default.aspx"><IMG SRC="/pics/Logo_Black.gif" alt="The Plastics Exchange" border="0"></a>
								</td>
								<td width="100%" bgcolor="#000000" height="50" align="right">
									<table cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td>
											</td>
											<td width="175" height="50" background="/pics/header_gradient.gif">
												<table border="0" width="100%" cellpadding="0" cellspacing="0">
													<tr>
														<td width="50%"></td>
														<td width="50%">
															<p align="right">
																<a href='/Administrator/Fax_Center.aspx'>
																	<asp:Image id="imgFax" runat="server" width="33px" height="38px" ImageUrl="/pics/icons/icon_fax.gif"></asp:Image></a>
														</td>
													</tr>
												</table>
											</td>
											<td valign="center" align="right" bgcolor="9D9D9D" STYLE="border-right: 1px outset #808080">
												<table cellspacing="3" cellpadding="3" border="0">
													<tr>
														<td>
															<font face="Verdana,Arial" color="black" size="1">Welcome,
																<asp:Label id="lblUser" runat="server">Guest</asp:Label>
																<br>
																<asp:Panel id="pnSignInSignUp" runat="server">
									                           <a href="/common/FUNCLogin.aspx"><font color="black"><font size="1"><b>Sign In </font></font>
																		</b></a>
									                           |<a href="/Public/Registration.aspx?Referrer=<%=Request.Url.ToString()%>"> <font color="black">
																			<font size="1" color="#FF0000"><b>Register</b></font></font></a>
                                                          </asp:Panel>
																<asp:HyperLink id="hlLogout" NavigateUrl="/common/FUNCLogout.aspx" Visible="false" runat="server"><font color="black">
																		<font size="1"><b>Logout </font></B></asp:HyperLink>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- end nested table that includes logo & sign in -->
					</td>
				</tr>
				<tr>
					<td vAlign="top" width="100%" colSpan="3">
						<!-- Search bar table -->
						<table height="25" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td align="left" width="100%" background="/pics/gray_gradient_header.gif" height="25">
									<!-- Load the Navigation Main Menu Bar  Here -->
									<table id="table3" cellSpacing="0" cellPadding="0" align="left" border="0" height="25"
										width="450" background="/pics/gray_gradient_header.gif">
										<tr>
											<td align="center" valign="middle" height="25">
												&nbsp;<A href="/default.aspx" class="topnavlinks">Home</A>&nbsp;
											</td>
											<td align="center" valign="middle" height="25"><font face="Verdana,Arial" size="1" color="white"></font></td>
											<td align="center" valign="middle" height="25">
												&nbsp;<A href="/Public/About_Us.aspx" class="topnavlinks">About Us</A>&nbsp;
											</td>
											<asp:Panel id="pnTopNavRegister" runat="server">
												<td align="center" valign="middle" height="25"><font color="white" size="1"></font></td>
												<td align="center" valign="middle" height="25">
													&nbsp;<a href="/Public/Registration.aspx?Referrer=<%=Request.Url.ToString()%>" class="topnavlinks">Register</a>&nbsp;
												</td>
											</asp:Panel>
											<td align="center" valign="middle" height="25"><font face="Verdana,Arial" size="1" color="white"></font></td>
											<td align="center" valign="middle" height="25">
												&nbsp;<A href="/Public/Submit_RFQ.aspx" class="topnavlinks">Request Resin </A>&nbsp;
											</td>
											<td align="center" valign="middle" height="25"><font color="white" size="1"></font></td>
											<td align="center" valign="middle" height="25">
												&nbsp;<A href="/Public/Contact_Us.aspx" class="topnavlinks">Contact Us</A>&nbsp;
											</td>
										</tr>
									</table>
								</td>
								<td align="right" valign="middle" width="25" height="25" background="/pics/gray_gradient_header.gif">
									<img src="/Pics/icons/icon_Phone.gif">
								</td>
								<td align="right" valign="middle" width="139" height="25" background="/pics/gray_gradient_header.gif">
									&nbsp;<font face="Verdana,Arial" size="2" color="white"><b>800.850.2380</b></font>&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="100%" height="100%">
						<!-- Build Left Nav Bar -->
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="00">
							<TBODY>
								<tr vAlign="top">
									<asp:Panel id="pnDefault" runat="server">
										<td vAlign="top" width="182" background="/pics/leftnav_bg.gif" bgColor="#f1f1f1" height="400"
											rowSpan="1">
											<table width="150">
												<asp:Panel id="pnLoginSubmitEmail" runat="server">
													<mbdb:DefaultButtons runat="server" id="DefaultLoginButton">
														<mbdb:DefaultButtonSetting parent="Password" button="btnLogin" />
														<mbdb:DefaultButtonSetting parent="UserName" button="btnLogin" />
													</mbdb:DefaultButtons>
													<tr>
														<td>
															<table border="0">
																<tr>
																	<td>Email:</td>
																</tr>
																<tr>
																	<td><asp:TextBox id="UserName" runat="server" style="width:155px;" /></td>
																</tr>
																<tr>
																	<td>Password:</td>
																</tr>
																<tr>
																	<td><asp:TextBox id="Password" TextMode="Password" runat="server" style="width:155px;" /></td>
																</tr>
																<tr>
																	<td align="right" valign="middle">
																		<a href="/Public/Forgot_Password.aspx">Forgot?</a> &nbsp
																		<asp:ImageButton id="btnLogin" ImageUrl="/Pics/icons/button_Login.gif" onclick="ClickLogin" CausesValidation="false"
																			runat="server" />
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td width="100%" STYLE="border-Top: 1px outset #808080;border-Bottom: 1px outset #808080">
															<mbdb:DefaultButtons runat="server" id="DefaultEmailButton">
																<mbdb:DefaultButtonSetting parent="txtEmail" button="btnSubmitEmail" />
															</mbdb:DefaultButtons>
															<table border="0">
																<tr>
																	<td><img src="/Pics/Mailbox.gif"></td>
																	<td><p align="left">Email me free market updates.</p>
																	</td>
																</tr>
																<tr>
																	<td colspan="2"><asp:TextBox id="txtEmail" runat="server" style="width:155px;" class="searchbox" /></td>
																</tr>
																<tr>
																	<td align="right" colspan="2">
																		<asp:ImageButton id="btnSubmitEmail" ImageUrl="/Pics/icons/button_Submit.gif" onclick="ClickSubmitEmail"
																			CausesValidation="false" runat="server" />
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</asp:Panel>
												<asp:Panel id="pnDemoNav" runat="server">
													<tr>
														<td>
															<div ID="divStayTopLeft" CLASS="c1_rootmenu" style="color:Black;background-color:#F1F1F1;width:150px;display:;position:absolute;z-index:;left:;top:;"
																c1_item_styles="className:c1_rootmenu_item;" c1_sel_item_styles="className:c1_rootmenu_selected_item;">
																<div ID="myHome" CLASS="c1_rootmenu_item" style="width:100%;cursor:pointer;cursor:hand;"
																	onmouseout="c1c_onmouseout(event,this);" onmouseover="c1c_onmouseover(event,this);"
																	onmousedown="window.open('/Research/Dashboard.aspx?Cont_Id=3','_self');">
																	<div STYLE="float:left;width:2px;">&nbsp;</div>
																	<div STYLE="float:right;width:10%;"></div>
																	<div ID="item_text" style="width:100%;">Dashboard</div>
																</div>
																<div ID="Dashboard" CLASS="c1_rootmenu_item" style="width:100%;cursor:pointer;cursor:hand;"
																	onmouseout="c1c_onmouseout(event,this);" onmouseover="c1c_onmouseover(event,this);"
																	onmousedown="window.open('/Public/Public_Research.aspx?a=PE','_self');">
																	<div STYLE="float:left;width:20px;">&nbsp;</div>
																	<div STYLE="float:right;width:10%;"></div>
																	<div ID="item_text" style="width:100%;">Research</div>
																</div>
																<div ID="News" CLASS="c1_rootmenu_item" style="width:100%;cursor:pointer;cursor:hand;"
																	onmouseout="c1c_onmouseout(event,this);" onmouseover="c1c_onmouseover(event,this);"
																	onmousedown="window.open('/Public/Public_News.aspx','_self');">
																	<div STYLE="float:left;width:20px;">&nbsp;</div>
																	<div STYLE="float:right;width:10%;"></div>
																	<div ID="item_text" style="width:100%;">News</div>
																</div>
																<div ID="Charts" CLASS="c1_rootmenu_item" style="width:100%;cursor:pointer;cursor:hand;"
																	onmouseout="c1c_onmouseout(event,this);" onmouseover="c1c_onmouseover(event,this);"
																	onmousedown="window.open('/Research/Charts.aspx','_self');">
																	<div STYLE="float:left;width:20px;">&nbsp;</div>
																	<div STYLE="float:right;width:10%;"></div>
																	<div ID="item_text" style="width:100%;">Resin Charts</div>
																</div>
																<div ID="c1cRtUid12" CLASS="c1_rootmenu_item" style="width:100%;cursor:pointer;cursor:hand;"
																	onmouseout="c1c_onmouseout(event,this);" onmouseover="c1c_onmouseover(event,this);"
																	onmousedown="window.open('/Spot/Spot_Floor.aspx','_self');">
																	<div STYLE="float:left;width:2px;">&nbsp;</div>
																	<div STYLE="float:right;width:10%;"></div>
																	<div ID="item_text" style="width:100%;">Spot Market</div>
																</div>
																<div ID="c1cRtUid13" CLASS="c1_rootmenu_item" style="width:100%;cursor:pointer;cursor:hand;"
																	onmouseout="c1c_onmouseout(event,this);" onmouseover="c1c_onmouseover(event,this);"
																	onmousedown="window.open('/Spot/Order_Entry.aspx','_self');">
																	<div STYLE="float:left;width:20px;">&nbsp;</div>
																	<div STYLE="float:right;width:10%;"></div>
																	<div ID="item_text" style="width:100%;">Order Entry</div>
																</div>
																<div ID="c1cRtUid1" CLASS="c1_rootmenu_item" style="width:100%;cursor:pointer;cursor:hand;"
																	onmouseout="c1c_onmouseout(event,this);" onmouseover="c1c_onmouseover(event,this);"
																	onmousedown="window.open('/Forward/Forward_Floor.aspx','_self');">
																	<div STYLE="float:left;width:2px;">&nbsp;</div>
																	<div STYLE="float:right;width:10%;"></div>
																	<div ID="item_text" style="width:100%;">Contract Market</div>
																</div>
																<!--<div ID="c1cRtUid3" CLASS="c1_rootmenu_item" c1_submenu_id="c1cRtUid8" c1_submenu_open="over" style="width:100%;cursor:pointer;cursor:hand;" onmouseout="c1c_onmouseout(event,this);" onmouseover="c1c_onmouseover(event,this);" onmousedown="window.open('/pages.aspx?pagesid=52','_self');">
                                                <div STYLE="float:left;width:2px;">&nbsp;</div>
                                                <div STYLE="float:right;width:10%;font-family:webdings;">4</div>
                                                <div ID="item_text" style="width:100%;">Test </div>
                                            </div>

                                         <div ID="c1cRtUid14" CLASS="c1_rootmenu_item" style="width:100%;cursor:pointer;cursor:hand;" onmouseout="c1c_onmouseout(event,this);" onmouseover="c1c_onmouseover(event,this);" onmousedown="window.open('/Spot/Spot_Floor.aspx?Export=true','_self');">
                                                <div STYLE="float:left;width:2px;">&nbsp;</div>
                                                <div STYLE="float:right;width:10%;"></div>
                                                <div ID="item_text" style="width:100%;">International Spot Floor</div>-->
																<table border="0" width="100%">
																	<tr>
																		<td width="20"><img src="/Pics/icons/QuestionMark_24x24.gif" width="24" height="24"></td>
																		<td><font size="2" color="#CC6600">Support Center</font></td>
																	</tr>
																	<tr>
																		<td colspan="2">
																			<a href="/Public/Contact_Us.aspx"><img src="/pics/arrow.gif" border="0">&nbsp<font size="1">Contact 
																					Us</font></a><BR>
																			<a href="/Public/FAQs.aspx"><img src="/pics/arrow.gif" border="0">&nbsp<font size="1">FAQ's</font></a><BR>
																			<a href="/docs/userguide.pdf"><img src="/pics/arrow.gif" border="0">&nbsp<font size="1">User 
																					Guide</font></a><BR>
																			<a href="/Public/Glossary.aspx"><img src="/pics/arrow.gif" border="0">&nbsp<font size="1">Glossary</font></a><BR>
																			<a href="/Public/Links/LinksMain.aspx"><img src="/pics/arrow.gif" border="0">&nbsp<font size="1">Plastics 
																					Links</font></a><BR>
																			<a href="/Public/Careers.aspx"><img src="/pics/arrow.gif" border="0">&nbsp<font size="1">Careers</font></a><BR>
																			<!-- <a href="#"><img src="/pics/arrow.gif" border="0">&nbsp<font size="1">Tutorial</font></a>-->
																		</td>
																	</tr>
																</table>
															</div>
															<!-- *********************************************************
                                            * You may use this code for free on any web page provided that
                                            * these comment lines and the following credit remain in the code.
                                            * TopLeft Floating Div from http://www.javascript-fx.com
                                            ********************************************************  -->
															<script type="text/javascript">

                                                function JSFX_FloatTopLeft()
                                                {
	                                               var startX = 0, startY = 75;
	                                               var ns = (navigator.appName.indexOf("Netscape") != -1);
	                                               var d = document;
	                                               var px = document.layers ? "" : "px";
	                                               function ml(id)
	                                               {
		                                              var el=d.getElementById?d.getElementById(id):d.all?d.all[id]:d.layers[id];
		                                              if(d.layers)el.style=el;
		                                              el.sP=function(x,y){this.style.left=x+px;this.style.top=y+px;};
		                                              el.x = startX; el.y = startY;
		                                              return el;
	                                               }
	                                               window.stayTopLeft=function()
	                                               {
		                                              var pY = ns ? pageYOffset : document.documentElement && document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop;
		                                              ftlObj.y += (pY + startY - ftlObj.y)/4;
		                                              ftlObj.sP(ftlObj.x, ftlObj.y);
		                                              setTimeout("stayTopLeft()", 40);
	                                               }
	                                               ftlObj = ml("divStayTopLeft");
	                                               stayTopLeft();
                                                }
                                                JSFX_FloatTopLeft();
															</script>
														</td>
													</tr>
												</asp:Panel>
												<asp:Panel id="pnSupportCenter" runat="server">
													<tr>
														<td>
														<img width="165" height="1">
															<!-- BEGIN Timpani Button Code  -->
															<asp:Label runat="server" ID="lbLivePerson"></asp:Label>
															<!-- END Button Code -->
														</td>
													</tr>
													<tr>
														<td>
															<table border="0" width="100%">
																<tr>
																	<td width="20"><img src="/Pics/icons/QuestionMark_24x24.gif" width="24" height="24"></td>
																	<td><font size="2" color="#CC6600">Support Center</font></td>
																</tr>
																<tr>
																	<td colspan="2">
																		<a href="/Public/Contact_Us.aspx"><img src="/pics/arrow.gif" border="0">&nbsp<font size="1">Contact 
																				Us</font></a><BR>
																		<a href="/Public/FAQs.aspx"><img src="/pics/arrow.gif" border="0">&nbsp<font size="1">FAQ's</font></a><BR>
																		<a href="/docs/userguide.pdf"><img src="/pics/arrow.gif" border="0">&nbsp<font size="1">User 
																				Guide</font></a><BR>
																		<a href="/Public/Glossary.aspx"><img src="/pics/arrow.gif" border="0">&nbsp<font size="1">Glossary</font></a><BR>
																		<a href="/Public/Links/LinksMain.aspx"><img src="/pics/arrow.gif" border="0">&nbsp<font size="1">Plastics 
																				Links</font></a><BR>
																		<a href="/Public/Careers.aspx"><img src="/pics/arrow.gif" border="0">&nbsp<font size="1">Careers</font></a><BR>
																		<!-- <a href="#"><img src="/pics/arrow.gif" border="0">&nbsp<font size="1">Tutorial</font></a>-->
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</asp:Panel>
												<asp:Panel id="pnRFQ" runat="server">
													<tr>
														<td width="100%" STYLE="border-top: 1px outset #808080">
															<table border="0" cellpadding="1" cellspacing="0">
																<tr>
																	<td colspan="2">
																		<A href="/Public/Submit_RFQ.aspx"><img border="0" src="/Pics/icons/icon_dollar.gif" align="middle" width="21" height="19">
																			<font size="2" color="#CC6600">Request Resin </font></A>
																	</td>
																</tr>
																<!--<tr>
				                                    <td width="35%">Resin:</td>
				                                    <td><asp:TextBox runat="server" id="txtNavResin" class="searchbox" /></td>
				                                </tr>
				                                <tr>
				                                    <td width="30%">Qty:</td>
				                                    <td><asp:TextBox runat="server" id="txtNavQty"  class="searchbox" /></td>
				                                </tr>
				                                <tr>
				                                    <td width="30%">Ship To:</td>
				                                    <td><asp:TextBox runat="server" id="txtNavShipTo" class="searchbox" /></td>
				                                </tr>
				                                <tr>
				                                    <td width="30%">Price:</td>
				                                    <td><asp:TextBox runat="server" id="txtNavPrice"  class="searchbox" /></td>
				                                </tr>
				                                <tr>
				                                    <td width="30%">Name:</td>
				                                    <td><asp:TextBox runat="server" id="txtNavName" class="searchbox" /></td>
				                                </tr>
				                             
				                                <tr>
				                                    <td width="30%">Email:</td>
				                                    <td><asp:TextBox runat="server" id="txtNavEmail" size=15 class="searchbox" /></td>
				                                </tr>
				                                <tr>
				                                    <td width="30%">Phone:</td>
				                                    <td><asp:TextBox runat="server" id="txtNavPhone" size=15 class="searchbox" /></td>
				                                </tr>
				                                <tr>
				                                    <td align="right" colspan="2">
				                                    <asp:ImageButton
															id="btnSubmitRFQ"
				                                            ImageUrl = "/Pics/icons/button_Submit.gif"
				                                            onclick = "ClickRFQ"
				                                            CausesValidation="false"
				                                            runat="server" />
				                                            &nbsp
				                                    </td>
				                                </tr>-->
															</table>
														<td>
													</tr>
												</asp:Panel>
											</table>
									</td></asp:Panel>
					
					<!-- close tables
					         </tr>
						 </TBODY>
				    </table>
				     -->
					<!--Done Left Nav Bar -->
					</td> 
					<!-- add in spacer to give it a little margin -->
					<asp:Panel id="pnHeaderMargin" runat="server">
						<td width="10" bgcolor="white">&nbsp&nbsp</td>
						<td width="100%" height="450" bgcolor="white"><BR>
					</asp:Panel>
					<asp:Panel id="pnNoMargin" Visible="false" runat="server">
						<td width="100%" bgcolor="white" height="450" STYLE="border-right: 1px outset #808080">
					</asp:Panel>
	<!-- Build MainPages -->
</asp:Panel>
<asp:Panel id="pnFooter" runat="server">
 <!-- begin footer -->
                 </td>

                 <asp:Panel id="pnFooterMargin" Visible="false" runat="server">
		<!-- add in spacer to give it a little margin -->
		<td STYLE="border-right: 1px outset #808080" bgcolor="white">&nbsp&nbsp</td>
	</asp:Panel>



	            </TR>
	            <tr>
		<td bgcolor="#464646" height="15" colspan="4" align="center">
			<b><a href="/Public/Privacy_Policy.aspx"><font face="Verdana,Arial" size="1" color="white">
					Privacy Statement</a> | 2005 The Plastics Exchange. LLC. | All Rights 
				Reserved. Patent Pending</b></font>
		</td>
	</tr>
	     <!-- begin close container table -->
	       </TD>
	      </Tr>
	  </table>
	  <asp:Literal ID="LivePerson" Visible =true Runat=server></asp:Literal>
	  <!-- end close container table -->

    </body>
</html>
</asp:Panel>
