using System;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using TPE.NewsCrawler;
using TPE.Utility;

namespace TPE.NewsCrawler
{
/*
	class StartCrawl
	{
		[STAThread]
		static void Main(string[] args)
		{
			NewsCrawler nc = new NewsCrawler(ConfigurationSettings.AppSettings["NewsCrawler_PhysicalPath"],
											 ConfigurationSettings.AppSettings["NewsCrawler_FileFilter"],
											 ConfigurationSettings.AppSettings["NewsCrawler_IndexFileName"]);
			nc.BuildCatalog();	
			nc.Test("c);
		}
	}
*/
	/// <summary>
	/// Summary description for NewsCrawler.
	/// </summary>
	class NewsCrawler
	{
		private string m_path;
		private string m_filter;
		private string m_catalogFileName;

		/// <summary>Working variable for the catalog being built</summary>
		private Catalog m_catalog ;

		/// <summary>Look for settings, then start crawl</summary>
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		public NewsCrawler(string path, string filter, string catalogFileName)
		{
			// Get settings from web.config
			m_path = path;
			m_filter = filter;
			m_catalogFileName = catalogFileName;
			
			if (null == m_filter) 
			{ 
				// default to xml files
				m_filter = "*.xml";
			}
			// Write config info to Trace (for debugging)
			string message = "\n\nConfiguration:\nPath: "+ m_path + "\nFilter: "+ m_filter;
			// for debugging/information
			System.Console.WriteLine(message);
			// write HTML header
			Console.WriteLine(@"Generating the catalog - sorry for the inconvenience, it will only take a few minutes...");
			// Build the catalog!
			BuildCatalog ();
			// Check if anything was found			
			
			if (m_catalog.Length > 0) 
			{
				Console.WriteLine ("\n\nFinished - " + m_catalog.Length + " words catalogued!");
			} 
			else
				Console.WriteLine ("\n\nSorry, nothing was cataloged. Check the settings in app.config.");

		} // c'tor

		public void Test()
		{
			string searchWord = "the";

			Console.WriteLine("Searching for '" + searchWord+"'...");
			Hashtable result = Catalog.Search(searchWord,this.m_path,this.m_catalogFileName);	// do the search
				
			if (result != null)
			{					
				foreach (TPE.NewsCrawler.File f in new IterSortHashValue(result,new TPE.NewsCrawler.DescendingNumberComparer()))
				{
					Console.WriteLine("\nfileName=" + f.Title + " wordCount=" +result[f].ToString());
				}
			}
			else
			{
				Console.WriteLine("No results found.");
			}
			Console.WriteLine("Press enter to quit.");
			Console.ReadLine();
		}

		/// <summary> Start the recursive processing, and save result to disk</summary>
		private void BuildCatalog () 
		{

			m_catalog = new Catalog();

			CrawlCatalog (m_path);

			// ### serialize the catalog ###
			m_catalog.save(m_path + @"\" + m_catalogFileName);

		} // BuildCatalog

		/// <summary>Stripping HTML, thanks to
		/// http://www.4guysfromrolla.com/webtech/042501-1.shtml</summary>
		protected string stripHtml(string strHtml) 
		{
			//Strips the HTML tags from strHTML
			System.Text.RegularExpressions.Regex objRegExp
				= new System.Text.RegularExpressions.Regex("<(.|\n)+?>");

			// Replace all tags with a space, otherwise words either side
			// of a tag might be concatenated
			string strOutput = objRegExp.Replace(strHtml, " ");

			// Replace all < and > with &lt; and &gt;
			strOutput = strOutput.Replace("<", "&lt;");
			strOutput = strOutput.Replace(">", "&gt;");

			return strOutput;
		}

		// variables used in recursive method CrawlCatalog
		string fileContents,fileurl;
		DateTime filedate;
		string filedesc="";
		string filetitle="";

		/// <summary>Crawl through a directory, processing files matching the pattern</summary>
		private void CrawlCatalog (string path) 
		{
			System.IO.DirectoryInfo m_dir = new System.IO.DirectoryInfo (path);
			// Look for matching files
			foreach (System.IO.FileInfo f in m_dir.GetFiles(m_filter)) 
			{
				if (f.Name != "0000.xml")	// ignore special file
				{
					fileurl = path + "\\" + f.Name;

					System.IO.StreamReader reader = System.IO.File.OpenText  (fileurl);
					fileContents = reader.ReadToEnd();
					reader.Close(); // now use the fileContents to build the catalog...

					// ### Grab the <TITLE> ###
					//Match TitleMatch = Regex.Match(fileContents, "<title>([^<]*)</title>", RegexOptions.IgnoreCase | RegexOptions.Multiline );
					filetitle = ReadHead(fileurl);
					// ### Parse out META data ###
					//Match DescriptionMatch = Regex.Match( fileContents, "<META NAME=\"DESCRIPTION\" CONTENT=\"([^<]*)\">", RegexOptions.IgnoreCase | RegexOptions.Multiline );
					// ### Get the file date ###
					filedate = ReadDate(fileurl);
					// ### Now remove HTML, convert to array, clean up words and index them ###
					fileContents = stripHtml (fileContents);


					string wordsOnly = stripHtml(fileContents);

					/*
					if (wordsOnly.Length > 350)
						filedesc = wordsOnly.Substring(0, 350);
					else if (wordsOnly.Length > 100)
						filedesc = wordsOnly.Substring(0, 100);
					else
						filedesc = wordsOnly; // file is only short!
					*/
				
					Regex r = new Regex(@"\s+");           // remove all whitespace
					wordsOnly = r.Replace(wordsOnly, " "); // compress all whitespace to one space
					string [] wordsOnlyA = wordsOnly.Split(' '); // results in an array of words

					// Create the object to represent this file (there will only ever be ONE instance per file)
					TPE.NewsCrawler.File infile = new TPE.NewsCrawler.File(fileurl,f.Name,filetitle,"",filedate);

					// ### Loop through words in the file ###
					int i = 0;     // Position of the word in the file (starts at zero)
					string key = ""; // the 'word' itself
					// Now loop through the words and add to the catalog
					foreach (string word in wordsOnlyA) 
					{
						key = word.Trim(' ', '?','\"', ',', '\'', ';', ':', '.', '(', ')').ToLower();
						m_catalog.Add (key, infile, i);
						i++;
					} // foreach word in the file
					Console.WriteLine (" parsed " + i.ToString() + " words");
				}

			} // foreach matching file

			// Now recursively call this method for all subfolders
			foreach (System.IO.DirectoryInfo d in m_dir.GetDirectories()) 
			{
				CrawlCatalog (path + @"\" + d.Name);
			} // foreach folder
		} // CrawlCatalog

		private void CrawlXML (string path) 
		{
			System.IO.DirectoryInfo m_dir = new System.IO.DirectoryInfo (path);
			// Look for matching files
			foreach (System.IO.FileInfo f in m_dir.GetFiles(m_filter)) 
			{
				if (f.Name != "0000.xml")	// ignore special file
				{
					string strUrl = path + "\\" + f.Name;

					using (XmlTextReader reader = new XmlTextReader(strUrl))
					{
						if (reader.NodeType == XmlNodeType.Element)
						{
							if (reader.LocalName.Equals("DateId"))
							{
								filedate = reader.ReadString();
							}
							else if  (reader.LocalName.Equals("Headline"))
							{
								filetitle = reader.ReadString();
							}
							else if (reader.LocalName.Equals("body.content"))
							{
								fileContents = reader.ReadString();
							}
						}
					}
					
					// ### Grab the <TITLE> ###
					//Match TitleMatch = Regex.Match(fileContents, "<title>([^<]*)</title>", RegexOptions.IgnoreCase | RegexOptions.Multiline );
					filetitle = ReadHead(fileurl);
					// ### Parse out META data ###
					//Match DescriptionMatch = Regex.Match( fileContents, "<META NAME=\"DESCRIPTION\" CONTENT=\"([^<]*)\">", RegexOptions.IgnoreCase | RegexOptions.Multiline );
					// ### Get the file date ###
					filedate = ReadDate(fileurl);
					// ### Now remove HTML, convert to array, clean up words and index them ###
					fileContents = stripHtml (fileContents);


					string wordsOnly = stripHtml(fileContents);

					/*
					if (wordsOnly.Length > 350)
						filedesc = wordsOnly.Substring(0, 350);
					else if (wordsOnly.Length > 100)
						filedesc = wordsOnly.Substring(0, 100);
					else
						filedesc = wordsOnly; // file is only short!
					*/
				
					Regex r = new Regex(@"\s+");           // remove all whitespace
					wordsOnly = r.Replace(wordsOnly, " "); // compress all whitespace to one space
					string [] wordsOnlyA = wordsOnly.Split(' '); // results in an array of words

					// Create the object to represent this file (there will only ever be ONE instance per file)
					TPE.NewsCrawler.File infile = new TPE.NewsCrawler.File(fileurl,f.Name,filetitle,"",filedate);

					// ### Loop through words in the file ###
					int i = 0;     // Position of the word in the file (starts at zero)
					string key = ""; // the 'word' itself
					// Now loop through the words and add to the catalog
					foreach (string word in wordsOnlyA) 
					{
						key = word.Trim(' ', '?','\"', ',', '\'', ';', ':', '.', '(', ')').ToLower();
						m_catalog.Add (key, infile, i);
						i++;
					} // foreach word in the file
					Console.WriteLine (" parsed " + i.ToString() + " words");
				}

			} // foreach matching file

			// Now recursively call this method for all subfolders
			foreach (System.IO.DirectoryInfo d in m_dir.GetDirectories()) 
			{
				CrawlCatalog (path + @"\" + d.Name);
			} // foreach folder
		}
		public string ReadHead(string UrlToXmlFile)
		{
			//only read headline node
			System.Text.StringBuilder sbXML = new System.Text.StringBuilder();
			XmlTextReader reader = null;
			reader = new XmlTextReader(UrlToXmlFile);
			object oName = reader.NameTable.Add("HeadLine");
			while (reader.Read())
			{
				if (reader.NodeType == XmlNodeType.Element)
				{
					if (reader.Name.Equals(oName))
					{
						sbXML.Append(reader.ReadString()).Append("");
					}
				}
			}
			reader.Close();
			return sbXML.ToString();
		} 

		public DateTime ReadDate(string UrlToXmlFile)
		{
			DateTime result = DateTime.Now;
			//only read date node
			XmlTextReader reader = null;
			reader = new XmlTextReader(UrlToXmlFile);
			try
			{
				object oName = reader.NameTable.Add("DateId");
				while (reader.Read())
				{
					if (reader.NodeType == XmlNodeType.Element)
					{
						if (reader.Name.Equals(oName))
						{
							result = ParseDate(reader.ReadString());
						}
					}
				}
			}
			finally
			{
				reader.Close();
			}
			return result;
		} 

		private DateTime ParseDate(string s)
		{
			DateTime result = DateTime.Now;
			try 
			{
				result = DateTime.Parse(s);
			}
			catch
			{
				switch (s.Length)
				{
					case 8:					
						result = new DateTime(Int32.Parse(s.Substring(0,4)),Int32.Parse(s.Substring(4,2)),Int32.Parse(s.Substring(6,2)));
						break;
					case 7:
						result = new DateTime(Int32.Parse(s.Substring(0,4)),Int32.Parse(s.Substring(4,1)),Int32.Parse(s.Substring(5,2)));
						break;
					default:
						result = new DateTime(1900,1,1);
						break;
				}
			}
			return result;
		}

	} // class NewsCrawler

}
