<%@ Control Language="C#" %>
<script runat="server">

    // Insert user control code here
    //
    public bool Footer = false;
    public string Heading ="";
    public string Align = "center";
    public string Width = "100%";
    public string bgcolor = "#F1F1F1";

    private void Page_Load(object sender, EventArgs e){
    // swap the files in the event that it is a header or a footer
        if (Footer){
            pnHeader.Visible = false;
            pnFooter.Visible = true;
        }else{
            pnHeader.Visible = true;
            pnFooter.Visible = false;
        }
    }

</script>
<!-- Insert content here -->
<asp:Panel id="pnHeader" runat="server">
<table border=0 align="<%=Align%>" cellspacing=0 cellpadding=0 <% if (Width != null) { Response.Write("width=\""+Width+"\"");}  %> >
	<tr>
		<td bgcolor=#000000 <TD class="ListHeadlineBold" >
		<%=Heading%>
		</td>
	</tr>
	<tr>
	   <td valign=top>
	    
</asp:Panel>
<asp:Panel id="pnFooter" Visible="False" runat="server">

		
		</td>
	</tr>
</table>
</asp:Panel>
