namespace localhost.include
{
	using System;
	using System.Configuration;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for Template.
	/// </summary>
	public partial class Template : System.Web.UI.UserControl
	{
		private string _PageTitle;
		private string _Width = "780";
		public bool Footer = false;
		
		public string PageTitle
		{
			get {return _PageTitle;}
			set {_PageTitle = value;}
		}

		public string Width
		{
			get {return _Width;}
			set {_Width = value;}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			PageTitle = "The Plastics Exchange: Commodity Plastic Resin, Buy Resin, Sell Resin, Plastic Materials";

			if (Request.UserHostAddress.ToString() == ConfigurationSettings.AppSettings["OfficeIP"].ToString())
			{
				LivePerson.Visible = false;
			}
			LivePerson.Text ="<script language=\"JavaScript1.2\"> "; 
			LivePerson.Text +="if (typeof(tagVars) == \"undefined\") tagVars = \"\"; ";
			string strUserName = string.Empty;
			if (Session["UserName"] != null) strUserName = Session["UserName"].ToString();
			LivePerson.Text +="tagVars += \"&VISITORVAR!User%20Name=" + strUserName + "\";";
			LivePerson.Text += "<" + "/SCRIPT>";
			LivePerson.Text +="<script language='javascript' src='https://server.iad.liveperson.net/hc/92199096/x.js?cmd=file&file=chatScript3&site=92199096&&category=en;woman;5'>";
			LivePerson.Text += "<" + "/SCRIPT>";

			if (Session["Id"] == null)
			{
				pnLogin.Visible = true;
				pnLogout.Visible = false;	
				pnDefault.Visible = true;
			}
			else
			{				
				pnLogin.Visible = false;
				pnLogout.Visible = true;
				lblUser.Text = Session["Name"].ToString();
				pnDefault.Visible = false;

				switch (Session["Typ"].ToString())
				{
					case "A":
						pnAdminMenu.Visible = true;
						// add in right click menu
						pnContextMenu.Visible = true;
						break;
					case "B":
						pnBrokerNav.Visible = true;
						break;
					case "T":
						pnBrokerNav.Visible = true;
						break;
					case "P": case "O":
						pnPurchaserNav.Visible = true;
						break;
					case "S":
						pnSellerNav.Visible = true;
//						pnPurchaserNav.Visible = true;
						break;
					case "D":
						pnDistributorNav.Visible = true;
						break;
					case "Demo":
						pnPurchaserNav.Visible = true;
						break;
				}

			}

			if (Footer)
			{
				pnHeader.Visible = false;
				pnFooter.Visible = true;
			}
			else
			{
				pnHeader.Visible = true;
				pnFooter.Visible = false;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnLogin.Click += new System.Web.UI.ImageClickEventHandler(this.ClickLogin);

		}
		#endregion

		private void ClickLogin(object sender, System.Web.UI.ImageClickEventArgs e)
		{	
			// Saving information into session object to be passed on to the login function
			Session["UserName"] = UserName.Text;
			Session["Password"] = Crypto.Encrypt(Password.Text);
			Response.Redirect("/Common/FUNCLogin.aspx");
		}
	}
}
