<%@ Control Language="c#" AutoEventWireup="True" Codebehind="Shipment_Documents.ascx.cs" Inherits="localhost.Administrator.Shipment_Documents" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<FIELDSET style="WIDTH: 468px; HEIGHT: 100%; text-align:left">
    <legend><span class="Header Color2 Bold">Shipment Documents</span></legend>
	<script language="JavaScript">
		function PreviewEmail()
		{
			var custom = '';
			if (document.getElementById('<%=txtCustom.ClientID %>')!=null)
			{
				custom = document.getElementById('<%=txtCustom.ClientID %>').value;
			}
			window.open('../administrator/Email.aspx?CustomField='+custom,'','toolbar=false,resizable=1,scrollbars=1,menubar=false,width=800,height=800,top=20,left=100');			
			document.getElementById('<%=btnEmail.ClientID %>').disabled = false;
		}
	</script>
	<TABLE id="Table1" style="WIDTH: 472px; HEIGHT: 100%">
		<TR align=left>
			<TD vAlign="top" align="center" colSpan="3"><asp:label CssClass="Content Color3 Bold" id="lblWarning" runat="server"></asp:label></TD>
		</TR>
		<TR>
			<TD vAlign="top" colSpan="3" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:checkbox CssClass="InputForm" id="chbMultiple" runat="server" Visible="False" Text="Group Orders" AutoPostBack="True"></asp:checkbox>&nbsp;</TD>
		</TR>
		<TR>
		    <TD align="right">
                <STRONG><asp:Label CssClass="Content Color2" ID="lblUnit" runat="server" Text="Unit:"></asp:Label></STRONG></TD>
		    <TD>
                <asp:CheckBox ID="cbMetric" runat="server" Text="Include Metric"  Checked="false" OnCheckedChanged="cbMetric_Click" AutoPostBack="True" style="font-size: 10pt"/></TD>
		</TR>
		<TR align=left>
			<TD vAlign="top" align="right"><STRONG><asp:label CssClass="Content Color2" id="lblType" runat="server">Type:</asp:label></STRONG></TD>
			<TD vAlign="top" colSpan="2"><STRONG></STRONG><asp:dropdownlist CssClass="InputForm" id="ddlType" runat="server" Width="176px" AutoPostBack="True" onselectedindexchanged="ddlType_SelectedIndexChanged">
					<asp:ListItem Value="1">Invoice</asp:ListItem>
					<asp:ListItem Value="2">Purchase Order</asp:ListItem>
					<asp:ListItem Value="3">Certificate</asp:ListItem>
					<asp:ListItem Value="4">Sales Confirmation</asp:ListItem>
					<asp:ListItem Value="5">Customer Release</asp:ListItem>
					<asp:ListItem Value="6">HC Title Transfer</asp:ListItem>
					<asp:ListItem Value="8">Warehouse Release</asp:ListItem>
					<asp:ListItem Value="9">Release from Inventory</asp:ListItem>
					<asp:ListItem Value="10">Proforma Invoice</asp:ListItem>
				</asp:dropdownlist></TD>
		</TR>
		<TR align=left>
			<TD align="right"><asp:label CssClass="Content Color2" id="lblSendTo" runat="server" Font-Bold="True">Send To:</asp:label></TD>
			<TD style="HEIGHT: 24px" colSpan="2"><asp:dropdownlist CssClass="InputForm" id="ddlSendTo" runat="server" Width="245px" AutoPostBack="False"></asp:dropdownlist></TD>
		</TR>
		<TR align=left>
			<TD vAlign="top" align="right"><asp:checkbox CssClass="Content Color2" id="chkCustomSendTo" runat="server" Text="Custom Send To:" oncheckedchanged="chkCustomSendTo_CheckedChanged"></asp:checkbox></TD>
			<TD vAlign="top" colSpan="2"><asp:textbox CssClass="InputForm" id="txtCustomSendTo" runat="server" Width="243px"></asp:textbox></TD>
		</TR>
		<TR align=left>
			<TD align="right"><asp:label CssClass="Content Color2" id="lblCC" runat="server" Font-Bold="True">Cc:</asp:label></TD>
			<TD colSpan="2"><asp:dropdownlist CssClass="InputForm" id="ddlCC" runat="server" Width="245px" AutoPostBack="False"></asp:dropdownlist></TD>
		</TR>
		<TR align=left>
			<TD align="right"><asp:checkbox CssClass="Content Color2" id="chkCustomCC" runat="server" Text="Custom Cc:" oncheckedchanged="CheckBox1_CheckedChanged"></asp:checkbox></TD>
			<TD colSpan="2"><asp:textbox CssClass="InputForm" id="txtCustomCC" runat="server" Width="243px"></asp:textbox></TD>
		</TR>
		<TR>
			<TD style="WIDTH: 130px"></TD>
			<TD colSpan="2"></TD>
		</TR>
		<TR align=left>
			<TD style="WIDTH: 176px; HEIGHT: 3px" colSpan="3"><asp:label CssClass="Content Color2" id="lblCustom" runat="server" Visible="False"></asp:label></TD>
		</TR>
		<TR>
			<TD style="WIDTH: 176px" colSpan="3"><asp:textbox CssClass="InputForm" id="txtCustom" runat="server" Visible="False" Width="470px" size="4" TextMode="MultiLine" Rows="3"></asp:textbox>
			</TD>
		</TR>
		<TR align=left>
			<TD><asp:label id="lblOrderID" runat="server" Visible="False"></asp:label></TD>
			<TD></TD>
			<TD style="HEIGHT: 18px"></TD>
		</TR>
		<TR align=left>
			<TD></TD>
			<TD colSpan="2"><INPUT class="Content Color2" id="btnPreview" onclick="PreviewEmail()" type="button" value="Preview Email">&nbsp;
				<asp:button CssClass="Content Color2" id="btnEmail" runat="server" Text="Send Email" Width="105px" onclick="btnEmail_Click"></asp:button></TD>
		</TR>
		<TR>
			<TD align="center" colSpan="3"><asp:label CssClass="Content Color3" id="lblMessage" runat="server"></asp:label></TD>
		</TR>
	</TABLE>
</FIELDSET>
