namespace localhost.Administrator
{
	using System;
	using System.Configuration;
	using System.Data;
	using System.Drawing;
	using System.Web;    
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Data.SqlClient;
	using localhost;
	using System.Web.Mail;
	using System.Text;
	using System.Collections;
	using TPE.Utility;

	/// <summary>
	///		Summary description for Shipment_Documents.
	/// </summary>
	public partial class Shipment_Documents : System.Web.UI.UserControl
	{

		protected void Page_Load(object sender, System.EventArgs e)
		{
			string dbConnectionString = Application["DBconn"].ToString();
			if (!IsPostBack)
			{
				lblWarning.Text = "";

				if ((Request.QueryString["Id"]!= null)&&(Request.QueryString["Id"].ToString()!=""))
				{
					lblOrderID.Text = Request.QueryString["Id"].ToString();
				}

				if (lblOrderID.Text != "")
				{
					//Remove Title Transfer letter if the size is not a r/c or if the r/c # is empty
					string[] ID = lblOrderID.Text.Split('-');
					
					if (!ShowInvoice(ID[0],ID[1], dbConnectionString))
					{
						ddlType.Items.Remove(ddlType.Items.FindByValue("1"));
					}

					if (!ShowTitleTransferLetter(ID[0],ID[1], dbConnectionString))
					{
						ddlType.Items.Remove(ddlType.Items.FindByValue("6"));
					}

					if ((Request.QueryString["DocumentType"]!=null)&&(Request.QueryString["DocumentType"].ToString()!=""))
					{
						ddlType.SelectedItem.Selected = false;
						ListItem item = ddlType.Items.FindByValue(Request.QueryString["DocumentType"].ToString());
						if (item != null) 
							item.Selected = true;
						else
						{
							lblWarning.Text = "The requested document is not available for that Transaction!";
						}
					}
//					else
//					{
						//Populate the screen using the first item in the ddlType
					PopulateScreen(ddlType.SelectedItem.Value, Application["DBconn"].ToString());
					//					}
				}
			}
		}

		public void PopulateScreen(string typeDocument, string dbConnectionString)
		{
			string[] ID = lblOrderID.Text.Split('-');

			ddlSendTo.Items.Clear();
			ddlCC.Items.Clear();
			
			//Types: 1-Invoice; 2-Purchase Order; 3-Certificate; 4-Sales Confirmation; 
			//	5-Customer Release; 6-HC Title Transfer; 7-Trucking Company; 8-Wahehouse Release
			string emailAdd = "";
            if ((typeDocument == "1") || (typeDocument == "3") || (typeDocument == "4") || (typeDocument == "5") || (typeDocument == "10"))
            {
                emailAdd = getEmailBuyer(ID[0], ID[1]);
            }
            else if (typeDocument == "2")
            {
                
                emailAdd = getEmailSeller(ID[0], ID[1], dbConnectionString);

            }
            else if ((typeDocument == "6") || (typeDocument == "8") || (typeDocument == "9"))
            {
                emailAdd = getEmailWarehouse(ID[0], ID[1], dbConnectionString);
            }

			//Custom field
			txtCustom.Text = "";
			lblCustom.Visible = true;
			txtCustom.Visible = true;

            string shipmentSize = "";

            if ((typeDocument == "1") || (typeDocument == "2") || (typeDocument == "4") || (typeDocument == "10"))
			{
				lblCustom.Text = "Comment:";


                if (typeDocument == "1" || typeDocument == "10")
                {
                    txtCustom.Text = "Please reference transaction number in your payments.";
                }

                if (typeDocument == "2")
                {
                    shipmentSize = getShipmentSize(ID[0], ID[1]);

                    StringBuilder strCustom = new StringBuilder();

                    if (shipmentSize == "190000")
                    {
                        strCustom.Append("Please provide hopper car info ASAP.");
                        strCustom.Append("<P><P><P><P><P><P><P><P><P><P><P>");
                        strCustom.Append("</B><CENTER><I>The Plastics Exchange reserves the right to cancel the order if hopper car is not shipped within 10 days of this order.</I></CENTER>");
                    }    
                    else 
                    {
                        strCustom.Append("Please provide release number ASAP.");
                    }
                    txtCustom.Text = strCustom.ToString();
                }
			}

			else if (typeDocument=="7")
			{
				lblCustom.Text = "Ship Date:";
			}
			else if (typeDocument=="8")
			{
				lblCustom.Text = "Release #:";
			}
			else
			{
				lblCustom.Visible = false;
				txtCustom.Visible = false;
			}

			//this button will be enable just after the user preview the email
			btnEmail.Enabled = false;
			lblMessage.Text = "";

			//if the email of the seller/buyer/warehouse is equal Mike's email, don't add
			//if ((emailAdd!=ConfigurationSettings.AppSettings["strEmailOwner"].ToString())&&(emailAdd!="")) ddlSendTo.Items.Add(emailAdd);

			//Adding other Emails
			ddlCC.Items.Add(""); //There is a option to do not cc anyone.

            ddlSendTo.Items.Add("---------------");

			SqlConnection conn = new SqlConnection(dbConnectionString);
			try
			{
				conn.Open();
				SqlDataReader sqlDTPers = DBLibrary.GetDataReaderFromSelect(conn,"select PERS_MAIL from person WHERE (PERS_TYPE = 'B' OR PERS_TYPE= 'A') and PERS_ENBL = 1");
				while (sqlDTPers.Read())
				{
					ddlSendTo.Items.Add(sqlDTPers["PERS_MAIL"].ToString());
					ddlCC.Items.Add(sqlDTPers["PERS_MAIL"].ToString());
				}
				sqlDTPers.Close();
			}
			finally 
			{
				conn.Close();
			}

			//Selecting email user logged on in CC field
			string emailAdminBroker = getEmailUser(Session["ID"].ToString(),dbConnectionString);
			ListItem itemCC = ddlCC.Items.FindByText(emailAdminBroker);
			if (itemCC!=null)
			{
				itemCC.Selected = true;
			}

			Session["EmailBody_Shipment_Documents_Type"] = ddlType.SelectedValue;
			Session["EmailBody_Shipment_Documents_TransactionID"] = lblOrderID.Text;  
            //Larry - record the info about the metric when the page load
            Session["EmailBody_Shipment_Documents_Metric"] = cbMetric.Checked;
		}

        
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		#region Certificate HTML
		public string CertificateHTML(string shipmentOrderID, string shipmentSKU, string DBConn)
		{
			StringBuilder Email = new StringBuilder("");
			SqlConnection conn = new SqlConnection(DBConn);
		
			//Gathering info about Person
			string SQLStatement = "Select * from PERSON WHERE PERS_ID=(SELECT SHIPMENT_BUYR FROM SHIPMENT WHERE SHIPMENT_ID = (Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID = @shipmentOrderID and SHIPMENT_SKU =@shipmentSKU))";
			Hashtable param = new Hashtable();
			param.Add("@shipmentOrderID ",shipmentOrderID );
			param.Add("@shipmentSKU",shipmentSKU);
			try
			{
				conn.Open();
				string personFirstName = String.Empty;
				string personLastName = String.Empty;
				string personTitle = String.Empty;

				SqlDataReader dtPerson = DBLibrary.GetDataReaderFromSelect(conn,SQLStatement,param);
				try
				{
					dtPerson.Read();
					personFirstName = HelperFunction.getSafeStringFromDB(dtPerson["PERS_FRST_NAME"]);
					personLastName = HelperFunction.getSafeStringFromDB(dtPerson["PERS_LAST_NAME"]);
					personTitle = HelperFunction.getSafeStringFromDB(dtPerson["PERS_TITL"]);
				}
				finally
				{
					dtPerson.Close();
				}

				//Gathering info about Shipment
				string companyName = String.Empty;
				string placeAddressOne = String.Empty;
				string placeZip = String.Empty;
				string placeCityState = String.Empty;

                SQLStatement = "SELECT (SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=PLAC_COMP),PLAC_ADDR_ONE,PLAC_ADDR_TWO,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL),PLAC_ZIP,(SELECT (SELECT CTRY_NAME FROM COUNTRY WHERE CTRY_CODE=LOCL_CTRY) FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL),PLAC_ADDR_THREE FROM PLACE WHERE PLAC_TYPE='H' AND PLAC_COMP=(SELECT (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR) FROM SHIPMENT WHERE SHIPMENT_ORDR_ID = @shipmentOrderID and SHIPMENT_SKU =@shipmentSKU)";
				SqlDataReader dtShipment = DBLibrary.GetDataReaderFromSelect(conn,SQLStatement,param);
				try
				{
					dtShipment.Read();
					companyName = HelperFunction.getSafeStringFromDB(dtShipment[0]);
					placeAddressOne = HelperFunction.getSafeStringFromDB(dtShipment["PLAC_ADDR_ONE"]);
					placeZip = HelperFunction.getSafeStringFromDB(dtShipment["PLAC_ZIP"]);
					placeCityState = HelperFunction.getSafeStringFromDB(dtShipment[3]);
				}
				finally
				{
					dtShipment.Close();
				}
            
				//Gathering info about Orders
				string orderID = String.Empty;
				bool orderIsSpot = false;
				string orderProdutoSpot = String.Empty;
				string orderDensity = String.Empty;
				string orderMelt = String.Empty;
				string orderContract = String.Empty;
				string shipmentSize = String.Empty;
				string orderPrime = String.Empty;
                string orderDate = String.Empty;

                SqlDataReader dtOrder = DBLibrary.GetDataReaderFromSelect(conn, "SELECT *, PROD_SPOT = ISNULL(shipment_product_spot,ORDR_PROD_SPOT), Cert_Date = LEFT(CONVERT(varchar(14),ISNULL(SHIPMENT_TRANSACTION_DATE,ORDR_DATE),101), 10) FROM ORDERS,SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ORDR_ID = @shipmentOrderID and SHIPMENT_SKU = @shipmentSKU", param);
				try
				{
					dtOrder.Read();
					orderID = HelperFunction.getSafeStringFromDB(dtOrder["ORDR_ID"]);
					orderIsSpot = System.Convert.ToBoolean(dtOrder["ORDR_ISSPOT"]);
                    orderProdutoSpot = HelperFunction.getSafeStringFromDB(dtOrder["PROD_SPOT"]);
					orderDensity = HelperFunction.getSafeStringFromDB(dtOrder["ORDR_DENS"]);
					orderMelt = HelperFunction.getSafeStringFromDB(dtOrder["ORDR_MELT"]);
					orderContract = HelperFunction.getSafeStringFromDB(dtOrder["ORDR_CONT"]);
					shipmentSize = HelperFunction.getSafeStringFromDB(dtOrder["SHIPMENT_SIZE"]);
					orderPrime = HelperFunction.getSafeStringFromDB(dtOrder["ORDR_PRIME"]);
                    orderDate = HelperFunction.getSafeStringFromDB(dtOrder["Cert_Date"]);

				}
				finally
				{
					dtOrder.Close();
				}
				
				//Gathering Order Date
                //string orderDate = String.Empty;

                //param.Clear();
                //param.Add("@shipmentOrderID ",shipmentOrderID );
                //SqlDataReader dtOrderDate = DBLibrary.GetDataReaderFromSelect(conn, "SELECT LEFT(CONVERT(varchar(14),ISNULL(SHIPMENT_TRANSACTION_DATE,ORDR_DATE),101), 10) FROM ORDERS, SHIPMENT WHERE ORDR_ID=@shipmentOrderID", param);
                //try
                //{
                //    dtOrderDate.Read();
                //    orderDate = HelperFunction.getSafeStringFromDB(dtOrderDate[0]);
                //}
                //finally
                //{
                //    dtOrderDate.Close();
                //}

				//Creating email Body
				Email.Append(HTMLHeader());

                Email.Append("<b><CENTER>CERTIFICATE OF ANALYSIS</CENTER></B>");
                Email.Append("<br></p><br><br><br>");

                Email.Append(DateTime.Now.ToShortDateString());
                
				//Email.Append("<br>" + personFirstName + "&nbsp;" + personLastName);
				Email.Append("<br>" + personTitle);
				Email.Append("<br>" + companyName);
				Email.Append("<br>" + placeAddressOne);
				Email.Append("<br>" + placeCityState);
				Email.Append("&nbsp;" + placeZip);
				Email.Append("<br><br><br>" + bl());

				Email.Append("Dear " + personFirstName + ",<br><br>" + bl());
				Email.Append("This letter certifies that the resin sold to&nbsp;" + companyName + " on<font color=navy>&nbsp;");
				Email.Append(orderDate);
				Email.Append("</font>, referenced by transaction #" + orderID+"-"+shipmentSKU);

				//190000-pound is railcar, display railcar # if exist
				if (shipmentSize=="190000")
				{

					param.Clear();
					param.Add("@orderID",orderID);
					SqlDataReader dtContentSHIP = DBLibrary.GetDataReaderFromSelect(conn,"SELECT SHIPMENT_COMMENT FROM SHIPMENT WHERE SHIPMENT_ORDR_ID=@orderID",param);
					try
					{
						dtContentSHIP.Read();
						if (dtContentSHIP.HasRows)
						{
							string shipmentComment = HelperFunction.getSafeStringFromDB(dtContentSHIP["SHIPMENT_COMMENT"]);
							if (shipmentComment!= "")
							{
								Email.Append(", r/c # ");
								dtContentSHIP.Close();
								dtContentSHIP = DBLibrary.GetDataReaderFromSelect(conn,"SELECT SHIPMENT_COMMENT FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='" + orderID + "'");
								while(dtContentSHIP.Read())
								{
									Email.Append(HelperFunction.getSafeStringFromDB(dtContentSHIP["SHIPMENT_COMMENT"])+ "<br>");
								}
							}
						}
					}
					finally
					{
						dtContentSHIP.Close();
					}
				}

				//if it's spot order, display Dens, Melt
				//only spot odder has Dens, Melt, Adds
				if (orderIsSpot)
				{
					Email.Append(",&nbsp;is " + orderProdutoSpot.Trim());
					if (orderPrime=="True")
					{
						Email.Append("&nbsp;and conforms to the producer's prime specification");
					}
					Email.Append(".<br><br>" + bl());
                    Email.Append("Nominal Specs<br>");
					if (orderDensity != "") Email.Append("Density (ASTM D1505)<b>&nbsp;&nbsp;&nbsp;" + orderDensity + "</b>g/cm3&nbsp;&nbsp;&nbsp;ASTM D1505<br>");
					if (orderMelt!= "") Email.Append("Melt Flow Rate (190C/2.16 kg - E) <b>&nbsp;&nbsp;&nbsp;" +  orderMelt + "</b>g/10min&nbsp;&nbsp;&nbsp;ASTM D1238<br>");
				}
				else
				{
					//if it's not spot order, have to find order details from CONTRACT table
					param.Clear();
					param.Add("@orderContract",orderContract);
					SqlDataReader dtContract = DBLibrary.GetDataReaderFromSelect(conn,"SELECT * FROM CONTRACT WHERE CONT_ID=@orderContract", param);
					try
					{
						dtContract.Read();
						Email.Append(",&nbsp;is " + dtContract["CONT_LABL"].ToString().Trim());
						if (orderPrime=="True")
						{
							Email.Append("&nbsp;and conforms to the producer's prime specification");
						}
						Email.Append(".<br><br>" + bl());
						string contractDesnsityTGT = HelperFunction.getSafeStringFromDB(dtContract["CONT_DENS_TGT"]);
						string contractMeltTGT = HelperFunction.getSafeStringFromDB(dtContract["CONT_MELT_TGT"]);
                        Email.Append("Nominal Specs<br>");
						if (contractDesnsityTGT!="") Email.Append("Density (ASTM D1505)<b>&nbsp;&nbsp;&nbsp;" + contractDesnsityTGT + "</b>g/cm3&nbsp;&nbsp;&nbsp;ASTM D1505<br>");
						if (contractMeltTGT!="") Email.Append("Melt Flow Rate (190C/2.16 kg - E) <b>&nbsp;&nbsp;&nbsp;" + contractMeltTGT + "</b>g/10min&nbsp;&nbsp;&nbsp;ASTM D1238<br>");
					}
					finally
					{
						dtContract.Close();
					}
				}
				
				Email.Append(HTMLFooter("Michael Greenberg, CEO"));
			}
			finally
			{
				conn.Close();
			}
			return Email.ToString();
		}
		#endregion

		#region HC Title Transfer
		private string TitleTransfer(string shipmentOrderID, string shipmentSKU, string dbConnectionString, string Name)
		{
			System.Text.StringBuilder sb=new System.Text.StringBuilder();
			SqlConnection conn = new SqlConnection(dbConnectionString);
			//string OrderId = ShipmentID(shipmentOrderID,shipmentSKU);
			string com_name="";
			string LocationName="";
			string Shipment_Comment="";
			string loctype="";
			string locContact="";
			string locPhone="";
			string sqlBuyer="select com_name=(select companyName=(select comp_name from company where comp_id=pers_comp) from person where pers_id=shipment_buyr) from shipment where shipment_ordr_id=@shipmentOrderID and shipment_sku = @shipmentSKU";
			Hashtable param = new Hashtable();
			param.Add("@shipmentOrderID",shipmentOrderID.ToString() );
			param.Add("@shipmentSKU",shipmentSKU.ToString().Trim());
			try
			{
				conn.Open();

				SqlDataReader rd= DBLibrary.GetDataReaderFromSelect(conn,sqlBuyer,param);
				try
				{
					if(rd.Read())
					{
						com_name=rd["com_name"].ToString().Trim();//buryer
					}
				}
				finally
				{
					rd.Close();
				}

				string sqlOrderTo="select (L.LOCL_CITY+', '+L.LOCL_STAT) LocationName, P.PLAC_TYPE locType, PLAC_PERS, PLAC_PHON, PLAC_INST from SHIPMENT S, LOCALITY L, PLACE P " +
					"WHERE S.SHIPMENT_TO = P.PLAC_ID and P.PLAC_LOCL = L.LOCL_ID and S.SHIPMENT_ORDR_ID=@shipmentOrderID and S.SHIPMENT_SKU = @shipmentSKU";
				rd= DBLibrary.GetDataReaderFromSelect(conn,sqlOrderTo, param);
				try
				{
					if(rd.Read())
					{
						//LocationName=rd["LocationName"].ToString().Trim();
						LocationName=HelperFunction.getSafeStringFromDB(rd["PLAC_INST"]);
						locContact = HelperFunction.getSafeStringFromDB(rd["plac_pers"]);
						locPhone = HelperFunction.getSafeStringFromDB(rd["plac_phon"]);

						if(rd["locType"]!=null)
						{
							if ((string)rd["locType"]=="H")
							{
								loctype = "Headquarters";
							}
							else if ((string)rd["locType"]=="S")
							{
								loctype = "Shipping Point";
							}
							else if ((string)rd["locType"]=="D")
							{
								loctype = "Delivery Point";
							}
							else
							{
								loctype= "Warehouse";
							}
						}
						else
						{
							loctype="warehouse";
						}
					}
				}
				finally
				{
					rd.Close();
				}
				string sqlShip="SELECT SHIPMENT_COMMENT FROM SHIPMENT WHERE SHIPMENT_ORDR_ID=@shipmentOrderID AND SHIPMENT_SKU = @shipmentSKU";
				rd= DBLibrary.GetDataReaderFromSelect(conn,sqlShip,param);
				try
				{
					if(rd.Read())
					{
						Shipment_Comment = HelperFunction.getSafeStringFromDB(rd["SHIPMENT_COMMENT"]);
					}
				}
				finally
				{
					rd.Close();
				}
				//creat email body
				sb.Append(HTMLHeader());

				sb.Append("Date: " + DateTime.Today.ToShortDateString() + "<br>");
				sb.Append("To: "+LocationName+"(<font face=arial black color=red>"+loctype+"</font>)<br>");
				sb.Append("Tel: " + locPhone + "<br>");
				sb.Append("Fax: <br><br>");
				sb.Append("Dear " + locContact + ",<br><br>");
				sb.Append(@"Upon receipt of Railcar #");
				sb.Append(Shipment_Comment.ToString().Trim());
				sb.Append(@", shipped by " + getCompanyNameSeller(shipmentOrderID,shipmentSKU, dbConnectionString) + " please accept it on behalf of The Plastics Exchange and immediately transfer title the Railcar to the account of&nbsp");
				sb.Append("<b>" + getCompanyNameBuyer(shipmentOrderID,shipmentSKU, dbConnectionString) + "</b>");
				sb.Append(@", please notify&nbsp;");
				sb.Append("<b>" + getCompanyNameBuyer(shipmentOrderID,shipmentSKU, dbConnectionString) + "</b>");
				sb.Append(@" to accept the Hopper car when it arrives.<br><br>");
				sb.Append(@"<br>We would appreciate if you maintain the anonymity of these parties.");
				sb.Append(@"<br><br><br>Thank you very much.");
				
				sb.Append(HTMLFooter(Name + "<br>Logistics Coordinator"));
				
			}
			finally
			{
				conn.Close();
			}
			return sb.ToString();
		}

		#endregion HC Title Transfer

		#region Warehouse Release
		private string WarehouseRelease(string shipmentOrderID, string shipmentSKU, string CustomField, string dbConnectionString, string Name)
		{
			System.Text.StringBuilder sb=new System.Text.StringBuilder("");

			SqlConnection conn = new SqlConnection(dbConnectionString);
			string quantity="";
			string product="";
			string releaseNumberFromSeller = BreakMultilineFieldHTML(CustomField);
			string LocationName="";
			string locContact="";
			string locPhone="";
			string locFax="";

			string shipmentID = ShipmentID(shipmentOrderID.ToString().Trim(),shipmentSKU.ToString().Trim(), dbConnectionString);
			string ShipMethod="";
			Hashtable param = new Hashtable();
			param.Add("@shipmentID",shipmentID);
			try
			{
				conn.Open();

				SqlDataReader drShipment = DBLibrary.GetDataReaderFromSelect(conn,"SELECT SHIPMENT_FOB FROM SHIPMENT WHERE SHIPMENT_ID=@shipmentID",param);
				try
				{
					if(drShipment.Read())
					{
						ShipMethod = HelperFunction.getSafeStringFromDB(drShipment["SHIPMENT_FOB"]);
					}
				}
				finally
				{
					drShipment.Close();
				}

				StringBuilder sbSQLOrderTo= new StringBuilder();

				sbSQLOrderTo.Append("select (L.LOCL_CITY+', '+L.LOCL_STAT) LocationName, P.PLAC_TYPE locType, PLAC_PERS, PLAC_PHON, PLAC_INST, PLAC_FAX ");
				sbSQLOrderTo.Append("from SHIPMENT S, LOCALITY L, PLACE P ");
				sbSQLOrderTo.Append((ShipMethod == "FOB/Delivered" ? "where S.SHIPMENT_TO = P.PLAC_ID " : "where S.SHIPMENT_FROM = P.PLAC_ID "));
				sbSQLOrderTo.Append(" and P.PLAC_LOCL = L.LOCL_ID and S.SHIPMENT_ORDR_ID=@shipmentOrderID and S.SHIPMENT_SKU = @shipmentSKU");
				string sqlOrderTo="select (L.LOCL_CITY+', '+L.LOCL_STAT) LocationName, P.PLAC_TYPE locType, PLAC_PERS, PLAC_PHON, PLAC_INST, PLAC_FAX " + 
					"from SHIPMENT S, LOCALITY L, PLACE P " +
					(ShipMethod == "FOB/Delivered" ? "where S.SHIPMENT_TO = P.PLAC_ID " : "where S.SHIPMENT_FROM = P.PLAC_ID ") +
					" and P.PLAC_LOCL = L.LOCL_ID " +
					"  and S.SHIPMENT_ORDR_ID='"+shipmentOrderID.ToString().Trim() + "'" + 
					"  and S.SHIPMENT_SKU = '" + shipmentSKU.ToString().Trim() + "'";
				param.Clear();
				param.Add("@shipmentOrderID",shipmentOrderID.ToString().Trim());
				param.Add("@shipmentSKU",shipmentSKU.ToString().Trim());
				SqlDataReader rd= DBLibrary.GetDataReaderFromSelect(conn,sqlOrderTo,param);
				try
				{
					if(rd.Read())
					{
						LocationName=HelperFunction.getSafeStringFromDB(rd["PLAC_INST"]);
						locContact = HelperFunction.getSafeStringFromDB(rd["plac_pers"]);
						locPhone = HelperFunction.getSafeStringFromDB(rd["plac_phon"]);
						locFax = HelperFunction.getSafeStringFromDB(rd["plac_fax"]);
					}
				}
				finally
				{
					rd.Close();
				}

				//Gathering info about Orders
				param.Clear();
				param.Add("@shipmentOrderID",shipmentOrderID.Trim());
				param.Add("@shipmentSKU",shipmentSKU.Trim());

                SqlDataReader dtOrder = DBLibrary.GetDataReaderFromSelect(conn, "SELECT *, PROD_SPOT = ISNULL(shipment_product_spot,ORDR_PROD_SPOT) FROM ORDERS,SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ORDR_ID = @shipmentOrderID and SHIPMENT_SKU =@shipmentSKU", param);
				try
				{
					dtOrder.Read();
					product = HelperFunction.getSafeStringFromDB(dtOrder["PROD_SPOT"]);
					quantity = HelperFunction.getSafeStringFromDB(dtOrder["SHIPMENT_WEIGHT"]) + " lbs";
					if (quantity.Trim() == " lbs") quantity = "0 lbs";
				}
				finally
				{
					dtOrder.Close();
				}
				
				//creat email body
				
				sb.Append(HTMLHeader());

				sb.Append("Date: " + DateTime.Today.ToShortDateString() + "<br>");
				sb.Append("To: "+LocationName+"<br>");
				sb.Append("Fax: " + locFax + "<br><br><br>");
				sb.Append("Dear " + locContact + ",<br><br>");
				
				sb.Append(getCompanyNameSeller(shipmentOrderID,shipmentSKU, dbConnectionString));
				sb.Append(" has released ");
				sb.Append(quantity + " - " + product);
				sb.Append(" to The Plastics Exchange, under the release #" + releaseNumberFromSeller + ", please release this same material to ");
				sb.Append(getCompanyNameBuyer(shipmentOrderID,shipmentSKU, dbConnectionString));
				sb.Append(@" under the release #" + shipmentOrderID + "-" + shipmentSKU + ".");
				sb.Append(@"<br><br>We would appreciate if " + LocationName + " would maintain the anonymity of all parties.");
				sb.Append(@"<br><br>Please fax us copy of straight Bill of Lading upon pick up of material");
				sb.Append(@"<br><br>Please confirm receipt of this request.");
				sb.Append(@"<br><br>Thank you very much.");
				
				//sb.Append(HTMLFooter(httpContext.Session["Name"].ToString() + "<br>Logistics Coordinator"));
				sb.Append(HTMLFooter(Name + "<br>Logistics Coordinator"));
			}
			finally
			{
				conn.Close();
			}
			return sb.ToString();
		}
		#endregion Warehouse Release
		
		#region Inventory Release
		private string InventoryRelease(string shipmentOrderID, string shipmentSKU, string dbConnectionString, string Name)
		{
			StringBuilder sb = new StringBuilder();

			string quantity="";
			string product="";
			string LocationName="";
			string locContact="";
			string locPhone="";
			string locFax="";

			string shipmentID = ShipmentID(shipmentOrderID.ToString().Trim(),shipmentSKU.ToString().Trim(), dbConnectionString);
			string ShipMethod="";
			SqlConnection conn = new SqlConnection(dbConnectionString);
			Hashtable param = new Hashtable();
			param.Add("@shipmentID",shipmentID);
			try
			{
				conn.Open();
				SqlDataReader drShipment = DBLibrary.GetDataReaderFromSelect(conn,"SELECT SHIPMENT_FOB FROM SHIPMENT WHERE SHIPMENT_ID=@shipmentID",param);
				try
				{
					if(drShipment.Read())
					{
						ShipMethod = HelperFunction.getSafeStringFromDB(drShipment["SHIPMENT_FOB"]);
					}
				}
				finally
				{
					drShipment.Close();
				}
				
				string sqlOrderTo="select (L.LOCL_CITY+', '+L.LOCL_STAT) LocationName, P.PLAC_TYPE locType, PLAC_PERS, PLAC_PHON, PLAC_INST, PLAC_FAX " + 
					"from SHIPMENT S, LOCALITY L, PLACE P " +
					(ShipMethod == "FOB/Delivered" ? "where S.SHIPMENT_TO = P.PLAC_ID " : "where S.SHIPMENT_FROM = P.PLAC_ID ") +
					" and P.PLAC_LOCL = L.LOCL_ID and S.SHIPMENT_ORDR_ID=@shipmentOrderID and S.SHIPMENT_SKU = @shipmentSKU";
				param.Clear();
				param.Add("@shipmentOrderID",shipmentOrderID.ToString().Trim()); 
				param.Add("@shipmentSKU",shipmentSKU.ToString().Trim());		
				SqlDataReader rd= DBLibrary.GetDataReaderFromSelect(conn,sqlOrderTo,param);
				try
				{
					if(rd.Read())
					{
						LocationName=HelperFunction.getSafeStringFromDB(rd["PLAC_INST"]);
						locContact = HelperFunction.getSafeStringFromDB(rd["plac_pers"]);
						locPhone = HelperFunction.getSafeStringFromDB(rd["plac_phon"]);
						locFax = HelperFunction.getSafeStringFromDB(rd["plac_fax"]);
					}
				}
				finally
				{
					rd.Close();
				}

				//Gathering info about Orders

				// param already set
                SqlDataReader dtOrder = DBLibrary.GetDataReaderFromSelect(conn, "SELECT *,PROD_SPOT = ISNULL(shipment_product_spot,ORDR_PROD_SPOT) FROM ORDERS,SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ORDR_ID = @shipmentOrderID and SHIPMENT_SKU = @shipmentSKU", param);			
				try
				{
					dtOrder.Read();
					product = HelperFunction.getSafeStringFromDB(dtOrder["PROD_SPOT"]);
					quantity = HelperFunction.getSafeStringFromDB(dtOrder["SHIPMENT_WEIGHT"]) + " lbs";
					if (quantity.Trim() == " lbs") quantity = "0 lbs";
				}
				finally
				{
					dtOrder.Close();
				}
				
				//creat email body
				sb.Append(HTMLHeader());

				sb.Append("Date: " + DateTime.Today.ToShortDateString() + "<br>");
				sb.Append("To: "+LocationName+"<br>");
				sb.Append("Fax: " + locFax + "<br><br><br>");
				sb.Append("Dear " + locContact + ",<br><br>");
				
				sb.Append("The Plastics Exchange has the following inventory with " + LocationName + ":");
				sb.Append("<br><br>" + quantity + " - " + product + ",");
				sb.Append("<br><br>Please release this material to " + getCompanyNameBuyer(shipmentOrderID,shipmentSKU, dbConnectionString));
				sb.Append(" under the release #" + shipmentOrderID + "-" + shipmentSKU + ".");
				sb.Append("<br><br>Please fax us copy of the straight Bill of Lading upon their pick up of the material.");
				sb.Append("<br><br>We would appreciate if you would confirm the receipt of this request.");
				sb.Append(@"<br><br><br>Thank you very much.");
				
				sb.Append(HTMLFooter(Name + "<br>Logistics Coordinator"));
			}
			finally
			{
				conn.Close();
			}
			return sb.ToString();
		}
		#endregion Inventory Release

		#region Release Customer
		private string ReleaseCustomer(string shipmentOrderID, string shipmentSKU, string dbConnectionString, string Name)
		{
			StringBuilder sb = new StringBuilder();
			Hashtable param = new Hashtable();

			SqlConnection conn = new SqlConnection(dbConnectionString);
            string strOrderNumber = shipmentOrderID + "-" + shipmentSKU;
	
			shipmentOrderID = shipmentOrderID.Trim();
			shipmentSKU = shipmentSKU.Trim();

			//Gathering info about Person
			param.Add("@shipmentOrderID",shipmentOrderID);
			param.Add("@shipmentSKU",shipmentSKU);
			string strBuyerName = String.Empty;
			string strBuyerFax = String.Empty;
			try
			{
				conn.Open();
				SqlDataReader dtPerson = DBLibrary.GetDataReaderFromSelect(conn,"Select * from PERSON WHERE PERS_ID=(SELECT SHIPMENT_BUYR FROM SHIPMENT WHERE SHIPMENT_ID = (Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID = @shipmentOrderID and SHIPMENT_SKU =@shipmentSKU))",param);
				try
				{
					dtPerson.Read();
					strBuyerName = HelperFunction.getSafeStringFromDB(dtPerson["PERS_FRST_NAME"]) + " " + HelperFunction.getSafeStringFromDB(dtPerson["PERS_LAST_NAME"]);
					strBuyerFax = HelperFunction.getSafeStringFromDB(dtPerson["PERS_FAX"]);
				}
				finally
				{	
					dtPerson.Close();
				}

				//Gathering info about Shipment
                SqlDataReader dtShipment = DBLibrary.GetDataReaderFromSelect(conn, "SELECT (SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=PLAC_COMP),PLAC_ADDR_ONE,PLAC_ADDR_TWO,(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL),PLAC_ZIP,(SELECT (SELECT CTRY_NAME FROM COUNTRY WHERE CTRY_CODE=LOCL_CTRY) FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL),PLAC_ADDR_THREE FROM PLACE WHERE PLAC_TYPE='H' AND PLAC_COMP=(SELECT (SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR) FROM SHIPMENT WHERE SHIPMENT_ORDR_ID = @shipmentOrderID and SHIPMENT_SKU = @shipmentSKU)", param);
				string strBuyerCompanyName = String.Empty;
				try
				{
					dtShipment.Read();
					strBuyerCompanyName = HelperFunction.getSafeStringFromDB(dtShipment[0]);
				}
				finally
				{
					dtShipment.Close();
				}
				
				string shipmentID = ShipmentID(shipmentOrderID.ToString().Trim(),shipmentSKU.ToString().Trim(), dbConnectionString);
				string ShipMethod=String.Empty;
				param.Clear();
				param.Add("@shipmentID",shipmentID);
				SqlDataReader drShipment = DBLibrary.GetDataReaderFromSelect(conn,"SELECT SHIPMENT_FOB FROM SHIPMENT WHERE SHIPMENT_ID=@shipmentID",param);
				try
				{
					if(drShipment.Read())
					{
						ShipMethod = HelperFunction.getSafeStringFromDB(drShipment["SHIPMENT_FOB"]);
					}
				}
				finally
				{
					drShipment.Close();
				}

                string sqlOrderTo = "select (L.LOCL_CITY+', '+L.LOCL_STAT) LOCATION_NAME, PLAC_PERS, PLAC_PHON, PLAC_INST, PLAC_ADDR_ONE, PLAC_ADDR_TWO, PLAC_ADDR_THREE, PLAC_ZIP " + 
					"from SHIPMENT S, LOCALITY L, PLACE P " +
					(ShipMethod == "FOB/Shipping" ? "where S.SHIPMENT_FROM = P.PLAC_ID " : "where S.SHIPMENT_TO = P.PLAC_ID ") +
					"	and P.PLAC_LOCL = L.LOCL_ID and S.SHIPMENT_ORDR_ID=@shipmentOrderID" + 
					"  and S.SHIPMENT_SKU = @shipmentSKU";
				param.Clear();
				param.Add("@shipmentOrderID",shipmentOrderID);
				param.Add("@shipmentSKU",shipmentSKU);
				SqlDataReader dtLocality = DBLibrary.GetDataReaderFromSelect(conn,sqlOrderTo,param);
				string strPickupLocationName = String.Empty;
				string strPhoneNumber = String.Empty;
				string strContact = String.Empty;
				string strPickupLocationAddress = String.Empty;
				string strPickupLocationAddress2 = String.Empty;
                string strPickupLocationAddress3 = String.Empty;
				string strPickupLocationZipCode = String.Empty;
				string strPickupLocationCityState = String.Empty;
				try
				{
					dtLocality.Read();
					strPickupLocationName = HelperFunction.getSafeStringFromDB(dtLocality["PLAC_INST"]);
					strPhoneNumber = HelperFunction.getSafeStringFromDB(dtLocality["PLAC_PHON"]);
					strContact = HelperFunction.getSafeStringFromDB(dtLocality["PLAC_PERS"]);
					strPickupLocationAddress = HelperFunction.getSafeStringFromDB(dtLocality["PLAC_ADDR_ONE"]);
					strPickupLocationAddress2 = HelperFunction.getSafeStringFromDB(dtLocality["PLAC_ADDR_TWO"]);
                    strPickupLocationAddress3 = HelperFunction.getSafeStringFromDB(dtLocality["PLAC_ADDR_THREE"]);
					strPickupLocationZipCode = HelperFunction.getSafeStringFromDB(dtLocality["PLAC_ZIP"]);
					strPickupLocationCityState = HelperFunction.getSafeStringFromDB(dtLocality["LOCATION_NAME"]);
				}
				finally
				{
					dtLocality.Close();
				}
	            
				//Gathering info about Orders
				// param already set
                SqlDataReader dtOrder = DBLibrary.GetDataReaderFromSelect(conn, "SELECT *,PROD_SPOT = ISNULL(shipment_product_spot,ORDR_PROD_SPOT) FROM ORDERS,SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ORDR_ID = @shipmentOrderID and SHIPMENT_SKU =@shipmentSKU", param);
				string strProductDescription = String.Empty;
				string strQuantity = String.Empty;
				string strPONumber = String.Empty;
				try
				{
					dtOrder.Read();
					strProductDescription = HelperFunction.getSafeStringFromDB(dtOrder["PROD_SPOT"]);
					strQuantity = HelperFunction.getSafeStringFromDB(dtOrder["SHIPMENT_WEIGHT"]) + " lbs";
					strPONumber = HelperFunction.getSafeStringFromDB(dtOrder["SHIPMENT_PO_NUM"]);
				}
				finally
				{
					dtOrder.Close();
				}

				string HTMLSpaces = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

				sb.Append(HTMLHeader());

				sb.Append("Date: " + DateTime.Today.ToShortDateString() + "<br>");
				sb.Append("To: " + strBuyerName + "<br>");
				sb.Append("Fax: " + strBuyerFax + "<br><br>");

				sb.Append("<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
				sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Release Information</b><br><br>");

				sb.Append("Customer's Name: " + strBuyerCompanyName + "<BR><BR>" + bl());
				sb.Append("Customer PO #: " + strPONumber + "<BR><BR>" + bl());
				sb.Append("TPE Reference #: " + strOrderNumber + "<BR><BR>" + bl());
				sb.Append("Product: " + strProductDescription + "<BR>" + bl());
				sb.Append("Quantity: " + strQuantity + "<BR><BR>" + bl());
				sb.Append("Pick up Location: " + strPickupLocationName + "<BR>" + bl());
				sb.Append(HTMLSpaces + HTMLSpaces + HTMLSpaces + strPickupLocationAddress + "<BR>" + bl());
				if (strPickupLocationAddress2.Trim().Length>0) sb.Append(HTMLSpaces + HTMLSpaces + HTMLSpaces + strPickupLocationAddress2 + "<BR>" + bl());
                if (strPickupLocationAddress3.Trim().Length>0) sb.Append(HTMLSpaces + HTMLSpaces + HTMLSpaces + strPickupLocationAddress3 + "<BR>" + bl());
				sb.Append(HTMLSpaces + HTMLSpaces + HTMLSpaces + strPickupLocationCityState + " - " + strPickupLocationZipCode + "<BR>" + bl());
				sb.Append("<BR>Phone #: " + strPhoneNumber + "<BR><BR>" + bl());
				sb.Append("Contact: " + strContact + "<BR><BR>" + bl());
				sb.Append("TPE Release #: " + strOrderNumber + "<BR><BR><BR>" + bl());
				
				sb.Append("Feel free to contact me if you have any other questions." + bl());

				//sb.Append(HTMLFooter(context.Session["Name"].ToString() + "<br>Logistics Coordinator"));
				sb.Append(HTMLFooter(Name + "<br>Logistics Coordinator"));
			}
			finally
			{
				conn.Close();
			}
			return sb.ToString();
		}
		#endregion Release Customer

		#region Invoice, Purchase Order, Sales Confirmation
		
            private string InvoicePOSalesConfirmation(bool metric, string shipmentOrderID, string shipmentSKU, string DocumentType, string CustomField, string dbConnectionString)
		        {
			SqlConnection conn = new SqlConnection(dbConnectionString);
			string ShipmentID = "";

            Boolean bAddInvoiceDate = false;
            Boolean bRailCar = false;

			Hashtable param = new Hashtable();

			string strLocation="";
			System.DateTime time;
			
			int terms = 0;

			//Looking for Shipment ID
			try
			{
				conn.Open();
				param.Add("@shipmentOrderID",shipmentOrderID);
				param.Add("@shipmentSKU",shipmentSKU);
				SqlDataReader drShipmentID = DBLibrary.GetDataReaderFromSelect(conn,"Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID=@shipmentOrderID and SHIPMENT_SKU =@shipmentSKU", param);
				try
				{
					drShipmentID.Read();
					ShipmentID = drShipmentID["SHIPMENT_ID"].ToString();
				}
				finally
				{
					drShipmentID.Close();
				}
				
				StringBuilder sbEmail = new StringBuilder("");
				sbEmail.Append(HTMLHeader());
				sbEmail.Append("<table border=0 cellspacing=0 cellpadding=0 width='650' align='center'>");
				sbEmail.Append("<tr><td colspan=3><br><center><SPAN STYLE='BACKGROUND-COLOR:#CCCCCC;FONT: BOLD 10pt ARIAL'>");
				
				//defining title
                if (DocumentType == "PO")
                {
                    sbEmail.Append("PURCHASE ORDER");
                }
                else if (DocumentType == "SC")
                {
                    sbEmail.Append("SALES CONFIRMATION");
                }
                else if (DocumentType == "PFIN")
                {
                    sbEmail.Append("PROFORMA INVOICE");
                    DocumentType = "IN";
                }
                else
                {
                    sbEmail.Append("INVOICE");
                }

				sbEmail.Append("</SPAN></center><br></td></tr>");
				sbEmail.Append("<tr valign=top>");
				
				StringBuilder sbSql = new StringBuilder();
				sbSql.Append("SELECT ");
				sbSql.Append("   COMP_NAME=(SELECT COMP_NAME FROM COMPANY WHERE COMP_ID=PLAC_COMP),");
				sbSql.Append("   PLAC_ADDR_ONE,");
				sbSql.Append("   PLAC_ADDR_TWO,");
				sbSql.Append("   (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL),");
				sbSql.Append("   PLAC_ZIP,");
				sbSql.Append("   (SELECT ");
				sbSql.Append("	 (SELECT CTRY_NAME FROM COUNTRY WHERE CTRY_CODE=LOCL_CTRY) ");
				sbSql.Append("	 FROM LOCALITY");
				sbSql.Append("	 WHERE LOCL_ID=PLAC_LOCL),");
                sbSql.Append("   COMP_id=(SELECT COMP_id FROM COMPANY WHERE COMP_ID=PLAC_COMP),");
                sbSql.Append("   PLAC_ADDR_THREE ");
                sbSql.Append("   FROM PLACE ");
				sbSql.Append("   WHERE PLAC_TYPE='H' ");
				sbSql.Append("   AND PLAC_COMP=(SELECT ");
				sbSql.Append("	(SELECT PERS_COMP FROM PERSON ");
				if(DocumentType=="PO")//select seller's company
				{
					sbSql.Append(" WHERE PERS_ID=SHIPMENT_SELR) ");
				}
				else//select buyer's company
				{
					sbSql.Append(" WHERE PERS_ID=SHIPMENT_BUYR) ");
				}
				sbSql.Append("FROM ORDERS,SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ID=@ShipmentID) ");

				param.Clear();
				param.Add("@ShipmentID",ShipmentID);
				SqlDataReader drOrder = DBLibrary.GetDataReaderFromSelect(conn,sbSql.ToString(),param);


                string CompanyID="";


                if(drOrder.Read())
				{
                    CompanyID = drOrder["COMP_id"].ToString();

					sbEmail.Append("<td><SPAN STYLE='FONT: 8pt ARIAL'><b><i>");
					if(DocumentType == "PO")
					{
						sbEmail.Append("Purchased From:");
						sbEmail.Append("</i></b><br>"+drOrder["COMP_NAME"].ToString()+"<br>"+drOrder["PLAC_ADDR_ONE"].ToString());
					}
					else
					{
						sbEmail.Append("Sold to:</i></b>");
						if(DocumentType == "IN") sbEmail.Append("<br>"+"Attn: <u>Accounts Payable Dept.</u>");
						sbEmail.Append("<br>"+drOrder["COMP_NAME"].ToString()+"<br>"+drOrder["PLAC_ADDR_ONE"].ToString());
					}
					if(drOrder["PLAC_ADDR_TWO"]!=null)
					{
						if(drOrder["PLAC_ADDR_TWO"].ToString().Length!=0)
						{
							sbEmail.Append("<br>"+drOrder["PLAC_ADDR_TWO"].ToString());
						}
					}
                    if (drOrder["PLAC_ADDR_THREE"] != null)
                    {
                        if (drOrder["PLAC_ADDR_THREE"].ToString().Length != 0)
                        {
                            sbEmail.Append("<br>" + drOrder["PLAC_ADDR_THREE"].ToString());
                        }
                    }
                    sbEmail.Append("<br>" + drOrder[3] + "  " + drOrder["PLAC_ZIP"]);
					if (drOrder[5].ToString()!="United States") sbEmail.Append("<br>"+drOrder[5]);
                    sbEmail.Append("</SPAN></td>");
				}
				drOrder.Close();
				
				//determines the ship term is FOB/Shipping or FOB/Delivered or Delivered Tpe
				string ShipMethod="";
				param.Clear();
				param.Add("@ShipmentID",ShipmentID);
				SqlDataReader drShipment = DBLibrary.GetDataReaderFromSelect(conn,"SELECT SHIPMENT_FOB FROM SHIPMENT WHERE SHIPMENT_ID=@ShipmentID",param);
				try
				{
					if(drShipment.Read())
					{
						ShipMethod = HelperFunction.getSafeStringFromDB(drShipment["SHIPMENT_FOB"]);
					}
				}
				finally
				{
					drShipment.Close();
				}

				//Building SQL query into a string(easy to read) returns company, Headquarters address info will be displayed in the right side of the screen
				sbSql = null;
				sbSql = new StringBuilder();
				sbSql.Append("SELECT ");
                sbSql.Append("   PLAC_ADDR_ONE, PLAC_ADDR_TWO, PLAC_ADDR_THREE,");
				sbSql.Append("   PLAC_INST, PLAC_PHON,PLAC_RAIL_NUM,");
				sbSql.Append("   (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) As Location,");
				sbSql.Append("   (SELECT LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) As STATE,");
				sbSql.Append("   (SELECT PLAC_ZIP FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) As Zip,");
				sbSql.Append("   (SELECT ");
				sbSql.Append("	   (SELECT CTRY_NAME FROM COUNTRY WHERE CTRY_CODE=LOCL_CTRY) ");
				sbSql.Append("	    FROM LOCALITY");
				sbSql.Append("	    WHERE LOCL_ID=PLAC_LOCL) As Country");
				sbSql.Append(" FROM ");
				sbSql.Append(" PLACE ");				

				//display address based on ship term for FOB/shipping
				if(ShipMethod=="FOB/Shipping")
				{
					sbSql.Append(" WHERE PLAC_ID=(SELECT SHIPMENT_FROM FROM SHIPMENT WHERE SHIPMENT_ID=@ShipmentID)");
				}
				else //ShipMethod=="FOB/Delivered"
				{
					sbSql.Append(" WHERE PLAC_ID=(SELECT SHIPMENT_TO FROM SHIPMENT WHERE SHIPMENT_ID=@ShipmentID)");
				}

				param.Clear();
				param.Add("@ShipmentID",ShipmentID);
				SqlDataReader drPlace = DBLibrary.GetDataReaderFromSelect(conn,sbSql.ToString(),param);
				sbSql = null;
                				
				string dress2 = String.Empty;
				string PlaceAddOne = String.Empty;
				string PlaceAddTwo = String.Empty;
                string PlaceAddThree = String.Empty;
				string Location = String.Empty;
				string Phone = String.Empty;
				string Zip = String.Empty;
				string RailNumber = String.Empty;
				string Country = String.Empty;
				string State = String.Empty;
				
                try
				{
					drPlace.Read();
					dress2 = HelperFunction.getSafeStringFromDB(drPlace["PLAC_INST"]);
					PlaceAddOne = HelperFunction.getSafeStringFromDB(drPlace["PLAC_ADDR_ONE"]);
					PlaceAddTwo = HelperFunction.getSafeStringFromDB(drPlace["PLAC_ADDR_TWO"]);
                    PlaceAddThree = HelperFunction.getSafeStringFromDB(drPlace["PLAC_ADDR_THREE"]);
					Location = HelperFunction.getSafeStringFromDB(drPlace["Location"]);
					Phone = HelperFunction.getSafeStringFromDB(drPlace["PLAC_PHON"]);
					Zip = HelperFunction.getSafeStringFromDB(drPlace["Zip"]);
					RailNumber = HelperFunction.getSafeStringFromDB(drPlace["PLAC_RAIL_NUM"]);
					Country = HelperFunction.getSafeStringFromDB(drPlace["Country"]);
					State = HelperFunction.getSafeStringFromDB(drPlace["State"]);
				}
				finally
				{
					drPlace.Close();
				}

				sbEmail.Append("<td><SPAN STYLE='FONT: 8pt ARIAL'><b><i>");
				if(DocumentType == "PO")
					if(ShipMethod=="FOB/Shipping")
						sbEmail.Append("Our Pickup:</i></b>");
					else
						if(ShipMethod=="FOB/Delivered")
						sbEmail.Append("Delivered To:</i></b>");
					else
						if(ShipMethod=="Delivered Tpe")
						sbEmail.Append("Our Pickup:</i></b>");
					else
						sbEmail.Append("Our Pickup:</i></b>");
				else
					if(ShipMethod=="FOB/Shipping")
					sbEmail.Append("Your Pickup:</i></b>");
				else
					if(ShipMethod=="FOB/Delivered")
					sbEmail.Append("Delivered To:</i></b>");
				else
					if(ShipMethod=="Delivered Tpe")
					sbEmail.Append("Delivered To:</i></b>");
				else
					sbEmail.Append("Ship to:</i></b>");

				if(ShipMethod=="Delivered Tpe")
				{
					sbSql = new StringBuilder();
					sbSql.Append("SELECT ");
					sbSql.Append("   PLAC_ADDR_ONE, ");
					sbSql.Append("   PLAC_ADDR_TWO,");
                    sbSql.Append("   PLAC_ADDR_THREE,");
					sbSql.Append("   PLAC_INST,");
					sbSql.Append("   PLAC_PHON,");
					sbSql.Append("   PLAC_RAIL_NUM,");
					sbSql.Append("   (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) As Location,");
					sbSql.Append("   (SELECT PLAC_ZIP FROM LOCALITY WHERE LOCL_ID=PLAC_LOCL) As Zip");
					sbSql.Append(" FROM ");
					sbSql.Append(" PLACE ");
					sbSql.Append( " WHERE PLAC_ID=(SELECT SHIPMENT_FROM FROM SHIPMENT WHERE SHIPMENT_ID=@ShipmentID)");
					param.Clear();
					param.Add("@ShipmentID",ShipmentID);
					SqlDataReader drPlace2 = DBLibrary.GetDataReaderFromSelect(conn,sbSql.ToString(),param);
					sbSql = null;
					try
					{
						drPlace2.Read();
						if(DocumentType == "PO")
						{
							if(HelperFunction.getSafeStringFromDB(drPlace2["PLAC_INST"]).Trim().Length>0)
							{
								sbEmail.Append("<br>"+drPlace2["PLAC_INST"].ToString()+"");
							}
							sbEmail.Append("<br>"+drPlace2["PLAC_ADDR_ONE"].ToString()+"<br>");

                            if (HelperFunction.getSafeStringFromDB(drPlace2["PLAC_ADDR_TWO"]).Trim().Length > 0)
                            {
                                sbEmail.Append(drPlace2["PLAC_ADDR_TWO"].ToString() + "<br>");
                            }
                            if (HelperFunction.getSafeStringFromDB(drPlace2["PLAC_ADDR_THREE"]).Trim().Length > 0)
                            {
                                sbEmail.Append(drPlace2["PLAC_ADDR_THREE"].ToString() + "<br>");
                            }
							strLocation = drPlace2["Location"].ToString();
							sbEmail.Append(drPlace2["Location"].ToString() + "  " + drPlace2["Zip"].ToString());
							if(HelperFunction.getSafeStringFromDB(drPlace2["PLAC_PHON"]).Trim().Length>0)
							{
								sbEmail.Append("<br>"+drPlace2["PLAC_PHON"].ToString());
							}
							if(HelperFunction.getSafeStringFromDB(drPlace2["PLAC_RAIL_NUM"]).Trim().Length>0)
							{
								sbEmail.Append("<br>Rail #:"+drPlace2["PLAC_RAIL_NUM"].ToString());
							}
							sbEmail.Append("<br>");

							sbEmail.Append("</SPAN></td>");
							sbEmail.Append("<td align=right>");
						}
					}
					finally
					{
						drPlace2.Close();
					}
				}

				if ((ShipMethod!="Delivered Tpe") || (DocumentType!="PO"))
				{
					if(dress2.Length>0)
					{
						sbEmail.Append("<br>"+dress2+"");
					}
					sbEmail.Append("<br>"+PlaceAddOne+"<br>");
                    if (PlaceAddTwo.Length > 0)
                    {
                        sbEmail.Append(PlaceAddTwo + "<br>");
                    }
                    if (PlaceAddThree.Length > 0)
                    {
                        sbEmail.Append(PlaceAddThree + "<br>");
                    }
					strLocation=Location;
					sbEmail.Append(Location+"  "+Zip+"");
					if (Country!="United States") sbEmail.Append("<br>"+Country);
					if(Phone.Length>0)
					{
						sbEmail.Append("<br>"+Phone);
					}
					if(RailNumber.Length>0)
					{
						sbEmail.Append("<br>Rail #:"+RailNumber);
					}
					sbEmail.Append("<br>");

					sbEmail.Append("</SPAN></td>");
					sbEmail.Append("<td align=right>");
				}
			
				sbSql = new StringBuilder();

				sbSql.Append(" SELECT ");
                sbSql.Append("    ORDR_DATE = CONVERT(varchar,ISNULL(SHIPMENT_TRANSACTION_DATE,ORDR_DATE),101), ");
				sbSql.Append("    ORDR_ID, ");
				sbSql.Append("    ORDR_BID, ");
                sbSql.Append("    ISNULL((SHIPMENT_BUYR_PRCE*(ISNULL(SHIPMENT_WEIGHT,SHIPMENT_QTY*(SHIPMENT_SIZE))))+ ((SHIPMENT_BUYR_PRCE*(ISNULL(SHIPMENT_WEIGHT,SHIPMENT_QTY*(SHIPMENT_SIZE)))) *(shipment_tax/100)), SHIPMENT_BUYR_PRCE*(ISNULL(SHIPMENT_WEIGHT,SHIPMENT_QTY*(SHIPMENT_SIZE)))), ");
				//sbSql.Append("    SHIPMENT_BUYR_PRCE*(ISNULL(SHIPMENT_WEIGHT,SHIPMENT_QTY*(SHIPMENT_SIZE))), ");
				sbSql.Append("    ISNULL(SHIPMENT_BUYER_TERMS,'30'), ");
				sbSql.Append("    SHIPMENT_QTY, ");
				sbSql.Append("    SHIPMENT_SIZE, ");
				sbSql.Append("    SHIPMENT_WEIGHT, ");
				sbSql.Append("    SHIPMENT_PRCE, ");
				sbSql.Append("    SHIPMENT_DATE_TAKE, ");
                sbSql.Append("    SHIPMENT_DATE_DELIVERED, ");
				sbSql.Append("    (SELECT CONT_LABL FROM CONTRACT WHERE CONT_ID=ORDR_CONT), ");
				sbSql.Append("    (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=SHIPMENT_TO)), ");
                sbSql.Append("    BUYERDUEDATE = DATEADD(day,CAST(SHIPMENT_BUYER_TERMS AS Integer),SHIPMENT_DATE_DELIVERED), ");
                sbSql.Append("    SELLERDUEDATE = DATEADD(day,CAST(SHIPMENT_SELLER_TERMS AS Integer),SHIPMENT_DATE_TAKE), ");
				sbSql.Append("    VARDATE=CONVERT(varchar(14),SHIPMENT_DATE_TAKE,101), ");
				sbSql.Append("    (SELECT CASE WHEN SHIPMENT_WEIGHT IS NULL THEN '0' ELSE '1' END), ");
				sbSql.Append("    ISNULL(SHIPMENT_WEIGHT,SHIPMENT_QTY*(SHIPMENT_SIZE)) AS Lbs, ");
				sbSql.Append("    VARMONTH=(SELECT FWD_MNTH+' '+RIGHT(CONVERT(varchar,FWD_YEAR),2) FROM FWDMONTH WHERE FWD_ID=SHIPMENT_MNTH), ");
				sbSql.Append("    SHIPMENT_INVOICE_SENT, ");
				sbSql.Append("    SHIPMENT_INVOICE_EMAIL_TO, ");
				sbSql.Append("    SHIPMENT_SKU, ");
				sbSql.Append("    SHIPMENT_PRCE, ");
				sbSql.Append("    SHIPMENT_PO_SENT, ");
				sbSql.Append("    SHIPMENT_PO_EMAIL_TO, ");
				//sbSql.Append("    SHIP=(SELECT TOP 1 SHIPMENT_DATE_TAKE FROM SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID), ");
                sbSql.Append("    SHIP=(SELECT TOP 1 SHIPMENT_DATE_DELIVERED FROM SHIPMENT WHERE SHIPMENT_ORDR_ID=ORDR_ID), ");
				sbSql.Append("    ORDR_PROD_SPOT, ");
                sbSql.Append("    PROD_SPOT = ISNULL(shipment_product_spot,ORDR_PROD_SPOT), ");
				sbSql.Append("    ORDR_ISSPOT, ");
				sbSql.Append("    SHIPMENT_SKU, ");
				sbSql.Append("    SHIPMENT_SELLER_TERMS, " );
				sbSql.Append("    SHIPMENT_BUYER_TERMS, " );
				sbSql.Append("    SHIPMENT_FOB, ");
				sbSql.Append("    ORDR_MELT, ");
				sbSql.Append("    ORDR_DENS, ");
				sbSql.Append("    SHIPMENT_PO_NUM, ");
				sbSql.Append("    ORDR_ADDS, ");
				sbSql.Append("    SHIPMENT_BUYR_PRCE, ");
				sbSql.Append("    SHIPMENT_TO, ");
				sbSql.Append("    SHIPMENT_TAX ");
				sbSql.Append(" FROM ORDERS, SHIPMENT ");
				sbSql.Append(" WHERE SHIPMENT_ORDR_ID=ORDR_ID AND SHIPMENT_ID=@ShipmentID");

				param.Clear();
				param.Add("@ShipmentID",ShipmentID);

				SqlDataReader drOrder2 = DBLibrary.GetDataReaderFromSelect(conn,sbSql.ToString(),param);
				string shipment_size=String.Empty;
				string shipment_fob=String.Empty;

				try
				{
					drOrder2.Read();

					decimal shipment_tax = HelperFunction.getSafeDecimalFromDB(drOrder2["SHIPMENT_TAX"]);
					long shipment_to = HelperFunction.getSafeLongFromDB(drOrder2["SHIPMENT_TO"]);

					sbEmail.Append("<table border=1 cellspacing=0 >");
					//Display P.O number if it's available, only for Invoices and Sales Confirmation
					if(DocumentType != "PO")
					{
						if(HelperFunction.getSafeStringFromDB(drOrder2["SHIPMENT_PO_NUM"]).Trim().Length>0)
						{
							sbEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC width=100>P.O. #</td><td STYLE='FONT: 8pt ARIAL' align=left>&nbsp;"+drOrder2["SHIPMENT_PO_NUM"].ToString()+"");
							sbEmail.Append("</td></tr>");
						}
					}
					//Display transaction number
					sbEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC width=100>Transaction #</td><td STYLE='FONT: 8pt ARIAL' align=left>&nbsp;"+drOrder2["ORDR_ID"].ToString()+"");
					if(drOrder2["SHIPMENT_SKU"]==null)
					{
                        sbEmail.Append("</td></tr>");
					}
					else
					{
						sbEmail.Append("-"+drOrder2["SHIPMENT_SKU"].ToString()+"</td></tr>");
					}
					sbEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Transaction Date</td><td STYLE='FONT: 8pt ARIAL' align=left>&nbsp;"+ String.Format("{0:MM/dd/yyyy}",drOrder2["ORDR_DATE"].ToString()) +"</td></tr>");
					bool checkIsShipping;
					if(DocumentType != "SC")
					{
						if(drOrder2["SHIP"].ToString().Length==0)
						{
							sbEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>");
							if(ShipMethod=="Delivered Tpe")
							{
								if(DocumentType == "PO")
								{
									sbEmail.Append("Pickup Date");
								}
								else
								{
									sbEmail.Append("Shipping Date");
									checkIsShipping=true;                                   
                                    
								}
							}
							else
							{
								if(ShipMethod=="FOB/Delivered")
								{
									sbEmail.Append("Shipping Date");
									checkIsShipping=true;
                                    
								}
								else
								{
									if(ShipMethod=="FOB/Shipping")
									{
										if(DocumentType == "PO")
											sbEmail.Append("Pickup Date");
										else
											sbEmail.Append("Release Date");
									}
									else
									{
										sbEmail.Append("Shipping Date");
										checkIsShipping=true;
                                        
									}
								}
							}
					
							if(DocumentType == "PO")
							{
								sbEmail.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left><font color=red>&nbsp;ASAP</td></tr>");
							}
							else
							{
                                if (drOrder2["SHIPMENT_DATE_DELIVERED"] is DBNull)
								{
									time=Convert.ToDateTime(drOrder2["ORDR_DATE"].ToString());
									time.AddDays(10);
									sbEmail.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left>&nbsp;"+ExchangeTimeFormat(time.ToString())+"</font></td></tr>");
							
								}
								else
								{
                                    sbEmail.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left>&nbsp;" + ExchangeTimeFormat(drOrder2["SHIPMENT_DATE_DELIVERED"].ToString()) + "</font></td></tr>");
								}
							}
						}
						else
						{
							sbEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>");
					
							if(ShipMethod=="Delivered Tpe")
                                if (DocumentType == "PO")
                                {
                                    sbEmail.Append("Pickup Date");
                                }
                                else
                                {
                                    sbEmail.Append("Shipping Date");
                                    bAddInvoiceDate = true;
                                }
							else
                                if (ShipMethod == "FOB/Delivered")
                                {
                                    sbEmail.Append("Shipping Date");
                                    bAddInvoiceDate = true;
                                }
                                else
                                    if (ShipMethod == "FOB/Shipping")
                                        if (DocumentType == "PO")
                                        {
                                            sbEmail.Append("Pickup Date");
                                        }
                                        else
                                        {
                                            sbEmail.Append("Release Date");
                                            bAddInvoiceDate = true;
                                        }

                                    else
                                        sbEmail.Append("Shipping Date");
					
							if (DocumentType == "PO")
								sbEmail.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left><font color=red>&nbsp;ASAP</td></tr>");
							else
                                if (drOrder2["SHIPMENT_DATE_DELIVERED"] is DBNull)
								sbEmail.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left><font color=red>&nbsp;"+ExchangeTimeFormat(drOrder2["VARDATE"].ToString())+"</td></tr>");
							else
                                sbEmail.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left><font color=red>&nbsp;" + ExchangeTimeFormat(drOrder2["SHIPMENT_DATE_DELIVERED"].ToString()) + "</td></tr>");


                        
                        }


                        if (bAddInvoiceDate && DocumentType != "PO")
                        {
                            sbEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>");
                            sbEmail.Append("Invoice Date");
                            sbEmail.Append("</td><td STYLE='FONT: 8pt ARIAL' align=left><font color=red>&nbsp;" + ExchangeTimeFormat(drOrder2["SHIPMENT_DATE_DELIVERED"].ToString()) + "</td></tr>");
                        }
                        

					}
					sbEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Transaction Value</td><td STYLE='");
			
					double  money = 0;
					if(DocumentType == "PO")
					{
						money = Convert.ToDouble(drOrder2["lbs"].ToString())*Convert.ToDouble(drOrder2["SHIPMENT_PRCE"].ToString());
						sbEmail.Append("FONT: 8pt ARIAL' align=left>&nbsp"+money.ToString("c")+"</td></tr>");
					}
					else
					{
						money = Convert.ToDouble(drOrder2[3].ToString());
						sbEmail.Append("FONT: 8pt ARIAL' align=left>&nbsp"+money.ToString("c")+"</td></tr>");
					}
					if(DocumentType != "SC")
					{
						if(DocumentType != "PO")
						{
                            if (drOrder2["SHIPMENT_DATE_DELIVERED"] is DBNull)
							{
								//if(rd["DAYS"].ToString().Trim().Length==0)
								//{
                                time = Convert.ToDateTime(drOrder2["ORDR_DATE"].ToString());
								time.AddDays(30);
								sbEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Payment Due</td><td STYLE='FONT: 8pt ARIAL' align=left><b>&nbsp;"+ExchangeTimeFormat(time.ToString())+"</b></td></tr>");
							}
							else
							{
								//time=Convert.ToDateTime(rd["SHIPMENT_DATE_TAKE"].ToString());
								//time.AddDays(Convert.ToInt32(rd["SHIPMENT_BUYER_TERMS"].ToString()));
                                sbEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Payment Due</td><td STYLE='FONT: 8pt ARIAL' align=left><b>&nbsp;" + ExchangeTimeFormat(drOrder2["BUYERDUEDATE"].ToString()) + "</b></td></tr>");
							}
						}
						/*
						else
						{
							if(drOrder2["VARDATE"].ToString().Trim().Length==0)
							{
								time=Convert.ToDateTime(drOrder2["VARDATE"].ToString());
								time.AddDays(Convert.ToInt32(drOrder2["DAYS"].ToString()));
								sbEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Payment Due</td><td STYLE='FONT: 8pt ARIAL' align=left><b>&nbsp;"+ExchangeTimeFormat(time.ToString())+"</b></td></tr>");
							}
							else
							{
								time=Convert.ToDateTime(drOrder2["VARDATE"].ToString());
								time.AddDays(Convert.ToInt32(drOrder2["DAYS"].ToString()));
								sbEmail.Append("<tr><td STYLE='FONT: 8pt ARIAL' bgcolor=#CCCCCC>Payment Due</td><td STYLE='FONT: 8pt ARIAL' align=left><b>&nbsp;"+ExchangeTimeFormat(time.ToString())+"</b></td></tr>");
							}
						}
						*/
					}
					sbEmail.Append("</table>");
					sbEmail.Append("</tr>");
					sbEmail.Append("<tr valign=top><td align=center colspan=3>");
					sbEmail.Append("<br>");
					sbEmail.Append("<table border=1 cellspacing=0 width=100% >");
                    
					if(drOrder2["SHIPMENT_SIZE"].ToString()=="1")
					{
						sbEmail.Append("<tr bgcolor=#CCCCCC>");                        
                        sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Size (lbs)</td>");                      						
					}
					else
					{
						sbEmail.Append("<tr bgcolor=#CCCCCC>");
						sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Amount</td>");
						sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Size</td>");
						if(drOrder2["SHIPMENT_WEIGHT"] is DBNull)
						{
                            sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Lbs<font color=red>&nbsp(Est)</td>");                            
						}
						else
						{
                            sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Lbs</td>");						
						}
					}
                    sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>$/Lb</td>");
                    //Larry
                    if (metric)
                    {
                        sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>MT</td>");
                        sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>$/MT</td>");
                        sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>25kg Bags</td>");
                    }

					sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Product</td>");
			
					if(DocumentType == "PO")
						if(ShipMethod=="FOB/Shipping")
							sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Our Pickup</td>");
						else
							if( ShipMethod=="FOB/Delivered")
							sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Delivered To</td>");
						else
							if(ShipMethod=="Delivered Tpe")
							sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Our Pickup</td>");
						else
							sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Our Pickup</td>");
					else
						if(ShipMethod=="FOB/Shipping")
						sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Your Pickup</td>");
					else
						if( ShipMethod=="FOB/Delivered")
						sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Delivered To</td>");
					else
						if(ShipMethod=="Delivered Tpe")
						sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Delivered To</td>");
					else
						sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Ship to</td>");
			
					sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Terms</td>");
					sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Total Amount</td>");
					sbEmail.Append("</tr>");
					sbEmail.Append("<tr>");
					if(drOrder2["SHIPMENT_SIZE"].ToString()=="1")
					{
						if(drOrder2["SHIPMENT_WEIGHT"].ToString().Trim().Length==0)
						{
							sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+String.Format("{0:#,###}",drOrder2["SHIPMENT_QTY"].ToString()).ToString()+"<font color=red>&nbsp(Est)</font></td>");
						}
						else
						{
							sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+String.Format("{0:#,###}",drOrder2["SHIPMENT_WEIGHT"].ToString()).ToString()+"</td>");
						}
					}
					else
					{
						sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+String.Format("{0:#,###}", 
							drOrder2["SHIPMENT_QTY"].ToString()).ToString()+"</td>");
						if(drOrder2["SHIPMENT_SIZE"]is DBNull )
						{
							sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>");
						}
						else
						{
							if(drOrder2["SHIPMENT_SIZE"].ToString()=="42000")
							{
								sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Truckload Boxes");
							}
							if(drOrder2["SHIPMENT_SIZE"].ToString()=="1")
							{
								sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Lb");
							}
							if(drOrder2["SHIPMENT_SIZE"].ToString()=="2.205")
							{
								sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Kilo");
							}
							if(drOrder2["SHIPMENT_SIZE"].ToString()=="190000")
							{
								sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Railcar");
                                bRailCar = true;
							}
							if(drOrder2["SHIPMENT_SIZE"].ToString()=="45000")
							{
								sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Bulk Truck");
							}
							if(drOrder2["SHIPMENT_SIZE"].ToString()=="44092")
							{
								sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Truckload Bags");
							}
                            if (drOrder2["SHIPMENT_SIZE"].ToString() == "43000")
                            {
                                sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Truckload Super Sacks");
                            }
							if(drOrder2["SHIPMENT_SIZE"].ToString()=="2205")
							{
								sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Metric Ton");
							}
							if(drOrder2["SHIPMENT_SIZE"].ToString()=="38500")
							{
								sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>20 Container");
							}
							if(drOrder2["SHIPMENT_SIZE"].ToString()=="44500")
							{
								sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>30 Container");
							}
						}
						if(drOrder2["SHIPMENT_QTY"].ToString()!="1")
						{
							sbEmail.Append("s");
							sbEmail.Append("</td>");
						}
						if(drOrder2["SHIPMENT_WEIGHT"] is DBNull)
						{
                            sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'><font color=red>" + String.Format("{0:#,###}", Convert.ToInt32(drOrder2["lbs"].ToString())).ToString() + "</td>");                          
						}
						else
						{
                            sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>" + String.Format("{0:#,###}", Convert.ToInt32(drOrder2["lbs"].ToString())).ToString() + "</td>");						
						}
					}
					if(DocumentType == "PO")
					{
                        sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>" + String.Format("${0:#,###0.0000}", Convert.ToDouble(drOrder2["SHIPMENT_PRCE"].ToString())) + "</td>");                       
					}
					else
					{
                        sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>" + String.Format("{0:#,###0.0000}", Convert.ToDouble(drOrder2["SHIPMENT_BUYR_PRCE"].ToString())) + "</td>");                      
					}

                    //Larry
                    if (metric)
                    {
                        double metricTon = Math.Round(Convert.ToInt32(drOrder2["lbs"].ToString()) / 2204.6226);
                        sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>" + metricTon.ToString() + "</td>");
                        if (DocumentType == "PO")
                        {
                            sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>" + String.Format("${0:#,###0.0000}", Convert.ToDouble(drOrder2["SHIPMENT_PRCE"].ToString()) * 2204.6226) + "</td>");                       
                        }
                        else
                        {
                            sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>" + String.Format("{0:#,###0.0000}", Convert.ToDouble(drOrder2["SHIPMENT_BUYR_PRCE"].ToString()) * 2204.6226) + "</td>");   
                        }
                        sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>" + String.Format("{0:#,###0.0}",Convert.ToString(metricTon/0.025)) +" Bags"+ "</td>");
                    }

					if(Convert.ToBoolean(drOrder2["ORDR_ISSPOT"]))
					{
						sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+drOrder2["PROD_SPOT"].ToString());
						if(HelperFunction.getSafeStringFromDB(drOrder2["ORDR_MELT"]).Trim().Length!=0)
						{
							sbEmail.Append(" - "+drOrder2["ORDR_MELT"].ToString()+" melt");
						}
				
						if(HelperFunction.getSafeStringFromDB(drOrder2["ORDR_DENS"]).Trim().Length!=0)
						{
							sbEmail.Append(" - "+drOrder2["ORDR_DENS"].ToString()+" density");
						}

						if(HelperFunction.getSafeStringFromDB(drOrder2["ORDR_ADDS"]).Trim().Length>0)
						{
							sbEmail.Append(" - "+drOrder2["ORDR_ADDS"].ToString());
						}
					}
					else
					{
						sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+drOrder2[9].ToString());
					}
					sbEmail.Append("</td>");
					sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+ strLocation + "</td>");
					if(DocumentType == "PO")
					{
						if(drOrder2["SHIPMENT_SELLER_TERMS"].ToString()=="0")
						{
							sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Cash</td>");
						}
						else
						{
							sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+drOrder2["SHIPMENT_SELLER_TERMS"].ToString()+" days</td>");
						}
					}
					else
					{
						if(drOrder2["SHIPMENT_BUYER_TERMS"].ToString()=="0")
						{
							sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>Cash</td>");
						}
						else
						{
							terms = System.Convert.ToInt32(drOrder2["SHIPMENT_BUYER_TERMS"].ToString());
							sbEmail.Append("<td STYLE='FONT: 8pt ARIAL'>"+drOrder2["SHIPMENT_BUYER_TERMS"].ToString()+" days</td>");
						}
					}
					sbEmail.Append("<td STYLE='");
					if(drOrder2[13].ToString()=="0")
					{
						sbEmail.Append("");
					}
					if(DocumentType == "PO")
					{
						money= Convert.ToDouble(drOrder2["lbs"].ToString())*Convert.ToDouble(drOrder2["SHIPMENT_PRCE"].ToString());
						sbEmail.Append("FONT: 8pt ARIAL'>"+money.ToString("C")+"</td>");
					}
					else
					{
                        //money = Convert.ToDouble(drOrder2["SHIPMENT_WEIGHT"].ToString()) * Convert.ToDouble(drOrder2["SHIPMENT_BUYR_PRCE"].ToString());
                        ////money = Convert.ToDouble(drOrder2[3].ToString());
                        //sbEmail.Append("FONT: 8pt ARIAL'>"+ money.ToString("C") + "</td>");

                        //*****CEF Chnage for GST and Handling of Null weight.
                        if (!drOrder2["SHIPMENT_WEIGHT"].Equals(System.DBNull.Value))
                        {
                            money = Convert.ToDouble(drOrder2["SHIPMENT_WEIGHT"].ToString()) * Convert.ToDouble(drOrder2["SHIPMENT_BUYR_PRCE"].ToString());
                        }
                        else
                        {
                            money = (Convert.ToDouble(drOrder2["SHIPMENT_SIZE"].ToString()) * Convert.ToDouble(drOrder2["SHIPMENT_QTY"].ToString())) * Convert.ToDouble(drOrder2["SHIPMENT_BUYR_PRCE"].ToString());
                        }
                        //money = Convert.ToDouble(drOrder2[3].ToString());
                        sbEmail.Append("FONT: 8pt ARIAL'>" + money.ToString("C") + "</td>");

					}


					if(Country == "Canada")
					{
						double qst = (double)HelperFunction.returnTaxByName("QST", Context);
						double gst = (double)HelperFunction.returnTaxByName("GST", Context);
						double hst = (double)HelperFunction.returnTaxByName("HST", Context);

						sbEmail.Append("</tr>");

						sbEmail.Append("<tr>");
						int colspan = 7;
						if(drOrder2["SHIPMENT_SIZE"].ToString()=="1")
						{
							//because we don't have column ammount when it's in LBS
							colspan = 5;
							
						}
                        //Larry
                        if (metric)
                        {
                            colspan = colspan + 3;
                        }

						sbEmail.Append("<td colspan=" + colspan + " Style='FONT: 8pt ARIAL' align=center>");

						if(HelperFunction.returnTaxName(shipment_to.ToString(), this.Context) == "QST")
						{
							sbEmail.Append("GST");
							sbEmail.Append("</td>");
							sbEmail.Append("<td Style='FONT: 8pt ARIAL'>");
							sbEmail.Append( String.Format("{0:C}", gst * money / 100));
							sbEmail.Append("</td>");

							sbEmail.Append("</tr>");

							sbEmail.Append("<tr>");
							sbEmail.Append("<td colspan=" + colspan + " Style='FONT: 8pt ARIAL' align=center>");

							sbEmail.Append("Sub-total");
							sbEmail.Append("</td>");
							sbEmail.Append("<td Style='FONT: 8pt ARIAL'>");
							double subtotal = money * ( 100 + gst ) / 100;
							sbEmail.Append(String.Format("{0:C}", subtotal));
							sbEmail.Append("</td>");
							sbEmail.Append("</tr>");
							sbEmail.Append("<tr>");

							sbEmail.Append("<td colspan=" + colspan + " Style='FONT: 8pt ARIAL' align=center>");
							sbEmail.Append("QST");
							sbEmail.Append("</td>");

							sbEmail.Append("<td Style='FONT: 8pt ARIAL'>");
							sbEmail.Append(String.Format("{0:C}", qst * subtotal / 100));
							sbEmail.Append("</td>");
							sbEmail.Append("</tr>");
							
							sbEmail.Append("<tr>");
							sbEmail.Append("<td colspan=" + colspan + " Style='FONT: 8pt ARIAL' align=center>");
							sbEmail.Append("Total");
							sbEmail.Append("</td>");

							sbEmail.Append("<td Style='FONT: 8pt ARIAL'>");
							sbEmail.Append(String.Format("{0:C}", subtotal + qst * subtotal / 100) );
							sbEmail.Append("</td>");
							sbEmail.Append("</tr>");


						}
						else
						{
							sbEmail.Append("" + HelperFunction.returnTaxName(shipment_to.ToString(), this.Context) + "");
							sbEmail.Append("</td>");
							sbEmail.Append("<td Style='FONT: 8pt ARIAL'>");
                            sbEmail.Append(String.Format("{0:C}", Convert.ToDouble(shipment_tax) * money / 100));
							//sbEmail.Append( String.Format("{0:C}",money));
							sbEmail.Append("</td>");

							sbEmail.Append("</tr>");

							sbEmail.Append("<tr>");
							if(drOrder2["SHIPMENT_SIZE"].ToString()=="1")
							{
                                //Larry
                                if (metric)
                                {
                                    sbEmail.Append("<td colspan=8 Style='FONT: 8pt ARIAL' align=center>");
                                }
                                else
                                {
                                    sbEmail.Append("<td colspan=5 Style='FONT: 8pt ARIAL' align=center>");                                                        
                                }
							}
							else
							{
                                if (metric)
                                {
                                    sbEmail.Append("<td colspan=10 Style='FONT: 8pt ARIAL' align=center>");
                                }
                                else
                                {
                                    sbEmail.Append("<td colspan=7 Style='FONT: 8pt ARIAL' align=center>");
                                }								
							}
							sbEmail.Append("Total");
							sbEmail.Append("</td>");
							sbEmail.Append("<td Style='FONT: 8pt ARIAL'>");
							sbEmail.Append(String.Format("{0:C}",money * (100 + Convert.ToDouble(shipment_tax)) / 100));
							sbEmail.Append("</td>");
						}
					}


			
					sbEmail.Append("</tr>");

					sbEmail.Append("</table><br>");
					sbEmail.Append("</td></tr>");
					sbEmail.Append("<br><tr><td colspan=4 Style'FONT: 8pt ARIAL'>");
					shipment_size=drOrder2["SHIPMENT_SIZE"].ToString();
					shipment_fob=drOrder2["SHIPMENT_FOB"].ToString();

				}
				finally
				{
					drOrder2.Close();
				}
				if(DocumentType == "PO")
				{
					//				if(shipment_fob!="FOB/Delivered")
					//				{
					//					sbEmail.Append("<span STYLE='FONT: 8pt ARIAL'>Please provide release # ASAP.</span><BR><BR>");
					//				}
				}
				else
				{
					if(DocumentType != "SC")
					{
						if(shipment_size=="190000")
						{
							param.Clear();
							param.Add("@ShipmentID",ShipmentID);
							SqlDataReader drShipComment = DBLibrary.GetDataReaderFromSelect(conn,"SELECT SHIPMENT_COMMENT FROM SHIPMENT WHERE SHIPMENT_ID=@ShipmentID",param);
							try
							{
								if (drShipComment.Read())
								{
									if(!(drShipComment["SHIPMENT_COMMENT"]is DBNull))
									{
										sbEmail.Append("<span STYLE='FONT: 8pt ARIAL'>Railcar # ");
										do
										{
											sbEmail.Append(drShipComment["SHIPMENT_COMMENT"].ToString()+"</span>");
										
										}while(drShipComment.Read());
										drShipComment.Close();
									}
									else
									{
										sbEmail.Append("</span>");
									}
								}
							}
							finally
							{
								drShipComment.Close();
							}
						}
					}
				}
			

				//Email Bottom
				if ((CustomField.Trim().Length>0))
				{
					sbEmail.Append("<span STYLE='FONT: 8pt ARIAL'><BR><b>"+BreakMultilineFieldHTML(CustomField)+"</b></span><br>");
				}
				// Zach - 2/8/05 - Removed at Mike's request 
				//sbEmail.Append("<span STYLE='FONT: 8pt ARIAL'>Thank you for your order.</span><br></td></tr>");
			
				sbEmail.Append("<br><br>");
				sbEmail.Append("<tr valign=top>");
				if((DocumentType != "PO")&&(DocumentType != "SC"))
				{
                    string MAGsellerID = "";
                    string sqlstr = "select shipment_selr from shipment where shipment_id ='"+ ShipmentID+"' ";
                    SqlDataReader MAGshipment = DBLibrary.GetDataReaderFromSelect(conn, sqlstr);
                    if (MAGshipment.Read())
                    {
                        MAGsellerID = MAGshipment["shipment_selr"].ToString();
                    }
                    if (MAGsellerID == "8625") //this one is for MAG-CSH
                    {
                        sbEmail.Append("<td colspan=2><SPAN STYLE='FONT: 8pt ARIAL'><b><i>WIRE TRANSFER INSTRUCTIONS:</i></b><br>MB Financial<br>33 W. Huron<BR>Chicago, IL 60654<BR>ABA # 071 001 737<BR><BR>For Further Credit:<BR>Michael Greenberg<BR>Account #: 4780005177</SPAN><BR><BR><BR></td>");
                        MAGshipment.Close();
                    }
                    else 
                    {
                        if (CompanyID == "508" || CompanyID == "386" || CompanyID == "492" || CompanyID == "489" || CompanyID == "290" || CompanyID == "478" || CompanyID == "497")
                        {
                            sbEmail.Append("<td colspan=1><SPAN STYLE='FONT: 8pt ARIAL'><b><i>CHECK REMITTANCE INSTRUCTIONS:</i></b><br>The Plastics Exchange<br>16510 N. 92nd Street. #1010<BR>Scottsdale, AZ 85260</SPAN></td>");


                            sbEmail.Append("<td colspan=1><SPAN STYLE='FONT: 8pt ARIAL'><b><i>International Wire Transfer Instructions::</i></b><P>Our wires via swift are routed through The Bank of New York <br><b><br>Swift Code:</b> CitiUS33  <br> <br><b>Beneficiary:</b><br>The Plastics Exchange<br>16510 N. 92nd Street. #1010<BR>Scottsdale, AZ 85260<BR><br><b>Beneficiary Account:</b> 071925046<BR><BR><b>For Further Credit:</b><BR>TheExchangeCorp.com DBA The Plastics Exchange, LLC, Account Number 801086470</SPAN><BR><BR><BR></td>");
                        }                    
                        else
                        {
                            sbEmail.Append("<td colspan=1><SPAN STYLE='FONT: 8pt ARIAL'><b><i>CHECK REMITTANCE INSTRUCTIONS:</i></b><br>The Plastics Exchange<br>16510 N. 92nd Street. #1010<BR>Scottsdale, AZ 85260</SPAN></td>");
                            sbEmail.Append("<td colspan=1><SPAN STYLE='FONT: 8pt ARIAL'><b><i>WIRE TRANSFER INSTRUCTIONS:</i></b><br>Citibank<br>Chicago, IL 60654<BR>ABA # 271070 801<BR><BR>For Further Credit:<BR>The Plastics Exchange<BR>Account #: 801086470</SPAN><BR><BR><BR></td>");
                        }
                    }
                    
				}
				sbEmail.Append("</tr>");
				//sbEmail.Append("<tr><td colspan=3 valign=top align=center></td></tr>");
				//sbEmail.Append("<tr><td colspan=3 valign=top align=center><font face=arial size=1 color=Black>730 North LaSalle, 3rd Floor</font>&nbsp;&nbsp;<font face=arial size=1 color=red>Chicago, IL 60654</font>&nbsp;&nbsp;<font face=arial size=1 color=Black><b>tel 312.202.0002</b></font>&nbsp;&nbsp;<font face=arial size=1 color=red>fax 312.202.0174</font></td></tr>");
				sbEmail.Append("</table>");
				sbEmail.Append("<BR>");
			
				if ((DocumentType=="IN") && (terms>0))
				{
					sbEmail.Append("<SPAN STYLE='FONT: 8pt ARIAL'><i>Terms: Payment due within " + terms.ToString() + " days of the date of the invoice.</i></SPAN>");
					sbEmail.Append("<BR><SPAN STYLE='FONT: 8pt ARIAL'><i>A finance charge of 0.75 % per 15 days will be added on all invoiced amounts outstanding after " + terms.ToString() + " days from the invoice date.</i></SPAN>");
					sbEmail.Append("<BR><SPAN STYLE='FONT: 8pt ARIAL'><i>Customer will be responsible for payment of all costs, expenses and fees (including legal fees) incurred by The Plastics Exchange.com to collect amounts invoiced.  In the event that customer fails to pay such invoices The Plastics Exchange.com  reserves the right to offset such amounts. Customer agrees to these terms by accepting services from The Plastics Exchange.com.</i></SPAN><BR>");
				}
				sbEmail.Append(HTMLFooter());
				return sbEmail.ToString();
			}
			finally
			{
				conn.Close();
			}
		}
		
        #endregion

		private string getShipmentSize(string shipmentOrderID, string shipmentSKU)
		{
		    string size = "";

		    SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());

		    Hashtable param = new Hashtable();
		    try
		    {
			conn.Open();
			param.Add("@shipmentOrderID", shipmentOrderID);
			param.Add("@shipmentSKU", shipmentSKU);

			//SqlDataReader sqlDTPers = DBLibrary.GetDataReaderFromSelect(conn,"Select PERS_MAIL from PERSON WHERE PERS_ID=(Select SHIPMENT_BUYR FROM SHIPMENT where SHIPMENT_ORDR_ID = @shipmentOrderID and SHIPMENT_SKU =@shipmentSKU)",param);
			SqlDataReader sqlDTPers = DBLibrary.GetDataReaderFromSelect(conn, "Select SHIPMENT_SIZE from SHIPMENT WHERE (SHIPMENT_ORDR_ID = @shipmentOrderID) and (SHIPMENT_SKU = @shipmentSKU)", param);
	                
			try
			{
			    while (sqlDTPers.Read())
			    {
				size = sqlDTPers["SHIPMENT_SIZE"].ToString();
			    }                    
			}
			finally
			{
			    sqlDTPers.Close();
			}
		    }
		    finally
		    {
			conn.Close();
		    }

		    return size;
		}

		private string getEmailBuyer(string shipmentOrderID, string shipmentSKU)
		{
			string email = "";
			
			SqlConnection conn = new SqlConnection(Application["DBconn"].ToString());

			Hashtable param = new Hashtable();
			try
			{
				conn.Open();
				param.Add("@shipmentOrderID",shipmentOrderID);
				param.Add("@shipmentSKU",shipmentSKU);
				
                //SqlDataReader sqlDTPers = DBLibrary.GetDataReaderFromSelect(conn,"Select PERS_MAIL from PERSON WHERE PERS_ID=(Select SHIPMENT_BUYR FROM SHIPMENT where SHIPMENT_ORDR_ID = @shipmentOrderID and SHIPMENT_SKU =@shipmentSKU)",param);
                SqlDataReader sqlDTPers = DBLibrary.GetDataReaderFromSelect(conn, "Select PERS_MAIL from PERSON WHERE PERS_ENBL='1' AND pers_comp=(Select pers_comp from PERSON WHERE PERS_ID=(Select SHIPMENT_BUYR FROM SHIPMENT where (SHIPMENT_ORDR_ID = @shipmentOrderID) and (SHIPMENT_SKU = @shipmentSKU))) order by pers_mail", param);
				
                //
                
                try
				{
                    while (sqlDTPers.Read())
                    {
                        ddlSendTo.Items.Add(sqlDTPers["PERS_MAIL"].ToString());
                        //ddlCC.Items.Add(sqlDTPers["PERS_MAIL"].ToString());
                    }
                    //sqlDTPers.Read();
                    email = "";
				}
				finally
				{
					sqlDTPers.Close();
				}
			}
			finally
			{
				conn.Close();
			}

			return email;
		}

		private string getEmailWarehouse(string shipmentOrderID, string shipmentSKU, string DBConn)
		{
			string email = "";
			SqlConnection conn = new SqlConnection(DBConn);
			Hashtable param = new Hashtable();

			string sqlOrderTo="SELECT PLAC_EMAIL FROM SHIPMENT S, LOCALITY L, PLACE P WHERE S.SHIPMENT_TO = P.PLAC_ID AND P.PLAC_LOCL = L.LOCL_ID AND S.SHIPMENT_ORDR_ID=@shipmentOrderID AND S.SHIPMENT_SKU = @shipmentSKU";
			param.Add("@shipmentSKU",shipmentSKU.Trim());
			param.Add("@shipmentOrderID",shipmentOrderID);
			conn.Open();
			try
			{
				SqlDataReader sqlDTPers = DBLibrary.GetDataReaderFromSelect(conn,sqlOrderTo,param);
				try
				{
					sqlDTPers.Read();
					email = sqlDTPers["PLAC_EMAIL"].ToString();
				}
				finally
				{
					sqlDTPers.Close();
				}
			}
			finally
			{
				conn.Close();
			}
			return email;
		}

		private string getEmailSeller(string shipmentOrderID, string shipmentSKU, string DBConn)
		{
			string email = "";
			SqlConnection conn = new SqlConnection(DBConn);
			
			Hashtable param = new Hashtable();
			param.Add("@shipmentOrderID",Int32.Parse(shipmentOrderID));
			param.Add("@shipmentSKU",shipmentSKU);
			try
			{
				conn.Open();

				//SqlDataReader sqlDTPers = DBLibrary.GetDataReaderFromSelect(conn,"Select PERS_MAIL from PERSON WHERE PERS_ID=(Select SHIPMENT_SELR FROM SHIPMENT where (SHIPMENT_ORDR_ID = @shipmentOrderID) and (SHIPMENT_SKU = @shipmentSKU))", param);

                 SqlDataReader sqlDTPers = DBLibrary.GetDataReaderFromSelect(conn, "Select PERS_MAIL from PERSON WHERE PERS_ENBL='1' AND pers_comp=(Select pers_comp from PERSON WHERE PERS_ID=(Select SHIPMENT_SELR FROM SHIPMENT where (SHIPMENT_ORDR_ID = @shipmentOrderID) and (SHIPMENT_SKU = @shipmentSKU)))order by pers_mail", param);
                
                
                try
				{
                    while (sqlDTPers.Read())
                    {
                        ddlSendTo.Items.Add(sqlDTPers["PERS_MAIL"].ToString());
                        //ddlCC.Items.Add(sqlDTPers["PERS_MAIL"].ToString());
                    }
					//sqlDTPers.Read();
                    email = "";
				}
				finally
				{
					sqlDTPers.Close();
				}
			}
			finally
			{
				conn.Close();
			}
			return email;
		}

		private string getCompanyNameSeller(string shipmentOrderID, string shipmentSKU, string DBConn)
		{
			string company = "";
			SqlConnection conn = new SqlConnection(DBConn);
			
			Hashtable param = new Hashtable();
			param.Add("@shipmentOrderID",shipmentOrderID);
			param.Add("@shipmentSKU",shipmentSKU);
			try
			{
				conn.Open();

				SqlDataReader sqlDTPers = DBLibrary.GetDataReaderFromSelect(conn,"Select COMP_NAME from COMPANY WHERE COMP_ID = (Select PERS_COMP from PERSON WHERE PERS_ID=(Select SHIPMENT_SELR FROM SHIPMENT where SHIPMENT_ORDR_ID = @shipmentOrderID and SHIPMENT_SKU = @shipmentSKU))", param);
				try
				{
					sqlDTPers.Read();
					company = sqlDTPers["COMP_NAME"].ToString();
				}
				finally
				{
					sqlDTPers.Close();
				}
			}
			finally
			{
				conn.Close();
			}

			return company;
		}

		private string getCompanyNameBuyer(string shipmentOrderID, string shipmentSKU, string DBConn)
		{
			string company = "";
			SqlConnection conn = new SqlConnection(DBConn);
			
			Hashtable param = new Hashtable();
			param.Add("@shipmentOrderID",shipmentOrderID);
			param.Add("@shipmentSKU",shipmentSKU);
			try
			{
				conn.Open();
				SqlDataReader sqlDTPers = DBLibrary.GetDataReaderFromSelect(conn,"Select COMP_NAME from COMPANY WHERE COMP_ID = (Select PERS_COMP from PERSON WHERE PERS_ID=(Select SHIPMENT_BUYR FROM SHIPMENT where SHIPMENT_ORDR_ID = @shipmentOrderID and SHIPMENT_SKU =@shipmentSKU))",param);
				try
				{
					sqlDTPers.Read();
					company = sqlDTPers["COMP_NAME"].ToString();
				}
				finally
				{
					sqlDTPers.Close();
				}
			}
			finally
			{
				conn.Close();
			}
			return company;
		}

		private string getEmailUser(string IDUser, string dbConnectionString)
		{
			string email = "";

			SqlConnection conn = new SqlConnection(dbConnectionString);
			
			Hashtable param = new Hashtable();
			param.Add("@IDUser",IDUser);
			try
			{
				conn.Open();

				SqlDataReader sqlDTPers = DBLibrary.GetDataReaderFromSelect(conn, "select PERS_MAIL from person WHERE  PERS_ENBL='1' AND PERS_ID = @IDUser", param);
				try
				{
					sqlDTPers.Read();
					email = sqlDTPers["PERS_MAIL"].ToString();
				}
				finally
				{
					sqlDTPers.Close();
				}
			}
			finally
			{
				conn.Close();
			}

			return email;
		}

		//break line
		private string bl()
		{
			return ((char)13).ToString();
		}

		private void SendEmail(string emailFrom, string emailTo, string emailCC, string emailSubject, string emailBody, string ShipmentID, string dbConnectionString)
		{
			MailMessage email = new MailMessage();
			email.From = emailFrom;
			email.To = emailTo;
			email.Cc = emailCC;
			
			//if Mike is not one of the recievers, include him in the bcc.
            if ((emailTo != ConfigurationSettings.AppSettings["strEmailOwner"].ToString()) && (emailCC != ConfigurationSettings.AppSettings["strEmailOwner"].ToString()))
			{
				if (email.Cc.Length>0) email.Cc += ";";
                email.Cc += ConfigurationSettings.AppSettings["strEmailOwner"].ToString();
			}

            if ((emailFrom == emailTo) && (emailTo == ConfigurationSettings.AppSettings["strEmailLogistics"].ToString()))
			{
				//Mike's address would already be here
				email.Cc += ";" + ConfigurationSettings.AppSettings["strEmailAdmin"].ToString();
			}

			//including a message
			InsertComment(ShipmentID, emailSubject + " was sent by email to: " + emailTo, dbConnectionString);

			email.Subject = emailSubject;
			email.Body = emailBody;
			email.BodyFormat = MailFormat.Html;
         

			TPE.Utility.EmailLibrary.Send(Context,email);
		}

		private void PreviewEmail(string emailFrom, string emailSubject, string emailBody)
		{
			this.Context.Session["EmailBody_Shipment_Documents_emailFrom"] = emailFrom;
			this.Context.Session["EmailBody_Shipment_Documents_emailSubject"] = emailSubject;
			this.Context.Session["EmailBody_Shipment_Documents_emailBody"] = emailBody;
			//Response.Redirect("Email.aspx",false);
			
			//string command = "<script language='javascript'> ";
			//command += "window.open('../administrator/Email.aspx','','toolbar=false,resizable=1,scrollbars=1,menubar=false,width=800,height=750,top=20,left=100') ";
			//command += "</script>";
			//,screenX='+xOffset+',screenY='+yOffset+', top='+yOffset+',left='+xOffset+''
			//Page.RegisterClientScriptBlock("Pop up Email Preview",command);
			//Page.RegisterStartupScript("Pop up Email Preview",command);
		}

		public void btnEmail_Click(object sender, System.EventArgs e)
		{
            //Larry - Add unit to btnEmailPreview function
			btnEmailPreview_Click(sender, cbMetric.Checked, System.Convert.ToInt32(ddlType.SelectedValue), lblOrderID.Text, txtCustom.Text, Application["DBconn"].ToString(), Context.Session["Name"].ToString());

           // SaveEmailComment(lblOrderID.Text, System.Convert.ToInt16(ddlType.SelectedValue));
        }

		private void SaveEmailComment(string TransactionID, int DocumentType)
		{
		    string[] ID = TransactionID.Split('-');
		    string sShipiD= HelperFunction.getShipmentID(ID[0],ID[1],this.Context);            

		    string sSQL = "INSERT into Shipment_Comment (Comment_Shipment, Comment_PersID, Comment_Date, Comment_Text) values ({0},{1},'{2}','{3}')";            

		    DBLibrary.ExecuteSQLStatement(Application["DBConn"].ToString(), string.Format(sSQL,sShipiD,Session["Id"].ToString(),System.DateTime.Today.Date,"Sent: " +  GetDocumentType(DocumentType)));
		}

		private string GetDocumentType(int DocumentType)
		{
		    switch (DocumentType)
		    {
			case 1:
	                    
			    return "Invoice";
			    break;
			case 2:
			    return "Purchase Order";
			    break;
			case 3:
			    return "Certificate";
			    break;
			case 4:
			    return "Sales Confirmation";
			    break;
			case 5:
			    return "Release Customer";
			    break;
			case 6:
			    return "Title Transfer";
			    break;
			case 8:
			    return "Release Material";
			    break;
			case 9:
			    return "Release from Inventory";
			    break;
            case 10:
                return "Proforma Invoice";
                break;
			default:
			    return "N/A";
			    break;
		    }
		}

		public void btnEmailPreview_Click(object sender, bool metric, int DocumentType, string TransactionID, string CustomField, string dbConnectionString, string Name)
		{
			string emailBody = "";
			string emailSubject = "";
			string[] ID = TransactionID.Split('-');

            //Larry - Add unit to InvoicePOSalesConfirmation function
			switch(DocumentType)
			{
				case 1:
					emailBody = InvoicePOSalesConfirmation(metric, ID[0], ID[1], "IN", CustomField, dbConnectionString);
					emailSubject = "Invoice";
					break;
				case 2:
					emailBody = InvoicePOSalesConfirmation(metric, ID[0], ID[1], "PO", CustomField, dbConnectionString);
					emailSubject = "Purchase Order";
					break;
				case 3:
					emailBody = CertificateHTML(ID[0], ID[1], dbConnectionString);
					emailSubject = "Certificate";
					break;
				case 4:
					emailBody = InvoicePOSalesConfirmation(metric, ID[0], ID[1], "SC",CustomField, dbConnectionString);
					emailSubject = "Sales Confirmation";
					break;
				case 5:
					emailBody = ReleaseCustomer(ID[0], ID[1], dbConnectionString,Name);
					emailSubject = "Release Customer";
					break;
				case 6:
					emailBody = TitleTransfer(ID[0], ID[1],dbConnectionString,Name);
					emailSubject = "Title Transfer";
					break;
				case 8:
					emailBody = WarehouseRelease(ID[0], ID[1],CustomField, dbConnectionString, Name);
					emailSubject = "Release Material";
					break;
				case 9:
					emailBody = InventoryRelease(ID[0], ID[1], dbConnectionString, Name);
					emailSubject = "Release from Inventory";
					break;
                case 10:
                    emailBody = InvoicePOSalesConfirmation(metric, ID[0], ID[1], "PFIN", CustomField, dbConnectionString);
                    emailSubject = "Proforma Invoice";
                    break;
				default:
					break;
			}
			
			if (((Button)sender).Text  == "Preview Email")
			{
				PreviewEmail(getEmailUser(Context.Session["ID"].ToString(),dbConnectionString),emailSubject,emailBody);
			}
			else
			{
				string emailTO=ddlSendTo.SelectedItem.Text;
				string emailCC=ddlCC.SelectedItem.Text;
				if (chkCustomSendTo.Checked == true) emailTO = txtCustomSendTo.Text;
				if (chkCustomCC.Checked == true) emailCC = txtCustomCC.Text;

				SendEmail(getEmailUser(Session["ID"].ToString(),dbConnectionString),emailTO,emailCC,emailSubject,emailBody, ShipmentID(ID[0], ID[1], dbConnectionString), dbConnectionString);
				btnEmail.Enabled = false;
				lblMessage.Text = ddlType.SelectedItem.Text + " sent to " + ddlSendTo.SelectedItem.Text;
			}
		}

		private string ExchangeTimeFormat(string str)
		{
			string mystr="";
			if (str.Length > 2)
			{
				mystr=Convert.ToDateTime(str).ToString("MM/dd/yyyy");
			}
			return mystr.Replace("-","/");
		}

		private string ShipmentID(string shipmentOrderID, string shipmentSKU, string DBConn)
		{
			string ShipmentID = String.Empty;

			SqlConnection conn = new SqlConnection(DBConn);	
			Hashtable param = new Hashtable();
			param.Add("@shipmentOrderID",Int32.Parse(shipmentOrderID));
			param.Add("@shipmentSKU",shipmentSKU);
			try
			{
				conn.Open();
			
				//Looking for Shipment ID
				SqlDataReader drShipmentID = DBLibrary.GetDataReaderFromSelect(conn, "Select SHIPMENT_ID FROM SHIPMENT where SHIPMENT_ORDR_ID=@shipmentOrderID and SHIPMENT_SKU =@shipmentSKU",param);
				try
				{
					drShipmentID.Read();
					ShipmentID = drShipmentID["SHIPMENT_ID"].ToString();
				}
				finally
				{
					drShipmentID.Close();
				}
			}
			finally
			{
				conn.Close();
			}
			return ShipmentID;
		}

		private bool ShowTitleTransferLetter(string shipmentOrderID, string shipmentSKU, string DBConn)
		{
			bool bShow = false;
			
			SqlConnection conn = new SqlConnection(DBConn);	
			Hashtable param = new Hashtable();
			param.Add("@shipmentOrderID",shipmentOrderID);
			param.Add("@shipmentSKU",shipmentSKU);
			try
			{
				conn.Open();
			
				string sqlShip="SELECT SHIPMENT_SIZE, SHIPMENT_COMMENT FROM SHIPMENT WHERE SHIPMENT_ORDR_ID=@shipmentOrderID AND SHIPMENT_SKU = @shipmentSKU";
				SqlDataReader rd= DBLibrary.GetDataReaderFromSelect(conn,sqlShip,param);
				try
				{
					if(rd.Read())
					{
						if ((rd["SHIPMENT_SIZE"].ToString().Equals("190000")) && (HelperFunction.getSafeStringFromDB(rd["SHIPMENT_COMMENT"]) != ""))
						{
							bShow = true;
						}
					}
				}
				finally
				{
					rd.Close();
				}
			}
			finally
			{
				conn.Close();
			}
			return bShow;
		}

		private bool ShowInvoice(string shipmentOrderID, string shipmentSKU, string DBConn)
		{
			bool bShow = false;

			SqlConnection conn = new SqlConnection(DBConn);	
			Hashtable param = new Hashtable();
			param.Add("@shipmentOrderID",shipmentOrderID.Trim());
			param.Add("@shipmentSKU",shipmentSKU.Trim());
			try
			{
				conn.Open();
				string sqlShip="SELECT SHIPMENT_WEIGHT FROM SHIPMENT WHERE SHIPMENT_ORDR_ID=@shipmentOrderID AND SHIPMENT_SKU = @shipmentSKU";
				SqlDataReader rd= DBLibrary.GetDataReaderFromSelect(conn,sqlShip,param);
				try
				{
					if(rd.Read())
					{
						if (!rd["SHIPMENT_WEIGHT"].ToString().Equals(""))
						{
							bShow = true;
						}
					}
				}
				finally
				{
					rd.Close();
				}
				return bShow;
			}
			finally
			{
				conn.Close();
			}
		}
		
		private string HTMLHeader()
		{
			return HTMLHeader("left");
		}
		private string HTMLHeader(string alignmentType)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder("");
			sb.Append("<HTML>" + bl());
			sb.Append("<BODY bgColor=#ffffff STYLE='FONT: 10pt ARIAL'>" + bl());
			sb.Append("<BR><BR><BR>" + bl());
			sb.Append("<p align=left><table align=" + alignmentType + " border=0 cellspacing=0 cellpadding=0>" + bl());
			sb.Append("<tr><td align=" + alignmentType + "><img src=http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/images/email/tpelogo.gif></td>" + bl());
			sb.Append("<td align=" + alignmentType + ">" + bl());
			sb.Append("<font face=arial black size=4 color=Black>The</font><font face=arial black size=4 color=red>Plastics</font><font face=arial black size=4 color=Black>Exchange</font><font face=arial black size=4 color=red>.</font><font face=arial black size=4 color=Black>com</font>" + bl());
			sb.Append("</td>" + bl());
			sb.Append("</tr></table>" + bl());
			sb.Append("<br>" + bl());
			sb.Append("<br></p><br><br><br>");
    


			return sb.ToString();
		}

		private string HTMLFooter(string HtmlPersonName)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder("");
			sb.Append("<br>" + bl());
			sb.Append("<br>" + bl());
			sb.Append("<br>" + bl());
			sb.Append("Sincerely,<br>" + bl());
			sb.Append("<br>" + bl());
			sb.Append(HtmlPersonName + bl());
			sb.Append("<br>" + bl());
			sb.Append("The Plastics Exchange" + bl());
			sb.Append("<br>Tel: "+ ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + bl());
            sb.Append("<br>Fax: " + ConfigurationSettings.AppSettings["FaxNumber"].ToString() + bl());
			sb.Append("<br>" + bl());
			sb.Append("<br>" + bl());
			sb.Append("<table align=center width='100%'><tr><td colspan=3 valign=top align=center><hr></td></tr>");
            sb.Append("<tr><td colspan=3 valign=top align=center><font face=arial size=1 color=Black>16510 N. 92nd Street. #1010</font>&nbsp;&nbsp;<font face=arial size=1 color=red>Scottsdale, AZ 85260</font>&nbsp;&nbsp;<font face=arial size=1 color=Black><b>tel " + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + "</b></font>&nbsp;&nbsp;<font face=arial size=1 color=red>fax " + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + "</font></td></tr></table>");
			sb.Append("</BODY></HTML>" + bl());

			return sb.ToString();
		}

		private void btnPreview_Click(object sender, System.EventArgs e)
		{
		
		}

		protected void ddlType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			PopulateScreen(ddlType.SelectedValue, Application["DBconn"].ToString());
		}

		private string HTMLFooter()
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder("");
			sb.Append("<br>" + bl());
			sb.Append("<br>" + bl());
			sb.Append("<br>" + bl());
			sb.Append("<br>" + bl());
			sb.Append("<table align=center width='100%'><tr><td colspan=3 valign=top align=center><hr></td></tr>");
            sb.Append("<tr><td colspan=3 valign=top align=center><font face=arial size=1 color=Black>16510 N. 92nd Street. #1010</font>&nbsp;&nbsp;<font face=arial size=1 color=red>Scottsdale, AZ 85260</font>&nbsp;&nbsp;<font face=arial size=1 color=Black><b>tel " + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + "</b></font>&nbsp;&nbsp;<font face=arial size=1 color=red>fax " + ConfigurationSettings.AppSettings["FaxNumber"].ToString() + "</font></td></tr></table>");
			sb.Append("</BODY></HTML>" + bl());

			return sb.ToString();
		}

		private void InsertComment(string ShipmentID, string Message, string DBConn)
		{
			string strSQL = "INSERT into Shipment_Comment (Comment_Shipment, Comment_PersID, Comment_Date, Comment_Text) values ('"+ShipmentID+"','"+ Session["Id"].ToString()+"','"+System.DateTime.Today.Date+"','"+Message+"')";
			DBLibrary.ExecuteSQLStatement(DBConn, strSQL);
		}

		protected void chkCustomSendTo_CheckedChanged(object sender, System.EventArgs e)
		{
			//txtCustomSendTo.Enabled = chkCustomSendTo.Checked;
			//ddlSendTo.Enabled = !chkCustomSendTo.Checked;
		}

		protected void CheckBox1_CheckedChanged(object sender, System.EventArgs e)
		{
			//txtCustomCC.Enabled = chkCustomCC.Checked;
			//ddlCC.Enabled = !chkCustomCC.Checked;
		}

		private string BreakMultilineFieldHTML(string txtContent)
		{
			return txtContent.Replace(System.Convert.ToString((char)13),"<BR>").ToString();
		}

		protected void cbMetric_Click(object sender, System.EventArgs e)
		{
		    //Larry - get the new info about the metric and save it to the session
		    Session["EmailBody_Shipment_Documents_Metric"] = cbMetric.Checked;
		}

	}
}