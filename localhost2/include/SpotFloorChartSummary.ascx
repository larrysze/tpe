<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SpotFloorChartSummary.ascx.cs" Inherits="localhost.include.SpotFloorChartSummary" %>

  <script language="javascript" type="text/javascript">
    
    function erase()
    {
        var target = document.someform.somename;
        target.value = "";
    }
    
    		
		Chart1 = new Image();
		Chart1.src = "/Research/Charts/Chart_1_1Y.png";
		
		
		function defaultOnLoad(indx)
		{
		    changeChartName(indx);
		}
		
    function LoadImg()
    {          
		Chart2 = new Image();
		Chart2.src = "/Research/Charts/Chart_2_1Y.png";
	    
		Chart3 = new Image();
		Chart3.src = "/Research/Charts/Chart_3_1Y.png";
	    
		Chart4 = new Image();
		Chart4.src = "/Research/Charts/Chart_6_1Y.png";
	    
		Chart5 = new Image();
		Chart5.src = "/Research/Charts/Chart_10_1Y.png";
	    
		Chart6 = new Image();
		Chart6.src = "/Research/Charts/Chart_14_1Y.png";
	    
		Chart7 = new Image();
		Chart7.src = "/Research/Charts/Chart_17_1Y.png";
	    
		Chart8 = new Image();
		Chart8.src = "/Research/Charts/Chart_26_1Y.jpg";
	    
		Chart9 = new Image();
		Chart9.src = "/Research/Charts/Chart_22_1Y.png";
	    
		Chart10 = new Image();
		Chart10.src = "/Research/Charts/Chart_19_1Y.png";
	    
		Chart11 = new Image();
		Chart11.src = "/Research/Charts/Chart_20_1Y.png";
		
    }
    

    function changeChartName(grade){

        switch(grade){       
		        case "1":
			        document.getElementById("lblChartName").innerHTML = "HDPE Film - HMW";
			        break;
		        case "5":
			        document.getElementById("lblChartName").innerHTML = "HDPE Inj - Pail";
			        break;
		        case "3":
			        document.getElementById("lblChartName").innerHTML = "HDPE Blow - HIC";
			        break;			        
		        case "6":
			        document.getElementById("lblChartName").innerHTML = "LDPE Film - Clarity";
			        break;				        
		        case "10":
			        document.getElementById("lblChartName").innerHTML = "LDPE Inj - 20 melt";
			        break;				        
		        case "11":
			        document.getElementById("lblChartName").innerHTML = "LLDPE Film - Butene";
			        break;				        			       
		        case "17":
			        document.getElementById("lblChartName").innerHTML = "LLDPE Inj - 20 melt";			        
			        break;				        			            
		        case "9":
			        document.getElementById("lblChartName").innerHTML = "HoPP Inj - 20 melt";			        			        
			        break;				        			            			        
		        case "22":
			        document.getElementById("lblChartName").innerHTML = "CoPP Inj - 20 melt";			        			        
			        break;				        			            			        
		        case "19":
			        document.getElementById("lblChartName").innerHTML = "GPPS Inj - 8 melt";			        			        
			        break;				        			            			        
		        case "20":
			        document.getElementById("lblChartName").innerHTML = "HIPS Inj - 8 melt";			        			        
			        break;
				        			            			                        			        
        }

    }
	
    </script>



<TABLE cellSpacing=0 cellPadding=0 width=780 align=center bgColor=#333333 border=0><TBODY><TR><TD width="100%" colSpan=3 height=18>
    <APPLET height=18 width=780 code="menuscroll2.class" viewastext>
        <PARAM NAME="yposition" VALUE="14">
        <PARAM NAME="arrow_color" VALUE="FFFFFF">
        <PARAM NAME="highlight_rect_color" VALUE="CC6600">
        <PARAM NAME="regcode" VALUE="8mnxzaqre58g">
        <PARAM NAME="highlight_arrow_color" VALUE="CC6600">
        <PARAM NAME="font_size" VALUE="10">
        <PARAM NAME="info" VALUE="Applet by Gokhan Dagli,www.appletcollection.com">
        <PARAM NAME="applet_height" VALUE="14">
        <PARAM NAME="text_color" VALUE="000000">
        <PARAM NAME="font_type" VALUE="Arial,Verdana">
        <PARAM NAME="space" VALUE="10">
        <PARAM NAME="bgcolor" VALUE="000000">
        <PARAM NAME="arrow_bgcolor" VALUE="000000">
        <PARAM NAME="scroll_delay" VALUE="10">
        <PARAM NAME="highlight_text_color" VALUE="FFFFFF">
        <PARAM NAME="mouse_over" VALUE="stop">
        <PARAM NAME="rect_color" VALUE="CCCCCC">
        <PARAM NAME="border_color" VALUE="000000">
        <PARAM NAME="applet_width" VALUE="100%">
        <PARAM NAME="xspace" VALUE="1">
        <PARAM NAME="font_style" VALUE="0">
        <PARAM NAME="scroll_jump" VALUE="1">
        <asp:placeholder id="phSpotParameters" runat="server" />
    </APPLET> 
                
                </TD></TR><TR><TD class="LinkNormal OrangeLink" vAlign=middle align=left width=184 background="/pics/home_bar_titles_r1_c1.jpg" height=10>
                <STRONG class="Header Color1">Resin Markets</STRONG> </TD><TD background="/pics/home_bar_titles_r1_c1.jpg"></TD><TD style="HEIGHT: 14px" align=left width="50%" background="/pics/home_bar_titles_r1_c1.jpg"><DIV id="lblChartName" class="Header Bold Color3">loading...</DIV></TD></TR><TR bgColor=black><TD style="HEIGHT: 14px"><STRONG class="Content Bold Color3">&nbsp;<asp:label id="lblType" runat="server" Font-Size="9"></asp:label></STRONG>&nbsp; <SPAN style="HEIGHT: 12px" class="LinkNormal WhiteLink"><asp:hyperlink id="hplTotalLbs" runat="server" onprerender="hplTotalLbs_PreRender"></asp:hyperlink></SPAN> <asp:label id="lblMode" runat="server" Visible="False"></asp:label> </TD><TD style="HEIGHT: 10px" align=left>                                                  
                </TD><TD align=center></TD></TR></TBODY></TABLE>
 <TABLE cellSpacing=0 
 cellPadding=0 width=780 align=center bgColor=#000000 border=0><TBODY><TR><TD width=390>
                <asp:datagrid id="dgSpot" runat="server" width="100%" bordercolor="Black" enableviewstate="False" horizontalalign="Center" autogeneratecolumns="False" cellpadding="3" font-size="Small" gridlines="Horizontal" borderstyle="None" backcolor="Transparent" borderwidth="0px" forecolor="White" cssclass="LinkNormal WhiteLink">
                    <AlternatingItemStyle BorderStyle="Solid" BackColor="Black"></AlternatingItemStyle>
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <Columns>
                        <asp:HyperLinkColumn DataNavigateUrlField="offr_grade" DataNavigateUrlFormatString="/spot/Spot_Floor.aspx?Filter={0}" DataTextField="GRADE" HeaderText="Resin">
                            <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
                        </asp:HyperLinkColumn>
                        <asp:BoundColumn DataField="varsize" HeaderText="Total lbs" DataFormatString="{0:#,##}">
                            <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
                            <ItemStyle Wrap="False" CssClass="Content Bold Color4"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Min" HeaderText="Low" DataFormatString="{0:0.##0}">
                            <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
                            <ItemStyle Wrap="False" CssClass="Content Bold Color4"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Max" HeaderText="High" DataFormatString="{0:0.##0}">
                            <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
                            <ItemStyle Wrap="False" CssClass="Content Bold Color4"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="Price Range">
                            <HeaderStyle CssClass="Content Bold Color4"></HeaderStyle>
                            <ItemStyle Wrap="False" CssClass="Content Bold Color4"></ItemStyle>
                        </asp:TemplateColumn>
                    </Columns>
                    <ItemStyle BorderColor="Black" BorderWidth="0px" />
                </asp:datagrid>
                </TD><TD><asp:literal id="LiteralImg" runat="server"></asp:literal> </TD></TR></TBODY></TABLE>


