<%@ Control Language="c#" AutoEventWireup="True" Codebehind="ScreenInstructions.ascx.cs" Inherits="localhost.include.ScreenInstructions" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<link href="/tpe.css" rel="stylesheet" type="text/css">
<style type="text/css">
body
{
	margin:0px;	
}

#alwayshide
{
	position: absolute;
	top:0;
	float: right;
	right:3;
}
.dhtmlgoodies_question{	/* Styling question */
	/* Start layout CSS */
	font-weight:bold;
	text-align:right;
	color:black;
	width:<% if (Width != null) { Response.Write(Width+"px;");} else {Response.Write("780px;");} %>
	margin-bottom:2px;
	margin-top:2px;
	height:'18px';
	
	/* End layout CSS */
	
	overflow:hidden;
	cursor:pointer;
}
	
.dhtmlgoodies_answer{	/* Parent box of slide down content */
	/* Start layout CSS */
	/*border:1px solid #317082;*/
	width:780px;
	
	/* End layout CSS */
	
	visibility:hidden;
	height:0px;
	overflow:hidden;
	position:relative;

}
.dhtmlgoodies_answer_content{	/* Content that is slided down */
	padding:1px;
	/*font-size:0.9em;	*/
	position:relative;
	margin-right:35px;
}
.showOrHide{
	color: red;
	/*font-style:italic;
	font-weight:normal;*/
}
</style>
<script type="text/javascript">
/************************************************************************************************************
(C) www.dhtmlgoodies.com, November 2005

This is a script from www.dhtmlgoodies.com. You will find this and a lot of other scripts at our website.	

Terms of use:
You are free to use this script as long as the copyright message is kept intact. However, you may not
redistribute, sell or repost it without our permission.


Thank you!

www.dhtmlgoodies.com
Alf Magne Kalleland

Updated:
	April, 3rd, 2006: Fixed problem with initial sliding in IE.

************************************************************************************************************/

var dhtmlgoodies_slideSpeed = 11;	// 10 Higher value = faster
var dhtmlgoodies_timer = 9;	// 10 Lower value = faster

var objectIdToSlideDown = false;
var dhtmlgoodies_activeId = false;

function showHideContent(e,inputId)
{
	if(!inputId)inputId = this.id;
	inputId = inputId + '';
	var numericId = inputId.replace(/[^0-9]/g,'');
	
	var answerDiv = document.getElementById('dhtmlgoodies_a' + numericId);
	
	objectIdToSlideDown = false;
	
	if(!answerDiv.style.display || answerDiv.style.display=='none'){

		if(dhtmlgoodies_activeId &&  dhtmlgoodies_activeId!=numericId){			
			objectIdToSlideDown = numericId;
			slideContent(dhtmlgoodies_activeId,(dhtmlgoodies_slideSpeed*-1));
		}else{
			
			answerDiv.style.display='block';
			answerDiv.style.visibility = 'visible';
			
			slideContent(numericId,dhtmlgoodies_slideSpeed);
		}
	}else{
		slideContent(numericId,(dhtmlgoodies_slideSpeed*-1));
		dhtmlgoodies_activeId = false;
	}	
	// my code
	var showOrHide = document.getElementById('showOrHide');
	if (showOrHide.innerHTML == 'show') {showOrHide.innerHTML = 'hide';} else {showOrHide.innerHTML = 'show';}
	var alwaysHide = document.getElementById('alwaysHide');
	if (showOrHide.innerHTML == 'hide')
	{
		alwaysHide.style.visibility = 'visible';
	}
	else
	{
		alwaysHide.style.visibility = 'hidden';
	}
	// end my code	
}

function slideContent(inputId,direction)
{
	var obj =document.getElementById('dhtmlgoodies_a' + inputId);
	var contentObj = document.getElementById('dhtmlgoodies_ac' + inputId);
	height = obj.clientHeight;
	height = height + direction;
	rerunFunction = true;
	if(height>contentObj.offsetHeight){
		height = contentObj.offsetHeight;
		rerunFunction = false;
	}
	if(height<=1){
		height = 1;
		rerunFunction = false;
	}

	obj.style.height = height + 'px';
	var topPos = height - contentObj.offsetHeight;
	if(topPos>0)topPos=0;
	contentObj.style.top = topPos + 'px';
	if(rerunFunction){
		setTimeout('slideContent(' + inputId + ',' + direction + ')',dhtmlgoodies_timer);
	}else{
		if(height<=1){
			obj.style.display='none'; 
			if(objectIdToSlideDown && objectIdToSlideDown!=inputId){
				document.getElementById('dhtmlgoodies_a' + objectIdToSlideDown).style.display='block';
				document.getElementById('dhtmlgoodies_a' + objectIdToSlideDown).style.visibility='visible';
				slideContent(objectIdToSlideDown,dhtmlgoodies_slideSpeed);				
			}
		}else{
			dhtmlgoodies_activeId = inputId;
		}
	}
}


function initShowHideDivs()
{
	var alwaysHide = (readCookie('tpe_alwayshideinstructions') == 'true');
    var divs = document.getElementsByTagName('DIV');
	var divCounter = 1;
	for(var no=0;no<divs.length;no++){
		if(divs[no].className=='dhtmlgoodies_question')
		{
			divs[no].onclick = showHideContent;
			divs[no].id = 'dhtmlgoodies_q'+divCounter;
			var answer = divs[no].nextSibling;
			while(answer && answer.tagName!='DIV')
			{
				answer = answer.nextSibling;
			}
			
			answer.id = 'dhtmlgoodies_a'+divCounter;	
			
			contentDiv = answer.getElementsByTagName('DIV')[0];
			contentDiv.style.top = 0 - contentDiv.offsetHeight + 'px'; 	
			contentDiv.className='dhtmlgoodies_answer_content';
			contentDiv.id = 'dhtmlgoodies_ac' + divCounter;
			
			if (alwaysHide == true)
			{
				answer.style.display='none';
				answer.style.height='1px';
				// change text to 'show'
				var showOrHide = document.getElementById('showOrHide');
				showOrHide.innerHTML = 'show';
				document.getElementById('alwaysHideBox').checked = true;
			}
			else
			{
				answer.style.display='block';
				answer.style.visibility = 'visible';
				slideContent(1,100);
			}
								
			divCounter++;
		}			
	}	
	/* hide if cookied, else defulat is to show */		
}
function hideall(theBox)
{
	if (theBox.checked)
	{
		createCookie('tpe_alwayshideinstructions','true',999)
		// now hide the instructions
		showHideContent(null,'1');		
	}
	else
	{
		eraseCookie('tpe_alwayshideinstructions');
	}
}

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0)
		{
			return c.substring(nameEQ.length,c.length);
		}
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

window.onload = initShowHideDivs;
</script>

<!-- CONTENT -->
<div class="dhtmlgoodies_question"><span class=black_underline><a href='#' onclick=';'><span class="redBold" id="showOrHide">hide</span><span class="blackbold"> Instructions</span></a></span>&nbsp;</div>
<div class="dhtmlgoodies_answer">
	<div>
	<div align="right" id="alwaysHide"><input type="checkBox" id="alwaysHideBox" onclick="hideall(this)"><span class=blackcontent>always hide</span></div>
	<%=Content%>
	</div>
</div>