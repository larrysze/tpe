using System;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Collections;
using System.Configuration;
using TPE.Utility;

namespace localhost
{
	public enum TruckType
	{
		BulkTruck = 0,
		TruckLoad = 1,
	}

	/// <summary>
	/// Summary description for HelperFunctionDistances.
	/// </summary>
	public class HelperFunctionDistances
	{
		public HelperFunctionDistances()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public bool calculateFreightByZipCode(string zipCodeFROM, string zipCodeTO, ref decimal refFreight, TruckType truckType, string dbConnectionString)
		{
			string placeFROM ="";
			string placeTO = "";
			return calculateFreightByZipCode(zipCodeFROM, zipCodeTO, ref placeFROM, ref placeTO, ref refFreight, truckType, dbConnectionString);
		}

		public bool calculateFreightByZipCode(string zipCodeFROM, string zipCodeTO, ref string refPlaceFROM, ref string refPlaceTO, ref decimal refFreight, TruckType truckType, string dbConnectionString)
		{
			bool validFreight = true;
			decimal freightFactor = 0;
			bool validFROM = false;
			bool validTO = false;
			double latitudeFROM = 0;
			double latitudeTO = 0;
			double longitudeFROM = 0;
			double longitudeTO = 0;
			string CityFROM = "";
			string CityTO = "";
			string StateFROM = "";
			string StateTO = "";
			
			//Fetch latitute/longitude from table by zipcode with city/state to/from
			string strSQL="SELECT DISTANCE_CITY, DISTANCE_STATE, DISTANCE_LATITUDE, DISTANCE_LONGITUDE " +
				"FROM DISTANCE_ZIPCODE WHERE DISTANCE_ZIPCODE = @ZipCode";//'" + zipCodeFROM + "'";
			Hashtable param = new Hashtable();
			param.Add("@ZipCode",zipCodeFROM);
			SqlConnection conn = new SqlConnection(dbConnectionString);
			SqlDataReader drDistances = null;
			try
			{
				conn.Open();
				try
				{
					drDistances = DBLibrary.GetDataReaderFromSelect(conn,strSQL, param);

					if (drDistances.Read())
					{
						latitudeFROM = System.Convert.ToDouble(drDistances["DISTANCE_LATITUDE"].ToString());
						longitudeFROM = System.Convert.ToDouble(drDistances["DISTANCE_LONGITUDE"].ToString());
						CityFROM = drDistances["DISTANCE_CITY"].ToString();
						StateFROM = drDistances["DISTANCE_STATE"].ToString();
						validFROM = true;
					}
				}
				finally
				{
					drDistances.Close();
				}
			
				string strSQL2="SELECT DISTANCE_CITY, DISTANCE_STATE, DISTANCE_LATITUDE, DISTANCE_LONGITUDE " +
					"FROM DISTANCE_ZIPCODE WHERE DISTANCE_ZIPCODE = @ZipCode";//'" + zipCodeTO + "'";
				param.Clear();
				param.Add("@ZipCode",zipCodeTO);
				try
				{
					drDistances = DBLibrary.GetDataReaderFromSelect(conn,strSQL2,param);
					if (drDistances.Read())
					{
						latitudeTO = System.Convert.ToDouble(drDistances["DISTANCE_LATITUDE"].ToString());
						longitudeTO = System.Convert.ToDouble(drDistances["DISTANCE_LONGITUDE"].ToString());
						CityTO = drDistances["DISTANCE_CITY"].ToString();
						StateTO = drDistances["DISTANCE_STATE"].ToString();
						validTO = true;
					}
				} 
				finally
				{
					drDistances.Close();
				}
			}
			finally
			{
				conn.Close();
			}

			//Calculate distances in miles
			double milesDistance;
			if ((validFROM) && (validTO))
			{
				milesDistance = distance(latitudeFROM, longitudeFROM, latitudeTO, longitudeTO, 'M');
				
				if (truckType == TruckType.TruckLoad) // TRUCK LOAD
				{
					decimal minimumFreight = 0;
					//Fetch the factor from shipping_freight_matrix by stateTO and stateFROM
					if (getTruckLoadFreightFactor(StateFROM, StateTO, ref freightFactor, ref minimumFreight, dbConnectionString)==true)
					{
						//Multiply the factor per number of miles
						refFreight = freightFactor * System.Convert.ToDecimal(milesDistance);
						//If the final freight doesn't reach the minimum freight
						if (refFreight < minimumFreight) refFreight = minimumFreight;

						refPlaceFROM = CityFROM + ", " + StateFROM;
						refPlaceTO = CityTO + ", " + StateTO;
					}
					else
					{
						validFreight = false;
					}
				}
				else //BULK TRUCK
				{
					if (getBulkTruckFreight(System.Convert.ToInt32(milesDistance), ref refFreight, dbConnectionString))
					{
						refPlaceFROM = CityFROM + ", " + StateFROM;
						refPlaceTO = CityTO + ", " + StateTO;
					}
					else
					{
						validFreight = false;
					}
				}
			}
			else
			{
				validFreight = false;
			}
            
			return validFreight;
		}

		public bool calculateFreightByCity(string CityFROM, string StateFROM, string CityTO, string StateTO, ref decimal refFreight, TruckType truckType, string dbConnectionString)
		{
			bool validFreight = true;
			decimal freightFactor = 0;
			bool validFROM = false;
			bool validTO = false;
			double latitudeFROM = 0;
			double latitudeTO = 0;
			double longitudeFROM = 0;
			double longitudeTO = 0;
			
			//Fetch latitute/longitude from table by zipcode with city/state to/from
			string strSQL="SELECT DISTANCE_LATITUDE, DISTANCE_LONGITUDE FROM DISTANCE_CITY WHERE DISTANCE_CITY = @CityFrom AND DISTANCE_STATE = @StateFrom";
			Hashtable param = new Hashtable();
			param.Add("@CityFrom",CityFROM);
			param.Add("@StateFrom",StateFROM);
			SqlConnection conn = new SqlConnection(dbConnectionString);
			SqlDataReader drDistances = null;
			try
			{
				conn.Open();
				try
				{
					drDistances = DBLibrary.GetDataReaderFromSelect(conn,strSQL,param);
					if (drDistances.Read())
					{
						latitudeFROM = System.Convert.ToDouble(drDistances["DISTANCE_LATITUDE"].ToString());
						longitudeFROM = System.Convert.ToDouble(drDistances["DISTANCE_LONGITUDE"].ToString());
						validFROM = true;
					}
				}
				finally
				{
					drDistances.Close();
				}

				string strSQL2="SELECT DISTANCE_LATITUDE, DISTANCE_LONGITUDE FROM DISTANCE_CITY WHERE DISTANCE_CITY = @CityTO AND DISTANCE_STATE = @StateTO";
				//string strSQL2="SELECT DISTANCE_LATITUDE, DISTANCE_LONGITUDE " +
				//	"FROM DISTANCE_CITY WHERE DISTANCE_CITY = '" + CityTO + "' AND DISTANCE_STATE = '" + StateTO +"'";
				param.Clear();
				param.Add("@CityTO",CityTO);
				param.Add("@StateTO",StateTO);
				try
				{
					drDistances = DBLibrary.GetDataReaderFromSelect(conn,strSQL2,param);
					if (drDistances.Read())
					{
						latitudeTO = System.Convert.ToDouble(drDistances["DISTANCE_LATITUDE"].ToString());
						longitudeTO = System.Convert.ToDouble(drDistances["DISTANCE_LONGITUDE"].ToString());
						validTO = true;
					}
				}
				finally
				{
					drDistances.Close();
				}
			}
			finally
			{
				conn.Close();
			}

			//Calculate distances in miles
			double milesDistance;
			if ((validFROM) && (validTO))
			{
				milesDistance = distance(latitudeFROM, longitudeFROM, latitudeTO, longitudeTO, 'M');
				
				if (truckType == TruckType.TruckLoad) // TRUCK LOAD
				{
					decimal minimumFreight = 0;
					
					//Fetch the factor from shipping_freight_matrix by stateTO and stateFROM
					if (getTruckLoadFreightFactor(StateFROM, StateTO,ref freightFactor, ref minimumFreight, dbConnectionString)==true)
					{
						//Multiply the factor per number of miles
						refFreight = freightFactor * System.Convert.ToDecimal(milesDistance);
						//If the final freight doesn't reach the minimum freight
						if (refFreight < minimumFreight) refFreight = minimumFreight;
					}
					else
					{
						validFreight = false;
					}
				}
				else //BULK TRUCK
				{
					if (!getBulkTruckFreight(System.Convert.ToInt32(milesDistance), ref refFreight, dbConnectionString))
					{
						validFreight = false;
					}
				}
			}
			else
			{
				validFreight = false;
			}
            
			return validFreight;
		}

		private bool getTruckLoadFreightFactor(string stateFROM, string stateTO, ref decimal refFreightFactor, ref decimal refMinimumFreight, string dbConnectionString)
		{
			//freightFactor
			bool freightFactor = false;

			string strSQL="SELECT PRICE, PRICE_MINIMUM FROM FREIGHT_MATRIX_TRUCKLOAD WHERE STATE_FROM = @stateFROM AND STATE_TO = @stateTO";
			Hashtable param = new Hashtable();
			param.Add("@stateFROM",stateFROM);
			param.Add("@stateTO",stateTO);

			SqlConnection conn = new SqlConnection(dbConnectionString);
			try
			{
				conn.Open();
				SqlDataReader drFFactor = DBLibrary.GetDataReaderFromSelect(conn,strSQL,param);
				try
				{
					if (drFFactor.Read())
					{
                        //April 7th, 2010, MG wants to increase 10% of the freight.         
                        //June 18th, 2010 MG wants to increase another 10% of the freight.         
						refFreightFactor = System.Convert.ToDecimal(drFFactor["PRICE"].ToString()) * 120 / 100;
						refMinimumFreight = System.Convert.ToDecimal(drFFactor["PRICE_MINIMUM"].ToString());
						freightFactor = true;
					}
				}
				finally
				{
					drFFactor.Close();
				}
			}
			finally
			{
				conn.Close();
			}
			return freightFactor;
		}

		private bool getBulkTruckFreight(int distanceMiles, ref decimal refFreight, string dbConnectionString)
		{
			//freightFactor
			bool freightAvailable = false;
			string strSQL="SELECT TOP 1 BTFM_PRICE FROM FREIGHT_MATRIX_BULK_TRUCK WHERE (BTFM_MILES < = @distanceMiles + 9) AND (@distanceMiles <= (SELECT TOP 1 BTFM_MILES FROM FREIGHT_MATRIX_BULK_TRUCK ORDER BY BTFM_MILES DESC)) ORDER BY BTFM_MILES DESC";
			Hashtable param = new Hashtable();
			param.Add("@distanceMiles", distanceMiles);
			
			SqlConnection conn = new SqlConnection(dbConnectionString);
			try
			{
				conn.Open();
	
				SqlDataReader drFreight = DBLibrary.GetDataReaderFromSelect(conn, strSQL, param);
				try
				{
					if (drFreight.Read())
					{
						refFreight = System.Convert.ToDecimal(drFreight["BTFM_PRICE"].ToString());
						freightAvailable = true;
					}
				}
				finally
				{
					drFreight.Close();
				}
			}
			finally
			{
				conn.Close();
			}
			return freightAvailable;
		}

		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//:::                                                                         :::
		//:::  This routine calculates the distance between two points (given the     :::
		//:::  latitude/longitude of those points). It is being used to calculate     :::
		//:::  the distance between two ZIP Codes or Postal Codes using our           :::
		//:::  ZIPCodeWorld(TM) and PostalCodeWorld(TM) products.                     :::
		//:::                                                                         :::
		//:::  Definitions:                                                           :::
		//:::    South latitudes are negative, east longitudes are positive           :::
		//:::                                                                         :::
		//:::  Passed to function:                                                    :::
		//:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
		//:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
		//:::    unit = the unit you desire for results                               :::
		//:::           where: 'M' is statute miles                                   :::
		//:::                  'K' is kilometers (default)                            :::
		//:::                  'N' is nautical miles                                  :::
		//:::                                                                         :::
		//:::  United States ZIP Code/ Canadian Postal Code databases with latitude   :::
		//:::  & longitude are available at http://www.zipcodeworld.com               :::
		//:::                                                                         :::
		//:::  For enquiries, please contact sales@zipcodeworld.com                   :::
		//:::                                                                         :::
		//:::  Official Web site: http://www.zipcodeworld.com                         :::
		//:::                                                                         :::
		//:::  Hexa Software Development Center ?All Rights Reserved 2004            :::
		//:::                                                                         :::
		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		private double distance(double lat1, double lon1, double lat2, double lon2, char unit) 
		{
			double theta = lon1 - lon2;
			double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
			dist = Math.Acos(dist);
			dist = rad2deg(dist);
			dist = dist * 60 * 1.1515;
			if (unit == 'K') 
			{
				dist = dist * 1.609344;
			} 
			else if (unit == 'N') 
			{
				dist = dist * 0.8684;
			}
			return (dist);
		}

		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//::  This function converts decimal degrees to radians             :::
		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		private double deg2rad(double deg) 
		{
			return (deg * Math.PI / 180.0);
		}

		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		//::  This function converts radians to decimal degrees             :::
		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		private double rad2deg(double rad) 
		{
			return (rad / Math.PI * 180.0);
		}

		public bool calculateInternationalFreight(int portIDFrom, int portIDTo, ref decimal refFreight, string dbConnectionString)
		{
			bool freightFactor = false;

			if (portIDFrom != portIDTo)
			{
				string strSQL="SELECT FMI.IFM_PRICE FROM FREIGHT_MATRIX_INTERNATIONAL FMI, INTERNATIONAL_PORT IP, INTERNATIONAL_PORT IP2 WHERE FMI.IFM_ORIGIN_PORT = IP.PORT_ID AND FMI.IFM_ORIGIN_PORT = @portIDFrom AND IP.PORT_ENBL = 1 AND IP2.PORT_GROUP = FMI.IFM_DESTINATION_GROUP AND IP2.PORT_ID = @portIDTo";
				Hashtable param = new Hashtable();
				param.Add("@portIDFrom",portIDFrom);
				param.Add("@portIDTo",portIDTo);
				SqlConnection conn = new SqlConnection(dbConnectionString);
				SqlDataReader drFreight = null;
				try
				{
					conn.Open();
					try 
					{
						drFreight = DBLibrary.GetDataReaderFromSelect(conn,strSQL,param);
						if (drFreight.Read())
						{
							refFreight = System.Convert.ToDecimal(drFreight["IFM_PRICE"].ToString());
							freightFactor = true;
						}
					}
					finally
					{
						drFreight.Close();
					}
				}
				finally
				{
					conn.Close();
				}
			}
			return freightFactor;
		}

		static public bool IsValidZipCode(string zipCode, ref string City, ref string State, string dbConnectionString)
		{
			bool valid = false;

			string strSQL="SELECT DISTANCE_CITY, DISTANCE_STATE FROM DISTANCE_ZIPCODE WHERE DISTANCE_ZIPCODE = @zipCode";
			Hashtable param = new Hashtable();
			param.Add("@zipCode",zipCode);
			SqlConnection conn = new SqlConnection(dbConnectionString);
			try
			{
				conn.Open();
				SqlDataReader drDistances = DBLibrary.GetDataReaderFromSelect(conn,strSQL,param);
				try
				{
					if (drDistances.Read())
					{
						City = drDistances["DISTANCE_CITY"].ToString();
						State = drDistances["DISTANCE_STATE"].ToString();
						valid = true;
					}
				}
				finally
				{
					drDistances.Close();
				}
			}
			finally
			{
				conn.Close();
			}
			return valid;
		}

		public void loadMostPopularCities(DropDownList ddlControl, string strUserType)
		{
			ddlControl.Items.Clear();
			ddlControl.Items.Add(new ListItem("Select Popular City", ""));

			if (strUserType == "A" || strUserType == "L")
			{
				ddlControl.Items.Add(new ListItem("CA - Buena Park", "90620"));
				ddlControl.Items.Add(new ListItem("IL - Chicago", "60610"));
				ddlControl.Items.Add(new ListItem("TX - Houston", "77001"));
				ddlControl.Items.Add(new ListItem("---------------", ""));
			}
			ddlControl.Items.Add(new ListItem("AK - Anchorage", "99501"));
			ddlControl.Items.Add(new ListItem("AL - Birmingham", "35201"));
			ddlControl.Items.Add(new ListItem("AL - Mobile", "36601"));
			ddlControl.Items.Add(new ListItem("AR - Little Rock", "72201"));
			ddlControl.Items.Add(new ListItem("AZ - Phoenix", "85040"));
			ddlControl.Items.Add(new ListItem("AZ - Tucson", "85731"));
			ddlControl.Items.Add(new ListItem("AZ - Nogales", "85621"));
			ddlControl.Items.Add(new ListItem("BC - Vancouver", "V5K 1A1"));
			ddlControl.Items.Add(new ListItem("CA - Buena Park", "90620"));
			ddlControl.Items.Add(new ListItem("CA - Calexico", "92231"));
			ddlControl.Items.Add(new ListItem("CA - Fresno", "93717"));
			ddlControl.Items.Add(new ListItem("CA - Los Angeles", "90001"));
			ddlControl.Items.Add(new ListItem("CA - Oakland", "94617"));
			ddlControl.Items.Add(new ListItem("CA - Orange County", "92808"));
			ddlControl.Items.Add(new ListItem("CA - Riverside", "92509"));
			ddlControl.Items.Add(new ListItem("CA - Sacramento", "94232"));
			ddlControl.Items.Add(new ListItem("CA - San Diego", "92119"));
			ddlControl.Items.Add(new ListItem("CA - San Francisco", "94101"));
			ddlControl.Items.Add(new ListItem("CA - San Jose", "95121"));
			ddlControl.Items.Add(new ListItem("CT - Bridgeport", "06605"));
			ddlControl.Items.Add(new ListItem("CO - Colorado Springs", "80901"));
			ddlControl.Items.Add(new ListItem("CO - Denver", "80218"));
			ddlControl.Items.Add(new ListItem("DC - Washington", "20012"));
			ddlControl.Items.Add(new ListItem("DE - Wilmington", "19885"));
			ddlControl.Items.Add(new ListItem("FL - Jacksonville", "32201"));
			ddlControl.Items.Add(new ListItem("FL - Miami-Dade", "33187"));
			ddlControl.Items.Add(new ListItem("FL - Orlando", "32824"));
			ddlControl.Items.Add(new ListItem("FL - Tampa-St. Petersburg", "33619"));
			ddlControl.Items.Add(new ListItem("GA - Atlanta", "30315"));
			ddlControl.Items.Add(new ListItem("GA - Norcross", "30003"));
			ddlControl.Items.Add(new ListItem("HI - Honolulu", "96821"));
			ddlControl.Items.Add(new ListItem("IA - Des Moines", "50301"));
			ddlControl.Items.Add(new ListItem("ID - Boise", "83701"));
			ddlControl.Items.Add(new ListItem("IL - Chicago", "60610"));
			ddlControl.Items.Add(new ListItem("IL - Chicago Heights", "60411"));
			ddlControl.Items.Add(new ListItem("IL - Streator ", "61364"));
			ddlControl.Items.Add(new ListItem("IL - Villa Park", "60181"));
			ddlControl.Items.Add(new ListItem("IL - East Saint Louis", "62201"));
			ddlControl.Items.Add(new ListItem("IN - Terre Haute", "47801"));
			ddlControl.Items.Add(new ListItem("IN - Indianapolis", "46201"));
			ddlControl.Items.Add(new ListItem("KS - Wichita", "67201"));
			ddlControl.Items.Add(new ListItem("KY - Lexington", "40507"));
			ddlControl.Items.Add(new ListItem("KY - Louisville", "40201"));
			ddlControl.Items.Add(new ListItem("LA - New Orleans", "70112"));
			ddlControl.Items.Add(new ListItem("LA - Shreveport", "71101"));
			ddlControl.Items.Add(new ListItem("LA - DeQuincy", "70633"));
			ddlControl.Items.Add(new ListItem("MA - Boston", "02101"));
			ddlControl.Items.Add(new ListItem("MA - Worcester", "01601"));
			ddlControl.Items.Add(new ListItem("MA - Ware", "01082"));
			ddlControl.Items.Add(new ListItem("MD - Baltimore", "21224"));
			ddlControl.Items.Add(new ListItem("ME - Portland", "04101"));
			ddlControl.Items.Add(new ListItem("MI - Detroit", "48201"));
			ddlControl.Items.Add(new ListItem("MI - Grand Rapids", "49501"));
			ddlControl.Items.Add(new ListItem("MN - Minneapolis", "55401"));
			ddlControl.Items.Add(new ListItem("MO - Kansas City", "64101"));
			ddlControl.Items.Add(new ListItem("MO - St. Louis", "63101"));
			ddlControl.Items.Add(new ListItem("MS - Jackson", "39203"));
			ddlControl.Items.Add(new ListItem("MT - Billings", "59101"));            
			ddlControl.Items.Add(new ListItem("NC - Charlotte", "28201"));
			ddlControl.Items.Add(new ListItem("NC - Raleigh", "27603"));
			ddlControl.Items.Add(new ListItem("ND - Fargo", "58104"));
			ddlControl.Items.Add(new ListItem("NE - Omaha", "68112"));
			ddlControl.Items.Add(new ListItem("NH - Manchester", "03101"));
			ddlControl.Items.Add(new ListItem("NJ - Newark", "07101"));
			ddlControl.Items.Add(new ListItem("NJ - Jersey City", "07097"));
			ddlControl.Items.Add(new ListItem("NJ - Edison", "08817"));
			ddlControl.Items.Add(new ListItem("NM - Albuquerque", "87101"));
			ddlControl.Items.Add(new ListItem("NV - Las Vegas", "89101"));
			ddlControl.Items.Add(new ListItem("NY - Buffalo", "14201"));
			ddlControl.Items.Add(new ListItem("NY - New York", "10001"));
			ddlControl.Items.Add(new ListItem("OH - Akron", "44333"));
			ddlControl.Items.Add(new ListItem("OH - Cincinnati", "45201"));
			ddlControl.Items.Add(new ListItem("OH - Cleveland", "44101"));
			ddlControl.Items.Add(new ListItem("OH - Columbus", "43201"));
			ddlControl.Items.Add(new ListItem("OH - Toledo", "43601"));
			ddlControl.Items.Add(new ListItem("OH - Mogadore", "44260")); 
			ddlControl.Items.Add(new ListItem("OK - Oklahoma City", "73101"));
			ddlControl.Items.Add(new ListItem("OK - Tulsa", "74101"));
			ddlControl.Items.Add(new ListItem("OR - Portland", "97202"));
			ddlControl.Items.Add(new ListItem("ON - Hamilton", "L8K 2C7"));
			ddlControl.Items.Add(new ListItem("ON - Guelph", "N1H 4M8"));
            ddlControl.Items.Add(new ListItem("ON - Mississauga", "L5H 1Z6"));
            ddlControl.Items.Add(new ListItem("ON - Toronto", "M5B 2H1"));
			ddlControl.Items.Add(new ListItem("PA - Philadelphia", "19019"));
			ddlControl.Items.Add(new ListItem("PA - Pittsburgh", "15201"));
			ddlControl.Items.Add(new ListItem("QC - Lachine", "H8S 1R1"));
			ddlControl.Items.Add(new ListItem("RI - Providence", "02902"));
			ddlControl.Items.Add(new ListItem("SC - Columbia", "29201"));
			ddlControl.Items.Add(new ListItem("SC - Greer", "29650"));
			ddlControl.Items.Add(new ListItem("SD - Sioux Falls", "57101"));
			ddlControl.Items.Add(new ListItem("TN - Memphis", "37501"));
			ddlControl.Items.Add(new ListItem("TN - Nashville", "37201"));
			ddlControl.Items.Add(new ListItem("TX - Austin", "78701"));
			ddlControl.Items.Add(new ListItem("TX - Brownsville", "78520"));
			ddlControl.Items.Add(new ListItem("TX - Corpus Christi", "78401"));
			ddlControl.Items.Add(new ListItem("TX - Dallas", "75201"));
			ddlControl.Items.Add(new ListItem("TX - El Paso", "79901"));
			ddlControl.Items.Add(new ListItem("TX - Fort Worth", "76101"));
			ddlControl.Items.Add(new ListItem("TX - Houston", "77001"));
			ddlControl.Items.Add(new ListItem("TX - Laport Texas", "77571"));
			ddlControl.Items.Add(new ListItem("TX - Laredo", "78040"));
			ddlControl.Items.Add(new ListItem("TX - San Antonio", "78201"));
			ddlControl.Items.Add(new ListItem("UT - Salt Lake City", "84101"));
			ddlControl.Items.Add(new ListItem("VA - Norfolk", "23501"));
			ddlControl.Items.Add(new ListItem("VA - Virginia Beach", "23450"));
			ddlControl.Items.Add(new ListItem("VT - Burlington", "05402"));
			ddlControl.Items.Add(new ListItem("WA - Tacoma ", "98401"));
			ddlControl.Items.Add(new ListItem("WA - Seattle", "98101"));
			ddlControl.Items.Add(new ListItem("WI - Madison", "53701"));
			ddlControl.Items.Add(new ListItem("WI - Milwaukee", "53201"));
			ddlControl.Items.Add(new ListItem("WI - Wausau", "54401"));
			ddlControl.Items.Add(new ListItem("WV - Charleston", "25301"));
			ddlControl.Items.Add(new ListItem("WY - Cheyenne", "82001"));
		}
	}
}