<%@ Page Language="C#" MasterPageFile="~/MasterPages/Menu.Master" AutoEventWireup="true" CodeBehind="CompanyType.aspx.cs" Inherits="localhost.Hauler.CompanyType" Title="Change Company Type" %>


<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>





<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" runat="server">
<br />
Please select the company that you want to update from the drop down list below.
<br />
<asp:DropDownList runat="server" ID="ddlCompany"></asp:DropDownList>&nbsp;&nbsp;
<asp:button runat="server" ID="btnFind" OnClick="OnClick_btnFind" Text="Find"/>
<br />
<br />

<asp:Panel ID="pnlCompany" runat="server" visible="false">
    <asp:Label runat="server" ID="lblCompany"></asp:Label>&nbsp;&nbsp;
    <asp:Label runat="server" ID="lblCompID" visible="false"></asp:Label>
    <asp:DropDownList runat="server" ID="ddlType" >
    <asp:ListItem Text="Buyer" Value="P"></asp:ListItem>
    <asp:ListItem Text="Seller" Value="S"></asp:ListItem>
    <asp:ListItem Text="Distributor" Value="D"></asp:ListItem>
    </asp:DropDownList>&nbsp;&nbsp;
    <asp:Button runat="server" ID="btnUpdate" OnClick="OnClick_btnUpdate" Text="Update"/>
    <br />
    <br />
    <asp:Label runat="server" ID="lblResult" ForeColor="red" ></asp:Label>&nbsp;&nbsp;
</asp:Panel>

</asp:Content>
