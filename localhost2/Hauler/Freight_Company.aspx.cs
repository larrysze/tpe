using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Mail;
using System.Text;
using MetaBuilders.WebControls;
using System.Text.RegularExpressions;

namespace localhost.Hauler
{
	/// <summary>
	/// Summary description for Freight_Company.
	/// </summary>
	public partial class Freight_Company : System.Web.UI.Page
	{
        
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            Master.Width = "1200px";
            if (!IsPostBack)
			{
				Bind();
			}

			
		}


		protected void Bind()
		{
			
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			SqlCommand cmdship;
			SqlDataReader dtrship;


			//Get the order ID from previous Filled Order page
			string[] arrID;
			Regex r = new Regex("-"); // Split on dashes.
			arrID = r.Split(Request.QueryString["ID"].ToString());
			
			cmdship= new SqlCommand("SELECT SHIPMENT_SIZE,SHIPMENT_ID FROM SHIPMENT WHERE SHIPMENT_ORDR_ID='" + arrID[0].ToString()+"' AND SHIPMENT_SKU='" + arrID[1].ToString()+"'", conn);
			dtrship= cmdship.ExecuteReader();
			if (dtrship.Read() )
			{
				ViewState["ID"] = dtrship["SHIPMENT_ID"].ToString();
				string search = "";
				string searchUnContacted ="";
				int foundSize = 0;
				
				if(dtrship["SHIPMENT_SIZE"].ToString()== "42000" )
				{
					search = "WHERE BOXES='1' AND EMAIL <>'' ";
					searchUnContacted = "WHERE BOXES='1' AND (EMAIL ='' OR EMAIL IS NULL)";
					foundSize = 1;
					ViewState["Size"] = "truckload boxes";
				}
				else if(dtrship["SHIPMENT_SIZE"].ToString()== "45000" )
				{
					search = "WHERE BULKS='1' AND EMAIL <>''";
					searchUnContacted = "WHERE BULKS='1' AND (EMAIL ='' OR EMAIL IS NULL)";
					foundSize = 1;
					ViewState["Size"] = "bulk truck";
				}
				else if(dtrship["SHIPMENT_SIZE"].ToString()== "44092" )
				{
					search = "WHERE BAGS='1' AND EMAIL <>''";
					searchUnContacted = "WHERE BAGS='1' AND (EMAIL ='' OR EMAIL IS NULL)";
					foundSize = 1;
					ViewState["Size"] = "truckload bags";
				}
			
				ViewState["searchUnContacted"]= searchUnContacted;
				ViewState["search"]= search;

				
				// bind the contacted company's list
				SqlConnection conn3 = new SqlConnection(Application["DBConn"].ToString());
				conn3.Open();
				SqlDataAdapter dadContent = new SqlDataAdapter("SELECT FREE_COMP_ID,CONTACT_NAME,EMAIL,COMP_NAME,PHONE,FAX,ADRESSE,CITY,STATE,ZIP FROM FREIGHT_COMPANY "+search,conn3);
				DataSet dstContent = new DataSet();
				dadContent.Fill(dstContent);
				contacted.DataSource = dstContent;
				contacted.DataBind();
				conn3.Close();
				// bind the uncontacted company's list
				if(foundSize == 0)
				{
					pnSizeNotFound.Visible=true;
					lblNick.Text = Session["Nick"].ToString();
					pnUnContacted.Visible=false;
					pnContacted.Visible=false;
				}
				SqlConnection conn4 = new SqlConnection(Application["DBConn"].ToString());
				conn4.Open();
				SqlDataAdapter dadContent4 = new SqlDataAdapter("SELECT CONTACT_NAME,EMAIL,COMP_NAME,PHONE,FAX,ADRESSE,CITY,STATE,ZIP FROM FREIGHT_COMPANY "+searchUnContacted,conn4);
				DataSet dstContent4 = new DataSet();
				dadContent4.Fill(dstContent4);
				uncontacted.DataSource = dstContent4;
				uncontacted.DataBind();
				conn4.Close();
			}	  
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnSendMail_Click(object sender, System.EventArgs e)
		{



			RowSelectorColumn rsc;
			rsc = RowSelectorColumn.FindColumn(contacted);
			string strFreightId="";
			for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++)
			{
				int selIndex = rsc.SelectedIndexes[selectedIndex];
				strFreightId += "'"+contacted.DataKeys[selIndex].ToString()+"',";
			}
			strFreightId = strFreightId.Substring(0,strFreightId.Length - 1);
		
			SqlConnection conn2;
			conn2 = new SqlConnection(Application["DBConn"].ToString());
			conn2.Open();
			SqlCommand cmdCompanies;
			SqlDataReader dtrCompanies;
			cmdCompanies= new SqlCommand("SELECT CONTACT_NAME,EMAIL,COMP_NAME FROM FREIGHT_COMPANY "+ViewState["search"].ToString()+" AND FREE_COMP_ID in ("+strFreightId+")", conn2);
			dtrCompanies= cmdCompanies.ExecuteReader();
			while(dtrCompanies.Read() )
			{
				Send_mail(dtrCompanies["CONTACT_NAME"].ToString(),dtrCompanies["EMAIL"].ToString());
			}
			//set panels invisible
			pnSizeNotFound.Visible=false;
			pnContacted.Visible=false;
			pnUnContacted.Visible=false;
			pnEmailSent.Visible=true;
			conn2.Close();
		}

		private int Send_mail(string name_to,string email_to)
		{	
			
			SqlConnection conn;
			conn = new SqlConnection(Application["DBConn"].ToString());
			conn.Open();
			StringBuilder sbSQL = new StringBuilder();

			

			sbSQL.Append("	Select ");
			sbSQL.Append("		SHIPMENT_ORDR_ID,");
			sbSQL.Append("		VARDELIVERY=(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=SHIPMENT_TO)), ");
			sbSQL.Append("		ORIGINADDRESS = (Select PLAC_ADDR_ONE +' '+PLAC_ADDR_TWO  FROM PLACE WHERE PLAC_ID = SHIPMENT_FROM),");
			sbSQL.Append("		DELIVERYADDRESS = (Select PLAC_ADDR_ONE +' '+PLAC_ADDR_TWO  FROM PLACE WHERE PLAC_ID = SHIPMENT_TO),");
			sbSQL.Append("		VARORIGIN= (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=SHIPMENT_FROM)), ");
			sbSQL.Append("		ORIGINADDRESS = (Select PLAC_ADDR_ONE +' '+PLAC_ADDR_TWO  FROM PLACE WHERE PLAC_ID = SHIPMENT_FROM),		");
			sbSQL.Append("		ORIGINZIP = (Select PLAC_ZIP FROM PLACE WHERE PLAC_ID = SHIPMENT_FROM), ");
			sbSQL.Append("		DELIVERYZIP = (Select PLAC_ZIP FROM PLACE WHERE PLAC_ID = SHIPMENT_TO) ");
			sbSQL.Append("	FROM ");
			sbSQL.Append("		SHIPMENT ");
			sbSQL.Append("	WHERE ");
			sbSQL.Append("		SHIPMENT_ID ='"+ViewState["ID"].ToString()+"' ");


			SqlCommand cmdMail;
			SqlDataReader dtrMail;
			cmdMail = new SqlCommand(sbSQL.ToString(),conn);
			dtrMail = cmdMail.ExecuteReader();
			dtrMail.Read();
			


			try 
			{				
				string strsubject = "RFQ from " + dtrMail["VARORIGIN"].ToString() + " to " + dtrMail["VARDELIVERY"].ToString() ;
				MailMessage mail1;
				mail1 = new MailMessage();
                mail1.From = "<" + Session["Email"].ToString() + ">" + Session["Name"].ToString();

                //mail1.To = "wanderley@theplasticsexchange.com";
				mail1.To= email_to;
                mail1.Subject = strsubject;
				mail1.Body =  "<HTML><head><title>The Plastics Exchange</title><link type='text/css' rel='stylesheet' href='http://wifi.tiscali.fr/css/ie.css'></head><body topmargin='5' leftmargin='5' marginheight='5' marginwidth='5'>"           
					+ "Hello " + name_to + ",<br><br>"
					+ "Can you quote the cheapest way possible for this "+ViewState["Size"].ToString()+".<br><br>" 
					+ "<B>Pick up at:</B><br>"
					+ dtrMail["ORIGINADDRESS"].ToString()+ "<br>"
					+ dtrMail["VARORIGIN"].ToString() + " " + dtrMail["ORIGINZIP"].ToString()
					+ "<br><br>"
					+ "<b>Deliver at:</b><br>"
					+ dtrMail["DELIVERYADDRESS"].ToString() + "<br>"
					+ dtrMail["VARDELIVERY"].ToString()+ " " + dtrMail["DELIVERYZIP"].ToString()
					+ "<br><br>"
					+ "Please let me know when can you move this load, I need one load ASAP.<br><br>"
					+ "Thank you very much.<br><br>"
					+ "Sincerely,<br>"
					+ Session["Name"].ToString() + "<br>"
					+ "<a href='http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "' >The Plastics Exchange</a> <br>"
                    + "Tel: " + ConfigurationSettings.AppSettings["PhoneNumber"].ToString() + "<br><br><br>"
					+ "</body>" 
					+ "</html>";
                mail1.BodyFormat = MailFormat.Html;
                //SmtpMail.SmtpServer = "localhost";
				TPE.Utility.EmailLibrary.Send(Context,mail1);






				return(1);
			} 

			catch(Exception exMail) 
			{ 
				Response.Write("ERROR SEND MAIL "+ exMail.Message);
				return(-1);
			}        
		}
		





	}
}
