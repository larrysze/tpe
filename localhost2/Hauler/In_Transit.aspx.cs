using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Mail;
using System.Text.RegularExpressions;

namespace localhost.Hauler
{

	public partial class In_Transit : System.Web.UI.Page
	{

	/************************************************************************
	*   1. File Name       :Hauler\In_Transit.aspx                          *
	*   2. Description     :                                                *
	*   3. Modification Log:                                                *
	*     Ver No.       Date          Author             Modification       *
	*   -----------------------------------------------------------------   *
	*      1.00      2-26-2004      Zach                Comment             *
	*                                                                       *
	************************************************************************/

		
		string strOrderBy;
		string strSQL;

		ArrayList aux;
		ArrayList ArrayUpdate;
        
		protected System.Web.UI.WebControls.DataListItem dli;
		protected System.Web.UI.WebControls.Label lblSort;
		protected System.Web.UI.WebControls.DropDownList ddlFilter;
		protected System.Web.UI.WebControls.DropDownList ddlStatusDL;
		protected System.Web.UI.WebControls.LinkButton LinkButton1;

		
		public void Page_Load(object sender, EventArgs e)
		{
            Master.Width = "1200px";
            
			if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C") && ((string)Session["Typ"] != "X"))
			{
				Response.Redirect("/default.aspx");
			}
            
			strSQL =""; 


			// Load sample data only once, when the page is first loaded.
			if (!IsPostBack)
			{	
				ArrayUpdate= new ArrayList();
				//				ViewState["search"] ="";
				aux=new ArrayList();
				Bind("ID DESC");
			}
		}

		//  These functions get call by the Sorting button at the top of the column.
		//  There is probably a more elegant way to do it w/ a switch statement but I didn't
		//  set it up like that.
		
		// Binds the content data.  This is always called after strOrderBy has been determined
		

		public void Bind(string strSort)
		{
			btnShowAll.Visible = false;

			bool limit = false;
			bool bSearch = false;

			string strSearch = txtSearch.Text;
			if(strSearch.Trim().Length >= 2)
			{
				bSearch = true;
				btnShowAll.Visible = true;
			}
			else
			{
				txtSearch.Text = "";
			}


			if( ((ddlStatus.SelectedValue.ToString() == "Delivered") || (ddlStatus.SelectedValue.ToString() == "All")) && !bSearch)
			{
				limit = true;
			}
			
			if(limit)
			{
				strSQL = " SELECT TOP 50 *,";
				lblTitle.Visible = true;

			}
			else
			{
				strSQL = " SELECT *,";
				lblTitle.Visible = false;
			}


			strSQL+="	          CAST(SHIPMENT_ORDR_ID AS VARCHAR)+'-'+CAST(SHIPMENT_SKU AS VARCHAR) AS ID,	";
			strSQL += "			   VARSHIPORDRID = SHIPMENT_ORDR_ID	 ,";
			strSQL += "			   VARSHIPSKU = SHIPMENT_SKU	 ,";
			strSQL += "			   VARSHIPID = SHIPMENT_ID  ,";
			//strSQL+=" 	      CAST( CAST ( CAST (SHIPMENT_ID AS VARCHAR)+ CAST(SHIPMENT_SKU AS VARCHAR))AS INTEGER )AS IDORDER, ";
			strSQL +="             VARWEIGHT=SHIPMENT_WEIGHT, ";
            strSQL +="             VARESTWEIGHT=ISNULL(SHIPMENT_WEIGHT,SHIPMENT_SIZE*SHIPMENT_QTY), ";
			strSQL+="              VARDELIVERY=(SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=SHIPMENT_TO)), ";
			strSQL+="              VARORIGIN= (SELECT LOCL_CITY+', '+LOCL_STAT FROM LOCALITY WHERE LOCL_ID=(SELECT PLAC_LOCL FROM PLACE WHERE PLAC_ID=SHIPMENT_FROM)) , ";
//			strSQL+="              SHIPMENT_SIZE,";
			strSQL+="              VARCOMMENT=SHIPMENT_COMMENT,";
			strSQL+="              VARDATETAKEN=CONVERT(VARCHAR,SHIPMENT_DATE_TAKE,101), ";
			strSQL+="              VARDATEDELIVERED=CONVERT(VARCHAR,SHIPMENT_DATE_DELIVERED,101), ";
			strSQL+="              VARSTATUS=SHIPMENT_SHIP_STATUS,";
			strSQL+="              VARBUYR  =(SELECT LEFT(COMP_NAME,12)+'...' FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)) ,";
			strSQL+="              VARSELR =(SELECT LEFT(COMP_NAME,12)+'...' FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR))";
			strSQL+="FROM Shipment WHERE SHIPMENT_ORDR_ID is not null AND SHIPMENT_SIZE<>190000 ";
            
    
			switch(ddlStatus.SelectedItem.Value)
				{ 
					case "Enroute": strSQL=strSQL+"and SHIPMENT_SHIP_STATUS=2";
							break;
					case "Delivered"://redirect de page to delivered.aspx or show results???????
							strSQL=strSQL+" and SHIPMENT_SHIP_STATUS=3";
							break;
					case "Inventory":strSQL=strSQL+" and SHIPMENT_SHIP_STATUS=1";
							break;
				}
			  
				switch(ddlSizes.SelectedItem.Value)
				{
					case "RailCars": strSQL=strSQL+" and SHIPMENT_SIZE=190000 ";
							break;
					case "Truckloads":strSQL=strSQL+" and SHIPMENT_SIZE>=42000 and SHIPMENT_SIZE<=45000 ";
							break;
					case "Other":strSQL=strSQL+" and SHIPMENT_SIZE<>42000 and SHIPMENT_SIZE<>45000 and SHIPMENT_SIZE<>190000 ";
							break;
				}

			if(bSearch)
			{

				strSQL = "SELECT * FROM( " + strSQL;
				strSQL += ") AS T";
                strSQL += " WHERE SHIPMENT_LOT_NUMBER LIKE '%" + strSearch + "%' OR SHIPMENT_RELEASE_NUMBER LIKE '%" + strSearch + "%' OR SHIPMENT_PO_NUM LIKE '%" + strSearch + "%' OR isnull(T.VARBUYR, '')+'_'+isnull(T.VARSELR, '')+'_'+isnull(T.VARDELIVERY, '')+'_'+isnull(T.VARORIGIN, '')+'_'+CAST(T.ID AS VARCHAR)+ '_' + isnull(VARCOMMENT, '') LIKE '%" + strSearch + "%'";
                if (Session["Typ"].ToString() == "X")
                {
                    string ID = Session["Id"].ToString();
                    int IntID = 0;
                    if(int.TryParse(ID, out IntID))
                    {
                        strSQL += " AND SHIPMENT_BROKER_ID = " + ID;
                    }
                        
                }
                strSQL += " ORDER BY T.ID DESC";
			}
			else
			{
                if (Session["Typ"].ToString() == "X")
                {
                    string ID = Session["Id"].ToString();
                    int IntID = 0;
                    if (int.TryParse(ID, out IntID))
                    {
                        strSQL += " AND SHIPMENT_BROKER_ID = " + ID;
                    }

                }
                strSQL += " ORDER BY " + strSort;
			}

			SqlDataAdapter dadContent;
			DataSet dstContent;
			using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
			{
				dadContent = new SqlDataAdapter(strSQL ,conn);
				dstContent = new DataSet();
				dadContent.Fill(dstContent);
			
				dl.DataSource = dstContent;
				dl.DataBind();
			}
		}

		protected void bUpdate_Click (object sender, EventArgs e)
		{
			//Response.Write("entering bUpdate_Click");
//			TextBox textComment2, txtWeight;
			//txtComment = (TextBox)E.Item.FindControl("TxtComment");
			//textComment2 = (TextBox)E.Item.FindControl("TxtComment2");
			//txtWeight = (TextBox)E.Item.FindControl("TextWeight");
			
			String query;
			String auxquery="";
			String strID="";
			int iCounter=0;
			String date="";
			String date2="";
			String []split=null;
			string delimiterStr="/";
			char[] delimiter=delimiterStr.ToCharArray();
			
			
			foreach ( DataListItem myDataListItem in dl.Items)
			{
				strID = dl.DataKeys[iCounter].ToString();

				string Id0,Id1;
				string[] tempId = new string[2];
				char[] splitter  = {'-'};  				
				tempId = strID.Split(splitter);
				Id0 = tempId[0];
				Id1 = tempId[1];
							    
				date=((TextBox)myDataListItem.FindControl("TxtDateTaken")).Text;

				if (date.Equals("") && ((DropDownList)myDataListItem.FindControl("ddlStatusDL2")).SelectedValue=="2")date=DateTime.Today.ToShortDateString();
				date2=((TextBox)myDataListItem.FindControl("TxtDateDelivered")).Text;		
			
			
				if (date2.Equals("") && ((DropDownList)myDataListItem.FindControl("ddlStatusDL2")).SelectedValue=="3")date2=DateTime.Today.ToShortDateString();
				query = "UPDATE Shipment SET SHIPMENT_SHIP_STATUS="+((DropDownList)myDataListItem.FindControl("ddlStatusDL2")).SelectedValue;


                if (!((TextBox)myDataListItem.FindControl("TxtWeight")).Text.Equals(""))
                    query = query + " ,SHIPMENT_WEIGHT='" + ((TextBox)myDataListItem.FindControl("TxtWeight")).Text + "' ";     //,SHIPMENT_COMMENT='"+((TextBox)myDataListItem.FindControl("TxtComment")).Text+"' "; 

                else
                    query = query + " ,SHIPMENT_WEIGHT=null";       // ,SHIPMENT_COMMENT='" + ((TextBox)myDataListItem.FindControl("TxtComment")).Text + "' ";

                if (!((TextBox)myDataListItem.FindControl("TxtComment")).Text.Equals(""))
                    query = query + " , SHIPMENT_COMMENT='" + ((TextBox)myDataListItem.FindControl("TxtComment")).Text + "' ";
                else
                    query = query + " , SHIPMENT_COMMENT=NULL";
			
				if (!date.Equals(""))
					query=query+" ,SHIPMENT_DATE_TAKE='"+date+"' ";
				if (!date2.Equals(""))
					query=query+" ,SHIPMENT_DATE_DELIVERED='"+date2+"' ";

				query=query+" WHERE (SHIPMENT_ORDR_ID = '"+Id0+"') AND (SHIPMENT_SKU = '"+Id1+"')";

			
				TPE.Utility.DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(),query);				
				iCounter++;
			}		
			Bind("ID ASC");
			
			Form_Verif();
		}


		protected void Form_Verif()
		{	
			//Response.Write("entering Form_Verif");
			int i=0;
			int SumWeightRC, SumWeightBT;
			int countBT = 0;
			int countRC = 0;
			int nbrecord;
			int nbblank = 0;
			double diff;
			char[] splitter = {'-'};
			string strid="";
			string Id0,Id1;
			string[] tempId = new string[2];
			string bodyEmail = "";
			

			//Check each result row of the DataList
			foreach ( DataListItem myDataListItem in dl.Items)
			{
				strid = dl.DataKeys[i].ToString();

				//Check if SHIPMENT_ORDR_ID-SHIPMENT_SKU contain "RC" for Rail Car
				if ( strid.IndexOf("RC") > 0 )
				{	
					SumWeightRC = 0;
					SumWeightBT = 0;
					countBT = 0;
					countRC = 0;
					nbrecord = 0;		
					nbblank = 0;
					
					//Get the SHIPMENT_ORDR_ID of the selected row 				
					tempId = strid.Split(splitter);
					Id0 = tempId[0];	//SHIPMENT_ORDR_ID
					Id1 = tempId[1];	//SHIPMENT_SKU

					SqlConnection conn;
					conn = new SqlConnection(Application["DBConn"].ToString());
					conn.Open();
	
					//Select all shipment with the corresponding SHIPMENT_ORDR_ID
					SqlCommand cmdCheckBT;
					SqlDataReader dtrCheckBT;
					cmdCheckBT= new SqlCommand("SELECT * FROM SHIPMENT WHERE SHIPMENT_ORDR_ID = '"+Id0+"'", conn);
					dtrCheckBT= cmdCheckBT.ExecuteReader(); 
					
					while (dtrCheckBT.Read())
					{
						nbrecord++;
						//Check if the SHIPMENT_SKU selected contains "RC" and save in "SumWeightRC" the SHIPMENT_WEIGHT linked to this row
						if ( (dtrCheckBT["SHIPMENT_SKU"].ToString()).StartsWith("RC") )
						{
							if ( dtrCheckBT["SHIPMENT_WEIGHT"] is DBNull )
							{
								SumWeightRC=0;	
								nbblank++;
							}
							else	
								SumWeightRC += Convert.ToInt32(dtrCheckBT["SHIPMENT_WEIGHT"].ToString());
							countRC++;
						}
						else //case where SHIPMENT_SKU contain "BT", sum in SumWeightBT the SHIPMENT_WEIGHT of all rows with the SHIPMENT_ORDR_ID selected and without "RC" in their SHIPMENT_SKU 
						{
							if ( dtrCheckBT["SHIPMENT_WEIGHT"] is DBNull )
							{nbblank++;}
							else
								SumWeightBT += Convert.ToInt32(dtrCheckBT["SHIPMENT_WEIGHT"].ToString());
							countBT++;
						}
					}
					
					//verify if the value of the RT SHIPMENT_WEIGHT field equal the BT SHIPMENT_WEIGHT ones
					if ( SumWeightRC != SumWeightBT && nbblank == 0)
					{
						//Response.Write("pb, need to send mail<br><br>");
						diff = SumWeightRC-SumWeightBT;
						bodyEmail = Body_mail(Id0,diff) + bodyEmail;
					}					
					conn.Close();
				}			
				i++;
			}
			if (bodyEmail.Trim()!="") Send_mail(bodyEmail);
			//Response.Write("RC found: "+countRC+"<br>");
			//Response.Write("BT found for this RC: "+countBT+"<br><br>");
		}

		protected string Body_mail(string SHIP_ORDR_ID, double diff)
		{	
			// message body definition
			return "<body topmargin='5' leftmargin='5' marginheight='5' marginwidth='5'>"
				+ "Problem: Not equal weights  -  Difference = "+ diff +", Order Number: " + SHIP_ORDR_ID 
				+ "</body>" 
				+ "<BR>";
		}

		protected void Send_mail(string bodyEmail)
		{	
			try 
			{				
				string strsubject = "Not equal weights Report";

				MailMessage maildef=new MailMessage();
				maildef.From = "<noresponse@theplasticsexchange.com>TPE: PB of Weights";
				maildef.To= "helio@theplasticsexchange.com";
				maildef.Subject = strsubject;

				// message body definition
				maildef.Body = "<HTML><head><title>The Plastics Exchange</title><link type='text/css' rel='stylesheet' href='http://wifi.tiscali.fr/css/ie.css'></head>" + bodyEmail + "</HTML>";

				maildef.BodyFormat = MailFormat.Html;
				TPE.Utility.EmailLibrary.Send(Context,maildef);
			} 

			catch(Exception exMail) 
			{ 
				Response.Write("ERROR SEND MAIL "+ exMail.Message);
			}        
		}
	
		protected void dl_ItemDataBound(object sender, DataListItemEventArgs e) 
		{
			
			DropDownList ddl;
			//DropDownList ddl2;
			TextBox railcar;
			ImageButton link;
			
			if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
				try 
				{
					ddl = (DropDownList) e.Item.FindControl("ddlStatusDL2");
					
					if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "SHIPMENT_SHIP_STATUS")) ==1)
					{
						ddl.Items.Add(new ListItem("INV","1"));
						ddl.Items.Add(new ListItem("EnR","2"));
						ddl.Items.Add(new ListItem("DEL","3"));
					}
					else if (Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "SHIPMENT_SHIP_STATUS")) ==2)
					{	
						ddl.Items.Add(new ListItem("EnR","2"));
						ddl.Items.Add(new ListItem("INV","1"));
						ddl.Items.Add(new ListItem("DEL","3"));
					}
					else
					{
						ddl.Items.Add(new ListItem("DEL","3"));
						//ddl.Items.Add(new ListItem("Inventory","1"));
						ddl.Items.Add(new ListItem("EnR","2"));
					}
						
			        railcar=(TextBox) e.Item.FindControl("TxtComment");
					link=(ImageButton) e.Item.FindControl("ImageRailCar");
					if (!railcar.Text.Equals("")) 
					{
							link.Visible=true;
					}

					ddl.DataBind();
				}
					
				catch(Exception except) 
				{
					Trace.Write("", "Ack!", except);
				}             							
		}
		

		public void Change_Status(object sender, EventArgs e)
		{
				Bind(" ID ASC ");
		}

		public void Change_Sizes(object sender, EventArgs e)
		{	
			  Bind(" ID ASC ");
		}

		public void ShowAll(object sender, EventArgs e)
		{
			ViewState["search"]="";
			Bind(lblSort.Text);
		}

		//Response.Redirect( "https://www.steelroads.com/servlet/SAServlet?CONTROLLER=ETScriptedAccessCtlr&ScriptedAccessType=Dynamic&ACTION=&LANGUAGE=en&ImportList&EquipmentList="+ );
					
		
		public void BuyerSort_Click(object sender, EventArgs e)
		{   
			if (SortBuyr.Value.Equals("DESC"))
			{
				Bind(" VARBUYR ASC ");
				SortBuyr.Value="ASC";
			}
			else 
			{
					Bind(" VARBUYR DESC " );
					SortBuyr.Value="DESC";
			}
		}

		public void SellerSort_Click(object sender, EventArgs e)
		{
			if (SortSellr.Value.Equals("DESC"))
			{
				Bind(" VARSELR ASC ");
				SortSellr.Value="ASC";
			}
			else 
			{
				Bind(" VARSELR DESC ");
				SortSellr.Value="DESC";
			}
		}

		public void TradeSort_Click(object sender, EventArgs e)
		{
			if (SortTrade.Value.Equals("DESC"))
			 {
				Bind(" ID ASC ");
				SortTrade.Value="ASC";
			 }
			else 
			{
				Bind(" ID DESC ");
				SortTrade.Value="DESC";
			}
		}

        public void EstWeightSort_Click(object sender, EventArgs e)
        {
            if (SortWeight.Value.Equals("DESC"))
            {
                Bind(" VARESTWEIGHT ASC ");
                SortWeight.Value = "ASC";
            }
            else
            {
                Bind(" VARESTWEIGHT DESC ");
                SortWeight.Value = "DESC";
            }
        }

		public void DeliveryPointSort_Click(object sender, EventArgs e)
		{
			if (SortDelivery.Value.Equals("DESC"))
			{
				Bind(" VARDELIVERY ASC ");
				SortDelivery.Value="ASC";
			}
			else 
			{
				Bind(" VARDELIVERY DESC ");
				SortDelivery.Value="DESC";
			}
		}

		public void OriginPointSort_Click(object sender, EventArgs e)
		{
			if (SortOrigin.Value.Equals("DESC"))
			{
				Bind(" VARORIGIN ASC ");
				SortOrigin.Value="ASC";
			}
			else 
			{
				Bind(" VARORIGIN DESC ");
				SortOrigin.Value="DESC";
			}
		}

		public void StatusSort_Click(object sender, EventArgs e)
		{
			if (SortStatus.Value.Equals("DESC"))
			{
				Bind(" VARSTATUS ASC ");
				SortStatus.Value="ASC";
			}
			else 
			{
				Bind(" VARSTATUS DESC ");
				SortStatus.Value="DESC";
			}
		}

		public void WeightSort_Click(object sender, EventArgs e)
		{
			if (SortWeight.Value.Equals("DESC"))
			{
				Bind(" VARWEIGHT ASC ");
				SortWeight.Value="ASC";
			}
			else 
			{
				Bind(" VARWEIGHT DESC ");
				SortWeight.Value="DESC";
			}
		}

		public void DateTakenSort_Click(object sender, EventArgs e)
		{
			if (SortDateTaken.Value.Equals("DESC"))
			{
				Bind(" VARDATETAKEN ASC ");
				SortDateTaken.Value="ASC";
			}
			else 
			{
				Bind(" VARDATETAKEN DESC ");
				SortDateTaken.Value="DESC";
			}
		}

		public void DateDeliveredSort_Click(object sender, EventArgs e)
		{
			if (SortDateDelivered.Value.Equals("DESC"))
			{
				Bind(" VARDATEDELIVERED ASC ");
				SortDateDelivered.Value="ASC";
			}
			else 
			{
				Bind(" VARDATEDELIVERED DESC ");
				SortDateDelivered.Value="DESC";
			}
		}

		public void CommentSort_Click(object sender, EventArgs e)
		{
			if (SortComment.Value.Equals("DESC"))
			{
				Bind(" VARCOMMENT ASC ");
				SortComment.Value="ASC";
			}
			else 
			{
				Bind(" VARCOMMENT DESC ");
				SortComment.Value="DESC";
			}
		}

	
		#region Cdigo generado por el Diseador de Web Forms
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
	
		private void InitializeComponent()
		{

		}
		#endregion

		protected void btnShowAll_Click(object sender, System.EventArgs e)
		{
			txtSearch.Text = "";
			Bind(" ID DESC ");
		}

		protected void btnSearch_Click( object sender, System.EventArgs e )
		{
			Bind(" ID DESC ");

		}

		protected void ddlStatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Bind(" ID DESC ");
		
		}

		protected void ddlSizes_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Bind(" ID DESC ");
		
		}

	}
}
