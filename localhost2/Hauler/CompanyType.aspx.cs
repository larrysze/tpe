using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Hauler
{
    public partial class CompanyType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C"))
            {
                Response.Redirect("../default.aspx");
            }
            if (!IsPostBack)
            {
                HelperFunction.fillCompany(ddlCompany, this.Context);
            }
            
        }

        protected void OnClick_btnFind(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(Application["DBConn"].ToString());
            try
            {
                conn.Open();
                string strSQL = "SELECT * from COMPANY WHERE Comp_id = '" + ddlCompany.SelectedValue.ToString() + "'";
                using (SqlDataReader dtrTrans = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
                {
                    if (dtrTrans.Read())
                    {
                        lblCompany.Text = dtrTrans["comp_name"].ToString();
                        lblCompID.Text = dtrTrans["comp_ID"].ToString();
                        ddlType.SelectedValue = dtrTrans["comp_type"].ToString();
                        pnlCompany.Visible = true;                        
                    }
                }                
            }
            catch
            {
 
            }
        }

        protected void OnClick_btnUpdate(object sender, EventArgs e)
        {
            string SQL = "";
            SQL += "Update company ";
            SQL += " Set comp_type ='" + ddlType.SelectedValue.ToString() + "' ";
            SQL += " Where comp_id='" + lblCompID.Text + "' ";
            DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), SQL);

            lblResult.Text = lblCompany.Text+"'s Company Type has changed to " + ddlType.SelectedItem.Text;
        }

    }
}
