<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WeightCompare.aspx.cs" Inherits="localhost.Hauler.WeightCompare" MasterPageFile="~/MasterPages/Menu.Master" Title="Weight Compare" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphJavaScript" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphCssLink" runat="server">
</asp:Content>



<asp:Content ID="Content3" ContentPlaceHolderID="cphMain" runat="server">

<br/>
&nbsp;<asp:Label ID="lblDate" Text="Date:" runat="server" CssClass="Content Color2"></asp:Label>
<asp:dropdownlist id="ddlMonth" runat="server" CssClass="InputForm" onselectedindexchanged="ddlMonth_SelectedIndexChanged" EnableViewState="True" AutoPostBack="True"></asp:dropdownlist>
&nbsp;&nbsp;<asp:Label ID="lblWarehouse" Text="Warehouse:" runat="server" CssClass="Content Color2"></asp:Label>
<asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="InputForm" onselectedindexchanged="ddlWarehouse_SelectedIndexChanged" EnableViewState="True" AutoPostBack="True"></asp:DropDownList>
<br />
<br />
<asp:Panel ID="pnlWeight" runat="server" CssClass="Content Color2">
</asp:Panel>
<br />


</asp:Content>
