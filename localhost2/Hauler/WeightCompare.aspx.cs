using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using TPE.Utility;

namespace localhost.Hauler
{
    public partial class WeightCompare : System.Web.UI.Page
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.Width = "1500px";
            if (((string)Session["Typ"] != "A") && ((string)Session["Typ"] != "C") && ((string)Session["Typ"] != "L"))
            {
                Response.Redirect("../default.aspx");
            }
            if (!IsPostBack)
            {                
                CreateDateList();
                CreateTable();
            }
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            CreateTable();
        }

        protected void ddlWarehouse_SelectedIndexChanged(object sender, System.EventArgs e)
        {            
            CreateTable();
        }

        protected void CreateDateList()
        {
            ListItem lstItem = new ListItem("Show All Dates", "*", true);
            ddlMonth.Items.Add(lstItem);
            HelperFunction.fillDatesDesc(1, 2003, ddlMonth);
            ddlMonth.SelectedItem.Selected = false;
            ddlMonth.Items[1].Selected = true;

            HelperFunction.fillWarehouse(ddlWarehouse, this.Context);

        }
        protected void CreateTable()
        {            
            string[] orders = new string[200];
            int i = 0;

            string strWeight = "";
            strWeight += "<table cellSpacing=0 cellPadding=0 width='100%' align=center border=1>";
            strWeight += "<tr bgcolor='#FAB303'><td align=center><b>RC Order ID</b></td>";            
            strWeight += "<td align=center><b>Buyer</b></td>";
            strWeight += "<td align=center><b>Seller</b></td>";
            strWeight += "<td align=center><b>Warehouse</b></td>";
            strWeight += "<td align=center><b>RC#</b></td>";
            strWeight += "<td align=center><b>Buy Price</b></td>";
            strWeight += "<td align=center><b>RC Weight(lbs)</b></td>";
            strWeight += "<td align=center><b>BT/TL Order ID</b></td>";
            strWeight += "<td align=center><b>Buyer</b></td>";
            strWeight += "<td align=center><b>Seller</b></td>";
            strWeight += "<td align=center><b>Warehouse</b></td>";
            strWeight += "<td align=center><b>RC#</b></td>";
            strWeight += "<td align=center><b>Buy Price</b></td>";            
            strWeight += "<td align=center><b>BT/TL Weight(lbs)</b></td>";
            strWeight += "<td align=center><b>Diff</b></td></tr>";
            strWeight += "<tr><td colspan=15 bgcolor='#FAB303'>&nbsp;</td></tr>";

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();
                string strSQL = "select shipment_ordr_id from (select shipment_ordr_id,count(shipment_ordr_id) as totalofshipment  from shipment where (shipment_sku='RC1' or shipment_sku like 'TL%' or shipment_sku like 'BT%') ";
                
                if (ddlMonth.SelectedValue.ToString() != "*")
                    strSQL += "and year(shipment_transaction_date) = '" + ddlMonth.SelectedValue.ToString().Substring(0, 4) + "' and month(shipment_transaction_date) = '" + ddlMonth.SelectedValue.ToString().Substring(4, 2) + "' ";
                if (ddlWarehouse.SelectedValue.ToString() != "*")
                    strSQL += "and (SELECT PLAC_LABL FROM PLACE WHERE PLAC_ID=SHIPMENT_FROM) LIKE '%"+ddlWarehouse.SelectedValue.ToString().Trim()+"%' ";
                
                strSQL += "group by shipment_ordr_id ) as Q1 where  totalofshipment > 1 order by shipment_ordr_id desc";
                using (SqlDataReader dtrOrder = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
                {
                    while (dtrOrder.Read())
                    {
                        orders[i] = dtrOrder["shipment_ordr_id"].ToString();
                        i++;
                    }
                }
            }

            string OrderNumber = "";
            string Weight = "";
            string Buyer = "";
            string Seller = "";
            string Warehouse = "";
            string RCnum = "";
            string BuyPrice = "";            
            int totalRCWeight = 0;
            int totalBTTLWeight = 0;
            int j = 0;
            while (orders[j] != null)
            {
                using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
                {
                    conn.Open();
                    string strSQL = "select shipment_date_take,shipment_ordr_id,shipment_sku,VARBUYR=(SELECT LEFT(COMP_NAME,12)+'...' FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_BUYR)),VARSELR=(SELECT LEFT(COMP_NAME,12)+'...' FROM COMPANY WHERE COMP_ID=(SELECT PERS_COMP FROM PERSON WHERE PERS_ID=SHIPMENT_SELR)),VARWAREHOUSE=(SELECT PLAC_LABL FROM PLACE WHERE PLAC_ID=SHIPMENT_FROM),shipment_comment, VARBUYPRICE = LEFT(SHIPMENT_BUYR_PRCE,6),VARWEIGHT=ISNULL(shipment_weight,shipment_size*shipment_qty) from shipment where shipment_ordr_id =" + orders[j].ToString() + " ";
                    using (SqlDataReader dtrWeight = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
                    {
                        while (dtrWeight.Read())
                        {
                            if (dtrWeight["shipment_sku"].ToString() == "RC1")
                            {
                                if (j > 0)
                                {
                                    int diff = totalRCWeight - totalBTTLWeight;
                                    strWeight += "<tr><td colspan=12> </td>";
                                    strWeight += "<td align=center><b>Total Weight</b></td><td align=center><b>" + totalBTTLWeight + "</b></td>";
                                    if (diff>0) strWeight += "<td align=center><b><font color= red>" + diff + "</font></b></td></tr>";
                                    else strWeight += "<td align=center><b>" + diff + "</b></td></tr>";

                                    strWeight += "<tr><td colspan=15 bgcolor='#FAB303'>&nbsp;</td></tr>";
                                }
                                totalRCWeight = 0;
                                totalBTTLWeight = 0;
                                OrderNumber = dtrWeight["shipment_ordr_id"].ToString() + "-" + dtrWeight["shipment_sku"].ToString();
                                Weight = dtrWeight["VARWEIGHT"].ToString();
                                Buyer = dtrWeight["VARBUYR"].ToString();
                                Seller = dtrWeight["VARSELR"].ToString();
                                Warehouse = dtrWeight["VARWAREHOUSE"].ToString();
                                RCnum = dtrWeight["Shipment_comment"].ToString();
                                BuyPrice = dtrWeight["VARBUYPRICE"].ToString();
                                totalRCWeight = Convert.ToInt32(Weight);
                                continue;
                            }
                            strWeight += "<tr><td align=center><a href=\"/administrator/Transaction_Details.aspx?Market=Spot&Referer=/Common/Filled_Orders.aspx&ID=" + OrderNumber + "\"><b>" + OrderNumber + "</b></a></td>";
                            strWeight += "<td align=center>" + Buyer + "</td>";
                            strWeight += "<td align=center>" + Seller + "</td>";
                            strWeight += "<td align=center>" + Warehouse + "</td>";
                            strWeight += "<td align=center>" + RCnum + "</td>";
                            strWeight += "<td align=center>" + BuyPrice + "</td>";
                            strWeight += "<td align=center><b>" + Weight + "</b></td>";

                            OrderNumber = "";
                            Weight = "";
                            Buyer = "";
                            Seller = "";
                            Warehouse = "";
                            RCnum = "";
                            BuyPrice = "";

                            string color = "";
                            if (dtrWeight["VARBUYR"].ToString().StartsWith("The Plastic"))
                            {
                                if (dtrWeight["shipment_date_take"] is DBNull)
                                {
                                    color = "Green";
                                }
                                if (dtrWeight["shipment_date_take"].ToString() != "")
                                {
                                    color = "Blue";
                                }
                            }                            
                            else 
                                color = "Black";

                            strWeight += "<td align=center><a href=\"/administrator/Transaction_Details.aspx?Market=Spot&Referer=/Common/Filled_Orders.aspx&ID=" + dtrWeight["shipment_ordr_id"].ToString() + "-" + dtrWeight["shipment_sku"].ToString() + "\"><b>" + dtrWeight["shipment_ordr_id"].ToString() + "-" + dtrWeight["shipment_sku"].ToString() + "</b></a></td>";
                            strWeight += "<td align=center>" + dtrWeight["VARBUYR"].ToString() + "</td>";
                            strWeight += "<td align=center>" + dtrWeight["VARSELR"].ToString() + "</td>";
                            strWeight += "<td align=center>" + dtrWeight["VARWAREHOUSE"].ToString() + "</td>";
                            strWeight += "<td align=center>" + dtrWeight["Shipment_comment"].ToString() + "</td>";
                            strWeight += "<td align=center>" + dtrWeight["VARBUYPRICE"].ToString() + "</td>";
                            strWeight += "<td align=center><font color= "+color+">" + dtrWeight["VARWEIGHT"].ToString() + "</font></td><td align=center></td></tr>";
                            totalBTTLWeight = totalBTTLWeight + Convert.ToInt32(dtrWeight["VARWEIGHT"].ToString());
                        }
                    }
                }

                //strWeight += "<tr><td><b>Total Weight</b></td><td><b>" + totalRCWeight + " lbs</b></td><td><b>Total Weight</b></td><td><b>" + totalBTTLWeight + " lbs</b></td></tr>";                
                //strWeight += "</tr>";
                j++;
            }
            int diff2 = totalRCWeight - totalBTTLWeight;
            strWeight += "<tr><td colspan=12> </td>";            
            strWeight += "<td align=center><b>Total Weight</b></td><td align=center><b>" + totalBTTLWeight + "</b></td>";
            if (diff2 > 0) strWeight += "<td align=center><b><font color= red>" + diff2 + "</font></b></td></tr>";
            else strWeight += "<td align=center><b>" + diff2 + "</b></td></tr>";            
            strWeight += "</table>";

            pnlWeight.Controls.Add(new LiteralControl(strWeight));
        }
    }
}
