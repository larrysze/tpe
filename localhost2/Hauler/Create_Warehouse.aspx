<%@ Page Language="C#" %>
<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Data.SqlClient" %>
<script runat="server">

    /************************************************************************
    *   1. File Name       :Hauler\Create_Warehouse.aspx                    *
    *   2. Description     :Create new warehouse, attach location to a warehouse*
    *                       Once warehouse is created or attached, we have to*
    *                       update all table related to location ID, change old ID*
    *                       to the ID of the warehouse                      *
    *                                                                       *
    *                                                                       *
    *   3. Modification Log:                                                *
    *     Ver No.       Date          Author             Modification       *
    *   -----------------------------------------------------------------   *
    *      1.00      2-26-2004        Xiaoda              Comment           *
    *                                                                       *
    ************************************************************************/

     public void Page_Load(object sender, EventArgs e){
        txtLabel.MaxLength = 10;
      if (!IsPostBack){
         Bind("company ASC");
       }
    }

     public void SortDG(Object sender, DataGridSortCommandEventArgs e ){

          string[] SortExprs;
          string CurrentSearchMode;
          string NewSearchMode;
          string ColumnToSort;
          string NewSortExpr;

          //  Parse the sort expression - delimiter space
          Regex r = new Regex(" "); // Split on spaces.
          SortExprs = r.Split(e.SortExpression) ;
          ColumnToSort = SortExprs[0];
            // If a sort order is specified get it, else default is descending
          if (SortExprs.Length > 1){
              CurrentSearchMode = SortExprs[1].ToUpper();
              if (CurrentSearchMode == "ASC"){
                 NewSearchMode = "DESC";
              }else{
                 NewSearchMode = "ASC";
              }
          }else{   // If no mode specified, Default is descending
              NewSearchMode = "DeSC";
          }

          //  Derive the new sort expression.
          NewSortExpr = ColumnToSort + " " + NewSearchMode;

          // Figure out the column index
          int iIndex;
          iIndex = 1;
          switch(ColumnToSort.ToUpper()){
            case "COMPANY":
              iIndex = 1;
              break;
            case "PLAC_PERS":
              iIndex = 2;
              break;
            case "PLAC_PHON":
              iIndex = 3;
              break;
            case "PLAC_ADDR_ONE":
              iIndex = 4;
              break;
            case "PLAC_ZIP":
              iIndex = 5;
              break;
            case "PLAC_TYPE":
              iIndex = 6;
              break;
            case "PLAC_INST":
              iIndex = 7;
              break;
           case "City":
              iIndex = 8;
              break;
           case "State":
              iIndex = 9;
              break;
           case "country":
              iIndex = 10;
              break;

          }
          // alter the column's sort expression
          dg.Columns[iIndex].SortExpression = NewSortExpr;

          // reset dbase page to the first one whether the sorting changes
          dg.CurrentPageIndex = 0;

          // store the new SortExpr in a labl for use by page function
          //lblSort.Text =NewSortExpr;
          // Sort the data in new order


          Bind(NewSortExpr);
        }

          public void Bind(string SQLOrder){

                 SqlConnection conn;
                 conn = new SqlConnection(Application["DBconn"].ToString());
                 conn.Open();
                 string strSQL;
                 strSQL ="";

                 //Display all the location from PLACE table except headquarters and warehouse
                 //Reason: We don't want to destroy current warehouse, shouldn't change H.Q
                 strSQL="select company=(select COMP_NAME from Company where comp_ID=Plac_comp), *, city=(Select locl_city from locality where LOCL_ID=PLAC_LOCL), state=(Select locl_STAT from locality where LOCL_ID=PLAC_LOCL), country=(Select locl_ctry from locality where LOCL_ID=PLAC_LOCL) from place WHERE PLAC_TYPE<>'W' AND PLAC_TYPE<>'H' ORDER BY "+ SQLOrder;
                 SqlDataAdapter dadContent;
                 DataSet dstContent;
                 dadContent = new SqlDataAdapter(strSQL ,conn);
                 dstContent = new DataSet();
                 dadContent.Fill(dstContent);
                 dg.DataSource = dstContent;
                 dg.DataBind();

                 //Display all existing warehouse
                 SqlCommand cmdList;
                 cmdList= new SqlCommand("SELECT city=(Select locl_city from locality where LOCL_ID=PLAC_LOCL),state=(Select locl_STAT from locality where LOCL_ID=PLAC_LOCL), country=(Select locl_ctry from locality where LOCL_ID=PLAC_LOCL), PLAC_ID, PLAC_INST, PLAC_LABL FROM PLACE WHERE PLAC_TYPE='W'", conn);
                 SqlDataReader dtrList;
                 dtrList=cmdList.ExecuteReader();

                //build the warehouse label, city, state and country into one string
                 string warehouse;
                 warehouse="";
                 string id;
                 ddlList.Items.Clear();
                 while (dtrList.Read()){
                 id=dtrList["PLAC_ID"].ToString();
                 //warehouse=dtrList["PLAC_LABL"].ToString();
                 //warehouse=warehouse+",";
                 warehouse=dtrList["CITY"].ToString();
                 warehouse=warehouse+",";
                 warehouse=warehouse+dtrList["STATE"].ToString();
                 warehouse=warehouse+",";
                 //warehouse=warehouse+dtrList["COUNTRY"].ToString();
                 warehouse=warehouse+dtrList["PLAC_LABL"].ToString();
                 //add all warehouses to drop-down list
                 ddlList.Items.Add(new ListItem (warehouse,id));
                  }
                 dtrList.Close();
                 conn.Close();
           }

          public void Add(object sender, EventArgs e){

            //we have to update all table related to location ID, change old ID
            //to the ID of the new warehouse just attached!
            SqlConnection connadd;
            connadd = new SqlConnection(Application["DBconn"].ToString());
            connadd.Open();
            SqlCommand cmdAdd;
            RowSelectorColumn rsc;
            rsc = RowSelectorColumn.FindColumn(dg);
            for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++)
              {

             int selIndex = rsc.SelectedIndexes[selectedIndex];
             cmdAdd= new SqlCommand("UPDATE SHIPMENT SET SHIPMENT_FROM="+ddlList.SelectedItem.Value+" WHERE SHIPMENT_FROM="+dg.DataKeys[selIndex].ToString()+"", connadd);
             cmdAdd.ExecuteNonQuery();
             cmdAdd= new SqlCommand("UPDATE SHIPMENT SET SHIPMENT_TO="+ddlList.SelectedItem.Value+" WHERE SHIPMENT_TO="+dg.DataKeys[selIndex].ToString()+"", connadd);
             cmdAdd.ExecuteNonQuery();
             cmdAdd= new SqlCommand("UPDATE BBID SET BID_TO="+ddlList.SelectedItem.Value+" WHERE BID_TO="+dg.DataKeys[selIndex].ToString()+"", connadd);
             cmdAdd.ExecuteNonQuery();
             cmdAdd= new SqlCommand("UPDATE BBIDARCH SET BID_TO="+ddlList.SelectedItem.Value+" WHERE BID_TO="+dg.DataKeys[selIndex].ToString()+"", connadd);
             cmdAdd.ExecuteNonQuery();
             cmdAdd= new SqlCommand("UPDATE BBOFFER SET OFFR_FROM="+ddlList.SelectedItem.Value+" WHERE OFFR_FROM="+dg.DataKeys[selIndex].ToString()+"", connadd);
             cmdAdd.ExecuteNonQuery();
             cmdAdd= new SqlCommand("UPDATE BBOFFERARCH SET OFFR_FROM="+ddlList.SelectedItem.Value+" WHERE OFFR_FROM="+dg.DataKeys[selIndex].ToString()+"", connadd);
             cmdAdd.ExecuteNonQuery();
             cmdAdd= new SqlCommand("UPDATE OFFER SET OFFR_FROM="+ddlList.SelectedItem.Value+" WHERE OFFR_FROM="+dg.DataKeys[selIndex].ToString()+"", connadd);
             cmdAdd.ExecuteNonQuery();
             cmdAdd= new SqlCommand("UPDATE BID SET BID_TO="+ddlList.SelectedItem.Value+" WHERE BID_TO="+dg.DataKeys[selIndex].ToString()+"", connadd);
             cmdAdd.ExecuteNonQuery();

             //delete the location which won't be used again!
             cmdAdd= new SqlCommand("DELETE FROM PLACE WHERE PLAC_ID="+dg.DataKeys[selIndex].ToString()+"", connadd);
             cmdAdd.ExecuteNonQuery();

                }

                 connadd.Close();
                 Server.Transfer("Create_Warehouse.aspx");
          }




          public void Enter(object sender, EventArgs e){

             //Create the new warehouse in PLACE table, PLAC_TYPE='W': Warehouse
             SqlConnection conninsert;
             conninsert = new SqlConnection(Application["DBconn"].ToString());
             conninsert.Open();
             SqlCommand cmdInsert;
             cmdInsert= new SqlCommand("INSERT INTO PLACE(PLAC_LOCL, PLAC_PERS, PLAC_PHON, PLAC_ADDR_ONE, PLAC_ADDR_TWO, PLAC_ZIP, PLAC_TYPE, PLAC_ENBL, PLAC_INST, PLAC_RAIL_NUM, PLAC_LABL)  VALUES("+lbltest.Text+",'"+txtContact.Text+"','"+txtPhone.Text+"','"+txtAddr.Text+"','"+txtAddr2.Text+"','"+txtZip.Text+"','W', 1, '"+txtWName.Text+"','"+txtRail.Text+"','"+txtLabel.Text+"' )", conninsert);
             cmdInsert.ExecuteNonQuery();

             //Find new Plac_ID for the warehouse we just inserted into PLACE
             cmdInsert= new SqlCommand("SELECT TOP 1 PLAC_ID FROM PLACE order by plac_ID desc", conninsert);
             SqlDataReader dtrPlace;
             dtrPlace=cmdInsert.ExecuteReader();
             dtrPlace.Read();
             string PlacId=dtrPlace["PLAC_ID"].ToString();
             dtrPlace.Close();

             //update the SHIPMENT table base on new place id
             RowSelectorColumn rsc;
             rsc = RowSelectorColumn.FindColumn(dg);
             for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++){
             int selIndex = rsc.SelectedIndexes[selectedIndex];
             cmdInsert= new SqlCommand("UPDATE SHIPMENT SET SHIPMENT_FROM="+PlacId+" WHERE SHIPMENT_FROM="+dg.DataKeys[selIndex].ToString()+"", conninsert);
             cmdInsert.ExecuteNonQuery();
             cmdInsert= new SqlCommand("UPDATE SHIPMENT SET SHIPMENT_TO="+PlacId+" WHERE SHIPMENT_TO="+dg.DataKeys[selIndex].ToString()+"", conninsert);
             cmdInsert.ExecuteNonQuery();


             //update the BBID table base on new place id
             cmdInsert= new SqlCommand("UPDATE BBID SET BID_TO="+PlacId+" WHERE BID_TO="+dg.DataKeys[selIndex].ToString()+"", conninsert);
             cmdInsert.ExecuteNonQuery();

             //update the BBIDARCH table base on new place id
            cmdInsert= new SqlCommand("UPDATE BBIDARCH SET BID_TO="+PlacId+" WHERE BID_TO="+dg.DataKeys[selIndex].ToString()+"", conninsert);
             cmdInsert.ExecuteNonQuery();

             //update the BBOFFER table base on new place id
            cmdInsert= new SqlCommand("UPDATE BBOFFER SET OFFR_FROM="+PlacId+" WHERE OFFR_FROM="+dg.DataKeys[selIndex].ToString()+"", conninsert);
             cmdInsert.ExecuteNonQuery();

             //update the BBOFFERARCH table base on new place id
            cmdInsert= new SqlCommand("UPDATE BBOFFERARCH SET OFFR_FROM="+PlacId+" WHERE OFFR_FROM="+dg.DataKeys[selIndex].ToString()+"", conninsert);
             cmdInsert.ExecuteNonQuery();

             //update the OFFER table base on new place id
            cmdInsert= new SqlCommand("UPDATE OFFER SET OFFR_FROM="+PlacId+" WHERE OFFR_FROM="+dg.DataKeys[selIndex].ToString()+"", conninsert);
             cmdInsert.ExecuteNonQuery();

             //update the BID table base on new place id
            cmdInsert= new SqlCommand("UPDATE BID SET BID_TO="+PlacId+" WHERE BID_TO="+dg.DataKeys[selIndex].ToString()+"", conninsert);
             cmdInsert.ExecuteNonQuery();

             //Delete the record from place table.
             cmdInsert= new SqlCommand("DELETE FROM PLACE WHERE PLAC_ID="+dg.DataKeys[selIndex].ToString()+"", conninsert);
             cmdInsert.ExecuteNonQuery();
                }
                 conninsert.Close();
                 Server.Transfer("Create_Warehouse.aspx");
          }

          public void Create(object sender, EventArgs e){
            //Display all location info(from checkbox)

            //Hide main panel, show the panel for create new warehouse
            pnldefault.Visible = false;
            pnlcreate.Visible = true;
            RowSelectorColumn rsc;

            rsc = RowSelectorColumn.FindColumn(dg);
            SqlConnection connread;
            connread = new SqlConnection(Application["DBconn"].ToString());
            connread.Open();
            SqlDataReader dtrread;
            SqlCommand cmdread;
            int count=1;
               for (int selectedIndex=0; selectedIndex< rsc.SelectedIndexes.Length;selectedIndex++){
                   if (IsPostBack){
                   int selIndex = rsc.SelectedIndexes[selectedIndex];
                   cmdread= new SqlCommand("SELECT *, city=(Select locl_city from locality where LOCL_ID=PLAC_LOCL), state=(Select locl_STAT from locality where LOCL_ID=PLAC_LOCL), country=(Select locl_ctry from locality where LOCL_ID=PLAC_LOCL) FROM PLACE WHERE PLAC_ID="+dg.DataKeys[selIndex].ToString(), connread);
                   dtrread= cmdread.ExecuteReader();
                   dtrread.Read();
                   lbltest.Text=dtrread["PLAC_LOCL"].ToString();
               if(count<=1) {

                  if(dtrread["PLAC_PERS"].ToString()!="") {
                  txtContact.Text=dtrread["PLAC_PERS"].ToString();
                  }
                  if(dtrread["PLAC_PHON"].ToString()!="") {
                  txtPhone.Text=dtrread["PLAC_PHON"].ToString();

                  }
                  if(dtrread["PLAC_ADDR_ONE"].ToString()!="") {

                    txtAddr.Text=dtrread["PLAC_ADDR_ONE"].ToString();
                  }
                  if(dtrread["PLAC_ADDR_TWO"].ToString()!="") {
                  txtAddr2.Text=dtrread["PLAC_ADDR_TWO"].ToString();
                  }
                  if(dtrread["PLAC_ZIP"].ToString()!="") {

                    txtZip.Text=dtrread["PLAC_ZIP"].ToString();
                  }
                  if(dtrread["PLAC_RAIL_NUM"].ToString()!="") {

                  txtRail.Text=dtrread["PLAC_RAIL_NUM"].ToString();
                  }
                  if(dtrread["PLAC_INST"].ToString()!="") {

                    txtWName.Text=dtrread["PLAC_INST"].ToString();
                  }
                  if(dtrread["PLAC_INST"].ToString()!="") {

                  txtLabel.Text=dtrread["PLAC_LABL"].ToString();
                  }

                  if(dtrread["CITY"].ToString()!="") {
                  lblCity1.Text=dtrread["CITY"].ToString();
                  }
                  if(dtrread["STATE"].ToString()!="") {


                  lblState1.Text=dtrread["STATE"].ToString();

                  }
                  if(dtrread["COUNTRY"].ToString()!="") {


                  lblCountry1.Text=dtrread["COUNTRY"].ToString();
                  }

                lblContact.Text="";
                lblPhone.Text="";
                lblAddr.Text="";
                lblAddr2.Text="";
                lblZip.Text="";
                lblWName.Text="";
                lblLabel.Text="";
                lblCity2.Text="";
                lblState2.Text="";
                lblCountry2.Text="";
                lblRail.Text="";
             }

             else {


                if (txtRail.Text != dtrread["PLAC_RAIL_NUM"].ToString()){
                lblRail.Text +=dtrread["PLAC_RAIL_NUM"].ToString()+"<b>....</b>";
                }
                if (txtContact.Text != dtrread["PLAC_PERS"].ToString()){
                lblContact.Text +=dtrread["PLAC_PERS"].ToString()+"<b>....</b>";
                }
                if (txtPhone.Text != dtrread["PLAC_PHON"].ToString()){
                lblPhone.Text +=dtrread["PLAC_PHON"].ToString()+"<b>....</b>";
                }
                if (txtAddr.Text != dtrread["PLAC_ADDR_ONE"].ToString()){
                lblAddr.Text +=dtrread["PLAC_ADDR_ONE"].ToString()+"<b>....</b>";
                }
                if (txtAddr2.Text != dtrread["PLAC_ADDR_TWO"].ToString()){
                lblAddr2.Text +=dtrread["PLAC_ADDR_TWO"].ToString()+"<b>....</b>";
                }
                if (txtZip.Text != dtrread["PLAC_ZIP"].ToString()){
                lblZip.Text +=dtrread["PLAC_ZIP"].ToString()+"<b>....</b>";
                }
                if (txtWName.Text != dtrread["PLAC_INST"].ToString()){
                lblWName.Text +=dtrread["PLAC_INST"].ToString()+"<b>....</b>";

                }
                if (txtLabel.Text != dtrread["PLAC_LABL"].ToString()){
                lblLabel.Text +=dtrread["PLAC_LABL"].ToString()+"<b>....</b>";

                }
                if (lblCity1.Text != dtrread["PLAC_PERS"].ToString()){
                lblCity2.Text +=dtrread["CITY"].ToString()+"<b>....</b>";
                }
                if (lblState1.Text != dtrread["PLAC_PERS"].ToString()){
                lblState2.Text +=dtrread["STATE"].ToString()+"<b>....</b>";
                }
                if (lblCountry1.Text != dtrread["PLAC_PERS"].ToString()){
                lblCountry2.Text +=dtrread["COUNTRY"].ToString()+"<b>....</b>";
                }
               }//close else
                dtrread.Close();
                count=count+1;
                }//postback
            }//close for loop




         }

</script>
<form runat="server" id="Form1">
    <TPE:Template PageTitle="warehouse" Runat="Server" />


<asp:Panel id="pnldefault" runat="server" visible="true">
          <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
            <tr>
                <td align="center"><h3>Warehouse</h3></td>
            </tr>
            <tr><td align=center>

            <asp:Button runat="server" id="btncreate" onclick="Create" class="tpebutton" Text="Create a New WareHouse" />
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            <asp:DropDownList id="ddlList" runat="server" /><asp:Label id="lbltest2" Visible="true" runat="server" />
            <asp:Button runat="server" id="btnadd" onclick="Add" class="tpebutton" Text="Add To This WareHouse" />
                 </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="95%">
                        <tr valign=bottom>
                            <td align=left>
                            </td>

                            <td align="right">



                            </td>
                        </tr>
                     </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid runat="server" id="dg"
                                    CssClass = "DataGrid"
                                    ItemStyle-CssClass="DataGridRow"
                                    AlternatingItemStyle-CssClass = "DataGridRow_Alternate"
                                    HeaderStyle-CssClass="DataGridHeader"
                                    Width="85%"
                                    HorizontalAlign="Center"
                                    CellPadding="2"
                                    DataKeyField="PLAC_ID"
                                    ShowFooter="True"
                                    onSortCommand="SortDG"
                                    AllowSorting="true"
                                    AutoGenerateColumns="false">

                    <Columns>
		               <mbrsc:RowSelectorColumn SelectionMode="Multiple" AllowSelectAll="true" />
		               <asp:BoundColumn DataField="company" HeaderText="Company" SortExpression="COMPANY DESC"/>
		               <asp:BoundColumn DataField="PLAC_PERS" HeaderText="Person " SortExpression="PLAC_PERS  DESC"/>
		               <asp:BoundColumn DataField="PLAC_PHON" HeaderText="Phone" SortExpression="PLAC_PHON  DESC"/>
		               <asp:BoundColumn DataField="PLAC_ADDR_ONE" HeaderText="Address" SortExpression="PLAC_ADDR_ONE  DESC"/>
		               <asp:BoundColumn DataField="PLAC_ZIP" HeaderText="Zip" SortExpression="PLAC_ZIP  DESC"/>
		               <asp:BoundColumn DataField="PLAC_TYPE" HeaderText="Type" SortExpression="PLAC_TYPE  DESC"/>
		               <asp:BoundColumn DataField="PLAC_INST" HeaderText="Place Name" SortExpression="PLAC_INST  DESC"/>
		               <asp:BoundColumn DataField="City" HeaderText="City" SortExpression="city  DESC"/>
		               <asp:BoundColumn DataField="State" HeaderText="State" SortExpression="state  DESC"/>
		               <asp:BoundColumn DataField="Country" HeaderText="County" SortExpression="country  DESC"/>
		               <asp:BoundColumn DataField="PLAC_ID" HeaderText="ID" />
		            </Columns>
                    </asp:datagrid>
                 </td>
              </tr>

          </table>
          </asp:Panel>


           <asp:Panel id="pnlcreate" runat="server" visible="false">
          <table border=0>
          <tr><td colspan=2></td><td>Other Values</td></tr>


          <tr><td>Contact:</td>
	      <td><asp:TextBox id="txtContact" size="15" maxsize="40" runat="server" /></td>
	      <td><asp:Label id="lblContact" Visible="true" runat="server" /> </td>
          </tr>
          <tr><td>Phone:</td>
	      <td><asp:TextBox id="txtPhone" size="15" maxsize="40" runat="server" /></td>
	      <td><asp:Label id="lblPhone" Visible="true" runat="server" /> </td>
          </tr>
          <tr><td>Address:</td>
	      <td><asp:TextBox id="txtAddr" size="25" maxsize="40" runat="server" /></td>
	      <td><asp:Label id="lblAddr" Visible="true" runat="server" /> </td>
          </tr>
          <tr><td></td>
	      <td><asp:TextBox id="txtAddr2" size="25" maxsize="40" runat="server" /></td>
	      <td><asp:Label id="lblAddr2" Visible="true" runat="server" /> </td>
          </tr>
          <tr><td>Zip:</td>
	      <td><asp:TextBox id="txtZip" size="10" maxsize="10" runat="server" /></td>
	      <td><asp:Label id="lblZip" Visible="true" runat="server" /> </td>
          </tr>
          <tr><td>Warehouse Name:</td>
              <asp:RequiredFieldValidator runat="server" ControlToValidate="txtWName" Display="none" ErrorMessage="Need a warehouse name!" />
	      <td><asp:TextBox id="txtWName" size="35" maxsize="40" runat="server" textmode=multiline row=2 /></td>
	      <td width=250><asp:Label id="lblWName" Visible="true" runat="server" Wrapping="Wrap" /> <asp:ValidationSummary runat="server" HeaderText=" Please correct the error in red." /></td>
          </tr>
          <tr><td>Warehouse Label:</td>
	      <asp:RequiredFieldValidator runat="server" ControlToValidate="txtLabel" Display="none" ErrorMessage="Need a warehouse label!" />
	      <td><asp:TextBox id="txtLabel" size="10" maxsize="10" runat="server" /></td>
	      <td width=250><asp:Label id="lblLabel" Visible="true" runat="server" /> </td>
          </tr>
          <tr><td>Rail #:</td>
	      <td><asp:TextBox id="txtRail" size="10" maxsize="10" runat="server" /></td>
	      <td><asp:Label id="lblRail" Visible="true" runat="server" /> </td>
          </tr>
          <tr><td>City:</td>
	      <td><asp:Label id="lblCity1" Visible="true" runat="server" /></td>
	      <td><asp:Label id="lblCity2" Visible="true" runat="server" /> </td>
          </tr>
          <tr><td>State:</td>
	      <td><asp:Label id="lblState1" Visible="true" runat="server" /></td>
	      <td><asp:Label id="lblState2" Visible="true" runat="server" /> </td>
          </tr>
          <tr><td>Country:</td>
	      <td><asp:Label id="lblCountry1" Visible="true" runat="server" /></td>
	      <td><asp:Label id="lblCountry2" Visible="true" runat="server" /> </td>
          </tr>

          </table>
          <p align=center>
          <asp:Label id="lbltest" Visible="false" runat="server" />

         <asp:Button runat="server" id="btnenter" onclick="Enter" class="tpebutton" Text="Enter" />
          </p>
           </asp:Panel>
<TPE:Template Footer="true" Runat="Server" />

</form>
