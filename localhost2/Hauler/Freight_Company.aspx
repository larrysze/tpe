<%@ Register TagPrefix="mbrsc" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.RowSelectorColumn" %>

<%@ Page language="c#" Codebehind="Freight_Company.aspx.cs" AutoEventWireup="True" Inherits="localhost.Hauler.Freight_Company" MasterPageFile="~/MasterPages/Menu.Master" Title="Freight Company" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">

			<asp:panel ID="pnContacted" Runat="server" visible="True">
				<div class="DivTitleBarMenu"><span class="Header Color1 Bold">Companies capable of shipping this load:</span></div>
				<asp:DataGrid BackColor="#000000" CellSpacing="1" BorderWidth="0" id="contacted" runat="server" Width="100%" HorizontalAlign="Center" CellPadding="2"
					DataKeyField="FREE_COMP_ID" AutoGenerateColumns="false" ShowFooter="False" HeaderStyle-CssClass="LinkNormal OrangeColor"
					AlternatingItemStyle-CssClass="LinkNormal LightGray" ItemStyle-CssClass="LinkNormal DarkGray" CssClass="DataGrid" FooterStyle-CssClass="Content Bold Color4 FooterColor">
					<Columns>
						<mbrsc:RowSelectorColumn SelectionMode="Multiple" AllowSelectAll="true" />
						<asp:BoundColumn DataField="COMP_NAME" HeaderText="Company Name" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="CONTACT_NAME" HeaderText="Contact Name" ItemStyle-HorizontalAlign="Center" />
						<asp:BoundColumn DataField="EMAIL" HeaderText="Email" />
						<asp:BoundColumn DataField="PHONE" HeaderText="Phone" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="FAX" HeaderText="Fax" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="ADRESSE" HeaderText="Address" />
						<asp:BoundColumn DataField="CITY" HeaderText="City" />
						<asp:BoundColumn DataField="STATE" HeaderText="State" />
						<asp:BoundColumn DataField="ZIP" HeaderText="Zip" />
					</Columns>
				</asp:DataGrid><br />
				<asp:Button CssClass="Content Color2" id="btnSendMail" runat="server" Text="Send Emails to request quote" onclick="btnSendMail_Click"></asp:Button>
			</asp:panel>
		
			<asp:panel ID="pnSizeNotFound" Runat="server" visible="False" CssClass="Content">
				<CENTER><BR><B>Sorry
						<asp:Label id="lblNick" runat="server">Helio</asp:Label>, you can't send quotes 
						for this shipment size.</B></CENTER>
			</asp:panel>
			<br>
			<asp:panel ID="pnUnContacted" Runat="server" visible="True">
				<div class="DivTitleBarMenu"><span class="Header Color1 Bold">Companies unselected because of an empty Email:</span></div>
				<asp:DataGrid BackColor="#000000" CellSpacing="1" BorderWidth="0" id="uncontacted" runat="server" Width="100%" HorizontalAlign="Center" CellPadding="2"
					DataKeyField="COMP_NAME" AutoGenerateColumns="false" ShowFooter="False" HeaderStyle-CssClass="LinkNormal OrangeColor"
					AlternatingItemStyle-CssClass="LinkNormal LightGray" ItemStyle-CssClass="LinkNormal DarkGray" CssClass="DataGrid" FooterStyle-CssClass="Content Bold Color4 FooterColor">
					<Columns>
						<asp:BoundColumn DataField="COMP_NAME" HeaderText="Company Name" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="CONTACT_NAME" HeaderText="Contact Name" ItemStyle-HorizontalAlign="Center" />
						<asp:BoundColumn DataField="EMAIL" HeaderText="Email" />
						<asp:BoundColumn DataField="PHONE" HeaderText="Phone" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="FAX" HeaderText="Fax" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="ADRESSE" HeaderText="Address" />
						<asp:BoundColumn DataField="CITY" HeaderText="City" />
						<asp:BoundColumn DataField="STATE" HeaderText="State" />
						<asp:BoundColumn DataField="ZIP" HeaderText="Zip" />
					</Columns>
				</asp:DataGrid>
			</asp:panel>
			<asp:panel ID="pnEmailSent" Runat="server" visible="False" CssClass="Content Color2">
				<CENTER><B>Emails have been sent succesfully</B></CENTER>
			</asp:panel>

</asp:Content>