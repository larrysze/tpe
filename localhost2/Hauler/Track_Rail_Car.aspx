<%@ Page Language="c#" CodeBehind="Track_Rail_Car.aspx.cs" AutoEventWireup="True" Inherits="localhost.Hauler.Track_Rail_Car" MasterPageFile="~/MasterPages/Menu.Master" MaintainScrollPositionOnPostback="false" %>
<%@ MasterType VirtualPath="~/MasterPages/Menu.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" tagPrefix="asp" namespace="System.Web.UI" %>


<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphMain">

<style type="text/css">
    .progressAjaxPleaseWait
    {
        display: block;
        position: absolute;
        padding: 2px 3px;
    }
    .containerAjaxPleaseWait
    {
        border: solid 1px #808080;
        border-width: 1px 0px;
    }
    .headerAjaxPleaseWait
    {
        background: url(http://www.theplasticexchange.com/images/sprite.png) repeat-x 0px 0px;
        border-color: #808080 #808080 #ccc;
        border-style: solid;
        border-width: 0px 1px 1px;
        padding: 0px 10px;
        color: #000000;
        font-size: 9pt;
        font-weight: bold;
        line-height: 1.9;  
        font-family: arial,helvetica,clean,sans-serif;
    }
    .bodyAjaxPleaseWait
    {
        background-color: #f2f2f2;
        border-color: #808080;
        border-style: solid;
        border-width: 0px 1px;
        padding: 10px;
    }
</style>
        <asp:ScriptManager id="ScriptManager1" runat="Server" />
    
           <script type="text/javascript">
                function onUpdating()
                {
                    // get the update progress div
                    var pnlPopup = $get('<%= this.pnlPopup.ClientID %>'); 

                    //  get the gridview element        
                    var gridView = $get('<%= this.dg.ClientID %>');
                    
                    // make it visible
                    pnlPopup.style.display = '';	    
                    
                    // get the bounds of both the gridview and the progress div
                    var gridViewBounds = Sys.UI.DomElement.getBounds(gridView);
                    var pnlPopupBounds = Sys.UI.DomElement.getBounds(pnlPopup);
                    
                    //  center of gridview
                    var x = gridViewBounds.x + Math.round(gridViewBounds.width / 2) - Math.round(pnlPopupBounds.width / 2);
                    var y = gridViewBounds.y + Math.round(gridViewBounds.height / 2) - Math.round(pnlPopupBounds.height / 2);	    


                    //	set the progress element to this position
                    Sys.UI.DomElement.setLocation(pnlPopup, x, y);           
                }

                function onUpdated() 
                {
                    // get the update progress div
                    var pnlPopup = $get('<%= this.pnlPopup.ClientID %>'); 
                    // make it invisible
                    pnlPopup.style.display = 'none';
                }            
                
               
                 
                
        </script>
    


<table cellspacing="0" cellpadding="0">
      <tr>
	        <td>
	     <cc1:UpdatePanelAnimationExtender runat="server" TargetControlID="UpdatePanel1">
                <Animations>
                    <OnUpdating>
                        <Parallel duration="0">
                            <%-- place the update progress div over the gridview control --%>
                            <ScriptAction Script="onUpdating();" />  
                            <FadeOut minimumOpacity=".5" />
                         </Parallel>
                    </OnUpdating>
                    <OnUpdated>
                        <Parallel duration="0">
                         <FadeIn minimumOpacity=".5" />
                            <%--find the update progress div and place it over the gridview control--%>
                            <ScriptAction Script="onUpdated();" /> 
                        </Parallel> 
                    </OnUpdated>
                </Animations>
            </cc1:UpdatePanelAnimationExtender>            
            
            <asp:Panel ID="pnlPopup" runat="server" CssClass="progressAjaxPleaseWait" style="display:none;">
                <div class="containerAjaxPleaseWait">
                    <div class="headerAjaxPleaseWait">Loading...</div>
                    <div class="bodyAjaxPleaseWait">
                        <img src="http://www.theplasticexchange.com/images/activity.gif" />
                    </div>
                </div>
            </asp:Panel> 

	   
	        </td>
	        </tr>
	        </table>

     <asp:UpdatePanel id="UpdatePanel1" runat="Server">
 <ContentTemplate>


<div class="DivTitleBarMenu"><span class="Header Color1 Bold">Inventory</span></div>
	<table cellspacing="0" cellpadding="0">
	
		<tr>
			<td>
			
			<asp:datagrid Width="1073" BorderWidth="0" BackColor="#000000" CellSpacing="1" id="dg_Inventory" runat="server" ShowFooter="false" HorizontalAlign="Left" CellPadding="2"
					AutoGenerateColumns="false" HeaderStyle-CssClass="DataGridHeader" AlternatingItemStyle-CssClass="DataGridRow_Alternate"
					ItemStyle-CssClass="DataGridRow" CssClass="DataGrid">
					<AlternatingItemStyle CssClass="LinkNormal LightGray"></AlternatingItemStyle>
					<ItemStyle CssClass="LinkNormal DarkGray"></ItemStyle>
					<HeaderStyle CssClass="LinkNormal Bold OrangeColor"></HeaderStyle>
					<Columns>
						<asp:ButtonColumn Text="Update" ButtonType="PushButton" CommandName="Submit"></asp:ButtonColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="ORDER_NUMBER" DataNavigateUrlFormatString="/administrator/Transaction_Details.aspx?ID={0}" 
 DataTextField="ORDER_NUMBER" HeaderText="Trade">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="SELLER_ID" DataNavigateUrlFormatString="/administrator/Company_Info.aspx?Id={0}" 
 DataTextField="TEXTSELLER" HeaderText="Seller">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="BUYER_ID" DataNavigateUrlFormatString="/administrator/Company_Info.aspx?Id={0}" 
 DataTextField="TEXTBUYER" HeaderText="Buyer">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:TemplateColumn HeaderText="Weight">
							<ItemTemplate>
								<asp:TextBox CssClass="InputForm" id="txtWeight" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SHIPMENT_WEIGHT") %>' size="15">
								</asp:TextBox>
								<asp:RangeValidator id="RangeValidator1" runat="server" ErrorMessage="Invalid!" Type="Integer" MinimumValue="0" 
 MaximumValue="190000000" ControlToValidate="txtWeight" Display="Dynamic"></asp:RangeValidator>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="Date Taken">
							<ItemTemplate>
								<asp:TextBox CssClass="InputForm" id="txtDate" size="15" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SHIPMENT_DATE_TAKE") %>'>
								</asp:TextBox>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="Railcar #">
							<ItemTemplate>
								<asp:TextBox CssClass="InputForm" id="txtRCNum" size="15" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RAIL_NUMBER") %>' >
								</asp:TextBox>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn Visible="False" DataField="SHIPMENT_ID" HeaderText="Railcar #"></asp:BoundColumn>
						<asp:TemplateColumn>
							<ItemTemplate></ItemTemplate>
						</asp:TemplateColumn>
					</Columns>
				</asp:datagrid>
				<!---->
				
				</td>
		</tr>
		
		<tr>
			<td><div class="DivTitleBarMenu"><span class="Header Color1 Bold">Shipped</span>
				<span class="Content Color1">(Status is updated from SteelRoads.com every hour on the hour)</span></div></td>
		</tr>
		
		<tr>
			<td>
			
			<asp:datagrid BackColor="#000000" CellSpacing="1" BorderWidth="0" id="dg" runat="server" HorizontalAlign="Left" CellPadding="2" 
			AutoGenerateColumns="False" AllowSorting="True" 
					HeaderStyle-CssClass="DataGridHeader" AlternatingItemStyle-CssClass="DataGridRow_Alternate"
					ItemStyle-CssClass="DataGridRow" CssClass="DataGrid">
					<AlternatingItemStyle CssClass="LinkNormal LightGray"></AlternatingItemStyle>
					<ItemStyle CssClass="LinkNormal DarkGray"></ItemStyle>
					<HeaderStyle CssClass="LinkNormal Bold Color1 OrangeColor"></HeaderStyle>
					<Columns>
						<asp:ButtonColumn Text="History" ButtonType="PushButton" CommandName="Track"></asp:ButtonColumn>
						
						<asp:HyperLinkColumn DataNavigateUrlField="ORDER_NUMBER" DataNavigateUrlFormatString="/administrator/Transaction_Details.aspx?ID={0}" 
                        DataTextField="ORDER_NUMBER" HeaderText="Trade" SortExpression="ORDER_NUMBER ASC">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
												
						<asp:HyperLinkColumn DataNavigateUrlField="SELLER_ID" DataNavigateUrlFormatString="/administrator/Company_Info.aspx?Id={0}" 
                        DataTextField="TEXTSELLER" HeaderText="Seller" SortExpression="TEXTSELLER ASC">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:HyperLinkColumn DataNavigateUrlField="BUYER_ID" DataNavigateUrlFormatString="/administrator/Company_Info.aspx?Id={0}" 
                        DataTextField="TEXTBUYER" HeaderText="Buyer" SortExpression="TEXTBUYER ASC">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:HyperLinkColumn>
						<asp:BoundColumn DataField="RAIL_NUMBER" HeaderText="Railcar #" SortExpression="RAIL_NUMBER ASC">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="CURRENT_LOCATION" HeaderText="Current"  SortExpression="CURRENT_LOCATION ASC">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="EVENT_TIME" HeaderText="Updated" DataFormatString="{0:MM/dd/yyyy}" SortExpression="EVENT_TIME ASC">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="DAYS_SITTING" HeaderText="Days"  SortExpression="DAYS_SITTING ASC">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="EVENT_CODE" HeaderText="Last Event"  SortExpression="EVENT_CODE ASC">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="DESTINATION_LOCATION" HeaderText="Destination"  SortExpression="DESTINATION_LOCATION ASC">
							<ItemStyle Wrap="False"></ItemStyle>
						</asp:BoundColumn>
						<asp:ButtonColumn Text="Returned" ButtonType="PushButton" CommandName="Delivered"></asp:ButtonColumn>
					</Columns>
				</asp:datagrid></td>
		</tr>
		
		<tr>
			<td>
			
			    <div id="divHistory" class="DivTitleBarMenu">
			        <span class="Content Color1 Bold">Rail Car History </span>
			        <span class="Content Color1">Enter RC # or Order #&nbsp;</span>
				    <asp:textbox CssClass="InputForm" id="txtSearch" runat="server" AutoCompleteType="disabled"  />
				    
				    <cc1:AutoCompleteExtender 
				        runat="server" 
				        id="autocomplete1" 
				        TargetControlID="txtSearch"
				        ServiceMethod="GetSuggestions"
				        ServicePath="/rc/Service1.asmx"
				        MinimumPrefixLength="2"
				        CompletionInterval="1000"
                        EnableCaching="true"
                        CompletionSetCount="10"  				    				    
				    >
				    
				    </cc1:AutoCompleteExtender>
				    
				    <asp:button Height="20" id="btnTrack" runat="server" Text="Get History" onclick="btnTrack_Click" />
				    
				    
				    
				</div>
	        
	        </td>
		</tr>
		
		<tr>
			<td>
			
			<asp:datagrid Width="1073" BackColor="#000000" BorderWidth="0" CellSpacing="1" id="dg_Track" runat="server" ShowFooter="false" HorizontalAlign="Left" CellPadding="2"
					AutoGenerateColumns="false" HeaderStyle-CssClass="LinkNormal Bold Color1 OrangeColor" AlternatingItemStyle-CssClass="LinkNormal DarkGray"
					ItemStyle-CssClass="LinkNormal LightGray" CssClass="DataGrid">
					<Columns>
					    <asp:BoundColumn DataField="OrderNumber" HeaderText="Order Number" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="EVENT_LOCATION_CITY" HeaderText="City" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="EVENT_LOCATION_STATE" HeaderText="St" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="EVENT_TIME" HeaderText="Time" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="EVENT_STATUS" HeaderText="" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="EVENT_CODE" HeaderText="Event" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="EVENT_DESTINATION_CITY" HeaderText="Dest City" ItemStyle-Wrap="false" />
						<asp:BoundColumn DataField="EVENT_DESTINATION_STATE" HeaderText="St" ItemStyle-Wrap="false" />
					</Columns>
				</asp:datagrid>
				<asp:label id="lblNoTrackInfo" runat="server" Visible="False" Font-Bold="True" ForeColor="Red" CssClass="Content Bold"><br />There is no information for this request.<br />&nbsp;</asp:label></td>
				
		</tr>
		
	</table>


</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>