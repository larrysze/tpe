using System;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using TPE.Utility;
using System.Web.Mail;
using System.Configuration;
using System.Text.RegularExpressions;


namespace localhost
{
    /// <summary>
    /// 
    /// PROJECT NOTES: 
    ///		web site requires following additions to registry: run regedit then,
    ///		in HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Eventlog\Application
    ///		add Keys: EmailLibrary, and TPE (needed for event logging)
    /// 
    /// HomePage
    /// </summary>
    public partial class homepage : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.Label Label1;
        protected System.Web.UI.WebControls.Label Label2;
        protected System.Web.UI.WebControls.Label Label3;
        protected System.Web.UI.WebControls.PlaceHolder Placeholder1;
        protected System.Web.UI.WebControls.Label Label4;
        protected System.Web.UI.WebControls.Literal Literal1;
        protected System.Web.UI.WebControls.DataGrid Datagrid1;
        protected System.Web.UI.WebControls.HyperLink Hyperlink1;

        pcwDataShare.pcwdata pcw = new pcwDataShare.pcwdata();

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // flags the user if they were refered by a partner
            if (Request.QueryString["Referrer"] != null)
            {
                Session["Referrer"] = Request.QueryString["Referrer"].ToString();
            }

            RandomizeChart();            

            if (!Page.IsPostBack)
            {
                //HtmlGenericControl control = (HtmlGenericControl)Page.Controls[0].FindControl("MasterPageBody");
                //control.Attributes.Add("onload", "defaultOnLoad('" + chart_num + "')");

                BindSpotSummary();
                BindContract();
                addWeeklyReview();
                saveMUPararaphs();// comment this when there is a problem with calling pcw news.

                //Add spot offers			
                using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
                {
                    conn.Open();
                    AddSpotOffers(conn);
                    AddNews(conn);
                }

                initMarketUpdateLink();
            }
        }

        private void initMarketUpdateLink()
        {
            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();

                string sSQL = "SELECT TOP 1 review_date from WEEKLY_REVIEWS WHERE published = 1  ORDER BY review_date DESC";

                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, sSQL);

                DateTime review_date;
                while (dtr.Read())
                {
                    review_date = Convert.ToDateTime(dtr["review_date"].ToString());

                    string dateFileName = review_date.Month + "_" + review_date.Day + "_" + review_date.Year + ".pdf";
                    string strPdfFileName = Server.MapPath("/Research/Market_Update/pdf/" + dateFileName );
                    if (System.IO.File.Exists(strPdfFileName))
                    {
                        hlPe.NavigateUrl = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Market_Update/pdf/" + dateFileName;
                        hlPp.NavigateUrl = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Market_Update/pdf/" + dateFileName;
                    }

                }

            }
        }

        public static bool IsEmail(string email)
        {
            string MatchEmailPattern =
                @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                 + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				            [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                 + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				            [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                 + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

            if (email != null) 
                return Regex.IsMatch(email, MatchEmailPattern);
            else 
                return false;
        }


        protected void btnJoin_Click(object sender, EventArgs e)
        {            

            if (!EmailInSystem())
            {
                Hashtable param = new Hashtable();
                string email = txtEmailSubs.Text;

                if (!IsEmail(email))
                {
                    txtEmailSubs.Text = "Please enter a valid email address";
                    Response.Redirect("http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/default.aspx");
                }

                param.Add("@Email", email);
                

                string strSqlInsert = "INSERT INTO mailing (MAI_MAIL, MAI_CODE) VALUES (@Email, '0')";
                DBLibrary.ExecuteSQLStatement(Application["DBconn"].ToString(), strSqlInsert, param);
                
                //Mike wants to receive an email when somebody subscribed to MU
                string txtMessage = email + " added to subscription list.";                
                string txtSubject = email + " in MU list";
                MailMessage mailToMike = new MailMessage();
                mailToMike.From = "research@theplasticsexchange.com";
                mailToMike.To = "michael@theplasticexchange.com";
                mailToMike.Subject = txtSubject;
                mailToMike.Body = txtMessage;
                EmailLibrary.Send(mailToMike);

                
                Response.Redirect("https://" + ConfigurationSettings.AppSettings["SecureDomainName"].ToString() + "/Public/Registration.aspx?Referrer=EmailJoin&SubsEmail="+email);
            }
            else
            {
                Response.Redirect("http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/default.aspx");
            }
        }

        private bool EmailInSystem()
        {
            //lblEmail.Text = "";
            //Check if the email is in the DB
            Hashtable param = new Hashtable();
            string email = txtEmailSubs.Text;
            bool inSystem;

            if (txtEmailSubs.Text.Trim().Length > 0)
            {
                string strSql = "select MAI_MAIL FROM mailing WHERE MAI_MAIL = @Email";
                param.Add("@Email", email);
                DataTable dt = DBLibrary.GetDataTableFromSelect(Application["DBconn"].ToString(), strSql, param);

                if (dt.Rows.Count == 0)
                    inSystem = false;
                else
                {
                    inSystem = true;                    
                }
            }
            else
            {
                inSystem = true;
            }
            return inSystem;
        }


        private void saveMUPararaphs()
        {

            string lnk1= null;
            string lnk2= null;

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();

                string sSQL = "SELECT TOP 1 review_date from WEEKLY_REVIEWS WHERE published = 1  ORDER BY review_date DESC";

                SqlDataReader dtr = DBLibrary.GetDataReaderFromSelect(conn, sSQL);

                DateTime review_date;
                while (dtr.Read())
                {
                    review_date = Convert.ToDateTime(dtr["review_date"].ToString());

                    string dateFileName = review_date.Month + "_" + review_date.Day + "_" + review_date.Year + ".pdf";
                    string strPdfFileName = Server.MapPath("/Research/Market_Update/pdf/" + dateFileName);
                    if (System.IO.File.Exists(strPdfFileName))
                    {
                        lnk1 = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Market_Update/pdf/" + dateFileName;
                        lnk2 = "http://" + ConfigurationSettings.AppSettings["DomainName"].ToString() + "/Research/Market_Update/pdf/" + dateFileName;
                    }
                }
            }

            using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
            {
                conn.Open();
                string strSQL = "select top 1 polyethylene, polypropylene  from weekly_reviews order by id desc";
                using (SqlDataReader dtrMU = DBLibrary.GetDataReaderFromSelect(conn, strSQL))
                {
                    if (dtrMU.Read())
                    {

                        string weekPE = dtrMU["polyethylene"].ToString().Substring(0, 450) + "...";
                        string weekPP = dtrMU["polypropylene"].ToString().Substring(0, 450) + "...";

                        string more_link_begin = " <span class='LinkNormal Content'><a target='_blank' href='";
                        string more_link_end = "'><span class='blackbold'>[More]</span></a></span>";

                        lblPP.InnerHtml = weekPP + more_link_begin + lnk2 + more_link_end;                        
                        lblPE.InnerHtml = weekPE + more_link_begin + lnk1 + more_link_end;
                    }
                }
            }


        }

        private void addWeeklyReview()
        {
            string strSQL = "SELECT MAX(review_date) FROM weekly_reviews WHERE published=1";
            DateTime current_review_date = Convert.ToDateTime(TPE.Utility.DBLibrary.ExecuteScalarSQLStatement(Application["DBConn"].ToString(), strSQL, null));
            //			lblDateMR.Text = current_review_date.Month + "/" + current_review_date.Day + "/" + current_review_date.Year;
        }

        private void BindSpotSummary()//bind the data with the grid.
        {
            //select the data from the bboffer table,and group the record by the data stored in the OFFR_PROD field of the BBOFFER table.to calculate the count of records,the maximum price,the minimum price and the count of the weight for each group.
            string seleStr = "SELECT ";
            seleStr += "count=count(*), ";
            seleStr += "prodlink=('/Spot/Spot_Floor.aspx?Filter='), ";
            seleStr += "GRADE= (SELECT GRADE_NAME from GRADE WHERE GRADE_ID = OFFR_GRADE), ";
            seleStr += "offr_grade, ";
            seleStr += "Min=Cast(min(offr_prce) AS money) , ";//Cast(min(offr_prce) AS money)
            seleStr += "Max=Cast(max(offr_prce) AS money), ";
            seleStr += "VARSIZE=sum(OFFR_QTY*OFFR_SIZE) ";
            //seleStr+="FROM bboffer where offr_grade is not null group by offr_grade  ";
            seleStr += "FROM bboffer where offr_grade is not null and offr_port is null and offr_grade <> 11 and offr_grade <> 10 and OFFR_SIZE <> '38500' and OFFR_SIZE <> '44500' group by offr_grade  ";
            seleStr += "ORDER BY MAX DESC";

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString()))
            {
                //connect to the database.
                System.Data.DataSet ds = new DataSet();
                System.Data.SqlClient.SqlDataAdapter ad = new System.Data.SqlClient.SqlDataAdapter(seleStr, conn);
                ad.Fill(ds);//set the query data to dataset.

                dgSpot.DataSource = ds;//get data from the dataset.
                dgSpot.DataBind();
            }
        }

        private void BindContract()
        {
            int fwdMonth = GetTPEForwardMonth(DateTime.Today.Month, DateTime.Today.Year.ToString());

            string seleStr = @"Select 
                    CONT_LABL= (SELECT GRADE_NAME from GRADE WHERE GRADE_ID = (select spotid from homepagespotcontract where contractid=CONT_ID)), 
                    CONT_ID,
                    QTYBID =(SELECT SUM(BID_QTY) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='{0}' AND BID_SIZE='1'),
                    BID = (SELECT MAX(BID_PRCE) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='{1}' AND BID_SIZE='1'),
                    OFFR = (SELECT MAX(OFFR_PRCE) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='{2}' AND OFFR_SIZE='1'), 
                    QTYOFFR = (SELECT SUM(OFFR_QTY) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='{3}' AND OFFR_SIZE='1'),
                    VARSIZE =(SELECT SUM(OFFR_QTY*OFFR_SIZE) FROM bboffer where offr_grade=(select spotid from homepagespotcontract where contractid=CONT_ID)) 
                    From CONTRACT WHERE CONT_CATG <>'Monomer' AND CONT_ENABLED='True' and CONT_ID <> 19 and CONT_ID <> 20
                    ORDER BY OFFR DESC";

            seleStr = string.Format(seleStr, fwdMonth, fwdMonth, fwdMonth, fwdMonth);

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString()))
            {
                //connect to the database.
                System.Data.DataSet ds = new DataSet();
                System.Data.SqlClient.SqlDataAdapter ad = new System.Data.SqlClient.SqlDataAdapter(seleStr, conn);
                ad.Fill(ds);//set the query data to dataset.

                dgContract.DataSource = ds;//get data from the dataset.
                dgContract.DataBind();
            }
        }

        public int GetTPEForwardMonth(int sMonth, string sYear)
        {
            Hashtable htParam = new Hashtable();
            htParam.Add("@month", GetMonth(sMonth));
            htParam.Add("@year", sYear);

            string sSQL = "SELECT FWD_ID FROM FWDMONTH WHERE FWD_MNTH=@MONTH AND FWD_YEAR=@year";
            string test = HttpContext.Current.Application["DBconn"].ToString();
            int forwardID = int.Parse(DBLibrary.ExecuteScalarSQLStatement(HttpContext.Current.Application["DBconn"].ToString(), sSQL, htParam).ToString());

            return forwardID;
        }

        public string GetMonth(int month)
        {
            string sMonthName = "";

            switch (month)
            {
                case 1:
                    sMonthName = "January";
                    break;
                case 2:
                    sMonthName = "February";
                    break;
                case 3:
                    sMonthName = "March";
                    break;
                case 4:
                    sMonthName = "April";
                    break;
                case 5:
                    sMonthName = "May";
                    break;
                case 6:
                    sMonthName = "June";
                    break;
                case 7:
                    sMonthName = "July";
                    break;
                case 8:
                    sMonthName = "August";
                    break;
                case 9:
                    sMonthName = "September";
                    break;
                case 10:
                    sMonthName = "October";
                    break;
                case 11:
                    sMonthName = "November";
                    break;
                case 12:
                    sMonthName = "December";
                    break;

            }

            return sMonthName;


        }


        protected void hplTotalLbs_PreRender(object sender, System.EventArgs e)
        {
            //Total lbs.
            decimal totalLbs = totalLbs = HelperFunction.totalPoundsOffers(this.Context);
            hplTotalLbs.Text = "Total Spot: " + String.Format("{0:#,###}", totalLbs) + " lbs";
            hplTotalLbs.NavigateUrl = "/spot/Spot_Floor.aspx?Filter=-1";
        }

        private void AddSpotOffers(SqlConnection conn)
        {
            StringBuilder sbSpotOffers = new StringBuilder();
            string strOffer;
            SqlCommand cmdSpotOffers;
            SqlDataReader dtrSpotOffers;

            cmdSpotOffers = new SqlCommand("SELECT OFFR_ID,OFFR_QTY,VARSIZE=CAST(OFFR_QTY AS VARCHAR)+' '+(CASE OFFR_SIZE WHEN 1 THEN 'lbs' WHEN 2.205 THEN 'Kilo' WHEN 2205 THEN 'Metric Ton' WHEN 42000 THEN 'TL Boxes' WHEN 45000 THEN 'BT' WHEN 44092 THEN 'TL Bags' WHEN 190000 THEN 'HC'	END)+(CASE WHEN OFFR_QTY>1 AND OFFR_SIZE <> 44092 THEN ' ' ELSE ' ' END),OFFR_PROD, OFFR_PRCE, OFFR_DETL, OFFR_DENS, OFFR_MELT From BBOFFER WHERE OFFR_HOT='1' AND OFFR_ZIP is not null", conn);
            dtrSpotOffers = cmdSpotOffers.ExecuteReader();
            int iCount;
            iCount = 1;

            // create list of parameters for java applet
            ArrayList offerArray = new ArrayList();
            ArrayList offerIDArray = new ArrayList();
            // read amounts into two parrallel arrays
            while (dtrSpotOffers.Read())
            {
                strOffer = dtrSpotOffers["VARSIZE"].ToString();
                strOffer += dtrSpotOffers["OFFR_PROD"].ToString();
                strOffer += " ";
                strOffer += dtrSpotOffers["OFFR_DETL"].ToString();
                if (HelperFunction.getSafeStringFromDB(dtrSpotOffers["OFFR_MELT"]).Trim() != "")
                {
                    strOffer += " Melt: ";
                    strOffer += HelperFunction.getSafeStringFromDB(dtrSpotOffers["OFFR_MELT"]).Trim();
                }
                if (HelperFunction.getSafeStringFromDB(dtrSpotOffers["OFFR_DENS"]).Trim() != "")
                {
                    strOffer += " Density: ";
                    strOffer += HelperFunction.getSafeStringFromDB(dtrSpotOffers["OFFR_DENS"]).Trim();
                }
                strOffer += " $";
                strOffer += dtrSpotOffers["OFFR_PRCE"].ToString();

                offerArray.Add(strOffer);
                offerIDArray.Add(dtrSpotOffers["OFFR_ID"].ToString());
                iCount++;
            }
            dtrSpotOffers.Close();

            // the parameters need to start random point.  this logic defines that random pnt and displays the records for after
            // the point and then the ones before it
            Random rand = new Random();
            int iRandom = rand.Next(1, (iCount - 1));

            for (int k = iRandom; k <= iCount - 1; k++)
            {
                sbSpotOffers.Append("<param name=text" + (k - iRandom).ToString() + " value='" + offerArray[k - 1].ToString() + "'>");
                sbSpotOffers.Append("<param name=link" + (k - iRandom).ToString() + " value=" + System.Configuration.ConfigurationSettings.AppSettings["UrlPrefix"] + "://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/Spot/Inquire.aspx?Id=" + offerIDArray[k - 1].ToString() + "><param name=target_frame" + k.ToString() + " value=_self>");
            }
            for (int k = 0; k <= (iCount - iRandom - 1); k++)
            {
                sbSpotOffers.Append("<param name=text" + k.ToString() + " value='" + offerArray[k].ToString() + "'>");
                sbSpotOffers.Append("<param name=link" + k.ToString() + " value=" + System.Configuration.ConfigurationSettings.AppSettings["UrlPrefix"] + "://" + System.Configuration.ConfigurationSettings.AppSettings["DomainName"] + "/Spot/Inquire.aspx?Id=" + offerIDArray[k].ToString() + "><param name=target_frame" + k.ToString() + " value=_self>");
            }
            // add parameters as literal control
            phSpotParameters.Controls.Add(new LiteralControl(sbSpotOffers.ToString()));
        }

        private void AddMarketResearch()
        {
            string strPE = "";
            string strPS = "";
            string strPP = "";

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dtr = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn, "SELECT * FROM MARKET_UPDATES WHERE published=1 and DATE = (SELECT MAX(DATE) FROM MARKET_UPDATES WHERE PUBLISHED=1)");

                if (dtr.Read())
                {
                    //***** CF: 3-1-2007: Changed since it was reading 2nd paragraph instead of first

                    //strPS = dtr["polystyrene"].ToString();
                    //strPP = dtr["polypropylene"].ToString();
                    //strPE = dtr["polyethylene"].ToString();

                    strPS = dtr["pretext_ps"].ToString();
                    strPP = dtr["pretext_pp"].ToString();
                    strPE = dtr["pretext_pe"].ToString();

                    if (strPE.Length > 300)
                    {
                        strPE = strPE.Substring(0, 300) + "...";
                    }

                    if (strPS.Length > 300)
                    {
                        strPS = strPS.Substring(0, 300) + "...";
                    }

                    if (strPP.Length > 300)
                    {
                        strPP = strPP.Substring(0, 300) + "...";
                    }

                    DateTime dateMU = Convert.ToDateTime(dtr["date"].ToString());
                    //tmpDateMU.Text = "<div style=\"VISIBILITY: hidden; display: none\" id=tmpDateMU>" + dateMU.ToString("MMMM yyyy") + "</div>";

                    //tmpMonthPE.Text = "<div style=\"visibility: hidden; display: none\" id=tmpMonthPE>" + strPE + "</div>";
                    //tmpMonthPP.Text = "<div style=\"visibility: hidden; display: none\" id=tmpMonthPP>" + strPP + "</div>";
                    //tmpMonthPS.Text = "<div style=\"visibility: hidden; display: none\" id=tmpMonthPS>" + strPS + "</div>";


                }
            }

        }

        private void AddNews(SqlConnection conn)
        {
            // add news
            string strNews = "";
            SqlCommand cmdNews;
            SqlDataReader dtrNews;

            cmdNews = new SqlCommand("spShortNews", conn);
            cmdNews.CommandType = CommandType.StoredProcedure;
            dtrNews = cmdNews.ExecuteReader();
            strNews += "<table border=0>";

            while (dtrNews.Read())
            {
                string news_headline = dtrNews["HEADLINE"].ToString();
                string id = "";
                id = dtrNews["ID"].ToString();
                string url = "/Public/News_Template.aspx?Buffer=&Id=" + id;
                string news_date = dtrNews["strDate"].ToString();

                if (news_headline.StartsWith("The Plastics Exchange Week in Review"))
                {
                    news_headline = "<font color='red'>" + news_headline + "</font>";
                    long ticks = Convert.ToDateTime(news_date).Ticks;
                    url = "/Research/WeeklyReview.aspx?date=" + ticks;
                }
                else if (news_headline.StartsWith("The Plastics Exchange Monthly Update"))
                {
                    news_headline = "<font color='red'>" + news_headline + "</font>";
                    long ticks = Convert.ToDateTime(news_date).Ticks;
                    url = "/Public/Public_Research.aspx?date=" + ticks;
                }

                if (news_headline.StartsWith("The Plastics Exchange Spot Market Sentiment"))
                {
                    news_headline = "<font color='red'>" + news_headline + "</font>";
                }

                strNews += "<tr><td valign=\"top\"><td valign=\"top\" class='LinkNormal'><a href=\"" + url + "\"><span class='Content Color2'>" + news_headline + "</span></a><span class='Content Color2'> (" + news_date + ")</span></td></tr>";
            }

            dtrNews.Close();
            strNews += "</table>";
            phNews.Controls.Add(new LiteralControl(strNews));
        }

        private void RandomizeChart()
        {
            string seleStr = @"SELECT top 1
                                offr_grade,
                                VARSIZE=sum(OFFR_QTY*OFFR_SIZE) 
                                FROM bboffer 
                                where offr_grade is not null and offr_port is null 
                                and OFFR_SIZE <> '38500' and OFFR_SIZE <> '44500' 
                                group by offr_grade  
                                ORDER BY VARSIZE DESC";

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(Application["DBConn"].ToString()))
            {
                SqlDataReader dr = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn, seleStr);
                dr.Read();

                lblSpotChartID.Text = dr["offr_grade"].ToString();

                LiteralImg.Text = "<IMG border='0' name='ChartImg' src='/Research/Charts/home/Spot_" + dr["offr_grade"].ToString() + ".png'>";
            }

            string contractid = Convert.ToString(TPE.Utility.DBLibrary.ExecuteScalarSQLStatement(Application["DBConn"].ToString(), "select contractid from HomePageSpotContract where spotid=" + lblSpotChartID.Text));
            lblContractChartID.Text = contractid;            
        }



        private void dgSummaryGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            string strGrade;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                e.Item.Cells[4].Text = e.Item.Cells[2].Text + "-" + e.Item.Cells[3].Text;

                strGrade = (DataBinder.Eval(e.Item.DataItem, "offr_grade")).ToString();

                e.Item.Attributes.Add("class", "hspotcontent");
                e.Item.Attributes.Add("onclick", "window.location ='/spot/Spot_Floor.aspx?Filter=" + (DataBinder.Eval(e.Item.DataItem, "offr_grade")).ToString() + "';");
                e.Item.Attributes.Add("onmouseover", "ChartImg.src = '/Research/Charts/home/Spot_" + strGrade + ".png';this.style.backgroundColor='#FF8B00';this.style.cursor='hand';");
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='black';this.style.cursor='pointer';");

                //e.Item.Attributes.Add("onmouseover", "ChartImg.src = '/Research/Charts/Chart_" + strGrade + "_1Y.png';this.style.backgroundColor='#FE0002';this.style.cursor='hand';changeChartName('" + strGrade + "');");                
                //e.Item.Attributes.Add("onmouseover", "ChartImg.src = '/Research/ChartsSpot/Chart_" + strGrade + "_1Y.png';this.style.backgroundColor='FD0100';this.style.cursor='hand';changeChartName('" + strGrade + "');");
                //e.Item.Attributes.Add("onmouseover", "javascript:changeChart('Chart_"+strGrade+"_1Y')");
            }
        }

        protected void dgContract_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            string strGrade;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                strGrade = (DataBinder.Eval(e.Item.DataItem, "cont_id")).ToString();

                e.Item.Attributes.Add("class", "hspotcontent");
                e.Item.Attributes.Add("onclick", "window.location ='/spot/Spot_Floor.aspx?Filter=" + (DataBinder.Eval(e.Item.DataItem, "cont_id")).ToString() + "';");
                e.Item.Attributes.Add("onmouseover", "ChartImg.src = '/Research/Charts/home/Contract_" + strGrade + ".png';this.style.backgroundColor='#FE0002';this.style.cursor='hand';");
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='black';this.style.cursor='pointer';");

                //e.Item.Attributes.Add("onmouseover", "ChartImg.src = '/Research/Charts/Chart_" + strGrade + "_1Y.png';this.style.backgroundColor='#FF8B00';this.style.cursor='hand';changeChartName('" + strGrade + "');");                
                //e.Item.Attributes.Add("onmouseover", "javascript:changeChart('Chart_"+strGrade+"_1Y')");
                //e.Item.Attributes.Add("onmouseover", "ChartImg.src = '/Research/Charts/Chart_" + strGrade + "_1Y.png';this.style.backgroundColor='111111';this.style.cursor='hand';changeChartName('" + strGrade + "');");
                //e.Item.Attributes.Add("onmouseover", "ChartImg.src = '/Research/ChartsSpot/Chart_" + strGrade + "_1Y.png';this.style.backgroundColor='111111';this.style.cursor='hand';changeChartName('" + strGrade + "');");
                //e.Item.Attributes.Add("onmouseover", "javascript:changeChart('Chart_"+strGrade+"_1Y')");                
            }

        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgSpot.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgSummaryGrid_ItemDataBound);
            this.dgContract.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgContract_ItemDataBound);

        }
        #endregion


    }
}
