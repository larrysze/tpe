<%@ Page Language="C#" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Web.Mail" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Data" %>
<script runat="server">

    /************************************************************************
    *   1. File Name       :News\AddNews.aspx                               *
    *   2. Description     :Allows user to submit/edit news stories         *
    *   3. Modification Log:                                                *
    *     Ver No.       Date          Author             Modification       *
    *   -----------------------------------------------------------------   *
    *      1.00      2-25-2004      Zach                Comment             *
    *                                                                       *
    ************************************************************************/
    private void Page_Load(object sender, EventArgs e){
    
       //if (Session["Typ"].ToString() !="A" && Session["Typ"].ToString() !="N") Response.Redirect("/default.aspx");
       Bind();
    
    }
    private void Bind(){
         string strSQL;
         SqlDataAdapter dadContent;
         DataSet dstContent;
         
         using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
         {
			conn.Open();
	    
			strSQL ="SELECT NEWS_ID,NEWS_DATEs=CONVERT(varchar(12) , NEWS_DATE, 101),NEWS_LABL FROM NEWS ORDER BY NEWS_DATE DESC,NEWS_ID DESC ";
			dadContent = new SqlDataAdapter(strSQL,conn);
			dstContent = new DataSet();
			dadContent.Fill(dstContent);
			dg.DataSource = dstContent;
			dg.DataBind();
	    
	    
			if (Request.QueryString["Decline"] != null){
			// remove the company from the list
				SqlCommand cmdContent;
				cmdContent= new SqlCommand("Delete From NEWS where NEWS_ID='"+Request.QueryString["Decline"].ToString()+"'", conn);
				cmdContent.ExecuteNonQuery();
				conn.Close();
				Response.Redirect("AddNews.aspx");
			}
		}
    }
    private void ChangeDGPage (object sender,DataGridPageChangedEventArgs e){
        dg.CurrentPageIndex = e.NewPageIndex;
        Bind();
    }
    private void ClickAddStory(object sender, EventArgs e){
        pnMain.Visible = false;
        pnAdd.Visible = true;
    }
    
    private void ClickCancel(object sender, EventArgs e){
        pnMain.Visible = true;
        pnAdd.Visible = false;
    }
    private void ClickSubmitStory(object sender, EventArgs e){
         using (SqlConnection conn = new SqlConnection(Application["DBconn"].ToString()))
         {
			conn.Open();
			SqlCommand cmdInsert;
			cmdInsert= new SqlCommand("INSERT INTO NEWS (NEWS_LABL,NEWS_CONTENT,NEWS_AREA,NEWS_TYPE) VALUES ('"+txtHeadline.Text+"','"+ParseParagraph(Parse(txtStory.Text))+"','"+ddlLocation.SelectedItem.ToString()+"','"+ddlCategory.SelectedItem.ToString()+"')", conn);
			cmdInsert.ExecuteNonQuery();
         }
         Bind();
         // reset fields now that we are done
         txtStory.Text = "";
         txtHeadline.Text = "";
         // swap back panels
         pnMain.Visible = true;
         pnAdd.Visible = false;
    }
    
    
    
    // <summary>
    // The purpose of this method is for the program to render the carriage returns properly as new paragraphs
    // </summary>
    private string ParseParagraph(string strTextToParse){
        string[] arrParagraph;
        string strText;
        Regex r = new Regex(((char)13).ToString()); // Split on CR's.
        // compiler forces the array to be used outside of the switch statement
        // the following two lines will be overwritten but it is included to satisfy the compiler requirement
        arrParagraph = r.Split(strTextToParse);
        StringBuilder sb = new StringBuilder();
    
    
        // loops through the paragraphs now in the array and adds html to them
        for (int k=0;k < arrParagraph.Length;k++)
        {
            sb.Append("<P align=justify>" + arrParagraph[k]+"</p>");
        }
        // returns the properly formatted paragraph
        return sb.ToString();
    }
    
    
      // <summary>
      // Removes bad charachters that would crap out out the db insert
      // </summary>
      public string Parse(string TextIN){
       //  remove ' character
        string Parse;
        Parse = TextIN;
        while (Parse.IndexOf("'") > 0){
            Parse = Parse.Replace("'", "");
        }
        // remove " character
        while (Parse.IndexOf("\"") > 0){
            Parse = Parse.Replace("\"", "");
        }
        return Parse;
    
      }

</script>
<html>
    <head>
    </head>
    <body>
        <form runat="server">


            <TPE:Template Margins="true" PageTitle="Submit News" Runat="Server" />
            <asp:Panel id="pnMain"  runat="server">

            <table border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td align="center"><span class="PageHeader">Submit News</span></td>
                </tr>
                <tr>
                    <td><asp:Button runat="server" Text="Add Story" onclick="ClickAddStory" /></td>
                </tr>
                <tr>
                    <td>

                            <asp:DataGrid id="dg" runat="server"
                                CssClass = "DataGrid"
                                ItemStyle-CssClass="DataGridRow"
                                AlternatingItemStyle-CssClass = "DataGridRow_Alternate"
                                HeaderStyle-CssClass="DataGridHeader"
                                AutoGenerateColumns="false"
                                AllowPaging="True"
                                PageSize="10"
                                OnPageIndexChanged="ChangeDGPage"
                                PagerStyle-Visible="True"
                                CellPadding="2"
                                PagerStyle-Mode ="NumericPages"
                                HorizontalAlign="Center"
                                Width="400px" >
                                <Columns>
                                    <asp:HyperLinkColumn
                                        HeaderText=""
                                        ItemStyle-Wrap="false"
                                        DataNavigateUrlField="NEWS_ID"
                                        DataNavigateUrlFormatString= "/News/AddNews.aspx?Decline={0}"
                                        Text= "Delete"  />

                                    <asp:HyperLinkColumn
                                        HeaderText=""
                                        ItemStyle-Wrap="false"
                                        DataNavigateUrlField="NEWS_ID"
                                        DataNavigateUrlFormatString= "/News/Edit_Story.aspx?Id={0}"
                                        Text= "Edit"  />
                                    <asp:BoundColumn
                                        DataField="NEWS_DATEs" HeaderText="Date"
                                        ItemStyle-Wrap="false"/>
                                    <asp:HyperLinkColumn
                                        HeaderText=""
                                        ItemStyle-Wrap="false"
                                        DataNavigateUrlField="NEWS_ID"
                                        DataNavigateUrlFormatString= "/News/Resin_News_Template.aspx?Id={0}"
                                        DataTextField= "NEWS_LABL"  />






                                </Columns>
                            </asp:DataGrid>
                       </td>
                   </tr>
               </table>



            </asp:Panel>

            <asp:Panel id="pnAdd"  runat="server" Visible="false">
            <table border="0" cellpadding="0" width="450" cellspacing="0" align="center">
                <tr>
                    <td colspan="2" align="center"><span class="PageHeader">Submit New Story</span></td>
                </tr>
                <tr>
                    <td colspan="2">Location:
                        <asp:DropDownList id="ddlLocation" runat="server">

                            <asp:ListItem Text="North America" />
                            <asp:ListItem Text="Africa" />
                            <asp:ListItem Text="Asia" />
                            <asp:ListItem Text="Central/South America" />
                            <asp:ListItem Text="Europe" />
                            <asp:ListItem Text="Middle East" />
                            <asp:ListItem Text="N/A" />
                        </asp:DropDownList>
                    Category:
                        <asp:DropDownList id="ddlCategory" runat="server">
                            <asp:ListItem Text="General" />
                            <asp:ListItem Text="PE" />
                            <asp:ListItem Text="PP" />
                            <asp:ListItem Text="PS" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Headline:<BR>
                        <asp:TextBox runat="server"
                                id="txtHeadline"
                                TextMode = "MultiLine"
                                Rows = "2"
                                Columns ="50"
                                maxlength="200"
                            />


                    </td>
                </tr>
                <tr>

                    <td>
                        Story:<BR>
                        <asp:TextBox runat="server"
                                id="txtStory"
                                TextMode = "MultiLine"
                                Rows = "30"
                                Columns ="50"
                                maxlength="4000"
                                />
                    </td>
                 </tr>
                 <tr>
                    <td colspan="2" align="center">
                        <asp:Button runat="server" onclick="ClickSubmitStory" Text="Submit" />
                        &nbsp&nbsp&nbsp&nbsp&nbsp
                        <asp:Button runat="server" onclick="ClickCancel" Text="Cancel" />
                    </td>
                 </tr>
            </table>



            </asp:Panel>


            <TPE:Template Footer="true" Runat="Server" />
        </form>
    </body>
</html>
