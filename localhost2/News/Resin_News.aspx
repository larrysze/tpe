<%@ import Namespace="System.Web.Mail" %>
<%@ import Namespace="System.Data.SqlClient" %>
<%@ Register TagPrefix="TPE" TagName="Instructions" Src="/include/ScreenInstructions.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Web_Box" Src="/include/Box_Template.ascx" %>
<%@ Register TagPrefix="TPE" TagName="Template" Src="/include/Template.ascx" %>
<%@ Page Language="c#" CodeBehind="Resin_News.aspx.cs" AutoEventWireup="false" Inherits="localhost.News.Resin_News" %>
<script runat="server">

    /******************************************************************************
    '*   1. File Name       : Public\Public_News.aspx                            *
    '*   2. Description     : Public_News(View all news)                         *
    '*			                                                         *
    '*   3. Modification Log:                                                    *
    '*     Ver No.       Date          Author             Modification           *
    '*   -----------------------------------------------------------------       *
    '*                                                                           *
    '*      2.00       2-27-2003       Xiaoda               Comment              *
    '*****************************************************************************/
       SqlConnection conn;
       double dCredit_Available;

        public void Page_Load(object sender, EventArgs e){

             conn = new SqlConnection(Application["DBconn"].ToString());
             conn.Open();
             String color;
             String DStr;

             //Execute store procedure spNews which returns news label and Id
             DStr="Exec spNews";
             SqlCommand cmdDel;
             SqlDataReader dtrDel;
             cmdDel= new SqlCommand(DStr, conn);
             dtrDel= cmdDel.ExecuteReader();
            while (dtrDel.Read())
                {
                //Buliding the pages with New in every row
                offers.Controls.Add (new LiteralControl("<tr ><td valign=\"top\"><img src=/images/icons/arRight.gif border=0><img src=/images/1x1.gif width=3 height=13 border=0><span >"+dtrDel["NEWS_DATEs"]+"</span></td><td><a href=/News/Resin_News_Template.aspx?Buffer=&Id="+dtrDel["NEWS_ID"]+">"+dtrDel["NEWS_LABL"]+"</a></td></tr>"));

                }
                dtrDel.Close();


             }

</script>
<form runat="server">
	<TPE:Template PageTitle="Plastic Resin News" Runat="server" id="Template1" />
	<BR>
	<TPE:Instructions Text="On our News page you can access the latest plastics related stories or go back in time and view the stories held in our database. Just click on the news story title to bring up the full article." Runat="server" id="Instructions1" />
	<BR>
	<BR>
	<TPE:Web_Box Heading="Plastics News" width="100%" Runat="server" id="Web_Box1" />
	<table cellspacing="2" cellpadding="2" border="0">
		<asp:PlaceHolder id="offers" runat="server"></asp:PlaceHolder>
	</table>
	<TPE:Web_Box Footer="True" Runat="server" id="Web_Box2" />
	<BR>
	<TPE:Template Footer="true" Runat="server" id="Template2" />
</form>
