using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Web;
using System.Web.SessionState;
using System.Web.Mail;
using localhost.Common;

namespace localhost 
{
	
	public class Global : System.Web.HttpApplication
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		public Global()
		{
			InitializeComponent();
		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{
			Application["iUsersOnline"] = 0;
        	    
			 //database varialbles pulled from web config file
            Application["DBconn"] = "Server=" + System.Configuration.ConfigurationSettings.AppSettings["DBServer"] + ";UID=" + ConfigurationSettings.AppSettings["DBUserId"] + ";PWD=" + ConfigurationSettings.AppSettings["DBPassword"] + ";database=" + ConfigurationSettings.AppSettings["Database"];
            //Application["DBconn"] = "Server=" + System.Configuration.ConfigurationSettings.AppSettings["DBServer"] + ";UID=tpe_site;PWD=austerlitz;database=TPEDevelopment";
            //Application["DBconn"] = "Server=" + System.Configuration.ConfigurationSettings.AppSettings["DBServer"] + ";UID=tpe_production;PWD=tpe_production1234;database=production";

            Application["MonoDB"] = "Server=" + System.Configuration.ConfigurationSettings.AppSettings["DBServer"] + ";UID=" + ConfigurationSettings.AppSettings["DBUserId"] + ";PWD=" + ConfigurationSettings.AppSettings["DBPassword"] + ";database=" + ConfigurationSettings.AppSettings["Database"];
            //Application["MonoDB"] = "Server=" + System.Configuration.ConfigurationSettings.AppSettings["DBServer"] + ";UID=tpe_site;PWD=austerlitz;database=PCE_temp";
            Application["PCW"] = "Server=" + System.Configuration.ConfigurationSettings.AppSettings["DBServer"] + ";UID=" + ConfigurationSettings.AppSettings["DBUserId"] + ";PWD=" + ConfigurationSettings.AppSettings["DBPassword"] + ";database=" + ConfigurationSettings.AppSettings["Database"];

            //Application["DBconn"] = "Server=" + System.Configuration.ConfigurationSettings.AppSettings["DBServer"] + ";UID=tpe_site;PWD=austerlitz;database=Production";
			
			Application["strEmailAdmin"] = System.Configuration.ConfigurationSettings.AppSettings["strEmailAdmin"];
			Application["strEmailOwner"] = System.Configuration.ConfigurationSettings.AppSettings["strEmailOwner"];
			Application["strEmailInfo"] = System.Configuration.ConfigurationSettings.AppSettings["strEmailInfo"];
			Application["strEmailLogin"] = System.Configuration.ConfigurationSettings.AppSettings["strEmailLogin"];
			Application["strEmailLogistics"] = System.Configuration.ConfigurationSettings.AppSettings["DBstrEmailLogisticsServer"];
			Application["IsDebug"] = Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["IsDebug"]);
			Application["MinLeadsToEmailInThread"] = System.Configuration.ConfigurationSettings.AppSettings["MinLeadsToEmailInThread"];
			Application["MaxLeadsToDisplayOnSendEmailScreen"] = System.Configuration.ConfigurationSettings.AppSettings["MaxLeadsToDisplayOnSendEmailScreen"];
			
			if (Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["SendEmails"]))
			{
				SmtpMail.SmtpServer =System.Configuration.ConfigurationSettings.AppSettings["SmtpServer"]; 
			}
		
		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{
            
			// Code that runs when a new session is started
			Session["Typ"] = "";
			Application["iUsersOnline"] = Convert.ToInt32(Application["iUsersOnline"]) + 1;
			
			//Reading a Cookies
			if (Request.Cookies["Id"]!= null)
				FUNCLogin.Login(this.Context,Convert.ToInt32(Request.Cookies["Id"].Value),true);
			
			
			
		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_Error(Object sender, EventArgs e)
		{
			//Code that runs when an unhandled error occurs

			// if debug, then don't handle errors so they get displayed
            
                if (!Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["IsDebug"]))
			    {	
				    logError(Server.GetLastError().ToString());
				    if (Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["SendEmails"]))
				    {	 

					    String message = "<font face=verdana color=red>"
						    + "<h4>" + Request.Url.ToString() + "</h4>"
                            + "<P>" + Request.ServerVariables["REMOTE_ADDR"].ToString()
						    + "<P><pre><font color='red'>" + Server.GetLastError().ToString() + "</pre>"
						    + "</font>";

					    Response.Write(message);

					    MailMessage mail = new MailMessage();
                        mail.From = "DevelopmentTeam@theplasticexchange.com";

                        mail.To = ConfigurationSettings.AppSettings["strEmailAdmin"].ToString(); 
                        //mail.To = (string)Application["strEmailAdmin"];
    	
					    // this changes the subject that that I know what the errors are
					    if (Server.GetLastError().ToString().LastIndexOf("System.IO.FileNotFoundException") > 0)
					    {
						    mail.Subject = "404 ERROR:" + Request.Url.ToString();
					    }
					    else if (Server.GetLastError().ToString().IndexOf("Invalid_Viewstate") >0 || Server.GetLastError().ToString().IndexOf("Invalid_Viewstate_Client_Disconnected") >0)
					    {
						    mail.Subject = "ViewState ERROR:" + Request.Url.ToString();
					    }
					    else
					    {
						    mail.Subject = "ERROR:" + Request.Url.ToString();
					    }

					    mail.Body = message;
					    mail.BodyFormat = MailFormat.Html;
                        if (Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["SendErrorEmail"]))
                        {
                            TPE.Utility.EmailLibrary.SendWithoutContext(mail);
                        }					    
					    Server.ClearError(); // try to reduce "object moved to here" problem
					    Response.Redirect("/common/error.aspx");
				    }
			    }
            
			
		}

		void logError(string message)
		{
            ///     Site errors are logged to event viewer
            ///		requires following additions to registry: run regedit then,
            ///		in HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Eventlog\Application
            ///		add Keys: EmailLibrary, and TPE (needed for event logging)
	
			//Log error in Event Viewer
			EventLog el = new EventLog();
			el.Log = "Application";
			el.Source = "TPE";
			el.MachineName = ".";
			el.WriteEntry(message,EventLogEntryType.Error);
		}

		protected void Session_End(Object sender, EventArgs e)
		{
			// Code that runs when a session ends
			if (Session["Typ"].ToString() != "A" && Session["Typ"].ToString() != "B")
			{
				// admin and brokers are already removed from the count by default
				Application["iUsersOnline"] = Convert.ToInt32(Application["iUsersOnline"]) - 1;
			}
		
		}

		protected void Application_End(Object sender, EventArgs e)
		{

		}
			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

