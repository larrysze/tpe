using TPE.Utility;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;

namespace tpeDataShare
{
    [Serializable]
    public class inlist
    {
        public string f1;
        public string f2;
        public string f3;
    }

    /// <summary>
    /// Summary description for tpeData
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class tpeData : System.Web.Services.WebService
    {
        [System.Xml.Serialization.XmlInclude(typeof(inlist))]

        [WebMethod]
        public DataSet DrawTPE()
        {
            clsCommon c = new clsCommon();

            int fwdMonth = c.GetTPEForwardMonth(DateTime.Today.Month, DateTime.Today.Year.ToString());

            string SQLContract = @"Select 
                    CONT_LABL= (SELECT GRADE_NAME from GRADE WHERE GRADE_ID = (select spotid from homepagespotcontract where contractid=CONT_ID)), 
                    CONT_ID,
                    QTYBID =(SELECT SUM(BID_QTY) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='{0}' AND BID_SIZE='1'),
                    BID = (SELECT MAX(BID_PRCE) FROM BID WHERE BID_CONT=CONT_ID AND BID_MNTH='{1}' AND BID_SIZE='1'),
                    OFFR = (SELECT MAX(OFFR_PRCE) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='{2}' AND OFFR_SIZE='1'), 
                    QTYOFFR = (SELECT SUM(OFFR_QTY) FROM OFFER WHERE OFFR_CONT=CONT_ID AND OFFR_MNTH='{3}' AND OFFR_SIZE='1'),
                    VARSIZE =(SELECT SUM(OFFR_QTY*OFFR_SIZE) FROM bboffer where offr_grade=(select spotid from homepagespotcontract where contractid=CONT_ID)),
                    SPOT_ID=(select spotid from homepagespotcontract where contractid=CONT_ID) 
                    From CONTRACT WHERE CONT_CATG <>'Monomer' AND CONT_ENABLED='True' AND cont_id<>17 and cont_id<>10
                    ORDER BY VARSIZE DESC";

            //using (SqlConnection connTPE = new SqlConnection(Application["TPE"].ToString()))
            //{
            //    dt = DBLibrary.GetDataTableFromStoredProcedure(connTPE, "spSpotFloorSummary");
            //}
            //strExpr = "grade_id<>7 and grade_id<>5 and grade_id<>0 and grade_id<>1";                

            SQLContract = string.Format(SQLContract, fwdMonth, fwdMonth, fwdMonth, fwdMonth);

            DataSet ds = new DataSet();
            ds.Tables.Add(DBLibrary.GetDataTableFromSelect(Application["TPE"].ToString(), SQLContract));

            //string strExpr = "cont_id<>17 and cont_id<>10";//let me put this into the code above...
            /*string strSort = "VARSIZE DESC";
            DataRow[] dr = dt.Select(strExpr, strSort);
            DataTable dt2;
            foreach (DataRow d in dr) dt2.ImportRow(d);
            return dt2;*/
            return ds;
        }

        [WebMethod]
        public void GetTPEBidAsk(int contID, ref string low, ref string high, ref string weight)
        {
            DataTable dt;
            DataRow[] foundRows;
            string strExpr = "";

            using (SqlConnection connTPE = new SqlConnection(Application["TPE"].ToString()))
            {
                dt = DBLibrary.GetDataTableFromStoredProcedure(connTPE, "spSpotFloorSummary");
            }

            strExpr = "grade_id=" + contID;
            foundRows = dt.Select(strExpr);

            //if (foundRows.Length > 0)
            if (foundRows[0][3].ToString().Length > 0)
            {
                low = double.Parse(foundRows[0][3].ToString()).ToString("#0.000");
                high = double.Parse(foundRows[0][4].ToString()).ToString("#0.000");
                weight = double.Parse(foundRows[0][5].ToString()).ToString("###,000,000");
            }
            else
            {
                low = "-";
                high = "-";
                weight = "-";
            }


            //int.Parse(foundRows[i][0].ToString())


            //            using (SqlConnection conn = new SqlConnection(Application["TPE"].ToString()))
            //            {
            //                conn.Open();

            //                string sSQL = @"SELECT TOP 1 BID.BID_PRCE as BID, OFFER.OFFR_PRCE as OFFR FROM  BID INNER 
            //                                JOIN CONTRACT ON BID.BID_CONT = CONTRACT.CONT_ID INNER JOIN OFFER ON 
            //                                CONTRACT.CONT_ID = OFFER.OFFR_CONT AND BID.BID_MNTH = OFFER.OFFR_MNTH INNER JOIN FWDMONTH ON 
            //                                BID.BID_MNTH = FWDMONTH.FWD_ID WHERE     (CONTRACT.CONT_ENABLED = 'True'  AND CONTRACT.cont_id={0} 
            //                                AND FWDMONTH.FWD_ACTV=1 ) ORDER BY FWDMONTH.FWD_ID";

            //                sSQL = string.Format(sSQL, GetContractIDFromDB(contID));

            //                SqlDataReader dtr = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn, sSQL);

            //if (dtr.Read())
            //{
            //    bid = double.Parse(dtr["Bid"].ToString().Trim()).ToString("#0.000");
            //    ask = double.Parse(dtr["OFFR"].ToString().Trim()).ToString("#0.000");
            //}
        }

        /*private string GetContractIDFromDB(int contID)
        {
            string ContractID = "";

            using (SqlConnection conn = new SqlConnection(Application["TPE"].ToString()))
            {
                conn.Open();
                string sSQL = "select contractid from homepagespotcontract where spotid={0}";
                sSQL = string.Format(sSQL, contID);

                SqlDataReader dtr = TPE.Utility.DBLibrary.GetDataReaderFromSelect(conn, sSQL);

                if (dtr.Read())
                {
                    ContractID = dtr["contractid"].ToString();
                }
            }

            return ContractID;
        }*/

        [WebMethod]
        public ArrayList WebServc_GetChartData(bool bMonth, string ContID)
        {
            string sql = "";
            ArrayList list = new ArrayList();

            if (bMonth)
            {
                sql = @"select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, 
                    contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id={0} and export_price.date > DATEADD(dd,-30,getdate())  
                    ORDER BY export_price.date ";
            }
            else
            {
                sql = @"select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract 
                    where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id={0} and export_price.date  > DATEADD(dd,-365,getdate()) 
                    and datepart(dw,export_price.date) = 6";
                //                sql = @"select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,export_price.date from export_price, contract 
                //                    where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id={0} and export_price.date  > DATEADD(dd,-365,getdate()) 
                //                    and datepart(dw,export_price.date) = 6 UNION select contract.cont_labl, export_price.Cont_ID,export_price.bid,export_price.ask,
                //                    export_price.date from export_price, contract where export_price.Cont_ID=contract.Cont_ID AND export_price.cont_id={1} and 
                //                    export_price.date  > DATEADD(dd,-7,getdate()) ORDER BY export_price.date ";

            }

            if (bMonth)
            {
                sql = string.Format(sql, ContID);
            }
            else
            {
                sql = string.Format(sql, ContID, ContID);
            }

            using (SqlConnection conn = new SqlConnection(Application["TPE"].ToString()))
            {
                SqlDataReader dtr = null;

                dtr = DBLibrary.GetDataReaderFromSelect(conn, sql);

                while (dtr.Read())
                {
                    inlist il = new inlist();
                    il.f1 = dtr["date"].ToString();
                    il.f2 = dtr["ask"].ToString();
                    list.Add(il);
                }
            }
            return list;
        }

        [WebMethod]
        public ArrayList WebServc_Get_LLDPE_HOPP(bool bMonth)
        {
            string sSQL1 = "";
            string sSQL2 = "";
            ArrayList list = new ArrayList();

            if (bMonth)
            {
                //LLDPE            
                sSQL1 = "select convert(char,date,110) as date2, convert(numeric(10,5),((bid + ask) /2)) as ask from export_price where cont_id=11 AND export_price.date between DATEADD(dd,-30,'{0}') and '{1}' order by date ASC";
                //HoPP
                sSQL2 = "select convert(char,date,110) as date2, convert(numeric(10,5),((bid + ask) /2)) as ask from export_price where cont_id=9 AND export_price.date between DATEADD(dd,-30,'{0}') and '{1}' order by date ASC";
            }
            else
            {
                //LLDPE            
                sSQL1 = "select convert(char,date,110) as date2, convert(numeric(10,5),((bid + ask) /2)) as ask from export_price where cont_id=11 AND export_price.date between DATEADD(mm,-12,'{0}') and '{1}' and datepart(dw,export_price.date) = 6 order by date ASC";
                //HoPP
                sSQL2 = "select convert(char,date,110) as date2, convert(numeric(10,5),((bid + ask) /2)) as ask from export_price where cont_id=9 AND export_price.date between DATEADD(mm,-12,'{0}') and '{1}' and datepart(dw,export_price.date) = 6 order by date ASC";
            }

            using (SqlConnection conn1 = new SqlConnection(Application["TPE"].ToString()))
            {
                SqlDataReader dtr1 = null;
                dtr1 = DBLibrary.GetDataReaderFromSelect(conn1, string.Format(sSQL1, DateTime.Now.ToShortDateString(), DateTime.Now.ToShortDateString()));

                using (SqlConnection conn2 = new SqlConnection(Application["TPE"].ToString()))
                {
                    SqlDataReader dtr2 = null;
                    dtr2 = DBLibrary.GetDataReaderFromSelect(conn2, string.Format(sSQL2, DateTime.Now.ToShortDateString(), DateTime.Now.ToShortDateString()));

                    while (dtr1.Read())
                    {
                        while (dtr2.Read())
                        {
                            if (dtr1["date2"].ToString() == dtr2["date2"].ToString())
                            {
                                inlist il = new inlist();
                                il.f1 = dtr1["date2"].ToString();
                                il.f2 = dtr1["ask"].ToString();
                                il.f3 = dtr2["ask"].ToString();
                                list.Add(il);

                                break;
                            }
                        }
                    }
                }
            }
            return list;
        }

        [WebMethod]
        public ArrayList WebServc_Get_HOPP_GPPS(bool bMonth)
        {
            string sSQL1 = "";
            string sSQL2 = "";
            ArrayList list = new ArrayList();

            if (bMonth)
            {
                //HOPP
                sSQL1 = "select convert(char,date,110) as date2, convert(numeric(10,5),((bid + ask) /2)) as ask from export_price where cont_id=9 AND export_price.date between DATEADD(dd,-30,'{0}') and '{1}' order by date ASC";
                //HoPP
                sSQL2 = "select convert(char,date,110) as date2, convert(numeric(10,5),((bid + ask) /2)) as ask from export_price where cont_id=19 AND export_price.date between DATEADD(dd,-30,'{0}') and '{1}' order by date ASC";
            }
            else
            {
                //HOPP      
                sSQL1 = "select convert(char,date,110) as date2, convert(numeric(10,5),((bid + ask) /2)) as ask from export_price where cont_id=9 AND export_price.date between DATEADD(mm,-12,'{0}') and '{1}' and datepart(dw,export_price.date) = 6 order by date ASC";
                //HoPP
                sSQL2 = "select convert(char,date,110) as date2, convert(numeric(10,5),((bid + ask) /2)) as ask from export_price where cont_id=19 AND export_price.date between DATEADD(mm,-12,'{0}') and '{1}' and datepart(dw,export_price.date) = 6 order by date ASC";
            }

            using (SqlConnection conn1 = new SqlConnection(Application["TPE"].ToString()))
            {
                SqlDataReader dtr1 = null;
                dtr1 = DBLibrary.GetDataReaderFromSelect(conn1, string.Format(sSQL1, DateTime.Now.ToShortDateString(), DateTime.Now.ToShortDateString()));

                using (SqlConnection conn2 = new SqlConnection(Application["TPE"].ToString()))
                {
                    SqlDataReader dtr2 = null;
                    dtr2 = DBLibrary.GetDataReaderFromSelect(conn2, string.Format(sSQL2, DateTime.Now.ToShortDateString(), DateTime.Now.ToShortDateString()));

                    while (dtr1.Read())
                    {
                        while (dtr2.Read())
                        {
                            if (dtr1["date2"].ToString() == dtr2["date2"].ToString())
                            {
                                inlist il = new inlist();
                                il.f1 = dtr1["date2"].ToString();
                                il.f2 = dtr1["ask"].ToString();
                                il.f3 = dtr2["ask"].ToString();
                                list.Add(il);

                                break;
                            }
                        }
                    }
                }
            }
            return list;
        }

        [WebMethod]
        public double GetTPEValue(int contid, int numberOfRecords)
        {
            clsSpreads c = new clsSpreads();
            double dValue = 0;
            string sSQL = "select top {0} bid,ask from export_price where cont_id={1} order by date desc";

            using (SqlConnection conn = new SqlConnection(HttpContext.Current.Application["TPE"].ToString()))
            {
                SqlDataReader dtr = null;
                dtr = DBLibrary.GetDataReaderFromSelect(conn, string.Format(sSQL, numberOfRecords, contid));

                while (dtr.Read())
                {
                    dValue = c.getSpreadCalcValue(Convert.ToString(dtr[0]), Convert.ToString(dtr[1]));
                    if (dValue > 0)
                    {
                        break;
                    }
                }
            }
            return dValue;
        }

        /*private ArrayList GetTPEValueHistory(int contid, int numberOfRecords)
        {
            double dValue = 0;
            ArrayList arTable = new ArrayList();
            string sSQL = "select top {0} date, bid,ask from export_price where cont_id={1} order by date desc";

            using (SqlConnection conn = new SqlConnection(HttpContext.Current.Application["TPE"].ToString()))
            {
                SqlDataReader dtr = null;
                dtr = DBLibrary.GetDataReaderFromSelect(conn, string.Format(sSQL, numberOfRecords, contid));

                while (dtr.Read())
                {
                    SpreadItems s = new SpreadItems();

                    s.dtTrans = Convert.ToDateTime(dtr[0]);
                    s.SpreadValue = getSpreadCalcValue(Convert.ToString(dtr[1]), Convert.ToString(dtr[2]));
                    arTable.Add(s);
                }
            }
            //arTable.Sort();
            return arTable;
        }*/



    }
}
